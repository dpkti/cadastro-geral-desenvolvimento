Attribute VB_Name = "Mod_CAD900"
Option Explicit

Function caractere(ByVal KeyAscii) As Integer
'fun��o que define os caracteres aceit�veis no campo "nome da carteira"



    Dim car_valido(65) As Integer
    'array para guardar os valores dos caracteres v�lidos
    Dim x As Byte   '�ndice da array
    Dim y As Byte   'valor da array


    'preenchimento da array:
    

    car_valido(0) = 8    'backspace
    car_valido(1) = 32   'espa�o


    
    'valores referentes aos caracteres de "a" a "z"
    y = 97
    
        For x = 2 To 27 Step 1
    
            car_valido(x) = y
        
            y = y + 1
        
        Next x
    '-------------------------------------------------

    'valores referentes aos caracteres de "0" a "9"
    y = 48

        For x = 28 To 37 Step 1
    
            car_valido(x) = y
        
            y = y + 1
        
        Next x
    '-------------------------------------------------


    car_valido(38) = 3    'ctrl + C
    car_valido(39) = 22   'crtl + V



    'valores referentes aos caracteres de "A" a "Z"
    y = 65
    
        For x = 40 To 65 Step 1
            
            car_valido(x) = y
                
            y = y + 1
                
        Next x
    '-------------------------------------------------


'�nicio da fun��o:

    Dim cont As Byte    'vari�vel que percorre a array
    Dim resp As Byte    'vari�vel de controle


    For cont = 0 To 65 Step 1
    
        If KeyAscii = car_valido(cont) Then
        
            resp = 1
            GoTo SIM
            
            
        Else
        
            resp = 0
        
        End If
    
    Next cont
    
        KeyAscii = 0
        caractere = KeyAscii
        Exit Function
    
    
    
SIM:
        
        If resp = 1 Then
        
        caractere = KeyAscii
        
        End If
    
    
        
End Function

Sub pr_cbonome_PESQUISA()

    OraParameters.Remove "NOME"
    OraParameters.Add "NOME", frmPesquisa.cboNomeFilial.Text, 1
    
    CmdSql = "begin producao.pck_cad900.pr_cbonome(:vCURSOR,:NOME,:vErro);end;"
    
    db.executesql CmdSql
    
    If Val(db.Parameters("vErro").Value) <> 0 Then
                
        MsgBox "Erro - " & db.Parameters("vErro").Value & ". Favor avisar Depto. de Sistemas!", 48, msg_titl
        Exit Sub
        
    End If
    
    Set db_cons = db.Parameters("vCursor").Value

End Sub


Sub pr_cmdCONFIRMA_INSERT()

    OraParameters.Remove "vCod_Carteira"
    OraParameters.Remove "vCod_Filial"
    OraParameters.Remove "vNome_Carteira"
    OraParameters.Remove "vErro"
    
    
    OraParameters.Add "vCod_Carteira", frmCad900.txtCodCar.Text, 1
    OraParameters.Add "vCod_Filial", frmCad900.cboCodFilial.Text, 1
    OraParameters.Add "vNome_Carteira", UCase(frmCad900.txtNomeCar.Text), 1
    OraParameters.Add "vErro", 0, 1
    
    
    CmdSql = "begin producao.pck_cad900.pr_cmdCONFIRMA_INSERT" & _
             "(:vCod_Carteira, :vCod_Filial, :vNome_Carteira, :vErro); end;"
             
    
    db.executesql CmdSql
    
    If Val(db.Parameters("vErro").Value) <> 0 Then
                
        Call Process_Line_Errors(SQL)
        Exit Sub
        
    End If
    
    Set db_cons = db.Parameters("vCursor").Value

End Sub

Sub pr_cmdCONFIRMA_UPDATE()
    
    OraParameters.Remove "vCod_Carteira"
    OraParameters.Remove "vCod_Filial"
    OraParameters.Remove "vNome_Carteira"
    OraParameters.Remove "vErro"
    
    
    OraParameters.Add "vCod_Carteira", frmCad900.txtCodCar.Text, 1
    OraParameters.Add "vCod_Filial", frmCad900.cboCodFilial.Text, 1
    OraParameters.Add "vNome_Carteira", UCase(frmCad900.txtNomeCar.Text), 1
    OraParameters.Add "vErro", 0, 1
    
    
    CmdSql = "begin producao.pck_cad900.pr_cmdCONFIRMA_UPDATE" & _
             "(:vCod_Carteira, :vCod_Filial, :vNome_Carteira, :vErro); end;"
             
    
    db.executesql CmdSql
    
    If Val(db.Parameters("vErro").Value) <> 0 Then
                
        Call Process_Line_Errors(SQL)
        Exit Sub
        
    End If
    
    Set db_cons = db.Parameters("vCursor").Value

End Sub

Sub pr_combocod_PESQUISA()

    OraParameters.Remove "COD"
    OraParameters.Add "COD", frmPesquisa.cboCodFilial.Text, 1
    
    
    CmdSql = "begin producao.pck_cad900.pr_cbocod(:vCURSOR,:COD,:vErro);end;"
    db.executesql CmdSql
    
    OraParameters.Remove "COD"
    
    Set db_cons = db.Parameters("vCursor").Value
    
    If Val(db.Parameters("vErro").Value) <> 0 Then
                
        Call Process_Line_Errors(SQL)
        Exit Sub
        
    End If


End Sub

Sub pr_del()

OraParameters.Remove "v_rowid"
OraParameters.Add "v_rowid", rowid, ORAPARM_INPUT
OraParameters("v_rowid").servertype = ORATYPE_VARCHAR2

CmdPl = "begin producao.pck_cad900.pr_del(:v_rowid, :verro); end;"

db.executesql CmdPl

If Val(db.Parameters("vErro").Value) <> 0 Then
    Call Process_Line_Errors(SQL)
    Exit Sub
End If

OraParameters.Remove "v_cod_cli"

End Sub

Sub pr_form_combo_codigo()

    CmdSql = "begin producao.pck_cad900.pr_form_combo_codigo(:vCURSOR, :vErro); End;"
    
    db.executesql CmdSql
    
    Set db_cons = db.Parameters("vCursor").Value
    
    If Val(db.Parameters("vErro").Value) <> 0 Then
                
        Call Process_Line_Errors(SQL)
        Exit Sub
        
    End If

End Sub


Sub pr_form_combo_nome()

    CmdSql = "begin producao.pck_cad900.pr_form_combo_nome(:vCURSOR, :vErro); End;"
    
    db.executesql CmdSql
    
    Set db_cons = db.Parameters("vCursor").Value
    
    If Val(db.Parameters("vErro").Value) <> 0 Then
                
        Call Process_Line_Errors(SQL)
        Exit Sub
                
    End If

End Sub


Sub pr_cbocod()

    OraParameters.Remove "COD"
    OraParameters.Add "COD", frmCad900.cboCodFilial.Text, 1
    
    
    CmdSql = "begin producao.pck_cad900.pr_cbocod(:vCURSOR,:COD,:vErro);end;"
    db.executesql CmdSql
    
    OraParameters.Remove "COD"
    
    Set db_cons = db.Parameters("vCursor").Value
    
    If Val(db.Parameters("vErro").Value) <> 0 Then
                
        Call Process_Line_Errors(SQL)
        Exit Sub
        
    End If


End Sub







Sub pr_cbonome()

    OraParameters.Remove "NOME"
    OraParameters.Add "NOME", frmCad900.cboNomeFilial.Text, 1
    
    CmdSql = "begin producao.pck_cad900.pr_cbonome(:vCURSOR,:NOME,:vErro);end;"
    
    db.executesql CmdSql
    
    If Val(db.Parameters("vErro").Value) <> 0 Then
                
        MsgBox "Erro - " & db.Parameters("vErro").Value & ". Favor avisar Depto. de Sistemas!", 48, msg_titl
        Exit Sub
        
    End If
    
    Set db_cons = db.Parameters("vCursor").Value


End Sub


Sub LIMPA_CAMPOS()

    With frmCad900
    
        .cboCodFilial.Text = ""
        .cboNomeFilial.Text = ""
        .txtNomeCar.Text = ""
    
    End With

End Sub


Sub pr_Grid()

    OraParameters.Remove "COD"
    OraParameters.Add "COD", frmPesquisa.cboCodFilial.Text, 1
    
    CmdSql = "begin producao.pck_cad900.pr_grid(:vCURSOR,:COD,:vErro);end;"
    
    db.executesql CmdSql
    
    If Val(db.Parameters("vErro").Value) <> 0 Then
                
        MsgBox "Erro - " & db.Parameters("vErro").Value & ". Favor avisar Depto. de Sistemas!", 48, msg_titl
        Exit Sub
        
    End If
    
    Set db_cons = db.Parameters("vCursor").Value

End Sub

Sub pr_Campos()

    OraParameters.Remove "vCod"
    OraParameters.Add "vCod", frmCad900.txtCodCar.Text, 1
    OraParameters("vCod").servertype = ORATYPE_NUMBER
            
    CmdSql = "begin producao.pck_cad900.pr_Campos(:vCursor, :vCod, :vErro); end;"
            
    db.executesql CmdSql
            
    Set db_cons = db.Parameters("vCursor").Value
            
    If Val(db.Parameters("vErro").Value) <> 0 Then
            
        Call Process_Line_Errors(SQL)
        End
        
    End If

End Sub

Sub pr_grid_2()

    OraParameters.Remove "vCOD"
    OraParameters.Add "vCOD", cod_carteira, 1
    
    
    CmdSql = "begin producao.pck_cad900.pr_grid_2(:vCursor, :vCod, :vErro); end;"
    
    db.executesql CmdSql
    
    Set db_cons = db.Parameters("vCursor").Value
    
    If Val(db.Parameters("vErro").Value) <> 0 Then
            
        Call Process_Line_Errors(SQL)
        End
        
    End If

End Sub



Sub pr_limpa()

OraParameters.Remove "v_cod_carteira"
OraParameters.Add "v_cod_carteira", cod_carteira, ORAPARM_INPUT


CmdSql = "begin pck_cad900.pr_limpa(:v_cod_carteira, :verro); end;"

db.executesql CmdSql

If Val(db.Parameters("verro").Value) <> 0 Then

    Call Process_Line_Errors(SQL)
    Exit Sub

End If

OraParameters.Remove "v_cod_carteira"

End Sub


Sub pr_sequencia()

    CmdSql = "begin producao.pck_cad900.pr_sequencia(:vCursor, :vErro); end;"
    
    db.executesql CmdSql
    
    Set db_cons = db.Parameters("vCursor").Value
    
    If Val(db.Parameters("vErro").Value) <> 0 Then
                
        Call Process_Line_Errors(SQL)
        Exit Sub
        
    End If

End Sub


Sub VALIDA_cmdCONFIRMA() 'consist�ncia para impedir o uso do bot�o "confirma" sem que haja necessidade

    With frmCad900
    
        If .txtNomeCar.Text = "" Or .cboCodFilial.Text = "" Or .cboNomeFilial.Text = "" Then
        
            .cmdConfirma.Enabled = False
        
        Else
        
            .cmdConfirma.Enabled = True
            
        End If
        
    End With
    
End Sub


