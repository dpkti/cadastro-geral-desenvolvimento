VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.MDIForm MDIFormCAD900 
   BackColor       =   &H80000003&
   Caption         =   "CAD900 - Cadastro de Carteira de Clientes"
   ClientHeight    =   3195
   ClientLeft      =   165
   ClientTop       =   735
   ClientWidth     =   4680
   Icon            =   "MDICAD900.frx":0000
   LinkTopic       =   "MDIForm1"
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   405
      Left            =   0
      TabIndex        =   1
      Top             =   2790
      Width           =   4680
      _ExtentX        =   8255
      _ExtentY        =   714
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   4050
      Top             =   30
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDICAD900.frx":030A
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDICAD900.frx":0626
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDICAD900.frx":0D7A
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDICAD900.frx":1096
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Toolbar TBMdi 
      Align           =   1  'Align Top
      Height          =   660
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   4680
      _ExtentX        =   8255
      _ExtentY        =   1164
      ButtonWidth     =   1032
      ButtonHeight    =   1005
      Appearance      =   1
      ImageList       =   "ImageList1"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   8
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "TBCadastro"
            Object.ToolTipText     =   "Cadastrar/Alterar Carteira de Clientes:"
            ImageIndex      =   3
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "TBPesquisa"
            Object.ToolTipText     =   "Pesquisar Carteira de Clientes:"
            ImageIndex      =   4
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "TBSobre"
            Object.ToolTipText     =   "Informa��es Sobre o Programa:"
            ImageIndex      =   2
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "TBSair"
            Object.ToolTipText     =   "Sair do CAD - 900:"
            ImageIndex      =   1
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuCadastro 
      Caption         =   "&Cadastro"
   End
   Begin VB.Menu mnuPesquisa 
      Caption         =   "&Pesquisa"
   End
   Begin VB.Menu mnuSobre 
      Caption         =   "&Sobre"
   End
   Begin VB.Menu mnuSair 
      Caption         =   "Sai&r"
      Begin VB.Menu mnuSair2 
         Caption         =   "Sair do CAD-900"
      End
   End
End
Attribute VB_Name = "MDIFormCAD900"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub MDIForm_Load()

On Error GoTo Trata_Erro
  
  msg_titl = "CAD900 - Cadastro de Carteira de Clientes"
  
  If App.PrevInstance Then
  
    MsgBox "Este programa j� est� aberto!", vbCritical, msg_titl
    End
    
  End If
  
  'conex�o oracle
  Set orasession = CreateObject("oracleinprocserver.xorasession")
  Set db = orasession.OpenDatabase("PRODUCAO", "cad900/prod", 0&)
  'Set db = orasession.OpenDatabase("desenv", "PRODUCAO/DES", 0&)
  
  
  Set OraParameters = db.Parameters
  OraParameters.Remove "vCursor"
  OraParameters.Add "vCursor", 0, 2
  OraParameters("vCursor").servertype = ORATYPE_CURSOR
  OraParameters("vCursor").DynasetOption = ORADYN_NO_BLANKSTRIP
  OraParameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
  
  OraParameters.Remove "vErro"
  OraParameters.Add "vErro", 0, 2
  OraParameters("vErro").servertype = ORATYPE_NUMBER
  
  OraParameters.Remove "vErroMsg"
  OraParameters.Add "vErroMsg", "", 2
  OraParameters("vErroMsg").servertype = ORATYPE_VARCHAR2
  
  
 Exit Sub

Trata_Erro:
   
   Call Process_Line_Errors(SQL)

End Sub

Private Sub MDIForm_QueryUnload(Cancel As Integer, UnloadMode As Integer)

    Set db_cons = Nothing
    db.Close
    
End Sub

Private Sub mnuCadastro_Click()

    frmCad900.Show vbModal

End Sub

Private Sub mnuPesquisa_Click()

    frmPesquisa.Show vbModal

End Sub

Private Sub mnuSair2_Click()
    
    Set db_cons = Nothing
    db.Close
    End
    
End Sub

Private Sub mnuSobre_Click()

    frmSobre.Show vbModal

End Sub

Private Sub TBMdi_ButtonClick(ByVal Button As MSComctlLib.Button)

    Select Case Button.Key
    
        Case "TBCadastro"
        
            Call mnuCadastro_Click
            
        Case "TBPesquisa"
        
            Call mnuPesquisa_Click
            
        Case "TBSobre"
        
            Call mnuSobre_Click
            
        Case "TBSair"
        
            Call mnuSair2_Click
            
    End Select

End Sub


