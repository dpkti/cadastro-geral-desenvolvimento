VERSION 5.00
Begin VB.Form frmSobre 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Informações Sobre o Programa:"
   ClientHeight    =   4605
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5280
   Icon            =   "frmSobre.frx":0000
   LinkTopic       =   "Form3"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4605
   ScaleWidth      =   5280
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdOk 
      Caption         =   "OK!"
      Height          =   525
      Left            =   2101
      TabIndex        =   1
      ToolTipText     =   "OK!"
      Top             =   3960
      Width           =   1245
   End
   Begin VB.PictureBox Picture1 
      Height          =   1065
      Left            =   1006
      Picture         =   "frmSobre.frx":0742
      ScaleHeight     =   1005
      ScaleWidth      =   3375
      TabIndex        =   0
      Top             =   210
      Width           =   3435
   End
   Begin VB.Label Label6 
      BackColor       =   &H80000009&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "supsist@dpk.com.br"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1605
      TabIndex        =   7
      Top             =   3480
      Width           =   2265
   End
   Begin VB.Label Label5 
      BackColor       =   &H80000009&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Renato H."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   2070
      TabIndex        =   6
      Top             =   2670
      Width           =   1155
   End
   Begin VB.Label Label4 
      Alignment       =   2  'Center
      BackColor       =   &H80000009&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "CAD900 -Cadastro de Carteiras de Clientes"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   255
      TabIndex        =   5
      Top             =   1830
      Width           =   4785
   End
   Begin VB.Label Label3 
      Caption         =   "Contato:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2205
      TabIndex        =   4
      Top             =   3090
      Width           =   1065
   End
   Begin VB.Label Label2 
      Caption         =   "Responsável:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   1875
      TabIndex        =   3
      Top             =   2250
      Width           =   1725
   End
   Begin VB.Label Label1 
      Caption         =   "Programa:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   2070
      TabIndex        =   2
      Top             =   1410
      Width           =   1335
   End
End
Attribute VB_Name = "frmSobre"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdOk_Click()

    Unload Me

End Sub

Private Sub Form_Load()

    SendKeys "{TAB}"

End Sub
