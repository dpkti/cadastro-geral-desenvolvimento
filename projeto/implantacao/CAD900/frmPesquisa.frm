VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmPesquisa 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Pesquisa Carteira de Clientes:"
   ClientHeight    =   2460
   ClientLeft      =   150
   ClientTop       =   435
   ClientWidth     =   9135
   Icon            =   "frmPesquisa.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2460
   ScaleWidth      =   9135
   StartUpPosition =   1  'CenterOwner
   Begin MSFlexGridLib.MSFlexGrid grdPesquisa 
      Height          =   2895
      Left            =   285
      TabIndex        =   6
      Top             =   2610
      Width           =   8565
      _ExtentX        =   15108
      _ExtentY        =   5106
      _Version        =   393216
      Rows            =   5
      FixedCols       =   0
      BackColor       =   16777215
      BackColorFixed  =   -2147483648
      GridLines       =   3
      FormatString    =   "^Cod. Carteira  |<Nome Carteira                                                                                   "
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame fraPesquisa 
      Height          =   2445
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   9135
      Begin VB.CommandButton cmdPesquisa 
         Caption         =   "Pesquisa:"
         Height          =   525
         Left            =   3945
         TabIndex        =   3
         ToolTipText     =   "Pesquisa Dados:"
         Top             =   1770
         Width           =   1245
      End
      Begin VB.ComboBox cboCodFilial 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   480
         Left            =   1410
         TabIndex        =   1
         ToolTipText     =   "Escolha o c�digo da filial"
         Top             =   1110
         Width           =   945
      End
      Begin VB.ComboBox cboNomeFilial 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   480
         Left            =   3030
         TabIndex        =   0
         ToolTipText     =   "Escolha o nome da filial"
         Top             =   1110
         Width           =   4695
      End
      Begin VB.Label lblCodFilial 
         Caption         =   "C�digo da Filial:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   645
         Left            =   1410
         TabIndex        =   5
         Top             =   300
         Width           =   1305
      End
      Begin VB.Label lblNomeFilial 
         Caption         =   "Nome da Filial:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   645
         Left            =   3030
         TabIndex        =   4
         Top             =   300
         Width           =   1215
      End
   End
   Begin VB.Menu mnuVoltar 
      Caption         =   "&Voltar"
   End
End
Attribute VB_Name = "frmPesquisa"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cboCodFilial_Click()

    With frmPesquisa
    
    Call pr_combocod_PESQUISA
        
    If db_cons.EOF = True Then
        
        Exit Sub
        
    Else
    
        .cboNomeFilial.Text = db_cons!nome_filial
        
    End If
    
    End With
    
    Set db_cons = Nothing
    

End Sub


Private Sub cboCodFilial_GotFocus()

    frmPesquisa.Height = 3165
    frmPesquisa.Top = 3960

End Sub


Private Sub cboCodFilial_KeyPress(KeyAscii As Integer)

    If IsNumeric(KeyAscii) = True Or False Then
    
        KeyAscii = 0
    
    End If

End Sub

Private Sub cboNomeFilial_Click()

    With frmPesquisa
    
    Call pr_cbonome_PESQUISA
       
    If db_cons.EOF = True Then
        
        Exit Sub
        
    Else
    
        .cboCodFilial.Text = db_cons!cod_filial
        
    End If
    
    End With
    
    Set db_cons = Nothing

End Sub


Private Sub cboNomeFilial_GotFocus()

    frmPesquisa.Height = 3165
    frmPesquisa.Top = 3960

End Sub


Private Sub cboNomeFilial_KeyPress(KeyAscii As Integer)

    If IsNumeric(KeyAscii) = True Or False Then
    
        KeyAscii = 0
    
    End If

End Sub

Private Sub cmdPesquisa_Click()

    'verifica se os campos est�o preenchidos
    If cboCodFilial.Text = "" Or cboNomeFilial.Text = "" Then
    
        MsgBox "N�o H� Dados Para Pesquisa!", vbCritical, msg_titl
        
        cboNomeFilial.SetFocus
        
    Else
        
        'carrega grid
        Call pr_Grid
        
        If db_cons.EOF = True Then
        
            MsgBox "N�o h� dados referentes a essa filial!", vbCritical, msg_titl
            
            cboCodFilial.Text = ""
            cboNomeFilial.Text = ""
            cboNomeFilial.SetFocus
            Exit Sub
            
        Else
        
            grdPesquisa.Rows = db_cons.recordcount + 1
            
            
            For i = 1 To db_cons.recordcount Step 1
            
                grdPesquisa.Row = i
                grdPesquisa.Col = 0
                grdPesquisa.Text = db_cons!cod_carteira
                grdPesquisa.Col = 1
                grdPesquisa.Text = db_cons!nome_carteira
                db_cons.movenext
                
            Next i
            
            grdPesquisa.ToolTipText = "Total de Registros: " & db_cons.recordcount
            frmPesquisa.Height = 6345
            frmPesquisa.Top = 2760
           
        
        End If
        
    End If
    
    Set db_cons = Nothing
        

End Sub

Private Sub Form_Load()

    With frmPesquisa
    
    'inicializa combo com os c�digos das filiais
            
    Call pr_form_combo_codigo
            
    If db_cons.EOF = True Then
    
        MsgBox "N�o h� dados para constru��o do campo 'C�digo da Filial'!", vbCritical, msg_titl
                
    Else
    
        For i = 1 To db_cons.recordcount
            
            .cboCodFilial.AddItem CStr(db_cons!cod_filial)
            db_cons.movenext
            
        Next i
    
    End If
    
    Set db_cons = Nothing
    
    
    
    
    'inicializa combo com o nome das filiais
    
    Call pr_form_combo_nome
            
    If db_cons.EOF = True Then
    
        MsgBox "N�o h� dados para constru��o do campo 'Nome da Filial'!", vbCritical, msg_titl
                
    Else
    
        For i = 1 To db_cons.recordcount
            
            .cboNomeFilial.AddItem CStr(db_cons!nome_filial)
            db_cons.movenext
            
        Next i
    
    End If
    
End With

Set db_cons = Nothing

End Sub

Private Sub grdPesquisa_DblClick()

    grdPesquisa.Col = 0
    cod_carteira = grdPesquisa.Text
    
    frmPesquisa.grdPesquisa.Col = 0
    frmPesquisa_2.lblCodCar.Caption = frmPesquisa.grdPesquisa.Text
    frmPesquisa.grdPesquisa.Col = 1
    frmPesquisa_2.lblNomeCar.Caption = frmPesquisa.grdPesquisa.Text
    
    Call pr_grid_2
    
    If db_cons.EOF = True Then
    
        MsgBox "N�o h� clientes relacionados a esse c�digo de carteira", vbCritical, msg_titl
        Exit Sub
        
    Else
    
        frmPesquisa_2.grdCliente.Rows = db_cons.recordcount + 1
        frmPesquisa_2.grdRowId.Rows = db_cons.recordcount + 1
    
        For i = 1 To db_cons.recordcount Step 1
    
            With frmPesquisa_2.grdCliente
    
                .Row = i
                .Col = 0
                .Text = db_cons!cod_cliente
                .Col = 1
                .Text = db_cons!nome_cliente
                                
                frmPesquisa_2.grdRowId.Row = i
                frmPesquisa_2.grdRowId.Col = 0
                frmPesquisa_2.grdRowId.Text = db_cons!rowid
                
                db_cons.movenext
                    
            End With
    
        Next i
        
        frmPesquisa_2.grdCliente.ToolTipText = "Total de Registros: " & db_cons.recordcount
        SendKeys "^{HOME}"
        frmPesquisa_2.Show vbModal
    
    End If
    
    Set db_cons = Nothing
    
End Sub


Private Sub mnuVoltar_Click()

    Unload Me

End Sub
