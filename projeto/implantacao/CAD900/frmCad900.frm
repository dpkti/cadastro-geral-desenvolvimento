VERSION 5.00
Begin VB.Form frmCad900 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cadastro/Altera��o/Consulta de Carteira de Clientes:"
   ClientHeight    =   4470
   ClientLeft      =   150
   ClientTop       =   435
   ClientWidth     =   8685
   Icon            =   "frmCad900.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4470
   ScaleWidth      =   8685
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame fraCadastro 
      Caption         =   "Cadastro:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4455
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   8685
      Begin VB.TextBox txtCodCar 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   480
         Left            =   180
         MaxLength       =   4
         TabIndex        =   0
         ToolTipText     =   "Digite o c�digo da carteira"
         Top             =   1380
         Width           =   705
      End
      Begin VB.ComboBox cboCodFilial 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   480
         Left            =   210
         TabIndex        =   3
         ToolTipText     =   "Escolha o c�digo da filial"
         Top             =   2910
         Width           =   945
      End
      Begin VB.ComboBox cboNomeFilial 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   480
         Left            =   1830
         TabIndex        =   4
         ToolTipText     =   "Escolha o nome da filial"
         Top             =   2910
         Width           =   4695
      End
      Begin VB.Frame fraButtons 
         Height          =   765
         Left            =   2940
         TabIndex        =   5
         Top             =   3510
         Width           =   2805
         Begin VB.CommandButton cmdCancela 
            Caption         =   "Cancelar:"
            Height          =   525
            Left            =   120
            TabIndex        =   7
            ToolTipText     =   "Apagar os dados"
            Top             =   180
            Width           =   1245
         End
         Begin VB.CommandButton cmdConfirma 
            Caption         =   "Confirmar:"
            Height          =   525
            Left            =   1440
            TabIndex        =   6
            ToolTipText     =   "Confirmar os dados"
            Top             =   180
            Width           =   1245
         End
      End
      Begin VB.TextBox txtNomeCar 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   1800
         MaxLength       =   40
         TabIndex        =   1
         ToolTipText     =   "Digite o nome da carteira"
         Top             =   1380
         Width           =   6705
      End
      Begin VB.Label lblCodCar 
         Caption         =   "C�digo da Carteira:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   645
         Left            =   210
         TabIndex        =   11
         Top             =   570
         Width           =   1305
      End
      Begin VB.Label lblNomeCar 
         Caption         =   "Nome da Carteira:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   645
         Left            =   1830
         TabIndex        =   10
         Top             =   570
         Width           =   1185
      End
      Begin VB.Label lblCodFilial 
         Caption         =   "C�digo da Filial:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   645
         Left            =   210
         TabIndex        =   9
         Top             =   2100
         Width           =   1305
      End
      Begin VB.Label lblNomeFilial 
         Caption         =   "Nome da Filial:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   645
         Left            =   1830
         TabIndex        =   8
         Top             =   2100
         Width           =   1215
      End
   End
   Begin VB.Menu mnuVoltar 
      Caption         =   "&Voltar"
   End
End
Attribute VB_Name = "frmCad900"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cboCodFilial_Click()

        
    Call pr_cbocod
        
    If db_cons.EOF = True Then
        
        Exit Sub
        
    Else
    
        cboNomeFilial.Text = db_cons!nome_filial
        
    End If
    
    Call VALIDA_cmdCONFIRMA
    
End Sub

Private Sub cboCodFilial_KeyPress(KeyAscii As Integer)

    If IsNumeric(KeyAscii) = True Or False Then
    
        KeyAscii = 0
    
    End If

End Sub

Private Sub cboCodFilial_LostFocus()

    Call VALIDA_cmdCONFIRMA

End Sub

Private Sub cboNomeFilial_Click()

    Call pr_cbonome
       
    If db_cons.EOF = True Then
        
        Exit Sub
        
    Else
    
        cboCodFilial.Text = db_cons!cod_filial
        
    End If
    
    Call VALIDA_cmdCONFIRMA

End Sub

Private Sub cboNomeFilial_KeyPress(KeyAscii As Integer)

    If IsNumeric(KeyAscii) = True Or False Then
    
        KeyAscii = 0
    
    End If

End Sub

Private Sub cboNomeFilial_LostFocus()

    Call VALIDA_cmdCONFIRMA

End Sub

Private Sub cmdCancela_Click()

    Call LIMPA_CAMPOS
    txtCodCar.SetFocus
    
    Call pr_sequencia
    If IsNull(db_cons!seq) = True Then
                  
        txtCodCar.Text = 1
        seq = 1
                
    Else
        
        seq = Val(db_cons!seq) + 1
    
        txtCodCar.Text = seq
                
    End If

End Sub

Private Sub cmdConfirma_Click()

    If fl_sit = 0 Then
    
        Dim resposta As Byte
        
            resposta = MsgBox("Confirma Atualiza��o dos Dados?", vbQuestion + vbYesNo, msg_titl)
            
            If resposta = 7 Then  'resposta negativa
            
                Call LIMPA_CAMPOS
                
                Call pr_sequencia
           
                If IsNull(db_cons!seq) = True Then
                  
                    txtCodCar.Text = 1
                    seq = 1
                
                Else
        
                    seq = Val(db_cons!seq) + 1
    
                    txtCodCar.Text = seq
                
                End If
                    
                txtCodCar.SetFocus
            
            End If
            
            If resposta = 6 Then 'resposta positiva
            
                Call pr_cmdCONFIRMA_UPDATE
            
                'MsgBox "Atualiza��o Efetuada com Sucesso", vbInformation, msg_titl
            
                Call cmdCancela_Click
                Exit Sub
                
            End If
            
    End If
    
    

    
    
    If fl_sit = 1 Then
    
        resposta = MsgBox("Confirma Inclus�o dos Dados?", vbQuestion + vbYesNo, msg_titl)
            
        If resposta = 7 Then  'resposta negativa
            
            Call LIMPA_CAMPOS
                
            Call pr_sequencia
           
            If IsNull(db_cons!seq) = True Then
                  
                txtCodCar.Text = 1
                seq = 1
                
            Else
        
                
            seq = Val(db_cons!seq) + 1
    
            txtCodCar.Text = seq
                
            End If
                    
        txtCodCar.SetFocus
            
        End If
            
        If resposta = 6 Then 'resposta positiva
    
            Call pr_cmdCONFIRMA_INSERT
            
            'MsgBox "Inser��o Efetuada com Sucesso", vbInformation, msg_titl
                        
            Call pr_sequencia
           
            If IsNull(db_cons!seq) = True Then
                  
                txtCodCar.Text = 1
                seq = 1
                
            Else
                    
                seq = Val(db_cons!seq) + 1
    
                txtCodCar.Text = seq
                
            End If
        
        Call LIMPA_CAMPOS
        
        txtCodCar.SetFocus
                
        Exit Sub
            
        
        End If
        
    End If

End Sub

Private Sub Form_Load()

    
    'define sequ�ncia do campo "C�digo da Carteira"
    
    Call pr_sequencia
           
    If IsNull(db_cons!seq) = True Then
                  
        txtCodCar.Text = 1
        seq = 1
                
    Else
        
        seq = Val(db_cons!seq) + 1
    
        txtCodCar.Text = seq
                
    End If
    
            
    
    
    'inicializa combo com os c�digos das filiais
            
    Call pr_form_combo_codigo
            
    If db_cons.EOF = True Then
    
        MsgBox "N�o h� dados para constru��o do campo 'C�digo da Filial'!", vbCritical, msg_titl
                
    Else
    
        For i = 1 To db_cons.recordcount
            
            cboCodFilial.AddItem CStr(db_cons!cod_filial)
            db_cons.movenext
            
        Next i
    
    End If
    
    
    
    
    'inicializa combo com o nome das filiais
    
    Call pr_form_combo_nome
            
    If db_cons.EOF = True Then
    
        MsgBox "N�o h� dados para constru��o do campo 'Nome da Filial'!", vbCritical, msg_titl
                
    Else
    
        For i = 1 To db_cons.recordcount
            
            cboNomeFilial.AddItem CStr(db_cons!nome_filial)
            db_cons.movenext
            
        Next i
    
    End If
    
    

End Sub

Private Sub mnuVoltar_Click()

    Unload Me

End Sub

Private Sub txtCodCar_GotFocus()
    
        
    Call pr_sequencia
           
    If IsNull(db_cons!seq) = True Then
                  
        txtCodCar.Text = 1
        seq = 1
                
    Else
        
        seq = Val(db_cons!seq) + 1
    
        txtCodCar.Text = seq
                
    End If
    
    Set db_cons = Nothing
    
    Call LIMPA_CAMPOS
    txtCodCar.SelStart = 0
    txtCodCar.SelLength = Len(txtCodCar.Text)
    fraCadastro.Caption = "Cadastro:"

End Sub


Private Sub txtCodCar_KeyPress(KeyAscii As Integer)

    KeyAscii = Numerico(KeyAscii)

End Sub


Private Sub txtCodCar_LostFocus()
    
    
    'verifica sequencia do campo "C�digo da Carteira"
    
    Call pr_sequencia
            
    If db_cons.EOF = True Then
                           
        MsgBox "N�o h� dados para defini��o da sequencia do campo cod_carteira", vbCritical, msg_titl
        Exit Sub
        
    Else
        
        If IsNull(db_cons!seq) = True Then
                  
            txtCodCar.Text = 1
            seq = 1
                    
        Else
        
        seq = Val(db_cons!seq) + 1
        
        End If
    
        
        If txtCodCar.Text = "" Then
        
            txtCodCar.Text = seq
            txtCodCar.SetFocus
            
            
        ElseIf Val(txtCodCar.Text) = seq Then
        
            fl_sit = 1
            
            fraCadastro.Caption = "Inclus�o de Dados:"
            
            Call LIMPA_CAMPOS
            
        ElseIf Val(txtCodCar.Text) > seq Then
        
            txtCodCar.Text = seq
            txtCodCar.SetFocus
            
                
        ElseIf Val(txtCodCar.Text) < seq Then
        
            If Val(txtCodCar.Text) = 0 Then
                txtCodCar.Text = seq
                txtCodCar.SetFocus
                Exit Sub
            End If
                
        
            fl_sit = 0
            
            fraCadastro.Caption = "Altera��o de Dados:"
            
            Call pr_Campos
            
        If db_cons.EOF = True Then
    
            MsgBox "N�o h� dados para o carregamento do grid de consulta"
        
        Else
    
            txtNomeCar.Text = db_cons!nome_carteira
            cboCodFilial.Text = db_cons!cod_filial
            cboNomeFilial.Text = db_cons!nome_filial
    
            txtNomeCar.SetFocus
    
        End If
        
        End If
    
    End If
    
    Set db_cons = Nothing
    
    Call VALIDA_cmdCONFIRMA

End Sub

Private Sub txtNomeCar_KeyPress(KeyAscii As Integer)
    
    'MsgBox KeyAscii
    
    KeyAscii = caractere(KeyAscii)
       
    Call VALIDA_cmdCONFIRMA

End Sub

Private Sub txtNomeCar_LostFocus()

    Call VALIDA_cmdCONFIRMA

End Sub


