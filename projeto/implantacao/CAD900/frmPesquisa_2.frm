VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmPesquisa_2 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Clientes Relacionados:"
   ClientHeight    =   5370
   ClientLeft      =   150
   ClientTop       =   435
   ClientWidth     =   8265
   Icon            =   "frmPesquisa_2.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5370
   ScaleWidth      =   8265
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame fraCliente 
      Caption         =   "Clientes Relacionados:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4035
      Left            =   30
      TabIndex        =   3
      Top             =   1290
      Width           =   8175
      Begin VB.PictureBox Picture1 
         Height          =   555
         Left            =   360
         Picture         =   "frmPesquisa_2.frx":030A
         ScaleHeight     =   495
         ScaleWidth      =   495
         TabIndex        =   7
         ToolTipText     =   "D� um duplo clique na linha do grid para excluir o registro do cliente!"
         Top             =   3390
         Width           =   555
      End
      Begin MSFlexGridLib.MSFlexGrid grdRowId 
         Height          =   405
         Left            =   7020
         TabIndex        =   6
         Top             =   2790
         Visible         =   0   'False
         Width           =   765
         _ExtentX        =   1349
         _ExtentY        =   714
         _Version        =   393216
         Cols            =   1
         FixedCols       =   0
         FormatString    =   "<RowId                                                      "
      End
      Begin VB.CommandButton cmdLimpar 
         Caption         =   "Limpar"
         Height          =   525
         Left            =   6660
         TabIndex        =   5
         Top             =   3390
         Width           =   1245
      End
      Begin MSFlexGridLib.MSFlexGrid grdCliente 
         Height          =   2895
         Left            =   360
         TabIndex        =   4
         Top             =   420
         Width           =   7545
         _ExtentX        =   13309
         _ExtentY        =   5106
         _Version        =   393216
         FixedCols       =   0
         BackColor       =   16777215
         BackColorFixed  =   -2147483648
         GridLines       =   3
         FormatString    =   "^Cod. Cliente:|<Nome do Cliente:                                                                "
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame fraCarteira 
      Caption         =   "Carteira:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Left            =   0
      TabIndex        =   0
      Top             =   30
      Width           =   8265
      Begin VB.Label lblNomeCar 
         Alignment       =   2  'Center
         BackColor       =   &H80000009&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   1020
         TabIndex        =   2
         ToolTipText     =   "Nome da carteira"
         Top             =   540
         Width           =   7065
      End
      Begin VB.Label lblCodCar 
         Alignment       =   2  'Center
         BackColor       =   &H80000009&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   180
         TabIndex        =   1
         ToolTipText     =   "C�digo da carteira"
         Top             =   540
         Width           =   675
      End
   End
   Begin VB.Menu mnuVoltar 
      Caption         =   "&Voltar"
   End
End
Attribute VB_Name = "frmPesquisa_2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdLimpar_Click()

Dim resposta As VbMsgBoxResult

resposta = MsgBox("Deseja excluir todos os registros de clientes dessa carteira ?", vbYesNo + vbCritical, msg_titl)

If resposta = vbNo Then

    Exit Sub
    
End If

If resposta = vbYes Then

    Call pr_limpa
    MsgBox "N�o h� mais nenhum cliente relacionado a essa carteira !", vbInformation, msg_titl
    Unload Me

End If



End Sub

Private Sub grdCliente_DblClick()

    Dim linha As Integer
    linha = grdCliente.Row
    
    grdRowId.Row = linha
    rowid = grdRowId.Text
    
            
    Dim resposta As VbMsgBoxResult
    
    resposta = MsgBox("Deseja excluir este registro ?", vbYesNo + vbQuestion, msg_titl)
        
    If resposta = vbNo Then
        Exit Sub
    End If
    
    If resposta = vbYes Then
        
        Call pr_del
        
        'refresh do grids
        
        Call pr_grid_2
    
        If db_cons.EOF = True Then
    
            MsgBox "N�o h� mais nenhum cliente relacionado a esse c�digo de carteira", vbInformation, msg_titl
            Unload Me
            Exit Sub
        
        Else
    
            frmPesquisa_2.grdCliente.Rows = db_cons.recordcount + 1
            frmPesquisa_2.grdRowId.Rows = db_cons.recordcount + 1
    
            For i = 1 To db_cons.recordcount Step 1
    
                With frmPesquisa_2.grdCliente
    
                    .Row = i
                    .Col = 0
                    .Text = db_cons!cod_cliente
                    .Col = 1
                    .Text = db_cons!nome_cliente
                    
                    grdRowId.Row = i
                    grdRowId.Col = 0
                    grdRowId.Text = db_cons!rowid
                            
                    db_cons.movenext
                    
                End With
    
            Next i
            
        
            frmPesquisa_2.grdCliente.ToolTipText = "Total de Registros: " & db_cons.recordcount
            SendKeys "^{HOME}"
        
        End If
        
    End If
    
End Sub

Private Sub mnuVoltar_Click()
    
    Unload Me
    
End Sub


Private Sub Picture1_Click()

    MsgBox "Para excluir um registro de cliente, basta clicar na linha do grid. Para excluir " & _
            "todos os registros desta carteira clique no bot�o 'Limpar'.", vbInformation, msg_titl

End Sub
