VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.MDIForm MDIMenu 
   BackColor       =   &H00E0E0E0&
   Caption         =   "CAD270 - Cadastro de Contabiliza��o"
   ClientHeight    =   4230
   ClientLeft      =   1650
   ClientTop       =   2025
   ClientWidth     =   6720
   Icon            =   "MDIMENU.frx":0000
   LinkTopic       =   "MDIForm1"
   LockControls    =   -1  'True
   WindowState     =   2  'Maximized
   Begin Threed.SSPanel pnlInf 
      Align           =   2  'Align Bottom
      Height          =   660
      Left            =   0
      TabIndex        =   0
      Top             =   3570
      Width           =   6720
      _Version        =   65536
      _ExtentX        =   11853
      _ExtentY        =   1164
      _StockProps     =   15
      ForeColor       =   192
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Alignment       =   1
   End
   Begin Threed.SSPanel pnlSup 
      Align           =   1  'Align Top
      Height          =   660
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   6720
      _Version        =   65536
      _ExtentX        =   11853
      _ExtentY        =   1164
      _StockProps     =   15
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin Threed.SSCommand cmdSair 
         Height          =   525
         Left            =   3720
         TabIndex        =   4
         Top             =   60
         Width           =   555
         _Version        =   65536
         _ExtentX        =   979
         _ExtentY        =   926
         _StockProps     =   78
         Picture         =   "MDIMENU.frx":030A
      End
      Begin Threed.SSCommand cmdConsulta 
         Height          =   525
         Left            =   810
         TabIndex        =   3
         Top             =   60
         Width           =   555
         _Version        =   65536
         _ExtentX        =   979
         _ExtentY        =   926
         _StockProps     =   78
         Picture         =   "MDIMENU.frx":0624
      End
      Begin Threed.SSCommand cmdContabilizacao 
         Height          =   525
         Left            =   120
         TabIndex        =   2
         Top             =   60
         Width           =   555
         _Version        =   65536
         _ExtentX        =   979
         _ExtentY        =   926
         _StockProps     =   78
         Picture         =   "MDIMENU.frx":093E
      End
   End
   Begin VB.Menu mnuContabilizacao 
      Caption         =   "&Contabiliza��o"
   End
   Begin VB.Menu mnuConsulta 
      Caption         =   "C&onsulta"
   End
   Begin VB.Menu mnuSobre 
      Caption         =   "So&bre"
   End
   Begin VB.Menu mnuSair 
      Caption         =   "&Sair"
   End
End
Attribute VB_Name = "MDIMenu"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdConsulta_Click()
    frmConsulta.Show vbModal
End Sub


Private Sub cmdConsulta_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    pnlInf.Caption = "Consulta de Contabiliza��o"
End Sub


Private Sub cmdContabilizacao_Click()
    frmManutencao.Show vbModal
End Sub


Private Sub cmdContabilizacao_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    pnlInf.Caption = "Inclus�o / Altera��o de Contabiliza��o"
End Sub


Private Sub cmdSair_Click()
    End
End Sub

Private Sub cmdSair_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    pnlInf.Caption = "Sair do Sistema"
End Sub


Private Sub MDIForm_Load()
    
    Dim prv_ANO_REAL As String       'RECEBE ANO ('YYYY') DA DATA REAL DO ORACLE
    Dim prv_MES_REAL As String       'RECEBE MES ('MM') DA DATA REAL DO ORACLE
    Dim prv_ANOMES_REAL As String    'RECEBE ANOMES ('YYYYMM') DA DATA REAL DO ORACLE
    
    On Error GoTo TRATA_ERRO
    
    '///////////////////////////////////////////////////
    'ESTIPULA CABE�ALHO PADR�O PARA TODAS AS MENSAGENS
    '///////////////////////////////////////////////////
    MSG_TIT = "CAD270 - Cadastro de Contabiliza��o"
    
    '//////////////////////////////////////////////////////////
    'VERIFICA SE J� EXISTE UMA INST�NCIA DO PROGRAMA EM ABERTO
    '//////////////////////////////////////////////////////////
    If App.PrevInstance Then
        MsgBox "J� existe uma inst�ncia do programa no ar.", , MSG_TIT
        End
    End If

    '//////////////////////////////////////////
    '///  ESTABELECE CONEC��O COM O ORACLE  ///
    '//////////////////////////////////////////
    Set dbOra = CreateObject("oracleinproCServer.xorasession")
   'Set db = dbOra.OpenDatabase("SDPK", "CAD270/PROD", 0&)
   'Set db = dbOra.OpenDatabase("DESENV_NOVO", "PRODUCAO/DES", 0&)
    Set db = dbOra.OpenDatabase("PRODUCAO", "CAD270/PROD", 0&)

    
    '8 -> PARA UMA LINHA
    '0 -> PARA VARIAS LINHAS
    Set db_CONS = db.dbCreateDynaset("SELECT DT_REAL FROM DATAS", 8&)
    
    '////////////////////////////////////////////
    'DATAS
    '////////////////////////////////////////////
    'DATA DO BANCO NO FORMATO ('DD/MM/YY')
    'DataReal = db_CONS!DT_REAL
    DataReal = Format(db_CONS!DT_REAL, "dd/mm/yyyy")
    
    'M�S ('MM'), ANO ('YYYY') E ANOM�S ('YYYYMM') DO BANCO
    prv_MES_REAL = Format(Mid(DataReal, 4, 2), "00")
    prv_ANO_REAL = Year(DataReal)
    prv_ANOMES_REAL = prv_ANO_REAL & prv_MES_REAL
    
    'DATA DO BANCO NO FORMATO ('DD/MM/YYYY')
    def_DATAREAL_YYYY = Format(Mid(DataReal, 1, 2), "00") & "/" & prv_MES_REAL & "/" & Year(DataReal)
    
    Exit Sub
   

TRATA_ERRO:
    Call Process_Line_Errors

End Sub


Private Sub mnuConsulta_Click()
    Call cmdConsulta_Click
End Sub

Private Sub mnuContabilizacao_Click()
    Call cmdContabilizacao_Click
End Sub


Private Sub mnuSair_Click()
    Call cmdSair_Click
End Sub

Private Sub mnuSobre_Click()
    frmSobre.Show vbModal
End Sub


Private Sub pnlSup_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    pnlInf.Caption = ""
End Sub


