VERSION 5.00
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "msmask32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Begin VB.Form frmImport 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Importa��o do Arquivo"
   ClientHeight    =   1725
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5805
   ControlBox      =   0   'False
   Icon            =   "frmImport.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   115
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   387
   Begin VB.Frame fra 
      Caption         =   "Arquivo para Leitura"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1230
      Left            =   45
      TabIndex        =   1
      Top             =   90
      Width           =   5685
      Begin Bot�o.cmd cmd1 
         Height          =   690
         Left            =   135
         TabIndex        =   2
         TabStop         =   0   'False
         ToolTipText     =   "Abrir"
         Top             =   360
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmImport.frx":23D2
         PICN            =   "frmImport.frx":23EE
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MSMask.MaskEdBox txtArquivo 
         Height          =   330
         Left            =   1035
         TabIndex        =   3
         Top             =   585
         Width           =   4335
         _ExtentX        =   7646
         _ExtentY        =   582
         _Version        =   393216
         Appearance      =   0
         MaxLength       =   14
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   "###,###,##0.00"
         PromptChar      =   "�"
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   1395
      Width           =   5805
      _ExtentX        =   10239
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   10186
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComDlg.CommonDialog CMMDIALOG 
      Left            =   4950
      Top             =   90
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
End
Attribute VB_Name = "frmImport"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim strcaminho As String
Dim strNomeArq As String
Dim strCaminhoNovo As String
Dim strcaminhoBackup As String
Dim TEXTO As String

Dim cod_cliente As Double       ''122         15
Dim Rating_atual As String      ''152         02
Dim Rating_anterior As String   ''154         02
Dim Dt_alteracao As String      ''156         08


Private Sub cmd1_Click()

    On Error GoTo Trata_Erro

          Dim LINHAS() As String

1         CMMDIALOG.FileName = ""

2         CMMDIALOG.ShowOpen
          
3         strcaminho = CMMDIALOG.FileName
4         strNomeArq = CMMDIALOG.FileTitle
          
          
5         'txtArquivo.Text = strcaminho
          
6         Screen.MousePointer = 11
          
          'Verifica se foi indicado o diret�rio e nome do arquivo a ser lido
7         If strcaminho = "" Then
              'MsgBox "Indique o arquivo a ser aberto", , MSG_TIT
              'Screen.MousePointer = 0
8             Screen.MousePointer = 0
9             Exit Sub
10        End If
          
11        Screen.MousePointer = 11
              
12        strCaminhoNovo = "H:\oracle\dados\32bits\arquivo\equifax\" & strNomeArq
13        strcaminhoBackup = "H:\oracle\dados\32bits\arquivo\equifax\salva\" & strNomeArq
              
14        'FileCopy strcaminho, strCaminhoNovo
          
15        vSql = "PRODUCAO.PCK_CAD025.PR_INC_EQUIFAX(:PM_COD_CLI,:PM_RAT_ATU,:PM_RAT_ANT,:DT_ULT_ALT,:PM_CODERRO,:PM_TXTERRO)"
          
16        linha = ""
17        TEXTO = ""
          
18        Open strCaminhoNovo For Input As #1
19        Input #1, TEXTO
20        Close #1
          
21        LINHAS = Split(TEXTO, Chr(10))
          
22        For i = 0 To UBound(LINHAS) - 1
          
23            If Trim(LINHAS(i)) <> "" Then
                  
24                cod_cliente = Mid(LINHAS(i), 122, 15)
25                Rating_atual = Mid(LINHAS(i), 152, 2)
26                Rating_anterior = Trim(Mid(LINHAS(i), 154, 2))
27                Dt_alteracao = Trim(Mid(LINHAS(i), 156, 8))
                  
28                vBanco.Parameters.Remove "PM_COD_CLI"
29                vBanco.Parameters.Add "PM_COD_CLI", cod_cliente, 1
30                vBanco.Parameters.Remove "PM_RAT_ATU"
31                vBanco.Parameters.Add "PM_RAT_ATU", Rating_atual, 1
32                vBanco.Parameters.Remove "PM_RAT_ANT"
33                vBanco.Parameters.Add "PM_RAT_ANT", IIf(Rating_anterior = "", Null, Rating_anterior), 1
34                vBanco.Parameters.Remove "DT_ULT_ALT"
35                vBanco.Parameters.Add "DT_ULT_ALT", IIf(Dt_alteracao = "", Null, Dt_alteracao), 1
                  
36                vBanco.Parameters.Remove "PM_CODERRO"
37                vBanco.Parameters.Add "PM_CODERRO", 0, 2
38                vBanco.Parameters.Remove "PM_TXTERRO"
39                vBanco.Parameters.Add "PM_TXTERRO", "", 2
                  
                  
40                vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
               
41                vErro = IIf(vBanco.Parameters("PM_CODERRO") = "", 0, vBanco.Parameters("PM_CODERRO"))
          
42                If vErro <> 0 Then
43                    MsgBox vBanco.Parameters("PM_TXTERRO") & ".Ligue para o Depto de Sistemas"
44                    Screen.MousePointer = 0
45                    Exit Sub
46                End If
47            End If
          
48        Next
          
49        vSql = "PRODUCAO.PCK_CAD025.PR_INC_CONTROLE(:PM_CODERRO,:PM_TXTERRO)"
          
50        vBanco.Parameters.Remove "PM_CODERRO"
51        vBanco.Parameters.Add "PM_CODERRO", 0, 2
52        vBanco.Parameters.Remove "PM_TXTERRO"
53        vBanco.Parameters.Add "PM_TXTERRO", "", 2
              
54        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
          
55        vErro = IIf(vBanco.Parameters("PM_CODERRO") = "", 0, vBanco.Parameters("PM_CODERRO"))
          
56        If vErro <> 0 Then
57            MsgBox vBanco.Parameters("PM_TXTERRO") & ".Ligue para o Depto de Sistemas"
58            Screen.MousePointer = 0
59            Exit Sub
60        End If
          
61        FileCopy strCaminhoNovo, strcaminhoBackup
62        Kill strCaminhoNovo
          
63        Screen.MousePointer = 0
          
64        MsgBox "Arquivo importado com sucesso"
          
65        vSql = "PRODUCAO.PR_CLASS_EQUIFAX"
          
66        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
          
67        If Val(vErro) <> 0 Then
68            MsgBox vBanco.Parameters("PM_TXTERRO") & ".Ligue para o Depto de Sistemas"
69            Screen.MousePointer = 0
70            Exit Sub
71        End If
          
          
72        MsgBox "Banco de dados DPK atualizado com as informa��es Equifax"
          
73        txtArquivo.Text = ""
74        Exit Sub

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub Cmd1_Click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If

End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    
    Me.Top = 0
    Me.Left = 0
    
End Sub
