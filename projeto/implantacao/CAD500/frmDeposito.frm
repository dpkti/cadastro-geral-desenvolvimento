VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form frmDeposito 
   Caption         =   "Escolha do CD de Apoio"
   ClientHeight    =   1635
   ClientLeft      =   3165
   ClientTop       =   3480
   ClientWidth     =   4830
   ClipControls    =   0   'False
   LinkTopic       =   "Form1"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   1635
   ScaleWidth      =   4830
   Begin VB.ComboBox cmbDepositos 
      Height          =   315
      Left            =   405
      TabIndex        =   0
      Top             =   540
      Width           =   4020
   End
   Begin Threed.SSCommand sscmdVoltar 
      Height          =   465
      Left            =   2160
      TabIndex        =   2
      Top             =   1035
      Width           =   645
      _Version        =   65536
      _ExtentX        =   1138
      _ExtentY        =   820
      _StockProps     =   78
      Caption         =   "Voltar"
      ForeColor       =   8388608
   End
   Begin VB.Label lblDepositos 
      Caption         =   "Escolha o Dep�sito de Apoio para a Regional:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   240
      Left            =   405
      TabIndex        =   1
      Top             =   270
      Width           =   3975
   End
End
Attribute VB_Name = "frmDeposito"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmbDepositos_Click()
    CLOJA = Mid(cmbDepositos.Text, 1, 2)
    sscmdVoltar.Visible = True
End Sub


Private Sub Form_Load()
    sscmdVoltar.Visible = False
   'Busca de CD�s para montar Combo
    CmdSql = "Select distinct COD_LOJA,NOME_FANTASIA from loja order by cod_loja "
    Set db_CONS = db.dbcreatedynaset(CmdSql, 0&)
    
    If db_CONS.EOF Then
        MsgBox "N�O FOI ENCONTRADO NENHUM DEP�SITO. ENTRAR EM CONTATO COM DEPTO. DE SUPORTE!"
        End
    Else
        Do While Not db_CONS.EOF
            cmbDepositos.AddItem Format(db_CONS!COD_LOJA, "00") & " - " & db_CONS!NOME_FANTASIA
            db_CONS.MoveNext
        Loop
    End If
End Sub


Private Sub sscmdvoltar_Click()
    Unload Me
End Sub


