VERSION 5.00
Begin VB.Form FrmGerente 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Selecione um Gerente"
   ClientHeight    =   1500
   ClientLeft      =   4065
   ClientTop       =   4635
   ClientWidth     =   4110
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   1500
   ScaleWidth      =   4110
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdSair 
      Caption         =   "Fechar"
      Height          =   495
      Left            =   2280
      TabIndex        =   3
      Top             =   840
      Width           =   1215
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "Gravar Gerente"
      Height          =   495
      Left            =   720
      TabIndex        =   1
      Top             =   840
      Width           =   1335
   End
   Begin VB.ComboBox cmbGerente 
      Height          =   315
      ItemData        =   "FrmGerente.frx":0000
      Left            =   120
      List            =   "FrmGerente.frx":0018
      TabIndex        =   0
      Top             =   360
      Width           =   3855
   End
   Begin VB.Label lblGerenteAtual 
      AutoSize        =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   1320
      TabIndex        =   4
      Top             =   120
      Width           =   75
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Gerente Atual: "
      Height          =   195
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   1065
   End
End
Attribute VB_Name = "FrmGerente"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim Rst As Object

Private Sub cmdOK_Click()
    Dim vQtd As Integer
        
    vContinuar = False
        
    If cmbGerente = "" Then
        MsgBox "Selecione um gerente.", , "Aten��o"
        cmbGerente.SetFocus
        Exit Sub
    End If

    db.Parameters.Remove "COD_GERENTE"
    db.Parameters.Remove "COD_FILIAL"
    db.Parameters.Remove "Cod_ErrorPl"
    db.Parameters.Remove "Txt_ErrorPl"
    db.Parameters.Add "COD_GERENTE", CDbl(cmbGerente.ItemData(cmbGerente.ListIndex)), 1
    db.Parameters.Add "COD_FILIAL", CDbl(frmCadFil.txtCodFilial), 1
    db.Parameters.Add "Cod_ErrorPl", 0, 2
    db.Parameters.Add "Txt_ErrorPl", "", 2
    
    If Trim(lblGerenteAtual.Caption) = "" Then
        CmdSql = "BEGIN "
        CmdSql = CmdSql & "INSERT INTO r_gerente_nacional (COD_GERENTE, COD_REGIONAL) " & _
                          "VALUES (:COD_GERENTE, :COD_FILIAL);"
        CmdSql = CmdSql & "Commit; " & _
                          "Exception " & _
                          "When Others then " & _
                          "RollBack; " & _
                          ":Cod_ErrorPl := SQLCODE; " & _
                          ":Txt_ErrorPl := SQLERRM; " & _
                          "End;"
        db.ExecuteSQL CmdSql
        
        VErro = IIf((db.Parameters("cod_errorpl").Value = Null Or db.Parameters("cod_errorpl").Value = ""), 0, db.Parameters("cod_errorpl").Value)
        If Val(VErro) <> 0 Then
            Screen.MousePointer = 0
            MsgBox "Erro - " & db.Parameters("Txt_ErrorPl") & ". Favor avisar Depto. de Sistemas!", vbExclamation, MSG_TIT
        Else
            Screen.MousePointer = 0
        End If
    Else
    
        db.Parameters.Remove "COD_GERENTE_ANTIGO"
        db.Parameters.Add "COD_GERENTE_ANTIGO", lblGerenteAtual.Tag, 1
    
        db.ExecuteSQL "Delete from R_GERENTE_NACIONAL WHERE COD_GERENTE = :COD_GERENTE_ANTIGO and COD_REGIONAL=:COD_FILIAL"
    
        CmdSql = "BEGIN "
        CmdSql = CmdSql & "INSERT INTO r_gerente_nacional (COD_GERENTE, COD_REGIONAL) " & _
                          "VALUES (:COD_GERENTE, :COD_FILIAL);"
        CmdSql = CmdSql & "Commit; " & _
                          "Exception " & _
                          "When Others then " & _
                          "RollBack; " & _
                          ":Cod_ErrorPl := SQLCODE; " & _
                          ":Txt_ErrorPl := SQLERRM; " & _
                          "End;"
        db.ExecuteSQL CmdSql

        VErro = IIf((db.Parameters("cod_errorpl").Value = Null Or db.Parameters("cod_errorpl").Value = ""), 0, db.Parameters("cod_errorpl").Value)
        If Val(VErro) <> 0 Then
            Screen.MousePointer = 0
           MsgBox "Erro - " & db.Parameters("Txt_ErrorPl") & ". Favor avisar Depto. de Sistemas!", vbExclamation, MSG_TIT
        Else
            Screen.MousePointer = 0
        End If

    End If
    vContinuar = True
    Unload Me
End Sub

Private Sub cmdSair_Click()
    vContinuar = False
    Unload Me
End Sub

Private Sub Form_Load()
    
    Dim I As Integer
    
    Me.Top = (MDICadfr.Height - Me.Height) / 2
    Me.Left = (MDICadfr.Width - Me.Width) / 2

    db.Parameters.Remove "COD_FILIAL"
    db.Parameters.Add "COD_FILIAL", CDbl(frmCadFil.txtCodFilial), 1
    
    Set Rst = db.dbcreatedynaset("Select COD_GERENTE from r_gerente_nacional WHERE COD_REGIONAL = :COD_FILIAL", 0&)

    If Rst.EOF = False Then
        For I = 0 To cmbGerente.ListCount - 1
            If Rst!cod_Gerente = cmbGerente.ItemData(I) Then
                cmbGerente.ListIndex = I
                lblGerenteAtual.Caption = cmbGerente
                lblGerenteAtual.Tag = Rst!cod_Gerente
                Exit For
            End If
        Next
    End If
End Sub

