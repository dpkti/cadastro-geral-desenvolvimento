VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsAgrupada"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'SDS1889 - Ricardo Gomes
'Classe criada para futura reutiliza��o de c�digo
 

'---------------------------------------------------------------------------------------
' Procedure : Lojas
' Author    : ricardo.gs
' Date      : 22/2/2012
' Purpose   : SDS1889 - Popula o listbox de rela��o de lojas
'---------------------------------------------------------------------------------------
'
Public Sub Lojas(lst As ListBox)

  db_cons.MoveFirst
  For i = 1 To db_cons.RecordCount
    
    lst.AddItem (Format(db_cons("cod_loja"), "00") & "-" & db_cons("nome_fantasia"))
    db_cons.MoveNext
  Next

  For i = 0 To lst.ListCount - 1
    lst.Selected(i) = True
  Next

  lst.TopIndex = 0
End Sub


'---------------------------------------------------------------------------------------
' Procedure : planos
' Author    : ricardo.gs
' Date      : 22/2/2012
' Purpose   : SDS1889 - Popula o grid com os tipos de planos de pagamentos
'---------------------------------------------------------------------------------------
'
Public Sub Planos(opcao As Byte, grd As MSFlexGrid, Optional cod_plano As Integer)
  
  Call pr_con_planos(opcao)
  
  With grd
    .Rows = db_cons.RecordCount + 1
    For i = 1 To db_cons.RecordCount Step 1
      .Row = i
      .Col = 0
      .Text = db_cons!cod_plano
      .Col = 1
      .Text = db_cons!desc_plano
      .Col = 2
      .Text = db_cons!prazo_medio
      db_cons.MoveNext
    Next i
  End With
End Sub


'---------------------------------------------------------------------------------------
' Procedure : Intervalos
' Author    : ricardo.gs
' Date      : 22/2/2012
' Purpose   : SDS1889 - Popula o grid de intervalos cadastrados
'---------------------------------------------------------------------------------------
'
Public Sub Intervalos(opcao As Byte, grd As MSFlexGrid, planoAgrup As Integer)
  
  Call pr_con_faturamento_agrupado(opcao, 0, 0, 0, 0, planoAgrup)

  If db_cons.EOF = True Then
    Exit Sub
  End If
  
  grd.Rows = 1
  
  Do While Not db_cons.EOF
    grd.AddItem Trim(db_cons!cod_loja) & vbTab & Trim(db_cons!cod_cliente) & vbTab _
      & Replace(Format(db_cons!vl_inicial, "0.00"), ",", ".") & vbTab & Replace(Format(db_cons!vl_final, "0.00"), ",", ".") _
      & vbTab & Trim(CStr(db_cons!cod_plano) & " - " & CStr(db_cons!desc_plano))
    db_cons.MoveNext
    
  Loop
  
  
  
  
  
  
  '  With grd
  '    .Rows = db_cons.RecordCount + 1
  '    grdRow.Rows = db_cons.RecordCount + 1
  '    .ToolTipText = "Registros: " & db_cons.RecordCount
  '
  '    For i = 1 To db_cons.RecordCount
  '      .Row = i
  '      grdRow.Row = i
  '      .Col = 0: .Text = Trim(db_cons!cod_loja)
  '      .Col = 1: .Text = Replace(db_cons!vl_inicial, ",", ".")
  '      .Col = 2: .Text = Replace(db_cons!vl_final, ",", ".")
  '      .Col = 3: .Text = Trim(CStr(db_cons!cod_plano) & " - " & CStr(db_cons!desc_plano))
  '      grdRow.Text = db_cons!rowid
  '      db_cons.MoveNext
  '    Next i
  '  End With
        
End Sub


'---------------------------------------------------------------------------------------
' Procedure : MfgSort
' Author    : ricardo.gs
' Date      : 22/2/2012
' Purpose   : SDS1889 - Classifica o grid de plano de pagamento pela coluna selecionada
'---------------------------------------------------------------------------------------
'
Public Sub MfgSort(vMfg As MSFlexGrid)
    
  Dim vType As String
    
  With vMfg
    
    If Left(.TextMatrix(0, .MouseCol), 1) = ">" Then
      .TextMatrix(0, .MouseCol) = "< " & Mid(.TextMatrix(0, .MouseCol), 3)
      vType = "A"
      GoTo label_sort
    ElseIf Left(.TextMatrix(0, .MouseCol), 1) = "<" Then
      .TextMatrix(0, .MouseCol) = "> " & Mid(.TextMatrix(0, .MouseCol), 3)
      vType = "D"
      GoTo label_sort
    Else
      .FormatString = .FormatString
      .TextMatrix(0, .MouseCol) = "> " & .TextMatrix(0, .MouseCol)
      vType = "D"
      GoTo label_sort
    End If
    
label_sort:

    If IsNumeric(.Text) Then
    
      If InStr(1, .Text, ".") > 0 Then
    
        For i = 1 To vMfg.Rows - 1
            
          .Row = i
          .Text = Format(.Text, "0.00")
            
        Next
        
      End If
    
      If vType = "A" Then
        .Sort = flexSortGenericAscending
      Else
        .Sort = flexSortNumericDescending
      End If
        
      If InStr(1, .Text, ".") > 0 Then
        
        For i = 1 To vMfg.Rows - 1
        
          .Row = i
          .Text = Format(.Text, "standard")
          
        Next
          
      End If
    
    Else
    
      If vType = "A" Then
        .Sort = flexSortStringAscending
      Else
        .Sort = flexSortStringDescending
      End If
    
    End If
    
  End With

End Sub

'---------------------------------------------------------------------------------------
' Procedure : ExibiPlano
' Author    : ricardo.gs
' Date      : 22/2/2012
' Purpose   : SDS1889 - Exibe o plano de pagamento selecionado
'---------------------------------------------------------------------------------------
'
Public Sub ExibiPlano(grd As MSFlexGrid, lbl As Label)
  With grd
    .Col = 0
    lbl.Caption = Mid(lbl.Caption, 1, 19) & .Text
  End With
End Sub


'---------------------------------------------------------------------------------------
' Procedure : SelTextBox
' Author    : ricardo.gs
' Date      : 22/2/2012
' Purpose   : SDS1889- Selecina todo o texto
'---------------------------------------------------------------------------------------
'
Public Sub SelTextBox(txt As TextBox)
  txt.SelStart = 0
  txt.SelLength = Len(txt.Text)
End Sub


'---------------------------------------------------------------------------------------
' Procedure : InputValor
' Author    : ricardo.gs
' Date      : 22/2/2012
' Purpose   : SDS1889 - Padroniza os dados inputados como valor
'---------------------------------------------------------------------------------------
'
Public Function InputValor(KeyAscii As Integer, txt As TextBox) As Integer

  If Asc(",") = KeyAscii Then
    KeyAscii = Asc(".")
  End If
  InputValor = Valor(KeyAscii, txt.Text)
End Function


'---------------------------------------------------------------------------------------
' Procedure : FormataValor
' Author    : ricardo.gs
' Date      : 22/2/2012
' Purpose   : SDS1889 - Formata os campos com valor
'---------------------------------------------------------------------------------------
'
Public Sub FormataValor(txt As TextBox)
  
  If txt.Text = "" Then
    If txt.Name <> "txtVl_FinalCl" Then
      txt.Text = 0
    Else
      txt.Text = 999999999.99
    End If
  ElseIf txt.Text > 999999999.99 Then
    txt.Text = 0
  End If
  txt.Text = Format(txt.Text, "0.00")

  
End Sub


'---------------------------------------------------------------------------------------
' Procedure : ValidarValor
' Author    : ricardo.gs
' Date      : 22/2/2012
' Purpose   : SDS1889 - Valida se os valores informados
'---------------------------------------------------------------------------------------
'
Public Function ValidarValor(valorIni As Double, valorFinal As Double) As Boolean
  
  If valorIni > valorFinal Then
    ValidarValor = False
  Else
    ValidarValor = True
  End If
End Function


'---------------------------------------------------------------------------------------
' Procedure : SelLoja
' Author    : ricardo.gs
' Date      : 22/2/2012
' Purpose   : SDS1889 - Marca ou desmarca todos as lojas
'---------------------------------------------------------------------------------------
'
Public Sub SelLoja(lst As ListBox, btn As CommandButton)
  If btn.Caption = "Todos" Then
    For i = 0 To lst.ListCount - 1
      lst.Selected(i) = True
    Next
    lst.TopIndex = 0
    btn.Caption = "Desmarcar"
  ElseIf btn.Caption = "Desmarcar" Then
    For i = 0 To lst.ListCount - 1
      lst.Selected(i) = False
    Next
    lst.TopIndex = 0
    btn.Caption = "Todos"
  End If
End Sub


'---------------------------------------------------------------------------------------
' Procedure : BuscaCliente
' Author    : ricardo.gs
' Date      : 22/2/2012
' Purpose   : Busca o nome do cliente e a flag fl_agrup_diferenciada
'---------------------------------------------------------------------------------------
'
Public Function BuscaCliente(codCliente As Long) As Object
  
  'Define o par�metro
  db.Parameters.Remove "codCliente"
  db.Parameters.Add "codCliente", codCliente, 1
  
  'Executa a procedure que busca os dados do cliente
  sql = "begin producao.pck_cad080.pr_con_cliente(:vCursor, :codCliente, :vErro, :vDesc_Erro); end;"
  db.ExecuteSQL sql
  
  If Val(db.Parameters("vErro").Value) <> 0 Then
    Call Process_Line_Errors
  Else
    Set BuscaCliente = db.Parameters("vCursor").Value
  End If
End Function
  
  
Public Function AtuAgrupClient(opcao As String, cod_loja As Byte, cod_cliente As Long, _
vl_inicial As String, vl_final As String, cod_plano As Integer, cod_plano_agrup As Integer) As Boolean

  On Error GoTo trata_erro

  db.Parameters.Remove "vOpcao"
  db.Parameters.Remove "vCodLoja"
  db.Parameters.Remove "vCodCliente"
  db.Parameters.Remove "vVlInicial"
  db.Parameters.Remove "vVlFinal"
  db.Parameters.Remove "vCodPlano"
  db.Parameters.Remove "vCodPlanoAgrup"

  db.Parameters.Add "vOpcao", opcao, 1
  db.Parameters.Add "vCodLoja", cod_loja, 1
  db.Parameters.Add "vCodCliente", cod_cliente, 1
  db.Parameters.Add "vVlInicial", vl_inicial, 1
  db.Parameters.Add "vVlFinal", vl_final, 1
  db.Parameters.Add "vCodPlano", cod_plano, 1
  db.Parameters.Add "vCodPlanoAgrup", cod_plano_agrup, 1

  sql = "begin producao.pck_cad080.pr_atu_agrup_cliente(:vOpcao, :vCodLoja, :vCodCliente, :vVlInicial, " _
  & ":vVlFinal, :vCodPlano, :vCodPlanoAgrup, :vErro, :vDesc_Erro); end;"
  
  db.ExecuteSQL sql

  If Val(db.Parameters("vErro").Value) <> 0 Then
    Err.Number = Val(db.Parameters("vErro").Value)
    Call Process_Line_Errors(1, cod_loja, cod_cliente)
    AtuAgrupClient = False
  Else
    AtuAgrupClient = True
  End If

  Exit Function
trata_erro:
  Call Process_Line_Errors

  
End Function

