VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmFaturamento_Agrup 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Dados do Faturamento Agrupado:"
   ClientHeight    =   8190
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9240
   Icon            =   "frmFaturamento_Agrup.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8190
   ScaleWidth      =   9240
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin TabDlg.SSTab tbsAgrupada 
      Height          =   8175
      Left            =   120
      TabIndex        =   39
      Top             =   0
      Width           =   8895
      _ExtentX        =   15690
      _ExtentY        =   14420
      _Version        =   393216
      Tabs            =   2
      Tab             =   1
      TabHeight       =   520
      TabCaption(0)   =   "Agrupada:"
      TabPicture(0)   =   "frmFaturamento_Agrup.frx":014A
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "fraAgrupada"
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Agrupada por Cliente:"
      TabPicture(1)   =   "frmFaturamento_Agrup.frx":0166
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "fraAgrupadaCl"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).ControlCount=   1
      Begin VB.Frame fraAgrupadaCl 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   7725
         Left            =   120
         TabIndex        =   18
         Top             =   360
         Width           =   8655
         Begin VB.TextBox txtNomeCliente 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   1680
            Locked          =   -1  'True
            MaxLength       =   30
            TabIndex        =   32
            ToolTipText     =   "Nome do Cliente"
            Top             =   360
            Width           =   3315
         End
         Begin VB.TextBox txtCodCliente 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   840
            MaxLength       =   6
            TabIndex        =   19
            ToolTipText     =   "C�difo do cliente"
            Top             =   360
            Width           =   795
         End
         Begin VB.Frame fraIntervalosCl 
            Caption         =   "Intervalos j� cadastrados:"
            Height          =   1845
            Left            =   120
            TabIndex        =   36
            Top             =   5160
            Width           =   8400
            Begin MSFlexGridLib.MSFlexGrid grdIntervalosCl 
               Height          =   1455
               Left            =   60
               TabIndex        =   25
               TabStop         =   0   'False
               Top             =   240
               Width           =   8265
               _ExtentX        =   14579
               _ExtentY        =   2566
               _Version        =   393216
               Cols            =   5
               FixedCols       =   0
               Enabled         =   -1  'True
               GridLines       =   2
               SelectionMode   =   1
               AllowUserResizing=   1
               BorderStyle     =   0
               FormatString    =   $"frmFaturamento_Agrup.frx":0182
            End
         End
         Begin VB.Frame fraCdCl 
            Caption         =   "Selecione um ou mais CD�s:"
            Height          =   1545
            Left            =   1223
            TabIndex        =   35
            Top             =   3570
            Width           =   5865
            Begin VB.ListBox lstLojasCl 
               Appearance      =   0  'Flat
               BeginProperty Font 
                  Name            =   "Courier New"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   990
               ItemData        =   "frmFaturamento_Agrup.frx":020D
               Left            =   1890
               List            =   "frmFaturamento_Agrup.frx":020F
               Style           =   1  'Checkbox
               TabIndex        =   23
               Top             =   360
               Width           =   2445
            End
            Begin VB.CommandButton cmdTodosNenhumCl 
               Caption         =   "Desmarcar"
               Height          =   315
               Left            =   4830
               Picture         =   "frmFaturamento_Agrup.frx":0211
               TabIndex        =   24
               Top             =   1140
               Width           =   975
            End
         End
         Begin VB.Frame fraValoresCl 
            Caption         =   "Informe os valores:"
            Height          =   855
            Left            =   1223
            TabIndex        =   29
            Top             =   2670
            Width           =   5865
            Begin VB.TextBox txtVl_InicialCL 
               Appearance      =   0  'Flat
               BeginProperty Font 
                  Name            =   "Courier New"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               Left            =   990
               MaxLength       =   12
               TabIndex        =   21
               ToolTipText     =   "Digite o Valor Inicial:"
               Top             =   450
               Width           =   1875
            End
            Begin VB.TextBox txtVl_FinalCl 
               Appearance      =   0  'Flat
               BeginProperty Font 
                  Name            =   "Courier New"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               Left            =   3000
               MaxLength       =   12
               TabIndex        =   22
               ToolTipText     =   "Digite o Valor Final:"
               Top             =   450
               Width           =   1875
            End
            Begin VB.Label Label4 
               BackStyle       =   0  'Transparent
               Caption         =   "Valor Inicial:"
               Height          =   165
               Left            =   990
               TabIndex        =   30
               Top             =   270
               Width           =   1245
            End
            Begin VB.Label Label3 
               BackStyle       =   0  'Transparent
               Caption         =   "Valor Final:"
               Height          =   165
               Left            =   3000
               TabIndex        =   31
               Top             =   270
               Width           =   1245
            End
         End
         Begin VB.Frame fraGrdCl 
            Caption         =   "Selecione o plano:"
            Height          =   1845
            Left            =   1223
            TabIndex        =   33
            Top             =   810
            Width           =   5865
            Begin MSFlexGridLib.MSFlexGrid grdPlano_PgtoCl 
               Height          =   1215
               Left            =   60
               TabIndex        =   20
               Top             =   270
               Width           =   5715
               _ExtentX        =   10081
               _ExtentY        =   2143
               _Version        =   393216
               Rows            =   4
               Cols            =   3
               FixedCols       =   0
               Enabled         =   -1  'True
               GridLines       =   2
               SelectionMode   =   1
               AllowUserResizing=   1
               BorderStyle     =   0
               FormatString    =   "<Cod. Plano:     |<Descri��o:                                                 |<Prazo M�dio:     "
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin VB.Label lblPlanoCl 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BackStyle       =   0  'Transparent
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Plano Selecionado: "
               ForeColor       =   &H80000008&
               Height          =   225
               Left            =   60
               TabIndex        =   34
               Top             =   1530
               Width           =   1845
            End
         End
         Begin VB.CommandButton cmdVoltarCL 
            Caption         =   "Voltar"
            Height          =   525
            Left            =   4898
            TabIndex        =   28
            ToolTipText     =   "Clique para fechar a tela"
            Top             =   7110
            Width           =   1245
         End
         Begin VB.CommandButton cmdOkCl 
            Caption         =   "OK"
            Height          =   525
            Left            =   2318
            TabIndex        =   26
            ToolTipText     =   "Clique para confirmar a inclus�o dos dados"
            Top             =   7110
            Width           =   1245
         End
         Begin VB.CommandButton cmdCancelarCl 
            Caption         =   "Cancelar"
            Height          =   525
            Left            =   3608
            TabIndex        =   27
            ToolTipText     =   "Clique para limpar os campos"
            Top             =   7110
            Width           =   1245
         End
         Begin MSFlexGridLib.MSFlexGrid grdRowIdCl 
            Height          =   765
            Left            =   120
            TabIndex        =   37
            Top             =   1080
            Visible         =   0   'False
            Width           =   855
            _ExtentX        =   1508
            _ExtentY        =   1349
            _Version        =   393216
            Cols            =   1
            FixedCols       =   0
            Enabled         =   -1  'True
            GridLines       =   2
            FormatString    =   "RowID"
         End
         Begin VB.Label Label5 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Cliente:"
            Height          =   195
            Left            =   120
            TabIndex        =   38
            Top             =   480
            Width           =   525
         End
      End
      Begin VB.Frame fraAgrupada 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   7245
         Left            =   -74760
         TabIndex        =   0
         Top             =   360
         Width           =   8535
         Begin VB.CommandButton cmdCancelar 
            Caption         =   "Cancelar"
            Height          =   525
            Left            =   3608
            TabIndex        =   16
            ToolTipText     =   "Clique para limpar os campos"
            Top             =   6630
            Width           =   1245
         End
         Begin VB.CommandButton cmdOK 
            Caption         =   "OK"
            Height          =   525
            Left            =   2318
            TabIndex        =   15
            ToolTipText     =   "Clique para confirmar a inclus�o dos dados"
            Top             =   6630
            Width           =   1245
         End
         Begin VB.CommandButton cmdVoltar 
            Caption         =   "Voltar"
            Height          =   525
            Left            =   4898
            TabIndex        =   14
            ToolTipText     =   "Clique para fechar a tela"
            Top             =   6630
            Width           =   1245
         End
         Begin VB.Frame fraGrd 
            Caption         =   "Selecione o plano:"
            Height          =   1845
            Left            =   1223
            TabIndex        =   11
            Top             =   330
            Width           =   5865
            Begin MSFlexGridLib.MSFlexGrid grdPlano_Pgto 
               Height          =   1215
               Left            =   60
               TabIndex        =   12
               Top             =   270
               Width           =   5715
               _ExtentX        =   10081
               _ExtentY        =   2143
               _Version        =   393216
               Rows            =   4
               Cols            =   3
               FixedCols       =   0
               Enabled         =   -1  'True
               GridLines       =   2
               AllowUserResizing=   1
               BorderStyle     =   0
               FormatString    =   "<Cod. Plano:     |<Descri��o:                                                 |<Prazo M�dio:     "
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin VB.Label lblPlano 
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BackStyle       =   0  'Transparent
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Plano Selecionado: "
               ForeColor       =   &H80000008&
               Height          =   225
               Left            =   60
               TabIndex        =   13
               Top             =   1530
               Width           =   1845
            End
         End
         Begin VB.Frame fraValores 
            Caption         =   "Informe os valores:"
            Height          =   855
            Left            =   1223
            TabIndex        =   6
            Top             =   2190
            Width           =   5865
            Begin VB.TextBox txtVl_Final 
               Appearance      =   0  'Flat
               BeginProperty Font 
                  Name            =   "Courier New"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               Left            =   3000
               MaxLength       =   12
               TabIndex        =   8
               ToolTipText     =   "Digite o Valor Final:"
               Top             =   450
               Width           =   1875
            End
            Begin VB.TextBox txtVl_Inicial 
               Appearance      =   0  'Flat
               BeginProperty Font 
                  Name            =   "Courier New"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               Left            =   990
               MaxLength       =   12
               TabIndex        =   7
               ToolTipText     =   "Digite o Valor Inicial:"
               Top             =   450
               Width           =   1875
            End
            Begin VB.Label Label2 
               BackStyle       =   0  'Transparent
               Caption         =   "Valor Final:"
               Height          =   165
               Left            =   3000
               TabIndex        =   10
               Top             =   270
               Width           =   1245
            End
            Begin VB.Label Label1 
               BackStyle       =   0  'Transparent
               Caption         =   "Valor Inicial:"
               Height          =   165
               Left            =   990
               TabIndex        =   9
               Top             =   270
               Width           =   1245
            End
         End
         Begin VB.Frame fraCD 
            Caption         =   "Selecione um ou mais CD�s:"
            Height          =   1545
            Left            =   1223
            TabIndex        =   3
            Top             =   3090
            Width           =   5865
            Begin VB.CommandButton cmdTodosNenhum 
               Caption         =   "Desmarcar"
               Height          =   315
               Left            =   4830
               Picture         =   "frmFaturamento_Agrup.frx":069B
               TabIndex        =   5
               Top             =   1140
               Width           =   975
            End
            Begin VB.ListBox lstLojas 
               Appearance      =   0  'Flat
               BeginProperty Font 
                  Name            =   "Courier New"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   990
               ItemData        =   "frmFaturamento_Agrup.frx":0B25
               Left            =   1890
               List            =   "frmFaturamento_Agrup.frx":0B27
               Style           =   1  'Checkbox
               TabIndex        =   4
               Top             =   360
               Width           =   2445
            End
         End
         Begin VB.Frame fraIntervalos 
            Caption         =   "Intervalos j� cadastrados:"
            Height          =   1845
            Left            =   128
            TabIndex        =   1
            Top             =   4680
            Width           =   8055
            Begin MSFlexGridLib.MSFlexGrid grdIntervalos 
               Height          =   1455
               Left            =   60
               TabIndex        =   2
               TabStop         =   0   'False
               Top             =   240
               Width           =   7905
               _ExtentX        =   13944
               _ExtentY        =   2566
               _Version        =   393216
               Cols            =   4
               FixedCols       =   0
               Enabled         =   -1  'True
               GridLines       =   2
               SelectionMode   =   1
               AllowUserResizing=   1
               BorderStyle     =   0
               FormatString    =   $"frmFaturamento_Agrup.frx":0B29
            End
         End
         Begin MSFlexGridLib.MSFlexGrid grdRowID 
            Height          =   765
            Left            =   120
            TabIndex        =   17
            Top             =   480
            Visible         =   0   'False
            Width           =   855
            _ExtentX        =   1508
            _ExtentY        =   1349
            _Version        =   393216
            Cols            =   1
            FixedCols       =   0
            Enabled         =   -1  'True
            GridLines       =   2
            FormatString    =   "RowID"
         End
      End
   End
End
Attribute VB_Name = "frmFaturamento_Agrup"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim teste As Integer
Dim min As Double
Dim max As Double
Dim Qtde As Integer
Dim agrupada As New clsAgrupada

Private Sub cmdCancelar_Click()
  Call Planos(3)
  lblPlano.Caption = "Plano Selecionado: "
  cmdOK.Caption = "OK"
  txtVl_Inicial.Text = ""
  txtVl_Final.Text = ""
  For i = 0 To lstLojas.ListCount - 1 Step 1
    lstLojas.Selected(i) = True
  Next i
  cmdTodosNenhum.Caption = "Desmarcar"
  fraCD.Enabled = True
  grdPlano_Pgto.SetFocus
  fraIntervalos.Enabled = True
  teste = 1
End Sub


'---------------------------------------------------------------------------------------
' Procedure : cmdCancelarCl_Click
' Author    : ricardo.gs
' Date      : 22/2/2012
' Purpose   : SDS1889 - Reinicia os campos da aba Agrupada por Cliente
'---------------------------------------------------------------------------------------
'
Private Sub cmdCancelarCl_Click()
  Call LimparAgrupCliente(False)
  
  txtCodCliente.SetFocus
End Sub

Private Sub cmdOK_Click()
  If Checa_Campo = False Then
    Exit Sub
  End If
  If cmdOK.Caption = "OK" Then 'inclus�o de novo intervalo
    If plano_valido = False Then
      lblPlano = "Plano Selecionado: "
      Exit Sub
    End If
    If max_interval(1) = False Then
      Call cmdCancelar_Click
      Exit Sub
    End If
    If MsgBox("Deseja cadastrar este intervalo?", vbQuestion + vbYesNo, msg_title) = vbNo Then
      Call cmdCancelar_Click
      Exit Sub
    Else
      For i = 0 To lstLojas.ListCount - 1
        lstLojas.ListIndex = i
        If lstLojas.Selected(i) = True Then
          Call pr_manut_faturamento_agrupado("I", Left(lstLojas.Text, 2), txtVl_Inicial, txtVl_Final, Right(lblPlano.Caption, 3), Right(fraAgrupada.Caption, 3))
        End If
      Next i
      Call Intervalos(1)
      If MsgBox("Deseja cadastrar outro intervalo?", vbQuestion + vbYesNo, msg_title) = vbYes Then
        Call cmdCancelar_Click
      Else
        Call cmdVoltar_Click
      End If
    End If
  End If
    
  If cmdOK.Caption = "Alterar" Then 'altera��o de intervalo
    If max_interval(teste) = False Then
      Call cmdCancelar_Click
      Exit Sub
    End If
    If MsgBox("Deseja alterar este intervalo?", vbQuestion + vbYesNo, msg_title) = vbNo Then
      Call cmdCancelar_Click
      Exit Sub
    Else
      For i = 0 To lstLojas.ListCount - 1 Step 1
        lstLojas.ListIndex = i
        If lstLojas.Selected(i) = True Then
          Call pr_manut_faturamento_agrupado("U", Left(lstLojas.Text, 2), txtVl_Inicial, txtVl_Final, Right(lblPlano.Caption, 3), Right(fraAgrupada.Caption, 3), grdRowID.Text)
        End If
      Next i
      Call Intervalos(1)
      If MsgBox("Deseja continuar?", vbQuestion + vbYesNo, msg_title) = vbYes Then
        Call cmdCancelar_Click
      Else
        Call cmdVoltar_Click
      End If
    End If
  End If
End Sub

Private Sub cmdOkCl_Click()
  Dim item As Integer
  Dim result As Boolean
  If ValidaCampos = False Then
    Exit Sub
  End If
  
  
  For item = 0 To lstLojasCl.ListCount - 1
      
    If lstLojasCl.Selected(item) = True Then
      lstLojasCl.ListIndex = item
      If cmdOkCl.Caption = "OK" Then
        result = agrupada.AtuAgrupClient("I", Left(lstLojasCl.Text, 2), txtCodCliente.Text, _
          txtVl_InicialCL.Text, txtVl_FinalCl.Text, Right(lblPlanoCl.Caption, 3), Right(fraAgrupada.Caption, 3))
        If result = False Then
          Exit For
        End If
      Else
        result = agrupada.AtuAgrupClient("U", Left(lstLojasCl.Text, 2), txtCodCliente.Text, txtVl_InicialCL.Text, txtVl_FinalCl.Text, _
          Right(lblPlanoCl.Caption, 3), Right(fraAgrupada.Caption, 3))
        If result = False Then
          MsgBox "N�o foi poss�vel alterar o plano " & Right(lblPlanoCl.Caption, 3) & " na loja " & Left(lstLojasCl.Text, 2) _
          & vbCrLf & "A opera��o ser� cancelada", vbOKOnly + vbInformation, msg_title
          Exit For
        End If
      End If
    End If
  Next

  
  If result = True Then
    sql = "begin commit; end;"
    db.ExecuteSQL sql
    Call cmdCancelarCl_Click
    
    If MsgBox("Deseja cadastrar outro intervalo?", vbYesNo + vbQuestion, msg_title) = vbNo Then
      Call cmdVoltarCL_Click
    End If
  Else
    sql = "begin rollback; end;"
    db.ExecuteSQL sql
  End If
End Sub

Private Sub cmdTodosNenhum_Click()
  If cmdTodosNenhum.Caption = "Todos" Then
    For i = 0 To lstLojas.ListCount - 1
      lstLojas.Selected(i) = True
    Next
    lstLojas.TopIndex = 0
    cmdTodosNenhum.Caption = "Desmarcar"
  ElseIf cmdTodosNenhum.Caption = "Desmarcar" Then
    For i = 0 To lstLojas.ListCount - 1
      lstLojas.Selected(i) = False
    Next
    lstLojas.TopIndex = 0
    cmdTodosNenhum.Caption = "Todos"
  End If

End Sub


'---------------------------------------------------------------------------------------
' Procedure : cmdTodosNenhumCl_Click
' Author    : ricardo.gs
' Date      : 22/2/2012
' Purpose   : SDS1889 - Marca ou desmarca todas as lojas
'---------------------------------------------------------------------------------------
'
Private Sub cmdTodosNenhumCl_Click()
 Call agrupada.SelLoja(lstLojasCl, cmdTodosNenhumCl)
 Me.Refresh
End Sub

Private Sub cmdVoltar_Click()
  If grdIntervalos.ToolTipText = "" Then ' indica que n�o existem intervalos cadastrados
    frmCadastro.cboFl_Agrupada = "N�o"
    Unload Me
  Else
    frmCadastro.cmdAlteraAgrupada.Visible = True
    frmCadastro.cboFl_Agrupada.Enabled = False
    Unload Me
  End If
End Sub

Private Sub cmdVoltarCL_Click()
  Call cmdVoltar_Click
End Sub

Private Sub Form_Activate()
  'fraAgrupada.Caption = "Plano: " & CStr(frmCadastro.txtDescricao.Text) & " - " & Format(CStr(frmCadastro.txtCod_Plano.Text), "000")
End Sub

Private Sub Form_Load()
  fraAgrupada.Caption = "Plano: " & CStr(frmCadastro.txtDescricao.Text) & " - " & Format(CStr(frmCadastro.txtCod_Plano.Text), "000")
  'SDS1889 - Ricardo Gomes
  fraAgrupadaCl.Caption = "Plano: " & CStr(frmCadastro.txtDescricao.Text) _
    & " - " & Format(CStr(frmCadastro.txtCod_Plano.Text), "000")
    
  Call Lojas(1)
  'Popula o lista de loja da aba Agrupada por Cliente
  Call agrupada.Lojas(lstLojasCl)
  Call Planos(3)
  'Popula o grid de plano de pagamento da aba Agrupada por Cliente
  Call agrupada.Planos(7, grdPlano_PgtoCl)
  Call Intervalos(1)
  'Popula o grid de intervalos da aba Agrupada por Cliente
  Call agrupada.Intervalos(2, grdIntervalosCl, Right(fraAgrupadaCl.Caption, 3))
  'Seta o foco para a aba agrupada
  tbsAgrupada.Tab = 0
End Sub

Sub Lojas(opcao As Byte)

  Call pr_con_lojas(opcao)

  For i = 1 To db_cons.RecordCount
    lstLojas.AddItem (Format(db_cons("cod_loja"), "00") & "-" & db_cons("nome_fantasia"))
    db_cons.MoveNext
  Next

  For i = 0 To lstLojas.ListCount - 1
    lstLojas.Selected(i) = True
  Next

  lstLojas.TopIndex = 0

End Sub

Sub Planos(opcao As Byte, Optional cod_plano As Integer)

  Call pr_con_planos(opcao, cod_plano)

  With grdPlano_Pgto
    .Rows = db_cons.RecordCount + 1
    For i = 1 To db_cons.RecordCount Step 1
      .Row = i
      .Col = 0
      .Text = db_cons!cod_plano
      .Col = 1
      .Text = db_cons!desc_plano
      .Col = 2
      .Text = db_cons!prazo_medio
      db_cons.MoveNext
    Next i
  End With

End Sub

Sub seleciona_planos_loja(vCodLoja As Integer, vCodPlano As Integer)

  Call pr_seleciona_plano_loja(vCodLoja, vCodPlano)
    
  With grdPlano_Pgto
    .Rows = db_cons.RecordCount + 1
    For i = 1 To db_cons.RecordCount Step 1
      .Row = i
      .Col = 0
      .Text = db_cons!cod_plano
      .Col = 1
      .Text = db_cons!desc_plano
      .Col = 2
      .Text = db_cons!prazo_medio
      db_cons.MoveNext
    Next i
  End With

End Sub

Private Sub grdIntervalos_DblClick()
    
  Call cmdCancelar_Click
  cmdOK.Caption = "Alterar"
    
  With grdIntervalos
    .Col = 0
    For i = 0 To lstLojas.ListCount - 1
      lstLojas.ListIndex = i
      If Left(lstLojas.Text, 2) = .Text Then
        lstLojas.Selected(i) = True
        fraCD.Enabled = False
      Else
        lstLojas.Selected(i) = False
      End If
    Next i
    .Col = 1
    txtVl_Inicial.Text = .Text
    .Col = 2
    txtVl_Final.Text = .Text
    .Col = 3
    lblPlano = "Plano Selecionado: " & Left(.Text, 3)
    grdRowID.Row = .Row
    
    'Call planos(4, Trim(Right(fraAgrupada.Caption, 3)))
    Call seleciona_planos_loja(.TextMatrix(.Row, 0), Right(fraAgrupada.Caption, 3))
        
  End With
    
  Call max_interval(2)
  fraIntervalos.Enabled = False
    
End Sub


'---------------------------------------------------------------------------------------
' Procedure : grdIntervalosCl_DblClick
' Author    : ricardo.gs
' Date      : 22/2/2012
' Purpose   : Busca as informa��es do plano de pagamento
'---------------------------------------------------------------------------------------
'
Private Sub grdIntervalosCl_DblClick()
  Call pr_con_faturamento_agrupado(3, grdIntervalosCl.TextMatrix(grdIntervalosCl.Row, 0), 0, 0, _
                                   Left(grdIntervalosCl.TextMatrix(grdIntervalosCl.Row, 4), 3), _
    							   Right(fraAgrupada.Caption, 3), grdIntervalosCl.TextMatrix(grdIntervalosCl.Row, 1))
  
  If db_cons.EOF Then
    MsgBox "Nenhum registro foi localizado", vbOKOnly + vbInformation, msg_title
    Exit Sub
  End If
  
  Call ExibirDados
    
End Sub


'---------------------------------------------------------------------------------------
' Procedure : ExibirDados
' Author    : ricardo.gs
' Date      : 22/2/2012
' Purpose   : Exibi os dados do agrupamento do cliente
'---------------------------------------------------------------------------------------
'
Private Sub ExibirDados()
  
  
  txtCodCliente.Text = Trim(db_cons!cod_cliente)
  txtNomeCliente.Text = db_cons!nome_cliente
  'txtVl_InicialCL.Text = db_cons!vl_inicial
  'txtVl_FinalCl.Text = db_cons!vl_final
  
  txtVl_InicialCL.Text = grdIntervalosCl.TextMatrix(grdIntervalosCl.Row, 2)
  txtVl_FinalCl.Text = grdIntervalosCl.TextMatrix(grdIntervalosCl.Row, 3)
  
  
  Dim item As Integer
  Dim top As Integer
  
  For item = 0 To lstLojasCl.ListCount - 1
    lstLojasCl.ListIndex = item
    
    If Mid(lstLojasCl.Text, 1, 2) = grdIntervalosCl.TextMatrix(grdIntervalosCl.Row, 0) Then
      lstLojasCl.Selected(item) = True
      top = item
    Else
      lstLojasCl.Selected(item) = False
    End If
  Next
  
  lstLojasCl.TopIndex = top
  'lblPlanoCl.Caption = Mid(lblPlanoCl.Caption, 1, 19) & db_cons!cod_plano
  lblPlanoCl.Caption = Mid(lblPlanoCl.Caption, 1, 19) & Left(grdIntervalosCl.TextMatrix(grdIntervalosCl.Row, 4), 3)
  
  fraCdCl.Enabled = False
  fraIntervalosCl.Enabled = False
  txtCodCliente.Locked = True
  
  cmdOkCl.Caption = "Alterar"
  
End Sub


Private Sub grdIntervalosCl_Scroll()
  'MsgBox grdIntervalosCl.TopRow
  grdIntervalosCl.SetFocus
  
End Sub

Private Sub grdPlano_Pgto_Click()

  If grdPlano_Pgto.MouseRow = 0 Then
    
    Call MfgSort(grdPlano_Pgto)
        
  End If
    
End Sub


Private Sub grdPlano_Pgto_DblClick()

  With grdPlano_Pgto
    .Col = 0
    lblPlano.Caption = Mid(lblPlano.Caption, 1, 19) & .Text
  End With

End Sub

'---------------------------------------------------------------------------------------
' Procedure : grdPlano_Pgto_DblClick
' Author    : ricardo.gs
' Date      : 22/2/2012
' Purpose   : SDS1889 - Classifica o grid de plano de pagamento
'---------------------------------------------------------------------------------------
'
Private Sub grdPlano_PgtoCl_Click()
  If grdPlano_PgtoCl.MouseRow = 0 Then
    Call agrupada.MfgSort(grdPlano_PgtoCl)
  End If
End Sub


'---------------------------------------------------------------------------------------
' Procedure : grdPlano_PgtoCl_DblClick
' Author    : ricardo.gs
' Date      : 22/2/2012
' Purpose   : SDS1889 - Exibe o plano selecionado
'---------------------------------------------------------------------------------------
'
Private Sub grdPlano_PgtoCl_DblClick()
  Call agrupada.ExibiPlano(grdPlano_PgtoCl, lblPlanoCl)
  txtVl_InicialCL.SetFocus
End Sub

Private Sub grdPlano_PgtoCl_GotFocus()
  grdPlano_PgtoCl.Col = 0
  grdPlano_PgtoCl.Row = 1
End Sub

'---------------------------------------------------------------------------------------
' Procedure : tbsAgrupada_Click
' Author    : ricardo.gs
' Date      : 22/2/2012
' Purpose   : SDS1889 - Seta o foco no campo c�digo do cliente quando a aba
'             Agrupado por Cliente � selecionada
'---------------------------------------------------------------------------------------
'
Private Sub tbsAgrupada_Click(PreviousTab As Integer)
  If tbsAgrupada.Tab = 1 Then
    txtCodCliente.SetFocus
  End If
End Sub


'---------------------------------------------------------------------------------------
' Procedure : txtCodCliente_LostFocus
' Author    : ricardo.gs
' Date      : 22/2/2012
' Purpose   : SDS1889 - Busca o nome do cliente e se � habilitado para
'             inclus�o de agrupamento
'---------------------------------------------------------------------------------------

Private Sub txtCodCliente_Validate(Cancel As Boolean)
  
  Call LimparAgrupCliente(True)
  
  If txtCodCliente.Text <> Empty Then
    Set db_cons = agrupada.BuscaCliente(txtCodCliente.Text)
  
    If db_cons.EOF Then
      MsgBox "Cliente " & txtCodCliente.Text & " n�o localizado", vbOKOnly + vbInformation, msg_title
      Cancel = True
      txtCodCliente.Text = Empty
      Exit Sub
    End If
  
    txtNomeCliente.Text = db_cons!nome_cliente
  
    If db_cons!fl_agrup_diferenciada <> "S" Then
      MsgBox "O cliente " & txtCodCliente.Text & " - " & txtNomeCliente.Text & _
        " n�o est� cadastrado com Agrupamento Diferenciado, " & _
        "verificar o cadastro de clientes.", vbOKOnly + vbInformation, msg_title
      Cancel = True
      txtCodCliente.Text = Empty
      txtNomeCliente.Text = Empty
      Exit Sub
    Else
      txtNomeCliente.Text = db_cons!nome_cliente
'      Call pr_con_faturamento_agrupado(3, 0, 0, 0, 0, Right(fraAgrupada.Caption, 3), txtCodCliente.Text)
'
'      If db_cons.EOF Then
'        Exit Sub
'      End If
'      If MsgBox("Existe um plano de pagamento para o cliente informado." & vbCrLf & "Deseja alterar?", _
'      vbYesNo + vbQuestion, msg_title) = vbYes Then
'        Call ExibirDados
'      Else
'        Call cmdCancelarCl_Click
'        Cancel = True
'      End If
    End If
  End If
  
End Sub

Private Sub txtVl_Final_GotFocus()
  Call agrupada.SelTextBox(txtVl_Final)
End Sub

Private Sub txtVl_Final_KeyPress(KeyAscii As Integer)
  If Asc(",") = KeyAscii Then
    KeyAscii = Asc(".")
  End If
  KeyAscii = Valor(KeyAscii, txtVl_Final)
End Sub

Private Sub txtVl_Final_LostFocus()
  If txtVl_Final = "" Then
    txtVl_Final = 999999999.99
  ElseIf txtVl_Final > 999999999.99 Then
    txtVl_Final = 999999999.99
  End If
  txtVl_Final = Format(txtVl_Final, "0.00")
End Sub


'---------------------------------------------------------------------------------------
' Procedure : txtVl_FinalCl_LostFocus
' Author    : ricardo.gs
' Date      : 22/2/2012
' Purpose   : SDS1889 - formata e valida o dado digitado
'---------------------------------------------------------------------------------------
'
Private Sub txtVl_FinalCl_LostFocus()
Dim result As Boolean

  'Formato o valor
  Call agrupada.FormataValor(txtVl_FinalCl)
  
  'Valida o valor
  result = agrupada.ValidarValor(CDbl(txtVl_InicialCL.Text), CDbl(txtVl_FinalCl.Text))
  
  'Exibe msg caso o valor final for menor que o valor inicial
  If result = False Then
    MsgBox "O valor final n�o pode ser menor que o valor inicial", vbOKOnly + vbExclamation, Me.Caption
    txtVl_FinalCl.SetFocus
  End If
End Sub

Private Sub txtVl_Inicial_GotFocus()
  txtVl_Inicial.SelStart = 0
  txtVl_Inicial.SelLength = 100
End Sub

Private Sub txtVl_Inicial_KeyPress(KeyAscii As Integer)
  If Asc(",") = KeyAscii Then
    KeyAscii = Asc(".")
  End If
  KeyAscii = Valor(KeyAscii, txtVl_Inicial)
End Sub

Private Sub txtVl_Inicial_LostFocus()
  If txtVl_Inicial = "" Then
    txtVl_Inicial = 0
  ElseIf txtVl_Inicial > 999999999.99 Then
    txtVl_Inicial = 0
  End If
  txtVl_Inicial = Format(txtVl_Inicial, "0.00")
End Sub


Function Checa_Campo() As Boolean
    
  Dim teste As Boolean

  If lblPlano.Caption = "Plano Selecionado: " Then
    MsgBox "Selecione um plano...", vbCritical, msg_title
    Checa_Campo = False
    Exit Function
  ElseIf txtVl_Inicial.Text = "" Then
    MsgBox "Digite o valor inicial...", vbCritical, msg_title
    txtVl_Inicial.SetFocus
    Checa_Campo = False
    Exit Function
  ElseIf txtVl_Final.Text = "" Then
    MsgBox "Digite o valor final...", vbCritical, msg_title
    txtVl_Final.SetFocus
    Checa_Campo = False
    Exit Function
  End If
  If Val(txtVl_Final.Text) < Val(txtVl_Inicial.Text) Then
    MsgBox "O intervalo inicial n�o pode ser maior que o intervalo final...", vbCritical, msg_title
    txtVl_Inicial.SetFocus
    Checa_Campo = False
    Exit Function
  End If

  For i = 0 To lstLojas.ListCount - 1 Step 1
    If lstLojas.Selected(i) = True Then
      teste = True
      GoTo next1
    End If
  Next i
next1:
  If teste = False Then
    MsgBox "Selecione pelo menos um CD...", vbCritical, msg_title
    Checa_Campo = False
    Exit Function
  End If

  Checa_Campo = True
    
End Function

Sub Intervalos(opcao As Byte)

  Call pr_con_faturamento_agrupado(opcao, 0, 0, 0, 0, Right(fraAgrupada.Caption, 3))

  If db_cons.EOF = True Then
    Exit Sub
  End If

  With grdIntervalos
    .Rows = db_cons.RecordCount + 1
    grdRowID.Rows = db_cons.RecordCount + 1
    .ToolTipText = "Registros: " & db_cons.RecordCount
    
    For i = 1 To db_cons.RecordCount
      .Row = i
      grdRowID.Row = i
      .Col = 0: .Text = Trim(db_cons!cod_loja)
      .Col = 1: .Text = Replace(db_cons!vl_inicial, ",", ".")
      .Col = 2: .Text = Replace(db_cons!vl_final, ",", ".")
      .Col = 3: .Text = Trim(CStr(db_cons!cod_plano) & " - " & CStr(db_cons!desc_plano))
      grdRowID.Text = db_cons!rowid
      db_cons.MoveNext
    Next i
  End With
        
End Sub

Function max_interval(opcao As Integer)
    
  If teste = -1 Then
    max_interval = True
    Exit Function
  End If
    
  If opcao = 1 Then 'inclus�o de um novo intervalo
    For i = 0 To lstLojas.ListCount - 1
      lstLojas.ListIndex = i
      If lstLojas.Selected(i) = True Then
        Call pr_con_max_interval(1, Val(Right(fraAgrupada.Caption, 3)), Val(Left(lstLojas.Text, 2)))
        If IsNull(db_cons!maior_intervalo) = True Then
          max_interval = True
          GoTo next1
          'Exit Function
        End If
        If Val(db_cons!maior_intervalo) > Val(txtVl_Inicial.Text) Then
          MsgBox "N�o ser� poss�vel gravar esse intervalo para o CD " & lstLojas.Text & ". Voc� deve cadastrar um valor inicial maior que " & CStr(db_cons!maior_intervalo), vbCritical, msg_title
          max_interval = False
          Exit Function
        Else
next1:
          max_interval = True
        End If
      End If
    Next
  End If
    
  'Altera��o de Intervalos
  If opcao = 2 Then
    For i = 0 To lstLojas.ListCount - 1
      lstLojas.ListIndex = i
      If lstLojas.Selected(i) = True Then
        Call pr_con_max_interval(1, Val(Right(fraAgrupada.Caption, 3)), Val(Left(lstLojas.Text, 2)))
        min = db_cons!menor_intervalo
        max = db_cons!maior_intervalo
        Qtde = db_cons!Qtde
      End If
    Next
    If Qtde = 1 Then
      max_interval = True
      teste = -1
      Exit Function
    ElseIf Qtde > 1 And txtVl_Inicial = min Then
      teste = 3
      Exit Function
    ElseIf Qtde > 1 And txtVl_Final = max Then
      teste = 4
      Exit Function
    Else
      teste = 5
      Exit Function
    End If
  End If
    
  If opcao = 3 Then
    With grdIntervalos
      .Row = .Row + 1
      .Col = 1
      If Val(txtVl_Inicial.Text) > .Text Then
        MsgBox "O valor inicial desse intervalo deve ser menor que " & .Text & ".", vbCritical, msg_title
        max_interval = False
        Exit Function
      ElseIf Val(txtVl_Final.Text) > .Text Then
        MsgBox "O valor final desse intervalo deve ser menor que " & .Text & ".", vbCritical, msg_title
        max_interval = False
        Exit Function
      End If
    End With
    max_interval = True
  End If
    
  If opcao = 4 Then
    With grdIntervalos
      .Row = .Row - 1
      .Col = 2
      If Val(txtVl_Inicial.Text) < .Text Then
        MsgBox "O valor inicial desse intervalo deve ser maior que " & .Text & ".", vbCritical, msg_title
        max_interval = False
        Exit Function
      ElseIf Val(txtVl_Final.Text) < .Text Then
        MsgBox "O valor final desse intervalo deve ser maior que " & .Text & ".", vbCritical, msg_title
        max_interval = False
        Exit Function
      End If
    End With
    max_interval = True
  End If
    
  If opcao = 5 Then
    With grdIntervalos
      .Row = .Row - 1
      .Col = 2
      If Val(txtVl_Inicial.Text) < .Text Then
        MsgBox "O valor inicial desse intervalo deve ser maior que " & .Text & ".", vbCritical, msg_title
        max_interval = False
        Exit Function
      End If
      .Row = .Row + 2
      .Col = 1
      If Val(txtVl_Final.Text) > .Text Then
        MsgBox "O valor final desse intervalo deve ser menor que " & .Text & ".", vbCritical, msg_title
        max_interval = False
        Exit Function
      End If
    End With
    max_interval = True
  End If
    
End Function

Function plano_valido()
  Call pr_con_planos(5, Right(fraAgrupada.Caption, 3))
  If db_cons.EOF = True Then
    plano_valido = True
    Exit Function
  Else
    For i = 1 To db_cons.RecordCount
      If Val(Right(lblPlano.Caption, 3)) = db_cons!Planos Then
        MsgBox "O Plano " & Right(lblPlano.Caption, 3) & " j� est� sendo utilizado. Selecione outro plano!", vbCritical, msg_title
        Exit Function
      End If
      db_cons.MoveNext
    Next
  End If
  plano_valido = True
End Function
 

'---------------------------------------------------------------------------------------
' Procedure : txtVl_FinalCl_GotFocus
' Author    : ricardo.gs
' Date      : 22/2/2012
' Purpose   : SDS1889 - Seleciona todo o texxto do campos
'---------------------------------------------------------------------------------------
'
Private Sub txtVl_FinalCl_GotFocus()
  Call agrupada.SelTextBox(txtVl_FinalCl)
End Sub


'---------------------------------------------------------------------------------------
' Procedure : txtVl_FinalCl_KeyPress
' Author    : ricardo.gs
' Date      : 22/2/2012
' Purpose   : SDS1889 - Valida o dado digitado
'---------------------------------------------------------------------------------------
'
Private Sub txtVl_FinalCl_KeyPress(KeyAscii As Integer)
  KeyAscii = agrupada.InputValor(KeyAscii, txtVl_FinalCl)
End Sub


'---------------------------------------------------------------------------------------
' Procedure : txtVl_InicialCL_GotFocus
' Author    : ricardo.gs
' Date      : 22/2/2012
' Purpose   : SDS1889 - Seleciona todo o texto
'---------------------------------------------------------------------------------------
'
Private Sub txtVl_InicialCL_GotFocus()
Call agrupada.SelTextBox(txtVl_InicialCL)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : txtVl_InicialCL_KeyPress
' Author    : ricardo.gs
' Date      : 22/2/2012
' Purpose   : SDS1889 - Valida o dado digitado
'---------------------------------------------------------------------------------------
'
Private Sub txtVl_InicialCL_KeyPress(KeyAscii As Integer)
  KeyAscii = agrupada.InputValor(KeyAscii, txtVl_InicialCL)
End Sub

Private Sub txtCodCliente_GotFocus()
  On Error Resume Next
  txtCodCliente.SelStart = 0
  txtCodCliente.SelLength = Len(txtCodCliente.Text)
End Sub

Private Sub txtCodCliente_KeyPress(KeyAscii As Integer)
  KeyAscii = Numerico(KeyAscii)
End Sub

Private Sub txtNomeCliente_GotFocus()
  On Error Resume Next
  txtNomeCliente.SelStart = 0
  txtNomeCliente.SelLength = Len(txtNomeCliente.Text)
End Sub


'---------------------------------------------------------------------------------------
' Procedure : txtVl_InicialCL_LostFocus
' Author    : ricardo.gs
' Date      : 22/2/2012
' Purpose   : SDS1889 - Formata o valor do campo
'---------------------------------------------------------------------------------------
'
Private Sub txtVl_InicialCL_LostFocus()
  Call agrupada.FormataValor(txtVl_InicialCL)
End Sub


'---------------------------------------------------------------------------------------
' Procedure : LimparAgrupCliente
' Author    : ricardo.gs
' Date      : 22/2/2012
' Purpose   : SDS1889 - SDS1889 - Reinicia os campos da aba Agrupada por Cliente
'---------------------------------------------------------------------------------------
'
Private Sub LimparAgrupCliente(search As Boolean)

  If search = False Then
    txtCodCliente.Locked = False
    txtCodCliente.Text = Empty
  End If

  txtNomeCliente.Text = Empty
  lblPlanoCl.Caption = Mid(lblPlano.Caption, 1, 19)
  txtVl_InicialCL.Text = Empty
  txtVl_FinalCl.Text = Empty
  
  cmdTodosNenhumCl.Caption = "Todos"
  Call cmdTodosNenhumCl_Click
  
  cmdOkCl.Caption = "OK"
  fraCdCl.Enabled = True
  fraIntervalosCl.Enabled = True
  
  Call agrupada.Intervalos(2, grdIntervalosCl, Right(fraAgrupadaCl.Caption, 3))
End Sub


'---------------------------------------------------------------------------------------
' Procedure : ValidaCampos
' Author    : ricardo.gs
' Date      : 22/2/2012
' Purpose   : SDS1889 - Valida os campos da aba Agrupada por Cliente
'---------------------------------------------------------------------------------------
'
Private Function ValidaCampos() As Boolean

  Dim item As Integer
  
  ValidaCampos = True
  
  If lblPlanoCl.Caption = "Plano Selecionado: " Then
    MsgBox "Selecione um plano.", vbCritical, msg_title
    ValidaCampos = False
    Exit Function
  ElseIf txtVl_InicialCL.Text = Empty Then
    MsgBox "Digite o valor inicial.", vbCritical, msg_title
    txtVl_InicialCL.SetFocus
    ValidaCampos = False
    Exit Function
  ElseIf txtVl_FinalCl.Text = Empty Then
    MsgBox "Digite o valor final.", vbCritical, msg_title
    txtVl_FinalCl.SetFocus
    ValidaCampos = False
    Exit Function
  ElseIf txtCodCliente.Text = Empty Then
    MsgBox "Informe o cliente.", vbCritical, msg_title
    txtVl_FinalCl.SetFocus
    ValidaCampos = False
    Exit Function
  End If
  
  If Val(txtVl_FinalCl.Text) < Val(txtVl_InicialCL.Text) Then
    MsgBox "O valor inicial n�o pode ser maior que o valor final.", vbCritical, msg_title
    txtVl_InicialCL.SetFocus
    ValidaCampos = False
    Exit Function
  End If

  For item = 0 To lstLojasCl.ListCount - 1 Step 1
    If lstLojasCl.Selected(item) = False Then
      ValidaCampos = False
    Else
      ValidaCampos = True
      Exit For
    End If
  Next item
  
  If ValidaCampos = False Then
    MsgBox "Selecione pelo menos um CD", vbCritical, msg_title
  End If
End Function
