--- PACKAGE DO SISTEMA CAD990
--- DATA: 28/04/2005
--- ANALISTA: FABIO GUIDOTTI
--- dir: F:\SISTEMAS\ORACLE\CAD990\PCK_CAD990.SQL

CREATE OR REPLACE Package PRODUCAO.PCK_CAD990 is

	Type tp_Cursor is Ref Cursor;
									
									
	PROCEDURE PR_CON_LOGIN( PM_LOGIN   IN VARCHAR2,
							PM_SENHA   IN VARCHAR2,
							PM_CURSOR1 IN OUT tp_Cursor,
							PM_CODERRO OUT NUMBER,
							PM_TXTERRO OUT VARCHAR2);
							
							
	PROCEDURE PR_CON_GRUPOS( PM_CURSOR1 IN OUT tp_Cursor,
							 PM_CODERRO OUT NUMBER,
							 PM_TXTERRO OUT VARCHAR2);  
							 
							 
	PROCEDURE PR_ACT_GRUPOS( PM_CODGRUPO IN NUMBER,
							 PM_GRUPO    IN VARCHAR2,
							 PM_DESCR    IN VARCHAR2,
							 PM_PRIORI   IN NUMBER,
							 PM_ACT      IN VARCHAR2,
							 PM_CODERRO  OUT NUMBER,
							 PM_TXTERRO  OUT VARCHAR2); 
							 
							 
	PROCEDURE PR_CON_ACOES( PM_CURSOR1 IN OUT tp_Cursor,
							PM_CODERRO OUT NUMBER,
							PM_TXTERRO OUT VARCHAR2);
							
							
	PROCEDURE PR_ACT_ACOES( PM_CODACAO IN NUMBER,
							PM_ACAO    IN VARCHAR2,
							PM_TPACAO  IN NUMBER,
							PM_ACT     IN VARCHAR2,
							PM_CODERRO OUT NUMBER,
							PM_TXTERRO OUT VARCHAR2);
							
							
	PROCEDURE PR_CON_GRUPOACAO( PM_CURSOR1 IN OUT tp_Cursor,
								PM_CODERRO OUT NUMBER,
								PM_TXTERRO OUT VARCHAR2);
							
							
	PROCEDURE PR_ACT_GRUPOACAO( PM_CODGRUPO  IN NUMBER,
								PM_CODACAO   IN NUMBER,
								PM_GRUPO_OLD IN NUMBER,
								PM_ACAO_OLD  IN NUMBER,
								PM_PRIOR     IN NUMBER,
								PM_ACT       IN VARCHAR2,
								PM_CODERRO   OUT NUMBER,
								PM_TXTERRO   OUT VARCHAR2);
								
								
	PROCEDURE PR_CON_CLIGRUPO( PM_CODCLI  IN NUMBER,
							   PM_CURSOR1 IN OUT tp_Cursor,
							   PM_CODERRO OUT NUMBER,
							   PM_TXTERRO OUT VARCHAR2);
							   
							   
	PROCEDURE PR_ACT_CLIGRUPO( PM_CLIENTE   IN NUMBER,
							   PM_GRUPO     IN NUMBER,
							   PM_GRUPO_OLD IN NUMBER,
							   PM_ACT       IN VARCHAR2,
							   PM_CODERRO   OUT NUMBER,
							   PM_TXTERRO   OUT VARCHAR2);
							   
							   
	PROCEDURE PR_CON_GRUPOCLI( PM_CODCLI  IN NUMBER,
							   PM_GRUPO   IN NUMBER,
							   PM_CURSOR1 IN OUT tp_Cursor,
							   PM_CODERRO OUT NUMBER,
							   PM_TXTERRO OUT VARCHAR2);
							   
							   
	PROCEDURE PR_CON_GRUPOCLIHIST( PM_CODCLI  IN NUMBER,
								   PM_GRUPO   IN NUMBER,
								   PM_CURSOR1 IN OUT tp_Cursor,
								   PM_CODERRO OUT NUMBER,
								   PM_TXTERRO OUT VARCHAR2);
				  					  
End PCK_CAD990;
---------------
/

CREATE OR REPLACE Package Body PRODUCAO.PCK_CAD990 is
	
	
	PROCEDURE PR_CON_LOGIN( PM_LOGIN   IN VARCHAR2,
							PM_SENHA   IN VARCHAR2,
							PM_CURSOR1 IN OUT tp_Cursor,
							PM_CODERRO OUT NUMBER,
							PM_TXTERRO OUT VARCHAR2) AS	 

	BEGIN
		
		PM_CODERRO := 0;
		
		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR 
		SELECT 
		A.COD_USUARIO, A.LOGIN,descriptografa_senha(A.SENHA) SENHA,
		A.NOME_USUARIO ATENDENTE, B.COD_PERFIL, C.DESCRICAO
		FROM HELPDESK.USUARIO A, INTRANET.R_PERFIL_USUARIO B, INTRANET.PERFIL C
		WHERE
		UPPER(A.LOGIN) = UPPER(PM_LOGIN) AND
		PRODUCAO.DESCRIPTOGRAFA_SENHA(A.SENHA) = PM_SENHA AND
		B.COD_USUARIO = A.COD_USUARIO AND
		B.COD_PERFIL = C.COD_PERFIL AND
		(B.COD_PERFIL = 43);
		
	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;
		
	END PR_CON_LOGIN;
	
	
	
	PROCEDURE PR_CON_GRUPOS( PM_CURSOR1 IN OUT tp_Cursor,
							 PM_CODERRO OUT NUMBER,
							 PM_TXTERRO OUT VARCHAR2) AS	 

	BEGIN
		
		PM_CODERRO := 0;
		
		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR 
		SELECT
		A.COD_GRUPO "C�D.",
		A.DESC_GRUPO "GRUPO",
		A.DESC_TELA "DESCRI��O",
		A.NUM_PRIORIDADE_LIG "PRIORIDADE"
		FROM PRODUCAO.GRUPO_CLIENTE A
		ORDER BY A.COD_GRUPO;
		
	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;
		
	END PR_CON_GRUPOS;
	
	
	PROCEDURE PR_ACT_GRUPOS( PM_CODGRUPO IN NUMBER,
							 PM_GRUPO    IN VARCHAR2,
							 PM_DESCR    IN VARCHAR2,
							 PM_PRIORI   IN NUMBER,
							 PM_ACT      IN VARCHAR2,
							 PM_CODERRO  OUT NUMBER,
							 PM_TXTERRO  OUT VARCHAR2) AS	 

	BEGIN
		
		PM_CODERRO := 0;
		
		IF PM_ACT = 'I' THEN
		
			INSERT INTO PRODUCAO.GRUPO_CLIENTE VALUES
			((SELECT NVL(MAX(A.COD_GRUPO),0)+1 FROM PRODUCAO.GRUPO_CLIENTE A),
			PM_GRUPO,PM_DESCR,PM_PRIORI);
		
		ELSIF PM_ACT = 'A' THEN
		
			UPDATE PRODUCAO.GRUPO_CLIENTE SET
			DESC_GRUPO = PM_GRUPO,
			DESC_TELA = PM_DESCR,
			NUM_PRIORIDADE_LIG = PM_PRIORI
			WHERE COD_GRUPO = PM_CODGRUPO;
		
		END IF;
		
		COMMIT;
		
	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;
		
	END PR_ACT_GRUPOS;
	
	
	PROCEDURE PR_CON_ACOES( PM_CURSOR1 IN OUT tp_Cursor,
							PM_CODERRO OUT NUMBER,
							PM_TXTERRO OUT VARCHAR2) AS	 

	BEGIN
		
		PM_CODERRO := 0;
		
		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR 
		SELECT
		A.COD_ACAO "C�D.",
		A.DESC_ACAO "A��O",
		DECODE(A.TP_ACAO,1,'1 - RELECIONAMENTO',
				DECODE(A.TP_ACAO,2,'2 - COMERCIAL',
						DECODE(A.TP_ACAO,3,'3 - CLIENTE INATIVO'))) "TIPO"
		FROM PRODUCAO.ACAO A
		ORDER BY A.COD_ACAO;
		
	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;
		
	END PR_CON_ACOES;
	
	
	PROCEDURE PR_ACT_ACOES( PM_CODACAO IN NUMBER,
							PM_ACAO    IN VARCHAR2,
							PM_TPACAO  IN NUMBER,
							PM_ACT     IN VARCHAR2,
							PM_CODERRO OUT NUMBER,
							PM_TXTERRO OUT VARCHAR2) AS	 

	BEGIN
		
		PM_CODERRO := 0;
		
		IF PM_ACT = 'I' THEN
		
			INSERT INTO PRODUCAO.ACAO VALUES
			((SELECT NVL(MAX(A.COD_ACAO),0)+1 FROM PRODUCAO.ACAO A),
			PM_TPACAO,PM_ACAO);
		
		ELSIF PM_ACT = 'A' THEN
		
			UPDATE PRODUCAO.ACAO SET
			DESC_ACAO = PM_ACAO,
			TP_ACAO = PM_TPACAO
			WHERE COD_ACAO = PM_CODACAO;
		
		END IF;
		
		COMMIT;
		
	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;
		
	END PR_ACT_ACOES;
	
	
	PROCEDURE PR_CON_GRUPOACAO( PM_CURSOR1 IN OUT tp_Cursor,
								PM_CODERRO OUT NUMBER,
								PM_TXTERRO OUT VARCHAR2) AS	 

	BEGIN
		
		PM_CODERRO := 0;
		
		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR 
		SELECT
		A.COD_GRUPO "C�D",
		A.COD_GRUPO ||' - '|| B.DESC_GRUPO "GRUPO",
		A.COD_ACAO ||' - '|| C.DESC_ACAO "A��O",
		A.NUM_PRIORIDADE "PRIORIDADE"
		FROM PRODUCAO.R_GRUPO_ACAO A,PRODUCAO.GRUPO_CLIENTE B,PRODUCAO.ACAO C
		WHERE
		A.COD_GRUPO = B.COD_GRUPO AND
		A.COD_ACAO = C.COD_ACAO
		ORDER BY A.NUM_PRIORIDADE;
		
	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;
		
	END PR_CON_GRUPOACAO;
	
	
	PROCEDURE PR_ACT_GRUPOACAO( PM_CODGRUPO  IN NUMBER,
								PM_CODACAO   IN NUMBER,
								PM_GRUPO_OLD IN NUMBER,
								PM_ACAO_OLD  IN NUMBER,
								PM_PRIOR     IN NUMBER,
								PM_ACT       IN VARCHAR2,
								PM_CODERRO   OUT NUMBER,
								PM_TXTERRO   OUT VARCHAR2) AS	 

	BEGIN
		
		PM_CODERRO := 0;
		
		IF PM_ACT = 'I' THEN
		
			INSERT INTO PRODUCAO.R_GRUPO_ACAO VALUES
			(PM_CODGRUPO,PM_CODACAO,PM_PRIOR);
		
		ELSIF PM_ACT = 'A' THEN
		
			UPDATE PRODUCAO.R_GRUPO_ACAO SET
			COD_GRUPO = PM_CODGRUPO,
			COD_ACAO = PM_CODACAO,
			NUM_PRIORIDADE = PM_PRIOR
			WHERE 
			COD_GRUPO = PM_GRUPO_OLD AND
			COD_ACAO = PM_ACAO_OLD;
		
		END IF;
		
		COMMIT;
		
	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;
		
	END PR_ACT_GRUPOACAO;
	
	
	PROCEDURE PR_CON_CLIGRUPO( PM_CODCLI  IN NUMBER,
							   PM_CURSOR1 IN OUT tp_Cursor,
							   PM_CODERRO OUT NUMBER,
							   PM_TXTERRO OUT VARCHAR2) AS	 

	BEGIN
		
		PM_CODERRO := 0;
		
		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR 
		SELECT 
		A.COD_CLIENTE "CLIENTE",
		A.COD_GRUPO || ' - ' || B.DESC_GRUPO "GRUPO"
		FROM PRODUCAO.R_CLIENTE_GRUPO A, PRODUCAO.GRUPO_CLIENTE B
		WHERE
		A.COD_GRUPO = B.COD_GRUPO AND
		A.COD_CLIENTE = PM_CODCLI;
		
	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;
		
	END PR_CON_CLIGRUPO;
	
	
	PROCEDURE PR_ACT_CLIGRUPO( PM_CLIENTE   IN NUMBER,
							   PM_GRUPO     IN NUMBER,
							   PM_GRUPO_OLD IN NUMBER,
							   PM_ACT       IN VARCHAR2,
							   PM_CODERRO   OUT NUMBER,
							   PM_TXTERRO   OUT VARCHAR2) AS	 

	BEGIN
		
		PM_CODERRO := 0;
		
		IF PM_ACT = 'I' THEN
		
			INSERT INTO PRODUCAO.R_CLIENTE_GRUPO VALUES
			(PM_CLIENTE,PM_GRUPO);
		
		ELSIF PM_ACT = 'A' THEN
		
			UPDATE PRODUCAO.R_CLIENTE_GRUPO SET
			COD_GRUPO = PM_GRUPO
			WHERE 
			COD_CLIENTE = PM_CLIENTE;
			
			INSERT INTO PRODUCAO.R_CLIENTE_GRUPO_HIST VALUES
			(PM_CLIENTE,SYSDATE,PM_GRUPO_OLD,PM_GRUPO);
		
		END IF;
		
		COMMIT;
		
	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;
		
	END PR_ACT_CLIGRUPO;
	
	
	PROCEDURE PR_CON_GRUPOCLI( PM_CODCLI  IN NUMBER,
							   PM_GRUPO   IN NUMBER,
							   PM_CURSOR1 IN OUT tp_Cursor,
							   PM_CODERRO OUT NUMBER,
							   PM_TXTERRO OUT VARCHAR2) AS	 

	BEGIN
		
		PM_CODERRO := 0;
		
		
		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR 
		SELECT 
		A.COD_CLIENTE "C�D. CLIENTE",
		A.NOME_CLIENTE "CLIENTE",
		A.COD_CIDADE "C�D. CIDADE",
		C.NOME_CIDADE "CIDADE",
		C.COD_UF "UF",
		B.COD_GRUPO || ' - ' || d.desc_grupo "GRUPO"
		FROM PRODUCAO.CLIENTE A, CIDADE C, PRODUCAO.GRUPO_CLIENTE D, PRODUCAO.R_CLIENTE_GRUPO B
		where
		a.cod_cliente = b.cod_cliente and
		a.cod_cidade = c.cod_cidade and
		b.cod_grupo = d.cod_grupo AND
		A.COD_CLIENTE = DECODE(PM_CODCLI,0,A.COD_CLIENTE,PM_CODCLI) AND
		B.COD_GRUPO = DECODE(PM_GRUPO,0,B.COD_GRUPO,PM_GRUPO)
		ORDER BY A.NOME_CLIENTE;
		
	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;
		
	END PR_CON_GRUPOCLI;
	
	
	PROCEDURE PR_CON_GRUPOCLIHIST( PM_CODCLI  IN NUMBER,
								   PM_GRUPO   IN NUMBER,
								   PM_CURSOR1 IN OUT tp_Cursor,
								   PM_CODERRO OUT NUMBER,
								   PM_TXTERRO OUT VARCHAR2) AS	 

	BEGIN
		
		PM_CODERRO := 0;
		
		
		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR 
		SELECT 
		A.COD_CLIENTE "C�D. CLIENTE",
		A.NOME_CLIENTE "CLIENTE",
		A.COD_CIDADE "C�D. CIDADE",
		C.NOME_CIDADE "CIDADE",
		C.COD_UF "UF",
		TO_CHAR(b.dt_alteracao,'DD/MM/RRRR HH24:MI:SS') "DT. ALTERA��O", 
		B.COD_GRUPO_ANT || ' - ' || d.desc_grupo "GRUPO ANT.",
		B.COD_GRUPO_NOVO || ' - ' || E.desc_grupo "GRUPO NOVO"
		FROM PRODUCAO.CLIENTE A, CIDADE C, PRODUCAO.GRUPO_CLIENTE E, PRODUCAO.GRUPO_CLIENTE D, PRODUCAO.R_CLIENTE_GRUPO_HIST B
		where
		a.cod_cliente = b.cod_cliente and
		a.cod_cidade = c.cod_cidade and
		B.COD_GRUPO_ANT = d.cod_grupo AND
		B.COD_GRUPO_NOVO = E.COD_GRUPO AND
		A.COD_CLIENTE = DECODE(PM_CODCLI,0,A.COD_CLIENTE,PM_CODCLI) AND
		B.COD_GRUPO_ANT = DECODE(PM_GRUPO,0,B.COD_GRUPO_ANT,PM_GRUPO)
		ORDER BY A.NOME_CLIENTE, B.DT_ALTERACAO DESC;
		
	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;
		
	END PR_CON_GRUPOCLIHIST;
	
End PCK_CAD990;
---------------
/