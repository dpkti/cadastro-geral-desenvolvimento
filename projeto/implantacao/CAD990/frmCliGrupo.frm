VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form frmCliGrupo 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Relacionamento Cliente x Grupo"
   ClientHeight    =   2730
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7290
   Icon            =   "frmCliGrupo.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   182
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   486
   Begin MSComDlg.CommonDialog CMD 
      Left            =   6660
      Top             =   180
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Frame fra 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1410
      Left            =   45
      TabIndex        =   3
      Top             =   945
      Width           =   7170
      Begin VB.TextBox txtCOD 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3375
         TabIndex        =   8
         Top             =   855
         Visible         =   0   'False
         Width           =   1140
      End
      Begin VB.ComboBox cmbGrupo 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   990
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   855
         Width           =   2220
      End
      Begin VB.TextBox txtCodCliente 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   990
         MaxLength       =   6
         TabIndex        =   5
         Top             =   405
         Width           =   690
      End
      Begin VB.TextBox txtNomeCliente 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1755
         Locked          =   -1  'True
         TabIndex        =   4
         Top             =   405
         Width           =   5280
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Grupo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   315
         TabIndex        =   9
         Top             =   900
         Width           =   555
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         Caption         =   "Cliente:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   270
         TabIndex        =   6
         Top             =   450
         Width           =   690
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   2400
      Width           =   7290
      _ExtentX        =   12859
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   12806
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      Top             =   810
      Width           =   7170
      _ExtentX        =   12647
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCliGrupo.frx":23D2
      PICN            =   "frmCliGrupo.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd1 
      Height          =   690
      Left            =   810
      TabIndex        =   10
      TabStop         =   0   'False
      ToolTipText     =   "Gravar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCliGrupo.frx":30C8
      PICN            =   "frmCliGrupo.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd2 
      Height          =   690
      Left            =   1530
      TabIndex        =   11
      TabStop         =   0   'False
      ToolTipText     =   "Cancelar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCliGrupo.frx":3DBE
      PICN            =   "frmCliGrupo.frx":3DDA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd3 
      Height          =   690
      Left            =   2250
      TabIndex        =   12
      TabStop         =   0   'False
      ToolTipText     =   "Cancelar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCliGrupo.frx":46B4
      PICN            =   "frmCliGrupo.frx":46D0
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmCliGrupo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim ACT As String
Dim ArqPath As String

Private Sub cmd1_Click()

    If txtCodCliente.Text = "" Then
        MsgBox "Selecione um Cliente", , "Aten��o"
        txtCodCliente.SetFocus
        Exit Sub
    End If
    
    If cmbGrupo.Text = "" Then
        MsgBox "Escolha um Grupo", , "Aten��o"
        cmbGrupo.SetFocus
        Exit Sub
    End If
    
    
    vBanco.Parameters.Remove "PM_CLIENTE"
    vBanco.Parameters.Add "PM_CLIENTE", txtCodCliente.Text, 1
    
    vBanco.Parameters.Remove "PM_GRUPO"
    vBanco.Parameters.Add "PM_GRUPO", Left(cmbGrupo.Text, InStr(1, cmbGrupo.Text, " - ") - 1), 1
    
    vBanco.Parameters.Remove "PM_GRUPO_OLD"
    vBanco.Parameters.Add "PM_GRUPO_OLD", txtCOD.Text, 1
    
    vBanco.Parameters.Remove "PM_ACT"
    vBanco.Parameters.Add "PM_ACT", ACT, 1
    
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "Producao.PCK_CAD990.PR_ACT_CLIGRUPO(:PM_CLIENTE,:PM_GRUPO,:PM_GRUPO_OLD,:PM_ACT,:PM_CODERRO,:PM_TXTERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    If ACT = "I" Then
        MsgBox "A��o Incluida com sucesso"
    Else
        MsgBox "A��o Alterada com sucesso"
    End If
    
    'LOADGRID
    cmd2_Click


End Sub

Private Sub cmd2_Click()

    txtCodCliente.Text = ""
    txtNomeCliente.Text = ""
    cmbGrupo.ListIndex = -1
    txtCOD.Text = 0
    ACT = "I"

End Sub

Private Sub cmd3_Click()
    
    CMD.Filter = "Excel (*.xls)|*.xls"
    CMD.ShowOpen
    
    ArqPath = CMD.FileName
    
    If MsgBox("Confirma o inicio da importa��o", vbYesNo, "Aten��o") = vbYes Then
        IMPORXLS
    End If

End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    
    Me.Top = 1500
    Me.Left = 300
    
    txtCOD.Text = 0
    
    '**** COMBO GRUPOS
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
    
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "Producao.PCK_CAD990.PR_CON_GRUPOS(:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
    
    vVB_Generica_001.PreencheComboList vObjOracle, cmbGrupo, "C�D.", "GRUPO"
    '***********
    
    ACT = "I"
    
End Sub

Public Sub txtCodCliente_LOSTFOCUS()

    If txtCodCliente.Text <> "" Then
    
        Set vObjOracle = vVB_Generica_001.TabelaCliente(vBanco, txtCodCliente, , , , , , , , , 0)
        
        If Not vObjOracle.EOF Then
        
            txtNomeCliente.Text = vObjOracle("CLIENTE")

            vBanco.Parameters.Remove "PM_CODCLIENTE"
            vBanco.Parameters.Add "PM_CODCLIENTE", txtCodCliente.Text, 1

            vBanco.Parameters.Remove "PM_CURSOR1"
            vBanco.Parameters.Add "PM_CURSOR1", 0, 3
            vBanco.Parameters("PM_CURSOR1").ServerType = 102
            vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
            vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

            vBanco.Parameters.Remove "PM_CODERRO"
            vBanco.Parameters.Add "PM_CODERRO", 0, 2
            vBanco.Parameters.Remove "PM_TXTERRO"
            vBanco.Parameters.Add "PM_TXTERRO", "", 2

            vSql = "Producao.PCK_CAD990.PR_CON_CLIGRUPO(:PM_CODCLIENTE,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"

            vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

            Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value

            If Not vObjOracle.EOF Then
            
                cmbGrupo.Text = vObjOracle(1)
                txtCOD = Left(vObjOracle(1), InStr(1, vObjOracle(1), " - ") - 1)
                ACT = "A"

            End If
            
        Else

            MsgBox "Cliente n�o encontrado", , "Aten��o"
            cmd2_Click
            txtCodCliente.SetFocus

        End If
    Else
        cmd2_Click
    End If

End Sub

Private Sub txtCodCliente_DblClick()
    frmClientes.Show 1
End Sub



Private Sub IMPORXLS()
On Error GoTo Trata_erro

    Dim xl As New Excel.Application
    Dim xlB As Excel.Workbook
    Dim Fim, ErroC As Integer
    Dim Celula As String
    Dim CODCLI As Double
    Dim CODGRUPO As Integer
    Dim ErrCod As String
    
    Screen.MousePointer = 11
    DoEvents
    
    'Verifica se foi indicado o diret�rio e nome do arquivo a ser lido
    If ArqPath = "" Then
        MsgBox "Indique o arquivo a ser aberto", , MSG_TIT
        Screen.MousePointer = 0
        Exit Sub
    End If

    xl.Workbooks.Add ArqPath
    
    'xlB.Activate
    xl.Visible = True
    
    If xl.Cells(1, 1).Value = "" Then
        MsgBox "Primeira linha do Arquivo n�o poder estar em branco", vbInformation, "Aten��o"
        Screen.MousePointer = 0
        Exit Sub
    ElseIf xl.Cells(2, 1).Value = "" Then
        
        Fim = 1
    
    Else
        
        xl.Cells(1, 1).Select
        xl.ActiveCell.End(xlDown).Select

        Fim = xl.ActiveCell.Row
        
    End If
    
    xl.Cells(1, 1).Select
    
    FLERRO = 0
    
    For i = 1 To Fim
        ErroC = 0
        
        CODCLI = xl.Cells(i, 1).Value
        CODGRUPO = xl.Cells(i, 2).Value
        
        If IsNumeric(CODCLI) And IsNumeric(CODGRUPO) Then
        
            Set vObjOracle = vVB_Generica_001.TabelaCliente(vBanco, CODCLI)
            
            If Not vObjOracle.EOF Then
            
                
                vBanco.Parameters.Remove "PM_CODCLIENTE"
                vBanco.Parameters.Add "PM_CODCLIENTE", CODCLI, 1
                
                vBanco.Parameters.Remove "PM_CURSOR1"
                vBanco.Parameters.Add "PM_CURSOR1", 0, 3
                vBanco.Parameters("PM_CURSOR1").ServerType = 102
                vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
                vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
                
                vBanco.Parameters.Remove "PM_CODERRO"
                vBanco.Parameters.Add "PM_CODERRO", 0, 2
                vBanco.Parameters.Remove "PM_TXTERRO"
                vBanco.Parameters.Add "PM_TXTERRO", "", 2
                
                vSql = "Producao.PCK_CAD990.PR_CON_CLIGRUPO(:PM_CODCLIENTE,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
                
                vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
                
                Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
                
                If Not vObjOracle.EOF Then
                
                    CODGRUPO_ANT = Left(vObjOracle(1), InStr(1, vObjOracle(1), " - ") - 1)
                    ACT = "A"
                
                Else
                
                    CODGRUPO_ANT = 0
                    ACT = "I"
                
                End If
                
                
                vBanco.Parameters.Remove "PM_CLIENTE"
                vBanco.Parameters.Add "PM_CLIENTE", CODCLI, 1
                
                vBanco.Parameters.Remove "PM_GRUPO"
                vBanco.Parameters.Add "PM_GRUPO", CODGRUPO, 1
                
                vBanco.Parameters.Remove "PM_GRUPO_OLD"
                vBanco.Parameters.Add "PM_GRUPO_OLD", CODGRUPO_ANT, 1
                
                vBanco.Parameters.Remove "PM_ACT"
                vBanco.Parameters.Add "PM_ACT", ACT, 1
                
                vBanco.Parameters.Remove "PM_CODERRO"
                vBanco.Parameters.Add "PM_CODERRO", 0, 2
                vBanco.Parameters.Remove "PM_TXTERRO"
                vBanco.Parameters.Add "PM_TXTERRO", "", 2
                
                vSql = "Producao.PCK_CAD990.PR_ACT_CLIGRUPO(:PM_CLIENTE,:PM_GRUPO,:PM_GRUPO_OLD,:PM_ACT,:PM_CODERRO,:PM_TXTERRO)"
                
                vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
                
            
            Else
            
                ErrCod = ErrCod & CODCLI & " - "
                
            End If
            
        Else
        
            ErrCod = ErrCod & CODCLI & " - "
        
        End If
        
        
    Next
    
    If ErrCod <> "" Then
        MsgBox "C�digos com problema" & vbCrLf & Left(ErrCod, Len(ErrCod) - 3), vbCritical, "Aten��o"
    End If
    
    xl.Workbooks.Close
    
    Set xl = Nothing
    Screen.MousePointer = vbNormal
    
Trata_erro:
    If Err.Number = -2147417851 Then
        Resume Next
    ElseIf Err.Number <> 0 Then
        MsgBox Err.Number & vbCrLf & Err.Description
        Screen.MousePointer = vbNormal
    End If
End Sub
