VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmCadastro 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Formas De Pagamento"
   ClientHeight    =   3615
   ClientLeft      =   1260
   ClientTop       =   1530
   ClientWidth     =   6570
   Icon            =   "frmCadastro.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   241
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   438
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraCadastro 
      Caption         =   "Formas de Pagamento"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   2235
      Left            =   0
      TabIndex        =   7
      Top             =   930
      Width           =   6570
      Begin VB.ComboBox cboDesc_Forma_Pagto 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000006&
         Height          =   330
         Left            =   1320
         TabIndex        =   1
         Top             =   1350
         Width           =   4785
      End
      Begin VB.TextBox txtCod_Forma_Pagto 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1320
         MaxLength       =   2
         TabIndex        =   0
         Top             =   600
         Width           =   495
      End
      Begin VB.Label lbl1 
         Appearance      =   0  'Flat
         Caption         =   "Descri��o"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   225
         Left            =   300
         TabIndex        =   9
         Top             =   1440
         Width           =   870
      End
      Begin VB.Label lbl2 
         Appearance      =   0  'Flat
         Caption         =   "C�digo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   540
         TabIndex        =   8
         Top             =   690
         Width           =   600
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   5
      Top             =   3285
      Width           =   6570
      _ExtentX        =   11589
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   11536
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   810
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   15
      TabIndex        =   2
      ToolTipText     =   "Voltar"
      Top             =   15
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadastro.frx":23D2
      PICN            =   "frmCadastro.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdLimpar 
      Height          =   690
      Left            =   810
      TabIndex        =   3
      ToolTipText     =   "Limpar"
      Top             =   15
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadastro.frx":30C8
      PICN            =   "frmCadastro.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdGravar 
      Default         =   -1  'True
      Height          =   690
      Left            =   1590
      TabIndex        =   4
      ToolTipText     =   "Gravar"
      Top             =   15
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadastro.frx":39BE
      PICN            =   "frmCadastro.frx":39DA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmCadastro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim vFormaPagto_ant As String

'Private Sub cboDesc_Forma_Pagto_Change()
'
'    If Mid(cboDesc_Forma_Pagto, 4, 1) = "-" Then
'        cboDesc_Forma_Pagto.Text = Right(cboDesc_Forma_Pagto, 40)
'    End If
'
'End Sub

Private Sub cboDesc_Forma_Pagto_GotFocus()

    Call vVB_Generica_001.SelecionaCampo(cboDesc_Forma_Pagto)
    
End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub

Private Sub Form_Load()
    
    Me.Top = 0
    Me.Left = 0
    Set vObjOracle = vVB_Cobranca_001.TabelaFormaPagamento(vBanco)
    Call vVB_Generica_001.PreencheComboList(vObjOracle, cboDesc_Forma_Pagto, vObjOracle.Fields(0).Name, vObjOracle.Fields(1).Name)
    
End Sub

Private Sub cboDesc_Forma_Pagto_Click()

    txtCod_Forma_Pagto.Text = Left(cboDesc_Forma_Pagto.Text, 2)
    vFormaPagto_ant = cboDesc_Forma_Pagto.Text
    If Mid(cboDesc_Forma_Pagto, 4, 1) = "-" Or Mid(cboDesc_Forma_Pagto, 3, 1) = "-" Then
        cboDesc_Forma_Pagto.Text = Trim(Mid(cboDesc_Forma_Pagto, 5, Len(cboDesc_Forma_Pagto) - 4))
    End If
    
End Sub

Private Sub cboDesc_Forma_Pagto_KeyPress(KeyAscii As Integer)

    If Len(cboDesc_Forma_Pagto) >= 40 And InStr(1, "/ABCDEFGHIJLMNOPQRSTUVW -.;,*XZY1234567890", Chr(KeyAscii)) <> 0 Then
        KeyAscii = 0
    End If
    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
    
End Sub

Private Sub cmdGravar_Click()

    If Trim(txtCod_Forma_Pagto) = "" Then
        Call vVB_Generica_001.Informar("Campo(s) Em Branco!")
        txtCod_Forma_Pagto.SetFocus
        Exit Sub
    End If
    If Trim(cboDesc_Forma_Pagto) = "" Then
        Call vVB_Generica_001.Informar("Campo(s) Em Branco!")
        cboDesc_Forma_Pagto.SetFocus
        Exit Sub
    End If
    If vFormaPagto_ant = cboDesc_Forma_Pagto.Text Then
        Call vVB_Generica_001.Informar("Descri��o N�o Alterada!")
        cboDesc_Forma_Pagto.SetFocus
        Exit Sub
    End If
    If vFormaPagto_ant = Trim(txtCod_Forma_Pagto & "-" & cboDesc_Forma_Pagto.Text) Then
        Call vVB_Generica_001.Informar("Descri��o N�o Alterada!")
        cboDesc_Forma_Pagto.SetFocus
        Exit Sub
    End If
    If Len(cboDesc_Forma_Pagto.Text) > 40 Then
        Call vVB_Generica_001.Informar("Descri��o pode ter no m�ximo 40 caracteres!")
        cboDesc_Forma_Pagto = ""
        cboDesc_Forma_Pagto.SetFocus
        Exit Sub
    End If
    If vVB_Generica_001.Perguntar("Confirma Dados?") = 6 Then
       Call vVB_Generica_001.ExcluiBind(vBanco)
       Call Criar_Cursor(vBanco, "vCursor")
       vBanco.Parameters.Add "vCodForma", txtCod_Forma_Pagto.Text, 1
       vBanco.Parameters.Add "vDescForma", cboDesc_Forma_Pagto.Text, 1
       vSql = "cobranca.pck_cad250.pr_alterar(:vCursor, :vCodForma, :vDescForma)"
       vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
       If vErro <> "" Then
           vVB_Generica_001.ProcessaErro (vErro)
       Else
           Call vVB_Generica_001.Informar("Registro Gravado!")
           Call cmdLimpar_Click
           Set vObjOracle = vVB_Cobranca_001.TabelaFormaPagamento(vBanco)
           Call vVB_Generica_001.PreencheComboList(vObjOracle, cboDesc_Forma_Pagto, vObjOracle.Fields(0).Name, vObjOracle.Fields(1).Name)
       End If
    Else
       Call cmdLimpar_Click
    End If

End Sub


Private Sub cmdLimpar_Click()
    txtCod_Forma_Pagto = ""
    cboDesc_Forma_Pagto = ""
    txtCod_Forma_Pagto.SetFocus
    
End Sub


Private Sub txtCod_Forma_Pagto_Change()
          
    If Len(txtCod_Forma_Pagto) = 2 Then
        cboDesc_Forma_Pagto.SetFocus
    End If
    Call txtCod_Forma_Pagto_LostFocus
    
End Sub

Private Sub txtCod_Forma_Pagto_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Numero(KeyAscii)
    
End Sub

Private Sub txtCod_Forma_Pagto_LostFocus()

    If Len(txtCod_Forma_Pagto.Text) = 0 Then
        cboDesc_Forma_Pagto.Text = ""
        Exit Sub
    Else
        Call vVB_Generica_001.ExcluiBind(vBanco)
        Call Criar_Cursor(vBanco, "vCursor")
        vBanco.Parameters.Add "vCodForma", txtCod_Forma_Pagto.Text, 1
        vSql = "cobranca.pck_cad250.pr_consultar(:vCursor, :vCodForma)"
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        If vErro <> "" Then
            vVB_Generica_001.ProcessaErro (vErro)
        Else
            Set vObjOracle = vParameters("vCursor").Value
        End If
        cboDesc_Forma_Pagto.Text = IIf(IsNull(vObjOracle!desc_forma_pagto) = True, "", vObjOracle!desc_forma_pagto)
        vFormaPagto_ant = cboDesc_Forma_Pagto.Text
    End If
    
End Sub
    


