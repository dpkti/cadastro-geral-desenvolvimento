create or replace package cobranca.pck_cad250 is

  -- Author  : CARLOS.MOURA
  -- Created : 24/6/2004 15:52:00
  -- Purpose : 
  
  type tp_cursor is ref cursor;
  
 PROCEDURE pr_alterar
  (
    p_cursor	in out tp_cursor,
    pm_cod_forma_pagto in cobranca.forma_pagto.cod_forma_pagto%type,
    pm_desc_forma_pagto in cobranca.forma_pagto.desc_forma_pagto%type
  );

  PROCEDURE pr_consultar
  (
    p_cursor	in out tp_cursor,
    pm_cod_forma_pagto in cobranca.forma_pagto.cod_forma_pagto%type
   );

end;
/
create or replace package body cobranca.pck_cad250 is
  PROCEDURE pr_alterar
  (
    p_cursor	in out tp_cursor,
    pm_cod_forma_pagto in cobranca.forma_pagto.cod_forma_pagto%type,
    pm_desc_forma_pagto in cobranca.forma_pagto.desc_forma_pagto%type
   )
   is
   begin

        UPDATE cobranca.forma_pagto SET desc_forma_pagto=pm_desc_forma_pagto WHERE cod_forma_pagto=pm_cod_forma_pagto;
        IF SQL%NOTFOUND THEN
            INSERT INTO cobranca.forma_pagto (cod_forma_pagto,desc_forma_pagto) VALUES (pm_cod_forma_pagto,pm_desc_forma_pagto);
        END IF;
        open p_cursor for select 'X' from dual;
        COMMIT;

   end pr_alterar;

  PROCEDURE pr_consultar
  (
    p_cursor	in out tp_cursor,
    pm_cod_forma_pagto in cobranca.forma_pagto.cod_forma_pagto%type
   )
   is
   begin
        open p_cursor for
        select desc_forma_pagto
        from cobranca.forma_pagto
        where cod_forma_pagto=pm_cod_forma_pagto;
        COMMIT;

    end pr_consultar;

end pck_cad250;
/
