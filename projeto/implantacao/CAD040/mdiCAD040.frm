VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.MDIForm mdiCAD040 
   Appearance      =   0  'Flat
   BackColor       =   &H8000000F&
   Caption         =   "CAD040 - Cadastro de Representantes"
   ClientHeight    =   8175
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11595
   Icon            =   "mdiCAD040.frx":0000
   LinkTopic       =   "MDIForm1"
   WindowState     =   2  'Maximized
   Begin Threed.SSPanel SSPanel1 
      Align           =   1  'Align Top
      Height          =   1230
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   11595
      _Version        =   65536
      _ExtentX        =   20452
      _ExtentY        =   2170
      _StockProps     =   15
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BevelWidth      =   0
      BorderWidth     =   0
      BevelOuter      =   0
      Begin pkGradientControl.pkGradient pkGradient1 
         Height          =   30
         Left            =   45
         TabIndex        =   2
         Top             =   1125
         Width           =   11580
         _ExtentX        =   20426
         _ExtentY        =   53
         Color1          =   0
         Color2          =   -2147483633
         BackColor       =   -2147483633
      End
      Begin Bot�o.cmd cmdRepresentante 
         Height          =   915
         Left            =   1350
         TabIndex        =   3
         TabStop         =   0   'False
         ToolTipText     =   "Informa��es do Representante"
         Top             =   120
         Width           =   960
         _ExtentX        =   1693
         _ExtentY        =   1614
         BTYPE           =   3
         TX              =   "Cadastro"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiCAD040.frx":23D2
         PICN            =   "mdiCAD040.frx":23EE
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdPessoal 
         Height          =   915
         Left            =   2355
         TabIndex        =   4
         TabStop         =   0   'False
         ToolTipText     =   "Informa��es Pessoais"
         Top             =   120
         Width           =   960
         _ExtentX        =   1693
         _ExtentY        =   1614
         BTYPE           =   3
         TX              =   "Pessoal"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiCAD040.frx":30C8
         PICN            =   "mdiCAD040.frx":30E4
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdRepresentacao 
         Height          =   915
         Left            =   3360
         TabIndex        =   5
         TabStop         =   0   'False
         ToolTipText     =   "Informa��es sobre a Representa��o"
         Top             =   120
         Width           =   960
         _ExtentX        =   1693
         _ExtentY        =   1614
         BTYPE           =   3
         TX              =   "Empresa"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiCAD040.frx":3DBE
         PICN            =   "mdiCAD040.frx":3DDA
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdContrato 
         Height          =   915
         Left            =   4365
         TabIndex        =   6
         TabStop         =   0   'False
         ToolTipText     =   "Contrato"
         Top             =   120
         Width           =   960
         _ExtentX        =   1693
         _ExtentY        =   1614
         BTYPE           =   3
         TX              =   "Contrato"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiCAD040.frx":2D9FC
         PICN            =   "mdiCAD040.frx":2DA18
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdQuota 
         Height          =   915
         Left            =   6765
         TabIndex        =   7
         TabStop         =   0   'False
         ToolTipText     =   "Cadastro/C�pia/Importa��o de Quota"
         Top             =   120
         Width           =   960
         _ExtentX        =   1693
         _ExtentY        =   1614
         BTYPE           =   3
         TX              =   "Quota"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiCAD040.frx":2E6F2
         PICN            =   "mdiCAD040.frx":2E70E
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdSair 
         Height          =   915
         Left            =   10815
         TabIndex        =   8
         TabStop         =   0   'False
         ToolTipText     =   "Sair do sistema"
         Top             =   120
         Width           =   720
         _ExtentX        =   1270
         _ExtentY        =   1614
         BTYPE           =   3
         TX              =   "Sair"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiCAD040.frx":2EFE8
         PICN            =   "mdiCAD040.frx":2F004
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdSobre 
         Height          =   915
         Left            =   10080
         TabIndex        =   9
         TabStop         =   0   'False
         ToolTipText     =   "Sobre"
         Top             =   120
         Width           =   720
         _ExtentX        =   1270
         _ExtentY        =   1614
         BTYPE           =   3
         TX              =   "Sobre"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiCAD040.frx":2FCDE
         PICN            =   "mdiCAD040.frx":2FCFA
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdImporta 
         Height          =   915
         Left            =   7740
         TabIndex        =   10
         TabStop         =   0   'False
         ToolTipText     =   "Cadastro/C�pia/Importa��o de Quota"
         Top             =   120
         Width           =   960
         _ExtentX        =   1693
         _ExtentY        =   1614
         BTYPE           =   3
         TX              =   "Importa"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiCAD040.frx":309D4
         PICN            =   "mdiCAD040.frx":309F0
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdTrocarTime 
         Height          =   915
         Left            =   8715
         TabIndex        =   11
         TabStop         =   0   'False
         ToolTipText     =   "Troca de Gerente/Representantes"
         Top             =   120
         Width           =   1290
         _ExtentX        =   2275
         _ExtentY        =   1614
         BTYPE           =   3
         TX              =   "Rel Gerente"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiCAD040.frx":316CA
         PICN            =   "mdiCAD040.frx":316E6
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdDesligamento 
         Height          =   915
         Left            =   5370
         TabIndex        =   12
         TabStop         =   0   'False
         ToolTipText     =   "Datas de"
         Top             =   120
         Width           =   1320
         _ExtentX        =   2328
         _ExtentY        =   1614
         BTYPE           =   3
         TX              =   "Desligamento"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiCAD040.frx":31FC0
         PICN            =   "mdiCAD040.frx":31FDC
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Image Image1 
         Height          =   1185
         Left            =   -90
         Picture         =   "mdiCAD040.frx":32CB6
         Stretch         =   -1  'True
         ToolTipText     =   "Acessar a Intranet"
         Top             =   -45
         Width           =   1455
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   7845
      Width           =   11595
      _ExtentX        =   20452
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   7303
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   2646
            MinWidth        =   2646
            Object.ToolTipText     =   "Usu�rio da rede"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   2381
            MinWidth        =   2381
            Object.ToolTipText     =   "Usu�rio do banco de dados"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   3528
            MinWidth        =   3528
            Object.ToolTipText     =   "Banco de dados conectado"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            Alignment       =   1
            Object.Width           =   2381
            MinWidth        =   2381
            TextSave        =   "20/07/15"
            Object.ToolTipText     =   "Data"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   1
            Object.Width           =   1587
            MinWidth        =   1587
            TextSave        =   "17:55"
            Object.ToolTipText     =   "Hora"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "mdiCAD040"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdContrato_Click()
    frmContrato.Show vbModal
End Sub

Private Sub cmdCopia_Quota_Click()
'    frmCopia_Quota.Show vbModal
End Sub

Private Sub cmdImporta_Quota_Click()
'    frmImportacao.Show vbModal
End Sub

Private Sub cmdDesligamento_Click()
    frmConsultaRepres.Show 1
End Sub

Private Sub cmdImporta_Click()

    frmImporta.Show vbModal
    
End Sub

Private Sub cmdPessoal_Click()
    frmPessoal.Show vbModal
End Sub

Private Sub cmdQuota_Click()

    frmQuota.Show vbModal
    
End Sub

Private Sub cmdRepresentacao_Click()

    frmRepresentacao.Show vbModal
    
End Sub

Private Sub cmdRepresentante_Click()
    frmRepresentante.Show vbModal
End Sub

Private Sub cmdSair_Click()

    If vVB_Generica_001.Sair = 6 Then
        Set vObjOracle = Nothing
        Set vBanco = Nothing
        Set vSessao = Nothing
        
        End
    End If

End Sub
Private Sub cmdSobre_Click()
    
    frmSobre.Show 1

End Sub

Private Sub cmdTrocarTime_Click()
    frmTrocaGerente.Show 1
End Sub

Private Sub Image1_Click()

    Shell "C:\Arquivos de programas\Internet Explorer\IEXPLORE.EXE intranet.dpk.com.br", vbNormalFocus

End Sub


Private Sub MDIForm_Load()
    frmLogo.Show 1

   vErro = vVB_Generica_001.ConectaOracle("SDPK", "CAD040", True, Me)
   'vErro = vVB_Generica_001.ConectaOracle("SDPKT", "CAD040", True, Me)
    
    If vErro <> "" Then
        Call vVB_Generica_001.ProcessaErro(vErro)
        End
    End If

    vCD = vVB_Generica_001.vCD
    vTipoCD = vVB_Generica_001.vTipoCD
    Set vSessao = vVB_Generica_001.vSessao
    Set vBanco = vVB_Generica_001.vBanco
    
    Set vObjOracle = vVB_Generica_001.InformacoesSistema(vBanco, UCase(App.Title))
    
    If vObjOracle.EOF Then
        Call vVB_Generica_001.Informar("Sistema n�o cadastrado. � necess�rio fazer o cadastro para executar o programa.")
        End
    End If
    
    Call DefinirTelaSobre
    
    Me.Caption = Me.Caption & " - Vers�o: " & App.Major & "." & App.Minor & "." & App.Revision
    
    ' fpallini - Ci&T - Inicializa o servi�o de ProxySAP
    Set oServicoClienteSAP = New ServicoClienteSAP
    oServicoClienteSAP.ModoDebug = IIf(Command = "DEBUG", True, False)
End Sub
