VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form frmConsultaRepres 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Data de Desligamento"
   ClientHeight    =   7860
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11235
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "frmConsultaRepres.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7860
   ScaleWidth      =   11235
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ListView lsvRepres 
      Height          =   6945
      Left            =   90
      TabIndex        =   2
      Top             =   870
      Width           =   11115
      _ExtentX        =   19606
      _ExtentY        =   12250
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   10
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "C�d."
         Object.Width           =   1058
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Nome Repres"
         Object.Width           =   2999
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Tipo"
         Object.Width           =   882
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   3
         Text            =   "C�d."
         Object.Width           =   1058
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "Nome Filial"
         Object.Width           =   3881
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   5
         Text            =   "Dt Deslig"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   6
         Text            =   "C�d."
         Object.Width           =   1058
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   7
         Text            =   "Nome Gerente"
         Object.Width           =   2646
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   8
         Text            =   "Total Cliente"
         Object.Width           =   1940
      EndProperty
      BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   9
         Text            =   "Total Dupl."
         Object.Width           =   1764
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   0
      Top             =   810
      Width           =   11145
      _ExtentX        =   19659
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsultaRepres.frx":23D2
      PICN            =   "frmConsultaRepres.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdExcel 
      Height          =   690
      Left            =   10440
      TabIndex        =   3
      TabStop         =   0   'False
      ToolTipText     =   "Exportar para Excel"
      Top             =   90
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsultaRepres.frx":30C8
      PICN            =   "frmConsultaRepres.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmConsultaRepres"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdVoltar_Click()
    Unload Me
End Sub
Private Sub Form_Load()
    
    Dim vObj As Object
    Dim i As Integer
    Dim Litem As ListItem
    
    Criar_Cursor vBanco.Parameters, "vCursor"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, "PRODUCAO.PCK_CAD040.PR_SELECT_REPRES_DESLIGAMENTO(:VCURSOR)")
    If vErro <> "" Then
    Else
        Set vObj = vVB_Generica_001.vBanco.Parameters("vCURSOR").Value
    End If

    For i = 1 To vObj.RecordCount
        Set Litem = Me.lsvRepres.ListItems.Add
        Litem = vObj!cod_repres
        Litem.SubItems(1) = vObj!Pseudonimo
        Litem.SubItems(2) = vObj!TIPO
        Litem.SubItems(3) = vObj!Cod_filial
        Litem.SubItems(4) = vObj!filial
        Litem.SubItems(5) = vObj!DT_DESLIGAMENTO
        Litem.SubItems(6) = vObj!COD_GERENTE
        Litem.SubItems(7) = vObj!GERENTE
        Litem.SubItems(8) = vObj!TOTAL_CLIENTE
        Litem.SubItems(9) = vObj!TOTAL_DUPLICATAS
        vObj.MoveNext
    Next

End Sub

Private Sub lsvRepres_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    
    If ColumnHeader.Index = 1 Or ColumnHeader.Index = 4 Or ColumnHeader.Index = 7 Then
        SortLvwOnLong lsvRepres, ColumnHeader.Index
    ElseIf ColumnHeader.Index = 6 Then
        SortLvwOnDate lsvRepres, ColumnHeader.Index
    Else
        lsvRepres.SortKey = ColumnHeader.Index - 1
        lsvRepres.Sorted = True
        lsvRepres.SortOrder = lvwAscending
    End If

End Sub
Private Sub cmdExcel_Click()
    Dim oExcel As Object
    Dim objExlSht As Object
    Dim i As Integer
    Dim ii As Integer

    Me.MousePointer = vbHourglass

    Me.Refresh

    Set oExcel = CreateObject("Excel.Application")
    oExcel.Workbooks.Add                                           'inclui o workbook
    Set objExlSht = oExcel.ActiveWorkbook.Sheets(1)
 
    oExcel.Cells(1, 1) = "C�d Repres"
    oExcel.Cells(1, 2) = "Nome Repres"
    oExcel.Cells(1, 3) = "Tipo"
    oExcel.Cells(1, 4) = "C�d Filial"
    oExcel.Cells(1, 5) = "Nome Filial"
    oExcel.Cells(1, 6) = "C�d Dt Desligamento"
    oExcel.Cells(1, 7) = "C�d Gerente"
    oExcel.Cells(1, 8) = "Nome Gerente"
    oExcel.Cells(1, 9) = "Total Cliente"
    oExcel.Cells(1, 10) = "Total Duplicatas"
    
    For i = 1 To lsvRepres.ListItems.Count
        oExcel.Cells(i + 1, 1).Select
        oExcel.ActiveCell = lsvRepres.ListItems(i)
        For ii = 1 To lsvRepres.ColumnHeaders.Count - 1
            oExcel.Cells(i + 1, ii + 1).Select
            oExcel.ActiveCell = lsvRepres.ListItems(i).SubItems(ii)
        Next
    Next
    
    oExcel.Visible = True

    Set objExlSht = Nothing
    Set oExcel = Nothing
    
    Me.MousePointer = vbNormal
End Sub

