VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form frmDuplAberto 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Duplicatas em Aberto"
   ClientHeight    =   6180
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6270
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "frmDuplAberto.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   6180
   ScaleWidth      =   6270
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ListView lsvDuplicatas 
      Height          =   5025
      Left            =   45
      TabIndex        =   2
      Top             =   1125
      Width           =   6165
      _ExtentX        =   10874
      _ExtentY        =   8864
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   5
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Duplicata"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Vencimento"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Cod Cliente"
         Object.Width           =   2117
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "CD"
         Object.Width           =   882
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "Digito"
         Object.Width           =   2
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   0
      Top             =   810
      Width           =   6195
      _ExtentX        =   10927
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   30
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmDuplAberto.frx":23D2
      PICN            =   "frmDuplAberto.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdImprimir 
      Height          =   690
      Left            =   5490
      TabIndex        =   4
      TabStop         =   0   'False
      ToolTipText     =   "Imprimir"
      Top             =   60
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmDuplAberto.frx":30C8
      PICN            =   "frmDuplAberto.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdGerarArquivo 
      Height          =   690
      Left            =   4725
      TabIndex        =   5
      TabStop         =   0   'False
      ToolTipText     =   "Gerar Arquivo"
      Top             =   60
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmDuplAberto.frx":3DBE
      PICN            =   "frmDuplAberto.frx":3DDA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdTrocarRepres 
      Height          =   690
      Left            =   3960
      TabIndex        =   6
      TabStop         =   0   'False
      ToolTipText     =   "Trocar Respons�vel"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmDuplAberto.frx":4AB4
      PICN            =   "frmDuplAberto.frx":4AD0
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Duplicatas em Aberto:"
      Height          =   195
      Left            =   60
      TabIndex        =   3
      Top             =   900
      Width           =   1560
   End
End
Attribute VB_Name = "frmDuplAberto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim rs As Object
Public repNovo As Integer

Private Sub cmdGerarArquivo_Click()
    Dim vArq As Integer
    vArq = FreeFile
    Open "C:\Duplicatas.txt" For Output As #vArq
    
    Print #vArq, "Duplic" & vbTab & "Dt Vencto" & vbTab & "Cliente"
    For i = 1 To lsvDuplicatas.ListItems.Count
        Print #vArq, lsvDuplicatas.ListItems(i) & vbTab & lsvDuplicatas.ListItems(i).SubItems(1) & vbTab & lsvDuplicatas.ListItems(i).SubItems(2) & vbTab & lsvDuplicatas.ListItems(i).SubItems(3) & vbTab & lsvDuplicatas.ListItems(i).SubItems(4)
    Next
    
    Close #vArq
    MsgBox "Arquivo C:\DUPLICATAS.TXT Gerado com sucesso!!", vbInformation, "Aten��o"
    
End Sub

Private Sub cmdImprimir_Click()

    If Me.lsvDuplicatas.ListItems.Count = 0 Then Exit Sub

    Printer.Print "Duplicata" & vbTab & "Vencimento" & vbTab & "Cliente"
    For i = 1 To 50
        Printer.Print lsvDuplicatas.ListItems(i) & vbTab & lsvDuplicatas.ListItems(i).SubItems(1) & vbTab & lsvDuplicatas.ListItems(i).SubItems(2) & vbTab & lsvDuplicatas.ListItems(i).SubItems(3) & vbTab & lsvDuplicatas.ListItems(i).SubItems(4)
    Next
    
    Close #vArq
    MsgBox "Impress�o efetuada com sucesso!!", vbInformation, "Aten��o"
End Sub

Private Sub cmdTrocarRepres_Click()
    Dim tpRepres As String
    
    tpRepres = Trim(Left(frmRepresentante.cmbTipo_Repres.Text, InStr(frmRepresentante.cmbTipo_Repres.Text, " - ")))
    
    If tpRepres = "A" Or tpRepres = "M" Or tpRepres = "V" Then
    
        If MsgBox("Confirma a transfer�ncia de todas as duplicatas para os novos respons�veis?", vbYesNo, "ATEN��O") = vbYes Then
            trocarDuplicatas rs
        End If
    
    Else
    
        frmTrocarDuplicatas.Show 1
    
    End If
    
    
    'If flFormDupl_Fechado = False Then
    '    Me.Visible = True
    'End If
    'StayOnTop frmDuplAberto
End Sub

Private Sub cmdVoltar_Click()

    Unload Me
    
End Sub


Public Sub Preencher_lsv(Rst As Object)
    
    Dim Litem As ListItem
    
    lsvDuplicatas.ListItems.Clear
    
    For i = 1 To Rst.RecordCount
        Set Litem = lsvDuplicatas.ListItems.Add
        Litem = Rst!num_fatura & "-" & Rst!num_ordem
        Litem.SubItems(1) = Rst!Dt_Vencimento
        Litem.SubItems(2) = Rst!COD_CLIENTE
        Litem.SubItems(3) = Rst!cod_loja
        Litem.SubItems(4) = Rst!num_ordem
        Rst.MoveNext
    Next
    
End Sub

Private Sub Form_Load()
    Set rs = vObjOracle
    Preencher_lsv rs
    'StayOnTop frmDuplAberto
End Sub

Private Sub Form_LostFocus()
    If Form_Aberto("FORMDUPLABERTO") = True Then
        frmDuplAberto.SetFocus
    End If
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    frmRepresentante.Visible = True
    frmRepresentante.WindowState = 0
End Sub

Private Sub trocarDuplicatas(Rst As Object)

    Screen.MousePointer = vbHourglass

    Dim rs1 As Object
    Dim vret As Boolean
    
    Rst.MoveFirst
    
    While Not Rst.EOF
        
        Criar_Cursor vBanco.Parameters, "vCursor"
        vBanco.Parameters.Remove "PM_CODCLI"
        vBanco.Parameters.Add "PM_CODCLI", Rst!COD_CLIENTE, 1
        'vBanco.Parameters.Remove "PM_CODVEND"
        'vBanco.Parameters.Add "PM_CODVEND", frmRepresentante.txtCod_Repres.Text, 1
        
        vSql = "Producao.PCK_CAD040.PR_CON_DUPLICATA_REPRES(:PM_CODCLI,:vCursor)"
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        Set rs1 = vBanco.Parameters("VCURSOR").Value
        
        If rs1.RecordCount > 1 Then
            
            Set frmSelecResponsavel.rs = rs1
            frmSelecResponsavel.Show 1
            
            vret = transfereDuplicata(frmRepresentante.txtCod_Repres.Text, repNovo, Rst!cod_loja, Rst!num_fatura, Rst!num_ordem, Rst!Dt_Vencimento)
            If vret = True Then
                Unload Me
                Exit Sub
            End If
            
        ElseIf rs1.RecordCount = 0 Then
            MsgBox ("Cliente sem representante - " & Rst!COD_CLIENTE)
            Unload Me
            Exit Sub
        Else
        
            If rs1!COD_VEND = 0 Or rs1!COD_VEND = frmRepresentante.txtCod_Repres.Text Then
                repNovo = rs1!cod_repres
            Else
                repNovo = rs1!COD_VEND
            End If
            
            vret = transfereDuplicata(frmRepresentante.txtCod_Repres.Text, repNovo, Rst!cod_loja, Rst!num_fatura, Rst!num_ordem, Rst!Dt_Vencimento)
            If vret = True Then
                Unload Me
                Exit Sub
            End If
        
        End If
    
        Rst.MoveNext
        
    Wend
    
    desativaRepres
    
    Screen.MousePointer = vbNormal
    
    MsgBox ("Duplicatas transferidas com sucesso")
    
    Unload Me

End Sub



Public Function transfereDuplicata(repAnt As Integer, repNovo As Integer, loja As Integer, fatura As String, ordem As Integer, dtFatura As String) As Boolean

    vBanco.Parameters.Remove "PM_CODREPRES"
    vBanco.Parameters.Add "PM_CODREPRES", repAnt, 1
    vBanco.Parameters.Remove "PM_CODREPRES_NOVO"
    vBanco.Parameters.Add "PM_CODREPRES_NOVO", repNovo, 1
    vBanco.Parameters.Remove "PM_CODLOJA"
    vBanco.Parameters.Add "PM_CODLOJA", loja, 1
    vBanco.Parameters.Remove "PM_NUMDUPLI"
    vBanco.Parameters.Add "PM_NUMDUPLI", fatura, 1
    vBanco.Parameters.Remove "PM_NUMORDEM"
    vBanco.Parameters.Add "PM_NUMORDEM", ordem, 1
    vBanco.Parameters.Remove "PM_DTFATURA"
    vBanco.Parameters.Add "PM_DTFATURA", dtFatura, 1
    vBanco.Parameters.Remove "PM_ERRO"
    vBanco.Parameters.Add "PM_ERRO", 0, 2
    vBanco.Parameters.Remove "PM_MSGERRO"
    vBanco.Parameters.Add "PM_MSGERRO", "", 2
    
    vSql = "Producao.PCK_CAD040.PR_TRANSFERENCIA_DUPLICATA(:PM_CODREPRES,:PM_CODREPRES_NOVO,:PM_CODLOJA,:PM_NUMDUPLI,:PM_NUMORDEM,:PM_DTFATURA,:PM_ERRO,:PM_MSGERRO)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    If vErro <> "" Then
        Call vVB_Generica_001.Informar("Erro no processo de transfer�ncia das duplicatas  - " & vBanco.Parameters("PM_MSGERRO").Value & vbCrLf & " Entre em contato com o SUPORTE.")
        transfereDuplicata = True
    Else
        transfereDuplicata = False
    End If

End Function


Private Sub desativaRepres()

    Dim dtDeslig As String
        
    If frmRepresentante.txtDt_Desligamento.Text = "" Then
        dtDeslig = Format(Date, "DD/MM/YYYY")
        frmRepresentante.txtDt_Desligamento.Text = Format(Date, "DD/MM/YYYY")
    Else
        dtDeslig = frmRepresentante.txtDt_Desligamento.Text
    End If
    
    vBanco.Parameters.Remove "PM_CODREPRES"
    vBanco.Parameters.Add "PM_CODREPRES", frmRepresentante.txtCod_Repres.Text, 1
    vBanco.Parameters.Remove "PM_DTDESLIG"
    vBanco.Parameters.Add "PM_DTDESLIG", dtDeslig, 1
    vBanco.Parameters.Remove "PM_ERRO"
    vBanco.Parameters.Add "PM_ERRO", 0, 2
    vBanco.Parameters.Remove "PM_MSGERRO"
    vBanco.Parameters.Add "PM_MSGERRO", "", 2
    
    vSql = "Producao.PCK_CAD040.PR_DESATIVA_REPRES(:PM_CODREPRES,:PM_DTDESLIG,:PM_ERRO,:PM_MSGERRO)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    If vErro <> "" Then
        Call vVB_Generica_001.Informar("Erro no processo de desativa��o do representante - " & vBanco.Parameters("PM_MSGERRO").Value & vbCrLf & " Entre em contato com o SUPORTE.")
        Exit Sub
    End If
    
    frmRepresentante.txtSituacao.Text = "DESATIVADO"
    frmRepresentante.txtSituacao.ForeColor = vbRed
    Set frmRepresentante.cmdSituacao.PictureNormal = LoadPicture(App.Path & "\ativo 32x32.ico")
    vSituacao = 9
    'frmRepresentante.txtDt_Desligamento.SetFocus

End Sub




