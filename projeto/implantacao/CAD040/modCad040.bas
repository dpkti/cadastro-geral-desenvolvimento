Attribute VB_Name = "modCad040"
Public vObjOracle2 As Object                 'OBJETO DO ORACLE
Public Conteudo As String
Public TIPO As Integer
Public vSituacao As Integer
Public flFormDupl_Fechado As Boolean

'Denis - Reparando os fontes
Public vVB_Generica_001 As New clsVB_Generica_001
Public vVB_Venda_001 As New clsVB_Venda_001
Public vVB_PAAC_001 As New clsVB_PAAC_001
Public vVB_Cobranca_001 As New clsVB_Cobranca_001
Public vVB_Autolog_001 As New clsVB_Autolog_001
Public vErro As String
Public vUser_Intranet As String         'Pega o login do usu�rio da intranet
'TODO REVIEW EOLIVEIRA
Public oServicoClienteSAP As ServicoClienteSAP


#If Win32 Then
    Public Const HWND_TOPMOST& = -1
#Else
    Public Const HWND_TOPMOST& = -1
#End If 'WIN32

#If Win32 Then
    Const SWP_NOMOVE& = &H2
    Const SWP_NOSIZE& = &H1
#Else
    Const SWP_NOMOVE& = &H2
    Const SWP_NOSIZE& = &H1
#End If 'WIN32

#If Win32 Then
    Declare Function SetWindowPos& Lib "user32" (ByVal hWnd As Long, ByVal hWndInsertAfter As Long, ByVal x As Long, ByVal y As Long, ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long)
#Else
    Declare Sub SetWindowPos Lib "user" (ByVal hWnd As Integer, ByVal hWndInsertAfter As Integer, ByVal x As Integer, ByVal y As Integer, ByVal cx As Integer, ByVal cy As Integer, ByVal wFlags As Integer)
#End If 'WIN32


Public Sub Criar_Cursor(Banco As Object, pNomeCursor As String)
    Banco.Remove pNomeCursor
    Banco.Add pNomeCursor, 0, 3
    Banco(pNomeCursor).serverType = 102
    Banco(pNomeCursor).DynasetOption = &H2&
    Banco(pNomeCursor).DynasetCacheParams 256, 16, 20, 2000, 0
End Sub

Public Sub MMAAAA(ByRef KeyAscii, ByRef txtCampo)
    
    On Error GoTo TrataErro

    Dim bTam As Byte    'tamanho do campo JA digitado
    
    Dim strData As String
    Dim ParteAno As Double
    Dim bKey As Byte
    bTam = Len(txtCampo.Text)
    
    If KeyAscii = 8 And bTam > 0 Then 'backspace
        If Mid$(txtCampo.Text, bTam) = "/" Then
            txtCampo.Text = Mid$(txtCampo.Text, 1, bTam - 2)
        Else
            txtCampo.Text = Mid$(txtCampo.Text, 1, bTam - 1)
        End If
            
    ElseIf Chr$(KeyAscii) >= "0" And Chr$(KeyAscii) <= "9" Then
        If bTam = 1 Then
            strData = txtCampo.Text & Chr$(KeyAscii)
            If CInt(strData) < 1 Or CInt(strData > 12) Then
                Beep
            Else
                txtCampo.Text = txtCampo.Text & Chr$(KeyAscii) & "/"
            End If
        
        ElseIf bTam = 5 Then
            strData = Mid$(txtCampo.Text, 1, 2)     'mes
            If CInt(strData) < 1 Or CInt(strData > 12) Then
               Beep
            Else
               strData = txtCampo.Text & Chr$(KeyAscii)
               If Not IsDate(CDate(strData)) Then
                  Beep
               Else
                  txtCampo.Text = strData
               End If
            End If
            
        ElseIf bTam < 7 Then
        
            'SE ESTIVER NO PRIMEIRO ALGARISMO DO ANO, N�O PODE SER MENOR QUE 1
            If bTam = 3 Then
                ParteAno = val(Mid$(txtCampo.Text, 4, 1)) & val(Chr$(KeyAscii))
                If ParteAno < 1 Then
                    Beep
                Else
                    bKey = KeyAscii
                End If
            ElseIf bTam = 4 Then
                ParteAno = val(Mid$(txtCampo.Text, 4, 2)) & val(Chr$(KeyAscii))
                If ParteAno <= 18 Then
                    Beep
                Else
                    bKey = KeyAscii
                End If
            Else
                bKey = KeyAscii
            End If
        Else
            bKey = 0
        End If
    Else
        Beep
    End If
    
    SendKeys "{END}"
    KeyAscii = bKey
    Exit Sub
    
TrataErro:

    If Err.Number = 13 Then
        MsgBox strData, vbInformation, "Data Inv�lida"
        KeyAscii = 0
        Beep
        Err.Clear
    End If

End Sub


Sub Atualizar_Ramal_OnLine(pRamal As Long, pCodRepres As Long)
    
    Dim vDep As String
    Dim Rst As Object

    Criar_Cursor vBanco.Parameters, "pm_Cursor"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, "PRODUCAO.PCK_CAD040.Pr_Select_Lojas_Ativas(:pm_CURSOR)")
        
    If vErro <> "" Then
       Exit Sub
    End If
        
    Set Rst = vBanco.Parameters("pm_Cursor").Value
        
    Do While Not Rst.EOF
            
       vDep = "DEP" & Format(Rst!cod_loja.Value, "00") & "."
                
       SUCCESS = Ping(Rst!ip, Rst!cod_loja, ECHO)
                
       'Sem Comunica��o
       If InStr(GetStatusCode(SUCCESS), ("ip success")) = 0 Or ECHO.RoundTripTime > Rst!time_out Then
               
       'Comunica��o OK
       Else
           'Vai alterar situa��o on-line no CD (somente vai ativar on-line, desativar n�o)
           If Rst!cod_loja = 1 Then
              vDep = ""
           End If
           vBanco.Parameters.Remove "CODREPRES": vBanco.Parameters.Add "CODREPRES", val(pCodRepres), 1
           vBanco.Parameters.Remove "DEP": vBanco.Parameters.Add "DEP", vDep, 1
           vBanco.Parameters.Remove "RAMAL": vBanco.Parameters.Add "RAMAL", pRamal, 1
           vBanco.Parameters.Remove "vErro": vBanco.Parameters.Add "vErro", 0, 2
           vBanco.Parameters.Remove "vMsgErro": vBanco.Parameters.Add "vMsgErro", "", 2
                        
           vBanco.ExecuteSQL "BEGIN PRODUCAO.PCK_CAD040.PR_GRAVA_RAMAL_ONLINE(:CODREPRES,:DEP,:RAMAL,:vErro,:vMsgErro); END;"
                    
           If val(IIf(IsNull(vBanco.Parameters("vErro")), 0, vBanco.Parameters("vErro"))) <> 0 Then
              MsgBox "Erro: " & val(vBanco.Parameters("vErro").Value) & " - " & _
                     vBanco.Parameters("vMsgErro").Value & ". Contate o Suporte!", vbInformation, "CAD040 - Aten��o"
                     Exit Sub
           End If
        
       End If
              
       Rst.MoveNext
        
       Loop

End Sub

Public Function StayOnTop(Form As Form)
    Dim lFlags As Long
    Dim lStay As Long

    lFlags = SWP_NOSIZE Or SWP_NOMOVE
    lStay = SetWindowPos(Form.hWnd, HWND_TOPMOST, 0, 0, 0, 0, lFlags)
End Function

Public Function Form_Aberto(frm As String) As Boolean
    Dim f As Form
    For Each f In Forms
        If UCase(frm) = UCase(f.Name) Then
            Form_Aberto = True
            Set f = Nothing
            Exit Function
        End If
    Next
    Form_Aberto = False
    Set f = Nothing
End Function

Sub Aguardar()

    frmAguardar.Show
    frmAguardar.Refresh

End Sub
Sub DefinirTelaSobre()

    vNomeSistema = UCase(App.Title)
    vDescricaoSistema = vObjOracle.Fields(0)
    vAnalistaResponsavel = vObjOracle.Fields(1)
    vAnalistaBackup = vObjOracle.Fields(2)
    
    frmSobre.lblNomeSistema = vNomeSistema & " - " & vDescricaoSistema
    frmSobre.lblResponsavel = UCase(vAnalistaResponsavel)
    frmSobre.lblBackup = UCase(vAnalistaBackup)
    
End Sub


' Kevlin Toshinari Ossada - CiT - 28/08/2012
' Fun��o que busca cliente no SAP e retorna o codigo SAP
Public Function ObterCodSAP(ClienteCGC As String) As String
    Dim oCliente As ClienteSAP
    Dim msgErro As String
    
    If vVB_Generica_001.ValidaCIC(ClienteCGC) Then
        Set oCliente = BuscarClienteSAP(2, ClienteCGC, msgErro)
    ElseIf vVB_Generica_001.ValidaCGC(ClienteCGC) Then
        Set oCliente = BuscarClienteSAP(1, ClienteCGC, msgErro)
    End If
    
    If msgErro <> "" Then
        MsgBox msgErro
    End If
    
    ObterCodSAP = oCliente.NoClienteSAP
    
End Function

' fpallini - Ci&T - 22/08/2012 - [ISA-330]
' Fun��o B�sica para buscar cliente no SAP
Function BuscarClienteSAP(TipoPessoa As Integer, CnpjOrCpf As String, ByRef msgErro As String) As ClienteSAP
    
    Dim oRetornoPI As New RetornoPI
    msgErro = ""
    
    vUser_Intranet = vVB_Generica_001.NomeUsuarioRede
        
    If TipoPessoa = 1 Then
        'Chamada do Metodo que busca o cliente no SAP por CNPJ
        Set oRetornoPI = oServicoClienteSAP.ConsultaRepresentante(vUser_Intranet, "", "", "", CnpjOrCpf, "")
    ElseIf TipoPessoa = 2 Then
        'Chamada do Metodo que busca o cliente no SAP por CPF
        Set oRetornoPI = oServicoClienteSAP.ConsultaRepresentante(vUser_Intranet, "", "", CnpjOrCpf, "", "")
    End If
    
    ' Se teve sucesso utiliza o cliente retornado
    If oRetornoPI.Sucesso() Then
        Set BuscarClienteSAP = oRetornoPI.retorno
        BuscarClienteSAP.DtAlteracaoSAP = Date
        BuscarClienteSAP.DataReCadastro = Date
    ElseIf oRetornoPI.CodErro = "W" Then ' Se n�o encontrou o representante, trata-se de um novo no SAP.
        Set BuscarClienteSAP = New ClienteSAP
        BuscarClienteSAP.DtAlteracaoSAP = Date
        BuscarClienteSAP.DataReCadastro = Date
        BuscarClienteSAP.DtCriacaoSAP = Date
        BuscarClienteSAP.DataCadastro = Date
        BuscarClienteSAP.AnoCadastro = Year(Date)
        BuscarClienteSAP.MesCadastro = Month(Date)
    Else
        Set BuscarClienteSAP = New ClienteSAP
        msgErro = "Erro ao buscar representante no SAP" & vbNewLine & oRetornoPI.CodErro & " : " & oRetornoPI.mensagem
    End If

End Function


' fpallini - Ci&T - 22/08/2012 - [ISA-330]
' Fun��o B�sica para Salvar Cliente no SAP
Function EnviarClienteSAP(oCliente As ClienteSAP) As Boolean

    Dim retornoCadastro As RetornoPI
    
    vUser_Intranet = vVB_Generica_001.NomeUsuarioRede
    
    ' Faz o cadastro do novo cliente
    Set retornoCadastro = oServicoClienteSAP.CadastrarRepresentante(vUser_Intranet, oCliente)
    
    ' Se o retorno do cadastro obteve sucesso
    If retornoCadastro.Sucesso() Then
        EnviarClienteSAP = True
    Else
        EnviarClienteSAP = False
        
        Dim descricaoMsgBox As String
        
        ' Obter erros do SAP
        descricaoMsgBox = retornoCadastro.GetErrosSAP()
    
        ' Se n�o teve erros do SAP retorna o erro da posi��o zero.
        If descricaoMsgBox = "" Then
                
           descricaoMsgBox = retornoCadastro.mensagem
                
        End If
        
        MsgBox "Erro ao gravar o representante no SAP. Cadastro Abortado." & vbNewLine & vbNewLine & descricaoMsgBox, vbCritical, "CAD040 - Aten��o"
        
    End If

End Function


' oliveira@ 10/09/2012 CiT - [ISA 524]
'Concatena rua, endereco e complemento em uma string (separados por v�rgula)
Function ConcatenarEnderecoSAP(rua As String, Numero As String, complemento As String) As String

    Dim endereco As String
    endereco = rua & ", " & IIf(Trim(Numero) = "", "0", Numero) & ", " & complemento
    
    ConcatenarEnderecoSAP = endereco

End Function

' oliveira@ 10/09/2012 CiT - [ISA 524]
'Desconcatena um endereco separando-o por v�rgula em rua, numero e complemento
Sub DesconcatenarEnderecoSAP(endereco As String, ByRef rua As String, ByRef Numero As String, ByRef complemento As String)
 
    Dim arrayEndereco() As String
    Dim I As Integer
    arrayEndereco = Split(endereco, ",", 3)
        
    For I = 0 To UBound(arrayEndereco)
        Select Case I
            Case 0
                rua = Trim(arrayEndereco(I)) ' seta rua
            Case 1
                Numero = Trim(arrayEndereco(I)) ' seta numero
            Case 2
                complemento = Trim(arrayEndereco(I)) ' seta complemento
        End Select
    Next I

End Sub

' oliveira@ 10/09/2012 CiT - [ISA 524]
Function ValidaLetra(ByVal KeyAscii As Integer) As Integer

    On Error GoTo Trata_Erro
    
    If KeyAscii = 8 Or KeyAscii = 32 Or KeyAscii = 13 Then

        ValidaLetra = KeyAscii
        Exit Function

    End If

    If KeyAscii < 65 Or KeyAscii > 90 Then

        KeyAscii = 0
        
    End If

    ValidaLetra = KeyAscii
    
    Exit Function

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Function ValidaLetra" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description, "Aten��o", &H40000
    End If

End Function

' oliveira@ 10/09/2012 CiT - [ISA 524]
Function ValidaNumero(ByVal KeyAscii As Integer) As Integer

On Error GoTo Trata_Erro

    If KeyAscii = 8 Or KeyAscii = 13 Then

        ValidaNumero = KeyAscii
        Exit Function

    End If

    If Chr(KeyAscii) < "0" Or Chr(KeyAscii) > "9" Then

        KeyAscii = 0

    End If

    ValidaNumero = KeyAscii

Exit Function

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Function ValidaNumero" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description, "Aten��o", &H40000
    End If
End Function

' oliveira@ 10/09/2012 CiT - [ISA 524]
Function ValidaAlfaNumerico(ByVal KeyAscii As Integer) As Integer

On Error GoTo Trata_Erro

    If KeyAscii = 8 Or KeyAscii = 32 Or KeyAscii = 13 Then

        ValidaAlfaNumerico = KeyAscii
        Exit Function

    End If
    
    If InStr(1, vCharEspeciais, Chr(KeyAscii)) > 0 Then
        
        KeyAscii = 0

    End If

    ValidaAlfaNumerico = KeyAscii

Exit Function

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Function ValidaAlfaNumerico" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description, "Aten��o", &H40000
    End If
End Function

' oliveira@ 10/09/2012 CiT - [ISA 524]
Public Function ValidaMaiuscula(pKeyascii As Integer) As Integer

    ValidaMaiuscula = Asc(UCase(Chr(pKeyascii)))

    Exit Function

End Function

'ecampos - CIT - 07/01/2013
'Retorno = False se n�o cont�m somente n�meros
'Retorno = True se cont�m apenas n�meros
Public Function ValidaSomenteNumeros(Numero As String) As Boolean
        If IsNumeric(Numero) = True And InStr(Numero, ",") = False And InStr(Numero, ".") = False Then
                ValidaSomenteNumeros = True
            Else
                ValidaSomenteNumeros = False
        End If
End Function

'ecampos - CIT - 07/01/2013
Public Function ValidaBranco(pTXT As Object) As Boolean
        
    Dim vbranco As Byte
    Dim I  As Long
    
    'Atribui true na fun��o
    ValidaBranco = True
    
    'Valida se existe mais de um espa�o em branco nos campos
    For I = 1 To Len(pTXT)
        If Asc(Mid(pTXT, I, 1)) = 32 Then
            vbranco = vbranco + 1
        Else
            vbranco = 0
        End If
        If vbranco >= 2 Then
            ValidaBranco = False
            Exit Function
        End If
    Next
 
End Function

' oliveira@ 10/09/2012 CiT - [ISA 524]
Function ValidaCamposEndereco(campoEndereco As String, caracterProibido As String) As Boolean
    ValidaCamposEndereco = InStr(campoEndereco, caracterProibido) <> 0
End Function


