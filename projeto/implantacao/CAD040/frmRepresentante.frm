VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmRepresentante 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DADOS DO REPRESENTANTE"
   ClientHeight    =   8205
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10710
   Icon            =   "frmRepresentante.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   547
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   714
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtCod_Repres_SAP 
      Appearance      =   0  'Flat
      BackColor       =   &H80000004&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   9300
      Locked          =   -1  'True
      TabIndex        =   7
      Top             =   900
      Width           =   1290
   End
   Begin VB.TextBox txtCentro_Custo 
      Appearance      =   0  'Flat
      BackColor       =   &H80000004&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   5355
      Locked          =   -1  'True
      TabIndex        =   30
      Top             =   2520
      Width           =   2715
   End
   Begin VB.TextBox txtDt_Desligamento 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   9450
      MaxLength       =   10
      TabIndex        =   32
      Top             =   2520
      Width           =   1140
   End
   Begin VB.TextBox txtDt_Cadastro 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   9450
      MaxLength       =   10
      TabIndex        =   26
      Top             =   2115
      Width           =   1140
   End
   Begin VB.Frame fraIntranet 
      Caption         =   "Dados para Intranet e Informatizados"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   645
      Left            =   90
      TabIndex        =   86
      Top             =   6705
      Width           =   10500
      Begin VB.ComboBox cmbBase 
         BackColor       =   &H80000004&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   5310
         Locked          =   -1  'True
         TabIndex        =   92
         Top             =   225
         Width           =   1185
      End
      Begin VB.ComboBox cmbVisao 
         BackColor       =   &H80000004&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   7425
         Locked          =   -1  'True
         TabIndex        =   94
         Top             =   225
         Width           =   2985
      End
      Begin VB.ComboBox cmbVdr 
         BackColor       =   &H80000004&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   3105
         Locked          =   -1  'True
         TabIndex        =   90
         Top             =   225
         Width           =   1185
      End
      Begin VB.TextBox txtCxFTP 
         Appearance      =   0  'Flat
         BackColor       =   &H80000004&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1080
         Locked          =   -1  'True
         TabIndex        =   88
         Top             =   225
         Width           =   1050
      End
      Begin VB.Label lblBase 
         Appearance      =   0  'Flat
         Caption         =   "Base?"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   4770
         TabIndex        =   91
         Top             =   270
         Width           =   555
      End
      Begin VB.Label lblVDR 
         Appearance      =   0  'Flat
         Caption         =   "VDR?"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   2565
         TabIndex        =   89
         Top             =   270
         Width           =   555
      End
      Begin VB.Label lblVisao 
         Appearance      =   0  'Flat
         Caption         =   "CD's:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   6930
         TabIndex        =   93
         Top             =   270
         Width           =   465
      End
      Begin VB.Label lblCxFTP 
         Appearance      =   0  'Flat
         Caption         =   "Caixa FTP:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   135
         TabIndex        =   87
         Top             =   270
         Width           =   915
      End
   End
   Begin VB.Frame fraEnd_Corresp 
      Caption         =   "Endere�o de Correspond�ncia"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1860
      Left            =   90
      TabIndex        =   58
      Top             =   4770
      Width           =   10500
      Begin VB.TextBox txtCompl_Corresp 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   5775
         TabIndex        =   64
         Top             =   225
         Width           =   1185
      End
      Begin VB.TextBox txtNum_Corresp 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3735
         TabIndex        =   62
         Top             =   225
         Width           =   705
      End
      Begin VB.TextBox txtObservacao 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3645
         MaxLength       =   30
         TabIndex        =   85
         Top             =   1440
         Width           =   6765
      End
      Begin VB.TextBox txtEtiqueta 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1710
         MaxLength       =   1
         TabIndex        =   83
         Top             =   1440
         Width           =   465
      End
      Begin VB.TextBox txtCxPostal_Corresp 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   9585
         MaxLength       =   5
         TabIndex        =   74
         Top             =   630
         Width           =   825
      End
      Begin VB.TextBox txtFax_Corresp 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   4140
         MaxLength       =   13
         TabIndex        =   79
         Top             =   1035
         Width           =   1320
      End
      Begin VB.TextBox txtCelular_Corresp 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   6945
         MaxLength       =   13
         TabIndex        =   81
         Top             =   1035
         Width           =   1320
      End
      Begin VB.TextBox txtFone_Corresp 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1125
         MaxLength       =   9
         TabIndex        =   77
         Top             =   1035
         Width           =   1320
      End
      Begin VB.TextBox txtDDD_Corresp 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   630
         MaxLength       =   4
         TabIndex        =   76
         Top             =   1035
         Width           =   465
      End
      Begin VB.TextBox txtCEP_Corresp 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   6945
         MaxLength       =   8
         TabIndex        =   72
         Top             =   630
         Width           =   1185
      End
      Begin VB.TextBox txtEndereco_Corresp 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   630
         TabIndex        =   60
         Top             =   225
         Width           =   2385
      End
      Begin VB.TextBox txtBairro_Corresp 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   8130
         MaxLength       =   20
         TabIndex        =   66
         Top             =   225
         Width           =   2280
      End
      Begin VB.ComboBox cmbUF_Corresp 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   630
         TabIndex        =   68
         Top             =   630
         Width           =   825
      End
      Begin VB.ComboBox cmbCidade_Corresp 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   2460
         TabIndex        =   70
         Top             =   600
         Width           =   3315
      End
      Begin VB.Label Label6 
         Appearance      =   0  'Flat
         Caption         =   "Complemento:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   4560
         TabIndex        =   63
         Top             =   270
         Width           =   1230
      End
      Begin VB.Label Label5 
         Appearance      =   0  'Flat
         Caption         =   "Num.:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   3120
         TabIndex        =   61
         Top             =   270
         Width           =   510
      End
      Begin VB.Label lblObservacao 
         Appearance      =   0  'Flat
         Caption         =   "Observa��o:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   2565
         TabIndex        =   84
         Top             =   1485
         Width           =   1095
      End
      Begin VB.Label lblEtiqueta 
         Appearance      =   0  'Flat
         Caption         =   "Imprime Etiqueta?"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   135
         TabIndex        =   82
         Top             =   1485
         Width           =   1500
      End
      Begin VB.Label lblCxPostal_Corresp 
         Appearance      =   0  'Flat
         Caption         =   "Cx.Postal:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   8730
         TabIndex        =   73
         Top             =   675
         Width           =   825
      End
      Begin VB.Label lblFax_Corresp 
         Appearance      =   0  'Flat
         Caption         =   "Fax:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   3735
         TabIndex        =   78
         Top             =   1080
         Width           =   375
      End
      Begin VB.Label lblCelular_Corresp 
         Appearance      =   0  'Flat
         Caption         =   "Celular:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   6270
         TabIndex        =   80
         Top             =   1080
         Width           =   645
      End
      Begin VB.Label lblFone_Corresp 
         Appearance      =   0  'Flat
         Caption         =   "Fone:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   90
         TabIndex        =   75
         Top             =   1080
         Width           =   510
      End
      Begin VB.Label lblCEP_Corresp 
         Appearance      =   0  'Flat
         Caption         =   "CEP:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   6495
         TabIndex        =   71
         Top             =   675
         Width           =   420
      End
      Begin VB.Label lblEndereco_Corresp 
         Appearance      =   0  'Flat
         Caption         =   "End.:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   90
         TabIndex        =   59
         Top             =   270
         Width           =   510
      End
      Begin VB.Label lblBairro_Corresp 
         Appearance      =   0  'Flat
         Caption         =   "Bairro:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   7560
         TabIndex        =   65
         Top             =   270
         Width           =   600
      End
      Begin VB.Label lblUF_Corresp 
         Appearance      =   0  'Flat
         Caption         =   "UF:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   270
         TabIndex        =   67
         Top             =   675
         Width           =   330
      End
      Begin VB.Label lblCidade_Corresp 
         Appearance      =   0  'Flat
         Caption         =   "Cidade:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   1785
         TabIndex        =   69
         Top             =   675
         Width           =   600
      End
   End
   Begin VB.Frame FraEnd_Resid 
      Caption         =   "Endere�o Residencial"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1050
      Left            =   90
      TabIndex        =   43
      Top             =   3645
      Width           =   10500
      Begin VB.TextBox txtCompl_Resid 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   5895
         TabIndex        =   49
         Top             =   225
         Width           =   945
      End
      Begin VB.TextBox txtNum_Resid 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3840
         TabIndex        =   47
         Top             =   225
         Width           =   705
      End
      Begin VB.ComboBox cmbCidade_Resid 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   2340
         TabIndex        =   55
         Top             =   630
         Width           =   4515
      End
      Begin VB.ComboBox cmbUF_Resid 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   630
         TabIndex        =   53
         Top             =   630
         Width           =   825
      End
      Begin VB.TextBox txtBairro_Resid 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   8145
         MaxLength       =   20
         TabIndex        =   51
         Top             =   225
         Width           =   2265
      End
      Begin VB.TextBox txtEndereco_Resid 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   630
         MaxLength       =   40
         TabIndex        =   45
         Top             =   225
         Width           =   2505
      End
      Begin VB.TextBox txtCEP_Resid 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   8160
         MaxLength       =   8
         TabIndex        =   57
         Top             =   630
         Width           =   1140
      End
      Begin VB.Label Label4 
         Appearance      =   0  'Flat
         Caption         =   "Complemento:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   4680
         TabIndex        =   48
         Top             =   270
         Width           =   1230
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         Caption         =   "Num.:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   3240
         TabIndex        =   46
         Top             =   270
         Width           =   510
      End
      Begin VB.Label lblCidade_Resid 
         Appearance      =   0  'Flat
         Caption         =   "Cidade:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   1665
         TabIndex        =   54
         Top             =   675
         Width           =   600
      End
      Begin VB.Label lblUF_Resid 
         Appearance      =   0  'Flat
         Caption         =   "UF:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   315
         TabIndex        =   52
         Top             =   675
         Width           =   330
      End
      Begin VB.Label lblBairro_Resid 
         Appearance      =   0  'Flat
         Caption         =   "Bairro:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   7515
         TabIndex        =   50
         Top             =   240
         Width           =   600
      End
      Begin VB.Label lblEndereco_Resid 
         Appearance      =   0  'Flat
         Caption         =   "End.:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   135
         TabIndex        =   44
         Top             =   270
         Width           =   510
      End
      Begin VB.Label lblCEP_Resid 
         Appearance      =   0  'Flat
         Caption         =   "CEP:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   7695
         TabIndex        =   56
         Top             =   675
         Width           =   420
      End
   End
   Begin VB.ComboBox cmbSegmento 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   5850
      TabIndex        =   42
      Top             =   3150
      Width           =   4740
   End
   Begin VB.Frame fraConta 
      Caption         =   "Conta Corrente"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   645
      Left            =   90
      TabIndex        =   33
      Top             =   2925
      Width           =   4560
      Begin VB.TextBox txtConta 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2925
         MaxLength       =   12
         TabIndex        =   38
         Top             =   225
         Width           =   1140
      End
      Begin VB.TextBox txtDig_Conta 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   4095
         MaxLength       =   1
         TabIndex        =   39
         Top             =   225
         Width           =   375
      End
      Begin VB.TextBox txtAgencia 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   855
         MaxLength       =   4
         TabIndex        =   35
         Top             =   225
         Width           =   735
      End
      Begin VB.TextBox txtDig_Agencia 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1620
         MaxLength       =   1
         TabIndex        =   36
         Top             =   225
         Width           =   375
      End
      Begin VB.Label lblConta 
         Appearance      =   0  'Flat
         Caption         =   "N� C/C:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   2250
         TabIndex        =   37
         Top             =   270
         Width           =   645
      End
      Begin VB.Label lblAgencia 
         Appearance      =   0  'Flat
         Caption         =   "Ag�ncia:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   90
         TabIndex        =   34
         Top             =   270
         Width           =   735
      End
   End
   Begin VB.ComboBox cmbTipo_Repres 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      ItemData        =   "frmRepresentante.frx":0CCA
      Left            =   5040
      List            =   "frmRepresentante.frx":0CCC
      TabIndex        =   24
      Top             =   2115
      Width           =   2760
   End
   Begin VB.ComboBox cmbFilial 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   630
      TabIndex        =   28
      Top             =   2520
      Width           =   3750
   End
   Begin VB.ComboBox cmbGerente 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   855
      TabIndex        =   22
      Top             =   2115
      Width           =   2715
   End
   Begin VB.ComboBox cmbDivisao 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   8550
      TabIndex        =   20
      Top             =   1710
      Width           =   2040
   End
   Begin VB.TextBox txtRamal 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   6435
      MaxLength       =   9
      TabIndex        =   18
      Top             =   1710
      Width           =   735
   End
   Begin VB.TextBox txtFax 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3630
      MaxLength       =   13
      TabIndex        =   16
      Top             =   1710
      Width           =   1560
   End
   Begin VB.TextBox txtFone 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1395
      MaxLength       =   9
      TabIndex        =   14
      Top             =   1710
      Width           =   1455
   End
   Begin VB.TextBox txtDDD 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   855
      MaxLength       =   4
      TabIndex        =   13
      Top             =   1710
      Width           =   510
   End
   Begin VB.TextBox txtNome_Repres 
      Appearance      =   0  'Flat
      BackColor       =   &H80000018&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3630
      MaxLength       =   40
      TabIndex        =   11
      ToolTipText     =   "D� duplo click para pesquisar pelo NOME"
      Top             =   1305
      Width           =   6960
   End
   Begin VB.TextBox txtCIC 
      Appearance      =   0  'Flat
      BackColor       =   &H80000018&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   855
      MaxLength       =   14
      TabIndex        =   9
      ToolTipText     =   "Digite o CIC para fazer a pesquisar"
      Top             =   1305
      Width           =   1995
   End
   Begin VB.TextBox txtSituacao 
      Appearance      =   0  'Flat
      BackColor       =   &H80000004&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   6900
      Locked          =   -1  'True
      TabIndex        =   5
      Top             =   900
      Width           =   1770
   End
   Begin VB.TextBox txtPseudonimo 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3630
      MaxLength       =   20
      TabIndex        =   3
      Top             =   900
      Width           =   2310
   End
   Begin VB.TextBox txtCod_Repres 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   855
      MaxLength       =   4
      TabIndex        =   1
      Top             =   900
      Width           =   1140
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   101
      Top             =   7875
      Width           =   10710
      _ExtentX        =   18891
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   18838
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   100
      Top             =   810
      Width           =   10590
      _ExtentX        =   18680
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdCancelar 
      Height          =   690
      Left            =   2265
      TabIndex        =   98
      TabStop         =   0   'False
      ToolTipText     =   "Cancelar"
      Top             =   60
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmRepresentante.frx":0CCE
      PICN            =   "frmRepresentante.frx":0CEA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdGravar 
      Height          =   690
      Left            =   780
      TabIndex        =   97
      TabStop         =   0   'False
      ToolTipText     =   "Gravar"
      Top             =   60
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmRepresentante.frx":15C4
      PICN            =   "frmRepresentante.frx":15E0
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdSituacao 
      Height          =   690
      Left            =   45
      TabIndex        =   96
      TabStop         =   0   'False
      ToolTipText     =   "Ativar/Desativar"
      Top             =   60
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmRepresentante.frx":22BA
      PICN            =   "frmRepresentante.frx":22D6
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdDocumentos 
      Height          =   375
      Left            =   4455
      TabIndex        =   95
      TabStop         =   0   'False
      Top             =   7425
      Width           =   2580
      _ExtentX        =   4551
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "Documentos Cadastrados"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmRepresentante.frx":25F0
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   9945
      TabIndex        =   99
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmRepresentante.frx":260C
      PICN            =   "frmRepresentante.frx":2628
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd1 
      Height          =   690
      Left            =   1530
      TabIndex        =   102
      TabStop         =   0   'False
      ToolTipText     =   "Duplicatas em aberto"
      Top             =   60
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmRepresentante.frx":3302
      PICN            =   "frmRepresentante.frx":331E
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      Caption         =   "SAP"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   8880
      TabIndex        =   6
      Top             =   945
      Width           =   420
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      Caption         =   "C.Custo:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   4590
      TabIndex        =   29
      Top             =   2610
      Width           =   780
   End
   Begin VB.Label lblSegmento2 
      Appearance      =   0  'Flat
      Caption         =   "Segmento:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   4860
      TabIndex        =   41
      Top             =   3240
      Width           =   960
   End
   Begin VB.Label lblSegmento1 
      Appearance      =   0  'Flat
      Caption         =   "Tipo de"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   4860
      TabIndex        =   40
      Top             =   3015
      Width           =   780
   End
   Begin VB.Label lblDt_Desligamento 
      Appearance      =   0  'Flat
      Caption         =   "Dt. Desligam.:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   8235
      TabIndex        =   31
      Top             =   2565
      Width           =   1185
   End
   Begin VB.Label lblDt_Cadastro 
      Appearance      =   0  'Flat
      Caption         =   "Dt. Cadastro:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   8280
      TabIndex        =   25
      Top             =   2160
      Width           =   1140
   End
   Begin VB.Label lblTipo_Repres 
      Appearance      =   0  'Flat
      Caption         =   "Tp. Repres.:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   3960
      TabIndex        =   23
      Top             =   2160
      Width           =   1050
   End
   Begin VB.Label lblFilial 
      Appearance      =   0  'Flat
      Caption         =   "Filial:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   180
      TabIndex        =   27
      Top             =   2565
      Width           =   420
   End
   Begin VB.Label lblGerente 
      Appearance      =   0  'Flat
      Caption         =   "Gerente:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   90
      TabIndex        =   21
      Top             =   2160
      Width           =   780
   End
   Begin VB.Label lblDivisao 
      Appearance      =   0  'Flat
      Caption         =   "Divis�o:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   7875
      TabIndex        =   19
      Top             =   1755
      Width           =   645
   End
   Begin VB.Label lblRamal 
      Appearance      =   0  'Flat
      Caption         =   "Ramal:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   5850
      TabIndex        =   17
      Top             =   1755
      Width           =   555
   End
   Begin VB.Label lblFax 
      Appearance      =   0  'Flat
      Caption         =   "Fax:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   3180
      TabIndex        =   15
      Top             =   1755
      Width           =   375
   End
   Begin VB.Label lblFone 
      Appearance      =   0  'Flat
      Caption         =   "Fone:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   315
      TabIndex        =   12
      Top             =   1755
      Width           =   510
   End
   Begin VB.Label lblNome_Repres 
      Appearance      =   0  'Flat
      Caption         =   "Nome:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   3000
      TabIndex        =   10
      Top             =   1350
      Width           =   600
   End
   Begin VB.Label lblCIC 
      Appearance      =   0  'Flat
      Caption         =   "N� CIC:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   180
      TabIndex        =   8
      Top             =   1350
      Width           =   645
   End
   Begin VB.Label lblSituacao 
      Appearance      =   0  'Flat
      Caption         =   "Situa��o:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   6090
      TabIndex        =   4
      Top             =   945
      Width           =   780
   End
   Begin VB.Label lblPseudonimo 
      Appearance      =   0  'Flat
      Caption         =   "Pseudonimo:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   2505
      TabIndex        =   2
      Top             =   945
      Width           =   1095
   End
   Begin VB.Label lblCod_Repres 
      Appearance      =   0  'Flat
      Caption         =   "C�digo:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   180
      TabIndex        =   0
      Top             =   945
      Width           =   645
   End
End
Attribute VB_Name = "frmRepresentante"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmbFilial_Click()
    '-- Buscar atrav�s do c�digo da filial o centro de custo
        vBanco.Parameters.Remove "PM_CODFILIAL"
        vBanco.Parameters.Add "PM_CODFILIAL", Mid(cmbFilial.Text, 1, 4), 1
        
        Criar_Cursor vBanco.Parameters, "vCursor"

        vSql = "Producao.PCK_CAD040.PR_BUSCA_CCUSTO_FILIAL(:PM_CODFILIAL, :vCursor)"

        If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
            Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_CCUSTO_FILIAL, entre em contato com o SUPORTE!")
            Exit Sub
        Else
            Set vObjOracle = vBanco.Parameters("VCURSOR").Value
        End If
    
        txtCentro_Custo.Text = vObjOracle!COD_CENTRO_CUSTO & " - " & vObjOracle!DESC_CENTRO_CUSTO
End Sub

Private Sub cmbGerente_Click()
    
    If cmbGerente.Text <> "" Then
        
        Criar_Cursor vBanco.Parameters, "vCursor"
        
        Set vObjOracle = vVB_Venda_001.TabelaGerente(vBanco, Mid(cmbGerente.Text, 1, 4), , 0)
        
        CIC_GER = vObjOracle!CIC
        
        vBanco.Parameters.Remove "PM_CIC"
        vBanco.Parameters.Add "PM_CIC", CIC_GER, 1
        
        Criar_Cursor vBanco.Parameters, "vCursor"

        vSql = "Producao.PCK_CAD040.PR_BUSCA_FILIAL_GERENTE(:PM_CIC, :vCursor)"

        If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
            Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_FILIAL_GERENTE, entre em contato com o SUPORTE!")
            Exit Sub
        Else
            Set vObjOracle = vBanco.Parameters("VCURSOR").Value
        End If
        
        Call vVB_Generica_001.PreencheComboList(vObjOracle, cmbFilial, "COD_FILIAL", "NOME_FILIAL")
            
    End If

End Sub

Private Sub cmbTipo_Repres_Change()
    cmbTipo_Repres_LostFocus
End Sub

Private Sub cmbTipo_Repres_Click()
    cmbTipo_Repres_LostFocus
End Sub

Private Sub cmbTipo_Repres_LostFocus()
    Dim objTipo As Object
    Dim vTipoAntigo As String
    Dim vRegs As Long
    
    If Me.txtCod_Repres <> "" Then
    
        Set objTipo = vBanco.CreateDynaset("SELECT TIPO FROM REPRESENTANTE WHERE COD_REPRES = " & txtCod_Repres, &O0)
        vTipoAntigo = "" & objTipo!TIPO
        
        If objTipo!TIPO = "R" Then
            vTipoAntigo = "R - REPRESENTANTE"
        ElseIf objTipo!TIPO = "A" Then
            vTipoAntigo = "A - TELEATENDIMENTO"
        ElseIf objTipo!TIPO = "V" Then
            vTipoAntigo = "V - TELEVENDAS"
        ElseIf objTipo!TIPO = "M" Then
            vTipoAntigo = "M - TELEMISTO"
        ElseIf objTipo!TIPO = "G" Then
            vTipoAntigo = "G - GERENTE"
        ElseIf objTipo!TIPO = "P" Then
            vTipoAntigo = "P - PROMOTOR"
        ElseIf objTipo!TIPO = "C" Then
            vTipoAntigo = "C - CURINGA"
        ElseIf objTipo!TIPO = "F" Then
            vTipoAntigo = "F - FUNCIONARIO PROMOTOR"
        End If
        
        If UCase(cmbTipo_Repres) = "F - FUNCIONARIO PROMOTOR" And (vTipoAntigo <> "F - FUNCIONARIO PROMOTOR" And vTipoAntigo <> "") Then
            Set objTipo = vBanco.CreateDynaset("SELECT COUNT(*) R_CLIE_REPRES FROM R_CLIE_REPRES A, CLIENTE B WHERE A.COD_CLIENTE = B.COD_CLIENTE AND B.SITUACAO = 0 AND COD_REPRES = " & txtCod_Repres, &O0)
            vRegs = 0 & objTipo!r_clie_repres
            If vRegs > 0 Then
                MsgBox "Existem clientes associados a este representante.", vbInformation, "Aten��o"
                cmbTipo_Repres = vTipoAntigo
                Exit Sub
            End If
            Set objTipo = vBanco.CreateDynaset("SELECT COUNT(*) R_REPVEN FROM R_REPVEN WHERE COD_REPRES = " & txtCod_Repres, &O0)
            vRegs = 0 & objTipo!r_repven
            
            If vRegs > 0 Then
                MsgBox "Existem Vendedores Internos associadas a este representante.", vbInformation, "Aten��o"
                cmbTipo_Repres = vTipoAntigo
                Exit Sub
            End If
            Exit Sub
        End If
    End If
    
    If UCase(cmbTipo_Repres) = "C - CURINGA" Then
        Dim vObjLoja As Object
        
        Criar_Cursor vBanco.Parameters, "vCursor"
    
        vBanco.Parameters.Remove "PM_COD_LOJA"
        vBanco.Parameters.Add "PM_COD_LOJA", 1, 1
    
        vErro = vVB_Generica_001.ExecutaPl(vBanco, "PRODUCAO.PCK_CAD040.PR_SELECT_LOJA(:vCursor, :PM_COD_LOJA)")
                
        If vErro <> "" Then
            MsgBox vErro
            Exit Sub
        Else
            Set vObjLoja = vBanco.Parameters("vCursor").Value
        End If
    
        txtPseudonimo = "ADMINISTRATIVO"
        txtNome_Repres = "ADMINISTRATIVO"
       
        txtEndereco_Resid = vObjLoja!endereco
        txtBairro_Resid = vObjLoja!bairro
        cmbCidade_Resid = vObjLoja!cod_cidade & " - " & vObjLoja!nome_cidade
        cmbUF_Resid = vObjLoja!Cod_uf
        txtCEP_Resid = vObjLoja!CEP
        txtDDD = vObjLoja!DDD
        txtFone = vObjLoja!Fone
        txtFax = vObjLoja!Fax
        
    End If

    txtPseudonimo.Enabled = Not UCase(cmbTipo_Repres) = "C - CURINGA"
    txtNome_Repres.Enabled = Not UCase(cmbTipo_Repres) = "C - CURINGA"
    txtEndereco_Resid.Enabled = Not UCase(cmbTipo_Repres) = "C - CURINGA"
    txtBairro_Resid.Enabled = Not UCase(cmbTipo_Repres) = "C - CURINGA"
    cmbCidade_Resid.Enabled = Not UCase(cmbTipo_Repres) = "C - CURINGA"
    cmbUF_Resid.Enabled = Not UCase(cmbTipo_Repres) = "C - CURINGA"
    txtCEP_Resid.Enabled = Not UCase(cmbTipo_Repres) = "C - CURINGA"
    txtDDD.Enabled = Not UCase(cmbTipo_Repres) = "C - CURINGA"
    txtFone.Enabled = Not UCase(cmbTipo_Repres) = "C - CURINGA"
    txtFax.Enabled = Not UCase(cmbTipo_Repres) = "C - CURINGA"

End Sub

Private Sub cmbUF_Corresp_Click()
    
    Criar_Cursor vBanco.Parameters, "vCursor"
    
    Set vObjOracle = vVB_Generica_001.TabelaCidade(vBanco, , , , cmbUF_Corresp.Text)
    Call vVB_Generica_001.PreencheComboList(vObjOracle, cmbCidade_Corresp, "C�D. CIDADE", "CIDADE")
   

End Sub

Private Sub cmbUF_Resid_Click()

    Criar_Cursor vBanco.Parameters, "vCursor"
    
    Set vObjOracle = vVB_Generica_001.TabelaCidade(vBanco, , , , cmbUF_Resid.Text)
    Call vVB_Generica_001.PreencheComboList(vObjOracle, cmbCidade_Resid, "C�D. CIDADE", "CIDADE")

End Sub

Private Sub cmd1_Click()
    If txtCod_Repres.Text <> "" Then
        Set vObjOracle = vVB_Cobranca_001.TabelaDuplicata(vBanco, , , , , , , , 0, , txtCod_Repres.Text)
        
        If vObjOracle.RecordCount > 0 Then
            frmDuplAberto.Show 1
        Else
            MsgBox ("Nenhuma duplicata em aberto para este representante")
            Exit Sub
        End If

    End If
End Sub

Private Sub cmdCancelar_Click()
    txtCod_Repres.Text = ""
    txtPseudonimo.Text = ""
    txtSituacao.Text = ""
    txtSituacao.ForeColor = vbBlack
    txtCIC.Text = ""
    txtNome_Repres.Text = ""
    txtDDD.Text = ""
    txtFone.Text = ""
    txtFax.Text = ""
    txtRamal.Text = ""
    cmbDivisao.Text = ""
    cmbGerente.Text = ""
    cmbFilial.Text = ""
    txtCentro_Custo.Text = ""
    cmbTipo_Repres.Text = ""
    txtDt_Cadastro.Text = ""
    txtDt_Desligamento.Text = ""
    txtAgencia.Text = ""
    txtDig_Agencia.Text = ""
    txtConta.Text = ""
    txtDig_Conta.Text = ""
    cmbSegmento.Text = ""
    txtEndereco_Resid.Text = ""
    txtBairro_Resid.Text = ""
    cmbUF_Resid.Text = ""
    cmbCidade_Resid.Text = ""
    txtCEP_Resid.Text = ""
    txtEndereco_Corresp.Text = ""
    txtBairro_Corresp.Text = ""
    cmbUF_Corresp.Text = ""
    cmbCidade_Corresp.Text = ""
    txtCEP_Corresp.Text = ""
    txtCxPostal_Corresp.Text = ""
    txtDDD_Corresp.Text = ""
    txtFone_Corresp.Text = ""
    txtFax_Corresp.Text = ""
    txtCelular_Corresp.Text = ""
    txtEtiqueta.Text = ""
    txtObservacao.Text = ""
    txtCxFTP.Text = ""
    cmbVdr.Text = ""
    cmbBase.Text = ""
    cmbVisao.Text = ""
    
    cmbDivisao.Text = "D - DPK"
    
    ' fpallini - Ci&T - 21/09/12 - Limpando Campos Adicionais da Integra��o SAP.
    txtCod_Repres_SAP = ""
    txtNum_Corresp = ""
    txtNum_Resid = ""
    txtCompl_Corresp = ""
    txtCompl_Resid = ""

    txtCod_Repres.SetFocus
End Sub

Private Sub cmdDocumentos_Click()
    
    If txtCod_Repres.Text <> "" Then
        frmDocumentos.txtCod_Repres.Text = txtCod_Repres.Text
        frmDocumentos.txtPseudonimo.Text = txtPseudonimo.Text
        frmDocumentos.txtCPF.Text = txtCIC.Text
    
        frmDocumentos.Show vbModal
    End If
    
End Sub

Private Sub cmdGravar_Click()
    Dim vDivisao As String
    Dim vTipo As String
    Dim vFilial As Long
    Dim vSexo As String
    Dim vEstcivil As String
    Dim vPseudo As String
    Dim vNome_Rep As String
    Dim vCICGER As Double
    
    txtNum_Resid.SetFocus
    
    If txtCod_Repres.Text = "" Then
        txtCod_Repres.SetFocus
        Exit Sub
    ElseIf txtPseudonimo.Text = "" Then
        Call vVB_Generica_001.Informar("Digite o pseudonimo do representante!")
        txtPseudonimo.SetFocus
        Exit Sub
    ElseIf txtCIC.Text = "" Then
        Call vVB_Generica_001.Informar("Digite o CIC do representante!")
        txtCIC.SetFocus
        Exit Sub
    ElseIf txtNome_Repres.Text = "" Then
        Call vVB_Generica_001.Informar("Digite o nome do representante!")
        txtNome_Repres.SetFocus
        Exit Sub
    ElseIf cmbDivisao.Text = "" Then
        Call vVB_Generica_001.Informar("Escolha a divis�o do representante!")
        cmbDivisao.SetFocus
        Exit Sub
    ElseIf cmbGerente.Text = "" Then
        Call vVB_Generica_001.Informar("Escolha o gerente do representante!")
        cmbGerente.SetFocus
        Exit Sub
    ElseIf txtEndereco_Resid.Text = "" Then
        Call vVB_Generica_001.Informar("Digite o endere�o do representante!")
        txtEndereco_Resid.SetFocus
        Exit Sub
    'ElseIf txtNum_Resid.Text = "" Then
    '    Call vVB_Generica_001.Informar("Digite o n�mero do endere�o do representante!")
    '    txtNum_Resid.SetFocus
    '    Exit Sub
    ElseIf txtBairro_Resid.Text = "" Then
        Call vVB_Generica_001.Informar("Digite o bairro do representante!")
        txtBairro_Resid.SetFocus
        Exit Sub
    ElseIf cmbUF_Resid.Text = "" Then
        Call vVB_Generica_001.Informar("Escolha a UF do representante!")
        cmbUF_Resid.SetFocus
        Exit Sub
    ElseIf cmbCidade_Resid.Text = "" Then
        Call vVB_Generica_001.Informar("Escolha a cidade do representante!")
        cmbCidade_Resid.SetFocus
        Exit Sub
    ElseIf txtCEP_Resid.Text = "" Then
        Call vVB_Generica_001.Informar("Digite o CEP do representante!")
        txtCEP_Resid.SetFocus
        Exit Sub
    'oliveira@ - CiT - [11/09/2012] - [ISA 471] - validando para nao passar campos vazios e invalidos no XML
    ElseIf Trim(txtDDD.Text) = "" Then
        Call vVB_Generica_001.Informar("� necess�rio preencher o DDD.")
        txtDDD.SetFocus
        Exit Sub
    'oliveira@ - CiT - [11/09/2012] - [ISA 471] - validando para nao passar campos vazios e invalidos no XML
    ElseIf Trim(txtFone.Text) = "" Then
        Call vVB_Generica_001.Informar("� necess�rio preencher o Telefone.")
        txtFone.SetFocus
        Exit Sub
        
        
        
    'Eveline Dupas - CIT - 07/01/2013
    'Valida��o de quantidade de espa�os em branco nos campos
    ElseIf Not ValidaBranco(txtPseudonimo) Then
        Call vVB_Generica_001.Informar("Existe mais de um espa�o em branco no campo Pseud�nimo")
        txtPseudonimo.SetFocus
        Exit Sub
    ElseIf Not ValidaBranco(txtNome_Repres) Then
        Call vVB_Generica_001.Informar("Existe mais de um espa�o em branco no campo nome do representante!")
        txtNome_Repres.SetFocus
        Exit Sub
    ElseIf Not ValidaBranco(txtEndereco_Resid) Then
        Call vVB_Generica_001.Informar("Existe mais de um espa�o em branco no campo endere�o do representante!")
        txtEndereco_Resid.SetFocus
        Exit Sub
    ElseIf Not ValidaBranco(txtNum_Resid) Then
        Call vVB_Generica_001.Informar("Existe mais de um espa�o em branco no campo n�mero do endere�o do representante!")
        txtNum_Resid.SetFocus
        Exit Sub
    ElseIf Not ValidaBranco(txtBairro_Resid) Then
        Call vVB_Generica_001.Informar("Existe mais de um espa�o em branco no campo bairro do representante!")
        txtBairro_Resid.SetFocus
        Exit Sub
    ElseIf Not ValidaBranco(txtCEP_Resid) Then
        Call vVB_Generica_001.Informar("Existe mais de um espa�o em branco no campo CEP do representante!")
        txtCEP_Resid.SetFocus
        Exit Sub
    ElseIf Not ValidaBranco(txtDDD) Then
        Call vVB_Generica_001.Informar("Existe mais de um espa�o em branco no campo DDD.")
        txtDDD.SetFocus
        Exit Sub
    ElseIf Not ValidaBranco(txtFone) Then
        Call vVB_Generica_001.Informar("Existe mais de um espa�o em branco no campo Telefone.")
        txtFone.SetFocus
        Exit Sub
    ElseIf Not ValidaBranco(txtFax) Then
        Call vVB_Generica_001.Informar("Existe mais de um espa�o em branco no campo Fax.")
        txtFax.SetFocus
        Exit Sub
    ElseIf Not ValidaBranco(txtRamal) Then
        Call vVB_Generica_001.Informar("Existe mais de um espa�o em branco no campo Ramal.")
        txtRamal.SetFocus
        Exit Sub
    ElseIf Not ValidaBranco(txtAgencia) Then
        Call vVB_Generica_001.Informar("Existe mais de um espa�o em branco no campo ag�ncia.")
        txtAgencia.SetFocus
        Exit Sub
    ElseIf Not ValidaBranco(txtConta) Then
        Call vVB_Generica_001.Informar("Existe mais de um espa�o em branco no campo conta.")
        txtConta.SetFocus
        Exit Sub
    ElseIf Not ValidaBranco(txtCompl_Resid) Then
        Call vVB_Generica_001.Informar("Existe mais de um espa�o em branco no campo complemento.")
        txtCompl_Resid.SetFocus
        Exit Sub
    ElseIf Not ValidaBranco(txtEndereco_Corresp) Then
        Call vVB_Generica_001.Informar("Existe mais de um espa�o em branco no campo endere�o da correspond�ncia do representante!")
        txtEndereco_Corresp.SetFocus
        Exit Sub
    ElseIf Not ValidaBranco(txtNum_Corresp) Then
        Call vVB_Generica_001.Informar("Existe mais de um espa�o em branco no campo n�mero do endere�o da correspond�ncia do representante!")
        txtNum_Corresp.SetFocus
        Exit Sub
    ElseIf Not ValidaBranco(txtCompl_Corresp) Then
        Call vVB_Generica_001.Informar("Existe mais de um espa�o em branco no campo complemento.")
        txtCompl_Corresp.SetFocus
        Exit Sub
    ElseIf Not ValidaBranco(txtBairro_Corresp) Then
        Call vVB_Generica_001.Informar("Existe mais de um espa�o em branco no campo bairro da correspond�ncia do representante!")
        txtBairro_Corresp.SetFocus
        Exit Sub
    ElseIf Not ValidaBranco(txtCEP_Corresp) Then
        Call vVB_Generica_001.Informar("Existe mais de um espa�o em branco no campo CEP da correspond�ncia do representante!")
        txtCEP_Corresp.SetFocus
        Exit Sub
    ElseIf Not ValidaBranco(txtDDD_Corresp) Then
        Call vVB_Generica_001.Informar("Existe mais de um espa�o em branco no campo DDD da correspond�ncia.")
        txtDDD_Corresp.SetFocus
        Exit Sub
    ElseIf Not ValidaBranco(txtFone_Corresp) Then
        Call vVB_Generica_001.Informar("Existe mais de um espa�o em branco no campo Telefone da correspond�ncia.")
        txtFone_Corresp.SetFocus
        Exit Sub
    ElseIf Not ValidaBranco(txtFax_Corresp) Then
        Call vVB_Generica_001.Informar("Existe mais de um espa�o em branco no campo Fax da correspond�ncia.")
        txtFax_Corresp.SetFocus
        Exit Sub
    ElseIf Not ValidaBranco(txtCEP_Corresp) Then
        Call vVB_Generica_001.Informar("Existe mais de um espa�o em branco no campo CEP da correspond�ncia.")
        txtCEP_Corresp.SetFocus
        Exit Sub
    ElseIf Not ValidaBranco(txtCxPostal_Corresp) Then
        Call vVB_Generica_001.Informar("Existe mais de um espa�o em branco no campo Caixa Postal da correspond�ncia.")
        txtCxPostal_Corresp.SetFocus
        Exit Sub
    ElseIf Not ValidaBranco(txtCelular_Corresp) Then
        Call vVB_Generica_001.Informar("Existe mais de um espa�o em branco no campo Celular da correspond�ncia.")
        txtCelular_Corresp.SetFocus
        Exit Sub
        
        
    'Eveline Dupas - CIT - 07/01/2013
    'Valida��o de campos que aceitam somente n�meros
    ElseIf Not ValidaSomenteNumeros(txtDDD.Text) Then
        Call vVB_Generica_001.Informar("O campo DDD est� inv�lido.")
        txtDDD.SetFocus
        Exit Sub
    ElseIf Not ValidaSomenteNumeros(txtFone.Text) Then
        Call vVB_Generica_001.Informar("O campo telefone est� inv�lido.")
        txtFone.SetFocus
        Exit Sub
    ElseIf txtFax.Text <> "" And Not ValidaSomenteNumeros(txtFax.Text) Then
        Call vVB_Generica_001.Informar("O campo Fax est� inv�lido.")
        txtFax.SetFocus
        Exit Sub
    ElseIf txtRamal.Text <> "" And Not ValidaSomenteNumeros(txtRamal.Text) Then
        Call vVB_Generica_001.Informar("O campo Ramal est� inv�lido.")
        txtRamal.SetFocus
        Exit Sub
    
    '********END RESIDENCIA
    'Eduardo Faria de Oliveira - CIT - 10/09/2012
    ElseIf ValidaCamposEndereco(txtEndereco_Resid.Text, ",") Then
        Call vVB_Generica_001.Informar("N�o � permitida a digita��o de v�rgula (,) nos campos de endere�o.")
        txtEndereco_Resid.SetFocus
        Exit Sub
    'Eveline Dupas - CIT - 07/01/2013
    ElseIf Not ValidaSomenteNumeros(txtNum_Resid.Text) And Trim(txtNum_Resid.Text) <> "" Then
        Call vVB_Generica_001.Informar("O campo n�mero do endere�o residencial est� inv�lido.")
        txtNum_Resid.SetFocus
        Exit Sub
    'Eduardo Faria de Oliveira - CIT - 10/09/2012
    ElseIf ValidaCamposEndereco(txtCompl_Resid.Text, ",") Then
        Call vVB_Generica_001.Informar("N�o � permitida a digita��o de v�rgula (,) nos campos de endere�o.")
        txtCompl_Resid.SetFocus
        Exit Sub
    'oliveira@ - CiT - [11/09/2012] - [ISA 471] - validando para nao passar campos vazios e invalidos no XML
    ElseIf Len(ConcatenarEnderecoSAP(txtEndereco_Resid.Text, txtNum_Resid.Text, txtCompl_Resid.Text)) > 40 Then
        Call vVB_Generica_001.Informar("O endere�o de resid�ncia completo n�o deve ultrapassar " & IIf(Trim(txtNum_Resid.Text) = "", "35 caract�res, pois o n�mero est� vazio.", "36 caract�res."))
        Exit Sub
    ElseIf Not ValidaSomenteNumeros(txtCEP_Resid.Text) Then
        Call vVB_Generica_001.Informar("O campo CEP residencial est� inv�lido!")
        txtCEP_Resid.SetFocus
        Exit Sub
        
    '************END CORRESPONDENCIA
    'Eduardo Faria de Oliveira - CIT - 10/09/2012
    ElseIf ValidaCamposEndereco(txtEndereco_Corresp.Text, ",") Then
        Call vVB_Generica_001.Informar("N�o � permitida a digita��o de v�rgula (,) nos campos de endere�o.")
        txtEndereco_Corresp.SetFocus
        Exit Sub
    'Eveline Dupas - CIT - 07/01/2013
    ElseIf Trim(txtNum_Corresp.Text) <> "" And Not ValidaSomenteNumeros(txtNum_Corresp.Text) Then
        Call vVB_Generica_001.Informar("O campo n�mero do endere�o de correspond�ncia est� inv�lido.")
        txtNum_Corresp.SetFocus
        Exit Sub
    'Eduardo Faria de Oliveira - CIT - 10/09/2012
    ElseIf ValidaCamposEndereco(txtCompl_Corresp.Text, ",") Then
        Call vVB_Generica_001.Informar("N�o � permitida a digita��o de v�rgula (,) nos campos de endere�o.")
        txtCompl_Corresp.SetFocus
        Exit Sub
    ElseIf Not ValidaSomenteNumeros(txtCEP_Corresp.Text) And Trim(txtCEP_Corresp.Text) <> "" Then
        Call vVB_Generica_001.Informar("CEP de correspond�ncia inv�lido!")
        txtCEP_Corresp.SetFocus
        Exit Sub
    ElseIf Not ValidaSomenteNumeros(txtCxPostal_Corresp.Text) And Trim(txtCxPostal_Corresp.Text) <> "" Then
        Call vVB_Generica_001.Informar("Caixa Postal de correspond�ncia est� inv�lida!")
        txtCxPostal_Corresp.SetFocus
        Exit Sub
    ElseIf Not ValidaSomenteNumeros(txtDDD_Corresp.Text) And Trim(txtDDD_Corresp.Text) <> "" Then
        Call vVB_Generica_001.Informar("O DDD de correspond�ncia est� inv�lida!")
        txtDDD_Corresp.SetFocus
        Exit Sub
    ElseIf Not ValidaSomenteNumeros(txtFone_Corresp.Text) And Trim(txtFone_Corresp.Text) <> "" Then
        Call vVB_Generica_001.Informar("O telefone de correspond�ncia est� inv�lido!")
        txtFone_Corresp.SetFocus
        Exit Sub
    ElseIf Not ValidaSomenteNumeros(txtFax_Corresp.Text) And Trim(txtFax_Corresp.Text) <> "" Then
        Call vVB_Generica_001.Informar("O fax de correspond�ncia est� inv�lido!")
        txtFax_Corresp.SetFocus
        Exit Sub
    ElseIf Not ValidaSomenteNumeros(txtCelular_Corresp.Text) And Trim(txtCelular_Corresp.Text) <> "" Then
        Call vVB_Generica_001.Informar("O celular de correspond�ncia est� inv�lido!")
        txtCelular_Corresp.SetFocus
        Exit Sub
    ElseIf Len(ConcatenarEnderecoSAP(txtEndereco_Corresp.Text, txtNum_Corresp.Text, txtCompl_Corresp.Text)) > 40 Then
        Call vVB_Generica_001.Informar("O endere�o de correspond�ncia completo n�o deve ultrapassar " & IIf(Trim(txtNum_Corresp.Text) = "", "35 caract�res, pois o n�mero est� vazio.", "36 caract�res."))
        Exit Sub
    
    
    
    'Eveline Dupas - CIT - 07/01/2013
    'valida��o de informa��es banc�rias
    ElseIf Trim(txtAgencia.Text) <> "" And Not ValidaSomenteNumeros(txtAgencia.Text) Then
        Call vVB_Generica_001.Informar("O campo ag�ncia est� inv�lido!")
        txtAgencia.SetFocus
        Exit Sub
    ElseIf Trim(txtConta.Text) <> "" And Not ValidaSomenteNumeros(txtConta.Text) Then
        Call vVB_Generica_001.Informar("O campo conta banc�ria est� inv�lido!")
        txtConta.SetFocus
        Exit Sub
    ElseIf Trim(txtDig_Conta.Text) <> "" And Not ValidaSomenteNumeros(txtDig_Conta.Text) Then
        Call vVB_Generica_001.Informar("O campo d�gito da conta banc�ria est� inv�lido!")
        txtDig_Conta.SetFocus
        Exit Sub
    
    ElseIf txtEtiqueta.Text <> "S" And txtEtiqueta.Text <> "N" Then
        Call vVB_Generica_001.Informar("O campo Imprime Etiqueta deve conter o valor 'S' ou 'N'")
        txtEtiqueta.SetFocus
        Exit Sub
    ElseIf Len(txtDt_Cadastro.Text) <> 8 And txtDt_Cadastro.Text <> "" Then
        Call vVB_Generica_001.Informar("O Data de Cadastro est� inv�lido.")
        txtDt_Cadastro.SetFocus
        Exit Sub
    ElseIf Len(txtDt_Desligamento.Text) <> 8 And txtDt_Desligamento.Text <> "" Then
        Call vVB_Generica_001.Informar("O Data de Desligamento est� inv�lido.")
        txtDt_Desligamento.SetFocus
        Exit Sub
        
    End If
    
    vDivisao = Mid(cmbDivisao.Text, 1, 1)
    
    If cmbTipo_Repres.Text = "" Then
        Call vVB_Generica_001.Informar("� necess�rio preencher o tipo do representante.")
        cmbTipo_Repres.SetFocus
        Exit Sub
    Else
        vTipo = Mid(cmbTipo_Repres.Text, 1, 1)
    End If
    
    If cmbFilial.Text = "" Then
        Call vVB_Generica_001.Informar("� necess�rio preencher a Filial.")
        cmbFilial.SetFocus
        Exit Sub
    Else
        vFilial = Mid(cmbFilial.Text, 1, 4)
    End If
 
    'oliveira@ 10/09/2012 - CiT - [ISA 465]
    Dim oRepresentante As ClienteSAP
    Dim msgErro As String
    
    If vVB_Generica_001.ValidaCIC(txtCIC.Text) Then
        Set oRepresentante = BuscarClienteSAP(2, txtCIC.Text, msgErro)
    ElseIf vVB_Generica_001.ValidaCGC(txtCIC.Text) Then
        Set oRepresentante = BuscarClienteSAP(1, txtCIC.Text, msgErro)
    Else
        MsgBox "CPF ou CNPJ inv�lido", vbCritical, "CAD040 - Aten��o"
        Exit Sub
    End If
    
    If msgErro <> "" Then
        MsgBox msgErro
        Exit Sub
    End If
    
    Call PopularObjetoClienteSAP(oRepresentante)
    
    If Not EnviarClienteSAP(oRepresentante) Then
        'MsgBox "Erro ao gravar o cliente no SAP. Cadastro Abortado.", vbCritical, "CAD040 - Aten��o"
        Exit Sub
    End If
    
    ' Se funcionou o envio do cliente pro SAP ent�o persistimos no legado
    txtCod_Repres_SAP.Text = oRepresentante.NoClienteSAP
    
    If vTipo = "G" Then
        vCICGER = txtCIC.Text
    Else
        Criar_Cursor vBanco.Parameters, "vCursor"
        Set vObjOracle = vVB_Venda_001.TabelaGerente(vBanco, Mid(cmbGerente.Text, 1, 4), , 0)
        
        vCICGER = vObjOracle!CIC
    End If

   '-- ATUALIZA��O DA TABELA REPRESENTANTE
    vPseudo = Trim(txtPseudonimo.Text)
    vNome_Rep = Trim(txtNome_Repres.Text)
 
    vBanco.Parameters.Remove "PM_CIC"
    vBanco.Parameters.Add "PM_CIC", Trim(txtCIC.Text), 1
    vBanco.Parameters.Remove "PM_CODREPRES"
    vBanco.Parameters.Add "PM_CODREPRES", Trim(txtCod_Repres.Text), 1
    vBanco.Parameters.Remove "PM_NOME"
    vBanco.Parameters.Add "PM_NOME", Trim(txtNome_Repres.Text), 1
    vBanco.Parameters.Remove "PM_PSEUDONIMO"
    vBanco.Parameters.Add "PM_PSEUDONIMO", Trim(txtPseudonimo.Text), 1
    vBanco.Parameters.Remove "PM_DDD"
    vBanco.Parameters.Add "PM_DDD", IIf(txtDDD.Text = "", 0, txtDDD.Text), 1
    vBanco.Parameters.Remove "PM_FONE"
    vBanco.Parameters.Add "PM_FONE", IIf(txtFone.Text = "", 0, txtFone.Text), 1
    vBanco.Parameters.Remove "PM_FAX"
    vBanco.Parameters.Add "PM_FAX", IIf(txtFax.Text = "", 0, txtFax.Text), 1
    vBanco.Parameters.Remove "PM_RAMAL"
    vBanco.Parameters.Add "PM_RAMAL", IIf(txtRamal.Text = "", 0, txtRamal.Text), 1
    vBanco.Parameters.Remove "PM_CODFILIAL"
    vBanco.Parameters.Add "PM_CODFILIAL", IIf(Mid(cmbFilial.Text, 1, 4) = 0, 0, Mid(cmbFilial.Text, 1, 4)), 1
    vBanco.Parameters.Remove "PM_DIVISAO"
    vBanco.Parameters.Add "PM_DIVISAO", vDivisao, 1
    vBanco.Parameters.Remove "PM_TIPO"
    vBanco.Parameters.Add "PM_TIPO", vTipo, 1
    vBanco.Parameters.Remove "PM_DTCADASTR"
    vBanco.Parameters.Add "PM_DTCADASTR", Trim(txtDt_Cadastro.Text), 1
    vBanco.Parameters.Remove "PM_DTDESLIG"
    vBanco.Parameters.Add "PM_DTDESLIG", IIf(Trim(txtDt_Desligamento.Text) = "", Null, Trim(txtDt_Desligamento.Text)), 1
    vBanco.Parameters.Remove "PM_AGENCIA"
    vBanco.Parameters.Add "PM_AGENCIA", IIf(Trim(txtAgencia.Text) = "", Null, Trim(txtAgencia.Text)), 1
    vBanco.Parameters.Remove "PM_DIGAGENCIA"
    vBanco.Parameters.Add "PM_DIGAGENCIA", IIf(Trim(txtDig_Agencia.Text) = "", Null, Trim(txtDig_Agencia.Text)), 1
    vBanco.Parameters.Remove "PM_CONTA"
    vBanco.Parameters.Add "PM_CONTA", IIf(Trim(txtConta.Text) = "", Null, Trim(txtConta.Text)), 1
    vBanco.Parameters.Remove "PM_DIGCONTA"
    vBanco.Parameters.Add "PM_DIGCONTA", IIf(Trim(txtDig_Conta.Text) = "", Null, Trim(txtDig_Conta.Text)), 1
    vBanco.Parameters.Remove "PM_SITUACAO"
    vBanco.Parameters.Add "PM_SITUACAO", vSituacao, 1
    vBanco.Parameters.Remove "PM_SEQFRANQUIA"
    vBanco.Parameters.Add "PM_SEQFRANQUIA", "0", 1
    vBanco.Parameters.Remove "PM_CICGER"
    vBanco.Parameters.Add "PM_CICGER", IIf(vCICGER = 0, 0, vCICGER), 1
    vBanco.Parameters.Remove "PM_ENDERECO"
    'oliveira@ - CIT - 10/09/2012 - Endere�o Residencia seraparado por v�rgula
    'vBanco.Parameters.Add "PM_ENDERECO", Trim(txtEndereco_Resid.Text), 1
    vBanco.Parameters.Add "PM_ENDERECO", IIf(txtEndereco_Resid.Text = "", Null, ConcatenarEnderecoSAP(txtEndereco_Resid.Text, txtNum_Resid.Text, txtCompl_Resid.Text)), 1

    vBanco.Parameters.Remove "PM_BAIRRO"
    vBanco.Parameters.Add "PM_BAIRRO", Trim(txtBairro_Resid.Text), 1
    vBanco.Parameters.Remove "PM_CIDADE"
    vBanco.Parameters.Add "PM_CIDADE", val(cmbCidade_Resid.Text), 1
    vBanco.Parameters.Remove "PM_CEP"
    vBanco.Parameters.Add "PM_CEP", IIf(Trim(txtCEP_Resid.Text) = 0, Null, Trim(txtCEP_Resid.Text)), 1
    vBanco.Parameters.Remove "PM_TPSEGMENTO"
    vBanco.Parameters.Add "PM_TPSEGMENTO", IIf(val(Mid(cmbSegmento.Text, 1, 2)) = 0, Null, val(Mid(cmbSegmento.Text, 1, 2))), 1
   
   '-- FLAG DA TABELA HELPDESK.USUARIO
    If txtDt_Desligamento.Text <> "" And vSituacao = 9 Then
        vBanco.Parameters.Remove "PM_FLATIVO"
        vBanco.Parameters.Add "PM_FLATIVO", "N", 1
    Else
        vBanco.Parameters.Remove "PM_FLATIVO"
        vBanco.Parameters.Add "PM_FLATIVO", "S", 1
    End If
    
    ' fpallini - Ci&T - 21/09/12 - [ISA-444] Persistir COD_SAP no legado.
    vBanco.Parameters.Remove "PM_CODSAP"
    vBanco.Parameters.Add "PM_CODSAP", Trim(txtCod_Repres_SAP.Text), 1
    
    Criar_Cursor vBanco.Parameters, "vCursor"
    
    vSql = "Producao.PCK_CAD040.PR_ATUALIZA_REPRESENTANTE(:PM_CODREPRES, " & _
           ":PM_CIC, :PM_NOME, :PM_PSEUDONIMO, :PM_DDD, :PM_FONE, " & _
           ":PM_FAX, :PM_RAMAL, :PM_CODFILIAL, :PM_DIVISAO, :PM_TIPO, " & _
           "TO_DATE(:PM_DTCADASTR,'DD/MM/RR'), TO_DATE(:PM_DTDESLIG,'DD/MM/RR'), " & _
           ":PM_AGENCIA, :PM_DIGAGENCIA, " & _
           ":PM_CONTA, :PM_DIGCONTA, :PM_SITUACAO, :PM_SEQFRANQUIA, " & _
           ":PM_CICGER, :PM_ENDERECO, :PM_BAIRRO, :PM_CIDADE, :PM_CEP, " & _
           ":PM_TPSEGMENTO, :PM_FLATIVO, :PM_CODSAP)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    If vErro <> "" Then
        Call vVB_Generica_001.Informar("Erro no processo de grava��o da PR_ATUALIZA_REPRESENTANTE. Entre em contato com o SUPORTE.")
        Exit Sub
    End If
                
    If Trim(txtEndereco_Corresp.Text) <> "" Then
       '-- ATUALIZA��O DA TABELA REPR_END_CORRESP
        vBanco.Parameters.Remove "PM_CODREPRES"
        vBanco.Parameters.Add "PM_CODREPRES", Trim(txtCod_Repres.Text), 1
        vBanco.Parameters.Remove "PM_ENDERECO"
        'oliveira@ - CIT - 10/09/2012 - Endere�o Correspondencia seraparado por v�rgula
        'vBanco.Parameters.Add "PM_ENDERECO", IIf(Trim(txtEndereco_Corresp.Text) = "", Null, Trim(txtEndereco_Corresp.Text)), 1
        vBanco.Parameters.Add "PM_ENDERECO", IIf(txtEndereco_Corresp.Text = "", Null, ConcatenarEnderecoSAP(txtEndereco_Corresp.Text, txtNum_Corresp.Text, txtCompl_Corresp.Text)), 1
        vBanco.Parameters.Remove "PM_BAIRRO"
        vBanco.Parameters.Add "PM_BAIRRO", IIf(Trim(txtBairro_Corresp.Text) = "", Null, Trim(txtBairro_Corresp.Text)), 1
        vBanco.Parameters.Remove "PM_CIDADE"
        vBanco.Parameters.Add "PM_CIDADE", val(cmbCidade_Corresp.Text), 1
        vBanco.Parameters.Remove "PM_CEP"
        vBanco.Parameters.Add "PM_CEP", IIf(Trim(txtCEP_Corresp.Text) = "", 0, Trim(txtCEP_Corresp.Text)), 1
        vBanco.Parameters.Remove "PM_DDD"
        vBanco.Parameters.Add "PM_DDD", IIf(Trim(txtDDD_Corresp.Text) = "", 0, Trim(txtDDD_Corresp.Text)), 1
        vBanco.Parameters.Remove "PM_FONE"
        vBanco.Parameters.Add "PM_FONE", IIf(Trim(txtFone_Corresp.Text) = "", 0, Trim(txtFone_Corresp.Text)), 1
        vBanco.Parameters.Remove "PM_FAX"
        vBanco.Parameters.Add "PM_FAX", IIf(Trim(txtFax_Corresp.Text) = "", 0, Trim(txtFax_Corresp.Text)), 1
        vBanco.Parameters.Remove "PM_CELULAR"
        vBanco.Parameters.Add "PM_CELULAR", IIf(Trim(txtCelular_Corresp.Text) = "", 0, Trim(txtCelular_Corresp.Text)), 1
        vBanco.Parameters.Remove "PM_CXPOSTAL"
        vBanco.Parameters.Add "PM_CXPOSTAL", IIf(Trim(txtCxPostal_Corresp.Text) = "", 0, Trim(txtCxPostal_Corresp.Text)), 1
        vBanco.Parameters.Remove "PM_FLSESER"
        vBanco.Parameters.Add "PM_FLSESER", IIf(Trim(txtEtiqueta.Text) = "", Null, Trim(txtEtiqueta.Text)), 1
        vBanco.Parameters.Remove "PM_OBSERVACAO"
        vBanco.Parameters.Add "PM_OBSERVACAO", IIf(Trim(txtObservacao.Text) = "", Null, Trim(txtObservacao.Text)), 1
        
        Criar_Cursor vBanco.Parameters, "vCursor"
        
        vSql = "PRODUCAO.PCK_CAD040.PR_ATUALIZA_ENDCORRESP(:PM_CODREPRES, " & _
               ":PM_ENDERECO, :PM_BAIRRO, :PM_CIDADE, :PM_CEP, :PM_DDD, " & _
               ":PM_FONE, :PM_FAX, :PM_CELULAR, :PM_CXPOSTAL, :PM_FLSESER, " & _
               ":PM_OBSERVACAO)"
        
        If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
            Call vVB_Generica_001.Informar("Erro no processo de grava��o da PR_ATUALIZA_ENDCORRESP. Entre em contato com o SUPORTE.")
            Exit Sub
        End If
        
    End If
    '********
    'Eduardo
    '31/10/05
    '********
    If txtRamal <> "" Then Atualizar_Ramal_OnLine txtRamal, txtCod_Repres
        
    stbBarra.Panels(1).Text = "Pronto!"
    
    Call vVB_Generica_001.Informar("Transa��o efetuada com sucesso!")
    Call cmdCancelar_Click
    
End Sub

Private Sub cmdSituacao_Click()

If txtCod_Repres.Text <> "" Then
    
    If vSituacao = 0 Then
    
        'Verificar se existem carteiras associadas ao representante
        '25/10/2006 - Eduardo Relvas
        If UCase(Me.cmbTipo_Repres) = "F - FUNCIONARIO PROMOTOR" Then
            Set vObjOracle = vBanco.CreateDynaset("Select count(*) qtd from producao.r_promotor_carteira where cod_repres_promotor = " & Me.txtCod_Repres, &H0)
            If val(vObjOracle!Qtd) > 0 Then
               MsgBox "N�o � possivel desativar representante com carteiras associadas.", vbInformation, "Aten��o"
               Exit Sub
            End If
        End If
        
       '-- VERIFICA SE EXISTE ALGUM CLIENTE ATIVO LIGADO � ESTE REPRESENTANTE
        Criar_Cursor vBanco.Parameters, "vCursor"
         
        Set vObjOracle = vVB_Generica_001.ClienteAtivo(vBanco, txtCod_Repres.Text)
            
        If vObjOracle.RecordCount <> 0 Then
            MsgBox "N�o � poss�vel desativar representante com cliente ativo!", vbInformation, "Aten��o"
            Exit Sub
        End If
       
       '-- VERIFICA SE O REPRESENTANTE � UM FRANQUEADOR
        vBanco.Parameters.Remove "PM_CODREPRES"
        vBanco.Parameters.Add "PM_CODREPRES", txtCod_Repres.Text, 1
        
        Criar_Cursor vBanco.Parameters, "vCursor"

        vSql = "Producao.PCK_CAD040.PR_BUSCA_FRANQUEADOR(:PM_CODREPRES, NULL, :vCursor)"

        If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
            Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
            Exit Sub
        Else
            Set vObjOracle = vBanco.Parameters("VCURSOR").Value
        End If
        
        If vObjOracle.RecordCount <> 0 Then
            Call vVB_Generica_001.Informar("N�o � poss�vel desativar representante FRANQUEADOR!")
            Exit Sub
        End If
        
        '-- VERIFICA SE EXISTE ALGUMA DUPLICATA EM ABERTO LIGADA � ESTE REPRESENTANTE
        Criar_Cursor vBanco.Parameters, "vCursor"
        
        Set vObjOracle = vVB_Cobranca_001.TabelaDuplicata(vBanco, , , , , , , , 0, , txtCod_Repres.Text)
        
        If Not Nothing Is vObjOracle Then
            If vObjOracle.RecordCount > 0 Then
                MsgBox "Este representante/vendedor possui duplicatas em aberto, para desativa-lo ser� necess�rio informar quem ser� o novo respons�vel", vbInformation, "Aten��o"
                flFormDupl_Fechado = False
                frmDuplAberto.Show vbModal
                Exit Sub
            End If
        End If
        
        txtSituacao.Text = "DESATIVADO"
        txtSituacao.ForeColor = vbRed
        Set cmdSituacao.PictureNormal = LoadPicture(App.Path & "\ativo 32x32.ico")
        vSituacao = 9
        txtDt_Desligamento.SetFocus
    Else
        txtSituacao.Text = "ATIVO"
        txtSituacao.ForeColor = vbBlack
        Set cmdSituacao.PictureNormal = LoadPicture(App.Path & "\Inativo 32x32.ico")
        vSituacao = 0
        txtDT_DESLIGA = ""
    End If
    
    cmdGravar.SetFocus
End If
    
End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    Dim Valido As Boolean
    Dim CIC_GER As String
    
    'hemerson - 21/07/2015 -- TI-2869
     txtPseudonimo.MaxLength = 15
    
   '-- PREENCHE O COMBO DE DIVIS�O
    Criar_Cursor vBanco.Parameters, "VCURSOR"
    
    Set vObjOracle = vVB_Generica_001.TabelaDivisao(vBanco)
'TODO REVIEW EOLIVEIRA
'    Call VB_Generica_001.PreencheComboList(vObjOracle, cmbDivisao, "DIVISAO", "DESC. DIVISAO")
    
    cmbDivisao.Text = "D - DPK"
   
   '-- PREENCHE OS COMBOS DE UF DA TELA DE REPRESENTANTE
    Criar_Cursor vBanco.Parameters, "vCursor"
    
    Set vObjOracle = vVB_Generica_001.TabelaUF(vBanco)
    Call vVB_Generica_001.PreencheComboList(vObjOracle, cmbUF_Resid, "UF")

    vObjOracle.MoveFirst
    Call vVB_Generica_001.PreencheComboList(vObjOracle, cmbUF_Corresp, "UF")
    
   '-- PREENCHE O COMBO DE GERENTES
    Criar_Cursor vBanco.Parameters, "vCursor"
    
    Set vObjOracle = vVB_Venda_001.TabelaGerente(vBanco, , , 0)
    
    Call vVB_Generica_001.PreencheComboList(vObjOracle, cmbGerente, "C�D.GERENTE", "PSEUD�NIMO")
    'Cit => Call vVB_Generica_001.PreencheComboList(vObjOracle, cmbGerente, "COD.GERENTE", "PSEUDONIMO")
    
   '-- PREENCHE O COMBO DE TIPOS DE GERENTE
    cmbTipo_Repres.AddItem "C - CURINGA"
    cmbTipo_Repres.AddItem "F - FUNCIONARIO PROMOTOR"
    cmbTipo_Repres.AddItem "G - GERENTE"
    cmbTipo_Repres.AddItem "M - TELEMISTO"
    cmbTipo_Repres.AddItem "P - PROMOTOR"
    cmbTipo_Repres.AddItem "R - REPRESENTANTE"
    cmbTipo_Repres.AddItem "V - TELEVENDAS"
   
   '-- CARREGA O COMBO DE TIPO DE SEGMENTOS
    Criar_Cursor vBanco.Parameters, "vCursor"
    
    vSql = "Producao.PCK_CAD040.PR_BUSCA_TIPO_SEGMENTO(:vCursor)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_TIPO_SEGMENTO, entre em contato com o SUPORTE!")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("VCURSOR").Value
    End If
   
    Do While Not vObjOracle.EOF
        Conteudo = ""
        TIPO = vObjOracle!COD_TIPO_SEGMENTO
        Do While val(TIPO) = val(vObjOracle!COD_TIPO_SEGMENTO)
            If Conteudo = "" Then
                Conteudo = Format(vObjOracle!COD_TIPO_SEGMENTO, "00") & " - " & vObjOracle!DESC_SEGMENTO
            Else
                Conteudo = Conteudo & ", " & vObjOracle!DESC_SEGMENTO
            End If
            TIPO = vObjOracle!COD_TIPO_SEGMENTO
            vObjOracle.MoveNext
            
            If vObjOracle.EOF Then
                Exit Do
            End If
        Loop
        Call vVB_Generica_001.PreencheComboListValor(cmbSegmento, False, Conteudo)
    Loop
   
   '-- PREENCHE O COMBO DE COMBINA��O DE LOJAS PARA VIS�O DO REPRESENTANTE
    Criar_Cursor vBanco.Parameters, "VCURSOR"
    
    Set vObjOracle = vVB_PAAC_001.TabelaTipoLoja(vBanco)

    Do While Not vObjOracle.EOF
        TIPO = vObjOracle.Fields(2).Value
        Conteudo = ""
        Conteudo = Format(vObjOracle.Fields(2).Value, "00") & " - "
        Do While val(TIPO) = val(vObjOracle.Fields(2).Value)
            Conteudo = Conteudo & vObjOracle.Fields(0).Value & ","
            vObjOracle.MoveNext
            If vObjOracle.EOF Then
                Exit Do
            End If
        Loop
        Conteudo = Mid(Conteudo, 1, Len(Conteudo) - 1)
        Call vVB_Generica_001.PreencheComboListValor(cmbVisao, False, Conteudo)
    Loop
    
   '-- PREENCHE O COMBO DE VDR E BASE
    cmbVdr.AddItem "S - SIM"
    cmbVdr.AddItem "N - N�O"
    
    cmbBase.AddItem "S - SIM"
    cmbBase.AddItem "N - N�O"
    
     'carlosj CiT (25/09/2012) - correcao [ISA-632] - caso a conexao do legado nao esteja OK
     'os parametros ja estarao carregados
     Call oServicoClienteSAP.CarregarParametrosSistema
    
End Sub

Private Sub txtAgencia_KeyPress(KeyAscii As Integer)
    KeyAscii = ValidaNumero(KeyAscii)
End Sub

Private Sub txtBairro_Corresp_KeyPress(KeyAscii As Integer)
    
    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)

End Sub

Private Sub txtBairro_Resid_KeyPress(KeyAscii As Integer)
    
    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)

End Sub

Private Sub txtCelular_Corresp_KeyPress(KeyAscii As Integer)
    KeyAscii = ValidaNumero(KeyAscii)
End Sub

Private Sub txtCEP_Corresp_KeyPress(KeyAscii As Integer)
    KeyAscii = ValidaNumero(KeyAscii)
End Sub

Private Sub txtCEP_Resid_KeyPress(KeyAscii As Integer)
    KeyAscii = ValidaNumero(KeyAscii)
End Sub

Private Sub txtCIC_KeyPress(KeyAscii As Integer)
    KeyAscii = ValidaNumero(KeyAscii)
End Sub

Private Sub txtCIC_LostFocus()
    If txtCIC.Text = "" Then
        Exit Sub
    ElseIf txtCIC.Text <> 0 Then
        Valido = vVB_Generica_001.ValidaCIC(txtCIC.Text)
        
        If Valido = False Then
            vVB_Generica_001.Informar ("CIC inv�lido. Favor digit�-lo novamente!")
            txtCIC.Text = ""
            txtCIC.SetFocus
            Exit Sub
        End If
    Else
        Exit Sub
    End If
    
   '-- CARREGA OS DADOS DO REPRESENTANTE NA TELA ATRAV�S DO CIC
    Criar_Cursor vBanco.Parameters, "vCursor"
    
    Set vObjOracle = vVB_Venda_001.TabelaRepresentante(vBanco, , , txtCIC.Text)
    
    If Not vObjOracle.EOF Then
        txtCod_Repres.Text = vObjOracle.Fields(1).Value
        Call txtCod_Repres_LostFocus
    End If
    
End Sub

Private Sub txtCod_Repres_Click()
    Call cmdCancelar_Click
End Sub

Private Sub txtCod_Repres_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Numero(KeyAscii)
End Sub

Private Sub txtCod_Repres_LostFocus()
    Dim PIC As Picture
    Dim rua As String
    Dim Numero As String
    Dim complemento As String
   
    If txtCod_Repres.Text <> "" Then
       '-- CARREGA OS DADOS DO REPRESENTANTE NA TELA
        Criar_Cursor vBanco.Parameters, "vCursor"
        
        Set vObjOracle = vVB_Venda_001.TabelaRepresentante(vBanco, txtCod_Repres.Text)
        
        If vObjOracle.EOF Then
            txtSituacao.Text = "ATIVO"
            txtSituacao.ForeColor = vbBlack
            Set cmdSituacao.PictureNormal = LoadPicture(App.Path & "\Inativo 32x32.ico")
            txtDt_Cadastro.Text = Date
        Else
            txtPseudonimo.Text = vObjOracle.Fields(3).Value
    
            If vObjOracle.Fields(15).Value = 0 Then
                txtSituacao.Text = "ATIVO"
                txtSituacao.ForeColor = vbBlack
                vSituacao = 0
                Set cmdSituacao.PictureNormal = LoadPicture(App.Path & "\Inativo 32x32.ico")
            Else
                txtSituacao.Text = "DESATIVADO"
                txtSituacao.ForeColor = vbRed
                vSituacao = 9
                Set cmdSituacao.PictureNormal = LoadPicture(App.Path & "\ATIVO 32x32.ICO")
            End If
            
            txtCIC.Text = vObjOracle.Fields(0).Value
            txtNome_Repres.Text = vObjOracle.Fields(2).Value
            txtDDD.Text = IIf(IsNull(vObjOracle.Fields(8).Value), "", vObjOracle.Fields(8).Value)
            txtFone.Text = IIf(IsNull(vObjOracle.Fields(9).Value), "", vObjOracle.Fields(9).Value)
            txtFax.Text = IIf(IsNull(vObjOracle.Fields(10).Value), "", vObjOracle.Fields(10).Value)
            txtRamal.Text = IIf(IsNull(vObjOracle.Fields(11).Value), "", vObjOracle.Fields(11).Value)

            'Kevlin Toshinari Ossada - Ci&T - 11/9/2012 - Carregar o cod sap no campo
            vBanco.Parameters.Remove "PM_COD_REPRES"
            vBanco.Parameters.Add "PM_COD_REPRES", CInt(txtCod_Repres.Text), 1
            
            Criar_Cursor vBanco.Parameters, "vCursor"
            
            vSql = "PRODUCAO.PCK_CAD040.PR_BUSCA_COD_SAP_REPRESENTANTE(:PM_COD_REPRES, :VCURSOR)"
            
            If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
                vVB_Generica_001.Informar ("Erro no processamento, entre em contato com o SUPORTE!")
                Exit Sub
            Else
                Set vObjOracle2 = vBanco.Parameters("VCURSOR").Value
                If Not vObjOracle2.EOF Then
                    txtCod_Repres_SAP.Text = IIf(IsNull(vObjOracle2!COD_SAP.Value), "", vObjOracle2!COD_SAP.Value)
                End If
            End If
            

            
            
            If vObjOracle.Fields(14).Value = "D" Then
                cmbDivisao.Text = "D - DPK"
           'ElseIf vObjOracle.Fields(14).Value = "B" Then
           '    cmbDivisao.Text = "B - BLAU PECAS"
            ElseIf vObjOracle.Fields(14).Value = "P" Then
                cmbDivisao.Text = "P - DPASCHOAL"
            ElseIf vObjOracle.Fields(14).Value = "Z" Then
                cmbDivisao.Text = "Z - AUTOZ"
            End If
            
           '-- BUSCA O NOME DO GERENTE ATRAV�S DO CIC
            Criar_Cursor vBanco.Parameters, "vCursor"
        
            Set vObjOracle2 = vVB_Venda_001.TabelaGerente(vBanco, 0, vObjOracle.Fields(13).Value)
    
            cmbGerente.Text = Format(vObjOracle2.Fields(2), "0000") & " - " & vObjOracle2.Fields(1)
            
            cmbFilial.Text = Format(vObjOracle.Fields(12).Value, "0000") & " - " & vObjOracle.Fields(39).Value
            
           '-- Buscar atrav�s do c�digo da filial o centro de custo
            vBanco.Parameters.Remove "PM_CODFILIAL"
            vBanco.Parameters.Add "PM_CODFILIAL", vObjOracle.Fields(12).Value, 1
             
            Criar_Cursor vBanco.Parameters, "vCursor"
            
            vSql = "Producao.PCK_CAD040.PR_BUSCA_CCUSTO_FILIAL(:PM_CODFILIAL, :vCursor)"
            
            If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
                Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_CCUSTO_FILIAL, entre em contato com o SUPORTE!")
                Exit Sub
            Else
                Set vObjOracle2 = vBanco.Parameters("VCURSOR").Value
            End If
            
            txtCentro_Custo.Text = vObjOracle2!COD_CENTRO_CUSTO & " - " & vObjOracle2!DESC_CENTRO_CUSTO
            
            If vObjOracle.Fields(16).Value = "R" Then
                cmbTipo_Repres.Text = "R - Representante"
            ElseIf vObjOracle.Fields(16).Value = "A" Then
                cmbTipo_Repres.Text = "A - Teleatendimento"
            ElseIf vObjOracle.Fields(16).Value = "V" Then
                cmbTipo_Repres.Text = "V - Televendas"
            ElseIf vObjOracle.Fields(16).Value = "M" Then
                cmbTipo_Repres.Text = "M - Telemisto"
            ElseIf vObjOracle.Fields(16).Value = "G" Then
                cmbTipo_Repres.Text = "G - Gerente"
            ElseIf vObjOracle.Fields(16).Value = "P" Then
                cmbTipo_Repres.Text = "P - Promotor"
            ElseIf vObjOracle.Fields(16).Value = "C" Then
                cmbTipo_Repres.Text = "C - Curinga"
            ElseIf vObjOracle.Fields(16).Value = "F" Then
                cmbTipo_Repres.Text = "F - Funcionario Promotor"
            End If
            
            txtDt_Cadastro.Text = Format(vObjOracle.Fields(18).Value, "dd/mm/yy")
            txtDt_Desligamento.Text = Format(vObjOracle.Fields(19).Value, "dd/mm/yy")
            txtAgencia.Text = IIf(IsNull(vObjOracle.Fields(20).Value), "", vObjOracle.Fields(20).Value)
            txtDig_Agencia.Text = IIf(IsNull(vObjOracle.Fields(21).Value), "", vObjOracle.Fields(21).Value)
            txtConta.Text = IIf(IsNull(vObjOracle.Fields(22).Value), "", vObjOracle.Fields(22).Value)
            txtDig_Conta.Text = IIf(IsNull(vObjOracle.Fields(23).Value), "", vObjOracle.Fields(23).Value)
            cmbSegmento.Text = vObjOracle.Fields(24).Value & " - " & vObjOracle.Fields(25).Value
            
            'oliveira@ - CIT - 10/09/12 - Carrega os endere�os de Residencia
            'txtEndereco_Resid.Text = IIf(IsNull(vObjOracle.Fields(4).Value), "", vObjOracle.Fields(4).Value)
            rua = ""
            Numero = ""
            complemento = ""
            
            If Not IsNull(vObjOracle.Fields(4).Value) Then
                Call DesconcatenarEnderecoSAP(vObjOracle.Fields(4).Value, rua, Numero, complemento)
            End If
            
            txtEndereco_Resid.Text = rua
            If Numero = "" Then
                txtNum_Resid.Text = 0
            Else
                txtNum_Resid.Text = Numero
            End If
            txtCompl_Resid.Text = complemento
        
            
            txtBairro_Resid.Text = IIf(IsNull(vObjOracle.Fields(5).Value), "", vObjOracle.Fields(5).Value)
            txtCEP_Resid.Text = IIf(IsNull(vObjOracle.Fields(7).Value), "", vObjOracle.Fields(7).Value)
            
           '-- BUSCA O NOME DA CIDADE E UF DO ENDERE�O RESIDENCIAL
            Criar_Cursor vBanco.Parameters, "vCursor"
        
            Set vObjOracle2 = vVB_Generica_001.TabelaCidade(vBanco, vObjOracle.Fields(6).Value)
    
            cmbCidade_Resid.Text = Format(vObjOracle2.Fields(0).Value, "0000") & " - " & vObjOracle2.Fields(1)
            cmbUF_Resid.Text = vObjOracle2.Fields(3)
        
            '-- CARREGA OS DADOS DE ENDERE�O DE CORRESPOND�NCIA
            'oliveira@ - CIT - 10/09/12 - Carrega os endere�os de Correspondencia
            'txtEndereco_Corresp.Text = IIf(IsNull(vObjOracle.Fields(26).Value), " ", vObjOracle.Fields(26).Value)
            rua = ""
            Numero = ""
            complemento = ""
            
            If Not IsNull(vObjOracle.Fields(26).Value) Then
                Call DesconcatenarEnderecoSAP(vObjOracle.Fields(26).Value, rua, Numero, complemento)
            End If
            
            txtEndereco_Corresp.Text = rua
            If Numero = "" Then
                txtNum_Corresp.Text = 0
            Else
                txtNum_Corresp.Text = Numero
            End If
            txtCompl_Corresp.Text = complemento
    
            txtBairro_Corresp.Text = IIf(IsNull(vObjOracle.Fields(27).Value), " ", vObjOracle.Fields(27).Value)
            txtCEP_Corresp.Text = IIf(IsNull(vObjOracle.Fields(29).Value), 0, vObjOracle.Fields(29).Value)
            txtCxPostal_Corresp.Text = IIf(IsNull(vObjOracle.Fields(34).Value), 0, vObjOracle.Fields(34).Value)
            txtDDD_Corresp.Text = IIf(IsNull(vObjOracle.Fields(30).Value), 0, vObjOracle.Fields(30).Value)
            txtFone_Corresp.Text = IIf(IsNull(vObjOracle.Fields(31).Value), 0, vObjOracle.Fields(31).Value)
            txtFax_Corresp.Text = IIf(IsNull(vObjOracle.Fields(32).Value), 0, vObjOracle.Fields(32).Value)
            txtCelular_Corresp.Text = IIf(IsNull(vObjOracle.Fields(33).Value), 0, vObjOracle.Fields(33).Value)
            
            If Not IsNull(vObjOracle.Fields(28).Value) Then
               '-- BUSCA O NOME DA CIDADE E UF DO ENDERE�O DE CORRESPOND�NCIA
                Criar_Cursor vBanco.Parameters, "vCursor"
            
                Set vObjOracle2 = vVB_Generica_001.TabelaCidade(vBanco, vObjOracle.Fields(28).Value)
        
                cmbCidade_Corresp.Text = IIf(IsNull(vObjOracle2.Fields(0).Value), " ", Format(vObjOracle2.Fields(0).Value, "0000")) & " - " & vObjOracle2.Fields(1)
                cmbUF_Corresp.Text = IIf(IsNull(vObjOracle2.Fields(3)), " ", vObjOracle2.Fields(3))
            End If
            
            txtEtiqueta.Text = IIf(IsNull(vObjOracle.Fields(35).Value), "N", vObjOracle.Fields(35).Value)
            txtObservacao.Text = IIf(IsNull(vObjOracle.Fields(36).Value), " ", vObjOracle.Fields(36).Value)
            
           '-- CARREGA PARTE DE INTRANET E INFORMATIZADOS
            txtCxFTP.Text = IIf(IsNull(vObjOracle.Fields(37).Value), "", vObjOracle.Fields(37).Value)
            
            If vObjOracle.Fields(38).Value = "S" Then
                cmbVdr.Text = "S - SIM"
            Else
                cmbVdr.Text = "N - N�O"
            End If
            
            Criar_Cursor vBanco.Parameters, "vCursor"
      Set vVB_PAAC_001 = New clsVB_PAAC_001
        'TODO REVIEW EOLIVEIRA
            Set vObjOracle2 = vVB_PAAC_001.BaseRepresentante(vBanco, txtCod_Repres.Text)
    
            If vObjOracle2.Fields(5).Value = "S" Then
                cmbBase.Text = "S - SIM"
            Else
                cmbBase.Text = "N - N�O"
            End If
            
            If Not vObjOracle2.EOF Then
                TIPO = vObjOracle2.Fields(4).Value
                Conteudo = Format(TIPO, "00") & " - "
                Do While TIPO = vObjOracle2.Fields(4).Value
                    Conteudo = Conteudo & vObjOracle2.Fields(2).Value & ","
                    vObjOracle2.MoveNext
                    If vObjOracle2.EOF Then
                        Exit Do
                    End If
                Loop
                Conteudo = Mid(Conteudo, 1, Len(Conteudo) - 1)
                cmbVisao.Text = Conteudo
            End If
        End If
    End If
End Sub

' oliveira@ 10/09/2012 CiT - [ISA 524]
Private Sub txtCompl_Corresp_KeyPress(KeyAscii As Integer)
    KeyAscii = ValidaMaiuscula(KeyAscii)
    KeyAscii = ValidaAlfaNumerico(KeyAscii)
End Sub

' oliveira@ 10/09/2012 CiT - [ISA 524]
Private Sub txtCompl_Resid_KeyPress(KeyAscii As Integer)
    KeyAscii = ValidaMaiuscula(KeyAscii)
    KeyAscii = ValidaAlfaNumerico(KeyAscii)
End Sub

Private Sub txtConta_KeyPress(KeyAscii As Integer)
    KeyAscii = ValidaNumero(KeyAscii)
End Sub

Private Sub txtCxPostal_Corresp_KeyPress(KeyAscii As Integer)
    KeyAscii = ValidaNumero(KeyAscii)
End Sub

Private Sub txtDDD_Corresp_KeyPress(KeyAscii As Integer)
    KeyAscii = ValidaNumero(KeyAscii)
End Sub

Private Sub txtDDD_KeyPress(KeyAscii As Integer)
    KeyAscii = ValidaNumero(KeyAscii)
End Sub

Private Sub txtDig_KeyPress(KeyAscii As Integer)
    KeyAscii = ValidaNumero(KeyAscii)
End Sub

Private Sub txtDt_Cadastro_KeyPress(KeyAscii As Integer)
    
    Call vVB_Generica_001.Data(KeyAscii, txtDt_Cadastro)

End Sub

Private Sub txtDt_Desligamento_KeyPress(KeyAscii As Integer)
    
    Call vVB_Generica_001.Data(KeyAscii, txtDt_Desligamento)

End Sub

Private Sub txtEndereco_Corresp_KeyPress(KeyAscii As Integer)
    ' oliveira@ 10/09/2012 CiT - [ISA 524]
    'KeyAscii = VB_Generica_001.Maiuscula(KeyAscii)
    KeyAscii = ValidaMaiuscula(KeyAscii)
    KeyAscii = ValidaAlfaNumerico(KeyAscii)

End Sub

Private Sub txtEndereco_Resid_KeyPress(KeyAscii As Integer)
    ' oliveira@ 10/09/2012 CiT - [ISA 524]
    'KeyAscii = VB_Generica_001.Maiuscula(KeyAscii)
    KeyAscii = ValidaMaiuscula(KeyAscii)
    KeyAscii = ValidaAlfaNumerico(KeyAscii)
End Sub

Private Sub txtEtiqueta_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
End Sub

Private Sub txtFax_Corresp_KeyPress(KeyAscii As Integer)
    KeyAscii = ValidaNumero(KeyAscii)
End Sub

Private Sub txtFax_KeyPress(KeyAscii As Integer)
    KeyAscii = ValidaNumero(KeyAscii)
End Sub

Private Sub txtFone_Corresp_KeyPress(KeyAscii As Integer)
    KeyAscii = ValidaNumero(KeyAscii)
End Sub

Private Sub txtFone_KeyPress(KeyAscii As Integer)
    KeyAscii = ValidaNumero(KeyAscii)
End Sub

Private Sub txtNome_Repres_DblClick()
    Dim vNOME As String
    
    Call cmdCancelar_Click
    
    vNOME = InputBox("Digite o NOME do representante!", "Informa��o")

    If vNOME = "" Then
        Exit Sub
    Else
       '-- CARREGA OS DADOS DO REPRESENTANTE NA TELA ATRAV�S DO NOME
        Criar_Cursor vBanco.Parameters, "vCursor"
        
        Set vObjOracle = vVB_Venda_001.TabelaRepresentante(vBanco, , UCase(vNOME))
         
        If vObjOracle.EOF Then
            Call vVB_Generica_001.Informar("Nenhum representante encontrado!")
        Else
           '-- CARREGA O GRID
            Call vVB_Generica_001.CarregaGridTabela(frmConsulta.mfgRepresentante, vObjOracle, 3, "1;2")
            frmConsulta.Show vbModal
             
            If txtCod_Repres.Text <> "" Then
                Call txtCod_Repres_LostFocus
            Else
                txtNome_Repres.SetFocus
            End If
        End If
    End If
End Sub

Private Sub txtNome_Repres_KeyPress(KeyAscii As Integer)
    
    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)

End Sub

' oliveira@ 10/09/2012 CiT - [ISA 524]
Private Sub txtNum_Corresp_KeyPress(KeyAscii As Integer)
    KeyAscii = ValidaNumero(KeyAscii)
End Sub

' oliveira@ 10/09/2012 CiT - [ISA 524]
Private Sub txtNum_Resid_KeyPress(KeyAscii As Integer)
    KeyAscii = ValidaNumero(KeyAscii)
End Sub

Private Sub txtObservacao_KeyPress(KeyAscii As Integer)
    
    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)

End Sub

Private Sub txtPseudonimo_KeyPress(KeyAscii As Integer)
    
    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)

End Sub

'oliveira@ - CiT - [11/09/2012] - [ISA 471]
Sub PopularObjetoClienteSAP(ByRef oCliente As ClienteSAP)
        
        If val(txtCod_Repres.Text) > 0 Then
            oCliente.NoClientePOS = txtCod_Repres.Text
        End If
        
        If val(txtCod_Repres_SAP.Text) > 0 Then
            oCliente.NoClienteSAP = txtCod_Repres_SAP.Text
        End If
        
        oCliente.Name1 = txtNome_Repres.Text
        oCliente.Name2 = txtPseudonimo.Text
        
        If vVB_Generica_001.ValidaCIC(txtCIC.Text) Then
            oCliente.CPF = Format(txtCIC.Text, "00000000000")
        ElseIf vVB_Generica_001.ValidaCGC(txtCIC.Text) Then
            oCliente.CNPJ = Format(txtCIC.Text, "00000000000000")
        End If
                        
        If IsDate(txtDt_Cadastro.Text) Then
            oCliente.DataCadastro = CDate(txtDt_Cadastro.Text)
            oCliente.AnoCadastro = Year(oCliente.DataCadastro)
            oCliente.MesCadastro = Month(oCliente.DataCadastro)
        End If
                      
        ' Se cliente n�o tinha endere�o 0, seta o primeiro
        If oCliente.EnderecoPrincipal Is Nothing Then
            oCliente.EnderecoPrincipal = New endereco
        End If
        
        If oCliente.EnderecoCobranca Is Nothing Then
            oCliente.EnderecoCobranca = New endereco
        End If
        
        'Endere�o
        '***end residencia ***principal
        oCliente.EnderecoPrincipal.rua = txtEndereco_Resid.Text
        oCliente.EnderecoPrincipal.NoResidencia = txtNum_Resid.Text
        oCliente.EnderecoPrincipal.complemento = txtCompl_Resid.Text
        oCliente.EnderecoPrincipal.bairro = txtBairro_Resid.Text
        oCliente.EnderecoPrincipal.CEP = txtCEP_Resid.Text
        oCliente.EnderecoPrincipal.CxPostal = ""
        oCliente.EnderecoPrincipal.cidade = Split(cmbCidade_Resid.Text, " - ")(1)
        oCliente.EnderecoPrincipal.Regiao = cmbUF_Resid.Text
        oCliente.EnderecoPrincipal.DDDFone = txtDDD.Text
        oCliente.EnderecoPrincipal.Telefone = txtFone.Text
        oCliente.EnderecoPrincipal.TelFax = txtFax.Text
        
        '***end cobranca
        oCliente.EnderecoCobranca.rua = txtEndereco_Corresp.Text
        oCliente.EnderecoCobranca.NoResidencia = txtNum_Corresp.Text
        oCliente.EnderecoCobranca.complemento = txtCompl_Corresp.Text
        oCliente.EnderecoCobranca.bairro = txtBairro_Corresp.Text
        oCliente.EnderecoCobranca.CEP = txtCEP_Corresp.Text
        oCliente.EnderecoCobranca.CxPostal = txtCxPostal_Corresp.Text
        If cmbCidade_Corresp.Text <> "" Then
            oCliente.EnderecoCobranca.cidade = Split(cmbCidade_Corresp.Text, " - ")(1)
        End If
        oCliente.EnderecoCobranca.Regiao = cmbUF_Corresp.Text
        oCliente.EnderecoCobranca.DDDFone = txtDDD_Corresp.Text
        oCliente.EnderecoCobranca.Telefone = txtFone_Corresp.Text
        oCliente.EnderecoCobranca.Celular = txtCelular_Corresp.Text
        oCliente.EnderecoCobranca.TelFax = txtFax_Corresp.Text
        
        'Contato
        If oCliente.ContatoPrincipal Is Nothing Then
            oCliente.ContatoPrincipal = New Contato
        End If
        
        oCliente.ContatoPrincipal.Nome = txtNome_Repres.Text
        oCliente.ContatoPrincipal.TpContato = "1"
        
        'Fiscal
        If oCliente.FISCAL Is Nothing Then
            oCliente.FISCAL = New FISCAL
        End If
        
        oCliente.FISCAL.MsgFiscal1 = "N"
        oCliente.FISCAL.MsgFiscal2 = "N"
        
        'Banco
         If oCliente.BancoPrincipal Is Nothing Then
            oCliente.BancoPrincipal = New Banco
        End If
        
        ' OrgVendas
        If oCliente.OrgVendas Is Nothing Then
            oCliente.OrgVendas = New OrgVendas
        End If
        
'        oCliente.BancoPrincipal.Agencia = txtAgencia.Text & txtDig_Agencia.Text
'        oCliente.BancoPrincipal.ContaCorrente = txtConta.Text & txtDig_Conta.Text
    
End Sub

Private Sub txtRamal_KeyPress(KeyAscii As Integer)
    KeyAscii = ValidaNumero(KeyAscii)
End Sub
