VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form frmTrocarDuplicatas 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Transferir Duplicatas"
   ClientHeight    =   4065
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6750
   Icon            =   "frmTrocarDuplicatas.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4065
   ScaleWidth      =   6750
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdTrocar 
      Caption         =   "Trocar"
      Enabled         =   0   'False
      Height          =   420
      Left            =   4815
      TabIndex        =   4
      Top             =   3240
      Width           =   1860
   End
   Begin VB.Frame fra 
      Appearance      =   0  'Flat
      Caption         =   "Novo Representante"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   2265
      Left            =   45
      TabIndex        =   3
      Top             =   900
      Width           =   6630
      Begin VB.TextBox txtFilial 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1080
         Locked          =   -1  'True
         TabIndex        =   15
         Top             =   720
         Width           =   5370
      End
      Begin VB.TextBox txtSegmento 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1080
         Locked          =   -1  'True
         TabIndex        =   13
         Top             =   1125
         Width           =   5370
      End
      Begin VB.TextBox txtPseudonimo 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2475
         Locked          =   -1  'True
         MaxLength       =   15
         TabIndex        =   10
         Top             =   315
         Width           =   3975
      End
      Begin VB.TextBox txtSituacao 
         Appearance      =   0  'Flat
         BackColor       =   &H80000004&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1080
         Locked          =   -1  'True
         TabIndex        =   9
         Top             =   1530
         Width           =   1770
      End
      Begin VB.TextBox txtDt_Desligamento 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   5175
         Locked          =   -1  'True
         TabIndex        =   8
         Top             =   1530
         Width           =   1275
      End
      Begin VB.TextBox txtCodRepres 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1440
         MaxLength       =   5
         TabIndex        =   6
         Top             =   315
         Width           =   960
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Filial:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   630
         TabIndex        =   16
         Top             =   810
         Width           =   360
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         Caption         =   "Segmento:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   90
         TabIndex        =   14
         Top             =   1215
         Width           =   960
      End
      Begin VB.Label lblSituacao 
         Appearance      =   0  'Flat
         Caption         =   "Situa��o:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   270
         TabIndex        =   12
         Top             =   1620
         Width           =   780
      End
      Begin VB.Label lblDt_Desligamento 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Dt. Desligamento:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   3645
         TabIndex        =   11
         Top             =   1620
         Width           =   1470
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Representante:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   90
         TabIndex        =   7
         Top             =   405
         Width           =   1275
      End
      Begin VB.Label lblMsg 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   195
         Left            =   135
         TabIndex        =   5
         Top             =   1935
         Width           =   6390
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   3735
      Width           =   6750
      _ExtentX        =   11906
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   11853
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      Top             =   810
      Width           =   6630
      _ExtentX        =   11695
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmTrocarDuplicatas.frx":23D2
      PICN            =   "frmTrocarDuplicatas.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmTrocarDuplicatas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdTrocar_Click()

    If MsgBox("Confirma a transfer�ncia de todas as duplicatas para este novo respons�vel?", vbYesNo, "ATEN��O") = vbYes Then
    
        Criar_Cursor vBanco.Parameters, "vCursor"
        Set vObjOracle = vVB_Cobranca_001.TabelaDuplicata(vBanco, , , , , , , , 0, , frmRepresentante.txtCod_Repres.Text)
            
        For i = 1 To vObjOracle.RecordCount
        
            vBanco.Parameters.Remove "PM_CODREPRES"
            vBanco.Parameters.Add "PM_CODREPRES", frmRepresentante.txtCod_Repres.Text, 1
            vBanco.Parameters.Remove "PM_CODREPRES_NOVO"
            vBanco.Parameters.Add "PM_CODREPRES_NOVO", txtCodRepres.Text, 1
            vBanco.Parameters.Remove "PM_CODLOJA"
            vBanco.Parameters.Add "PM_CODLOJA", vObjOracle!cod_loja, 1
            vBanco.Parameters.Remove "PM_NUMDUPLI"
            vBanco.Parameters.Add "PM_NUMDUPLI", vObjOracle!num_fatura, 1
            vBanco.Parameters.Remove "PM_NUMORDEM"
            vBanco.Parameters.Add "PM_NUMORDEM", vObjOracle!num_ordem, 1
            vBanco.Parameters.Remove "PM_DTFATURA"
            vBanco.Parameters.Add "PM_DTFATURA", vObjOracle!Dt_Vencimento, 1
            vBanco.Parameters.Remove "PM_ERRO"
            vBanco.Parameters.Add "PM_ERRO", 0, 2
            vBanco.Parameters.Remove "PM_MSGERRO"
            vBanco.Parameters.Add "PM_MSGERRO", "", 2
            
            
            vSql = "Producao.PCK_CAD040.PR_TRANSFERENCIA_DUPLICATA(:PM_CODREPRES,:PM_CODREPRES_NOVO,:PM_CODLOJA,:PM_NUMDUPLI,:PM_NUMORDEM,:PM_DTFATURA,:PM_ERRO,:PM_MSGERRO)"
            vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
            
            If vErro <> "" Then
                Call vVB_Generica_001.Informar("Erro no processo de transfer�ncia das duplicatas  - " & vBanco.Parameters("PM_MSGERRO").Value & vbCrLf & " Entre em contato com o SUPORTE.")
                Exit Sub
            End If
        
            vObjOracle.MoveNext
        Next
        
        Dim dtDeslig As String
        
        If frmRepresentante.txtDt_Desligamento.Text = "" Then
            dtDeslig = Format(Date, "DD/MM/YYYY")
            frmRepresentante.txtDt_Desligamento.Text = Format(Date, "DD/MM/YYYY")
        Else
            dtDeslig = frmRepresentante.txtDt_Desligamento.Text
        End If
        
        vBanco.Parameters.Remove "PM_CODREPRES"
        vBanco.Parameters.Add "PM_CODREPRES", frmRepresentante.txtCod_Repres.Text, 1
        vBanco.Parameters.Remove "PM_DTDESLIG"
        vBanco.Parameters.Add "PM_DTDESLIG", dtDeslig, 1
        vBanco.Parameters.Remove "PM_ERRO"
        vBanco.Parameters.Add "PM_ERRO", 0, 2
        vBanco.Parameters.Remove "PM_MSGERRO"
        vBanco.Parameters.Add "PM_MSGERRO", "", 2
        
        
        vSql = "Producao.PCK_CAD040.PR_DESATIVA_REPRES(:PM_CODREPRES,:PM_DTDESLIG,:PM_ERRO,:PM_MSGERRO)"
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        
        If vErro <> "" Then
            Call vVB_Generica_001.Informar("Erro no processo de transfer�ncia das duplicatas  - " & vBanco.Parameters("PM_MSGERRO").Value & vbCrLf & " Entre em contato com o SUPORTE.")
            Exit Sub
        End If
        
        frmRepresentante.txtSituacao.Text = "DESATIVADO"
        frmRepresentante.txtSituacao.ForeColor = vbRed
        Set frmRepresentante.cmdSituacao.PictureNormal = LoadPicture(App.Path & "\ativo 32x32.ico")
        vSituacao = 9
        'frmRepresentante.txtDt_Desligamento.SetFocus
        
        MsgBox ("Duplicatas transferidas com sucesso")
        
        flFormDupl_Fechado = True
        Unload Me
        Unload frmDuplAberto
        
    End If

End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub

Private Sub txtCodRepres_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Numero(KeyAscii)
End Sub


Private Sub txtCodRepres_LostFocus()

    Criar_Cursor vBanco.Parameters, "vCursor"
    Set vObjOracle = vVB_Venda_001.TabelaRepresentante(vBanco, txtCodRepres.Text)
    
    If vObjOracle.EOF Then
        MsgBox ("C�digo inv�lido")
    Else
        txtPseudonimo.Text = vObjOracle.Fields(3).Value

        If vObjOracle.Fields(15).Value = 0 Then
            txtSituacao.Text = "ATIVO"
            txtSituacao.ForeColor = vbBlack
        Else
            txtSituacao.Text = "DESATIVADO"
            txtSituacao.ForeColor = vbRed
        End If
        
        txtDt_Desligamento.Text = Format(vObjOracle.Fields(19).Value, "dd/mm/yy")
        txtSegmento.Text = vObjOracle.Fields(24).Value & " - " & vObjOracle.Fields(25).Value
        txtFilial.Text = Format(vObjOracle.Fields(12).Value, "0000") & " - " & vObjOracle.Fields(39).Value
    End If
    
    If txtDt_Desligamento.Text <> "" Then
        lblMsg.Caption = "Este representante esta desligado da empresa"
        cmdTrocar.Enabled = False
    ElseIf txtSituacao.Text <> "ATIVO" Then
        lblMsg.Caption = "Este representante n�o esta ATIVO"
        cmdTrocar.Enabled = False
    ElseIf frmRepresentante.cmbSegmento.Text <> txtSegmento Then
        lblMsg.Caption = "Este representante n�o tem o mesmo segmento do atual"
        cmdTrocar.Enabled = False
    Else
        cmdTrocar.Enabled = True
        lblMsg.Caption = ""
    End If
    
End Sub
