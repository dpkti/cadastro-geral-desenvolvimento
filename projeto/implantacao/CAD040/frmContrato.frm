VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmContrato 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GERA��O DO CONTRATO"
   ClientHeight    =   3240
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4830
   Icon            =   "frmContrato.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   216
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   322
   StartUpPosition =   2  'CenterScreen
   Begin MSComDlg.CommonDialog CD 
      Left            =   3270
      Top             =   90
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.TextBox txtTestemunha2 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1485
      TabIndex        =   8
      Text            =   "FERNANDA APARECIDA CLAUDINO"
      Top             =   2475
      Width           =   3210
   End
   Begin VB.TextBox txtTestemunha1 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1485
      TabIndex        =   6
      Text            =   "CLARISSA MARIA VENDRAME POMPEU "
      Top             =   2070
      Width           =   3210
   End
   Begin VB.Frame fraTipo_Contrato 
      Caption         =   "Tipo de Contrato"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   645
      Left            =   135
      TabIndex        =   1
      Top             =   1350
      Width           =   4560
      Begin VB.OptionButton optTipo_Repres 
         Appearance      =   0  'Flat
         Caption         =   "Representante"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   135
         TabIndex        =   2
         Top             =   315
         Width           =   1590
      End
      Begin VB.OptionButton optTipo_Paac 
         Appearance      =   0  'Flat
         Caption         =   "Paac"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   2160
         TabIndex        =   3
         Top             =   315
         Width           =   825
      End
      Begin VB.OptionButton optTipo_Franquia 
         Appearance      =   0  'Flat
         Caption         =   "Franquia"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   3375
         TabIndex        =   4
         Top             =   315
         Width           =   1050
      End
   End
   Begin VB.TextBox txtCodigo 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3375
      TabIndex        =   0
      Top             =   945
      Width           =   915
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   13
      Top             =   2910
      Width           =   4830
      _ExtentX        =   8520
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   8467
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   11
      Top             =   810
      Width           =   4740
      _ExtentX        =   8361
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   4095
      TabIndex        =   10
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmContrato.frx":0CCA
      PICN            =   "frmContrato.frx":0CE6
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdGera_Arquivo 
      Height          =   690
      Left            =   45
      TabIndex        =   9
      TabStop         =   0   'False
      ToolTipText     =   "Gerar Arquivo"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmContrato.frx":19C0
      PICN            =   "frmContrato.frx":19DC
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label lblTestemunha2 
      Appearance      =   0  'Flat
      Caption         =   "Testemunha 2:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   135
      TabIndex        =   7
      Top             =   2520
      Width           =   1365
   End
   Begin VB.Label lblTestemunha1 
      Appearance      =   0  'Flat
      Caption         =   "Testemunha 1:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   135
      TabIndex        =   5
      Top             =   2115
      Width           =   1365
   End
   Begin VB.Label lblCodigo 
      Appearance      =   0  'Flat
      Caption         =   "C�d. Representante / Paac / Franquia:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   135
      TabIndex        =   12
      Top             =   990
      Width           =   3255
   End
End
Attribute VB_Name = "frmContrato"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdGera_Arquivo_Click()
  
    Dim val
    Dim tmp_conteudo As Variant
    Dim CODREPR As Integer
    Dim Nome_Arquivo As String
    
   '-- DEFINE O NOME DO ARQUIVO TXT
    If optTipo_Franquia.Value = True Then
        Nome_Arquivo = "Franquia.TXT"
    ElseIf optTipo_Paac.Value = True Then
        Nome_Arquivo = "Paac.TXT"
    ElseIf optTipo_Repres.Value = True Then
        Nome_Arquivo = "Repres.TXT"
    End If
    
    If Trim(txtCodigo) = "" Then
        MsgBox "Informe o c�digo.", vbInformation, "Aten��o"
        txtCodigo.SetFocus
        Exit Sub
    End If
    
   '-- SELECIONA OS DADOS PARA EMISS�O DO CONTRATO
    If optTipo_Franquia.Value = True Or optTipo_Paac.Value = True Then
       '-- VERIFICA O FRANQUEADOR DA FILIAL
        vBanco.Parameters.Remove "PM_CODFILIAL"
        vBanco.Parameters.Add "PM_CODFILIAL", txtCodigo.Text, 1
        
        Criar_Cursor vBanco.Parameters, "vCursor"

        vSql = "Producao.PCK_CAD040.PR_BUSCA_FRANQUEADOR(NULL ,:PM_CODFILIAL, :vCursor)"

        If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
            Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_FRANQUEADOR, entre em contato com o SUPORTE!")
            Exit Sub
        Else
            Set vObjOracle = vBanco.Parameters("VCURSOR").Value
        End If
           
        CODREPR = vObjOracle!cod_franqueador
    Else
        CODREPR = txtCodigo.Text
    End If
    
   '-- VERIFICA OS DADOS PARA MONTAGEM DO CONTRATO
    vBanco.Parameters.Remove "PM_CODREPRES"
    vBanco.Parameters.Add "PM_CODREPRES", CODREPR, 1
    
    Criar_Cursor vBanco.Parameters, "vCursor"
    
    vSql = "Producao.PCK_CAD040.PR_BUSCA_CONTRATO(:PM_CODREPRES, :vCursor)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_CONTRATO, entre em contato com o SUPORTE!")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("VCURSOR").Value
    End If
    
   '-- Formata data de cadastro (dt_cadstr)
    DATA_CADASTRO = Format(Day(vObjOracle!dt_cadastr), "00") & " de "
    Select Case Month(vObjOracle!dt_cadastr)
           Case 1: DATA_CADASTRO = DATA_CADASTRO & "Janeiro"
           Case 2: DATA_CADASTRO = DATA_CADASTRO & "Fevereiro"
           Case 3: DATA_CADASTRO = DATA_CADASTRO & "Mar�o"
           Case 4: DATA_CADASTRO = DATA_CADASTRO & "Abril"
           Case 5: DATA_CADASTRO = DATA_CADASTRO & "Maio"
           Case 6: DATA_CADASTRO = DATA_CADASTRO & "Junho"
           Case 7: DATA_CADASTRO = DATA_CADASTRO & "Julho"
           Case 8: DATA_CADASTRO = DATA_CADASTRO & "Agosto"
           Case 9: DATA_CADASTRO = DATA_CADASTRO & "Setembro"
           Case 10: DATA_CADASTRO = DATA_CADASTRO & "Outubro"
           Case 11: DATA_CADASTRO = DATA_CADASTRO & "Novembro"
           Case 12: DATA_CADASTRO = DATA_CADASTRO & "Dezembro"
    End Select

    DATA_CADASTRO = DATA_CADASTRO & " de " & Year(vObjOracle!dt_cadastr)
    
   '-- BUSCA RG DO REPRESENTANTE PARA MONTAGEM DO CONTRATO
    vBanco.Parameters.Remove "PM_CODREPRES"
    vBanco.Parameters.Add "PM_CODREPRES", CODREPR, 1
        
    Criar_Cursor vBanco.Parameters, "vCursor"
                    
    vSql = "PRODUCAO.PCK_CAD040.PR_BUSCA_PESSOAL(:PM_CODREPRES, :VCURSOR)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        vVB_Generica_001.Informar ("Erro no processamento, entre em contato com o SUPORTE!")
        Exit Sub
    Else
        Set vObjOracle2 = vBanco.Parameters("VCURSOR").Value
    End If
    
   'strMsg = "C:\TESTE\" & Nome_Arquivo
   'strMsg2 = "C:\TESTE\CONTRATO.TXT"
    strMsg = "h:\ORACLE\DADOS\CONTRATO\" & Nome_Arquivo
    strMsg2 = "h:\ORACLE\DADOS\CONTRATO\contrato.TXT"

    Open strMsg2 For Output As #2
    Open strMsg For Input As #1
    Line Input #1, tmp_linha
    tam_total = Len(tmp_linha)
    Do While Not EOF(1)
        If Len(tmp_linha) = 0 Then
            If Len(tmp_conteudo) > 2 Then
                Print #2, tmp_conteudo
            End If
            tmp_conteudo = ""
            Line Input #1, tmp_linha
            tam_total = Len(tmp_linha)
        End If
        tmp_i = InStr(tmp_linha, "#")
        If tmp_i <> 0 Then
            tmp_aux = Mid(tmp_linha, 1, tmp_i - 1)
            CONT = CONT + 1
            dado = CONT
            If CONT = 1 Then
                tmp_aux = tmp_aux & " " & vObjOracle!razao_social
            ElseIf CONT = 2 Then
                tmp_aux = tmp_aux & vObjOracle!CGC
            ElseIf CONT = 3 Then
                tmp_aux = tmp_aux & vObjOracle!ENDERECO & " - " & vObjOracle!bairro & " - " & _
                          vObjOracle!cidade & " - " & vObjOracle!uf
            ElseIf CONT = 4 Then
                tmp_aux = tmp_aux & vObjOracle!nome_repres
            ElseIf CONT = 5 Then
                tmp_aux = tmp_aux & vObjOracle2!RG
            ElseIf CONT = 6 Then
                tmp_aux = tmp_aux & vObjOracle!CIC
            ElseIf CONT = 7 Then
               '-- BUSCA CIDADE DOS CLIENTES DO REPRESENTANTE
                vBanco.Parameters.Remove "PM_CODREPRES"
                vBanco.Parameters.Add "PM_CODREPRES", CODREPR, 1
                    
                Criar_Cursor vBanco.Parameters, "vCursor"
                                
                vSql = "PRODUCAO.PCK_CAD040.PR_BUSCA_CIDADE_CLIENTE(:PM_CODREPRES, :VCURSOR)"
                
                If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
                    vVB_Generica_001.Informar ("Erro no processamento da PR_BUSCA_CIDADE_CLIENTE, entre em contato com o SUPORTE!")
                    Exit Sub
                Else
                    Set vObjOracle2 = vBanco.Parameters("VCURSOR").Value
                End If
                
                Do While Not vObjOracle2.EOF
                    tmp_aux = tmp_aux & vObjOracle2!nome_cidade & ", "
                    vObjOracle2.MoveNext
                Loop
            ElseIf CONT = 8 Then
                tmp_aux = tmp_aux & DATA_CADASTRO & vbCrLf
            ElseIf CONT = 9 Then
                tmp_aux = tmp_aux & vObjOracle!nome_repres
            ElseIf CONT = 10 Then
                tmp_aux = tmp_aux & frmContrato.txtTestemunha1 & vbCrLf
            ElseIf CONT = 11 Then
                tmp_aux = tmp_aux & frmContrato.txtTestemunha2 & vbCrLf
            End If
            tmp_conteudo = tmp_conteudo & tmp_aux
            tmp_linha = Mid(tmp_linha, tmp_i + 7, tam_total)
        Else
            tmp_conteudo = tmp_conteudo & tmp_linha & vbCrLf
            tmp_linha = ""
        End If
    Loop
    
    Call vVB_Generica_001.Informar("Fim da Gera��o do Contrato")
    
    txtCodigo.Text = ""
    txtCodigo.SetFocus
    
    Close #2
    Close #1
    
   val = Shell("C:\Arquivos de Programas\Microsoft Office\Office12\WINWORD.EXE /a h:\ORACLE\DADOS\CONTRATO\contrato.txt", 1)
   ' val = Shell("C:\Arquivos de programas\Microsoft Office\Office\WINWORD.EXE /a h:\ORACLE\DADOS\CONTRATO\contrato.txt", 1)

    'Printer.EndDoc
    Screen.MousePointer = 0
 
End Sub

Private Sub cmdVoltar_Click()
    Unload Me
End Sub



Private Sub txtCodigo_Click()

    txtCodigo.Text = ""

End Sub

Private Sub txtCodigo_KeyPress(KeyAscii As Integer)
    If KeyAscii < 47 Or KeyAscii > 58 Then
        If KeyAscii <> 8 Then
            KeyAscii = 0
        End If
    End If
End Sub

