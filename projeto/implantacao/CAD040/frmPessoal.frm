VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MsFlxGrd.ocx"
Begin VB.Form frmPessoal 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DADOS PESSOAIS"
   ClientHeight    =   8085
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10860
   Icon            =   "frmPessoal.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   539
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   724
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtCod_Repres_SAP 
      Appearance      =   0  'Flat
      BackColor       =   &H80000004&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   9420
      Locked          =   -1  'True
      TabIndex        =   49
      Top             =   945
      Width           =   1290
   End
   Begin VB.Frame fraFilhos 
      Caption         =   "Filhos"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   2850
      Left            =   90
      TabIndex        =   60
      Top             =   4860
      Width           =   7710
      Begin VB.TextBox txtDtNasc_Filhos 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1170
         TabIndex        =   38
         Top             =   675
         Width           =   1230
      End
      Begin VB.OptionButton optFeminino_Filhos 
         Appearance      =   0  'Flat
         Caption         =   "Feminino"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   3870
         TabIndex        =   40
         Top             =   765
         Value           =   -1  'True
         Width           =   1050
      End
      Begin VB.OptionButton optMasculino_Filhos 
         Appearance      =   0  'Flat
         Caption         =   "Masculino"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   5355
         TabIndex        =   41
         Top             =   765
         Width           =   1095
      End
      Begin VB.TextBox txtNome_Filhos 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1440
         TabIndex        =   36
         Top             =   270
         Width           =   5235
      End
      Begin Bot�o.cmd cmdGrava_Filhos 
         Height          =   690
         Left            =   6840
         TabIndex        =   42
         TabStop         =   0   'False
         ToolTipText     =   "Gravar"
         Top             =   270
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmPessoal.frx":0CCA
         PICN            =   "frmPessoal.frx":0CE6
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MSFlexGridLib.MSFlexGrid mfgFilhos_Cadastrados 
         Height          =   1455
         Left            =   135
         TabIndex        =   53
         Top             =   1305
         Width           =   7440
         _ExtentX        =   13123
         _ExtentY        =   2566
         _Version        =   393216
         Cols            =   5
         BackColorBkg    =   -2147483633
         HighLight       =   0
         ScrollBars      =   2
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         OLEDropMode     =   1
      End
      Begin VB.Label lblFilhos_Cadastrados 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Caption         =   "J� cadastrados:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   90
         TabIndex        =   52
         Top             =   1080
         Width           =   1320
      End
      Begin VB.Label lblSexo_Filhos 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Caption         =   "Sexo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   3105
         TabIndex        =   39
         Top             =   765
         Width           =   600
      End
      Begin VB.Label lblNome_Filhos 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Caption         =   "Nome Filho(a):"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   90
         TabIndex        =   35
         Top             =   315
         Width           =   1275
      End
      Begin VB.Label lblDtNasc_Filhos 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Caption         =   "Data Nasc.:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   90
         TabIndex        =   37
         Top             =   720
         Width           =   1005
      End
   End
   Begin VB.Frame fraFutebol 
      Caption         =   "Futebol"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1140
      Left            =   5355
      TabIndex        =   59
      Top             =   3645
      Width           =   5415
      Begin VB.ComboBox cmbUf_Futebol 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   675
         TabIndex        =   32
         Top             =   675
         Width           =   825
      End
      Begin VB.ComboBox cmbCidade_Futebol 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   2295
         TabIndex        =   34
         Top             =   675
         Width           =   3030
      End
      Begin VB.TextBox txtTime_Futebol 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   675
         TabIndex        =   30
         Top             =   270
         Width           =   4650
      End
      Begin VB.Label lblUf_Futebol 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Caption         =   "UF:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   225
         TabIndex        =   31
         Top             =   720
         Width           =   375
      End
      Begin VB.Label lblTime_Futebol 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Caption         =   "Time:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   90
         TabIndex        =   29
         Top             =   315
         Width           =   510
      End
      Begin VB.Label lblCidade_Futebol 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Caption         =   "Cidade:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   1620
         TabIndex        =   33
         Top             =   720
         Width           =   600
      End
   End
   Begin VB.Frame fraConjuge 
      Caption         =   "C�njuge"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1140
      Left            =   90
      TabIndex        =   58
      Top             =   3645
      Width           =   5145
      Begin VB.TextBox txtDtNasc_Conjuge 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1170
         TabIndex        =   28
         Top             =   675
         Width           =   1230
      End
      Begin VB.TextBox txtNome_Conjuge 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   765
         TabIndex        =   26
         Top             =   270
         Width           =   4245
      End
      Begin VB.Label lblDtNasc_Conjuge 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Caption         =   "Data Nasc.:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   90
         TabIndex        =   27
         Top             =   720
         Width           =   1005
      End
      Begin VB.Label lblNome_Conjuge 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Caption         =   "Nome:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   90
         TabIndex        =   25
         Top             =   315
         Width           =   600
      End
   End
   Begin VB.Frame fraEstado_Civil 
      Caption         =   "Estado Civil"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   690
      Left            =   2925
      TabIndex        =   57
      Top             =   2880
      Width           =   7845
      Begin VB.OptionButton optOutros 
         Appearance      =   0  'Flat
         Caption         =   "Outros"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   6660
         TabIndex        =   24
         Top             =   315
         Width           =   960
      End
      Begin VB.OptionButton optDivorciado 
         Appearance      =   0  'Flat
         Caption         =   "Divorciado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   3375
         TabIndex        =   22
         Top             =   315
         Width           =   1185
      End
      Begin VB.OptionButton optViuvo 
         Appearance      =   0  'Flat
         Caption         =   "Vi�vo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   5220
         TabIndex        =   23
         Top             =   315
         Width           =   825
      End
      Begin VB.OptionButton optCasado 
         Appearance      =   0  'Flat
         Caption         =   "Casado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   135
         TabIndex        =   20
         Top             =   315
         Value           =   -1  'True
         Width           =   1050
      End
      Begin VB.OptionButton optSolteiro 
         Appearance      =   0  'Flat
         Caption         =   "Solteiro"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   1710
         TabIndex        =   21
         Top             =   315
         Width           =   960
      End
   End
   Begin VB.Frame fraSexo 
      Caption         =   "Sexo"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   690
      Left            =   90
      TabIndex        =   56
      Top             =   2880
      Width           =   2715
      Begin VB.OptionButton optMasculino 
         Appearance      =   0  'Flat
         Caption         =   "Masculino"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   1530
         TabIndex        =   19
         Top             =   315
         Value           =   -1  'True
         Width           =   1095
      End
      Begin VB.OptionButton optFeminino 
         Appearance      =   0  'Flat
         Caption         =   "Feminino"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   135
         TabIndex        =   18
         Top             =   315
         Width           =   1050
      End
   End
   Begin VB.TextBox txtCod_Repres 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   810
      TabIndex        =   1
      Top             =   945
      Width           =   1140
   End
   Begin VB.TextBox txtPseudonimo 
      Appearance      =   0  'Flat
      BackColor       =   &H80000004&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   4230
      Locked          =   -1  'True
      TabIndex        =   47
      Top             =   945
      Width           =   3030
   End
   Begin VB.Frame fraPessoal 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1545
      Left            =   90
      TabIndex        =   55
      Top             =   1260
      Width           =   10680
      Begin VB.TextBox txtDt_Nascimento 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   5175
         TabIndex        =   5
         Top             =   270
         Width           =   1275
      End
      Begin VB.ComboBox cmbUf_Naturalidade 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1305
         TabIndex        =   9
         Top             =   675
         Width           =   825
      End
      Begin VB.TextBox txtEmail 
         Appearance      =   0  'Flat
         BackColor       =   &H80000004&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   4815
         Locked          =   -1  'True
         TabIndex        =   51
         Top             =   1080
         Width           =   4515
      End
      Begin VB.TextBox txtPeso 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   9720
         TabIndex        =   15
         Top             =   675
         Width           =   870
      End
      Begin VB.ComboBox cmbNaturalidade 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   3780
         TabIndex        =   11
         Top             =   675
         Width           =   3030
      End
      Begin VB.TextBox txtAltura 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   7875
         TabIndex        =   13
         Top             =   675
         Width           =   825
      End
      Begin VB.TextBox txtHobby 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1305
         TabIndex        =   17
         Top             =   1080
         Width           =   1905
      End
      Begin VB.TextBox txtNacionalidade 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   8460
         TabIndex        =   7
         Top             =   270
         Width           =   2130
      End
      Begin VB.TextBox txtRG 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1305
         TabIndex        =   3
         Top             =   270
         Width           =   1905
      End
      Begin VB.Label lblUf_Naturalidade 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Caption         =   "UF:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   855
         TabIndex        =   8
         Top             =   720
         Width           =   375
      End
      Begin VB.Label lblEmail 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Caption         =   "E-mail:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   3915
         TabIndex        =   50
         Top             =   1125
         Width           =   825
      End
      Begin VB.Label lblPeso 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Caption         =   "Peso:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   8820
         TabIndex        =   14
         Top             =   720
         Width           =   825
      End
      Begin VB.Label lblAltura 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Caption         =   "Altura:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   6975
         TabIndex        =   12
         Top             =   720
         Width           =   825
      End
      Begin VB.Label lblNaturalidade 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Caption         =   "Naturalidade:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   2565
         TabIndex        =   10
         Top             =   720
         Width           =   1140
      End
      Begin VB.Label lblDt_Nacimento 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Caption         =   "Data Nasc.:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   4095
         TabIndex        =   4
         Top             =   315
         Width           =   960
      End
      Begin VB.Label lblHobby 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Caption         =   "Hobby:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   360
         TabIndex        =   16
         Top             =   1080
         Width           =   825
      End
      Begin VB.Label lblNacionalidade 
         Appearance      =   0  'Flat
         Caption         =   "Nacionalidade:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   7245
         TabIndex        =   6
         Top             =   315
         Width           =   1230
      End
      Begin VB.Label lblRG 
         Appearance      =   0  'Flat
         Caption         =   "Num. RG:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   450
         TabIndex        =   2
         Top             =   315
         Width           =   825
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   61
      Top             =   7755
      Width           =   10860
      _ExtentX        =   19156
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   19103
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   54
      Top             =   810
      Width           =   10770
      _ExtentX        =   18997
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   10125
      TabIndex        =   45
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmPessoal.frx":19C0
      PICN            =   "frmPessoal.frx":19DC
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdCancelar 
      Height          =   690
      Left            =   810
      TabIndex        =   44
      TabStop         =   0   'False
      ToolTipText     =   "Cancelar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmPessoal.frx":26B6
      PICN            =   "frmPessoal.frx":26D2
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdGravar 
      Height          =   690
      Left            =   45
      TabIndex        =   43
      TabStop         =   0   'False
      ToolTipText     =   "Gravar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmPessoal.frx":2FAC
      PICN            =   "frmPessoal.frx":2FC8
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      Caption         =   "SAP"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   9000
      TabIndex        =   48
      Top             =   990
      Width           =   420
   End
   Begin VB.Label lblCod_Repres 
      Appearance      =   0  'Flat
      Caption         =   "C�digo:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   135
      TabIndex        =   0
      Top             =   990
      Width           =   645
   End
   Begin VB.Label lblPseudonimo 
      Appearance      =   0  'Flat
      Caption         =   "Pseudonimo:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   3105
      TabIndex        =   46
      Top             =   990
      Width           =   1095
   End
End
Attribute VB_Name = "frmPessoal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Dim cicCgc As String

Private Sub cmbUf_Futebol_Click()

   '-- CARREGA O COMBO DE CIDADE DO TIME DE FUTEBOL CONFORME A UF ESCOLHIDA
    Criar_Cursor vBanco.Parameters, "vCursor"
     
    Set vObjOracle = vVB_Generica_001.TabelaCidade(vBanco, , , , cmbUf_Futebol)
    Call vVB_Generica_001.PreencheComboList(vObjOracle, cmbCidade_Futebol, "C�D. CIDADE", "CIDADE")

End Sub

Private Sub cmbUf_Naturalidade_Click()

   '-- CARREGA O COMBO DE NATURALIDADE CONFORME A UF ESCOLHIDA
    Criar_Cursor vBanco.Parameters, "vCursor"
     
    Set vObjOracle = vVB_Generica_001.TabelaCidade(vBanco, , , , cmbUf_Naturalidade)
    Call vVB_Generica_001.PreencheComboList(vObjOracle, cmbNaturalidade, "C�D. CIDADE", "CIDADE")

End Sub

Private Sub cmdCancelar_Click()

    txtCod_Repres.Text = ""
    txtPseudonimo.Text = ""
    txtRG.Text = ""
    txtDt_Nascimento.Text = ""
    txtNacionalidade.Text = ""
    cmbUf_Naturalidade.Text = ""
    cmbNaturalidade.Text = ""
    txtAltura.Text = ""
    txtPeso.Text = ""
    txtHobby.Text = ""
    txtEmail.Text = ""
    optMasculino.Value = True
    optCasado.Value = True
    txtNome_Conjuge.Text = ""
    txtDtNasc_Conjuge.Text = ""
    txtTime_Futebol.Text = ""
    cmbUf_Futebol.Text = ""
    cmbCidade_Futebol.Text = ""
    txtNome_Filhos.Text = ""
    txtDtNasc_Filhos.Text = ""
    optFeminino_Filhos.Value = True
    
    Call vVB_Generica_001.LimpaFlexGridComTitulo(mfgFilhos_Cadastrados)
    
        
    ' fpallini - Ci&T - 21/09/12 - Limpando Campos Adicionais da Integra��o SAP.
    txtCod_Repres_SAP = ""
    
    txtCod_Repres.SetFocus
    
End Sub

Private Sub cmdGrava_Filhos_Click()

    If txtNome_Filhos.Text = "" Then
        MsgBox "N�o � poss�vel cadastrar, digite o nome do(a) filho(a)."
        Exit Sub
    ElseIf txtDtNasc_Filhos.Text = "" Then
        MsgBox "N�o � poss�vel cadastrar, digite a data de nascimento do(a) filho(a)."
        Exit Sub
    End If

   '-- INSERE OS DADOS DO FILHO(A)
    vBanco.Parameters.Remove "PM_CODREPRES"
    vBanco.Parameters.Add "PM_CODREPRES", txtCod_Repres.Text, 1
    vBanco.Parameters.Remove "PM_FILHO"
    vBanco.Parameters.Add "PM_FILHO", txtNome_Filhos.Text, 1
    
    If optFeminino_Filhos.Value = True Then
        vBanco.Parameters.Remove "PM_SEXO"
        vBanco.Parameters.Add "PM_SEXO", "F", 1
    Else
        vBanco.Parameters.Remove "PM_SEXO"
        vBanco.Parameters.Add "PM_SEXO", "M", 1
    End If
    
    vBanco.Parameters.Remove "PM_DTNASC"
    vBanco.Parameters.Add "PM_DTNASC", CDate(txtDtNasc_Filhos.Text), 1
    
    Criar_Cursor vBanco.Parameters, "vCursor"
    
    vSql = "Producao.PCK_CAD040.PR_ATUALIZA_REPRFILHOS(:PM_CODREPRES, " & _
           ":PM_FILHO, :PM_SEXO, :PM_DTNASC)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
        Exit Sub
    End If
    
    Call txtCod_Repres_LostFocus

End Sub

Private Sub cmdGravar_Click()
    Dim oRepresentante As ClienteSAP
    Dim msgErro As String

    If txtCod_Repres.Text = "" Then
        Call vVB_Generica_001.Informar("Digite o C�digo do Representante.")
        Exit Sub
        txtCod_Repres.SetFocus
    ElseIf txtRG.Text = "" Then
        Call vVB_Generica_001.Informar("Entre com o n�mero do RG.")
        Exit Sub
        txtRG.SetFocus
    ElseIf txtCod_Repres_SAP.Text = "" Then
        Call vVB_Generica_001.Informar("Representante sem n�mero SAP associado. Verificar cadastro na tela principal.")
        Exit Sub
    End If
        
    'Kevlin Toshinari Ossada - Ci&T - 10/9/2012 - Busca Cliente no SAP
    If vVB_Generica_001.ValidaCIC(cicCgc) Then
        Set oRepresentante = BuscarClienteSAP(2, cicCgc, msgErro)
    ElseIf vVB_Generica_001.ValidaCGC(cicCgc) Then
        Set oRepresentante = BuscarClienteSAP(1, cicCgc, msgErro)
    Else
        MsgBox "CPF ou CNPJ inv�lido", vbCritical, "CAD040 - Aten��o"
        Exit Sub
    End If
    
    If msgErro <> "" Then
        MsgBox msgErro
        Exit Sub
    End If

    'Kevlin Toshinari Ossada - Ci&T - 11/9/2012 - Seta propriedade data de nascimento
    If txtDt_Nascimento.Text = "" Then
        Call vVB_Generica_001.Informar("A data de nascimento � obrigat�ria.")
        Exit Sub
    ElseIf Len(txtDt_Nascimento.Text) <> 8 Then
        Call vVB_Generica_001.Informar("A data de nascimento est� inv�lida.")
        Exit Sub
    Else
        oRepresentante.DtNascimento = CDate(txtDt_Nascimento.Text)
    End If
    
    'Kevlin Toshinari Ossada - Ci&T - 11/9/2012 - Tenta enviar cliente para o SAP
    If Not EnviarClienteSAP(oRepresentante) Then
        Exit Sub
    End If
    

   '-- ATUALIZA DADOS PESSOAIS
    vBanco.Parameters.Remove "PM_CODREPRES"
    vBanco.Parameters.Add "PM_CODREPRES", txtCod_Repres.Text, 1
    vBanco.Parameters.Remove "PM_NACIONALIDADE"
    vBanco.Parameters.Add "PM_NACIONALIDADE", txtNacionalidade.Text, 1
    vBanco.Parameters.Remove "PM_CIDADE"
    vBanco.Parameters.Add "PM_CIDADE", val(cmbNaturalidade.Text), 1
    vBanco.Parameters.Remove "PM_RG"
    vBanco.Parameters.Add "PM_RG", txtRG.Text, 1
    vBanco.Parameters.Remove "PM_DTNASC"
    vBanco.Parameters.Add "PM_DTNASC", CDate(txtDt_Nascimento.Text), 1, 12
    
    If optFeminino.Value = True Then
        vBanco.Parameters.Remove "PM_SEXO"
        vBanco.Parameters.Add "PM_SEXO", "F", 1
    Else
        vBanco.Parameters.Remove "PM_SEXO"
        vBanco.Parameters.Add "PM_SEXO", "M", 1
    End If
            
    If optCasado.Value = True Then
        vBanco.Parameters.Remove "PM_ESTCIVIL"
        vBanco.Parameters.Add "PM_ESTCIVIL", "C", 1
    ElseIf optSolteiro.Value = True Then
        vBanco.Parameters.Remove "PM_ESTCIVIL"
        vBanco.Parameters.Add "PM_ESTCIVIL", "S", 1
    ElseIf optDivorciado.Value = True Then
        vBanco.Parameters.Remove "PM_ESTCIVIL"
        vBanco.Parameters.Add "PM_ESTCIVIL", "D", 1
    ElseIf optViuvo.Value = True Then
        vBanco.Parameters.Remove "PM_ESTCIVIL"
        vBanco.Parameters.Add "PM_ESTCIVIL", "V", 1
    Else
        vBanco.Parameters.Remove "PM_ESTCIVIL"
        vBanco.Parameters.Add "PM_ESTCIVIL", "O", 1
    End If
    
    vBanco.Parameters.Remove "PM_CONJUGE"
    vBanco.Parameters.Add "PM_CONJUGE", Trim(txtNome_Conjuge.Text), 1
    
    vBanco.Parameters.Remove "PM_DTNASC_CONJUGE"
    vBanco.Parameters.Add "PM_DTNASC_CONJUGE", IIf(Trim(txtDtNasc_Conjuge.Text) = "", Null, Trim(txtDtNasc_Conjuge.Text)), 1, 12
    vBanco.Parameters.Remove "PM_HOBBY"
    vBanco.Parameters.Add "PM_HOBBY", Trim(txtHobby.Text), 1
    vBanco.Parameters.Remove "PM_TIME"
    vBanco.Parameters.Add "PM_TIME", Trim(txtTime_Futebol.Text), 1
    vBanco.Parameters.Remove "PM_CIDADE_TIME"
    vBanco.Parameters.Add "PM_CIDADE_TIME", IIf(val(cmbCidade_Futebol.Text) = 0, Null, val(cmbCidade_Futebol.Text)), 1
    vBanco.Parameters.Remove "PM_ALTURA"
    vBanco.Parameters.Add "PM_ALTURA", Trim(txtAltura.Text), 1
    vBanco.Parameters.Remove "PM_PESO"
    vBanco.Parameters.Add "PM_PESO", Trim(txtPeso.Text), 1
    
    Criar_Cursor vBanco.Parameters, "vCursor"
    
    vSql = "Producao.PCK_CAD040.PR_ATUALIZA_INFPESSOAL(:PM_CODREPRES, " & _
           ":PM_NACIONALIDADE, :PM_CIDADE, :PM_RG, to_date(:PM_DTNASC, 'dd/mm/rr'), :PM_SEXO, " & _
           ":PM_ESTCIVIL, :PM_CONJUGE, to_date(:PM_DTNASC_CONJUGE, 'dd/mm/rr'), :PM_HOBBY, " & _
           ":PM_TIME, :PM_CIDADE_TIME, :PM_ALTURA, :PM_PESO)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
        Exit Sub
    Else
        Call vVB_Generica_001.Informar("Grava��o conclu�da com sucesso!")
        Call cmdCancelar_Click
    End If

End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()

    'Kevlin Toshinari Ossada
    Set vVB_Venda_001 = New clsVB_Venda_001
    'Set VB_Generica_001 = New clsVB_Generica_001
    
   '-- CARREGA OS COMBOS DE UF DA NATURALIDADE E UF DO TIME DE FUTEBOL
    Criar_Cursor vBanco.Parameters, "vCursor"
     
    Set vObjOracle = vVB_Generica_001.TabelaUF(vBanco)
    Call vVB_Generica_001.PreencheComboList(vObjOracle, cmbUf_Naturalidade, "UF")
    
    vObjOracle.MoveFirst
    Call vVB_Generica_001.PreencheComboList(vObjOracle, cmbUf_Futebol, "UF")
    
     'carlosj CiT (25/09/2012) - correcao [ISA-632] - caso a conexao do legado nao esteja OK
     'os parametros ja estarao carregados
     Call oServicoClienteSAP.CarregarParametrosSistema

End Sub

Private Sub txtCod_Repres_Click()

    Call cmdCancelar_Click

End Sub

Private Sub txtCod_Repres_LostFocus()
    Dim vCIC As Long

   '-- ATUALIZA OS CAMPOS DE DADOS PESSOAIS
    If txtCod_Repres.Text <> "" Then
        Criar_Cursor vBanco.Parameters, "vCursor"
        
        Set vObjOracle = vVB_Venda_001.TabelaRepresentante(vBanco, txtCod_Repres.Text)
        
        If Not vObjOracle.EOF Then
            txtPseudonimo.Text = vObjOracle.Fields(3)
            
            'Kevlin Toshinari Ossada - Ci&T - 11/9/2012 - Carregar o cod sap no campo
            vBanco.Parameters.Remove "PM_COD_REPRES"
            vBanco.Parameters.Add "PM_COD_REPRES", CInt(txtCod_Repres.Text), 1
            
            Criar_Cursor vBanco.Parameters, "vCursor"
            
            vSql = "PRODUCAO.PCK_CAD040.PR_BUSCA_COD_SAP_REPRESENTANTE(:PM_COD_REPRES, :VCURSOR)"
            
            If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
                vVB_Generica_001.Informar ("Erro no processamento, entre em contato com o SUPORTE!")
                Exit Sub
            Else
                Set vObjOracle2 = vBanco.Parameters("VCURSOR").Value
                If Not vObjOracle2.EOF Then
                    txtCod_Repres_SAP.Text = IIf(IsNull(vObjOracle2!COD_SAP.Value), "", vObjOracle2!COD_SAP.Value)
                    cicCgc = IIf(IsNull(vObjOracle2!CIC.Value), "", vObjOracle2!CIC.Value)
                End If
            End If
                
            If txtCod_Repres_SAP.Text = "" Then
                Call vVB_Generica_001.Informar("Representante sem n�mero SAP associado. Verificar cadastro na tela principal.")
                Call cmdCancelar_Click
                Exit Sub
            End If
        
            vBanco.Parameters.Remove "PM_CODREPRES"
            vBanco.Parameters.Add "PM_CODREPRES", txtCod_Repres.Text, 1
                
            Criar_Cursor vBanco.Parameters, "vCursor"
                            
            vSql = "PRODUCAO.PCK_CAD040.PR_BUSCA_PESSOAL(:PM_CODREPRES, :VCURSOR)"
            
            If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
                vVB_Generica_001.Informar ("Erro no processamento, entre em contato com o SUPORTE!")
                Exit Sub
            Else
                Set vObjOracle = vBanco.Parameters("VCURSOR").Value
            End If
        
            If Not vObjOracle.EOF Then
                If vObjOracle!Sexo.Value = "M" Then
                    optMasculino.Value = True
                Else
                    optFeminino.Value = True
                End If
                
                If vObjOracle!ESTADO_CIVIL.Value = "C" Then
                    optCasado.Value = True
                ElseIf vObjOracle!ESTADO_CIVIL.Value = "S" Then
                    optSolteiro.Value = True
                ElseIf vObjOracle!ESTADO_CIVIL.Value = "D" Then
                    optDivorciado.Value = True
                ElseIf vObjOracle!ESTADO_CIVIL.Value = "V" Then
                    optViuvo.Value = True
                ElseIf vObjOracle!ESTADO_CIVIL.Value = "O" Then
                    optOutros.Value = True
                End If
                
                txtRG.Text = vObjOracle!RG.Value
                txtDt_Nascimento.Text = Format(vObjOracle!Dt_Nasc.Value, "dd/mm/yy")
                txtNacionalidade.Text = vObjOracle!nacionalidade.Value
                                 
                Set vObjOracle2 = vVB_Generica_001.TabelaCidade(vBanco, vObjOracle!NATURALIDADE.Value)
                
                cmbNaturalidade.Text = vObjOracle!NATURALIDADE.Value & " - " & vObjOracle2!cidade.Value
                
                cmbUf_Naturalidade.Text = vObjOracle!UF_NASC.Value
                txtNome_Conjuge.Text = vObjOracle!conjuge.Value
                txtDtNasc_Conjuge.Text = Format(vObjOracle!dt_nasc_conj.Value, "dd/mm/yy")
                txtHobby.Text = vObjOracle!hobby.Value
                txtTime_Futebol.Text = vObjOracle!Time.Value
                
                If vObjOracle!CID_TIME.Value <> 0 Then
                    Set vObjOracle2 = vVB_Generica_001.TabelaCidade(vBanco, vObjOracle!CID_TIME.Value)
                
                    cmbCidade_Futebol.Text = vObjOracle!CID_TIME.Value & " - " & vObjOracle2!cidade.Value
                End If
                
                cmbUf_Futebol.Text = vObjOracle!UF_TIME.Value
                
                txtAltura.Text = vObjOracle!altura.Value
                txtPeso.Text = vObjOracle!Peso.Value
                
               '-- BUSCA E-MAIL NA TABELA HELPDESK.USUARIO - CADASTRO DA INTRANET
                Criar_Cursor vBanco.Parameters, "vCursor"
                                
                vBanco.Parameters.Remove "PM_CODREPRES"
                vBanco.Parameters.Add "PM_CODREPRES", txtCod_Repres.Text, 1

                vSql = "PRODUCAO.PCK_CAD040.PR_BUSCA_EMAIL(:PM_CODREPRES, :VCURSOR)"
                    
                If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
                    vVB_Generica_001.Informar ("Erro no processamento, entre em contato com o SUPORTE!")
                    Exit Sub
                Else
                    Set vObjOracle = vBanco.Parameters("VCURSOR").Value
                End If
                    
                txtEmail.Text = IIf(IsNull(vObjOracle!E_MAIL), "", vObjOracle!E_MAIL)
            End If
        Else
            txtPseudonimo.SetFocus
        End If
           
       '-- ATUALIZA OS CAMPOS COM DADOS DOS FILHOS
        vBanco.Parameters.Remove "PM_CODREPRES"
        vBanco.Parameters.Add "PM_CODREPRES", txtCod_Repres.Text, 1
            
        Criar_Cursor vBanco.Parameters, "vCursor"
                        
        vSql = "PRODUCAO.PCK_CAD040.PR_BUSCA_FILHOS(:PM_CODREPRES, :VCURSOR)"
        
        If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
            vVB_Generica_001.Informar ("Erro no processamento, entre em contato com o SUPORTE!")
            Exit Sub
        Else
            Set vObjOracle = vBanco.Parameters("VCURSOR").Value
        End If
        
       '-- CARREGA O GRID
        Call vVB_Generica_001.CarregaGridTabela(mfgFilhos_Cadastrados, vObjOracle, 5, "1;2;3;4")
    
    End If
    
End Sub

Private Sub txtDt_Nascimento_KeyPress(KeyAscii As Integer)
    
    Call vVB_Generica_001.Data(KeyAscii, txtDt_Nascimento)

End Sub

Private Sub txtDtNasc_Conjuge_KeyPress(KeyAscii As Integer)
    
    Call vVB_Generica_001.Data(KeyAscii, txtDtNasc_Conjuge)

End Sub

Private Sub txtDtNasc_Filhos_KeyPress(KeyAscii As Integer)
    
    Call vVB_Generica_001.Data(KeyAscii, txtDtNasc_Filhos)

End Sub

Private Sub txtHobby_KeyPress(KeyAscii As Integer)
    
    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)

End Sub

Private Sub txtNacionalidade_KeyPress(KeyAscii As Integer)
    
    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)

End Sub

Private Sub txtNome_Conjuge_KeyPress(KeyAscii As Integer)
    
    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)

End Sub

Private Sub txtNome_Filhos_KeyPress(KeyAscii As Integer)
    
    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)

End Sub

Private Sub txtTime_Futebol_KeyPress(KeyAscii As Integer)
    
    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)

End Sub
