VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmDocumentos 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "TIPOS DE DOCUMENTOS"
   ClientHeight    =   6465
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5985
   Icon            =   "frmDocumentos.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   431
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   399
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtEntregue 
      Appearance      =   0  'Flat
      BackColor       =   &H80000004&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1575
      Locked          =   -1  'True
      TabIndex        =   8
      Top             =   5760
      Width           =   4245
   End
   Begin VB.TextBox txtCPF 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1035
      TabIndex        =   5
      Top             =   1350
      Width           =   1950
   End
   Begin VB.TextBox txtPseudonimo 
      Appearance      =   0  'Flat
      BackColor       =   &H80000004&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   2385
      Locked          =   -1  'True
      TabIndex        =   3
      Top             =   945
      Width           =   3435
   End
   Begin VB.TextBox txtCod_Repres 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1035
      TabIndex        =   1
      Top             =   945
      Width           =   1005
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   11
      Top             =   6135
      Width           =   5985
      _ExtentX        =   10557
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   10504
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   10
      Top             =   810
      Width           =   5865
      _ExtentX        =   10345
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   5220
      TabIndex        =   9
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmDocumentos.frx":23D2
      PICN            =   "frmDocumentos.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSFlexGridLib.MSFlexGrid mfgDocumentos 
      Height          =   3840
      Left            =   765
      TabIndex        =   6
      Top             =   1800
      Width           =   4425
      _ExtentX        =   7805
      _ExtentY        =   6773
      _Version        =   393216
      Rows            =   13
      BackColorBkg    =   -2147483633
      AllowUserResizing=   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblEntregue 
      Appearance      =   0  'Flat
      Caption         =   "Doc. Entregues:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   135
      TabIndex        =   7
      Top             =   5805
      Width           =   1410
   End
   Begin VB.Label lblCPF 
      Appearance      =   0  'Flat
      Caption         =   "N�m.CPF:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   135
      TabIndex        =   4
      Top             =   1395
      Width           =   870
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      Caption         =   "-"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   2160
      TabIndex        =   2
      Top             =   990
      Width           =   105
   End
   Begin VB.Label lblCod_Repres 
      Appearance      =   0  'Flat
      Caption         =   "C�digo:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   315
      TabIndex        =   0
      Top             =   990
      Width           =   690
   End
End
Attribute VB_Name = "frmDocumentos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdCancelar_Click()

    txtCod_Repres.Text = ""
    txtPseudonimo.Text = ""
    txtCPF.Text = ""
    txtEntregue.Text = ""
    
    Call vVB_Generica_001.LimpaFlexGridComTitulo(mfgDocumentos)
    
    txtCod_Repres.SetFocus
    
End Sub

Private Sub cmdVoltar_Click()

    Unload Me
    
    frmRepresentante.cmdGravar.SetFocus

End Sub

Private Sub Form_Activate()
    txtCod_Repres.Enabled = False
    txtPseudonimo.Enabled = False
    txtCPF.Enabled = False
    
    Call txtCod_Repres_LostFocus
End Sub

Private Sub mfgDocumentos_DblClick()
    mfgDocumentos.Col = 1
    
   '-- VERIFICA SE O DOCUMENTO QUE EST� SENDO GRAVADO J� EXISTE NO BANCO
   '-- PARA O REPRESENTANTE EM QUEST�O
    vBanco.Parameters.Remove "PM_CIC"
    vBanco.Parameters.Add "PM_CIC", txtCPF.Text, 1
    vBanco.Parameters.Remove "PM_TIPO"
    vBanco.Parameters.Add "PM_TIPO", val(mfgDocumentos.Text), 1
    
    Criar_Cursor vBanco.Parameters, "vCursor"

    vSql = "Producao.PCK_CAD040.PR_BUSCA_REPRES_DOCUMENTOS(:PM_CIC, :PM_TIPO, :vCursor)"

    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("VCURSOR").Value
    End If

   '-- SE N�O EXISTIR AINDA ESTE REGISTRO, FAZ O INSERT NA TABELA
    If vObjOracle.EOF Then
        vBanco.Parameters.Remove "PM_CIC"
        vBanco.Parameters.Add "PM_CIC", txtCPF.Text, 1
        vBanco.Parameters.Remove "PM_TIPO"
        vBanco.Parameters.Add "PM_TIPO", mfgDocumentos.Text, 1
        
        Criar_Cursor vBanco.Parameters, "vCursor"
        
        vSql = "Producao.PCK_CAD040.PR_ATUALIZA_TIPO_DOCTO(:PM_CIC, :PM_TIPO)"
        
        If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
            Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
            Exit Sub
        End If
        
        If txtEntregue.Text = "" Then
            txtEntregue.Text = Format(mfgDocumentos.Text, "00")
        Else
            txtEntregue.Text = txtEntregue.Text & "," & Format(mfgDocumentos.Text, "00")
        End If
    Else
        If vVB_Generica_001.Perguntar("Registro j� existente! Deseja excluir?") = vbYes Then
            vBanco.Parameters.Remove "PM_CIC"
            vBanco.Parameters.Add "PM_CIC", txtCPF.Text, 1
            vBanco.Parameters.Remove "PM_TIPO"
            vBanco.Parameters.Add "PM_TIPO", mfgDocumentos.Text, 1
            
            Criar_Cursor vBanco.Parameters, "vCursor"
            
            vSql = "Producao.PCK_CAD040.PR_EXCLUI_DOCTO_REPRES(:PM_CIC, :PM_TIPO)"
            
            If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
                Call vVB_Generica_001.Informar("Erro no processamento da PR_EXCLUI_DOCTO_REPRES, entre em contato com o SUPORTE!")
                Exit Sub
            Else
                Call vVB_Generica_001.Informar("Documento exclu�do com sucesso!")
                Call txtCod_Repres_LostFocus
            End If
        End If
    End If

End Sub

Private Sub txtCod_Repres_LostFocus()
    txtEntregue.Text = ""
   
   '-- TRAZ TODOS OS DOCUMENTOS CADASTRADOS NO BANCO E CARREGA O GRID
    Criar_Cursor vBanco.Parameters, "vCursor"

    vSql = "Producao.PCK_CAD040.PR_BUSCA_TIPO_DOCUMENTOS(:vCursor)"

    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("VCURSOR").Value
    End If
    
   '-- CARREGA O GRID
    Call vVB_Generica_001.CarregaGridTabela(mfgDocumentos, vObjOracle, 3)
        
   '-- TRAZ TODOS OS DOCUMENTOS J� CADASTRADOS NO BANCO PARA O REPRESENTANTE
   '-- QUE EST� SENDO CONSULTADO
    vBanco.Parameters.Remove "PM_CIC"
    vBanco.Parameters.Add "PM_CIC", txtCPF.Text, 1
    
    Criar_Cursor vBanco.Parameters, "vCursor"

    vSql = "Producao.PCK_CAD040.PR_BUSCA_REPRES_DOCUMENTOS(:PM_CIC,NULL,:vCursor)"

    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("VCURSOR").Value
    End If
        
    Do While Not vObjOracle.EOF
        If txtEntregue.Text = "" Then
            txtEntregue.Text = Format(vObjOracle!cod_tipo, "00")
        Else
        txtEntregue.Text = txtEntregue.Text & "," & Format(vObjOracle!cod_tipo, "00")
        End If
        vObjOracle.MoveNext
    Loop

End Sub
