VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form frmRepresentacao 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DADOS DA REPRESENTA��O"
   ClientHeight    =   7365
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7920
   Icon            =   "frmRepresentacao.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   7365
   ScaleWidth      =   7920
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ListView lsvRepresentantes 
      Height          =   1335
      Left            =   30
      TabIndex        =   46
      Top             =   5610
      Width           =   7845
      _ExtentX        =   13838
      _ExtentY        =   2355
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "C�digo"
         Object.Width           =   2469
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Representante"
         Object.Width           =   5292
      EndProperty
   End
   Begin VB.TextBox txtSituacao 
      Appearance      =   0  'Flat
      BackColor       =   &H80000004&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   5850
      Locked          =   -1  'True
      TabIndex        =   41
      Top             =   1350
      Width           =   1950
   End
   Begin VB.Frame fraConta 
      Caption         =   "Conta Corrente"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   645
      Left            =   90
      TabIndex        =   27
      Top             =   4635
      Width           =   4560
      Begin VB.TextBox txtDig_Agencia 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1620
         TabIndex        =   30
         Top             =   225
         Width           =   375
      End
      Begin VB.TextBox txtAgencia 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   855
         TabIndex        =   29
         Top             =   225
         Width           =   735
      End
      Begin VB.TextBox txtDig_Conta 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   4095
         TabIndex        =   33
         Top             =   225
         Width           =   375
      End
      Begin VB.TextBox txtConta 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2925
         TabIndex        =   32
         Top             =   225
         Width           =   1140
      End
      Begin VB.Label lblAgencia 
         Appearance      =   0  'Flat
         Caption         =   "Ag�ncia:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   90
         TabIndex        =   28
         Top             =   270
         Width           =   735
      End
      Begin VB.Label lblConta 
         Appearance      =   0  'Flat
         Caption         =   "N� C/C:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   2250
         TabIndex        =   31
         Top             =   270
         Width           =   645
      End
   End
   Begin VB.TextBox txtCxPostal 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   4365
      TabIndex        =   26
      Top             =   4185
      Width           =   1455
   End
   Begin VB.TextBox txtCEP 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1455
      TabIndex        =   24
      Top             =   4185
      Width           =   1455
   End
   Begin VB.ComboBox cmbUF 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   1455
      TabIndex        =   20
      Top             =   3780
      Width           =   915
   End
   Begin VB.ComboBox cmbCidade 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   3510
      TabIndex        =   22
      Top             =   3780
      Width           =   4290
   End
   Begin VB.TextBox txtBairro 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1455
      TabIndex        =   18
      Top             =   3375
      Width           =   2580
   End
   Begin VB.TextBox txtEndereco 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1455
      TabIndex        =   16
      Top             =   2970
      Width           =   5235
   End
   Begin VB.TextBox txtCelular 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   6435
      TabIndex        =   14
      Top             =   2565
      Width           =   1365
   End
   Begin VB.TextBox txtDDD 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1455
      TabIndex        =   9
      Top             =   2565
      Width           =   465
   End
   Begin VB.TextBox txtFone 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1950
      TabIndex        =   10
      Top             =   2565
      Width           =   1365
   End
   Begin VB.TextBox txtCGC 
      Appearance      =   0  'Flat
      BackColor       =   &H80000018&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1455
      TabIndex        =   3
      ToolTipText     =   "Digite o CGC para buscar os dados da representa��o!"
      Top             =   1350
      Width           =   2535
   End
   Begin VB.TextBox txtNum_Cori 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1455
      TabIndex        =   7
      Top             =   2160
      Width           =   1905
   End
   Begin VB.TextBox txtRazao_Social 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1455
      TabIndex        =   5
      Top             =   1755
      Width           =   6345
   End
   Begin VB.ComboBox cmbCGC 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   1455
      TabIndex        =   43
      Top             =   1350
      Visible         =   0   'False
      Width           =   2580
   End
   Begin VB.TextBox txtFax 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3960
      TabIndex        =   12
      Top             =   2565
      Width           =   1365
   End
   Begin VB.TextBox txtPseudonimo 
      Appearance      =   0  'Flat
      BackColor       =   &H80000004&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   4635
      Locked          =   -1  'True
      TabIndex        =   39
      Top             =   945
      Width           =   3165
   End
   Begin VB.TextBox txtCod_Repres 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1455
      TabIndex        =   1
      Top             =   945
      Width           =   1140
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   44
      Top             =   7035
      Width           =   7920
      _ExtentX        =   13970
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   13917
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   42
      Top             =   810
      Width           =   7800
      _ExtentX        =   13758
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   7155
      TabIndex        =   37
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmRepresentacao.frx":29C12
      PICN            =   "frmRepresentacao.frx":29C2E
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdCancelar 
      Height          =   690
      Left            =   1575
      TabIndex        =   36
      TabStop         =   0   'False
      ToolTipText     =   "Cancelar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmRepresentacao.frx":2A908
      PICN            =   "frmRepresentacao.frx":2A924
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdGravar 
      Height          =   690
      Left            =   810
      TabIndex        =   35
      TabStop         =   0   'False
      ToolTipText     =   "Gravar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmRepresentacao.frx":2B1FE
      PICN            =   "frmRepresentacao.frx":2B21A
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdSituacao 
      Height          =   690
      Left            =   45
      TabIndex        =   34
      TabStop         =   0   'False
      ToolTipText     =   "Ativar/Desativar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmRepresentacao.frx":2BEF4
      PICN            =   "frmRepresentacao.frx":2BF10
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdExcluirRepresentante 
      Height          =   690
      Left            =   7170
      TabIndex        =   45
      TabStop         =   0   'False
      ToolTipText     =   "Excluir Representante da Representa��o"
      Top             =   4890
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmRepresentacao.frx":2C22A
      PICN            =   "frmRepresentacao.frx":2C246
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "Representantes da Representa��o:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   30
      TabIndex        =   47
      Top             =   5400
      Width           =   2880
   End
   Begin VB.Label lblSituacao 
      Appearance      =   0  'Flat
      Caption         =   "Situa��o:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   5040
      TabIndex        =   40
      Top             =   1395
      Width           =   825
   End
   Begin VB.Label lblCxPostal 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      Caption         =   "Cx. Postal:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   3330
      TabIndex        =   25
      Top             =   4230
      Width           =   960
   End
   Begin VB.Label lblCEP 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "CEP:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   975
      TabIndex        =   23
      Top             =   4230
      Width           =   375
   End
   Begin VB.Label lblUF 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "UF:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   1080
      TabIndex        =   19
      Top             =   3825
      Width           =   270
   End
   Begin VB.Label lblCidade 
      Appearance      =   0  'Flat
      Caption         =   "Cidade:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   2835
      TabIndex        =   21
      Top             =   3825
      Width           =   645
   End
   Begin VB.Label lblBairro 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "Bairro:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   840
      TabIndex        =   17
      Top             =   3420
      Width           =   510
   End
   Begin VB.Label lblEndereco 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "Endere�o:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   510
      TabIndex        =   15
      Top             =   3015
      Width           =   840
   End
   Begin VB.Label lblCelular 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      Caption         =   "Celular:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   5715
      TabIndex        =   13
      Top             =   2610
      Width           =   645
   End
   Begin VB.Label lblFone 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "Fone:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   885
      TabIndex        =   8
      Top             =   2610
      Width           =   465
   End
   Begin VB.Label lblCGC 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "Num. CGC:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   465
      TabIndex        =   2
      Top             =   1395
      Width           =   885
   End
   Begin VB.Label lblNum_Cori 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "Inscr.Prefeitura:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   45
      TabIndex        =   6
      Top             =   2205
      Width           =   1305
   End
   Begin VB.Label lblRazao_Social 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "Raz�o Social:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   315
      TabIndex        =   4
      Top             =   1800
      Width           =   1035
   End
   Begin VB.Label lblFax 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      Caption         =   "Fax:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   3510
      TabIndex        =   11
      Top             =   2610
      Width           =   375
   End
   Begin VB.Label lblPseudonimo 
      Appearance      =   0  'Flat
      Caption         =   "Pseudonimo:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   3510
      TabIndex        =   38
      Top             =   990
      Width           =   1095
   End
   Begin VB.Label lblCod_Repres 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "C�digo:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   735
      TabIndex        =   0
      Top             =   990
      Width           =   615
   End
End
Attribute VB_Name = "frmRepresentacao"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmbCGC_Click()
     txtNum_Cori.Text = ""
     txtRazao_Social.Text = ""
     txtDDD.Text = ""
     txtFone.Text = ""
     txtFax.Text = ""
     txtCelular.Text = ""
     txtEndereco.Text = ""
     txtBairro.Text = ""
     txtCEP.Text = ""
     txtCxPostal.Text = ""
     txtAgencia.Text = ""
     txtDig_Agencia.Text = ""
     txtConta.Text = ""
     txtDig_Conta.Text = ""
    
    '-- BUSCA OS DADOS ATRAV�S DO CGC DA EMPRESA DO REPRESENTANTE
     Criar_Cursor vBanco.Parameters, "vCursor"
     
     Set vObjOracle = vVB_Venda_001.TabelaRepresentacao(vBanco, , , cmbCGC.Text)
     
     txtNum_Cori.Text = IIf(IsNull(vObjOracle.Fields(2).Value), "", vObjOracle.Fields(2).Value)
     txtRazao_Social.Text = vObjOracle.Fields(1).Value
     txtDDD.Text = IIf(IsNull(vObjOracle.Fields(7).Value), "", vObjOracle.Fields(7).Value)
     txtFone.Text = IIf(IsNull(vObjOracle.Fields(8).Value), "", vObjOracle.Fields(8).Value)
     txtFax.Text = IIf(IsNull(vObjOracle.Fields(9).Value), "", vObjOracle.Fields(9).Value)
     txtCelular.Text = IIf(IsNull(vObjOracle.Fields(10).Value), "", vObjOracle.Fields(10).Value)
     txtEndereco.Text = vObjOracle.Fields(3).Value
     txtBairro.Text = vObjOracle.Fields(4).Value
     
     '-- BUSCA O NOME DA CIDADE E UF
     Criar_Cursor vBanco.Parameters, "vCursor"
     
     Set vObjOracle2 = vVB_Generica_001.TabelaCidade(vBanco, vObjOracle.Fields(5).Value)
     
     cmbCidade.Text = vObjOracle2.Fields(0).Value & " - " & vObjOracle2.Fields(1)
     cmbUF.Text = vObjOracle2.Fields(3)
     
     txtCEP.Text = vObjOracle.Fields(6)
     txtCxPostal.Text = IIf(IsNull(vObjOracle.Fields(11).Value), "", vObjOracle.Fields(11).Value)
     txtAgencia.Text = IIf(IsNull(vObjOracle.Fields(12).Value), "", vObjOracle.Fields(12).Value)
     txtDig_Agencia.Text = IIf(IsNull(vObjOracle.Fields(13).Value), "", vObjOracle.Fields(13).Value)
     txtConta.Text = IIf(IsNull(vObjOracle.Fields(14).Value), "", vObjOracle.Fields(14).Value)
     txtDig_Conta.Text = IIf(IsNull(vObjOracle.Fields(15).Value), "", vObjOracle.Fields(15).Value)
     
     If vObjOracle.Fields(16).Value = 0 Then
         txtSituacao.Text = "ATIVO"
         txtSituacao.ForeColor = vbBlack
         Set cmdSituacao.PictureNormal = LoadPicture(App.Path & "\Inativo 32x32.ico")
     Else
         txtSituacao.Text = "DESATIVADO"
         txtSituacao.ForeColor = vbRed
         Set cmdSituacao.PictureNormal = LoadPicture(App.Path & "\ATIVO 32x32.ICO")
     End If

End Sub

Private Sub cmbUF_Click()
   '-- PREENCHE O COMBO DE CIDADES ATRAV�S DA UF ESCOLHIDA
    Criar_Cursor vBanco.Parameters, "vCursor"
    
    Set vObjOracle = vVB_Generica_001.TabelaCidade(vBanco, , , , cmbUF.Text)
    Call vVB_Generica_001.PreencheComboList(vObjOracle, cmbCidade, "C�D. CIDADE", "CIDADE")
    
End Sub

Private Sub cmdCancelar_Click()
    
    lsvRepresentantes.ListItems.Clear
    
    txtCod_Repres.Text = ""
    txtPseudonimo.Text = ""
    txtCGC.Text = ""
    cmbCGC.Text = ""
    txtSituacao.Text = ""
    txtSituacao.ForeColor = vbBlack
    txtRazao_Social.Text = ""
    txtNum_Cori.Text = ""
    txtDDD.Text = ""
    txtFone.Text = ""
    txtFax.Text = ""
    txtCelular.Text = ""
    txtEndereco.Text = ""
    txtBairro.Text = ""
    cmbUF.Text = ""
    cmbCidade.Text = ""
    txtCEP.Text = ""
    txtCxPostal.Text = ""
    txtAgencia.Text = ""
    txtDig_Agencia.Text = ""
    txtConta.Text = ""
    txtDig_Conta.Text = ""
    
    txtCGC.Visible = True
    cmbCGC.Visible = False
    
    txtCod_Repres.SetFocus
    
End Sub

Private Sub cmdExcluirRepresentante_Click()
    If lsvRepresentantes.SelectedItem = "" Then
        MsgBox "Selecione um Representante da lista.", vbInformation, "Aten��o"
        Exit Sub
    End If
    
    If lsvRepresentantes.ListItems.Count = 1 Then
        MsgBox "Voc� n�o pode excluir o �nico representante desta representa��o.", vbInformation, "Aten��o"
        Exit Sub
    End If
    
    If MsgBox("Confirma a exclus�o do Representante selecionado desta Representa��o ?", vbQuestion + vbYesNo) = vbYes Then
        vBanco.Parameters.Remove "PM_CGC"
        vBanco.Parameters.Add "PM_CGC", Me.txtCGC, 1
        vBanco.Parameters.Remove "PM_COD_REPRES"
        vBanco.Parameters.Add "PM_COD_REPRES", lsvRepresentantes.SelectedItem, 1
        
        vErro = vVB_Generica_001.ExecutaPl(vBanco, "PRODUCAO.PCK_CAD040.PR_EXCLUI_REPRES_REPRESENTACAO(:PM_CGC, :PM_COD_REPRES)")
        
        If vErro <> "" Then
            MsgBox "Erro ao Excluir Representante da Representacao" & vbCrLf & vErro, vbInformation, "Aten��o"
        Else
            Preencher_Representantes_Representacao
        End If
    End If
End Sub

Private Sub cmdGravar_Click()

    If txtCGC.Visible = True And txtCGC.Text = "" Then
        Call vVB_Generica_001.Informar("Por favor, entre com o CGC!")
        Exit Sub
    End If
    
    If txtRazao_Social.Text = "" Then
        Call vVB_Generica_001.Informar("Por favor, entre com a Raz�o Social!")
        Exit Sub
    End If
        
    If txtEndereco.Text = "" Then
        Call vVB_Generica_001.Informar("Por favor, entre com o Endere�o da empresa!")
        Exit Sub
    End If
        
    If cmbCidade.Text = "" Then
        Call vVB_Generica_001.Informar("Por favor, escolha a Cidade da empresa!")
        Exit Sub
    End If
    
   '-- ATUALIZA��O DA TABELA REPRESENTA��O
    If txtCGC.Visible = True Then
        vBanco.Parameters.Remove "PM_CGC"
        vBanco.Parameters.Add "PM_CGC", txtCGC.Text, 1
    Else
        vBanco.Parameters.Remove "PM_CGC"
        vBanco.Parameters.Add "PM_CGC", cmbCGC.Text, 1
    End If
 
    vBanco.Parameters.Remove "PM_RAZAO"
    vBanco.Parameters.Add "PM_RAZAO", txtRazao_Social.Text, 1
    vBanco.Parameters.Remove "PM_NUMCORI"
    vBanco.Parameters.Add "PM_NUMCORI", val(txtNum_Cori.Text), 1
    vBanco.Parameters.Remove "PM_DDD"
    vBanco.Parameters.Add "PM_DDD", txtDDD.Text, 1
    vBanco.Parameters.Remove "PM_FONE"
    vBanco.Parameters.Add "PM_FONE", txtFone.Text, 1
    vBanco.Parameters.Remove "PM_FAX"
    vBanco.Parameters.Add "PM_FAX", txtFax.Text, 1
    vBanco.Parameters.Remove "PM_CELULAR"
    vBanco.Parameters.Add "PM_CELULAR", txtCelular.Text, 1
    vBanco.Parameters.Remove "PM_ENDERECO"
    vBanco.Parameters.Add "PM_ENDERECO", txtEndereco.Text, 1
    vBanco.Parameters.Remove "PM_BAIRRO"
    vBanco.Parameters.Add "PM_BAIRRO", txtBairro.Text, 1
    vBanco.Parameters.Remove "PM_CIDADE"
    vBanco.Parameters.Add "PM_CIDADE", val(Mid(cmbCidade.Text, 1, InStr(1, cmbCidade, "-") - 1)), 1
    vBanco.Parameters.Remove "PM_CEP"
    vBanco.Parameters.Add "PM_CEP", txtCEP.Text, 1
    vBanco.Parameters.Remove "PM_CXPOSTAL"
    vBanco.Parameters.Add "PM_CXPOSTAL", txtCxPostal.Text, 1
    vBanco.Parameters.Remove "PM_AGENCIA"
    vBanco.Parameters.Add "PM_AGENCIA", txtAgencia.Text, 1
    vBanco.Parameters.Remove "PM_DIGAGENCIA"
    vBanco.Parameters.Add "PM_DIGAGENCIA", txtDig_Agencia.Text, 1
    vBanco.Parameters.Remove "PM_CONTA"
    vBanco.Parameters.Add "PM_CONTA", txtConta.Text, 1
    vBanco.Parameters.Remove "PM_DIGCONTA"
    vBanco.Parameters.Add "PM_DIGCONTA", txtDig_Conta.Text, 1
    If txtSituacao.Text = "ATIVO" Then
        vBanco.Parameters.Remove "PM_SITUACAO"
        vBanco.Parameters.Add "PM_SITUACAO", 0, 1
    Else
        vBanco.Parameters.Remove "PM_SITUACAO"
        vBanco.Parameters.Add "PM_SITUACAO", 9, 1
    End If
    
    'Criar_Cursor vBanco.Parameters, "vCursor"
    
    vSql = "Producao.PCK_CAD040.PR_ATUALIZA_REPRESENTACAO(:PM_CGC, " & _
           ":PM_RAZAO, :PM_NUMCORI, :PM_DDD, :PM_FONE, :PM_FAX, :PM_CELULAR, " & _
           ":PM_ENDERECO, :PM_BAIRRO, :PM_CIDADE, :PM_CEP, :PM_CXPOSTAL, " & _
           ":PM_AGENCIA, :PM_DIGAGENCIA, :PM_CONTA, :PM_DIGCONTA, :PM_SITUACAO)"
           
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    If vErro <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento PR_ATUALIZA_REPRESENTACAO, entre em contato com o SUPORTE!")
        Exit Sub
    End If

    If txtCGC.Visible = True Then
        vBanco.Parameters.Remove "PM_CODREPRES"
        vBanco.Parameters.Add "PM_CODREPRES", txtCod_Repres.Text, 1
        
        'Criar_Cursor vBanco.Parameters, "vCursor"
        
        vSql = "Producao.PCK_CAD040.PR_ATUALIZA_REPCGC(:PM_CODREPRES, :PM_CGC)"
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        If vErro <> "" Then
            Call vVB_Generica_001.Informar("Erro no processamento PR_ATUALIZA_REPCGC, entre em contato com o SUPORTE!")
            Exit Sub
        End If
    End If
    
    Call vVB_Generica_001.Informar("Transa��o efetuada com sucesso!")
    
    Call cmdCancelar_Click
    
    Unload frmAguardar

End Sub

Private Sub cmdSituacao_Click()
   If vSituacao = 0 Then
        txtSituacao.Text = "DESATIVADO"
        txtSituacao.ForeColor = vbRed
        Set cmdSituacao.PictureNormal = LoadPicture(App.Path & "\ativo 32x32.ico")
        vSituacao = 9
    Else
        txtSituacao.Text = "ATIVO"
        txtSituacao.ForeColor = vbBlack
        Set cmdSituacao.PictureNormal = LoadPicture(App.Path & "\Inativo 32x32.ico")
        vSituacao = 0
    End If

End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    
   '-- PREENCHE O COMBO DE UF DA TELA DE REPRESENTA��O
    Criar_Cursor vBanco.Parameters, "vCursor"
    
    Set vObjOracle = vVB_Generica_001.TabelaUF(vBanco)
    Call vVB_Generica_001.PreencheComboList(vObjOracle, cmbUF, "UF")
    
End Sub

Private Sub txtBairro_KeyPress(KeyAscii As Integer)
    
    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)

End Sub

Private Sub txtCGC_LostFocus()
    Dim Valido As Boolean
    
    If txtCGC <> "" Then
        Valido = vVB_Generica_001.ValidaCGC(txtCGC.Text)
        
        If Valido = False Then
            Call vVB_Generica_001.Informar("CGC Inv�lido!")
            txtCGC.Text = ""
            Exit Sub
            txtCGC.SetFocus
        End If
    
        If txtCod_Repres.Text = "" Then
           '-- BUSCA O PSEUDONIMO E C�DIGO DO REPRESENTANTE ATRAV�S DO CGC
            vBanco.Parameters.Remove "PM_CGC"
            vBanco.Parameters.Add "PM_CGC", txtCGC.Text, 1
             
            Criar_Cursor vBanco.Parameters, "vCursor"
            
            vSql = "Producao.PCK_CAD040.PR_BUSCA_REPCGC(:PM_CGC, :VCURSOR)"
             
            If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
                Call vVB_Generica_001.Informar("Erro no processamento PR_BUSCA_REPCGC, entre em contato com o SUPORTE!")
                Exit Sub
            Else
                Set vObjOracle = vBanco.Parameters("VCURSOR").Value
            End If
             
            If Not vObjOracle.EOF Then
                txtCod_Repres.Text = vObjOracle!Cod_Repres.Value
                txtPseudonimo.Text = vObjOracle!PSEUDONIMO.Value
            End If
        End If
        
       '-- BUSCA OS DADOS ATRAV�S DO CGC DA EMPRESA DO REPRESENTANTE
        Criar_Cursor vBanco.Parameters, "vCursor"
        
        Set vObjOracle = vVB_Venda_001.TabelaRepresentacao(vBanco, , , txtCGC.Text)
        
        If Not vObjOracle.EOF Then
            txtCGC.Text = vObjOracle.Fields(0).Value
            txtNum_Cori.Text = IIf(IsNull(vObjOracle.Fields(2).Value), "", vObjOracle.Fields(2).Value)
            txtRazao_Social.Text = vObjOracle.Fields(1).Value
            txtDDD.Text = IIf(IsNull(vObjOracle.Fields(7).Value), "", vObjOracle.Fields(7).Value)
            txtFone.Text = IIf(IsNull(vObjOracle.Fields(8).Value), "", vObjOracle.Fields(8).Value)
            txtFax.Text = IIf(IsNull(vObjOracle.Fields(9).Value), "", vObjOracle.Fields(9).Value)
            txtCelular.Text = IIf(IsNull(vObjOracle.Fields(10).Value), "", vObjOracle.Fields(10).Value)
            txtEndereco.Text = vObjOracle.Fields(3).Value
            txtBairro.Text = vObjOracle.Fields(4).Value
            
            '-- BUSCA O NOME DA CIDADE E UF
            Criar_Cursor vBanco.Parameters, "vCursor"
            
            Set vObjOracle2 = vVB_Generica_001.TabelaCidade(vBanco, vObjOracle.Fields(5).Value)
            
            cmbCidade.Text = vObjOracle2.Fields(0).Value & " - " & vObjOracle2.Fields(1)
            cmbUF.Text = vObjOracle2.Fields(3)
            
            txtCEP.Text = vObjOracle.Fields(6)
            txtCxPostal.Text = IIf(IsNull(vObjOracle.Fields(11).Value), "", vObjOracle.Fields(11).Value)
            txtAgencia.Text = IIf(IsNull(vObjOracle.Fields(12).Value), "", vObjOracle.Fields(12).Value)
            txtDig_Agencia.Text = IIf(IsNull(vObjOracle.Fields(13).Value), "", vObjOracle.Fields(13).Value)
            txtConta.Text = IIf(IsNull(vObjOracle.Fields(14).Value), "", vObjOracle.Fields(14).Value)
            txtDig_Conta.Text = IIf(IsNull(vObjOracle.Fields(15).Value), "", vObjOracle.Fields(15).Value)
            
            If vObjOracle.Fields(16).Value = 0 Then
                txtSituacao.Text = "ATIVO"
                txtSituacao.ForeColor = vbBlack
                Set cmdSituacao.PictureNormal = LoadPicture(App.Path & "\Inativo 32x32.ico")
            Else
                txtSituacao.Text = "DESATIVADO"
                txtSituacao.ForeColor = vbRed
                Set cmdSituacao.PictureNormal = LoadPicture(App.Path & "\ATIVO 32x32.ICO")
            End If
        Else
            txtSituacao.Text = "ATIVO"
            txtSituacao.ForeColor = vbBlack
            txtRazao_Social.SetFocus
        End If
    End If
    
End Sub

Private Sub txtCod_Repres_Change()
    Preencher_Representantes_Representacao
End Sub

Private Sub txtCod_Repres_GotFocus()
    Call cmdCancelar_Click
End Sub

Private Sub txtCod_Repres_LostFocus()
    
    If txtCod_Repres.Text <> "" Then
       '-- BUSCA O PSEUDONIMO DO REPRESENTANTE
        Criar_Cursor vBanco.Parameters, "vCursor"
        
        Set vObjOracle = vVB_Venda_001.TabelaRepresentante(vBanco, txtCod_Repres.Text)
        
        If vObjOracle.EOF Then
            Call vVB_Generica_001.Informar("Representante n�o encontrado!")
            Call cmdCancelar_Click
        Else
            txtPseudonimo.Text = vObjOracle.Fields(3).Value
    
           '-- BUSCA OS DADOS DA REPRESENTA��O ATRAV�S DO C�DIGO DO REPRESENTANTE
            Criar_Cursor vBanco.Parameters, "vCursor"
        
            Set vObjOracle = vVB_Venda_001.TabelaRepresentacao(vBanco, txtCod_Repres.Text)
            
            If vObjOracle.RecordCount = 1 Then
                cmbCGC.Visible = False
                txtCGC.Visible = True
                txtCGC.Text = vObjOracle.Fields(0).Value
                txtNum_Cori.Text = IIf(IsNull(vObjOracle.Fields(2).Value), "", vObjOracle.Fields(2).Value)
                txtRazao_Social.Text = vObjOracle.Fields(1).Value
                txtDDD.Text = IIf(IsNull(vObjOracle.Fields(7).Value), "", vObjOracle.Fields(7).Value)
                txtFone.Text = IIf(IsNull(vObjOracle.Fields(8).Value), "", vObjOracle.Fields(8).Value)
                txtFax.Text = IIf(IsNull(vObjOracle.Fields(9).Value), "", vObjOracle.Fields(9).Value)
                txtCelular.Text = IIf(IsNull(vObjOracle.Fields(10).Value), "", vObjOracle.Fields(10).Value)
                txtEndereco.Text = vObjOracle.Fields(3).Value
                txtBairro.Text = vObjOracle.Fields(4).Value
                
               '-- BUSCA O NOME DA CIDADE E UF
                Criar_Cursor vBanco.Parameters, "vCursor"
            
                Set vObjOracle2 = vVB_Generica_001.TabelaCidade(vBanco, vObjOracle.Fields(5).Value)
        
                cmbCidade.Text = Format(vObjOracle2.Fields(0), "0000") & " - " & vObjOracle2.Fields(1)
                cmbUF.Text = vObjOracle2.Fields(3)
                
                txtCEP.Text = vObjOracle.Fields(6)
                txtCxPostal.Text = IIf(IsNull(vObjOracle.Fields(11).Value), "", vObjOracle.Fields(11).Value)
                txtAgencia.Text = IIf(IsNull(vObjOracle.Fields(12).Value), "", vObjOracle.Fields(12).Value)
                txtDig_Agencia.Text = IIf(IsNull(vObjOracle.Fields(13).Value), "", vObjOracle.Fields(13).Value)
                txtConta.Text = IIf(IsNull(vObjOracle.Fields(14).Value), "", vObjOracle.Fields(14).Value)
                txtDig_Conta.Text = IIf(IsNull(vObjOracle.Fields(15).Value), "", vObjOracle.Fields(15).Value)
                
                If vObjOracle.Fields(16).Value = 0 Then
                    txtSituacao.Text = "ATIVO"
                    txtSituacao.ForeColor = vbBlack
                    vSituacao = 0
                    Set cmdSituacao.PictureNormal = LoadPicture(App.Path & "\Inativo 32x32.ico")
                Else
                    txtSituacao.Text = "DESATIVADO"
                    txtSituacao.ForeColor = vbRed
                    vSituacao = 9
                    Set cmdSituacao.PictureNormal = LoadPicture(App.Path & "\ATIVO 32x32.ICO")
                End If
                
            ElseIf vObjOracle.RecordCount > 1 Then
                txtCGC.Visible = False
                cmbCGC.Visible = True
                Do While Not vObjOracle.EOF
                    Call vVB_Generica_001.PreencheComboList(vObjOracle, cmbCGC, "CGC")
                Loop
                cmbCGC.ListIndex = 0
            Else
                cmbCGC.Visible = False
                txtCGC.Visible = True
                txtCGC.SetFocus
            End If
        End If
        
        Preencher_Representantes_Representacao
    
    End If
    
End Sub

Private Sub txtEndereco_KeyPress(KeyAscii As Integer)
    
    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)

End Sub

Private Sub txtRazao_Social_KeyPress(KeyAscii As Integer)
    
    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)

End Sub

Sub Preencher_Representantes_Representacao()
    Dim litem As ListItem
    Dim i As Integer
    Dim vRst As Object
    
    lsvRepresentantes.ListItems.Clear
    
    If txtCod_Repres = "" Then Exit Sub
    
    Criar_Cursor vBanco.Parameters, "vCursor"
    
    vBanco.Parameters.Remove "PM_CGC"
    vBanco.Parameters.Add "PM_CGC", txtCGC.Text, 1
    
    vSql = "Producao.PCK_CAD040.PR_SELECT_REPRES_REPRESENTACAO(:VCURSOR, :PM_CGC)"
             
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
       Call vVB_Generica_001.Informar("Erro no processamento PR_BUSCA_REPCGC, entre em contato com o SUPORTE!")
       Exit Sub
    Else
       Set vRst = vBanco.Parameters("VCURSOR").Value
    End If
    
    For i = 1 To vRst.RecordCount
        Set litem = lsvRepresentantes.ListItems.Add
        litem = vRst!Cod_Repres
        litem.SubItems(1) = vRst!nome_repres
        vRst.MoveNext
    Next
    
    cmdExcluirRepresentante.Enabled = Not (vRst.RecordCount <= 1)
    
    vRst.Close
    Set vRst = Nothing
    
End Sub
