VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmImporta 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "IMPORTA��O AUTOM�TICA DE QUOTA"
   ClientHeight    =   7425
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10770
   Icon            =   "frmImporta.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   495
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   718
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraArquivo 
      Caption         =   "Arquivo"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1095
      Left            =   90
      TabIndex        =   1
      Top             =   945
      Width           =   10635
      Begin Bot�o.cmd cmdAbrir_Arquivo 
         Height          =   690
         Left            =   135
         TabIndex        =   2
         TabStop         =   0   'False
         ToolTipText     =   "Sobre"
         Top             =   270
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmImporta.frx":0CCA
         PICN            =   "frmImporta.frx":0CE6
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MSComDlg.CommonDialog Controle 
         Left            =   90
         Top             =   405
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin VB.TextBox txtCaminho 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   945
         TabIndex        =   3
         Top             =   450
         Width           =   6315
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   10
      Top             =   7095
      Width           =   10770
      _ExtentX        =   18997
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   18944
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   0
      Top             =   810
      Width           =   10680
      _ExtentX        =   18838
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   10035
      TabIndex        =   9
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmImporta.frx":19C0
      PICN            =   "frmImporta.frx":19DC
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdGera_Log 
      Height          =   690
      Left            =   45
      TabIndex        =   7
      TabStop         =   0   'False
      ToolTipText     =   "Gera Arquivo de Log"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmImporta.frx":26B6
      PICN            =   "frmImporta.frx":26D2
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdGravar 
      Height          =   690
      Left            =   810
      TabIndex        =   8
      TabStop         =   0   'False
      ToolTipText     =   "Gravar Arquivo Importado"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmImporta.frx":33AC
      PICN            =   "frmImporta.frx":33C8
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSFlexGridLib.MSFlexGrid mfgArquivo 
      Height          =   3840
      Left            =   90
      TabIndex        =   11
      Top             =   2160
      Width           =   10635
      _ExtentX        =   18759
      _ExtentY        =   6773
      _Version        =   393216
      Cols            =   8
      BackColorBkg    =   -2147483633
      HighLight       =   0
      AllowUserResizing=   1
      Appearance      =   0
      FormatString    =   "^ REG. |^ DATA |^ REPRES. |^ COTA |^ COTAUS |^ %TELEM. |^ I/A |^ OBSERVA��O "
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblMensagem3 
      Appearance      =   0  'Flat
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00004080&
      Height          =   240
      Left            =   135
      TabIndex        =   6
      Top             =   6750
      Width           =   10545
   End
   Begin VB.Label lblMensagem2 
      Appearance      =   0  'Flat
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   240
      Left            =   135
      TabIndex        =   5
      Top             =   6435
      Width           =   10545
   End
   Begin VB.Label lblMensagem1 
      Appearance      =   0  'Flat
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   240
      Left            =   135
      TabIndex        =   4
      Top             =   6120
      Width           =   10545
   End
End
Attribute VB_Name = "frmImporta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdAbrir_Arquivo_Click()
    
    Screen.MousePointer = 11
    
    txtCaminho.Text = ""
    Call vVB_Generica_001.LimpaFlexGridComTitulo(mfgArquivo)
        
    mfgArquivo.FormatString = "^ REG. |^ DATA |^ REPRES. |^ COTA |^ COTAUS |^ %TELEM. |^ I/A |^ OBSERVA��O "
    lblMensagem1.Caption = ""
    lblMensagem2.Caption = ""
    lblMensagem3.Caption = ""
    
   '-- Indica que o controle ser� usado para abrir o arquivo
    Controle.ShowOpen
    txtCaminho.Text = Controle.FileName
    vNomeArq = txtCaminho.Text

   '-- Verifica se foi indicado o diret�rio e nome do arquivo a ser lido
    If txtCaminho.Text = "" Then
        Call vVB_Generica_001.Informar("Indique o arquivo a ser aberto")
        Screen.MousePointer = 0
        Exit Sub
    End If

   '-- LEITURA DO ARQUIVO
    Open vNomeArq For Input As #1
    
    lblMensagem1.Caption = ""
    lblMensagem1.Visible = True
    
   '-- L� enquanto n�o for final de arquivo
    Do While Not EOF(1)
        i = i + 1
        lblMensagem1.Caption = "CARREGANDO ...." & i
        lblMensagem1.Refresh
        
       '-- Inicializa flags e vari�veis
        vDATA = ""
        vREPR = ""
        vCOTA = ""
        vCOTAUS = ""
        vTELEM = ""
        vTRAN = ""
        vFL_DPK_OK = True
        
       'L� registro - linha - do arquivo, joga na vari�vel  e seta ponteiro para o pr�ximo
        Line Input #1, varLinhaArquivo
       'Se linha lida for em branco, sai do loop
        If Mid(varLinhaArquivo, 1, 33) = "" Then
            Exit Do
        End If
        
       '-- Qtd. Linhas do grid ser� correspondente � qtd. de linhas lidas do arquivo texto
        mfgArquivo.Rows = i + 1
        mfgArquivo.Row = i
        
       '-- 1a Coluna vai receber a numera��o de registros lidos do arquivo
        mfgArquivo.Col = 0
        mfgArquivo.Text = Format(i, "000")
        
       '-- Atribui valores da linha do arquivo para as vari�veis
        vDATA = Mid(varLinhaArquivo, 1, 7)
        vREPR = Trim(Mid(varLinhaArquivo, 8, 4))
        vCOTA = Trim(Mid(varLinhaArquivo, 12, 10))
        vCOTAUS = Trim(Mid(varLinhaArquivo, 22, 10))
        vTELEM = Trim(Mid(varLinhaArquivo, 32, 3))
        
       '-- Valida��es gerais com o CD, DPK e SITUA��O que vieram no arq., verificando se n�o est�o inv�lidos
        If vDATA = "" Or val(vDATA) = 0 Then
            mfgArquivo.Col = 7
            mfgArquivo.Text = "DATA Inv�lida! "
            vFL_DPK_OK = False
        ElseIf vREPR = "" Or val(vREPR) = 0 Then
            mfgArquivo.Col = 7
            mfgArquivo.Text = "REPRESENTANTE Inv�lido! "
            vFL_DPK_OK = False
        ElseIf vCOTA = "" Or val(vCOTA) = 0 Then
            mfgArquivo.Col = 7
            mfgArquivo.Text = "COTA Inv�lida! "
            vFL_DPK_OK = False
        Else
           '-- VERIFICA A SITUA��O DO REPRESENTANTE
            Criar_Cursor vBanco.Parameters, "vCursor"
             
            Set vObjOracle = vVB_Venda_001.TabelaRepresentante(vBanco, val(vREPR))
             
           '-- Se select n�o trouxer nada, h� problema com o Representante
            If vObjOracle.EOF Then
                mfgArquivo.Col = 7
                mfgArquivo.Text = "Probl. c/ Representante! "
                vFL_DPK_OK = False
            Else
               '-- Se a situa��o do REPRESENTANTE estiver desativado, n�o faz nada
                If val(vObjOracle!Situa��o) = 9 Then
                    mfgArquivo.Col = 7
                    mfgArquivo.Text = "Representante Desativado!"
                    vFL_DPK_OK = False
                End If
            End If
        End If
        
       '-- Se as valida��es acima estiverem corretas, continua; sen�o l� pr�xima linha do arq.
        If vFL_DPK_OK = True Then
            vBanco.Parameters.Remove "PM_CODREPRES"
            vBanco.Parameters.Add "PM_CODREPRES", val(vREPR), 1
            vBanco.Parameters.Remove "PM_ANOMES"
            vBanco.Parameters.Add "PM_ANOMES", Format(vDATA, "YYYYMM"), 1
            
            Criar_Cursor vBanco.Parameters, "vCursor"
            
            vSql = "Producao.PCK_CAD040.PR_BUSCA_QTD_QUOTA(:PM_CODREPRES, :PM_ANOMES, :VCURSOR)"
             
            If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
                Call vVB_Generica_001.Informar("Erro no processamento PR_BUSCA_QTD_QUOTA, entre em contato com o SUPORTE!")
                Exit Sub
            Else
                Set vObjOracle = vBanco.Parameters("VCURSOR").Value
            End If
            
           '-- Se select n�o trouxer nada, � inclus�o, sen�o � exclus�o
            If vObjOracle!Qtd = 0 Then
                vTRAN = "I"
            Else
                vTRAN = "A"
            End If
        End If
        
       '-- Alimenta o grid com as informa��es do arquivo, independente de estarem corretas ou n�o
        mfgArquivo.Col = 1
        mfgArquivo.Text = vDATA
        mfgArquivo.Col = 2
        mfgArquivo.Text = vREPR
        mfgArquivo.Col = 3
        mfgArquivo.Text = vCOTA
        mfgArquivo.Col = 4
        mfgArquivo.Text = vCOTAUS
        mfgArquivo.Col = 5
        mfgArquivo.Text = vTELEM
        mfgArquivo.Col = 6
        mfgArquivo.Text = vTRAN
    
   '-- L� pr�xima linha
    Loop
    
    Screen.MousePointer = 0
    
    mfgArquivo.ColWidth(0) = 550
    mfgArquivo.ColWidth(1) = 900
    mfgArquivo.ColWidth(2) = 800
    mfgArquivo.ColWidth(3) = 1100
    mfgArquivo.ColWidth(4) = 1100
    mfgArquivo.ColWidth(5) = 800
    mfgArquivo.ColWidth(6) = 400
    mfgArquivo.ColWidth(7) = 5000
   
    mfgArquivo.Visible = True
    Call vVB_Generica_001.Informar("Arquivo Lido!")
    lblMensagem1.Caption = i & " Linhas Lidas do Arquivo!"
    lblMensagem1.Refresh
    
    Close #1
    Screen.MousePointer = 0
        
    Exit Sub
        
Trata_Erro:
    
    If Err.Number = 53 Then
        Call vVB_Generica_001.Informar("Arquivo de Controle n�o Encontrado.")
    Else
        Call vVB_Generica_001.ProcessaErro(Err.Number & "-" & Err.Description)
    End If

End Sub

Private Sub cmdGera_Log_Click()

    Dim j As Long               '-- Ponteiro de caracter no campo observa��o
    Dim vTamObs As Long         '-- Guarda a qtd. de caracteres da coluna Observa��o
    Dim sOBS As String          '-- Guarda o conte�do da coluna de Observa��es
    Dim sREPRES As String       '-- Guarda o c�digo de DPK para impress�o
    Dim sDATA As String         '-- Guarda a Data
    
    If MsgBox("Confirma gera��o do arquivo de log?", vbYesNo, "CAD040") = vbYes Then
    
       '-- Gera arquivo de log com o nome LODDMMAA.TXT
        vdt_cad = Date
        Open "h:\COTA\LOG\Lo" & Mid(vdt_cad, 1, 2) & _
                            Mid(vdt_cad, 4, 2) & Mid(vdt_cad, 7, 2) & ".txt" For Output As #1
        
        Print #1, "LOG DO ARQUIVO: " & vNomeArq
        Print #1, ""
        Print #1, "REPR  DATA   OBSERVA��ES"
        Print #1, "---- ------- --------------------------------------------------------------------"
       '           1067 09/2002
        
       '-- Pega linha a linha do grid para gravar as que cont�m campo de Observa��o <> ""
        For i = 1 To mfgArquivo.Rows - 1
            
            mfgArquivo.Row = i
            
            mfgArquivo.Col = 2
            sREPRES = Trim(mfgArquivo.Text)         '-- Pega o c�digo do representante
            mfgArquivo.Col = 1
            sDATA = Trim(mfgArquivo.Text)           '-- Pega a data da cota
            mfgArquivo.Col = 7
            sOBS = Trim(mfgArquivo.Text)            '-- Pega as Observa��es
            
           '-- Se a coluna de Observa��o n�o estiver em branco
            If sOBS <> "" Then
                
               '-- Zero ponteiro que vai me indicar que caracter no campo obs. estou lendo
                j = 0
               '-- Pego o tamanho da observa��o para esta linha
                vTamObs = CLng(Len(sOBS))
                
               '-- Se for menor ou igual a 65, gravo a linha no arquivo normalmente
                If vTamObs <= 65 Then
                    Print #1, String(4 - Len(sREPRES), " ") & sREPRES & " " & _
                              String(7 - Len(sDATA), " ") & sDATA & " " & _
                              sOBS
               '-- Se for maior que 65 caracteres
                Else
                   '-- Imprimo os primeiros 65 caracteres
                    Print #1, String(4 - Len(sREPRES), " ") & sREPRES & " " & _
                              String(7 - Len(sDATA), " ") & sDATA & " " & _
                              Mid(sOBS, 1, 65)
                   
                   '-- Tamanho agora vai ser igual ao tamanho original - 65
                    vTamObs = vTamObs - 65
                   '-- Indico de que caracter vou come�ar a ler no cpo. Observa��o
                    j = j + 66
                    
                   '-- Enquanto o tamanho for maior que 0
                    Do While vTamObs > 0
                       '-- Se tamanho maior que 61, imprime os pr�ximos 61 caracteres e atualiza tam.e ponteiro
                        If vTamObs > 65 Then
                            Print #1, Space(19) & Mid(sOBS, j, 65)
                            vTamObs = vTamObs - 65
                            j = j + 66
                       '-- Se tamanho menor que 61, imprime os caracteres que faltam
                        Else
                            Print #1, Space(19) & Mid(sOBS, j, vTamObs)
                            vTamObs = 0
                        End If
                    Loop
                End If
            End If
        Next i
    
        Print #1, ""
        Print #1, "***************** FIM DO ARQUIVO DE LOG ***********************"
        
        Close #1
    
        Call vVB_Generica_001.Informar("Arquivo gerado com sucesso!")
    End If

End Sub

Private Sub cmdGravar_Click()
    Dim vObs As String            'Guarda o conte�do da coluna de Observa��es
    Dim vReg_Gravado As Long      'Guarda a qtd. de itens gravados
    Dim vReg_Nao_Gravado As Long  'Guarda a qtd. de itens n�o gravados
    Dim vLinSemObs As Long        'Guarda a qtd.de linhas sem observa��o
    Dim objconexao As Object      'Guarda resultado do select na LOJA_CONEXAO
    Dim vCommit As Long           'Contador de transa��es para fazer commit
    Dim K As Long                 'Auxiliar de bind

    If MsgBox("Confirma inclus�o/altera��o da Cota?", vbYesNo, "CAD040") = vbYes Then
    
        Screen.MousePointer = 11
    
       '-- Verifica se o grid n�o est� em branco
        mfgArquivo.Row = 1
        mfgArquivo.Col = 0
        If mfgArquivo.Text = "" Then
            Screen.MousePointer = 0
            Call vVB_Generica_001.Informar("Nenhum dado poder� ser alterado!")
            Exit Sub
        End If
    
        'Perguntar se a usu�ria deseja zerar as cotas do ano/mes em questao
        If MsgBox("Deseja Zerar Cotas j� existentes do Ano/M�s que est� sendo carregado ?", vbQuestion + vbYesNo, "Aten��o") = vbYes Then
            vBanco.Parameters.Remove "PM_ANOMES"
            vBanco.Parameters.Add "PM_ANOMES", Right(mfgArquivo.TextMatrix(1, 1), 4) & Left(mfgArquivo.TextMatrix(1, 1), 2), 1
            vErro = vVB_Generica_001.ExecutaPl(vBanco, "PRODUCAO.PCK_CAD040.PR_ZERAR_COTAS(:PM_ANOMES)")
        End If
        
       '-- Primeiro vai percorrer todo o grid p/ ver se tem itens a serem gravados
        cmd = ""
        vLinSemObs = 0
        For i = 1 To mfgArquivo.Rows - 1
            mfgArquivo.Row = i
            mfgArquivo.Col = 7
            vObs = mfgArquivo.Text
           '-- Se a coluna de Observa��o n�o contiver nenhuma mensagem de inconsist�ncia vai gravar
            If vObs = "" Then
                vLinSemObs = vLinSemObs + 1
            End If
        Next i
        If vLinSemObs = 0 Then
            Screen.MousePointer = 0
            Call vVB_Generica_001.Informar("Nenhum dado poder� ser alterado!")
            Exit Sub
        End If
        
       '-- Se houverem itens a serem gravados, vai iniciar todo o processo
       '-- Zera contador de linhas gravadas
        vReg_Gravado = 0
        vReg_Nao_Gravado = 0
    
       'Pega linha a linha do grid para gravar as que cont�m campo de Observa��o <> ""
        For i = 1 To mfgArquivo.Rows - 1
            
            mfgArquivo.Row = i
            mfgArquivo.Col = 7
            vObs = Trim(mfgArquivo.Text)
            
           '-- Se a coluna de Observa��o n�o contiver nenhuma mensagem de inconsist�ncia vai gravar
            If vObs = "" Then
                
               '-- Alimenta vari�veis com os dados do grid
                mfgArquivo.Col = 1
                vDATA = mfgArquivo.Text
                mfgArquivo.Col = 2
                vREPR = Trim(mfgArquivo.Text)
                mfgArquivo.Col = 3
                vCOTA = Trim(mfgArquivo.Text)
                mfgArquivo.Col = 4
                vCOTAUS = Trim(mfgArquivo.Text)
                mfgArquivo.Col = 5
                vTELEM = Trim(mfgArquivo.Text)
                
               '-- BUSCA O PSEUDONIMO E OUTROS DADOS DO REPRESENTANTE
                Criar_Cursor vBanco.Parameters, "vCursor"
                  
                Set vObjOracle = vVB_Venda_001.TabelaRepresentante(vBanco, val(vREPR))
                  
                If vObjOracle.EOF Then
                    Call vVB_Generica_001.Informar("Representante n�o encontrado!")
                    Exit Sub
                Else
                    vCIC = vObjOracle.Fields(0).Value
                End If
                
                vBanco.Parameters.Remove "PM_DATA"
                vBanco.Parameters.Add "PM_DATA", CLng(Mid(vDATA, 4, 4) & Mid(vDATA, 1, 2)), 1
                vBanco.Parameters.Remove "PM_CICCGC"
                vBanco.Parameters.Add "PM_CICCGC", val(vCIC), 1
                vBanco.Parameters.Remove "PM_COTA"
                vBanco.Parameters.Add "PM_COTA", CDbl(vCOTA), 1
                vBanco.Parameters.Remove "PM_COTAUS"
                vBanco.Parameters.Add "PM_COTAUS", CDbl(vCOTAUS), 1
                vBanco.Parameters.Remove "PM_TELEM"
                vBanco.Parameters.Add "PM_TELEM", CDbl(vTELEM), 1
                
                Criar_Cursor vBanco.Parameters, "vCursor"
                
                vSql = "Producao.PCK_CAD040.PR_ATUALIZA_QUOTA(:PM_CICCGC, " & _
                       ":PM_COTA, :PM_COTAUS, :PM_TELEM, :PM_DATA)"
                
                If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
                    Call vVB_Generica_001.Informar("Erro no processo de grava��o. Entre em contato com o SUPORTE.")
                    Exit Sub
                End If
                vReg_Gravado = vReg_Gravado + 1
            Else
                vReg_Nao_Gravado = vReg_Nao_Gravado + 1
            End If
        Next i
        Screen.MousePointer = 0
        Call vVB_Generica_001.Informar(vReg_Gravado & IIf(vReg_Gravado = 1, " Representante teve ", " Representantes tiveram ") & "sua Cota Inclu�da/Alterada!")
        lblMensagem2.Visible = True
        lblMensagem2.Caption = vReg_Gravado & IIf(vReg_Gravado = 1, " Cota Inclu�da/Alterada!", " Cotas Incluidas/Alteradas!")
        lblMensagem3.Visible = True
        lblMensagem3.Caption = vReg_Nao_Gravado & IIf(vReg_Nao_Gravado = 1, " Cota N�o Inclu�da/Alterada!", " Cotas N�o Inclu�das/Alteradas!")
    End If

End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub

Private Sub Form_Load()
    
    mfgArquivo.Visible = False
    lblMensagem1.Visible = False
    lblMensagem2.Visible = False
    lblMensagem3.Visible = False

End Sub

