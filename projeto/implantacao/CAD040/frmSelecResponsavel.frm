VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmSelecResponsavel 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Sele��o de Novo Respons�vel"
   ClientHeight    =   4140
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6495
   Icon            =   "frmSelecResponsavel.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   276
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   433
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fra 
      Appearance      =   0  'Flat
      Caption         =   "Selecione um Representante"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   2355
      Left            =   45
      TabIndex        =   5
      Top             =   1395
      Width           =   6405
      Begin MSFlexGridLib.MSFlexGrid grd 
         Height          =   2040
         Left            =   45
         TabIndex        =   6
         Top             =   270
         Width           =   6315
         _ExtentX        =   11139
         _ExtentY        =   3598
         _Version        =   393216
         BackColorBkg    =   -2147483633
         HighLight       =   0
         ScrollBars      =   1
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.TextBox txtCliente 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   720
      TabIndex        =   3
      Top             =   990
      Width           =   5685
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   3810
      Width           =   6495
      _ExtentX        =   11456
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   11404
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      Top             =   810
      Width           =   6405
      _ExtentX        =   11298
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmSelecResponsavel.frx":23D2
      PICN            =   "frmSelecResponsavel.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label lbl 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "Cliente:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   90
      TabIndex        =   4
      Top             =   1080
      Width           =   615
   End
End
Attribute VB_Name = "frmSelecResponsavel"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public rs As Object


Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    Dim i As Integer
    
    Me.Top = 0
    Me.Left = 0
    
    
    txtCliente.Text = rs(5)

    With grd
        
        .Cols = 4
    
        .Row = 0
        
        .Col = 0
        .ColWidth(.Col) = 100
        
        .Col = 1
        .Text = "C�D."
        .ColWidth(.Col) = 700
        
        .Col = 2
        .Text = "REPRES"
        .ColWidth(.Col) = 2100
        
        .Col = 3
        .Text = "FILIAL"
        .ColWidth(.Col) = 3350
        
        .Rows = rs.RecordCount + 1
        
        i = 1
        While Not rs.EOF
        
            If rs(2) <> frmRepresentante.txtCod_Repres.Text Then
                .Row = i
                
                .Col = 1
                .Text = IIf(rs(2) = "0", rs(0), rs(2))
                
                .Col = 2
                .Text = IIf(rs(2) = "0", rs(1), rs(3))
                
                .Col = 3
                .Text = rs(4)
                
                i = i + 1
            End If
            rs.MoveNext
        Wend
        
    End With
    
End Sub

Private Sub grd_Click()

    Dim linha As Integer

    With grd
        linha = .Row
    
        .BackColor = vbWhite
        
        For j = 1 To .Rows - 1
            .Row = j
            For i = 1 To 3
                .Col = i
                If j = linha Then
                    .CellBackColor = vbYellow
                Else
                    .CellBackColor = vbWhite
                End If
            Next
        Next
        
        .Row = linha
        .Col = 1
        
    End With

End Sub


Private Sub grd_DblClick()

    grd.Col = 1
    
    If MsgBox("Confirma a transferencia da duplicata deste cliente?", vbYesNo, "ATEN��O") = vbYes Then
        frmDuplAberto.repNovo = grd.Text
        Unload Me
    End If
        
End Sub
