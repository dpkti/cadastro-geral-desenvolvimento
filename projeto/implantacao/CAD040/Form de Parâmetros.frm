VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{E57FB72C-1106-44AF-9706-0BA099A707C7}#4.2#0"; "XPFrame.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "Msflxgrd.ocx"
Begin VB.Form frmParametros 
   Appearance      =   0  'Flat
   BackColor       =   &H00E0E0E0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   ".:  PAR�METROS DO SISTEMA  :."
   ClientHeight    =   4650
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7755
   ControlBox      =   0   'False
   Icon            =   "Form de Par�metros.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4650
   ScaleWidth      =   7755
   StartUpPosition =   2  'CenterScreen
   Begin CoolXPFrame.xpFrame fraSistema 
      Height          =   690
      Left            =   45
      Top             =   1440
      Width           =   7665
      _ExtentX        =   13520
      _ExtentY        =   1217
      Caption         =   "Sistema"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   13977088
      BackColor       =   14737632
      BorderColor     =   0
      ColorStyle      =   99
      Begin VB.TextBox txtNomeSoftware 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   720
         Locked          =   -1  'True
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   270
         Width           =   6855
      End
      Begin VB.TextBox txtCodSoftware 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   90
         Locked          =   -1  'True
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   270
         Width           =   600
      End
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   960
      Left            =   7065
      TabIndex        =   6
      TabStop         =   0   'False
      ToolTipText     =   "Sair do sistema"
      Top             =   45
      Width           =   645
      _ExtentX        =   1138
      _ExtentY        =   1693
      BTYPE           =   3
      TX              =   "Sair"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   2
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Form de Par�metros.frx":0CCA
      PICN            =   "Form de Par�metros.frx":0CE6
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdNovo 
      Height          =   960
      Left            =   1035
      TabIndex        =   0
      TabStop         =   0   'False
      ToolTipText     =   "Novo registro"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1693
      BTYPE           =   3
      TX              =   "Novo"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   2
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Form de Par�metros.frx":19C0
      PICN            =   "Form de Par�metros.frx":19DC
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdConsultar 
      Height          =   960
      Left            =   1755
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Consultar informa��es"
      Top             =   45
      Width           =   870
      _ExtentX        =   1535
      _ExtentY        =   1693
      BTYPE           =   3
      TX              =   "Consultar"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   2
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Form de Par�metros.frx":26B6
      PICN            =   "Form de Par�metros.frx":26D2
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdGravar 
      Height          =   960
      Left            =   2655
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Gravar informa��es"
      Top             =   45
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1693
      BTYPE           =   3
      TX              =   "Gravar"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   2
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Form de Par�metros.frx":33AC
      PICN            =   "Form de Par�metros.frx":33C8
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdExcluir 
      Height          =   960
      Left            =   3420
      TabIndex        =   3
      TabStop         =   0   'False
      ToolTipText     =   "Excluir informa��es"
      Top             =   45
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1693
      BTYPE           =   3
      TX              =   "Excluir"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   2
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Form de Par�metros.frx":40A2
      PICN            =   "Form de Par�metros.frx":40BE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdImprimir 
      Height          =   960
      Left            =   4185
      TabIndex        =   4
      TabStop         =   0   'False
      ToolTipText     =   "Rela��o de impressoras"
      Top             =   45
      Width           =   1050
      _ExtentX        =   1852
      _ExtentY        =   1693
      BTYPE           =   3
      TX              =   "Impressoras"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   2
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Form de Par�metros.frx":4D98
      PICN            =   "Form de Par�metros.frx":4DB4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdLimpar 
      Cancel          =   -1  'True
      Height          =   960
      Left            =   5265
      TabIndex        =   5
      TabStop         =   0   'False
      ToolTipText     =   "Limpar campos"
      Top             =   45
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1693
      BTYPE           =   3
      TX              =   "Limpar"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   2
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Form de Par�metros.frx":5A8E
      PICN            =   "Form de Par�metros.frx":5AAA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin CoolXPFrame.xpFrame fraNovo 
      Height          =   1050
      Left            =   45
      Top             =   2115
      Visible         =   0   'False
      Width           =   7665
      _ExtentX        =   13520
      _ExtentY        =   1852
      Caption         =   "Informa��es do par�metro"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   13977088
      BackColor       =   14737632
      BorderColor     =   0
      ColorStyle      =   99
      Begin VB.TextBox txtValorParametro 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   585
         MaxLength       =   150
         TabIndex        =   12
         Top             =   630
         Width           =   6990
      End
      Begin VB.TextBox txtNomeParametro 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   585
         MaxLength       =   40
         TabIndex        =   10
         Top             =   270
         Width           =   6990
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Valor"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   90
         TabIndex        =   11
         Top             =   675
         Width           =   360
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Nome"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   90
         TabIndex        =   9
         Top             =   315
         Width           =   405
      End
   End
   Begin CoolXPFrame.xpFrame fraParametros 
      Height          =   2490
      Left            =   45
      Top             =   2115
      Visible         =   0   'False
      Width           =   7665
      _ExtentX        =   13520
      _ExtentY        =   4392
      Caption         =   "Par�metros encontrados"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   13977088
      BackColor       =   14737632
      BorderColor     =   0
      ColorStyle      =   99
      Begin MSFlexGridLib.MSFlexGrid grdParametros 
         Height          =   2130
         Left            =   90
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   270
         Width           =   7485
         _ExtentX        =   13203
         _ExtentY        =   3757
         _Version        =   393216
         RowHeightMin    =   340
         BackColorBkg    =   14737632
         HighLight       =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Image Image2 
      Height          =   870
      Left            =   90
      Picture         =   "Form de Par�metros.frx":6784
      Stretch         =   -1  'True
      Top             =   90
      Width           =   870
   End
   Begin VB.Image Image4 
      Appearance      =   0  'Flat
      Height          =   375
      Left            =   0
      Picture         =   "Form de Par�metros.frx":A0CB
      Stretch         =   -1  'True
      Top             =   1035
      Width           =   7755
   End
   Begin VB.Image Image1 
      Height          =   1050
      Left            =   0
      Picture         =   "Form de Par�metros.frx":A201
      Stretch         =   -1  'True
      Top             =   0
      Width           =   7755
   End
End
Attribute VB_Name = "frmParametros"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public vAlterar As Boolean
Public vValorAnterior As String
Private Sub cmdConsultar_Click()

    vAlterar = False

    fraParametros.Visible = False
    fraNovo.Visible = False

    Set vObjOracle(0) = vClsTABELAS001.TabelaParametros(UCase(App.Title))

    If Not vObjOracle(0).EOF Then

        Call vClsDPK001.PreencheGrid(vObjOracle(0), grdParametros, True, 3, "1;2")

        fraParametros.Visible = True

    End If
    
End Sub
Private Sub cmdExcluir_Click()

    If fraNovo.Visible = False Then

        Exit Sub

    End If

    If txtNomeParametro = "" Then

        Call vClsDPK001.Informar("Informe o nome do par�metro desejado.")
        txtNomeParametro.SetFocus
        Exit Sub

    End If

    If txtValorParametro = "" Then

        Call vClsDPK001.Informar("Informe o valor do par�metro desejado.")
        txtValorParametro.SetFocus
        Exit Sub

    End If

    If vClsDPK001.Perguntar("Confirma opera��o?") = vbYes Then

        vAlterar = False

        Call vClsDPK001.CadastroParametro(2, Val(txtCodSoftware), txtNomeParametro, txtValorParametro)
        Call cmdConsultar_Click

        If Err.Number = 0 Then

            Call vClsDPK001.Informar("Opera��o realizada com sucesso.")

        End If

    End If
    
End Sub
Private Sub cmdGravar_Click()

    If fraNovo.Visible = False Then

        Exit Sub

    End If

    If txtNomeParametro = "" Then

        Call vClsDPK001.Informar("Informe o nome do par�metro desejado.")
        txtNomeParametro.SetFocus
        Exit Sub

    End If

    If txtValorParametro = "" Then

        Call vClsDPK001.Informar("Informe o valor do par�metro desejado.")
        txtValorParametro.SetFocus
        Exit Sub

    End If

    If vClsDPK001.Perguntar("Confirma opera��o?") = vbYes Then

        If vAlterar = False Then
        
            Call vClsDPK001.CadastroParametro(1, Val(txtCodSoftware), txtNomeParametro, txtValorParametro)
        
        Else
        
            Call vClsDPK001.CadastroParametro(4, Val(txtCodSoftware), txtNomeParametro, txtValorParametro, vValorAnterior)
        
        End If
        
        Call cmdConsultar_Click

        If Err.Number = 0 Then

            Call vClsDPK001.Informar("Opera��o realizada com sucesso.")

        End If

    End If
    
    vAlterar = False

End Sub
Private Sub cmdImprimir_Click()

    If fraNovo.Visible = True Then

        frmImpressoras.Show 1

    End If

End Sub
Private Sub cmdLimpar_Click()

    Call cmdConsultar_Click
    
End Sub
Private Sub cmdNovo_Click()

    txtNomeParametro.Text = ""
    txtValorParametro.Text = ""
    txtNomeParametro.Locked = False

    fraParametros.Visible = False
    fraNovo.Visible = True

    txtNomeParametro.SetFocus
    
    vAlterar = False

End Sub
Private Sub cmdSair_Click()

    Unload Me

End Sub
Private Sub Form_Load()

    Set vObjOracle(0) = vClsTABELAS001.TabelaInformacoesSistema(UCase(App.Title))
    txtCodSoftware = vObjOracle(0).Fields(2)
    txtNomeSoftware = UCase(vObjOracle(0).Fields(0)) & " - " & UCase(vObjOracle(0).Fields(1))

    Call cmdConsultar_Click

End Sub
Private Sub grdParametros_DblClick()

    txtNomeParametro.Text = grdParametros.TextMatrix(grdParametros.row, 1)
    txtValorParametro.Text = grdParametros.TextMatrix(grdParametros.row, 2)
    vValorAnterior = txtValorParametro.Text
    txtNomeParametro.Locked = True

    fraParametros.Visible = False
    fraNovo.Visible = True

    txtNomeParametro.SetFocus
    
    vAlterar = True

End Sub
Private Sub txtNomeParametro_GotFocus()

    Call vClsDPK001.SelecionaCampo(txtNomeParametro)

End Sub
Private Sub txtNomeParametro_KeyPress(KeyAscii As Integer)

    KeyAscii = vClsDPK001.Maiuscula(KeyAscii)

End Sub
Private Sub txtValorParametro_GotFocus()

    Call vClsDPK001.SelecionaCampo(txtValorParametro)

End Sub
Private Sub txtValorParametro_KeyPress(KeyAscii As Integer)

    KeyAscii = vClsDPK001.Maiuscula(KeyAscii)

End Sub
