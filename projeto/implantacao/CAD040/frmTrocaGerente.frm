VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form frmTrocaGerente 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Troca de Gerente / Representantes"
   ClientHeight    =   5310
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8715
   Icon            =   "frmTrocaGerente.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   354
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   581
   StartUpPosition =   2  'CenterScreen
   Begin Bot�o.cmd cmdEnviar 
      Height          =   585
      Left            =   4050
      TabIndex        =   10
      ToolTipText     =   "Selecionar Representante"
      Top             =   2460
      Width           =   645
      _ExtentX        =   1138
      _ExtentY        =   1032
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmTrocaGerente.frx":23D2
      PICN            =   "frmTrocaGerente.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.ComboBox cboNovoGerente 
      Height          =   315
      ItemData        =   "frmTrocaGerente.frx":2708
      Left            =   4770
      List            =   "frmTrocaGerente.frx":270A
      TabIndex        =   8
      Text            =   "Combo1"
      Top             =   1170
      Width           =   3855
   End
   Begin VB.ListBox lstRepresSelecionados 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2760
      Left            =   4800
      MultiSelect     =   1  'Simple
      Sorted          =   -1  'True
      TabIndex        =   6
      Top             =   1770
      Width           =   3825
   End
   Begin VB.ComboBox cboGerente 
      Height          =   315
      ItemData        =   "frmTrocaGerente.frx":270C
      Left            =   60
      List            =   "frmTrocaGerente.frx":270E
      TabIndex        =   4
      Text            =   "Combo1"
      Top             =   1170
      Width           =   3855
   End
   Begin VB.ListBox lstRepresentantes 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2760
      Left            =   90
      MultiSelect     =   1  'Simple
      Sorted          =   -1  'True
      TabIndex        =   2
      Top             =   1770
      Width           =   3825
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   0
      Top             =   810
      Width           =   8625
      _ExtentX        =   15214
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmTrocaGerente.frx":2710
      PICN            =   "frmTrocaGerente.frx":272C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdDevolver 
      Height          =   585
      Left            =   4050
      TabIndex        =   11
      ToolTipText     =   "Voltar Representante"
      Top             =   3330
      Width           =   645
      _ExtentX        =   1138
      _ExtentY        =   1032
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmTrocaGerente.frx":3406
      PICN            =   "frmTrocaGerente.frx":3422
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdSalvar 
      Height          =   690
      Left            =   7920
      TabIndex        =   12
      TabStop         =   0   'False
      ToolTipText     =   "Gravar Altera��es"
      Top             =   4560
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmTrocaGerente.frx":373C
      PICN            =   "frmTrocaGerente.frx":3758
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdMarcarTodos 
      Height          =   690
      Left            =   90
      TabIndex        =   13
      ToolTipText     =   "MarcarTodos"
      Top             =   4560
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmTrocaGerente.frx":4432
      PICN            =   "frmTrocaGerente.frx":444E
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdLimparTodos 
      Height          =   690
      Left            =   870
      TabIndex        =   14
      ToolTipText     =   "Desmarcar Todos"
      Top             =   4560
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmTrocaGerente.frx":5128
      PICN            =   "frmTrocaGerente.frx":5144
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      Caption         =   "Novo Gerente:"
      Height          =   195
      Left            =   4770
      TabIndex        =   9
      Top             =   960
      Width           =   1050
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      Caption         =   "Representantes:"
      Height          =   195
      Left            =   4800
      TabIndex        =   7
      Top             =   1560
      Width           =   1170
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "Gerentes:"
      Height          =   195
      Left            =   60
      TabIndex        =   5
      Top             =   960
      Width           =   690
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Representantes:"
      Height          =   195
      Left            =   90
      TabIndex        =   3
      Top             =   1560
      Width           =   1170
   End
End
Attribute VB_Name = "frmTrocaGerente"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cboGerente_Change()
     lstRepresentantes.Clear
End Sub

Private Sub cboGerente_Click()
    
    If cboGerente = "" Then Exit Sub
    
    vBanco.Parameters.Remove "PM_CIC"
    vBanco.Parameters.Add "PM_CIC", cboGerente.ItemData(cboGerente.ListIndex), 1
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, "PRODUCAO.PCK_CAD040.PR_SELECT_REPRESENTANTES(:PM_CURSOR, :PM_CIC)")
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR").Value
    
    Preenche_Combo lstRepresentantes, vObjOracle, "Pseudonimo", "COD_REPRES"
    
End Sub



Private Sub cmdDevolver_Click()
    For i = 0 To lstRepresSelecionados.ListCount - 1
        If lstRepresSelecionados.Selected(i) = True Then
           lstRepresentantes.AddItem lstRepresSelecionados.List(i)
           lstRepresentantes.ItemData(Me.lstRepresentantes.NewIndex) = Me.lstRepresSelecionados.ItemData(i)
        End If
    Next
    For i = lstRepresSelecionados.ListCount - 1 To 0 Step -1
        If lstRepresSelecionados.Selected(i) = True Then
           lstRepresSelecionados.RemoveItem (i)
        End If
    Next

End Sub

Private Sub cmdEnviar_Click()
    For i = 0 To lstRepresentantes.ListCount - 1
        If lstRepresentantes.Selected(i) = True Then
           lstRepresSelecionados.AddItem lstRepresentantes.List(i)
           lstRepresSelecionados.ItemData(lstRepresSelecionados.NewIndex) = lstRepresentantes.ItemData(i)
        End If
    Next
    For i = lstRepresentantes.ListCount - 1 To 0 Step -1
        If lstRepresentantes.Selected(i) = True Then
           lstRepresentantes.RemoveItem (i)
        End If
    Next
    cboNovoGerente.Enabled = lstRepresSelecionados.ListCount > 0
End Sub

Private Sub cmdLimparTodos_Click()
    For i = 0 To Me.lstRepresentantes.ListCount - 1
        lstRepresentantes.Selected(i) = False
    Next
End Sub

Private Sub cmdMarcarTodos_Click()
    For i = 0 To Me.lstRepresentantes.ListCount - 1
        lstRepresentantes.Selected(i) = True
    Next
End Sub

Private Sub cmdSalvar_Click()
    
    If cboNovoGerente = "" Then
       MsgBox "Selecione um novo gerente para os representantes selecionados.", vbInformation, "Aten��o"
       If cboNovoGerente.Enabled = True Then cboNovoGerente.SetFocus
       Exit Sub
    End If
    
    If cboGerente = cboNovoGerente Then
       MsgBox "O novo Gerente deve ser diferente do gerente atual.", vbInformation, "Aten��o"
       cboNovoGerente.SetFocus
       Exit Sub
    End If
    
    If lstRepresSelecionados.ListCount = 0 Then
       MsgBox "Selecione os representantes.", vbInformation, "Aten��o"
       Exit Sub
    End If
    
    'Atualizar o banco de dados
    vBanco.Parameters.Remove "PM_COD_GERENTE_NOVO"
    vBanco.Parameters.Add "PM_COD_GERENTE_NOVO", cboNovoGerente.ItemData(cboNovoGerente.ListIndex), 1
    
    For i = 0 To lstRepresSelecionados.ListCount - 1
        vBanco.Parameters.Remove "PM_COD_REPRES"
        vBanco.Parameters.Add "PM_COD_REPRES", lstRepresSelecionados.ItemData(i), 1
        
        vErro = vVB_Generica_001.ExecutaPl(vBanco, "PRODUCAO.PCK_CAD040.PR_UPDATE_CIC_GERENTE(:PM_COD_REPRES, :PM_COD_GERENTE_NOVO)")
        If vErro <> "" Then
            MsgBox vErro
            Exit Sub
        End If
    Next
    
    'Limpar Combo de Novo Gerente e List
    cboNovoGerente = ""
    lstRepresSelecionados.Clear
    
End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    
    Criar_Cursor vBanco.Parameters, "PM_CURSOR"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, "PRODUCAO.PCK_CAD040.PR_SELECT_GERENTEs(:PM_CURSOR)")
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR").Value
    
    Preenche_Combo cboGerente, vObjOracle, "Pseudonimo", "COD_REPRES"
    Preenche_Combo cboNovoGerente, vObjOracle, "Pseudonimo", "COD_REPRES"

    cboNovoGerente.Enabled = lstRepresSelecionados.ListCount > 0

End Sub

Public Sub Preenche_Combo(cbo As Object, Rst As Object, campoCombo As String, CampoItemData As String)
    
    cbo.Clear
    Rst.MoveFirst
    For i = 1 To Rst.RecordCount
        cbo.AddItem Rst(campoCombo)
        cbo.ItemData(cbo.NewIndex) = Rst(CampoItemData)
        Rst.MoveNext
    Next

End Sub
