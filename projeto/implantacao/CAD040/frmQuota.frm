VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmQuota 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "CADASTRO DE QUOTA"
   ClientHeight    =   5640
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6585
   Icon            =   "frmQuota.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   376
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   439
   StartUpPosition =   2  'CenterScreen
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   5850
      TabIndex        =   20
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmQuota.frx":08CA
      PICN            =   "frmQuota.frx":08E6
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   28
      Top             =   5310
      Width           =   6585
      _ExtentX        =   11615
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   11562
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin TabDlg.SSTab stbQuota 
      Height          =   4830
      Left            =   45
      TabIndex        =   27
      Top             =   360
      Width           =   6495
      _ExtentX        =   11456
      _ExtentY        =   8520
      _Version        =   393216
      Tabs            =   2
      Tab             =   1
      TabHeight       =   794
      ShowFocusRect   =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Cadastrar"
      TabPicture(0)   =   "frmQuota.frx":15C0
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "lblPREP"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "lblCod_Repres"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "lblPseudonimo"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "lblTipo"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "lblMes_Ano"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "lblQuota"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "lblDolar"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "lblQuota_Dolar"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "lblTelemarketing"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "mfgQuota"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "cmdCancelar"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "cmdGravar"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "cmdPesquisar"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).Control(13)=   "txtCod_Repres"
      Tab(0).Control(13).Enabled=   0   'False
      Tab(0).Control(14)=   "txtPseudonimo"
      Tab(0).Control(14).Enabled=   0   'False
      Tab(0).Control(15)=   "txtTipo"
      Tab(0).Control(15).Enabled=   0   'False
      Tab(0).Control(16)=   "txtMes_Ano"
      Tab(0).Control(16).Enabled=   0   'False
      Tab(0).Control(17)=   "txtQuota"
      Tab(0).Control(17).Enabled=   0   'False
      Tab(0).Control(18)=   "txtDolar"
      Tab(0).Control(18).Enabled=   0   'False
      Tab(0).Control(19)=   "txtQuota_Dolar"
      Tab(0).Control(19).Enabled=   0   'False
      Tab(0).Control(20)=   "txtTelemarketing"
      Tab(0).Control(20).Enabled=   0   'False
      Tab(0).ControlCount=   21
      TabCaption(1)   =   "Copiar"
      TabPicture(1)   =   "frmQuota.frx":15DC
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "lblAnterior"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "lblAtual"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "cmdCancelar_Copia"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "cmdCopiar"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).Control(4)=   "txtAnterior"
      Tab(1).Control(4).Enabled=   0   'False
      Tab(1).Control(5)=   "txtAtual"
      Tab(1).Control(5).Enabled=   0   'False
      Tab(1).ControlCount=   6
      Begin VB.TextBox txtTelemarketing 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   -70185
         TabIndex        =   9
         Top             =   2700
         Width           =   1500
      End
      Begin VB.TextBox txtQuota_Dolar 
         Appearance      =   0  'Flat
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "#,##0.0000"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1046
            SubFormatType   =   1
         EndProperty
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   -73650
         TabIndex        =   7
         Top             =   2700
         Width           =   1545
      End
      Begin VB.TextBox txtDolar 
         Appearance      =   0  'Flat
         BackColor       =   &H80000004&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   -70185
         Locked          =   -1  'True
         TabIndex        =   11
         Top             =   2295
         Width           =   1500
      End
      Begin VB.TextBox txtQuota 
         Appearance      =   0  'Flat
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "#,##0.0000"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1046
            SubFormatType   =   1
         EndProperty
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   -73650
         TabIndex        =   5
         Top             =   2295
         Width           =   1545
      End
      Begin VB.TextBox txtMes_Ano 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   -73650
         TabIndex        =   3
         Top             =   1890
         Width           =   1320
      End
      Begin VB.TextBox txtTipo 
         Appearance      =   0  'Flat
         BackColor       =   &H80000004&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   -73650
         Locked          =   -1  'True
         TabIndex        =   15
         Top             =   1485
         Width           =   2580
      End
      Begin VB.TextBox txtPseudonimo 
         Appearance      =   0  'Flat
         BackColor       =   &H80000004&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   -73650
         Locked          =   -1  'True
         TabIndex        =   13
         Top             =   1080
         Width           =   2580
      End
      Begin VB.TextBox txtCod_Repres 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   -73650
         TabIndex        =   1
         Top             =   675
         Width           =   825
      End
      Begin VB.TextBox txtAtual 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2880
         TabIndex        =   24
         Top             =   1485
         Width           =   1140
      End
      Begin VB.TextBox txtAnterior 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2880
         TabIndex        =   22
         Top             =   1080
         Width           =   1140
      End
      Begin Bot�o.cmd cmdCopiar 
         Height          =   690
         Left            =   4680
         TabIndex        =   25
         TabStop         =   0   'False
         ToolTipText     =   "Copiar Quota"
         Top             =   4005
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmQuota.frx":15F8
         PICN            =   "frmQuota.frx":1614
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdPesquisar 
         Height          =   690
         Left            =   -70950
         TabIndex        =   16
         TabStop         =   0   'False
         ToolTipText     =   "Consultar Quotas"
         Top             =   1125
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmQuota.frx":22EE
         PICN            =   "frmQuota.frx":230A
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdGravar 
         Height          =   690
         Left            =   -70095
         TabIndex        =   18
         TabStop         =   0   'False
         ToolTipText     =   "Gravar"
         Top             =   4005
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmQuota.frx":2FE4
         PICN            =   "frmQuota.frx":3000
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdCancelar 
         Height          =   690
         Left            =   -69330
         TabIndex        =   19
         TabStop         =   0   'False
         ToolTipText     =   "Cancelar"
         Top             =   4005
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmQuota.frx":3CDA
         PICN            =   "frmQuota.frx":3CF6
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdCancelar_Copia 
         Height          =   690
         Left            =   5445
         TabIndex        =   26
         TabStop         =   0   'False
         ToolTipText     =   "Cancelar"
         Top             =   4005
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmQuota.frx":45D0
         PICN            =   "frmQuota.frx":45EC
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MSFlexGridLib.MSFlexGrid mfgQuota 
         Height          =   1545
         Left            =   -74820
         TabIndex        =   17
         Top             =   3150
         Visible         =   0   'False
         Width           =   4560
         _ExtentX        =   8043
         _ExtentY        =   2725
         _Version        =   393216
         BackColorBkg    =   -2147483633
         HighLight       =   0
         ScrollBars      =   2
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblTelemarketing 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Caption         =   "% Telemarketing:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   -71805
         TabIndex        =   8
         Top             =   2745
         Width           =   1545
      End
      Begin VB.Label lblQuota_Dolar 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Caption         =   "Quota D�lar:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   -74820
         TabIndex        =   6
         Top             =   2745
         Width           =   1095
      End
      Begin VB.Label lblDolar 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Caption         =   "D�lar:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   -70950
         TabIndex        =   10
         Top             =   2340
         Width           =   690
      End
      Begin VB.Label lblQuota 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Caption         =   "Quota:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   -74820
         TabIndex        =   4
         Top             =   2340
         Width           =   1095
      End
      Begin VB.Label lblMes_Ano 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Caption         =   "M�s/Ano:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   -74820
         TabIndex        =   2
         Top             =   1935
         Width           =   1095
      End
      Begin VB.Label lblTipo 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Caption         =   "Tipo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   -74820
         TabIndex        =   14
         Top             =   1530
         Width           =   1095
      End
      Begin VB.Label lblPseudonimo 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Caption         =   "Pseudonimo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   -74820
         TabIndex        =   12
         Top             =   1125
         Width           =   1095
      End
      Begin VB.Label lblCod_Repres 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Caption         =   "C�digo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   -74910
         TabIndex        =   0
         Top             =   720
         Width           =   1185
      End
      Begin VB.Label lblPREP 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   285
         Left            =   -70875
         TabIndex        =   29
         Top             =   675
         Width           =   2040
      End
      Begin VB.Label lblAtual 
         Appearance      =   0  'Flat
         Caption         =   "M�s Atual:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   1935
         TabIndex        =   23
         Top             =   1530
         Width           =   960
      End
      Begin VB.Label lblAnterior 
         Appearance      =   0  'Flat
         Caption         =   "M�s Anterior:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   1710
         TabIndex        =   21
         Top             =   1125
         Width           =   1185
      End
   End
End
Attribute VB_Name = "frmQuota"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Num_CIC As Double
Public Vl_Dolar As Double
Private Sub cmdCancelar_Click()

    txtCod_Repres.Text = ""
    txtPseudonimo.Text = ""
    txtTipo.Text = ""
    txtMes_Ano.Text = ""
    txtQuota.Text = ""
    txtQuota_Dolar.Text = ""
    txtTelemarketing.Text = ""
    
    Call vVB_Generica_001.LimpaFlexGridComTitulo(mfgQuota)
    
    txtCod_Repres.Text = ""
    mfgQuota.Visible = False
    
End Sub

Private Sub cmdCancelar_Copia_Click()
    txtAnterior.Text = ""
    txtAtual.Text = ""
    txtAnterior.SetFocus
End Sub

Private Sub cmdCopiar_Click()

    Dim AAMM_ATUAL As Long
    Dim AAMM_ANT As Long
    Dim AAMM As Long
 
    If txtAtual.Text = "" Then
        Call vVB_Generica_001.Informar("Digite o M�S ATUAL!")
        txtAtual.SetFocus
        Exit Sub
    ElseIf Len(txtAtual) < 7 Then
        Call vVB_Generica_001.Informar("M�s Atual Inv�lido! Verifique.")
        txtAtual.SetFocus
        Exit Sub
    End If

    If txtAnterior.Text = "" Then
        Call vVB_Generica_001.Informar("Digite o M�S ANTERIOR!")
        txtAnterior.SetFocus
        Exit Sub
    ElseIf Len(txtAnterior) < 7 Then
        Call vVB_Generica_001.Informar("M�s Anterior Inv�lido! Verifique.")
        txtAnterior.SetFocus
        Exit Sub
    End If

    AAMM_ATUAL = Mid(txtAtual.Text, 4, 4) & Mid(txtAtual.Text, 1, 2)
    AAMM_ANT = Mid(txtAnterior.Text, 4, 4) & Mid(txtAnterior.Text, 1, 2)

    resp = MsgBox("ATEN��O !!! " & Chr(13) & _
                  "Os Representantes que j� possuirem Quota Cadastrada n�o ser�o afetados. " & Chr(13) & _
                  "Confirma C�pia de Quota dos Representantes?", vbYesNo, "CONFIRMA��O")

    If resp = vbNo Then
        txtAnterior.Text = ""
        txtAnterior.Text = ""
        txtAnterior.SetFocus
        Exit Sub
    End If

    Screen.MousePointer = 11

    AAMM = val(AAMM_ANT)
    
   '-- VERIFICA A QUOTA REFERENTE AO M�S E ANO SOLICITADOS
    vBanco.Parameters.Remove "PM_ANOMES"
    vBanco.Parameters.Add "PM_ANOMES", AAMM, 1, 2   '-- o n�mero 2 � para dizer que o par�metro ser� tipo number
    
    Criar_Cursor vBanco.Parameters, "vCursor"
    
    vSql = "Producao.PCK_CAD040.PR_BUSCA_QUOTA_ANOMES(:PM_ANOMES, NULL, :VCURSOR)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_QUOTA_ANOMES, entre em contato com o SUPORTE!")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("VCURSOR").Value
    End If
    
    If vObjOracle.EOF Then
        Call vVB_Generica_001.Informar("Quota n�o existente para este m�s.")
        txtAnterior.SetFocus
        Screen.MousePointer = 0
        Exit Sub
    End If

    AAMM = val(AAMM_ANT)
      
   '-- C�PIA DO M�S ANTERIOR PARA O M�S ATUAL
    vBanco.Parameters.Remove "PM_ANOMES"
    vBanco.Parameters.Add "PM_ANOMES", AAMM, 1, 2       '-- o n�mero 2 � para dizer que o par�metro ser� tipo number
    vBanco.Parameters.Remove "PM_ATUAL"
    vBanco.Parameters.Add "PM_ATUAL", AAMM_ATUAL, 1, 2  '-- o n�mero 2 � para dizer que o par�metro ser� tipo number
    
    Criar_Cursor vBanco.Parameters, "vCursor"
    
    vSql = "Producao.PCK_CAD040.PR_ATUALIZA_COPIA_QUOTA(:PM_ANOMES,:PM_ATUAL)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento da PR_ATUALIZA_COPIA_QUOTA, entre em contato com o SUPORTE!")
        Exit Sub
    End If
        
    Screen.MousePointer = 0
    Call vVB_Generica_001.Informar("C�pia de Quotas finalizada com sucesso!")
    txtAnterior.Text = ""
    txtAtual.Text = ""
    txtAnterior.SetFocus
    
End Sub

Private Sub cmdGravar_Click()
    Dim AnoMes As Long
    Dim ldPC_TLMK As Double
    
    tp_repres = ""
    tp_repres = Left(txtTipo.Text, 1)
    
    If (tp_repres = "R" Or tp_repres = "P") Then
        If (txtTelemarketing.Text = "" Or val(txtTelemarketing.Text) > 100) Then
            MsgBox "Percentual de Marketing inv�lido !"
            txtTelemarketing.Text = ""
            txtTelemarketing.SetFocus
            Exit Sub
        End If
    End If
    
    If txtTelemarketing.Text = "" And (tp_repres <> "R" And tp_repres <> "P") Then
        ldPC_TLMK = 0
    Else
        ldPC_TLMK = val(txtTelemarketing.Text)
    End If
    
    AnoMes = val(Mid(txtMes_Ano.Text, 4, 5) & Mid(txtMes_Ano.Text, 1, 2))
    
   '-- INSERE OS DADOS DA QUOTA
    vBanco.Parameters.Remove "PM_CIC"
    vBanco.Parameters.Add "PM_CIC", Num_CIC, 1
    vBanco.Parameters.Remove "PM_QUOTA"
    vBanco.Parameters.Add "PM_QUOTA", CDbl(txtQuota.Text), 1
    vBanco.Parameters.Remove "PM_QUOTAUS"
    vBanco.Parameters.Add "PM_QUOTAUS", CDbl(txtQuota_Dolar.Text), 1
    vBanco.Parameters.Remove "PM_PCTLMK"
    vBanco.Parameters.Add "PM_PCTLMK", ldPC_TLMK, 1
    vBanco.Parameters.Remove "PM_ANOMES"
    vBanco.Parameters.Add "PM_ANOMES", AnoMes, 1
    
    Criar_Cursor vBanco.Parameters, "vCursor"
    
    vSql = "Producao.PCK_CAD040.PR_ATUALIZA_QUOTA(:PM_CIC, :PM_QUOTA, " & _
           ":PM_QUOTAUS, :PM_PCTLMK, :PM_ANOMES)"
    
    
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento da PR_ATUALIZA_QUOTA, entre em contato com o SUPORTE!")
        Exit Sub
    End If
        
    Call vVB_Generica_001.Informar("Atualiza��o efetuada com sucesso!")
    
    Call cmdCancelar_Click

End Sub

Private Sub cmdPesquisar_Click()
    
   '-- BUSCA AS QUOTAS REGISTRADAS E CARREGA O GRID
    vBanco.Parameters.Remove "PM_CODREPRES"
    vBanco.Parameters.Add "PM_CODREPRES", txtCod_Repres.Text, 1
     
    Criar_Cursor vBanco.Parameters, "vCursor"
    
    vSql = "Producao.PCK_CAD040.PR_BUSCA_QUOTA(:PM_CODREPRES, :VCURSOR)"
     
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento PR_BUSCA_QUOTA, entre em contato com o SUPORTE!")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("VCURSOR").Value
    End If
    
    If Not vObjOracle.EOF Then
        Call vVB_Generica_001.CarregaGridTabela(mfgQuota, vObjOracle, 4)
    End If
    
    mfgQuota.Visible = True
    
End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub

Private Sub Form_Load()
    
   '-- VERIFICA O VALOR DO D�LAR CADASTRADO
    Criar_Cursor vBanco.Parameters, "vCursor"
    
    vSql = "Producao.PCK_CAD040.PR_BUSCA_DOLAR(:VCURSOR)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_DOLAR, entre em contato com o SUPORTE!")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("VCURSOR").Value
    End If
    
    If Not vObjOracle.EOF Then
        Vl_Dolar = vObjOracle!vl_uss
        txtDolar.Text = vObjOracle!vl_uss
    End If
        
End Sub

Private Sub txtCod_Repres_LostFocus()
    Dim TIPO As String
    
    If txtCod_Repres.Text = "" Then
        Exit Sub
    End If
    
    Screen.MousePointer = 11

   '-- BUSCA O PSEUDONIMO E OUTROS DADOS DO REPRESENTANTE
    Criar_Cursor vBanco.Parameters, "vCursor"
     
    Set vObjOracle = vVB_Venda_001.TabelaRepresentante(vBanco, val(txtCod_Repres.Text))
     
    If vObjOracle.EOF Then
        Call vVB_Generica_001.Informar("Representante n�o encontrado!")
        Call cmdCancelar_Click
    Else
        txtPseudonimo.Text = vObjOracle.Fields(3).Value
        TIPO = vObjOracle.Fields(16).Value
        
        cmdGravar.Enabled = True
       
        If TIPO = "F" Then
            txtTipo.Text = "F-Funcionario Promotor"
            MsgBox "N�o � poss�vel adicionar cotas para Funcion�rio Promotor.", vbInformation, "Aten��o"
            cmdGravar.Enabled = False
        ElseIf TIPO = "R" Then
            txtTipo.Text = "R-Representante"
        ElseIf TIPO = "A" Then
            txtTipo.Text = "A-Teleatendimento"
        ElseIf TIPO = "V" Then
            txtTipo.Text = "V-Televendas"
        ElseIf TIPO = "M" Then
            txtTipo.Text = "M-Telemisto"
        ElseIf TIPO = "G" Then
            txtTipo.Text = "G-Gerente"
        ElseIf TIPO = "P" Then
            txtTipo.Text = "P-Promotor"
        End If
     
        Num_CIC = vObjOracle!CIC.Value
   
    End If
            
                
            
    Screen.MousePointer = 0
    
    
    

End Sub

Private Sub txtMes_Ano_KeyPress(KeyAscii As Integer)
    
    Call MMAAAA(KeyAscii, txtMes_Ano.Text)

End Sub

Private Sub txtMes_Ano_LostFocus()
    Dim AnoMes As Double
    
    If txtMes_Ano.Text = "" Then
        Exit Sub
    ElseIf Len(txtMes_Ano.Text) < 7 Then
        MsgBox "Data Inv�lida."
        txtMes_Ano.SetFocus
        Exit Sub
    End If
    
    Screen.MousePointer = 11
    
    AnoMes = val(Mid(txtMes_Ano.Text, 4, 5) + Mid(txtMes_Ano.Text, 1, 2))
    
   '-- VERIFICA A QUOTA DO REPRESENTANTE REFERENTE AO M�S E ANO SOLICITADOS
    vBanco.Parameters.Remove "PM_ANOMES"
    vBanco.Parameters.Add "PM_ANOMES", AnoMes, 1
    vBanco.Parameters.Remove "PM_CIC"
    vBanco.Parameters.Add "PM_CIC", Num_CIC, 1
    
    Criar_Cursor vBanco.Parameters, "vCursor"
    
    vSql = "Producao.PCK_CAD040.PR_BUSCA_QUOTA_ANOMES(:PM_ANOMES, :PM_CIC, :VCURSOR)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_QUOTA_ANOMES, entre em contato com o SUPORTE!")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("VCURSOR").Value
    End If
    
    txtDolar.Text = Vl_Dolar
       
    If Mid(txtTipo.Text, 1, 1) = "R" Or Mid(txtTipo.Text, 1, 1) = "P" Then
        txtTelemarketing.Visible = True
        lblTelemarketing.Visible = True
    Else
        txtTelemarketing.Visible = False
        lblTelemarketing.Visible = False
    End If
    
    If Not vObjOracle.EOF Then
        flagleu = True
        txtQuota_Dolar.Text = Format(vObjOracle!QUOTA, "###,##0.0000")
        txtQuota.Text = Format(vObjOracle!QUOTA_FATURADA, "###,##0.0000")
        txtTelemarketing.Text = vObjOracle!PC_TLMK
    Else
        flagleu = False
    End If
    
    Screen.MousePointer = 0
    
    txtQuota.SetFocus

End Sub

Private Sub txtQuota_LostFocus()
    Dim DOLAR As Double
    
    If txtDolar.Text <> 0 Then
        DOLAR = CDbl(val(txtQuota.Text) / val(txtDolar.Text))
        txtQuota_Dolar.Text = Format(DOLAR, "###,##0.0000")
    End If
End Sub
