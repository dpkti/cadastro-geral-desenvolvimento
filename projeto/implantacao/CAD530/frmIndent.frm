VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form frmIndent 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Inclus�o Comiss�o Indent"
   ClientHeight    =   5205
   ClientLeft      =   135
   ClientTop       =   1515
   ClientWidth     =   9360
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5205
   ScaleWidth      =   9360
   Begin VB.TextBox txtSeq 
      Enabled         =   0   'False
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   2040
      MaxLength       =   5
      TabIndex        =   21
      Top             =   1440
      Width           =   255
   End
   Begin VB.Frame Frame3 
      Caption         =   "VALOR CONTABIL"
      ForeColor       =   &H000000FF&
      Height          =   855
      Left            =   960
      TabIndex        =   7
      Top             =   3120
      Width           =   3135
      Begin VB.TextBox txtVl_Contabil 
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   1440
         MaxLength       =   15
         TabIndex        =   8
         Top             =   360
         Width           =   1335
      End
      Begin VB.Label Label3 
         Caption         =   "VALOR"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   19
         Top             =   360
         Width           =   1335
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "COMISS�O REPRESENTANTE"
      ForeColor       =   &H000000FF&
      Height          =   855
      Left            =   960
      TabIndex        =   3
      Top             =   1920
      Width           =   3135
      Begin VB.TextBox txtComis1 
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   2160
         MaxLength       =   5
         TabIndex        =   4
         Top             =   360
         Width           =   615
      End
      Begin VB.Label Label4 
         Caption         =   "COMISS�O"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   18
         Top             =   360
         Width           =   1335
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "COMISS�O TELEMARKETING"
      ForeColor       =   &H000000FF&
      Height          =   855
      Left            =   5040
      TabIndex        =   5
      Top             =   1920
      Width           =   3135
      Begin VB.TextBox txtComisTl 
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   2280
         MaxLength       =   5
         TabIndex        =   6
         Top             =   360
         Width           =   615
      End
      Begin VB.Label Label8 
         Caption         =   "COMISS�O"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   17
         Top             =   360
         Width           =   1095
      End
   End
   Begin VB.ComboBox cboTp_pedido 
      ForeColor       =   &H00800000&
      Height          =   315
      Left            =   7080
      TabIndex        =   2
      Top             =   960
      Width           =   615
   End
   Begin VB.ComboBox cboTp_filial 
      ForeColor       =   &H00800000&
      Height          =   315
      Left            =   2040
      TabIndex        =   1
      Top             =   960
      Width           =   3015
   End
   Begin VB.ComboBox cboIndent 
      ForeColor       =   &H00800000&
      Height          =   315
      Left            =   2040
      TabIndex        =   0
      Top             =   480
      Width           =   1695
   End
   Begin Threed.SSFrame SSFrame1 
      Height          =   1095
      Left            =   5520
      TabIndex        =   9
      Top             =   3240
      Width           =   2655
      _Version        =   65536
      _ExtentX        =   4683
      _ExtentY        =   1931
      _StockProps     =   14
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Alignment       =   2
      Begin Threed.SSCommand SSCommand1 
         Height          =   495
         Left            =   120
         TabIndex        =   10
         Top             =   360
         Width           =   495
         _Version        =   65536
         _ExtentX        =   873
         _ExtentY        =   873
         _StockProps     =   78
         Picture         =   "frmIndent.frx":0000
      End
      Begin Threed.SSCommand SSCommand2 
         Height          =   495
         Left            =   1320
         TabIndex        =   12
         Top             =   360
         Width           =   495
         _Version        =   65536
         _ExtentX        =   873
         _ExtentY        =   873
         _StockProps     =   78
         Picture         =   "frmIndent.frx":0452
      End
      Begin Threed.SSCommand SSCommand3 
         Height          =   495
         Left            =   1920
         TabIndex        =   13
         Top             =   360
         Width           =   495
         _Version        =   65536
         _ExtentX        =   873
         _ExtentY        =   873
         _StockProps     =   78
         ForeColor       =   -2147483640
         Picture         =   "frmIndent.frx":076C
      End
      Begin Threed.SSCommand SSCommand4 
         Height          =   495
         Left            =   720
         TabIndex        =   11
         Top             =   360
         Width           =   495
         _Version        =   65536
         _ExtentX        =   873
         _ExtentY        =   873
         _StockProps     =   78
         Picture         =   "frmIndent.frx":0A86
      End
   End
   Begin VB.Label Label5 
      Caption         =   "SEQUENCIA"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   480
      TabIndex        =   20
      Top             =   1440
      Width           =   1455
   End
   Begin VB.Label Label1 
      Caption         =   "TIPO DE FILIAL"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   480
      TabIndex        =   16
      Top             =   960
      Width           =   1455
   End
   Begin VB.Label Label2 
      Caption         =   "TIPO DE PEDIDO"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   5400
      TabIndex        =   15
      Top             =   960
      Width           =   1575
   End
   Begin VB.Label Label9 
      Caption         =   "TIPO INDENT"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   480
      TabIndex        =   14
      Top             =   480
      Width           =   1455
   End
End
Attribute VB_Name = "frmIndent"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cboInternet_Change()

End Sub

Private Sub form_Load()
  Dim ss As Object
  Dim SQL As String
  Dim i As Long
  Dim tam As Long
  Dim strCateg_6 As String
  
  On Error GoTo TrataErro
  
  Screen.MousePointer = 11
  
intFl_OP = 2
txtVl_Contabil = 0
txtSeq = 0
'preenche combo INDENT
  V_SQL = "Begin producao.pck_cad530.pr_tp_indent(:vCursor,:vErro);END;"

  oradatabase.ExecuteSQL V_SQL
  
  Set ss = oradatabase.Parameters("vCursor").Value
  
  If oradatabase.Parameters("vErro").Value <> 0 Then
    Call Process_Line_Errors(SQL)
    Exit Sub
  End If
   
   For i = 1 To ss.RecordCount
     cboIndent.AddItem ss!tipo
     ss.MoveNext
   Next
  
  
  
  
  'preenche combo TIPO DE FILIAL
  V_SQL = "Begin producao.pck_cad530.pr_filial(:vCursor,:vErro);END;"

  oradatabase.ExecuteSQL V_SQL
  
  Set ss = oradatabase.Parameters("vCursor").Value
  
  If oradatabase.Parameters("vErro").Value <> 0 Then
    Call Process_Line_Errors(SQL)
    Exit Sub
  End If
   
   For i = 1 To ss.RecordCount
     cboTp_filial.AddItem ss!filial
     ss.MoveNext
   Next
   
   '=====================================================
   'preenche combo de tipo de pedido
   
     cboTp_pedido.AddItem "10"
     cboTp_pedido.AddItem "11"
     cboTp_pedido.AddItem "21"
     cboTp_pedido.AddItem "22"
   
  '=======================================================
  
  Screen.MousePointer = 0
  Exit Sub
  
TrataErro:
   If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
     Resume
   Else
     Call Process_Line_Errors(SQL)
   End If

End Sub

Private Sub SSCommand1_Click()
  On Error GoTo TrataErro

     Screen.MousePointer = 11
     If cboTp_filial = "" Or _
        cboIndent = "" Or _
        cboTp_pedido = "" Or _
        txtComis1 = "" Or _
        txtComisTl = "" Or _
        txtVl_Contabil = "" Then
        MsgBox "Para gravar � preciso preencher todos os campos", vbExclamation, "Aten��o"
        Screen.MousePointer = 0
        Exit Sub
     End If
        
     
          pl = "BEGIN"
     pl = pl & " Update vendas.comissao_indent set"
     pl = pl & " pc_comis_repr = :pc_comis_repr,"
     pl = pl & " pc_comis_tlmk = :pc_comis_tlmk,"
     pl = pl & " vl_contabil = :contabil"
     pl = pl & " where tp_indent = :tp_indent and"
     pl = pl & " tp_filial = :tp_filial and"
     pl = pl & " tp_pedido = :tp_pedido and"
     pl = pl & " sequencia = :seq and cod_class=3; "
     'pl = pl & " COMMIT;"
     pl = pl & " IF SQL%NOTFOUND THEN"
     pl = pl & " Insert into vendas.comissao_indent "
     pl = pl & " (tp_filial,tp_pedido,tp_indent,sequencia,"
     pl = pl & " pc_comis_repr,pc_comis_tlmk,vl_contabil, cod_class)"
     pl = pl & " values (:tp_filial,:tp_pedido,:tp_indent,"
     pl = pl & " :seq,:pc_comis_repr,:pc_comis_tlmk,:contabil, 3);"
     'pl = pl & " COMMIT;"
     pl = pl & " END IF;"
     pl = pl & " COMMIT;"
     pl = pl & "EXCEPTION"
     pl = pl & " WHEN OTHERS THEN"
     pl = pl & " ROLLBACK;"
     pl = pl & " :cod_errora := SQLCODE;"
     pl = pl & " :txt_errora := SQLERRM;"
     pl = pl & "END;"
     
     
     oradatabase.Parameters.Remove "tp_filial"
     oradatabase.Parameters.Add "tp_filial", Mid(Trim(cboTp_filial), 1, 1), 1
     oradatabase.Parameters.Remove "tp_pedido"
     oradatabase.Parameters.Add "tp_pedido", Mid(Trim(cboTp_pedido), 1, 2), 1
     oradatabase.Parameters.Remove "tp_indent"
     oradatabase.Parameters.Add "tp_indent", Mid(Trim(cboIndent), 1, 2), 1
     oradatabase.Parameters.Remove "seq"
     oradatabase.Parameters.Add "seq", txtSeq, 1
     oradatabase.Parameters.Remove "pc_comis_repr"
     oradatabase.Parameters.Add "pc_comis_repr", Trim(txtComis1), 1
     oradatabase.Parameters.Remove "pc_comis_tlmk"
     oradatabase.Parameters.Add "pc_comis_tlmk", Trim(txtComisTl), 1
     oradatabase.Parameters.Remove "contabil"
     oradatabase.Parameters.Add "contabil", txtVl_Contabil, 1
     
     
     oradatabase.Parameters.Remove "cod_errora"
     oradatabase.Parameters.Add "cod_errora", 0, 2
     oradatabase.Parameters.Remove "txt_errora"
     oradatabase.Parameters.Add "txt_errora", "", 2
          
          
     oradatabase.ExecuteSQL pl
     
     If Val(oradatabase.Parameters("cod_errora")) <> 0 Then
        MsgBox oradatabase.Parameters("txt_errora") & ".Ligue para o Depto de Sistemas"
        Screen.MousePointer = 0
        Exit Sub
    End If
    
    MsgBox "Inclus�o/Altera��o OK ", vbInformation, "Aten��o"
    Screen.MousePointer = 0
 
  Exit Sub
  
TrataErro:
   If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
     Resume
   Else
     Call Process_Line_Errors(SQL)
   End If
   

End Sub

Private Sub SSCommand2_Click()
   Call LIMPA_TELA
End Sub


Private Sub SSCommand3_Click()
  Unload Me
End Sub

Private Sub SSCommand4_Click()
 
 If cboIndent = "" Or cboTp_filial = "" Then
   MsgBox "Para executar a consulta � necess�rio escolher o tipo de INDENT e a FILIAL", vbInformation, "Aten��o"
   Exit Sub
 End If
 
 
 intFl_OP = 2
 frmPesquisa.Show 1

End Sub

Private Sub txtComis1_KeyPress(KeyAscii As Integer)
  KeyAscii = Valor(KeyAscii, txtComis1.Text)
End Sub


Private Sub txtComisTl_KeyPress(KeyAscii As Integer)
    KeyAscii = Valor(KeyAscii, txtComisTl.Text)
End Sub


Private Sub txtVl_Contabil_KeyPress(KeyAscii As Integer)
  KeyAscii = Valor(KeyAscii, txtVl_Contabil.Text)
End Sub


