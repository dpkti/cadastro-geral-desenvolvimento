VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form frmInclusao 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Inclus�o"
   ClientHeight    =   5265
   ClientLeft      =   75
   ClientTop       =   1425
   ClientWidth     =   9420
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5265
   ScaleWidth      =   9420
   Begin VB.ComboBox cboInternet 
      ForeColor       =   &H00800000&
      Height          =   315
      Left            =   2040
      TabIndex        =   0
      Top             =   360
      Width           =   855
   End
   Begin VB.ComboBox cboTp_filial 
      ForeColor       =   &H00800000&
      Height          =   315
      Left            =   2040
      TabIndex        =   1
      Top             =   840
      Width           =   3015
   End
   Begin VB.ComboBox cboTp_pedido 
      ForeColor       =   &H00800000&
      Height          =   315
      Left            =   7080
      TabIndex        =   2
      Top             =   840
      Width           =   615
   End
   Begin VB.Frame Frame2 
      Caption         =   "COMISS�O TELEMARKETING"
      ForeColor       =   &H000000FF&
      Height          =   1695
      Left            =   4440
      TabIndex        =   22
      Top             =   1920
      Width           =   3135
      Begin VB.TextBox txtComisTl 
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   2280
         MaxLength       =   5
         TabIndex        =   8
         Top             =   480
         Width           =   615
      End
      Begin VB.ComboBox cboAdic_T 
         ForeColor       =   &H00800000&
         Height          =   315
         Left            =   2280
         TabIndex        =   9
         Top             =   840
         Width           =   735
      End
      Begin VB.Label Label7 
         Caption         =   "REDU��O ADICIONAL"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   25
         Top             =   960
         Width           =   2415
      End
      Begin VB.Label Label8 
         Caption         =   "COMISS�O"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   23
         Top             =   600
         Width           =   1095
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "COMISS�O REPRESENTANTE"
      ForeColor       =   &H000000FF&
      Height          =   1695
      Left            =   360
      TabIndex        =   18
      Top             =   1920
      Width           =   3135
      Begin VB.TextBox txtComis3 
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   2160
         MaxLength       =   5
         TabIndex        =   6
         Top             =   960
         Width           =   615
      End
      Begin VB.TextBox txtComis2 
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   2160
         MaxLength       =   5
         TabIndex        =   5
         Top             =   600
         Width           =   615
      End
      Begin VB.TextBox txtComis1 
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   2160
         MaxLength       =   5
         TabIndex        =   4
         Top             =   240
         Width           =   615
      End
      Begin VB.ComboBox cboAdic_R 
         ForeColor       =   &H00800000&
         Height          =   315
         Left            =   2160
         TabIndex        =   7
         Top             =   1320
         Width           =   735
      End
      Begin VB.Label Label10 
         Caption         =   "REDU��O ADICIONAL"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   24
         Top             =   1320
         Width           =   2415
      End
      Begin VB.Label Label6 
         Caption         =   "COMISS�O3"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   21
         Top             =   960
         Width           =   1335
      End
      Begin VB.Label Label5 
         Caption         =   "COMISS�O2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   20
         Top             =   600
         Width           =   1335
      End
      Begin VB.Label Label4 
         Caption         =   "COMISS�O1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   19
         Top             =   240
         Width           =   1335
      End
   End
   Begin VB.ComboBox cboCategoria 
      ForeColor       =   &H00800000&
      Height          =   315
      Left            =   2040
      TabIndex        =   3
      Top             =   1320
      Width           =   855
   End
   Begin Threed.SSFrame SSFrame1 
      Height          =   1095
      Left            =   5760
      TabIndex        =   11
      Top             =   3960
      Width           =   2655
      _Version        =   65536
      _ExtentX        =   4683
      _ExtentY        =   1931
      _StockProps     =   14
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Alignment       =   2
      Begin Threed.SSCommand SSCommand4 
         Height          =   495
         Left            =   720
         TabIndex        =   13
         Top             =   360
         Width           =   495
         _Version        =   65536
         _ExtentX        =   873
         _ExtentY        =   873
         _StockProps     =   78
         Picture         =   "FRMINCLU.frx":0000
      End
      Begin Threed.SSCommand SSCommand3 
         Height          =   495
         Left            =   1920
         TabIndex        =   15
         Top             =   360
         Width           =   495
         _Version        =   65536
         _ExtentX        =   873
         _ExtentY        =   873
         _StockProps     =   78
         ForeColor       =   -2147483640
         Picture         =   "FRMINCLU.frx":0452
      End
      Begin Threed.SSCommand SSCommand2 
         Height          =   495
         Left            =   1320
         TabIndex        =   14
         Top             =   360
         Width           =   495
         _Version        =   65536
         _ExtentX        =   873
         _ExtentY        =   873
         _StockProps     =   78
         Picture         =   "FRMINCLU.frx":076C
      End
      Begin Threed.SSCommand SSCommand1 
         Height          =   495
         Left            =   120
         TabIndex        =   12
         Top             =   360
         Width           =   495
         _Version        =   65536
         _ExtentX        =   873
         _ExtentY        =   873
         _StockProps     =   78
         Picture         =   "FRMINCLU.frx":0A86
      End
   End
   Begin VB.Label Label9 
      Caption         =   "TIPO INTERNET"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   480
      TabIndex        =   26
      Top             =   360
      Width           =   1455
   End
   Begin VB.Label Label3 
      Caption         =   "CATEGORIA"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   480
      TabIndex        =   17
      Top             =   1320
      Width           =   1095
   End
   Begin VB.Label Label2 
      Caption         =   "TIPO DE PEDIDO"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   5400
      TabIndex        =   16
      Top             =   840
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "TIPO DE FILIAL"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   480
      TabIndex        =   10
      Top             =   840
      Width           =   1455
   End
End
Attribute VB_Name = "frmInclusao"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit





















Private Sub cboAdic_R_KeyPress(KeyAscii As Integer)
  KeyAscii = 0
  Beep
End Sub


Private Sub cboAdic_T_KeyPress(KeyAscii As Integer)
  KeyAscii = 0
  Beep
End Sub


Private Sub cboCategoria_Click()
  Dim ss As Object
  Dim tam As Long
  Dim i As Long
  
  On Error GoTo Trata_Erro
  
  If frmInclusao.cboTp_pedido.Text = "" Or _
     frmInclusao.cboCategoria.Text = "" Or _
     frmInclusao.cboTp_filial.Text = "" Or _
     frmInclusao.cboInternet.Text = "" Then
     MsgBox "Entre com todos os dados para fazer a consulta", vbExclamation, "Aten��o"
     Exit Sub
  End If
  
  Screen.MousePointer = 11
  SQL = "Select PC_COMISS_1,"
  SQL = SQL & " PC_COMISS_2,"
  SQL = SQL & " PC_COMISS_3,"
  SQL = SQL & " PC_COMISS_TLMK,"
  SQL = SQL & " FL_RED_REP,"
  SQL = SQL & " FL_RED_TLMK"
  SQL = SQL & " From VENDAS.COMISSAO_DPK"
  SQL = SQL & " Where TP_FILIAL = :TP_FILIAL AND"
  SQL = SQL & " TP_PEDIDO = :TP_PEDIDO AND"
  SQL = SQL & " CATEGORIA = :CATEGORIA AND"
  SQL = SQL & " COD_INTERNET = :COD_INTERNET"
  SQL = SQL & " Order by TP_FILIAL,TP_PEDIDO,"
  SQL = SQL & " CATEGORIA"


  oradatabase.Parameters.Remove "tp_filial"
  oradatabase.Parameters.Add "tp_filial", Mid(Trim(frmInclusao.cboTp_filial), 1, 1), 1
  oradatabase.Parameters.Remove "COD_INTERNET"
  oradatabase.Parameters.Add "COD_INTERNET", Mid(Trim(frmInclusao.cboInternet), 1, 1), 1
  oradatabase.Parameters.Remove "tp_pedido"
  oradatabase.Parameters.Add "tp_pedido", Mid(Trim(frmInclusao.cboTp_pedido), 1, 2), 1
  oradatabase.Parameters.Remove "categoria"
  oradatabase.Parameters.Add "categoria", Mid(Trim(frmInclusao.cboCategoria), 1, 1), 1
 
  Set ss = oradatabase.dbcreatedynaset(SQL, 0&)

  If ss.EOF Then
    MsgBox "N�o h� comiss�o cadastrada para sele��o escolhida", vbExclamation, "Aten��o"
    Screen.MousePointer = 0
    Exit Sub
  End If
  
  txtComis1 = ss!pc_comiss_1
  txtComis2 = ss!pc_comiss_2
  txtComis3 = ss!pc_comiss_3
  txtComisTl = ss!pc_comiss_tlmk
  
  
  tam = frmInclusao.cboAdic_R.ListCount
  For i = 0 To tam - 1
    If Mid(Trim(frmInclusao.cboAdic_R.List(i)), 1, 1) = ss!fl_red_rep Then
      frmInclusao.cboAdic_R = frmInclusao.cboAdic_R.List(i)
      Exit For
      End If
  Next i
  
  tam = frmInclusao.cboAdic_T.ListCount
  For i = 0 To tam - 1
    If Mid(Trim(frmInclusao.cboAdic_T.List(i)), 1, 1) = ss!fl_red_tlmk Then
      frmInclusao.cboAdic_T = frmInclusao.cboAdic_T.List(i)
      Exit For
      End If
  Next i

  Screen.MousePointer = 0
  Exit Sub
  
Trata_Erro:
If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
   Resume
Else
   Call Process_Line_Errors(SQL)
End If

End Sub

Private Sub cboCategoria_KeyPress(KeyAscii As Integer)
  KeyAscii = 0
  Beep
End Sub


Private Sub cboInternet_KeyPress(KeyAscii As Integer)
  KeyAscii = 0
  Beep
End Sub


Private Sub cboTp_filial_Click()
  
  frmInclusao.cboTp_pedido.Text = ""
  frmInclusao.cboCategoria.Text = ""
  frmInclusao.cboAdic_R.Text = ""
  frmInclusao.cboAdic_T.Text = ""
  
  frmInclusao.txtComis1.Text = ""
  frmInclusao.txtComis2.Text = ""
  frmInclusao.txtComis3.Text = ""
  frmInclusao.txtComisTl.Text = ""
  
End Sub

Private Sub cboTp_filial_KeyPress(KeyAscii As Integer)
  KeyAscii = 0
  Beep
End Sub


Private Sub cboTp_pedido_Click()
  
  frmInclusao.cboCategoria.Text = ""
  frmInclusao.cboAdic_R.Text = ""
  frmInclusao.cboAdic_T.Text = ""

  frmInclusao.txtComis1.Text = ""
  frmInclusao.txtComis2.Text = ""
  frmInclusao.txtComis3.Text = ""
  frmInclusao.txtComisTl.Text = ""
  
End Sub

Private Sub cboTp_pedido_KeyPress(KeyAscii As Integer)
  KeyAscii = 0
  Beep
End Sub


Private Sub form_Load()
  Dim ss As Object
  Dim SQL As String
  Dim i As Long
  Dim tam As Long
  Dim strCateg_6 As String
  
  On Error GoTo TrataErro
  
  intFl_OP = 1
  Screen.MousePointer = 11
  'preenche combo TIPO DE FILIAL
  V_SQL = "Begin producao.pck_cad530.pr_filial(:vCursor,:vErro);END;"

  oradatabase.ExecuteSQL V_SQL
  
  Set ss = oradatabase.Parameters("vCursor").Value
  
  If oradatabase.Parameters("vErro").Value <> 0 Then
    Call Process_Line_Errors(SQL)
    Exit Sub
  End If
   
   For i = 1 To ss.RecordCount
     cboTp_filial.AddItem ss!filial
     ss.MoveNext
   Next
   
   '=====================================================
   'preenche combo de tipo de pedido
   
     cboTp_pedido.AddItem "10"
     cboTp_pedido.AddItem "11"
     cboTp_pedido.AddItem "21"
     cboTp_pedido.AddItem "22"
   
  '=======================================================
  
  'preenche combo CATEGORIA
   V_SQL = "Begin producao.pck_cad530.pr_categoria(:vCursor,:vErro);END;"

  oradatabase.ExecuteSQL V_SQL
  
  Set ss = oradatabase.Parameters("vCursor").Value
  
  If oradatabase.Parameters("vErro").Value <> 0 Then
    Call Process_Line_Errors(SQL)
    Exit Sub
  End If
   
   cboCategoria.AddItem "0"
   For i = 1 To ss.RecordCount
     cboCategoria.AddItem ss!categoria
     ss.MoveNext
   Next
   strCateg_6 = "F"
   tam = frmInclusao.cboCategoria.ListCount
   For i = 0 To tam - 1
     If Mid(Trim(frmInclusao.cboCategoria.List(i)), 1, 2) = 6 Then
       strCateg_6 = "T"
     End If
   Next i
   If strCateg_6 = "F" Then
     cboCategoria.AddItem "6"
   End If
   
     
     cboCategoria.AddItem "9  - SP"
     cboCategoria.AddItem "10 - OF"
     
    
   '=========================================================
   'PREENCHE COMBO REDU��O DE COMISSAO ADICIONAL REPRESENTANTE
     cboAdic_R.AddItem "SIM"
     cboAdic_R.AddItem "NAO"
   
   '=========================================================
   'PREENCHE COMBO REDU��O DE COMISSAO ADICIONAL TELEMARKETING
     cboAdic_T.AddItem "SIM"
     cboAdic_T.AddItem "NAO"
   
   
   '=========================================================
   'PREENCHE COMBO DE TIPO INTERNET
   V_SQL = "Begin producao.pck_cad530.pr_tp_internet(:vCursor,:vErro);END;"

  oradatabase.ExecuteSQL V_SQL
  
  Set ss = oradatabase.Parameters("vCursor").Value
  
  If oradatabase.Parameters("vErro").Value <> 0 Then
    Call Process_Line_Errors(SQL)
    Exit Sub
  End If
   cboInternet.AddItem "N"
   For i = 1 To ss.RecordCount
     cboInternet.AddItem ss!COD_INTERNET
     ss.MoveNext
   Next
       
  

  Screen.MousePointer = 0
  Exit Sub
  
TrataErro:
   If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
     Resume
   Else
     Call Process_Line_Errors(SQL)
   End If

End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, x As Single, Y As Single)
   MDIForm1.ssMsg.Caption = ""
End Sub


Private Sub SSCommand1_Click()


  On Error GoTo TrataErro

     Screen.MousePointer = 11
     If cboTp_filial = "" Or _
        cboTp_pedido = "" Or _
        cboCategoria = "" Or _
        cboInternet = "" Or _
        cboAdic_R = "" Or _
        cboAdic_T = "" Or _
        txtComis1 = "" Or _
        txtComis2 = "" Or _
        txtComis3 = "" Or _
        txtComisTl = "" Then
        MsgBox "Para gravar � preciso preencher todos os campos", vbExclamation, "Aten��o"
        Screen.MousePointer = 0
        Exit Sub
     End If
        
        
     pl = "BEGIN"
     pl = pl & " Update vendas.comissao_dpk set"
     pl = pl & " pc_comiss_1 = :pc_comis_1,"
     pl = pl & " pc_comiss_2 = :pc_comis_2,"
     pl = pl & " pc_comiss_3 = :pc_comis_3,"
     pl = pl & " pc_comiss_tlmk = :pc_comis_tlmk,"
     pl = pl & " fl_red_rep = :fl_red_rep,"
     pl = pl & " fl_red_tlmk = :fl_red_tlmk"
     pl = pl & " where tp_filial = :tp_filial and"
     pl = pl & " tp_pedido = :tp_pedido and"
     pl = pl & " categoria = :categoria and"
     pl = pl & " cod_internet = :cod_internet; "
     
     pl = pl & " IF SQL%NOTFOUND THEN"
        pl = pl & " Insert into vendas.comissao_dpk "
        pl = pl & " (tp_filial,tp_pedido,categoria,"
        pl = pl & " pc_comiss_1,pc_comiss_2,pc_comiss_3,"
        pl = pl & " pc_comiss_tlmk,fl_red_rep,"
        pl = pl & " fl_red_tlmk,cod_internet)"
        pl = pl & " values (:tp_filial,:tp_pedido,:categoria,"
        pl = pl & " :pc_comis_1,:pc_comis_2,:pc_comis_3,"
        pl = pl & " :pc_comis_tlmk,:fl_red_rep,:fl_red_tlmk,:cod_internet);"
     pl = pl & " END IF;"
     
     ' Grava log (antigo mdb) -- TI-4169
     pl = pl & " INSERT INTO vendas.comissao_dpk_log"
     pl = pl & " (num_comiss, tp_filial, tp_pedido, categoria, pc_comiss_1, pc_comiss_2, pc_comiss_3,"
     pl = pl & "  pc_comiss_tlmk, fl_red_rep, fl_red_tlmk, usuario, cod_internet)"
     pl = pl & " VALUES(vendas.seq_comissao_dpk_LOG.nextval, :tp_filial, :tp_pedido, "
     pl = pl & "  :categoria, :pc_comis_1,:pc_comis_2,:pc_comis_3, :pc_comis_tlmk, "
     pl = pl & "  :fl_red_rep, :fl_red_tlmk,:usuario, :cod_internet );"
     
     pl = pl & " COMMIT;"
     
     pl = pl & "EXCEPTION"
     pl = pl & " WHEN OTHERS THEN"
     pl = pl & " ROLLBACK;"
     pl = pl & " :cod_errora := SQLCODE;"
     pl = pl & " :txt_errora := SQLERRM;"
     pl = pl & "END;"
                      
     oradatabase.Parameters.Remove "tp_filial"
     oradatabase.Parameters.Add "tp_filial", Mid(Trim(cboTp_filial), 1, 1), 1
     oradatabase.Parameters.Remove "tp_pedido"
     oradatabase.Parameters.Add "tp_pedido", Val(Mid(Trim(cboTp_pedido), 1, 2)), 1
     oradatabase.Parameters.Remove "categoria"
     oradatabase.Parameters.Add "categoria", Val(Mid(Trim(cboCategoria), 1, 2)), 1
     oradatabase.Parameters.Remove "cod_internet"
     oradatabase.Parameters.Add "cod_internet", Mid(Trim(cboInternet), 1, 1), 1
     oradatabase.Parameters.Remove "pc_comis_1"
     oradatabase.Parameters.Add "pc_comis_1", Trim(txtComis1), 1
     oradatabase.Parameters.Remove "pc_comis_2"
     oradatabase.Parameters.Add "pc_comis_2", Trim(txtComis2), 1
     oradatabase.Parameters.Remove "pc_comis_3"
     oradatabase.Parameters.Add "pc_comis_3", Trim(txtComis3), 1
     oradatabase.Parameters.Remove "pc_comis_tlmk"
     oradatabase.Parameters.Add "pc_comis_tlmk", Trim(txtComisTl), 1
     oradatabase.Parameters.Remove "fl_red_rep"
     oradatabase.Parameters.Add "fl_red_rep", Mid(Trim(cboAdic_R), 1, 1), 1
     oradatabase.Parameters.Remove "fl_red_tlmk"
     oradatabase.Parameters.Add "fl_red_tlmk", Mid(Trim(cboAdic_T), 1, 1), 1
     oradatabase.Parameters.Remove "usuario"
     oradatabase.Parameters.Add "usuario", strUser, 1
       
     oradatabase.Parameters.Remove "cod_errora"
     oradatabase.Parameters.Add "cod_errora", 0, 2
     oradatabase.Parameters.Remove "txt_errora"
     oradatabase.Parameters.Add "txt_errora", "", 2
                    
     oradatabase.ExecuteSQL pl
     
     If Val(oradatabase.Parameters("cod_errora")) <> 0 Then
        MsgBox oradatabase.Parameters("txt_errora") & ".Ligue para o Depto de Sistemas"
        Exit Sub
    End If

       ' TI-4169
       'dbAccess.Execute SQL, dbFailOnError
       'FreeLocks
       MsgBox "Inclus�o/Altera��o OK ", vbInformation, "Aten��o"
     'End If
     Screen.MousePointer = 0

  Exit Sub
  
TrataErro:
   If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
     Resume
   Else
     Call Process_Line_Errors(SQL)
   End If
   

End Sub

Private Sub SSCommand1_MouseMove(Button As Integer, Shift As Integer, x As Single, Y As Single)
   MDIForm1.ssMsg.Caption = "Salvar Inclus�o/Altera��o"
End Sub


Private Sub SSCommand2_Click()

  Call LIMPA_TELA


End Sub

Private Sub SSCommand2_MouseMove(Button As Integer, Shift As Integer, x As Single, Y As Single)
  MDIForm1.ssMsg.Caption = "Limpar a Tela"
End Sub


Private Sub SSCommand3_Click()
  Unload Me
End Sub

Private Sub SSCommand3_MouseMove(Button As Integer, Shift As Integer, x As Single, Y As Single)
  MDIForm1.ssMsg.Caption = "Sair do M�dulo de Inclus�o"
End Sub





Private Sub SSCommand4_Click()

 If cboTp_filial = "" Then
   MsgBox "Para efetuar a pesquisa selecione um tipo de filial", vbExclamation, "Aten��o"
   Exit Sub
 End If
 intFl_OP = 1
 frmPesquisa.Show 1

End Sub

Private Sub SSCommand4_MouseMove(Button As Integer, Shift As Integer, x As Single, Y As Single)
  MDIForm1.ssMsg.Caption = "Pesquisa"
End Sub


Private Sub txtComis1_KeyPress(KeyAscii As Integer)
  KeyAscii = Valor(KeyAscii, txtComis1.Text)
End Sub


Private Sub txtComis1_LostFocus()
  If txtComis1 = "" Then
    MsgBox "Este campo n�o pode ficar em branco", vbExclamation, "Aten��o"
    Exit Sub
  End If

  txtComis1 = FmtBR(txtComis1)
  
  If txtComis1 > 99.99 Then
    MsgBox "A comiss�o deve ser menor ou igual a 99.99", vbExclamation, "Aten��o"
    txtComis1.Text = ""
    Exit Sub
  End If
  
End Sub


Private Sub txtComis2_KeyPress(KeyAscii As Integer)
  KeyAscii = Valor(KeyAscii, txtComis2.Text)
End Sub


Private Sub txtComis2_LostFocus()
  If txtComis2 = "" Then
    MsgBox "Este campo n�o pode ficar em branco", vbExclamation, "Aten��o"
    
    Exit Sub
  End If
  txtComis2 = FmtBR(txtComis2)
  If txtComis2 > 99.99 Then
    MsgBox "A comiss�o deve ser menor ou igual a 99.99", vbExclamation, "Aten��o"
    txtComis2.Text = ""
    Exit Sub
  End If

End Sub


Private Sub txtComis3_KeyPress(KeyAscii As Integer)
  KeyAscii = Valor(KeyAscii, txtComis3.Text)
End Sub


Private Sub txtComis3_LostFocus()
  If txtComis3 = "" Then
    MsgBox "Este campo n�o pode ficar em branco", vbExclamation, "Aten��o"
    
    Exit Sub
  End If
  txtComis3 = FmtBR(txtComis3)
  If txtComis3 > 99.99 Then
    MsgBox "A comiss�o deve ser menor ou igual a 99.99", vbExclamation, "Aten��o"
    txtComis3.Text = ""
    Exit Sub
  End If

End Sub


Private Sub txtComisTl_KeyPress(KeyAscii As Integer)
  KeyAscii = Valor(KeyAscii, txtComisTl.Text)
End Sub


Private Sub txtComisTl_LostFocus()
  If txtComisTl = "" Then
    MsgBox "Este campo n�o pode ficar em branco", vbExclamation, "Aten��o"
    
    Exit Sub
  End If
  txtComisTl = FmtBR(txtComisTl)
  If txtComisTl > 99.99 Then
    MsgBox "A comiss�o deve ser menor ou igual a 99.99", vbExclamation, "Aten��o"
    txtComisTl.Text = ""
    Exit Sub
  End If

End Sub


