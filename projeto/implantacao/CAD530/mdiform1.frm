VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.MDIForm MDIForm1 
   BackColor       =   &H8000000C&
   Caption         =   "Cadastro de Comiss�o"
   ClientHeight    =   4230
   ClientLeft      =   1410
   ClientTop       =   1740
   ClientWidth     =   6720
   Icon            =   "mdiform1.frx":0000
   LinkTopic       =   "MDIForm1"
   Begin Threed.SSPanel SSPanel1 
      Align           =   1  'Align Top
      Height          =   495
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6720
      _Version        =   65536
      _ExtentX        =   11853
      _ExtentY        =   873
      _StockProps     =   15
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin Threed.SSCommand SSCommand2 
         Height          =   495
         Left            =   960
         TabIndex        =   4
         Top             =   0
         Width           =   495
         _Version        =   65536
         _ExtentX        =   873
         _ExtentY        =   873
         _StockProps     =   78
         Picture         =   "mdiform1.frx":030A
      End
      Begin Threed.SSCommand SSCommand3 
         Height          =   495
         Left            =   1680
         TabIndex        =   2
         Top             =   0
         Width           =   495
         _Version        =   65536
         _ExtentX        =   873
         _ExtentY        =   873
         _StockProps     =   78
         Picture         =   "mdiform1.frx":075C
      End
      Begin Threed.SSCommand SSCommand1 
         Height          =   495
         Left            =   240
         TabIndex        =   1
         Top             =   0
         Width           =   495
         _Version        =   65536
         _ExtentX        =   873
         _ExtentY        =   873
         _StockProps     =   78
         Picture         =   "mdiform1.frx":0A76
      End
   End
   Begin Threed.SSPanel ssMsg 
      Align           =   2  'Align Bottom
      Height          =   495
      Left            =   0
      TabIndex        =   3
      Top             =   3735
      Width           =   6720
      _Version        =   65536
      _ExtentX        =   11853
      _ExtentY        =   873
      _StockProps     =   15
      ForeColor       =   8388608
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Alignment       =   0
   End
   Begin VB.Menu mnuInclusir 
      Caption         =   "&Incluir"
   End
   Begin VB.Menu mnuIndent 
      Caption         =   "In&dent"
   End
   Begin VB.Menu mnuSair 
      Caption         =   "&Sair"
   End
   Begin VB.Menu mnuSobre 
      Caption         =   "S&obre"
   End
End
Attribute VB_Name = "MDIForm1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub MDIForm_Load()
    
    On Error GoTo TrataErro

    
    If App.PrevInstance Then
      MsgBox "J� EXISTE UMA INST�NCIA DO PROGRAMA NO AR"
      End
    End If
    
    'bFl_banco = 0  TI-4169
    'posicionar tela
    MDIForm1.Top = 0
    MDIForm1.Left = 0
    MDIForm1.Width = Screen.Width
    MDIForm1.Height = Screen.Height
    
    ' strUser = Environ("USER")
    strUser = BuscaUSERNAME
    
     'path na rede
    strPath = "H:\ORACLE\DADOS\32BITS\"
   ' strPath = "F:\ORACLE\DADOS\32BITS\"
    
        
    'Conexao oracle
    Set orasession = CreateObject("oracleinprocserver.xorasession")
    Set oradatabase = orasession.OpenDatabase("PRODUCAO", "CAD530" & "/PROD", 0&)
    'Set oradatabase = orasession.OpenDatabase("PRODUCAO", "PRODUCAO/DPK", 0&)
    'Set oradatabase = orasession.OpenDatabase("DESENV", "producao/des", 0&)
    
' TI-4169
    'COMPACTA E REPARA BANCO ACCESS
'    If Dir(strPath & "CAD530.BAK") <> "" Then
'      Kill strPath & "CAD530.BAK"
'    End If
'    FileCopy strPath & "CAD530.MDB", strPath & "CAD530.BAK"
'    DBEngine.CompactDatabase strPath & "CAD530.MDB", strPath & "CAD530NW.MDB", "", dbEncrypt
'    Kill strPath & "CAD530.MDB"
'    Name strPath & "CAD530NW.MDB" As strPath & "CAD530.MDB"
'    DBEngine.RepairDatabase strPath & "CAD530.MDB"
    
    'Conexao Access
'    Set dbAccess = OpenDatabase(strPath & "CAD530.MDB")
'    bFl_banco = 1
' TI-4169 fim
    
    Set OraParameters = oradatabase.Parameters
    OraParameters.Remove "vCursor"
    OraParameters.Add "vCursor", 0, 2
    OraParameters("vCursor").serverType = ORATYPE_CURSOR
    OraParameters("vCursor").DynasetOption = ORADYN_NO_BLANKSTRIP
    OraParameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
 
    OraParameters.Remove "vErro"
    OraParameters.Add "vErro", 0, 2
    OraParameters("vErro").serverType = ORATYPE_NUMBER
    
    Exit Sub


TrataErro:
   If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
     Resume
   Else
     Call Process_Line_Errors(SQL)
   End If

End Sub


Private Sub MDIForm_MouseMove(Button As Integer, Shift As Integer, x As Single, Y As Single)
  ssMsg.Caption = ""
End Sub



Private Sub mnuInclusir_Click()
  Call SSCommand1_Click
End Sub

Private Sub mnuIndent_Click()
  Call SSCommand2_Click
End Sub

Private Sub mnuSair_Click()
  Call SSCommand3_Click
End Sub

Private Sub mnuSobre_Click()
  frmSobre.Show 1
End Sub

Private Sub SSCommand1_Click()
  frmInclusao.Width = 9380
  frmInclusao.Top = 15
  frmInclusao.Left = 15
  frmInclusao.Height = 5440
  
  frmInclusao.Show
End Sub

Private Sub SSCommand1_MouseMove(Button As Integer, Shift As Integer, x As Single, Y As Single)
  ssMsg.Caption = "Incluir/Alterar Classifica��o"
End Sub





Private Sub SSCommand2_Click()
'  frmIndent.Width = 9380
'  frmIndent.Top = 15
'  frmIndent.Left = 15
'  frmIndent.Height = 5440
  
  frmIndent.Show
End Sub

Private Sub SSCommand3_Click()
  ' TI-4169
  Unload Me
  'dbAccess.Close
  'End
End Sub


Private Sub SSCommand3_MouseMove(Button As Integer, Shift As Integer, x As Single, Y As Single)
  ssMsg.Caption = "Sair do Programa"
End Sub


