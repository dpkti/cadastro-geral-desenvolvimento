Attribute VB_Name = "modCad167"
Option Explicit

Global Const ORATYPE_CURSOR = 102
Global Const ORADYN_NO_BLANKSTRIP = &H2&
Global Const ORAPARM_INPUT = 1              'CONSTANTE DE BIND INPUT
Global Const ORAPARM_OUTPUT = 2             'CONSTANTE DE BIND OUTPUT
Global Const ORAPARM_BOTH = 3               'CONSTANTE DE BIND INPUT/OUTPUT
Global Const ORATYPE_NUMBER = 2

Public Sub Criar_Cursor()
    vBanco.Parameters.Remove "PM_Cursor"
    vBanco.Parameters.Add "PM_Cursor", 0, 2
    vBanco.Parameters("PM_Cursor").serverType = ORATYPE_CURSOR
    vBanco.Parameters("PM_Cursor").DynasetOption = ORADYN_NO_BLANKSTRIP
    vBanco.Parameters("PM_Cursor").DynasetCacheParams 256, 16, 20, 2000, 0
End Sub

Public Sub Limpar()

    frmCadastro.txtAguaTelefone = ""
    frmCadastro.txtAluguel = ""
    frmCadastro.txtCombustiveis = ""
    frmCadastro.txtEncargosSociais = ""
    frmCadastro.txtEnergiaEletrica = ""
    frmCadastro.txtFinanceiras = ""
    frmCadastro.txtFrete = ""
    frmCadastro.txtOutras = ""
    frmCadastro.txtProLabore = ""
    frmCadastro.txtSalarios = ""
    frmCadastro.txtSeguro = ""
    frmCadastro.txtServicos = ""
    frmCadastro.txtTributosEstaduais = ""
    frmCadastro.txtTributosFederais = ""
    frmCadastro.txtTributosMunicipais = ""
    
    frmCadastro.cmdAlterar.Enabled = False
    frmCadastro.cmdConsultar.Enabled = False
    frmCadastro.cmdExcluir.Enabled = False
    frmCadastro.cmdInserir.Enabled = False
    
End Sub

Function Valor(ByVal keyascii As Integer, strCampo As String) As Integer
    
    If keyascii = 8 Then    'BACKSPACE
        Valor = keyascii
        Exit Function
    End If
    
    If Chr$(keyascii) = "," Then
        keyascii = Asc(".")
    End If
    
    If Chr$(keyascii) = "," Or Chr$(keyascii) = "." Then
        If InStr(strCampo, ",") > 0 Or InStr(strCampo, ".") > 0 Then
            keyascii = 0
            Beep
        End If
    Else
        If Chr$(keyascii) < "0" Or Chr$(keyascii) > "9" Then
            keyascii = 0
            Beep
        End If
    End If
    
    Valor = keyascii

End Function

Function Verifica_Valor(vValor As String) As Boolean
    Dim strValor As String
    Dim strValor_AUX As String
    Dim fl As Boolean
    
    strValor = Right$(vValor, 3)
    strValor_AUX = Mid$(strValor, 1, 1)
    
    If strValor_AUX <> "." Then
        MsgBox "Valor inv�lido, n�o est� no formato 0.00", vbCritical, "ATEN��O!!!"
        fl = False
    Else
        fl = True
    End If
    
    strValor = ""
    strValor_AUX = ""
    
    Verifica_Valor = fl

End Function

Public Sub Alterar_Limpar()

    frmCadastro.txtAguaTelefone = ""
    frmCadastro.txtAluguel = ""
    frmCadastro.txtCombustiveis = ""
    frmCadastro.txtEncargosSociais = ""
    frmCadastro.txtEnergiaEletrica = ""
    frmCadastro.txtFinanceiras = ""
    frmCadastro.txtFrete = ""
    frmCadastro.txtOutras = ""
    frmCadastro.txtProLabore = ""
    frmCadastro.txtSalarios = ""
    frmCadastro.txtSeguro = ""
    frmCadastro.txtServicos = ""
    frmCadastro.txtTributosEstaduais = ""
    frmCadastro.txtTributosFederais = ""
    frmCadastro.txtTributosMunicipais = ""

End Sub
