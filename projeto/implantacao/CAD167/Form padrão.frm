VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form frmCadastro 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cadastro"
   ClientHeight    =   8790
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4950
   Icon            =   "Form padr�o.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   586
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   330
   Begin VB.Frame fraBotoes 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   705
      Left            =   60
      TabIndex        =   39
      Top             =   8040
      Width           =   4845
      Begin Bot�o.cmd cmdInserir 
         Height          =   480
         Left            =   3450
         TabIndex        =   42
         TabStop         =   0   'False
         ToolTipText     =   "Consultar"
         Top             =   150
         Width           =   1260
         _ExtentX        =   2223
         _ExtentY        =   847
         BTYPE           =   3
         TX              =   "Incluir"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "Form padr�o.frx":23D2
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdAlterar 
         Height          =   480
         Left            =   150
         TabIndex        =   40
         TabStop         =   0   'False
         ToolTipText     =   "Consultar"
         Top             =   150
         Width           =   1260
         _ExtentX        =   2223
         _ExtentY        =   847
         BTYPE           =   3
         TX              =   "Alterar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "Form padr�o.frx":23EE
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdExcluir 
         Height          =   480
         Left            =   1807
         TabIndex        =   41
         TabStop         =   0   'False
         ToolTipText     =   "Consultar"
         Top             =   150
         Width           =   1260
         _ExtentX        =   2223
         _ExtentY        =   847
         BTYPE           =   3
         TX              =   "Excluir"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "Form padr�o.frx":240A
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdConfirmarAlteracao 
         Height          =   480
         Left            =   150
         TabIndex        =   43
         TabStop         =   0   'False
         ToolTipText     =   "Confirmar Altera��o"
         Top             =   150
         Visible         =   0   'False
         Width           =   1260
         _ExtentX        =   2223
         _ExtentY        =   847
         BTYPE           =   3
         TX              =   "Confirma Altera��o"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "Form padr�o.frx":2426
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
   End
   Begin VB.Frame fraDespesas 
      Appearance      =   0  'Flat
      Caption         =   "Despesas"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   6555
      Left            =   60
      TabIndex        =   8
      Top             =   1440
      Width           =   4845
      Begin VB.TextBox txtOutras 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2940
         MaxLength       =   15
         TabIndex        =   38
         Top             =   6120
         Width           =   1800
      End
      Begin VB.TextBox txtFinanceiras 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2940
         MaxLength       =   15
         TabIndex        =   36
         Top             =   5730
         Width           =   1800
      End
      Begin VB.TextBox txtFrete 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2940
         MaxLength       =   15
         TabIndex        =   34
         Top             =   5310
         Width           =   1800
      End
      Begin VB.TextBox txtSeguro 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2940
         MaxLength       =   15
         TabIndex        =   32
         Top             =   4860
         Width           =   1800
      End
      Begin VB.TextBox txtServicos 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2940
         MaxLength       =   15
         TabIndex        =   30
         Top             =   4470
         Width           =   1800
      End
      Begin VB.TextBox txtAluguel 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2940
         MaxLength       =   15
         TabIndex        =   28
         Top             =   4020
         Width           =   1800
      End
      Begin VB.TextBox txtEnergiaEletrica 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2940
         MaxLength       =   15
         TabIndex        =   26
         Top             =   3600
         Width           =   1800
      End
      Begin VB.TextBox txtAguaTelefone 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2940
         MaxLength       =   15
         TabIndex        =   24
         Top             =   3150
         Width           =   1800
      End
      Begin VB.TextBox txtTributosMunicipais 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2940
         MaxLength       =   15
         TabIndex        =   22
         Top             =   2730
         Width           =   1800
      End
      Begin VB.TextBox txtTributosEstaduais 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2940
         MaxLength       =   15
         TabIndex        =   20
         Top             =   2310
         Width           =   1800
      End
      Begin VB.TextBox txtTributosFederais 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2940
         MaxLength       =   15
         TabIndex        =   18
         Top             =   1890
         Width           =   1800
      End
      Begin VB.TextBox txtEncargosSociais 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2940
         MaxLength       =   15
         TabIndex        =   16
         Top             =   1470
         Width           =   1800
      End
      Begin VB.TextBox txtCombustiveis 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2940
         MaxLength       =   15
         TabIndex        =   14
         Top             =   1020
         Width           =   1800
      End
      Begin VB.TextBox txtSalarios 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2940
         MaxLength       =   15
         TabIndex        =   12
         Top             =   630
         Width           =   1800
      End
      Begin VB.TextBox txtProLabore 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0.00;(0.00)"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1046
            SubFormatType   =   1
         EndProperty
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2940
         MaxLength       =   15
         TabIndex        =   10
         Top             =   210
         Width           =   1800
      End
      Begin VB.Label lblOutras 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Outras"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   150
         TabIndex        =   37
         Top             =   6180
         Width           =   540
      End
      Begin VB.Label lblFinanceiras 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Financeiras"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   150
         TabIndex        =   35
         Top             =   5790
         Width           =   870
      End
      Begin VB.Label lblFrete 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Frete"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   150
         TabIndex        =   33
         Top             =   5370
         Width           =   435
      End
      Begin VB.Label lblSeguros 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Seguro"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   150
         TabIndex        =   31
         Top             =   4920
         Width           =   585
      End
      Begin VB.Label lblServicos 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Servi�os"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   150
         TabIndex        =   29
         Top             =   4530
         Width           =   660
      End
      Begin VB.Label lblAluguel 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Aluguel"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   150
         TabIndex        =   27
         Top             =   4080
         Width           =   600
      End
      Begin VB.Label lblEnergiaEletrica 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Energia el�trica"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   150
         TabIndex        =   25
         Top             =   3660
         Width           =   1245
      End
      Begin VB.Label lblAguaTelefone 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "�gua/Telefone"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   150
         TabIndex        =   23
         Top             =   3210
         Width           =   1230
      End
      Begin VB.Label lblTributosMunicipais 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Tributos Municipais"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   150
         TabIndex        =   21
         Top             =   2790
         Width           =   1530
      End
      Begin VB.Label lblProLabore 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Pr� labore"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   150
         TabIndex        =   9
         Top             =   270
         Width           =   825
      End
      Begin VB.Label lblSalarios 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "S�larios"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   150
         TabIndex        =   11
         Top             =   690
         Width           =   585
      End
      Begin VB.Label lblCombustiveis 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Combust�veis"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   150
         TabIndex        =   13
         Top             =   1080
         Width           =   1050
      End
      Begin VB.Label lblEncargosSociais 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Encargos Sociais"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   150
         TabIndex        =   15
         Top             =   1530
         Width           =   1320
      End
      Begin VB.Label lblTributosFederais 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Tributos Federais"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   150
         TabIndex        =   17
         Top             =   1950
         Width           =   1395
      End
      Begin VB.Label lblTributosEstaduais 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Tributos Estaduais"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   150
         TabIndex        =   19
         Top             =   2370
         Width           =   1485
      End
   End
   Begin VB.Frame fraCdAno 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   765
      Left            =   60
      TabIndex        =   2
      Top             =   660
      Width           =   4845
      Begin VB.ComboBox cboLoja 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         ItemData        =   "Form padr�o.frx":2442
         Left            =   90
         List            =   "Form padr�o.frx":244C
         TabIndex        =   5
         Top             =   345
         Width           =   1725
      End
      Begin VB.ComboBox cboAno 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         ItemData        =   "Form padr�o.frx":2460
         Left            =   1980
         List            =   "Form padr�o.frx":2470
         TabIndex        =   6
         Top             =   345
         Width           =   1725
      End
      Begin Bot�o.cmd cmdConsultar 
         Height          =   450
         Left            =   4020
         TabIndex        =   7
         TabStop         =   0   'False
         ToolTipText     =   "Consultar"
         Top             =   240
         Width           =   450
         _ExtentX        =   794
         _ExtentY        =   794
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "Form padr�o.frx":2488
         PICN            =   "Form padr�o.frx":24A4
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "CD"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   90
         TabIndex        =   3
         Top             =   150
         Width           =   210
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "ANO"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   1980
         TabIndex        =   4
         Top             =   150
         Width           =   330
      End
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      Top             =   630
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   570
      Left            =   45
      TabIndex        =   0
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   15
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1005
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Form padr�o.frx":283E
      PICN            =   "Form padr�o.frx":285A
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmCadastro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim vCodLoja As Integer
Dim vAno As Integer
Dim vOpcao As String
Dim vLost As Boolean

Private Sub cboAno_DropDown()
    
    Limpar
    cmdConsultar.Enabled = True
    cmdConfirmarAlteracao.Visible = False
    cmdAlterar.Visible = True
    
End Sub

Private Sub cmdAlterar_Click()
    
    fraDespesas.Enabled = True
    cmdExcluir.Enabled = False
    cmdAlterar.Enabled = False
    Alterar_Limpar
    frmCadastro.txtProLabore.SetFocus
    cmdConfirmarAlteracao.Visible = True
    cmdAlterar.Visible = False

End Sub

Private Sub cmdConfirmarAlteracao_Click()
    Dim vFL As Boolean
    Dim vValor As String

    vCodLoja = Left(cboLoja, 2)
    vAno = cboAno
    vOpcao = "A"

    If MsgBox("Confirma altera��o?", vbYesNo, "ATEN��O!!!") = vbYes Then

        If Trim(frmCadastro.txtProLabore) = "" Then
            MsgBox "Dados Inv�lidos, o valor n�o foi digitado", vbCritical, "ATEN��O!!!"
            frmCadastro.txtProLabore.SetFocus
            Exit Sub
        Else
            vValor = frmCadastro.txtProLabore
            vFL = Verifica_Valor(vValor)
            If vFL = False Then
                frmCadastro.txtProLabore.SetFocus
                Exit Sub
            End If
        End If

        If Trim(frmCadastro.txtSalarios) = "" Then
            MsgBox "Dados Inv�lidos, o valor n�o foi digitado", vbCritical, "ATEN��O!!!"
            frmCadastro.txtSalarios.SetFocus
            Exit Sub
        Else
            vValor = frmCadastro.txtSalarios
            vFL = Verifica_Valor(vValor)
            If vFL = False Then
                frmCadastro.txtSalarios.SetFocus
                Exit Sub
            End If
        End If

        If Trim(frmCadastro.txtCombustiveis) = "" Then
            MsgBox "Dados Inv�lidos, o valor n�o foi digitado", vbCritical, "ATEN��O!!!"
            frmCadastro.txtCombustiveis.SetFocus
            Exit Sub
        Else
            vValor = frmCadastro.txtCombustiveis
            vFL = Verifica_Valor(vValor)
            If vFL = False Then
                frmCadastro.txtCombustiveis.SetFocus
                Exit Sub
            End If
        End If

        If Trim(frmCadastro.txtEncargosSociais) = "" Then
            MsgBox "Dados Inv�lidos, o valor n�o foi digitado", vbCritical, "ATEN��O!!!"
            frmCadastro.txtEncargosSociais.SetFocus
            Exit Sub
        Else
            vValor = frmCadastro.txtEncargosSociais
            vFL = Verifica_Valor(vValor)
            If vFL = False Then
                frmCadastro.txtEncargosSociais.SetFocus
                Exit Sub
            End If
        End If

        If Trim(frmCadastro.txtTributosFederais) = "" Then
            MsgBox "Dados Inv�lidos, o valor n�o foi digitado", vbCritical, "ATEN��O!!!"
            frmCadastro.txtTributosFederais.SetFocus
            Exit Sub
        Else
            vValor = frmCadastro.txtTributosFederais
            vFL = Verifica_Valor(vValor)
            If vFL = False Then
                frmCadastro.txtTributosFederais.SetFocus
                Exit Sub
            End If
        End If

        If Trim(frmCadastro.txtTributosEstaduais) = "" Then
            MsgBox "Dados Inv�lidos, o valor n�o foi digitado", vbCritical, "ATEN��O!!!"
            frmCadastro.txtTributosEstaduais.SetFocus
            Exit Sub
        Else
            vValor = frmCadastro.txtTributosEstaduais
            vFL = Verifica_Valor(vValor)
            If vFL = False Then
                frmCadastro.txtTributosEstaduais.SetFocus
                Exit Sub
            End If
        End If

        If Trim(frmCadastro.txtTributosMunicipais) = "" Then
            MsgBox "Dados Inv�lidos, o valor n�o foi digitado", vbCritical, "ATEN��O!!!"
            frmCadastro.txtTributosMunicipais.SetFocus
            Exit Sub
        Else
            vValor = frmCadastro.txtTributosMunicipais
            vFL = Verifica_Valor(vValor)
            If vFL = False Then
                frmCadastro.txtTributosMunicipais.SetFocus
                Exit Sub
            End If
        End If

        If Trim(frmCadastro.txtAguaTelefone) = "" Then
            MsgBox "Dados Inv�lidos, o valor n�o foi digitado", vbCritical, "ATEN��O!!!"
            frmCadastro.txtAguaTelefone.SetFocus
            Exit Sub
        Else
            vValor = frmCadastro.txtAguaTelefone
            vFL = Verifica_Valor(vValor)
            If vFL = False Then
                frmCadastro.txtAguaTelefone.SetFocus
                Exit Sub
            End If
        End If

        If Trim(frmCadastro.txtEnergiaEletrica) = "" Then
            MsgBox "Dados Inv�lidos, o valor n�o foi digitado", vbCritical, "ATEN��O!!!"
            frmCadastro.txtEnergiaEletrica.SetFocus
            Exit Sub
        Else
            vValor = frmCadastro.txtEnergiaEletrica
            vFL = Verifica_Valor(vValor)
            If vFL = False Then
                frmCadastro.txtEnergiaEletrica.SetFocus
                Exit Sub
            End If
        End If

        If Trim(frmCadastro.txtAluguel) = "" Then
            MsgBox "Dados Inv�lidos, o valor n�o foi digitado", vbCritical, "ATEN��O!!!"
            frmCadastro.txtAluguel.SetFocus
            Exit Sub
        Else
            vValor = frmCadastro.txtAluguel
            vFL = Verifica_Valor(vValor)
            If vFL = False Then
                frmCadastro.txtAluguel.SetFocus
                Exit Sub
            End If
        End If

        If Trim(frmCadastro.txtServicos) = "" Then
            MsgBox "Dados Inv�lidos, o valor n�o foi digitado", vbCritical, "ATEN��O!!!"
            frmCadastro.txtServicos.SetFocus
            Exit Sub
        Else
            vValor = frmCadastro.txtServicos
            vFL = Verifica_Valor(vValor)
            If vFL = False Then
                frmCadastro.txtServicos.SetFocus
                Exit Sub
            End If
        End If

        If Trim(frmCadastro.txtSeguro) = "" Then
            MsgBox "Dados Inv�lidos, o valor n�o foi digitado", vbCritical, "ATEN��O!!!"
            frmCadastro.txtSeguro.SetFocus
            Exit Sub
        Else
            vValor = frmCadastro.txtSeguro
            vFL = Verifica_Valor(vValor)
            If vFL = False Then
                frmCadastro.txtSeguro.SetFocus
                Exit Sub
            End If
        End If

        If Trim(frmCadastro.txtFrete) = "" Then
            MsgBox "Dados Inv�lidos, o valor n�o foi digitado", vbCritical, "ATEN��O!!!"
            frmCadastro.txtFrete.SetFocus
            Exit Sub
        Else
            vValor = frmCadastro.txtFrete
            vFL = Verifica_Valor(vValor)
            If vFL = False Then
                frmCadastro.txtFrete.SetFocus
                Exit Sub
            End If
        End If

        If Trim(frmCadastro.txtFinanceiras) = "" Then
            MsgBox "Dados Inv�lidos, o valor n�o foi digitado", vbCritical, "ATEN��O!!!"
            frmCadastro.txtFinanceiras.SetFocus
            Exit Sub
        Else
            vValor = frmCadastro.txtFinanceiras
            vFL = Verifica_Valor(vValor)
            If vFL = False Then
                frmCadastro.txtFinanceiras.SetFocus
                Exit Sub
            End If
        End If

        If Trim(frmCadastro.txtOutras) = "" Then
            MsgBox "Dados Inv�lidos, o valor n�o foi digitado", vbCritical, "ATEN��O!!!"
            frmCadastro.txtOutras.SetFocus
            Exit Sub
        Else
            vValor = frmCadastro.txtOutras
            vFL = Verifica_Valor(vValor)
            If vFL = False Then
                frmCadastro.txtOutras.SetFocus
                Exit Sub
            End If
        End If

        Criar_Cursor

        vBanco.Parameters.Remove "Pm_Opcao"
        vBanco.Parameters.Add "Pm_Opcao", vOpcao, 1

        vBanco.Parameters.Remove "Pm_CodLoja"
        vBanco.Parameters.Add "Pm_CodLoja", vCodLoja, 1

        vBanco.Parameters.Remove "Pm_Ano"
        vBanco.Parameters.Add "Pm_Ano", vAno, 1

        vBanco.Parameters.Remove "pm_pro_labore"
        vBanco.Parameters.Add "pm_pro_labore", txtProLabore, 1

        vBanco.Parameters.Remove "pm_salarios"
        vBanco.Parameters.Add "pm_salarios", txtSalarios, 1

        vBanco.Parameters.Remove "pm_combustiveis"
        vBanco.Parameters.Add "pm_combustiveis", txtCombustiveis, 1

        vBanco.Parameters.Remove "pm_encargos_sociais"
        vBanco.Parameters.Add "pm_encargos_sociais", txtEncargosSociais, 1

        vBanco.Parameters.Remove "pm_tributos_federais"
        vBanco.Parameters.Add "pm_tributos_federais", txtTributosFederais, 1

        vBanco.Parameters.Remove "pm_tributos_estaduais"
        vBanco.Parameters.Add "pm_tributos_estaduais", txtTributosEstaduais, 1

        vBanco.Parameters.Remove "pm_tributos_municipais"
        vBanco.Parameters.Add "pm_tributos_municipais", txtTributosMunicipais, 1

        vBanco.Parameters.Remove "pm_agua_telefone"
        vBanco.Parameters.Add "pm_agua_telefone", txtAguaTelefone, 1

        vBanco.Parameters.Remove "pm_energia_eletrica"
        vBanco.Parameters.Add "pm_energia_eletrica", txtEnergiaEletrica, 1

        vBanco.Parameters.Remove "pm_aluguel"
        vBanco.Parameters.Add "pm_aluguel", txtAluguel, 1

        vBanco.Parameters.Remove "pm_servicos"
        vBanco.Parameters.Add "pm_servicos", txtServicos, 1

        vBanco.Parameters.Remove "pm_seguro"
        vBanco.Parameters.Add "pm_seguro", txtSeguro, 1

        vBanco.Parameters.Remove "pm_frete"
        vBanco.Parameters.Add "pm_frete", txtFrete, 1

        vBanco.Parameters.Remove "pm_despesas_financeiras"
        vBanco.Parameters.Add "pm_despesas_financeiras", txtFinanceiras, 1

        vBanco.Parameters.Remove "pm_outras_despesas"
        vBanco.Parameters.Add "pm_outras_despesas", txtOutras, 1

        vBanco.Parameters.Remove "pm_erro"
        vBanco.Parameters.Add "pm_erro", 0, 2

        vBanco.Parameters.Remove "pm_desc_erro"
        vBanco.Parameters.Add "pm_desc_erro", " ", 2

        vSql = "Producao.PCK_CAD167.pr_loja_despesas(:Pm_cursor,:Pm_Opcao,:Pm_codLoja,:Pm_Ano, :pm_pro_labore, :pm_salarios, :pm_combustiveis, :pm_encargos_sociais, :pm_tributos_federais, :pm_tributos_estaduais, :pm_tributos_municipais,:pm_agua_telefone, :pm_energia_eletrica, :pm_aluguel, :pm_servicos, :pm_seguro, :pm_frete, :pm_despesas_financeiras, :pm_outras_despesas, :pm_erro, :pm_desc_erro)"

        vVB_Generica_001.ExecutaPl vBanco, vSql

        MsgBox "Altera��o efetuada com sucesso", vbInformation, "ATEN��O!!!"

        Limpar
        cmdConfirmarAlteracao.Visible = False
        cmdAlterar.Visible = True
        cboAno.ListIndex = 0
        Exit Sub
    Else
        MsgBox "Altera��o n�o efetuada", vbCritical, "ATEN��O!!!"
        Limpar
        cmdConfirmarAlteracao.Visible = False
        cmdAlterar.Visible = True
        cboAno.ListIndex = 0
        Exit Sub
    End If
End Sub

Private Sub cmdConsultar_Click()
    
    On Error GoTo TrataErro
    
    If Trim(cboLoja) = "" And Trim(cboAno) = "" Then
        MsgBox "Para est� consulta � necessario informar o CD e ano", vbInformation, "ATEN��O!!!"
        Exit Sub
    End If
    
    cmdConsultar.Enabled = False
    
    fraDespesas.Enabled = True

    vCodLoja = Left(cboLoja, 2)
    vAno = cboAno
    vOpcao = "S"
    
    Criar_Cursor
    
    vBanco.Parameters.Remove "Pm_Opcao"
    vBanco.Parameters.Add "Pm_Opcao", vOpcao, 1
    
    vBanco.Parameters.Remove "Pm_CodLoja"
    vBanco.Parameters.Add "Pm_CodLoja", vCodLoja, 1
    
    vBanco.Parameters.Remove "Pm_Ano"
    vBanco.Parameters.Add "Pm_Ano", vAno, 1
    
    vBanco.Parameters.Remove "pm_pro_labore"
    vBanco.Parameters.Add "pm_pro_labore", Me.txtProLabore, 1
    
    vBanco.Parameters.Remove "pm_salarios"
    vBanco.Parameters.Add "pm_salarios", Me.txtSalarios, 1
    
    vBanco.Parameters.Remove "pm_combustiveis"
    vBanco.Parameters.Add "pm_combustiveis", Me.txtCombustiveis, 1
    
    vBanco.Parameters.Remove "pm_encargos_sociais"
    vBanco.Parameters.Add "pm_encargos_sociais", Me.txtEncargosSociais, 1
        
    vBanco.Parameters.Remove "pm_tributos_federais"
    vBanco.Parameters.Add "pm_tributos_federais", Me.txtTributosFederais, 1
    
    vBanco.Parameters.Remove "pm_tributos_estaduais"
    vBanco.Parameters.Add "pm_tributos_estaduais", Me.txtTributosEstaduais, 1
    
    vBanco.Parameters.Remove "pm_tributos_municipais"
    vBanco.Parameters.Add "pm_tributos_municipais", Me.txtTributosMunicipais, 1
    
    vBanco.Parameters.Remove "pm_agua_telefone"
    vBanco.Parameters.Add "pm_agua_telefone", Me.txtAguaTelefone, 1
    
    vBanco.Parameters.Remove "pm_energia_eletrica"
    vBanco.Parameters.Add "pm_energia_eletrica", Me.txtEnergiaEletrica, 1
    
    vBanco.Parameters.Remove "pm_aluguel"
    vBanco.Parameters.Add "pm_aluguel", Me.txtAluguel, 1
    
    vBanco.Parameters.Remove "pm_servicos"
    vBanco.Parameters.Add "pm_servicos", Me.txtServicos, 1
    
    vBanco.Parameters.Remove "pm_seguro"
    vBanco.Parameters.Add "pm_seguro", Me.txtSeguro, 1
    
    vBanco.Parameters.Remove "pm_frete"
    vBanco.Parameters.Add "pm_frete", Me.txtFrete, 1
    
    vBanco.Parameters.Remove "pm_despesas_financeiras"
    vBanco.Parameters.Add "pm_despesas_financeiras", Me.txtFinanceiras, 1
    
    vBanco.Parameters.Remove "pm_outras_despesas"
    vBanco.Parameters.Add "pm_outras_despesas", Me.txtOutras, 1
    
    vBanco.Parameters.Remove "pm_erro"
    vBanco.Parameters.Add "pm_erro", 0, 2
    
    vBanco.Parameters.Remove "pm_desc_erro"
    vBanco.Parameters.Add "pm_desc_erro", " ", 2

    vSql = "Producao.PCK_CAD167.pr_loja_despesas(:Pm_cursor,:Pm_Opcao,:Pm_codLoja,:Pm_Ano, :pm_pro_labore, :pm_salarios, :pm_combustiveis, :pm_encargos_sociais, :pm_tributos_federais, :pm_tributos_estaduais, :pm_tributos_municipais,:pm_agua_telefone, :pm_energia_eletrica, :pm_aluguel, :pm_servicos, :pm_seguro, :pm_frete, :pm_despesas_financeiras, :pm_outras_despesas, :pm_erro, :pm_desc_erro)"

    vVB_Generica_001.ExecutaPl vBanco, vSql

    Set vObjOracle = vBanco.Parameters("PM_CURSOR").Value
    
    If vObjOracle.EOF Then
        MsgBox "N�o h� informa��es cadastradas para este ano", vbInformation, "ATEN��O!!!"
        
        If MsgBox("Deseja cadastrar despesas para este ano?", vbYesNo, "Cadastro") = vbYes Then
            fraDespesas.Enabled = True
            cmdInserir.Enabled = True
            cmdAlterar.Enabled = False
            cmdExcluir.Enabled = False
            Me.txtProLabore.SetFocus
            
        Else
            cmdConsultar.Enabled = True
            cboAno.ListIndex = 0
            Exit Sub
        End If
    Else
        
        Me.txtProLabore = vObjOracle!pro_labore
        Me.txtSalarios = vObjOracle!salarios
        Me.txtCombustiveis = vObjOracle!salarios
        Me.txtEncargosSociais = vObjOracle!salarios
        Me.txtTributosFederais = vObjOracle!tributos_federais
        Me.txtTributosEstaduais = vObjOracle!tributos_estaduais
        Me.txtTributosMunicipais = vObjOracle!tributos_municipais
        Me.txtAguaTelefone = vObjOracle!agua_telefone
        Me.txtEnergiaEletrica = vObjOracle!energia_eletrica
        Me.txtAluguel = vObjOracle!aluguel
        Me.txtServicos = vObjOracle!servicos
        Me.txtSeguro = vObjOracle!seguro
        Me.txtFrete = vObjOracle!frete
        Me.txtFinanceiras = vObjOracle!despesas_financeiras
        Me.txtOutras = vObjOracle!outras_despesas
        
        fraDespesas.Enabled = False
        
        cmdInserir.Enabled = False
        cmdAlterar.Enabled = True
        cmdExcluir.Enabled = True
    
    End If
    
TrataErro:
    Exit Sub
    MsgBox "Erro inesperado, entre em contato com o analista", vbInformation, "ATEN��O!!!"

End Sub

Private Sub cmdExcluir_Click()
    Dim vFL As Boolean
    Dim vValor As String
    
    vCodLoja = Left(cboLoja, 2)
    vAno = cboAno
    vOpcao = "D"
    
    cmdAlterar.Enabled = False
    
    If Trim(frmCadastro.txtProLabore) = "" Then
        MsgBox "Dados Inv�lidos, o valor n�o foi digitado", vbCritical, "ATEN��O!!!"
        frmCadastro.txtProLabore.SetFocus
        Exit Sub
    Else
        vValor = frmCadastro.txtProLabore
        vFL = Verifica_Valor(vValor)
        If vFL = False Then
            frmCadastro.txtProLabore.SetFocus
            Exit Sub
        End If
    End If
    
    If Trim(frmCadastro.txtSalarios) = "" Then
        MsgBox "Dados Inv�lidos, o valor n�o foi digitado", vbCritical, "ATEN��O!!!"
        frmCadastro.txtSalarios.SetFocus
        Exit Sub
    Else
        vValor = frmCadastro.txtSalarios
        vFL = Verifica_Valor(vValor)
        If vFL = False Then
            frmCadastro.txtSalarios.SetFocus
            Exit Sub
        End If
    End If
    
    If Trim(frmCadastro.txtCombustiveis) = "" Then
        MsgBox "Dados Inv�lidos, o valor n�o foi digitado", vbCritical, "ATEN��O!!!"
        frmCadastro.txtCombustiveis.SetFocus
        Exit Sub
    Else
        vValor = frmCadastro.txtCombustiveis
        vFL = Verifica_Valor(vValor)
        If vFL = False Then
            frmCadastro.txtCombustiveis.SetFocus
            Exit Sub
        End If
    End If
    
    If Trim(frmCadastro.txtEncargosSociais) = "" Then
        MsgBox "Dados Inv�lidos, o valor n�o foi digitado", vbCritical, "ATEN��O!!!"
        frmCadastro.txtEncargosSociais.SetFocus
        Exit Sub
    Else
        vValor = frmCadastro.txtEncargosSociais
        vFL = Verifica_Valor(vValor)
        If vFL = False Then
            frmCadastro.txtEncargosSociais.SetFocus
            Exit Sub
        End If
    End If

    If Trim(frmCadastro.txtTributosFederais) = "" Then
        MsgBox "Dados Inv�lidos, o valor n�o foi digitado", vbCritical, "ATEN��O!!!"
        frmCadastro.txtTributosFederais.SetFocus
        Exit Sub
    Else
        vValor = frmCadastro.txtTributosFederais
        vFL = Verifica_Valor(vValor)
        If vFL = False Then
            frmCadastro.txtTributosFederais.SetFocus
            Exit Sub
        End If
    End If

    If Trim(frmCadastro.txtTributosEstaduais) = "" Then
        MsgBox "Dados Inv�lidos, o valor n�o foi digitado", vbCritical, "ATEN��O!!!"
        frmCadastro.txtTributosEstaduais.SetFocus
        Exit Sub
    Else
        vValor = frmCadastro.txtTributosEstaduais
        vFL = Verifica_Valor(vValor)
        If vFL = False Then
            frmCadastro.txtTributosEstaduais.SetFocus
            Exit Sub
        End If
    End If

    If Trim(frmCadastro.txtTributosMunicipais) = "" Then
        MsgBox "Dados Inv�lidos, o valor n�o foi digitado", vbCritical, "ATEN��O!!!"
        frmCadastro.txtTributosMunicipais.SetFocus
        Exit Sub
    Else
        vValor = frmCadastro.txtTributosMunicipais
        vFL = Verifica_Valor(vValor)
        If vFL = False Then
            frmCadastro.txtTributosMunicipais.SetFocus
            Exit Sub
        End If
    End If

    If Trim(frmCadastro.txtAguaTelefone) = "" Then
        MsgBox "Dados Inv�lidos, o valor n�o foi digitado", vbCritical, "ATEN��O!!!"
        frmCadastro.txtAguaTelefone.SetFocus
        Exit Sub
    Else
        vValor = frmCadastro.txtAguaTelefone
        vFL = Verifica_Valor(vValor)
        If vFL = False Then
            frmCadastro.txtAguaTelefone.SetFocus
            Exit Sub
        End If
    End If

    If Trim(frmCadastro.txtEnergiaEletrica) = "" Then
        MsgBox "Dados Inv�lidos, o valor n�o foi digitado", vbCritical, "ATEN��O!!!"
        frmCadastro.txtEnergiaEletrica.SetFocus
        Exit Sub
    Else
        vValor = frmCadastro.txtEnergiaEletrica
        vFL = Verifica_Valor(vValor)
        If vFL = False Then
            frmCadastro.txtEnergiaEletrica.SetFocus
            Exit Sub
        End If
    End If

    If Trim(frmCadastro.txtAluguel) = "" Then
        MsgBox "Dados Inv�lidos, o valor n�o foi digitado", vbCritical, "ATEN��O!!!"
        frmCadastro.txtAluguel.SetFocus
        Exit Sub
    Else
        vValor = frmCadastro.txtAluguel
        vFL = Verifica_Valor(vValor)
        If vFL = False Then
            frmCadastro.txtAluguel.SetFocus
            Exit Sub
        End If
    End If

    If Trim(frmCadastro.txtServicos) = "" Then
        MsgBox "Dados Inv�lidos, o valor n�o foi digitado", vbCritical, "ATEN��O!!!"
        frmCadastro.txtServicos.SetFocus
        Exit Sub
    Else
        vValor = frmCadastro.txtServicos
        vFL = Verifica_Valor(vValor)
        If vFL = False Then
            frmCadastro.txtServicos.SetFocus
            Exit Sub
        End If
    End If

    If Trim(frmCadastro.txtSeguro) = "" Then
        MsgBox "Dados Inv�lidos, o valor n�o foi digitado", vbCritical, "ATEN��O!!!"
        frmCadastro.txtSeguro.SetFocus
        Exit Sub
    Else
        vValor = frmCadastro.txtSeguro
        vFL = Verifica_Valor(vValor)
        If vFL = False Then
            frmCadastro.txtSeguro.SetFocus
            Exit Sub
        End If
    End If

    If Trim(frmCadastro.txtFrete) = "" Then
        MsgBox "Dados Inv�lidos, o valor n�o foi digitado", vbCritical, "ATEN��O!!!"
        frmCadastro.txtFrete.SetFocus
        Exit Sub
    Else
        vValor = frmCadastro.txtFrete
        vFL = Verifica_Valor(vValor)
        If vFL = False Then
            frmCadastro.txtFrete.SetFocus
            Exit Sub
        End If
    End If

    If Trim(frmCadastro.txtFinanceiras) = "" Then
        MsgBox "Dados Inv�lidos, o valor n�o foi digitado", vbCritical, "ATEN��O!!!"
        frmCadastro.txtFinanceiras.SetFocus
        Exit Sub
    Else
        vValor = frmCadastro.txtFinanceiras
        vFL = Verifica_Valor(vValor)
        If vFL = False Then
            frmCadastro.txtFinanceiras.SetFocus
            Exit Sub
        End If
    End If

    If Trim(frmCadastro.txtOutras) = "" Then
        MsgBox "Dados Inv�lidos, o valor n�o foi digitado", vbCritical, "ATEN��O!!!"
        frmCadastro.txtOutras.SetFocus
        Exit Sub
    Else
        vValor = frmCadastro.txtOutras
        vFL = Verifica_Valor(vValor)
        If vFL = False Then
            frmCadastro.txtOutras.SetFocus
            Exit Sub
        End If
    End If
    
    If MsgBox("Confirma exclus�o?", vbYesNo, "ATEN��O!!!") = vbYes Then
        Criar_Cursor
    
        vBanco.Parameters.Remove "Pm_Opcao"
        vBanco.Parameters.Add "Pm_Opcao", vOpcao, 1
        
        vBanco.Parameters.Remove "Pm_CodLoja"
        vBanco.Parameters.Add "Pm_CodLoja", vCodLoja, 1
        
        vBanco.Parameters.Remove "Pm_Ano"
        vBanco.Parameters.Add "Pm_Ano", vAno, 1
        
        vBanco.Parameters.Remove "pm_pro_labore"
        vBanco.Parameters.Add "pm_pro_labore", txtProLabore, 1
        
        vBanco.Parameters.Remove "pm_salarios"
        vBanco.Parameters.Add "pm_salarios", txtSalarios, 1
        
        vBanco.Parameters.Remove "pm_combustiveis"
        vBanco.Parameters.Add "pm_combustiveis", txtCombustiveis, 1
        
        vBanco.Parameters.Remove "pm_encargos_sociais"
        vBanco.Parameters.Add "pm_encargos_sociais", txtEncargosSociais, 1
            
        vBanco.Parameters.Remove "pm_tributos_federais"
        vBanco.Parameters.Add "pm_tributos_federais", txtTributosFederais, 1
        
        vBanco.Parameters.Remove "pm_tributos_estaduais"
        vBanco.Parameters.Add "pm_tributos_estaduais", txtTributosEstaduais, 1
        
        vBanco.Parameters.Remove "pm_tributos_municipais"
        vBanco.Parameters.Add "pm_tributos_municipais", txtTributosMunicipais, 1
    
        vBanco.Parameters.Remove "pm_agua_telefone"
        vBanco.Parameters.Add "pm_agua_telefone", txtAguaTelefone, 1
        
        vBanco.Parameters.Remove "pm_energia_eletrica"
        vBanco.Parameters.Add "pm_energia_eletrica", txtEnergiaEletrica, 1
        
        vBanco.Parameters.Remove "pm_aluguel"
        vBanco.Parameters.Add "pm_aluguel", txtAluguel, 1
        
        vBanco.Parameters.Remove "pm_servicos"
        vBanco.Parameters.Add "pm_servicos", txtServicos, 1
        
        vBanco.Parameters.Remove "pm_seguro"
        vBanco.Parameters.Add "pm_seguro", txtSeguro, 1
        
        vBanco.Parameters.Remove "pm_frete"
        vBanco.Parameters.Add "pm_frete", txtFrete, 1
        
        vBanco.Parameters.Remove "pm_despesas_financeiras"
        vBanco.Parameters.Add "pm_despesas_financeiras", txtFinanceiras, 1
        
        vBanco.Parameters.Remove "pm_outras_despesas"
        vBanco.Parameters.Add "pm_outras_despesas", txtOutras, 1
        
        vBanco.Parameters.Remove "pm_erro"
        vBanco.Parameters.Add "pm_erro", 0, 2
        
        vBanco.Parameters.Remove "pm_desc_erro"
        vBanco.Parameters.Add "pm_desc_erro", " ", 2
    
        vSql = "Producao.PCK_CAD167.pr_loja_despesas(:Pm_cursor,:Pm_Opcao,:Pm_codLoja,:Pm_Ano, :pm_pro_labore, :pm_salarios, :pm_combustiveis, :pm_encargos_sociais, :pm_tributos_federais, :pm_tributos_estaduais, :pm_tributos_municipais,:pm_agua_telefone, :pm_energia_eletrica, :pm_aluguel, :pm_servicos, :pm_seguro, :pm_frete, :pm_despesas_financeiras, :pm_outras_despesas, :pm_erro, :pm_desc_erro)"
    
        vVB_Generica_001.ExecutaPl vBanco, vSql
        
        MsgBox "Exclus�o efetuada com sucesso", vbInformation, "ATEN��O!!!"
        
        Limpar
        cboAno.ListIndex = 0
        Exit Sub
    Else
        MsgBox "Exclus�o n�o efetuada", vbCritical, "ATEN��O!!!"
        Limpar
        cboAno.ListIndex = 0
        Exit Sub
    End If

End Sub

Private Sub cmdInserir_Click()
    Dim vFL As Boolean
    Dim vValor As String
    
    vCodLoja = Left(cboLoja, 2)
    vAno = cboAno
    vOpcao = "I"
    
    If Trim(frmCadastro.txtProLabore) = "" Then
        MsgBox "Dados Inv�lidos, o valor n�o foi digitado", vbCritical, "ATEN��O!!!"
        frmCadastro.txtProLabore.SetFocus
        Exit Sub
    Else
        vValor = frmCadastro.txtProLabore
        vFL = Verifica_Valor(vValor)
        If vFL = False Then
            frmCadastro.txtProLabore.SetFocus
            Exit Sub
        End If
    End If
    
    If Trim(frmCadastro.txtSalarios) = "" Then
        MsgBox "Dados Inv�lidos, o valor n�o foi digitado", vbCritical, "ATEN��O!!!"
        frmCadastro.txtSalarios.SetFocus
        Exit Sub
    Else
        vValor = frmCadastro.txtSalarios
        vFL = Verifica_Valor(vValor)
        If vFL = False Then
            frmCadastro.txtSalarios.SetFocus
            Exit Sub
        End If
    End If
    
    If Trim(frmCadastro.txtCombustiveis) = "" Then
        MsgBox "Dados Inv�lidos, o valor n�o foi digitado", vbCritical, "ATEN��O!!!"
        frmCadastro.txtCombustiveis.SetFocus
        Exit Sub
    Else
        vValor = frmCadastro.txtCombustiveis
        vFL = Verifica_Valor(vValor)
        If vFL = False Then
            frmCadastro.txtCombustiveis.SetFocus
            Exit Sub
        End If
    End If
    
    If Trim(frmCadastro.txtEncargosSociais) = "" Then
        MsgBox "Dados Inv�lidos, o valor n�o foi digitado", vbCritical, "ATEN��O!!!"
        frmCadastro.txtEncargosSociais.SetFocus
        Exit Sub
    Else
        vValor = frmCadastro.txtEncargosSociais
        vFL = Verifica_Valor(vValor)
        If vFL = False Then
            frmCadastro.txtEncargosSociais.SetFocus
            Exit Sub
        End If
    End If

    If Trim(frmCadastro.txtTributosFederais) = "" Then
        MsgBox "Dados Inv�lidos, o valor n�o foi digitado", vbCritical, "ATEN��O!!!"
        frmCadastro.txtTributosFederais.SetFocus
        Exit Sub
    Else
        vValor = frmCadastro.txtTributosFederais
        vFL = Verifica_Valor(vValor)
        If vFL = False Then
            frmCadastro.txtTributosFederais.SetFocus
            Exit Sub
        End If
    End If

    If Trim(frmCadastro.txtTributosEstaduais) = "" Then
        MsgBox "Dados Inv�lidos, o valor n�o foi digitado", vbCritical, "ATEN��O!!!"
        frmCadastro.txtTributosEstaduais.SetFocus
        Exit Sub
    Else
        vValor = frmCadastro.txtTributosEstaduais
        vFL = Verifica_Valor(vValor)
        If vFL = False Then
            frmCadastro.txtTributosEstaduais.SetFocus
            Exit Sub
        End If
    End If

    If Trim(frmCadastro.txtTributosMunicipais) = "" Then
        MsgBox "Dados Inv�lidos, o valor n�o foi digitado", vbCritical, "ATEN��O!!!"
        frmCadastro.txtTributosMunicipais.SetFocus
        Exit Sub
    Else
        vValor = frmCadastro.txtTributosMunicipais
        vFL = Verifica_Valor(vValor)
        If vFL = False Then
            frmCadastro.txtTributosMunicipais.SetFocus
            Exit Sub
        End If
    End If

    If Trim(frmCadastro.txtAguaTelefone) = "" Then
        MsgBox "Dados Inv�lidos, o valor n�o foi digitado", vbCritical, "ATEN��O!!!"
        frmCadastro.txtAguaTelefone.SetFocus
        Exit Sub
    Else
        vValor = frmCadastro.txtAguaTelefone
        vFL = Verifica_Valor(vValor)
        If vFL = False Then
            frmCadastro.txtAguaTelefone.SetFocus
            Exit Sub
        End If
    End If

    If Trim(frmCadastro.txtEnergiaEletrica) = "" Then
        MsgBox "Dados Inv�lidos, o valor n�o foi digitado", vbCritical, "ATEN��O!!!"
        frmCadastro.txtEnergiaEletrica.SetFocus
        Exit Sub
    Else
        vValor = frmCadastro.txtEnergiaEletrica
        vFL = Verifica_Valor(vValor)
        If vFL = False Then
            frmCadastro.txtEnergiaEletrica.SetFocus
            Exit Sub
        End If
    End If

    If Trim(frmCadastro.txtAluguel) = "" Then
        MsgBox "Dados Inv�lidos, o valor n�o foi digitado", vbCritical, "ATEN��O!!!"
        frmCadastro.txtAluguel.SetFocus
        Exit Sub
    Else
        vValor = frmCadastro.txtAluguel
        vFL = Verifica_Valor(vValor)
        If vFL = False Then
            frmCadastro.txtAluguel.SetFocus
            Exit Sub
        End If
    End If

    If Trim(frmCadastro.txtServicos) = "" Then
        MsgBox "Dados Inv�lidos, o valor n�o foi digitado", vbCritical, "ATEN��O!!!"
        frmCadastro.txtServicos.SetFocus
        Exit Sub
    Else
        vValor = frmCadastro.txtServicos
        vFL = Verifica_Valor(vValor)
        If vFL = False Then
            frmCadastro.txtServicos.SetFocus
            Exit Sub
        End If
    End If

    If Trim(frmCadastro.txtSeguro) = "" Then
        MsgBox "Dados Inv�lidos, o valor n�o foi digitado", vbCritical, "ATEN��O!!!"
        frmCadastro.txtSeguro.SetFocus
        Exit Sub
    Else
        vValor = frmCadastro.txtSeguro
        vFL = Verifica_Valor(vValor)
        If vFL = False Then
            frmCadastro.txtSeguro.SetFocus
            Exit Sub
        End If
    End If

    If Trim(frmCadastro.txtFrete) = "" Then
        MsgBox "Dados Inv�lidos, o valor n�o foi digitado", vbCritical, "ATEN��O!!!"
        frmCadastro.txtFrete.SetFocus
        Exit Sub
    Else
        vValor = frmCadastro.txtFrete
        vFL = Verifica_Valor(vValor)
        If vFL = False Then
            frmCadastro.txtFrete.SetFocus
            Exit Sub
        End If
    End If

    If Trim(frmCadastro.txtFinanceiras) = "" Then
        MsgBox "Dados Inv�lidos, o valor n�o foi digitado", vbCritical, "ATEN��O!!!"
        frmCadastro.txtFinanceiras.SetFocus
        Exit Sub
    Else
        vValor = frmCadastro.txtFinanceiras
        vFL = Verifica_Valor(vValor)
        If vFL = False Then
            frmCadastro.txtFinanceiras.SetFocus
            Exit Sub
        End If
    End If

    If Trim(frmCadastro.txtOutras) = "" Then
        MsgBox "Dados Inv�lidos, o valor n�o foi digitado", vbCritical, "ATEN��O!!!"
        frmCadastro.txtOutras.SetFocus
        Exit Sub
    Else
        vValor = frmCadastro.txtOutras
        vFL = Verifica_Valor(vValor)
        If vFL = False Then
            frmCadastro.txtOutras.SetFocus
            Exit Sub
        End If
    End If
    
    Criar_Cursor
    
    vBanco.Parameters.Remove "Pm_Opcao"
    vBanco.Parameters.Add "Pm_Opcao", vOpcao, 1
    
    vBanco.Parameters.Remove "Pm_CodLoja"
    vBanco.Parameters.Add "Pm_CodLoja", vCodLoja, 1
    
    vBanco.Parameters.Remove "Pm_Ano"
    vBanco.Parameters.Add "Pm_Ano", vAno, 1
    
    vBanco.Parameters.Remove "pm_pro_labore"
    vBanco.Parameters.Add "pm_pro_labore", txtProLabore, 1
    
    vBanco.Parameters.Remove "pm_salarios"
    vBanco.Parameters.Add "pm_salarios", txtSalarios, 1
    
    vBanco.Parameters.Remove "pm_combustiveis"
    vBanco.Parameters.Add "pm_combustiveis", txtCombustiveis, 1
    
    vBanco.Parameters.Remove "pm_encargos_sociais"
    vBanco.Parameters.Add "pm_encargos_sociais", txtEncargosSociais, 1
        
    vBanco.Parameters.Remove "pm_tributos_federais"
    vBanco.Parameters.Add "pm_tributos_federais", txtTributosFederais, 1
    
    vBanco.Parameters.Remove "pm_tributos_estaduais"
    vBanco.Parameters.Add "pm_tributos_estaduais", txtTributosEstaduais, 1
    
    vBanco.Parameters.Remove "pm_tributos_municipais"
    vBanco.Parameters.Add "pm_tributos_municipais", txtTributosMunicipais, 1
    
    vBanco.Parameters.Remove "pm_agua_telefone"
    vBanco.Parameters.Add "pm_agua_telefone", txtAguaTelefone, 1
    
    vBanco.Parameters.Remove "pm_energia_eletrica"
    vBanco.Parameters.Add "pm_energia_eletrica", txtEnergiaEletrica, 1
    
    vBanco.Parameters.Remove "pm_aluguel"
    vBanco.Parameters.Add "pm_aluguel", txtAluguel, 1
    
    vBanco.Parameters.Remove "pm_servicos"
    vBanco.Parameters.Add "pm_servicos", txtServicos, 1
    
    vBanco.Parameters.Remove "pm_seguro"
    vBanco.Parameters.Add "pm_seguro", txtSeguro, 1
    
    vBanco.Parameters.Remove "pm_frete"
    vBanco.Parameters.Add "pm_frete", txtFrete, 1
    
    vBanco.Parameters.Remove "pm_despesas_financeiras"
    vBanco.Parameters.Add "pm_despesas_financeiras", txtFinanceiras, 1
    
    vBanco.Parameters.Remove "pm_outras_despesas"
    vBanco.Parameters.Add "pm_outras_despesas", txtOutras, 1
    
    vBanco.Parameters.Remove "pm_erro"
    vBanco.Parameters.Add "pm_erro", 0, 2
    
    vBanco.Parameters.Remove "pm_desc_erro"
    vBanco.Parameters.Add "pm_desc_erro", " ", 2

    vSql = "Producao.PCK_CAD167.pr_loja_despesas(:Pm_cursor,:Pm_Opcao,:Pm_codLoja,:Pm_Ano, :pm_pro_labore, :pm_salarios, :pm_combustiveis, :pm_encargos_sociais, :pm_tributos_federais, :pm_tributos_estaduais, :pm_tributos_municipais,:pm_agua_telefone, :pm_energia_eletrica, :pm_aluguel, :pm_servicos, :pm_seguro, :pm_frete, :pm_despesas_financeiras, :pm_outras_despesas, :pm_erro, :pm_desc_erro)"

    vVB_Generica_001.ExecutaPl vBanco, vSql
    
    MsgBox "Dados inclu�dos com sucesso", vbInformation, "ATEN��O!!!"
    
    Limpar
    cboAno.ListIndex = 0
    Exit Sub

End Sub

Private Sub cmdVoltar_Click()

    Unload Me
    
    'mdiCad167.Enabled = True

End Sub
Private Sub Form_Load()
    
    frmCadastro.Top = ((mdiCad167.Height + SSPanel1) - frmCadastro.Height) / 1.35
    frmCadastro.Left = (mdiCad167.Width - frmCadastro.Width) / 2
    
    fraDespesas.Enabled = False
    cmdInserir.Enabled = False
    cmdAlterar.Enabled = False
    cmdExcluir.Enabled = False
    
End Sub

'Private Sub txtOutras_LostFocus()

'End Sub

Private Sub txtProLabore_KeyPress(keyascii As Integer)
    keyascii = Valor(keyascii, txtProLabore)
End Sub

Private Sub txtSalarios_KeyPress(keyascii As Integer)
    keyascii = Valor(keyascii, txtSalarios)
End Sub

Private Sub txtCombustiveis_KeyPress(keyascii As Integer)
    keyascii = Valor(keyascii, txtCombustiveis)
End Sub

Private Sub txtEncargosSociais_KeyPress(keyascii As Integer)
    keyascii = Valor(keyascii, txtEncargosSociais)
End Sub

Private Sub txtTributosFederais_KeyPress(keyascii As Integer)
    keyascii = Valor(keyascii, txtTributosFederais)
End Sub

Private Sub txtTributosEstaduais_KeyPress(keyascii As Integer)
    keyascii = Valor(keyascii, txtTributosEstaduais)
End Sub

Private Sub txtTributosMunicipais_KeyPress(keyascii As Integer)
    keyascii = Valor(keyascii, txtTributosMunicipais)
End Sub

Private Sub txtAguaTelefone_KeyPress(keyascii As Integer)
    keyascii = Valor(keyascii, txtAguaTelefone)
End Sub

Private Sub txtEnergiaEletrica_KeyPress(keyascii As Integer)
    keyascii = Valor(keyascii, txtEnergiaEletrica)
End Sub

Private Sub txtAluguel_KeyPress(keyascii As Integer)
    keyascii = Valor(keyascii, txtAluguel)
End Sub

Private Sub txtServicos_KeyPress(keyascii As Integer)
    keyascii = Valor(keyascii, txtServicos)
End Sub

Private Sub txtSeguro_KeyPress(keyascii As Integer)
    keyascii = Valor(keyascii, txtSeguro)
End Sub

Private Sub txtFrete_KeyPress(keyascii As Integer)
    keyascii = Valor(keyascii, txtFrete)
End Sub

Private Sub txtFinanceiras_KeyPress(keyascii As Integer)
    keyascii = Valor(keyascii, txtFinanceiras)
End Sub

Private Sub txtOutras_KeyPress(keyascii As Integer)
    keyascii = Valor(keyascii, txtOutras)
End Sub
