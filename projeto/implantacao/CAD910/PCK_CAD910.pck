create or replace package producao.PCK_CAD910 is

  TYPE TP_CURSOR IS REF CURSOR;

  Procedure PR_SEL_ANALISE (cDados IN OUT TP_CURSOR);
  
  Procedure PR_SEL_RESSUPR (cDados IN OUT TP_CURSOR);
  
  Procedure PR_DEL_AN (cCodPrior IN Ressuprimento.Prioridade_Analise.Cod_Prioridade%type,
                       cCodLojaDestino IN Ressuprimento.Prioridade_Analise.Cod_Loja_Destino%type);
                       
  Procedure PR_DEL_RE (cCodPrior IN Ressuprimento.Prioridade_Ressuprimento.Cod_Prioridade%type,
                       cCodLojaDestino IN Ressuprimento.Prioridade_Ressuprimento.Cod_Loja_Destino%type,
                       cCodLojaOrigem IN Ressuprimento.Prioridade_Ressuprimento.Cod_Loja_Origem%type);                       
                       
  Procedure PR_SEL_LOJAS (cDados IN OUT TP_CURSOR);  
  
  Procedure PR_SEL_LOJAS_PRIOR (cDados IN OUT TP_CURSOR);
  
  Procedure PR_SEL_LOJAS_OR (cDados IN OUT TP_CURSOR,
                             cCodLojaDestino IN loja.cod_loja%type);
                             
  Procedure PR_SEL_REG_AN (cDados IN OUT TP_CURSOR,
                           cCodLojaDestino IN Ressuprimento.Prioridade_Analise.Cod_Loja_Destino%type);                                                  
                           
  Procedure PR_PROX_PRIOR_AN (cDados IN OUT TP_CURSOR);
  
  Procedure PR_INC_AN (cCodPrior IN Ressuprimento.Prioridade_Analise.Cod_Prioridade%type,
                       cCodLojaDestino IN Ressuprimento.Prioridade_Analise.Cod_Loja_Destino%type);                           
                       
  Procedure PR_SEL_REG_RE (cDados IN OUT TP_CURSOR,
                           cCodLojaDestino IN Ressuprimento.Prioridade_Ressuprimento.Cod_Loja_Destino%type,
                           cCodLojaOrigem IN Ressuprimento.Prioridade_Ressuprimento.Cod_Loja_Origem%type);                       
                           
  Procedure PR_PROX_PRIOR_RE (cDados IN OUT TP_CURSOR,
                              cCodLojaDestino IN Ressuprimento.Prioridade_Ressuprimento.Cod_Loja_Destino%type);                           
  
  Procedure PR_INC_RE (cCodPrior IN Ressuprimento.Prioridade_Ressuprimento.Cod_Prioridade%type,
                       cCodLojaDestino IN Ressuprimento.Prioridade_Ressuprimento.Cod_Loja_Destino%type,
                       cCodLojaOrigem IN Ressuprimento.Prioridade_Ressuprimento.Cod_Loja_Origem%type);  
                       
  Procedure PR_SEL_PRIOR_AN (cDados IN OUT TP_CURSOR,
                             cCodPrior IN Ressuprimento.Prioridade_Analise.Cod_Prioridade%type,
                             cCodLojaDestino IN Ressuprimento.Prioridade_Analise.Cod_Loja_Destino%type);                       
                             
  Procedure PR_SEL_PRIOR_AN_2 (cDados IN OUT TP_CURSOR,
                               cCodPrior IN Ressuprimento.Prioridade_Analise.Cod_Prioridade%type);
                               
  Procedure PR_UPD_AN (cCodPrior IN Ressuprimento.Prioridade_Analise.Cod_Prioridade%type,
                       cCodLojaDestino IN Ressuprimento.Prioridade_Analise.Cod_Loja_Destino%type);
                       
  Procedure PR_SEL_PRIOR_RE (cDados IN OUT TP_CURSOR,
                             cCodPrior IN Ressuprimento.Prioridade_Ressuprimento.Cod_Prioridade%type,
                             cCodLojaDestino IN Ressuprimento.Prioridade_Ressuprimento.Cod_Loja_Destino%type,
                             cCodLojaOrigem IN Ressuprimento.Prioridade_Ressuprimento.Cod_Loja_Origem%type);                       
                             
  Procedure PR_SEL_PRIOR_RE_2 (cDados IN OUT TP_CURSOR,
                               cCodPrior IN Ressuprimento.Prioridade_Ressuprimento.Cod_Prioridade%type,
                               cCodLojaDestino IN Ressuprimento.Prioridade_Ressuprimento.Cod_Loja_Destino%type);     
                               
  Procedure PR_UPD_RE (cCodPrior IN Ressuprimento.Prioridade_Ressuprimento.Cod_Prioridade%type,
                       cCodLojaDestino IN Ressuprimento.Prioridade_Ressuprimento.Cod_Loja_Destino%type,
                       cCodLojaOrigem IN Ressuprimento.Prioridade_Ressuprimento.Cod_Loja_Origem%type);                                                       
                                                                                  
end PCK_CAD910;
/
create or replace package body producao.PCK_CAD910 is

  -- 1) Grid An�lise
  Procedure PR_SEL_ANALISE (cDados IN OUT TP_CURSOR) is
  Begin
       OPEN cDados FOR
       Select PA.Cod_Prioridade, 
              PA.Cod_Loja_Destino, 
              L.Nome_Fantasia 
       From   Ressuprimento.Prioridade_Analise PA, Loja L 
       Where  PA.Cod_Loja_Destino = L.Cod_Loja 
       Order by PA.Cod_Prioridade;
  End;   
    
  -- 2) Grid Ressuprimento
  Procedure PR_SEL_RESSUPR (cDados IN OUT TP_CURSOR) is
  Begin
       OPEN cDados FOR
       Select PR.Cod_Loja_Destino, 
              L1.Nome_Fantasia Fanta1, 
              PR.Cod_Prioridade, 
              PR.Cod_Loja_Origem, 
              L2.Nome_Fantasia Fanta2 
       From   Ressuprimento.Prioridade_Ressuprimento PR, Loja L1, Loja L2 
       Where  PR.Cod_Loja_Destino = L1.Cod_Loja And 
              PR.Cod_Loja_Origem = L2.Cod_Loja 
       Order by PR.Cod_Loja_Destino, PR.Cod_Prioridade;
  End;  
  
  -- 3) Deleta Grid An�lise  
  Procedure PR_DEL_AN (cCodPrior IN Ressuprimento.Prioridade_Analise.Cod_Prioridade%type,
                       cCodLojaDestino IN Ressuprimento.Prioridade_Analise.Cod_Loja_Destino%type) is
  Begin
       Delete Ressuprimento.Prioridade_Analise 
       Where  Cod_Prioridade = cCodPrior And 
              Cod_Loja_Destino = cCodLojaDestino;             
                     
       Commit;
       
       Exception 
          When Others then 
          RollBack; 
  End;   
  
  -- 4) Deleta Grid Ressuprimento
  Procedure PR_DEL_RE (cCodPrior IN Ressuprimento.Prioridade_Ressuprimento.Cod_Prioridade%type,
                       cCodLojaDestino IN Ressuprimento.Prioridade_Ressuprimento.Cod_Loja_Destino%type,
                       cCodLojaOrigem IN Ressuprimento.Prioridade_Ressuprimento.Cod_Loja_Origem%type) is
  Begin
       Delete Ressuprimento.Prioridade_Ressuprimento
       Where  Cod_Prioridade = cCodPrior And 
              Cod_Loja_Destino = cCodLojaDestino And 
              Cod_Loja_Origem = cCodLojaOrigem;    
                            
       Commit;
       
       Exception 
          When Others then 
          RollBack; 
  End;  
  
  -- 5) Combo Lojas 
  Procedure PR_SEL_LOJAS (cDados IN OUT TP_CURSOR) is
  Begin
       OPEN cDados FOR
       SELECT COD_LOJA, 
              NOME_FANTASIA 
       FROM   LOJA 
       ORDER BY COD_LOJA;
  End;   
  
  -- 6) Combo Lojas organizado por Prioridade de An�lise
  Procedure PR_SEL_LOJAS_PRIOR (cDados IN OUT TP_CURSOR) is
  Begin
       OPEN cDados FOR
       Select A.Cod_Loja_Destino, 
              L.Nome_Fantasia 
       From   Ressuprimento.Prioridade_Analise A, Loja L 
       Where  A.Cod_Loja_Destino = L.Cod_Loja 
       Order by A.Cod_Prioridade;
  End;   
  
  -- 7) Combo Lojas Origem
  Procedure PR_SEL_LOJAS_OR (cDados IN OUT TP_CURSOR,
                             cCodLojaDestino IN loja.cod_loja%type) is
  Begin
       OPEN cDados FOR
       Select cod_loja, 
              nome_fantasia 
       from loja 
       where cod_loja <> cCodLojaDestino;
  End;  
  
  -- 8) Verifica se j� existe registro 
  Procedure PR_SEL_REG_AN (cDados IN OUT TP_CURSOR,
                           cCodLojaDestino IN Ressuprimento.Prioridade_Analise.Cod_Loja_Destino%type) is
  Begin
       OPEN cDados FOR
       Select Count(*) QtdReg 
       From   Ressuprimento.Prioridade_Analise 
       Where  Cod_Loja_Destino = cCodLojaDestino;
  End;    
  
  -- 9) Pr�x. C�d. Prioridade 
  Procedure PR_PROX_PRIOR_AN (cDados IN OUT TP_CURSOR) is
  Begin
       OPEN cDados FOR
       Select Max(Cod_Prioridade) MaiorPrior 
       From   Ressuprimento.Prioridade_Analise;
  End;  
  
  -- 10) Inclui An�lise
  Procedure PR_INC_AN (cCodPrior IN Ressuprimento.Prioridade_Analise.Cod_Prioridade%type,
                       cCodLojaDestino IN Ressuprimento.Prioridade_Analise.Cod_Loja_Destino%type) is
  Begin
       Update Ressuprimento.Prioridade_Analise 
       Set    Cod_Prioridade = cCodPrior
       Where  Cod_Prioridade = cCodPrior And 
              Cod_Loja_Destino = cCodLojaDestino;
    
       If SQL%NOTFOUND Then 
          Insert into Ressuprimento.Prioridade_Analise 
                 (Cod_Prioridade,Cod_Loja_Destino) 
          Values (cCodPrior,cCodLojaDestino); 
       End If;
                                
       Commit;
       
       Exception 
          When Others then 
          RollBack; 
  End;  
  
  -- 11) Verifica se j� existe registro 
  Procedure PR_SEL_REG_RE (cDados IN OUT TP_CURSOR,
                           cCodLojaDestino IN Ressuprimento.Prioridade_Ressuprimento.Cod_Loja_Destino%type,
                           cCodLojaOrigem IN Ressuprimento.Prioridade_Ressuprimento.Cod_Loja_Origem%type) is
  Begin
       OPEN cDados FOR
       Select Count(*) QtdReg 
       From   Ressuprimento.Prioridade_Ressuprimento 
       Where  Cod_Loja_Destino = cCodLojaDestino And 
              Cod_Loja_Origem = cCodLojaOrigem;
  End;    
  
  -- 12) Pr�x. C�d. Prioridade 
  Procedure PR_PROX_PRIOR_RE (cDados IN OUT TP_CURSOR,
                              cCodLojaDestino IN Ressuprimento.Prioridade_Ressuprimento.Cod_Loja_Destino%type) is
  Begin
       OPEN cDados FOR
       Select Max(Cod_Prioridade) MaiorPrior 
       From   Ressuprimento.Prioridade_Ressuprimento 
       Where  Cod_Loja_Destino = cCodLojaDestino;
  End;   
  
  -- 13) Inclui Ressuprimento
  Procedure PR_INC_RE (cCodPrior IN Ressuprimento.Prioridade_Ressuprimento.Cod_Prioridade%type,
                       cCodLojaDestino IN Ressuprimento.Prioridade_Ressuprimento.Cod_Loja_Destino%type,
                       cCodLojaOrigem IN Ressuprimento.Prioridade_Ressuprimento.Cod_Loja_Origem%type) is
  Begin
       Update Ressuprimento.Prioridade_Ressuprimento 
       Set    Cod_Prioridade = cCodPrior
       Where  Cod_Loja_Destino = cCodLojaDestino And 
              Cod_Prioridade = cCodPrior And 
              Cod_Loja_Origem = cCodLojaOrigem;
              
       If SQL%NOTFOUND Then 
          Insert into Ressuprimento.Prioridade_Ressuprimento 
                 (Cod_Loja_Destino, Cod_Prioridade, Cod_Loja_Origem)  
          Values (cCodLojaDestino, cCodPrior, cCodLojaOrigem);
       End If; 
                                
       Commit;
       
       Exception 
          When Others then 
          RollBack; 
  End; 
  
  -- 14) Verifica se j� existe esta prioridade 
  Procedure PR_SEL_PRIOR_AN (cDados IN OUT TP_CURSOR,
                             cCodPrior IN Ressuprimento.Prioridade_Analise.Cod_Prioridade%type,
                             cCodLojaDestino IN Ressuprimento.Prioridade_Analise.Cod_Loja_Destino%type) is
  Begin
       OPEN cDados FOR
       Select Count(*) QtdReg 
       From   Ressuprimento.Prioridade_Analise 
       Where  Cod_Loja_Destino = cCodLojaDestino And 
              Cod_Prioridade = cCodPrior;              
  End;    
  
  -- 15) Verifica se j� existe esta prioridade 
  Procedure PR_SEL_PRIOR_AN_2 (cDados IN OUT TP_CURSOR,
                               cCodPrior IN Ressuprimento.Prioridade_Analise.Cod_Prioridade%type) is
  Begin
       OPEN cDados FOR
       Select Count(*) QtdReg 
       From   Ressuprimento.Prioridade_Analise 
       Where  Cod_Prioridade = cCodPrior;              
  End;   
  
  -- 16) Altera Prioridade An.
  Procedure PR_UPD_AN (cCodPrior IN Ressuprimento.Prioridade_Analise.Cod_Prioridade%type,
                       cCodLojaDestino IN Ressuprimento.Prioridade_Analise.Cod_Loja_Destino%type) is
  Begin
       Update Ressuprimento.Prioridade_Analise 
       Set    Cod_Prioridade = cCodPrior
       Where  Cod_Loja_Destino = cCodLojaDestino;
                                
       Commit;
       
       Exception 
          When Others then 
          RollBack; 
  End;  
  
  -- 17) Verifica se j� existe esta prioridade 
  Procedure PR_SEL_PRIOR_RE (cDados IN OUT TP_CURSOR,
                             cCodPrior IN Ressuprimento.Prioridade_Ressuprimento.Cod_Prioridade%type,
                             cCodLojaDestino IN Ressuprimento.Prioridade_Ressuprimento.Cod_Loja_Destino%type,
                             cCodLojaOrigem IN Ressuprimento.Prioridade_Ressuprimento.Cod_Loja_Origem%type) is
  Begin
       OPEN cDados FOR
       Select Count(*) QtdReg 
       From   Ressuprimento.Prioridade_Ressuprimento 
       Where  Cod_Loja_Destino = cCodLojaDestino And 
              Cod_Prioridade = cCodPrior And
              Cod_Loja_Origem = cCodLojaOrigem;       
  End;     
  
  -- 18) Verifica se j� existe esta prioridade 
  Procedure PR_SEL_PRIOR_RE_2 (cDados IN OUT TP_CURSOR,
                               cCodPrior IN Ressuprimento.Prioridade_Ressuprimento.Cod_Prioridade%type,
                               cCodLojaDestino IN Ressuprimento.Prioridade_Ressuprimento.Cod_Loja_Destino%type) is
  Begin
       OPEN cDados FOR
       Select Count(*) QtdReg 
       From   Ressuprimento.Prioridade_Ressuprimento 
       Where  Cod_Prioridade = cCodPrior And 
              Cod_Loja_Destino = cCodLojaDestino;              
  End;   
  
  -- 19) Altera Prioridade Re.
  Procedure PR_UPD_RE (cCodPrior IN Ressuprimento.Prioridade_Ressuprimento.Cod_Prioridade%type,
                       cCodLojaDestino IN Ressuprimento.Prioridade_Ressuprimento.Cod_Loja_Destino%type,
                       cCodLojaOrigem IN Ressuprimento.Prioridade_Ressuprimento.Cod_Loja_Origem%type) is
  Begin
       Update Ressuprimento.Prioridade_Ressuprimento 
       Set    Cod_Prioridade = cCodPrior
       Where  Cod_Loja_Destino = cCodLojaDestino And 
              Cod_Loja_Origem = cCodLojaOrigem;
                                
       Commit;
       
       Exception 
          When Others then 
          RollBack; 
  End;    
  
end PCK_CAD910;
/
