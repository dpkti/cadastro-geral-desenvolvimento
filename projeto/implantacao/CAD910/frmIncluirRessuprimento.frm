VERSION 5.00
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form frmIncluirRessuprimento 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Inclus�o de Prioridade (Ressuprimento)"
   ClientHeight    =   2925
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4800
   Icon            =   "frmIncluirRessuprimento.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   195
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   320
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame FrameDadosA 
      Caption         =   "Dados"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1620
      Left            =   90
      TabIndex        =   5
      Top             =   900
      Width           =   4605
      Begin VB.ComboBox cmbLojaOrigemR 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         ItemData        =   "frmIncluirRessuprimento.frx":23D2
         Left            =   1710
         List            =   "frmIncluirRessuprimento.frx":23D4
         Style           =   2  'Dropdown List
         TabIndex        =   11
         Top             =   1090
         Width           =   2715
      End
      Begin VB.ComboBox cmbLojaDestinoR 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         ItemData        =   "frmIncluirRessuprimento.frx":23D6
         Left            =   1710
         List            =   "frmIncluirRessuprimento.frx":23D8
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   675
         Width           =   2715
      End
      Begin MSMask.MaskEdBox txtCodPrioridadeR 
         Height          =   330
         Left            =   1710
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   270
         Width           =   600
         _ExtentX        =   1058
         _ExtentY        =   582
         _Version        =   393216
         Enabled         =   0   'False
         MaxLength       =   2
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         PromptChar      =   "�"
      End
      Begin VB.Label lblLojaOrigemR 
         Appearance      =   0  'Flat
         Caption         =   "C�d. Loja Origem:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   135
         TabIndex        =   10
         Top             =   1120
         Width           =   1605
      End
      Begin VB.Label lblLojaDestinoR 
         Appearance      =   0  'Flat
         Caption         =   "C�d. Loja Destino:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   135
         TabIndex        =   8
         Top             =   720
         Width           =   1605
      End
      Begin VB.Label lblCodPrioridadeR 
         Appearance      =   0  'Flat
         Caption         =   "C�d. Prioridade:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   135
         TabIndex        =   6
         Top             =   315
         Width           =   1350
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   2595
      Width           =   4800
      _ExtentX        =   8467
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   8414
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   810
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmIncluirRessuprimento.frx":23DA
      PICN            =   "frmIncluirRessuprimento.frx":23F6
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdIncluir 
      Height          =   690
      Left            =   1575
      TabIndex        =   3
      TabStop         =   0   'False
      ToolTipText     =   "Incluir"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmIncluirRessuprimento.frx":30D0
      PICN            =   "frmIncluirRessuprimento.frx":30EC
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdLimpar 
      Height          =   690
      Left            =   810
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Limpar Campos"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmIncluirRessuprimento.frx":3DC6
      PICN            =   "frmIncluirRessuprimento.frx":3DE2
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmIncluirRessuprimento"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmbLojaDestinoR_Click()

    txtCodPrioridadeR = ""
    cmbLojaOrigemR.ListIndex = -1

    'carrega Combo Origem
    cmbLojaOrigemR.Clear
    
    'Call Aguardar
    Call vVB_Generica_001.ExcluiBind(vBanco)
    vBanco.Parameters.Add "vCursor", 0, 3
    vBanco.Parameters("vCursor").ServerType = 102
    vBanco.Parameters("vCursor").DynasetOption = &H2&
    vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
    vBanco.Parameters.Add "vCodLojaDestino", Mid(cmbLojaDestinoR, 1, 2), 1
    vSql = "PRODUCAO.PCK_CAD910.PR_SEL_LOJAS_OR(:vCursor,:vCodLojaDestino)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    'Unload frmAguardar
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro & ". Erro ao ler Lojas.")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("vCursor").Value
    End If
    While Not vObjOracle.EOF
        cmbLojaOrigemR.AddItem Format(vObjOracle!cod_loja, "00") & " - " & vObjOracle!Nome_Fantasia
        vObjOracle.MoveNext
    Wend

End Sub

Private Sub cmbLojaOrigemR_Click()

    txtCodPrioridadeR = ""

    'verifica se j� existe este registro
    'Call Aguardar
    Call vVB_Generica_001.ExcluiBind(vBanco)
    vBanco.Parameters.Add "vCursor", 0, 3
    vBanco.Parameters("vCursor").ServerType = 102
    vBanco.Parameters("vCursor").DynasetOption = &H2&
    vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
    vBanco.Parameters.Add "vCodLojaDestino", Mid(cmbLojaDestinoR, 1, 2), 1
    vBanco.Parameters.Add "vCodLojaOrigem", Mid(cmbLojaOrigemR, 1, 2), 1
    vSql = "PRODUCAO.PCK_CAD910.PR_SEL_REG_RE(:vCursor,:vCodLojaDestino,:vCodLojaOrigem)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    'Unload frmAguardar
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro & ". Erro ao verificar registro.")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("vCursor").Value
    End If
    If vObjOracle!QtdReg <> 0 Then
        Call vVB_Generica_001.Informar("Loja Destino e Origem j� cadastradas." & Chr(13) & "Utilize a op��o de Alterar Prioridade.")
        Call cmdLimpar_Click
        Exit Sub
    End If
    
    'verifica Pr�x. C�d. Prioridade
    'Call Aguardar
    Call vVB_Generica_001.ExcluiBind(vBanco)
    vBanco.Parameters.Add "vCursor", 0, 3
    vBanco.Parameters("vCursor").ServerType = 102
    vBanco.Parameters("vCursor").DynasetOption = &H2&
    vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
    vBanco.Parameters.Add "vCodLojaDestino", Mid(cmbLojaDestinoR, 1, 2), 1
    vSql = "PRODUCAO.PCK_CAD910.PR_PROX_PRIOR_RE(:vCursor,:vCodLojaDestino)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    'Unload frmAguardar
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro & ". Erro ao verificar Pr�x. C�d. Prioridade.")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("vCursor").Value
    End If
    If vObjOracle.EOF Then
        Call vVB_Generica_001.Informar("Tabela 'Ressuprimento.Prioridade_Ressuprimento' vazia.")
        Call cmdLimpar_Click
        Exit Sub
    End If
    If IsNull(vObjOracle!MaiorPrior) Then
        txtCodPrioridadeR = "01"
    Else
        txtCodPrioridadeR = Format(vObjOracle!MaiorPrior + 1, "00")
    End If

End Sub

Private Sub cmdIncluir_Click()

    'verifica campos
    If Trim(cmbLojaDestinoR) = "" Then
        Call vVB_Generica_001.Informar("Selecione a Loja Destino.")
        cmbLojaDestinoR.SetFocus
        Exit Sub
    End If
    If Trim(cmbLojaOrigemR) = "" Then
        Call vVB_Generica_001.Informar("Selecione a Loja Origem.")
        cmbLojaOrigemR.SetFocus
        Exit Sub
    End If
       
    'verificar se existe registro:
    'no click do combo � verificado

    'confirma inclus�o
    If vVB_Generica_001.Perguntar("Confirma inclus�o?") = 7 Then
        Call cmdLimpar_Click
        Exit Sub
    End If
    
    'atualiza banco
    Call Aguardar
    Call vVB_Generica_001.ExcluiBind(vBanco)
    vBanco.Parameters.Add "vCodPrior", txtCodPrioridadeR, 1
    vBanco.Parameters.Add "vCodLojaDestino", Mid(cmbLojaDestinoR, 1, 2), 1
    vBanco.Parameters.Add "vCodLojaOrigem", Mid(cmbLojaOrigemR, 1, 2), 1
    vSql = "PRODUCAO.PCK_CAD910.PR_INC_RE(:vCodPrior,:vCodLojaDestino,:vCodLojaOrigem)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    Unload frmAguardar
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro & ". Erro Inclus�o.")
        Call cmdLimpar_Click
        Exit Sub
    End If
    
    Call frmPrioridade.cmdLimparR_Click
    Call vVB_Generica_001.Informar("Inclus�o efetuada com sucesso.")
    Unload Me

End Sub

Private Sub cmdLimpar_Click()

    cmbLojaOrigemR.ListIndex = -1
    cmbLojaDestinoR.ListIndex = -1
    cmbLojaDestinoR.SetFocus
    txtCodPrioridadeR = ""

End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub

Private Sub Form_Load()
    
    Me.Top = 0
    Me.Left = 0
    
    'carrega combo Loja Destino
    cmbLojaDestinoR.Clear
    
    'Call Aguardar
    Call vVB_Generica_001.ExcluiBind(vBanco)
    vBanco.Parameters.Add "vCursor", 0, 3
    vBanco.Parameters("vCursor").ServerType = 102
    vBanco.Parameters("vCursor").DynasetOption = &H2&
    vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
    vSql = "PRODUCAO.PCK_CAD910.PR_SEL_LOJAS_PRIOR(:vCursor)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    'Unload frmAguardar
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro & ". Erro ao ler Lojas.")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("vCursor").Value
    End If
    While Not vObjOracle.EOF
        cmbLojaDestinoR.AddItem Format(vObjOracle!Cod_Loja_Destino, "00") & " - " & vObjOracle!Nome_Fantasia
        vObjOracle.MoveNext
    Wend
    
End Sub
