VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form frmPrioridade 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cadastro de Prioridade entre CD�s no Ressuprimento"
   ClientHeight    =   6030
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6780
   Icon            =   "frmPrioridade.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   402
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   452
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   5700
      Width           =   6780
      _ExtentX        =   11959
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   11906
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   810
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmPrioridade.frx":23D2
      PICN            =   "frmPrioridade.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin TabDlg.SSTab stbPrioridade 
      Height          =   4650
      Left            =   90
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   930
      Width           =   6600
      _ExtentX        =   11642
      _ExtentY        =   8202
      _Version        =   393216
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   617
      ShowFocusRect   =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "An�lise"
      TabPicture(0)   =   "frmPrioridade.frx":30C8
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Label96"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Label95"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Label94"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Label93"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "cmdAlterar"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "cmdLimparA"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "cmdExcluirA"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "cmdIncluirA"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "txtInicioNumNota"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "txtVlAdemeDec"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "txtCgcEmis"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "txtTamNumNota"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "txtInicioSerie"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).Control(13)=   "txtTamSerie"
      Tab(0).Control(13).Enabled=   0   'False
      Tab(0).Control(14)=   "txtVlAdemeInt"
      Tab(0).Control(14).Enabled=   0   'False
      Tab(0).Control(15)=   "FrameGridA"
      Tab(0).Control(15).Enabled=   0   'False
      Tab(0).ControlCount=   16
      TabCaption(1)   =   "Ressuprimento"
      TabPicture(1)   =   "frmPrioridade.frx":30E4
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "cmdAlterarR"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "cmdLimparR"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "cmdExcluirR"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "cmdIncluirR"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).Control(4)=   "FrameGridR"
      Tab(1).Control(4).Enabled=   0   'False
      Tab(1).ControlCount=   5
      Begin VB.Frame FrameGridR 
         Caption         =   "Dados"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   3150
         Left            =   -74855
         TabIndex        =   10
         Top             =   450
         Width           =   6280
         Begin MSFlexGridLib.MSFlexGrid mfgRessuprimento 
            Height          =   2730
            Left            =   150
            TabIndex        =   11
            Top             =   255
            Width           =   5970
            _ExtentX        =   10530
            _ExtentY        =   4815
            _Version        =   393216
            FixedCols       =   0
            BackColorBkg    =   -2147483633
            AllowBigSelection=   0   'False
            ScrollBars      =   2
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame FrameGridA 
         Caption         =   "Dados"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   3150
         Left            =   145
         TabIndex        =   4
         Top             =   450
         Width           =   4800
         Begin MSFlexGridLib.MSFlexGrid mfgAnalise 
            Height          =   2737
            Left            =   150
            TabIndex        =   5
            Top             =   255
            Width           =   4485
            _ExtentX        =   7911
            _ExtentY        =   4815
            _Version        =   393216
            FixedCols       =   0
            BackColorBkg    =   -2147483633
            AllowBigSelection=   0   'False
            ScrollBars      =   2
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.TextBox txtVlAdemeInt 
         Height          =   315
         Left            =   10065
         MaxLength       =   7
         TabIndex        =   22
         Top             =   561
         Width           =   855
      End
      Begin VB.TextBox txtTamSerie 
         Height          =   315
         Left            =   10065
         MaxLength       =   2
         TabIndex        =   21
         Top             =   1866
         Width           =   360
      End
      Begin VB.TextBox txtInicioSerie 
         Height          =   315
         Left            =   10065
         MaxLength       =   3
         TabIndex        =   20
         Top             =   1506
         Width           =   495
      End
      Begin VB.TextBox txtTamNumNota 
         Height          =   315
         Left            =   10065
         MaxLength       =   2
         TabIndex        =   19
         Top             =   2766
         Width           =   360
      End
      Begin VB.TextBox txtCgcEmis 
         Height          =   315
         Left            =   10065
         MaxLength       =   7
         TabIndex        =   18
         Top             =   3351
         Width           =   855
      End
      Begin VB.TextBox txtVlAdemeDec 
         Height          =   315
         Left            =   10065
         MaxLength       =   7
         TabIndex        =   17
         Top             =   921
         Width           =   855
      End
      Begin VB.TextBox txtInicioNumNota 
         Height          =   315
         Left            =   10065
         MaxLength       =   3
         TabIndex        =   16
         Top             =   2406
         Width           =   495
      End
      Begin Bot�o.cmd cmdIncluirA 
         Height          =   690
         Left            =   4965
         TabIndex        =   8
         TabStop         =   0   'False
         ToolTipText     =   "Incluir"
         Top             =   3755
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmPrioridade.frx":3100
         PICN            =   "frmPrioridade.frx":311C
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdExcluirA 
         Height          =   690
         Left            =   5730
         TabIndex        =   9
         TabStop         =   0   'False
         ToolTipText     =   "Excluir"
         Top             =   3755
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmPrioridade.frx":3DF6
         PICN            =   "frmPrioridade.frx":3E12
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdLimparA 
         Height          =   690
         Left            =   3430
         TabIndex        =   6
         TabStop         =   0   'False
         ToolTipText     =   "Atualizar tela"
         Top             =   3750
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmPrioridade.frx":46EC
         PICN            =   "frmPrioridade.frx":4708
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdIncluirR 
         Height          =   690
         Left            =   -70035
         TabIndex        =   14
         TabStop         =   0   'False
         ToolTipText     =   "Incluir"
         Top             =   3755
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmPrioridade.frx":53E2
         PICN            =   "frmPrioridade.frx":53FE
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdExcluirR 
         Height          =   690
         Left            =   -69270
         TabIndex        =   15
         TabStop         =   0   'False
         ToolTipText     =   "Excluir"
         Top             =   3755
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmPrioridade.frx":60D8
         PICN            =   "frmPrioridade.frx":60F4
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdLimparR 
         Height          =   690
         Left            =   -71570
         TabIndex        =   12
         TabStop         =   0   'False
         ToolTipText     =   "Atualizar tela"
         Top             =   3750
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmPrioridade.frx":69CE
         PICN            =   "frmPrioridade.frx":69EA
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdAlterar 
         Height          =   690
         Left            =   4195
         TabIndex        =   7
         TabStop         =   0   'False
         ToolTipText     =   "Alterar Prioridade"
         Top             =   3755
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmPrioridade.frx":76C4
         PICN            =   "frmPrioridade.frx":76E0
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdAlterarR 
         Height          =   690
         Left            =   -70805
         TabIndex        =   13
         TabStop         =   0   'False
         ToolTipText     =   "Alterar Prioridade"
         Top             =   3755
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmPrioridade.frx":83BA
         PICN            =   "frmPrioridade.frx":83D6
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label Label93 
         Caption         =   "999"
         BeginProperty Font 
            Name            =   "MS Serif"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   135
         Left            =   10620
         TabIndex        =   26
         Top             =   2451
         Width           =   345
      End
      Begin VB.Label Label94 
         Caption         =   "99"
         BeginProperty Font 
            Name            =   "MS Serif"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   135
         Left            =   10485
         TabIndex        =   25
         Top             =   1921
         Width           =   255
      End
      Begin VB.Label Label95 
         Caption         =   "999"
         BeginProperty Font 
            Name            =   "MS Serif"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   135
         Left            =   10620
         TabIndex        =   24
         Top             =   1551
         Width           =   300
      End
      Begin VB.Label Label96 
         Caption         =   "99"
         BeginProperty Font 
            Name            =   "MS Serif"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   135
         Left            =   10485
         TabIndex        =   23
         Top             =   2811
         Width           =   300
      End
   End
End
Attribute VB_Name = "frmPrioridade"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdAlterar_Click()

    'verifica se usu�rio selecionou uma Prioridade a ser alterada
    If mfgAnalise.Col <> 0 Then
        Call vVB_Generica_001.Informar("Selecione a Prioridade a ser alterada.")
        mfgAnalise.SetFocus
        Exit Sub
    End If
   
    'abre tela
    mfgAnalise.Col = 0
    frmAlterarAnalise.txtCodPrioridadeA = mfgAnalise
    mfgAnalise.Col = 1
    frmAlterarAnalise.txtLojaDestino = mfgAnalise
    frmAlterarAnalise.Show

End Sub

Private Sub cmdAlterarR_Click()

    'verifica se usu�rio selecionou uma Prioridade a ser alterada
    If mfgRessuprimento.Col <> 1 Then
        Call vVB_Generica_001.Informar("Selecione a Prioridade a ser alterada.")
        mfgRessuprimento.SetFocus
        Exit Sub
    End If
   
    'abre tela
    mfgRessuprimento.Col = 0
    frmAlterarRessuprimento.txtLojaDestinoR = mfgRessuprimento
    mfgRessuprimento.Col = 1
    frmAlterarRessuprimento.txtCodPrioridadeR = mfgRessuprimento
    mfgRessuprimento.Col = 2
    frmAlterarRessuprimento.txtLojaOrigemR = mfgRessuprimento
    frmAlterarRessuprimento.Show


End Sub

Private Sub cmdExcluirA_Click()

Dim vExcluir As String
Dim i As Integer

    'verifica se existem linhas no grid a serem exclu�das
    vExcluir = "N"
    For i = 1 To mfgAnalise.Rows - 1
        mfgAnalise.Row = i
        mfgAnalise.Col = 2
        If mfgAnalise = "X" Then
            vExcluir = "S"
            Exit For
        End If
    Next i
    
    If vExcluir = "N" Then
        Call vVB_Generica_001.Informar("Selecione ao menos uma linha para exclus�o.")
        mfgAnalise.SetFocus
        Exit Sub
    End If
    
    'confirma exclus�o
    If vVB_Generica_001.Perguntar("Confirma exclus�o?") = 7 Then
        Call cmdLimparA_Click
        Exit Sub
    End If
       
    For i = 1 To mfgAnalise.Rows - 1
    
        mfgAnalise.Row = i
        mfgAnalise.Col = 2
        If mfgAnalise = "X" Then
            'atualiza banco
            Call Aguardar
            Call vVB_Generica_001.ExcluiBind(vBanco)
            mfgAnalise.Col = 0
            vBanco.Parameters.Add "vCodPrior", mfgAnalise, 1
            mfgAnalise.Col = 1
            vBanco.Parameters.Add "vCodLojaDestino", Mid(mfgAnalise, 1, 2), 1
            vSql = "PRODUCAO.PCK_CAD910.PR_DEL_AN(:vCodPrior,:vCodLojaDestino)"
            vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
            Unload frmAguardar
            If vErro <> "" Then
                vVB_Generica_001.ProcessaErro (vErro & ". Erro Exclus�o.")
                Call cmdLimparA_Click
                Exit Sub
            End If
        End If
                       
    Next i
    
    Call cmdLimparA_Click
    Call vVB_Generica_001.Informar("Exclus�o efetuada com sucesso.")

End Sub

Private Sub cmdExcluirR_Click()

Dim vExcluir As String
Dim i As Integer

    'verifica se existem linhas no grid a serem exclu�das
    vExcluir = "N"
    For i = 1 To mfgRessuprimento.Rows - 1
        mfgRessuprimento.Row = i
        mfgRessuprimento.Col = 3
        If mfgRessuprimento = "X" Then
            vExcluir = "S"
            Exit For
        End If
    Next i
    
    If vExcluir = "N" Then
        Call vVB_Generica_001.Informar("Selecione ao menos uma linha para exclus�o.")
        mfgRessuprimento.SetFocus
        Exit Sub
    End If
    
    'confirma exclus�o
    If vVB_Generica_001.Perguntar("Confirma exclus�o?") = 7 Then
        Call cmdLimparR_Click
        Exit Sub
    End If
       
    For i = 1 To mfgRessuprimento.Rows - 1
    
        mfgRessuprimento.Row = i
        mfgRessuprimento.Col = 3
        If mfgRessuprimento = "X" Then
            'atualiza banco
            Call Aguardar
            Call vVB_Generica_001.ExcluiBind(vBanco)
            mfgRessuprimento.Col = 1
            vBanco.Parameters.Add "vCodPrior", mfgRessuprimento, 1
            mfgRessuprimento.Col = 0
            vBanco.Parameters.Add "vCodLojaDestino", Mid(mfgRessuprimento, 1, 2), 1
            mfgRessuprimento.Col = 2
            vBanco.Parameters.Add "vCodLojaOrigem", Mid(mfgRessuprimento, 1, 2), 1
            vSql = "PRODUCAO.PCK_CAD910.PR_DEL_RE(:vCodPrior,:vCodLojaDestino,:vCodLojaOrigem)"
            vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
            Unload frmAguardar
            If vErro <> "" Then
                vVB_Generica_001.ProcessaErro (vErro & ". Erro Exclus�o.")
                Call cmdLimparR_Click
                Exit Sub
            End If
        End If
                       
    Next i
    
    Call cmdLimparR_Click
    Call vVB_Generica_001.Informar("Exclus�o efetuada com sucesso.")

End Sub

Private Sub cmdIncluirA_Click()

    frmIncluirAnalise.Show

End Sub

Private Sub cmdIncluirR_Click()

    frmIncluirRessuprimento.Show

End Sub

Public Sub cmdLimparA_Click()

    Call Define_Grid_Analise

End Sub

Public Sub cmdLimparR_Click()

    Call Define_Grid_Ressuprimento

End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub

Public Sub Define_Grid_Analise()

Dim i As Integer

    'define grid An�lise
    mfgAnalise.Rows = 2
    mfgAnalise.Cols = 3
    mfgAnalise.Row = 1
    
    'limpa linha
    For i = 0 To mfgAnalise.Cols - 1
        mfgAnalise.Col = i
        mfgAnalise.Text = ""
    Next i
    
    'cabe�alho e alinhamento coluna
    mfgAnalise.Row = 0
    
    mfgAnalise.Col = 0
    mfgAnalise.Text = "C�d.Prioridade"
    mfgAnalise.FixedAlignment(0) = 4
    mfgAnalise.ColAlignment(0) = 4
    mfgAnalise.ColWidth(0) = 1500
    
    mfgAnalise.Col = 1
    mfgAnalise.Text = "CD Destino"
    mfgAnalise.FixedAlignment(1) = 4
    mfgAnalise.ColAlignment(1) = 1
    mfgAnalise.ColWidth(1) = 1900
    
    mfgAnalise.Col = 2
    mfgAnalise.Text = "Excluir"
    mfgAnalise.FixedAlignment(2) = 4
    mfgAnalise.ColAlignment(2) = 4
    mfgAnalise.ColWidth(2) = 800
    
    'carrega grid An�lise
    'Call Aguardar
    Call vVB_Generica_001.ExcluiBind(vBanco)
    vBanco.Parameters.Add "vCursor", 0, 3
    vBanco.Parameters("vCursor").ServerType = 102
    vBanco.Parameters("vCursor").DynasetOption = &H2&
    vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
    vSql = "PRODUCAO.PCK_CAD910.PR_SEL_ANALISE(:vCursor)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    'Unload frmAguardar
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro & ". Erro ao preencher grid An�lise.")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("vCursor").Value
    End If
    If Not vObjOracle.EOF Then
        'prepara grid
        mfgAnalise.Rows = 2
        mfgAnalise.Row = 1
        For i = 0 To mfgAnalise.Cols - 1
            mfgAnalise.Col = i
            mfgAnalise.Text = ""
        Next i
        'carrega dados do banco
        While Not vObjOracle.EOF
            'adiciona linha
            If mfgAnalise.Rows = 2 Then
                mfgAnalise.Col = 0
                mfgAnalise.Row = 1
                If mfgAnalise.Text <> "" Then
                    mfgAnalise.Rows = mfgAnalise.Rows + 1
                End If
            Else
                mfgAnalise.Rows = mfgAnalise.Rows + 1
            End If
            'escreve os dados
            mfgAnalise.Row = mfgAnalise.Rows - 1
            mfgAnalise.Col = 0
            mfgAnalise.Text = Format(CStr(vObjOracle!Cod_Prioridade), "00")
            mfgAnalise.Col = 1
            mfgAnalise.Text = Format(CStr(vObjOracle!Cod_Loja_Destino), "00") & " - " & CStr(vObjOracle!Nome_Fantasia)
            mfgAnalise.Col = 2
            mfgAnalise.Text = ""
            'pr�ximo registro
            vObjOracle.MoveNext
        Wend
    End If

End Sub

Public Sub Define_Grid_Ressuprimento()

Dim i As Integer

    'define grid Ressuprimento
    mfgRessuprimento.Rows = 2
    mfgRessuprimento.Cols = 4
    mfgRessuprimento.Row = 1
    
    'limpa linha
    For i = 0 To mfgRessuprimento.Cols - 1
        mfgRessuprimento.Col = i
        mfgRessuprimento.Text = ""
    Next i
    
    'cabe�alho e alinhamento coluna
    mfgRessuprimento.Row = 0
    
    mfgRessuprimento.Col = 0
    mfgRessuprimento.Text = "CD Destino"
    mfgRessuprimento.FixedAlignment(0) = 4
    mfgRessuprimento.ColAlignment(0) = 1
    mfgRessuprimento.ColWidth(0) = 1700
    
    mfgRessuprimento.Col = 1
    mfgRessuprimento.Text = "C�d.Prioridade"
    mfgRessuprimento.FixedAlignment(1) = 4
    mfgRessuprimento.ColAlignment(1) = 4
    mfgRessuprimento.ColWidth(1) = 1500
    
    mfgRessuprimento.Col = 2
    mfgRessuprimento.Text = "CD Origem"
    mfgRessuprimento.FixedAlignment(2) = 4
    mfgRessuprimento.ColAlignment(2) = 1
    mfgRessuprimento.ColWidth(2) = 1700
        
    mfgRessuprimento.Col = 3
    mfgRessuprimento.Text = "Excluir"
    mfgRessuprimento.FixedAlignment(3) = 4
    mfgRessuprimento.ColAlignment(3) = 4
    mfgRessuprimento.ColWidth(3) = 800
    
    'carrega grid Ressuprimento
    'Call Aguardar
    Call vVB_Generica_001.ExcluiBind(vBanco)
    vBanco.Parameters.Add "vCursor", 0, 3
    vBanco.Parameters("vCursor").ServerType = 102
    vBanco.Parameters("vCursor").DynasetOption = &H2&
    vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
    vSql = "PRODUCAO.PCK_CAD910.PR_SEL_RESSUPR(:vCursor)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    'Unload frmAguardar
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro & ". Erro ao preencher grid Ressuprimento.")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("vCursor").Value
    End If
    If Not vObjOracle.EOF Then
        'prepara grid
        mfgRessuprimento.Rows = 2
        mfgRessuprimento.Row = 1
        For i = 0 To mfgRessuprimento.Cols - 1
            mfgRessuprimento.Col = i
            mfgRessuprimento.Text = ""
        Next i
        'carrega dados do banco
        While Not vObjOracle.EOF
            'adiciona linha
            If mfgRessuprimento.Rows = 2 Then
                mfgRessuprimento.Col = 0
                mfgRessuprimento.Row = 1
                If mfgRessuprimento.Text <> "" Then
                    mfgRessuprimento.Rows = mfgRessuprimento.Rows + 1
                End If
            Else
                mfgRessuprimento.Rows = mfgRessuprimento.Rows + 1
            End If
            'escreve os dados
            mfgRessuprimento.Row = mfgRessuprimento.Rows - 1
            mfgRessuprimento.Col = 0
            mfgRessuprimento.Text = Format(CStr(vObjOracle!Cod_Loja_Destino), "00") & " - " & CStr(vObjOracle!Fanta1)
            mfgRessuprimento.Col = 1
            mfgRessuprimento.Text = Format(CStr(vObjOracle!Cod_Prioridade), "00")
            mfgRessuprimento.Col = 2
            mfgRessuprimento.Text = Format(CStr(vObjOracle!Cod_Loja_Origem), "00") & " - " & CStr(vObjOracle!Fanta2)
            mfgRessuprimento.Col = 3
            mfgRessuprimento.Text = ""
            'pr�ximo registro
            vObjOracle.MoveNext
        Wend
    End If

End Sub

Private Sub Form_Load()
    
    Me.Top = 0
    Me.Left = 0
    
    'carrega grid
    Call Define_Grid_Analise
    
    'carrega grid
    Call Define_Grid_Ressuprimento
    
    'posiciona na 1� Tab
    stbPrioridade.Tab = 0
        
End Sub

Private Sub mfgRessuprimento_DblClick()

    If mfgRessuprimento.Row > 0 Then
        If mfgRessuprimento.Col = 3 Then
            If Trim(mfgRessuprimento.Text) = "" Then
                mfgRessuprimento.Text = "X"
            Else
                mfgRessuprimento.Text = ""
            End If
        End If
    End If

End Sub

Private Sub mfgAnalise_DblClick()

    If mfgAnalise.Row > 0 Then
        If mfgAnalise.Col = 2 Then
            If Trim(mfgAnalise.Text) = "" Then
                mfgAnalise.Text = "X"
            Else
                mfgAnalise.Text = ""
            End If
        End If
    End If

End Sub

Private Sub stbPrioridade_Click(PreviousTab As Integer)

    If stbPrioridade.Tab = 0 Then
        Call Define_Grid_Analise
    End If
    If stbPrioridade.Tab = 1 Then
        Call Define_Grid_Ressuprimento
    End If

End Sub
