VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Begin VB.Form frmGrupo 
   BackColor       =   &H00E0E0E0&
   Caption         =   "CAD030 - Cadastro de Grupo"
   ClientHeight    =   5595
   ClientLeft      =   450
   ClientTop       =   2190
   ClientWidth     =   6045
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   5595
   ScaleWidth      =   6045
   Begin VB.Frame Frame1 
      Height          =   3495
      Left            =   210
      TabIndex        =   4
      Top             =   1125
      Width           =   5610
      Begin MSGrid.Grid grdGrupo 
         Height          =   2850
         Left            =   375
         TabIndex        =   5
         Top             =   390
         Width           =   4860
         _Version        =   65536
         _ExtentX        =   8572
         _ExtentY        =   5027
         _StockProps     =   77
         ForeColor       =   4210752
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MouseIcon       =   "frmGrupo.frx":0000
      End
   End
   Begin VB.Frame Frame2 
      Height          =   975
      Left            =   195
      TabIndex        =   0
      Top             =   150
      Width           =   5610
      Begin VB.TextBox txtDesc_Grupo 
         ForeColor       =   &H00404040&
         Height          =   315
         Left            =   1635
         MaxLength       =   20
         TabIndex        =   3
         Top             =   360
         Width           =   3255
      End
      Begin VB.TextBox txtCod_Grupo 
         ForeColor       =   &H00404040&
         Height          =   285
         Left            =   1110
         MaxLength       =   2
         TabIndex        =   2
         Top             =   375
         Width           =   495
      End
      Begin VB.Label Label1 
         Caption         =   "Grupo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   255
         Left            =   405
         TabIndex        =   1
         Top             =   405
         Width           =   735
      End
   End
   Begin Threed.SSCommand cmdLimpar 
      Height          =   585
      Left            =   4470
      TabIndex        =   6
      ToolTipText     =   "Limpar"
      Top             =   4785
      Width           =   585
      _Version        =   65536
      _ExtentX        =   1032
      _ExtentY        =   1032
      _StockProps     =   78
      Picture         =   "frmGrupo.frx":001C
   End
   Begin Threed.SSCommand cmdGravar 
      Height          =   585
      Left            =   3720
      TabIndex        =   7
      ToolTipText     =   "Gravar"
      Top             =   4785
      Width           =   585
      _Version        =   65536
      _ExtentX        =   1032
      _ExtentY        =   1032
      _StockProps     =   78
      ForeColor       =   -2147483640
      Picture         =   "frmGrupo.frx":08F6
   End
   Begin Threed.SSCommand cmdSair 
      Height          =   585
      Left            =   5220
      TabIndex        =   8
      ToolTipText     =   "Voltar"
      Top             =   4785
      Width           =   585
      _Version        =   65536
      _ExtentX        =   1032
      _ExtentY        =   1032
      _StockProps     =   78
      ForeColor       =   -2147483640
      Picture         =   "frmGrupo.frx":15D0
   End
End
Attribute VB_Name = "frmGrupo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Function fiIncluir(liCodigo)
    Dim liLinha As Integer
    fiIncluir = True
    grdGrupo.Col = 0
    For liLinha = 1 To grdGrupo.Rows - 1
        grdGrupo.Row = liLinha
        If Val(grdGrupo) = txtCod_Grupo Then
            fiIncluir = False
            Exit For
        End If
    Next liLinha
End Function

Private Sub cmdGravar_Click()
Dim lstrPLSQL As String
Dim vTPOP As String
Dim vMSG As String

If Trim(txtDesc_Grupo) = "" Then
    MsgBox "Descri��o de Grupo inv�lida !", vbCritical, "CAD030 - Aten��o"
    Exit Sub
End If
If Trim(txtCod_Grupo) = "" Then
    MsgBox "C�digo de Grupo inv�lido !", vbCritical, "CAD030 - Aten��o"
    Exit Sub
End If
    
If Not fiIncluir(txtCod_Grupo) Then
    vTPOP = "A"
    vMSG = "Altera��o"
Else
    vTPOP = "I"
    vMSG = "Inclus�o"
End If

If MsgBox("Confirma " & vMSG & " de grupo?", vbYesNo + vbQuestion, "CAD030 - Aten��o") = 6 Then
        
    Set OraParameters = vBanco.Parameters
    OraParameters.Remove "CODGRU":   OraParameters.Add "CODGRU", CDbl(txtCod_Grupo), 1
    OraParameters.Remove "DESGRU":   OraParameters.Add "DESGRU", txtDesc_Grupo, 1
    OraParameters.Remove "TPOP":     OraParameters.Add "TPOP", vTPOP, 1
    OraParameters.Remove "vErro":    OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    
    vBanco.ExecuteSQL "BEGIN PRODUCAO.PCK_CAD030.PR_ALTINS_GRUPO" & _
                             "(:CODGRU,:DESGRU,:TPOP,:vErro,:vMsgErro); END;"
                             
    If Val(IIf(IsNull(OraParameters("vErro")), 0, OraParameters("vErro"))) <> 0 Then
        MsgBox "Erro: " & Val(OraParameters("vErro").Value) & " - " & _
                          OraParameters("vMsgErro").Value & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
        Exit Sub
    Else
        MsgBox vMSG & " OK!", vbInformation, "CAD030 - Aten��o"
        MsgBox "Por favor, fa�a os devidos relacionamentos para o DPKNet!", vbInformation, "ATEN��O"
        Call Carrega_Grid_Grupo
        txtCod_Grupo = ""
        txtDesc_Grupo = ""
    End If

End If
End Sub

Private Sub cmdLimpar_Click()
    txtCod_Grupo = ""
    txtDesc_Grupo = ""
End Sub


Private Sub cmdSair_Click()
    Unload Me
End Sub


Private Sub Form_Load()
    
    Me.Top = 1425
    Me.Left = 110
    'Me.Top = MDIForm1.Top + 50
    'Me.Top = MDIForm1.Left + 50
    
    With grdGrupo
        .Row = 0
        .Col = 0
        .Text = "Codigo"
        .ColWidth(0) = 1000
        .FixedAlignment(0) = 2
        
        .Col = 1
        .Text = "Descri��o"
        .ColWidth(1) = 3500
        .FixedAlignment(1) = 2
        .ColAlignment(1) = 0
    End With
    Carrega_Grid_Grupo

End Sub

Sub Carrega_Grid_Grupo()

Dim liLinha As Integer
Dim lstrSQL As String
Dim objGrupos As Object
    
    Screen.MousePointer = vbHourglass
    MDIForm1.sspMsg.Caption = "PESQUISANDO GRUPOS . . ."

    Set OraParameters = vBanco.Parameters
    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    Criar_Cursor vBanco.Parameters, "PM_CURSOR1"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, "PRODUCAO.PCK_CAD030.Pr_Sel_Grupo(:PM_CURSOR1, :vErro, :vMsgErro)")
    If vErro = "" Then
        Set objGrupos = OraParameters("PM_CURSOR1").Value
    Else
        MsgBox "Problemas para selecionar grupos cadastrados!", vbCritical, "CAD030 - Aten��o"
        Exit Sub
    End If
    
    If objGrupos.EOF Then
        MsgBox "Grupo n�o encontrado", vbInformation, "CAD030 - Aten��o"
    Else
        liLinha = 0
        On Error Resume Next
        While Not objGrupos.EOF
            liLinha = liLinha + 1
            MDIForm1.sspMsg.Caption = "PESQUISANDO SUBGRUPOS . . . " & liLinha
            With grdGrupo
            .Rows = liLinha + 1
            .Row = liLinha
            .Col = 0
            .Text = objGrupos.Fields("cod_grupo")
            .Col = 1
            .Text = objGrupos.Fields("desc_grupo")
            End With
            objGrupos.MoveNext
        Wend
    End If

    MDIForm1.sspMsg.Caption = ""
    Screen.MousePointer = vbDefault

End Sub

Private Sub grdGrupo_DblClick()
    grdGrupo.Col = 0
    If grdGrupo <> "" Then
        txtCod_Grupo = grdGrupo.Text
        grdGrupo.Col = 1
        txtDesc_Grupo = grdGrupo.Text
        txtDesc_Grupo.SetFocus
    End If

End Sub

Private Sub txtDesc_Grupo_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
End Sub


