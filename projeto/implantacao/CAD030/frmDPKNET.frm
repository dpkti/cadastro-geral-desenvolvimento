VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form frmDPKNET 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Relacionamento DPK x DPKNET"
   ClientHeight    =   6570
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11130
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6570
   ScaleWidth      =   11130
   Begin VB.ComboBox cmbSubGrupos 
      Height          =   315
      Left            =   1305
      TabIndex        =   17
      Top             =   3735
      Width           =   3435
   End
   Begin VB.ComboBox cmbGrupos 
      Height          =   315
      Left            =   1035
      TabIndex        =   15
      Top             =   900
      Width           =   3570
   End
   Begin VB.Frame Frame3 
      Caption         =   "Subgrupos DPKNET"
      Height          =   2310
      Left            =   45
      TabIndex        =   8
      Top             =   4185
      Width           =   11040
      Begin VB.ListBox lstSubGrupos 
         Height          =   1620
         Left            =   90
         TabIndex        =   12
         Top             =   540
         Width           =   5190
      End
      Begin VB.ListBox lstSubGruposSelec 
         Height          =   1620
         Left            =   5850
         TabIndex        =   11
         Top             =   540
         Width           =   5100
      End
      Begin VB.CommandButton cmdADDsub 
         Caption         =   ">>"
         Height          =   465
         Left            =   5355
         TabIndex        =   10
         Top             =   810
         Width           =   420
      End
      Begin VB.CommandButton cmdDELsub 
         Caption         =   "<<"
         Height          =   465
         Left            =   5355
         TabIndex        =   9
         Top             =   1485
         Width           =   420
      End
      Begin VB.Label Label4 
         Caption         =   "Disponiveis"
         Height          =   330
         Left            =   90
         TabIndex        =   14
         Top             =   315
         Width           =   1050
      End
      Begin VB.Label Label3 
         Caption         =   "Selecionados"
         Height          =   330
         Left            =   5850
         TabIndex        =   13
         Top             =   315
         Width           =   1050
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Grupos DPKNET"
      Height          =   2310
      Left            =   45
      TabIndex        =   1
      Top             =   1305
      Width           =   11040
      Begin VB.CommandButton cmdDEL 
         Caption         =   "<<"
         Height          =   465
         Left            =   5310
         TabIndex        =   7
         Top             =   1485
         Width           =   420
      End
      Begin VB.CommandButton cmdADD 
         Caption         =   ">>"
         Height          =   465
         Left            =   5310
         TabIndex        =   6
         Top             =   810
         Width           =   420
      End
      Begin VB.ListBox lstGruposSelec 
         Height          =   1620
         Left            =   5805
         TabIndex        =   3
         Top             =   540
         Width           =   5145
      End
      Begin VB.ListBox lstGrupos 
         Height          =   1620
         Left            =   90
         TabIndex        =   2
         Top             =   540
         Width           =   5145
      End
      Begin VB.Label Label2 
         Caption         =   "Selecionados"
         Height          =   330
         Left            =   5805
         TabIndex        =   5
         Top             =   315
         Width           =   1050
      End
      Begin VB.Label Label1 
         Caption         =   "Disponiveis"
         Height          =   330
         Left            =   90
         TabIndex        =   4
         Top             =   315
         Width           =   1050
      End
   End
   Begin Threed.SSCommand cmdSair 
      Height          =   630
      Left            =   45
      TabIndex        =   0
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   630
      _Version        =   65536
      _ExtentX        =   1111
      _ExtentY        =   1111
      _StockProps     =   78
      ForeColor       =   -2147483640
      Picture         =   "frmDPKNET.frx":0000
   End
   Begin VB.Label Label6 
      AutoSize        =   -1  'True
      Caption         =   "Subgrupos DPK:"
      Height          =   195
      Left            =   90
      TabIndex        =   18
      Top             =   3825
      Width           =   1185
   End
   Begin VB.Label Label5 
      AutoSize        =   -1  'True
      Caption         =   "Grupos DPK:"
      Height          =   195
      Left            =   90
      TabIndex        =   16
      Top             =   990
      Width           =   930
   End
   Begin VB.Line Line1 
      X1              =   45
      X2              =   10935
      Y1              =   765
      Y2              =   765
   End
End
Attribute VB_Name = "frmDPKNET"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmbGrupos_Click()
    
    lstSubGrupos.Clear
    lstSubGruposSelec.Clear
    
    GruposComRelacionamento
    GruposSemRelacionamento
    SubGrupos
End Sub


Private Sub cmbSubGrupos_Click()
    SubGruposComRelacionamento
    SubGruposSemRelacionamento
End Sub

Private Sub cmdADD_Click()

    If lstGrupos.Text <> "" Then

        Set OraParameters = vBanco.Parameters
        OraParameters.Remove "CODGRUPO": OraParameters.Add "CODGRUPO", Left(cmbGrupos.Text, InStr(cmbGrupos.Text, " - ")), 1
        OraParameters.Remove "CODGRUPODPKNET": OraParameters.Add "CODGRUPODPKNET", Left(lstGrupos.Text, InStr(lstGrupos.Text, " - ")), 1
        OraParameters.Remove "ACT": OraParameters.Add "ACT", "I", 1
        OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
        OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
        
        vErro = vVB_Generica_001.ExecutaPl(vBanco, "PRODUCAO.PCK_CAD030.PR_ACT_GRUPODPKNET(:CODGRUPO, :CODGRUPODPKNET, :ACT, :vErro, :vMsgErro)")
        If vErro <> "" Then
            MsgBox "Problemas com cadastro!" & OraParameters("vErro").Value & " - " & OraParameters("vMsgErro").Value, vbCritical, "CAD030 - Aten��o"
            Exit Sub
        End If
        
        GruposComRelacionamento
        GruposSemRelacionamento
        
    End If

End Sub

Private Sub cmdADDsub_Click()


    If lstSubGrupos.Text <> "" Then

        Set OraParameters = vBanco.Parameters
        OraParameters.Remove "CODGRUPO": OraParameters.Add "CODGRUPO", Left(cmbGrupos.Text, InStr(cmbGrupos.Text, " - ")), 1
        OraParameters.Remove "CODGRUPODPKNET": OraParameters.Add "CODGRUPODPKNET", Left(lstSubGrupos.Text, InStr(lstSubGrupos.Text, " - ")), 1
        OraParameters.Remove "CODSUBGRUPO": OraParameters.Add "CODSUBGRUPO", Left(cmbSubGrupos.Text, InStr(cmbSubGrupos.Text, " - ")), 1
        OraParameters.Remove "CODSUBGRUPODPKNET": OraParameters.Add "CODSUBGRUPODPKNET", Mid(lstSubGrupos.Text, InStr(lstSubGrupos.Text, "/") + 1, InStr(InStr(lstSubGrupos.Text, "/"), lstSubGrupos.Text, " - ") - InStr(lstSubGrupos.Text, "/")), 1
        OraParameters.Remove "ACT": OraParameters.Add "ACT", "I", 1
        OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
        OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
        
        vErro = vVB_Generica_001.ExecutaPl(vBanco, "PRODUCAO.PCK_CAD030.PR_ACT_SUBGRUPODPKNET(:CODGRUPO, :CODGRUPODPKNET, :CODSUBGRUPO, :CODSUBGRUPODPKNET,:ACT, :vErro, :vMsgErro)")
        If vErro <> "" Then
            MsgBox "Problemas com cadastro!" & OraParameters("vErro").Value & " - " & OraParameters("vMsgErro").Value, vbCritical, "CAD030 - Aten��o"
            Exit Sub
        End If
        
        SubGruposComRelacionamento
        SubGruposSemRelacionamento
        
    End If

End Sub

Private Sub cmdDEL_Click()
    If lstGruposSelec.Text <> "" Then

        Set OraParameters = vBanco.Parameters
        OraParameters.Remove "CODGRUPO": OraParameters.Add "CODGRUPO", Left(cmbGrupos.Text, InStr(cmbGrupos.Text, " - ")), 1
        OraParameters.Remove "CODGRUPODPKNET": OraParameters.Add "CODGRUPODPKNET", Left(lstGruposSelec.Text, InStr(lstGruposSelec.Text, " - ")), 1
        OraParameters.Remove "ACT": OraParameters.Add "ACT", "D", 1
        OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
        OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
        
        vErro = vVB_Generica_001.ExecutaPl(vBanco, "PRODUCAO.PCK_CAD030.PR_ACT_GRUPODPKNET(:CODGRUPO, :CODGRUPODPKNET, :ACT, :vErro, :vMsgErro)")
        If vErro <> "" Then
            MsgBox "Problemas com cadastro!" & OraParameters("vErro").Value & " - " & OraParameters("vMsgErro").Value, vbCritical, "CAD030 - Aten��o"
            Exit Sub
        End If
        
        GruposComRelacionamento
        GruposSemRelacionamento
        
    End If
    
End Sub

Private Sub cmdDELsub_Click()

    If lstSubGruposSelec.Text <> "" Then

        Set OraParameters = vBanco.Parameters
        OraParameters.Remove "CODGRUPO": OraParameters.Add "CODGRUPO", Left(cmbGrupos.Text, InStr(cmbGrupos.Text, " - ")), 1
        OraParameters.Remove "CODGRUPODPKNET": OraParameters.Add "CODGRUPODPKNET", Left(lstSubGruposSelec.Text, InStr(lstSubGruposSelec.Text, " - ")), 1
        OraParameters.Remove "CODSUBGRUPO": OraParameters.Add "CODSUBGRUPO", Left(cmbSubGrupos.Text, InStr(cmbSubGrupos.Text, " - ")), 1
        OraParameters.Remove "CODSUBGRUPODPKNET": OraParameters.Add "CODSUBGRUPODPKNET", Mid(lstSubGruposSelec.Text, InStr(lstSubGruposSelec.Text, "/") + 1, InStr(InStr(lstSubGruposSelec.Text, "/"), lstSubGruposSelec.Text, " - ") - InStr(lstSubGruposSelec.Text, "/")), 1
        OraParameters.Remove "ACT": OraParameters.Add "ACT", "D", 1
        OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
        OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
        
        vErro = vVB_Generica_001.ExecutaPl(vBanco, "PRODUCAO.PCK_CAD030.PR_ACT_SUBGRUPODPKNET(:CODGRUPO, :CODGRUPODPKNET, :CODSUBGRUPO, :CODSUBGRUPODPKNET,:ACT, :vErro, :vMsgErro)")
        If vErro <> "" Then
            MsgBox "Problemas com cadastro!" & OraParameters("vErro").Value & " - " & OraParameters("vMsgErro").Value, vbCritical, "CAD030 - Aten��o"
            Exit Sub
        End If
        
        SubGruposComRelacionamento
        SubGruposSemRelacionamento
        
    End If

End Sub

Private Sub cmdSair_Click()
    Unload Me
End Sub



Private Sub Form_Load()

    Set OraParameters = vBanco.Parameters
    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    Criar_Cursor vBanco.Parameters, "PM_CURSOR1"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, "PRODUCAO.PCK_CAD030.Pr_Sel_Grupo(:PM_CURSOR1, :vErro, :vMsgErro)")
    If vErro = "" Then
        Set objGrupos = OraParameters("PM_CURSOR1").Value
    Else
        MsgBox "Problemas para selecionar grupos cadastrados!", vbCritical, "CAD030 - Aten��o"
        Exit Sub
    End If
    
    cmbGrupos.AddItem ("")
    
    While Not objGrupos.EOF
    
        cmbGrupos.AddItem (objGrupos("cod_grupo") & " - " & objGrupos("desc_grupo"))
        
        objGrupos.MoveNext
    Wend

End Sub



Private Sub GruposComRelacionamento()

    lstGruposSelec.Clear
    
    If Left(cmbGrupos.Text, InStr(cmbGrupos.Text, " - ")) <> "" Then

        Set OraParameters = vBanco.Parameters
        OraParameters.Remove "CODGRUPO": OraParameters.Add "CODGRUPO", Left(cmbGrupos.Text, InStr(cmbGrupos.Text, " - ")), 1
        OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
        OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
        Criar_Cursor vBanco.Parameters, "PM_CURSOR1"
        
        vErro = vVB_Generica_001.ExecutaPl(vBanco, "PRODUCAO.PCK_CAD030.PR_SEL_GRUPO_COMRELACAO(:PM_CURSOR1, :CODGRUPO, :vErro, :vMsgErro)")
        If vErro = "" Then
            Set objGrupos = OraParameters("PM_CURSOR1").Value
        Else
            MsgBox "Problemas para selecionar grupos cadastrados!", vbCritical, "CAD030 - Aten��o"
            Exit Sub
        End If
        
        While Not objGrupos.EOF
        
            lstGruposSelec.AddItem (objGrupos("cod_grupo_dpknet") & " - " & objGrupos("descricao"))
            
            objGrupos.MoveNext
        Wend
    
    End If

End Sub


Private Sub GruposSemRelacionamento()

    lstGrupos.Clear

    If Left(cmbGrupos.Text, InStr(cmbGrupos.Text, " - ")) <> "" Then

        Set OraParameters = vBanco.Parameters
        OraParameters.Remove "CODGRUPO": OraParameters.Add "CODGRUPO", Left(cmbGrupos.Text, InStr(cmbGrupos.Text, " - ")), 1
        OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
        OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
        Criar_Cursor vBanco.Parameters, "PM_CURSOR1"
        
        vErro = vVB_Generica_001.ExecutaPl(vBanco, "PRODUCAO.PCK_CAD030.PR_SEL_GRUPO_SEMRELACAO(:PM_CURSOR1, :CODGRUPO, :vErro, :vMsgErro)")
        If vErro = "" Then
            Set objGrupos = OraParameters("PM_CURSOR1").Value
        Else
            MsgBox "Problemas para selecionar grupos cadastrados!", vbCritical, "CAD030 - Aten��o"
            Exit Sub
        End If
        
        While Not objGrupos.EOF
        
            lstGrupos.AddItem (objGrupos("cod_grupo_dpknet") & " - " & objGrupos("descricao"))
            
            objGrupos.MoveNext
        Wend
    
    End If

End Sub



Private Sub SubGrupos()

    Set OraParameters = vBanco.Parameters
    OraParameters.Remove "CODGRUPO": OraParameters.Add "CODGRUPO", Left(cmbGrupos.Text, InStr(cmbGrupos.Text, " - ")), 1
    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    Criar_Cursor vBanco.Parameters, "PM_CURSOR1"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, "PRODUCAO.PCK_CAD030.PR_SEL_SUBGRUPO(:PM_CURSOR1, :CODGRUPO, :vErro, :vMsgErro)")
    If vErro = "" Then
        Set objGrupos = OraParameters("PM_CURSOR1").Value
    Else
        MsgBox "Problemas para selecionar grupos cadastrados!", vbCritical, "CAD030 - Aten��o"
        Exit Sub
    End If
    
    cmbSubGrupos.Clear
    
    While Not objGrupos.EOF
    
        cmbSubGrupos.AddItem (objGrupos("cod_subgrupo") & " - " & objGrupos("desc_subgrupo"))
        
        objGrupos.MoveNext
    Wend

End Sub

Private Sub SubGruposComRelacionamento()

    Set OraParameters = vBanco.Parameters
    OraParameters.Remove "CODGRUPO": OraParameters.Add "CODGRUPO", Left(cmbGrupos.Text, InStr(cmbGrupos.Text, " - ")), 1
    OraParameters.Remove "CODSUBGRUPO": OraParameters.Add "CODSUBGRUPO", Left(cmbSubGrupos.Text, InStr(cmbSubGrupos.Text, " - ")), 1
    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    Criar_Cursor vBanco.Parameters, "PM_CURSOR1"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, "PRODUCAO.PCK_CAD030.PR_SEL_SUBGRUPO_COMRELACAO(:PM_CURSOR1, :CODGRUPO, :CODSUBGRUPO, :vErro, :vMsgErro)")
    If vErro = "" Then
        Set objGrupos = OraParameters("PM_CURSOR1").Value
    Else
        MsgBox "Problemas para selecionar grupos cadastrados!", vbCritical, "CAD030 - Aten��o"
        Exit Sub
    End If
    
    lstSubGruposSelec.Clear
    
    While Not objGrupos.EOF
    
        lstSubGruposSelec.AddItem (objGrupos("cod_grupo_dpknet") & " - " & objGrupos("grupo") & "/" & objGrupos("cod_subgrupo_dpknet") & " - " & objGrupos("subgrupo"))
        
        objGrupos.MoveNext
    Wend

End Sub


Private Sub SubGruposSemRelacionamento()

    Set OraParameters = vBanco.Parameters
    OraParameters.Remove "CODGRUPO": OraParameters.Add "CODGRUPO", Left(cmbGrupos.Text, InStr(cmbGrupos.Text, " - ")), 1
    OraParameters.Remove "CODSUBGRUPO": OraParameters.Add "CODSUBGRUPO", Left(cmbSubGrupos.Text, InStr(cmbSubGrupos.Text, " - ")), 1
    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    Criar_Cursor vBanco.Parameters, "PM_CURSOR1"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, "PRODUCAO.PCK_CAD030.PR_SEL_SUBGRUPO_SEMRELACAO(:PM_CURSOR1, :CODGRUPO, :CODSUBGRUPO, :vErro, :vMsgErro)")
    If vErro = "" Then
        Set objGrupos = OraParameters("PM_CURSOR1").Value
    Else
        MsgBox "Problemas para selecionar grupos cadastrados!", vbCritical, "CAD030 - Aten��o"
        Exit Sub
    End If
    
    lstSubGrupos.Clear
    
    While Not objGrupos.EOF
    
        lstSubGrupos.AddItem (objGrupos("cod_grupo_dpknet") & " - " & objGrupos("grupo") & "/" & objGrupos("cod_subgrupo_dpknet") & " - " & objGrupos("subgrupo"))
        
        objGrupos.MoveNext
    Wend

End Sub


