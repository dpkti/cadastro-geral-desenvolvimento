VERSION 5.00
Begin VB.Form frmSobre1 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "SOBRE"
   ClientHeight    =   3240
   ClientLeft      =   1275
   ClientTop       =   1620
   ClientWidth     =   6840
   ClipControls    =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   3240
   ScaleWidth      =   6840
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Height          =   375
      Left            =   3000
      TabIndex        =   4
      Top             =   2760
      Width           =   975
   End
   Begin VB.PictureBox SSCommand1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   870
      Left            =   75
      Picture         =   "FRMSOBRE.frx":0000
      ScaleHeight     =   810
      ScaleWidth      =   1695
      TabIndex        =   3
      Top             =   240
      Width           =   1755
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Caption         =   "Analista Respons�vel: Alethea (Maxxipel) - Ramal Suporte: 776/705"
      ForeColor       =   &H00800000&
      Height          =   375
      Left            =   2625
      TabIndex        =   2
      Top             =   1845
      Width           =   3015
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   "Programa: CAD030  -  Cadastro de Grupo e Subgrupo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   345
      Left            =   1635
      TabIndex        =   1
      Top             =   1245
      Width           =   5040
   End
   Begin VB.Label lblDpk 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackColor       =   &H00800000&
      BackStyle       =   0  'Transparent
      Caption         =   "Distribuidora de Pe�as Ltda"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   435
      Left            =   1920
      TabIndex        =   0
      Top             =   480
      Width           =   4770
   End
End
Attribute VB_Name = "frmSobre1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Private Sub cmdOK_Click()
  Unload frmSobre
  Set frmSobre = Nothing
End Sub


