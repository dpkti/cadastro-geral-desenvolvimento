VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmPromotor 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Associar Promotor a Carteira"
   ClientHeight    =   7920
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8145
   ControlBox      =   0   'False
   Icon            =   "frmPromotor.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7920
   ScaleWidth      =   8145
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ListView lsvCarteira 
      Height          =   3075
      Left            =   30
      TabIndex        =   6
      Top             =   4800
      Width           =   8085
      _ExtentX        =   14261
      _ExtentY        =   5424
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   3
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Regional"
         Object.Width           =   4410
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Promotor"
         Object.Width           =   4410
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Carteira"
         Object.Width           =   4410
      EndProperty
   End
   Begin VB.ComboBox CBOPROMOTOR 
      Height          =   315
      Left            =   1200
      TabIndex        =   2
      Text            =   "Combo1"
      Top             =   870
      Width           =   4275
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   0
      Top             =   810
      Width           =   8085
      _ExtentX        =   14261
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   30
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmPromotor.frx":23D2
      PICN            =   "frmPromotor.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdCarteira 
      Height          =   690
      Left            =   7410
      TabIndex        =   5
      TabStop         =   0   'False
      ToolTipText     =   "Ver Carteira"
      Top             =   60
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmPromotor.frx":30C8
      PICN            =   "frmPromotor.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSComctlLib.ListView LsvCartDisponivel 
      Height          =   3075
      Left            =   30
      TabIndex        =   7
      Top             =   1500
      Width           =   8085
      _ExtentX        =   14261
      _ExtentY        =   5424
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      Checkboxes      =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   3
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "C�d"
         Object.Width           =   1411
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Representante"
         Object.Width           =   4410
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Associado a Funcion�rio Promotor"
         Object.Width           =   5292
      EndProperty
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "Carteira:"
      Height          =   195
      Left            =   30
      TabIndex        =   4
      Top             =   1290
      Width           =   585
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Promotor"
      Height          =   195
      Left            =   60
      TabIndex        =   3
      Top             =   930
      Width           =   630
   End
End
Attribute VB_Name = "frmPromotor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : frmPromotor
' Author    : c.samuel.oliveira
' Date      : 09/11/15
' Purpose   : TI-1371
'---------------------------------------------------------------------------------------


Private Sub CBOPROMOTOR_Click()
    
   Dim litem As ListItem
   
   CURSOR "vCursor"

   vBanco.Parameters.Remove "Cod_Repres"
   vBanco.Parameters.Add "Cod_Repres", Val(CBOPROMOTOR), 1

   'TI-1371
   vBanco.Parameters.Remove "PM_TIPO"
   vBanco.Parameters.Add "PM_TIPO", IIf(FL_TIPO = 0, "P", "R"), 1
   'FIM TI-1371
   
   vSql = "PRODUCAO.PCK_CAD300.PR_SELECT_CARTEIRA(:vCursor, :COD_REPRES, :PM_TIPO)"
   vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
   If vErro <> "" Then
       vVB_Generica_001.ProcessaErro (vErro)
   Else
       Set vObjOracle = vBanco.Parameters("vCursor").Value
   End If
    
   LsvCartDisponivel.ListItems.Clear
   Do While vObjOracle.EOF = False
      Set litem = Me.LsvCartDisponivel.ListItems.Add
      litem = vObjOracle!Cod_Repres
      litem.SubItems(1) = vObjOracle!Pseudonimo
      If Val(vObjOracle!cod_repres_promotor) <> 0 And vObjOracle!cod_repres_promotor <> Val(CBOPROMOTOR) Then
        litem.SubItems(2) = vObjOracle!cod_repres_promotor & " - " & vObjOracle!NOME_TIPO_F
      End If
      vObjOracle.MoveNext
   Loop
    
   Call vVB_Generica_001.ExcluiBind(vBanco)
    
   Marcar_Representantes
    
   Detalhes_Carteira
    
End Sub


Private Sub cmdCarteira_Click()
    If Me.Height > 5085 Then
        Me.Height = 5085
    Else
        Me.Height = 8145
    End If
    
    Me.Top = (Screen.Height - Me.Height) / 2
    Me.Left = (Screen.Width - Me.Width) / 2
    
End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    'TI-1371
    If FL_TIPO = 1 Then
        Me.Caption = "Associar Representante a Carteira"
        Me.Label1.Caption = "Representante"
        Me.LsvCartDisponivel.ColumnHeaders(3).Text = "Associado a Funcion�rio Representante"
    End If
    'FIM TI-1371
    
    Me.Height = 5085
    Promotor
End Sub

Sub Promotor()

    CURSOR "vCursor"

   'TI-1371
   vBanco.Parameters.Remove "PM_TIPO"
   vBanco.Parameters.Add "PM_TIPO", IIf(FL_TIPO = 0, "F", "R"), 1
   'FIM TI-1371
   
    vSql = "PRODUCAO.PCK_CAD300.PR_SELECT_PROMOTOR(:vCursor,:PM_TIPO)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro)
    Else
        Set vObjOracle = vBanco.Parameters("vCursor").Value
    End If
    
    CBOPROMOTOR.Clear
    
    If vObjOracle.RecordCount <> 0 Then
        Call vVB_Generica_001.PreencheComboList(vObjOracle, frmPromotor.CBOPROMOTOR, "COD_REPRES", "PSEUDONIMO")
    End If

    Call vVB_Generica_001.ExcluiBind(vBanco)

End Sub

Sub Marcar_Representantes()
    
    Me.LsvCartDisponivel.Tag = "PREENCHENDO"
    
    CURSOR "vCursor"

    vBanco.Parameters.Remove "Pm_Cod_Promotor"
    vBanco.Parameters.Add "Pm_Cod_Promotor", Val(CBOPROMOTOR), 1
    
    vSql = "PRODUCAO.PCK_CAD300.PR_SELECT_RELACIONAMENTOS(:vCursor, :Pm_Cod_Promotor)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro)
    Else
        Set vObjOracle = vBanco.Parameters("vCursor").Value
    End If

    Call vVB_Generica_001.ExcluiBind(vBanco)

    For i = 1 To vObjOracle.RecordCount
        For II = 1 To LsvCartDisponivel.ListItems.Count
            If Val(LsvCartDisponivel.ListItems(II)) = vObjOracle!Cod_Repres Then
                LsvCartDisponivel.ListItems(II).Checked = True
                Exit For
            End If
        Next
        vObjOracle.MoveNext
    Next
    Me.LsvCartDisponivel.Tag = ""
End Sub

Sub Detalhes_Carteira()
    Dim litem As ListItem
    
    CURSOR "vCursor"

    vBanco.Parameters.Remove "Pm_Cod_Promotor"
    vBanco.Parameters.Add "Pm_Cod_Promotor", Val(CBOPROMOTOR), 1
    
    vSql = "PRODUCAO.PCK_CAD300.PR_SELECT_DETALHES_CARTEIRA(:vCursor, :Pm_Cod_Promotor)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro)
    Else
        Set vObjOracle = vBanco.Parameters("vCursor").Value
    End If

    Me.lsvCarteira.ListItems.Clear

    For i = 1 To vObjOracle.RecordCount
        Set litem = Me.lsvCarteira.ListItems.Add
        litem = vObjOracle!Reg & " - " & vObjOracle!regional
        litem.SubItems(1) = vObjOracle!Cod_promotor & " - " & vObjOracle!Promotor
        litem.SubItems(2) = vObjOracle!Cod_carteira & " - " & vObjOracle!carteira
        vObjOracle.MoveNext
    Next

    Call vVB_Generica_001.ExcluiBind(vBanco)

End Sub

Private Sub LsvCartDisponivel_ItemCheck(ByVal Item As MSComctlLib.ListItem)
    Dim strMSG As String 'TI-1371
    
    If LsvCartDisponivel.Tag <> "" Then Exit Sub
    
    vBanco.Parameters.Remove "Pm_Cod_Promotor"
    vBanco.Parameters.Add "Pm_Cod_Promotor", Val(CBOPROMOTOR), 1
    
    vBanco.Parameters.Remove "Pm_Cod_Repres"
    vBanco.Parameters.Add "Pm_Cod_Repres", Item, 1
    
    If Item.SubItems(2) <> "" Then
        If Item.Checked = True Then
            'TI-1371
            If FL_TIPO = 0 Then
                strMSG = "Esta carteira est� associado a outro promotor, deseja excluir esta associa��o ?"
            ElseIf FL_TIPO = 1 Then
                strMSG = "Esta carteira est� associado a outro representante, deseja excluir esta associa��o ?"
                If FnValidaRepres Then
                    Item.Checked = False
                    Exit Sub
                End If
            End If
            'FIM TI-1371
            If MsgBox(strMSG, vbQuestion + vbYesNo) = vbYes Then 'TI-1371
                
                vBanco.Parameters.Remove "Pm_Cod_Promotor"
                vBanco.Parameters.Add "Pm_Cod_Promotor", Val(Item.SubItems(2)), 1
                vBanco.Parameters.Remove "Pm_Cod_Repres"
                vBanco.Parameters.Add "Pm_Cod_Repres", Item, 1
                vSql = "PRODUCAO.PCK_CAD300.PR_EXCLUIR_PROMOTOR(:Pm_Cod_Promotor, :Pm_Cod_repres)"
                vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
                If vErro <> "" Then vVB_Generica_001.ProcessaErro (vErro)
                    
                vBanco.Parameters.Remove "Pm_Cod_Promotor"
                vBanco.Parameters.Add "Pm_Cod_Promotor", Val(CBOPROMOTOR), 1
                vBanco.Parameters.Remove "Pm_Cod_Repres"
                vBanco.Parameters.Add "Pm_Cod_Repres", Item, 1
                vSql = "PRODUCAO.PCK_CAD300.PR_INSERT_PROMOTOR(:Pm_Cod_Promotor, :Pm_Cod_repres)"
                vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
                If vErro <> "" Then vVB_Generica_001.ProcessaErro (vErro)
                
            Else
                Item.Checked = False
            End If
        End If
    Else
        If Item.Checked = True Then
            'TI-1371
            If FL_TIPO = 1 Then
                If FnValidaRepres Then
                    Item.Checked = False
                    Exit Sub
                End If
            End If
            'FIM TI-1371
            vSql = "PRODUCAO.PCK_CAD300.PR_INSERT_PROMOTOR(:Pm_Cod_Promotor, :Pm_Cod_repres)"
        Else
            vSql = "PRODUCAO.PCK_CAD300.PR_EXCLUIR_PROMOTOR(:Pm_Cod_Promotor, :Pm_Cod_repres)"
        End If
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        If vErro <> "" Then vVB_Generica_001.ProcessaErro (vErro)
    
    End If
    
    Detalhes_Carteira 'TI-1371
    
    Call vVB_Generica_001.ExcluiBind(vBanco)

End Sub
'TI-1371
Private Function FnValidaRepres() As Boolean
    Dim te As Integer
    CURSOR ("vCursor")
    vBanco.Parameters.Remove "P_COD_REPRES"
    vBanco.Parameters.Add "P_COD_REPRES", Val(CBOPROMOTOR), 1
    vSql = "PRODUCAO.PCK_CAD300.PR_SEL_VALIDA_REPRES(:vCursor, :P_COD_REPRES)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro)
    Else
        Set vObjOracle = vBanco.Parameters("vCursor").Value
    End If
    If Not vObjOracle.BOF And Not vObjOracle.EOF Then
        If vObjOracle!tipo = "1" Then
            MsgBox "V�nculo n�o permitido! Representante possui cliente associado."
        ElseIf vObjOracle!tipo = "2" Then
            MsgBox "V�nculo n�o permitido! Representante possui vendedor associado."
        End If
        FnValidaRepres = True
    End If
    
End Function
'FIM TI-1371
