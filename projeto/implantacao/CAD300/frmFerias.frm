VERSION 5.00
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "msmask32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmFerias 
   Caption         =   "frmFerias"
   ClientHeight    =   7440
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   7635
   LinkTopic       =   "frmFerias"
   ScaleHeight     =   7440
   ScaleWidth      =   7635
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame1 
      Caption         =   "Regras cadastradas."
      Height          =   4215
      Left            =   120
      TabIndex        =   9
      Top             =   3120
      Width           =   7455
      Begin VB.CommandButton cmdExcluir 
         Caption         =   "Excluir regra"
         Height          =   375
         Left            =   5760
         TabIndex        =   17
         Top             =   3720
         Width           =   1455
      End
      Begin VB.CommandButton cmdAtualiza 
         Caption         =   "Atualizar lista"
         Height          =   375
         Left            =   120
         TabIndex        =   16
         Top             =   3720
         Width           =   1455
      End
      Begin MSComctlLib.ListView lstDados 
         Height          =   3375
         Left            =   120
         TabIndex        =   10
         Top             =   240
         Width           =   7155
         _ExtentX        =   12621
         _ExtentY        =   5953
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         AllowReorder    =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   6
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "ID_REGRA_FERIAS"
            Object.Width           =   0
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   1
            Text            =   "VENDEDOR"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   2
            Text            =   "CARTEIRA"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   3
            Text            =   "VENDEDOR"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   4
            Text            =   "DATA_INICIO"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   5
            Text            =   "DATA_FIM"
            Object.Width           =   2117
         EndProperty
      End
   End
   Begin VB.Frame Panel 
      Caption         =   "Cadastro do vendedor substituto durante as f�rias"
      Height          =   2115
      Left            =   120
      TabIndex        =   0
      Top             =   960
      Width           =   7425
      Begin VB.ComboBox cboVendedorFerias 
         Height          =   315
         Left            =   1440
         Style           =   2  'Dropdown List
         TabIndex        =   15
         Top             =   480
         Width           =   5835
      End
      Begin VB.ComboBox cboCarteiras 
         Height          =   315
         Left            =   1440
         Style           =   2  'Dropdown List
         TabIndex        =   14
         Top             =   870
         Width           =   2805
      End
      Begin VB.ComboBox cboVendedorDestino 
         Height          =   315
         Left            =   1440
         Style           =   2  'Dropdown List
         TabIndex        =   13
         Top             =   1230
         Width           =   5835
      End
      Begin MSMask.MaskEdBox txtDtFim 
         Height          =   345
         Left            =   2910
         TabIndex        =   11
         Top             =   1590
         Width           =   1035
         _ExtentX        =   1826
         _ExtentY        =   609
         _Version        =   393216
         MaxLength       =   10
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox txtDtInicio 
         Height          =   315
         Left            =   1440
         TabIndex        =   12
         Top             =   1620
         Width           =   1005
         _ExtentX        =   1773
         _ExtentY        =   556
         _Version        =   393216
         MaxLength       =   10
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin VB.Label Label5 
         Caption         =   "at�:"
         Height          =   225
         Left            =   2550
         TabIndex        =   5
         Top             =   1650
         Width           =   375
      End
      Begin VB.Label Label4 
         Caption         =   "Per�odo:"
         Height          =   255
         Left            =   720
         TabIndex        =   4
         Top             =   1680
         Width           =   675
      End
      Begin VB.Label Label3 
         Caption         =   "Carteiras:"
         Height          =   255
         Left            =   690
         TabIndex        =   3
         Top             =   900
         Width           =   705
      End
      Begin VB.Label Label2 
         Caption         =   "Vendedor Destino:"
         Height          =   285
         Left            =   30
         TabIndex        =   2
         Top             =   1260
         Width           =   1425
      End
      Begin VB.Label Label1 
         Caption         =   "Vendedor F�rias:"
         Height          =   225
         Left            =   150
         TabIndex        =   1
         Top             =   540
         Width           =   1245
      End
   End
   Begin Bot�o.cmd cmdFim 
      Height          =   690
      Left            =   0
      TabIndex        =   6
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   30
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmFerias.frx":0000
      PICN            =   "frmFerias.frx":001C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd btnSalvar 
      Height          =   690
      Left            =   720
      TabIndex        =   7
      TabStop         =   0   'False
      ToolTipText     =   "Gravar"
      Top             =   30
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmFerias.frx":0CF6
      PICN            =   "frmFerias.frx":0D12
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   0
      TabIndex        =   8
      Top             =   750
      Width           =   7305
      _ExtentX        =   12885
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
End
Attribute VB_Name = "frmFerias"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub btnSalvar_Click()
    'validando os dados
    
    If Val(cboVendedorFerias.Text) = 0 Then
        MsgBox "Selecione o vendedor que deve entrar em f�rias"
        Exit Sub
    End If
    
    If Val(cboVendedorDestino.Text) = 0 Then
        MsgBox "Selecione o vendedor que deve receber a comiss�o"
        Exit Sub
    End If
    
    If Not IsDate(txtDtInicio) Or Not IsDate(txtDtFim.Text) Then
        MsgBox "O intervalo informado n�o � v�lido"
        Exit Sub
    End If
    
    If txtDtFim.Text < txtDtInicio.Text Then
        MsgBox "A data inicial deve ser menor que a final"
        Exit Sub
    End If
    
    CURSOR "pm_cursor"
    'salvando o registro de f�rias
    'quando for um novo registro
    
    'removendo os parametros
    vBanco.Parameters.Remove "PR_COD_REPRES_PRINCIPAL"
    vBanco.Parameters.Remove "PR_COD_REPRES_SUBSTITUTO"
    vBanco.Parameters.Remove "PR_REPRES_CARTEIRA"
    vBanco.Parameters.Remove "PR_DATA_INICIO"
    vBanco.Parameters.Remove "PR_DATA_FIM"
    vBanco.Parameters.Remove "PR_ATIVO"
    vBanco.Parameters.Remove "PR_EXCLUIDO"
    'adicionando os parametros
    vBanco.Parameters.Add "PR_COD_REPRES_PRINCIPAL", Val(cboVendedorFerias.Text), 1
    vBanco.Parameters.Add "PR_COD_REPRES_SUBSTITUTO", Val(cboVendedorDestino.Text), 1
    vBanco.Parameters.Add "PR_REPRES_CARTEIRA", Val(cboCarteiras.Text), 1
    vBanco.Parameters.Add "PR_DATA_INICIO", Format(txtDtInicio.Text, "dd/mm/yyyy"), 1
    vBanco.Parameters.Add "PR_DATA_FIM", Format(txtDtFim.Text, "dd/mm/yyyy"), 1
    vBanco.Parameters.Add "PR_ATIVO", "1", 1
    vBanco.Parameters.Add "PR_EXCLUIDO", "0", 1
    'criando uma string com os parametros
    vSql = "PRODUCAO.PCK_CAD300.PR_INSERT_VENDEDOR_REGRA(:PM_CURSOR, :PR_COD_REPRES_PRINCIPAL, :PR_COD_REPRES_SUBSTITUTO, :PR_REPRES_CARTEIRA, :PR_DATA_INICIO, :PR_DATA_FIM, :PR_ATIVO, :PR_EXCLUIDO)"

    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro)
    Else
        Set vObjOracle = vBanco.Parameters("pm_cursor").Value
        
        If vObjOracle.RecordCount > 0 Then
            MsgBox vObjOracle!mensagem
        Else
            MsgBox "Erro ao cadastrar f�rias!"
        End If
    End If
    
End Sub

Private Sub cboVendedorFerias_Click()
   CURSOR "PM_CURSOR"

   vBanco.Parameters.Remove "PM_COD_REPRES"
   vBanco.Parameters.Add "PM_COD_REPRES", Val(cboVendedorFerias), 1
   vSql = "PRODUCAO.PCK_CAD300.PR_SEL_CARTEIRA_VENDEDOR(:PM_CURSOR, :PM_COD_REPRES)"
   
   vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro)
    Else
        Set vObjOracle = vBanco.Parameters("PM_CURSOR").Value
    End If
    
    cboCarteiras.Clear
    
    If vObjOracle.RecordCount <> 0 Then
        Call vVB_Generica_001.PreencheComboList(vObjOracle, frmFerias.cboCarteiras, "COD_REPRES", "PSEUDONIMO")
    End If
    
    cboCarteiras.AddItem "0 - TODAS CARTEIRAS", 0
    cboCarteiras.ListIndex = cboCarteiras.NewIndex
    
    Call vVB_Generica_001.ExcluiBind(vBanco)

End Sub

Private Sub cmdAtualiza_Click()
    
    LoadDados
    
End Sub

Private Sub cmdExcluir_Click()
    
    Dim codigoRegra As Integer
    If lstDados.ListItems.Count > 0 Then
        codigoRegra = Val(lstDados.SelectedItem.Text)
    Else
        codigoRegra = 0
    End If
    
    If codigoRegra = 0 Then
        MsgBox "Nenhuma regra selecionada"
        Exit Sub
    End If
    vBanco.Parameters.Remove "PR_ID_REGRA_FERIAS"
    vBanco.Parameters.Add "PR_ID_REGRA_FERIAS", codigoRegra, 1
    vSql = "PRODUCAO.PCK_CAD300.PR_DEL_VENDEDOR_REGRAS(:PR_ID_REGRA_FERIAS)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro)
    Else
        MsgBox "Regra excluida com sucesso!"
    End If
    
End Sub

Private Sub cmdFim_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Call LoadVendedores
    LoadDados
    
End Sub
 
Sub LoadVendedores()

    CURSOR "vCursor"

    vSql = "PRODUCAO.PCK_CAD300.PR_SEL_VENDEDORES(:vCursor)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro)
    Else
        Set vObjOracle = vBanco.Parameters("vCursor").Value
    End If

    cboVendedorFerias.Clear

    If vObjOracle.RecordCount <> 0 Then
        Call vVB_Generica_001.PreencheComboList(vObjOracle, frmFerias.cboVendedorFerias, "COD_REPRES", "NOME_REPRES")
    End If

    cboVendedorDestino.Clear

    If vObjOracle.RecordCount <> 0 Then
        Call vVB_Generica_001.PreencheComboList(vObjOracle, frmFerias.cboVendedorDestino, "COD_REPRES", "NOME_REPRES")
    End If

    Call vVB_Generica_001.ExcluiBind(vBanco)

End Sub




Sub LoadDados()

   Dim litem As ListItem
   
   CURSOR "PM_CURSOR"

   vSql = "PRODUCAO.PCK_CAD300.PR_SEL_VENDEDOR_REGRAS(:PM_CURSOR)"
   vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
   If vErro <> "" Then
       vVB_Generica_001.ProcessaErro (vErro)
   Else
       Set vObjOracle = vBanco.Parameters("PM_CURSOR").Value
   End If
    
   lstDados.ListItems.Clear
   Do While vObjOracle.EOF = False
      Set litem = Me.lstDados.ListItems.Add
      litem = vObjOracle!ID_REGRA_FERIAS
      litem.SubItems(1) = vObjOracle!COD_REPRES_PRINCIPAL
      litem.SubItems(2) = IIf(Trim(vObjOracle!COD_REPRES_CARTEIRA) = 0, "TODOS", vObjOracle!COD_REPRES_CARTEIRA)
      litem.SubItems(3) = vObjOracle!COD_REPRES_SUBSTITUTO
      litem.SubItems(4) = vObjOracle!DATA_INICIO
      litem.SubItems(5) = vObjOracle!DATA_FIM
      litem.ToolTipText = ""
      vObjOracle.MoveNext
   Loop
    
   Call vVB_Generica_001.ExcluiBind(vBanco)
    
   'Marcar_Representantes
    
   'Detalhes_Carteira
    
End Sub



