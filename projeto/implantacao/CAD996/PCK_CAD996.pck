CREATE OR REPLACE PACKAGE PRODSIC.PCK_CAD996 AUTHID DEFINER IS

  -- Author  : CONSULTOR30
  -- Created : 6/1/2009 9:00
  -- Purpose :

  TYPE TP_CURSOR IS REF CURSOR;
  
		-- DECLARA PROCEDURE PR_CNPJ_CONTRATANTE
		PROCEDURE PR_CNPJ_CONTRATANTE(PM_CURSOR OUT TP_CURSOR,
		                              PM_CNPJ   IN  NUMBER);

		-- DECLARA PROCEDURE PR_CNPJ_DESTINATARIO
		PROCEDURE PR_CNPJ_DESTINATARIO(PM_CURSOR OUT TP_CURSOR,
		                               PM_CNPJ   IN  NUMBER);

		-- DECLARA PROCEDURE PR_CNPJ_TRANSPORTADORA
		PROCEDURE PR_CNPJ_TRANSPORTADORA(PM_CURSOR OUT TP_CURSOR,
		                                 PM_CNPJ   IN  NUMBER);

		-- DECLARA PROCEDURE PR_GRAVA_CTRC
		PROCEDURE PR_CADASTRA_CTRC(PM_ACAO       IN  NUMBER,
																													PM_MSG        OUT VARCHAR2,
																													PM_COD_LOJA   IN  NUMBER,
																													PM_COD_TRANSP IN  NUMBER,
																													PM_CNPJ_DEST  IN  NUMBER,
																													PM_NUM_CTRC   IN  NUMBER,
																													PM_SERIE      IN  VARCHAR2,
																													PM_DT_CTRC    IN  VARCHAR2,
																													PM_MOVIMENTO  IN  VARCHAR2,
																													PM_COD_CFOP   IN  NUMBER,
																													PM_VL_FRETE   IN  NUMBER,
																													PM_VL_BASE    IN  NUMBER,
																													PM_PC_ICMS    IN  NUMBER,
																													PM_VL_ICMS    IN  NUMBER,
																													PM_SUBSERIE   IN  VARCHAR2);																																																																																																

  -- DECLARA PROCEDURE PR_VERIFICA_CTRC
		PROCEDURE PR_VERIFICA_CTRC(PM_CURSOR     OUT TP_CURSOR,
		                           PM_COD_LOJA   IN  NUMBER,
																													PM_COD_TRANSP IN  NUMBER,
																													PM_CNPJ_DEST  IN  NUMBER,
																													PM_NUM_CTRC   IN  NUMBER,
																													PM_SERIE      IN  VARCHAR2,
																													PM_DT_CTRC    IN  VARCHAR2);

  -- DECLARA PROCEDURE PR_CONSULTA_CTRC
		PROCEDURE PR_CONSULTA_CTRC(PM_CURSOR     OUT TP_CURSOR,
		                           PM_COD_LOJA   IN  NUMBER,
																													PM_COD_TRANSP IN  NUMBER,
																													PM_CNPJ_DEST  IN  NUMBER,
																													PM_NUM_CTRC   IN  NUMBER,																													
																													PM_DT_CTRC    IN  VARCHAR2);
																																																										
END PCK_CAD996;
/
CREATE OR REPLACE PACKAGE BODY PRODSIC.PCK_CAD996 IS
    
		-- CRIA PROCEDURE PR_CNPJ_CONTRATANTE
		PROCEDURE PR_CNPJ_CONTRATANTE(PM_CURSOR OUT TP_CURSOR,
																																PM_CNPJ   IN  NUMBER) IS
				
		BEGIN
				
				-- ABRE CURSOR 
				OPEN PM_CURSOR FOR 
				
						-- SELECT QUE RETORNA OS DADOS DO CD
						SELECT 
								A.COD_LOJA,
								A.NOME_FANTASIA,
								B.COD_UF
						FROM 
								LOJA A, 
								CIDADE B
						WHERE A.CGC        = PM_CNPJ
						AND   A.COD_CIDADE = B.COD_CIDADE;
				
		END PR_CNPJ_CONTRATANTE;

		-- CRIA PROCEDURE PR_CNPJ_DESTINATARIO
		PROCEDURE PR_CNPJ_DESTINATARIO(PM_CURSOR OUT TP_CURSOR,
																																	PM_CNPJ   IN  NUMBER) IS
				
		BEGIN
				
				-- ABRE CURSOR 
				OPEN PM_CURSOR FOR 
				
						-- SELECT QUE RETORNA OS DADOS DO DESTINATARIO
						SELECT DISTINCT 
								RAZAO
						FROM
								(SELECT 
										NOME_CLIENTE RAZAO
								FROM 
										CLIENTE
								WHERE CGC = PM_CNPJ
								UNION
								SELECT 
										A.NOME_FORNEC RAZAO
								FROM 
										FORNECEDOR A
								WHERE CGC = PM_CNPJ );
				
		END PR_CNPJ_DESTINATARIO;

		-- CRIA PROCEDURE PR_CNPJ_TRANSPORTADORA
		PROCEDURE PR_CNPJ_TRANSPORTADORA(PM_CURSOR OUT TP_CURSOR,
																																			PM_CNPJ   IN  NUMBER) IS
				
		BEGIN
				
				-- ABRE CURSOR 
				OPEN PM_CURSOR FOR 
				
						-- SELECT QUE RETORNA OS DADOS DA TRANSPORTADORA
						SELECT 
								A.NOME_TRANSP RAZAO, 
								A.COD_TRANSP
						FROM
								PRODSIC.TRANSPORTADORA A,
								EDI.R_TRANSP_CGC B
						WHERE (B.CGC       = PM_CNPJ 
						OR    B.CGC_TRANSP = PM_CNPJ) 
						AND   A.COD_TRANSP = B.COD_TRANSP;
				
		END PR_CNPJ_TRANSPORTADORA;

  -- CRIA PROCEDURE PR_GRAVA_CTRC
		PROCEDURE PR_CADASTRA_CTRC(PM_ACAO       IN  NUMBER,
																													PM_MSG        OUT VARCHAR2,
																													PM_COD_LOJA   IN  NUMBER,
																													PM_COD_TRANSP IN  NUMBER,
																													PM_CNPJ_DEST  IN  NUMBER,
																													PM_NUM_CTRC   IN  NUMBER,
																													PM_SERIE      IN  VARCHAR2,
																													PM_DT_CTRC    IN  VARCHAR2,
																													PM_MOVIMENTO  IN  VARCHAR2,
																													PM_COD_CFOP   IN  NUMBER,
																													PM_VL_FRETE   IN  NUMBER,
																													PM_VL_BASE    IN  NUMBER,
																													PM_PC_ICMS    IN  NUMBER,
																													PM_VL_ICMS    IN  NUMBER,
																													PM_SUBSERIE   IN  VARCHAR2) IS		
																													
				BEGIN
				
						--VERIFICA ACAO, QUANDO FOR 1 INSERE OU ATUALIZA
						IF PM_ACAO = 1	THEN
						
							 BEGIN
							
								  --INSERE DADOS
										INSERT INTO FISCAL.CONTROLE_CTRC
											 (COD_LOJA,
				        COD_TRANSP,
												CNPJ_DESTINATARIO,
												NUM_CTRC,
												SERIE_CTRC,
												DT_CTRC,
												DT_MOVIMENTO,
												COD_CFOP,
												VL_FRETE,
												VL_BASE_CALCULO,
												PC_ICMS,
												VL_ICMS,
												SUBSERIE_CTRC)
										VALUES
											 (PM_COD_LOJA,
												PM_COD_TRANSP,
												PM_CNPJ_DEST,
												PM_NUM_CTRC,
												PM_SERIE,
												TO_DATE(PM_DT_CTRC,'DDMMRRRR'),
												TO_DATE(PM_MOVIMENTO,'DDMMRRRR'),
												PM_COD_CFOP,
												PM_VL_FRETE,
												PM_VL_BASE,
												PM_PC_ICMS,
												PM_VL_ICMS,
												PM_SUBSERIE);
									
										--MSG DO PARAMETRO DE RETORNO
										PM_MSG := 'Inclus�o efetuada com sucesso!';
							
							 EXCEPTION
							
							
								  WHEN DUP_VAL_ON_INDEX THEN
								
											 --SE O DADO J� EXISTIR ATUALIZA TABELA
											 BEGIN
											
														UPDATE FISCAL.CONTROLE_CTRC
														SET   DT_MOVIMENTO      = TO_DATE(PM_MOVIMENTO,'DDMMRRRR'),
																				COD_CFOP          = PM_COD_CFOP,
																				VL_FRETE          = PM_VL_FRETE,
																				VL_BASE_CALCULO   = PM_VL_BASE,
																				PC_ICMS           = PM_PC_ICMS,
																				VL_ICMS           = PM_VL_ICMS,
																				SUBSERIE_CTRC     = PM_SUBSERIE
														WHERE COD_LOJA          = PM_COD_LOJA 
														AND			COD_TRANSP        = PM_COD_TRANSP
														AND			CNPJ_DESTINATARIO = PM_CNPJ_DEST
														AND			NUM_CTRC          = PM_NUM_CTRC
														AND			SERIE_CTRC        = PM_SERIE
														AND			DT_CTRC           = TO_DATE(PM_DT_CTRC,'DDMMRRRR');
													
														--MSG DO PARAMETRO DE RETORNO
														PM_MSG := 'Atualiza��o efetuada com sucesso!';
									
									   EXCEPTION
									   
												  WHEN OTHERS THEN
														  
														  NULL;
														
												END;
												
										WHEN OTHERS THEN
										
											 NULL;
								
							 END;
						
						ELSIF PM_ACAO = 2	THEN
						
	       --EXCLUI DADOS
							 DELETE FROM FISCAL.CONTROLE_CTRC
								WHERE COD_LOJA          = PM_COD_LOJA 
								AND			COD_TRANSP        = PM_COD_TRANSP
								AND			CNPJ_DESTINATARIO = PM_CNPJ_DEST
								AND			NUM_CTRC          = PM_NUM_CTRC
								AND			SERIE_CTRC        = PM_SERIE
								AND			DT_CTRC           = TO_DATE(PM_DT_CTRC,'DDMMRRRR');
						
							 --MSG DO PARAMETRO DE RETORNO
							 PM_MSG := 'Exclus�o efetuada com sucesso!';
						
						END IF;
						COMMIT;
						
				END PR_CADASTRA_CTRC;	

  -- CRIA PROCEDURE PR_VERIFICA_CTRC
		PROCEDURE PR_VERIFICA_CTRC(PM_CURSOR     OUT TP_CURSOR,
		                           PM_COD_LOJA   IN  NUMBER,
																													PM_COD_TRANSP IN  NUMBER,
																													PM_CNPJ_DEST  IN  NUMBER,
																													PM_NUM_CTRC   IN  NUMBER,
																													PM_SERIE      IN  VARCHAR2,
																													PM_DT_CTRC    IN  VARCHAR2) IS	
		  
		  BEGIN		
						
				  --ABRE CURSOR
						OPEN PM_CURSOR FOR
						  
						  -- SELECT QUE RETORNA TODOS OS DADOS DA TABELA CONTROLE_CTRC
								SELECT 
								  A.DT_MOVIMENTO,
										A.COD_CFOP,
										A.VL_FRETE,
										A.VL_BASE_CALCULO,
										A.PC_ICMS,
										A.VL_ICMS,
										A.SUBSERIE_CTRC
								FROM 
								  FISCAL.CONTROLE_CTRC A
								WHERE A.COD_LOJA          = PM_COD_LOJA 
								AND			A.COD_TRANSP        = PM_COD_TRANSP
								AND			A.CNPJ_DESTINATARIO = PM_CNPJ_DEST
								AND			A.NUM_CTRC          = PM_NUM_CTRC
								AND			A.SERIE_CTRC        = PM_SERIE
								AND			A.DT_CTRC           = TO_DATE(PM_DT_CTRC,'DDMMRRRR');
								
		  END PR_VERIFICA_CTRC;

  -- CRIA PROCEDURE PR_CONSULTA_CTRC
		PROCEDURE PR_CONSULTA_CTRC(PM_CURSOR     OUT TP_CURSOR,
		                           PM_COD_LOJA   IN  NUMBER,
																													PM_COD_TRANSP IN  NUMBER,
																													PM_CNPJ_DEST  IN  NUMBER,
																													PM_NUM_CTRC   IN  NUMBER,																													
																													PM_DT_CTRC    IN  VARCHAR2) IS	
		  
		  BEGIN		
						
				  --ABRE CURSOR
						OPEN PM_CURSOR FOR
						  
						  -- SELECT QUE RETORNA TODOS OS DADOS DA TABELA CONTROLE_CTRC
								SELECT 
								  (SELECT CGC FROM LOJA WHERE COD_LOJA = A.COD_LOJA) CGC,
										(SELECT CGC_TRANSP FROM EDI.R_TRANSP_CGC WHERE COD_TRANSP = A.COD_TRANSP) CGC_TRANSP,
										A.CNPJ_DESTINATARIO,
										A.NUM_CTRC,
										A.SERIE_CTRC,
										A.DT_CTRC,
								  A.DT_MOVIMENTO,
										A.COD_CFOP,
										A.VL_FRETE,
										A.VL_BASE_CALCULO,
										A.PC_ICMS,
										A.VL_ICMS,
										A.SUBSERIE_CTRC
								FROM 
								  FISCAL.CONTROLE_CTRC A
								WHERE A.COD_LOJA          = NVL(PM_COD_LOJA,A.COD_LOJA) 
								AND			A.COD_TRANSP        = NVL(PM_COD_TRANSP,A.COD_TRANSP)
								AND			A.CNPJ_DESTINATARIO = NVL(PM_CNPJ_DEST,A.CNPJ_DESTINATARIO)
								AND			A.NUM_CTRC          = NVL(PM_NUM_CTRC,A.NUM_CTRC)
								AND			A.DT_CTRC           = NVL(TO_DATE(PM_DT_CTRC,'DDMMRRRR'),A.DT_CTRC);
								
		  END PR_CONSULTA_CTRC;
											
END PCK_CAD996;
/
