VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmImportacao 
   Caption         =   "Importa��o"
   ClientHeight    =   6045
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10725
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   6045
   ScaleWidth      =   10725
   StartUpPosition =   3  'Windows Default
   Begin MSFlexGridLib.MSFlexGrid mfgArq 
      Height          =   4875
      Left            =   30
      TabIndex        =   0
      Top             =   0
      Width           =   10665
      _ExtentX        =   18812
      _ExtentY        =   8599
      _Version        =   393216
      Cols            =   14
      AllowUserResizing=   1
   End
   Begin Bot�o.cmd cmdImportar 
      Height          =   675
      Left            =   8790
      TabIndex        =   1
      ToolTipText     =   "Sair do Programa"
      Top             =   4920
      Width           =   1905
      _ExtentX        =   3360
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   "Salvar Arquivo"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmImportacao.frx":0000
      PICN            =   "frmImportacao.frx":001C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   2
      Top             =   5670
      Width           =   10725
      _ExtentX        =   18918
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   18415
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmImportacao"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdImportar_Click()
          
      Dim ArrDados(13) As String
      Dim vCor As String
      Dim i As Long, j As Long
      Dim vMSG As String

1         If Len(StatusBar1.Panels(1).Text) > 0 Then
2             MsgBox "Existe erros nos Dados.", vbCritical, "Aten��o"
3             Exit Sub
4         End If
          
5         On Error GoTo Trata_erro
          
6         Screen.MousePointer = 11
          
7         Conectar
          
8         For i = 1 To mfgArq.Rows - 1
          
9             With mfgArq
10                .Row = i
11                .Col = 1
12                vCor = .CellBackColor
13            End With
              
14            If vCor <> 65535 Then
15                For j = 1 To 13
                  
16                    With mfgArq
17                        .Row = i
18                        .Col = j
19                        ArrDados(j) = .Text
20                    End With
                      
21                Next
                      
22                vBanco.Parameters.Remove "pm_acao"
23                vBanco.Parameters.Add "pm_acao", 1, 1
                   
24                vBanco.Parameters.Remove "pm_msg"
25                vBanco.Parameters.Add "pm_msg", "", 2
              
26                vBanco.Parameters.Remove "PM_COD_LOJA"
27                vBanco.Parameters.Add "PM_COD_LOJA", RecuperaLoja(ArrDados(1)), 1
              
28                vBanco.Parameters.Remove "PM_COD_TRANSP"
29                vBanco.Parameters.Add "PM_COD_TRANSP", RecuperaTransp(ArrDados(3)), 1
              
30                vBanco.Parameters.Remove "PM_CNPJ_DEST"
31                vBanco.Parameters.Add "PM_CNPJ_DEST", Val(ArrDados(2)), 1
                  
32                vBanco.Parameters.Remove "PM_NUM_CTRC"
33                vBanco.Parameters.Add "PM_NUM_CTRC", Val(ArrDados(4)), 1
              
34                vBanco.Parameters.Remove "PM_SERIE"
35                vBanco.Parameters.Add "PM_SERIE", ArrDados(5), 1
              
36                vBanco.Parameters.Remove "PM_DT_CTRC"
37                vBanco.Parameters.Add "PM_DT_CTRC", Replace(ArrDados(7), "/", ""), 1
                  
38                vBanco.Parameters.Remove "PM_MOVIMENTO"
39                vBanco.Parameters.Add "PM_MOVIMENTO", Replace(ArrDados(8), "/", ""), 1
              
40                vBanco.Parameters.Remove "PM_COD_CFOP"
41                vBanco.Parameters.Add "PM_COD_CFOP", Val(ArrDados(9)), 1
              
42                vBanco.Parameters.Remove "PM_VL_FRETE"
43                vBanco.Parameters.Add "PM_VL_FRETE", IIf(Len(ArrDados(10)) = 0, 0, Val(ArrDados(10))), 1
              
44                vBanco.Parameters.Remove "PM_VL_BASE"
45                vBanco.Parameters.Add "PM_VL_BASE", IIf(Len(ArrDados(11)) = 0, 0, Val(ArrDados(11))), 1
                  
46                vBanco.Parameters.Remove "PM_PC_ICMS"
47                vBanco.Parameters.Add "PM_PC_ICMS", IIf(Len(ArrDados(12)) = 0, 0, Val(ArrDados(12))), 1
              
48                vBanco.Parameters.Remove "PM_VL_ICMS"
49                vBanco.Parameters.Add "PM_VL_ICMS", IIf(Len(ArrDados(13)) = 0, 0, Round(Val(ArrDados(13)), 2)), 1
              
50                vBanco.Parameters.Remove "PM_SUBSERIE"
51                vBanco.Parameters.Add "PM_SUBSERIE", IIf(Len(ArrDados(6)) = 0, "01", ArrDados(6)), 1
              
52                vBanco.ExecuteSQL "Begin PRODSIC.PCK_CAD996.PR_CADASTRA_CTRC(:PM_ACAO, " & _
                                                                              ":PM_MSG, " & _
                                                                              ":PM_COD_LOJA, " & _
                                                                              ":PM_COD_TRANSP, " & _
                                                                              ":PM_CNPJ_DEST, " & _
                                                                              ":PM_NUM_CTRC, " & _
                                                                              ":PM_SERIE, " & _
                                                                              ":PM_DT_CTRC, " & _
                                                                              ":PM_MOVIMENTO, " & _
                                                                              ":PM_COD_CFOP, " & _
                                                                              ":PM_VL_FRETE, " & _
                                                                              ":PM_VL_BASE, " & _
                                                                              ":PM_PC_ICMS, " & _
                                                                              ":PM_VL_ICMS, " & _
                                                                              ":PM_SUBSERIE);END;"
          
                 ' vMSG = vBanco.Parameters("pm_msg").Value
                         
53            End If
54        Next
          
55        Desconectar
          
56        Quantidade
          
57        MsgBox "Inclus�o/Altera��o OK ", vbInformation, "Aten��o"
58        Screen.MousePointer = 0
59        Unload Me
          
Trata_erro:
60        If Err.Number <> 0 Then
61            MsgBox "Sub: cmdImportar_Click" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl
62            Screen.MousePointer = 0
63            Desconectar
64      End If
          
End Sub

Private Sub Form_Load()
    
    Me.Top = 2000
    Me.Left = 2000
    
    frmImportacao.mfgArq.Clear
    
    mfgArq.ColWidth(0) = 200
    
    With mfgArq
        .Row = 0
        
        .Col = 1
        .Text = "CNPJ CD CONTRATANTE"
        .ColWidth(.Col) = 2100
        
        .Col = 2
        .Text = "CNPJ DESTINAT�RIO"
        .ColWidth(.Col) = 1800
        
        .Col = 3
        .Text = "CNPJ TRANSPORTADORA"
        .ColWidth(.Col) = 2200
             
        .Col = 4
        .Text = "NR. CTRC"
        .ColWidth(.Col) = 900
        
        .Col = 5
        .Text = "SERIE CTRC"
        .ColWidth(.Col) = 1100
        
        .Col = 6
        .Text = "SUBSERIE CTRC"
        .ColWidth(.Col) = 1400
        
        .Col = 7
        .Text = "DT. CTRC"
        .ColWidth(.Col) = 1000
        
        .Col = 8
        .Text = "DT. MOVIMENTO"
        .ColWidth(.Col) = 1400
        
        .Col = 9
        .Text = "CFOP"
        .ColWidth(.Col) = 700
        
        .Col = 10
        .Text = "VL. FRETE"
        .ColWidth(.Col) = 1000
        
        .Col = 11
        .Text = "VL. BASE"
        .ColWidth(.Col) = 1000
        
        .Col = 12
        .Text = "PC. ICMS"
        .ColWidth(.Col) = 1000
        
        .Col = 13
        .Text = "VL. ICMS"
        .ColWidth(.Col) = 1000
        
    End With
    
End Sub

Function RecuperaLoja(ByVal CNPJ As String)

      Dim vObj As Object
      Dim vCDLoja As Integer

1         On Error GoTo Trata_erro
          
          'Verifica se digitou algum cnpj de contratante,
          'se digitou carrega os dados do contratante
2         If Len(CNPJ) = 0 Then
3             Exit Function
4         End If
           
5         vBanco.Parameters.Remove "PM_CNPJ"
6         vBanco.Parameters.Add "PM_CNPJ", CNPJ, 1

7         Criar_Cursor

8         vBanco.ExecuteSQL "Begin PRODSIC.PCK_CAD996.PR_CNPJ_CONTRATANTE(:pm_cursor,:PM_CNPJ);END;"
          
9         Set vObj = vBanco.Parameters("pm_cursor").Value

10        If Not vObj.BOF And Not vObj.EOF Then
11            vCDLoja = vObj!cod_loja
12        Else
13            vCDLoja = 0
14        End If

15        Set vObj = Nothing

16        RecuperaLoja = vCDLoja
          
Trata_erro:
17        If Err.Number <> 0 Then
18            MsgBox "Sub: RecuperaLoja" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl
19        End If
          
End Function

Function RecuperaTransp(ByVal CNPJ As String)

      Dim vObj As Object
      Dim vTrans As Long

1         On Error GoTo Trata_erro
          
          'Verifica se digitou algum cnpj de contratante,
          'se digitou carrega os dados do contratante
2         If Len(CNPJ) = 0 Then
3             Exit Function
4         End If
           
5         vBanco.Parameters.Remove "PM_CNPJ"
6         vBanco.Parameters.Add "PM_CNPJ", CNPJ, 1

7         Criar_Cursor

8         vBanco.ExecuteSQL "Begin PRODSIC.PCK_CAD996.PR_CNPJ_TRANSPORTADORA(:pm_cursor,:PM_CNPJ);END;"
          
9         Set vObj = vBanco.Parameters("pm_cursor").Value

10        If Not vObj.BOF And Not vObj.EOF Then
11            vTrans = vObj!COD_TRANSP
12        Else
13            vTrans = 0
14        End If

15        Set vObj = Nothing
             
16        RecuperaTransp = vTrans
          
Trata_erro:
17        If Err.Number <> 0 Then
18            MsgBox "Sub: RecuperaTransp" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl
19        End If
          
End Function

Function ValidaDest(ByVal CNPJ As String)

      Dim vObj As Object
      Dim vDest As Byte

1         On Error GoTo Trata_erro
          
          'Verifica se digitou algum cnpj de contratante,
          'se digitou carrega os dados do contratante
2         If Len(CNPJ) = 0 Then
3             Exit Function
4         End If
           
5         vBanco.Parameters.Remove "PM_CNPJ"
6         vBanco.Parameters.Add "PM_CNPJ", CNPJ, 1

7         Criar_Cursor

8         vBanco.ExecuteSQL "Begin PRODSIC.PCK_CAD996.PR_CNPJ_DESTINATARIO(:pm_cursor,:PM_CNPJ);END;"
          
9         Set vObj = vBanco.Parameters("pm_cursor").Value

10        If Not vObj.BOF And Not vObj.EOF Then
11            vDest = 1
12        Else
13            vDest = 0
14        End If

15        Set vObj = Nothing
             
16        ValidaDest = vDest
          
Trata_erro:
17        If Err.Number <> 0 Then
18            MsgBox "Sub: ValidaDest" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl
19        End If
          
End Function
