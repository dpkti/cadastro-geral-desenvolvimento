VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "msmask32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmCadastro 
   Caption         =   "Cadastro CTRC Fiscal"
   ClientHeight    =   4500
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10725
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   4500
   ScaleWidth      =   10725
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame fraBotoes 
      Height          =   1155
      Left            =   30
      TabIndex        =   30
      Top             =   3330
      Width           =   10665
      Begin Bot�o.cmd cmdExcluir 
         Height          =   675
         Left            =   3675
         TabIndex        =   31
         ToolTipText     =   "Excluir Registro"
         Top             =   270
         Width           =   1665
         _ExtentX        =   2937
         _ExtentY        =   1191
         BTYPE           =   3
         TX              =   "Excluir"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadastro.frx":0000
         PICN            =   "frmCadastro.frx":001C
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdSalvar 
         Height          =   675
         Left            =   255
         TabIndex        =   32
         ToolTipText     =   "Salvar Registro"
         Top             =   270
         Width           =   1665
         _ExtentX        =   2937
         _ExtentY        =   1191
         BTYPE           =   3
         TX              =   "Incluir/Alterar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadastro.frx":0CF6
         PICN            =   "frmCadastro.frx":0D12
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdConsultar 
         Height          =   675
         Left            =   1965
         TabIndex        =   33
         ToolTipText     =   "Consultar Registro"
         Top             =   270
         Width           =   1665
         _ExtentX        =   2937
         _ExtentY        =   1191
         BTYPE           =   3
         TX              =   "Consultar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadastro.frx":19EC
         PICN            =   "frmCadastro.frx":1A08
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdLimpar 
         Height          =   675
         Left            =   5385
         TabIndex        =   34
         ToolTipText     =   "Sair do Programa"
         Top             =   270
         Width           =   1665
         _ExtentX        =   2937
         _ExtentY        =   1191
         BTYPE           =   3
         TX              =   "Limpar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadastro.frx":26E2
         PICN            =   "frmCadastro.frx":26FE
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdSair 
         Height          =   675
         Left            =   8820
         TabIndex        =   35
         ToolTipText     =   "Sair do Programa"
         Top             =   270
         Width           =   1665
         _ExtentX        =   2937
         _ExtentY        =   1191
         BTYPE           =   3
         TX              =   "Sair"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadastro.frx":33D8
         PICN            =   "frmCadastro.frx":33F4
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdImportar 
         Height          =   675
         Left            =   7110
         TabIndex        =   36
         ToolTipText     =   "Sair do Programa"
         Top             =   270
         Width           =   1665
         _ExtentX        =   2937
         _ExtentY        =   1191
         BTYPE           =   3
         TX              =   "Importar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadastro.frx":40CE
         PICN            =   "frmCadastro.frx":40EA
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
   End
   Begin VB.Frame fraDados 
      Height          =   3315
      Left            =   30
      TabIndex        =   0
      Top             =   0
      Width           =   10665
      Begin MSComDlg.CommonDialog CMMDIALOG 
         Left            =   9900
         Top             =   120
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin MSMask.MaskEdBox mskMovimento 
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "dd/MM/yyyy"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1046
            SubFormatType   =   3
         EndProperty
         Height          =   315
         Left            =   4530
         TabIndex        =   8
         Top             =   2430
         Width           =   1155
         _ExtentX        =   2037
         _ExtentY        =   556
         _Version        =   393216
         PromptInclude   =   0   'False
         MaxLength       =   10
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox mskDtCTRC 
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "dd/MM/yyyy"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1046
            SubFormatType   =   3
         EndProperty
         Height          =   315
         Left            =   1980
         TabIndex        =   7
         Top             =   2430
         Width           =   1155
         _ExtentX        =   2037
         _ExtentY        =   556
         _Version        =   393216
         PromptInclude   =   0   'False
         MaxLength       =   10
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin VB.TextBox txtVlICMS 
         Height          =   315
         Left            =   9210
         MaxLength       =   14
         TabIndex        =   13
         Top             =   2850
         Width           =   1155
      End
      Begin VB.TextBox txtPcICMS 
         Height          =   315
         Left            =   7110
         MaxLength       =   5
         TabIndex        =   12
         Top             =   2850
         Width           =   1155
      End
      Begin VB.TextBox txtCFOP 
         Height          =   315
         Left            =   7110
         MaxLength       =   1
         TabIndex        =   9
         Top             =   2430
         Width           =   1155
      End
      Begin VB.TextBox txtSubSerie 
         Height          =   315
         Left            =   7110
         MaxLength       =   2
         TabIndex        =   6
         Top             =   2010
         Width           =   1155
      End
      Begin VB.TextBox txtBase 
         Height          =   315
         Left            =   4530
         MaxLength       =   14
         TabIndex        =   11
         Top             =   2850
         Width           =   1155
      End
      Begin VB.TextBox txtSerie 
         Height          =   315
         Left            =   4530
         MaxLength       =   3
         TabIndex        =   5
         Top             =   2010
         Width           =   1125
      End
      Begin VB.TextBox txtFrete 
         Height          =   315
         Left            =   1980
         MaxLength       =   14
         TabIndex        =   10
         Top             =   2850
         Width           =   1155
      End
      Begin VB.TextBox txtNrCTRC 
         Height          =   315
         Left            =   1980
         MaxLength       =   6
         TabIndex        =   4
         Top             =   2010
         Width           =   1155
      End
      Begin VB.TextBox txtTransp 
         Height          =   315
         Left            =   1980
         MaxLength       =   14
         TabIndex        =   3
         Top             =   1560
         Width           =   2115
      End
      Begin VB.TextBox txtDest 
         Height          =   315
         Left            =   1980
         MaxLength       =   14
         TabIndex        =   2
         Top             =   1140
         Width           =   2115
      End
      Begin VB.TextBox txtContr 
         Height          =   315
         Left            =   1980
         MaxLength       =   14
         TabIndex        =   1
         Top             =   690
         Width           =   2115
      End
      Begin VB.Label lblVlQtde 
         AutoSize        =   -1  'True
         Height          =   195
         Left            =   2010
         TabIndex        =   38
         Top             =   360
         Width           =   45
      End
      Begin VB.Label lblQtde 
         Caption         =   "Qtde. de Registros"
         Height          =   255
         Left            =   150
         TabIndex        =   37
         Top             =   330
         Width           =   1725
      End
      Begin VB.Label lblvlICMS 
         Caption         =   "Vl. ICMS"
         Height          =   285
         Left            =   8400
         TabIndex        =   29
         Top             =   2910
         Width           =   765
      End
      Begin VB.Label lblPcICMS 
         Caption         =   "Pc. ICMS"
         Height          =   285
         Left            =   5850
         TabIndex        =   28
         Top             =   2940
         Width           =   915
      End
      Begin VB.Label lblCFOP 
         Caption         =   "CFOP"
         Height          =   285
         Left            =   5850
         TabIndex        =   27
         Top             =   2520
         Width           =   885
      End
      Begin VB.Label lblSubSerie 
         Caption         =   "Subserie CTRC"
         Height          =   285
         Left            =   5820
         TabIndex        =   26
         Top             =   2070
         Width           =   1185
      End
      Begin VB.Label lblBase 
         Caption         =   "Vl. Base"
         Height          =   285
         Left            =   3300
         TabIndex        =   25
         Top             =   2940
         Width           =   1035
      End
      Begin VB.Label lblMovimento 
         Caption         =   "Dt. Movimento"
         Height          =   285
         Left            =   3300
         TabIndex        =   24
         Top             =   2520
         Width           =   1125
      End
      Begin VB.Label lblSerie 
         Caption         =   "Serie CTRC"
         Height          =   285
         Left            =   3300
         TabIndex        =   23
         Top             =   2070
         Width           =   1035
      End
      Begin VB.Label lblFrete 
         Caption         =   "Vl. Frete"
         Height          =   285
         Left            =   150
         TabIndex        =   22
         Top             =   2940
         Width           =   1275
      End
      Begin VB.Label lblDtCTRC 
         Caption         =   "Dt. CTRC"
         Height          =   285
         Left            =   150
         TabIndex        =   21
         Top             =   2520
         Width           =   1275
      End
      Begin VB.Label lblNrCTRC 
         Caption         =   "Nr. CTRC"
         Height          =   285
         Left            =   150
         TabIndex        =   20
         Top             =   2070
         Width           =   1275
      End
      Begin VB.Label lblRazTransp 
         AutoSize        =   -1  'True
         Height          =   195
         Left            =   4290
         TabIndex        =   19
         Top             =   1650
         Width           =   45
      End
      Begin VB.Label lblRazDest 
         AutoSize        =   -1  'True
         Height          =   195
         Left            =   4290
         TabIndex        =   18
         Top             =   1200
         Width           =   45
      End
      Begin VB.Label lblRazContr 
         AutoSize        =   -1  'True
         Height          =   195
         Left            =   4290
         TabIndex        =   17
         Top             =   750
         Width           =   45
      End
      Begin VB.Label lblTransp 
         Caption         =   "CNPJ Tranportadora"
         Height          =   255
         Left            =   120
         TabIndex        =   16
         Top             =   1590
         Width           =   1635
      End
      Begin VB.Label lblDest 
         Caption         =   "CNPJ Destinat�rio "
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Top             =   1170
         Width           =   1635
      End
      Begin VB.Label lblContr 
         Caption         =   "CNPJ CD Contratante"
         Height          =   255
         Left            =   120
         TabIndex        =   14
         Top             =   750
         Width           =   1725
      End
   End
End
Attribute VB_Name = "frmCadastro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim vLoja As Integer
Dim vTransp As Integer
 Dim FLERRO As Integer

Private Sub cmdConsultar_Click()
          Dim vObj As Object
          Dim Itmx As Object, i As Integer
          
1         On Error GoTo Trata_erro
          
2         frmConsulta.lsvConsulta.ListItems.Clear
          
3         Conectar
          
4         vBanco.Parameters.Remove "PM_COD_LOJA"
5         vBanco.Parameters.Add "PM_COD_LOJA", IIf(vLoja = 0, Null, vLoja), 1

6         vBanco.Parameters.Remove "PM_COD_TRANSP"
7         vBanco.Parameters.Add "PM_COD_TRANSP", IIf(vTransp = 0, Null, vTransp), 1

8         vBanco.Parameters.Remove "PM_CNPJ_DEST"
9         vBanco.Parameters.Add "PM_CNPJ_DEST", IIf(Len(txtDest.Text) = 0, Null, Val(txtDest.Text)), 1
          
10        vBanco.Parameters.Remove "PM_NUM_CTRC"
11        vBanco.Parameters.Add "PM_NUM_CTRC", IIf(Val(txtNrCTRC.Text) = 0, Null, Val(txtNrCTRC.Text)), 1

12        vBanco.Parameters.Remove "PM_DT_CTRC"
13        vBanco.Parameters.Add "PM_DT_CTRC", IIf(Len(mskDtCTRC.Text) = 0, Null, mskDtCTRC.Text), 1

14        Criar_Cursor

15        vBanco.ExecuteSQL "Begin PRODSIC.PCK_CAD996.PR_CONSULTA_CTRC(:pm_cursor, " & _
                                                                      ":PM_COD_LOJA, " & _
                                                                      ":PM_COD_TRANSP, " & _
                                                                      ":PM_CNPJ_DEST, " & _
                                                                      ":PM_NUM_CTRC, " & _
                                                                      ":PM_DT_CTRC);END;"
16        Set vObj = vBanco.Parameters("pm_cursor").Value
          
          'Case a consulta retorne apenas 1 registro carrega os campos
          'Sen�o abre um listview para que o usuario escolha qual registro ele deseja
17        If vObj.RecordCount = 0 Then
18            MsgBox "A Consulta n�o retornou dados.", vbCritical, "Aten��o"
19        ElseIf vObj.RecordCount = 1 Then
                  
20            txtContr.Text = vObj!cgc
21            txtDest.Text = vObj!CNPJ_DESTINATARIO
22            txtTransp.Text = vObj!cgc_transp
23            txtNrCTRC.Text = vObj!NUM_CTRC
24            txtSerie.Text = vObj!serie_CTRC
25            mskDtCTRC.Text = vObj!dt_CTRC
26            mskMovimento.Text = vObj!DT_MOVIMENTO
27            txtCFOP.Text = vObj!COD_CFOP
28            txtFrete.Text = vObj!VL_FRETE
29            txtBase.Text = vObj!VL_BASE_CALCULO
30            txtPcICMS.Text = vObj!PC_ICMS
31            txtVlICMS.Text = vObj!VL_ICMS
32            txtSubSerie.Text = vObj!SUBSERIE_CTRC
33            Botoes True, False, True
34            txtContr_LostFocus
35            txtDest_LostFocus
36            txtTransp_LostFocus
              
37        ElseIf vObj.RecordCount > 1 Then
              
              'Carregando listview
38            For i = 1 To vObj.RecordCount
39                Set Itmx = frmConsulta.lsvConsulta.ListItems.Add()
40                    Itmx.Text = vObj!cgc
41                    Itmx.SubItems(1) = vObj!CNPJ_DESTINATARIO
42                    Itmx.SubItems(2) = vObj!cgc_transp
43                    Itmx.SubItems(3) = vObj!NUM_CTRC
44                    Itmx.SubItems(4) = vObj!serie_CTRC
45                    Itmx.SubItems(5) = vObj!SUBSERIE_CTRC
46                    Itmx.SubItems(6) = vObj!dt_CTRC
47                    Itmx.SubItems(7) = vObj!DT_MOVIMENTO
48                    Itmx.SubItems(8) = vObj!COD_CFOP
49                    Itmx.SubItems(9) = vObj!VL_FRETE
50                    Itmx.SubItems(10) = vObj!VL_BASE_CALCULO
51                    Itmx.SubItems(11) = vObj!PC_ICMS
52                    Itmx.SubItems(12) = vObj!VL_ICMS
53                vObj.MoveNext
54            Next
              
55            frmConsulta.Show
56            Botoes True, False, True

57        End If
          
58        Set vObj = Nothing

59        Desconectar
          
Trata_erro:
60        If Err.Number <> 0 Then
61            MsgBox "Sub: cmdConsultar_Click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
62        End If
          
End Sub

Private Sub cmdExcluir_Click()

      Dim vObj As Object, vMSG As String

1         On Error GoTo Trata_erro
          
2         If MsgBox("Confirma exclus�o do regisro?", vbYesNo) = vbNo Then
3             Exit Sub
4         End If
          
5         Conectar
           
6         vBanco.Parameters.Remove "pm_acao"
7         vBanco.Parameters.Add "pm_acao", 2, 1
           
8         vBanco.Parameters.Remove "pm_msg"
9         vBanco.Parameters.Add "pm_msg", "", 2

10        vBanco.Parameters.Remove "PM_COD_LOJA"
11        vBanco.Parameters.Add "PM_COD_LOJA", vLoja, 1

12        vBanco.Parameters.Remove "PM_COD_TRANSP"
13        vBanco.Parameters.Add "PM_COD_TRANSP", vTransp, 1

14        vBanco.Parameters.Remove "PM_CNPJ_DEST"
15        vBanco.Parameters.Add "PM_CNPJ_DEST", Val(txtDest.Text), 1
          
16        vBanco.Parameters.Remove "PM_NUM_CTRC"
17        vBanco.Parameters.Add "PM_NUM_CTRC", Val(txtNrCTRC.Text), 1

18        vBanco.Parameters.Remove "PM_SERIE"
19        vBanco.Parameters.Add "PM_SERIE", txtSerie.Text, 1

20        vBanco.Parameters.Remove "PM_DT_CTRC"
21        vBanco.Parameters.Add "PM_DT_CTRC", mskDtCTRC.Text, 1
          
22        vBanco.Parameters.Remove "PM_MOVIMENTO"
23        vBanco.Parameters.Add "PM_MOVIMENTO", "", 1

24        vBanco.Parameters.Remove "PM_COD_CFOP"
25        vBanco.Parameters.Add "PM_COD_CFOP", 0, 1

26        vBanco.Parameters.Remove "PM_VL_FRETE"
27        vBanco.Parameters.Add "PM_VL_FRETE", 0, 1

28        vBanco.Parameters.Remove "PM_VL_BASE"
29        vBanco.Parameters.Add "PM_VL_BASE", 0, 1
          
30        vBanco.Parameters.Remove "PM_PC_ICMS"
31        vBanco.Parameters.Add "PM_PC_ICMS", 0, 1

32        vBanco.Parameters.Remove "PM_VL_ICMS"
33        vBanco.Parameters.Add "PM_VL_ICMS", 0, 1

34        vBanco.Parameters.Remove "PM_SUBSERIE"
35        vBanco.Parameters.Add "PM_SUBSERIE", "", 1

36        vBanco.ExecuteSQL "Begin PRODSIC.PCK_CAD996.PR_CADASTRA_CTRC(:PM_ACAO, " & _
                                                                      ":PM_MSG, " & _
                                                                      ":PM_COD_LOJA, " & _
                                                                      ":PM_COD_TRANSP, " & _
                                                                      ":PM_CNPJ_DEST, " & _
                                                                      ":PM_NUM_CTRC, " & _
                                                                      ":PM_SERIE, " & _
                                                                      ":PM_DT_CTRC, " & _
                                                                      ":PM_MOVIMENTO, " & _
                                                                      ":PM_COD_CFOP, " & _
                                                                      ":PM_VL_FRETE, " & _
                                                                      ":PM_VL_BASE, " & _
                                                                      ":PM_PC_ICMS, " & _
                                                                      ":PM_VL_ICMS, " & _
                                                                      ":PM_SUBSERIE);END;"
          
37        vMSG = vBanco.Parameters("pm_msg").Value
          
38        MsgBox vMSG, vbInformation, "CAD996"
          
39        Desconectar
          
40        LimpaDados
          
41        Quantidade
          
Trata_erro:
42        If Err.Number <> 0 Then
43            MsgBox "Sub: cmdExcluir_Click" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl
44        End If

End Sub

Private Sub cmdImportar_Click()
1     On Error GoTo Trata_erro

          Dim xl As New Excel.Application
          'Dim xlB As Excel.Workbook
          Dim Fim, ErroC As Integer, i As Long, j As Long, X As Integer
          Dim Celula, strcaminho As String
          Dim mat_import(13)


2         CMMDIALOG.ShowOpen
          
3         strcaminho = CMMDIALOG.FileName
          
4         If InStr(1, strcaminho, ".xls") = 0 Then
5             MsgBox "O arquivo de importa��o deve ser em Excel", , "Aten��o!"
6             Exit Sub
7         End If
             
8         Screen.MousePointer = 11
          
          'Verifica se foi indicado o diret�rio e nome do arquivo a ser lido
9         If strcaminho = "" Then
10            MsgBox "Indique o arquivo a ser aberto"
11            Screen.MousePointer = 0
12            Exit Sub
13        End If
          
14        xl.Workbooks.Add strcaminho

15        If xl.Cells(1, 1).Value = "" Then

16            MsgBox "Primeira linha do Arquivo n�o poder estar em branco", vbInformation, "Aten��o"
17            Screen.MousePointer = 0
18            Exit Sub

19        ElseIf xl.Cells(2, 1).Value = "" Then

20            Fim = 1

21        Else

22            xl.Cells(1, 1).Select
23            xl.ActiveCell.End(xlDown).Select

24            Fim = xl.ActiveCell.Row

25        End If

26        xl.Cells(1, 1).Select

27        FLERRO = 0
          
28        frmImportacao.Show

29        frmImportacao.StatusBar1.Panels(1).Text = ""
          
30        Conectar
          
31        For i = 2 To Fim
32            For j = 1 To 13
33                ErroC = 0

34                Celula = xl.Cells(i, j).Value
35                Celula = Trim(UCase(Celula))

36                Select Case j
                      Case 1
37                        If Not IsNumeric(Celula) Then
38                            ErroC = 1
39                        End If
40                        If frmImportacao.RecuperaLoja(Celula) = 0 Then
41                            ErroC = 1
42                        End If
43                    Case 2
44                        If Not IsNumeric(Celula) Then
45                            ErroC = 1
46                        End If
47                        If frmImportacao.ValidaDest(Celula) = 0 Then
48                            ErroC = 1
49                        End If
50                    Case 3
51                        If Not IsNumeric(Celula) Then
52                            ErroC = 1
53                        End If
54                        If frmImportacao.RecuperaTransp(Celula) = 0 Then
55                            ErroC = 1
56                        End If
57                    Case 4
58                        If Not IsNumeric(Celula) Then
59                            ErroC = 1
60                        End If
61                    Case 5
62                        If Len(Celula) = 0 Then
63                            ErroC = 1
64                        End If
65                    Case 7, 8
66                        If Not IsDate(Celula) Then
67                            ErroC = 1
68                        End If
69                    Case 9
70                        If Not IsNumeric(Celula) Then
71                            ErroC = 1
72                        End If
73                    Case 10, 12
74                        If Len(Celula) > 0 Then
75                            If Not IsNumeric(Celula) Then
76                                ErroC = 1
77                            End If
78                        End If
79                    Case 11
80                        If Len(Celula) > 0 Then
81                            If Not IsNumeric(Celula) Then
82                                ErroC = 1
83                                If xl.Cells(i, 11).Value < xl.Cells(i, 10).Value Then
84                                    ErroC = 1
85                                End If
86                            End If
87                        End If
88                    Case 13
89                        If Len(Celula) > 0 Then
90                            If Not IsNumeric(Celula) Then
91                                ErroC = 1
92                                If Round(xl.Cells(i, 13).Value, 2) <> Round((xl.Cells(i, 11).Value * xl.Cells(i, 12).Value) / 100, 2) Then
93                                    ErroC = 1
94                                End If
95                            End If
96                        End If
97                End Select

98                If ErroC = 1 Then
99                    FLERRO = 1
100                   frmImportacao.StatusBar1.Panels(1).Text = "* Registros com problemas"

101                   With frmImportacao.mfgArq
102                       For X = 1 To 13
103                           .Row = i - 1
104                           .Col = j
105                           .CellBackColor = vbYellow
106                       Next
107                   End With
108               End If

109               With frmImportacao.mfgArq
110                   .Rows = i + 1
111                   .Row = i - 1
112                   .Col = j
113                   .Text = UCase(Celula)
114               End With
115               DoEvents
116           Next
117           DoEvents
118       Next

119       frmImportacao.mfgArq.Rows = i - 1

120       Set xl = Nothing
          
121       Desconectar
          
122       Screen.MousePointer = vbNormal
               
Trata_erro:
123       If Err.Number = -2147417851 Then
124           Resume Next
125       ElseIf Err.Number <> 0 Then
126           MsgBox "Sub: cmdImportar_Click" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl
127           Screen.MousePointer = vbNormal
128       End If
End Sub

Private Sub cmdSair_Click()
    End
End Sub

Private Sub cmdLimpar_Click()
    LimpaDados
End Sub

Private Sub cmdSalvar_Click()
    
    If ValidaCampos Then
        GravaDados
    End If
    
End Sub

Private Sub GravaDados()

      Dim vObj As Object, vMSG As String

1         On Error GoTo Trata_erro
          
2         Conectar
           
3         vBanco.Parameters.Remove "pm_acao"
4         vBanco.Parameters.Add "pm_acao", 1, 1
           
5         vBanco.Parameters.Remove "pm_msg"
6         vBanco.Parameters.Add "pm_msg", "", 2

7         vBanco.Parameters.Remove "PM_COD_LOJA"
8         vBanco.Parameters.Add "PM_COD_LOJA", vLoja, 1

9         vBanco.Parameters.Remove "PM_COD_TRANSP"
10        vBanco.Parameters.Add "PM_COD_TRANSP", vTransp, 1

11        vBanco.Parameters.Remove "PM_CNPJ_DEST"
12        vBanco.Parameters.Add "PM_CNPJ_DEST", Val(txtDest.Text), 1
          
13        vBanco.Parameters.Remove "PM_NUM_CTRC"
14        vBanco.Parameters.Add "PM_NUM_CTRC", Val(txtNrCTRC.Text), 1

15        vBanco.Parameters.Remove "PM_SERIE"
16        vBanco.Parameters.Add "PM_SERIE", txtSerie.Text, 1

17        vBanco.Parameters.Remove "PM_DT_CTRC"
18        vBanco.Parameters.Add "PM_DT_CTRC", mskDtCTRC.Text, 1
          
19        vBanco.Parameters.Remove "PM_MOVIMENTO"
20        vBanco.Parameters.Add "PM_MOVIMENTO", mskMovimento.Text, 1

21        vBanco.Parameters.Remove "PM_COD_CFOP"
22        vBanco.Parameters.Add "PM_COD_CFOP", Val(txtCFOP.Text), 1

23        vBanco.Parameters.Remove "PM_VL_FRETE"
24        vBanco.Parameters.Add "PM_VL_FRETE", Val(txtFrete.Text), 1

25        vBanco.Parameters.Remove "PM_VL_BASE"
26        vBanco.Parameters.Add "PM_VL_BASE", Val(txtBase.Text), 1
          
27        vBanco.Parameters.Remove "PM_PC_ICMS"
28        vBanco.Parameters.Add "PM_PC_ICMS", Val(txtPcICMS.Text), 1

29        vBanco.Parameters.Remove "PM_VL_ICMS"
30        vBanco.Parameters.Add "PM_VL_ICMS", Val(txtVlICMS.Text), 1

31        vBanco.Parameters.Remove "PM_SUBSERIE"
32        vBanco.Parameters.Add "PM_SUBSERIE", IIf(Len(txtSubSerie.Text) = 0, "01", txtSubSerie.Text), 1

33        vBanco.ExecuteSQL "Begin PRODSIC.PCK_CAD996.PR_CADASTRA_CTRC(:PM_ACAO, " & _
                                                                      ":PM_MSG, " & _
                                                                      ":PM_COD_LOJA, " & _
                                                                      ":PM_COD_TRANSP, " & _
                                                                      ":PM_CNPJ_DEST, " & _
                                                                      ":PM_NUM_CTRC, " & _
                                                                      ":PM_SERIE, " & _
                                                                      ":PM_DT_CTRC, " & _
                                                                      ":PM_MOVIMENTO, " & _
                                                                      ":PM_COD_CFOP, " & _
                                                                      ":PM_VL_FRETE, " & _
                                                                      ":PM_VL_BASE, " & _
                                                                      ":PM_PC_ICMS, " & _
                                                                      ":PM_VL_ICMS, " & _
                                                                      ":PM_SUBSERIE);END;"
          
34        vMSG = vBanco.Parameters("pm_msg").Value
          
35        MsgBox vMSG, vbInformation, "CAD996"
          
36        Desconectar

37        LimpaDados
          
38        Quantidade

Trata_erro:
39        If Err.Number <> 0 Then
40            MsgBox "Sub: GravaDados" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl
41        End If
          
End Sub
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
          
          'Passa para o pr�ximo campo apertando a tecla enter
1         If KeyCode = 13 Or KeyCode = 11 Then
2             SendKeys "{tab}"
3             KeyCode = 0
4         End If
          
End Sub

Private Sub Form_Load()

    Botoes False, True, False
    
    Quantidade
    
End Sub

 Sub mskDtCTRC_LostFocus()

      Dim vObj As Object, vData As String

1         On Error GoTo Trata_erro
        
          'Verifica se digitou alguma data,
          'se digitou carrega os dados do ctrc
2         If mskDtCTRC.Text = "" Then
3             Exit Sub
4         End If

5         vData = Mid(mskDtCTRC.Text, 1, 2) & "/" & Mid(mskDtCTRC.Text, 3, 2) & "/" & Mid(mskDtCTRC.Text, 5, 4)
6         If Not Datavalida(vData) Then
7             MsgBox "Dt. CTRC inv�lida.", vbCritical, "Aten��o"
8             mskDtCTRC.SetFocus
9             Exit Sub
10        End If

11        Conectar
           
12        vBanco.Parameters.Remove "PM_COD_LOJA"
13        vBanco.Parameters.Add "PM_COD_LOJA", vLoja, 1

14        vBanco.Parameters.Remove "PM_COD_TRANSP"
15        vBanco.Parameters.Add "PM_COD_TRANSP", vTransp, 1

16        vBanco.Parameters.Remove "PM_CNPJ_DEST"
17        vBanco.Parameters.Add "PM_CNPJ_DEST", Val(txtDest.Text), 1
          
18        vBanco.Parameters.Remove "PM_NUM_CTRC"
19        vBanco.Parameters.Add "PM_NUM_CTRC", Val(txtNrCTRC.Text), 1

20        vBanco.Parameters.Remove "PM_SERIE"
21        vBanco.Parameters.Add "PM_SERIE", txtSerie.Text, 1

22        vBanco.Parameters.Remove "PM_DT_CTRC"
23        vBanco.Parameters.Add "PM_DT_CTRC", mskDtCTRC.Text, 1

24        Criar_Cursor

25        vBanco.ExecuteSQL "Begin PRODSIC.PCK_CAD996.PR_VERIFICA_CTRC(:pm_cursor, " & _
                                                                      ":PM_COD_LOJA, " & _
                                                                      ":PM_COD_TRANSP, " & _
                                                                      ":PM_CNPJ_DEST, " & _
                                                                      ":PM_NUM_CTRC, " & _
                                                                      ":PM_SERIE, " & _
                                                                      ":PM_DT_CTRC);END;"
          
26        Set vObj = vBanco.Parameters("pm_cursor").Value

27        If Not vObj.BOF And Not vObj.EOF Then
              
              'se o usu�rio quiser alterar os dados, carrega os dados, sen�o limpa os dados
28            If MsgBox("Registro j� existe, deseja alter�-lo?", vbYesNo) = vbYes Then
                  
29                mskMovimento.Text = vObj!DT_MOVIMENTO
30                txtCFOP.Text = vObj!COD_CFOP
31                txtFrete.Text = vObj!VL_FRETE
32                txtBase.Text = vObj!VL_BASE_CALCULO
33                txtPcICMS.Text = vObj!PC_ICMS
34                txtVlICMS.Text = vObj!VL_ICMS
35                txtSubSerie.Text = vObj!SUBSERIE_CTRC
36                DisabilitaCampos False, False, False, False, False, False
37                Botoes True, False, True
                  
38            Else
              
39                LimpaDados
40                txtContr.SetFocus
41                Botoes False, True, False
                  
42            End If
              
43        Else
          
44            Botoes True, True, False
              
45        End If

46        Set vObj = Nothing
          
47        Desconectar

Trata_erro:
48        If Err.Number <> 0 Then
49            MsgBox "Sub: mskDtCTRC_LostFocus" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl
50        End If

End Sub

Private Sub mskMovimento_Validate(Cancel As Boolean)
    Dim vData As String
    
    vData = Mid(mskMovimento.Text, 1, 2) & "/" & Mid(mskMovimento.Text, 3, 2) & "/" & Mid(mskMovimento.Text, 5, 4)
    If Len(mskMovimento.Text) > 0 And Not Datavalida(vData) Then
        MsgBox "Dt. Movimento inv�lida.", vbCritical, "Aten��o"
        Cancel = True
    End If
    
End Sub

Private Sub txtBase_Validate(Cancel As Boolean)

    If Not Val(txtBase.Text) <= Val(txtFrete.Text) Then
        MsgBox "O campo Vl. Base n�o pode ser maior que o campo Vl. Frete.", vbCritical, "Aten��o"
        Cancel = True
    End If
    
End Sub

Private Sub txtCFOP_Validate(Cancel As Boolean)

    If Val(txtCFOP.Text) <> 1 And Val(txtCFOP.Text) <> 2 Then
        MsgBox "O campo CFOP deve ser preenchido com 1 ou 2.", vbCritical, "Aten��o"
        Cancel = True
    End If
    
End Sub

Private Sub txtContr_KeyPress(KeyAscii As Integer)
    KeyAscii = Valor(KeyAscii, txtContr)
End Sub

Sub txtContr_LostFocus()

      Dim vObj As Object

1         On Error GoTo Trata_erro
          
          'Verifica se digitou algum cnpj de contratante,
          'se digitou carrega os dados do contratante
2         If txtContr.Text = "" Then
3             Exit Sub
4         End If
          
5         Conectar
           
6         vBanco.Parameters.Remove "PM_CNPJ"
7         vBanco.Parameters.Add "PM_CNPJ", txtContr.Text, 1

8         Criar_Cursor

9         vBanco.ExecuteSQL "Begin PRODSIC.PCK_CAD996.PR_CNPJ_CONTRATANTE(:pm_cursor,:PM_CNPJ);END;"
          
10        Set vObj = vBanco.Parameters("pm_cursor").Value

11        If Not vObj.BOF And Not vObj.EOF Then
12            vLoja = vObj!cod_loja
13            lblRazContr = vObj!NOME_FANTASIA & " - " & vObj!COD_UF
14        Else
15            vLoja = 0
16            lblRazContr = ""
17            txtContr.SetFocus
18            MsgBox "CNPJ do CD n�o localizado", vbCritical, "Aten��o"
19        End If

20        Set vObj = Nothing
          
21        Desconectar

Trata_erro:
22        If Err.Number <> 0 Then
23            MsgBox "Sub: txtContr_LostFocus" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl
24        End If
          
End Sub

Private Sub txtDest_KeyPress(KeyAscii As Integer)
    KeyAscii = Valor(KeyAscii, txtDest)
End Sub

Sub txtDest_LostFocus()

      Dim vObj As Object

1         On Error GoTo Trata_erro
          
          'Verifica se digitou algum cnpj de destinat�rio,
          'se digitou carrega os dados do destinat�rio
2         If txtDest.Text = "" Then
3             Exit Sub
4         End If
          
5         Conectar
           
6         vBanco.Parameters.Remove "PM_CNPJ"
7         vBanco.Parameters.Add "PM_CNPJ", txtDest.Text, 1

8         Criar_Cursor

9         vBanco.ExecuteSQL "Begin PRODSIC.PCK_CAD996.PR_CNPJ_DESTINATARIO(:pm_cursor,:PM_CNPJ);END;"
          
10        Set vObj = vBanco.Parameters("pm_cursor").Value

11        If Not vObj.BOF And Not vObj.EOF Then
12            lblRazDest = vObj!razao
13        Else
14            lblRazDest = ""
15            txtDest.SetFocus
16            MsgBox "CNPJ cliente/fornecedor n�o localizado", vbCritical, "Aten��o"
17        End If

18        Set vObj = Nothing
          
19        Desconectar

Trata_erro:
20        If Err.Number <> 0 Then
21            MsgBox "Sub: txtDest_LostFocus" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
22        End If

End Sub

Private Sub txtFrete_Validate(Cancel As Boolean)

    If Val(txtFrete.Text) = 0 Then
        MsgBox "O campo Vl. Frete n�o pode ser 0.", vbCritical, "Aten��o"
        Cancel = True
    End If
    
End Sub

Private Sub txtTransp_KeyPress(KeyAscii As Integer)
    KeyAscii = Valor(KeyAscii, txtTransp)
End Sub

Private Sub txtNrCTRC_KeyPress(KeyAscii As Integer)
    KeyAscii = Numerico(KeyAscii)
End Sub

Private Sub txtCFOP_KeyPress(KeyAscii As Integer)
    KeyAscii = Valor(KeyAscii, txtCFOP)
End Sub

Private Sub txtFrete_KeyPress(KeyAscii As Integer)
    KeyAscii = Valor(KeyAscii, txtBase)
End Sub

Private Sub txtBase_KeyPress(KeyAscii As Integer)
    KeyAscii = Valor(KeyAscii, txtBase)
End Sub

Private Sub txtPcICMS_KeyPress(KeyAscii As Integer)
    KeyAscii = Valor(KeyAscii, txtPcICMS)
End Sub

Private Sub LimpaDados()
    
    txtContr.Text = ""
    lblRazContr = ""
    txtDest.Text = ""
    lblRazDest = ""
    txtTransp.Text = ""
    lblRazTransp = ""
    txtNrCTRC.Text = ""
    txtSerie.Text = ""
    txtSubSerie.Text = ""
    mskDtCTRC.Text = ""
    mskMovimento.Text = ""
    txtCFOP.Text = ""
    txtFrete.Text = ""
    txtBase.Text = ""
    txtPcICMS.Text = ""
    txtVlICMS.Text = ""
    vLoja = 0
    vTransp = 0
    DisabilitaCampos True, True, True, True, True, True
    txtContr.SetFocus
    Botoes False, True, False
    
End Sub

Private Sub DisabilitaCampos(ByVal vC1 As Boolean, ByVal vC2 As Boolean, ByVal vC3 As Boolean, ByVal vC4 As Boolean, ByVal vC5 As Boolean, ByVal vC6 As Boolean)
    
    txtContr.Enabled = vC1
    txtDest.Enabled = vC2
    txtTransp.Enabled = vC3
    txtNrCTRC.Enabled = vC4
    txtSerie.Enabled = vC5
    mskDtCTRC.Enabled = vC6
    
End Sub

Private Sub Botoes(ByVal vB1 As Boolean, ByVal vB2 As Boolean, ByVal vB3 As Boolean)
    
    cmdSalvar.Enabled = vB1
    cmdConsultar.Enabled = vB2
    cmdExcluir.Enabled = vB3
    
End Sub

Sub txtTransp_LostFocus()

      Dim vObj As Object

1         On Error GoTo Trata_erro
          
          'Verifica se digitou algum cnpj de transportadora,
          'se digitou carrega os dados da transportadora
2         If txtTransp.Text = "" Then
3             Exit Sub
4         End If
          
5         Conectar
           
6         vBanco.Parameters.Remove "PM_CNPJ"
7         vBanco.Parameters.Add "PM_CNPJ", txtTransp.Text, 1

8         Criar_Cursor

9         vBanco.ExecuteSQL "Begin PRODSIC.PCK_CAD996.PR_CNPJ_TRANSPORTADORA(:pm_cursor,:PM_CNPJ);END;"
          
10        Set vObj = vBanco.Parameters("pm_cursor").Value

11        If Not vObj.BOF And Not vObj.EOF Then
12            vTransp = vObj!COD_TRANSP
13            lblRazTransp = vObj!razao
14        Else
15            vTransp = 0
16            lblRazTransp = ""
17            txtTransp.SetFocus
18            MsgBox "CNPJ da transportadora n�o localizado", vbCritical, "Aten��o"
19        End If

20        Set vObj = Nothing
          
21        Desconectar

Trata_erro:
22        If Err.Number <> 0 Then
23            MsgBox "Sub: txtTransp_LostFocus" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
24        End If

End Sub

Private Sub txtVlICMS_KeyPress(KeyAscii As Integer)
    KeyAscii = Valor(KeyAscii, txtVlICMS)
End Sub

Function ValidaCampos() As Boolean
          
1         ValidaCampos = True
          
          'Realiza valida��o dos campos digitados pelo usu�rio
2         If txtContr.Text = "" Then
3             MsgBox "Preencha o campo CNPJ CD Contratante corretamente.", vbCritical, "Aten��o"
4             ValidaCampos = False
5             txtContr.SetFocus
6             Exit Function
7         End If
8         If txtDest.Text = "" Then
9             MsgBox "Preencha o campo CNPJ Destinat�rio corretamente.", vbCritical, "Aten��o"
10            ValidaCampos = False
11            txtDest.SetFocus
12            Exit Function
13        End If
14        If txtTransp.Text = "" Then
15            MsgBox "Preencha o campo CNPJ Tranportadora corretamente.", vbCritical, "Aten��o"
16            ValidaCampos = False
17            txtTransp.SetFocus
18            Exit Function
19        End If
20        If txtNrCTRC.Text = "" Then
21            MsgBox "Preencha o campo Nr. CTRC corretamente.", vbCritical, "Aten��o"
22            ValidaCampos = False
23            txtNrCTRC.SetFocus
24            Exit Function
25        End If
26        If txtNrCTRC.Text = "" Then
27            MsgBox "O campo Nr. CTRC n�o pode ser 0.", vbCritical, "Aten��o"
28            ValidaCampos = False
29            txtNrCTRC.SetFocus
30            Exit Function
31        End If
32        If txtSerie.Text = "" Then
33            MsgBox "Preencha o campo Serie CTRC corretamente.", vbCritical, "Aten��o"
34            ValidaCampos = False
35            txtSerie.SetFocus
36            Exit Function
37        End If
38        If mskDtCTRC.Text = "" Or Len(mskDtCTRC.Text) < 8 Then
39            MsgBox "Preencha o campo Dt. CTRC corretamente.", vbCritical, "Aten��o"
40            ValidaCampos = False
41            mskDtCTRC.SetFocus
42            Exit Function
43        End If
44        If mskMovimento.Text = "" Or Len(mskMovimento.Text) < 8 Then
45            MsgBox "Preencha o campo Dt. Movimento corretamente.", vbCritical, "Aten��o"
46            ValidaCampos = False
47            mskMovimento.SetFocus
48            Exit Function
49        End If
50        If Val(txtCFOP.Text) <> 1 And Val(txtCFOP.Text) <> 2 Then
51            MsgBox "O campo CFOP deve ser preenchido com 1 ou 2.", vbCritical, "Aten��o"
52            ValidaCampos = False
53            txtCFOP.SetFocus
54            Exit Function
55        End If
56        If txtFrete.Text = "" Then
57            MsgBox "Preencha o campo Vl. Frete corretamente.", vbCritical, "Aten��o"
58            ValidaCampos = False
59            txtFrete.SetFocus
60            Exit Function
61        End If
62        If Val(txtFrete.Text) = 0 Then
63            MsgBox "O campo Vl. Frete n�o pode ser 0.", vbCritical, "Aten��o"
64            ValidaCampos = False
65            txtFrete.SetFocus
66            Exit Function
67        End If
68        If txtBase.Text = "" Then
69            MsgBox "Preencha o campo Vl. Base corretamente.", vbCritical, "Aten��o"
70            ValidaCampos = False
71            txtBase.SetFocus
72            Exit Function
73        End If
74        If Not Val(txtBase.Text) <= Val(txtFrete.Text) Then
75            MsgBox "O campo Vl. Base n�o pode ser maior que o campo Vl. Frete.", vbCritical, "Aten��o"
76            ValidaCampos = False
77            txtBase.SetFocus
78            Exit Function
79        End If
80        If txtPcICMS.Text = "" Then
81            MsgBox "Preencha o campo Pc. ICMS corretamente.", vbCritical, "Aten��o"
82            ValidaCampos = False
83            txtPcICMS.SetFocus
84            Exit Function
85        End If
86        If txtVlICMS.Text = "" Then
87            MsgBox "Preencha o campo Vl. ICMS corretamente.", vbCritical, "Aten��o"
88            ValidaCampos = False
89            txtVlICMS.SetFocus
90            Exit Function
91        End If
92        If Not Val(txtVlICMS.Text) = Round((Val(txtBase.Text) * Val(txtPcICMS.Text)) / 100, 2) Then
93            MsgBox "Vl. ICMS incorreto.", vbCritical, "Aten��o"
94            ValidaCampos = False
95            txtVlICMS.SetFocus
96            Exit Function
97        End If
          
End Function
