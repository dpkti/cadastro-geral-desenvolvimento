Attribute VB_Name = "mdCAD996"
Option Explicit

'Variaveis de Cursor
Global Const ORATYPE_CURSOR = 102
Global Const ORADYN_NO_BLANKSTRIP = &H2&
Global Const ORAPARM_INPUT = 1              'CONSTANTE DE BIND INPUT
Global Const ORAPARM_OUTPUT = 2             'CONSTANTE DE BIND OUTPUT
Global Const ORAPARM_BOTH = 3               'CONSTANTE DE BIND INPUT/OUTPUT
Global Const ORATYPE_NUMBER = 2

Public vBanco As Object
Public vSessao As Object

Sub Conectar(Optional pForm As Form)

1         On Error GoTo Trata_erro
          
2         If Not pForm Is Nothing Then
3             pForm.STB.Panels(1).Text = "Conectando... Aguarde..."
4         End If
              
5         Set vSessao = CreateObject("oracleinprocserver.xorasession")
6         Set vBanco = vSessao.OpenDatabase("BATCH", "CAD996/dpkf8Hg", 0&)
          
7         If Not pForm Is Nothing Then
8             pForm.STB.Panels(1).Text = "Pronto"
9         End If

Trata_erro:
10        If Err.Number <> 0 Then
11            MsgBox "Sub: Conectar" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
12        End If
          
End Sub

Sub Desconectar(Optional pForm As Form)

1         On Error GoTo Trata_erro

2         If Not pForm Is Nothing Then
3             pForm.STB.Panels(1).Text = "Desconectando... Aguarde..."
4         End If

          Dim vIndice

5         If vBanco Is Nothing Then Conectar

6         For vIndice = vBanco.Parameters.Count To 0 Step -1

7             vBanco.Parameters.Remove (vIndice)

8         Next
          
9         Set vSessao = Nothing
10        Set vBanco = Nothing

Trata_erro:
11        If Err.Number <> 0 Then
12            MsgBox "Sub: Desconectar" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
13        End If
          
End Sub

Sub Criar_Cursor()
          
1         vBanco.Parameters.Remove "PM_Cursor"
2         vBanco.Parameters.Add "PM_Cursor", 0, 2
3         vBanco.Parameters("PM_Cursor").serverType = ORATYPE_CURSOR
4         vBanco.Parameters("PM_Cursor").DynasetOption = ORADYN_NO_BLANKSTRIP
5         vBanco.Parameters("PM_Cursor").DynasetCacheParams 256, 16, 20, 20000, 0

End Sub

Function Valor(ByVal KeyAscii As Integer, strCampo As String) As Integer
          
          'Fun��o para validar n�mero
1         If KeyAscii = 8 Then    'BACKSPACE
2             Valor = KeyAscii
3             Exit Function
4         End If
5         If Chr$(KeyAscii) = "," Then
6             KeyAscii = Asc(".")
7         End If
8         If Chr$(KeyAscii) = "," Or Chr$(KeyAscii) = "." Then
9             If InStr(strCampo, ",") > 0 Or InStr(strCampo, ".") > 0 Then
10                KeyAscii = 0
11                Beep
12            End If
13        Else
14            If Chr$(KeyAscii) < "0" Or Chr$(KeyAscii) > "9" Then
15                KeyAscii = 0
16                Beep
17            End If
18        End If
          
19        Valor = KeyAscii
End Function

Function Numerico(ByVal KeyAscii As Integer) As Integer
1         If KeyAscii = 8 Then    'BACKSPACE
2             Numerico = KeyAscii
3             Exit Function
4         End If
5         If Chr(KeyAscii) < "0" Or Chr(KeyAscii) > "9" Then
6             KeyAscii = 0
7             Beep
8         End If
9         Numerico = KeyAscii
End Function
Public Function Datavalida(arg1 As String) As Boolean
          ''--- Valida data no formato dd/mm/aaaa ---
          Dim i1 As Integer, s1 As String
          Dim i2 As Integer, s2 As String
          
1         Datavalida = False
          
2         If Len(arg1) < 5 Or Len(arg1) > 10 Then Exit Function
          
3         If Not IsDate(arg1) Then Exit Function
          
4         i1 = InStr(1, arg1, "/")
          
5         If i1 < 2 Or i1 > 3 Then Exit Function
          
6         s1 = Left(arg1, i1 - 1)
          
7         If s1 < 1 Or s1 > 31 Then Exit Function
          
8         i2 = InStr(i1 + 1, arg1, "/")
          
9         If i2 < 4 Or i2 > 6 Then Exit Function
          
10        s2 = Mid(arg1, i1 + 1, i2 - i1 - 1)
          
11        If s2 < 1 Or s2 > 12 Then Exit Function
          
12        Datavalida = True
          
End Function

Sub Quantidade()

      Dim vObj As Object

1     On Error GoTo Trata_erro

2         Conectar
          
3         Criar_Cursor

4         vBanco.ExecuteSQL "Begin PRODSIC.PCK_CAD996.PR_QTDE_CTRC(:pm_cursor);END;"
          
5         Set vObj = vBanco.Parameters("pm_cursor").Value

6         If Not vObj.BOF And Not vObj.EOF Then

7             frmCadastro.lblVlQtde = vObj!QTDE

8         End If

9         Set vObj = Nothing
          
10        Desconectar
          
Trata_erro:
11        If Err.Number <> 0 Then
12            MsgBox "Sub: Quantidade" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
13        End If
End Sub

