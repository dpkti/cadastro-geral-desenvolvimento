VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmConsulta 
   Caption         =   "Consulta"
   ClientHeight    =   4290
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5835
   LinkTopic       =   "Form1"
   ScaleHeight     =   4290
   ScaleWidth      =   5835
   StartUpPosition =   3  'Windows Default
   Begin MSComctlLib.ListView lsvConsulta 
      Height          =   4275
      Left            =   30
      TabIndex        =   0
      Top             =   0
      Width           =   5775
      _ExtentX        =   10186
      _ExtentY        =   7541
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   13
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "CNPJ CD Contratante"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "CNPJ Destinatário"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "CNPJ Transportadora"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Nr. CTRC"
         Object.Width           =   1642
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "Serie CTRC"
         Object.Width           =   1977
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Text            =   "SubSerie CTRC"
         Object.Width           =   2365
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Text            =   "Dt. CTRC"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   7
         Text            =   "Dt. Movimento"
         Object.Width           =   2365
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   8
         Text            =   "CFOP"
         Object.Width           =   1235
      EndProperty
      BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   9
         Text            =   "Vl. Frete"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   10
         Text            =   "Vl. Base"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   11
         Text            =   "Pc. ICMS"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   12
         Text            =   "Vl. ICMS"
         Object.Width           =   0
      EndProperty
   End
End
Attribute VB_Name = "frmConsulta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub lsvConsulta_DblClick()

    frmCadastro.txtContr.Text = lsvConsulta.SelectedItem.Text
    frmCadastro.txtDest.Text = lsvConsulta.SelectedItem.SubItems(1)
    frmCadastro.txtTransp.Text = lsvConsulta.SelectedItem.SubItems(2)
    frmCadastro.txtNrCTRC.Text = lsvConsulta.SelectedItem.SubItems(3)
    frmCadastro.txtSerie.Text = lsvConsulta.SelectedItem.SubItems(4)
    frmCadastro.mskDtCTRC.Text = lsvConsulta.SelectedItem.SubItems(6)
    frmCadastro.mskMovimento.Text = lsvConsulta.SelectedItem.SubItems(7)
    frmCadastro.txtCFOP.Text = lsvConsulta.SelectedItem.SubItems(8)
    frmCadastro.txtFrete.Text = lsvConsulta.SelectedItem.SubItems(9)
    frmCadastro.txtBase.Text = lsvConsulta.SelectedItem.SubItems(10)
    frmCadastro.txtPcICMS.Text = lsvConsulta.SelectedItem.SubItems(11)
    frmCadastro.txtVlICMS.Text = lsvConsulta.SelectedItem.SubItems(12)
    frmCadastro.txtSubSerie.Text = lsvConsulta.SelectedItem.SubItems(5)
    frmCadastro.txtContr_LostFocus
    frmCadastro.txtDest_LostFocus
    frmCadastro.txtTransp_LostFocus
    
    Unload Me
    
End Sub
