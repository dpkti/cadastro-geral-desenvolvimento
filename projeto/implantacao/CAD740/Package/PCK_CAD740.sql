create or replace package PRODUCAO.PCK_CAD740 is

  TYPE TP_CURSOR IS REF CURSOR;

  Procedure PR_VER_COD (cDados IN OUT TP_CURSOR,
                        cForn IN item_cadastro.cod_fornecedor%type);
                        
  Procedure PR_CON_CLIE (cDados IN OUT TP_CURSOR,
                         cForn IN vdr.controle_vdr.cod_fornecedor%type);
  
  Procedure PR_CON_GR (cDados IN OUT TP_CURSOR,
                       cForn IN vdr.controle_vdr.cod_fornecedor%type);   
                       
  Procedure PR_NOME (cDados IN OUT TP_CURSOR,
                     cForn IN fornecedor.cod_fornecedor%type);   

  Procedure PR_IMPRIMIR (cDados IN OUT TP_CURSOR,
                         cForn IN vdr.controle_vdr.cod_fornecedor%type);
                         
  Procedure PR_CLIENTE (cDados IN OUT TP_CURSOR);
  
  Procedure PR_LINHA (cDados IN OUT TP_CURSOR);    
  
  Procedure PR_GRUPO (cDados IN OUT TP_CURSOR,
                      cForn IN item_cadastro.cod_fornecedor%type);   
                      
  Procedure PR_SUBGRUPO (cDados IN OUT TP_CURSOR,
                         cGrupo IN grupo.cod_grupo%type);
                         
  Procedure PR_SUBGRUPO_2 (cDados IN OUT TP_CURSOR,
                           cGrupo IN grupo.cod_grupo%type);
                           
  Procedure PR_INC (cForn IN VDR.CONTROLE_VDR.COD_FORNECEDOR%type,
                    cGrupo IN VDR.CONTROLE_VDR.COD_GRUPO%type,
                    cSubgrupo IN VDR.CONTROLE_VDR.COD_SUBGRUPO%type,
                    cCliente IN VDR.CONTROLE_VDR.COD_TIPO_CLIENTE%type,
                    cVdr IN VDR.CONTROLE_VDR.COD_VDR%type,
                    cProduto IN VDR.CONTROLE_VDR.LINHA_PRODUTO%type);
                     
end PCK_CAD740;
/
create or replace package body PRODUCAO.PCK_CAD740 is

  -- 1) Verifica c�d.
  Procedure PR_VER_COD (cDados IN OUT TP_CURSOR,
                        cForn IN item_cadastro.cod_fornecedor%type) is
  Begin
       OPEN cDados FOR
       Select cod_fornecedor 
       from   item_cadastro 
       where  cod_fornecedor = cForn;
  End;
  
  -- 2) por Tipo Cliente
  Procedure PR_CON_CLIE (cDados IN OUT TP_CURSOR,
                         cForn IN vdr.controle_vdr.cod_fornecedor%type) is
  Begin
       OPEN cDados FOR
       Select cod_fornecedor,cod_grupo,cod_subgrupo, 
              cod_tipo_cliente,cod_vdr,linha_produto 
       from   vdr.controle_vdr 
       Where  cod_fornecedor = cForn
       Order By Cod_Tipo_Cliente, Cod_Grupo, Cod_Subgrupo;        
  End;
  
  -- 3) por Grupo
  Procedure PR_CON_GR (cDados IN OUT TP_CURSOR,
                       cForn IN vdr.controle_vdr.cod_fornecedor%type) is
  Begin
       OPEN cDados FOR
       Select cod_fornecedor,cod_grupo,cod_subgrupo, 
              cod_tipo_cliente,cod_vdr,linha_produto 
       from   vdr.controle_vdr 
       Where  cod_fornecedor = cForn
       Order By Cod_Grupo, Cod_Subgrupo;     
  End;
  
  -- 4) Consulta Nome
  Procedure PR_NOME (cDados IN OUT TP_CURSOR,
                     cForn IN fornecedor.cod_fornecedor%type) is
  Begin
       OPEN cDados FOR
       Select nome_fornec 
       from   fornecedor
       where  cod_fornecedor = cForn;
  End;
  
  -- 5) Imprimir
  Procedure PR_IMPRIMIR (cDados IN OUT TP_CURSOR,
                         cForn IN vdr.controle_vdr.cod_fornecedor%type) is
  Begin
       OPEN cDados FOR
       Select distinct a.cod_fornecedor, decode(a.cod_grupo,null,0,a.cod_grupo) grupo , 
              decode(a.cod_grupo,null,'',b.desc_grupo) descricao, 
              decode(a.cod_subgrupo,null,0,a.cod_subgrupo) subgrupo, 
              decode(a.cod_subgrupo,null,'',c.desc_subgrupo) descsub, 
              a.cod_tipo_cliente, d.desc_tipo_cli, 
              a.cod_vdr, a.linha_produto, e.desc_linha 
       from   vdr.controle_vdr a, grupo b, subgrupo c, tipo_cliente d, linha_produto e
       where  a.cod_fornecedor = cForn and 
              a.cod_grupo = b.cod_grupo(+) and
              a.cod_subgrupo = c.cod_subgrupo(+) and 
              a.cod_grupo = c.cod_grupo(+) and
              a.cod_tipo_cliente = d.cod_tipo_cli and
              a.linha_produto = e.linha_produto
       order by decode(a.cod_grupo,null,0,a.cod_grupo), 
                decode(a.cod_subgrupo,null,0,a.cod_subgrupo);
  End;
  
  -- 6) Combo Cliente
  Procedure PR_CLIENTE (cDados IN OUT TP_CURSOR) is
  Begin
       OPEN cDados FOR
       Select cod_tipo_cli,desc_tipo_cli
       from   tipo_cliente
       order by cod_tipo_cli;
  End;
  
  -- 7) Combo Linha Produto
  Procedure PR_LINHA (cDados IN OUT TP_CURSOR) is
  Begin
       OPEN cDados FOR
       Select linha_produto,desc_linha 
       from   linha_produto 
       order by linha_produto;
  End;
  
  -- 8) Combo Grupos
  Procedure PR_GRUPO (cDados IN OUT TP_CURSOR,
                      cForn IN item_cadastro.cod_fornecedor%type) is
  Begin
       OPEN cDados FOR
       Select distinct a.cod_grupo,a.desc_grupo
       from   grupo a, item_cadastro b 
       where  b.cod_fornecedor = cForn and
              b.cod_grupo = a.cod_grupo 
       order by a.cod_grupo;
  End;
  
  -- 9) grid SubGrupos
  Procedure PR_SUBGRUPO (cDados IN OUT TP_CURSOR,
                         cGrupo IN grupo.cod_grupo%type) is
  Begin
       OPEN cDados FOR
       Select count(*) QtdReg 
       From   subgrupo a, grupo b 
       Where  b.cod_grupo = cGrupo and
              b.cod_grupo = a.cod_grupo 
       Order by a.cod_subgrupo;
  End;
  
  -- 10) grid SubGrupos
  Procedure PR_SUBGRUPO_2 (cDados IN OUT TP_CURSOR,
                           cGrupo IN grupo.cod_grupo%type) is
  Begin
       OPEN cDados FOR
       Select distinct a.cod_subgrupo,a.desc_subgrupo 
       from   subgrupo a,grupo b 
       where  b.cod_grupo = cGrupo and
              b.cod_grupo = a.cod_grupo
       order by a.cod_subgrupo;
  End;
  
  -- 11) Cadastro
  Procedure PR_INC (cForn IN VDR.CONTROLE_VDR.COD_FORNECEDOR%type,
                    cGrupo IN VDR.CONTROLE_VDR.COD_GRUPO%type,
                    cSubgrupo IN VDR.CONTROLE_VDR.COD_SUBGRUPO%type,
                    cCliente IN VDR.CONTROLE_VDR.COD_TIPO_CLIENTE%type,
                    cVdr IN VDR.CONTROLE_VDR.COD_VDR%type,
                    cProduto IN VDR.CONTROLE_VDR.LINHA_PRODUTO%type) is
  Begin
       Update VDR.CONTROLE_VDR 
       Set Cod_Vdr = cVdr, 
           Linha_Produto = cProduto 
       Where Cod_Fornecedor = cForn and
             Cod_Grupo = cGrupo and
             Cod_Subgrupo = cSubgrupo and
             Cod_Tipo_Cliente = cCliente; 
       IF SQL%NOTFOUND THEN 
             Insert into VDR.CONTROLE_VDR 
                    (cod_fornecedor, cod_grupo, cod_subgrupo, 
                     cod_tipo_cliente, cod_vdr, linha_produto) 
             Values (cForn, cGrupo, cSubgrupo, 
                     cCliente, cVdr, cProduto); 
       END IF;
       
       Commit;

       Exception
          When Others then
          RollBack;    
  End;     
      
end PCK_CAD740;
 
/
