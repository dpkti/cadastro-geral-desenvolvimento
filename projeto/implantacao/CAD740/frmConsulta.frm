VERSION 5.00
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmConsulta 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Consulta de Controle VDR"
   ClientHeight    =   7110
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6795
   Icon            =   "frmConsulta.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   474
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   453
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame_Consulta 
      Caption         =   "Por ordem de"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   780
      Left            =   90
      TabIndex        =   10
      Top             =   1800
      Width           =   6630
      Begin VB.OptionButton opt_Cliente 
         Caption         =   "Tipo Cliente"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1980
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   360
         Width           =   1680
      End
      Begin VB.OptionButton opt_Grupo 
         Caption         =   "Grupo/Subgrupo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   4095
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   360
         Width           =   1860
      End
   End
   Begin VB.Frame FrameDados 
      Caption         =   "Dados"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   750
      Left            =   90
      TabIndex        =   6
      Top             =   945
      Width           =   6630
      Begin MSMask.MaskEdBox txtCod 
         Height          =   330
         Left            =   1935
         TabIndex        =   8
         Top             =   270
         Width           =   600
         _ExtentX        =   1058
         _ExtentY        =   582
         _Version        =   393216
         MaxLength       =   3
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         PromptChar      =   "�"
      End
      Begin MSMask.MaskEdBox txtNome 
         Height          =   330
         Left            =   2655
         TabIndex        =   9
         Top             =   270
         Width           =   3840
         _ExtentX        =   6773
         _ExtentY        =   582
         _Version        =   393216
         BackColor       =   -2147483648
         Enabled         =   0   'False
         MaxLength       =   30
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         PromptChar      =   "�"
      End
      Begin VB.Label lblcod 
         Appearance      =   0  'Flat
         Caption         =   "C�d. Fornecedor:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   405
         TabIndex        =   7
         Top             =   315
         Width           =   1530
      End
   End
   Begin VB.Frame Frame_Controle 
      Caption         =   "Controle VDR"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   4020
      Left            =   90
      TabIndex        =   13
      Top             =   2700
      Width           =   6630
      Begin MSFlexGridLib.MSFlexGrid mfgControle 
         Height          =   3615
         Left            =   135
         TabIndex        =   14
         Top             =   270
         Width           =   6330
         _ExtentX        =   11165
         _ExtentY        =   6376
         _Version        =   393216
         FixedCols       =   0
         BackColorBkg    =   -2147483633
         AllowBigSelection=   0   'False
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   6780
      Width           =   6795
      _ExtentX        =   11986
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   11933
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   810
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsulta.frx":23D2
      PICN            =   "frmConsulta.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdConsultar 
      Height          =   690
      Left            =   1575
      TabIndex        =   3
      TabStop         =   0   'False
      ToolTipText     =   "Consultar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsulta.frx":30C8
      PICN            =   "frmConsulta.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdLimpar 
      Height          =   690
      Left            =   810
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Limpar campos"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsulta.frx":3DBE
      PICN            =   "frmConsulta.frx":3DDA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdImprimir 
      Height          =   690
      Left            =   2340
      TabIndex        =   4
      TabStop         =   0   'False
      ToolTipText     =   "Imprimir"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsulta.frx":4AB4
      PICN            =   "frmConsulta.frx":4AD0
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmConsulta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : frmConsulta
' Author    : C.SAMUEL.OLIVEIRA
' Date      : 06/10/16
' Purpose   : TI-5073
'---------------------------------------------------------------------------------------


Private Sub cmdConsultar_Click()

    'verifica campos
    If Trim(txtCod) = "" Then
        Call vVB_Generica_001.Informar("Digite o C�digo.")
        txtCod.SetFocus
        Exit Sub
    End If
    
    If opt_Cliente = False And opt_Grupo = False Then
        Call vVB_Generica_001.Informar("Selecione a ordem em que a consulta deve aparecer.")
        opt_Cliente.TabStop = False
        opt_Grupo.TabStop = False
        Exit Sub
    End If
    
    'carrega grid
    Call Aguardar
    Call vVB_Generica_001.ExcluiBind(vBanco)
    vBanco.Parameters.Add "vCursor", 0, 3
    vBanco.Parameters("vCursor").ServerType = 102
    vBanco.Parameters("vCursor").DynasetOption = &H2&
    vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
    vBanco.Parameters.Add "vCOD", txtCod, 1
    If opt_Cliente Then
        vSql = "PRODUCAO.PCK_CAD740.PR_CON_CLIE(:vCursor,:vCOD)"
    Else
        vSql = "PRODUCAO.PCK_CAD740.PR_CON_GR(:vCursor,:vCOD)"
    End If
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    Unload frmAguardar
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro & ". Erro ao consultar por CGC.")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("vCursor").Value
    End If
    If Not vObjOracle.EOF Then
        'define grid
        mfgControle.Rows = 2
        mfgControle.Cols = 7 'TI-5073
        mfgControle.Row = 1
        'limpa linha
        For i = 0 To mfgControle.Cols - 1
            mfgControle.Col = i
            mfgControle.Text = ""
        Next i
        'encontra dados
        With mfgControle
            .Row = 1
            While Not vObjOracle.EOF
                .Col = 0
                .Text = Format(vObjOracle!cod_fornecedor, "000")
                .Col = 1
                .Text = Format(vObjOracle!cod_grupo, "00")
                .Col = 2
                .Text = Format(vObjOracle!cod_subgrupo, "00")
                .Col = 3
                .Text = vObjOracle!cod_tipo_cliente
                .Col = 4
                .Text = vObjOracle!cod_vdr
                .Col = 5
                .Text = vObjOracle!linha_produto
                'TI-5073
                .Col = 6
                .Text = vObjOracle!comiss_vdr
                'FIM TI-5073
                 vObjOracle.MoveNext
                 .Rows = .Rows + 1
                 .Row = .Rows - 1
                .Refresh
            Wend
            .Rows = .Rows - 1
        End With
        txtCod.Enabled = False
        If opt_Cliente = True Then
            opt_Cliente.Enabled = False
            opt_Grupo.Enabled = False
            opt_Cliente = True
        Else
            opt_Cliente.Enabled = False
            opt_Grupo.Enabled = False
            opt_Grupo = True
        End If
    Else
        'define grid
        mfgControle.Rows = 2
        mfgControle.Cols = 7
        mfgControle.Row = 1
        'limpa linha
        For i = 0 To mfgControle.Cols - 1
            mfgControle.Col = i
            mfgControle.Text = ""
        Next i
        Call vVB_Generica_001.Informar("N�o existem par�metros cadastrados para o Fornecedor informado.")
        Call cmdLimpar_Click
    End If

End Sub

Private Sub cmdImprimir_Click()

Dim vLinha As Integer

    'verifica campos
    If Trim(txtCod) = "" Then
        Call vVB_Generica_001.Informar("Digite o C�digo.")
        txtCod.SetFocus
        Exit Sub
    End If

    'confirma impress�o
    If vVB_Generica_001.Perguntar("Confirma impress�o do relat�rio?") = 7 Then
        Call cmdLimpar_Click
        Exit Sub
    End If
    
    'carrega dados
    Call Aguardar
    Call vVB_Generica_001.ExcluiBind(vBanco)
    vBanco.Parameters.Add "vCursor", 0, 3
    vBanco.Parameters("vCursor").ServerType = 102
    vBanco.Parameters("vCursor").DynasetOption = &H2&
    vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
    vBanco.Parameters.Add "vCOD", txtCod, 1
    vSql = "PRODUCAO.PCK_CAD740.PR_IMPRIMIR(:vCursor,:vCOD)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    Unload frmAguardar
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro & ". Erro impress�o.")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("vCursor").Value
    End If
    vLinha = 1
    If Not vObjOracle.EOF Then
        While vObjOracle.EOF = False
            If vLinha = 1 Or vLinha = 2 Or vLinha = 4 Then
                Printer.Print ""
            ElseIf vLinha = 3 Then
                Printer.FontUnderline = True
                Printer.FontBold = True
                Printer.FontItalic = True
                Printer.FontName = "Garamond"
                Printer.FontSize = 18
                Printer.Print Spc(26); "Relat�rio de Par�metros"
            ElseIf vLinha = 5 Then
                Printer.FontUnderline = False
                Printer.FontBold = True
                Printer.FontItalic = False
                Printer.FontName = "Arial"
                Printer.FontSize = 12
                Printer.Print Tab(1); "Fornecedor:  " & vObjOracle!cod_fornecedor & " - " & txtNome;
                Printer.Print
                Printer.Print
            ElseIf vLinha = 6 Then
                Printer.FontUnderline = False
                Printer.FontBold = True
                Printer.FontItalic = False
                Printer.FontName = "Arial"
                Printer.FontSize = 12
                Printer.Print String(150, "-")
                Printer.Print Tab(1); "Grupo";
                Printer.Print Tab(18); "SubGrupo";
                Printer.Print Tab(47); "Cliente";
                Printer.Print Tab(71); "VDR";
                Printer.Print Tab(78); "Linha"
                Printer.Print String(150, "-")
            Else
                Printer.FontUnderline = False
                Printer.FontBold = False
                Printer.FontItalic = False
                Printer.FontName = "Times New Roman"
                Printer.FontSize = 10
                Printer.Print Tab(2); vObjOracle!grupo;
                Printer.Print Tab(5); IIf(IsNull(vObjOracle!descricao), "", Mid(vObjOracle!descricao, 1, 14));
                Printer.Print Tab(26); vObjOracle!subgrupo;
                Printer.Print Tab(30); IIf(IsNull(vObjOracle!descsub), "", Mid(vObjOracle!descsub, 1, 20));
                Printer.Print Tab(66); vObjOracle!cod_tipo_cliente;
                Printer.Print Tab(70); Mid(vObjOracle!desc_tipo_cli, 1, 20);
                Printer.Print Tab(102); vObjOracle!cod_vdr;
                Printer.Print Tab(110); vObjOracle!linha_produto;
                Printer.Print Tab(114); Mid(vObjOracle!desc_linha, 1, 18)
                vObjOracle.MoveNext
            End If
            vLinha = vLinha + 1
            If vLinha >= 65 Then
                Printer.NewPage
                vLinha = 1
            End If
        Wend
        Printer.EndDoc
    Else
        Call vVB_Generica_001.Informar("N�o existem informa��es para gerar o relat�rio.")
        Call cmdLimpar_Click
    End If

End Sub

Private Sub cmdLimpar_Click()

    txtCod.Enabled = True
    
    txtCod = ""
    txtNome = ""

    'define grid
    mfgControle.Rows = 2
    mfgControle.Cols = 7 'TI-5073
    mfgControle.Row = 1
    
    'limpa linha
    For i = 0 To mfgControle.Cols - 1
        mfgControle.Col = i
        mfgControle.Text = ""
    Next i
          
    opt_Cliente.Value = False
    opt_Cliente.Enabled = True
    opt_Grupo.Value = True
    opt_Grupo.Enabled = True
          
    txtCod.SetFocus
    
End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub

Private Sub Form_Load()
    
    Me.Top = 0
    Me.Left = 0
    
    'define grid
    mfgControle.Rows = 2
    mfgControle.Cols = 7 'TI-5073
    mfgControle.Row = 1
    
    'limpa linha
    For i = 0 To mfgControle.Cols - 1
        mfgControle.Col = i
        mfgControle.Text = ""
    Next i
        
    'cabe�alho e alinhamento coluna
    mfgControle.Row = 0
    
    mfgControle.Col = 0
    mfgControle.Text = "Forn."
    mfgControle.FixedAlignment(0) = 4
    mfgControle.ColAlignment(0) = 4
    mfgControle.ColWidth(0) = 1000
    
    mfgControle.Col = 1
    mfgControle.Text = "Grupo"
    mfgControle.FixedAlignment(1) = 4
    mfgControle.ColAlignment(1) = 4
    mfgControle.ColWidth(1) = 1000
    
    mfgControle.Col = 2
    mfgControle.Text = "Subgrupo"
    mfgControle.FixedAlignment(2) = 4
    mfgControle.ColAlignment(2) = 4
    mfgControle.ColWidth(2) = 1000
    
    mfgControle.Col = 3
    mfgControle.Text = "Cliente"
    mfgControle.FixedAlignment(3) = 4
    mfgControle.ColAlignment(3) = 4
    mfgControle.ColWidth(3) = 1000
    
    mfgControle.Col = 4
    mfgControle.Text = "VDR"
    mfgControle.FixedAlignment(4) = 4
    mfgControle.ColAlignment(4) = 4
    mfgControle.ColWidth(4) = 1000
    
    mfgControle.Col = 5
    mfgControle.Text = "Linha"
    mfgControle.FixedAlignment(5) = 4
    mfgControle.ColAlignment(5) = 4
    mfgControle.ColWidth(5) = 1000
    
    'TI-5073
    mfgControle.Col = 6
    mfgControle.ColWidth(6) = 0
    'FIM TI-5073
    stbBarra.Panels(1) = "Duplo click na linha para alterar o cadastro"
    
    opt_Grupo.Value = True
    
End Sub

Private Sub mfgControle_DblClick()

Dim vSubGr As String
        
    mfgControle.Row = mfgControle.RowSel
    
    mfgControle.Col = 0
    If Trim(mfgControle.Text) = "" Then
        Exit Sub
    End If
    frmCadastro.txtCod = mfgControle.Text
    Call frmCadastro.txtCod_LostFocus
    
    mfgControle.Col = 1
    For i = 0 To frmCadastro.cmbGrupo.ListCount - 1
        If Mid(frmCadastro.cmbGrupo.List(i), 1, 2) = mfgControle.Text Then
            frmCadastro.cmbGrupo.ListIndex = i
            Exit For
        End If
    Next
    Call frmCadastro.cmbGrupo_Click
        
    mfgControle.Col = 2
    For i = 0 To frmCadastro.mfgSubGrupos.Rows - 1
        frmCadastro.mfgSubGrupos.Row = i
        frmCadastro.mfgSubGrupos.Col = 0
        If Mid(frmCadastro.mfgSubGrupos.Text, 1, 2) = mfgControle.Text Then
            frmCadastro.mfgSubGrupos.Row = i
            frmCadastro.mfgSubGrupos.Col = 1
            frmCadastro.mfgSubGrupos.Text = "X"
            frmCadastro.mfgSubGrupos.Col = 0
            vSubGr = frmCadastro.mfgSubGrupos.Text
            Exit For
        End If
    Next
    'colocar s� a linha do subgrupo em quest�o ...
    frmCadastro.mfgSubGrupos.Rows = 2
    frmCadastro.mfgSubGrupos.Row = 1
    frmCadastro.mfgSubGrupos.Col = 0
    frmCadastro.mfgSubGrupos.Text = vSubGr
    frmCadastro.mfgSubGrupos.Col = 1
    frmCadastro.mfgSubGrupos.Text = "X"
    
    mfgControle.Col = 3
    For i = 0 To frmCadastro.cmbCliente.ListCount - 1
        If Mid(frmCadastro.cmbCliente.List(i), 1, 2) = mfgControle.Text Then
            frmCadastro.cmbCliente.ListIndex = i
            Exit For
        End If
    Next
    
    mfgControle.Col = 4
    frmCadastro.txt_VDR = mfgControle.Text
    
    mfgControle.Col = 5
    For i = 0 To frmCadastro.cmbLinha.ListCount - 1
        If Mid(frmCadastro.cmbLinha.List(i), 1, 1) = mfgControle.Text Then
            frmCadastro.cmbLinha.ListIndex = i
            Exit For
        End If
    Next
    
    'TI-5370
    mfgControle.Col = 6
    frmCadastro.chkComissVDR.Value = IIf(mfgControle.Text = "S", 1, 0)
    'FIM TI-5370
    
    frmCadastro.txtCod.Enabled = False
    frmCadastro.txtNome.Enabled = False
    frmCadastro.cmbGrupo.Enabled = False
    frmCadastro.mfgSubGrupos.Enabled = False
    frmCadastro.cmbCliente.Enabled = False
    frmCadastro.txt_VDR.Enabled = True
    frmCadastro.cmbLinha.Enabled = True
   
    frmCadastro.Show
    Unload Me

End Sub

Private Sub txtCod_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtCod)

End Sub

Private Sub txtCod_LostFocus()

    If Trim(txtCod) <> "" Then
        'verifica c�digo
        'Call Aguardar
        Call vVB_Generica_001.ExcluiBind(vBanco)
        vBanco.Parameters.Add "vCursor", 0, 3
        vBanco.Parameters("vCursor").ServerType = 102
        vBanco.Parameters("vCursor").DynasetOption = &H2&
        vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
        vBanco.Parameters.Add "vFORN", txtCod, 1
        vSql = "PRODUCAO.PCK_CAD740.PR_VER_COD(:vCursor,:vFORN)"
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        'Unload frmAguardar
        If vErro <> "" Then
            vVB_Generica_001.ProcessaErro (vErro & ". Erro ao verificar C�digo.")
            Exit Sub
        Else
            Set vObjOracle = vBanco.Parameters("vCursor").Value
        End If
        If vObjOracle.EOF Then
            Call vVB_Generica_001.Informar("Fornecedor inv�lido.")
            txtCod = ""
            txtCod.SetFocus
            txtCod.Enabled = True
            Exit Sub
        Else
            txtCod = Format(txtCod, "000")
            txtCod.Enabled = False
            
            'Call Aguardar
            Call vVB_Generica_001.ExcluiBind(vBanco)
            vBanco.Parameters.Add "vCursor", 0, 3
            vBanco.Parameters("vCursor").ServerType = 102
            vBanco.Parameters("vCursor").DynasetOption = &H2&
            vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
            vBanco.Parameters.Add "vFORN", txtCod, 1
            vSql = "PRODUCAO.PCK_CAD740.PR_NOME(:vCursor,:vFORN)"
            vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
            'Unload frmAguardar
            If vErro <> "" Then
                vVB_Generica_001.ProcessaErro (vErro & ". Erro ao verificar Nome.")
                Exit Sub
            Else
                Set vObjOracle = vBanco.Parameters("vCursor").Value
            End If
            If Not vObjOracle.EOF Then
                txtNome = vObjOracle!nome_fornec
                txtNome.Enabled = False
            End If
           
        End If
    End If

End Sub
