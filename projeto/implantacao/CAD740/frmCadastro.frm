VERSION 5.00
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmCadastro 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cadastro de Controle VDR"
   ClientHeight    =   5910
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6990
   Icon            =   "frmCadastro.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   394
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   466
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame_Dados 
      Caption         =   "Dados"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   4605
      Left            =   90
      TabIndex        =   5
      Top             =   900
      Width           =   6825
      Begin VB.CheckBox chkComissVDR 
         Appearance      =   0  'Flat
         Caption         =   "Sim"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   6000
         TabIndex        =   20
         Top             =   4185
         Width           =   690
      End
      Begin VB.Frame Frame_SubGr 
         Caption         =   "Assinale os Subgrupos"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   1995
         Left            =   135
         TabIndex        =   11
         Top             =   1170
         Width           =   6570
         Begin MSFlexGridLib.MSFlexGrid mfgSubGrupos 
            Height          =   1590
            Left            =   135
            TabIndex        =   12
            Top             =   270
            Width           =   6287
            _ExtentX        =   11086
            _ExtentY        =   2805
            _Version        =   393216
            BackColorBkg    =   -2147483633
            AllowBigSelection=   0   'False
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.ComboBox cmbCliente 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         ItemData        =   "frmCadastro.frx":23D2
         Left            =   1665
         List            =   "frmCadastro.frx":23D4
         Style           =   2  'Dropdown List
         TabIndex        =   14
         Top             =   3330
         Width           =   3450
      End
      Begin VB.ComboBox cmbGrupo 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         ItemData        =   "frmCadastro.frx":23D6
         Left            =   1665
         List            =   "frmCadastro.frx":23D8
         Style           =   2  'Dropdown List
         TabIndex        =   10
         Top             =   720
         Width           =   2820
      End
      Begin VB.ComboBox cmbLinha 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         ItemData        =   "frmCadastro.frx":23DA
         Left            =   1665
         List            =   "frmCadastro.frx":23DC
         Style           =   2  'Dropdown List
         TabIndex        =   18
         Top             =   4140
         Width           =   2910
      End
      Begin MSMask.MaskEdBox txt_VDR 
         Height          =   330
         Left            =   1665
         TabIndex        =   16
         Top             =   3735
         Width           =   465
         _ExtentX        =   820
         _ExtentY        =   582
         _Version        =   393216
         MaxLength       =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         PromptChar      =   "�"
      End
      Begin MSMask.MaskEdBox txtCod 
         Height          =   330
         Left            =   1665
         TabIndex        =   7
         Top             =   315
         Width           =   600
         _ExtentX        =   1058
         _ExtentY        =   582
         _Version        =   393216
         MaxLength       =   3
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         PromptChar      =   "�"
      End
      Begin MSMask.MaskEdBox txtNome 
         Height          =   330
         Left            =   2340
         TabIndex        =   8
         Top             =   315
         Width           =   4370
         _ExtentX        =   7699
         _ExtentY        =   582
         _Version        =   393216
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         Enabled         =   0   'False
         MaxLength       =   30
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         PromptChar      =   "�"
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         Caption         =   "Comiss�o VDR:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   4680
         TabIndex        =   19
         Top             =   4185
         Width           =   1350
      End
      Begin VB.Label lblLinha 
         Appearance      =   0  'Flat
         Caption         =   "Linha do Produto:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   135
         TabIndex        =   17
         Top             =   4185
         Width           =   1530
      End
      Begin VB.Label lblVDR 
         Appearance      =   0  'Flat
         Caption         =   "C�d. VDR:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   135
         TabIndex        =   15
         Top             =   3780
         Width           =   1350
      End
      Begin VB.Label lblCliente 
         Appearance      =   0  'Flat
         Caption         =   "Tipo do Cliente:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   330
         Left            =   135
         TabIndex        =   13
         Top             =   3375
         Width           =   2655
      End
      Begin VB.Label lblGrupo 
         Appearance      =   0  'Flat
         Caption         =   "Grupo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   135
         TabIndex        =   9
         Top             =   765
         Width           =   1845
      End
      Begin VB.Label lblCod 
         Appearance      =   0  'Flat
         Caption         =   "C�d. Fornecedor:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   135
         TabIndex        =   6
         Top             =   360
         Width           =   1575
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   5580
      Width           =   6990
      _ExtentX        =   12330
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   12277
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   810
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadastro.frx":23DE
      PICN            =   "frmCadastro.frx":23FA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdLimpar 
      Height          =   690
      Left            =   810
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Limpar campos"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadastro.frx":30D4
      PICN            =   "frmCadastro.frx":30F0
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdOk 
      Default         =   -1  'True
      Height          =   690
      Left            =   1575
      TabIndex        =   3
      TabStop         =   0   'False
      ToolTipText     =   "Ok"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadastro.frx":3DCA
      PICN            =   "frmCadastro.frx":3DE6
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmCadastro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : frmCadastro
' Author    : C.SAMUEL.OLIVEIRA
' Date      : 06/10/16
' Purpose   : TI-5073
'---------------------------------------------------------------------------------------


Public Sub cmbGrupo_Click()

Dim vNum As Integer
Dim vNome As String
Dim vNumReg As Long

    mfgSubGrupos.Rows = 2
    mfgSubGrupos.Row = 1
    mfgSubGrupos.Col = 0
    mfgSubGrupos.Text = "00"
    mfgSubGrupos.Col = 1
    mfgSubGrupos.Text = ""
    
    If Trim(cmbGrupo) <> "" And Trim(cmbGrupo) <> "00" Then
        vNum = InStr(1, cmbGrupo, " ", 1)
        vNome = Mid(cmbGrupo.Text, 1, vNum - 1)
        
        'Call Aguardar
        Call vVB_Generica_001.ExcluiBind(vBanco)
        vBanco.Parameters.Add "vCursor", 0, 3
        vBanco.Parameters("vCursor").ServerType = 102
        vBanco.Parameters("vCursor").DynasetOption = &H2&
        vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
        vBanco.Parameters.Add "vGRUPO", vNome, 1
        vSql = "PRODUCAO.PCK_CAD740.PR_SUBGRUPO(:vCursor,:vGRUPO)"
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        'Unload frmAguardar
        If vErro <> "" Then
            vVB_Generica_001.ProcessaErro (vErro & ". Erro ao verificar Subgrupos.")
            Exit Sub
        Else
            Set vObjOracle = vBanco.Parameters("vCursor").Value
        End If
        vNumReg = vObjOracle!QtdReg
                 
        Call vVB_Generica_001.ExcluiBind(vBanco)
        vBanco.Parameters.Add "vCursor", 0, 3
        vBanco.Parameters("vCursor").ServerType = 102
        vBanco.Parameters("vCursor").DynasetOption = &H2&
        vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
        vBanco.Parameters.Add "vGRUPO", vNome, 1
        vSql = "PRODUCAO.PCK_CAD740.PR_SUBGRUPO_2(:vCursor,:vGRUPO)"
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        'Unload frmAguardar
        If vErro <> "" Then
            vVB_Generica_001.ProcessaErro (vErro & ". Erro ao verificar Subgrupos.")
            Exit Sub
        Else
            Set vObjOracle = vBanco.Parameters("vCursor").Value
        End If
        If vObjOracle.EOF = True Then
            Call vVB_Generica_001.Informar("N�o existem Subgrupos para o Grupo informado.")
            mfgSubGrupos.Visible = False
        Else
            mfgSubGrupos.Rows = vNumReg + 2:
            mfgSubGrupos.Col = 0
            For i = 2 To vNumReg + 1
                mfgSubGrupos.Row = i
                mfgSubGrupos.Text = Format(vObjOracle!cod_subgrupo, "00") & " - " & vObjOracle!desc_subgrupo
                vObjOracle.MoveNext
            Next i
        End If
        
    Else
        mfgSubGrupos.Rows = 2
    End If

End Sub

Private Sub cmdLimpar_Click()

    txtCod = ""
    txtNome = ""

    cmbGrupo.Clear
    cmbGrupo.AddItem ""
    cmbGrupo.Enabled = False
    
    'Limpa grid
    mfgSubGrupos.Rows = 1
    
    cmbCliente.ListIndex = -1
    txt_VDR = "1"
    cmbLinha.ListIndex = -1
    
    txtCod.Enabled = True
    txtCod.SetFocus
        
    mfgSubGrupos.Enabled = True
    cmbCliente.Enabled = True
    txt_VDR.Enabled = True
    cmbLinha.Enabled = True
    
    chkComissVDR.Value = False 'TI-5073

End Sub

Private Sub cmdOk_Click()

Dim vContSubgr As Integer

    'verifica campos
    If Trim(txtCod) = "" Then
        Call vVB_Generica_001.Informar("Digite o C�digo.")
        txtCod.Enabled = True
        txtCod.TabStop = True
        txtCod.SetFocus
        Exit Sub
    End If
    If Trim(cmbGrupo) = "" Then
        Call vVB_Generica_001.Informar("Selecione um Grupo.")
        cmbGrupo.SetFocus
        Exit Sub
    End If
    
    mfgSubGrupos.Col = 1
    For i = 1 To mfgSubGrupos.Rows - 1
        mfgSubGrupos.Row = i
        If mfgSubGrupos = "X" Then
            vContSubgr = vContSubgr + 1
        End If
    Next i
    If vContSubgr = 0 Then
        Call vVB_Generica_001.Informar("Selecione um SubGrupo.")
        mfgSubGrupos.SetFocus
        Exit Sub
    End If
    
    If Trim(cmbCliente) = "" Then
        Call vVB_Generica_001.Informar("Selecione o Tipo de Cliente.")
        cmbCliente.SetFocus
        Exit Sub
    End If
    If Trim(txt_VDR) = "" Then
        Call vVB_Generica_001.Informar("Digite o Valor VDR.")
        txt_VDR.SetFocus
        Exit Sub
    End If
    If Trim(cmbLinha) = "" Then
        Call vVB_Generica_001.Informar("Selecione a Linha do Produto.")
        cmbLinha.SetFocus
        Exit Sub
    End If
        
    'confirma inclus�o
    If vVB_Generica_001.Perguntar("Confirma as informa��es?") = 7 Then
        Call cmdLimpar_Click
        Exit Sub
    End If
            
    'verifica linhas do grid Subgrupos
    vQtdLinhas = mfgSubGrupos.Rows
    For i = 1 To vQtdLinhas - 1
        mfgSubGrupos.Col = 1
        mfgSubGrupos.Row = i
        If mfgSubGrupos.Text = "X" Then
            mfgSubGrupos.Col = 0
            'atualiza banco
            Call Aguardar
            Call vVB_Generica_001.ExcluiBind(vBanco)
            vBanco.Parameters.Add "cFORN", txtCod, 1
            vBanco.Parameters.Add "cGrupo", Mid(cmbGrupo, 1, 2), 1
            vBanco.Parameters.Add "cSubgrupo", Mid(mfgSubGrupos, 1, 2), 1
            vBanco.Parameters.Add "cCliente", Mid(cmbCliente, 1, 2), 1
            vBanco.Parameters.Add "cVdr", txt_VDR, 1
            vBanco.Parameters.Add "cProduto", Mid(cmbLinha, 1, 1), 1
            vBanco.Parameters.Add "cComiss", IIf(chkComissVDR.Value, "S", "N"), 1 'TI-5073
            vSql = "PRODUCAO.PCK_CAD740.PR_INC(:cFORN,:cGrupo,:cSubgrupo,:cCliente,:cVdr,:cProduto,:cComiss)" 'TI-5073
            vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
            Unload frmAguardar
            If vErro <> "" Then
                vVB_Generica_001.ProcessaErro (vErro & ". Erro Altera��o.")
                Call cmdLimpar_Click
                Exit Sub
            End If
            vContSubgr = vContSubgr + 1
        End If
    Next i
    
    'atualiza tela
    Call vVB_Generica_001.Informar("Dados atualizados com sucesso.")
    Call cmdLimpar_Click
    
End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub

Private Sub Form_Load()
    
    Me.Top = 0
    Me.Left = 0
    
    'define grid
    mfgSubGrupos.Rows = 2
    mfgSubGrupos.Cols = 2
    mfgSubGrupos.Row = 1
    
    'limpa linha
    For i = 0 To mfgSubGrupos.Cols - 1
        mfgSubGrupos.Col = i
        mfgSubGrupos.Text = ""
    Next i
        
    'cabe�alho e alinhamento coluna
    mfgSubGrupos.Row = 0
    
    mfgSubGrupos.Col = 0
    mfgSubGrupos.Text = "Subgrupo"
    mfgSubGrupos.FixedAlignment(0) = 4
    mfgSubGrupos.ColAlignment(0) = 1
    mfgSubGrupos.ColWidth(0) = 5000
    
    mfgSubGrupos.Col = 1
    mfgSubGrupos.Text = "Cadastrar"
    mfgSubGrupos.FixedAlignment(1) = 4
    mfgSubGrupos.ColAlignment(1) = 4
    mfgSubGrupos.ColWidth(1) = 1000
    
    'Combo Cliente
    'Call Aguardar
    Call vVB_Generica_001.ExcluiBind(vBanco)
    vBanco.Parameters.Add "vCursor", 0, 3
    vBanco.Parameters("vCursor").ServerType = 102
    vBanco.Parameters("vCursor").DynasetOption = &H2&
    vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
    vSql = "PRODUCAO.PCK_CAD740.PR_CLIENTE(:vCursor)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    'Unload frmAguardar
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro & ". Erro ao ler Clientes.")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("vCursor").Value
    End If
    While Not vObjOracle.EOF
        cmbCliente.AddItem Format(vObjOracle!cod_tipo_cli, "00") & " - " & vObjOracle!desc_tipo_cli
        vObjOracle.MoveNext
    Wend
    
    'Combo Linha
    'Call Aguardar
    Call vVB_Generica_001.ExcluiBind(vBanco)
    vBanco.Parameters.Add "vCursor", 0, 3
    vBanco.Parameters("vCursor").ServerType = 102
    vBanco.Parameters("vCursor").DynasetOption = &H2&
    vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
    vSql = "PRODUCAO.PCK_CAD740.PR_LINHA(:vCursor)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    'Unload frmAguardar
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro & ". Erro ao ler Linha de Produto.")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("vCursor").Value
    End If
    While Not vObjOracle.EOF
        cmbLinha.AddItem vObjOracle!linha_produto & " - " & vObjOracle!desc_linha
        vObjOracle.MoveNext
    Wend
    
    txt_VDR = "1"
        
End Sub

Private Sub txtCod_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtCod)

End Sub

Public Sub txtCod_LostFocus()

    If Trim(txtCod) <> "" Then
        'verifica c�digo
        'Call Aguardar
        Call vVB_Generica_001.ExcluiBind(vBanco)
        vBanco.Parameters.Add "vCursor", 0, 3
        vBanco.Parameters("vCursor").ServerType = 102
        vBanco.Parameters("vCursor").DynasetOption = &H2&
        vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
        vBanco.Parameters.Add "vFORN", txtCod, 1
        vSql = "PRODUCAO.PCK_CAD740.PR_VER_COD(:vCursor,:vFORN)"
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        'Unload frmAguardar
        If vErro <> "" Then
            vVB_Generica_001.ProcessaErro (vErro & ". Erro ao verificar C�digo.")
            Exit Sub
        Else
            Set vObjOracle = vBanco.Parameters("vCursor").Value
        End If
        If vObjOracle.EOF Then
            Call vVB_Generica_001.Informar("Fornecedor inv�lido.")
            txtCod = ""
            txtCod.SetFocus
            txtCod.Enabled = True
            Exit Sub
        Else
            txtCod = Format(txtCod, "000")
            txtCod.Enabled = False
            
            'Call Aguardar
            Call vVB_Generica_001.ExcluiBind(vBanco)
            vBanco.Parameters.Add "vCursor", 0, 3
            vBanco.Parameters("vCursor").ServerType = 102
            vBanco.Parameters("vCursor").DynasetOption = &H2&
            vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
            vBanco.Parameters.Add "vFORN", txtCod, 1
            vSql = "PRODUCAO.PCK_CAD740.PR_NOME(:vCursor,:vFORN)"
            vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
            'Unload frmAguardar
            If vErro <> "" Then
                vVB_Generica_001.ProcessaErro (vErro & ". Erro ao verificar Nome.")
                Exit Sub
            Else
                Set vObjOracle = vBanco.Parameters("vCursor").Value
            End If
            If Not vObjOracle.EOF Then
                txtNome = vObjOracle!nome_fornec
                txtNome.Enabled = False
            End If
            
            'Call Aguardar
            Call vVB_Generica_001.ExcluiBind(vBanco)
            vBanco.Parameters.Add "vCursor", 0, 3
            vBanco.Parameters("vCursor").ServerType = 102
            vBanco.Parameters("vCursor").DynasetOption = &H2&
            vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
            vBanco.Parameters.Add "vFORN", txtCod, 1
            vSql = "PRODUCAO.PCK_CAD740.PR_GRUPO(:vCursor,:vFORN)"
            vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
            'Unload frmAguardar
            If vErro <> "" Then
                vVB_Generica_001.ProcessaErro (vErro & ". Erro ao verificar Grupos.")
                Exit Sub
            Else
                Set vObjOracle = vBanco.Parameters("vCursor").Value
            End If
            If Not vObjOracle.EOF Then
                cmbGrupo.Enabled = True
                cmbGrupo.Clear
                cmbGrupo.AddItem "00"
                While vObjOracle.EOF = False
                    cmbGrupo.AddItem Format(vObjOracle!cod_grupo, "00") & " - " & vObjOracle!desc_grupo
                    vObjOracle.MoveNext
                Wend
            Else
                cmbGrupo.Clear
                cmbGrupo.AddItem "00"
                cmbGrupo.Enabled = False
            End If
        End If
    End If

End Sub

Private Sub txt_VDR_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txt_VDR)

End Sub

Private Sub txt_VDR_LostFocus()

    If txt_VDR <> "" Then
        If txt_VDR = "0" Then
            Call vVB_Generica_001.Informar("Valor inv�lido.")
            txt_VDR = ""
            txt_VDR.SetFocus
        End If
    End If
    
End Sub

Private Sub mfgSubGrupos_DblClick()

Dim vLinha As Long

    vLinha = mfgSubGrupos.Row
    
    If vLinha <> 0 Then
        mfgSubGrupos.Col = 1
        If mfgSubGrupos.Text = "X" Then
            mfgSubGrupos.Text = ""
        Else
            mfgSubGrupos.Text = "X"
        End If
    End If

End Sub
