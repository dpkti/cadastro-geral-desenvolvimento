create or replace package PCK_CAD220 is

  -- Author  : CONSULTOR17
  -- Created : 11/01/05 15:29:52
  -- Purpose : Cadastro de Franquia
  
  Type TP_CURSOR IS REF CURSOR;
  
  PROCEDURE PR_UPDATE_REPRES (pCODFILIAL IN NUMBER,
                               pCODREPR IN NUMBER);
                               
  PROCEDURE PR_SELECT_NOME_REPRES (cCURSOR IN OUT TP_CURSOR,
                                   pCODFIL IN NUMBER,
                                   pCODFRA IN NUMBER,
                                   pSEQ IN NUMBER);
                               
  PROCEDURE PR_SELECT_FRANQ_REPRES (cCURSOR IN OUT TP_CURSOR,
                                    pCODFIL IN NUMBER,
                                    pCODFRA IN NUMBER);
                               
  PROCEDURE PR_SELECT_FILIAL (cCURSOR IN OUT TP_CURSOR,
                              pCODFIL1 IN NUMBER);
                               
  PROCEDURE PR_SELECT_PSEUD_REPRES (cCURSOR IN OUT TP_CURSOR,
                                    pCODFIL2 IN NUMBER,
                                    pCODREPR IN NUMBER);                                                                                                                                                      
end PCK_CAD220;
/
create or replace package body PCK_CAD220 is

   PROCEDURE PR_UPDATE_REPRES (pCODFILIAL IN NUMBER,
                               pCODREPR IN NUMBER)IS
   BEGIN           
        UPDATE REPRESENTANTE 
        SET SEQ_FRANQUIA = 0 
        WHERE COD_FILIAL = pCODFILIAL AND 
              COD_REPRES = pCODREPR; 
        COMMIT; 
   END PR_UPDATE_REPRES;
-----------------------------------------------------------------        
   PROCEDURE PR_SELECT_NOME_REPRES (cCURSOR IN OUT TP_CURSOR,
                                    pCODFIL IN NUMBER,
                                    pCODFRA IN NUMBER,
                                    pSEQ IN NUMBER)IS
   BEGIN
        OPEN cCURSOR FOR
        SELECT R.NOME_REPRES 
        FROM FILIAL F, REPRESENTANTE R 
        WHERE F.TP_FILIAL = 'F'           AND 
              F.COD_FILIAL = R.COD_FILIAL AND 
              R.COD_FILIAL = pCODFIL      AND 
              F.COD_FRANQUEADOR = pCODFRA AND 
              R.SEQ_FRANQUIA = pSEQ;
   END PR_SELECT_NOME_REPRES; 
-----------------------------------------------------------------
   PROCEDURE PR_SELECT_FRANQ_REPRES (cCURSOR IN OUT TP_CURSOR,
                                     pCODFIL IN NUMBER,
                                     pCODFRA IN NUMBER)IS
   BEGIN
        OPEN cCURSOR FOR
        SELECT F.COD_FRANQUEADOR ||'.'||R.SEQ_FRANQUIA Cod,
               R.NOME_REPRES "Nome Representante", 
               R.PSEUDONIMO "Pseudonimo", 
               R.COD_REPRES "Origem" 
        FROM FILIAL F, 
             REPRESENTANTE R 
        WHERE F.TP_FILIAL = 'F'         AND 
              R.SEQ_FRANQUIA = 0 and --<> 0       AND 
              R.COD_FILIAL = pCODFIL    AND 
              COD_FRANQUEADOR = pCODFRA AND 
              R.SITUACAO = 0 
        ORDER BY (F.COD_FRANQUEADOR + R.SEQ_FRANQUIA), 
                  R.NOME_REPRES;
   END PR_SELECT_FRANQ_REPRES;
-----------------------------------------------------------------
   PROCEDURE PR_SELECT_FILIAL (cCURSOR IN OUT TP_CURSOR,
                               pCODFIL1 IN NUMBER)IS
   BEGIN 
        OPEN cCURSOR FOR
        SELECT NOME_FILIAL, 
               COD_FRANQUEADOR 
        FROM FILIAL 
        WHERE COD_FILIAL = pCODFIL1 AND
              TP_FILIAL = 'F';
   END PR_SELECT_FILIAL;
-----------------------------------------------------------------
   PROCEDURE PR_SELECT_PSEUD_REPRES (cCURSOR IN OUT TP_CURSOR,
                               pCODFIL2 IN NUMBER,
                               pCODREPR IN NUMBER)IS
   BEGIN
        OPEN cCURSOR FOR      
        SELECT NOME_REPRES, 
               PSEUDONIMO 
        FROM REPRESENTANTE 
        WHERE COD_FILIAL = pCODFIL2 AND 
              COD_REPRES = pCODREPR AND 
              TIPO = 'R';
   END PR_SELECT_PSEUD_REPRES;
-----------------------------------------------------------------   
end PCK_CAD220;
/
