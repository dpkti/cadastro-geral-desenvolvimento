VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmCadGarantia 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cadastro"
   ClientHeight    =   3360
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5715
   Icon            =   "frmCadGarantia.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   224
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   381
   Begin VB.ComboBox cmbResp 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   1485
      TabIndex        =   5
      Top             =   2475
      Width           =   4110
   End
   Begin VB.ComboBox cmbSituacao 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   1485
      TabIndex        =   4
      Top             =   1980
      Width           =   1725
   End
   Begin VB.TextBox txtDTFIM 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   4410
      TabIndex        =   3
      Top             =   1530
      Width           =   1230
   End
   Begin VB.TextBox txtDTINI 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1485
      TabIndex        =   2
      Top             =   1530
      Width           =   1230
   End
   Begin VB.TextBox txtRepres 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   2250
      Locked          =   -1  'True
      TabIndex        =   9
      Top             =   1080
      Width           =   3390
   End
   Begin VB.TextBox txtCodRepres 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1485
      TabIndex        =   1
      Top             =   1080
      Width           =   690
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   3030
      Width           =   5715
      _ExtentX        =   10081
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   9578
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   6
      Top             =   810
      Width           =   5640
      _ExtentX        =   9948
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   7
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadGarantia.frx":23D2
      PICN            =   "frmCadGarantia.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd1 
      Height          =   690
      Left            =   855
      TabIndex        =   13
      TabStop         =   0   'False
      ToolTipText     =   "Gravar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadGarantia.frx":30C8
      PICN            =   "frmCadGarantia.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd2 
      Height          =   690
      Left            =   1575
      TabIndex        =   15
      TabStop         =   0   'False
      ToolTipText     =   "Cancelar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadGarantia.frx":3DBE
      PICN            =   "frmCadGarantia.frx":3DDA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label4 
      Appearance      =   0  'Flat
      Caption         =   "Autorizado por:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   180
      TabIndex        =   14
      Top             =   2520
      Width           =   1320
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      Caption         =   "Situa��o:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   675
      TabIndex        =   12
      Top             =   2025
      Width           =   960
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      Caption         =   "Data T�rmino:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   3195
      TabIndex        =   11
      Top             =   1575
      Width           =   1320
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      Caption         =   "Data Inicio:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   495
      TabIndex        =   10
      Top             =   1575
      Width           =   1005
   End
   Begin VB.Label lbl 
      Appearance      =   0  'Flat
      Caption         =   "Representante:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   135
      TabIndex        =   8
      Top             =   1125
      Width           =   1455
   End
End
Attribute VB_Name = "frmCadGarantia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim ACT As String
Dim vSIT As Integer

Private Sub cmd1_Click()

    If txtCodRepres.Text = "" Then
        MsgBox "Escolha uma Representante", , "Aten��o"
        txtCodRepres.SetFocus
        Exit Sub
    End If

    If txtDTINI.Text = "" Then
        MsgBox "Entre coma Data de Inicio", , "Aten��o"
        txtDTINI.SetFocus
        Exit Sub
    End If
    
    If ACT = "I" Then
        If CDate(DATA_FATURA) > CDate(txtDTINI.Text) Then
            MsgBox "Data de Inicio deve ser maior que a data de faturamento", , "Aten��o"
            txtDTINI.SetFocus
            Exit Sub
        End If
    End If
    
    If txtDTFIM.Text = "" Then
        MsgBox "Entre coma Data de Termino", , "Aten��o"
        txtDTFIM.SetFocus
        Exit Sub
    End If
    
    If CDate(DATA_FATURA) > CDate(txtDTFIM.Text) Then
        MsgBox "Data de Termino deve ser maior que a data de faturamento", , "Aten��o"
        txtDTFIM.SetFocus
        Exit Sub
    End If
    
    If CDate(txtDTINI.Text) > CDate(txtDTFIM.Text) Then
        MsgBox "Data de Termino deve ser maior que a data de Inicio", , "Aten��o"
        txtDTFIM.SetFocus
        Exit Sub
    End If
    
    If cmbSituacao.Text = "" Then
        MsgBox "Escolha uma situa��o", , "Aten��o"
        cmbSituacao.SetFocus
        Exit Sub
    End If
    
    If cmbResp.Text = "" Then
        MsgBox "Selecione o Respons�vel pela libera�ao", , "Aten��o"
        cmbResp.SetFocus
        Exit Sub
    End If
    
    If cmbSituacao.Text = "ATIVO" Then
        vSIT = 0
    Else
        vSIT = 9
    End If
    
    vRESP = Mid(cmbResp.Text, 1, InStr(1, cmbResp.Text, " - ") - 1)
    
    vBanco.Parameters.Remove "PM_USRSCP"
    vBanco.Parameters.Add "PM_USRSCP", COD_USR_SPC, 1
    
    vBanco.Parameters.Remove "PM_ACT"
    vBanco.Parameters.Add "PM_ACT", ACT, 1
    
    vBanco.Parameters.Remove "PM_CODREPR"
    vBanco.Parameters.Add "PM_CODREPR", txtCodRepres.Text, 1
    
    vBanco.Parameters.Remove "PM_DTINI"
    vBanco.Parameters.Add "PM_DTINI", txtDTINI.Text, 1
    
    vBanco.Parameters.Remove "PM_DTFIM"
    vBanco.Parameters.Add "PM_DTFIM", txtDTFIM.Text, 1
    
    vBanco.Parameters.Remove "PM_SITUACAO"
    vBanco.Parameters.Add "PM_SITUACAO", vSIT, 1
    
    vBanco.Parameters.Remove "PM_CODRESP"
    vBanco.Parameters.Add "PM_CODRESP", vRESP, 1
    
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "Producao.PCK_CAD970.PR_ACT_GARANTIA(:PM_USRSCP,:PM_ACT,:PM_CODREPR,:PM_DTINI,:PM_DTFIM,:PM_SITUACAO,:PM_CODRESP,:PM_CODERRO,:PM_TXTERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    If vErro <> "" Then
        MsgBox "ERRO:" & vBanco.Parameters("PM_TXTERRO"), , "ATEN��O"
        Exit Sub
    End If
    
    ACT = "A"
    
    MsgBox "Gravado com sucesso!"
    
    txtCodRepres.Enabled = False
    txtDTINI.Enabled = False

End Sub

Private Sub cmd2_Click()
    
    txtCodRepres.Text = ""
    txtCodRepres.Enabled = True
    txtRepres.Text = ""
    
    txtDTFIM.Text = ""
    
    txtDTINI.Text = ""
    txtDTINI.Enabled = True
    
    cmbResp.Text = ""
    cmbSituacao = ""
    
    ACT = ""
    
End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    
    Me.Top = 1500
    Me.Left = 300
    
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
    
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "Producao.PCK_CAD970.PR_CON_RESPONSAVEIS(:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
    
    vVB_Generica_001.PreencheComboList vObjOracle, cmbResp, "COD_USUARIO", "NOME_USUARIO"
    
    
    cmbSituacao.AddItem "ATIVO"
    cmbSituacao.AddItem "CANCELADO"
    
    
End Sub

Private Sub txtCodRepres_DblClick()
    frmRepres.Show 1
End Sub

Private Sub txtCodRepres_KeyPress(Keyascii As Integer)
   Keyascii = vVB_Generica_001.Numero(Keyascii)
End Sub

Private Sub txtCodRepres_LostFocus()
      If txtCodRepres.Text <> "" Then
    
        Set vObjOracle = vVB_Venda_001.TabelaRepresentante(vBanco, txtCodRepres.Text, , , , , , , "R", 0)
        
        If Not vObjOracle.EOF Then
            txtRepres.Text = vObjOracle("PSEUDONIMO")
            
            vBanco.Parameters.Remove "PM_CODREPRES"
            vBanco.Parameters.Add "PM_CODREPRES", txtCodRepres.Text, 1
            
            vBanco.Parameters.Remove "PM_CURSOR1"
            vBanco.Parameters.Add "PM_CURSOR1", 0, 3
            vBanco.Parameters("PM_CURSOR1").ServerType = 102
            vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
            vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
            
            vBanco.Parameters.Remove "PM_CODERRO"
            vBanco.Parameters.Add "PM_CODERRO", 0, 2
            vBanco.Parameters.Remove "PM_TXTERRO"
            vBanco.Parameters.Add "PM_TXTERRO", "", 2
            
            vSql = "Producao.PCK_CAD970.PR_CON_GARANTIA(:PM_CODREPRES,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
            
            vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        
            Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
            
            If Not vObjOracle.EOF Then
                
                txtDTINI.Text = vObjOracle(2)
                txtDTFIM.Text = vObjOracle(3)
                
                If vObjOracle(4) = "0" Then
                    cmbSituacao.Text = "ATIVO"
                Else
                    cmbSituacao.Text = "CANCELADO"
                End If
                
                ACT = "A"
                
                txtCodRepres.Enabled = False
                txtDTINI.Enabled = True
                
            Else
                txtDTFIM.Text = ""
                txtDTINI.Text = ""
                cmbSituacao.Text = ""
                ACT = "I"
            End If
            
        Else
            txtRepres.Text = ""
        End If
    End If

End Sub

Private Sub txtDTINI_KeyPress(Keyascii As Integer)
    DATA Keyascii, txtDTINI
End Sub

Private Sub txtDTFIM_KeyPress(Keyascii As Integer)
    DATA Keyascii, txtDTFIM
End Sub
