Attribute VB_Name = "Module1"
Public COD_USR_SPC As Integer
Public DATA_FATURA As Date


Sub DATA(ByRef Keyascii, ByRef TxtCampo)
    On Error GoTo TrataErro

    Dim bTam As Byte    'tamanho do campo JA digitado
    Dim strData As String
    Dim bKey As Byte
    bTam = Len(TxtCampo.Text)
    
    If Keyascii = 8 And bTam > 0 Then 'backspace
        If Mid$(TxtCampo.Text, bTam) = "/" Then
            TxtCampo.Text = Mid$(TxtCampo.Text, 1, bTam - 2)
        Else
            TxtCampo.Text = Mid$(TxtCampo.Text, 1, bTam - 1)
        End If
            
    ElseIf Chr$(Keyascii) >= "0" And Chr$(Keyascii) <= "9" Then
        If bTam = 1 Then
            strData = TxtCampo.Text & Chr$(Keyascii)
            If CInt(strData) < 1 Or CInt(strData > 31) Then
                Beep
            Else
                TxtCampo.Text = TxtCampo.Text & Chr$(Keyascii) & "/"
            End If
            
        ElseIf bTam = 4 Then
            strData = Mid$(TxtCampo.Text, 4) & Chr$(Keyascii)
            If CInt(strData) < 1 Or CInt(strData > 12) Then
                Beep
            Else
                TxtCampo.Text = TxtCampo.Text & Chr$(Keyascii) & "/"
            End If
        
        ElseIf bTam = 7 Then
            strData = Mid$(TxtCampo.Text, 1, 2)     'dia
            If CInt(strData) < 1 Or CInt(strData > 31) Then
                Beep
            Else
                strData = Mid$(TxtCampo.Text, 4, 2)     'mes
                If CInt(strData) < 1 Or CInt(strData > 12) Then
                    Beep
                Else
                    strData = TxtCampo.Text & Chr$(Keyascii)
                    If Not IsDate(CDate(strData)) Then
                        Beep
                    Else
                        TxtCampo.Text = strData
                    End If
                End If
            End If
            
        ElseIf bTam < 8 Then
            bKey = Keyascii
            
        Else
            bKey = 0
        End If
    Else
        Beep
    End If
    
    SendKeys "{END}"
    Keyascii = bKey
    Exit Sub
    
TrataErro:

    If Err.Number = 13 Then
        MsgBox strData, vbInformation, "Data Inv�lida"
        Keyascii = 0
        Beep
        Err.Clear
    End If

End Sub
