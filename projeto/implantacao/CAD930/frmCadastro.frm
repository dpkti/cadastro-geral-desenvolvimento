VERSION 5.00
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form frmCadastro 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cadastro de Advogados"
   ClientHeight    =   5400
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5700
   Icon            =   "frmCadastro.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   360
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   380
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame FrameAdv 
      Caption         =   "Advogado"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   795
      Left            =   90
      TabIndex        =   5
      Top             =   945
      Width           =   5550
      Begin MSMask.MaskEdBox txtCodAdv 
         Height          =   330
         Left            =   675
         TabIndex        =   7
         Top             =   270
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   582
         _Version        =   393216
         MaxLength       =   3
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         PromptChar      =   "�"
      End
      Begin MSMask.MaskEdBox txtSigla 
         Height          =   330
         Left            =   2205
         TabIndex        =   9
         Top             =   270
         Width           =   3165
         _ExtentX        =   5583
         _ExtentY        =   582
         _Version        =   393216
         MaxLength       =   25
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         PromptChar      =   "�"
      End
      Begin VB.Label lblCod 
         Appearance      =   0  'Flat
         Caption         =   "C�d.:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   135
         TabIndex        =   6
         Top             =   315
         Width           =   570
      End
      Begin VB.Label lblSigla 
         Appearance      =   0  'Flat
         Caption         =   "Sigla:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   1710
         TabIndex        =   8
         Top             =   315
         Width           =   435
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Dados"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   3225
      Left            =   90
      TabIndex        =   10
      Top             =   1800
      Width           =   5550
      Begin VB.ComboBox cmbCidade 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         ItemData        =   "frmCadastro.frx":23D2
         Left            =   1080
         List            =   "frmCadastro.frx":23D4
         Style           =   2  'Dropdown List
         TabIndex        =   20
         Top             =   1910
         Width           =   3735
      End
      Begin VB.ComboBox cmbSituacao 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         ItemData        =   "frmCadastro.frx":23D6
         Left            =   1080
         List            =   "frmCadastro.frx":23E0
         Style           =   2  'Dropdown List
         TabIndex        =   14
         Top             =   668
         Width           =   1890
      End
      Begin MSMask.MaskEdBox txtBairro 
         Height          =   330
         Left            =   1080
         TabIndex        =   18
         Top             =   1485
         Width           =   2580
         _ExtentX        =   4551
         _ExtentY        =   582
         _Version        =   393216
         MaxLength       =   30
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         PromptChar      =   "�"
      End
      Begin MSMask.MaskEdBox txtEnd 
         Height          =   330
         Left            =   1080
         TabIndex        =   16
         Top             =   1080
         Width           =   4290
         _ExtentX        =   7567
         _ExtentY        =   582
         _Version        =   393216
         MaxLength       =   33
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         PromptChar      =   "�"
      End
      Begin MSMask.MaskEdBox txtCEP 
         Height          =   330
         Left            =   1080
         TabIndex        =   22
         Top             =   2340
         Width           =   1050
         _ExtentX        =   1852
         _ExtentY        =   582
         _Version        =   393216
         MaxLength       =   8
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         PromptChar      =   "�"
      End
      Begin MSMask.MaskEdBox txtDDD 
         Height          =   330
         Left            =   2745
         TabIndex        =   24
         Top             =   2340
         Width           =   555
         _ExtentX        =   979
         _ExtentY        =   582
         _Version        =   393216
         MaxLength       =   4
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         PromptChar      =   "�"
      End
      Begin MSMask.MaskEdBox txtFone 
         Height          =   330
         Left            =   3960
         TabIndex        =   26
         Top             =   2340
         Width           =   1410
         _ExtentX        =   2487
         _ExtentY        =   582
         _Version        =   393216
         MaxLength       =   9
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         PromptChar      =   "�"
      End
      Begin MSMask.MaskEdBox txtEmail 
         Height          =   330
         Left            =   1080
         TabIndex        =   28
         Top             =   2745
         Width           =   4290
         _ExtentX        =   7567
         _ExtentY        =   582
         _Version        =   393216
         MaxLength       =   60
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         PromptChar      =   "�"
      End
      Begin MSMask.MaskEdBox txtNome 
         Height          =   330
         Left            =   1080
         TabIndex        =   12
         Top             =   270
         Width           =   4290
         _ExtentX        =   7567
         _ExtentY        =   582
         _Version        =   393216
         MaxLength       =   40
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         PromptChar      =   "�"
      End
      Begin VB.Label lblSituacao 
         Appearance      =   0  'Flat
         Caption         =   "Situa��o:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   135
         TabIndex        =   13
         Top             =   720
         Width           =   795
      End
      Begin VB.Label lblEnd 
         Appearance      =   0  'Flat
         Caption         =   "Endere�o:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   135
         TabIndex        =   15
         Top             =   1125
         Width           =   975
      End
      Begin VB.Label lblBairro 
         Appearance      =   0  'Flat
         Caption         =   "Bairro:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   135
         TabIndex        =   17
         Top             =   1530
         Width           =   615
      End
      Begin VB.Label lblCidade 
         Appearance      =   0  'Flat
         Caption         =   "Cidade:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   135
         TabIndex        =   19
         Top             =   1950
         Width           =   675
      End
      Begin VB.Label lblDDD 
         Appearance      =   0  'Flat
         Caption         =   "DDD:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   2250
         TabIndex        =   23
         Top             =   2385
         Width           =   435
      End
      Begin VB.Label lblCEP 
         Appearance      =   0  'Flat
         Caption         =   "CEP:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   135
         TabIndex        =   21
         Top             =   2385
         Width           =   390
      End
      Begin VB.Label lblFone 
         Appearance      =   0  'Flat
         Caption         =   "Fone:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   3420
         TabIndex        =   25
         Top             =   2385
         Width           =   480
      End
      Begin VB.Label lblEmail 
         Appearance      =   0  'Flat
         Caption         =   "E-mail:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   135
         TabIndex        =   27
         Top             =   2790
         Width           =   705
      End
      Begin VB.Label lblNome 
         Appearance      =   0  'Flat
         Caption         =   "Nome:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   135
         TabIndex        =   11
         Top             =   315
         Width           =   570
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   5070
      Width           =   5700
      _ExtentX        =   10054
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   10001
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   810
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadastro.frx":23FF
      PICN            =   "frmCadastro.frx":241B
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdLimpar 
      Height          =   690
      Left            =   810
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Limpar Campos"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadastro.frx":30F5
      PICN            =   "frmCadastro.frx":3111
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdGravar 
      Height          =   690
      Left            =   1575
      TabIndex        =   3
      TabStop         =   0   'False
      ToolTipText     =   "Inclus�o/Altera��o"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadastro.frx":3DEB
      PICN            =   "frmCadastro.frx":3E07
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmCadastro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdGravar_Click()

    'verifica campos
    If Trim(txtCodAdv) = "" Then
        Call vVB_Generica_001.Informar("Digite o C�digo.")
        txtCodAdv.SetFocus
        Exit Sub
    End If
    If Trim(txtSigla) = "" Then
        Call vVB_Generica_001.Informar("Digite a Sigla.")
        txtSigla.SetFocus
        Exit Sub
    End If
    If Trim(txtNome) = "" Then
        Call vVB_Generica_001.Informar("Digite o Nome.")
        txtNome.SetFocus
        Exit Sub
    End If
    If Trim(cmbSituacao) = "" Then
        Call vVB_Generica_001.Informar("Selecione a Situa��o.")
        cmbSituacao.SetFocus
        Exit Sub
    End If
    If Trim(txtEnd) = "" Then
        Call vVB_Generica_001.Informar("Digite o Endere�o.")
        txtEnd.SetFocus
        Exit Sub
    End If
    If Trim(txtBairro) = "" Then
        Call vVB_Generica_001.Informar("Digite o Bairro.")
        txtBairro.SetFocus
        Exit Sub
    End If
    If Trim(cmbCidade) = "" Then
        Call vVB_Generica_001.Informar("Selecione a Cidade.")
        cmbCidade.SetFocus
        Exit Sub
    End If
    If Trim(txtCEP) = "" Then
        Call vVB_Generica_001.Informar("Digite o CEP.")
        txtCEP.SetFocus
        Exit Sub
    End If
    If Trim(txtDDD) = "" Then
        Call vVB_Generica_001.Informar("Digite o DDD.")
        txtDDD.SetFocus
        Exit Sub
    End If
    If Trim(txtFone) = "" Then
        Call vVB_Generica_001.Informar("Digite o Telefone.")
        txtFone.SetFocus
        Exit Sub
    End If
    If Trim(txtEmail) = "" Then
        Call vVB_Generica_001.Informar("Digite o E-mail.")
        txtEmail.SetFocus
        Exit Sub
    End If
    
    If Not txtCodAdv.Enabled Then
   
        'confirma altera��o
        If vVB_Generica_001.Perguntar("Confirma altera��o?") = 7 Then
            Call cmdLimpar_Click
            Exit Sub
        End If
        
        'atualiza banco
        Call Aguardar
        Call vVB_Generica_001.ExcluiBind(vBanco)
        vBanco.Parameters.Add "v_cod_advogado", txtCodAdv, 1
        vBanco.Parameters.Add "v_Sigla", txtSigla, 1
        vBanco.Parameters.Add "v_nome", txtNome, 1
        vBanco.Parameters.Add "v_endereco", txtEnd, 1
        vBanco.Parameters.Add "v_bairro", txtBairro, 1
        vBanco.Parameters.Add "v_cep", txtCEP, 1
        vBanco.Parameters.Add "v_cod_cidade", Mid(cmbCidade, 1, 4), 1
        vBanco.Parameters.Add "v_ddd", txtDDD, 1
        vBanco.Parameters.Add "v_fone", txtFone, 1
        vBanco.Parameters.Add "v_situacao", Mid(cmbSituacao, 1, 1), 1
        vBanco.Parameters.Add "v_email", txtEmail, 1
        vSql = "PRODUCAO.PCK_CAD930.PR_UPD_ADVOGADO(:v_cod_advogado,:v_Sigla,:v_nome,:v_endereco,:v_bairro,:v_cep,:v_cod_cidade,:v_ddd,:v_fone,:v_situacao,:v_email)"
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        Unload frmAguardar
        If vErro <> "" Then
            vVB_Generica_001.ProcessaErro (vErro & ". Erro Altera��o.")
            Call cmdLimpar_Click
            Exit Sub
        End If
        
        Call vVB_Generica_001.Informar("Altera��o efetuada com sucesso.")
        Call cmdLimpar_Click
        
    Else
        
        'confirma inclus�o
        If vVB_Generica_001.Perguntar("Confirma inclus�o?") = 7 Then
            Call cmdLimpar_Click
            Exit Sub
        End If
        
        'atualiza banco
        Call Aguardar
        Call vVB_Generica_001.ExcluiBind(vBanco)
        vBanco.Parameters.Add "v_cod_advogado", txtCodAdv, 1
        vBanco.Parameters.Add "v_Sigla", txtSigla, 1
        vBanco.Parameters.Add "v_nome", txtNome, 1
        vBanco.Parameters.Add "v_endereco", txtEnd, 1
        vBanco.Parameters.Add "v_bairro", txtBairro, 1
        vBanco.Parameters.Add "v_cep", txtCEP, 1
        vBanco.Parameters.Add "v_cod_cidade", Mid(cmbCidade, 1, 4), 1
        vBanco.Parameters.Add "v_ddd", txtDDD, 1
        vBanco.Parameters.Add "v_fone", txtFone, 1
        vBanco.Parameters.Add "v_situacao", Mid(cmbSituacao, 1, 1), 1
        vBanco.Parameters.Add "v_email", txtEmail, 1
        vSql = "PRODUCAO.PCK_CAD930.PR_INC_ADVOGADO(:v_cod_advogado,:v_Sigla,:v_nome,:v_endereco,:v_bairro,:v_cep,:v_cod_cidade,:v_ddd,:v_fone,:v_situacao,:v_email)"
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        Unload frmAguardar
        If vErro <> "" Then
            vVB_Generica_001.ProcessaErro (vErro & ". Erro Inclus�o.")
            Call cmdLimpar_Click
            Exit Sub
        End If
        
        Call vVB_Generica_001.Informar("Inclus�o efetuada com sucesso.")
        Call cmdLimpar_Click
    
    End If

End Sub

Public Sub cmdLimpar_Click()
      
    txtCodAdv.Enabled = True
    txtNome.Enabled = True
    txtCodAdv = ""
    txtNome = ""
    txtSigla = ""
    cmbSituacao.ListIndex = -1
    txtEnd = ""
    txtBairro = ""
    cmbCidade.ListIndex = -1
    txtCEP = ""
    txtDDD = ""
    txtFone = ""
    txtEmail = ""
    'txtCodAdv.SetFocus
    
    'carrega pr�x. cod.
    Call vVB_Generica_001.ExcluiBind(vBanco)
    vBanco.Parameters.Add "vCursor", 0, 3
    vBanco.Parameters("vCursor").ServerType = 102
    vBanco.Parameters("vCursor").DynasetOption = &H2&
    vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
    vSql = "PRODUCAO.PCK_CAD930.pr_con_sequence(:vCursor)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro & ". Erro ao verificar Pr�x. C�d.")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("vCursor").Value
    End If
    If IsNull(vObjOracle!seq) Then
        txtCodAdv = "0001"
    Else
        txtCodAdv = Format(vObjOracle!seq, "000")
    End If

End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub

Private Sub Form_Load()
    
    Me.Top = 0
    Me.Left = 0
    
    'carrega pr�x. cod.
    Call vVB_Generica_001.ExcluiBind(vBanco)
    vBanco.Parameters.Add "vCursor", 0, 3
    vBanco.Parameters("vCursor").ServerType = 102
    vBanco.Parameters("vCursor").DynasetOption = &H2&
    vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
    vSql = "PRODUCAO.PCK_CAD930.pr_con_sequence(:vCursor)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro & ". Erro ao verificar Pr�x. C�d.")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("vCursor").Value
    End If
    If IsNull(vObjOracle!seq) Then
        txtCodAdv = "0001"
    Else
        txtCodAdv = Format(vObjOracle!seq, "000")
    End If
    
    'carrega combo C�digo Cidade
    cmbCidade.Clear
    
    Call Aguardar
    Call vVB_Generica_001.ExcluiBind(vBanco)
    vBanco.Parameters.Add "vCursor", 0, 3
    vBanco.Parameters("vCursor").ServerType = 102
    vBanco.Parameters("vCursor").DynasetOption = &H2&
    vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
    vSql = "PRODUCAO.PCK_CAD930.PR_SEL_CID(:vCursor)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    Unload frmAguardar
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro & ". Erro ao ler Cidades.")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("vCursor").Value
    End If
    While Not vObjOracle.EOF
        cmbCidade.AddItem Format(vObjOracle!cod_CIDADE, "0000") & " - " & vObjOracle!NOME_CIDADE & " (" & vObjOracle!COD_UF & ")"
        vObjOracle.MoveNext
    Wend
    
End Sub

Public Sub txtCodAdv_LostFocus()

    If Trim(txtCodAdv) = "" Then
        Exit Sub
    End If
       
    vOpcao = "4"
    'carrega campos
    'Call Aguardar
    Call vVB_Generica_001.ExcluiBind(vBanco)
    vBanco.Parameters.Add "vCursor", 0, 3
    vBanco.Parameters("vCursor").ServerType = 102
    vBanco.Parameters("vCursor").DynasetOption = &H2&
    vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
    vBanco.Parameters.Add "v_opcao", "4", 1
    vBanco.Parameters.Add "v_cod_advogado", txtCodAdv, 1
    vBanco.Parameters.Add "v_nome", "", 1
    vBanco.Parameters.Add "v_sigla", "", 1
    vSql = "PRODUCAO.PCK_CAD930.pr_con_advogado(:vCursor,:v_opcao,:v_cod_advogado,:v_nome,:v_sigla)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    'Unload frmAguardar
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro & ". Erro ao verificar Advogados.")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("vCursor").Value
    End If
    If Not vObjOracle.EOF Then
        txtCodAdv = Format(vObjOracle!cod_advogado, "000")
        txtCodAdv.Enabled = False
        txtNome = vObjOracle!nome
        txtNome.Enabled = False
        If IsNull(vObjOracle!bairro) Then
            txtBairro = " - "
        Else
            txtBairro = vObjOracle!bairro
        End If
        
        For i = 0 To cmbCidade.ListCount - 1
            If cmbCidade.List(i) = Format(CStr(vObjOracle!cod_CIDADE), "0000") & " - " & vObjOracle!CIDADE & " (" & vObjOracle!UF & ")" Then
                cmbCidade.ListIndex = i
                Exit For
            End If
        Next i
        
        txtDDD = vObjOracle!ddd
        If IsNull(vObjOracle!email) Then
            txtEmail = " - "
        Else
            txtEmail = vObjOracle!email
        End If
        txtEnd = vObjOracle!endereco
       
        txtSigla = vObjOracle!sigla
        If vObjOracle!situacao = 0 Then
            cmbSituacao.ListIndex = 0
        Else
            cmbSituacao.ListIndex = 1
        End If
        If IsNull(vObjOracle!fone) Then
            txtFone = " - "
        Else
            txtFone = vObjOracle!fone
        End If
        txtCEP = vObjOracle!cep
        'txtSigla.SetFocus
    End If

End Sub

Private Sub txtEmail_LostFocus()

    If Trim(txtEmail) = "" Then
        Exit Sub
    End If
    
    If InStr(txtEmail, "@") = 0 Then
        Call vVB_Generica_001.Informar("Endere�o de e-mail inv�lido.")
        txtEmail = ""
        txtEmail.SetFocus
        Exit Sub
    End If

End Sub

Private Sub txtSigla_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
    
End Sub

Private Sub txtNome_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
    
End Sub

Private Sub txtEnd_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
    
End Sub

Private Sub txtBairro_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
    
End Sub

Private Sub txtCodAdv_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtCodAdv)

End Sub

Private Sub txtCEP_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtCEP)

End Sub

Private Sub txtDDD_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtDDD)

End Sub

Private Sub txtFone_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtFone)

End Sub

Private Sub txtSigla_LostFocus()

    'verifica se sigla digitada j� existe
    If (Trim(txtSigla) = "" Or txtCodAdv.Enabled = False) Then
        Exit Sub
    End If
    
    Call vVB_Generica_001.ExcluiBind(vBanco)
    vBanco.Parameters.Add "vCursor", 0, 3
    vBanco.Parameters("vCursor").ServerType = 102
    vBanco.Parameters("vCursor").DynasetOption = &H2&
    vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
    vBanco.Parameters.Add "vSigla", txtSigla, 1
    vSql = "PRODUCAO.PCK_CAD930.pr_con_sigla(:vCursor,:vSigla)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro & ". Erro ao verificar Sigla.")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("vCursor").Value
    End If
    If Not IsNull(vObjOracle!sigla) Then
        Call vVB_Generica_001.Informar("Esta Sigla j� foi usada. Por favor, digite outra.")
        txtSigla = ""
        txtSigla.SetFocus
    End If

End Sub
