--Package pck_Cad930
--Sistema: Cad930 - Cadastro de Advogado
--Analista: Renato H.	--Backup: Elaine
--Data: 17/11/03
--Dir: f:\sistemas\oracle\Cad930\Pl\pck_cad930.sql


CREATE OR REPLACE PACKAGE pck_Cad930 IS


TYPE tp_cursor IS REF CURSOR;

------------------------------------------------------------------------------------------------------------------
--CONSULTAS:
------------------------------------------------------------------------------------------------------------------

--1
PROCEDURE pr_con_sequence(var_sequence out number);
--Traz o valor do pr�ximo c�digo de advogado que ser� usado
-----------------------------------------------------------

--2
PROCEDURE pr_con_UF(pm_cursor in out tp_cursor,
					pm_erro out number,
					pm_desc_erro out varchar);
--Carrega o combo Uf com todos os codigos de Uf dispon�veis
-----------------------------------------------------------					

--3
PROCEDURE pr_con_cidade(pm_cursor in out tp_cursor,
					pm_UF in producao.cidade.cod_uf%Type,
					pm_erro out number,
					pm_desc_erro out varchar);
--Carrega o combo Cidade de acordo com a UF escolhida
-----------------------------------------------------------					

--4
PROCEDURE pr_con_advogado(pm_cursor in out tp_cursor,
					pm_opcao in number,
					pm_cod_advogado in cobranca.advogado.cod_advogado%Type,
					pm_nome in cobranca.advogado.nome%TYPE,
					pm_sigla in cobranca.advogado.sigla%TYPE,
					pm_erro out number,
					pm_desc_erro out varchar);					
--Carrega o grid de consulta de advogados
-----------------------------------------------------------		

--5
PROCEDURE pr_con_sigla(pm_cursor in out tp_cursor,
					   pm_erro out number,
					   pm_desc_erro out varchar);
--evita que duas senha iguias sejam cadastradas
-----------------------------------------------------------							   

------------------------------------------------------------------------------------------------------------------
--ALTERA��ES:
------------------------------------------------------------------------------------------------------------------

--1
PROCEDURE pr_inc_advogado(pm_cod_advogado in cobranca.advogado.cod_advogado%TYPE,
					pm_Sigla in cobranca.advogado.sigla%TYPE,
					pm_nome in cobranca.advogado.nome%TYPE,
					pm_endereco in cobranca.advogado.endereco%TYPE,
					pm_bairro in cobranca.advogado.bairro%TYPE,
					pm_cep in cobranca.advogado.cep%TYPE,
					pm_cod_cidade in cobranca.advogado.cod_cidade%TYPE,
					pm_ddd in cobranca.advogado.ddd%TYPE,
					pm_fone in cobranca.advogado.fone%TYPE,
					pm_situacao in cobranca.advogado.situacao%TYPE,
					pm_email in cobranca.advogado.email%TYPE,
					pm_erro out number,
					pm_desc_erro out varchar);
--Salva registro de um advogado;
-----------------------------------------------------------					

--2
PROCEDURE pr_alt_advogado(pm_cod_advogado in cobranca.advogado.cod_advogado%TYPE,
					pm_Sigla in cobranca.advogado.sigla%TYPE,
					pm_nome in cobranca.advogado.nome%TYPE,
					pm_endereco in cobranca.advogado.endereco%TYPE,
					pm_bairro in cobranca.advogado.bairro%TYPE,
					pm_cep in cobranca.advogado.cep%TYPE,
					pm_cod_cidade in cobranca.advogado.cod_cidade%TYPE,
					pm_ddd in cobranca.advogado.ddd%TYPE,
					pm_fone in cobranca.advogado.fone%TYPE,
					pm_situacao in cobranca.advogado.situacao%TYPE,
					pm_email in cobranca.advogado.email%TYPE,
					pm_erro out number,
					pm_desc_erro out varchar);
--Altera registro de um advogado;
-----------------------------------------------------------					




end pck_Cad930;
/
------------------------------------------------------------------------------------------------------------------




CREATE OR REPLACE PACKAGE BODY pck_Cad930 IS

--1
PROCEDURE pr_con_sequence(var_sequence out number)

IS

BEGIN
	
	select decode(max(cod_advogado), null, 1, max(cod_advogado)+1) into var_sequence from cobranca.advogado;
	--select max(cod_advogado)+1 into var_sequence from cobranca.advogado;
	
end pr_con_sequence;
-----------------------------------------------------------

--2
PROCEDURE pr_con_UF(pm_cursor in out tp_cursor,
					pm_erro out number,
					pm_desc_erro out varchar)

IS

BEGIN

	pm_erro := 0;
	pm_desc_erro := 'vazio';

	open pm_cursor for

	select distinct(cod_UF) UF from producao.cidade order by cod_uf;
	
EXCEPTION

	WHEN OTHERS THEN
	PM_ERRO := SQLCODE;
	PM_desc_erro := SQLERRM;
	
end pr_con_UF;
-----------------------------------------------------------

--3
PROCEDURE pr_con_cidade(pm_cursor in out tp_cursor,
					pm_UF in producao.cidade.cod_uf%Type,
					pm_erro out number,
					pm_desc_erro out varchar)
					
is 

begin

	pm_erro:=0;
	pm_desc_erro:='vazio';
	
	open pm_cursor for
	
	select to_char(cod_cidade, '0000') || ' - ' || nome_cidade  cidade from producao.cidade where cod_uf=pm_UF order by nome_cidade;
	
EXCEPTION

	WHEN OTHERS THEN
	PM_ERRO := SQLCODE;
	PM_desc_erro := SQLERRM;
	
end pr_con_cidade;
-----------------------------------------------------------

--4
PROCEDURE pr_con_advogado(pm_cursor in out tp_cursor,
					pm_opcao in number,
					pm_cod_advogado in cobranca.advogado.cod_advogado%Type,
					pm_nome in cobranca.advogado.nome%TYPE,
					pm_sigla in cobranca.advogado.sigla%TYPE,
					pm_erro out number,
					pm_desc_erro out varchar)					
is
begin

	pm_erro:=0;
	pm_desc_erro:='vazio';
	
	if pm_opcao=1 then    --todos os advogados
	
		open pm_cursor for
	
			select a.*, b.nome_cidade cidade, b.cod_uf UF 
			from cobranca.advogado a, producao.cidade b
			where a.cod_cidade=b.cod_cidade
			order by a.nome;
		
	end if;
	
	if pm_opcao=2 then    --advogados ativos
	
		open pm_cursor for 
		
			select a.*, b.nome_cidade cidade, b.cod_uf UF 
			from cobranca.advogado a, producao.cidade b
			where a.situacao=0 and
			a.cod_cidade=b.cod_cidade
			order by a.nome;
			
	end if;
	
	if pm_opcao=3 then    --advogados desativados
	
		open pm_cursor for
	
			select a.*, b.nome_cidade cidade, b.cod_uf UF 
			from cobranca.advogado a, producao.cidade b
			where a.situacao=9 and
			a.cod_cidade=b.cod_cidade
			order by a.nome;
		
	end if;
	
	if pm_opcao=4 then    --advogado espec�fico (por codigo de advogado)
	
		open pm_cursor for
		
			select a.*, b.nome_cidade cidade , b.cod_uf UF from cobranca.advogado a, producao.cidade b
			where cod_advogado=pm_cod_advogado and
			a.cod_cidade=b.cod_cidade;
		
	end if;
	
	if pm_opcao=5 then    --advogado espec�fico (por nome)
		
			open pm_cursor for
			
				select a.*, b.nome_cidade cidade, b.cod_uf UF from cobranca.advogado a, producao.cidade b
				where nome like pm_nome and 
				a.cod_cidade=b.cod_cidade
				order by a.nome;

				
			
	end if;
	
	if pm_opcao=6 then    --advogado espec�fico (por sigla)
			
				open pm_cursor for
				
					select a.*, b.nome_cidade cidade, b.cod_uf UF from cobranca.advogado a, producao.cidade b
					where sigla like pm_sigla and 
					a.cod_cidade=b.cod_cidade
					order by a.sigla;
				
	end if;
	
EXCEPTION

	WHEN OTHERS THEN
	PM_ERRO := SQLCODE;
	PM_desc_erro := SQLERRM;
	
end pr_con_advogado;
-----------------------------------------------------------

--5
PROCEDURE pr_con_sigla(pm_cursor in out tp_cursor,
					   pm_erro out number,
					   pm_desc_erro out varchar)
					   
is 

begin

	pm_erro:=0;
	pm_desc_erro:='vazio';
	
	open pm_cursor for
	select sigla from cobranca.advogado;
	
EXCEPTION

	WHEN OTHERS THEN
	PM_ERRO := SQLCODE;
	PM_desc_erro := SQLERRM;
					   
end pr_con_sigla;
-----------------------------------------------------------					   

------------------------------------------------------------------------------------------------------------------
--ALTERA��ES:
------------------------------------------------------------------------------------------------------------------

--1
PROCEDURE pr_inc_advogado(pm_cod_advogado in cobranca.advogado.cod_advogado%TYPE,
					pm_Sigla in cobranca.advogado.sigla%TYPE,
					pm_nome in cobranca.advogado.nome%TYPE,
					pm_endereco in cobranca.advogado.endereco%TYPE,
					pm_bairro in cobranca.advogado.bairro%TYPE,
					pm_cep in cobranca.advogado.cep%TYPE,
					pm_cod_cidade in cobranca.advogado.cod_cidade%TYPE,
					pm_ddd in cobranca.advogado.ddd%TYPE,
					pm_fone in cobranca.advogado.fone%TYPE,
					pm_situacao in cobranca.advogado.situacao%TYPE,
					pm_email in cobranca.advogado.email%TYPE,
					pm_erro out number,
					pm_desc_erro out varchar)
					
					
is

begin

	pm_erro:=0;
	pm_desc_erro:='vazio';
	
	insert into cobranca.advogado values(cobranca.seq_advogado.nextval, pm_sigla, pm_nome, pm_endereco, pm_bairro, pm_cod_cidade,
	pm_cep, pm_ddd, pm_fone, pm_situacao, pm_email);
										 
EXCEPTION

	WHEN OTHERS THEN
	PM_ERRO := SQLCODE;
	PM_desc_erro := SQLERRM;
	
end pr_inc_advogado;
-----------------------------------------------------------					


--2
PROCEDURE pr_alt_advogado(pm_cod_advogado in cobranca.advogado.cod_advogado%TYPE,
					pm_Sigla in cobranca.advogado.sigla%TYPE,
					pm_nome in cobranca.advogado.nome%TYPE,
					pm_endereco in cobranca.advogado.endereco%TYPE,
					pm_bairro in cobranca.advogado.bairro%TYPE,
					pm_cep in cobranca.advogado.cep%TYPE,
					pm_cod_cidade in cobranca.advogado.cod_cidade%TYPE,
					pm_ddd in cobranca.advogado.ddd%TYPE,
					pm_fone in cobranca.advogado.fone%TYPE,
					pm_situacao in cobranca.advogado.situacao%TYPE,
					pm_email in cobranca.advogado.email%TYPE,
					pm_erro out number,
					pm_desc_erro out varchar)
					
					
is

begin

	pm_erro:=0;
	pm_desc_erro:='vazio';
	
	update cobranca.advogado set 
		sigla=pm_sigla,
		nome=pm_nome,
		endereco=pm_endereco,
		bairro=pm_bairro,
		cep=pm_cep,
		cod_cidade=pm_cod_cidade,
		ddd=pm_ddd,
		fone=pm_fone,
		situacao=pm_situacao,
		email=pm_email
	where cod_advogado=pm_cod_advogado;
	
										 
EXCEPTION

	WHEN OTHERS THEN
	PM_ERRO := SQLCODE;
	PM_desc_erro := SQLERRM;
	
end pr_alt_advogado;
-----------------------------------------------------------					




end pck_Cad930;
/
------------------------------------------------------------------------------------------------------------------




