VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form frmConsulta 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Consulta de Advogados"
   ClientHeight    =   6945
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9480
   Icon            =   "frmConsulta.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   463
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   632
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   6615
      Width           =   9480
      _ExtentX        =   16722
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   16669
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   810
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsulta.frx":23D2
      PICN            =   "frmConsulta.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin TabDlg.SSTab stbAdvogados 
      Height          =   5595
      Left            =   90
      TabIndex        =   3
      Top             =   945
      Width           =   9315
      _ExtentX        =   16431
      _ExtentY        =   9869
      _Version        =   393216
      Tabs            =   2
      Tab             =   1
      TabsPerRow      =   2
      TabHeight       =   794
      ShowFocusRect   =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Todos"
      TabPicture(0)   =   "frmConsulta.frx":30C8
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "frameExibir"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "mfgAdvogados"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "Espec�fico"
      TabPicture(1)   =   "frmConsulta.frx":30E4
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "cmdAlterar"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "cmdLimpar"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "Frame1"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "FrameAdv"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).ControlCount=   4
      Begin VB.Frame FrameAdv 
         Caption         =   "Advogado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   795
         Left            =   135
         TabIndex        =   9
         Top             =   585
         Width           =   5550
         Begin MSMask.MaskEdBox txtCodAdv 
            Height          =   330
            Left            =   630
            TabIndex        =   11
            Top             =   270
            Width           =   600
            _ExtentX        =   1058
            _ExtentY        =   582
            _Version        =   393216
            MaxLength       =   3
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            PromptChar      =   "�"
         End
         Begin MSMask.MaskEdBox txtNome 
            Height          =   330
            Left            =   2070
            TabIndex        =   13
            Top             =   270
            Width           =   3300
            _ExtentX        =   5821
            _ExtentY        =   582
            _Version        =   393216
            MaxLength       =   40
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            PromptChar      =   "�"
         End
         Begin VB.Label lblNome 
            Appearance      =   0  'Flat
            Caption         =   "Nome:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   1485
            TabIndex        =   12
            Top             =   315
            Width           =   570
         End
         Begin VB.Label lblCod 
            Appearance      =   0  'Flat
            Caption         =   "C�d.:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   135
            TabIndex        =   10
            Top             =   315
            Width           =   570
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Dados"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   3270
         Left            =   135
         TabIndex        =   14
         Top             =   1440
         Width           =   5550
         Begin VB.ComboBox cmbSituacao 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            ItemData        =   "frmConsulta.frx":3100
            Left            =   1080
            List            =   "frmConsulta.frx":310A
            Style           =   2  'Dropdown List
            TabIndex        =   18
            Top             =   668
            Width           =   1890
         End
         Begin VB.ComboBox cmbCidade 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            ItemData        =   "frmConsulta.frx":3129
            Left            =   1080
            List            =   "frmConsulta.frx":312B
            Style           =   2  'Dropdown List
            TabIndex        =   24
            Top             =   1935
            Width           =   3735
         End
         Begin MSMask.MaskEdBox txtBairro 
            Height          =   330
            Left            =   1080
            TabIndex        =   22
            Top             =   1530
            Width           =   2580
            _ExtentX        =   4551
            _ExtentY        =   582
            _Version        =   393216
            MaxLength       =   30
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            PromptChar      =   "�"
         End
         Begin MSMask.MaskEdBox txtEnd 
            Height          =   330
            Left            =   1080
            TabIndex        =   20
            Top             =   1125
            Width           =   4290
            _ExtentX        =   7567
            _ExtentY        =   582
            _Version        =   393216
            MaxLength       =   33
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            PromptChar      =   "�"
         End
         Begin MSMask.MaskEdBox txtCEP 
            Height          =   330
            Left            =   1080
            TabIndex        =   26
            Top             =   2385
            Width           =   1050
            _ExtentX        =   1852
            _ExtentY        =   582
            _Version        =   393216
            MaxLength       =   8
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            PromptChar      =   "�"
         End
         Begin MSMask.MaskEdBox txtDDD 
            Height          =   330
            Left            =   2745
            TabIndex        =   28
            Top             =   2385
            Width           =   555
            _ExtentX        =   979
            _ExtentY        =   582
            _Version        =   393216
            MaxLength       =   4
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            PromptChar      =   "�"
         End
         Begin MSMask.MaskEdBox txtFone 
            Height          =   330
            Left            =   3960
            TabIndex        =   30
            Top             =   2385
            Width           =   1410
            _ExtentX        =   2487
            _ExtentY        =   582
            _Version        =   393216
            MaxLength       =   9
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            PromptChar      =   "�"
         End
         Begin MSMask.MaskEdBox txtEmail 
            Height          =   330
            Left            =   1080
            TabIndex        =   32
            Top             =   2790
            Width           =   4290
            _ExtentX        =   7567
            _ExtentY        =   582
            _Version        =   393216
            MaxLength       =   60
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            PromptChar      =   "�"
         End
         Begin MSMask.MaskEdBox txtSigla 
            Height          =   330
            Left            =   1080
            TabIndex        =   16
            Top             =   270
            Width           =   3165
            _ExtentX        =   5583
            _ExtentY        =   582
            _Version        =   393216
            MaxLength       =   25
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            PromptChar      =   "�"
         End
         Begin VB.Label lblSigla 
            Appearance      =   0  'Flat
            Caption         =   "Sigla:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   135
            TabIndex        =   15
            Top             =   315
            Width           =   435
         End
         Begin VB.Label lblEmail 
            Appearance      =   0  'Flat
            Caption         =   "E-mail:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   135
            TabIndex        =   31
            Top             =   2835
            Width           =   705
         End
         Begin VB.Label lblFone 
            Appearance      =   0  'Flat
            Caption         =   "Fone:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   3420
            TabIndex        =   29
            Top             =   2430
            Width           =   480
         End
         Begin VB.Label lblCEP 
            Appearance      =   0  'Flat
            Caption         =   "CEP:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   135
            TabIndex        =   25
            Top             =   2430
            Width           =   390
         End
         Begin VB.Label lblDDD 
            Appearance      =   0  'Flat
            Caption         =   "DDD:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   2250
            TabIndex        =   27
            Top             =   2430
            Width           =   435
         End
         Begin VB.Label lblCidade 
            Appearance      =   0  'Flat
            Caption         =   "Cidade:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   135
            TabIndex        =   23
            Top             =   2025
            Width           =   675
         End
         Begin VB.Label lblBairro 
            Appearance      =   0  'Flat
            Caption         =   "Bairro:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   135
            TabIndex        =   21
            Top             =   1575
            Width           =   615
         End
         Begin VB.Label lblEnd 
            Appearance      =   0  'Flat
            Caption         =   "Endere�o:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   135
            TabIndex        =   19
            Top             =   1170
            Width           =   975
         End
         Begin VB.Label lblSituacao 
            Appearance      =   0  'Flat
            Caption         =   "Situa��o:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   135
            TabIndex        =   17
            Top             =   720
            Width           =   795
         End
      End
      Begin VB.Frame frameExibir 
         Caption         =   "Exibir"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   1230
         Left            =   -74865
         TabIndex        =   4
         Top             =   540
         Width           =   2310
         Begin VB.OptionButton optDesativados 
            Appearance      =   0  'Flat
            Caption         =   "Desativados"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   135
            TabIndex        =   7
            Top             =   900
            Width           =   1500
         End
         Begin VB.OptionButton optAtivos 
            Appearance      =   0  'Flat
            Caption         =   "Ativos"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   135
            TabIndex        =   6
            Top             =   585
            Width           =   1050
         End
         Begin VB.OptionButton optTodos 
            Appearance      =   0  'Flat
            Caption         =   "Todos"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   135
            TabIndex        =   5
            Top             =   270
            Width           =   1050
         End
      End
      Begin MSFlexGridLib.MSFlexGrid mfgAdvogados 
         Height          =   3575
         Left            =   -74865
         TabIndex        =   8
         Top             =   1890
         Width           =   9045
         _ExtentX        =   15954
         _ExtentY        =   6297
         _Version        =   393216
         FixedCols       =   0
         BackColorBkg    =   -2147483633
         AllowBigSelection=   0   'False
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin Bot�o.cmd cmdLimpar 
         Height          =   690
         Left            =   135
         TabIndex        =   33
         TabStop         =   0   'False
         ToolTipText     =   "Limpar Campos"
         Top             =   4770
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmConsulta.frx":312D
         PICN            =   "frmConsulta.frx":3149
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdAlterar 
         Height          =   690
         Left            =   900
         TabIndex        =   34
         TabStop         =   0   'False
         ToolTipText     =   "Alterar"
         Top             =   4770
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmConsulta.frx":3E23
         PICN            =   "frmConsulta.frx":3E3F
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
   End
End
Attribute VB_Name = "frmConsulta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public vOpcao As String
Public vSigla As String
Public vNome As String

Private Sub cmdAlterar_Click()

    If Trim(txtCodAdv) = "" Then
        Call vVB_Generica_001.Informar("Informe o C�d. do Advogado para altera��o.")
        txtCodAdv.SetFocus
        Exit Sub
    End If

    Call frmCadastro.cmdLimpar_Click
    frmCadastro.txtCodAdv = txtCodAdv
    Call frmCadastro.txtCodAdv_LostFocus
    frmCadastro.Show
    
    Unload Me

End Sub

Private Sub cmdLimpar_Click()

    txtCodAdv.Enabled = True
    txtNome.Enabled = True
    txtSigla.Enabled = True
    cmbSituacao.Enabled = True
    txtEnd.Enabled = True
    txtBairro.Enabled = True
    cmbCidade.Enabled = True
    txtCEP.Enabled = True
    txtDDD.Enabled = True
    txtFone.Enabled = True
    txtEmail.Enabled = True
    txtCodAdv = ""
    txtNome = ""
    txtSigla = ""
    cmbSituacao.ListIndex = -1
    txtEnd = ""
    txtBairro = ""
    cmbCidade.ListIndex = -1
    txtCEP = ""
    txtDDD = ""
    txtFone = ""
    txtEmail = ""
    txtCodAdv.SetFocus
    vOpcao = 1

End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub

Private Sub Form_Load()
    
    Me.Top = 0
    Me.Left = 0
    
    optTodos.Value = True
    
    'define grid
    mfgAdvogados.Rows = 2
    mfgAdvogados.Cols = 12
    mfgAdvogados.Row = 1
    
    'limpa linha
    For i = 0 To mfgAdvogados.Cols - 1
        mfgAdvogados.Col = i
        mfgAdvogados.Text = ""
    Next i
    
    'cabe�alho e alinhamento coluna
    mfgAdvogados.Row = 0
    
    mfgAdvogados.Col = 0
    mfgAdvogados.Text = "Cod."
    mfgAdvogados.FixedAlignment(0) = 4
    mfgAdvogados.ColAlignment(0) = 4
    mfgAdvogados.ColWidth(0) = 600
    
    mfgAdvogados.Col = 1
    mfgAdvogados.Text = "Sigla"
    mfgAdvogados.FixedAlignment(1) = 4
    mfgAdvogados.ColAlignment(1) = 1
    mfgAdvogados.ColWidth(1) = 2000
    
    mfgAdvogados.Col = 2
    mfgAdvogados.Text = "Nome do Advogado"
    mfgAdvogados.FixedAlignment(2) = 4
    mfgAdvogados.ColAlignment(2) = 1
    mfgAdvogados.ColWidth(2) = 4000
    
    mfgAdvogados.Col = 3
    mfgAdvogados.Text = "Endere�o"
    mfgAdvogados.FixedAlignment(3) = 4
    mfgAdvogados.ColAlignment(3) = 1
    mfgAdvogados.ColWidth(3) = 4000

    mfgAdvogados.Col = 4
    mfgAdvogados.Text = "Bairro"
    mfgAdvogados.FixedAlignment(4) = 4
    mfgAdvogados.ColAlignment(4) = 1
    mfgAdvogados.ColWidth(4) = 1600

    mfgAdvogados.Col = 5
    mfgAdvogados.Text = "UF"
    mfgAdvogados.FixedAlignment(5) = 4
    mfgAdvogados.ColAlignment(5) = 4
    mfgAdvogados.ColWidth(5) = 500

    mfgAdvogados.Col = 6
    mfgAdvogados.Text = "Cidade"
    mfgAdvogados.FixedAlignment(6) = 4
    mfgAdvogados.ColAlignment(6) = 1
    mfgAdvogados.ColWidth(6) = 2600
    
    mfgAdvogados.Col = 7
    mfgAdvogados.Text = "CEP"
    mfgAdvogados.FixedAlignment(7) = 4
    mfgAdvogados.ColAlignment(7) = 1
    mfgAdvogados.ColWidth(7) = 1000
    
    mfgAdvogados.Col = 8
    mfgAdvogados.Text = "DDD"
    mfgAdvogados.FixedAlignment(8) = 4
    mfgAdvogados.ColAlignment(8) = 4
    mfgAdvogados.ColWidth(8) = 500
    
    mfgAdvogados.Col = 9
    mfgAdvogados.Text = "Telefone"
    mfgAdvogados.FixedAlignment(9) = 4
    mfgAdvogados.ColAlignment(9) = 1
    mfgAdvogados.ColWidth(9) = 1300
    
    mfgAdvogados.Col = 10
    mfgAdvogados.Text = "Situa��o"
    mfgAdvogados.FixedAlignment(10) = 4
    mfgAdvogados.ColAlignment(10) = 1
    mfgAdvogados.ColWidth(10) = 1600
    
    mfgAdvogados.Col = 11
    mfgAdvogados.Text = "E-mail"
    mfgAdvogados.FixedAlignment(11) = 4
    mfgAdvogados.ColAlignment(11) = 1
    mfgAdvogados.ColWidth(11) = 3200
    
    'atualiza tela
    vOpcao = "1"
    Call Carrega_Grid
    
    stbAdvogados.Tab = 0
    
    'carrega combo C�digo Cidade
    cmbCidade.Clear
    
    'Call Aguardar
    Call vVB_Generica_001.ExcluiBind(vBanco)
    vBanco.Parameters.Add "vCursor", 0, 3
    vBanco.Parameters("vCursor").ServerType = 102
    vBanco.Parameters("vCursor").DynasetOption = &H2&
    vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
    vSql = "PRODUCAO.PCK_CAD930.PR_SEL_CID(:vCursor)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    'Unload frmAguardar
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro & ". Erro ao ler Cidades.")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("vCursor").Value
    End If
    While Not vObjOracle.EOF
        cmbCidade.AddItem Format(vObjOracle!cod_CIDADE, "0000") & " - " & vObjOracle!NOME_CIDADE & " (" & vObjOracle!COD_UF & ")"
        vObjOracle.MoveNext
    Wend
    
End Sub

Public Sub Carrega_Grid()

    'carrega grid
    Call Aguardar
    Call vVB_Generica_001.ExcluiBind(vBanco)
    vBanco.Parameters.Add "vCursor", 0, 3
    vBanco.Parameters("vCursor").ServerType = 102
    vBanco.Parameters("vCursor").DynasetOption = &H2&
    vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
    If vOpcao = "1" Then
        vBanco.Parameters.Add "v_opcao", "1", 1
        vBanco.Parameters.Add "v_cod_advogado", "", 1
        vBanco.Parameters.Add "v_nome", "", 1
        vBanco.Parameters.Add "v_sigla", "", 1
    ElseIf vOpcao = "2" Then
        vBanco.Parameters.Add "v_opcao", "2", 1
        vBanco.Parameters.Add "v_cod_advogado", "", 1
        vBanco.Parameters.Add "v_nome", "", 1
        vBanco.Parameters.Add "v_sigla", "", 1
    ElseIf vOpcao = "3" Then
        vBanco.Parameters.Add "v_opcao", "3", 1
        vBanco.Parameters.Add "v_cod_advogado", "", 1
        vBanco.Parameters.Add "v_nome", "", 1
        vBanco.Parameters.Add "v_sigla", "", 1
    ElseIf vOpcao = "5" Then
        vBanco.Parameters.Add "v_opcao", "5", 1
        vBanco.Parameters.Add "v_cod_advogado", "", 1
        vBanco.Parameters.Add "v_nome", "%" & UCase(vNome) & "%", 1
        vBanco.Parameters.Add "v_sigla", "", 1
    ElseIf vOpcao = "6" Then
        vBanco.Parameters.Add "v_opcao", "6", 1
        vBanco.Parameters.Add "v_cod_advogado", "", 1
        vBanco.Parameters.Add "v_nome", "", 1
        vBanco.Parameters.Add "v_sigla", "%" & UCase(vSigla) & "%", 1
    End If
    vSql = "PRODUCAO.PCK_CAD930.pr_con_advogado(:vCursor,:v_opcao,:v_cod_advogado,:v_nome,:v_sigla)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    Unload frmAguardar
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro & ". Erro ao verificar Advogados.")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("vCursor").Value
    End If
    If vObjOracle.EOF Then
        'prepara grid
        mfgAdvogados.Rows = 2
        mfgAdvogados.Cols = 12
        mfgAdvogados.Row = 1
        For i = 0 To mfgAdvogados.Cols - 1
            mfgAdvogados.Col = i
            mfgAdvogados.Text = ""
        Next i
        Call vVB_Generica_001.Informar("N�o h� advogados cadastrados.")
    Else
        'prepara grid
        mfgAdvogados.Rows = 2
        mfgAdvogados.Cols = 12
        mfgAdvogados.Row = 1
        For i = 0 To mfgAdvogados.Cols - 1
            mfgAdvogados.Col = i
            mfgAdvogados.Text = ""
        Next i
        'carrega dados do banco
        While Not vObjOracle.EOF
            'adiciona linha
            If mfgAdvogados.Rows = 2 Then
                mfgAdvogados.Col = 0
                mfgAdvogados.Row = 1
                If mfgAdvogados.Text <> "" Then
                    mfgAdvogados.Rows = mfgAdvogados.Rows + 1
                End If
            Else
                mfgAdvogados.Rows = mfgAdvogados.Rows + 1
            End If
            'escreve os dados
            mfgAdvogados.Row = mfgAdvogados.Rows - 1
            mfgAdvogados.Col = 0
            mfgAdvogados.Text = Format(vObjOracle!cod_advogado, "000")
            mfgAdvogados.Col = 1
            mfgAdvogados.Text = vObjOracle!sigla
            mfgAdvogados.Col = 2
            mfgAdvogados.Text = vObjOracle!nome
            mfgAdvogados.Col = 3
            mfgAdvogados.Text = vObjOracle!endereco
            mfgAdvogados.Col = 4
            If IsNull(vObjOracle!bairro) Then
                mfgAdvogados.Text = "-"
            Else
                mfgAdvogados.Text = vObjOracle!bairro
            End If
            mfgAdvogados.Col = 5
            mfgAdvogados.Text = vObjOracle!UF
            mfgAdvogados.Col = 6
            mfgAdvogados.Text = Format(CStr(vObjOracle!cod_CIDADE), "0000") & " - " & CStr(vObjOracle!CIDADE)
            mfgAdvogados.Col = 7
            mfgAdvogados.Text = vObjOracle!cep
            mfgAdvogados.Col = 8
            If IsNull(vObjOracle!ddd) Then
                mfgAdvogados.Text = " - "
            Else
                mfgAdvogados.Text = vObjOracle!ddd
            End If
            mfgAdvogados.Col = 9
            If IsNull(vObjOracle!fone) Then
                mfgAdvogados.Text = " - "
            Else
                mfgAdvogados.Text = vObjOracle!fone
            End If
            mfgAdvogados.Col = 10
            If vObjOracle!situacao = 0 Then
                mfgAdvogados.Text = "0 - Ativo"
            Else
                mfgAdvogados.Text = "9 - Desativado"
            End If
            mfgAdvogados.Col = 11
            If IsNull(vObjOracle!email) Then
                mfgAdvogados.Text = " - "
            Else
                mfgAdvogados.Text = vObjOracle!email
            End If
            'pr�ximo registro
            vObjOracle.MoveNext
        Wend
    End If
    
End Sub

Private Sub mfgAdvogados_DblClick()

    mfgAdvogados.Col = 0
    If Trim(mfgAdvogados) = "" Then
        Call vVB_Generica_001.Informar("N�o h� advogado selecionado.")
        mfgAdvogados.SetFocus
        Exit Sub
    End If

    Call frmCadastro.cmdLimpar_Click
    frmCadastro.txtCodAdv = mfgAdvogados
    Call frmCadastro.txtCodAdv_LostFocus
    frmCadastro.Show
    
    Unload Me

End Sub

Private Sub optAtivos_Click()

    vOpcao = "2"
    Call Carrega_Grid

End Sub

Private Sub optDesativados_Click()

    vOpcao = "3"
    Call Carrega_Grid

End Sub

Private Sub optTodos_Click()

    vOpcao = "1"
    Call Carrega_Grid

End Sub

Private Sub stbAdvogados_Click(PreviousTab As Integer)

    If stbAdvogados.Tab = 0 Then
        If vOpcao <> 5 Then
            optTodos.Value = True
        End If
        stbBarra.Panels(1) = "Duplo click na linha para alterar os dados."
    ElseIf stbAdvogados.Tab = 1 Then
        Call cmdLimpar_Click
        stbBarra.Panels(1) = ""
    End If

End Sub

Private Sub txtCodAdv_LostFocus()

    If Trim(txtCodAdv) = "" Then
        Exit Sub
    End If
   
    vOpcao = "4"
    'carrega campos
    'Call Aguardar
    Call vVB_Generica_001.ExcluiBind(vBanco)
    vBanco.Parameters.Add "vCursor", 0, 3
    vBanco.Parameters("vCursor").ServerType = 102
    vBanco.Parameters("vCursor").DynasetOption = &H2&
    vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
    vBanco.Parameters.Add "v_opcao", "4", 1
    vBanco.Parameters.Add "v_cod_advogado", txtCodAdv, 1
    vBanco.Parameters.Add "v_nome", "", 1
    vBanco.Parameters.Add "v_sigla", "", 1
    vSql = "PRODUCAO.PCK_CAD930.pr_con_advogado(:vCursor,:v_opcao,:v_cod_advogado,:v_nome,:v_sigla)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    'Unload frmAguardar
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro & ". Erro ao verificar Advogados.")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("vCursor").Value
    End If
    If vObjOracle.EOF Then
        Call vVB_Generica_001.Informar("Advogado informado inexistente.")
        Call cmdLimpar_Click
    ElseIf vObjOracle.RecordCount = 1 Then
        txtCodAdv = Format(vObjOracle!cod_advogado, "000")
        txtCodAdv.Enabled = False
        txtNome = vObjOracle!nome
        txtNome.Enabled = False
        If IsNull(vObjOracle!bairro) Then
            txtBairro = " - "
        Else
            txtBairro = vObjOracle!bairro
        End If
        For i = 0 To cmbCidade.ListCount - 1
            If cmbCidade.List(i) = Format(CStr(vObjOracle!cod_CIDADE), "0000") & " - " & vObjOracle!CIDADE & " (" & vObjOracle!UF & ")" Then
                cmbCidade.ListIndex = i
                Exit For
            End If
        Next i
        txtDDD = vObjOracle!ddd
        If IsNull(vObjOracle!email) Then
            txtEmail = " - "
        Else
            txtEmail = vObjOracle!email
        End If
        txtEnd = vObjOracle!endereco
        txtSigla = vObjOracle!sigla
        If vObjOracle!situacao = 0 Then
            cmbSituacao.ListIndex = 0
        Else
            cmbSituacao.ListIndex = 1
        End If
        If IsNull(vObjOracle!fone) Then
            txtFone = " - "
        Else
            txtFone = vObjOracle!fone
        End If
        txtCEP = vObjOracle!cep
        txtSigla.Enabled = False
        cmbSituacao.Enabled = False
        txtEnd.Enabled = False
        txtBairro.Enabled = False
        cmbCidade.Enabled = False
        txtCEP.Enabled = False
        txtDDD.Enabled = False
        txtFone.Enabled = False
        txtEmail.Enabled = False
        'txtSigla.SetFocus
    End If
    
End Sub

Private Sub txtEmail_LostFocus()

    If Trim(txtEmail) = "" Then
        Exit Sub
    End If
    
    If InStr(txtEmail, "@") = 0 Then
        Call vVB_Generica_001.Informar("Endere�o de e-mail inv�lido.")
        txtEmail = ""
        txtEmail.SetFocus
        Exit Sub
    End If

End Sub

Private Sub txtNome_LostFocus()

    If Trim(txtNome) = "" Then
        Exit Sub
    End If
   
    'carrega campos
    'Call Aguardar
    Call vVB_Generica_001.ExcluiBind(vBanco)
    vBanco.Parameters.Add "vCursor", 0, 3
    vBanco.Parameters("vCursor").ServerType = 102
    vBanco.Parameters("vCursor").DynasetOption = &H2&
    vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
    vBanco.Parameters.Add "v_opcao", "5", 1
    vBanco.Parameters.Add "v_cod_advogado", "", 1
    vBanco.Parameters.Add "v_nome", "%" & UCase(txtNome) & "%", 1
    vBanco.Parameters.Add "v_sigla", "", 1
    vSql = "PRODUCAO.PCK_CAD930.pr_con_advogado(:vCursor,:v_opcao,:v_cod_advogado,:v_nome,:v_sigla)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    'Unload frmAguardar
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro & ". Erro ao verificar Advogados.")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("vCursor").Value
    End If
    If vObjOracle.EOF Then
        Call vVB_Generica_001.Informar("Advogado informado inexistente.")
        Call cmdLimpar_Click
    ElseIf vObjOracle.RecordCount = 1 Then
        txtCodAdv = Format(vObjOracle!cod_advogado, "000")
        txtCodAdv.Enabled = False
        txtNome = vObjOracle!nome
        txtNome.Enabled = False
        If IsNull(vObjOracle!bairro) Then
            txtBairro = " - "
        Else
            txtBairro = vObjOracle!bairro
        End If
        For i = 0 To cmbCidade.ListCount - 1
            If cmbCidade.List(i) = Format(CStr(vObjOracle!cod_CIDADE), "0000") & " - " & vObjOracle!CIDADE & " (" & vObjOracle!UF & ")" Then
                cmbCidade.ListIndex = i
                Exit For
            End If
        Next i
        txtDDD = vObjOracle!ddd
        If IsNull(vObjOracle!email) Then
            txtEmail = " - "
        Else
            txtEmail = vObjOracle!email
        End If
        txtEnd = vObjOracle!endereco
       
        txtSigla = vObjOracle!sigla
        If vObjOracle!situacao = 0 Then
            cmbSituacao.ListIndex = 0
        Else
            cmbSituacao.ListIndex = 1
        End If
        If IsNull(vObjOracle!fone) Then
            txtFone = " - "
        Else
            txtFone = vObjOracle!fone
        End If
        txtCEP = vObjOracle!cep
        txtSigla.Enabled = False
        cmbSituacao.Enabled = False
        txtEnd.Enabled = False
        txtBairro.Enabled = False
        cmbCidade.Enabled = False
        txtCEP.Enabled = False
        txtDDD.Enabled = False
        txtFone.Enabled = False
        txtEmail.Enabled = False
        'txtSigla.SetFocus
    ElseIf vObjOracle.RecordCount > 1 Then
        Call vVB_Generica_001.Informar("Foi encontrado mais de um registro.")
        vOpcao = "5"
        vNome = UCase(txtNome)
        Call Carrega_Grid
        optTodos.Value = False
        optAtivos.Value = False
        optDesativados.Value = False
        stbAdvogados.Tab = 0
    End If

End Sub

Private Sub txtSigla_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
    
End Sub

Private Sub txtNome_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
    
End Sub

Private Sub txtEnd_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
    
End Sub

Private Sub txtBairro_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
    
End Sub

Private Sub txtCodAdv_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtCodAdv)

End Sub

Private Sub txtCEP_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtCEP)

End Sub

Private Sub txtDDD_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtDDD)

End Sub

Private Sub txtFone_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtFone)

End Sub

