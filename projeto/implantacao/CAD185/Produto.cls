VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Produto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private pCodDPK As Long
Private pCodFabr As String
Private pDescricao As String
Private pPrecoVenda As Double
Private pPrecoOF As Double
Private pPrecoSP As Double
Private pPrecoVDR As Double
Private pCategoria As Integer
Private pSigla As String
Private pCodForn As Long
Private pDivisao As String

Private pCodUnidade As String
Private pCodLinha As String
Private pCodGrupo As String
Private pCodSubgrupo As String
Private pDescGrupo As String
Private pDescSubGrupo As String
Private pDescLinha As String

Private pPrecoCompra As Double

Public Property Get PrecoCompra() As Double
    PrecoCompra = pPrecoCompra
End Property

Public Property Get CodUnidade() As String
    CodUnidade = pCodUnidade
End Property

Public Property Get CodLinha() As String
    CodLinha = pCodLinha
End Property

Public Property Get CodGrupo() As String
    CodGrupo = pCodGrupo
End Property

Public Property Get CodSubgrupo() As String
    CodSubgrupo = pCodSubgrupo
End Property

Public Property Get DescGrupo() As String
    DescGrupo = pDescGrupo
End Property

Public Property Get DescSubGrupo() As String
    DescSubGrupo = pDescSubGrupo
End Property

Public Property Get DescLinha() As String
    DescLinha = pDescLinha
End Property




Public Property Get CodDPK() As Long
    CodDPK = pCodDPK
End Property

Public Property Get CodFabr() As String
    CodFabr = pCodFabr
End Property

Public Property Get Descricao() As String
    Descricao = pDescricao
End Property

Public Property Get PrecoVenda() As Double
    PrecoVenda = pPrecoVenda
End Property

Public Property Get PrecoOF() As Double
    PrecoOF = pPrecoOF
End Property

Public Property Get PrecoSP() As Double
    PrecoSP = pPrecoSP
End Property

Public Property Get PrecoVDR() As Double
    PrecoVDR = pPrecoVDR
End Property

Public Property Get Categoria() As Integer
    Categoria = pCategoria
End Property

Public Property Get Sigla() As String
    Sigla = pSigla
End Property

Public Property Get CodForn() As Long
  CodForn = pCodForn
End Property

Public Property Get Divisao() As String
    Divisao = pDivisao
End Property

Public Function BuscarCodFabr(intCodLoja As Integer, strCodFabr As String, lngCodFornec As Long) As Boolean
    'Selecionar os c�digos de f�brica que dever�o entrar na consulta
    Set OraParameters = vBanco.Parameters
    Criar_Cursor OraParameters, "vCursor"
    
    OraParameters.Remove "COD_LOJA":        OraParameters.Add "COD_LOJA", intCodLoja, 1
    OraParameters.Remove "COD_FABRICA":     OraParameters.Add "COD_FABRICA", strCodFabr, 1
    OraParameters.Remove "COD_FORNECEDOR":  OraParameters.Add "COD_FORNECEDOR", lngCodFornec, 1
    
    OraParameters.Remove "ERRO":            OraParameters.Add "ERRO", 0, 2
    
    vSql = "PRODUCAO.PCK_CAD180.PR_PROD_FABR(:COD_LOJA,:COD_FABRICA,:COD_FORNECEDOR,:ERRO,:vCursor)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
        Exit Function
    Else
        Set vObjOracle = OraParameters("vCursor").Value
    End If

    If vObjOracle.EOF Then
        Call vVB_Generica_001.Informar("Produto n�o encontrado!")
        BuscarCodFabr = False
        Screen.MousePointer = 0
        Exit Function
    End If
    
    pCodDPK = vObjOracle("COD_DPK")
    pDescricao = vObjOracle("DESC_ITEM")
        
    If Not BuscaInfosDPK(pCodDPK) Then
      BuscarCodFabr = False
    Else
      BuscarCodFabr = True
    End If
        
End Function


Public Function ListarCDSCodFabr(strCodFabr As String, lngCodFornec As Long) As Object
    'Selecionar os c�digos de f�brica que dever�o entrar na consulta
    Set OraParameters = vBanco.Parameters
    Criar_Cursor OraParameters, "vCursor"
    
    OraParameters.Remove "COD_LOJA":        OraParameters.Add "COD_LOJA", 0, 1
    OraParameters.Remove "COD_FABRICA":     OraParameters.Add "COD_FABRICA", strCodFabr, 1
    OraParameters.Remove "COD_FORNECEDOR":  OraParameters.Add "COD_FORNECEDOR", lngCodFornec, 1
    
    OraParameters.Remove "ERRO":            OraParameters.Add "ERRO", 0, 2
    
    vSql = "PRODUCAO.PCK_CAD180.PR_PROD_FABR(:COD_LOJA,:COD_FABRICA,:COD_FORNECEDOR,:ERRO,:vCursor)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
        Exit Function
    Else
        Set vObjOracle = OraParameters("vCursor").Value
    End If

    If vObjOracle.EOF Then
        Call vVB_Generica_001.Informar("Produto n�o encontrado em nenhum CD!")
        Set ListarCDSCodFabr = Nothing
        Screen.MousePointer = 0
        Exit Function
    End If
    
    pCodDPK = vObjOracle("COD_DPK")
    pDescricao = vObjOracle("DESC_ITEM")
    
    Set ListarCDSCodFabr = vObjOracle
    
    
End Function

Public Function BuscarPreco(lngCOD_DPK As Long, intCodLoja As Integer) As Double
    'Buscar Produto por COD_DPK antes
    
    'Buscar Precos para o produto
    Set OraParameters = vBanco.Parameters
        
    OraParameters.Remove "COD_DPK":       OraParameters.Add "COD_DPK", lngCOD_DPK, 1
    OraParameters.Remove "COD_LOJA":      OraParameters.Add "COD_LOJA", intCodLoja, 1
    OraParameters.Remove "PRECO_VENDA":   OraParameters.Add "PRECO_VENDA", 0, 2
    OraParameters.Remove "PRECO_OF":      OraParameters.Add "PRECO_OF", 0, 2
    OraParameters.Remove "PRECO_SP":      OraParameters.Add "PRECO_SP", 0, 2
    OraParameters.Remove "VDR":           OraParameters.Add "VDR", 0, 2
    
    OraParameters.Remove "ERRO":          OraParameters.Add "ERRO", 0, 2
    
    vSql = "PRODUCAO.PCK_CAD180.PR_PRECO_ITEM(:COD_DPK, :COD_LOJA, :PRECO_VENDA, :PRECO_OF, :PRECO_SP, :VDR, :ERRO)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
        BuscarPreco = False
        Exit Function
    End If

    If OraParameters("ERRO").Value <> 0 Then
        Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
        BuscarPreco = False
        Exit Function
    End If
    
    pPrecoVenda = OraParameters("PRECO_VENDA").Value
    pPrecoOF = OraParameters("PRECO_OF").Value
    pPrecoSP = OraParameters("PRECO_SP").Value
    pPrecoVDR = OraParameters("VDR").Value
    
    BuscarPreco = True
    
End Function


Public Function BuscarCategoria(lngCOD_DPK As Long, intCodLoja As Integer) As Double
    'Buscar Produto por COD_DPK antes
    
    'Buscar Precos para o produto
    Set OraParameters = vBanco.Parameters
        
    OraParameters.Remove "COD_DPK":       OraParameters.Add "COD_DPK", lngCOD_DPK, 1
    OraParameters.Remove "COD_LOJA":      OraParameters.Add "COD_LOJA", intCodLoja, 1
    OraParameters.Remove "CATEGORIA":     OraParameters.Add "CATEGORIA", 0, 2
    
    OraParameters.Remove "ERRO":          OraParameters.Add "ERRO", 0, 2
    
    vSql = "PRODUCAO.PCK_CAD180.PR_CATEG_ITEM(:COD_DPK, :COD_LOJA, :CATEGORIA, :ERRO)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
        BuscarCategoria = False
        Exit Function
    End If

    If OraParameters("ERRO").Value <> 0 Then
        Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
        BuscarCategoria = False
        Exit Function
    End If
    
    pCategoria = OraParameters("CATEGORIA").Value
    
    BuscarCategoria = True
    
End Function

Public Function VerificaVDR(lngCOD_DPK As Long) As Boolean
    Set OraParameters = vBanco.Parameters
        
    OraParameters.Remove "COD_DPK":   OraParameters.Add "COD_DPK", lngCOD_DPK, 1
    OraParameters.Remove "VDR":       OraParameters.Add "VDR", Null, 2
    
    OraParameters.Remove "ERRO":      OraParameters.Add "ERRO", 0, 2
    
    vSql = "PRODUCAO.PCK_CAD180.PR_VDR_ITEM (:COD_DPK, :VDR, :ERRO)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
        VerificaVDR = False
        Exit Function
    End If

    If OraParameters("ERRO").Value <> 0 Then
        Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
        VerificaVDR = False
        Exit Function
    End If
    
    VerificaVDR = (Trim(OraParameters("VDR").Value) = "S")
    
End Function

Public Function BuscarCodDPK(lngCOD_DPK As Long, intCodLoja As Integer) As Boolean
    
    'Selecionar os c�digos de f�brica que dever�o entrar na consulta
    Set OraParameters = vBanco.Parameters
    Criar_Cursor OraParameters, "vCursor"
    
    OraParameters.Remove "COD_DPK":         OraParameters.Add "COD_DPK", lngCOD_DPK, 1
    OraParameters.Remove "COD_LOJA":        OraParameters.Add "COD_LOJA", intCodLoja, 1
    
    OraParameters.Remove "ERRO":            OraParameters.Add "ERRO", 0, 2
    
    vSql = "PRODUCAO.PCK_CAD180.PR_PROD_DPK(:COD_DPK, :COD_LOJA,:ERRO,:vCursor)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
        BuscarCodDPK = False
        Exit Function
    Else
        Set vObjOracle = OraParameters("vCursor").Value
    End If

    If vObjOracle.EOF Then
        Call vVB_Generica_001.Informar("Produto n�o encontrado!")
        BuscarCodDPK = False
        Screen.MousePointer = 0
        Exit Function
    End If
    
    pCodDPK = lngCOD_DPK
    pSigla = vObjOracle("SIGLA")
    pCodForn = vObjOracle("CDFORN")
    pDivisao = vObjOracle("DIVISAO")
    pCodFabr = vObjOracle("CDFABR")
    pDescricao = vObjOracle("DESC_ITEM")
        
    If Not BuscaInfosDPK(pCodDPK) Then
      BuscarCodDPK = False
    Else
      BuscarCodDPK = True
    End If
    
End Function


Private Function BuscaInfosDPK(lngCOD_DPK As Long) As Boolean

    'Selecionar os c�digos de f�brica que dever�o entrar na consulta
    Set OraParameters = vBanco.Parameters
    Criar_Cursor OraParameters, "vCursor"
    
    OraParameters.Remove "COD_DPK":         OraParameters.Add "COD_DPK", lngCOD_DPK, 1
    
    OraParameters.Remove "ERRO":            OraParameters.Add "ERRO", 0, 2
    
    vSql = "PRODUCAO.PCK_CAD185.PR_INF_PROD_DPK(:COD_DPK, :ERRO,:vCursor)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
        BuscaInfosDPK = False
        Exit Function
    Else
        Set vObjOracle = OraParameters("vCursor").Value
    End If

    If vObjOracle.EOF Then
        Call vVB_Generica_001.Informar("Produto n�o encontrado!")
        BuscaInfosDPK = False
        Screen.MousePointer = 0
        Exit Function
    End If
    
    pCodUnidade = vObjOracle("COD_UNIDADE")
    pCodLinha = vObjOracle("CDLIN")
    pCodGrupo = vObjOracle("CDGRU")
    pCodSubgrupo = vObjOracle("CDSUB")
    pDescGrupo = vObjOracle("DESC_GRUPO")
    pDescSubGrupo = vObjOracle("DESC_SUBGRUPO")
    pDescLinha = vObjOracle("DESC_LINHA")
    
    BuscaInfosDPK = True

End Function

Public Function BuscaPrecoCompra(lngCOD_DPK As Long) As Boolean
    
    'Selecionar os c�digos de f�brica que dever�o entrar na consulta
    Set OraParameters = vBanco.Parameters
    
    OraParameters.Remove "COD_DPK":         OraParameters.Add "COD_DPK", lngCOD_DPK, 1
    OraParameters.Remove "PRECO_COMPRA":    OraParameters.Add "PRECO_COMPRA", 0, 2
    
    OraParameters.Remove "ERRO":            OraParameters.Add "ERRO", 0, 2
    
    vSql = "PRODUCAO.PCK_CAD185.PR_PRECO_COMPRA(:COD_DPK, :PRECO_COMPRA, :ERRO)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
        BuscaPrecoCompra = False
        Exit Function
    End If

    pPrecoCompra = OraParameters("PRECO_COMPRA").Value
    
    BuscaPrecoCompra = True
    
End Function
