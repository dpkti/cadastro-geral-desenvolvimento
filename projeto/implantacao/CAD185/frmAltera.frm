VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmAltera 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Altera��o do Pre�o de Compra"
   ClientHeight    =   4260
   ClientLeft      =   3240
   ClientTop       =   3315
   ClientWidth     =   6330
   Icon            =   "frmAltera.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   284
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   422
   Begin VB.Frame fraPreco 
      Height          =   1005
      Left            =   990
      TabIndex        =   23
      Top             =   2745
      Visible         =   0   'False
      Width           =   3705
      Begin VB.CheckBox chkOnline 
         Caption         =   "Atualiza Online"
         Height          =   195
         Left            =   135
         TabIndex        =   26
         Top             =   630
         Width           =   1920
      End
      Begin VB.TextBox txtPreco 
         Height          =   285
         Left            =   1530
         TabIndex        =   24
         Top             =   225
         Width           =   1815
      End
      Begin VB.Label lblPreco 
         AutoSize        =   -1  'True
         Caption         =   "Pre�o de Compra"
         Height          =   195
         Left            =   180
         TabIndex        =   25
         Top             =   270
         Width           =   1230
      End
   End
   Begin VB.TextBox txtCdForn 
      Height          =   285
      Left            =   990
      TabIndex        =   0
      Top             =   945
      Width           =   1005
   End
   Begin VB.TextBox txtFabrica 
      Height          =   285
      Left            =   990
      TabIndex        =   1
      Top             =   1275
      Width           =   2040
   End
   Begin VB.TextBox txtDesc 
      BackColor       =   &H8000000F&
      Height          =   285
      Left            =   990
      Locked          =   -1  'True
      TabIndex        =   11
      TabStop         =   0   'False
      Top             =   1605
      Width           =   4020
   End
   Begin VB.TextBox txtLinha 
      BackColor       =   &H8000000F&
      Height          =   285
      Left            =   990
      Locked          =   -1  'True
      TabIndex        =   10
      TabStop         =   0   'False
      Top             =   1935
      Visible         =   0   'False
      Width           =   2040
   End
   Begin VB.TextBox txtSubGr 
      BackColor       =   &H8000000F&
      Height          =   285
      Left            =   4005
      Locked          =   -1  'True
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   2295
      Visible         =   0   'False
      Width           =   2040
   End
   Begin VB.TextBox txtCdDpk 
      Height          =   285
      Left            =   3960
      TabIndex        =   2
      Top             =   1260
      Width           =   1050
   End
   Begin VB.TextBox txtUnid 
      BackColor       =   &H8000000F&
      Height          =   285
      Left            =   4005
      Locked          =   -1  'True
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   1935
      Visible         =   0   'False
      Width           =   1005
   End
   Begin VB.TextBox txtGrupo 
      BackColor       =   &H8000000F&
      Height          =   285
      Left            =   990
      Locked          =   -1  'True
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   2295
      Visible         =   0   'False
      Width           =   2040
   End
   Begin VB.TextBox txtForn 
      BackColor       =   &H8000000F&
      Height          =   285
      Left            =   2070
      Locked          =   -1  'True
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   945
      Width           =   2940
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   3
      Top             =   3930
      Width           =   6330
      _ExtentX        =   11165
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   11113
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   4
      Top             =   810
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   5
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmAltera.frx":23D2
      PICN            =   "frmAltera.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdGravar 
      Height          =   690
      Left            =   810
      TabIndex        =   20
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   0
      MPTR            =   1
      MICON           =   "frmAltera.frx":30C8
      PICN            =   "frmAltera.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdPesquisar 
      Height          =   690
      Left            =   1575
      TabIndex        =   21
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   0
      MPTR            =   1
      MICON           =   "frmAltera.frx":3DBE
      PICN            =   "frmAltera.frx":3DDA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdCancelar 
      Height          =   690
      Left            =   2340
      TabIndex        =   22
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   0
      MPTR            =   1
      MICON           =   "frmAltera.frx":4AB4
      PICN            =   "frmAltera.frx":4AD0
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label lblCodDpk 
      AutoSize        =   -1  'True
      Caption         =   "C�d. DPK"
      Height          =   195
      Left            =   3195
      TabIndex        =   19
      Top             =   1320
      Width           =   705
   End
   Begin VB.Label lblUnid 
      AutoSize        =   -1  'True
      Caption         =   "Unidade"
      Height          =   195
      Left            =   3300
      TabIndex        =   18
      Top             =   1980
      Visible         =   0   'False
      Width           =   600
   End
   Begin VB.Label lblGrupo 
      AutoSize        =   -1  'True
      Caption         =   "Grupo"
      Height          =   195
      Left            =   465
      TabIndex        =   17
      Top             =   2340
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.Label lblSubGrupo 
      AutoSize        =   -1  'True
      Caption         =   "SubGrupo"
      Height          =   195
      Left            =   3180
      TabIndex        =   16
      Top             =   2340
      Visible         =   0   'False
      Width           =   720
   End
   Begin VB.Label lblLinha 
      AutoSize        =   -1  'True
      Caption         =   "Linha"
      Height          =   195
      Left            =   510
      TabIndex        =   15
      Top             =   1980
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.Label lblDesc 
      AutoSize        =   -1  'True
      Caption         =   "Descri��o"
      Height          =   195
      Left            =   180
      TabIndex        =   14
      Top             =   1650
      Width           =   720
   End
   Begin VB.Label lblFabrica 
      AutoSize        =   -1  'True
      Caption         =   "F�brica"
      Height          =   195
      Left            =   375
      TabIndex        =   13
      Top             =   1320
      Width           =   525
   End
   Begin VB.Label lblFornec 
      AutoSize        =   -1  'True
      Caption         =   "Fornecedor"
      Height          =   195
      Left            =   90
      TabIndex        =   12
      Top             =   990
      Width           =   810
   End
End
Attribute VB_Name = "frmAltera"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim frnFornecedor As New Fornecedor
Dim prdProduto As New Produto

' Variaveis
Dim mDiv As String
Dim mstrCdForn As String
Dim mstrCdDpk As String


Private Sub cmdSair_Click()

    Unload Me

End Sub

Private Sub cmdGravar_Click()
  
  Dim vCDSOnline As String
  Dim vFl_Online As Boolean
  
  Dim vPreco As Double
  
  Dim strFl_Online As String
  Dim strFl_Geral As String
  
  Dim I As Integer
  
  Dim vLoja As Integer
  
  Dim IP As String, Timeout As Integer, CDLJ As String, Success As Long
  
  'Verifica se foi digitado um DPK para consultar/gravar pre�o
  If mstrCdDpk = "" Then
      MsgBox "� necess�rio digitar um DPK!", vbCritical, "CAD185 - Aten��o"
      Exit Sub
  End If
       
  'Verifica se existem pre�o para gravar
  If Val(txtPreco.Text) = 0 Then
      MsgBox "Pre�o de compra zerado!", vbInformation, "CAD185 - Aten��o"
  End If
  
  
  If MsgBox("Confirma Atualiza��o do Pre�o de Compra? ", vbYesNo + vbQuestion, "CAD185 - Aten��o") = vbNo Then
      Exit Sub
  End If
  
  If chkOnline.Value = 1 Then
   
      strFl_Online = "S"
      vFl_Online = True
          
      Set OraParameters = vBanco.Parameters
      Criar_Cursor OraParameters, "vCursor"
      
      OraParameters.Remove "LOJA_PADRAO":     OraParameters.Add "LOJA_PADRAO", DEPOSITO_DEFAULT, 1
      OraParameters.Remove "COD_LOJA":     OraParameters.Add "COD_LOJA", 0, 1
      OraParameters.Remove "ERRO":            OraParameters.Add "ERRO", 0, 2
      
      
      vSql = "PRODUCAO.PCK_CAD180.PR_CDS_IP(:LOJA_PADRAO, :COD_LOJA, :ERRO, :vCursor)"
      If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
          Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
          Exit Sub
      Else
          Set vObjOracle = OraParameters("vCursor").Value
      End If
  
      If vObjOracle.EOF Then
          MsgBox "N�o existem CDs cadastrados na tabela DEPOSITO_VISAO! " & Chr(13) & _
                "Favor avisar o analista respons�vel!", vbCritical, "CAD180 - ATEN��O!)"
          Screen.MousePointer = 0
          Exit Sub
      End If
            
      
      While Not vObjOracle.EOF
          
          IP = vObjOracle("IP")
          Timeout = vObjOracle("TIME_OUT")
          CDLJ = vObjOracle("Cod_Loja")
          Success = Ping(IP, CDLJ, ECHO)
  
          If InStr(GetStatusCode(Success), ("ip success")) = 0 Or _
              ECHO.RoundTripTime > Timeout Then
              MsgBox "CD " & CDLJ & " sem comunica��o!" & Chr(13) & _
                  "Por favor, tente novamente mais tarde. " & Chr(13) & _
                  "Caso contr�rio a atualiza��o ser� feita somente � noite!!", vbInformation, "CAD185 - ATEN��O!"
          Else
              vCDSOnline = vCDSOnline & ", " & CStr(CDLJ)
          End If
        
          vObjOracle.MoveNext
      Wend
      
      vCDSOnline = Mid(vCDSOnline, 3)
      
  End If
      
  vPreco = CDbl(txtPreco.Text)
  
  Set OraParameters = vBanco.Parameters
    
  OraParameters.Remove "COD_DPK":     OraParameters.Add "COD_DPK", Val(mstrCdDpk), 1
  OraParameters.Remove "PRECO_COMPRA":     OraParameters.Add "PRECO_COMPRA", vPreco, 1
  OraParameters.Remove "ONLINE":     OraParameters.Add "ONLINE", strFl_Online, 1
  OraParameters.Remove "LOJAS":     OraParameters.Add "LOJAS", vCDSOnline, 1
  OraParameters.Remove "COD_ERRO":     OraParameters.Add "COD_ERRO", 0, 2
  OraParameters.Remove "TXT_ERRO":     OraParameters.Add "TXT_ERRO", Null, 2
  
  vSql = "PRODUCAO.PCK_CAD185.PR_GRAVA(:COD_DPK, :PRECO_COMPRA, :ONLINE, :LOJAS, :COD_ERRO, :TXT_ERRO )"
  If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
      Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
      Exit Sub
  End If
  
  If OraParameters("COD_ERRO").Value <> 0 Then
      Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
      Exit Sub
  End If
  
  Screen.MousePointer = 0
  MsgBox "Atualiza��es Efetuadas com Sucesso!!", vbInformation, "CAD185 - Aten��o"
    
End Sub

Private Sub cmdPesquisar_Click()
  Screen.MousePointer = 11
  
  txtLinha.Visible = True
  txtUnid.Visible = True
  txtGrupo.Visible = True
  txtSubGr.Visible = True
    
  fraPreco.Visible = True
  
  DoEvents
  
  If prdProduto.CodDPK <> 0 Then
  
    If Not prdProduto.BuscaPrecoCompra(prdProduto.CodDPK) Then
        Call vVB_Generica_001.Informar("Produto sem pre�o de compra!")
        txtPreco.Text = "0.00"
    Else
        txtPreco.Text = Format(prdProduto.PrecoCompra, "0.000")
    End If
Else
        txtPreco.Text = "0.00"
End If
  Screen.MousePointer = 1

End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    
    Me.Top = 0
    Me.Left = 0
    
End Sub

Private Sub cmdCancelar_Click()
    
    'Limpa Todos os Campos(TextBox) do Form
    Dim Control As Object
    
    For Each Control In Me.Controls
      If TypeName(Control) = "TextBox" Then
        Control.Text = ""
      End If
    Next Control
    
    txtLinha.Visible = False
    txtUnid.Visible = False
    txtGrupo.Visible = False
    txtSubGr.Visible = False
    
    ' Esconde a Tabela de Pre�os
    fraPreco.Visible = False
    
End Sub

Private Sub txtcddpk_KeyPress(KeyAscii As Integer)
  KeyAscii = Numerico(KeyAscii)
End Sub

Private Sub txtcddpk_LostFocus()
Dim I As Integer

If Trim(txtCdDpk.Text) <> "" And Trim(txtFabrica.Text) = "" Then

    'Vai verificar dados do item no CD Campinas
    
    
    If Not prdProduto.BuscarCodDPK(CLng(txtCdDpk.Text), 1) Then
      Exit Sub
    End If
    
    txtFabrica.Text = prdProduto.CodFabr
    txtCdForn.Text = prdProduto.CodForn
    mstrCdForn = Trim(txtCdForn.Text)
    'txtCdForn_LostFocus
    txtForn.Text = prdProduto.Sigla
    mDiv = prdProduto.Divisao
    mstrCdDpk = Trim(txtCdDpk.Text)
    
    txtDesc.Text = Trim(prdProduto.Descricao)
    txtLinha.Text = Format(prdProduto.CodLinha, "00") & "-" & prdProduto.DescLinha
    txtGrupo.Text = Format(prdProduto.CodGrupo, "00") & "-" & prdProduto.DescGrupo
    txtSubGr.Text = Format(prdProduto.CodSubgrupo, "00") & "-" & prdProduto.DescSubGrupo
    txtUnid.Text = prdProduto.CodUnidade
    
    'txtFabrica_LostFocus
        
Else
    txtCdDpk.Enabled = True
End If

End Sub

Private Sub txtCdForn_Click()
    cmdCancelar_Click
End Sub

Private Sub txtCdForn_GotFocus()
    cmdCancelar_Click
End Sub

Private Sub txtCdForn_KeyPress(KeyAscii As Integer)
    KeyAscii = Numerico(KeyAscii)
End Sub

Private Sub txtCdForn_LostFocus()
  
  If Trim(txtCdForn.Text) <> "" Then
      
      If frnFornecedor.Buscar(CLng(txtCdForn.Text)) Then
          txtForn.Text = frnFornecedor.Sigla
          mDiv = frnFornecedor.Divisao
      End If
      
  Else
      txtForn.Text = ""
  End If
  
  mstrCdForn = Trim(txtCdForn.Text)

End Sub


Private Sub txtFabrica_Click()
    txtCdDpk.Text = ""
    mstrCdDpk = ""
    fraPreco.Visible = False
End Sub

Private Sub txtFabrica_GotFocus()
    txtCdDpk.Text = ""
    mstrCdDpk = ""
    fraPreco.Visible = False
End Sub

Private Sub txtFabrica_KeyPress(KeyAscii As Integer)
    KeyAscii = Texto(KeyAscii)
End Sub

Private Sub txtFabrica_LostFocus()
Dim I As Integer
Dim vLoja As Integer, vAux As Byte

If Trim(txtFabrica.Text) <> "" Then
 
    If prdProduto.BuscarCodFabr(1, Trim(txtFabrica.Text), CLng(mstrCdForn)) Then
        
        If Trim(txtCdDpk.Text) = "" Then
            
            txtDesc.Text = prdProduto.Descricao
            txtCdDpk.Text = prdProduto.CodDPK
            mstrCdDpk = Trim(txtCdDpk.Text)
            txtCdDpk.Enabled = False
        
            txtDesc.Text = Trim(prdProduto.Descricao)
            txtLinha.Text = Format(prdProduto.CodLinha, "00") & "-" & prdProduto.DescLinha
            txtGrupo.Text = Format(prdProduto.CodGrupo, "00") & "-" & prdProduto.DescGrupo
            txtSubGr.Text = Format(prdProduto.CodSubgrupo, "00") & "-" & prdProduto.DescSubGrupo
            txtUnid.Text = prdProduto.CodUnidade
      
        End If
        
    Else
    
        Exit Sub
        
    End If
        
Else
    txtCdDpk.Enabled = True

End If

End Sub


Private Sub txtPreco_KeyPress(KeyAscii As Integer)
    KeyAscii = Valor(KeyAscii, txtPreco.Text)
End Sub

Private Sub txtPreco_LostFocus()
 txtPreco.Text = FmtBR(txtPreco.Text)
    txtPreco.Text = Format(txtPreco.Text, "#,###,###,##0.000")
    If CDbl(txtPreco.Text) < 0 Then
        MsgBox "Pre�o de compra n�o pode ser negativo!", vbInformation, "Aten��o!"
        txtPreco.SetFocus
    End If
End Sub
