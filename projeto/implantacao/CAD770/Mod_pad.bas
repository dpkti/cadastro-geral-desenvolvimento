Attribute VB_Name = "MOD_PAD"
'VARIAVEIS DE BANCO DE DADOS
Global dbOra  As Object         'Para Abrir o Bco.
Global db As Object             'Database
Global db_CONS As Object        'Variavel Bco. de Consulta
Global cod_errora As Integer    'Cod. Erro PL-SQL
Global txt_errora As String     'Texto Erro PL-SQL
Global CmdSql As String         'String para o SQL
Global CmdPl As String          'String para o PL-SQL
Global CmdPlCD As String          'String para o PL-SQL

'Vari�veis auxiliares para consulta ao banco
Global db_CONS2 As Object        'Variavel Bco. de Consulta
Global CmdSql2 As String         'String para o SQL
Global db_CONS3 As Object        'Variavel Bco. de Consulta
Global CmdSql3 As String         'String para o SQL


Public ECHO As ICMP_ECHO_REPLY
Public success As Long
Public IP As String
Public TIMEOUT As Double
Public CDLJ As String



'VARIAVEIS AUXILIARES
Public MSG_TIT As String        'T�tulo MsgBox
Public FL_INAL As Integer       'Flag de Inclus�o=0/Altera��o=1
Public vDt_Cad As String
Public vAnoMes As String
Public vCD_Deposito As String   'Guarda o c�digo de loja do dep�sito onde o pgm. est� rodando

'PADRAO PARA MENSAGEM
Const ctnormal = 1
Const cterro = 2

'VARIAVEIS PARA A FUNCAO DO EXTENSO
Dim Unidade(20) As String
Dim Dezena(8)  As String
Dim Centena(9) As String


Sub MMAA(ByRef KeyAscii, ByRef txtCampo)
    On Error GoTo TrataErro

    Dim bTam As Byte    'tamanho do campo JA digitado
    Dim strData As String
    Dim bKey As Byte
    bTam = Len(txtCampo.Text)
    
    If KeyAscii = 8 And bTam > 0 Then 'backspace
        If Mid$(txtCampo.Text, bTam) = "/" Then
            txtCampo.Text = Mid$(txtCampo.Text, 1, bTam - 2)
        Else
            txtCampo.Text = Mid$(txtCampo.Text, 1, bTam - 1)
        End If
            
    ElseIf Chr$(KeyAscii) >= "0" And Chr$(KeyAscii) <= "9" Then
        If bTam = 1 Then
            strData = txtCampo.Text & Chr$(KeyAscii)
            If CInt(strData) < 1 Or CInt(strData > 12) Then
                Beep
            Else
                txtCampo.Text = txtCampo.Text & Chr$(KeyAscii) & "/"
            End If
        
        ElseIf bTam = 5 Then
            strData = Mid$(txtCampo.Text, 1, 2)     'mes
            If CInt(strData) < 1 Or CInt(strData > 12) Then
               Beep
            Else
               strData = txtCampo.Text & Chr$(KeyAscii)
               If Not IsDate(CDate(strData)) Then
                  Beep
               Else
                  txtCampo.Text = strData
               End If
            End If
            
        ElseIf bTam < 5 Then
            bKey = KeyAscii
            
        Else
            bKey = 0
        End If
    Else
        Beep
    End If
    
    SendKeys "{END}"
    KeyAscii = bKey
    Exit Sub
    
TrataErro:

    If Err.Number = 13 Then
        MsgBox strData, vbInformation, "Data Inv�lida"
        KeyAscii = 0
        Beep
        Err.Clear
    End If

End Sub

Function Def_cent(strAux As String, strUnitario As String, strVarios As String) As String
    
    Dim iVL As Integer
    Dim strRetorno As String
    Dim iOriginal As Integer

    If CInt(strAux) = 0 Then
        Def_cent = ""
        Exit Function
    End If

    iOriginal = CInt(strAux)

    If iOriginal > 99 Then
        strAux = Mid$(strAux, 1, 3)
        If strAux = "100" Then
            strRetorno = "CEM"
        Else
            strRetorno = Centena(CInt(Mid$(strAux, 1, 1)) - 1)
        End If
        strAux = Mid$(strAux, 2, 2)
    Else
        If Len(strAux) > 2 Then
            strAux = Mid$(strAux, 2, 2)
        End If
    End If
    
    iVL = CInt(strAux)
    If iVL > 0 And iVL < 20 Then
        If Len(strRetorno) > 0 Then
            strRetorno = strRetorno & " E " & Unidade(iVL)
        Else
            strRetorno = Unidade(iVL)
        End If
    ElseIf iVL > 19 Then
        iVL = CInt(Mid$(strAux, 1, 1))
        If Len(strRetorno) > 0 Then
            strRetorno = strRetorno & " E " & Dezena(iVL - 2)
        Else
            strRetorno = Dezena(iVL - 2)
        End If
        iVL = CInt(Mid$(strAux, 2, 1))
        If iVL > 0 Then
            strRetorno = strRetorno & " E " & Unidade(iVL)
        End If
    End If

    If strUnitario <> "" Then
        If iOriginal = 1 Then
            strRetorno = strRetorno & " " & strUnitario
        Else
            strRetorno = strRetorno & " " & strVarios
        End If
    End If

    Def_cent = strRetorno

End Function

Function Extenso(Valor As Double) As String

    Dim strDecimal As String
    Dim strInteiro As String
    Dim strAuxPrep As String
    Dim inteiro As Currency
    Dim decimal As Integer
    Dim I As Integer
    Dim J As Integer
    Dim strAux As String
    Dim strBilhao As String
    Dim strMILhao As String
    Dim strMIL As String
    Dim strCem As String
    
    'call Carregar dados
    Unidade(0) = "ZERO"
    Unidade(1) = "UM"
    Unidade(2) = "DOIS"
    Unidade(3) = "TR�S"
    Unidade(4) = "QUATRO"
    Unidade(5) = "CINCO"
    Unidade(6) = "SEIS"
    Unidade(7) = "SETE"
    Unidade(8) = "OITO"
    Unidade(9) = "NOVE"
    Unidade(10) = "DEZ"
    Unidade(11) = "ONZE"
    Unidade(12) = "DOZE"
    Unidade(13) = "TREZE"
    Unidade(14) = "QUATORZE"
    Unidade(15) = "QUINZE"
    Unidade(16) = "DEZESSEIS"
    Unidade(17) = "DEZESSETE"
    Unidade(18) = "DEZOITO"
    Unidade(19) = "DEZENOVE"
    
    Dezena(0) = "VINTE"
    Dezena(1) = "TRINTA"
    Dezena(2) = "QUARENTA"
    Dezena(3) = "CINQUENTA"
    Dezena(4) = "SESSENTA"
    Dezena(5) = "SETENTA"
    Dezena(6) = "OITENTA"
    Dezena(7) = "NOVENTA"

    Centena(0) = "CENTO"
    Centena(1) = "DUZENTOS"
    Centena(2) = "TREZENTOS"
    Centena(3) = "QUATROCENTOS"
    Centena(4) = "QUINHENTOS"
    Centena(5) = "SEISCENTOS"
    Centena(6) = "SETECENTOS"
    Centena(7) = "OITOCENTOS"
    Centena(8) = "NOVECENTOS"

    'separar valor
    inteiro = Int(Valor)
    strAux = CStr(Valor)
    I = InStr(1, strAux, ".")
    If I > 0 Then
        strAux = Mid$(strAux, I + 1)
        If Len(strAux) = 1 Then
            strAux = strAux & "0"
        ElseIf Len(strAux) > 2 Then
            strAux = Mid$(strAux, 1, 2)
        End If
        decimal = CInt(strAux)
    Else
        decimal = 0
    End If
    
    'definir valor decimal
    strDecimal = Def_cent(CStr(decimal), "CENTAVO", "CENTAVOS")
    If decimal = 0 Then
        strDecimal = ""
    End If
    
    'definir valor inteiro
    I = Len(CStr(inteiro))
    J = 1
    strBilhao = "N"
    strMILhao = "N"
    strMIL = "N"
    strCem = "N"
    strAuxPrep = ""
    If I > 9 Then  'BILHAO
        strBilhao = "S"
        strAux = Mid$(CStr(inteiro), J, I - 9)
        strInteiro = Def_cent(strAux, "BILH�O", "BILH�ES")
        J = J + I - 9
        I = 9
    End If

    If I > 6 Then  'MILHAO
        strAux = Mid$(CStr(inteiro), J, I - 6)
        If CInt(strAux) > 0 Then
            If Len(strInteiro) > 0 Then
                strInteiro = strInteiro & " , " & Def_cent(strAux, "MILH�O", "MILH�ES")
            Else
                strInteiro = Def_cent(strAux, "MILH�O", "MILH�ES")
            End If
            strMILhao = "S"
        End If
        If J > 1 Then
            strAuxPrep = strAux
        End If
        J = J + I - 6
        I = 6
    End If

    If I > 3 Then  'MIL
        strAux = Mid$(CStr(inteiro), J, I - 3)
        If CInt(strAux) > 0 Then
            If Len(strInteiro) > 0 Then
                If strBilhao = "S" And strMILhao = "N" Then
                    strInteiro = strInteiro & " E " & Def_cent(strAux, "MIL", "MIL")
                Else
                    strInteiro = strInteiro & " , " & Def_cent(strAux, "MIL", "MIL")
                End If
            Else
                strInteiro = Def_cent(strAux, "MIL", "MIL")
            End If
            strMIL = "S"
        End If
        If J > 1 Then
            strAuxPrep = strAuxPrep & strAux
        End If
        J = J + I - 3
        I = 3
    End If

    strAux = Mid$(CStr(inteiro), J, I)
    If CInt(strAux) > 0 Then
        If Len(strInteiro) > 0 Then
            If (strBilhao = "S" And strMIL = "N") Or (strMILhao = "S" And strMIL = "N") Then
                strInteiro = strInteiro & " E " & Def_cent(strAux, "", "")
            Else
                strInteiro = strInteiro & " , " & Def_cent(strAux, "", "")
            End If
        Else
            strInteiro = Def_cent(strAux, "", "")
        End If
    End If

    If inteiro = 1 Then
        strInteiro = strInteiro & " REAL"
    Else
        If strAuxPrep <> "" Then
            If CLng(strAuxPrep) = 0 Then
                strAuxPrep = " DE"
            Else
                strAuxPrep = ""
            End If
        End If
        strInteiro = strInteiro & strAuxPrep & " REAIS"
    End If

    'definir valor de retorno
    If inteiro = 0 And decimal = 0 Then
        Extenso = ""
    ElseIf inteiro = 0 Then
        Extenso = strDecimal
    Else
        If decimal = 0 Then
            Extenso = strInteiro
        Else
            Extenso = strInteiro & " E " & strDecimal
        End If
    End If

End Function


Sub Data(ByRef KeyAscii, ByRef txtCampo)
    On Error GoTo TrataErro

    Dim bTam As Byte    'tamanho do campo JA digitado
    Dim strData As String
    Dim bKey As Byte
    bTam = Len(txtCampo.Text)
    
    If KeyAscii = 8 And bTam > 0 Then 'backspace
        If Mid$(txtCampo.Text, bTam) = "/" Then
            txtCampo.Text = Mid$(txtCampo.Text, 1, bTam - 2)
        Else
            txtCampo.Text = Mid$(txtCampo.Text, 1, bTam - 1)
        End If
            
    ElseIf Chr$(KeyAscii) >= "0" And Chr$(KeyAscii) <= "9" Then
        If bTam = 1 Then
            strData = txtCampo.Text & Chr$(KeyAscii)
            If CInt(strData) < 1 Or CInt(strData > 31) Then
                Beep
            Else
                txtCampo.Text = txtCampo.Text & Chr$(KeyAscii) & "/"
            End If
            
        ElseIf bTam = 4 Then
            strData = Mid$(txtCampo.Text, 4) & Chr$(KeyAscii)
            If CInt(strData) < 1 Or CInt(strData > 12) Then
                Beep
            Else
                txtCampo.Text = txtCampo.Text & Chr$(KeyAscii) & "/"
            End If
        
        ElseIf bTam = 7 Then
            strData = Mid$(txtCampo.Text, 1, 2)     'dia
            If CInt(strData) < 1 Or CInt(strData > 31) Then
                Beep
            Else
                strData = Mid$(txtCampo.Text, 4, 2)     'mes
                If CInt(strData) < 1 Or CInt(strData > 12) Then
                    Beep
                Else
                    strData = txtCampo.Text & Chr$(KeyAscii)
                    If Not IsDate(CDate(strData)) Then
                        Beep
                    Else
                        txtCampo.Text = strData
                    End If
                End If
            End If
            
        ElseIf bTam < 8 Then
            bKey = KeyAscii
            
        Else
            bKey = 0
        End If
    Else
        Beep
    End If
    
    SendKeys "{END}"
    KeyAscii = bKey
    Exit Sub
    
TrataErro:

    If Err.Number = 13 Then
        MsgBox strData, vbInformation, "Data Inv�lida"
        KeyAscii = 0
        Beep
        Err.Clear
    End If

End Sub

Sub DATA_YYYY(ByRef KeyAscii, ByRef txtCampo)
    
    On Error GoTo TrataErro

    Dim bTam As Byte    'tamanho do campo JA digitado
    Dim strData As String
    Dim bKey As Byte
    
    bTam = Len(txtCampo.Text)

    If KeyAscii = 8 And bTam > 0 Then 'backspace
        If Mid$(txtCampo.Text, bTam) = "/" Then
            txtCampo.Text = Mid$(txtCampo.Text, 1, bTam - 2)
        Else
            txtCampo.Text = Mid$(txtCampo.Text, 1, bTam - 1)
        End If
            
    ElseIf Chr$(KeyAscii) >= "0" And Chr$(KeyAscii) <= "9" Then
        If bTam = 1 Then
            strData = txtCampo.Text & Chr$(KeyAscii)
            If CInt(strData) < 1 Or CInt(strData > 31) Then
                Beep
            Else
                txtCampo.Text = txtCampo.Text & Chr$(KeyAscii) & "/"
            End If
            
        ElseIf bTam = 4 Then
            strData = Mid$(txtCampo.Text, 4) & Chr$(KeyAscii)
            If CInt(strData) < 1 Or CInt(strData > 12) Then
                Beep
            Else
                txtCampo.Text = txtCampo.Text & Chr$(KeyAscii) & "/"
            End If
        
        ElseIf bTam = 9 Then
            strData = Mid$(txtCampo.Text, 1, 2)     'dia
            If CInt(strData) < 1 Or CInt(strData > 31) Then
                Beep
            Else
                strData = Mid$(txtCampo.Text, 4, 2)     'mes
                If CInt(strData) < 1 Or CInt(strData > 12) Then
                    Beep
                Else
                    strData = txtCampo.Text & Chr$(KeyAscii)
                    If Not IsDate(CDate(strData)) Then
                        Beep
                    Else
                        txtCampo.Text = strData
                    End If
                End If
            End If
            
        ElseIf bTam < 10 Then
            bKey = KeyAscii
            
        Else
            bKey = 0
        End If
    Else
        Beep
    End If
    
    SendKeys "{END}"
    KeyAscii = bKey
    Exit Sub
    
TrataErro:

    If Err.Number = 13 Then
        MsgBox strData, vbInformation, "Data Inv�lida"
        KeyAscii = 0
        Beep
        Err.Clear
    End If

End Sub

Function CONV_VALOR(strValor As String) As String
    Dim I As Byte
    I = InStr(strValor, ",")
    Do Until I = 0
        Mid(strValor, I, 1) = "."
        I = InStr(strValor, ",")
    Loop
    CONV_VALOR = strValor
End Function



Sub Process_Line_Errors()
    
    Dim iFnum As Integer
    'on error GoTo Handler_Process_Line_Errors
                
    If Err.Number = 3186 Or Err.Number = 3188 Or Err.Number = 3260 Or Err.Number = 3197 Then
        Resume
    Else
       'iFnum = FreeFile
       'Open strPathUsuario & "ERRORLOG.TXT" For Append As #iFnum
       'Print #iFnum, ""
       'Print #iFnum, App.ProductName & " " & Date & " " & Time
       'Print #iFnum, "  Erro: " & Err.Number & " - " & Err.Description
       'Print #iFnum, "  " & SQL
       'Close #iFnum
    
        MsgBox "Ocorreu o erro: " & Err.Number & " -" & Err.Description & ". Ligue para o departamento de sistemas"
         
        'cursor
        Screen.MousePointer = vbDefault
        'MsgBox "Erro na aplica��o, ligue para o departamento de sistemas", vbCritical, "A T E N � � O"
    
        'para a aplicacao
        End

    End If

    Exit Sub

Handler_Process_Line_Errors:
    DoEvents
    Resume Next

End Sub

Function FmtBR(ByVal Valor) As String

    Dim Temp As String
    Dim I As Integer
    
    Temp = Trim(Valor)
        
    For I = 1 To Len(Temp)
        If Mid$(Temp, I, 1) = "," Then
            Mid$(Temp, I, 1) = "."
        End If
    Next I

    FmtBR = Temp

End Function


Function iRetornaDigito(ByVal iOrigem As Long) As Integer

    Dim iSoma As Integer
    Dim gResto As Single
    Dim iTam As Integer
    Dim iFator As Integer
    Dim iContador As Integer
    Dim sOrigem As String

    iTam = Len(Trim$(Str$(iOrigem)))
    iFator = 2
    
    sOrigem = Trim$(Str$(iOrigem))
    
    For iContador = iTam To 1 Step -1
        iSoma = iSoma + Val(Mid$(sOrigem, iContador, 1)) * iFator
        iFator = iFator + 1
    Next iContador
    
    gResto = iSoma Mod 11
    
    If gResto = 0 Then
        iRetornaDigito = 1
    ElseIf gResto = 1 Then
        iRetornaDigito = 0
    Else
        iRetornaDigito = 11 - gResto
    End If

End Function


Function Maiusculo(KeyAscii As Integer) As Integer
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    Maiusculo = KeyAscii
End Function

Function Numerico(ByVal KeyAscii As Integer) As Integer
    If KeyAscii = 8 Then    'BACKSPACE
        Numerico = KeyAscii
        Exit Function
    End If
    If Chr(KeyAscii) < "0" Or Chr(KeyAscii) > "9" Then
        KeyAscii = 0
    End If
    Numerico = KeyAscii
End Function

Function Texto(ByVal KeyAscii As Integer) As Integer
    If KeyAscii = 8 Then    'BACKSPACE
        Texto = KeyAscii
        Exit Function
    End If
    
    If Chr$(KeyAscii) = "'" Or Chr$(KeyAscii) = "," Or Chr$(KeyAscii) = ";" Then
        KeyAscii = 0
        Beep
    Else
        KeyAscii = Asc(UCase(Chr$(KeyAscii)))
    End If
    
    Texto = KeyAscii
End Function

Function Valor(ByVal KeyAscii As Integer, strCampo As String) As Integer
    If KeyAscii = 8 Then    'BACKSPACE
        Valor = KeyAscii
        Exit Function
    End If
    
    If Chr$(KeyAscii) = "," Or Chr$(KeyAscii) = "." Then
        If InStr(strCampo, ",") > 0 Or InStr(strCampo, ".") > 0 Then
            KeyAscii = 0
            Beep
        End If
    Else
        If Chr$(KeyAscii) < "0" Or Chr$(KeyAscii) > "9" Then
            KeyAscii = 0
            Beep
        End If
    End If
    
   Valor = KeyAscii


End Function

Sub Hora(ByRef KeyAscii, ByRef txtCampo)

    On Error GoTo TrataErro

    Dim bTam As Byte    'tamanho do campo JA digitado
    Dim strData As String
    Dim bKey As Byte
    bTam = Len(txtCampo.Text)
    
    If Chr$(KeyAscii) >= "0" And Chr$(KeyAscii) <= "9" Then
    
        If bTam = 0 Then
            If Chr$(KeyAscii) = "0" Or Chr$(KeyAscii) = "1" Or Chr$(KeyAscii) = "2" Then
                bKey = KeyAscii
            Else
                Beep
                bKey = 0
            End If
                
        ElseIf bTam = 1 Then
            strData = txtCampo
            If Mid(strData, 1, 1) = "1" Or Mid(strData, 1, 1) = "0" Then
                txtCampo.Text = strData & Chr$(KeyAscii) & ":"
                
            ElseIf Mid(strData, 1, 1) = "2" And Chr$(KeyAscii) <= "3" Then
                txtCampo.Text = strData & Chr$(KeyAscii) & ":"
                
            Else
                Beep
                bKey = 0
                
            End If
            
        ElseIf bTam = 3 Then
            strData = txtCampo
            If Chr$(KeyAscii) < 6 Then
                bKey = KeyAscii
            Else
                Beep
                bKey = 0
            End If
            
        
        ElseIf bTam = 4 Then
            bKey = KeyAscii
            
        Else
            bKey = 0
            
        End If
        
    End If
      
    SendKeys "{END}"
    KeyAscii = bKey
    Exit Sub
    
TrataErro:

    If Err.Number = 13 Then
        MsgBox strData, vbInformation, "Hora Inv�lida"
        KeyAscii = 0
        Beep
        Err.Clear
    End If


End Sub

Function fVer_Conexao(ByVal pLoja As Integer) As Boolean

    If pLoja <> 1 And pLoja <> 6 Then
                    
        db.Parameters.Remove "LJ": db.Parameters.Add "LJ", pLoja, 1
                
        cmdPlSql = "Select COD_LOJA, IP, TIME_OUT " & _
                   "From PRODUCAO.LOJA_CONEXAO " & _
                   "Where COD_LOJA = :LJ "
                      
        Set objconexao = db.dbCreateDynaset(cmdPlSql, 0&)
                        
        If objconexao.EOF Then
            MsgBox "CD " & pLoja & " n�o cadastrada na tabela LOJA_CONEXAO! " & Chr(13) & _
                   "Chame o analista respons�vel.", vbCritical, "Aten��o"
            Exit Function
        End If
                    
        IP = objconexao!IP
        TIMEOUT = Val(objconexao!TIME_OUT)
        CDLJ = objconexao!Cod_Loja
                    
        success = Ping(IP, CDLJ, ECHO)
                    
        'Vai verificar se o tempo do ping, compensar a conex�o com o CD
        If InStr(GetStatusCode(success), ("ip success")) = 0 Or _
            ECHO.RoundTripTime > TIMEOUT Then
            'MsgBox "CD " & CDLJ & " sem comunica��o!", vbInformation, "Aten��o"
            fVer_Conexao = False
        Else
            fVer_Conexao = True
        End If
                
    End If

End Function


