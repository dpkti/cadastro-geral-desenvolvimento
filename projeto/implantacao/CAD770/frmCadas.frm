VERSION 4.00
Begin VB.Form frmCadastro 
   Caption         =   "CAD770 - Altera��o Autom�tica da Situa��o de Itens DPK por Leitura de Arquivo"
   ClientHeight    =   7545
   ClientLeft      =   2235
   ClientTop       =   1830
   ClientWidth     =   11190
   Height          =   7950
   Left            =   2175
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   7545
   ScaleWidth      =   11190
   Top             =   1485
   Width           =   11310
   WindowState     =   2  'Maximized
   Begin Threed.SSFrame SSFrame2 
      Height          =   7695
      Left            =   240
      TabIndex        =   0
      Top             =   255
      Width           =   9765
      _Version        =   65536
      _ExtentX        =   17224
      _ExtentY        =   13573
      _StockProps     =   14
      ForeColor       =   12582912
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin Threed.SSFrame SSFrame3 
         Height          =   1455
         Left            =   240
         TabIndex        =   1
         Top             =   255
         Width           =   9270
         _Version        =   65536
         _ExtentX        =   16351
         _ExtentY        =   2566
         _StockProps     =   14
         Caption         =   "Arquivo"
         ForeColor       =   12582912
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Begin VB.Label lblHelp1 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H0000FFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Abrir Arquivo"
            ForeColor       =   &H80000008&
            Height          =   225
            Left            =   1320
            TabIndex        =   4
            Top             =   1200
            Visible         =   0   'False
            Width           =   930
         End
         Begin Threed.SSCommand cmdArquivo 
            Height          =   855
            Left            =   375
            TabIndex        =   3
            Top             =   390
            Width           =   1335
            _Version        =   65536
            _ExtentX        =   2355
            _ExtentY        =   1508
            _StockProps     =   78
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Picture         =   "frmCadas.frx":0000
         End
         Begin MSComDlg.CommonDialog controle 
            Left            =   120
            Top             =   240
            _ExtentX        =   847
            _ExtentY        =   847
            _Version        =   393216
            DefaultExt      =   "*.PRN"
            InitDir         =   "C:\CAD770"
            MaxFileSize     =   32000
         End
         Begin VB.Label lblArquivo 
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H00C00000&
            Height          =   375
            Left            =   1875
            TabIndex        =   2
            Top             =   765
            Width           =   6990
         End
      End
      Begin VB.Label lblMsg3 
         AutoSize        =   -1  'True
         Caption         =   "Mensagem"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00004080&
         Height          =   240
         Left            =   300
         TabIndex        =   12
         Top             =   7185
         Width           =   1155
      End
      Begin VB.Label lblMsg2 
         AutoSize        =   -1  'True
         Caption         =   "Mensagem"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000080&
         Height          =   240
         Left            =   300
         TabIndex        =   11
         Top             =   6840
         Width           =   1155
      End
      Begin VB.Label lblMsg 
         AutoSize        =   -1  'True
         Caption         =   "Mensagem"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   240
         Left            =   300
         TabIndex        =   10
         Top             =   6360
         Width           =   1155
      End
      Begin MSGrid.Grid grdArquivo 
         Height          =   4200
         Left            =   210
         TabIndex        =   9
         Top             =   1890
         Width           =   9315
         _Version        =   65536
         _ExtentX        =   16431
         _ExtentY        =   7408
         _StockProps     =   77
         ForeColor       =   8388608
         BackColor       =   16777215
         Cols            =   12
      End
      Begin Threed.SSCommand cmdVoltar 
         Height          =   855
         Left            =   8115
         TabIndex        =   8
         Top             =   6360
         Width           =   1335
         _Version        =   65536
         _ExtentX        =   2355
         _ExtentY        =   1508
         _StockProps     =   78
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "frmCadas.frx":031A
      End
      Begin Threed.SSCommand cmdConfirma 
         Height          =   855
         Left            =   6495
         TabIndex        =   7
         Top             =   6360
         Width           =   1335
         _Version        =   65536
         _ExtentX        =   2355
         _ExtentY        =   1508
         _StockProps     =   78
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "frmCadas.frx":0634
      End
      Begin VB.Label lblHelp2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H0000FFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Gerar Arq.de Log"
         ForeColor       =   &H80000008&
         Height          =   225
         Left            =   4710
         TabIndex        =   6
         Top             =   7260
         Visible         =   0   'False
         Width           =   1245
      End
      Begin Threed.SSCommand cmdLog 
         Height          =   855
         Left            =   4785
         TabIndex        =   5
         Top             =   6360
         Width           =   1335
         _Version        =   65536
         _ExtentX        =   2355
         _ExtentY        =   1508
         _StockProps     =   78
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "frmCadas.frx":094E
      End
   End
End
Attribute VB_Name = "frmCadastro"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Public I As Integer                'Ponteiro / Contador
Dim vNomeArq As String
Dim varLinhaArquivo As String   'Recebe a linha lida do arquivo texto
Dim fl_Log As Boolean
Dim vGuardaCol As Integer
Dim vGuardaLin As Long

Dim vFL_EDI As String           'Guarda flag que diz se fornecedor trabalha com EDI ou n�o
Dim vCOD_FORN As Long           'Guarda o c�digo do fornecedor ref. ao DPK vindo do arquivo
Dim vFL_DPK_OK As Boolean       'Indica se DPK est� Ok ou n�o (se n�o estiver, nem continua demais valida��es

Dim vCD As String               'Recebe o c�digo de loja que veio no arquivo
Dim vDPK As String              'Recebe o c�digo DPK que veio no arquivo
Dim vSIT_DPK_ARQ As String      'Recebe a situa��o do DPK que veio no arquivo
Dim vSIT_DPK_ANT As String      'Recebe a situa��o do DPK que est� no banco de dados (Situa��o Anterior)
Dim vCODFABR_EDI As String      'Recebe o c�digo de f�brica EDI que est� no banco de dados
Dim vSEQ_TAB_ANT As String      'Recebe a sequ�ncia de tabela que veio no arquivo
Dim vCUEMA_ANT As String        'Recebe o valor do cuema que veio no arquivo
Dim vQTDMINFORN As String       'Recebe a qtd. min.fornecedor, caso for ativar item

Dim vFl_Erro As Boolean         'Vai indicar se a grava��o de determinado item, deu erro ou n�o
Sub LIMPA_GRID()
Dim L As Long
Dim C As Integer
Dim vTotLin As Long

vTotLin = grdArquivo.Rows - 1

'Varre todas as linhas do grid
For L = 1 To vTotLin
    'Se for a �ltima linha a ser removida, deleta os campos de cada coluna
    If L = vTotLin Then
        For C = 0 To grdArquivo.Cols - 1
            grdArquivo.Col = C
            grdArquivo.Text = ""
        Next C
    'Sen�o remove o item
    Else
        grdArquivo.RemoveItem 1
    End If
Next L

End Sub




Sub sRotina_Ativar_Item()
'Rotina de Valida��o para Ativar Item
    
    Screen.MousePointer = 11
    
    'Se CD diferente de Campinas, verifica se o DPK est� com situa��o de ativo para o CD Cps.
    If (vCD <> vCD_Deposito) And vFL_DPK_OK = True Then
        db.Parameters.Remove "CD_DEP": db.Parameters.Add "CD_DEP", Val(vCD_Deposito), 1
        db.Parameters.Remove "DPK": db.Parameters.Add "DPK", Val(vDPK), 1
        
        CmdSql = "Select * From Item_Estoque " & _
                 "Where Cod_Dpk = :DPK " & _
                 "And Cod_Loja = :CD_DEP " & _
                 "And Situacao = 0"
        
        Set db_CONS = db.dbcreatedynaset(CmdSql, 0&)
            
        'Se select n�o trouxer nada, n�o pode ativar no CD, antes de ativar em Campinas
        If db_CONS.EOF Then
            grdArquivo.Col = 10
            grdArquivo.Text = "Ative primeiro em Cps! "
            vFL_DPK_OK = False
        End If
    End If
                
    'Vai pegar o CUEMA e SEQ.TABELA do item que quer ativar.
    'Se quiser ativar em Cps., pega na ITEM_ESTOQUE de Campinas
    'Se quiser ativar em outro CD, pega na ITEM_ESTOQUE do CD (on-line)
    If vFL_DPK_OK = True Then
        db.Parameters.Remove "CD": db.Parameters.Add "CD", Val(vCD), 1
        db.Parameters.Remove "DPK": db.Parameters.Add "DPK", Val(vDPK), 1
        
        CmdSql = "Select e.Cuema CUEMA, g.Sequencia_Tab SEQTAB " & _
                 "From " & IIf(vCD <> vCD_Deposito, "DEP" & vCD & ".", "") & "Item_Estoque e, " & _
                           IIf(vCD <> vCD_Deposito, "DEP" & vCD & ".", "") & "Item_Global g " & _
                 "Where e.Cod_Loja = :CD " & _
                 "And g.Cod_Dpk = :DPK " & _
                 "And e.Cod_Dpk = g.Cod_Dpk "
        
        Set db_CONS = db.dbcreatedynaset(CmdSql, 0&)
            
        'Se n�o trouxer nada, ou se cuema estiver nulo ou cuema n�o for num�rico, n�o deixa ativar
        If (db_CONS.EOF Or IsNull(db_CONS!CUEMA) Or Not IsNumeric(db_CONS!CUEMA)) Then
            grdArquivo.Col = 10
            grdArquivo.Text = "Probl.c/Cuema! "
            vFL_DPK_OK = False
        'Se Seq.tab. n�o for num�rica, n�o deixa ativar
        ElseIf Not IsNumeric(db_CONS!SEQTAB) Then
            grdArquivo.Col = 10
            grdArquivo.Text = "Probl.c/Seq.Tab.! "
            vFL_DPK_OK = False
        'Se Seq.tab. estiver nula, atribui 0 (zero) a ela
        ElseIf Not IsNull(db_CONS!CUEMA) And Not IsNull(db_CONS!SEQTAB) Then
            vSEQ_TAB_ANT = db_CONS!SEQTAB
            vCUEMA_ANT = db_CONS!CUEMA
            vFL_DPK_OK = True
        'Se estiver tudo certo, atribui campos �s vari�veis
        ElseIf Not IsNull(db_CONS!CUEMA) Then
            vSEQ_TAB_ANT = "0"
            vCUEMA_ANT = db_CONS!CUEMA
            vFL_DPK_OK = True
        End If
    End If
    

End Sub






Sub sRotina_Gravar_Ativacao()
Dim Fl_Conexao As Boolean       'Vai receber o retorno da fun��o que verifica a comunica��o com os CD�s
Dim FL_DataCad As String
Dim strLoja As String
Dim varRec As Object

On Error GoTo Trata_Erro

'Alimenta vari�veis de bind que ser�o utilizadas na grava��o das informa��es
db.Parameters.Remove "CODEDI": db.Parameters.Add "CODEDI", vCODFABR_EDI, 1
db.Parameters.Remove "DPK": db.Parameters.Add "DPK", Val(vDPK), 1
db.Parameters.Remove "FORN": db.Parameters.Add "FORN", Val(vCOD_FORN), 1
db.Parameters.Remove "CD": db.Parameters.Add "CD", Val(vCD), 1
db.Parameters.Remove "SEQ": db.Parameters.Add "SEQ", Val(vSEQ_TAB_ANT), 1
db.Parameters.Remove "CUEMA": db.Parameters.Add "CUEMA", Val(vCUEMA_ANT), 1
db.Parameters.Remove "DT": db.Parameters.Add "DT", "", 1
db.Parameters.Remove "QTDMINFORN": db.Parameters.Add "QTDMINFORN", CDbl(vQTDMINFORN), 1
db.Parameters.Remove "Cod_Errora": db.Parameters.Add "Cod_Errora", 0, 2
db.Parameters.Remove "Txt_Errora": db.Parameters.Add "Txt_Errora", "", 2
       
CmdPl = ""
CmdPlCD = ""
vFl_Erro = False
       
CmdPl = "Begin "
    
'Seleciona data que depois ser� gravada como Dt.Cadastramento na ITEM_CADASTRO
CmdPl = CmdPl & "Select To_Char(Dt_Real,'DD/MM/RR') into :DT " & _
                "From DATAS; "
    
'Grava o novo c�digo de f�brica EDI
If vFL_EDI = "S" And vCODFABR_EDI <> "" Then
        
    CmdPl = CmdPl & "Update COMPRAS.R_ITEM_EDI " & _
                    "Set Cod_Fabrica_Edi = :CODEDI " & _
                    "Where Cod_Dpk = :DPK; " & _
                    "If SQL%NOTFOUND Then " & _
                        "Insert into COMPRAS.R_ITEM_EDI " & _
                        "(Cod_Dpk, Cod_Fornecedor, Cod_Fabrica_Edi) " & _
                        "Values(:DPK, :FORN, :CODEDI); " & _
                    "End If; "
End If




'Somente vai zerar pre�o de compra, quando estiver ativando no CD Campinas -
'Mudan�a de conceito deveria ter sido feita a partir da entrada do 2o. CD - Aleth�a 22/08/03 e 02/09/03
If Val(vCD) = 1 Then
    CmdPl = CmdPl & "Update ITEM_PRECO " & _
                    "Set Preco_venda = 0, Preco_Venda_Ant = 0, Preco_Of = 0, " & _
                        "Preco_Of_Ant = 0, Preco_Sp = 0, Preco_Sp_Ant = 0 " & _
                    "Where Cod_Loja = :CD " & _
                    "And Cod_Dpk = :DPK; "
    
    CmdPl = CmdPl & "Update ITEM_GLOBAL " & _
                    "Set Sequencia_Tab = :SEQ, " & _
                        "Preco_Compra = 0, " & _
                        "Preco_Compra_Ant = 0 " & _
                    "Where Cod_Dpk = :DPK; "
Else
    'Grava no CD <> 1 (Tab.CPS), o pre�o compra ou venda que estiver no CD 1
    CmdPl = CmdPl & " Update ITEM_PRECO " & _
                        "Set (Preco_Venda, Preco_Venda_Ant, " & _
                          "   Preco_Of, Preco_Of_Ant, " & _
                          "   Preco_SP, Preco_SP_Ant) = " & _
                                "(Select Preco_Venda, Preco_Venda_Ant, " & _
                                       "    Preco_Of, Preco_Of_Ant, " & _
                                       "    Preco_SP, Preco_SP_Ant " & _
                                   "From ITEM_PRECO " & _
                                  "Where Cod_Loja = 1 And Cod_Dpk = :DPK)" & _
                      "Where Cod_Loja = :CD And Cod_Dpk = :DPK; "
    'Para a ITEM_GLOBAL, n�o vou fazer nada se for ativar em CD <> CPS, pois esta tabela n�o possui COD_LOJA
End If

CmdPl = CmdPl & "Update ITEM_ESTOQUE " & _
                "Set Situacao = 0, " & _
                    "Cuema = :CUEMA, " & _
                    "Dt_Cuema = To_Date(:DT,'DD/MM/RR') " & _
                "Where Cod_Loja = :CD " & _
                "And Cod_Dpk = :DPK; "

CmdPl = CmdPl & "Update ITEM_CADASTRO " & _
                "Set Qtd_MinForn = :QTDMINFORN "
'05/02/03 - Somente alterar dt.cadastramento se for ativar no CD 01 - CPS
If Val(vCD) = 1 Then
    CmdPl = CmdPl & ", Dt_Cadastramento = To_Date(:DT,'DD/MM/RR') "
End If
CmdPl = CmdPl & "Where Cod_Dpk = :DPK; "


'Este trecho foi inserido em Fev/2002, quando entrou em produ��o o MPV - MEDIA DE PREVISAO DE VENDAS
'Vai incluir ou alterar dados na tabela COMPRAS.MPV_PREVISAO (Campinas),
'para que esta, esteja sempre com os mesmos itens ativos da ITEM_ESTOQUE.
'Obs.: Se o item estiver sendo reativado e j� existir na tabela MPV_PREVISAO
'      (o que n�o � para acontecer pois existe 1 procedure di�ria que acerta a tab.
'      com a ITEM_ESTOQUE) mas se acontecer, vai deixar os demais campos -
'      que n�o s�o chaves, zerados ou nulos
'      E se o item n�o existir, vai inserir somente a loja e dpk,
'      com o restante zerado ou nulo.
CmdPl = CmdPl & " Update COMPRAS.MPV_PREVISAO " & _
                "    Set Qtd_Prev1 = 0, Qtd_Prev2 = 0, Qtd_Prevori = 0, " & _
                "        Tp_Modelagem = Null, Tp_Modelagem_Ant = Null, Fl_Alt_Modelagem = Null " & _
                "  Where Cod_Loja = :CD " & _
                "    And Cod_Dpk = :DPK; "
CmdPl = CmdPl & " If SQL%NOTFOUND Then " & _
                    " Insert into COMPRAS.MPV_PREVISAO " & _
                    " (Cod_Loja, Cod_Dpk, Qtd_Prev1, Qtd_Prev2, Qtd_Prevori, " & _
                    "  Tp_Modelagem, Tp_Modelagem_Ant, Fl_Alt_Modelagem) " & _
                    " Values (:CD, :DPK, 0, 0, 0, Null, Null, Null); " & _
                " End If;"


'CmdPl = CmdPl & "Commit; " & _
                "Exception When Others Then Rollback; " & _
                ":Cod_Errora := SQLCODE; " & _
                ":Txt_Errora := SQLERRM; " & _
                "End; "

'Executa transa��es em Campinas
'db.ExecuteSQL CmdPl

'If db.Parameters("Cod_Errora") <> 0 Or Trim(db.Parameters("Txt_Errora")) <> "" Then
'    MsgBox db.Parameters("Cod_Errora") & " - " & db.Parameters("Txt_Errora") & Chr(13) & _
'            "Erro nas transa��es em Campinas! Ligue p/ Depto.Sistemas."
'    vFl_Erro = True
'    Exit Sub
'End If



'Se for para ativar em outro CD diferente de 01 - Campinas, verifica o TIME-OUT
If vCD_Deposito <> vCD Then

    Fl_Conexao = fVer_Conexao(vCD)

    'Se n�o houver comunica��o, marca um "X" no grid e n�o faz nada on-line no CD
    If Fl_Conexao = False Then
        grdArquivo.Col = 10
        grdArquivo.Text = "X"
    Else
        'CmdPl = "Begin "
        
        'Grava no CD <> 1 (Tab.CPS), o pre�o compra ou venda que estiver no CD 1 e n�o mais zerar os pre�os como era o conceito anterior
        'CmdPl = CmdPl & "Update DEP" & vCD & ".ITEM_PRECO " & _
                            "Set Preco_venda = 0, Preco_Venda_Ant = 0, Preco_Of = 0, " & _
                                "Preco_Of_Ant = 0, Preco_Sp = 0, Preco_Sp_Ant = 0 " & _
                            "Where Cod_Loja = :CD " & _
                            "And Cod_Dpk = :DPK; "
                            
                            
        '+++++++++++++++++++++++++++++++++++++++++++++++++++++++
        '+++++ PROCEDURE PARA RJ PR_CAD770 (22/08/05) ++++++++++
        '+++++++++++++++++++++++++++++++++++++++++++++++++++++++
        
'''        CmdPl = CmdPl & " Update DEP" & vCD & ".ITEM_PRECO " & _
                                "Set (Preco_Venda, Preco_Venda_Ant, " & _
                                     "   Preco_Of, Preco_Of_Ant, " & _
                                     "   Preco_SP, Preco_SP_Ant) = " & _
                                     "(Select Preco_Venda, Preco_Venda_Ant, " & _
                                             "   Preco_Of, Preco_Of_Ant, " & _
                                             "   Preco_SP, Preco_SP_Ant "
'''                       CmdPl = CmdPl & " From ITEM_PRECO " & _
                                       "Where Cod_Loja = 1 And Cod_Dpk = :DPK)" & _
                              "Where Cod_Loja = :CD And Cod_Dpk = :DPK; "
        
        
        'Somente vai zerar pre�o de compra, quando estiver ativando no CD Campinas -
        'Mudan�a de conceito deveria ter sido feita a partir da entrada do 2o. CD - Aleth�a 22/08/03
        'CmdPl = CmdPl & "Update DEP" & vCD & ".ITEM_GLOBAL " & _
                            "Set Sequencia_Tab = :SEQ, " & _
                                "Preco_Compra = 0, " & _
                                "Preco_Compra_Ant = 0 " & _
                            "Where Cod_Dpk = :DPK; "
    
' +++++++++++++++++
'        CmdPl = CmdPl & "Update DEP" & vCD & ".ITEM_ESTOQUE " & _
'                            "Set Situacao = 0, " & _
'                                "Cuema = :CUEMA, " & _
'                                "Dt_Cuema = To_Date(:DT,'DD/MM/RR') " & _
'                            "Where Cod_Loja = :CD " & _
'                            "And Cod_Dpk = :DPK; "
    
'++++++++++++++++++++
       ' CmdPl = CmdPl & "Update DEP" & vCD & ".ITEM_CADASTRO " & _
       '                     "Set Qtd_MinForn = :QTDMINFORN "
                            
                            
        '05/02/03 - Somente alterar dt.cadastramento se for ativar no CD 01 - CPS
       
       ' // Flag que identifica se a data de cadastro do item deve ser alterada no CD
       
       FL_DataCad = "N"
       If Val(vCD) = 1 Then
'           CmdPl = CmdPl & ", Dt_Cadastramento = To_Date(:DT,'DD/MM/RR') "
           FL_DataCad = "A"
       End If
 '      CmdPl = CmdPl & "Where Cod_Dpk = :DPK; "
 
    db.Parameters.Remove "DATACAD"
    db.Parameters.Add "DATACAD", FL_DataCad, 1
    
    End If
    
End If
    
CmdPl = CmdPl & "Commit; " & _
                "Exception When Others Then Rollback; " & _
                ":Cod_Errora := SQLCODE; " & _
                ":Txt_Errora := SQLERRM; " & _
                "End; "
                

'Executa transa��es no CD
db.EXECUTESQL CmdPl
' // Executando procedure de Itens Remota
If vCD <> 1 Then
   ' // Parametros
   ' - COD_DPK
   ' - LOJA
   ' - SITUACAO
   ' - CUEMA
   ' - MIN FORNECEDOR
   ' - FL DE ALTERACAO DE DATA
   
   Set varRec = db.dbcreatedynaset("SELECT COUNT(*) TOTAL FROM BATCH_DEP01CDDPK.DEPOSITO WHERE COD_LOJA=:CD", 0&)
   Do While Not varRec.EOF
      If CDbl(varRec!TOTAL) > 0 Then
         strLoja = "DPK"
      Else
         strLoja = Format(vCD, "00")
      End If
      varRec.MoveNext
   Loop
   Set varRec = Nothing
   
   '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
   '18/11/08 - (EDUARDO NADIM) - Altera��o na procedure por fins de performance    '
   '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
   'db.EXECUTESQL "BEGIN producao.pr_cad770@dep01_cd" & strLoja & "(:DPK,:CD,0,:CUEMA,:QTDMINFORN ,:DATACAD); END;"
   db.EXECUTESQL "BEGIN producao.pr_cad770(:DPK,:CD,0,:CUEMA,:QTDMINFORN ,:DATACAD); END;"
            
End If

    
If Val(IIf(IsNull(db.Parameters("Cod_Errora")), 0, db.Parameters("Cod_Errora"))) <> 0 Then
    Screen.MousePointer = 0
    MsgBox db.Parameters("Cod_Errora") & " - " & db.Parameters("Txt_Errora") & Chr(13) & _
            "Erro ao ativar item no CD " & vCD & "! Ligue p/ Depto.Sistemas."
    vFl_Erro = True
End If
    
Exit Sub


Trata_Erro:
If Err.Number <> 0 Or Err.Description <> "" Then
    Screen.MousePointer = 0
    MsgBox "Erro VB: " & Err.Number & Chr(13) & _
           Err.Description & " Ligue p/ Depto. Sistemas!"
    vFl_Erro = True
    Exit Sub
End If

    
End Sub

Sub sRotina_Gravar_Desativacao()
Dim Fl_Conexao As Boolean       'Vai receber o retorno da fun��o que verifica a comunica��o com os CD�s
Dim objTransito As Object
Dim objPedFabr As Object, objPedFabr2 As Object, objQtdAtu As Object, objDeposito As Object

On Error GoTo Trata_Erro


'Alimenta vari�veis de bind que ser�o utilizadas na grava��o das informa��es
'db.Parameters.Remove "CODEDI" & I:  db.Parameters.Add "CODEDI" & I, vCODFABR_EDI, 1
'db.Parameters.Remove "SEQ" & I:     db.Parameters.Add "SEQ" & I, Val(vSEQ_TAB_ANT), 1
'db.Parameters.Remove "CUEMA" & I:   db.Parameters.Add "CUEMA" & I, Val(vCUEMA_ANT), 1
db.Parameters.Remove "DPK":      db.Parameters.Add "DPK", Val(vDPK), 1
db.Parameters.Remove "FORN":    db.Parameters.Add "FORN", Val(vCOD_FORN), 1
db.Parameters.Remove "CD":      db.Parameters.Add "CD", Val(vCD), 1
db.Parameters.Remove "MSG":     db.Parameters.Add "MSG", "", 2
db.Parameters.Remove "PEDIDOS1": db.Parameters.Add "PEDIDOS1", 0, 2
db.Parameters.Remove "PEDIDOS2": db.Parameters.Add "PEDIDOS2", 0, 2
db.Parameters.Remove "DEPOSITOS": db.Parameters.Add "DEPOSITOS", 0, 2
db.Parameters.Remove "QUANTIDADE": db.Parameters.Add "QUANTIDADE", 0, 2
db.Parameters.Remove "DT":          db.Parameters.Add "DT", "", 1
db.Parameters.Remove "Cod_Errora":    db.Parameters.Add "Cod_Errora", 0, 2
db.Parameters.Remove "Txt_Errora":   db.Parameters.Add "Txt_Errora", "", 2
       
CmdPl = ""
vFl_Erro = False

        '-------------------------------------
        'Alterado em 27/07/06 - Alethea
        '-------------------------------------
       
If vCD <> vCD_Deposito Then

    Fl_Conexao = fVer_Conexao(vCD)

    'Se n�o houver comunica��o, marca um "X" no grid e n�o faz nada on-line no CD
    If Fl_Conexao = False Then
        grdArquivo.Col = 10
        grdArquivo.Text = "X"
    Else
        
        
        'Verifica se h� estoque do item
        CmdSql = "Select Qtd_Atual As QUANTIDADE " & _
                   "From DEP" & vCD & ".ITEM_CADASTRO c, " & _
                        "DEP" & vCD & ".ITEM_ESTOQUE e " & _
                  "Where c.Cod_Dpk = :DPK " & _
                    "And e.Cod_Dpk = c.Cod_Dpk " & _
                    "And c.Cod_Fornecedor = :FORN " & _
                    "And e.Cod_Loja+0 = :CD "
        Set objQtdAtu = db.dbcreatedynaset(CmdSql, 0&)
        If objQtdAtu!QUANTIDADE <> 0 And Val(vCD) <> 6 Then
            MsgBox "Item " & vDPK & " com saldo no estoque do CD " & vCD & " , n�o pode ser desativado!", vbInformation, "Aten��o"
            Screen.MousePointer = vbDefault
            CmdPl = ""
            vFl_Erro = True
            Exit Sub
        End If
        
        'Verifica on-line nos CDs se h� pedido na f�brica para o item
        CmdSql = "Select Count(*) AS PEDIDOS1 " & _
                   "From DEP" & vCD & ".ITEM_PEDIDO_COMPRA i, " & _
                        "DEP" & vCD & ".PEDIDO_COMPRA p " & _
                  "Where p.Num_Pedido = i.Num_Pedido " & _
                    "And p.Seq_Pedido = i.Seq_Pedido " & _
                    "And p.Cod_Loja = i.Cod_Loja " & _
                    "And i.Cod_Dpk = :DPK " & _
                    "And i.Situacao = 0 " & _
                    "And p.Situacao = 0 " & _
                    "And (p.Cod_Loja = :CD OR p.Cod_Loja_Entrega = :CD)"
        Set objPedFabr = db.dbcreatedynaset(CmdSql, 0&)
        If objPedFabr!PEDIDOS1 <> 0 Then
            MsgBox "Item " & vDPK & " com pedido na f�brica para o CD " & vCD & " , n�o pode ser desativado!", vbInformation, "Aten��o"
            Screen.MousePointer = vbDefault
            CmdPl = ""
            vFl_Erro = True
            Exit Sub
        End If
        
        'CmdPl = "Begin :MSG:=''; "
        'CmdPl = CmdPl & "Select Qtd_Atual Into :QUANTIDADE " & _
                "From DEP" & vCD & ".ITEM_CADASTRO c, " & _
                     "DEP" & vCD & ".ITEM_ESTOQUE e " & _
                "Where c.Cod_Dpk = :DPK " & _
                "And e.Cod_Dpk = c.Cod_Dpk " & _
                "And c.Cod_Fornecedor = :FORN " & _
                "And e.Cod_Loja+0 = :CD; "
        'CmdPl = CmdPl & "Select Count(*) Into :PEDIDOS1 " & _
                "From DEP" & vCD & ".ITEM_PEDIDO_COMPRA i, " & _
                     "DEP" & vCD & ".PEDIDO_COMPRA p " & _
                "Where p.Num_Pedido = i.Num_Pedido " & _
                "And p.Seq_Pedido = i.Seq_Pedido " & _
                "And p.Cod_Loja = i.Cod_Loja " & _
                "And i.Cod_Dpk = :DPK " & _
                "And i.Situacao = 0 " & _
                "And p.Situacao = 0 " & _
                "And (p.Cod_Loja = :CD OR p.Cod_Loja_Entrega = :CD); "
    End If
Else
    
    'Seleciona qtd.atual da item_estoque de CPS, para o cod_loja = Cps
    'Verifica se h� estoque do item
    CmdSql = "Select Qtd_Atual AS QUANTIDADE " & _
               "From ITEM_CADASTRO c, ITEM_ESTOQUE e " & _
              "Where c.Cod_Dpk = :DPK " & _
                "And e.Cod_Dpk = c.Cod_Dpk " & _
                "And Cod_Fornecedor = :FORN " & _
                "And e.Cod_Loja+0 = :CD"
    Set objQtdAtu = db.dbcreatedynaset(CmdSql, 0&)
    If objQtdAtu!QUANTIDADE <> 0 And Val(vCD) <> 6 Then
        MsgBox "Item " & vDPK & " com saldo no estoque do CD " & vCD & " , n�o pode ser desativado!", vbInformation, "Aten��o"
        Screen.MousePointer = vbDefault
        CmdPl = ""
        vFl_Erro = True
        Exit Sub
    End If
    
    'CmdPl = "Begin :MSG:=''; "
    'CmdPl = CmdPl & "Select Qtd_Atual Into :QUANTIDADE " & _
            "From ITEM_CADASTRO c, ITEM_ESTOQUE e " & _
            "Where c.Cod_Dpk = :DPK " & _
            "And e.Cod_Dpk = c.Cod_Dpk " & _
            "And Cod_Fornecedor = :FORN " & _
            "And e.Cod_Loja+0 = :CD; "
   
End If
   
   
   
'Seleciona qtd. de registros (CD's) ativos da item_estoque de Cps, quando cod_loja <> 1 e 6
'Seleciona qtd. de pedidos em aberto nas tabs. de Cps, com cod_loja = CD escolhido
'Se a qtd. de registros (CD's) da item_estoque for >0 e CD for diferente de 6 (verifica se ainda existem itens ativos)
'CmdPl = CmdPl & "Select Count(*) Into :DEPOSITOS " & _
                "From ITEM_ESTOQUE " & _
                "Where Cod_Dpk = :DPK " & _
                "And Situacao = 0 " & _
                "And Cod_Loja <> 1 " & _
                "And Cod_Loja <> 6; "

'Verifica se est� querendo desativar Campinas  - N�o deixa se ainda existirem outros CDs com situa��o de ativo
CmdSql = "Select Count(*) AS DEPOSITOS " & _
           "From ITEM_ESTOQUE " & _
          "Where Cod_Dpk = :DPK " & _
            "And Situacao = 0 " & _
            "And Cod_Loja <> 1 " & _
            "And Cod_Loja <> 6 "
Set objDeposito = db.dbcreatedynaset(CmdSql, 0&)
If objDeposito!DEPOSITOS <> 0 And Val(vCD) = 1 Then
    MsgBox "Item " & vDPK & " nao foi desativado nos demais depositos, n�o pode ser desativado em Campinas!", vbInformation, "Aten��o"
    Screen.MousePointer = vbDefault
    CmdPl = ""
    vFl_Erro = True
    Exit Sub
End If


'Verifica no banco PRODUCAO se h� pedido na f�brica para o item
CmdSql = "Select Count(*) AS PEDIDOS2 " & _
           "From ITEM_PEDIDO_COMPRA i, PEDIDO_COMPRA p " & _
          "Where p.Num_Pedido = i.Num_Pedido " & _
            "And p.Seq_Pedido = i.Seq_Pedido " & _
            "And p.Cod_Loja = i.Cod_Loja " & _
            "And i.Cod_Dpk = :DPK " & _
            "And i.Situacao = 0 " & _
            "And p.Situacao = 0 " & _
            "And (p.Cod_Loja = :CD OR p.Cod_Loja_Entrega = :CD)"
Set objPedFabr2 = db.dbcreatedynaset(CmdSql, 0&)
If objPedFabr2!PEDIDOS2 <> 0 Then
    MsgBox "Item " & vDPK & " com pedido na f�brica para o CD " & vCD & " , n�o pode ser desativado!", vbInformation, "Aten��o"
    Screen.MousePointer = vbDefault
    CmdPl = ""
    vFl_Erro = True
    Exit Sub
End If

'CmdPl = CmdPl & "Select Count(*) Into :PEDIDOS2 " & _
                "From ITEM_PEDIDO_COMPRA i, PEDIDO_COMPRA p " & _
                "Where p.Num_Pedido = i.Num_Pedido " & _
                "And p.Seq_Pedido = i.Seq_Pedido " & _
                "And p.Cod_Loja = i.Cod_Loja " & _
                "And i.Cod_Dpk = :DPK " & _
                "And i.Situacao = 0 " & _
                "And p.Situacao = 0 " & _
                "And (p.Cod_Loja = :CD OR p.Cod_Loja_Entrega = :CD); "
    
'CmdPl = CmdPl & "If :QUANTIDADE > 0 And :CD <> 6 " & _
'                "Then :MSG:=' Item com saldo em estoque no deposito ' || :CD; " & _
'                "Else "
'CmdPl = CmdPl & _
                    "If (:PEDIDOS1 + :PEDIDOS2) > 0 " & _
                    "Then :MSG:=' Item com pendencia na fabrica do deposito '|| :CD; " & _
                    "Else "
'CmdPl = CmdPl & _
                        "If :CD = 1 AND :DEPOSITOS > 0 " & _
                        "Then :MSG:=' Item nao foi desativado nos depositos!'; " & _
                        "Else " & _
                            "Update ITEM_ESTOQUE " & _
                            "Set Situacao = 9 " & _
                            "Where cod_dpk = :DPK " & _
                            "And cod_loja = :CD " & _
                            "And qtd_atual = 0; "
                            
                            
'****************
    'Verifica se item est� em tr�nsito para o cd em quest�o
    CmdSql = "Select SUM(Compras.F_TOTALTRANSFERENCIA(E.Cod_Dpk, L.Cod_Loja)) SOMA " & _
               "From Loja L, ITEM_ESTOQUE E " & _
              "Where E.Cod_Dpk = :DPK " & _
                "And L.Cod_Loja = :CD " & _
                "And L.Cod_Loja = E.Cod_Loja"
               
    Set objTransito = db.dbcreatedynaset(CmdSql, 0&)
                    
    If objTransito!SOMA <> 0 Then
        MsgBox "Item " & vDPK & " em tr�nsito n�o pode ser desativado!", vbInformation, "Aten��o"
        Screen.MousePointer = vbDefault
        CmdPl = ""
        vFl_Erro = True
        Exit Sub
    End If
'****************
                            
                            
                            
'Se estiver tudo ok, faz update na situa��o da ITEM_ESTOQUE
CmdPl = "Begin "
CmdPl = CmdPl & "Update ITEM_ESTOQUE " & _
                   "Set Situacao = 9 " & _
                 "Where cod_dpk = :DPK " & _
                   "And cod_loja = :CD " & _
                   "And qtd_atual = 0; "

If vCD <> vCD_Deposito And vCD <> 6 Then
    CmdPl = CmdPl & "Update DEP" & vCD & ".ITEM_ESTOQUE " & _
                       "Set Situacao = 9 " & _
                     "Where Cod_Dpk = :DPK " & _
                       "And Cod_Loja = :CD " & _
                       "And Qtd_Atual = 0; "
End If

'Somente vai zerar pre�o de compra, quando estiver ativando no CD Campinas -
'Mudan�a de conceito deveria ter sido feita a partir da entrada do 2o. CD - Aleth�a 22/08/03
'Neste trecho de desativar, o conceito j� estava correto

If vCD = 1 Then
    CmdPl = CmdPl & "Update ITEM_GLOBAL " & _
                       "Set Preco_Compra = 0, Preco_Compra_Ant = 0 " & _
                     "Where Cod_Dpk = :DPK; "
    
    CmdPl = CmdPl & "Delete From COMPRAS.R_ITEM_EDI " & _
                     "Where Cod_Dpk = :DPK; "
End If


'CmdPl = CmdPl & _
                            "If :CD = 1 " & _
                            "Then Update ITEM_GLOBAL " & _
                                 "Set Preco_Compra = 0, Preco_Compra_Ant = 0 " & _
                                 "Where Cod_Dpk = :DPK; "
'CmdPl = CmdPl & _
                                 "Delete From COMPRAS.R_ITEM_EDI " & _
                                 "Where Cod_Dpk = :DPK; " & _
                            "End If; " & _
                        "End If; " & _
                    "End If; " & _
                "End If; "

CmdPl = CmdPl & "Commit; " & _
                "Exception When Others Then Rollback; " & _
                ":Cod_Errora := SQLCODE; " & _
                ":Txt_Errora := SQLERRM; " & _
                "End; "
    
'Executa transa��es
db.EXECUTESQL CmdPl

If Val(IIf(IsNull(db.Parameters("Cod_Errora")), 0, db.Parameters("Cod_Errora"))) <> 0 Then
    Screen.MousePointer = 0
    MsgBox db.Parameters("Cod_Errora") & " - " & db.Parameters("Txt_Errora") & Chr(13) & _
            "Erro ao desativar item no CD " & vCD & "! Ligue p/ Depto.Sistemas."
    vFl_Erro = True
    CmdPl = ""
Else
    If Trim(db.Parameters("MSG")) <> "" Then
        MsgBox "N�o posso Gravar. " & db.Parameters("MSG"), vbCritical, "Aten��o!"
        vFl_Erro = True
        CmdPl = ""
    End If
End If


Exit Sub


Trata_Erro:

If Err.Number <> 0 Or Err.Description <> "" Then
    Screen.MousePointer = 0
    MsgBox "Erro VB: " & Err.Number & Chr(13) & _
           Err.Description & " Ligue p/ Depto. Sistemas!"
    CmdPl = ""
    vFl_Erro = True
    Exit Sub
End If

End Sub


Private Sub cmdArquivo_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp1.Visible = True
End Sub

Private Sub cmdConfirma_Click()

Dim vObs As String            'Guarda o conte�do da coluna de Observa��es
Dim vReg_Gravado As Long      'Guarda a qtd. de itens gravados
Dim vReg_Nao_Gravado As Long      'Guarda a qtd. de itens n�o gravados
Dim vLinSemObs As Long        'Guarda a qtd.de linhas sem observa��o
Dim objconexao As Object      'Guarda resultado do select na LOJA_CONEXAO



If MsgBox("Confirma altera��o da Situa��o dos Itens?", vbYesNo, "CAD760") = vbYes Then

    Screen.MousePointer = 11

    'Verifica se o grid n�o est� em branco
    grdArquivo.Row = 1
    grdArquivo.Col = 0
    If grdArquivo.Text = "" Then
        Screen.MousePointer = 0
        MsgBox "Nenhum dado poder� ser alterado!", vbCritical, "Aten��o!"
        Exit Sub
    End If

    'Primeiro vai percorrer todo o grid p/ ver se tem itens a serem gravados
    CmdPl = ""
    vLinSemObs = 0
    For I = 1 To grdArquivo.Rows - 1
        grdArquivo.Row = I
        grdArquivo.Col = 10
        vObs = grdArquivo.Text
        'Se a coluna de Observa��o n�o contiver nenhuma mensagem de inconsist�ncia vai gravar
        If vObs = "" Then
            vLinSemObs = vLinSemObs + 1
        End If
    Next I
    If vLinSemObs = 0 Then
        Screen.MousePointer = 0
        MsgBox "Nenhum dado poder� ser alterado!", vbCritical, "Aten��o!"
        Exit Sub
    End If
    
    'Se houverem itens a serem gravados, vai iniciar todo o processo
    'Zera contador de linhas gravadas
    vReg_Gravado = 0

    'Pega linha a linha do grid para gravar as que cont�m campo de Observa��o <> ""
    For I = 1 To grdArquivo.Rows - 1
        
        grdArquivo.Row = I
        grdArquivo.Col = 10
        vObs = Trim(grdArquivo.Text)
        
        'Se a coluna de Observa��o n�o contiver nenhuma mensagem de inconsist�ncia vai gravar
        If vObs = "" Then
            
            'Alimenta vari�veis com os dados do grid
            grdArquivo.Col = 1
            vCD = Format(Trim(grdArquivo.Text), "00")
            grdArquivo.Col = 2
            vDPK = Trim(grdArquivo.Text)
            grdArquivo.Col = 3
            vSIT_DPK_ARQ = Trim(grdArquivo.Text)
            grdArquivo.Col = 4
            vCOD_FORN = Trim(grdArquivo.Text)
            grdArquivo.Col = 5
            vCODFABR_EDI = Trim(grdArquivo.Text)
            grdArquivo.Col = 6
            vSEQ_TAB_ANT = Trim(grdArquivo.Text)
            grdArquivo.Col = 7
            vCUEMA_ANT = Trim(grdArquivo.Text)
            grdArquivo.Col = 8
            vFL_EDI = Trim(grdArquivo.Text)
            grdArquivo.Col = 9
            vQTDMINFORN = Trim(grdArquivo.Text)
            
            'L� a situa��o para qual o item ser� alterado
            grdArquivo.Col = 3
            If Val(grdArquivo.Text) = 0 Then
                Call sRotina_Gravar_Ativacao
            Else
                Call sRotina_Gravar_Desativacao
            End If
            
            'Se a grava��o de um item voltar com erro, p�ra a execu��o
            If vFl_Erro = True Then
                vReg_Nao_Gravado = vReg_Nao_Gravado + 1
            Else
                vReg_Gravado = vReg_Gravado + 1
            End If
        
        End If
            
            
    Next I

    Screen.MousePointer = 0
    MsgBox vReg_Gravado & IIf(vReg_Gravado = 1, " Item teve ", " Itens tiveram ") & "sua situa��o alterada!", vbInformation, "Aten��o!"
    lblMsg2.Visible = True
    lblMsg2 = vReg_Gravado & IIf(vReg_Gravado = 1, " Item Alterado!", " Itens Alterados!")
    lblMsg3.Visible = True
    lblMsg3 = vReg_Nao_Gravado & IIf(vReg_Nao_Gravado = 1, " Item N�o Alterado!", " Itens N�o Alterados!")
    
End If

End Sub
Private Sub cmdConfirma_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp2.Left = 6570
    lblHelp2.Top = 7260
    lblHelp2.Visible = True
    lblHelp2.Caption = "Gravar Dados"
End Sub


Private Sub cmdLimpar_Click()
    Call LIMPA_GRID
End Sub

Private Sub cmdLog_Click()

Dim J As Long           'Ponteiro de caracter no campo observa��o
Dim vTamObs As Long     'Guarda a qtd. de caracteres da coluna Observa��o
Dim sOBS As String      'Guarda o conte�do da coluna de Observa��es
Dim sCODEDI As String   'Guarda o c�digo de f�br. EDI para impress�o
Dim sDPK As String      'Guarda o c�digo de DPK para impress�o
Dim sSIT As String      'Guarda a situa��o para o qual o Item deveria ficar no CD
Dim sCD As String       'Guarda o c�digo loja para impress�o
Dim sQTDMINFORN As String   'Guarda a qtd.m�n.do forn.
    
    
If MsgBox("Confirma gera��o do arquivo de log?", vbYesNo, "CAD750") = vbYes Then

    'Gera arquivo de log com o nome LODDMMAA.TXT
    Open "C:\CAD770\LOG\Lo" & Mid(vDt_Cad, 1, 2) & _
                        Mid(vDt_Cad, 4, 2) & Mid(vDt_Cad, 7, 2) & ".txt" For Output As #1
    'Open "C:\Arquivos-Alethea\CAD770\LOG\Lo" & Mid(vDt_Cad, 1, 2) & _
                        Mid(vDt_Cad, 4, 2) & Mid(vDt_Cad, 7, 2) & ".txt" For Output As #1
    
    Print #1, "LOG DO ARQUIVO: " & vNomeArq
    Print #1, ""
    Print #1, "CD DPK   Sit C�digo F�brica EDI             Qt.Min.For Observa��es"
    Print #1, "-- ----- --- ------------------------------ ---------- -------------------------"
    
   'Print #1, "CD DPK   Sit C�digo F�brica EDI             Observa��es"
   'Print #1, "-- ----- --- ------------------------------ ------------------------------------"
   'Print #1, "DPK   Forn  Fabrica             Observa��es"
   'Print #1, "----- ----  ------------------- ------------------------------------------------------"
    
    
    '          01  54357 9-Desat.
    '                    0-Ativ.
    
    'Pega linha a linha do grid para gravar as que cont�m campo de Observa��o <> ""
    For I = 1 To grdArquivo.Rows - 1
        
        grdArquivo.Row = I
        
        grdArquivo.Col = 1
        sCD = Trim(grdArquivo.Text)        'Pega o c�digo loja
        grdArquivo.Col = 2
        sDPK = Trim(grdArquivo.Text)       'Pega o c�digo do DPK
        grdArquivo.Col = 3
        sSIT = Trim(grdArquivo.Text)       'Pega a situa��o do item
        grdArquivo.Col = 4
        sCODEDI = Trim(grdArquivo.Text)     'Pega o c�digo de f�br. EDI
        grdArquivo.Col = 9
        sQTDMINFORN = Trim(grdArquivo.Text) 'Pega a qtd.min.forn
        grdArquivo.Col = 10
        sOBS = Trim(grdArquivo.Text)        'Pega as Observa��es
        
        'Se a coluna de Observa��o n�o estiver em branco
        If sOBS <> "" Then
            
            'Zero ponteiro que vai me indicar que caracter no campo obs. estou lendo
            J = 0
            'Pego o tamanho da observa��o para esta linha
            vTamObs = CLng(Len(sOBS))
            
            'Se o campo de observa��o contiver mais de 36 caracteres, o arquivo de log ter�
            'que conter mais de 1 linha de mensagens para aquele item
            'Conto 36 pois antes da Obs., a 1a. linha ir� conter o CD-DPK  -SIT-C�d.F�br.EDI...(44 caracteres)
            'E para a impress�o correta do arquivo s� pode ter 80 colunas
            
            'Se for menor ou igual a 25, gravo a linha no arquivo normalmente
            If vTamObs <= 25 Then
                Print #1, String(2 - Len(sCD), " ") & sCD & " " & _
                          String(5 - Len(sDPK), " ") & sDPK & "  " & _
                          sSIT & Space(2 - Len(sSIT)) & " " & _
                          sCODEDI & Space(30 - Len(sCODEDI)) & " " & _
                          String(10 - Len(sQTDMINFORN), " ") & sQTDMINFORN & " " & sOBS
            'Sen�o
            Else
                'Imprimo os primeiros 25 caracteres
                Print #1, String(2 - Len(sCD), " ") & sCD & " " & _
                          String(5 - Len(sDPK), " ") & sDPK & "  " & _
                          sSIT & Space(2 - Len(sSIT)) & " " & _
                          sCODEDI & Space(30 - Len(sCODEDI)) & " " & _
                          String(10 - Len(sQTDMINFORN), " ") & sQTDMINFORN & " " & _
                          Mid(sOBS, 1, 25)
                
                'Tamanho agora vai ser igual ao tamanho original - 25
                vTamObs = vTamObs - 25
                'Indico de que caracter vou come�ar a ler no cpo. Observa��o
                J = J + 26
                
                'Enquanto o tamanho for maior que 0
                Do While vTamObs > 0
                    'Se tamanho maior que 25, imprime os pr�ximos 25 caracteres e atualiza tam.e ponteiro
                    If vTamObs > 25 Then
                        Print #1, Space(55) & Mid(sOBS, J, 25)
                        vTamObs = vTamObs - 25
                        J = J + 25
                    'Se tamanho menor que 25, imprime os caracteres que faltam
                    Else
                        Print #1, Space(55) & Mid(sOBS, J, vTamObs)
                        vTamObs = 0
                    End If
                Loop
            End If
            
        End If
            
    Next I

    Print #1, ""
    Print #1, "***************** FIM DO ARQUIVO DE LOG ***********************"
    
    Close #1

    MsgBox "Arquivo gerado com sucesso!", vbInformation, "Aten��o!"


End If

End Sub

Private Sub cmdLog_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp2.Left = 4695
    lblHelp2.Top = 7260
    lblHelp2.Visible = True
    lblHelp2.Caption = "Gerar Arq.de Log"
End Sub


Private Sub cmdVoltar_Click()
    Unload Me
End Sub

Private Sub cmdVoltar_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp2.Left = 8820
    lblHelp2.Top = 7260
    lblHelp2.Visible = True
    lblHelp2.Caption = "Voltar"
End Sub


Private Sub Form_Load()
    
    grdArquivo.Visible = False
    lblHelp1.Visible = False
    lblHelp2.Visible = False
    lblMsg.Visible = False
    lblMsg2.Visible = False
    lblMsg3.Visible = False

End Sub



Private Sub lblHelp1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp1.Visible = False
End Sub


Private Sub cmdArquivo_Click()
    
    'Vari�veis locais
    Dim vCD_ARQ As String       'Vai guardar o CD que todas as linhas do arquivo dever�o ter
    Dim objEDI As Object
    'OBS: Dever� ser feito um arquivo para cada CD
    
   ' on error GoTo Trata_Erro
    
    Screen.MousePointer = 11
    Call LIMPA_GRID
    
    'Indica que o controle ser� usado para abrir o arquivo
    controle.ShowOpen
    lblArquivo.Caption = controle.filename
    vNomeArq = lblArquivo

    'Verifica se foi indicado o diret�rio e nome do arquivo a ser lido
    If lblArquivo.Caption = "" Then
        MsgBox "Indique o arquivo a ser aberto", , MSG_TIT
        Exit Sub
    End If


    '/////////////////////////
    'MONTA O GRID
    '/////////////////////////
    grdArquivo.Visible = True
    grdArquivo.Row = 0
    
    grdArquivo.Col = 0
    grdArquivo.ColWidth(0) = 450
    grdArquivo.Text = " Reg"
    
    grdArquivo.Col = 1
    grdArquivo.ColWidth(1) = 350
    grdArquivo.ColAlignment(1) = 2
    grdArquivo.Text = " CD"
    
    grdArquivo.Col = 2
    grdArquivo.ColWidth(2) = 500
    grdArquivo.ColAlignment(2) = 2
    grdArquivo.Text = " Dpk"
    
    grdArquivo.Col = 3
    grdArquivo.ColWidth(3) = 450
    grdArquivo.ColAlignment(3) = 2
    grdArquivo.Text = " Sit"
    
    grdArquivo.Col = 4
    grdArquivo.ColWidth(4) = 450
    grdArquivo.ColAlignment(4) = 2
    grdArquivo.Text = " Forn"
    
    grdArquivo.Col = 5
    grdArquivo.ColWidth(5) = 2800
    grdArquivo.ColAlignment(5) = 2
    grdArquivo.Text = "       Cod.Fabr.EDI"
    
    grdArquivo.Col = 6
    grdArquivo.ColWidth(6) = 700
    grdArquivo.ColAlignment(6) = 2
    grdArquivo.Text = "Seq.Tab"
    
    grdArquivo.Col = 7
    grdArquivo.ColWidth(7) = 1200
    grdArquivo.ColAlignment(7) = 1
    grdArquivo.Text = "      Cuema "
    
    grdArquivo.Col = 8
    grdArquivo.ColWidth(8) = 350
    grdArquivo.ColAlignment(8) = 2
    grdArquivo.Text = "EDI"
    
    grdArquivo.Col = 9
    grdArquivo.ColWidth(9) = 1150
    grdArquivo.ColAlignment(9) = 2
    grdArquivo.Text = "Qt.Min.Forn"
    
    grdArquivo.Col = 10
    grdArquivo.ColWidth(10) = 10000
    grdArquivo.Text = "Observa��o"
    
    grdArquivo.Col = 11
    grdArquivo.ColWidth(11) = 1650
    grdArquivo.ColAlignment(11) = 2
    grdArquivo.Text = "CD S/Comunica��o"
    
    
    
    '//////////////////////////
    'LEITURA DO ARQUIVO
    '//////////////////////////
    Open vNomeArq For Input As #1
    
    'Inicializa contadores de linhas do grid
    I = 0
    lblMsg.Visible = True
    
    'L� enquanto n�o for final de arquivo
    Do While Not EOF(1)
    
        I = I + 1
        
        lblMsg = "CARREGANDO ...." & I
        lblMsg.Refresh
        
        'Inicializa flags e vari�veis
        'vStrLinhaArq = ""
        vFL_EDI = ""
        vCOD_FORN = 0
        vCD = ""
        vDPK = ""
        vSIT_DPK_ARQ = ""
        vSIT_DPK_ANT = ""
        vSEQ_TAB_ANT = ""
        vCUEMA_ANT = ""
        vCODFABR_EDI = ""
        vFL_DPK_OK = True
        
        
        'L� registro - linha - do arquivo, joga na vari�vel  e seta ponteiro para o pr�ximo
        Line Input #1, varLinhaArquivo
        'Se linha lida for em branco, sai do loop
        If Mid(varLinhaArquivo, 1, 48) = "" Then
            Exit Do
        End If
        
        'Qtd. Linhas do grid ser� correspondente � qtd. de linhas lidas do arquivo texto
        grdArquivo.Rows = I + 1
        grdArquivo.Row = I
        
        '1a Coluna vai receber a numera��o de registros lidos do arquivo
        grdArquivo.Col = 0
        grdArquivo.Text = Format(I, "0000")
        
        'Atribui valores da linha do arquivo para as vari�veis
        vCD = Format(Trim(Mid(varLinhaArquivo, 1, 2)), "00")
        'Se estiver lendo a primeira linha, guarda o CD na vari�vel para comparar com o CD das pr�ximas linhas
        If I = 1 Then
            vCD_ARQ = Format(Trim(Mid(varLinhaArquivo, 1, 2)), "00")
        End If
        vDPK = Trim(Mid(varLinhaArquivo, 3, 5))
        vSIT_DPK_ARQ = Trim(Mid(varLinhaArquivo, 8, 1))
        vCODFABR_EDI = Trim(Mid(varLinhaArquivo, 9, 30))
        vQTDMINFORN = Trim(Mid(varLinhaArquivo, 39, 10))
        
        'Valida��es gerais com o CD, DPK e SITUA��O que vieram no arq., verificando se n�o est�o inv�lidos
        If vCD = "" Or Val(vCD) = 0 Then
            grdArquivo.Col = 10
            grdArquivo.Text = "CD Inv�lido! "
            vFL_DPK_OK = False
        'Se estiver lendo linha do arq., maior que a 1a. verifica se o CD � igual ao da 1a. linha
        ElseIf I > 1 And vCD <> vCD_ARQ Then
            grdArquivo.Col = 10
            grdArquivo.Text = "CD Diferente do CD da 1a. Linha! "
            vFL_DPK_OK = False
        ElseIf vDPK = "" Or Val(vDPK) = 0 Then
            grdArquivo.Col = 10
            grdArquivo.Text = "DPK Inv�lido! "
            vFL_DPK_OK = False
        ElseIf (vSIT_DPK_ARQ = "") Or (vSIT_DPK_ARQ <> "0" And vSIT_DPK_ARQ <> "9") Then
            grdArquivo.Col = 10
            grdArquivo.Text = "Situa��o Inv�lida! "
            vFL_DPK_OK = False
        Else
            
            'Se for ativar item e qtd.min.forn > 99999999.9, ou zero ou branco, coloca msg. no cpo. Observa��o
            If vSIT_DPK_ARQ = "0" Then
                If (CDbl(vQTDMINFORN) > 99999999.9 Or CDbl(vQTDMINFORN) = 0 Or vQTDMINFORN = "") Then
                    grdArquivo.Col = 10
                    grdArquivo.Text = "Qt.Min.Forn. Inv.!"
                    vFL_DPK_OK = False
                End If
            End If
            
            'Valida��o Geral do CD e DPK na ITEM_ESTOQUE de Campinas
            'Selecionando Situa��o do Item e do Fornecedor, C�d.Forn. e Flag EDI
            db.Parameters.Remove "CD": db.Parameters.Add "CD", Val(vCD), 1
            db.Parameters.Remove "DPK": db.Parameters.Add "DPK", Val(vDPK), 1
            
            CmdSql = "Select F.Cod_Fornecedor, F.Fl_Edi, F.Situacao Sit_Forn, " & _
                            "E.Situacao Sit_Dpk " & _
                     "From Item_Cadastro C, Item_Estoque E, Fornecedor F " & _
                     "Where E.Cod_Loja = :CD " & _
                     "And E.Cod_Dpk = :DPK " & _
                     "And E.Cod_Dpk = C.Cod_Dpk " & _
                     "And C.Cod_Fornecedor = F.Cod_Fornecedor"
            
            Set db_CONS = db.dbcreatedynaset(CmdSql, 0&)
                
            'Se select n�o trouxer nada, h� problema com o DPK ou com o CD
            If db_CONS.EOF Then
                grdArquivo.Col = 10
                grdArquivo.Text = "Probl.c/CD ou DPK! "
                vFL_DPK_OK = False
            Else
                
                'Atribui � var., a situa��o anterior do DPK
                vSIT_DPK_ANT = db_CONS!Sit_Dpk
                
                'Se a situa��o do DPK no arquivo ou no banco, estiver substituido, n�o faz nada
                If Val(vSIT_DPK_ARQ) = 8 Or Val(vSIT_DPK_ANT) = 8 Then
                    grdArquivo.Col = 10
                    grdArquivo.Text = "DPK Substitu�do! "
                    vFL_DPK_OK = False
                'Sen�o, Se a situa��o do DPK for a mesma que veio no arquivo, significa que n�o � necess�rio fazer nada p/ este DPK
                ElseIf Val(vSIT_DPK_ARQ) = Val(vSIT_DPK_ANT) Then
                    grdArquivo.Col = 10
                    grdArquivo.Text = "DPK j� est� " & IIf(Val(vSIT_DPK_ARQ) = 0, "Ativo! ", "Desativado! ")
                    vFL_DPK_OK = False
                'Sen�o, Se quiser ativar o item mas o Fornecedor estiver desativado, n�o deixa fazer
                ElseIf Val(vSIT_DPK_ARQ) = 0 And Val(db_CONS!Sit_Forn) = 9 Then
                    grdArquivo.Col = 10
                    grdArquivo.Text = "Forn. Desativado! "
                    vFL_DPK_OK = False
                'Se for Ativar e o fornecedor for EDI e o c�digo de F�brica EDI estiver em branco, n�o pode ativar
                ElseIf (db_CONS!Fl_Edi = "S" And vCODFABR_EDI = "" And vSIT_DPK_ARQ = 0) Then
                    grdArquivo.Col = 10
                    grdArquivo.Text = "C�d.F�br.EDI Inv�lido! "
                    vFL_DPK_OK = False
                'Sen�o, continua demais valida��es
                Else
                    vFL_EDI = db_CONS!Fl_Edi
                    vCOD_FORN = db_CONS!Cod_Fornecedor
                    vFL_DPK_OK = True
                End If
            
            End If
        
        End If
        
        
        'Verifica se C�d.Fabr.EDI/Fornecedor j� est� cadastrado para outro DPK
        If db_CONS!Fl_Edi = "S" And vCODFABR_EDI <> "" Then
            db.Parameters.Remove "CODFORN": db.Parameters.Add "CODFORN", Val(vCOD_FORN), 1
            db.Parameters.Remove "CODFABREDI": db.Parameters.Add "CODFABREDI", vCODFABR_EDI, 1
            db.Parameters.Remove "DPK": db.Parameters.Add "DPK", Val(vDPK), 1
            
            CmdSql = "SELECT cod_dpk " & _
                      "FROM compras.r_item_edi " & _
                      "WHERE cod_fornecedor = :CODFORN " & _
                      "AND cod_fabrica_edi = :CODFABREDI " & _
                      "AND cod_dpk <> :DPK"
            Set objEDI = db.dbcreatedynaset(CmdSql, 0&)
            
            If Not objEDI.EOF Then
                grdArquivo.Col = 10
                grdArquivo.Text = "C�dF�br/Forn j� cad.p/outro DPK! "
                vFL_DPK_OK = False
            End If
        End If
        
        
        'Se as valida��es acima estiverem corretas, continua; sen�o l� pr�xima linha do arq.
        If vFL_DPK_OK = True Then
            If vSIT_DPK_ARQ = "0" Then
                Call sRotina_Ativar_Item
            'Para desativar, n�o precisa fazer nenhuma outra valida��o, somente testar conex�o
            End If
        End If
        
        'Alimenta o grid com as informa��es do arquivo, independente de estarem corretas ou n�o
        grdArquivo.Col = 1
        grdArquivo.Text = Format(vCD, "00")
        grdArquivo.Col = 2
        grdArquivo.Text = vDPK
        grdArquivo.Col = 3
        grdArquivo.Text = vSIT_DPK_ARQ
        grdArquivo.Col = 4
        grdArquivo.Text = IIf(vCOD_FORN = "0", "", Format(vCOD_FORN, "000"))
        grdArquivo.Col = 5
        grdArquivo.Text = vCODFABR_EDI
        grdArquivo.Col = 6
        grdArquivo.Text = Format(vSEQ_TAB_ANT, "000000")
        grdArquivo.Col = 7
        grdArquivo.Text = Format(vCUEMA_ANT, "00000000.0000")
        grdArquivo.Col = 8
        grdArquivo.Text = vFL_EDI
        grdArquivo.Col = 9
        grdArquivo.Text = Format(vQTDMINFORN, "#######0.0")
    
    'L� pr�xima linha
    Loop
    
    Screen.MousePointer = 0
    MsgBox "Arquivo Lido!", vbInformation, "Aten��o!"
    lblMsg = I & " Linhas Lidas do Arquivo!"
    lblMsg.Refresh
    
    Close #1    ' Close file.
    Screen.MousePointer = 0
        
    Exit Sub
        
        
Trata_Erro:
    
    If Err.Number = 53 Then
        MsgBox "Arquivo de Controle n�o Encontrado. ", , MSG_TIT
    Else
        Call Process_Line_Errors
    End If

End Sub
Private Sub lblHelp2_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp2.Visible = False
End Sub





Private Sub SSCommand1_Click()

End Sub

Private Sub SSFrame2_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp1.Visible = False
    lblHelp2.Visible = False
End Sub


Private Sub SSFrame3_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp1.Visible = False
End Sub



































