-- Procedure para fazer a parte de ativa��o on-line do item no cd que foi enviado no arquivo - 22/08/05 - J�nior/Alethea
create or replace procedure producao.pr_cad770
(pmCodDpk in producao.item_estoque.cod_dpk%type,
 pmCodLoja in producao.loja.cod_loja%type,
 pmSituacao in number,
 pmCuema in producao.item_estoque.cuema%type,
 pmQtdMinForn in producao.item_cadastro.qtd_minforn%type,
 pmFlagDataCad in char)  -- // A - Ativa / D - Desativar
 
 is
 varDtReal date;
 cursor c_cursor is
 Select Preco_Venda, Preco_Venda_Ant, Preco_Of, Preco_Of_Ant,Preco_SP, Preco_SP_Ant
   From ITEM_PRECO@lnk_producao 
  Where Cod_Loja = 1 And Cod_Dpk = pmCodDpk;
        
 begin
  
  -- // Atualizando ITEM_PRECO
 select dt_real into varDtReal from datas;
 
  
  for xx in c_cursor loop
  Update producao.ITEM_PRECO
  Set Preco_Venda = xx.preco_venda, 
      Preco_Venda_Ant=xx.preco_venda_ant,
      Preco_Of=xx.preco_of, 
      Preco_Of_Ant=xx.preco_of_ant, 
      Preco_SP=xx.preco_sp, 
      Preco_SP_Ant=xx.preco_sp_ant
  Where Cod_Loja = pmCodLoja And Cod_Dpk = pmCodDPk;
  end loop;
  
  -- // Atualizando Item Estoque
  
  Update producao.ITEM_ESTOQUE
  Set Situacao = 0, 
      Cuema = pmCuema,
      Dt_Cuema = varDtReal
Where Cod_Loja = pmCodLoja and
      Cod_Dpk = pmCodDpk;

 if pmFlagDataCad = 'A' then
    update producao.item_cadastro
    set qtd_minforn = pmQtdMinForn,
    dt_cadastramento = varDtReal
    where
    cod_dpk = pmCodDpk;
 else
    update producao.item_cadastro
     set qtd_minforn = pmQtdMinForn
     where
    cod_dpk = pmCodDpk;
 end if  ;

 commit;
 
 end;
/