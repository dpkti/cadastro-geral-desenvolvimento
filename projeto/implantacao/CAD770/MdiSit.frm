VERSION 4.00
Begin VB.MDIForm mdiSituacao 
   BackColor       =   &H00C0C0C0&
   Caption         =   "CAD770 - Altera��o Autom�tica da Situa��o de Itens DPK por Leitura de Arquivo"
   ClientHeight    =   8490
   ClientLeft      =   585
   ClientTop       =   2370
   ClientWidth     =   11220
   Height          =   9180
   Left            =   525
   LinkTopic       =   "MDIForm1"
   Top             =   1740
   Width           =   11340
   Begin Threed.SSPanel SSPanel2 
      Align           =   1  'Align Top
      Height          =   8025
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   11220
      _Version        =   65536
      _ExtentX        =   19791
      _ExtentY        =   14155
      _StockProps     =   15
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin Threed.SSFrame SSFrame1 
         Height          =   1095
         Left            =   135
         TabIndex        =   1
         Top             =   225
         Width           =   9930
         _Version        =   65536
         _ExtentX        =   17515
         _ExtentY        =   1931
         _StockProps     =   14
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Begin Threed.SSCommand cmdSair 
            Height          =   615
            Left            =   1470
            TabIndex        =   3
            Top             =   270
            Width           =   735
            _Version        =   65536
            _ExtentX        =   1296
            _ExtentY        =   1085
            _StockProps     =   78
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Picture         =   "MdiSit.frx":0000
         End
         Begin Threed.SSCommand cmdCadastrar 
            Height          =   615
            Left            =   375
            TabIndex        =   2
            Top             =   270
            Width           =   735
            _Version        =   65536
            _ExtentX        =   1296
            _ExtentY        =   1085
            _StockProps     =   78
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Picture         =   "MdiSit.frx":031A
         End
      End
      Begin Threed.SSFrame SSFrame2 
         Height          =   615
         Left            =   165
         TabIndex        =   4
         Top             =   6585
         Width           =   9750
         _Version        =   65536
         _ExtentX        =   17198
         _ExtentY        =   1085
         _StockProps     =   14
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Begin VB.Label lblMsg 
            Caption         =   "Ativar/Desativar Itens Por Leitura de Arquivo"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C00000&
            Height          =   255
            Left            =   225
            TabIndex        =   5
            Top             =   240
            Visible         =   0   'False
            Width           =   5895
         End
      End
   End
   Begin VB.Menu mnuAlterar 
      Caption         =   "&Ativar/Desativar"
      Begin VB.Menu mnuItens 
         Caption         =   "&Itens"
      End
   End
   Begin VB.Menu mnuSair 
      Caption         =   "&Sair"
   End
   Begin VB.Menu mnuSobre 
      Caption         =   "S&obre"
   End
End
Attribute VB_Name = "mdiSituacao"
Attribute VB_Creatable = False
Attribute VB_Exposed = False

Private Sub cmdCadastrar_Click()
    frmCadastro.Show vbModal
End Sub

Private Sub cmdCadastrar_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblMsg.Visible = True
    lblMsg.Caption = "Ativar/Desativar Itens DPK por Leitura de Arquivo"
End Sub


Private Sub cmdSair_Click()
    End
End Sub

Private Sub cmdSair_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblMsg.Visible = True
    lblMsg.Caption = "Sair do Programa"
End Sub


Private Sub MDIForm_Load()

    On Error GoTo TrataErro
    
    If App.PrevInstance Then
        MsgBox "J� EXISTE UMA INST�NCIA DO PROGRAMA NO AR"
        End
    End If
    
    'posicionar tela
    mdiSituacao.Top = 0
    mdiSituacao.Left = 0
    mdiSituacao.Width = Screen.Width
    mdiSituacao.Height = Screen.Height
        
    'Conexao oracle
    Set dbOra = CreateObject("oracleinproCServer.xorasession")
    
    'CAMPINAS
    'Set db = dbOra.OpenDatabase("sdpk_teste", "cad770/prod", 0&)
    Set db = dbOra.OpenDatabase("PRODUCAO", "CAD770/PROD", 0&)
           
    'Pega a data de faturamento do sistema para usar na montagem do nome do arq. de log e p/
    'servir de refer�ncia na data do cadastramento
    CmdSql = "Select To_Char(dt_faturamento,'dd/mm/yyyy') Dt_Cad From DATAS "
    Set db_CONS = db.dbcreatedynaset(CmdSql, 0&)
    
    vDt_Cad = Format(db_CONS!Dt_Cad, "dd/mm/yy")
    vAnoMes = Format(db_CONS!Dt_Cad, "yyyymm")
           
    CmdSql = "Select Cod_Loja From DEPOSITO"
    Set db_CONS = db.dbcreatedynaset(CmdSql, 0&)
           
    vCD_Deposito = Format(Trim(db_CONS!Cod_Loja), "00")
           
    Exit Sub

TrataErro:
    MsgBox "Ocorreu o erro: " & Err & "-" & Err.Description, vbExclamation, "Aten��o"
    Exit Sub
End Sub
Private Sub mnuItens_Click()
    Call cmdCadastrar_Click
End Sub

Private Sub mnuSair_Click()
    Call cmdSair_Click
End Sub

Private Sub mnuSobre_Click()
   frmSobre.Show vbModal
End Sub

Private Sub SSFrame1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblMsg.Visible = False
End Sub


Private Sub SSPanel2_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblMsg.Visible = False
End Sub


