VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form frmCad005 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Cad005 - Cadastro de E-mail"
   ClientHeight    =   5520
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8805
   ClipControls    =   0   'False
   Icon            =   "frmCad005.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5520
   ScaleWidth      =   8805
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   1365
      Left            =   30
      TabIndex        =   9
      Top             =   1440
      Width           =   8715
      Begin VB.TextBox txtEmail 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   990
         MaxLength       =   100
         TabIndex        =   14
         Top             =   900
         Width           =   7680
      End
      Begin VB.TextBox txtDtCadastro 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   7410
         Locked          =   -1  'True
         MaxLength       =   8
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   540
         Width           =   1260
      End
      Begin VB.TextBox txtContato 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   990
         MaxLength       =   70
         TabIndex        =   12
         Top             =   540
         Width           =   5220
      End
      Begin VB.TextBox txtSeq 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   990
         Locked          =   -1  'True
         MaxLength       =   100
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   180
         Width           =   540
      End
      Begin VB.ComboBox cboDepto 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   4380
         Style           =   2  'Dropdown List
         TabIndex        =   10
         Top             =   180
         Width           =   4275
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "E-mail:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   420
         TabIndex        =   19
         Top             =   990
         Width           =   525
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Dt.Cadastro:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   6360
         TabIndex        =   18
         Top             =   630
         Width           =   1020
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Contato:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   240
         TabIndex        =   17
         Top             =   630
         Width           =   720
      End
      Begin VB.Label Label4 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Sequ�ncia:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   60
         TabIndex        =   16
         Top             =   270
         Width           =   900
      End
      Begin VB.Label Label5 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Departamento:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   3120
         TabIndex        =   15
         Top             =   270
         Width           =   1245
      End
   End
   Begin MSComctlLib.ListView LsvEmail 
      Height          =   2175
      Left            =   30
      TabIndex        =   6
      Top             =   3300
      Width           =   8745
      _ExtentX        =   15425
      _ExtentY        =   3836
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   5
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Seq"
         Object.Width           =   882
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "E-mail"
         Object.Width           =   5115
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Contato"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Departamento"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "Data Cadastro"
         Object.Width           =   2293
      EndProperty
   End
   Begin VB.TextBox txtCodCliente 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H8000000F&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1080
      Locked          =   -1  'True
      MaxLength       =   100
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   1050
      Width           =   960
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   3
      Top             =   960
      Width           =   8715
      _ExtentX        =   15372
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   855
      Left            =   7920
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Sair do sistema"
      Top             =   60
      Width           =   855
      _ExtentX        =   1508
      _ExtentY        =   1508
      BTYPE           =   3
      TX              =   "Sair"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCad005.frx":0CCA
      PICN            =   "frmCad005.frx":0CE6
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdAdd 
      Height          =   405
      Left            =   7896
      TabIndex        =   0
      TabStop         =   0   'False
      ToolTipText     =   "Salvar E-mail"
      Top             =   2850
      Width           =   405
      _ExtentX        =   714
      _ExtentY        =   714
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCad005.frx":19C0
      PICN            =   "frmCad005.frx":19DC
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdExcluir 
      Height          =   405
      Left            =   8340
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Excluir E-mail selecionado"
      Top             =   2850
      Width           =   405
      _ExtentX        =   714
      _ExtentY        =   714
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCad005.frx":1F76
      PICN            =   "frmCad005.frx":1F92
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdNovo 
      Height          =   405
      Left            =   6570
      TabIndex        =   8
      TabStop         =   0   'False
      ToolTipText     =   "Adicionar E-mail"
      Top             =   2850
      Width           =   405
      _ExtentX        =   714
      _ExtentY        =   714
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCad005.frx":252C
      PICN            =   "frmCad005.frx":2548
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdAlterar 
      Height          =   405
      Left            =   7012
      TabIndex        =   21
      TabStop         =   0   'False
      ToolTipText     =   "Alterar E-mail"
      Top             =   2850
      Width           =   405
      _ExtentX        =   714
      _ExtentY        =   714
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCad005.frx":2A04
      PICN            =   "frmCad005.frx":2A20
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdLimpar 
      Height          =   405
      Left            =   7454
      TabIndex        =   22
      TabStop         =   0   'False
      ToolTipText     =   "Limpar"
      Top             =   2850
      Width           =   405
      _ExtentX        =   714
      _ExtentY        =   714
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCad005.frx":2FBA
      PICN            =   "frmCad005.frx":2FD6
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label lblNomeCliente 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   330
      Left            =   2070
      TabIndex        =   20
      Top             =   1050
      Width           =   6675
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "E-mails Cadastrados:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   60
      TabIndex        =   7
      Top             =   3060
      Width           =   1635
   End
   Begin VB.Label lblCodCliente 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "C�d.Cliente:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   60
      TabIndex        =   5
      Top             =   1140
      Width           =   990
   End
   Begin VB.Image Image1 
      Height          =   1035
      Left            =   -90
      Picture         =   "frmCad005.frx":32F0
      Stretch         =   -1  'True
      ToolTipText     =   "Acessar a Intranet"
      Top             =   -45
      Width           =   1305
   End
End
Attribute VB_Name = "frmCad005"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim vSessao As Object
Dim vBanco As Object
Dim vRst As Object

Private Sub cmdAdd_Click()
    Dim Litem As ListItem
    
    If txtCodCliente = "" Then
        MsgBox "O Campo C�digo do Cliente n�o pode estar em branco." & vbCrLf & "Entre em contato com o Helpdesk.", vbInformation, "Aten��o"
        Exit Sub
    End If
    If Trim(txtEmail) = "" Then
        MsgBox "Informe o E-mail.", vbInformation, "Aten��o"
        txtEmail.SetFocus
        Exit Sub
    End If
        
    'Validar Email
    If isEmail(txtEmail) = False Then
       MsgBox "E-mail inv�lido.", vbInformation, "Aten��o"
       txtEmail.SetFocus
       Exit Sub
    End If

        
'Verificar se existe um igual, se existir sair da rotina
    For i = 1 To LsvEmail.ListItems.Count
        LsvEmail.ListItems(i).Selected = True
        Set Litem = LsvEmail.SelectedItem
        If txtEmail = Litem.SubItems(1) And txtContato = Litem.SubItems(2) And cboDepto = Litem.SubItems(3) Then
            MsgBox "Este email j� existe.", vbInformation, "Aten��o"
            Exit Sub
        End If
    Next
    
    'Verificar se existe uma sequencia igual, se existir dar update
    If UCase(Frame1.Caption) = "ALTERAR" Then
        If MsgBox("Confirma a altera��o deste e-mail ?", vbQuestion + vbYesNo, "Aten��o") = vbYes Then
           Set Litem = LsvEmail.SelectedItem
           Litem = txtSeq
           Litem.SubItems(1) = txtEmail
           Litem.SubItems(2) = txtContato
           Litem.SubItems(3) = cboDepto
           Litem.SubItems(4) = txtDtCadastro
        End If
    ElseIf UCase(Frame1.Caption) = "NOVO" Then
       Set Litem = LsvEmail.ListItems.Add
       Litem = txtSeq
       Litem.SubItems(1) = txtEmail
       Litem.SubItems(2) = txtContato
       Litem.SubItems(3) = cboDepto
       Litem.SubItems(4) = txtDtCadastro
    End If
    
    'Inserir no banco
    vBanco.Parameters.Remove "Cod_Cliente"
    vBanco.Parameters.Remove "Sequencia"
    vBanco.Parameters.Remove "Email"
    vBanco.Parameters.Remove "CodDepto"
    vBanco.Parameters.Remove "Contato"
    
    vBanco.Parameters.Add "Cod_Cliente", txtCodCliente, 1
    vBanco.Parameters.Add "Sequencia", txtSeq, 1
    vBanco.Parameters.Add "Email", txtEmail, 1
    vBanco.Parameters.Add "CodDepto", cboDepto.ItemData(cboDepto.ListIndex), 1
    vBanco.Parameters.Add "Contato", txtContato, 1
    
    If UCase(Frame1.Caption) = "NOVO" Then
       vBanco.ExecuteSQL "BEGIN PRODUCAO.PCK_CAD005.PR_INCLUIR_EMAIL(:Cod_Cliente,:Sequencia, :Email, :CodDepto, :Contato); END;"
    ElseIf UCase(Frame1.Caption) = "ALTERAR" Then
       vBanco.ExecuteSQL "BEGIN PRODUCAO.PCK_CAD005.PR_ALTERAR_EMAIL(:Cod_Cliente,:Sequencia, :Email, :CodDepto, :Contato); END;"
    End If
    
    txtContato = ""
    txtEmail = ""
    txtSeq = ""
    cboDepto.ListIndex = -1
     
    Frame1.Enabled = False
    
    Botoes True, False, False, False, False
    
    
End Sub

Private Sub cmdAlterar_Click()
    Frame1.Enabled = True
    Frame1.Caption = "Alterar"
    Botoes False, False, True, True, False
End Sub

Private Sub cmdExcluir_Click()
    If Val(txtSeq) = 0 Then
       MsgBox "Selecione um item da lista.", vbInformation, "Aten��o"
       Exit Sub
    End If
    If MsgBox("Confirma a exclus�o do e-mail selecionado ?", vbQuestion + vbYesNo, "Aten��o") = vbYes Then
        
        vBanco.Parameters.Remove "COD_CLIENTE"
        vBanco.Parameters.Remove "Sequencia"
        vBanco.Parameters.Add "Cod_Cliente", txtCodCliente, 1
        vBanco.Parameters.Add "Sequencia", txtSeq, 1
        
        vBanco.ExecuteSQL "BEGIN PRODUCAO.PCK_CAD005.PR_EXCLUIR_EMAIL(:Cod_Cliente,:Sequencia); END;"
        
        vBanco.ExecuteSQL "BEGIN PRODUCAO.PCK_CAD005.PR_RESEQUENCIAR(:Cod_Cliente);END;"
        
        Preencher_Email
    End If
    
    txtSeq = ""
    txtContato = ""
    txtEmail = ""
    cboDepto.ListIndex = -1
        
    Frame1.Enabled = False
    
    Botoes True, False, False, False, False
    
End Sub

Private Sub cmdLimpar_Click()
    txtContato = ""
    txtEmail = ""
    cboDepto.ListIndex = -1
    Me.Frame1.Caption = ""
    
    Frame1.Enabled = False
    
    Botoes True, False, True, False, False
End Sub

Private Sub cmdNovo_Click()
    
    Frame1.Enabled = True
    
    If LsvEmail.ListItems.Count = 0 Then
        txtSeq = 1
    Else
        txtSeq = LsvEmail.ListItems(LsvEmail.ListItems.Count) + 1
    End If
    
    txtContato = ""
    txtEmail = ""
    cboDepto.ListIndex = -1
    
    cboDepto.SetFocus
    
    Frame1.Caption = "Novo"
    
    Botoes False, False, True, True, False
    
End Sub

Private Sub cmdSair_Click()

        Set vRst = Nothing
        Set vBanco = Nothing
        Set vSessao = Nothing
        
        End

End Sub
Private Sub Form_Load()
    
    Dim vParametros()
    
'    MsgBox Command$
    
'    vParametros = Split(Command$, ";")
    
'    MsgBox "0:" & vParametros(0) & vbCrLf & "1:" & vParametros(1)
    
'    Set vBanco = vParametros(0)
    
'    Me.txtCodCliente = vParametros(1)
    
'    Set vSessao = CreateObject("oracleinprocserver.xorasession")
'    Set vBanco = vSessao.OpenDatabase("SDPK_TESTE", "VDA020/PROD", 0&)

    

    Botoes True, False, False, False, False

    Preencher_Data
        
    Preencher_Depto
    
    Preencher_Email
    
    Pegar_nome_Cliente
    
End Sub

Private Sub LsvEmail_ItemClick(ByVal Item As MSComctlLib.ListItem)
    txtSeq = Item
    txtEmail = Item.SubItems(1)
    txtContato = Item.SubItems(2)
    cboDepto = Item.SubItems(3)
    txtDtCadastro = Item.SubItems(4)
        
    Frame1.Enabled = False
    Frame1.Caption = ""
    Botoes False, True, True, False, True
        
End Sub

Public Sub Criar_Cursor(Banco As Object, pNomeCursor As String)
    
    Banco.Remove pNomeCursor
    Banco.Add pNomeCursor, 0, 3
    Banco(pNomeCursor).serverType = 102
    Banco(pNomeCursor).DynasetOption = &H2&
    Banco(pNomeCursor).DynasetCacheParams 256, 16, 20, 2000, 0

End Sub

Sub Preencher_Data()
    
    Criar_Cursor vBanco.Parameters, "vCursor"

    vBanco.ExecuteSQL "BEGIN PRODUCAO.PCK_CAD005.PR_SELECT_DATA_CADASTRO(:vCursor); END;"

    Set vRst = vBanco.Parameters("vCursor").Value
            
    txtDtCadastro = vRst.Fields(0)

End Sub

Sub Preencher_Depto()
    
    Criar_Cursor vBanco.Parameters, "vCursor"

    vBanco.ExecuteSQL "BEGIN PRODUCAO.PCK_CAD005.PR_SELECT_DEPTO(:vCursor); END;"

    Set vRst = vBanco.Parameters("vCursor").Value
            
    For i = 1 To vRst.RecordCount
        cboDepto.AddItem vRst!Descricao
        cboDepto.ItemData(cboDepto.NewIndex) = vRst!Cod_Depto
        vRst.MoveNext
    Next

End Sub

Sub Preencher_Email()
    Dim Litem As ListItem
    
    vBanco.Parameters.Remove "Cod_Cliente"
    vBanco.Parameters.Add "Cod_cliente", txtCodCliente, 1
    
    Criar_Cursor vBanco.Parameters, "vCursor"
    
    vBanco.ExecuteSQL "Begin Producao.PCK_CAD005.PR_SELECT_EMAIL(:vCursor, :Cod_Cliente);End;"
    
    Set vRst = vBanco.Parameters("vCursor").Value
    
    LsvEmail.ListItems.Clear
    
    For i = 1 To vRst.RecordCount
        Set Litem = LsvEmail.ListItems.Add
        Litem = vRst!Sequencia
        Litem.SubItems(1) = vRst!email
        Litem.SubItems(2) = vRst!nome_Contato
        Litem.SubItems(3) = vRst!Descricao
        Litem.SubItems(4) = vRst!Dt_Email
        vRst.MoveNext
    Next
    Set vRst = Nothing
End Sub

Sub Pegar_nome_Cliente()

    vBanco.Parameters.Remove "Cod_Cliente"
    vBanco.Parameters.Add "Cod_Cliente", txtCodCliente, 1
    Criar_Cursor vBanco.Parameters, "vCursor"
    
    vBanco.ExecuteSQL "Begin Producao.PCK_CAD005.PR_SELECT_NOME_CLIENTE(:vCursor, :Cod_Cliente); END;"

    Set vRst = vBanco.Parameters("vCursor").Value
    
    If vRst.EOF = False Then Me.lblNomeCliente = vRst!nome_cliente
    
    Set vRst = Nothing

End Sub

Sub Botoes(pNovo As Boolean, pAlterar As Boolean, pDesfazer As Boolean, pGravar As Boolean, pExcluir As Boolean)

    cmdNovo.Enabled = pNovo
    cmdAlterar.Enabled = pAlterar
    cmdLimpar.Enabled = pDesfazer
    cmdAdd.Enabled = pGravar
    cmdExcluir.Enabled = pExcluir

End Sub

Function isEmail(ByVal pEmail As String) As Boolean
        
    Dim Conta As Integer, Flag As Integer, cValido As String
    
    isEmail = False
    
    If Len(pEmail) < 5 Then
        Exit Function
    End If

    
    'Verifica se existe caracter inv�lido
    
    For Conta = 1 To Len(pEmail)
        cValido = Mid(pEmail, Conta, 1)
        If Not (LCase(cValido) Like "[a-z]" Or cValido = _
            "@" Or cValido = "." Or cValido = "-" Or _
            cValido = "_") Then
            Exit Function
        End If
    Next
  

    'Verifica a exist�ncia de (@)
    
    If InStr(pEmail, "@") = 0 Then
        Exit Function
    Else
        Flag = 0
        
        For Conta = 1 To Len(pEmail)
            If Mid(pEmail, Conta, 1) = "@" Then
                Flag = Flag + 1
            End If
        Next
        
        If Flag > 1 Then Exit Function
    End If
  
    If Left(pEmail, 1) = "@" Then
        Exit Function
    ElseIf Right(pEmail, 1) = "@" Then
        Exit Function
    ElseIf InStr(pEmail, ".@") > 0 Then
        Exit Function
    ElseIf InStr(pEmail, "@.") > 0 Then
        Exit Function
    End If
  
  
    'Verifica a exist�ncia de (.)
    
    If InStr(pEmail, ".") = 0 Then
        Exit Function
    ElseIf Left(pEmail, 1) = "." Then
        Exit Function
    ElseIf Right(pEmail, 1) = "." Then
        Exit Function
    ElseIf InStr(pEmail, "..") > 0 Then
        Exit Function
    End If
    
  
    isEmail = True

End Function

