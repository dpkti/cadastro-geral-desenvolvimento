﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class Usuario
    {
        public string NomeUsuario { get; set; }
        public string Senha { get; set; }
        public string Login { get; set; }
        public bool Acesso { get; set; }
        public long CodUsuario { get; set; }
    }
}
