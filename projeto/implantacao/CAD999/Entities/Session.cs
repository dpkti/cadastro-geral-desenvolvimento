﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public static class Session
    {
        public static Int64 NumMix { get; set; }
        public static long Cod_Usuario { get; set; }
        public static bool CadAtivo { get; set; }
        public static bool Combo { get; set; } //TI-5398
    }
}
