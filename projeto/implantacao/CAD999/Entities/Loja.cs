﻿using System;

namespace Entities
{
    public class Loja
    {
        public String NomeLoja { get; set; }
        public int CodLoja { get; set; }
    }
}
