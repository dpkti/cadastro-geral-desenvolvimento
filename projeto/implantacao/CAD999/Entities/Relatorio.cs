﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class Relatorio
    {
        public string NomeLoja { get; set; }
        public Int64 NumMix { get; set; }
        public string Descricao { get; set; }
        public string VigenciaInicial { get; set; }
        public string VigenciaFinal { get; set; }
        public Int64 CodDPK { get; set; }
        public int CodForn { get; set; }
        public string Forn { get; set; }
        public string CodFabrica { get; set; }
        public string DescItem { get; set; }
        public int Qtde { get; set; }
        public bool Ativo { get; set; }
        //TI-5398
        public decimal Margem { get; set; }        
        public decimal PrecoUnitario { get; set; }
        public decimal PcDesc { get; set; }
        public decimal IcmRetido { get; set; }        
        public decimal MargemTotal { get; set; }
        public bool Combo { get; set; } 
        //FIM TI-5398
        
    }
}
