﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public static class EnumeradoresAplicacao
    {
        public enum Status
        {
            InicioValidacaoExcel,
            FimValidacaoExcel,
            InicioCriacaoMIX,
            FimCriacaoMIX,
            Erro
        }
    }
}
