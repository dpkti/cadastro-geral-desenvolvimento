﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class Mix
    {
        public string NomeLoja { get; set; }
        public Int64 NumMix { get; set; }
        public string VigenciaInicial { get; set; }
        public string VigenciaFinal { get; set; }
        public string Descricao { get; set; }        
        public bool Combo { get; set; } //TI-5398
        public decimal Margem { get; set; } //TI-5398
        public bool Ativo { get; set; }
    }
}
