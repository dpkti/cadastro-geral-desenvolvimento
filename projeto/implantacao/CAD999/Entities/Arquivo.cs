﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class Arquivo
    {
        public Int64 CodDPK { get; set; }
        public int Qtde { get; set; }
        public decimal PcDesc { get; set; } //TI-5398
    }
}
