﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class Item
    {
        public Int64 NumMix { get; set; }
        public int CodLoja { get; set; }
        public string CD { get; set; }
        public Int64 CodDPK { get; set; }
        public int CodForn { get; set; }
        public string Forn { get; set; }
        public string CodFabrica { get; set; }
        public string DescItem { get; set; }
        public int Qtde { get; set; }
        //TI-5398
        public decimal Margem { get; set; }        
        public decimal PrecoUnitario { get; set; }
        public decimal PcDesc { get; set; }
        public decimal IcmRetido { get; set; }        
        //FIM TI-5398
    }
}
