﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using DALC;
using Entities;
using Excel;
using UtilGeralDPA;
using System.Data;

namespace Business
{
    public class MixBLL: BaseBLL<MixBLL>
    {
        private List<Mix> listaMix = null;

        public List<Mix> SelecionarMix(string descricao)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            try
            {
                listaMix = MixDALC.Instance().SelecionarMix(descricao);
                return listaMix;
            }
            catch (Exception ex)
            {
                Logger.LogError("Erro ao carregar MIX.", ex);
                return listaMix;
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
        }

        public static Int64 InserirMix(Mix mix)
        {
            return MixDALC.InserirMix(mix);
        }

        public static void AlterarMix(Mix mix)
        {
            MixDALC.AlterarMix(mix);
        }

        public static List<Relatorio> ExportarExcel(Relatorio rel)
        {
            return MixDALC.ExportarExcel(rel);
        }

        //TI-5398
        public static decimal SelecionarMargem(Int64 numMix)
        {
            return MixDALC.SelecionarMargem(numMix);
        }
        //FIM TI-5398
    }
}
