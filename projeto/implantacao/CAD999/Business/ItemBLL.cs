﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using DALC;
using Entities;
using Excel;
using UtilGeralDPA;

namespace Business
{
    public class ItemBLL: BaseBLL<ItemBLL>
    {
        private List<Item> listaItem = null;

        public List<Item> SelecionarItem(Item item)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            try
            {
                listaItem = ItemDALC.Instance().SelecionarItem(item);
                return listaItem;
            }
            catch (Exception ex)
            {
                Logger.LogError("Erro ao carregar Itens.", ex);
                return listaItem;
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
        }

        public static string InserirItem(Item item)
        {
            return ItemDALC.InserirItem(item);
        }

        public static void ApagarItem(Item item)
        {
            ItemDALC.ApagarItem(item);
        }

        public static List<Arquivo> CarregarExcel(string caminhoArquivo)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            string caminhoOrigem;
            string arquivo;
            Item item = new Item();

            //System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.InvariantCulture;

            try
            {
                caminhoOrigem = caminhoArquivo;
                arquivo = "C:\\Windows\\Temp\\" + Path.GetFileName(caminhoOrigem).ToString();
                File.Copy(caminhoOrigem, arquivo, true);

                List<Arquivo> lstArquivo = new List<Arquivo>();
                int qtdDPK;
                Int64 codDPK;
                decimal pcDesc = 0; //TI-5398
                int count = 0;

                FileStream stream = File.Open(caminhoArquivo, FileMode.Open, FileAccess.Read);

                IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);

                excelReader.IsFirstRowAsColumnNames = true;

                while (excelReader.Read())
                {
                    if (count > 0)
                    {
                        Arquivo arq = new Arquivo();                        
                        if ((excelReader.GetValue(0) != null && excelReader.GetValue(1) != null) && (Int64.TryParse(excelReader.GetValue(0).ToString(), out codDPK)) && (int.TryParse(excelReader.GetValue(1).ToString(), out qtdDPK)) )
                        {
                            arq.CodDPK = excelReader.GetInt64(0);
                            arq.Qtde = excelReader.GetInt32(1);
                            arq.PcDesc = decimal.TryParse(excelReader.GetValue(2).ToString(), out pcDesc) ? pcDesc : 0;//TI-5398
                            lstArquivo.Add(arq);
                        }
                        else
                        {
                            if ((excelReader.GetValue(0) != null || excelReader.GetValue(1) != null))
                            {
                                ItemDALC.InserirLog(item, String.Format("Linha: {0} - Erro ao atualizar o Material: {1}, Qtde: {2}, PcDesc: {3}. Registro ignorado!", excelReader.Depth, excelReader.GetValue(0), excelReader.GetValue(1), excelReader.GetValue(2))); //TI-5398
                            }
                        }
                    }
                    count++;
                }

                excelReader.Close();

                File.Delete(arquivo);

                return lstArquivo;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
        }
    }
}
