﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Entities;
using UtilGeralDPA;

namespace DALC
{
    public class ItemDALC : BaseDALC<ItemDALC>
    {
        public List<Item> SelecionarItem(Item it)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            IDataReader reader = null;
            Item Item = null;
            List<Item> listaRetorno = new List<Item>();
            IRowMapper<Item> mapper = MapBuilder<Item>.MapAllProperties().Build();

            try
            {
                Database db = DatabaseFactory.CreateDatabase("ORA_SDPK_CONN");

                using (DbCommand cmd = db.GetStoredProcCommand("PRODUCAO.PCK_CAD999.PR_SEL_ITEM"))
                {
                    db.AddInParameter(cmd, "PM_COD_DPK", DbType.Int64, it.CodDPK);
                    db.AddInParameter(cmd, "PM_NUM_MIX", DbType.Int64, it.NumMix);

                    reader = db.ExecuteReader(cmd);
                    while (reader.Read())
                    {
                        Item = mapper.MapRow(reader);
                        listaRetorno.Add(Item);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                Logger.LogFinalMetodo(dataInicio);
            }

            return listaRetorno;
        }
        public static string InserirItem(Item item)
        {
            string strMsg = string.Empty;
            DateTime dataInicio = Logger.LogInicioMetodo();
            try
            {
                Database db = DatabaseFactory.CreateDatabase("ORA_SDPK_CONN");

                using (DbCommand cmd = db.GetStoredProcCommand("PRODUCAO.PCK_CAD999.PR_INS_ITEM"))
                {
                    db.AddInParameter(cmd, "PM_NUM_MIX", DbType.Int64, item.NumMix);
                    db.AddInParameter(cmd, "PM_COD_DPK", DbType.Int64, item.CodDPK);
                    db.AddInParameter(cmd, "PM_QTD_DPK", DbType.Int32, item.Qtde);
                    db.AddInParameter(cmd, "PM_PC_DESC", DbType.Decimal, item.PcDesc); //TI-5398
                    db.AddInParameter(cmd, "PM_COD_USUARIO", DbType.Int32, Session.Cod_Usuario);
                    db.AddOutParameter(cmd, "PM_MSG", DbType.String, 200);
                    db.ExecuteNonQuery(cmd);
                    strMsg = db.GetParameterValue(cmd, "PM_MSG").ToString();
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw ex;
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
            return strMsg;

        }

        public static void ApagarItem(Item item)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            try
            {
                Database db = DatabaseFactory.CreateDatabase("ORA_SDPK_CONN");

                using (DbCommand cmd = db.GetStoredProcCommand("PRODUCAO.PCK_CAD999.PR_DEL_ITEM"))
                {
                    db.AddInParameter(cmd, "PM_NUM_MIX", DbType.Int64, item.NumMix);
                    db.AddInParameter(cmd, "PM_COD_DPK", DbType.Int64, item.CodDPK);
                    db.AddInParameter(cmd, "PM_COD_USUARIO", DbType.Int32, Session.Cod_Usuario);
                    db.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw ex;
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

        }

        public static string InserirLog(Item item, string msg)
        {
            string strMsg = string.Empty;
            DateTime dataInicio = Logger.LogInicioMetodo();
            try
            {
                Database db = DatabaseFactory.CreateDatabase("ORA_SDPK_CONN");

                using (DbCommand cmd = db.GetStoredProcCommand("PRODUCAO.PCK_CAD999.PR_INS_LOG_ITEM"))
                {
                    db.AddInParameter(cmd, "PM_NUM_MIX", DbType.Int64, DBNull.Value);
                    db.AddInParameter(cmd, "PM_COD_DPK", DbType.Int64, DBNull.Value);
                    db.AddInParameter(cmd, "PM_QTD_DPK", DbType.Int32, DBNull.Value);
                    //TI-5398
                    db.AddInParameter(cmd, "PM_PRECO_UNITARIO", DbType.Decimal, 0);
                    db.AddInParameter(cmd, "PM_DESC1", DbType.Decimal, 0);
                    db.AddInParameter(cmd, "PM_DESC2", DbType.Decimal, 0);
                    db.AddInParameter(cmd, "PM_DESC3", DbType.Decimal, 0);
                    db.AddInParameter(cmd, "PM_DIFICM", DbType.Decimal, 0);
                    db.AddInParameter(cmd, "PM_ICMS_RETIDO", DbType.Decimal, 0);
                    db.AddInParameter(cmd, "PM_PC_MARGEM", DbType.Decimal, 0);
                    //FIM TI-5398
                    db.AddInParameter(cmd, "PM_COD_USUARIO", DbType.Int32, Session.Cod_Usuario);
                    db.AddInParameter(cmd, "PM_MSG", DbType.String, msg);
                    db.AddInParameter(cmd, "PM_SITUACAO", DbType.Int16, 1);
                    db.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw ex;
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
            return strMsg;

        }
    }
}
