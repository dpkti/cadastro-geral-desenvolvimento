﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Entities;
using UtilGeralDPA;

namespace DALC
{
    public class MixDALC : BaseDALC<MixDALC>
    {
        public List<Mix> SelecionarMix(string descricao)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            IDataReader reader = null;
            Mix Mix = null;
            List<Mix> listaRetorno = new List<Mix>();
            IRowMapper<Mix> mapper = MapBuilder<Mix>.MapAllProperties().Build();

            try
            {
                Database db = DatabaseFactory.CreateDatabase("ORA_SDPK_CONN");

                using (DbCommand cmd = db.GetStoredProcCommand("PRODUCAO.PCK_CAD999.PR_SEL_MIX"))
                {
                    db.AddInParameter(cmd, "PM_DESCRICAO", DbType.String, descricao);

                    reader = db.ExecuteReader(cmd);
                    while (reader.Read())
                    {
                        Mix = mapper.MapRow(reader);
                        listaRetorno.Add(Mix);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                Logger.LogFinalMetodo(dataInicio);
            }

            return listaRetorno;
        }

        public static Int64 InserirMix(Mix mix)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            Int64 intNumMix = 0;
            try
            {
                Database db = DatabaseFactory.CreateDatabase("ORA_SDPK_CONN");

                using (DbCommand cmd = db.GetStoredProcCommand("PRODUCAO.PCK_CAD999.PR_INS_MIX"))
                {
                    db.AddInParameter(cmd, "PM_COD_LOJA", DbType.Int16, Convert.ToInt16(mix.NomeLoja));
                    db.AddInParameter(cmd, "PM_DT_INCIAL", DbType.Date, mix.VigenciaInicial);
                    db.AddInParameter(cmd, "PM_DT_FINAL", DbType.Date, mix.VigenciaFinal);
                    db.AddInParameter(cmd, "PM_DESCRICAO", DbType.String, mix.Descricao);
                    db.AddInParameter(cmd, "PM_SITUACAO", DbType.Int16, Convert.ToInt16(mix.Ativo));
                    db.AddInParameter(cmd, "PM_COMBO", DbType.Int16, Convert.ToInt16(mix.Combo)); //TI-5398
                    db.AddOutParameter(cmd, "PM_NUM_MIX", DbType.Int64, 10000);
                    db.ExecuteNonQuery(cmd);
                    intNumMix = Convert.ToInt64(db.GetParameterValue(cmd, "PM_NUM_MIX"));

                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw ex;
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);                
            }

            return intNumMix;
        }

        public static void AlterarMix(Mix mix)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            try
            {
                Database db = DatabaseFactory.CreateDatabase("ORA_SDPK_CONN");

                using (DbCommand cmd = db.GetStoredProcCommand("PRODUCAO.PCK_CAD999.PR_UPD_MIX"))
                {
                    db.AddInParameter(cmd, "PM_DT_FINAL", DbType.Date, mix.VigenciaFinal);
                    db.AddInParameter(cmd, "PM_DESCRICAO", DbType.String, mix.Descricao);
                    db.AddInParameter(cmd, "PM_SITUACAO", DbType.Int16, Convert.ToInt16(mix.Ativo));
                    db.AddInParameter(cmd, "PM_COMBO", DbType.Int16, Convert.ToInt16(mix.Combo)); //TI-5398
                    db.AddInParameter(cmd, "PM_NUM_MIX", DbType.Int64, mix.NumMix);
                    db.ExecuteNonQuery(cmd);

                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw ex;
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

        }

        public static List<Relatorio> ExportarExcel(Relatorio rel)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            IDataReader reader = null;
            Relatorio relExcel = null;
            List<Relatorio> listaRetorno = new List<Relatorio>();
            IRowMapper<Relatorio> mapper = MapBuilder<Relatorio>.MapAllProperties().Build();

            try
            {
                Database db = DatabaseFactory.CreateDatabase("ORA_SDPK_CONN");

                using (DbCommand cmd = db.GetStoredProcCommand("PRODUCAO.PCK_CAD999.PR_SEL_EXCEL"))
                {
                    db.AddInParameter(cmd, "PM_NUM_MIX", DbType.Int64, rel.NumMix);

                    reader = db.ExecuteReader(cmd);
                    while (reader.Read())
                    {
                        relExcel = mapper.MapRow(reader);
                        listaRetorno.Add(relExcel);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                Logger.LogFinalMetodo(dataInicio);
            }

            return listaRetorno;
        }

        //TI-5398
        public static decimal SelecionarMargem(Int64 numMix)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            IDataReader reader = null;
            decimal valor = 0;

            try
            {
                Database db = DatabaseFactory.CreateDatabase("ORA_SDPK_CONN");
                using (DbCommand cmd = db.GetStoredProcCommand("PRODUCAO.PCK_CAD999.PR_SEL_MARGEM"))
                {
                    db.AddInParameter(cmd, "PM_NUM_MIX", DbType.Int64, numMix);

                    reader = db.ExecuteReader(cmd);
                    if (reader.Read())
                    {
                        valor = (decimal)reader[0];
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return valor;
        }  
        //FIM TI-5398
    }
}
