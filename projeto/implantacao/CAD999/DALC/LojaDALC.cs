﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Entities;
using UtilGeralDPA;
namespace DALC
{
    public class LojaDALC : BaseDALC<LojaDALC>
    {
        public List<Loja> SelecionarTodas()
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            IDataReader reader = null;
            Loja loja = null;
            List<Loja> listaRetorno = new List<Loja>();
            IRowMapper<Loja> mapper = MapBuilder<Loja>.MapAllProperties().Build();

            try
            {
                Database db = DatabaseFactory.CreateDatabase("ORA_SDPK_CONN");

                using (DbCommand cmd = db.GetStoredProcCommand("PRODUCAO.PCK_CAD999.PR_SEL_LOJA"))
                {
                    reader = db.ExecuteReader(cmd);
                    while (reader.Read())
                    {
                        loja = mapper.MapRow(reader);
                        listaRetorno.Add(loja);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                Logger.LogFinalMetodo(dataInicio);
            }

            return listaRetorno;
        }

    }
}
