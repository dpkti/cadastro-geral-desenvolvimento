﻿namespace Presentation
{
    partial class frmItem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmItem));
            this.grpMix = new System.Windows.Forms.GroupBox();
            this.btnExcluir = new System.Windows.Forms.Button();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.txtDPKBusca = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.grdItem = new System.Windows.Forms.DataGridView();
            this.chkSel = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.grpCad = new System.Windows.Forms.GroupBox();
            this.lblMargem = new System.Windows.Forms.Label();
            this.txtPcDesc = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtQTDE = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnIncluir = new System.Windows.Forms.Button();
            this.txtDPK = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.grpMix.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdItem)).BeginInit();
            this.grpCad.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpMix
            // 
            this.grpMix.Controls.Add(this.btnExcluir);
            this.grpMix.Controls.Add(this.btnBuscar);
            this.grpMix.Controls.Add(this.txtDPKBusca);
            this.grpMix.Controls.Add(this.label2);
            this.grpMix.Controls.Add(this.grdItem);
            this.grpMix.Location = new System.Drawing.Point(12, 60);
            this.grpMix.Name = "grpMix";
            this.grpMix.Size = new System.Drawing.Size(753, 397);
            this.grpMix.TabIndex = 9;
            this.grpMix.TabStop = false;
            this.grpMix.Text = "Itens do MIX";
            // 
            // btnExcluir
            // 
            this.btnExcluir.Location = new System.Drawing.Point(672, 15);
            this.btnExcluir.Name = "btnExcluir";
            this.btnExcluir.Size = new System.Drawing.Size(75, 23);
            this.btnExcluir.TabIndex = 6;
            this.btnExcluir.Text = "Excluir";
            this.btnExcluir.UseVisualStyleBackColor = true;
            this.btnExcluir.Click += new System.EventHandler(this.btnExcluir_Click);
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(221, 15);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(75, 23);
            this.btnBuscar.TabIndex = 5;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // txtDPKBusca
            // 
            this.txtDPKBusca.Location = new System.Drawing.Point(77, 17);
            this.txtDPKBusca.MaxLength = 18;
            this.txtDPKBusca.Name = "txtDPKBusca";
            this.txtDPKBusca.Size = new System.Drawing.Size(138, 20);
            this.txtDPKBusca.TabIndex = 4;
            this.txtDPKBusca.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDPKBusca_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Código DPK";
            // 
            // grdItem
            // 
            this.grdItem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdItem.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.chkSel});
            this.grdItem.Location = new System.Drawing.Point(6, 43);
            this.grdItem.Name = "grdItem";
            this.grdItem.Size = new System.Drawing.Size(741, 348);
            this.grdItem.TabIndex = 4;
            // 
            // chkSel
            // 
            this.chkSel.HeaderText = "Selecionar";
            this.chkSel.Name = "chkSel";
            this.chkSel.Width = 60;
            // 
            // grpCad
            // 
            this.grpCad.Controls.Add(this.lblMargem);
            this.grpCad.Controls.Add(this.txtPcDesc);
            this.grpCad.Controls.Add(this.label4);
            this.grpCad.Controls.Add(this.txtQTDE);
            this.grpCad.Controls.Add(this.label3);
            this.grpCad.Controls.Add(this.btnIncluir);
            this.grpCad.Controls.Add(this.txtDPK);
            this.grpCad.Controls.Add(this.label1);
            this.grpCad.Location = new System.Drawing.Point(12, 12);
            this.grpCad.Name = "grpCad";
            this.grpCad.Size = new System.Drawing.Size(753, 42);
            this.grpCad.TabIndex = 8;
            this.grpCad.TabStop = false;
            this.grpCad.Text = "Incluir Item";
            // 
            // lblMargem
            // 
            this.lblMargem.AutoSize = true;
            this.lblMargem.Location = new System.Drawing.Point(516, 19);
            this.lblMargem.Name = "lblMargem";
            this.lblMargem.Size = new System.Drawing.Size(0, 13);
            this.lblMargem.TabIndex = 14;
            // 
            // txtPcDesc
            // 
            this.txtPcDesc.Location = new System.Drawing.Point(448, 16);
            this.txtPcDesc.MaxLength = 5;
            this.txtPcDesc.Name = "txtPcDesc";
            this.txtPcDesc.Size = new System.Drawing.Size(62, 20);
            this.txtPcDesc.TabIndex = 3;
            this.txtPcDesc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPcDesc_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(380, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Desconto";
            // 
            // txtQTDE
            // 
            this.txtQTDE.Location = new System.Drawing.Point(300, 15);
            this.txtQTDE.MaxLength = 8;
            this.txtQTDE.Name = "txtQTDE";
            this.txtQTDE.Size = new System.Drawing.Size(62, 20);
            this.txtQTDE.TabIndex = 2;
            this.txtQTDE.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtQTDE_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(232, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "QTDE DPK";
            // 
            // btnIncluir
            // 
            this.btnIncluir.Location = new System.Drawing.Point(672, 13);
            this.btnIncluir.Name = "btnIncluir";
            this.btnIncluir.Size = new System.Drawing.Size(75, 23);
            this.btnIncluir.TabIndex = 4;
            this.btnIncluir.Text = "Incluir";
            this.btnIncluir.UseVisualStyleBackColor = true;
            this.btnIncluir.Click += new System.EventHandler(this.btnIncluir_Click);
            // 
            // txtDPK
            // 
            this.txtDPK.Location = new System.Drawing.Point(77, 16);
            this.txtDPK.MaxLength = 18;
            this.txtDPK.Name = "txtDPK";
            this.txtDPK.Size = new System.Drawing.Size(138, 20);
            this.txtDPK.TabIndex = 1;
            this.txtDPK.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDPK_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Código DPK";
            // 
            // frmItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(775, 464);
            this.Controls.Add(this.grpCad);
            this.Controls.Add(this.grpMix);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmItem";
            this.Text = "CAD999 - Cadastro e Manutenção de MIX de Produtos";
            this.grpMix.ResumeLayout(false);
            this.grpMix.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdItem)).EndInit();
            this.grpCad.ResumeLayout(false);
            this.grpCad.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpMix;
        private System.Windows.Forms.DataGridView grdItem;
        private System.Windows.Forms.GroupBox grpCad;
        private System.Windows.Forms.Button btnIncluir;
        private System.Windows.Forms.TextBox txtDPK;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.TextBox txtDPKBusca;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnExcluir;
        private System.Windows.Forms.DataGridViewCheckBoxColumn chkSel;
        private System.Windows.Forms.TextBox txtQTDE;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtPcDesc;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblMargem;

    }
}