﻿using System.ComponentModel;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using Business;
using System.Threading;
using System.Diagnostics;
using System;
using System.Drawing;
using Entities;

namespace Presentation
{
    public partial class frmItem : Form
    {
        Int64 intDPK;
        Int32 intQTDE;
        decimal pcDesc; //TI-5398

        public frmItem()
        {
            InitializeComponent();

            this.FormClosing += frmItem_FormClosing;

            if (!Session.CadAtivo)
            {
                grpCad.Enabled = false;
            }

            //TI-5398
            this.txtPcDesc.Text = "0.00";
            if (!Session.Combo)
                this.txtPcDesc.Enabled = false;
            //FIM TI-5398

            CarregarItem();

            grdItem.Columns["NumMix"].ReadOnly = true;
            grdItem.Columns["CodLoja"].ReadOnly = true;
            grdItem.Columns["CD"].ReadOnly = true;
            grdItem.Columns["CodDPK"].ReadOnly = true;
            grdItem.Columns["CodForn"].ReadOnly = true;
            grdItem.Columns["Forn"].ReadOnly = true;
            grdItem.Columns["CodFabrica"].ReadOnly = true;
            grdItem.Columns["DescItem"].ReadOnly = true;
            grdItem.Columns["Qtde"].ReadOnly = true;
            //TI-5398
            grdItem.Columns["Margem"].ReadOnly = true;
            grdItem.Columns["PrecoUnitario"].ReadOnly = true;
            grdItem.Columns["PcDesc"].ReadOnly = true;
            grdItem.Columns["IcmRetido"].ReadOnly = true;
            //FIM TI-5398

            grdItem.Columns["NumMix"].Width = 50;
            grdItem.Columns["CodLoja"].Width = 55;
            grdItem.Columns["CD"].Width = 80;
            grdItem.Columns["CodDPK"].Width = 55;
            grdItem.Columns["CodForn"].Width = 55;
            grdItem.Columns["Forn"].Width = 60;
            grdItem.Columns["CodFabrica"].Width = 100;
            grdItem.Columns["DescItem"].Width = 120;
            grdItem.Columns["Qtde"].Width = 35;
        }

        private void frmItem_FormClosing(Object sender, FormClosingEventArgs e)
        {
            Session.NumMix=0;
            Session.CadAtivo = false;
        }

        private void CarregarItem()
        {
            Item item = new Item();
            item.NumMix = Session.NumMix;
            item.CodDPK = 0;
            var it = ItemBLL.Instance().SelecionarItem(item);
            grdItem.DataSource = it;

            lblMargem.Text = string.Format("Margem Total: {0}", MixBLL.SelecionarMargem(Session.NumMix).ToString()); //TI-5398
        }

        private bool ValidarCadastro()
        {

            if (string.IsNullOrEmpty(txtDPK.Text))
            {
                MessageBox.Show("Preencha o campo Código DPK!", "CAD999", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtDPK.Focus();
                return false;
            }
            if (!Int64.TryParse(txtDPK.Text, out intDPK))
            {
                MessageBox.Show("Valor do Código DPK inválido! " + txtDPK.Text, "CAD999", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtDPK.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtQTDE.Text))
            {
                MessageBox.Show("Preencha o campo QTDE DPK!", "CAD999", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtQTDE.Focus();
                return false;
            }
            if (!Int32.TryParse(txtQTDE.Text, out intQTDE))
            {
                MessageBox.Show("Valor do QTDE DPK inválido! " + txtQTDE.Text, "CAD999", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtQTDE.Focus();
                return false;
            }
            //TI-5398
            if (!decimal.TryParse(txtPcDesc.Text.Trim(), out pcDesc))
            {
                MessageBox.Show("Valor do Desconto inválido! Formato correto: 99.99", "CAD999", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtPcDesc.Focus();
                return false;
            }
            if (!(pcDesc >= 0 && pcDesc <= 99.99m))
            {
                MessageBox.Show("Valor do Desconto muito grande! Formato correto: 99.99", "CAD999", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtPcDesc.Focus();
                return false;
            }
            //FIM TI-5398
            return true;
        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            controleAtivo(false);//TI-5398
            string strMsg = string.Empty;
            try
            {
                if (ValidarCadastro())
                {
                    Item item = new Item();
                    item.NumMix = Session.NumMix;
                    item.CodDPK = Convert.ToInt64(txtDPK.Text);
                    item.Qtde = Convert.ToInt32(txtQTDE.Text);
                    item.PcDesc = Convert.ToDecimal(txtPcDesc.Text);//TI-5398
                    strMsg = ItemBLL.InserirItem(item);
                    
                    if (string.IsNullOrEmpty(strMsg))
                    {
                        LimparCadastro();
                        CarregarItem();
                    }
                    else
                    {
                        MessageBox.Show(strMsg, "CAD999", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }

            }
            catch (Exception ex)
            {
               MessageBox.Show("Erro ao gravar item! - " + ex.Message, "CAD999", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            controleAtivo(true);//TI-5398
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            if (!Int64.TryParse(txtDPKBusca.Text, out intDPK) && !string.IsNullOrEmpty(txtDPKBusca.Text))
            {
                MessageBox.Show("Valor do Código DPK inválido! " + txtDPK.Text, "CAD999", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtDPKBusca.Focus();
            }
            else
            {
                Item item = new Item();
                item.NumMix = Session.NumMix;
                if(!string.IsNullOrEmpty(txtDPKBusca.Text))
                    item.CodDPK = Convert.ToInt64(txtDPKBusca.Text);
                var it = ItemBLL.Instance().SelecionarItem(item);
                grdItem.DataSource = it;
                txtDPKBusca.Text = string.Empty;
            }
        }

        private void LimparCadastro()
        {
            this.txtDPK.Text = string.Empty;
            this.txtQTDE.Text = string.Empty;
            this.txtPcDesc.Text = "0.00";//TI-5398
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            controleAtivo(false);//TI-5398
            try
            {
                Item item = new Item();
                item.NumMix = Session.NumMix;

                foreach (DataGridViewRow dr in grdItem.Rows)
                {
                    DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)dr.Cells[0];
                    if (chk.Value!=null )
                    {
                        item.CodDPK = Convert.ToInt64(grdItem.Rows[dr.Index].Cells[4].Value);
                        if ((bool)chk.Value == true)
                            ItemBLL.ApagarItem(item);
                    }
                }
                grdItem.EndEdit();
                CarregarItem();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro ao excluir item! - " + ex.Message, "CAD999", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            controleAtivo(true);//TI-5398
        }

        //TI-5398
        private void txtDPK_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (!(char.IsNumber(e.KeyChar)) && !(e.KeyChar == (char)Keys.Back))
            {

                e.Handled = true;

            }
        }

        private void txtQTDE_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && !(e.KeyChar == (char)Keys.Back))
            {

                e.Handled = true;

            }
        }

        private void txtDPKBusca_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && !(e.KeyChar == (char)Keys.Back))
            {

                e.Handled = true;

            }
        }
        
        private void txtPcDesc_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == ',')
                e.KeyChar = '.';

            if (!(char.IsNumber(e.KeyChar)) && !(e.KeyChar == (char)Keys.Back) && !(e.KeyChar == Char.Parse(".")))
                e.Handled = true;
        }

        private void controleAtivo(bool ativacao)
        {
            if (!ativacao)
                this.Cursor = Cursors.WaitCursor;
            else
                this.Cursor = Cursors.Default;
            btnIncluir.Enabled = ativacao;
            btnExcluir.Enabled = ativacao;
            btnBuscar.Enabled = ativacao;
            grdItem.Enabled = ativacao;
        }
        //FIM TI-5398
    }
}
