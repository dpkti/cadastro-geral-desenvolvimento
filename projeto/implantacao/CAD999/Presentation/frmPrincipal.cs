﻿using System.ComponentModel;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using Business;
using System.Threading;
using System.Diagnostics;
using System;
using System.Drawing;
using Entities;
using System.Data;

namespace Presentation
{
    public partial class frmPrincipal : Form
    {
        public frmPrincipal()
        {
            InitializeComponent();

            chkAtivo.Checked = true;
            CarregarLoja();

            this.FormClosing += frmPrincipal_FormClosing;
            mskDtIni.ValidatingType = typeof(System.DateTime);
            mskDtIni.TypeValidationCompleted += new TypeValidationEventHandler(mskDtIni_TypeValidationCompleted);
            mskDtFim.ValidatingType = typeof(System.DateTime);
            mskDtFim.TypeValidationCompleted += new TypeValidationEventHandler(mskDtFim_TypeValidationCompleted);

        }

        public void mskDtIni_TypeValidationCompleted(object sender, TypeValidationEventArgs e)
        {

            if (!e.IsValidInput && this.mskDtIni.MaskedTextProvider.AssignedEditPositionCount > 0)
            {
                toolTip1.ToolTipTitle = "Data Inválida";
                toolTip1.Show("Por favor, preencher a data no formata DD/MM/YYYY", mskDtIni, 5000);
                e.Cancel = true;
            }
        }

        public void mskDtFim_TypeValidationCompleted(object sender, TypeValidationEventArgs e)
        {

            if (!e.IsValidInput && this.mskDtFim.MaskedTextProvider.AssignedEditPositionCount > 0)
            {
                toolTip1.ToolTipTitle = "Data Inválida";
                toolTip1.Show("Por favor, preencher a data no formata DD/MM/YYYY", mskDtFim, 5000);
                e.Cancel = true;
            }
        }

        private void CarregarLoja()
        {
            var loja = LojaBLL.Instance().ListarTodas();
            ((ListBox)this.chklstLoja).DataSource = loja;
            ((ListBox)this.chklstLoja).DisplayMember = "NomeLoja";
            ((ListBox)this.chklstLoja).ValueMember = "CodLoja";
        }

        private void CarregarMIX()
        {
            var mix = MixBLL.Instance().SelecionarMix("");
            grdMIX.DataSource = mix;
        }

        private void frmPrincipal_FormClosing(Object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            controleAtivo(false);
            Int64 intNumMix = 0;
            try
            {
                if (ValidarCadastro())
                {
                    Mix mix = new Mix();
                    mix.Descricao = txtDescricao.Text;
                    mix.Ativo = chkAtivo.Checked;
                    mix.VigenciaInicial = mskDtIni.Text;
                    mix.VigenciaFinal = mskDtFim.Text;
                    mix.Combo = chkCombo.Checked; //TI-5398

                    if (File.Exists(this.txtCaminhoArquivo.Text) )
                    {
                        string msg = string.Empty;
                        Item item = new Item();
                        var lstArquivo = ItemBLL.CarregarExcel(this.txtCaminhoArquivo.Text);
                        foreach (object it in chklstLoja.CheckedItems)
                        {
                            var loja = (Loja)it;
                            mix.NomeLoja = loja.CodLoja.ToString();
                            intNumMix = MixBLL.InserirMix(mix);
                            lstArquivo.ForEach(o => {
                                item.NumMix = intNumMix;                         
                                item.CodDPK = o.CodDPK;
                                item.Qtde = o.Qtde;
                                item.PcDesc = o.PcDesc; //TI-5398
                                msg = ItemBLL.InserirItem(item);
                            });                                                      
                        }
                    }
                    else if (Session.NumMix > 0 && this.btnAbrir.Enabled == false)
                    {
                        mix.NumMix = Session.NumMix;
                        MixBLL.AlterarMix(mix);
                        Session.NumMix = 0;
                        Session.CadAtivo=false;
                    }
                    else
                    {
                        if (MessageBox.Show("O arquivo inválido ou não informado! Deseja criar MIX sem importar itens pelo Excel?", "CAD999", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)== DialogResult.Yes)
                        {
                            foreach (object item in chklstLoja.CheckedItems)
                            {
                                var loja = (Loja)item;
                                mix.NomeLoja = loja.CodLoja.ToString();
                                intNumMix = MixBLL.InserirMix(mix);
                            }   
                        }       
                    }
                    LimparCadastro();
                    CarregarMIX();
                    Session.NumMix = 0;
                    Session.CadAtivo = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro ao gravar dados! - " + ex.Message, "CAD999", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            controleAtivo(true);
         }

        private bool ValidarCadastro()
        {
            if (chklstLoja.CheckedItems.Count==0 && chklstLoja.Enabled==true)
            {
                MessageBox.Show("Selecione as lojas para realizar o cadastro!", "CAD999", MessageBoxButtons.OK, MessageBoxIcon.Information);
                chklstLoja.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtDescricao.Text))
            {
                MessageBox.Show("Preencha o campo Descrição!", "CAD999", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtDescricao.Focus();
                return false;
            }
            if (mskDtIni.Text == "  /  /")
            {
                MessageBox.Show("Preencha o campo Vigência Inicial!", "CAD999", MessageBoxButtons.OK, MessageBoxIcon.Information);
                mskDtIni.Focus();
                return false;
            }
            if (mskDtFim.Text == "  /  /")
            {
                MessageBox.Show("Preencha o campo Vigência Final!", "CAD999", MessageBoxButtons.OK, MessageBoxIcon.Information);
                mskDtFim.Focus();
                return false;
            }
            if (DateTime.Compare(Convert.ToDateTime(mskDtIni.Text), Convert.ToDateTime(mskDtFim.Text)) == 1)
            {
                MessageBox.Show("Vigência Final não pode ser menor que a Vigência Inicial!", "CAD999", MessageBoxButtons.OK, MessageBoxIcon.Information);
                mskDtFim.Focus();
                return false;
            }
            return true;
        }

        private void LimparCadastro()
        {
            chkTodas.Checked = false;
            for (int i = 0; i <= chklstLoja.Items.Count - 1; i++)
                chklstLoja.SetItemChecked(i, chkTodas.Checked);
            chkAtivo.Checked = true;
            txtDescricao.Text = string.Empty;
            mskDtIni.Text = string.Empty;
            mskDtFim.Text = string.Empty;            
            this.mskDtIni.Enabled = true;
            this.chklstLoja.Enabled = true;
            this.chkTodas.Enabled = true;
            this.txtCaminhoArquivo.Text = string.Empty;
            chkCombo.Checked = false;//TI-5398
        }

        private void chkTodas_CheckedChanged(object sender, EventArgs e)
        {
                for (int i = 0; i <= chklstLoja.Items.Count - 1; i++)
                    chklstLoja.SetItemChecked(i, chkTodas.Checked);
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            controleAtivo(false);
            var mix = MixBLL.Instance().SelecionarMix(txtBusca.Text);
            grdMIX.DataSource = mix;
            txtBusca.Text = string.Empty;
            controleAtivo(true);
        }

        private void controleAtivo(bool ativacao)
        {
            if (!ativacao)
                this.Cursor = Cursors.WaitCursor;
            else
                this.Cursor = Cursors.Default;
            btnAbrir.Enabled = ativacao;
            btnLimpar.Enabled = ativacao;
            btnSalvar.Enabled = ativacao;
            btnBuscar.Enabled = ativacao;
            btnExportar.Enabled = ativacao;
            grpMix.Enabled = ativacao;
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            controleAtivo(false);
            LimparCadastro();
            controleAtivo(true);
        }

        private void btnExportar_Click(object sender, EventArgs e)
        {
            controleAtivo(false);
            try
            {
                //Creating DataTable
                DataTable dt = new DataTable();
                Relatorio rel = new Relatorio();
                string strAtivo = string.Empty;

                //Adding the Columns
                dt.Columns.Add("NumMix");
                dt.Columns.Add("NomeLoja");
                dt.Columns.Add("Descricao");
                dt.Columns.Add("VigenciaInicial");
                dt.Columns.Add("VigenciaFinal");
                dt.Columns.Add("CodDPK");
                dt.Columns.Add("CodForn");
                dt.Columns.Add("Forn");
                dt.Columns.Add("CodFabrica");
                dt.Columns.Add("DescItem");
                dt.Columns.Add("Qtde");                
                //TI-5398
                dt.Columns.Add("Margem");
                dt.Columns.Add("PrecoUnitario");
                dt.Columns.Add("PcDesc");
                dt.Columns.Add("IcmRetido");
                dt.Columns.Add("Margem Total");
                dt.Columns.Add("Combo");
                //TI-5398
                dt.Columns.Add("Ativo");

                //Adding the Rows
                foreach (DataGridViewRow dr in grdMIX.Rows)
                {
                    DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)dr.Cells[0];
                    if (chk.Value != null)
                    {
                        rel.NumMix = Convert.ToInt64(grdMIX.Rows[dr.Index].Cells[2].Value);
                        if ((bool)chk.Value == true)
                        {
                            var relatorio = MixBLL.ExportarExcel(rel);
                            foreach (var item in relatorio)
                            {
                                var row = dt.NewRow();

                                if (item.Ativo)
                                    strAtivo = "Sim";
                                else
                                    strAtivo = "Não";

                                row["NumMix"] = Convert.ToInt64(item.NumMix);
                                row["NomeLoja"] = item.NomeLoja;
                                row["Descricao"] = item.Descricao;
                                row["VigenciaInicial"] = item.VigenciaInicial;
                                row["VigenciaFinal"] = item.VigenciaFinal;
                                row["CodDPK"] = Convert.ToInt64(item.CodDPK);
                                row["CodForn"] = Convert.ToInt32(item.CodForn);
                                row["Forn"] = item.Forn;
                                row["CodFabrica"] = item.CodFabrica;
                                row["DescItem"] = item.DescItem;
                                row["Qtde"] = Convert.ToInt32(item.Qtde);
                                //TI-5398
                                row["Margem"] = Convert.ToDecimal(item.Margem);
                                row["PrecoUnitario"] = Convert.ToDecimal(item.PrecoUnitario);
                                row["PcDesc"] = Convert.ToDecimal(item.PcDesc);
                                row["IcmRetido"] = Convert.ToDecimal(item.IcmRetido);
                                row["Margem Total"] = Convert.ToDecimal(item.MargemTotal);
                                 row["Combo"] = item.Combo ? "Sim" : "Não" ; 
                                //FIM TI-5398   
                                row["Ativo"] = strAtivo;
                                dt.Rows.Add(row);
                            }

                            chk.Value = false;
                        }
                    }
                }
                grdMIX.EndEdit();

                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Filter = "Excel Documents (*.xls)|*.xls";
                DataSet ds = new DataSet("Teste");
                ds.Locale = System.Threading.Thread.CurrentThread.CurrentCulture;
                ds.Tables.Add(dt);
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    ExcelLibrary.DataSetHelper.CreateWorkbook(sfd.FileName, ds);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Falha ao exportar excel." + ex.Message, "CAD999", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            LimparCadastro();
            controleAtivo(true);
        }

        private void grdMIX_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex >= 0 && e.RowIndex>=0)
            {
                if (grdMIX.Columns[e.ColumnIndex].Name != "chkSel")
                {
                    this.txtDescricao.Text = grdMIX.Rows[e.RowIndex].Cells[5].Value.ToString();
                    this.mskDtIni.Text = grdMIX.Rows[e.RowIndex].Cells[3].Value.ToString();
                    this.mskDtFim.Text = grdMIX.Rows[e.RowIndex].Cells[4].Value.ToString();
                    this.chkAtivo.Checked = (bool)grdMIX.Rows[e.RowIndex].Cells[8].Value;
                    this.chkCombo.Checked = (bool)grdMIX.Rows[e.RowIndex].Cells[6].Value;//TI-5398
                    Session.CadAtivo = (bool)grdMIX.Rows[e.RowIndex].Cells[8].Value;
                    Session.NumMix = (Int64)grdMIX.Rows[e.RowIndex].Cells[2].Value;
                    Session.Combo = (bool)grdMIX.Rows[e.RowIndex].Cells[6].Value;//TI-5398
                    this.txtCaminhoArquivo.Text = string.Empty;
                    this.mskDtIni.Enabled = false;
                    this.btnAbrir.Enabled = false;
                    this.chklstLoja.Enabled = false;
                    this.btnBuscar.Enabled = false;
                    this.btnExportar.Enabled = false;
                    this.chkTodas.Enabled = false;
                }
            }

        }

        private void grdMIX_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
            {
                controleAtivo(true);
                LimparCadastro();
                frmItem frm = new frmItem();
                frm.ShowDialog();
            }
        }

        private void btnAbrir_Click(object sender, EventArgs e)
        {
            controleAtivo(false);
            this.openFileDialog1.Filter = "Microsoft Excel (*.XLS;*.XLSX)|*.xls;*.xlsx";
            this.openFileDialog1.ShowDialog();
            this.txtCaminhoArquivo.Text = this.openFileDialog1.FileName;
            controleAtivo(true);
        }

        //TI-5398
        private void frmPrincipal_Activated(object sender, EventArgs e)
        {
            CarregarMIX();

            grdMIX.Columns["NomeLoja"].ReadOnly = true;
            grdMIX.Columns["NumMix"].ReadOnly = true;
            grdMIX.Columns["VigenciaInicial"].ReadOnly = true;
            grdMIX.Columns["VigenciaFinal"].ReadOnly = true;
            grdMIX.Columns["Descricao"].ReadOnly = true;
            grdMIX.Columns["Combo"].ReadOnly = true; 
            grdMIX.Columns["Margem"].ReadOnly = true;
            grdMIX.Columns["Ativo"].ReadOnly = true;
        }
        //FIM TI-5398

     }
}
