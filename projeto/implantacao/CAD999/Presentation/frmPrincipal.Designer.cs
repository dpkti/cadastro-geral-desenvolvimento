﻿namespace Presentation
{
    partial class frmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrincipal));
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.grpMix = new System.Windows.Forms.GroupBox();
            this.btnExportar = new System.Windows.Forms.Button();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.txtBusca = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.grdMIX = new System.Windows.Forms.DataGridView();
            this.chkSel = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.grpCad = new System.Windows.Forms.GroupBox();
            this.chkCombo = new System.Windows.Forms.CheckBox();
            this.chkTodas = new System.Windows.Forms.CheckBox();
            this.chklstLoja = new System.Windows.Forms.CheckedListBox();
            this.btnLimpar = new System.Windows.Forms.Button();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.mskDtFim = new System.Windows.Forms.MaskedTextBox();
            this.mskDtIni = new System.Windows.Forms.MaskedTextBox();
            this.chkAtivo = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDescricao = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtCaminhoArquivo = new System.Windows.Forms.TextBox();
            this.btnAbrir = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.grpMix.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdMIX)).BeginInit();
            this.grpCad.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.InitialDirectory = "c:\\";
            this.openFileDialog1.Title = "Selecionar Arquivo";
            // 
            // grpMix
            // 
            this.grpMix.Controls.Add(this.btnExportar);
            this.grpMix.Controls.Add(this.btnBuscar);
            this.grpMix.Controls.Add(this.txtBusca);
            this.grpMix.Controls.Add(this.label5);
            this.grpMix.Controls.Add(this.grdMIX);
            this.grpMix.Location = new System.Drawing.Point(12, 165);
            this.grpMix.Name = "grpMix";
            this.grpMix.Size = new System.Drawing.Size(753, 397);
            this.grpMix.TabIndex = 4;
            this.grpMix.TabStop = false;
            this.grpMix.Text = "MIX de Produtos";
            // 
            // btnExportar
            // 
            this.btnExportar.Location = new System.Drawing.Point(672, 17);
            this.btnExportar.Name = "btnExportar";
            this.btnExportar.Size = new System.Drawing.Size(75, 23);
            this.btnExportar.TabIndex = 13;
            this.btnExportar.Text = "Exportar";
            this.btnExportar.UseVisualStyleBackColor = true;
            this.btnExportar.Click += new System.EventHandler(this.btnExportar_Click);
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(421, 19);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(75, 23);
            this.btnBuscar.TabIndex = 12;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // txtBusca
            // 
            this.txtBusca.Location = new System.Drawing.Point(102, 19);
            this.txtBusca.MaxLength = 30;
            this.txtBusca.Name = "txtBusca";
            this.txtBusca.Size = new System.Drawing.Size(313, 20);
            this.txtBusca.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Descrição";
            // 
            // grdMIX
            // 
            this.grdMIX.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdMIX.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.chkSel});
            this.grdMIX.Location = new System.Drawing.Point(6, 46);
            this.grdMIX.Name = "grdMIX";
            this.grdMIX.Size = new System.Drawing.Size(741, 345);
            this.grdMIX.TabIndex = 4;
            this.grdMIX.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdMIX_CellClick);
            this.grdMIX.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdMIX_CellDoubleClick);
            // 
            // chkSel
            // 
            this.chkSel.HeaderText = "Selecionar";
            this.chkSel.Name = "chkSel";
            this.chkSel.Width = 60;
            // 
            // grpCad
            // 
            this.grpCad.Controls.Add(this.chkCombo);
            this.grpCad.Controls.Add(this.chkTodas);
            this.grpCad.Controls.Add(this.chklstLoja);
            this.grpCad.Controls.Add(this.btnLimpar);
            this.grpCad.Controls.Add(this.btnSalvar);
            this.grpCad.Controls.Add(this.mskDtFim);
            this.grpCad.Controls.Add(this.mskDtIni);
            this.grpCad.Controls.Add(this.chkAtivo);
            this.grpCad.Controls.Add(this.label4);
            this.grpCad.Controls.Add(this.label3);
            this.grpCad.Controls.Add(this.txtDescricao);
            this.grpCad.Controls.Add(this.label2);
            this.grpCad.Controls.Add(this.label1);
            this.grpCad.Location = new System.Drawing.Point(12, 69);
            this.grpCad.Name = "grpCad";
            this.grpCad.Size = new System.Drawing.Size(753, 90);
            this.grpCad.TabIndex = 5;
            this.grpCad.TabStop = false;
            this.grpCad.Text = "Cadastrar MIX de Produtos";
            // 
            // chkCombo
            // 
            this.chkCombo.AutoSize = true;
            this.chkCombo.Location = new System.Drawing.Point(688, 23);
            this.chkCombo.Name = "chkCombo";
            this.chkCombo.Size = new System.Drawing.Size(59, 17);
            this.chkCombo.TabIndex = 7;
            this.chkCombo.Text = "Combo";
            this.chkCombo.UseVisualStyleBackColor = true;
            // 
            // chkTodas
            // 
            this.chkTodas.AutoSize = true;
            this.chkTodas.Location = new System.Drawing.Point(11, 52);
            this.chkTodas.Name = "chkTodas";
            this.chkTodas.Size = new System.Drawing.Size(56, 17);
            this.chkTodas.TabIndex = 3;
            this.chkTodas.Text = "Todas";
            this.chkTodas.UseVisualStyleBackColor = true;
            this.chkTodas.CheckedChanged += new System.EventHandler(this.chkTodas_CheckedChanged);
            // 
            // chklstLoja
            // 
            this.chklstLoja.FormattingEnabled = true;
            this.chklstLoja.Location = new System.Drawing.Point(73, 19);
            this.chklstLoja.Name = "chklstLoja";
            this.chklstLoja.Size = new System.Drawing.Size(164, 64);
            this.chklstLoja.TabIndex = 4;
            // 
            // btnLimpar
            // 
            this.btnLimpar.Location = new System.Drawing.Point(591, 50);
            this.btnLimpar.Name = "btnLimpar";
            this.btnLimpar.Size = new System.Drawing.Size(75, 23);
            this.btnLimpar.TabIndex = 11;
            this.btnLimpar.Text = "Limpar";
            this.btnLimpar.UseVisualStyleBackColor = true;
            this.btnLimpar.Click += new System.EventHandler(this.btnLimpar_Click);
            // 
            // btnSalvar
            // 
            this.btnSalvar.Location = new System.Drawing.Point(672, 50);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(75, 23);
            this.btnSalvar.TabIndex = 10;
            this.btnSalvar.Text = "Salvar";
            this.btnSalvar.UseVisualStyleBackColor = true;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // mskDtFim
            // 
            this.mskDtFim.Location = new System.Drawing.Point(482, 50);
            this.mskDtFim.Mask = "00/00/0000";
            this.mskDtFim.Name = "mskDtFim";
            this.mskDtFim.Size = new System.Drawing.Size(70, 20);
            this.mskDtFim.TabIndex = 9;
            this.mskDtFim.ValidatingType = typeof(System.DateTime);
            // 
            // mskDtIni
            // 
            this.mskDtIni.Location = new System.Drawing.Point(327, 50);
            this.mskDtIni.Mask = "00/00/0000";
            this.mskDtIni.Name = "mskDtIni";
            this.mskDtIni.Size = new System.Drawing.Size(70, 20);
            this.mskDtIni.TabIndex = 8;
            this.mskDtIni.ValidatingType = typeof(System.DateTime);
            // 
            // chkAtivo
            // 
            this.chkAtivo.AutoSize = true;
            this.chkAtivo.Location = new System.Drawing.Point(630, 23);
            this.chkAtivo.Name = "chkAtivo";
            this.chkAtivo.Size = new System.Drawing.Size(50, 17);
            this.chkAtivo.TabIndex = 6;
            this.chkAtivo.Text = "Ativo";
            this.chkAtivo.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(403, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Vigência Final";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(243, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Vigência Inicial";
            // 
            // txtDescricao
            // 
            this.txtDescricao.Location = new System.Drawing.Point(327, 19);
            this.txtDescricao.MaxLength = 30;
            this.txtDescricao.Name = "txtDescricao";
            this.txtDescricao.Size = new System.Drawing.Size(297, 20);
            this.txtDescricao.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(243, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Descrição";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Loja";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtCaminhoArquivo);
            this.groupBox1.Controls.Add(this.btnAbrir);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(753, 51);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Importar MIX de Produtos";
            // 
            // txtCaminhoArquivo
            // 
            this.txtCaminhoArquivo.Location = new System.Drawing.Point(9, 21);
            this.txtCaminhoArquivo.Name = "txtCaminhoArquivo";
            this.txtCaminhoArquivo.Size = new System.Drawing.Size(657, 20);
            this.txtCaminhoArquivo.TabIndex = 1;
            // 
            // btnAbrir
            // 
            this.btnAbrir.Location = new System.Drawing.Point(672, 18);
            this.btnAbrir.Name = "btnAbrir";
            this.btnAbrir.Size = new System.Drawing.Size(75, 23);
            this.btnAbrir.TabIndex = 2;
            this.btnAbrir.Text = "Abrir...";
            this.btnAbrir.UseVisualStyleBackColor = true;
            this.btnAbrir.Click += new System.EventHandler(this.btnAbrir_Click);
            // 
            // frmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(774, 574);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.grpCad);
            this.Controls.Add(this.grpMix);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CAD999 - Cadastro e Manutenção de MIX de Produtos";
            this.Activated += new System.EventHandler(this.frmPrincipal_Activated);
            this.grpMix.ResumeLayout(false);
            this.grpMix.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdMIX)).EndInit();
            this.grpCad.ResumeLayout(false);
            this.grpCad.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.GroupBox grpMix;
        private System.Windows.Forms.DataGridView grdMIX;
        private System.Windows.Forms.GroupBox grpCad;
        private System.Windows.Forms.TextBox txtDescricao;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chkAtivo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.MaskedTextBox mskDtFim;
        private System.Windows.Forms.MaskedTextBox mskDtIni;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtCaminhoArquivo;
        private System.Windows.Forms.Button btnAbrir;
        private System.Windows.Forms.Button btnLimpar;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.TextBox txtBusca;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnExportar;
        private System.Windows.Forms.CheckBox chkTodas;
        private System.Windows.Forms.CheckedListBox chklstLoja;
        private System.Windows.Forms.DataGridViewCheckBoxColumn chkSel;
        private System.Windows.Forms.CheckBox chkCombo;
    }
}

