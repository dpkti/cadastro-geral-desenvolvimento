VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form frmCadastro 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cadastro"
   ClientHeight    =   5655
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7725
   Icon            =   "frmCadastro.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   377
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   515
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      Caption         =   "Planos Cadastrados"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   2175
      Left            =   45
      TabIndex        =   15
      Top             =   3105
      Visible         =   0   'False
      Width           =   7620
      Begin MSGrid.Grid grdPlanos 
         Height          =   1815
         Left            =   90
         TabIndex        =   16
         Top             =   270
         Width           =   7440
         _Version        =   65536
         _ExtentX        =   13123
         _ExtentY        =   3201
         _StockProps     =   77
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Cols            =   4
         HighLight       =   0   'False
      End
   End
   Begin VB.Frame fra 
      Appearance      =   0  'Flat
      Caption         =   "Dados para Cadastro"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   2085
      Left            =   45
      TabIndex        =   9
      Top             =   945
      Width           =   7620
      Begin VB.ComboBox cmbNovo 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1350
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   1215
         Width           =   3165
      End
      Begin VB.TextBox txtNomeCliente 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2115
         Locked          =   -1  'True
         TabIndex        =   11
         Top             =   405
         Width           =   5325
      End
      Begin VB.ComboBox cmbSituacao 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1350
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   1620
         Width           =   1725
      End
      Begin VB.ComboBox cmbOriginal 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1350
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   810
         Width           =   3165
      End
      Begin VB.TextBox txtCodCliente 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1350
         TabIndex        =   1
         Top             =   405
         Width           =   690
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         Caption         =   "Situa��o:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   540
         TabIndex        =   14
         Top             =   1665
         Width           =   870
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         Caption         =   "Plano Novo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   270
         TabIndex        =   13
         Top             =   1260
         Width           =   1185
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         Caption         =   "Plano Original:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   135
         TabIndex        =   12
         Top             =   900
         Width           =   1365
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         Caption         =   "Cliente:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   630
         TabIndex        =   10
         Top             =   450
         Width           =   690
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   5325
      Width           =   7725
      _ExtentX        =   13626
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   13573
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   5
      Top             =   810
      Width           =   7665
      _ExtentX        =   13520
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   6
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadastro.frx":23D2
      PICN            =   "frmCadastro.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd1 
      Height          =   690
      Left            =   855
      TabIndex        =   7
      TabStop         =   0   'False
      ToolTipText     =   "Gravar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadastro.frx":30C8
      PICN            =   "frmCadastro.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd2 
      Height          =   690
      Left            =   1575
      TabIndex        =   8
      TabStop         =   0   'False
      ToolTipText     =   "Cancelar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadastro.frx":3DBE
      PICN            =   "frmCadastro.frx":3DDA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd3 
      Height          =   690
      Left            =   2295
      TabIndex        =   17
      TabStop         =   0   'False
      ToolTipText     =   "Cancelar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadastro.frx":46B4
      PICN            =   "frmCadastro.frx":46D0
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmCadastro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public VACT As String

Private Sub cmd1_Click()

    If txtCodCliente.Text = "" Then
        MsgBox "Selecione um Cliente", , "Aten��o"
        txtCodCliente.SetFocus
        Exit Sub
    End If
    
    
    If VACT <> "A" Then
        If cmbOriginal.Text = "" Then
            MsgBox "Selecione o Plano Original", , "Aten��o"
            'cmbOriginal.SetFocus
            Exit Sub
        End If
        
        If cmbNovo.Text = "" Then
            MsgBox "Selecione o Plano Novo", , "Aten��o"
            'cmbNovo.SetFocus
            Exit Sub
        End If
    End If
    
    
    If cmbSituacao.Text = "" Then
        MsgBox "Selecione a situa��o", , "Aten��o"
        cmbSituacao.SetFocus
        Exit Sub
    End If
    
    
    If cmbOriginal.Text = cmbNovo.Text Then
    
        MsgBox "O plano Novo deve ser diferente do Original", , "Aten��o"
        cmbSituacao.SetFocus
        Exit Sub
    
    End If
    
    If VACT = "I" Then
        If cmbOriginal.ItemData(cmbOriginal.ListIndex) <> cmbNovo.ItemData(cmbNovo.ListIndex) Then
    
            MsgBox "Numero de parcelas do Novo deve ser igual ao Original", , "Aten��o"
            cmbSituacao.SetFocus
            Exit Sub
        End If
        
        grdPlanos.Col = 3
        For i = 1 To grdPlanos.Rows - 1
            grdPlanos.Row = i
            If cmbOriginal.Text = grdPlanos.Text Then
                MsgBox "Plano Original j� cadastrado", , "Aten��o"
                cmbOriginal.SetFocus
                Exit Sub
            End If
        Next
    End If
    
    If cmbSituacao.Text = "ATIVO" Then
        vSIT = 0
    Else
        vSIT = 9
    End If
    
    
    If cmbNovo.Text = "" Then
        codorig = 0
    Else
        codorig = Left(cmbNovo.Text, InStr(cmbNovo.Text, " - ") - 1)
    End If
    
    vBanco.Parameters.Remove "PM_CODCLI"
    vBanco.Parameters.Add "PM_CODCLI", txtCodCliente.Text, 1
    
    vBanco.Parameters.Remove "PM_PGTORIG"
    vBanco.Parameters.Add "PM_PGTORIG", Left(cmbOriginal.Text, InStr(cmbOriginal.Text, " - ") - 1), 1
    
    vBanco.Parameters.Remove "PM_PGTNOVO"
    vBanco.Parameters.Add "PM_PGTNOVO", codorig, 1
    
    vBanco.Parameters.Remove "PM_SITUACAO"
    vBanco.Parameters.Add "PM_SITUACAO", vSIT, 1
    
    vBanco.Parameters.Remove "PM_ACT"
    vBanco.Parameters.Add "PM_ACT", VACT, 1

    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2

    vSql = "Producao.PCK_CAD980.PR_act_CLIENTEPLANO(:PM_CODCLI,:PM_PGTORIG,:PM_PGTNOVO,:PM_SITUACAO,:PM_ACT,:PM_CODERRO,:PM_TXTERRO)"

    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    If VACT = "A" Then
        MsgBox "Plano Alterado com sucesso!", , ""
    Else
        MsgBox "Plano Incluido com sucesso!", , ""
    End If
    
    VACT = "I"
    Call cmd2_Click
    
    txtCodCliente.Text = vBanco.Parameters("PM_CODCLI")
    
End Sub

Public Sub cmd2_Click()
    
    txtCodCliente.Text = ""
    txtNomeCliente.Text = ""
    
    cmbNovo.ListIndex = -1
    cmbOriginal.ListIndex = -1
    cmbSituacao.ListIndex = -1
    
    cmbNovo.Enabled = True
    cmbOriginal.Enabled = True
    
    cmd3.Enabled = False
    
    Frame1.Visible = False
    vVB_Generica_001.LimpaGridComTitulo grdPlanos
    
    frmCadastro.Height = 3780
        
End Sub

Private Sub cmd3_Click()

    vBanco.Parameters.Remove "PM_CODCLI"
    vBanco.Parameters.Add "PM_CODCLI", txtCodCliente.Text, 1
    
    vBanco.Parameters.Remove "PM_PGTORIG"
    vBanco.Parameters.Add "PM_PGTORIG", Left(cmbOriginal.Text, InStr(cmbOriginal.Text, " - ") - 1), 1

    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2

    vSql = "Producao.PCK_CAD980.PR_DEL_CLIENTEPLANO(:PM_CODCLI,:PM_PGTORIG,:PM_CODERRO,:PM_TXTERRO)"

    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    MsgBox "Plano excluido com sucesso!", , ""
    
    VACT = "I"
    Call cmd2_Click
    
    txtCodCliente.Text = vBanco.Parameters("PM_CODCLI")


End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    
    Me.Top = 1500
    Me.Left = 300
    
    Me.Height = 3780
    
    cmbSituacao.AddItem "ATIVO"
    cmbSituacao.AddItem "CANCELADO"
    

    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
    
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "Producao.PCK_CAD980.PR_CON_PLANOSPAGTO(:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
    
    While Not vObjOracle.EOF
        
        cmbOriginal.AddItem vObjOracle("cod_plano") & " - " & vObjOracle("desc_plano")
        cmbOriginal.ItemData(cmbOriginal.ListCount - 1) = vObjOracle("num_pgto")
        
        vObjOracle.MoveNext
    Wend
    
    vObjOracle.MoveFirst
    
    While Not vObjOracle.EOF
        
        cmbNovo.AddItem vObjOracle("cod_plano") & " - " & vObjOracle("desc_plano")
        cmbNovo.ItemData(cmbNovo.ListCount - 1) = vObjOracle("num_pgto")
        
        vObjOracle.MoveNext
    Wend
    
End Sub

Private Sub grdPlanos_DblClick()
On Error GoTo trata
    
    grdPlanos.Col = 3
    cmbOriginal.Text = grdPlanos.Text
    cmbOriginal.Enabled = False
    
    grdPlanos.Col = 4
    cmbNovo.Text = grdPlanos.Text
    cmbNovo.Enabled = False
    
    cmd3.Enabled = True
    
    grdPlanos.Col = 5
    If grdPlanos.Text = 0 Then
        cmbSituacao.ListIndex = 0
    Else
        cmbSituacao.ListIndex = 1
    End If
    
    VACT = "A"
    
trata:
    If Err.Number = 383 Then
        Resume Next
    End If

End Sub

Private Sub txtCodCliente_LOSTFOCUS()

    vVB_Generica_001.LimpaGridComTitulo grdPlanos

    If txtCodCliente.Text <> "" And txtCodCliente.Text <> "0" Then
    
        Set vObjOracle = vVB_Generica_001.TabelaCliente(vBanco, txtCodCliente, , , , , , , , , 0)
        
        If Not vObjOracle.EOF Then
        
            txtNomeCliente.Text = vObjOracle("CLIENTE")

            vBanco.Parameters.Remove "PM_CODCLIENTE"
            vBanco.Parameters.Add "PM_CODCLIENTE", txtCodCliente.Text, 1

            vBanco.Parameters.Remove "PM_CURSOR1"
            vBanco.Parameters.Add "PM_CURSOR1", 0, 3
            vBanco.Parameters("PM_CURSOR1").ServerType = 102
            vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
            vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

            vBanco.Parameters.Remove "PM_CODERRO"
            vBanco.Parameters.Add "PM_CODERRO", 0, 2
            vBanco.Parameters.Remove "PM_TXTERRO"
            vBanco.Parameters.Add "PM_TXTERRO", "", 2

            vSql = "Producao.PCK_CAD980.PR_CON_CLIENTEPLANO(:PM_CODCLIENTE,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"

            vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

            Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value

            If Not vObjOracle.EOF Then
            
                vVB_Generica_001.CarregaGridTabela grdPlanos, vObjOracle, 6

            End If
            
            frmCadastro.Height = 6030
            Frame1.Visible = True
            
            VACT = "I"
        Else
            txtNomeCliente.Text = ""
            cmbOriginal.ListIndex = -1
            cmbNovo.ListIndex = -1
            cmbSituacao.ListIndex = -1
            cmbOriginal.Enabled = True
            cmbNovo.Enabled = True
            
            frmCadastro.Height = 3780
            Frame1.Visible = False
        End If
    Else
        cmd2_Click
    End If

End Sub

Private Sub txtCodCliente_DblClick()
    frmClientes.Show 1
End Sub

Private Sub txtCodCliente_LOSTFOCUS2()
    If txtCodCliente.Text <> "" And txtNomeCliente.Text = "" Then
        MsgBox "C�digo de Cliente Inv�lido!!", , "Aten��o"
        txtCodCliente.Text = ""
    End If
End Sub
