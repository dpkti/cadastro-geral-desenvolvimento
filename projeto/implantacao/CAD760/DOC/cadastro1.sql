[TABELAS]
PRODUCAO.ITEM_CADASTRO
PRODUCAO.ITEM_ESTOQUE
COMPRAS.R_ITEM_EDI
PRODUCAO.ITEM_GLOBAL
PRODUCAO.ITEM_ANALITICO
TRANSFERENCIA.ESTOQUE_MINIMO
[COLUNAS]
PRODUCAO.ITEM_CADASTRO.COD_DPK
PRODUCAO.ITEM_CADASTRO.COD_FORNECEDOR
PRODUCAO.ITEM_CADASTRO.COD_FABRICA
PRODUCAO.ITEM_CADASTRO.DESC_ITEM
PRODUCAO.ITEM_CADASTRO.COD_LINHA
PRODUCAO.ITEM_CADASTRO.COD_GRUPO
PRODUCAO.ITEM_CADASTRO.COD_SUBGRUPO
PRODUCAO.ITEM_CADASTRO.MASCARADO
PRODUCAO.ITEM_GLOBAL.SEQUENCIA_TAB
PRODUCAO.ITEM_CADASTRO.CLASS_FISCAL
PRODUCAO.ITEM_CADASTRO.PESO
PRODUCAO.ITEM_CADASTRO.VOLUME
PRODUCAO.ITEM_CADASTRO.COD_UNIDADE
PRODUCAO.ITEM_CADASTRO.COD_TRIBUTACAO
PRODUCAO.ITEM_CADASTRO.COD_TRIBUTACAO_IPI
PRODUCAO.ITEM_CADASTRO.PC_IPI
PRODUCAO.ITEM_CADASTRO.COD_PROCEDENCIA
PRODUCAO.ITEM_CADASTRO.QTD_MINFORN
PRODUCAO.ITEM_CADASTRO.QTD_MINVDA
PRODUCAO.ITEM_ESTOQUE.QTD_MAXVDA
COMPRAS.R_ITEM_EDI.COD_FABRICA_EDI
PRODUCAO.ITEM_ANALITICO.CATEGORIA
TRANSFERENCIA.ESTOQUE_MINIMO.MIN_ESTOQUE
[CRITERIOS]
[(]
[CAMPO1]
PRODUCAO.ITEM_CADASTRO.COD_DPK
[OPER]
=
[CAMPO2]
COMPRAS.R_ITEM_EDI.COD_DPK(+)
[)]
[OPER]
AND
[(]
[CAMPO1]
PRODUCAO.ITEM_CADASTRO.COD_DPK
[OPER]
=
[CAMPO2]
PRODUCAO.ITEM_ANALITICO.COD_DPK
[)]
[OPER]
AND
[(]
[CAMPO1]
PRODUCAO.ITEM_ANALITICO.COD_LOJA
[OPER]
=
[CAMPO2]
PRODUCAO.ITEM_ESTOQUE.COD_LOJA
[)]
[OPER]
AND
[(]
[CAMPO1]
PRODUCAO.ITEM_CADASTRO.COD_DPK
[OPER]
=
[CAMPO2]
PRODUCAO.ITEM_ESTOQUE.COD_DPK
[)]
[OPER]
AND
[(]
[CAMPO1]
PRODUCAO.ITEM_CADASTRO.COD_DPK
[OPER]
=
[CAMPO2]
PRODUCAO.ITEM_GLOBAL.COD_DPK
[)]
[OPER]
AND
[(]
[CAMPO1]
TRANSFERENCIA.ESTOQUE_MINIMO.COD_DPK
[OPER]
=
[CAMPO2]
PRODUCAO.ITEM_CADASTRO.COD_DPK
[)]
[OPER]
AND
[(]
[CAMPO1]
PRODUCAO.ITEM_CADASTRO.COD_FORNECEDOR
[OPER]
IN
[CAMPO2]
(570)
[)]
[OPER]
AND
[(]
[CAMPO1]
PRODUCAO.ITEM_ESTOQUE.SITUACAO
[OPER]
=
[CAMPO2]
0
[)]
[OPER]
AND
[(]
[CAMPO1]
PRODUCAO.ITEM_ESTOQUE.COD_LOJA
[OPER]
=
[CAMPO2]
1
[)]
[OPER]
[ORDENAR]
[AGRUPAR]
[FIM]
