VERSION 4.00
Begin VB.Form frmCadastro 
   Caption         =   "CAD760 - Altera��o Autom�tica do Cadastro de Itens DPK por Leitura de Arquivo"
   ClientHeight    =   8400
   ClientLeft      =   1800
   ClientTop       =   1605
   ClientWidth     =   10290
   Height          =   8805
   Left            =   1740
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   8400
   ScaleWidth      =   10290
   Top             =   1260
   Width           =   10410
   WindowState     =   2  'Maximized
   Begin Threed.SSFrame SSFrame2 
      Height          =   7695
      Left            =   240
      TabIndex        =   0
      Top             =   255
      Width           =   9765
      _Version        =   65536
      _ExtentX        =   17224
      _ExtentY        =   13573
      _StockProps     =   14
      ForeColor       =   12582912
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin Threed.SSFrame SSFrame3 
         Height          =   1455
         Left            =   240
         TabIndex        =   1
         Top             =   255
         Width           =   9270
         _Version        =   65536
         _ExtentX        =   16351
         _ExtentY        =   2566
         _StockProps     =   14
         Caption         =   "Arquivo"
         ForeColor       =   12582912
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Begin VB.Label lblHelp1 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H0000FFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Abrir Arquivo"
            ForeColor       =   &H80000008&
            Height          =   225
            Left            =   1320
            TabIndex        =   4
            Top             =   1200
            Visible         =   0   'False
            Width           =   930
         End
         Begin Threed.SSCommand cmdArquivo 
            Height          =   855
            Left            =   375
            TabIndex        =   3
            Top             =   390
            Width           =   1335
            _Version        =   65536
            _ExtentX        =   2355
            _ExtentY        =   1508
            _StockProps     =   78
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Picture         =   "frmCadas.frx":0000
         End
         Begin MSComDlg.CommonDialog controle 
            Left            =   120
            Top             =   240
            _ExtentX        =   847
            _ExtentY        =   847
            _Version        =   393216
            DefaultExt      =   "*.PRN"
            InitDir         =   "C:\CAD760"
         End
         Begin VB.Label lblArquivo 
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H00C00000&
            Height          =   375
            Left            =   1875
            TabIndex        =   2
            Top             =   765
            Width           =   6990
         End
      End
      Begin VB.Label lblMsg 
         AutoSize        =   -1  'True
         Caption         =   "Mensagem"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   240
         Left            =   255
         TabIndex        =   10
         Top             =   6360
         Width           =   1155
      End
      Begin MSGrid.Grid grdArquivo 
         Height          =   4200
         Left            =   210
         TabIndex        =   9
         Top             =   1890
         Width           =   9315
         _Version        =   65536
         _ExtentX        =   16431
         _ExtentY        =   7408
         _StockProps     =   77
         ForeColor       =   8388608
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Cols            =   31
         FixedRows       =   0
      End
      Begin Threed.SSCommand cmdVoltar 
         Height          =   855
         Left            =   8115
         TabIndex        =   8
         Top             =   6360
         Width           =   1335
         _Version        =   65536
         _ExtentX        =   2355
         _ExtentY        =   1508
         _StockProps     =   78
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "frmCadas.frx":031A
      End
      Begin Threed.SSCommand cmdConfirma 
         Height          =   855
         Left            =   6480
         TabIndex        =   7
         Top             =   6360
         Width           =   1335
         _Version        =   65536
         _ExtentX        =   2355
         _ExtentY        =   1508
         _StockProps     =   78
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "frmCadas.frx":0634
      End
      Begin VB.Label lblHelp2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H0000FFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Gerar Arq.de Log"
         ForeColor       =   &H80000008&
         Height          =   225
         Left            =   4710
         TabIndex        =   6
         Top             =   7260
         Visible         =   0   'False
         Width           =   1245
      End
      Begin Threed.SSCommand cmdLog 
         Height          =   855
         Left            =   4785
         TabIndex        =   5
         Top             =   6360
         Width           =   1335
         _Version        =   65536
         _ExtentX        =   2355
         _ExtentY        =   1508
         _StockProps     =   78
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "frmCadas.frx":094E
      End
   End
End
Attribute VB_Name = "frmCadastro"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Dim vNomeArq As String
Dim varLinhaArquivo As String   'Recebe a linha lida do arquivo texto
Dim fl_Log As Boolean
Dim vGuardaCol As Integer
Dim vGuardaLin As Long


Sub LIMPA_GRID()

        'Limpa dados do grid
        For I = 1 To grdArquivo.Rows - 1
                grdArquivo.RemoveItem 1
        Next I
        
End Sub





Private Sub cmdArquivo_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp1.Visible = True
End Sub

Private Sub cmdConfirma_Click()

'Dim vTamObs As Long           'Guarda a qtd. de caracteres da coluna Observa��o
'Dim vFl_SeqExiste As Boolean  'Indica se a sequ�ncia de tabela j� existe ou n�o
'Dim vSequencia As Double      'Guarda a sequ�ncia durante a pesquisa do MAX
'Dim vSeqArq As String         'Guarda a sequ�ncia que veio no arquivo
Dim vObs As String            'Guarda o conte�do da coluna de Observa��es
Dim I As Integer              'Ponteiro de linha do grid
Dim vReg_Gravado As Long      'Guarda a qtd. de itens gravados
Dim vCommit As Long           'Guarda a qtd. de itens para commitar de 20 em 20 nos CD's
Dim vLinSemObs As Long        'Guarda a qtd.de linhas sem observa��o
Dim vFl_Edi_2 As String       'Vai dizer se o fornecedor trabalha com EDI ou n�o
Dim vClassif_Forn_2 As String 'Vai dizer a classifica��o do fornecedor
Dim vCod_Fabr_Edi As String   'Vai conter o c�digo de f�brica edi
Dim vCD As String             'Vai conter o c�digo de loja para grava��o do item no cd
Dim vDep As String            'Vai conter o link do cd p/ grava��o no cd
Dim vNomeFanta As String      'Vai conter o nome fantasia do cd
Dim objconexao As Object      'Guarda resultado do select na LOJA_CONEXAO
Dim cmdPlSql As String        'Comando SQL da LOJA_CONEXAO
Dim fl_Conexao As Boolean     'Seta se pingou bem ou n�o no CD
Dim objFabrAnt As Object
Dim vFabrAnt As String
Dim vFabrAtu As String
Dim vSubGrupo As Integer
Dim vSubGrupoAnt As Integer
Dim vGrupo As Integer
Dim vGrupoAnt As Integer
Dim vDPK As Double

If MsgBox("Confirma altera��o dos dados?", vbYesNo, "CAD760") = vbYes Then

    'Primeiro vai percorrer todo o grid p/ ver se tem itens a serem gravados
    CmdPl = ""
    vLinSemObs = 0
    For I = 1 To grdArquivo.Rows - 1
        grdArquivo.Row = I
        grdArquivo.Col = 23
        vObs = grdArquivo.Text
        'Se a coluna de Observa��o n�o contiver nenhuma mensagem de inconsist�ncia vai gravar
        If vObs = "" Then
            vLinSemObs = vLinSemObs + 1
        End If
    Next I
    If vLinSemObs = 0 Then
        MsgBox "Nenhum dado poder� ser alterado!", vbCritical, "Aten��o!"
        Exit Sub
    End If
    
    'Se houverem itens a serem gravados, vai iniciar todo o processo
    'Zera contador de linhas gravadas
    vReg_Gravado = 0

    db.Parameters.Remove "DTCA": db.Parameters.Add "DTCA", vDt_Cad, 1
    db.Parameters.Remove "DTSUB": db.Parameters.Add "DTSUB", vDt_Cad, 1
    db.Parameters.Remove "AAMM": db.Parameters.Add "AAMM", vAnoMes, 1
    db.Parameters.Remove "cod_errora": db.Parameters.Add "cod_errora", 0, 2
    db.Parameters.Remove "txt_errora": db.Parameters.Add "txt_errora", "", 2
    
    'Pega linha a linha do grid para gravar as que cont�m campo de Observa��o = ""
    For I = 1 To grdArquivo.Rows - 1
        
        grdArquivo.Row = I
        grdArquivo.Col = 23
        vObs = Trim(grdArquivo.Text)
        
        'Se a coluna de Observa��o n�o contiver nenhuma mensagem de inconsist�ncia vai gravar
        If vObs = "" Then
            
            '//////////////////////////////////////////////
            'Vai alimentar as vari�veis normais e de bind
            '/////////////////////////////////////////////
            'Limpa vari�veis auxiliares de grava��o
            CmdPl = ""
            vFl_Edi_2 = ""
            vClassif_Forn_2 = ""
            vCod_Fabr_Edi = ""
            
            'Pega informa��es das 2 �ltimas colunas, para serem usadas como auxiliares na grava��o
            grdArquivo.Col = 21
            vCod_Fabr_Edi = Trim(grdArquivo.Text)
            grdArquivo.Col = 24
            vFl_Edi_2 = Trim(grdArquivo.Text)
            grdArquivo.Col = 25
            vClassif_Forn_2 = Trim(grdArquivo.Text)
            
            'Guarda informa��es da linha em refer�ncia do grid, nas vari�veis de bind
            grdArquivo.Col = 1
            db.Parameters.Remove "CODDPK" & I: db.Parameters.Add "CODDPK" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 2
            db.Parameters.Remove "FORN" & I: db.Parameters.Add "FORN" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 3
            vFabrAtu = Trim(grdArquivo.Text)
            db.Parameters.Remove "FABR" & I: db.Parameters.Add "FABR" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 4
            db.Parameters.Remove "DESC" & I: db.Parameters.Add "DESC" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 5
            db.Parameters.Remove "LINH" & I: db.Parameters.Add "LINH" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 6
            db.Parameters.Remove "GRUP" & I: db.Parameters.Add "GRUP" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 7
            db.Parameters.Remove "SUBG" & I: db.Parameters.Add "SUBG" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 8
            db.Parameters.Remove "MASC" & I: db.Parameters.Add "MASC" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 9
            db.Parameters.Remove "SEQ" & I: db.Parameters.Add "SEQ" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 10
            db.Parameters.Remove "CLAS" & I: db.Parameters.Add "CLAS" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 11
            db.Parameters.Remove "PESO" & I: db.Parameters.Add "PESO" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 12
            db.Parameters.Remove "VOLU" & I: db.Parameters.Add "VOLU" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 13
            db.Parameters.Remove "UNID" & I: db.Parameters.Add "UNID" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 14
            db.Parameters.Remove "TICM" & I: db.Parameters.Add "TICM" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 15
            db.Parameters.Remove "TIPI" & I: db.Parameters.Add "TIPI" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 16
            db.Parameters.Remove "PIPI" & I: db.Parameters.Add "PIPI" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 17
            db.Parameters.Remove "PROC" & I: db.Parameters.Add "PROC" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 18
            db.Parameters.Remove "MINF" & I: db.Parameters.Add "MINF" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 19
            db.Parameters.Remove "MINV" & I: db.Parameters.Add "MINV" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 20
            db.Parameters.Remove "MAXV" & I: db.Parameters.Add "MAXV" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 21
            db.Parameters.Remove "CEDI" & I: db.Parameters.Add "CEDI" & I, RTrim(grdArquivo.Text), 1
            grdArquivo.Col = 22
            db.Parameters.Remove "CATE" & I: db.Parameters.Add "CATE" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 26
            db.Parameters.Remove "MINEST" & I: db.Parameters.Add "MINEST" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 27
            vFabrAnt = Trim(grdArquivo.Text)
            db.Parameters.Remove "FABRANT" & I: db.Parameters.Add "FABRANT" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 28
            db.Parameters.Remove "FABRICANTE" & I: db.Parameters.Add "FABRICANTE" & I, Trim(grdArquivo.Text), 1
            
    
            '//////////////////////////////////////
            'PROCESSO PARA GRAVAR ITEM EM CAMPINAS
            '//////////////////////////////////////
            
            '28/03/03 - Aleth�a
            'Novo projeto de substitui��o n�o vai permitir que o c�d. de f�brica seja alterado neste programa
            '10/04/03 - Aleth�a
            'Solicitado a volta da altera��o autom�tica do c�d.f�br.
            CmdPl = "Update ITEM_CADASTRO " & _
                    "SET Cod_fabrica = :FABR" & I & ", Desc_Item = " & _
                        "TRANSLATE(:DESC" & I & ",'" & VAR_CAR_INVALIDO & "','" & VAR_CAR_VALIDO & "'), " & _
                        "Cod_linha = :LINH" & I & ", Cod_Grupo = :GRUP" & I & ", " & _
                        "Cod_Subgrupo = :SUBG" & I & ", Mascarado = :MASC" & I & ", " & _
                        "Class_Fiscal = :CLAS" & I & ", Peso = :PESO" & I & ", " & _
                        "Volume = :VOLU" & I & ", Cod_unidade = :UNID" & I & ", " & _
                        "Cod_Tributacao = :TICM" & I & ", Cod_Tributacao_Ipi = :TIPI" & I & ", "
            CmdPl = CmdPl & _
                        "Pc_Ipi = :PIPI" & I & ", Cod_Procedencia = :PROC" & I & ", " & _
                        "Qtd_MinForn = :MINF" & I & ", Qtd_MinVda = :MINV" & I & " " & _
                    "WHERE Cod_Dpk = :CODDPK" & I & "; "
            
            'CmdPl = "Update ITEM_CADASTRO " & _
                    "SET Desc_Item = :DESC" & I & ", " & _
                        "Cod_linha = :LINH" & I & ", Cod_Grupo = :GRUP" & I & ", " & _
                        "Cod_Subgrupo = :SUBG" & I & ", Mascarado = :MASC" & I & ", " & _
                        "Class_Fiscal = :CLAS" & I & ", Peso = :PESO" & I & ", " & _
                        "Volume = :VOLU" & I & ", Cod_unidade = :UNID" & I & ", " & _
                        "Cod_Tributacao = :TICM" & I & ", Cod_Tributacao_Ipi = :TIPI" & I & ", "
             'CmdPl = CmdPl & _
                        "Pc_Ipi = :PIPI" & I & ", Cod_Procedencia = :PROC" & I & ", " & _
                        "Qtd_MinForn = :MINF" & I & ", Qtd_MinVda = :MINV" & I & " " & _
                    "WHERE Cod_Dpk = :CODDPK" & I & "; "
           
            'Aleth�a - 10/04/03 - Se houver c�digo de f�brica na coluna 27, � porque � altera��o de c�digo de f�br., e ent�o vou atualizar a tab. R_FABRICA_DPK
            grdArquivo.Col = 27
            If (Trim(grdArquivo.Text) <> "") And (vFabrAnt <> vFabrAtu) Then
            
                CmdSql = "Select Count(*) Qtd " & _
                           "From Producao.R_FABRICA_DPK " & _
                          "Where Cod_Dpk = :CODDPK" & I & " " & _
                            "And Cod_Fabrica = :FABRANT" & I & " " & _
                            "And Fl_Tipo = 'S'"
                            
                Set objFabrAnt = db.dbcreatedynaset(CmdSql, 0&)
                
                If Val(objFabrAnt!qtd.Value) = 0 Then
                    CmdPl = CmdPl & "Insert Into Producao.R_FABRICA_DPK " & _
                                           "(Cod_Fabrica, Cod_Dpk, Dt_Cadastro, Fl_Tipo, Sigla) Values " & _
                                           "(:FABRANT" & I & ", :CODDPK" & I & ", " & _
                                           " To_Date(:DTSUB,'DD/MM/RR'), 'S', Null); "
                End If
            End If
           
            CmdPl = CmdPl & _
                    "Update ITEM_ANALITICO " & _
                    "Set Categoria = :CATE" & I & " " & _
                    "Where Cod_Dpk = :CODDPK" & I & "; "
    
            CmdPl = CmdPl & _
                    "Update Producao.R_ITEM_FABRICANTE " & _
                       "Set Cod_Fabricante = :FABRICANTE" & I & " " & _
                     "Where Cod_Dpk = :CODDPK" & I & "; " & _
                    "IF SQL%NOTFOUND Then " & _
                        "Insert into Producao.R_ITEM_FABRICANTE " & _
                        "(Cod_Dpk, Cod_Fabricante) Values " & _
                        "(:CODDPK" & I & ", :FABRICANTE" & I & "); " & _
                    "END IF; "
        
            CmdPl = CmdPl & _
                    "Update ITEM_GLOBAL " & _
                    "Set Sequencia_Tab = :SEQ" & I & " " & _
                    "Where Cod_Dpk = :CODDPK" & I & "; "
    
            CmdPl = CmdPl & _
                    "Update ITEM_ESTOQUE " & _
                    "Set Qtd_Maxvda = :MAXV" & I & " " & _
                    "WHERE Cod_Dpk = :CODDPK" & I & ";"
                    
            'Se o fornecedor trabalhar com EDI e o c�d.fabr.edi for diferente de branco
            'vai gravar na tabela compra.r_item_edi
            If vFl_Edi_2 = "S" And vCod_Fabr_Edi <> "" Then
                    
                CmdPl = CmdPl & _
                        "Update COMPRAS.R_ITEM_EDI " & _
                        "Set Cod_Fabrica_Edi = :CEDI" & I & " " & _
                        "Where cod_dpk = :CODDPK" & I & "; "
                CmdPl = CmdPl & _
                        "If SQL%NOTFOUND Then "
                CmdPl = CmdPl & _
                        "Insert Into COMPRAS.R_ITEM_EDI " & _
                        "(Cod_Dpk, Cod_Fornecedor, Cod_Fabrica_Edi) " & _
                        "VALUES (:CODDPK" & I & " ,:FORN" & I & " ,:CEDI" & I & "); "
                CmdPl = CmdPl & _
                        "End If;"
            End If
                    
                    
            CmdPl = CmdPl & _
                    "Update TRANSFERENCIA.ESTOQUE_MINIMO " & _
                    "Set Min_Estoque = :MINEST" & I & " " & _
                    "Where Cod_Dpk = :CODDPK" & I & "; "
            CmdPl = CmdPl & _
                    "If SQL%NOTFOUND Then "
            CmdPl = CmdPl & _
                    "Insert Into TRANSFERENCIA.ESTOQUE_MINIMO " & _
                    "(Cod_Dpk, Min_estoque) " & _
                    "VALUES (:CODDPK" & I & " ,:MINEST" & I & "); "
            CmdPl = CmdPl & _
                    "End If;"
                    
            'Executa transa��o gravando o item em Campinas
            'Em CPS � necess�rio gravar de 1 em 1 para n�o se perder a sequ�ncia do DPK e da tabela
            CmdPl = "BEGIN " & CmdPl & " Commit; " & _
                        "Exception " & _
                            "When Others then " & _
                                "RollBack; " & _
                        ":Cod_Errora := SQLCODE; " & _
                        ":Txt_Errora := SQLERRM; " & _
                    "End;"
            'Executa transa��es
            'On Error Resume Next
            db.ExecuteSQL CmdPl
        
            If IIf(IsNull(db.Parameters("cod_errora")), 0, Val(db.Parameters("cod_errora"))) <> 0 Then
                MsgBox db.Parameters("txt_errora") & ". Ligue p/ Depto.Sistemas"
                
                
                Exit Sub
            End If
        
        
            'Comparar as colunas de grupo e subgrupo - atual e novo - se forem diferentes, chamar a PR_ACERTA_CRUPO - 24/06/05 Ana
            grdArquivo.Col = 29
            vGrupoAnt = Trim(grdArquivo.Text)
            
            grdArquivo.Col = 6
            vGrupo = Trim(grdArquivo.Text)
            
            grdArquivo.Col = 30
            vSubGrupoAnt = Trim(grdArquivo.Text)
            
            grdArquivo.Col = 7
            vSubGrupo = Trim(grdArquivo.Text)
            
            grdArquivo.Col = 1
            vDPK = Trim(grdArquivo.Text)
            
            If vGrupoAnt <> vGrupo Or vSubGrupoAnt <> vSubGrupo Then
                db.Parameters.Remove "CODDPK": db.Parameters.Add "CODDPK", vDPK, 1
                db.Parameters.Remove "cod_errora": db.Parameters.Add "cod_errora", 0, 2
                db.Parameters.Remove "txt_errora": db.Parameters.Add "txt_errora", "", 2
                  
                CmdSql = "Begin PRODUCAO.PR_ACERTA_GRUPO(:CODDPK,:cod_errora,:txt_errora); End; "
                db.ExecuteSQL CmdSql
            
                If IsNull(db.Parameters("cod_errora").Value) Then
                    verro = 0
                Else
                    verro = db.Parameters("cod_errora").Value
                End If
                
                If Val(verro) <> 0 Then
                    MsgBox db.Parameters("txt_errora") & ".Ligue para o Depto de Sistemas", 0, "Aten��o"
                    Exit Sub
                End If
                vGrupoAnt = 0
                vGrupo = 0
                vSubGrupoAnt = 0
                vSubGrupo = 0
                vDPK = 0
            End If
        
            vReg_Gravado = vReg_Gravado + 1
            'MsgBox "Gravado " & vReg_Gravado & " itens em Campinas", vbInformation, "Aten��o!"
            
        'N�o faz nada se a linha do grid contiver msg.de inconsist�ncia no cpo.Observa��o
        End If
    
    Next I
    MsgBox "Altera��o OK! " & vReg_Gravado & " Itens Alterados em Campinas!", vbInformation, "Aten��o!"

    
    If MsgBox("Deseja alterar on-line nos CD�s?", vbYesNo, "CAD760") = vbYes Then
        
    
        '////////////////////////////////////
        'PROCESSO PARA ALTERAR ITENS NOS CD'S - Vai sempre incluir p/Cds que o Cad070 incluir
        '////////////////////////////////////
        
        db.Parameters.Remove "DTCA": db.Parameters.Add "DTCA", vDt_Cad, 1
        'db.Parameters.Remove "AAMM": db.Parameters.Add "AAMM", vAnoMes, 1
        db.Parameters.Remove "cod_errora": db.Parameters.Add "cod_errora", 0, 2
        db.Parameters.Remove "txt_errora": db.Parameters.Add "txt_errora", 0, 2
        
        CmdSql = "Select l.cod_loja CD, l.nome_fantasia NOME " & _
                 "From LOJA l, DEPOSITO_VISAO d " & _
                 "Where d.cod_loja = l.cod_loja " & _
                 "And d.cod_loja <> 1 " & _
                 "And d.nome_programa = 'CAD070'" & _
                 "Order by l.cod_loja"
        Set db_CONS = db.dbcreatedynaset(CmdSql, 0&)
        
        'Enquanto ainda houverem CD's a incluir os itens, faz o processo
        While Not db_CONS.EOF
        
            vCD = Format(Trim(db_CONS!CD.Value), "00")
            vDep = "DEP" & Format(Trim(db_CONS!CD.Value), "00") & "."
            vNomeFanta = Trim(db_CONS!NOME.Value)
            
            '////////////////////////////////////////////////////////////////////
            'TESTA TIME-OUT NO CD P/ VERIFICAR SE VALE A PENA GRAVAR ITEM ON-LINE
            '////////////////////////////////////////////////////////////////////
            db.Parameters.Remove "LJ": db.Parameters.Add "LJ", Val(vCD), 1
            
            cmdPlSql = "Select cod_loja, ip, time_out " & _
                       "From PRODUCAO.LOJA_CONEXAO " & _
                       "Where cod_loja = :LJ "
            Set objconexao = db.dbcreatedynaset(cmdPlSql, 0&)
                        
            If objconexao.EOF Then
                MsgBox "CD " & vCD & " n�o cadastrada na tabela LOJA_CONEXAO! " & Chr(13) & _
                        "Chame o analista respons�vel.", vbCritical, "Aten��o"
                Exit Sub
            End If
                    
            IP = objconexao!IP.Value
            TIMEOUT = Val(objconexao!TIME_OUT.Value)
            CDLJ = objconexao!cod_loja.Value
            success = Ping(IP, CDLJ, ECHO)
                    
            'Vai verificar se o tempo do ping, compensar a conex�o com o CD
            If InStr(GetStatusCode(success), ("ip success")) = 0 Or _
                ECHO.RoundTripTime > TIMEOUT Then
                MsgBox "CD " & CDLJ & " sem comunica��o!", vbInformation, "Aten��o"
                fl_Conexao = False
            Else
                fl_Conexao = True
            End If
                
            'Caso o timeout com o CD compense, prepara itens para serem gravado no CD
            If fl_Conexao = True Then
                    
                CmdPl = ""
                    
                vReg_Gravado = 0
                vCommit = 0
                
                'Pega linha a linha do grid para gravar as que cont�m campo de Observa��o <> ""
                For I = 1 To grdArquivo.Rows - 1
                    
                    grdArquivo.Row = I
                    grdArquivo.Col = 23
                    vObs = grdArquivo.Text
                    
                    'Se a coluna de Observa��o n�o contiver nenhuma mensagem de inconsist�ncia vai gravar
                    If vObs = "" Then
                        
                        vReg_Gravado = vReg_Gravado + 1
                    
                        'Vai alimentar as vari�veis normais e de bind
                        'Limpa vari�veis auxiliares de grava��o
                        vFl_Edi_2 = ""
                        vClassif_Forn_2 = ""
                        vCod_Fabr_Edi = ""
                                    
                        'Pega informa��es das 2 �ltimas colunas, para serem usadas como auxiliares na grava��o
                        grdArquivo.Col = 21
                        vCod_Fabr_Edi = Trim(grdArquivo.Text)
                        grdArquivo.Col = 24
                        vFl_Edi_2 = Trim(grdArquivo.Text)
                        grdArquivo.Col = 25
                        vClassif_Forn_2 = Trim(grdArquivo.Text)
                                    
                        'Guarda informa��es da linha em refer�ncia do grid, nas vari�veis de bind
                        grdArquivo.Col = 1
                        db.Parameters.Remove "CODDPK" & I: db.Parameters.Add "CODDPK" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 2
                        db.Parameters.Remove "FORN" & I: db.Parameters.Add "FORN" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 3
                        vFabrAtu = Trim(grdArquivo.Text)
                        db.Parameters.Remove "FABR" & I: db.Parameters.Add "FABR" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 4
                        db.Parameters.Remove "DESC" & I: db.Parameters.Add "DESC" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 5
                        db.Parameters.Remove "LINH" & I: db.Parameters.Add "LINH" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 6
                        db.Parameters.Remove "GRUP" & I: db.Parameters.Add "GRUP" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 7
                        db.Parameters.Remove "SUBG" & I: db.Parameters.Add "SUBG" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 8
                        db.Parameters.Remove "MASC" & I: db.Parameters.Add "MASC" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 9
                        db.Parameters.Remove "SEQ" & I: db.Parameters.Add "SEQ" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 10
                        db.Parameters.Remove "CLAS" & I: db.Parameters.Add "CLAS" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 11
                        db.Parameters.Remove "PESO" & I: db.Parameters.Add "PESO" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 12
                        db.Parameters.Remove "VOLU" & I: db.Parameters.Add "VOLU" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 13
                        db.Parameters.Remove "UNID" & I: db.Parameters.Add "UNID" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 14
                        db.Parameters.Remove "TICM" & I: db.Parameters.Add "TICM" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 15
                        db.Parameters.Remove "TIPI" & I: db.Parameters.Add "TIPI" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 16
                        db.Parameters.Remove "PIPI" & I: db.Parameters.Add "PIPI" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 17
                        db.Parameters.Remove "PROC" & I: db.Parameters.Add "PROC" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 18
                        db.Parameters.Remove "MINF" & I: db.Parameters.Add "MINF" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 19
                        db.Parameters.Remove "MINV" & I: db.Parameters.Add "MINV" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 20
                        db.Parameters.Remove "MAXV" & I: db.Parameters.Add "MAXV" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 21
                        db.Parameters.Remove "CEDI" & I: db.Parameters.Add "CEDI" & I, RTrim(grdArquivo.Text), 1
                        grdArquivo.Col = 22
                        db.Parameters.Remove "CATE" & I: db.Parameters.Add "CATE" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 27
                        vFabrAnt = Trim(grdArquivo.Text)
                        db.Parameters.Remove "FABRANT" & I: db.Parameters.Add "FABRANT" & I, Trim(grdArquivo.Text), 1
                        
                        
                        '//////////////////////////////////////
                        'PROCESSO PARA GRAVAR ITEM NOS CDS
                        '//////////////////////////////////////
                
                        '28/03/03 - Aleth�a
                        'Novo projeto de substitui��o n�o vai permitir que o c�d. de f�brica seja alterado neste programa
                        '10/04/03 - Aleth�a
                        'Solicitado a volta da altera��o autom�tica do c�d.f�br.
                        CmdPl = CmdPl & " Update " & vDep & "ITEM_CADASTRO " & _
                                "SET Cod_fabrica = :FABR" & I & ", Desc_Item = " & _
                                    "TRANSLATE(:DESC" & I & ",'" & VAR_CAR_INVALIDO & "','" & VAR_CAR_VALIDO & "'), " & _
                                    "Cod_linha = :LINH" & I & ", Cod_Grupo = :GRUP" & I & ", " & _
                                    "Cod_Subgrupo = :SUBG" & I & ", Mascarado = :MASC" & I & ", " & _
                                    "Class_Fiscal = :CLAS" & I & ", Peso = :PESO" & I & ", " & _
                                    "Volume = :VOLU" & I & ", Cod_unidade = :UNID" & I & ", " & _
                                    "Cod_Tributacao = :TICM" & I & ", Cod_Tributacao_Ipi = :TIPI" & I & ", "
                        CmdPl = CmdPl & _
                                    "Pc_Ipi = :PIPI" & I & ", Cod_Procedencia = :PROC" & I & ", " & _
                                    "Qtd_MinForn = :MINF" & I & ", Qtd_MinVda = :MINV" & I & " " & _
                                "Where Cod_Dpk = :CODDPK" & I & "; "
                        
                        'CmdPl = CmdPl & " Update " & vDep & "ITEM_CADASTRO " & _
                                "SET Desc_Item = :DESC" & I & ", " & _
                                    "Cod_linha = :LINH" & I & ", Cod_Grupo = :GRUP" & I & ", " & _
                                    "Cod_Subgrupo = :SUBG" & I & ", Mascarado = :MASC" & I & ", " & _
                                    "Class_Fiscal = :CLAS" & I & ", Peso = :PESO" & I & ", " & _
                                    "Volume = :VOLU" & I & ", Cod_unidade = :UNID" & I & ", " & _
                                    "Cod_Tributacao = :TICM" & I & ", Cod_Tributacao_Ipi = :TIPI" & I & ", "
                        'CmdPl = CmdPl & _
                                    "Pc_Ipi = :PIPI" & I & ", Cod_Procedencia = :PROC" & I & ", " & _
                                    "Qtd_MinForn = :MINF" & I & ", Qtd_MinVda = :MINV" & I & " " & _
                                "Where Cod_Dpk = :CODDPK" & I & "; "
                        
                        
                        'Insere no CD, se n�o encontrar
                        CmdPl = CmdPl & _
                                "If SQL%NOTFOUND Then "
                        
                                CmdPl = CmdPl & _
                                    "Insert into " & vDep & "ITEM_CADASTRO " & _
                                    "(cod_Dpk, cod_Fornecedor, cod_fabrica, desc_item, cod_linha," & _
                                    " cod_grupo, cod_subgrupo, dt_cadastramento, mascarado, class_fiscal," & _
                                    " peso, volume, cod_unidade, cod_tributacao, cod_tributacao_ipi," & _
                                    " pc_ipi, cod_procedencia, cod_dpk_ant, qtd_minforn, qtd_minvda) "
                                CmdPl = CmdPl & _
                                    "Values " & _
                                    "(:CODDPK" & I & ", :FORN" & I & ", :FABR" & I & ", " & _
                                    " TRANSLATE(:DESC" & I & ",'" & VAR_CAR_INVALIDO & "','" & VAR_CAR_VALIDO & "'), " & _
                                    " :LINH" & I & ", " & _
                                    " :GRUP" & I & ", :SUBG" & I & ", To_Date(:DTCA,'DD/MM/RR'), :MASC" & I & ", " & _
                                    " :CLAS" & I & ", :PESO" & I & ", :VOLU" & I & ", :UNID" & I & ", " & _
                                    " :TICM" & I & ", :TIPI" & I & ", :PIPI" & I & ", :PROC" & I & ", 0, " & _
                                    " :MINF" & I & ", :MINV" & I & "); "
                                    vCommit = vCommit + 1
                                        
                                CmdPl = CmdPl & _
                                    "Insert into " & vDep & "ITEM_GLOBAL " & _
                                    "(cod_dpk, preco_compra, preco_compra_ant, sequencia_tab) " & _
                                    "Values " & _
                                    "(:CODDPK" & I & ", 0, 0, :SEQ" & I & "); "
                                    vCommit = vCommit + 1
                                                
                                'Se a classifica��o do fornecedor for "A" - REVENDA, insere em outras tabelas
                                If vClassif_Forn_2 = "A" Then
                                        
                                    CmdPl = CmdPl & _
                                            "Insert into " & vDep & "TBITEM " & _
                                            "(cod_dpk, preco_venda, preco_of, preco_sp, preco_compra," & _
                                            " pc_desc1, pc_desc2, pc_icm, pc_margem_vda, prazo_fabrica," & _
                                            " fator_compra, fator_venda) " & _
                                            "Values " & _
                                            "(:CODDPK" & I & ", 0.00, 0.00, 0.00, 0.000, 0.00, 0.00, " & _
                                            " 0.00, 0.00, 0, 0.0000, 0.0000); "
                                    vCommit = vCommit + 1
                                        
                                    'Se o fornecedor trabalhar com EDI e o c�d.fabr.edi for diferente de branco
                                    'vai gravar na tabela compra.r_item_edi
                                    If vFl_Edi_2 = "S" And vCod_Fabr_Edi <> "" Then
                                        CmdPl = CmdPl & _
                                                "Insert into " & vDep & "R_ITEM_EDI " & _
                                                "(cod_dpk, cod_fornecedor, cod_fabrica_edi) " & _
                                                "Values " & _
                                                "(:CODDPK" & I & ", :FORN" & I & ", :CEDI" & I & "); "
                                        vCommit = vCommit + 1
                                    End If
                                        
                                End If
                        
                        'Se encontrou na ITEM_CADASTRO do CD, continua os updates
                        CmdPl = CmdPl & _
                                "Else "
                        vCommit = vCommit + 1
                        
                            CmdPl = CmdPl & _
                                    "Update " & vDep & "ITEM_GLOBAL " & _
                                    "Set Sequencia_Tab = :SEQ" & I & " " & _
                                    "Where Cod_Dpk = :CODDPK" & I & "; "
                            vCommit = vCommit + 1
                    
                            CmdPl = CmdPl & _
                                    "Update " & vDep & "R_ITEM_EDI " & _
                                    "Set Cod_Fabrica_Edi = :CEDI" & I & " " & _
                                    "Where cod_dpk = :CODDPK" & I & "; "
                            CmdPl = CmdPl & _
                                    "If SQL%NOTFOUND Then "
                                    CmdPl = CmdPl & _
                                        "Insert Into " & vDep & "R_ITEM_EDI " & _
                                        "(Cod_Dpk, Cod_Fornecedor, Cod_Fabrica_Edi) " & _
                                        "VALUES (:CODDPK" & I & " ,:FORN" & I & " ,:CEDI" & I & "); "
                            CmdPl = CmdPl & _
                                    "End If;"
                            vCommit = vCommit + 1
        
                            
                            'Aleth�a - 10/04/03 - Se houver c�digo de f�brica na coluna 27, � porque � altera��o de c�digo de f�br., e ent�o vou atualizar a tab. R_FABRICA_DPK
                            'A partir de agora, o item vai estar na ITEM_CADASTRO e ent�o ou acertar a R_FABRICA_DPK
                            grdArquivo.Col = 27
                            If (Trim(grdArquivo.Text) <> "") And (vFabrAnt <> vFabrAtu) Then
                                
                                CmdSql = "Select Count(*) Qtd " & _
                                           "From " & vDep & "R_FABRICA_DPK " & _
                                          "Where Cod_Dpk = :CODDPK" & I & " " & _
                                            "And Cod_Fabrica = :FABRANT" & I & " " & _
                                            "And Fl_Tipo = 'S'"
                                
                                Set objFabrAnt = db.dbcreatedynaset(CmdSql, 0&)
                                
                                If Val(objFabrAnt!qtd.Value) = 0 Then
                                    CmdPl = CmdPl & "Insert Into " & vDep & "R_FABRICA_DPK " & _
                                                           "(Cod_Fabrica, Cod_Dpk, Dt_Cadastro, Fl_Tipo, Sigla) Values " & _
                                                           "(:FABRANT" & I & ", :CODDPK" & I & ", " & _
                                                           " To_Date(:DTSUB,'DD/MM/RR'), 'S', Null); "
                                    
                                    
                                    vCommit = vCommit + 1
                                End If
                            End If
                
        
                        CmdPl = CmdPl & _
                                "End If;"
                
                
                        If vCommit >= 20 Then
                                        
                            'Executa transa��o gravando o item em Campinas
                            'Em CPS � necess�rio gravar de 1 em 1 para n�o se perder a sequ�ncia do DPK e da tabela
                                        
                            CmdPl = "BEGIN " & CmdPl & "Commit; "
                            CmdPl = CmdPl & "Exception "
                            CmdPl = CmdPl & "When Others then "
                            CmdPl = CmdPl & "RollBack; "
                            CmdPl = CmdPl & ":Cod_Errora := SQLCODE; "
                            CmdPl = CmdPl & ":Txt_Errora := SQLERRM; "
                            CmdPl = CmdPl & "End;"
                            'Executa transa��es
                            db.ExecuteSQL CmdPl
                            If IIf(IsNull(db.Parameters("cod_errora")), 0, Val(db.Parameters("cod_errora"))) <> 0 Then
                                MsgBox db.Parameters("txt_errora") & ". Ligue p/ Depto.Sistemas"
                                Exit Sub
                            End If
                                
                            CmdPl = ""
                            vCommit = 0
                                    
                        End If
                
                    End If
                
                Next I
                
                'Executa transa��o
                'Executa transa��o gravando o item em Campinas
                'Em CPS � necess�rio gravar de 1 em 1 para n�o se perder a sequ�ncia do DPK e da tabela
                
                CmdPl = "BEGIN " & CmdPl & "Commit; "
                CmdPl = CmdPl & "Exception "
                CmdPl = CmdPl & "When Others then "
                CmdPl = CmdPl & "RollBack; "
                CmdPl = CmdPl & ":Cod_Errora := SQLCODE; "
                CmdPl = CmdPl & ":Txt_Errora := SQLERRM; "
                CmdPl = CmdPl & "End;"
                'Executa transa��es
                db.ExecuteSQL CmdPl
                
                If IIf(IsNull(db.Parameters("cod_errora")), 0, Val(db.Parameters("cod_errora"))) <> 0 Then
                    MsgBox db.Parameters("txt_errora") & ". Ligue p/ Depto.Sistemas"
                    Exit Sub
                End If
                
                CmdPl = ""
                'MsgBox vReg_Gravado & " Itens Alterados em " & vNomeFanta & "!", vbInformation, "Aten��o!"
    
            End If
            
            'Caso o timeout n�o tenha compensado ou j� tenha feito transa��o para CD anterior, l� pr�ximo CD
            db_CONS.MoveNext
            
        Wend

        MsgBox "Altera��es on-line OK!", vbInformation, "Aten��o!"
    End If

End If

End Sub
Private Sub cmdConfirma_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp2.Left = 6570
    lblHelp2.Top = 7260
    lblHelp2.Visible = True
    lblHelp2.Caption = "Gravar Dados"
End Sub


Private Sub cmdLog_Click()

Dim I As Integer        'Ponteiro de linha do grid
Dim J As Long           'Ponteiro de caracter no campo observa��o
Dim vTamObs As Long     'Guarda a qtd. de caracteres da coluna Observa��o
Dim vObs As String      'Guarda o conte�do da coluna de Observa��es
Dim vForn As String     'Guarda o c�digo do fornecedor para impress�o
Dim vFabr As String     'Guarda o c�digo de f�brica para impress�o
Dim vDPK As String      'Guarda o c�digo DPK para impress�o
    
    
If MsgBox("Confirma gera��o do arquivo de log?", vbYesNo, "CAD750") = vbYes Then

    'Gera arquivo de log com o nome LODDMMAA.TXT
    Open "C:\CAD760\LOG\Lo" & Mid(vDt_Cad, 1, 2) & _
                        Mid(vDt_Cad, 4, 2) & Mid(vDt_Cad, 7, 2) & ".txt" For Output As #1
    'Open "C:\arquivos-alethea\CAD760\LOG\Lo" & Mid(vDt_Cad, 1, 2) & _
                        Mid(vDt_Cad, 4, 2) & Mid(vDt_Cad, 7, 2) & ".txt" For Output As #1
    
    Print #1, "LOG DO ARQUIVO: " & vNomeArq
    Print #1, ""
    Print #1, "DPK   Forn  Fabrica             Observa��es"
    Print #1, "----- ----  ------------------- ------------------------------------------------------"
    
    'Pega linha a linha do grid para gravar as que cont�m campo de Observa��o <> ""
    For I = 1 To grdArquivo.Rows - 1
        
        grdArquivo.Row = I
        
        grdArquivo.Col = 1
        vDPK = Trim(grdArquivo.Text)        'Pega o c�digo DPK
        grdArquivo.Col = 2
        vForn = Trim(grdArquivo.Text)       'Pega o c�digo do fornecedor
        grdArquivo.Col = 3
        vFabr = Trim(grdArquivo.Text)       'Pega o c�digo de f�brica
        grdArquivo.Col = 23
        vObs = Trim(grdArquivo.Text)        'Pega as Observa��es
        
        'Se a coluna de Observa��o n�o estiver em branco
        If vObs <> "" Then
            
            'Zero ponteiro que vai me indicar que caracter no campo obs. estou lendo
            J = 0
            'Pego o tamanho da observa��o para esta linha
            vTamObs = CLng(Len(vObs))
            
            'Se o campo de observa��o contiver mais de 49 caracteres, o arquivo de log ter�
            'que conter mais de 1 linha de mensagens para aquele item
            'Conto 49 pois antes da Obs., a 1a. linha ir� conter o C�d.DPK - C�d.Forn - C�d.F�br. (31 caracteres)
            'E para a impress�o correta do arquivo s� pode ter 80 colunas
            
            'Se for menor ou igual a 49, gravo a linha no arquivo normalmente
            If vTamObs <= 49 Then
                Print #1, String(5 - Len(vDPK), " ") & vDPK & " " & _
                          String(3 - Len(vForn), " ") & vForn & " - " & _
                          vFabr & Space(19 - Len(vFabr)) & " " & vObs
            'Sen�o
            Else
                'Imprimo os primeiros 49 caracteres
                Print #1, String(5 - Len(vDPK), " ") & vDPK & " " & _
                          String(3 - Len(vForn), " ") & vForn & " - " & _
                          vFabr & Space(19 - Len(vFabr)) & " " & Mid(vObs, 1, 49)
                'Tamanho agora vai ser igual ao tamanho original - 49
                vTamObs = vTamObs - 49
                'Indico de que caracter vou come�ar a ler no cpo. Observa��o
                J = J + 50
                
                'Enquanto o tamanho for maior que 0
                Do While vTamObs > 0
                    'Se tamanho maior que 49, imprime os pr�ximos 49 caracteres e atualiza tam.e ponteiro
                    If vTamObs > 49 Then
                        Print #1, Space(31) & Mid(vObs, J, 49)
                        vTamObs = vTamObs - 49
                        J = J + 50
                    'Se tamanho menor que 49, imprime os caracteres que faltam
                    Else
                        Print #1, Space(31) & Mid(vObs, J, vTamObs)
                        vTamObs = 0
                    End If
                Loop
            End If
            
        End If
            
    Next I

    Print #1, ""
    Print #1, "***************** FIM DO ARQUIVO DE LOG ***********************"
    
    Close #1

    MsgBox "Arquivo gerado com sucesso!", vbInformation, "Aten��o!"


End If

End Sub

Private Sub cmdLog_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp2.Left = 4695
    lblHelp2.Top = 7260
    lblHelp2.Visible = True
    lblHelp2.Caption = "Gerar Arq.de Log"
End Sub


Private Sub cmdVoltar_Click()
    Unload Me
End Sub

Private Sub cmdVoltar_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp2.Left = 8820
    lblHelp2.Top = 7260
    lblHelp2.Visible = True
    lblHelp2.Caption = "Voltar"
End Sub


Private Sub Form_Load()
    
    grdArquivo.Visible = False
    lblHelp1.Visible = False
    lblHelp2.Visible = False
    lblMsg.Visible = False

End Sub



Private Sub lblHelp1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp1.Visible = False
End Sub


Private Sub cmdArquivo_Click()
    
    'Vari�veis locais
    Dim I As Integer              'Ponteiro / Contador
    Dim vStrLinhaArq As String    'Guarda o peda�o (Coluna) do arquivo que est� sendo lido
    Dim vDivisao As String        'Guarda a divis�o a que o fornecedor pertence
    Dim vDPK As Double            'Guarda o c�digo DPK se o item j� estiver cadastrado
    'Dim vFl_Forn As Boolean       'Indica se fornecedor atendeu �s condi��es
    'Dim vFl_Fabr As Boolean       'Indica se f�brica atendeu �s condi��es
    Dim vCod_Trib_ICM As String   'Guarda o c�d. trib. Icm para depois validar class. fiscal
    Dim vCod_Trib_IPI As String   'Guarda o c�d. trib. Ipi para depois validar proced�ncia
    Dim vFl_Edi As String         'Guarda flag que diz se fornecedor trabalha com EDI ou n�o
    Dim vClassif_Forn As String   'Guarda classifica��o do fornecedor
    
    Dim vCodForn As Long            'Guarda o c�digo do fornecedor ref. ao DPK vindo do arquivo
    Dim vCodFabr As String          'Guarda o c�digo de f�brica ref. ao DPK vindo do arquivo
    Dim vSit_Dpk As Integer         'Guarda a situa��o do DPK vindo do arquivo
    Dim vFl_DPK_OK As Boolean       'Indica se DPK est� Ok ou n�o (se n�o estiver, nem continua demais valida��es
    Dim vSit_Forn As Integer        'Guarda a situa��o do Fornecedor vindo do arquivo
    Dim objSubTrib As Object
    Dim vTexto As String
    
    '29/08/08 - Sempre que alterar a classifica��o fiscal, enviar e-mail ao Fiscal
    Dim vClassFiscal_Ant As String
    Dim vClassFiscal_Atu As String
    
    
    'On Error GoTo Trata_Erro
    
    'Indica que o controle ser� usado para abrir o arquivo
    controle.ShowOpen
    lblArquivo.Caption = controle.filename
    vNomeArq = lblArquivo

    'Verifica se foi indicado o diret�rio e nome do arquivo a ser lido
    If lblArquivo.Caption = "" Then
        MsgBox "Indique o arquivo a ser aberto", , MSG_TIT
        Exit Sub
    End If


    '/////////////////////////
    'MONTA O GRID
    '/////////////////////////
    grdArquivo.Visible = True

    grdArquivo.Row = 0
    
    grdArquivo.Col = 0
    grdArquivo.ColWidth(0) = 450
    grdArquivo.Text = "Reg"
    
    grdArquivo.Col = 1
    grdArquivo.ColWidth(1) = 500
    grdArquivo.Text = "Dpk"
    
    grdArquivo.Col = 2
    grdArquivo.ColWidth(2) = 400
    grdArquivo.Text = "Forn"
    
    grdArquivo.Col = 3
    grdArquivo.ColWidth(3) = 1950
    grdArquivo.Text = "C�d. F�brica"
    
    grdArquivo.Col = 4
    grdArquivo.ColWidth(4) = 4200
    grdArquivo.Text = "Descri��o"
    
    grdArquivo.Col = 5
    grdArquivo.ColWidth(5) = 350
    grdArquivo.Text = "Lin"
    
    grdArquivo.Col = 6
    grdArquivo.ColWidth(6) = 350
    grdArquivo.Text = "Gru"
    
    grdArquivo.Col = 7
    grdArquivo.ColWidth(7) = 350
    grdArquivo.Text = "Sub"
    
    grdArquivo.Col = 8
    grdArquivo.ColWidth(8) = 1950
    grdArquivo.Text = "C�d. Mascarado"
    
    grdArquivo.Col = 9
    grdArquivo.ColWidth(9) = 700
    grdArquivo.Text = "Seq.Tab"
    
    grdArquivo.Col = 10
    grdArquivo.ColWidth(10) = 1000
    grdArquivo.Text = "Clas.Fisc."
    
    grdArquivo.Col = 11
    grdArquivo.ColWidth(11) = 900
    grdArquivo.Text = "Peso"
    
    grdArquivo.Col = 12
    grdArquivo.ColWidth(12) = 700
    grdArquivo.Text = "Vol"
    
    grdArquivo.Col = 13
    grdArquivo.ColWidth(13) = 350
    grdArquivo.Text = "Uni"
    
    grdArquivo.Col = 14
    grdArquivo.ColWidth(14) = 600
    grdArquivo.Text = "Tr.ICM"
    
    grdArquivo.Col = 15
    grdArquivo.ColWidth(15) = 600
    grdArquivo.Text = "Tr.IPI"
    
    grdArquivo.Col = 16
    grdArquivo.ColWidth(16) = 600
    grdArquivo.Text = "Pc.IPI"
    
    grdArquivo.Col = 17
    grdArquivo.ColWidth(17) = 400
    grdArquivo.Text = "Proc"
    
    grdArquivo.Col = 18
    grdArquivo.ColWidth(18) = 1100
    grdArquivo.Text = "Qt.Min.Forn"
    
    grdArquivo.Col = 19
    grdArquivo.ColWidth(19) = 1100
    grdArquivo.Text = "Qt.Min.Vda"
    
    grdArquivo.Col = 20
    grdArquivo.ColWidth(20) = 1100
    grdArquivo.Text = "Qt.Max.Vda"
    
    grdArquivo.Col = 21
    grdArquivo.ColWidth(21) = 3200
    grdArquivo.Text = "Cod.Fabr.EDI "
    
    grdArquivo.Col = 22
    grdArquivo.ColWidth(22) = 350
    grdArquivo.Text = "Cat"
    
    grdArquivo.Col = 23
    grdArquivo.ColWidth(23) = 10000
    grdArquivo.Text = "Observa��o"
    
    'Estas duas colunas ser�o utilizadas para identificar se o fornecedor trabalha com EDI ou n�o
    'E para identificar sua classifica��o que ser� utilizada no momento das consist�ncias e do cadastro
    grdArquivo.Col = 24
    grdArquivo.ColWidth(24) = 1
    grdArquivo.Text = "EDI"
    
    grdArquivo.Col = 25
    grdArquivo.ColWidth(25) = 1
    grdArquivo.Text = "Clas_Forn"
    
    'Esta coluna foi inserida posteriormente, devido �s altera��es de transfer�ncia autom�tica
    'onde o campo MIN_ESTOQUE da tabela TRANSFERENCIA.ESTOQUE_MINIMO, ser� alimentada pela
    'inclus�ao e altera��o de itens (autom�tica e manual)
    grdArquivo.Col = 26
    grdArquivo.ColWidth(26) = 1100
    grdArquivo.Text = "Qt.Min.Est."
    
    'Coluna inserida para controlar altera��o do c�digo de f�brica (P/depois inserir na R_FABRICA_DPK) - Aleth�a 10/04/03
    grdArquivo.Col = 27
    grdArquivo.ColWidth(27) = 1
    grdArquivo.Text = "Fabr.Ant."
    
    'Coluna inserida em Maio/04 para atender conta 900 - devido ao cadastro de Fabricante - Aleth�a
    grdArquivo.Col = 28
    grdArquivo.ColWidth(28) = 1100
    grdArquivo.Text = "Fabricante"
    
    'colunas inseridas para guardar o grupo e subgrupo antes da altera��o - 21/06/05 Ana
    grdArquivo.Col = 29
    grdArquivo.ColWidth(29) = 1
    grdArquivo.Text = "Gr.Ant"
    
    grdArquivo.Col = 30
    grdArquivo.ColWidth(30) = 1
    grdArquivo.Text = "SubGr.Ant"
    
    
    '//////////////////////////
    'LEITURA DO ARQUIVO
    '//////////////////////////
    Open vNomeArq For Input As #1
    
    'Inicializa contadores de linhas do grid
    I = 0
    lblMsg.Visible = True
    
    'L� enquanto n�o for final de arquivo
    Do While Not EOF(1)
    
        Screen.MousePointer = 11
        I = I + 1
        
        lblMsg = "CARREGANDO ...." & I
        lblMsg.Refresh
        
        'Inicializa flags e vari�veis
        vFl_DPK_OK = True
        
        'vFl_Forn = False
        'vFl_Fabr = False
        vDPK = 0
        vClassif_Forn = ""
        vFl_Edi = ""
        vDivisao = ""
        vCod_Trib_ICM = ""
        vCod_Trib_IPI = ""
        vStrLinhaArq = ""
        
        
        'L� registro - linha - do arquivo, joga na vari�vel  e seta ponteiro para o pr�ximo
        Line Input #1, varLinhaArquivo
        'Se linha lida for em branco, sai do loop
        If Mid(varLinhaArquivo, 1, 209) = "" Then
            Exit Do
        End If
        
        'Qtd. Linhas do grid ser� correspondente � qtd. de linhas lidas do arquivo texto
        grdArquivo.Rows = I + 1
        grdArquivo.Row = I
                
        '1a Coluna vai receber a numera��o de registros lidos do arquivo
        grdArquivo.Col = 0
        grdArquivo.Text = Format(I, "0000")
        
        'DPK
        
        'Atribui DPK a uma vari�vel
        vStrLinhaArq = Trim(Mid(varLinhaArquivo, 1, 5))
        vDPK = Val(vStrLinhaArq)
        '2a Coluna vai receber DPK lido do arq. independente de qq. problema
        grdArquivo.Col = 1
        grdArquivo.Text = Format(vStrLinhaArq, "#####")
        
        'Valida DPK
        If vStrLinhaArq = "" Then
            grdArquivo.Col = 23
            grdArquivo.Text = "DPK n/Existe! "
            vFl_DPK_OK = False
        Else
            
            db.Parameters.Remove "DPK"
            db.Parameters.Add "DPK", vStrLinhaArq, 1
            
            'Atribui Fornecedor a uma vari�vel
            vStrLinhaArq = Trim(Mid(varLinhaArquivo, 6, 3))
            
            '3a Col recebe c�d.forn. lido do arq. independente de fornecedor atender ou n�o
            grdArquivo.Col = 2
            grdArquivo.Text = Format(vStrLinhaArq, "###")
                
            'Valida Fornecedor
            If vStrLinhaArq = "" Then
                grdArquivo.Col = 23
                grdArquivo.Text = "Forn.n/Existe! "
            Else
                db.Parameters.Remove "FORN"
                db.Parameters.Add "FORN", Val(vStrLinhaArq), 1
            End If

            'Este select vai verificar dados do DPK e tamb�m do fornecedor - que s�o cpos.chaves
            CmdSql = "Select C.Cod_Fornecedor, C.Cod_Fabrica, E.Situacao Sit_Dpk, " & _
                     "F.Classificacao, F.Fl_Edi, F.Divisao, F.Situacao Sit_Forn, C.COD_GRUPO, C.COD_SUBGRUPO " & _
                     "From ITEM_ESTOQUE E, ITEM_CADASTRO C, FORNECEDOR F " & _
                     "Where E.Cod_Loja = 1 " & _
                     "And C.Cod_Dpk = :DPK " & _
                     "And C.Cod_Fornecedor = :FORN " & _
                     "And C.Cod_Dpk = E.Cod_Dpk " & _
                     "And C.Cod_Fornecedor = F.Cod_Fornecedor " & _
                     "And F.Classificacao in ('A', 'C')"
            
            Set db_CONS = db.dbcreatedynaset(CmdSql, 0&)
            
            If db_CONS.EOF Then
                grdArquivo.Col = 23
                grdArquivo.Text = "DPK ou Forn. n/Existe! "
                vFl_DPK_OK = False
            Else
                
                'Verifica situa��o do item e do fornecedor
                vSit_Dpk = db_CONS!Sit_Dpk.Value
                vSit_Forn = db_CONS!Sit_Forn.Value
                
                If vSit_Dpk = 8 Then
                    grdArquivo.Col = 23
                    grdArquivo.Text = "DPK Subst.! "
                    vFl_DPK_OK = False
                ElseIf vSit_Dpk = 9 Then
                    grdArquivo.Col = 23
                    grdArquivo.Text = "DPK Desat.! "
                    vFl_DPK_OK = False
                ElseIf vSit_Forn = 9 Then
                    grdArquivo.Col = 23
                    grdArquivo.Text = "Forn.Desat.! "
                    vFl_DPK_OK = False
                Else
                    'Dados do DPK
                    vCodForn = db_CONS!Cod_Fornecedor.Value
                    vCodFabr = db_CONS!Cod_Fabrica.Value
                                    
                    'Dados do Fornecedor
                    vClassif_Forn = db_CONS!Classificacao.Value
                    vFl_Edi = db_CONS!Fl_Edi.Value
                    vDivisao = db_CONS!Divisao.Value
                
                    '25a Col recebe o fl_edi p/ser usado depois, na grava��o do item
                    grdArquivo.Col = 24
                    grdArquivo.Text = Trim(vFl_Edi)
                    '26a Col recebe a clas.do forn. p/ser usado depois, na grava��o do item
                    grdArquivo.Col = 25
                    grdArquivo.Text = Trim(vClassif_Forn)
                    
                    '30� coluna recebe o valor do grupo trazido no select para comparar com o do arquivo - 22/06/05
                    grdArquivo.Col = 29
                    grdArquivo.Text = db_CONS!COD_GRUPO.Value
                    
                    '31� coluna recebe o valor do subgrupo trazido no select para comparar com o do arquivo - 22/06/2005
                    grdArquivo.Col = 30
                    grdArquivo.Text = db_CONS!COD_SUBGRUPO.Value
                    
                End If
            
            End If
        
        End If
        
        'Se o c�digo DPK estiver OK, continua valida��o da linha, sen�o passa p/ pr�xima
        If vFl_DPK_OK = True Then
        
            
            '////////////////////////////
            'CONSISTE C�D. F�BRICA
            '////////////////////////////
            vStrLinhaArq = Trim(Mid(varLinhaArquivo, 9, 19))
            
            '4a Coluna vai receber c�d.de f�brica lido do arq. independente de atender ou n�o
            grdArquivo.Col = 3
            grdArquivo.Text = vStrLinhaArq
            
            If vStrLinhaArq = "" Then
                grdArquivo.Col = 23
                grdArquivo.Text = grdArquivo.Text & "F�br.Inv.! "
            Else
            
                '28/03/2003 - Aleth�a
                'Com o novo projeto de substitui��o, o usu�rio n�o mais poder� fazer a
                'substitui��o pelo sistema de altera��o de item
                '10/04/03 - Aleth�a
                'Foi solicitado a volta da altera��o autom�tica do c�d.f�brica - preparado para
                'cadastrar na R_FABRICA_DPK
                
                db.Parameters.Remove "FABR"
                'db.Parameters.Add "FABR", Val(vStrLinhaArq), 1
                db.Parameters.Add "FABR", vStrLinhaArq, 1
            
                'Se o c�d.f�brica do arquivo for igual ao cadastrado para o DPK em quest�o
                'ent�o n�o � altera��o de cod.f�brica e n�o precisa valid�-lo
                If vStrLinhaArq <> vCodFabr Then
                    
                    lstrSql = "Select * " & _
                              "From ITEM_CADASTRO " & _
                              "Where Cod_Fabrica = :FABR " & _
                              "And Cod_Fornecedor = :FORN " & _
                              "And Cod_Dpk <> :DPK "
                    
                    Set db_CONS = db.dbcreatedynaset(lstrSql, 0&)
                    
                    'Se encontrou algum DPK com o Forn.do arq. e Fabr. que se deseja alterar,
                    'grava diverg�ncia que j� existe
                    
                    If Not db_CONS.EOF Then
                        grdArquivo.Col = 23
                        grdArquivo.Text = grdArquivo.Text & "F�br.j� cad.p/este Forn.! "
                    Else
                        'Guarda c�d.f�brica atual (antigo no caso de altera��o) - 10/04/03
                        grdArquivo.Col = 27
                        grdArquivo.Text = Trim(vCodFabr)
                    End If
                    'grdArquivo.Col = 23
                    'grdArquivo.Text = grdArquivo.Text & "F�br.n/pode ser alterado por este pgm.! "
                                
                End If
                
            End If
            
            
            '////////////////////////////
            'CONSISTE DESCRI��O DO ITEM
            '////////////////////////////
            '5a Coluna vai receber a descri��o do item lida do arq.
            'Descri��o n�o pode estar em branco
            vStrLinhaArq = Trim(Mid(varLinhaArquivo, 28, 40))
            
            grdArquivo.Col = 4
            grdArquivo.Text = vStrLinhaArq
            
            If vStrLinhaArq = "" Then
                grdArquivo.Col = 23
                grdArquivo.Text = grdArquivo.Text & "Descr.Inv.! "
            End If
            
            
            '////////////////////////////
            'CONSISTE LINHA
            '////////////////////////////
            vStrLinhaArq = Trim(Mid(varLinhaArquivo, 68, 2))
            
            '6a Col vai receber a linha lida do arq. independente de atender ou n�o
            grdArquivo.Col = 5
            grdArquivo.Text = Format(vStrLinhaArq, "##")
            
            If vStrLinhaArq = "" Then
                grdArquivo.Col = 23
                grdArquivo.Text = grdArquivo.Text & "Linha n/Cad.! "
            Else
                db.Parameters.Remove "LIN"
                db.Parameters.Add "LIN", Val(vStrLinhaArq), 1
                
                CmdSql = "Select Count(*) QtdLin " & _
                         "From LINHA " & _
                         "Where Cod_Linha = :LIN "
                
                Set db_CONS = db.dbcreatedynaset(CmdSql, 0&)
                
                'Se linha n�o cadastrada, coloca msg na col. de Obs. do grid
                If db_CONS!QtdLin.Value = 0 Then
                    grdArquivo.Col = 23
                    grdArquivo.Text = grdArquivo.Text & "Linha n/Cad.! "
                End If
            End If
        
            
            
            '////////////////////////////
            'CONSISTE GRUPO
            '////////////////////////////
            vStrLinhaArq = Trim(Mid(varLinhaArquivo, 70, 2))
            
            '7a Coluna vai receber c�d.do grupo lido do arq. independente de atender ou n�o
            grdArquivo.Col = 6
            grdArquivo.Text = Format(vStrLinhaArq, "##")
            
            If vStrLinhaArq = "" Then
                grdArquivo.Col = 23
                grdArquivo.Text = grdArquivo.Text & "Grupo n/Cad.! "
            Else
                db.Parameters.Remove "GRU"
                db.Parameters.Add "GRU", Val(vStrLinhaArq), 1
                
                CmdSql = "Select Count(*) QtdGru " & _
                         "From GRUPO " & _
                         "Where Cod_Grupo = :GRU "
                
                Set db_CONS = db.dbcreatedynaset(CmdSql, 0&)
                
                'Se grupo n�o cadastrado, coloca msg na col. de Obs. do grid
                If db_CONS!QtdGru.Value = 0 Then
                    grdArquivo.Col = 23
                    grdArquivo.Text = grdArquivo.Text & "Grupo n/Cad.! "
                End If
            End If
                    
                    
                    
                    
            '////////////////////////////
            'CONSISTE SUBGRUPO
            '////////////////////////////
            vStrLinhaArq = Trim(Mid(varLinhaArquivo, 72, 2))
            
            '8a Coluna vai receber c�d.do grupo lido do arq. independente de atender ou n�o
            grdArquivo.Col = 7
            grdArquivo.Text = Format(vStrLinhaArq, "##")
            
            If vStrLinhaArq = "" Then
                grdArquivo.Col = 23
                grdArquivo.Text = grdArquivo.Text & "Subgr. n/Cad.p/o Gr.! "
            Else
                db.Parameters.Remove "SUB"
                db.Parameters.Add "SUB", Val(vStrLinhaArq), 1
                
                CmdSql = "Select Count(*) QtdSub " & _
                         "From SUBGRUPO " & _
                         "Where Cod_Grupo = :GRU " & _
                         "And Cod_Subgrupo = :SUB"
                
                Set db_CONS = db.dbcreatedynaset(CmdSql, 0&)
                
                'Se grupo/subgrupo n�o cadastrados, coloca msg na col. de Obs. do grid
                If db_CONS!QtdSub.Value = 0 Then
                    grdArquivo.Col = 23
                    grdArquivo.Text = grdArquivo.Text & "Subgr. n/Cad.p/o Gr.! "
                End If
            End If
        
            
            
            
            
            '////////////////////////////
            'CONSISTE MASCARADO
            '////////////////////////////
            '9a Coluna vai receber c�d.MASCARADO lido do arq.
            'Descri��o n�o pode estar em branco
            vStrLinhaArq = Trim(Mid(varLinhaArquivo, 74, 19))
            
            grdArquivo.Col = 8
            grdArquivo.Text = vStrLinhaArq
            
            If vStrLinhaArq = "" Then
                grdArquivo.Col = 23
                grdArquivo.Text = grdArquivo.Text & "Masc.Inv.! "
            End If
            
            
            
            '////////////////////////////
            'CONSISTE SEQUENCIA DE TABELA
            '////////////////////////////
            
            'Atribui a sequ�ncia na 9a. coluna do grid
            'Qualquer sequ�ncia (diferente de branco ""), que vier no arquivo, eu vou cadastrar,
            'independente de j� existir algum outro item com esta mesma sequ�ncia de tabela
            'O �nico problema de existirem dois itens com a mesma seq. tabela � que na hora
            'de imprimir a Lista de Pre�o, o sistema vai colocar estes dois itens na order
            'em que ele achar que � a correta (segundo Vanessa - Compras)
            vStrLinhaArq = Trim(Mid(varLinhaArquivo, 93, 6))
            
            grdArquivo.Col = 9
            grdArquivo.Text = vStrLinhaArq
            
            If vStrLinhaArq = "" Then
                grdArquivo.Col = 23
                grdArquivo.Text = grdArquivo.Text & "Seq.Inv.! "
            End If
            
                    
                    
                    
            '////////////////////////////
            'CONSISTE COD. TRIBUTA��O ICM
            '////////////////////////////
            'Ser� necess�rio fazer esta consist�ncia antes, pois o resultado ser� usado
            'na consist�ncia da Classifica��o Fiscal
            
            'Se cod. tributa��o ICM do arquivo, vier em branco, coloca mensagem no cpo. Observa��o
            vStrLinhaArq = Trim(Mid(varLinhaArquivo, 127, 1))
            If vStrLinhaArq = "" Then
                grdArquivo.Col = 23
                grdArquivo.Text = grdArquivo.Text & "Cod.Trib.ICM Inv.! "
            'Se divis�o do fornecedor for DPK, o c�d.trib.icm deve ser (0,1,4 ou 8)
            'Conforme solicita��o da Controladoria (Beth), somente deixar CT 1 ou 8 - CT 4 somente no cadastro manual - Alethea - 18/01/08
            'ElseIf vDivisao = "D" And vStrLinhaArq <> "0" And _
                                      vStrLinhaArq <> "1" And _
                                      vStrLinhaArq <> "4" And _
                                      vStrLinhaArq <> "8" Then
            ElseIf vDivisao = "D" And vStrLinhaArq <> "1" And vStrLinhaArq <> "8" Then
                grdArquivo.Col = 23
                grdArquivo.Text = grdArquivo.Text & "Cod.Trib.Icm Inv.! "
            'Se divis�o do fornecedor for Blau, o c�d.trib.icm deve ser (0 ou 4)
            'Conforme solicita��o da Controladoria (Beth), somente deixar CT 1 ou 8 - CT 4 somente no cadastro manual - Alethea - 18/01/08
            'ElseIf vDivisao = "B" And vStrLinhaArq <> "0" And _
                                      vStrLinhaArq <> "4" Then
            ElseIf vDivisao = "B" And vStrLinhaArq <> "1" Then
                grdArquivo.Col = 23
                grdArquivo.Text = grdArquivo.Text & "Cod.Trib.Icm Inv.! "
            End If
            'Atribui o c�d.trib.ICM na 15a. coluna do grid, independente do resultado
            grdArquivo.Col = 14
            grdArquivo.Text = IIf(vStrLinhaArq = "", "", Format(vStrLinhaArq, "0"))
            vCod_Trib_ICM = vStrLinhaArq
            
            
            
                    
            '////////////////////////////
            'CONSISTE CLASSIF. FISCAL
            '////////////////////////////
            vStrLinhaArq = Trim(Mid(varLinhaArquivo, 99, 10))
            If vStrLinhaArq = "" Then
                grdArquivo.Col = 23
                grdArquivo.Text = grdArquivo.Text & "Clas.Fiscal Inv.! "
            Else
                db.Parameters.Remove "FIS"
                db.Parameters.Add "FIS", Val(vStrLinhaArq), 1
                
                CmdSql = "Select * " & _
                         "From COMPRAS.SUBST_TRIBUTARIA " & _
                         "Where class_fiscal = :FIS "
                
                Set db_CONS = db.dbcreatedynaset(CmdSql, 0&)
                
                'Se n�o encontrar Class.Fiscal, verifica Cod.Trib.ICM e coloca msg na col. de Obs. do grid
                'Observa��o existente no Cda.Item - CAD070
                'Devido �s altera��es p/ carga de 06/08/2001, foi necess�rio consistir tamb�m o cod.tributa��o = 8
                'Aliq.Reduzida - Altera��es de Desc. Per�odo / Lista de Pre�o com 4% / etc. - Aleth�a (31/07/01)
                If db_CONS.EOF And (vCod_Trib_ICM = "1" Or vCod_Trib_ICM = "8") Then
                    grdArquivo.Col = 23
                    grdArquivo.Text = grdArquivo.Text & "Clas.Fiscal Inv.! "
                
                
                'Verifica��o implementada em 26/04/05 para que n�o d� problema na hora da venda, no caso de faltar algum
                'cadastro de uf origem destino - Alethea / Marici
                ElseIf (vCod_Trib_ICM = "1" Or vCod_Trib_ICM = "8") Then
                
                    db.Parameters.Remove "FIS": db.Parameters.Add "FIS", Val(vStrLinhaArq), 1
                    
                    CmdSql = "Select :FIS class_fiscal, b.cod_uf UF1, a.cod_uf UF2 " & _
                                "From (Select b.cod_uf from loja a, cidade b where a.cod_cidade=b.cod_cidade) b, " & _
                                "     Uf a " & _
                               "Where (:FIS, b.cod_uf, a.cod_uf) NOT IN " & _
                                    " (Select class_fiscal, cod_uf_origem, cod_uf_destino " & _
                                        "From compras.subst_tributaria " & _
                                       "Where class_fiscal = :FIS)"
                    
                    Set objSubTrib = db.dbcreatedynaset(CmdSql, 0&)
                
                    'Se ainda houver algum cadastro a ser feito, monta um e-mail e manda para Fernanda do Fiscal
                    vTexto = ""
                    While Not objSubTrib.EOF
                    
                        vTexto = vTexto & Trim(objSubTrib!Class_fiscal) & String(12 - Len(Trim(objSubTrib!Class_fiscal)), " ") & "   " & _
                                          Trim(objSubTrib!UF1) & " - " & Trim(objSubTrib!UF2) & vbCrLf
                        objSubTrib.MoveNext
                    
                    Wend
                
                    If vTexto <> "" Then
                    
                        vTexto = "Favor cadastrar na COMPRAS.SUBST_TRIBUTARIA, conforme discriminado abaixo: " & vbCrLf & vbCrLf & _
                                 "Clas. Fiscal - UF Origem - UF Destino" & vbCrLf & vbCrLf & _
                                 vTexto
                    
                        db.Parameters.Remove "TEXTO": db.Parameters.Add "TEXTO", vTexto, 1
                        db.Parameters.Remove "vErro": db.Parameters.Add "vErro", 0, 2
                        'CmdSql = "Begin Intranet.Envia_Email('alethea@maxxipel.com.br','CLASS_SUBST_TRIBUTARIA', :TEXTO); End; "
                        CmdSql = "Begin Intranet.Envia_Email('fiscal@dpaschoal.com.br','CLASS_SUBST_TRIBUTARIA', :TEXTO); End; "
                        db.ExecuteSQL CmdSql
                    End If
                
                
                
                
                Else
                    'Alterado em 23/01/2003
                    'Verifica na tabela ANTECIPACAO_TRIBUTARIA se existe class.fiscal cadastrada,
                    'Se existir, o Cod.Tributacao (ICM) tem que ser = 1
                    db.Parameters.Remove "UF_ORI": db.Parameters.Add "UF_ORI", db_CONS!Cod_Uf_Origem.Value, 1
                    db.Parameters.Remove "UF_DES": db.Parameters.Add "UF_DES", db_CONS!Cod_Uf_Destino.Value, 1
                    
                    'Alterada estrutura de tabela a partir da entrada do CD Belem - Alethea - 08/06/05
                    'Tabela ANTECIPACAO_TRIBUTARIA ser� inutilizada
                    'CmdSql = "Select Count(*) QtdAnt " & _
                               "From Producao.Antecipacao_Tributaria " & _
                              "Where Cod_Uf_Origem = :UF_ORI " & _
                                "And Cod_Uf_Destino = :UF_DES "
                    db.Parameters.Remove "FIS": db.Parameters.Add "FIS", Val(vStrLinhaArq), 1
                    CmdSql = "Select Count(*) QtdAnt " & _
                               "From Compras.Class_Antec_Entrada " & _
                              "Where Cod_Uf_Origem = :UF_ORI " & _
                                "And Cod_Uf_Destino = :UF_DES " & _
                                "And Class_Fiscal = :FIS "
                    
                    Set db_CONS = db.dbcreatedynaset(CmdSql, 0&)
                    
                    If (db_CONS!QtdAnt.Value > 0) And (vCod_Trib_ICM <> "1") Then
                        grdArquivo.Col = 23
                        grdArquivo.Text = grdArquivo.Text & "C�d.Trib.ICM tem que ser 1! "
                    End If
                End If
            
                '----------------------------------------
                '29/08/08 - Sempre que houver altera��o da clas.fiscal, enviar e-mail ao Fiscal - Alethea
                vClassFiscal_Atu = CStr(Val(vStrLinhaArq))
                'seleciona a class.fiscal anterior
                CmdSql = "Select class_fiscal " & _
                         "From PRODUCAO.ITEM_CADASTRO " & _
                         "Where cod_dpk = :DPK "
                Set db_CONS = db.dbcreatedynaset(CmdSql, 0&)
                vClassFiscal_Ant = CStr(Val(db_CONS!Class_fiscal))
                
                If vClassFiscal_Ant <> vClassFiscal_Atu Then
                    vTexto = "Alterada a Classificacao Fiscal do DPK " & vDPK & vbCrLf & vbCrLf & _
                             "Anterior..: " & vClassFiscal_Ant & vbCrLf & _
                             "Atual.....: " & vClassFiscal_Atu
                
                    db.Parameters.Remove "TEXTO": db.Parameters.Add "TEXTO", vTexto, 1
                    db.Parameters.Remove "vErro": db.Parameters.Add "vErro", 0, 2
                    'CmdSql = "Begin producao.Envia_Email('alethea@maxxipel.com.br','ALTERACAO DE CLASSIFICACAO FISCAL', :TEXTO); End; "
                    CmdSql = "Begin Intranet.Envia_Email('fiscal@dpaschoal.com.br','ALTERACAO DE CLASSIFICACAO FISCAL', :TEXTO); End; "
                   
                    db.ExecuteSQL CmdSql
                End If
                
                '----------------------------------------
            
            
            
            End If
            
            '11a Coluna vai receber clas.fiscal lida do arq. independente de atender ou n�o
            grdArquivo.Col = 10
            'grdArquivo.Text = Format(vStrLinhaArq, "##########")
            grdArquivo.Text = vStrLinhaArq
                    
                    
                    
                    
            '////////////////////////////
            'CONSISTE PESO
            '////////////////////////////
            vStrLinhaArq = Trim(Mid(varLinhaArquivo, 109, 9))
            'Se peso for maior do que 99999.999 coloca msg. no cpo. Observa��o
            If CDbl(vStrLinhaArq) > 99999.999 Then
                grdArquivo.Col = 23
                grdArquivo.Text = grdArquivo.Text & "Peso Inv.! "
            End If
            'Atribui peso que veio no arquivo na 12a. coluna do grid
            'Se peso do arquivo, vier em branco, atribui 0
            grdArquivo.Col = 11
            grdArquivo.Text = Format(IIf(vStrLinhaArq = "", 0, vStrLinhaArq), "####0.000")
                    
                    
                    
                    
            '////////////////////////////
            'CONSISTE VOLUME
            '////////////////////////////
            vStrLinhaArq = Trim(Mid(varLinhaArquivo, 118, 7))
            'Se peso for maior do que 999.999 coloca msg. no cpo. Observa��o
            If CDbl(vStrLinhaArq) > 999.999 Then
                grdArquivo.Col = 23
                grdArquivo.Text = grdArquivo.Text & "Vol. Inv.! "
            End If
            'Atribui vol. que veio no arquivo na 13a. coluna do grid
            'Se volume do arquivo, vier em branco, atribui 0
            grdArquivo.Col = 12
            grdArquivo.Text = Format(IIf(vStrLinhaArq = "", 0, vStrLinhaArq), "##0.000")
                    
                    
                    
                    
            '////////////////////////////
            'CONSISTE UNIDADE
            '////////////////////////////
            vStrLinhaArq = Trim(Mid(varLinhaArquivo, 125, 2))
            If vStrLinhaArq = "" Then
                grdArquivo.Col = 23
                grdArquivo.Text = grdArquivo.Text & "Unid. Inv.! "
            Else
                db.Parameters.Remove "UNI"
                db.Parameters.Add "UNI", vStrLinhaArq, 1
                
                CmdSql = "Select Count(*) QtdUni " & _
                         "From UNIDADE " & _
                         "Where Cod_Unidade = :UNI "
                
                Set db_CONS = db.dbcreatedynaset(CmdSql, 0&)
                
                If db_CONS!QtdUni.Value = 0 Then
                    grdArquivo.Col = 23
                    grdArquivo.Text = grdArquivo.Text & "Unid. n/cad.! "
                End If
            End If
            
            '14a Coluna vai receber unidade lida do arq. independente de atender ou n�o
            grdArquivo.Col = 13
            grdArquivo.Text = vStrLinhaArq
                    
                    
                    
                    
            '////////////////////////////
            'CONSISTE COD. TRIBUTA��O IPI
            '////////////////////////////
            'Se cod. tributa��o IPI do arquivo vier em branco, coloca mensagem no cpo. Observa��o
            vStrLinhaArq = Trim(Mid(varLinhaArquivo, 128, 1))
            If vStrLinhaArq = "" Then
                grdArquivo.Col = 23
                grdArquivo.Text = grdArquivo.Text & "Cod.Trib.IPI Inv.! "
            'C�d.trib.ipi deve ser (0 ou 1)
            ElseIf vStrLinhaArq <> "0" And vStrLinhaArq <> "1" Then
                grdArquivo.Col = 23
                grdArquivo.Text = grdArquivo.Text & "Cod.Trib.IPI Inv.! "
            End If
            'Atribui o c�d.trib.IPI na 16a. coluna do grid, independente de atender ou n�o
            grdArquivo.Col = 15
            grdArquivo.Text = IIf(vStrLinhaArq = "", "", Format(vStrLinhaArq, "0"))
            vCod_Trib_IPI = vStrLinhaArq
                    
    
    
    
            '////////////////////////////
            'CONSISTE PERCENTUAL IPI
            '////////////////////////////
            vStrLinhaArq = Trim(Mid(varLinhaArquivo, 129, 5))
            'Se peso for maior do que 99999.999 coloca msg. no cpo. Observa��o
            If CDbl(vStrLinhaArq) > 99.99 Then
                grdArquivo.Col = 23
                grdArquivo.Text = grdArquivo.Text & "%IPI Inv.! "
            End If
            'Atribui %IPI que veio no arquivo na 17a. coluna do grid, independente de atender ou n�o
            'Se %IPI do arquivo, vier em branco, atribui 0
            grdArquivo.Col = 16
            grdArquivo.Text = Format(IIf(vStrLinhaArq = "", 0, vStrLinhaArq), "#0.00")
    
            
            
            
            '////////////////////////////
            'CONSISTE COD_PROCEDENCIA
            '////////////////////////////
            'Para ter c�d.de proced�ncia, precisa ter cod.trib.IPI
            vStrLinhaArq = Trim(Mid(varLinhaArquivo, 134, 1))
            
            'Se c�d.proced�ncia vier em branco no arquivo, coloca msg.no cpo. Observa��o
            If vStrLinhaArq = "" Then
                grdArquivo.Col = 23
                grdArquivo.Text = grdArquivo.Text & "Proced.Inv.! "
            'Se cod. tributa��o IPI do arquivo vier em branco, coloca mensagem no cpo. Observa��o
            ElseIf vCod_Trib_IPI = "" Then
                grdArquivo.Col = 23
                grdArquivo.Text = grdArquivo.Text & "Proced.Inv.! "
            'Se cod.tributa��o IPI diferente de branco
            'Se item ainda n�o estiver cadastrado e a divis�o do fornecedor for DPK,
            'o c�digo de proced�ncia tem que ser = 0, sen�o coloca msg. no cpo. observa��o
            ElseIf vDPK = 0 And vDivisao = "D" And vStrLinhaArq <> "0" Then
                grdArquivo.Col = 23
                grdArquivo.Text = grdArquivo.Text & "Proced.Inv.! "
            'Sen�o verifica o cod.trib.ipi para ent�o validar proced�ncia
            Else
                'Se cod.trib.ipi = 1, procedencia pode ser 0, 1 ou 2
                'Em 20/11/06, foi alterado para que produtos de ct IPI = 1 (importados) somente possam ter procedencia estrangeira - Alethea
                'If vCod_Trib_IPI = "1" And vStrLinhaArq <> "0" And _
                                         vStrLinhaArq <> "1" And _
                                         vStrLinhaArq <> "2" Then
                If vCod_Trib_IPI = "1" And vStrLinhaArq <> "1" And _
                                           vStrLinhaArq <> "2" Then
                    grdArquivo.Col = 23
                    grdArquivo.Text = grdArquivo.Text & "Proced.Inv.! "
                'Sen�o procedencia pode ser 0, 1 ou 2
                ElseIf vStrLinhaArq <> "0" And vStrLinhaArq <> "2" Then
                    grdArquivo.Col = 23
                    grdArquivo.Text = grdArquivo.Text & "Proced.Inv.! "
                End If
            End If
            
            'Atribui o c�d.proced�ncia na 18a. coluna do grid, independente de atender ou n�o
            grdArquivo.Col = 17
            grdArquivo.Text = IIf(vStrLinhaArq = "", "", Format(vStrLinhaArq, "0"))
            
            
            
            
            '////////////////////////////
            'CONSISTE QTD.MIN.DO FORNEC.
            '////////////////////////////
            vStrLinhaArq = Trim(Mid(varLinhaArquivo, 135, 10))
            'Se qtd.min.forn > 99999999.9, ou zero ou branco, coloca msg. no cpo. Observa��o
            If CDbl(vStrLinhaArq) > 99999999.9 Or CDbl(vStrLinhaArq) = 0 Or vStrLinhaArq = "" Then
                grdArquivo.Col = 23
                grdArquivo.Text = grdArquivo.Text & "Qt.Min.Forn. Inv.! "
            End If
            'Atribui qtd.min.forn que veio no arquivo na 19a. coluna do grid
            grdArquivo.Col = 18
            grdArquivo.Text = Format(IIf(CDbl(vStrLinhaArq) = 0, "", vStrLinhaArq), "#######0.0")
            
            
            
            
            '////////////////////////////
            'CONSISTE QTD.MIN. VENDA
            '////////////////////////////
            vStrLinhaArq = Trim(Mid(varLinhaArquivo, 145, 10))
            'Se qtd.min.vda > 99999999.9, ou zero ou branco, coloca msg. no cpo. Observa��o
            If CDbl(vStrLinhaArq) > 99999999.9 Or CDbl(vStrLinhaArq) = 0 Or vStrLinhaArq = "" Then
                grdArquivo.Col = 23
                grdArquivo.Text = grdArquivo.Text & "Qt.Min.Vda. Inv.! "
            End If
            'Atribui qtd.min.venda que veio no arquivo na 20a. coluna do grid
            grdArquivo.Col = 19
            grdArquivo.Text = Format(IIf(CDbl(vStrLinhaArq) = 0, "", vStrLinhaArq), "#######0.0")
            
            
            
            
            '////////////////////////////
            'CONSISTE QTD.M�X. VENDA
            '////////////////////////////
            vStrLinhaArq = Trim(Mid(varLinhaArquivo, 155, 10))
            'Se qtd.min.vda > 99999999.9, coloca msg. no cpo. Observa��o
            If CDbl(vStrLinhaArq) > 99999999.9 Then
                grdArquivo.Col = 23
                grdArquivo.Text = grdArquivo.Text & "Qt.Min.Vda. Inv.! "
            End If
            'Atribui qtd.min.venda que veio no arquivo na 21a. coluna do grid
            'Se vier em branco, atribui zero
            grdArquivo.Col = 20
            grdArquivo.Text = Format(IIf(CDbl(vStrLinhaArq) = 0, 0, vStrLinhaArq), "#######0.0")
            
            
            
            
            '////////////////////////////
            'CONSISTE C�D. F�BR. EDI
            '////////////////////////////
            'Deixar com espa�os em branco a esq, conf.solic.Andre - 02/02/04 - Aleth�a
            vStrLinhaArq = RTrim(Mid(varLinhaArquivo, 165, 30))
            'Se o fornecedor trabalhar com EDI, o c�digo de f�br. EDI � obrigat�rio
            If vFl_Edi = "S" And vStrLinhaArq = "" Then
                grdArquivo.Col = 23
                grdArquivo.Text = grdArquivo.Text & "C�d.F�br.EDI Inv.! "
            Else
                'Verifica se j� existe o c�d.f�br.edi cadastrado
                db.Parameters.Remove "EDI"
                db.Parameters.Add "EDI", vStrLinhaArq, 1
                
                CmdSql = "Select Cod_Dpk " & _
                         "From COMPRAS.R_ITEM_EDI " & _
                         "Where Cod_Fornecedor = :FORN " & _
                         "And Cod_Fabrica_Edi = :EDI " & _
                         "And Cod_Dpk <> :DPK "
                
                Set db_CONS = db.dbcreatedynaset(CmdSql, 0&)
                
                If Not db_CONS.EOF Then
                    grdArquivo.Col = 23
                    grdArquivo.Text = grdArquivo.Text & "DPK " & db_CONS!Cod_Dpk.Value & _
                                      " j� cad.p/c�d.fabr.EDI! "
                End If
            
            End If
            '22a Coluna vai receber c�d.f�br.edi lida do arq. independente de atender ou n�o
            grdArquivo.Col = 21
            grdArquivo.Text = vStrLinhaArq
            
            
            
                 
            '////////////////////////////
            'CONSISTE CATEGORIA
            '////////////////////////////
            vStrLinhaArq = Trim(Mid(varLinhaArquivo, 195, 1))
            'Se vier branco no arquivo, coloca msg.no cpo. Observa��o
            If vStrLinhaArq = "" Then
                grdArquivo.Col = 23
                grdArquivo.Text = grdArquivo.Text & "Categ. Inv.! "
            'Sen�o, verifica se a categoria existe
            Else
                db.Parameters.Remove "CATEG"
                db.Parameters.Add "CATEG", vStrLinhaArq, 1
            
                CmdSql = "Select Count(*) QtdCateg " & _
                         "From CATEG_SINAL " & _
                         "Where Categoria = :CATEG"
            
                Set db_CONS = db.dbcreatedynaset(CmdSql, 0&)
                
                If db_CONS!QtdCateg.Value = 0 Then
                    grdArquivo.Col = 23
                    grdArquivo.Text = grdArquivo.Text & "Categ. Inv.! "
                End If
            End If
            '23a Coluna vai receber categoria lida do arq. independente de atender ou n�o
            grdArquivo.Col = 22
            grdArquivo.Text = Format(vStrLinhaArq, "#")
        
            '////////////////////////////
            'CONSISTE ESTOQUE MINIMO DO ITEM
            '////////////////////////////
            vStrLinhaArq = Trim(Mid(varLinhaArquivo, 196, 10))
            'Se qtd.min.est. > 99999999.9, coloca msg. no cpo. Observa��o
            If CDbl(vStrLinhaArq) > 99999999.9 Or CDbl(vStrLinhaArq) = 0 Or vStrLinhaArq = "" Then
                grdArquivo.Col = 23
                grdArquivo.Text = grdArquivo.Text & "Min.Est. Inv.! "
            End If
            'Atribui qtd.min.estoque que veio no arquivo na 27a. coluna do grid
            'Se vier em branco, atribui zero
            grdArquivo.Col = 26
            grdArquivo.Text = Format(IIf(CDbl(vStrLinhaArq) = 0, "", vStrLinhaArq), "#######0.0")
        
        
            '////////////////////////////
            'CONSISTE C�DIGO DO FABRICANTE
            '////////////////////////////
            vStrLinhaArq = Trim(Mid(varLinhaArquivo, 206, 3))
            'Se vier branco no arquivo, coloca msg.no cpo. Observa��o
            If vStrLinhaArq = "" Then
                grdArquivo.Col = 23
                grdArquivo.Text = grdArquivo.Text & "Fabricante Inv.! "
            'Sen�o, verifica se o fabricante existe
            Else
                db.Parameters.Remove "FABRICANTE"
                db.Parameters.Add "FABRICANTE", vStrLinhaArq, 1
            
                CmdSql = "Select Count(*) QtdFabr " & _
                         "From Producao.Fabricante " & _
                         "Where Cod_Fabricante = :FABRICANTE"
            
                Set db_CONS = db.dbcreatedynaset(CmdSql, 0&)
                
                If db_CONS!QtdFabr = 0 Then
                    grdArquivo.Col = 23
                    grdArquivo.Text = grdArquivo.Text & "Fabricante Inv.! "
                End If
            End If
            '27a Coluna vai receber fabricante lida do arq. independente de atender ou n�o
            grdArquivo.Col = 28
            grdArquivo.Text = Format(vStrLinhaArq, "###")
        
        
        
        End If
        
    Loop
    
    MsgBox "Arquivo Lido!", vbInformation, "Aten��o!"
    lblMsg = I & " Linhas Lidas do Arquivo!"
    lblMsg.Refresh
    
    Close #1    ' Close file.
    Screen.MousePointer = 0
        
    Exit Sub
        
        
Trata_Erro:
    
    If Err.Number = 53 Then
        MsgBox "Arquivo de Controle n�o Encontrado. ", , MSG_TIT
    Else
        Call Process_Line_Errors
    End If

End Sub




Private Sub lblHelp2_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp2.Visible = False
End Sub





Private Sub SSFrame2_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp1.Visible = False
    lblHelp2.Visible = False
End Sub


Private Sub SSFrame3_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp1.Visible = False
End Sub



































