VERSION 4.00
Begin VB.Form frmFabricante 
   Caption         =   "Carga inicial de DPK x FABRICANTE"
   ClientHeight    =   7830
   ClientLeft      =   720
   ClientTop       =   2115
   ClientWidth     =   9915
   Height          =   8235
   Left            =   660
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   7830
   ScaleWidth      =   9915
   Top             =   1770
   Width           =   10035
   WindowState     =   2  'Maximized
   Begin Threed.SSFrame SSFrame2 
      Height          =   7695
      Left            =   60
      TabIndex        =   0
      Top             =   30
      Width           =   9765
      _Version        =   65536
      _ExtentX        =   17224
      _ExtentY        =   13573
      _StockProps     =   14
      ForeColor       =   12582912
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin Threed.SSFrame SSFrame3 
         Height          =   1455
         Left            =   225
         TabIndex        =   1
         Top             =   255
         Width           =   9270
         _Version        =   65536
         _ExtentX        =   16351
         _ExtentY        =   2566
         _StockProps     =   14
         Caption         =   "Arquivo"
         ForeColor       =   12582912
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Begin VB.Label lblArquivo 
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H00C00000&
            Height          =   375
            Left            =   1875
            TabIndex        =   4
            Top             =   765
            Width           =   6990
         End
         Begin MSComDlg.CommonDialog controle 
            Left            =   120
            Top             =   240
            _ExtentX        =   847
            _ExtentY        =   847
            _Version        =   393216
            DefaultExt      =   "*.PRN"
            InitDir         =   "C:\CAD760"
         End
         Begin Threed.SSCommand cmdArquivo 
            Height          =   855
            Left            =   375
            TabIndex        =   3
            Top             =   390
            Width           =   1335
            _Version        =   65536
            _ExtentX        =   2355
            _ExtentY        =   1508
            _StockProps     =   78
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Picture         =   "frmFabricante.frx":0000
         End
         Begin VB.Label lblHelp1 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H0000FFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Abrir Arquivo"
            ForeColor       =   &H80000008&
            Height          =   225
            Left            =   1320
            TabIndex        =   2
            Top             =   1200
            Visible         =   0   'False
            Width           =   930
         End
      End
      Begin Threed.SSCommand scmdLayOut 
         Height          =   735
         Left            =   7275
         TabIndex        =   11
         Top             =   6405
         Width           =   735
         _Version        =   65536
         _ExtentX        =   1296
         _ExtentY        =   1296
         _StockProps     =   78
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "frmFabricante.frx":031A
      End
      Begin Threed.SSCommand cmdLog 
         Height          =   735
         Left            =   5385
         TabIndex        =   10
         Top             =   6405
         Width           =   735
         _Version        =   65536
         _ExtentX        =   1296
         _ExtentY        =   1296
         _StockProps     =   78
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "frmFabricante.frx":076C
      End
      Begin VB.Label lblHelp2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H0000FFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Gerar Arq.de Log"
         ForeColor       =   &H80000008&
         Height          =   225
         Left            =   4875
         TabIndex        =   9
         Top             =   7140
         Visible         =   0   'False
         Width           =   1245
      End
      Begin Threed.SSCommand cmdConfirma 
         Height          =   735
         Left            =   6330
         TabIndex        =   8
         Top             =   6405
         Width           =   735
         _Version        =   65536
         _ExtentX        =   1296
         _ExtentY        =   1296
         _StockProps     =   78
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "frmFabricante.frx":0A86
      End
      Begin Threed.SSCommand cmdVoltar 
         Height          =   735
         Left            =   8775
         TabIndex        =   7
         Top             =   6405
         Width           =   735
         _Version        =   65536
         _ExtentX        =   1296
         _ExtentY        =   1296
         _StockProps     =   78
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "frmFabricante.frx":0DA0
      End
      Begin MSGrid.Grid grdArquivo 
         Height          =   4200
         Left            =   210
         TabIndex        =   6
         Top             =   1890
         Width           =   9315
         _Version        =   65536
         _ExtentX        =   16431
         _ExtentY        =   7408
         _StockProps     =   77
         ForeColor       =   8388608
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Cols            =   29
         FixedRows       =   0
      End
      Begin VB.Label lblMsg 
         AutoSize        =   -1  'True
         Caption         =   "Mensagem"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   240
         Left            =   255
         TabIndex        =   5
         Top             =   6360
         Width           =   1155
      End
   End
End
Attribute VB_Name = "frmFabricante"
Attribute VB_Creatable = False
Attribute VB_Exposed = False

Private Sub cmdArquivo_Click()

Dim vFl_DPK_OK As Boolean, vDPK As Double, vFabricante As Long
Dim I As Integer              'Ponteiro / Contador
Dim vStrLinhaArq As String    'Guarda o peda�o (Coluna) do arquivo que est� sendo lido

    'Lay-out do Arquivo:
    'Cod_DPK            Num�rico(5)
    'Cod_Fabricante     Num�rico(3)
    'Exemplo: 54357001

    'Indica que o controle ser� usado para abrir o arquivo
    controle.ShowOpen
    lblArquivo.Caption = controle.filename
    vNomeArq = lblArquivo

    'Verifica se foi indicado o diret�rio e nome do arquivo a ser lido
    If lblArquivo.Caption = "" Then
        MsgBox "Indique o arquivo a ser aberto", , MSG_TIT
        Exit Sub
    End If


    '/////////////////////////
    'MONTA O GRID
    '/////////////////////////
    grdArquivo.Visible = True
    grdArquivo.Row = 0
    
    grdArquivo.Col = 0
    grdArquivo.ColWidth(0) = 450
    grdArquivo.Text = "Reg"
    
    grdArquivo.Col = 1
    grdArquivo.ColWidth(1) = 500
    grdArquivo.Text = "Dpk"
    
    grdArquivo.Col = 2
    grdArquivo.ColWidth(2) = 1100
    grdArquivo.Text = "Fabricante"
    
    grdArquivo.Col = 3
    grdArquivo.ColWidth(3) = 10000
    grdArquivo.Text = "Observa��o"
    
    
    
    '//////////////////////////
    'LEITURA DO ARQUIVO
    '//////////////////////////
    Open vNomeArq For Input As #1
    
    'Inicializa contadores de linhas do grid
    I = 0
    lblMsg.Visible = True
    
    'L� enquanto n�o for final de arquivo
    Do While Not EOF(1)
    
        Screen.MousePointer = 11
        I = I + 1
        
        lblMsg = "CARREGANDO ...." & I
        lblMsg.Refresh
        
        'Inicializa flags e vari�veis
        vFl_DPK_OK = True
        vDPK = 0
        vFabricante = 0
        vStrLinhaArq = ""
        
        'L� registro - linha - do arquivo, joga na vari�vel  e seta ponteiro para o pr�ximo
        Line Input #1, varLinhaArquivo
        'Se linha lida for em branco, sai do loop
        If Mid(varLinhaArquivo, 1, 8) = "" Then
            Exit Do
        End If
        
        With grdArquivo
        
            'Qtd. Linhas do grid ser� correspondente � qtd. de linhas lidas do arquivo texto
            .Rows = I + 1
            .Row = I
                    
            '1a. Coluna vai receber a numera��o de registros lidos do arquivo
            .Col = 0
            .Text = Format(I, "0000")
            
            'Completar colunas de acordo com informa��es lidas no arquivo
            vStrLinhaArq = Trim(Mid(varLinhaArquivo, 1, 5))
            vDPK = Val(vStrLinhaArq)
            .Col = 1
            .Text = vStrLinhaArq
        
            'Valida DPK
            If vStrLinhaArq = "" Then
                grdArquivo.Col = 3
                grdArquivo.Text = "DPK n/Existe! "
            Else
                db.Parameters.Remove "DPK": db.Parameters.Add "DPK", vDPK, 1
                'Este select vai verificar dados do DPK
                CmdSql = "Select E.Situacao " & _
                           "From ITEM_ESTOQUE E, ITEM_CADASTRO C " & _
                          "Where E.Cod_Loja = 1 " & _
                            "And E.Cod_Dpk = :DPK " & _
                            "And E.Cod_Dpk = C.Cod_Dpk "
                Set db_CONS = db.dbcreatedynaset(CmdSql, 0&)
                
                If db_CONS.EOF Then
                    grdArquivo.Col = 3
                    grdArquivo.Text = "DPK n/Existe! "
                Else
                    If Val(db_CONS!Situacao.Value) = 8 Then
                        grdArquivo.Col = 3
                        grdArquivo.Text = "DPK Substitu�do! "
                    ElseIf Val(db_CONS!Situacao.Value) = 9 Then
                        grdArquivo.Col = 3
                        grdArquivo.Text = "DPK Desativado! "
                    Else
                        'Verifica Fabricante
                        vStrLinhaArq = Trim(Mid(varLinhaArquivo, 6, 3))
                        vFabricante = Val(vStrLinhaArq)
                        .Col = 2
                        .Text = vStrLinhaArq
                    
                        If vStrLinhaArq = "" Then
                            grdArquivo.Col = 3
                            grdArquivo.Text = "Fabricante n/Existe! "
                        Else
                            db.Parameters.Remove "FABRICANTE": db.Parameters.Add "FABRICANTE", vFabricante, 1
                            CmdSql = "Select Situacao " & _
                                       "From Producao.FABRICANTE " & _
                                      "Where Cod_Fabricante = :FABRICANTE "
                            Set db_CONS = db.dbcreatedynaset(CmdSql, 0&)
                            
                            If db_CONS.EOF Then
                                grdArquivo.Col = 3
                                grdArquivo.Text = "Fabricante n/Existe! "
                            Else
                                If Val(db_CONS!Situacao.Value) = 9 Then
                                    grdArquivo.Col = 3
                                    grdArquivo.Text = "Fabricante Desativado! "
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        End With
    Loop

    MsgBox "Arquivo Lido!", vbInformation, "Aten��o!"
    lblMsg = I & " Linhas Lidas do Arquivo!"
    lblMsg.Refresh
    
    Close #1    ' Close file.
    Screen.MousePointer = 0
        
    Exit Sub
        
       
Trata_Erro:
    
    If Err.Number = 53 Then
        MsgBox "Arquivo de Controle n�o Encontrado. ", , MSG_TIT
    Else
        Call Process_Line_Errors
    End If

End Sub


Private Sub cmdArquivo_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp1.Visible = True
End Sub


Private Sub cmdConfirma_Click()
Dim vObs As String            'Guarda o conte�do da coluna de Observa��es
Dim I As Integer              'Ponteiro de linha do grid
Dim vReg_Gravado As Long      'Guarda a qtd. de itens gravados
Dim vCommit As Long           'Guarda a qtd. de itens para commitar de 20 em 20 nos CD's
Dim vLinSemObs As Long        'Guarda a qtd.de linhas sem observa��o
Dim cmdPlSql As String        'Comando SQL da LOJA_CONEXAO

If MsgBox("Confirma carga dos DPK x Fabricante?", vbYesNo, "CAD760") = vbYes Then

    'Primeiro vai percorrer todo o grid p/ ver se tem itens a serem gravados
    CmdPl = ""
    vLinSemObs = 0
    For I = 1 To grdArquivo.Rows - 1
        grdArquivo.Row = I
        grdArquivo.Col = 3
        vObs = grdArquivo.Text
        'Se a coluna de Observa��o n�o contiver nenhuma mensagem de inconsist�ncia vai gravar
        If vObs = "" Then
            vLinSemObs = vLinSemObs + 1
        End If
    Next I
    If vLinSemObs = 0 Then
        MsgBox "Nenhum dado poder� ser alterado!", vbCritical, "Aten��o!"
        Exit Sub
    End If
    
    'Se houverem itens a serem gravados, vai iniciar todo o processo
    'Zera contador de linhas gravadas
    vReg_Gravado = 0
    CmdPl = ""
    vCommit = 0

    db.Parameters.Remove "cod_errora": db.Parameters.Add "cod_errora", 0, 2
    db.Parameters.Remove "txt_errora": db.Parameters.Add "txt_errora", "", 2
    
    'Pega linha a linha do grid para gravar as que cont�m campo de Observa��o = ""
    For I = 1 To grdArquivo.Rows - 1
        
        grdArquivo.Row = I
        grdArquivo.Col = 3
        vObs = Trim(grdArquivo.Text)
        
        'Se a coluna de Observa��o n�o contiver nenhuma mensagem de inconsist�ncia vai gravar
        If vObs = "" Then
            
            '//////////////////////////////////////////////
            'Vai alimentar as vari�veis normais e de bind
            '/////////////////////////////////////////////
            'Limpa vari�veis auxiliares de grava��o
            'Guarda informa��es da linha em refer�ncia do grid, nas vari�veis de bind
            grdArquivo.Col = 1
            db.Parameters.Remove "CODDPK" & I:      db.Parameters.Add "CODDPK" & I, Val(Trim(grdArquivo.Text)), 1
            grdArquivo.Col = 2
            db.Parameters.Remove "FABRICANTE" & I:  db.Parameters.Add "FABRICANTE" & I, Val(Trim(grdArquivo.Text)), 1
    
            '//////////////////////////////////////
            'PROCESSO PARA GRAVAR RELACIONAMENTO - SOMENTE EM CAMPINAS
            '//////////////////////////////////////
            
            CmdPl = CmdPl & _
                    "Update Producao.R_ITEM_FABRICANTE " & _
                       "Set Cod_Fabricante = :FABRICANTE" & I & " " & _
                     "WHere Cod_Dpk = :CODDPK" & I & "; " & _
                    "IF SQL%NOTFOUND THEN " & _
                        "Insert into Producao.R_ITEM_FABRICANTE " & _
                        "(Cod_Dpk, Cod_Fabricante) Values " & _
                        "(:CODDPK" & I & ", :FABRICANTE" & I & "); " & _
                    "END IF; "

            vCommit = vCommit + 1
            vReg_Gravado = vReg_Gravado + 1

            If vCommit >= 20 Then

                'Executa transa��o gravando o item em Campinas
                CmdPl = "BEGIN " & CmdPl & " Commit; " & _
                            "Exception When Others then RollBack; " & _
                            ":Cod_Errora := SQLCODE; :Txt_Errora := SQLERRM; " & _
                        "End;"
                db.ExecuteSQL CmdPl
            
                If IIf(IsNull(db.Parameters("cod_errora")), 0, Val(db.Parameters("cod_errora"))) <> 0 Then
                    MsgBox db.Parameters("txt_errora") & ". Ligue p/ Depto.Sistemas"
                    Exit Sub
                End If
                vCommit = 0
                CmdPl = ""
            
            End If
            
        End If
    
    Next I
    
    If vCommit <> 0 Then
        'Executa transa��o dos �ltimos relacionamentos que possam vir a sobrar
        CmdPl = "BEGIN " & CmdPl & " Commit; " & _
                    "Exception When Others then RollBack; " & _
                    ":Cod_Errora := SQLCODE; :Txt_Errora := SQLERRM; " & _
                "End;"
        db.ExecuteSQL CmdPl
    
        If IIf(IsNull(db.Parameters("cod_errora")), 0, Val(db.Parameters("cod_errora"))) <> 0 Then
            MsgBox db.Parameters("txt_errora") & ". Ligue p/ Depto.Sistemas"
            Exit Sub
        End If
    End If
    
    MsgBox "Carga OK! " & vReg_Gravado & " relacionamentos gravados em Campinas!", vbInformation, "Aten��o!"
End If

End Sub


Private Sub cmdConfirma_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp2.Left = cmdConfirma.Left
    lblHelp2.Top = cmdConfirma.Top + 735
    lblHelp2.Visible = True
    lblHelp2.Caption = "Gravar Dados"

End Sub


Private Sub cmdLog_Click()
Dim I As Integer                'Ponteiro de linha do grid
Dim J As Long                   'Ponteiro de caracter no campo observa��o
Dim vTamObs As Long             'Guarda a qtd. de caracteres da coluna Observa��o
Dim vObs As String              'Guarda o conte�do da coluna de Observa��es
Dim vFabricante As String       'Guarda o c�digo de fabricante para impress�o
Dim vDPK As String              'Guarda o c�digo DPK para impress�o
    
    
If MsgBox("Confirma gera��o do arquivo de log?", vbYesNo, "CAD750") = vbYes Then

    'Gera arquivo de log com o nome LODDMMAA.TXT
    Open "C:\CAD760\LOG\Fa" & Mid(vDt_Cad, 1, 2) & _
                        Mid(vDt_Cad, 4, 2) & Mid(vDt_Cad, 7, 2) & ".txt" For Output As #1
    'Open "C:\windows\temp\Fa" & Mid(vDt_Cad, 1, 2) & _
                        Mid(vDt_Cad, 4, 2) & Mid(vDt_Cad, 7, 2) & ".txt" For Output As #1
    
    Print #1, "LOG DO ARQUIVO: " & vNomeArq
    Print #1, ""
    Print #1, "DPK   Fabricante                            Observa��es"
    Print #1, "----- ---------- ---------------------------------------------------------------"
    
    'Pega linha a linha do grid para gravar as que cont�m campo de Observa��o <> ""
    For I = 1 To grdArquivo.Rows - 1
        
        grdArquivo.Row = I
        
        grdArquivo.Col = 1
        vDPK = Trim(grdArquivo.Text)        'Pega o c�digo DPK
        grdArquivo.Col = 2
        vFabricante = Trim(grdArquivo.Text) 'Pega o c�digo do fabricante
        grdArquivo.Col = 3
        vObs = Trim(grdArquivo.Text)        'Pega as Observa��es
        
        'Se a coluna de Observa��o n�o estiver em branco
        If vObs <> "" Then
            
            'Zero ponteiro que vai me indicar que caracter no campo obs. estou lendo
            J = 0
            'Pego o tamanho da observa��o para esta linha
            vTamObs = CLng(Len(vObs))
            
            'Se o campo de observa��o contiver mais de 63 caracteres, o arquivo de log ter�
            'que conter mais de 1 linha de mensagens para aquele item
            'Conto 63 pois antes da Obs., a 1a. linha ir� conter o C�d.DPK - C�d.F�bricante (17 caracteres)
            'E para a impress�o correta do arquivo s� pode ter 80 colunas
            
            'Se for menor ou igual a 63, gravo a linha no arquivo normalmente
            If vTamObs <= 63 Then
                Print #1, String(5 - Len(vDPK), " ") & vDPK & " " & _
                          String(10 - Len(vFabricante), " ") & vFabricante & " " & _
                          vObs
            'Sen�o
            Else
                'Imprimo os primeiros 63 caracteres
                Print #1, String(5 - Len(vDPK), " ") & vDPK & " " & _
                          String(10 - Len(vFabricante), " ") & vFabricante & " " & _
                          Mid(vObs, 1, 63)
                'Tamanho agora vai ser igual ao tamanho original - 63
                vTamObs = vTamObs - 63
                'Indico de que caracter vou come�ar a ler no cpo. Observa��o
                J = J + 64
                
                'Enquanto o tamanho for maior que 0
                Do While vTamObs > 0
                    'Se tamanho maior que 63, imprime os pr�ximos 63 caracteres e atualiza tam.e ponteiro
                    If vTamObs > 63 Then
                        Print #1, Space(17) & Mid(vObs, J, 63)
                        vTamObs = vTamObs - 63
                        J = J + 64
                    'Se tamanho menor que 63, imprime os caracteres que faltam
                    Else
                        Print #1, Space(17) & Mid(vObs, J, vTamObs)
                        vTamObs = 0
                    End If
                Loop
            End If
            
        End If
            
    Next I

    Print #1, ""
    Print #1, "***************** FIM DO ARQUIVO DE LOG ***********************"
    
    Close #1

    MsgBox "Arquivo gerado com sucesso!", vbInformation, "Aten��o!"


End If

End Sub


Private Sub cmdLog_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp2.Left = cmdLog.Left
    lblHelp2.Top = cmdLog.Top + 735
    lblHelp2.Visible = True
    lblHelp2.Caption = "Gerar Arq.de Log"
End Sub


Private Sub cmdVoltar_Click()
    Unload Me
End Sub

Private Sub cmdVoltar_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp2.Left = cmdVoltar.Left
    lblHelp2.Top = cmdVoltar.Top + 735
    lblHelp2.Visible = True
    lblHelp2.Caption = "Voltar"
End Sub


Private Sub scmdLayOut_Click()
    frmLayOut.Show vbModal
End Sub


Private Sub scmdLayOut_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp2.Left = scmdLayOut.Left
    lblHelp2.Top = scmdLayOut.Top + 735
    lblHelp2.Visible = True
    lblHelp2.Caption = "Lay-Out"
End Sub


