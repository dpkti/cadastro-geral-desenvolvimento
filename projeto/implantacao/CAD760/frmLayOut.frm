VERSION 4.00
Begin VB.Form frmLayOut 
   Caption         =   "Lay-Out do Arquivo para Carga de relacionamento DPK x Fabricante"
   ClientHeight    =   4785
   ClientLeft      =   1665
   ClientTop       =   2325
   ClientWidth     =   8535
   Height          =   5190
   Left            =   1605
   LinkTopic       =   "Form1"
   ScaleHeight     =   4785
   ScaleWidth      =   8535
   Top             =   1980
   Width           =   8655
   Begin VB.Frame Frame1 
      Height          =   3900
      Left            =   105
      TabIndex        =   0
      Top             =   60
      Width           =   8355
      Begin VB.Label Label25 
         AutoSize        =   -1  'True
         Caption         =   "5) Fazer arquivos de - no m�ximo - "
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   4905
         TabIndex        =   16
         Top             =   1590
         Width           =   3000
      End
      Begin VB.Label Label24 
         AutoSize        =   -1  'True
         Caption         =   "   100 linhas."
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   4950
         TabIndex        =   15
         Top             =   1785
         Width           =   1125
      End
      Begin VB.Line Line5 
         X1              =   0
         X2              =   4740
         Y1              =   2235
         Y2              =   2235
      End
      Begin VB.Label Label17 
         AutoSize        =   -1  'True
         Caption         =   "54358002"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   585
         TabIndex        =   14
         Top             =   2985
         Width           =   840
      End
      Begin VB.Label Label16 
         AutoSize        =   -1  'True
         Caption         =   "00007003"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   585
         TabIndex        =   13
         Top             =   3165
         Width           =   840
      End
      Begin VB.Label Label13 
         AutoSize        =   -1  'True
         Caption         =   "2) Campos AlfaNum�ricos dever�o vir "
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   4905
         TabIndex        =   12
         Top             =   1035
         Width           =   3255
      End
      Begin VB.Label Label12 
         AutoSize        =   -1  'True
         Caption         =   "   com espa�os em branco � direita."
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   4950
         TabIndex        =   11
         Top             =   1245
         Width           =   3060
      End
      Begin VB.Label Label11 
         AutoSize        =   -1  'True
         Caption         =   "   zeros � esquerda."
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   4935
         TabIndex        =   9
         Top             =   675
         Width           =   1710
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         Caption         =   "1) Campos Num�ricos dever�o vir com"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   4890
         TabIndex        =   8
         Top             =   465
         Width           =   3270
      End
      Begin VB.Label Label9 
         AutoSize        =   -1  'True
         Caption         =   "Observa��es:"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000040C0&
         Height          =   195
         Left            =   4920
         TabIndex        =   7
         Top             =   210
         Width           =   1185
      End
      Begin VB.Line Line3 
         X1              =   4740
         X2              =   4740
         Y1              =   105
         Y2              =   3885
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         Caption         =   "54357001"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   585
         TabIndex        =   6
         Top             =   2805
         Width           =   840
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "03232001"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   585
         TabIndex        =   5
         Top             =   2625
         Width           =   840
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Exemplo de Arquivo:"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000040C0&
         Height          =   195
         Left            =   570
         TabIndex        =   4
         Top             =   2385
         Width           =   1755
      End
      Begin VB.Line Line2 
         X1              =   0
         X2              =   4740
         Y1              =   2265
         Y2              =   2265
      End
      Begin VB.Line Line1 
         X1              =   0
         X2              =   4710
         Y1              =   585
         Y2              =   585
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Campo                            Tipo de Dados"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000040C0&
         Height          =   195
         Left            =   570
         TabIndex        =   3
         Top             =   375
         Width           =   3510
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "C�digo Fabricante           NUM�RICO (03)          "
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   570
         TabIndex        =   2
         Top             =   990
         Width           =   3675
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "C�digo Dpk                    NUM�RICO (05)"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   585
         TabIndex        =   1
         Top             =   735
         Width           =   3585
      End
   End
   Begin Threed.SSCommand cmdVoltar 
      Height          =   660
      Left            =   7770
      TabIndex        =   10
      Top             =   4035
      Width           =   660
      _Version        =   65536
      _ExtentX        =   1164
      _ExtentY        =   1164
      _StockProps     =   78
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BevelWidth      =   4
      Outline         =   0   'False
      Picture         =   "frmLayOut.frx":0000
   End
End
Attribute VB_Name = "frmLayOut"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Private Sub cmdVoltar_Click()
    Unload Me
End Sub


