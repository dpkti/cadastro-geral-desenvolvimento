VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.MDIForm MDI 
   BackColor       =   &H8000000C&
   Caption         =   "CAD890 - CADASTRO DE EQUIPAMENTOS POR FILIAL"
   ClientHeight    =   3195
   ClientLeft      =   165
   ClientTop       =   735
   ClientWidth     =   4680
   Icon            =   "MDI.frx":0000
   LinkTopic       =   "MDIForm1"
   LockControls    =   -1  'True
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   3240
      Top             =   60
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI.frx":0442
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI.frx":0B96
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDI.frx":0EB2
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   660
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   4680
      _ExtentX        =   8255
      _ExtentY        =   1164
      ButtonWidth     =   1032
      ButtonHeight    =   1005
      Appearance      =   1
      ImageList       =   "ImageList1"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   6
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "TBCadastro"
            Object.ToolTipText     =   "Cadastrar/Alterar/Consultar equipamentos: "
            ImageIndex      =   3
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "TBSobre"
            Object.ToolTipText     =   "Informa��es sobre o programa:"
            ImageIndex      =   1
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "TBSair"
            Object.ToolTipText     =   "Sair do programa:"
            ImageIndex      =   2
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuCadastro 
      Caption         =   "&Cadastro"
   End
   Begin VB.Menu mnuSobre 
      Caption         =   "Sob&re"
   End
   Begin VB.Menu mnuSair 
      Caption         =   "&Sair"
   End
End
Attribute VB_Name = "MDI"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub MDIForm_Load()

On Error GoTo Trata_Erro
  
  msg_titl = "CAD890 - Cadastro de Equipamentos por Filial"
  
  If App.PrevInstance Then
  
    MsgBox "Este programa j� est� aberto!", vbCritical, msg_titl
    End
    
  End If
  
  
  Set orasession = CreateObject("oracleinprocserver.xorasession")
  Set db = orasession.OpenDatabase("PRODUCAO", "cad890/PROD", 0&)
  'Set db = orasession.OpenDatabase("desenv", "PRODUCAO/DES", 0&)
  
  
  Set OraParameters = db.Parameters
  OraParameters.Remove "vCursor"
  OraParameters.Add "vCursor", 0, 2
  OraParameters("vCursor").serverType = ORATYPE_CURSOR
  OraParameters("vCursor").DynasetOption = ORADYN_NO_BLANKSTRIP
  OraParameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
  
  OraParameters.Remove "vErro"
  OraParameters.Add "vErro", 0, 2
  OraParameters("vErro").serverType = ORATYPE_NUMBER
  
  OraParameters.Remove "vErroMsg"
  OraParameters.Add "vErroMsg", "", 2
  OraParameters("vErroMsg").serverType = ORATYPE_VARCHAR2
  
  
 Exit Sub

Trata_Erro:
   
   Call Process_Line_Errors(SQL)

End Sub

Private Sub MDIForm_QueryUnload(Cancel As Integer, UnloadMode As Integer)

    db.Close

End Sub

Private Sub mnuCadastro_Click()

    frmCAD890.Show vbModal

End Sub

Private Sub mnuSair_Click()

    End

End Sub

Private Sub mnuSobre_Click()

    frmSobre.Show vbModal

End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)

    Select Case Button.Key
    
        Case "TBCadastro"
            
            Call mnuCadastro_Click
                            
        Case "TBSobre"
        
            Call mnuSobre_Click
            
        Case "TBSair"
        
            Call mnuSair_Click
            
    End Select

End Sub
