VERSION 5.00
Begin VB.Form frmSobre 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Informações Sobre o Programa:"
   ClientHeight    =   4755
   ClientLeft      =   45
   ClientTop       =   615
   ClientWidth     =   5070
   Icon            =   "frmSobre.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4755
   ScaleWidth      =   5070
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox Picture1 
      Height          =   1065
      Left            =   803
      Picture         =   "frmSobre.frx":0742
      ScaleHeight     =   1005
      ScaleWidth      =   3375
      TabIndex        =   1
      Top             =   240
      Width           =   3435
   End
   Begin VB.CommandButton Command1 
      Caption         =   "OK!"
      Height          =   525
      Left            =   1913
      TabIndex        =   0
      ToolTipText     =   "OK!"
      Top             =   4170
      Width           =   1245
   End
   Begin VB.Label Label1 
      Caption         =   "Programa:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   1868
      TabIndex        =   7
      Top             =   1620
      Width           =   1335
   End
   Begin VB.Label Label2 
      Caption         =   "Responsável:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   1673
      TabIndex        =   6
      Top             =   2460
      Width           =   1725
   End
   Begin VB.Label Label3 
      Caption         =   "Contato:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2003
      TabIndex        =   5
      Top             =   3300
      Width           =   1065
   End
   Begin VB.Label Label4 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "CAD890 -Cadastro de Equipamentos por Filial"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   53
      TabIndex        =   4
      Top             =   2040
      Width           =   4965
   End
   Begin VB.Label Label5 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Renato"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   2093
      TabIndex        =   3
      Top             =   2880
      Width           =   885
   End
   Begin VB.Label Label6 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "supsist@dpk.com.br"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1403
      TabIndex        =   2
      Top             =   3690
      Width           =   2265
   End
   Begin VB.Menu mnuSair 
      Caption         =   "&Sair"
   End
End
Attribute VB_Name = "frmSobre"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Command1_Click()
    Call mnuSair_Click
End Sub

Private Sub mnuSair_Click()
    Unload Me
End Sub
