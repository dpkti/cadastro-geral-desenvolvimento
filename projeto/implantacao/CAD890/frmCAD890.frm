VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmCAD890 
   Appearance      =   0  'Flat
   BackColor       =   &H80000004&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cadastro/Altera��o/Consulta de Equipamento por Filial"
   ClientHeight    =   3465
   ClientLeft      =   150
   ClientTop       =   435
   ClientWidth     =   7980
   Icon            =   "frmCAD890.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3465
   ScaleWidth      =   7980
   StartUpPosition =   1  'CenterOwner
   Begin MSFlexGridLib.MSFlexGrid grdEquip 
      Height          =   2595
      Left            =   90
      TabIndex        =   11
      TabStop         =   0   'False
      Top             =   3570
      Width           =   7785
      _ExtentX        =   13732
      _ExtentY        =   4577
      _Version        =   393216
      Rows            =   7
      Cols            =   3
      FixedCols       =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   7380
      Top             =   60
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCAD890.frx":0442
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCAD890.frx":0896
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCAD890.frx":0BB2
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCAD890.frx":0ECE
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   660
      Left            =   0
      TabIndex        =   10
      Top             =   0
      Width           =   7980
      _ExtentX        =   14076
      _ExtentY        =   1164
      ButtonWidth     =   1032
      ButtonHeight    =   1005
      Appearance      =   1
      ImageList       =   "ImageList1"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "TBPesquisar"
            Object.ToolTipText     =   "Exibe/Oculta Grid :"
            ImageIndex      =   1
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "TBSair"
            Object.ToolTipText     =   "Voltar:"
            ImageIndex      =   4
         EndProperty
      EndProperty
   End
   Begin VB.Frame fraCad 
      Caption         =   "Cadastro:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2715
      Left            =   0
      TabIndex        =   2
      Top             =   750
      Width           =   7965
      Begin VB.Frame fraCmd 
         Height          =   795
         Left            =   2490
         TabIndex        =   9
         Top             =   1740
         Width           =   2715
         Begin VB.CommandButton cmdConfirma 
            Caption         =   "Confirma:"
            Height          =   525
            Left            =   1380
            TabIndex        =   4
            ToolTipText     =   "Confirma Dados:"
            Top             =   180
            Width           =   1245
         End
         Begin VB.CommandButton cmdCancela 
            Caption         =   "Cancela:"
            Height          =   525
            Left            =   90
            TabIndex        =   3
            ToolTipText     =   "Apaga Dados:"
            Top             =   180
            Width           =   1245
         End
      End
      Begin VB.ComboBox cboNome 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   1703
         TabIndex        =   0
         ToolTipText     =   "Escolha o nome da filial"
         Top             =   1260
         Width           =   3855
      End
      Begin VB.TextBox txtQtde 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   5723
         MaxLength       =   3
         TabIndex        =   1
         ToolTipText     =   "Digite a quantidade de equipamentos:"
         Top             =   1260
         Width           =   1275
      End
      Begin VB.ComboBox cboCod 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   383
         TabIndex        =   7
         TabStop         =   0   'False
         ToolTipText     =   "Escolha o c�digo da filial:"
         Top             =   1260
         Width           =   885
      End
      Begin VB.Label lblNome 
         Caption         =   "Nome da Filial:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   675
         Left            =   1710
         TabIndex        =   8
         Top             =   450
         Width           =   1035
      End
      Begin VB.Label lblCod 
         Caption         =   "C�digo da Filial:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   675
         Left            =   390
         TabIndex        =   6
         Top             =   450
         Width           =   1035
      End
      Begin VB.Label lblQtde 
         Caption         =   "Quantidade de Equipamento:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   705
         Left            =   5730
         TabIndex        =   5
         Top             =   420
         Width           =   1875
      End
   End
   Begin VB.Menu mnuPesquisa 
      Caption         =   "&Pesquisar"
   End
   Begin VB.Menu mnuVoltar 
      Caption         =   "&Voltar"
   End
End
Attribute VB_Name = "frmCAD890"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cboCod_KeyPress(KeyAscii As Integer)
    
    If IsNumeric(KeyAscii) = False Or True Then
    
        KeyAscii = 0
        
    End If
    
End Sub

Private Sub cboCod_LostFocus()

    Call VALIDA_cmdCONFIRMA

End Sub

Private Sub cboNome_Click()

    OraParameters.Remove "NOME"
    OraParameters.Add "NOME", cboNome.Text, 1
    
    CmdSql = "begin producao.pck_cad890.pr_cbonome(:vCURSOR,:NOME,:vErro);end;"
    
    db.executesql CmdSql
    
    OraParameters.Remove "NOME"
    
    If Val(db.Parameters("vErro").Value) <> 0 Then
                
        MsgBox "Erro - " & db.Parameters("vErro").Value & ". Favor avisar Depto. de Sistemas!", 48, msg_titl
        Exit Sub
        
    End If
    
    Set db_cons = db.Parameters("vCursor").Value
    
    If db_cons.EOF = True Then
        
        Exit Sub
        
    Else
    
        cboCod.Text = db_cons!cod_filial
        
    End If
    
    OraParameters.Remove "COD"
    OraParameters.Add "COD", Val(cboCod.Text), 1
    
    CmdSql = "begin producao.pck_cad890.pr_qtdequip(:vCURSOR,:COD,:vErro);end;"
             
    db.executesql CmdSql
    
    OraParameters.Remove "COD"
    
    Set db_cons = db.Parameters("vCursor").Value
    
    If Val(db.Parameters("vErro").Value) <> 0 Then
                
        MsgBox "Erro - " & db.Parameters("vErro").Value & ". Favor avisar Depto. de Sistemas!", 48, msg_titl
        Exit Sub
        
    End If
    
    If db_cons.EOF = True Then
        
        fl_sit = 1
        
        txtQtde.Text = "-"
        
    Else
    
        fl_sit = 0
        
        txtQtde.Text = db_cons!qtd_equipamento
        
    End If
    

End Sub

Private Sub cboNome_KeyPress(KeyAscii As Integer)

    If IsNumeric(KeyAscii) = False Or True Then
    
        KeyAscii = 0
        
    End If

End Sub

Private Sub cboNome_LostFocus()

    Call VALIDA_cmdCONFIRMA

End Sub

Private Sub cmdCancela_Click()

    cboCod.Text = ""
    cboNome.Text = ""
    txtQtde.Text = ""
    
    fraCad.Caption = "Cadastro:"
    cboNome.SetFocus
    
End Sub

Private Sub cmdConfirma_Click()

    If fl_sit = 1 Then
    
        Dim resposta As Byte
        
        resposta = MsgBox("Confirma Inclus�o??", vbYesNo + vbQuestion, msg_titl)
        
        If Val(resposta) = 7 Then
        
            Call cmdCancela_Click
            Exit Sub
        
        End If
        
        If Val(resposta) = 6 Then
        
            Dim quantidade As String
            quantidade = txtQtde.Text
            
            If quantidade = "-" Then
                
                quantidade = 0
                
            End If
            
            OraParameters.Remove "COD"
            OraParameters.Remove "QTD"
            OraParameters.Remove "Cod_ErrorPL"
                        
            OraParameters.Add "COD", cboCod.Text, 1
            OraParameters.Add "QTD", quantidade, 1
                        
            CmdSql = "begin producao.pck_cad890.pr_cmdconfirma_insert(:COD,:QTD,:vErro); END;"
            
            db.executesql CmdSql
            
            OraParameters.Remove "COD"
            OraParameters.Remove "QTD"
            
            Call Form_Load
            Call cmdCancela_Click
            
        End If
            
            If Val(OraParameters("vErro").Value) <> 0 Then
                
                MsgBox "Erro - " & OraParameters("vErro").Value & ". Favor avisar Depto. de Sistemas!", 48, msg_titl
                Exit Sub
            
            Else
            
                MsgBox "Inclus�o OK!", vbInformation, msg_titl
            
            End If
            
        End If
        
    If fl_sit = 0 Then
    
        resposta = MsgBox("Confirma Altera��o??", vbYesNo + vbQuestion, msg_titl)
                        
        If Val(resposta) = 7 Then
        
            Call cmdCancela_Click
            Exit Sub
        
        End If
        
        If Val(resposta) = 6 Then
        
            OraParameters.Remove "COD"
            OraParameters.Remove "QTD"
            OraParameters.Remove "txt_errorpl"
            
            OraParameters.Add "COD", cboCod.Text, 1
            OraParameters.Add "QTD", txtQtde.Text, 1
                        
            CmdSql = "begin producao.pck_cad890.pr_cmdconfirma_update(:COD,:QTD,:vErro,:vErroMsg); end;"
            
            db.executesql CmdSql
            
            OraParameters.Remove "COD"
            OraParameters.Remove "QTD"
                                    
            Call Form_Load
            Call cmdCancela_Click
            
        End If
            
            If Val(OraParameters("vErro").Value) <> 0 Then
                
                MsgBox "Erro - " & OraParameters("vErro").Value & _
                OraParameters("vErroMsg").Value & _
                "Favor avisar Depto. de Sistemas!", vbCritical, msg_titl
                
            Else
                
                MsgBox "Altera��o OK!", vbInformation, msg_titl
            
            End If
            
        End If
    
    
        
End Sub

Private Sub Form_Load()

    Dim i As Integer

    CmdSql = "begin producao.pck_cad890.pr_form_combo_codigo(:vCURSOR, :vErro); End;"
    
    db.executesql CmdSql
    
    Set db_cons = db.Parameters("vCursor").Value
    
    If Val(db.Parameters("vErro").Value) <> 0 Then
                
        MsgBox "Erro - " & db.Parameters("vErro").Value & ". Favor avisar Depto. de Sistemas!", 48, msg_titl
        Exit Sub
        
    End If
            
    If db_cons.EOF = True Then
    
        MsgBox "N�o h� dados para constru��o do campo 'C�digo da Filial'!", vbCritical, msg_titl
        Exit Sub
        
    Else
    
        For i = 1 To db_cons.recordcount
            
            cboCod.AddItem CStr(db_cons!cod_filial)
            db_cons.movenext
            
        Next i
    
    End If
    
    
    
    
    
    
    CmdSql = "begin producao.pck_cad890.pr_form_combo_nome(:vCURSOR, :vErro); End;"
    
    db.executesql CmdSql
    
    Set db_cons = db.Parameters("vCursor").Value
    
    If Val(db.Parameters("vErro").Value) <> 0 Then
                
        MsgBox "Erro - " & db.Parameters("vErro").Value & ". Favor avisar Depto. de Sistemas!", 48, msg_titl
        Call Process_Line_Errors(SQL)
        Exit Sub
        
    End If
            
    If db_cons.EOF = True Then
    
        MsgBox "N�o h� dados para constru��o do campo 'Nome da Filial'!", vbCritical, msg_titl
        Exit Sub
        
    Else
    
        For i = 1 To db_cons.recordcount
            
            cboNome.AddItem CStr(db_cons!nome_filial)
            db_cons.movenext
            
        Next i
    
    End If
    
    
    
    
    
    
    CmdSql = "begin producao.pck_cad890.pr_form_grid(:vCursor, :vErro); End;"
    
    db.executesql CmdSql
             
    Set db_cons = db.Parameters("vCursor").Value
    
    If Val(db.Parameters("vErro").Value) <> 0 Then
                
        MsgBox "Erro - " & db.Parameters("vErro").Value & ". Favor avisar Depto. de Sistemas!", 48, msg_titl
        Call Process_Line_Errors(SQL)
        Exit Sub
    
    End If
    
    If db_cons.EOF = True Then
    
        MsgBox "N�o h� dados para constru��o do grid de consulta!", vbCritical, msg_titl
        Exit Sub
        
    Else
    
        grdEquip.Rows = db_cons.recordcount + 1
        grdEquip.FormatString = "^Cod. Filial:|<Nome Filial:|^Qtde. Equipamento:"
        
        
        For i = 1 To db_cons.recordcount Step 1
        
            With grdEquip
        
                .Row = i
                .Col = 0
                .Text = db_cons!cod_filial
                .Col = 1
                .ColWidth(1) = 4000
                .Text = db_cons!nome_filial
                .Col = 2
                .Text = db_cons!qtd_equipamento
                db_cons.movenext
            
            End With
        
        Next i
    
    End If
        
Exit Sub
    
End Sub


Private Sub grdEquip_DblClick()
    
        grdEquip.Col = 0
        
        cboCod.Text = grdEquip.Text
        
        Call cboCod_Click
        
        txtQtde.SetFocus
        
End Sub


Private Sub mnuPesquisa_Click()
    
    If frmCAD890.Height < 5000 Then
            
            frmCAD890.Height = 7000
            frmCAD890.Top = 1395
    Else
                
            frmCAD890.Height = 4125
            frmCAD890.Top = 2220
               
    End If
                
End Sub

Private Sub mnuVoltar_Click()
    
    Unload Me

End Sub

Private Sub cboCod_Click()
        
    OraParameters.Remove "COD"
    OraParameters.Add "COD", cboCod.Text, 1
        
    CmdSql = "begin producao.pck_cad890.pr_cbocod(:vCursor,:COD,:vErro); end;"
    
    db.executesql CmdSql
    
    OraParameters.Remove "COD"
    
    Set db_cons = db.Parameters("vCursor").Value
    
    
      
    If db_cons.EOF = True Then
        
        Exit Sub
        
    Else
    
        cboNome.Text = db_cons!nome_filial
        
    End If
    
    OraParameters.Remove "COD"
    OraParameters.Add "COD", cboCod.Text, 1
    
CmdSql = "begin producao.pck_cad890.pr_qtdequip(:vCURSOR,:COD, :vErro);end;"
             
    db.executesql CmdSql
    
    Set db_cons = db.Parameters("vCursor").Value
    
    If Val(db.Parameters("vErro").Value) <> 0 Then
                
        MsgBox "Erro - " & db.Parameters("vErro").Value & ". Favor avisar Depto. de Sistemas!", 48, msg_titl
        Exit Sub
        
    End If
    
    If db_cons.EOF = True Then
        
        fl_sit = 1
        
        txtQtde.Text = "-"
        
    Else
    
        fl_sit = 0
        
        txtQtde.Text = db_cons!qtd_equipamento
        
    End If
    

End Sub

    Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)

    Select Case Button.Key
    
        Case "TBSair"
            
            Call mnuVoltar_Click
            
        Case "TBPesquisar"
        
            Call mnuPesquisa_Click
            
    End Select
    

End Sub

Private Sub txtQtde_GotFocus()

    txtQtde.SelLength = Len(txtQtde.Text)
    
    If fl_sit = 1 Then
        
        fraCad.Caption = "Inclus�o de Dados:"
        
    Else
    
        fraCad.Caption = "Altera��o de Dados:"
        
    End If
    
End Sub

Private Sub txtQtde_KeyPress(KeyAscii As Integer)
    
    KeyAscii = Numerico(KeyAscii)
    
End Sub

Private Sub txtQtde_LostFocus()

    Call VALIDA_cmdCONFIRMA

End Sub
