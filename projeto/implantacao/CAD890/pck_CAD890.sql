
--PACKAGE CAD890
--SISTEMA: CAD890 - CADASTRO DE EQUIPAMENTOS POR FILIAL
--ANALISTA: RENATO H.
--DATA: 03/07/03

CREATE OR REPLACE PACKAGE PCK_CAD890

IS 

TYPE tp_cursor IS REF CURSOR;


PROCEDURE PR_CBONOME(p_cursor  IN OUT  tp_cursor,
                     p_nome  IN  filial.nome_filial%TYPE,
					 p_erro  OUT  NUMBER);
					 
					 
PROCEDURE PR_CBOCOD(p_cursor  IN OUT  tp_cursor,
					p_cod  IN  filial.cod_filial%TYPE,
					p_erro  OUT  NUMBER);
					
					 
PROCEDURE PR_QTDEQUIP(p_cursor  IN OUT  tp_cursor,
					  p_cod  IN  filial.cod_filial%TYPE,	
					  p_erro OUT NUMBER);
					  
					  
PROCEDURE PR_CMDCONFIRMA_INSERT(p_cod  IN  filial.cod_filial%TYPE,
						p_qtd  IN  comissao.r_filial_equipamento.qtd_equipamento%TYPE,
						p_erro  OUT  NUMBER);
						
						
PROCEDURE PR_CMDCONFIRMA_UPDATE(p_cod  IN  comissao.r_filial_equipamento.cod_filial%TYPE,
						p_qtd  IN  comissao.r_filial_equipamento.qtd_equipamento%TYPE,
						p_erro  OUT  NUMBER,
						p_erro_msg  OUT  VARCHAR2);
						
						  
PROCEDURE PR_FORM_COMBO_CODIGO(p_cursor  IN OUT  tp_cursor,
						       p_erro  OUT NUMBER);
							   
						
PROCEDURE PR_FORM_COMBO_NOME(p_cursor  IN OUT  tp_cursor,
						     p_erro  OUT NUMBER);
							 
							 
PROCEDURE PR_FORM_GRID(p_cursor  IN OUT  tp_cursor,
					   p_erro  OUT  NUMBER);
					   
						 
END PCK_CAD890;
/






CREATE OR REPLACE PACKAGE BODY PCK_cad890 IS


PROCEDURE PR_CBONOME(p_cursor  IN OUT  tp_cursor,
                     p_nome  IN  filial.nome_filial%TYPE,
					 p_erro OUT NUMBER)
  IS
    
  BEGIN
  
  p_erro := 0;
  
  OPEN p_cursor FOR 
  
  select cod_filial 
  from filial 
  where nome_filial=p_nome;
  
  EXCEPTION
  WHEN OTHERS THEN
  p_erro := SQLCODE;
  
END PR_CBONOME;





PROCEDURE PR_CBOCOD(p_cursor  IN OUT tp_cursor,
					p_cod  IN  filial.cod_filial%TYPE,
					p_erro OUT NUMBER)
					
  IS 
  
  BEGIN
  
  p_erro := 0;
  
  OPEN p_cursor FOR
  
  select nome_filial 
  from filial 
  where cod_filial=p_cod;
  
  EXCEPTION
  WHEN OTHERS THEN
  p_erro := SQLCODE;
  
END PR_CBOCOD;
    




PROCEDURE PR_QTDEQUIP(p_cursor  IN OUT  tp_cursor,
					  p_cod  IN  filial.cod_filial%TYPE,
					  p_erro OUT NUMBER)
					
  IS
	
  BEGIN
  
  p_erro := 0;
	
  OPEN p_cursor FOR
  
  select qtd_equipamento 
  from comissao.r_filial_equipamento
  where cod_filial=p_cod;
  
  EXCEPTION
  WHEN OTHERS THEN
  p_erro := SQLCODE;
					  
END PR_QTDEQUIP;
	




PROCEDURE PR_CMDCONFIRMA_INSERT(p_cod  IN  filial.cod_filial%TYPE,						 
						  p_qtd  IN  comissao.r_filial_equipamento.qtd_equipamento%TYPE,
						  p_erro  OUT  NUMBER)
						  
  IS
	
  BEGIN
	
  p_erro := 0;
	
  INSERT INTO comissao.r_filial_equipamento
  VALUES(p_cod,p_qtd);
	
  COMMIT;
	
  EXCEPTION
  WHEN OTHERS THEN
  p_erro := SQLCODE;
	
END PR_CMDCONFIRMA_INSERT;	





PROCEDURE PR_CMDCONFIRMA_UPDATE(p_cod  IN  comissao.r_filial_equipamento.cod_filial%TYPE,
						p_qtd  IN  comissao.r_filial_equipamento.qtd_equipamento%TYPE,
						p_erro  OUT  NUMBER,
						p_erro_msg  OUT  VARCHAR2)
						
  IS
  
  BEGIN
  
  p_erro := 0;
  p_erro_msg := '';
    
  UPDATE comissao.r_filial_equipamento
  SET qtd_equipamento=p_qtd
  WHERE cod_filial=p_cod;
  
  COMMIT;
  
  EXCEPTION
  WHEN OTHERS THEN
  ROLLBACK;
  p_erro := SQLCODE;
  p_erro_msg := SQLERRM;
    
END PR_CMDCONFIRMA_UPDATE;
	




PROCEDURE PR_FORM_COMBO_CODIGO(p_cursor  IN OUT  tp_cursor,
						       p_erro  OUT NUMBER)

  IS
	
  BEGIN
	
  p_erro := 0;
	
  OPEN p_cursor FOR
  
  select cod_filial 
  from filial
  where divisao='D'
  order by cod_filial;
					  
  EXCEPTION
  WHEN OTHERS THEN
  p_erro := SQLCODE;
				  
END PR_FORM_COMBO_CODIGO;





PROCEDURE PR_FORM_COMBO_NOME(p_cursor  IN OUT  tp_cursor,
 						     p_erro  OUT  NUMBER)

  IS
	
  BEGIN
	
  p_erro := 0;
	
  OPEN p_cursor FOR
  
  select nome_filial 
  from filial
  where divisao='D'
  order by nome_filial;
					  
  EXCEPTION
  WHEN OTHERS THEN
  p_erro := SQLCODE;
				  
END PR_FORM_COMBO_NOME;





PROCEDURE PR_FORM_GRID(p_cursor  IN OUT  tp_cursor,
					   p_erro OUT  NUMBER)
					   
  IS
  
  BEGIN
  
  p_erro := 0;
  
  OPEN p_cursor FOR 
  
  select a.cod_filial, b.nome_filial, a.qtd_equipamento
  from comissao.r_filial_equipamento a, filial b
  where a.cod_filial=b.cod_filial
  order by a.cod_filial;
  
  EXCEPTION
  WHEN OTHERS THEN
  ROLLBACK;
  p_erro := SQLCODE;
  
END PR_FORM_GRID;


END PCK_CAD890;  
/  