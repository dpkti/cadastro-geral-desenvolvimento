Attribute VB_Name = "Mod_Pad"
Option Explicit

Public orasession As Object
Public db As Object
Public db_cons As Object
Public CmdSql As String
Public CmdPl As String
Public strPath As String
Public strPathUsuario As String
Public bFl_banco As Byte
Public strUser As String
Public strDiretorio_Salva As String

Public SQL As String
Public pl As String
Public dt_faturamento As Date
Public dt_ini_fech_dia As Date
Public dt_ini_fech_mensal As Date
Public dt_fin_fech_mensal As Date

Public cod_errora As Integer
Public txt_errora As String
Public txtresposta As String
Public ss As Object

Public cont As Long
Public strDiretorio As String
Public lngNum_Cotacao As Long
Public lngCod_Loja As Long
Public strArquivo As String
Public lngCod_Loja_Destino As Long

Public OraParameters As Object
Public VarRec As Object
Public VarRec1 As Object
Public V_SQL As String
 
Global Const ORATYPE_CURSOR = 102
Global Const ORADYN_NO_BLANKSTRIP = &H2&
Global Const ORAPARM_INPUT = 1              'CONSTANTE DE BIND INPUT
Global Const ORAPARM_OUTPUT = 2             'CONSTANTE DE BIND OUTPUT
Global Const ORAPARM_BOTH = 3               'CONSTANTE DE BIND INPUT/OUTPUT
Global Const ORATYPE_NUMBER = 2             'CONSTANTE DE SERVER_TYPE PARA N�MEROS
Global Const ORATYPE_VARCHAR2 = 1           'CONSTANTE DE SERVER_TYPE PARA CARACTERES

Public msg_titl As String 'usada como t�tulo em Msgboxes e Inputboxes
Public fl_sit As Byte  'usada para acusar estado de inclus�o(1) ou altera��o(0) no banco de dados

 
 Sub Process_Line_Errors(ByRef SQL)
    Dim iFnum As Integer
          
    If Err.Number = 3186 Or Err.Number = 3188 Or Err.Number = 3260 Then
      Resume
    Else
      MsgBox "Ocorreu o erro: " & Err.Number & " -" & Err.Description & ". Ligue para o departamento de sistemas", vbCritical, msg_titl
    
      'fechar banco de dados
      'If bFl_banco = 1 Then
      '  dbaccess.Close
      'End If
      'cursor
      Screen.MousePointer = vbDefault
    
      'para a aplicacao
      End
    End If
    Exit Sub

End Sub

Function Valor(ByVal KeyAscii As Integer, strCampo As String) As Integer
    If KeyAscii = 8 Then    'BACKSPACE
        Valor = KeyAscii
        Exit Function
    End If
    
    If Chr$(KeyAscii) = "," Or Chr$(KeyAscii) = "." Then
        If InStr(strCampo, ",") > 0 Or InStr(strCampo, ".") > 0 Then
            KeyAscii = 0
            Beep
        End If
    Else
        If Chr$(KeyAscii) < "0" Or Chr$(KeyAscii) > "9" Then
            KeyAscii = 0
            Beep
        End If
    End If
    
    Valor = KeyAscii
End Function

Function FmtBR(ByVal Valor) As String

    Dim Temp As String
    Dim i As Integer
    
    Temp = Trim(Valor)
        
    For i = 1 To Len(Temp)
        If Mid$(Temp, i, 1) = "," Then
            Mid$(Temp, i, 1) = "."
        End If
    Next i

    FmtBR = Temp

End Function

Function Texto(ByVal KeyAscii As Integer) As Integer
    If KeyAscii = 8 Then    'BACKSPACE
        Texto = KeyAscii
        Exit Function
    End If
    
    If Chr$(KeyAscii) = "'" Then
        KeyAscii = 0
        Beep
    Else
        KeyAscii = Asc(UCase(Chr$(KeyAscii)))
    End If
    
    Texto = KeyAscii
End Function

Function Numerico(ByVal KeyAscii As Integer) As Integer
    If KeyAscii = 8 Then    'BACKSPACE
        Numerico = KeyAscii
        Exit Function
    End If
    If Chr(KeyAscii) < "0" Or Chr(KeyAscii) > "9" Then
        KeyAscii = 0
        Beep
    End If
    Numerico = KeyAscii
End Function

Function Maiusculo(KeyAscii As Integer) As Integer
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    Maiusculo = KeyAscii
End Function

Sub VALIDA_cmdCONFIRMA() 'consist�ncia para impedir o uso do bot�o "confirma" sem que haja necessidade

    With frmCAD890
    
        If .txtQtde.Text = "" Or .cboCod.Text = "" Or .cboNome.Text = "" Then
        
            .cmdConfirma.Enabled = False
        
        Else
        
            .cmdConfirma.Enabled = True
            
        End If
        
    End With
    
End Sub

