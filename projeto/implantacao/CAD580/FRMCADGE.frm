VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FRMCADGER 
   Caption         =   "CAD580 - Cadastro de Gerentes"
   ClientHeight    =   3255
   ClientLeft      =   3030
   ClientTop       =   3735
   ClientWidth     =   6870
   LinkTopic       =   "Form1"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   3255
   ScaleWidth      =   6870
   Begin VB.Frame Frame1 
      Height          =   1515
      Left            =   900
      TabIndex        =   9
      Top             =   540
      Width           =   5835
      Begin VB.TextBox TXT_EMAIL 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   315
         Left            =   900
         MaxLength       =   50
         TabIndex        =   2
         Top             =   1080
         Width           =   4815
      End
      Begin VB.ComboBox CBO_FILIAL 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "FRMCADGE.frx":0000
         Left            =   900
         List            =   "FRMCADGE.frx":0002
         TabIndex        =   1
         Top             =   660
         Width           =   4815
      End
      Begin VB.ComboBox CBO_COD_SUPER_COORD 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "FRMCADGE.frx":0004
         Left            =   900
         List            =   "FRMCADGE.frx":0006
         TabIndex        =   0
         Top             =   240
         Width           =   4815
      End
      Begin VB.Label Label12 
         Caption         =   "E-mail"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   1140
         Width           =   675
      End
      Begin VB.Label Label7 
         Caption         =   "Filial"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   720
         Width           =   495
      End
      Begin VB.Label Label1 
         Caption         =   "C�digo"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   300
         Width           =   735
      End
   End
   Begin Threed.SSPanel SSPanel1 
      Align           =   3  'Align Left
      Height          =   2835
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   735
      _Version        =   65536
      _ExtentX        =   1296
      _ExtentY        =   5001
      _StockProps     =   15
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderWidth     =   1
      RoundedCorners  =   0   'False
      Alignment       =   1
      Begin Threed.SSCommand CMDCANCELAR 
         Height          =   615
         Left            =   60
         TabIndex        =   7
         Top             =   1380
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   1085
         _StockProps     =   78
         ForeColor       =   -2147483638
         BevelWidth      =   1
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FRMCADGE.frx":0008
      End
      Begin Threed.SSCommand CMDGRAVAR 
         Height          =   615
         Left            =   60
         TabIndex        =   6
         Top             =   60
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   1085
         _StockProps     =   78
         ForeColor       =   -2147483638
         BevelWidth      =   1
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FRMCADGE.frx":0322
      End
      Begin Threed.SSCommand CMDEXCLUIR 
         Height          =   615
         Left            =   60
         TabIndex        =   5
         Top             =   720
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   1085
         _StockProps     =   78
         ForeColor       =   -2147483638
         BevelWidth      =   1
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FRMCADGE.frx":0774
      End
      Begin Threed.SSCommand CMDVOLTAR 
         Height          =   615
         Left            =   60
         TabIndex        =   4
         Top             =   2160
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   1085
         _StockProps     =   78
         ForeColor       =   -2147483638
         BevelWidth      =   1
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FRMCADGE.frx":0A8E
      End
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   420
      Left            =   0
      TabIndex        =   8
      Top             =   2835
      Width           =   6870
      _ExtentX        =   12118
      _ExtentY        =   741
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   10.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "FRMCADGER"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub CBO_COD_SUPER_COORD_Click()
    S_SELECT1 = "SELECT A.E_MAIL " & _
                "FROM   CONTROLE_BASES.GERENCIA A " & _
                "WHERE  A.COD_GERENTE = " & Val(CBO_COD_SUPER_COORD)
    Set obj_checar2 = BANCO.DBCREATEDYNASET(S_SELECT1, 0&)
    If Not obj_checar2.EOF Then
        TXT_EMAIL = obj_checar2!E_MAIL
    Else
        TXT_EMAIL = ""
    End If
End Sub
Private Sub CMDCANCELAR_Click()
    CBO_COD_SUPER_COORD.ListIndex = -1
    CBO_FILIAL.ListIndex = -1
    TXT_EMAIL = ""
    CBO_COD_SUPER_COORD.SetFocus
End Sub
Private Sub CMDEXCLUIR_Click()
    Dim OBJ_CHECAR As Object
    Dim S_SELECT1
    Dim S_SQL1
    If CBO_COD_SUPER_COORD <> "" Then
        If CBO_FILIAL <> "" Then
            S_SELECT1 = "SELECT * " & _
                        "FROM   INTERNET.GERENTE " & _
                        "WHERE  COD_GERENTE = " & Val(CBO_COD_SUPER_COORD) & " AND " & _
                                "COD_FILIAL = " & Val(CBO_FILIAL)
            Set OBJ_CHECAR = BANCO.DBCREATEDYNASET(S_SELECT1, 0&)
            If OBJ_CHECAR.EOF Then
                MsgBox "Relacionamento n�o existe!"
                Exit Sub
            Else
                S_SQL1 = "DELETE INTERNET.GERENTE WHERE COD_GERENTE = " & Val(CBO_COD_SUPER_COORD) & " AND COD_FILIAL = " & Val(CBO_FILIAL)
                BANCO.DBEXECUTESQL S_SQL1
            End If
            S_SELECT1 = "SELECT * " & _
                        "FROM   CONTROLE_BASES.GERENCIA " & _
                        "WHERE  COD_GERENTE = " & Val(CBO_COD_SUPER_COORD)
            Set obj_checar2 = BANCO.DBCREATEDYNASET(S_SELECT1, 0&)
            If Not obj_checar2.EOF Then
                S_SQL1 = "DELETE CONTROLE_BASES.GERENCIA WHERE COD_GERENTE = " & Val(CBO_COD_SUPER_COORD)
                BANCO.DBEXECUTESQL S_SQL1
            End If
        Else
            MsgBox "Escolha a Filial!"
            Exit Sub
        End If
    Else
        MsgBox "Escolha o C�digo!"
        Exit Sub
    End If
    TXT_EMAIL = ""
    CBO_COD_SUPER_COORD.ListIndex = -1
    CBO_FILIAL.ListIndex = -1
    CBO_COD_SUPER_COORD = ""
    CBO_FILIAL = ""
    CBO_COD_SUPER_COORD.SetFocus
End Sub
Private Sub CMDGRAVAR_Click()
    Dim OBJ_CHECAR As Object
    Dim obj_checar2 As Object
    Dim OBJ_CIC As Object
    Dim S_SELECT1
    Dim S_SQL1
    If CBO_COD_SUPER_COORD <> "" Then
        If CBO_FILIAL <> "" Then
            S_SELECT1 = "SELECT * " & _
                        "FROM   INTERNET.GERENTE " & _
                        "WHERE  COD_GERENTE = " & Val(CBO_COD_SUPER_COORD) & " AND " & _
                                "COD_FILIAL = " & Val(CBO_FILIAL)
            Set OBJ_CHECAR = BANCO.DBCREATEDYNASET(S_SELECT1, 0&)
            If OBJ_CHECAR.EOF Then
                S_SQL1 = "INSERT INTO INTERNET.GERENTE VALUES(" & Val(CBO_COD_SUPER_COORD) & ", " & Val(CBO_FILIAL) & ")"
                BANCO.DBEXECUTESQL S_SQL1
            Else
                MsgBox "Relacionamento j� existe!"
                Exit Sub
            End If
            If TXT_EMAIL <> "" Then
                S_SELECT1 = "SELECT * " & _
                            "FROM   CONTROLE_BASES.GERENCIA " & _
                            "WHERE  COD_GERENTE = " & Val(CBO_COD_SUPER_COORD)
                Set obj_checar2 = BANCO.DBCREATEDYNASET(S_SELECT1, 0&)
                If obj_checar2.EOF Then
                    S_SELECT1 = "SELECT CIC " & _
                                "FROM   REPRESENTANTE " & _
                                "WHERE  COD_REPRES = " & Val(CBO_COD_SUPER_COORD)
                    Set OBJ_CIC = BANCO.DBCREATEDYNASET(S_SELECT1, 0&)
                    V_CIC = OBJ_CIC!Cic
                    S_SQL1 = "INSERT INTO CONTROLE_BASES.GERENCIA VALUES (" & Val(CBO_COD_SUPER_COORD) & ", " & V_CIC & ", '" & TXT_EMAIL & "')"
                    BANCO.DBEXECUTESQL S_SQL1
                'Else
                    'MsgBox "E-Mail j� cadastrado para este gerente!"
                    'Exit Sub
                End If
            End If
        Else
            MsgBox "Escolha a Filial!"
            Exit Sub
        End If
    Else
        MsgBox "Escolha o C�digo!"
        Exit Sub
    End If
    TXT_EMAIL = ""
    CBO_COD_SUPER_COORD.ListIndex = -1
    CBO_FILIAL.ListIndex = -1
    CBO_COD_SUPER_COORD = ""
    CBO_FILIAL = ""
    CBO_COD_SUPER_COORD.SetFocus
End Sub

Private Sub CMDVOLTAR_Click()
    Unload Me
End Sub
Private Sub Form_Load()
    Dim S_SELECT1
    Dim S_SELECT2
    Dim OBJ_FILIAL As Object
    Dim OBJ_COORD As Object
    Screen.MousePointer = vbHourglass
    CBO_FILIAL.Clear
    CBO_COD_SUPER_COORD.Clear
    StatusBar1.SimpleText = "Aguarde... Carregando Coordenadores/Supervisores..."
    S_SELECT1 = _
        "SELECT " & _
            "COD_FILIAL, " & _
            "NOME_FILIAL " & _
        "FROM " & _
            "FILIAL " & _
        "WHERE " & _
            "TP_FILIAL = 'R' " & _
        "ORDER BY " & _
            "COD_FILIAL"
    Set OBJ_FILIAL = BANCO.DBCREATEDYNASET(S_SELECT1, 0&)
    While Not OBJ_FILIAL.EOF
        CBO_FILIAL.AddItem OBJ_FILIAL!cod_filial & "-" & OBJ_FILIAL!NOME_FILIAL
        OBJ_FILIAL.MoveNext
    Wend
    StatusBar1.SimpleText = "Aguarde... Carregando Filiais..."
    S_SELECT2 = _
        "SELECT " & _
            "COD_REPRES, " & _
            "NOME_REPRES " & _
        "FROM " & _
            "REPRESENTANTE " & _
        "ORDER BY " & _
            "COD_REPRES"
    Set OBJ_COORD = BANCO.DBCREATEDYNASET(S_SELECT2, 0&)
    While Not OBJ_COORD.EOF
        CBO_COD_SUPER_COORD.AddItem OBJ_COORD!COD_REPRES & "-" & OBJ_COORD!NOME_REPRES
        OBJ_COORD.MoveNext
    Wend
    StatusBar1.SimpleText = ""
    Screen.MousePointer = vbDefault
End Sub

Private Sub TXT_EMAIL_LostFocus()
    N_VALIDAR_EMAIL = InStr(1, TXT_EMAIL, "@")
    If N_VALIDAR_EMAIL = 0 Then
        MsgBox "Email inv�lido!"
        TXT_EMAIL.SetFocus
        Exit Sub
    End If
End Sub
