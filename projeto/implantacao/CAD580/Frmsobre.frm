VERSION 5.00
Begin VB.Form FRMSOBRE 
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "CAD580 - Sobre"
   ClientHeight    =   3210
   ClientLeft      =   2565
   ClientTop       =   3135
   ClientWidth     =   5895
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   3210
   ScaleWidth      =   5895
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox Picture1 
      AutoSize        =   -1  'True
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Height          =   1380
      Left            =   2040
      Picture         =   "Frmsobre.frx":0000
      ScaleHeight     =   1380
      ScaleWidth      =   1650
      TabIndex        =   3
      Top             =   60
      Width           =   1650
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Height          =   375
      Left            =   2490
      TabIndex        =   0
      Top             =   2700
      Width           =   975
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "   Responsável:  Eduardo Relvas   Maxxipel (3728-8161)"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   495
      Left            =   1650
      TabIndex        =   2
      Top             =   2040
      Width           =   2745
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Cadastro de Supervisor/Coordenador"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   330
      Left            =   1200
      TabIndex        =   1
      Top             =   1560
      Width           =   3495
   End
End
Attribute VB_Name = "FRMSOBRE"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
    FRMSOBRE.Hide
End Sub


Private Sub Timer1_Timer()
    While LOGO.Left < 216
        LOGO.Left = LOGO.Left + 10
    Wend
    Timer2.Interval = 500
    Timer1.Interval = 0
    Timer3.Interval = 0
    Timer4.Interval = 0
End Sub


Private Sub Timer2_Timer()
    While SCR.Left > 1800
        SCR.Left = SCR.Left - 5
    Wend
    FERNANDO.Visible = True
    Timer3.Interval = 500
    Timer2.Interval = 0
    Timer1.Interval = 0
    Timer4.Interval = 0
End Sub


Private Sub Timer3_Timer()
    While FERNANDO.Top < 792
        FERNANDO.Top = FERNANDO.Top + 1
    Wend
    TEL.Visible = True
    Timer4.Interval = 500
    Timer3.Interval = 0
    Timer1.Interval = 0
    Timer2.Interval = 0
End Sub


Private Sub Timer4_Timer()
    While TEL.Top < 1080
        TEL.Top = TEL.Top + 1
    Wend
    Timer4.Interval = 0
    Timer1.Interval = 0
    Timer2.Interval = 0
    Timer3.Interval = 0
    Command1.Visible = True
    Command1.SetFocus
End Sub


Private Sub cmdOK_Click()
    Unload Me
End Sub


