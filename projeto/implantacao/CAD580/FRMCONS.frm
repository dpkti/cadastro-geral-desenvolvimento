VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Begin VB.Form FRMCONS 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "CAD580 - Consulta de Supervisor / Coordenador"
   ClientHeight    =   4980
   ClientLeft      =   1665
   ClientTop       =   1365
   ClientWidth     =   8355
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4980
   ScaleWidth      =   8355
   ShowInTaskbar   =   0   'False
   Begin VB.Timer Timer1 
      Interval        =   3000
      Left            =   720
      Top             =   4176
   End
   Begin Threed.SSPanel SSPanel1 
      Align           =   3  'Align Left
      Height          =   4575
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   735
      _Version        =   65536
      _ExtentX        =   1296
      _ExtentY        =   8070
      _StockProps     =   15
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderWidth     =   1
      RoundedCorners  =   0   'False
      Alignment       =   1
      Begin Threed.SSCommand CMDVOLTAR 
         Height          =   615
         Left            =   60
         TabIndex        =   1
         Top             =   60
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   1085
         _StockProps     =   78
         ForeColor       =   -2147483638
         BevelWidth      =   1
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FRMCONS.frx":0000
      End
   End
   Begin MSGrid.Grid Grid1 
      Height          =   4335
      Left            =   900
      TabIndex        =   0
      Top             =   120
      Width           =   7335
      _Version        =   65536
      _ExtentX        =   12938
      _ExtentY        =   7646
      _StockProps     =   77
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Rows            =   1
      Cols            =   4
      FixedRows       =   0
      FixedCols       =   0
      HighLight       =   0   'False
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   405
      Left            =   0
      TabIndex        =   2
      Top             =   4575
      Width           =   8355
      _ExtentX        =   14737
      _ExtentY        =   714
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   10.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "FRMCONS"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Sub CARREGA_GRID()
    Dim OBJ_GRID As Object
    Grid1.Rows = 1
    Grid1.Cols = 5
    Grid1.Row = 0
    Grid1.Col = 0
    Grid1.Text = "C�DIGO"
    Grid1.Col = 1
    Grid1.Text = "PSEUD�NIMO"
    Grid1.Col = 2
    Grid1.Text = "EMAIL"
    Grid1.Col = 3
    Grid1.Text = "SITUA��O"
    
    Grid1.Col = 4
    Grid1.Text = "FILIAL"
    
    Grid1.ColWidth(0) = 900     'C�DIGO
    Grid1.ColWidth(1) = 1500    'PSEUD�NIMO
    Grid1.ColWidth(2) = 3500    'EMAIL
    Grid1.ColWidth(3) = 1200    'SITUA��O
    Grid1.ColWidth(4) = 3500    'COD_FILIAL
    S_SELECT1 = "SELECT COD_SUPER_COORD," & _
                "       PSEUDONIMO," & _
                "       EMAIL," & _
                "       SITUACAO, A.COD_FILIAL || '-' || B.NOME_FILIAL FILIAL " & _
                "FROM   INTERNET.SUPER_COORD A, FILIAL B " & _
                "WHERE  A.COD_FILIAL = B.COD_FILIAL"
    Set OBJ_GRID = BANCO.DBCREATEDYNASET(S_SELECT1, 4&)
    While Not OBJ_GRID.EOF
        Grid1.Rows = Grid1.Rows + 1
        Grid1.Row = Grid1.Row + 1
        Grid1.Col = 0
        Grid1.Text = OBJ_GRID!COD_SUPER_COORD
        Grid1.Col = 1
        Grid1.Text = OBJ_GRID!PSEUDONIMO
        Grid1.Col = 2
        Grid1.Text = "" & OBJ_GRID!EMAIL
        Grid1.Col = 3
        If OBJ_GRID!SITUACAO = 0 Then
            Grid1.Text = "ATIVO"
        Else
            Grid1.Text = "DESATIVADO"
        End If
        Grid1.Col = 4
        Grid1.Text = "" & OBJ_GRID!Filial
        OBJ_GRID.MoveNext
    Wend
End Sub

Private Sub CMDVOLTAR_Click()
    FRMCONS.Hide
End Sub

Private Sub CMDVOLTAR_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    StatusBar1.SimpleText = "Voltar"
End Sub


Private Sub Form_Activate()
    CARREGA_GRID
End Sub

Private Sub Grid1_DblClick()
    If Grid1.Row <> 0 Then
        Grid1.Col = 0
        N_COD_SUPER_COORD = Grid1.Text
        FRMCADASTRO.TXT_COD_SUPER_COORD = N_COD_SUPER_COORD
        FRMCADASTRO.TXT_COD_SUPER_COORD_LostFocus
        FRMCONS.Hide
    End If
End Sub

Private Sub Timer1_Timer()
    StatusBar1.SimpleText = ""
End Sub


