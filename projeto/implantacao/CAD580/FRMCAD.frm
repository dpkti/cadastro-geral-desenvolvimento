VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FRMCADASTRO 
   Caption         =   "CAD580 - Cadastro de Supervisor / Coordenador"
   ClientHeight    =   5175
   ClientLeft      =   3720
   ClientTop       =   3645
   ClientWidth     =   7830
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5175
   ScaleWidth      =   7830
   Begin VB.Frame Frame1 
      Height          =   4425
      Left            =   840
      TabIndex        =   21
      Top             =   0
      Width           =   6135
      Begin VB.ComboBox cboFilial 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "FRMCAD.frx":0000
         Left            =   1320
         List            =   "FRMCAD.frx":0002
         Style           =   2  'Dropdown List
         TabIndex        =   36
         Top             =   3900
         Width           =   4695
      End
      Begin VB.TextBox TXT_NOME 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1320
         MaxLength       =   40
         TabIndex        =   1
         Top             =   660
         Width           =   4635
      End
      Begin VB.ComboBox CBO_CIDADE 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "FRMCAD.frx":0004
         Left            =   1320
         List            =   "FRMCAD.frx":0006
         TabIndex        =   5
         Top             =   2280
         Width           =   2655
      End
      Begin VB.TextBox TXT_SITUACAO 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3000
         MaxLength       =   1
         TabIndex        =   12
         Text            =   "0"
         Top             =   3480
         Width           =   315
      End
      Begin VB.TextBox TXT_DIVISAO 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1320
         MaxLength       =   1
         TabIndex        =   11
         Text            =   "D"
         Top             =   3480
         Width           =   315
      End
      Begin VB.TextBox TXT_EMAIL 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   315
         Left            =   1320
         MaxLength       =   50
         TabIndex        =   10
         Top             =   3060
         Width           =   4665
      End
      Begin VB.TextBox TXT_CELULAR 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4500
         MaxLength       =   13
         TabIndex        =   9
         Top             =   2640
         Width           =   1485
      End
      Begin VB.TextBox TXT_FONE 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2070
         MaxLength       =   9
         TabIndex        =   8
         Top             =   2640
         Width           =   1215
      End
      Begin VB.TextBox TXT_DDD 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1320
         MaxLength       =   4
         TabIndex        =   7
         Top             =   2640
         Width           =   735
      End
      Begin VB.TextBox TXT_CEP 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4890
         MaxLength       =   8
         TabIndex        =   6
         Top             =   2220
         Width           =   1095
      End
      Begin VB.TextBox TXT_BAIRRO 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1320
         MaxLength       =   20
         TabIndex        =   4
         Top             =   1860
         Width           =   2535
      End
      Begin VB.TextBox TXT_ENDERECO 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1320
         MaxLength       =   40
         TabIndex        =   3
         Top             =   1440
         Width           =   4665
      End
      Begin VB.TextBox TXT_PSEUDONIMO 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1320
         MaxLength       =   15
         TabIndex        =   2
         Top             =   1020
         Width           =   1935
      End
      Begin VB.TextBox TXT_CIC 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4140
         MaxLength       =   14
         TabIndex        =   0
         Top             =   240
         Width           =   1815
      End
      Begin VB.TextBox TXT_COD_SUPER_COORD 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1320
         MaxLength       =   6
         TabIndex        =   15
         Top             =   240
         Width           =   855
      End
      Begin Threed.SSCommand CMDATIVA_DESATIVA 
         Height          =   315
         Left            =   4860
         TabIndex        =   13
         Top             =   3450
         Width           =   1155
         _Version        =   65536
         _ExtentX        =   2037
         _ExtentY        =   556
         _StockProps     =   78
         Caption         =   "&Desativar"
         ForeColor       =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BevelWidth      =   1
         RoundedCorners  =   0   'False
         Outline         =   0   'False
      End
      Begin VB.Label Label15 
         Caption         =   "Filial"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   150
         TabIndex        =   37
         Top             =   3960
         Width           =   735
      End
      Begin VB.Label Label14 
         Caption         =   "Situa��o"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2100
         TabIndex        =   35
         Top             =   3540
         Width           =   915
      End
      Begin VB.Label Label13 
         Caption         =   "Divis�o"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   34
         Top             =   3540
         Width           =   735
      End
      Begin VB.Label Label12 
         Caption         =   "E-mail"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   33
         Top             =   3120
         Width           =   675
      End
      Begin VB.Label Label11 
         Caption         =   "Celular"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   3780
         TabIndex        =   32
         Top             =   2700
         Width           =   735
      End
      Begin VB.Label Label10 
         Caption         =   "Fone"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   1440
         TabIndex        =   31
         Top             =   2700
         Width           =   555
      End
      Begin VB.Label Label9 
         Caption         =   "DDD"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   30
         Top             =   2700
         Width           =   435
      End
      Begin VB.Label Label8 
         Caption         =   "CEP"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   4410
         TabIndex        =   29
         Top             =   2280
         Width           =   435
      End
      Begin VB.Label Label7 
         Caption         =   "Cidade"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   28
         Top             =   2340
         Width           =   735
      End
      Begin VB.Label Label6 
         Caption         =   "Bairro"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   27
         Top             =   1920
         Width           =   615
      End
      Begin VB.Label Label5 
         Caption         =   "Endere�o"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   26
         Top             =   1500
         Width           =   975
      End
      Begin VB.Label Label4 
         Caption         =   "Pseud�nimo"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   25
         Top             =   1080
         Width           =   1215
      End
      Begin VB.Label Label3 
         Caption         =   "Nome"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   24
         Top             =   720
         Width           =   615
      End
      Begin VB.Label Label2 
         Caption         =   "CIC"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   3690
         TabIndex        =   23
         Top             =   300
         Width           =   435
      End
      Begin VB.Label Label1 
         Caption         =   "C�digo"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   22
         Top             =   300
         Width           =   735
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   3000
      Left            =   720
      Top             =   3420
   End
   Begin Threed.SSPanel SSPanel1 
      Align           =   3  'Align Left
      Height          =   4755
      Left            =   0
      TabIndex        =   19
      Top             =   0
      Width           =   735
      _Version        =   65536
      _ExtentX        =   1296
      _ExtentY        =   8387
      _StockProps     =   15
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderWidth     =   1
      RoundedCorners  =   0   'False
      Alignment       =   1
      Begin Threed.SSCommand CMDVOLTAR 
         Height          =   615
         Left            =   60
         TabIndex        =   18
         Top             =   2160
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   1085
         _StockProps     =   78
         ForeColor       =   -2147483638
         BevelWidth      =   1
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FRMCAD.frx":0008
      End
      Begin Threed.SSCommand CMDEXCLUIR 
         Height          =   615
         Left            =   60
         TabIndex        =   16
         Top             =   720
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   1085
         _StockProps     =   78
         ForeColor       =   -2147483638
         BevelWidth      =   1
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FRMCAD.frx":0322
      End
      Begin Threed.SSCommand CMDGRAVAR 
         Height          =   615
         Left            =   60
         TabIndex        =   14
         Top             =   60
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   1085
         _StockProps     =   78
         ForeColor       =   -2147483638
         BevelWidth      =   1
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FRMCAD.frx":063C
      End
      Begin Threed.SSCommand CMDCANCELAR 
         Height          =   615
         Left            =   60
         TabIndex        =   17
         Top             =   1380
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   1085
         _StockProps     =   78
         ForeColor       =   -2147483638
         BevelWidth      =   1
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FRMCAD.frx":0A8E
      End
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   420
      Left            =   0
      TabIndex        =   20
      Top             =   4755
      Width           =   7830
      _ExtentX        =   13811
      _ExtentY        =   741
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   10.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "FRMCADASTRO"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Sub CARREGAR_CIDADES()
    Dim OBJ_CIDADE As Object
    CBO_CIDADE.Clear
    S_SELECT1 = "SELECT NOME_CIDADE, " & _
                "       COD_UF " & _
                "FROM   CIDADE " & _
                "ORDER BY NOME_CIDADE"
    Set OBJ_CIDADE = BANCO.DBCREATEDYNASET(S_SELECT1, 0&)
    While Not OBJ_CIDADE.EOF
        CBO_CIDADE.AddItem OBJ_CIDADE!NOME_CIDADE & "-" & OBJ_CIDADE!COD_UF
        OBJ_CIDADE.MoveNext
    Wend
End Sub

Sub GERAR_CODIGO()
    Dim OBJ_CODIGO As Object
    S_SELECT2 = "SELECT NVL(MAX(COD_SUPER_COORD),0) COD_SUPER_COORD " & _
                "FROM   INTERNET.SUPER_COORD"
    Set OBJ_CODIGO = BANCO.DBCREATEDYNASET(S_SELECT2, 4&)
    TXT_COD_SUPER_COORD = OBJ_CODIGO!COD_SUPER_COORD + 1
End Sub

Sub HABILITA_ALTERACAO()
    TXT_COD_SUPER_COORD.Enabled = False
    TXT_CIC.Enabled = False
End Sub

Sub HABILITA_INCLUSAO()
    TXT_COD_SUPER_COORD.Enabled = True
    TXT_CIC.Enabled = True
End Sub

Sub LIMPA_CAMPOS()
    TXT_COD_SUPER_COORD = ""
    TXT_CIC = ""
    TXT_NOME.Text = ""
    TXT_PSEUDONIMO = ""
    TXT_ENDERECO = ""
    TXT_BAIRRO = ""
    CBO_CIDADE = ""
    CBO_CIDADE.ListIndex = -1
    TXT_CEP = ""
    TXT_DDD = ""
    TXT_FONE = ""
    TXT_CELULAR = ""
    TXT_EMAIL = ""
    TXT_DIVISAO = "D"
    TXT_SITUACAO = 0
    CMDATIVA_DESATIVA.Caption = "&Desativar"
    cboFilial.ListIndex = -1
End Sub

Private Sub CBO_CIDADE_GotFocus()
    Dim OBJ_CIDADE As Object
    S_ESTADO = InputBox("Digite as iniciais do Estado", "Estado")
    StatusBar1.SimpleText = "Carregando cidades..."
    Me.MousePointer = 11
    Screen.MousePointer = vbHourglass
    CBO_CIDADE.Clear
    StatusBar1.SimpleText = "Aguarde... Carregando cidades..."
    S_SELECT1 = "SELECT NOME_CIDADE, " & _
                "       COD_UF " & _
                "FROM   CIDADE " & _
                "WHERE  COD_UF = '" & S_ESTADO & "' " & _
                "ORDER BY NOME_CIDADE"
    Set OBJ_CIDADE = BANCO.DBCREATEDYNASET(S_SELECT1, 0&)
    While Not OBJ_CIDADE.EOF
        CBO_CIDADE.AddItem OBJ_CIDADE!NOME_CIDADE & "-" & OBJ_CIDADE!COD_UF
        OBJ_CIDADE.MoveNext
    Wend
    StatusBar1.SimpleText = ""
    Me.MousePointer = 0
    Screen.MousePointer = vbDefault
    StatusBar1.SimpleText = ""
End Sub

Private Sub CBO_CIDADE_LostFocus()
    Dim OBJ_COD_CIDADE
    If CBO_CIDADE <> "" Then
        N_POSICAO = InStr(1, CBO_CIDADE, "-")
        S_NOME_CIDADE = Mid(CBO_CIDADE, 1, N_POSICAO - 1)
        S_SELECT1 = "SELECT COD_CIDADE " & _
                    "FROM   CIDADE " & _
                    "WHERE  NOME_CIDADE = '" & S_NOME_CIDADE & "'"
        Set OBJ_COD_CIDADE = BANCO.DBCREATEDYNASET(S_SELECT1, 0&)
        N_COD_CIDADE = OBJ_COD_CIDADE!COD_CIDADE
    End If
End Sub


Private Sub CMDATIVA_DESATIVA_Click()
On Error GoTo ERRO3
    If TXT_NOME <> "" Then
        If TXT_SITUACAO = 0 Then
            TXT_SITUACAO = 9
            CMDATIVA_DESATIVA.Caption = "&Ativar"
        Else
            TXT_SITUACAO = 0
            CMDATIVA_DESATIVA.Caption = "&Desativar"
        End If
        S_SQL1 = "UPDATE INTERNET.SUPER_COORD " & _
                 "SET    SITUACAO = " & TXT_SITUACAO & _
                 "WHERE  COD_SUPER_COORD = " & TXT_COD_SUPER_COORD
        BANCO.ExecuteSQL S_SQL1
    End If
ERRO3:
    If Err <> 0 Then
        MsgBox "Erro: " & Err & " - " & Error(Err) & Chr(13) & "Favor avisar analista respons�vel"
    End If
    Exit Sub
End Sub

Private Sub CMDATIVA_DESATIVA_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    StatusBar1.SimpleText = "Ativa/Desativa Supervisor / Coordenador"
End Sub


Private Sub CMDCANCELAR_Click()
    LIMPA_CAMPOS
    HABILITA_INCLUSAO
    TXT_COD_SUPER_COORD.SetFocus
    GERAR_CODIGO
    TXT_CIC.SetFocus
End Sub

Private Sub CMDCANCELAR_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    StatusBar1.SimpleText = "Cancelar"
End Sub


Private Sub CMDEXCLUIR_Click()
On Error GoTo ERRO2
    Dim OBJ_CHECAR As Object
    If EXISTE = 1 Then
        A = MsgBox("Confirma Exclus�o?", 36, "CAD580")
        If A = 6 Then
            S_SELECT1 = "SELECT * " & _
                        "FROM   INTERNET.R_FIL_SUPER_COORD " & _
                        "WHERE  COD_SUPER_COORD = " & TXT_COD_SUPER_COORD
            Set OBJ_CHECAR = BANCO.DBCREATEDYNASET(S_SELECT1, 4&)
            If OBJ_CHECAR.RecordCount <> 0 Then
                S_SQL1 = "DELETE INTERNET.R_FIL_SUPER_COORD " & _
                         "WHERE  COD_SUPER_COORD = " & TXT_COD_SUPER_COORD
                BANCO.ExecuteSQL S_SQL1
            End If
            S_SQL2 = "DELETE INTERNET.SUPER_COORD " & _
                     "WHERE  COD_SUPER_COORD = " & TXT_COD_SUPER_COORD
            BANCO.ExecuteSQL S_SQL2
            LIMPA_CAMPOS
            HABILITA_INCLUSAO
            TXT_COD_SUPER_COORD.SetFocus
            GERAR_CODIGO
            TXT_CIC.SetFocus
        End If
    End If
ERRO2:
    If Err <> 0 Then
        MsgBox "Erro: " & Err & " - " & Error(Err) & Chr(13) & "Favor avisar analista respons�vel"
    End If
    Exit Sub
End Sub

Private Sub CMDEXCLUIR_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    StatusBar1.SimpleText = "Excluir"
End Sub


Private Sub CMDGRAVAR_Click()
On Error GoTo ERRO1
    If TXT_NOME = "" Then
        MsgBox "Digite o NOME!"
        TXT_NOME.SetFocus
        Exit Sub
    End If
    If TXT_ENDERECO = "" Then
        MsgBox "Digite o ENDERE�O!"
        TXT_ENDERECO.SetFocus
        Exit Sub
    End If
    If CBO_CIDADE = "" Then
        MsgBox "Escolha a CIDADE!"
        CBO_CIDADE.SetFocus
        Exit Sub
    End If
    If EXISTE = 0 Then
        A = MsgBox("Confirma Inclus�o?", 36, "CAD580")
        If A = 6 Then
            S_SQL1 = "INSERT INTO INTERNET.SUPER_COORD " & _
                     "VALUES(" & TXT_COD_SUPER_COORD & ", " & _
                     IIf(TXT_CIC = "", 0, TXT_CIC) & ", " & _
                     "'" & TXT_NOME & "', " & _
                     "'" & TXT_PSEUDONIMO & "', " & _
                     "'" & TXT_ENDERECO & "', " & _
                     "'" & TXT_BAIRRO & "', " & _
                     N_COD_CIDADE & ", " & _
                     IIf(TXT_CEP = "", 0, TXT_CEP) & ", " & _
                     IIf(TXT_DDD = "", 0, TXT_DDD) & ", "
            S_SQL1 = S_SQL1 & IIf(TXT_FONE = "", 0, TXT_FONE) & ", " & _
                              IIf(TXT_CELULAR = "", 0, TXT_CELULAR) & ", " & _
                              "'" & TXT_EMAIL & "', " & _
                              "'" & TXT_DIVISAO & "', " & _
                              TXT_SITUACAO & "," & _
                              cboFilial.ItemData(cboFilial.ListIndex) & ")"
        End If
    Else
        A = MsgBox("Confirma Altera��o?", 36, "CAD580")
        If A = 6 Then
            S_SQL1 = "UPDATE INTERNET.SUPER_COORD " & _
                     "SET NOME = '" & TXT_NOME & "', " & _
                     "PSEUDONIMO = '" & TXT_PSEUDONIMO & "', " & _
                     "ENDERECO = '" & TXT_ENDERECO & "', " & _
                     "BAIRRO = '" & TXT_BAIRRO & "', " & _
                     "COD_CIDADE = " & N_COD_CIDADE & ", " & _
                     "CEP = " & IIf(TXT_CEP = "", 0, TXT_CEP) & ", " & _
                     "DDD = " & IIf(TXT_DDD = "", 0, TXT_DDD) & ", " & _
                     "FONE = " & IIf(TXT_FONE = "", 0, TXT_FONE) & ", " & _
                     "CELULAR = " & IIf(TXT_CELULAR = "", 0, TXT_CELULAR) & ", "
            S_SQL1 = S_SQL1 & "EMAIL = '" & TXT_EMAIL & "', " & _
                     "COD_FILIAL = " & cboFilial.ItemData(cboFilial.ListIndex) & _
                              " WHERE COD_SUPER_COORD = " & TXT_COD_SUPER_COORD
        End If
    End If
    BANCO.ExecuteSQL S_SQL1
    LIMPA_CAMPOS
    HABILITA_INCLUSAO
    TXT_COD_SUPER_COORD.SetFocus
    GERAR_CODIGO
    TXT_CIC.SetFocus
ERRO1:
    If Err <> 0 Then
        MsgBox "Erro: " & Err & " - " & Error(Err) & Chr(13) & "Favor avisar analista respons�vel"
    End If
    Exit Sub
End Sub
Private Sub CMDGRAVAR_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    StatusBar1.SimpleText = "Gravar"
End Sub


Private Sub CMDVOLTAR_Click()
    FRMCADASTRO.Hide
End Sub

Private Sub CMDVOLTAR_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    StatusBar1.SimpleText = "Voltar"
End Sub


Private Sub Form_Activate()
    If TXT_CIC.Enabled Then
        TXT_CIC.SetFocus
    Else
        TXT_NOME.SetFocus
    End If
End Sub

Private Sub Form_Load()
'    CARREGAR_CIDADES
    GERAR_CODIGO
    Preencher_Combo_Filial
End Sub
Private Sub Timer1_Timer()
    StatusBar1.SimpleText = ""
End Sub


Function F_CIC(llCic)
    Dim Cic As String
    Dim liContador As Integer
    Dim s_cic, j_cic, r_cic, d_cic
    F_CIC = True

    Cic = Format(llCic, "00000000000") '11
    s_cic = 0
    j_cic = 2
    For liContador = 9 To 1 Step -1
        s_cic = s_cic + Val(Mid(Cic, liContador, 1)) * j_cic
        j_cic = j_cic + 1
    Next
    r_cic = s_cic Mod 11
    If r_cic = 0 Or r_cic = 1 Then
        d_cic = 0
    Else
        d_cic = 11 - r_cic
    End If
    If d_cic <> Val(Mid(Cic, 10, 1)) Then
        F_CIC = False
        Exit Function
    End If
    s_cic = 0
    j_cic = 2
    For liContador = 10 To 2 Step -1
        s_cic = s_cic + Val(Mid(Cic, liContador, 1)) * j_cic
        j_cic = j_cic + 1
    Next
    r_cic = s_cic Mod 11
    If r_cic = 0 Or r_cic = 1 Then
        d_cic = 0
    Else
        d_cic = 11 - r_cic
    End If

    If d_cic <> Val(Mid(Cic, 11, 1)) Then
        F_CIC = False
        Exit Function
    End If
End Function

Private Sub TXT_BAIRRO_Change()
    If TXT_BAIRRO <> "" Then
        N_TAM = Len(TXT_BAIRRO)
        S_LETRA = Mid(TXT_BAIRRO, N_TAM, N_TAM)
        If S_LETRA <> "" Then
            If (Asc(S_LETRA) <> 32 And Asc(S_LETRA) < 65) Or (Asc(S_LETRA) > 90 And Asc(S_LETRA) <> 199) Then
                MsgBox ("Digite somente LETRA em mai�sculo!")
                SendKeys "{BACKSPACE}"
                Exit Sub
            End If
        End If
    End If
End Sub

Private Sub TXT_CELULAR_Change()
    If TXT_CELULAR <> "" Then
        If Not IsNumeric(TXT_CELULAR) Then
            MsgBox ("Digite somente N�MERO!")
            SendKeys "{BACKSPACE}"
            Exit Sub
        End If
    End If
End Sub

Private Sub TXT_CEP_Change()
    If TXT_CEP <> "" Then
        If Not IsNumeric(TXT_CEP) Then
            MsgBox ("Digite somente N�MERO!")
            SendKeys "{BACKSPACE}"
            Exit Sub
        End If
    End If
End Sub

Private Sub TXT_CIC_Change()
    If TXT_CIC <> "" Then
        If Not IsNumeric(TXT_CIC) Then
            MsgBox ("Digite somente N�MERO!")
            SendKeys "{BACKSPACE}"
            Exit Sub
        End If
    End If
End Sub

Private Sub TXT_CIC_LostFocus()
    If Not F_CIC(TXT_CIC) Then
        MsgBox "CIC Inv�lido!"
        TXT_CIC.SetFocus
        TXT_CIC = ""
        Exit Sub
    End If
End Sub

Private Sub TXT_COD_SUPER_COORD_Change()
    If TXT_COD_SUPER_COORD <> "" Then
        If Not IsNumeric(TXT_COD_SUPER_COORD) Then
            SendKeys "{BACKSPACE}"
            Exit Sub
        End If
    End If
End Sub

Private Sub TXT_COD_SUPER_COORD_KeyPress(KeyAscii As Integer)
    If KeyAscii = 9 Then
        TXT_COD_SUPER_COORD.TabIndex = 0
        TXT_CIC.SetFocus
        TXT_COD_SUPER_COORD.TabIndex = 14
    End If
End Sub


Public Sub TXT_COD_SUPER_COORD_LostFocus()
    Dim OBJ_CHECAR_CIDADE As Object
    Dim OBJ_CHECAR As Object
    If TXT_COD_SUPER_COORD <> "" Then
        S_SELECT1 = "SELECT * " & _
                    "FROM   INTERNET.SUPER_COORD " & _
                    "WHERE  COD_SUPER_COORD = " & TXT_COD_SUPER_COORD
        Set OBJ_CHECAR = BANCO.DBCREATEDYNASET(S_SELECT1, 0&)
        If OBJ_CHECAR.EOF Then
            LIMPA_CAMPOS
            GERAR_CODIGO
            HABILITA_INCLUSAO
            EXISTE = 0
            TXT_CIC.SetFocus
        Else
            TXT_CIC = OBJ_CHECAR!Cic
            TXT_NOME = "" & OBJ_CHECAR!NOME
            TXT_PSEUDONIMO = "" & OBJ_CHECAR!PSEUDONIMO
            TXT_ENDERECO = "" & OBJ_CHECAR!ENDERECO
            TXT_BAIRRO = "" & OBJ_CHECAR!BAIRRO
            S_SELECT1 = "SELECT NOME_CIDADE, " & _
                        "       COD_UF " & _
                        "FROM   CIDADE " & _
                        "WHERE  COD_CIDADE = " & OBJ_CHECAR!COD_CIDADE
            Set OBJ_CHECAR_CIDADE = BANCO.DBCREATEDYNASET(S_SELECT1, 0&)
            CBO_CIDADE = OBJ_CHECAR_CIDADE!NOME_CIDADE & "-" & OBJ_CHECAR_CIDADE!COD_UF
            If CBO_CIDADE <> "" Then
                N_POSICAO = InStr(1, CBO_CIDADE, "-")
                S_NOME_CIDADE = Mid(CBO_CIDADE, 1, N_POSICAO - 1)
                S_SELECT1 = "SELECT COD_CIDADE " & _
                            "FROM   CIDADE " & _
                            "WHERE  NOME_CIDADE = '" & S_NOME_CIDADE & "'"
                Set OBJ_COD_CIDADE = BANCO.DBCREATEDYNASET(S_SELECT1, 0&)
                N_COD_CIDADE = OBJ_COD_CIDADE!COD_CIDADE
            End If
            TXT_CEP = OBJ_CHECAR!CEP
            TXT_DDD = OBJ_CHECAR!DDD
            TXT_FONE = OBJ_CHECAR!FONE
            TXT_CELULAR = OBJ_CHECAR!CELULAR
            If OBJ_CHECAR!EMAIL <> "" Then
                TXT_EMAIL = OBJ_CHECAR!EMAIL
            End If
            TXT_SITUACAO = OBJ_CHECAR!SITUACAO
            If TXT_SITUACAO = 0 Then
                CMDATIVA_DESATIVA.Caption = "&Desativar"
            Else
                CMDATIVA_DESATIVA.Caption = "&Ativar"
            End If
            For i = 0 To Me.cboFilial.ListCount - 1
                If cboFilial.ItemData(i) = OBJ_CHECAR!cod_filial Then
                    Me.cboFilial.ListIndex = i
                    Exit For
                End If
            Next
            HABILITA_ALTERACAO
            EXISTE = 1
        End If
    End If
End Sub

Private Sub TXT_DDD_Change()
    If TXT_DDD <> "" Then
        If Not IsNumeric(TXT_DDD) Then
            MsgBox ("Digite somente N�MERO!")
            SendKeys "{BACKSPACE}"
            Exit Sub
        End If
    End If
End Sub

Private Sub TXT_EMAIL_LostFocus()
    n_validar_email = InStr(1, TXT_EMAIL, "@")
    If n_validar_email = 0 Then
        MsgBox "Email inv�lido!"
        TXT_EMAIL.SetFocus
        Exit Sub
    End If
End Sub
Private Sub TXT_ENDERECO_Change()
    If TXT_ENDERECO <> "" Then
        N_TAM = Len(TXT_ENDERECO)
        S_LETRA = Mid(TXT_ENDERECO, N_TAM, N_TAM)
        If S_LETRA <> "" Then
            If Asc(S_LETRA) < 48 Or Asc(S_LETRA) > 57 Then
                If Asc(S_LETRA) <> 32 Then
                    If Asc(S_LETRA) < 65 Or Asc(S_LETRA) > 90 Then
                        If Asc(S_LETRA) <> 199 Then
                            MsgBox ("Digite somente LETRA em mai�sculo!")
                            SendKeys "{BACKSPACE}"
                            Exit Sub
                        End If
                    End If
                End If
            End If
        End If
    End If
End Sub

Private Sub TXT_FONE_Change()
    If TXT_FONE <> "" Then
        If Not IsNumeric(TXT_FONE) Then
            MsgBox ("Digite somente N�MERO!")
            SendKeys "{BACKSPACE}"
            Exit Sub
        End If
    End If
End Sub

Private Sub TXT_NOME_Change()
    If TXT_NOME <> "" Then
        N_TAM = Len(TXT_NOME)
        S_LETRA = Mid(TXT_NOME, N_TAM, N_TAM)
        If S_LETRA <> "" Then
            If (Asc(S_LETRA) <> 32 And Asc(S_LETRA) < 65) Or (Asc(S_LETRA) > 90 And Asc(S_LETRA) <> 199) Then
                MsgBox ("Digite somente LETRA em mai�sculo!")
                SendKeys "{BACKSPACE}"
                Exit Sub
            End If
        End If
    End If
End Sub

Private Sub TXT_NOME_DblClick()
    FRMCONS.Show 1
End Sub


Private Sub TXT_PSEUDONIMO_Change()
    If TXT_PSEUDONIMO <> "" Then
        N_TAM = Len(TXT_PSEUDONIMO)
        S_LETRA = Mid(TXT_PSEUDONIMO, N_TAM, N_TAM)
        If S_LETRA <> "" Then
            If (Asc(S_LETRA) <> 32 And Asc(S_LETRA) < 65) Or (Asc(S_LETRA) > 90 And Asc(S_LETRA) <> 199) Then
                                MsgBox ("Digite somente LETRA em mai�sculo!")
                SendKeys "{BACKSPACE}"
                Exit Sub
            End If
        End If
    End If
End Sub


Sub Preencher_Combo_Filial()
    Dim vSql As String
    Dim vRst As Object
    vSql = "select Cod_filial, to_char(COD_FILIAL,'0000') || ' - ' || Nome_Filial as FILIAL from filial where divisao='D' order by Nome_Filial"
    Set vRst = BANCO.DBCREATEDYNASET(vSql, 0&)
    
    For i = 1 To vRst.RecordCount
        cboFilial.AddItem vRst!Filial
        cboFilial.ItemData(cboFilial.NewIndex) = vRst!cod_filial
        vRst.MoveNext
    Next
    vRst.Close
    Set vRst = Nothing
End Sub
