VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FRMRELACIONAMENTO 
   Caption         =   "CAD580 - Relacionamento"
   ClientHeight    =   8820
   ClientLeft      =   1335
   ClientTop       =   1755
   ClientWidth     =   8880
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8820
   ScaleWidth      =   8880
   Begin VB.ListBox lstFiliaisAtribuidas 
      Height          =   2205
      Left            =   1680
      Sorted          =   -1  'True
      TabIndex        =   16
      Top             =   5640
      Width           =   6375
   End
   Begin VB.ListBox lstFiliaisDisponiveis 
      Height          =   2205
      ItemData        =   "FRMREL.frx":0000
      Left            =   1680
      List            =   "FRMREL.frx":0007
      Sorted          =   -1  'True
      TabIndex        =   15
      Top             =   1800
      Width           =   6375
   End
   Begin VB.Frame Frame2 
      Height          =   975
      Left            =   3840
      TabIndex        =   12
      Top             =   4320
      Width           =   1935
      Begin Threed.SSCommand cmdRetirar 
         Height          =   615
         Left            =   1080
         TabIndex        =   14
         Top             =   240
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   1085
         _StockProps     =   78
         ForeColor       =   -2147483638
         BevelWidth      =   1
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FRMREL.frx":0022
      End
      Begin Threed.SSCommand cmdEnviar 
         Height          =   615
         Left            =   240
         TabIndex        =   13
         Top             =   240
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   1085
         _StockProps     =   78
         ForeColor       =   -2147483638
         BevelWidth      =   1
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FRMREL.frx":033C
      End
   End
   Begin VB.Frame Frame1 
      Height          =   735
      Left            =   1680
      TabIndex        =   7
      Top             =   120
      Width           =   6375
      Begin VB.ComboBox CBO_COD_SUPER_COORD 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "FRMREL.frx":0656
         Left            =   960
         List            =   "FRMREL.frx":0658
         TabIndex        =   0
         Top             =   240
         Width           =   4815
      End
      Begin VB.Label Label1 
         Caption         =   "C�digo"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   300
         Width           =   735
      End
   End
   Begin Threed.SSPanel SSPanel1 
      Align           =   3  'Align Left
      Height          =   8400
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   14817
      _StockProps     =   15
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderWidth     =   1
      RoundedCorners  =   0   'False
      Alignment       =   1
      Begin Threed.SSCommand CMDCANCELAR 
         Height          =   615
         Left            =   120
         TabIndex        =   3
         Top             =   120
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   1085
         _StockProps     =   78
         ForeColor       =   -2147483638
         BevelWidth      =   1
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FRMREL.frx":065A
      End
      Begin Threed.SSCommand CMDGRAVAR 
         Height          =   615
         Left            =   120
         TabIndex        =   1
         Top             =   6360
         Visible         =   0   'False
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   1085
         _StockProps     =   78
         ForeColor       =   -2147483638
         BevelWidth      =   1
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FRMREL.frx":0974
      End
      Begin Threed.SSCommand CMDEXCLUIR 
         Height          =   615
         Left            =   120
         TabIndex        =   2
         Top             =   7080
         Visible         =   0   'False
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   1085
         _StockProps     =   78
         ForeColor       =   -2147483638
         BevelWidth      =   1
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FRMREL.frx":0DC6
      End
      Begin Threed.SSCommand CMDVOLTAR 
         Height          =   615
         Left            =   120
         TabIndex        =   4
         Top             =   960
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   1085
         _StockProps     =   78
         ForeColor       =   -2147483638
         BevelWidth      =   1
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FRMREL.frx":10E0
      End
   End
   Begin Threed.SSCommand SSCommand3 
      Height          =   615
      Left            =   0
      TabIndex        =   11
      Top             =   0
      Width           =   615
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   1085
      _StockProps     =   78
      ForeColor       =   -2147483638
      BevelWidth      =   1
      RoundedCorners  =   0   'False
      Outline         =   0   'False
      Picture         =   "FRMREL.frx":13FA
   End
   Begin Threed.SSCommand SSCommand2 
      Height          =   615
      Left            =   0
      TabIndex        =   10
      Top             =   0
      Width           =   615
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   1085
      _StockProps     =   78
      ForeColor       =   -2147483638
      BevelWidth      =   1
      RoundedCorners  =   0   'False
      Outline         =   0   'False
      Picture         =   "FRMREL.frx":184C
   End
   Begin Threed.SSCommand SSCommand1 
      Height          =   615
      Left            =   0
      TabIndex        =   9
      Top             =   0
      Width           =   615
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   1085
      _StockProps     =   78
      ForeColor       =   -2147483638
      BevelWidth      =   1
      RoundedCorners  =   0   'False
      Outline         =   0   'False
      Picture         =   "FRMREL.frx":1C9E
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   420
      Left            =   0
      TabIndex        =   6
      Top             =   8400
      Width           =   8880
      _ExtentX        =   15663
      _ExtentY        =   741
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   10.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label3 
      Caption         =   "Filiais Atribu�das"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1680
      TabIndex        =   18
      Top             =   5280
      Width           =   1935
   End
   Begin VB.Label Label2 
      Caption         =   "Filiais Dispon�veis"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1680
      TabIndex        =   17
      Top             =   1440
      Width           =   1935
   End
End
Attribute VB_Name = "FRMRELACIONAMENTO"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Private Sub CBO_COD_SUPER_COORD_Click()
    Dim VOBJ1 As Object
    Dim VOBJ2 As Object
    Dim S_SELECT1 As String
    Dim S_SELECT2 As String
    Dim S_SQL1 As String
    Dim i As Integer
    Dim j As Integer
    
    'SELECT 1 CONTEM AS INFORMA��ES SOBRE A LISTA DE FILIAIS ATRIBU�DAS
    S_SELECT1 = "select i.cod_filial, f.nome_filial " & _
                "from internet.r_fil_super_coord i, filial f " & _
                "Where i.cod_filial = f.cod_filial " & _
                "and cod_super_coord = " & Val(CBO_COD_SUPER_COORD) & _
                "order by i.cod_filial"
    
    'SELECT 2 CONTEM AS INFORMA��ES SOBRE A LISTA DE FILIAIS DISPON�VEIS
    S_SELECT2 = "select cod_filial, nome_filial " & _
                "from filial " & _
                "where cod_filial not in (" & _
                "select cod_filial " & _
                "from internet.r_fil_super_coord " & _
                "where cod_super_coord = " & Val(CBO_COD_SUPER_COORD) & ")" & _
                "order by cod_filial"
    
    Set VOBJ1 = BANCO.DBCREATEDYNASET(S_SELECT1, 0&)
    
    Set VOBJ2 = BANCO.DBCREATEDYNASET(S_SELECT2, 0&)
    
    lstFiliaisAtribuidas.Clear
    VOBJ1.MoveFirst
    For i = 1 To VOBJ1.RecordCount
        lstFiliaisAtribuidas.AddItem Format(VOBJ1.Fields(0).Value, "000") & " - " & VOBJ1.Fields(1).Value
        lstFiliaisAtribuidas.ItemData(lstFiliaisAtribuidas.NewIndex) = VOBJ1("COD_FILIAL")
        VOBJ1.MoveNext
    Next
    
    lstFiliaisDisponiveis.Clear
    VOBJ2.MoveFirst
    For j = 1 To VOBJ2.RecordCount
        lstFiliaisDisponiveis.AddItem Format(VOBJ2.Fields(0).Value, "000") & " - " & VOBJ2.Fields(1).Value
        lstFiliaisDisponiveis.ItemData(lstFiliaisDisponiveis.NewIndex) = VOBJ2("COD_FILIAL")
        VOBJ2.MoveNext
    Next
End Sub

Private Sub CMDCANCELAR_Click()
    CBO_COD_SUPER_COORD.ListIndex = -1
    CBO_COD_SUPER_COORD = ""
    CBO_COD_SUPER_COORD.SetFocus
    lstFiliaisAtribuidas.Clear
    lstFiliaisDisponiveis.Clear
End Sub

Private Sub CMDCANCELAR_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    StatusBar1.SimpleText = "Cancelar"
End Sub




Private Sub cmdEnviar_Click()
    Dim i As Integer
    Dim j As Integer
    Dim S_SQL1 As String
    
    For i = 0 To lstFiliaisDisponiveis.ListCount - 1 'adicionar item na lista
        If lstFiliaisDisponiveis.Selected(i) = True Then
            lstFiliaisAtribuidas.AddItem lstFiliaisDisponiveis.List(i)
            lstFiliaisAtribuidas.ItemData(lstFiliaisAtribuidas.NewIndex) = lstFiliaisDisponiveis.ItemData(i)
            S_SQL1 = "INSERT INTO INTERNET.R_FIL_SUPER_COORD VALUES(" & Val(CBO_COD_SUPER_COORD) & ", " & Val(lstFiliaisDisponiveis) & ")"
            BANCO.DBEXECUTESQL S_SQL1
        End If
    Next
    For j = lstFiliaisDisponiveis.ListCount - 1 To 0 Step -1 'remover item da lista
        If lstFiliaisDisponiveis.Selected(j) = True Then
            lstFiliaisDisponiveis.RemoveItem (j)
        End If
    Next
End Sub


'Private Sub CMDEXCLUIR_Click()
  '  Dim OBJ_CHECAR As Object
   ' Dim S_SELECT1
  '  Dim S_SQL1
  '  If CBO_COD_SUPER_COORD <> "" Then
  '      If CBO_FILIAL <> "" Then
   '         S_SELECT1 = "SELECT * " & _
  '                      "FROM   INTERNET.R_FIL_SUPER_COORD " & _
   '                     "WHERE  COD_SUPER_COORD = " & Val(CBO_COD_SUPER_COORD) & " AND " & _
   '                             "COD_FILIAL = " & Val(CBO_FILIAL)
    '        Set OBJ_CHECAR = BANCO.DBCREATEDYNASET(S_SELECT1, 0&)
    '        If OBJ_CHECAR.EOF Then
     '           MsgBox "Relacionamento n�o existe!"
     '           Exit Sub
     '       Else
     '           S_SQL1 = "DELETE INTERNET.R_FIL_SUPER_COORD WHERE COD_SUPER_COORD = " & Val(CBO_COD_SUPER_COORD) & " AND COD_FILIAL = " & Val(CBO_FILIAL)
      '          BANCO.DBEXECUTESQL S_SQL1
      '      End If
     '   Else
     '       MsgBox "Escolha a Filial!"
    '        Exit Sub
    '    End If
  '  Else
   '     MsgBox "Escolha o C�digo!"
   '     Exit Sub
   ' End If
   ' CBO_COD_SUPER_COORD.ListIndex = -1
  '  CBO_FILIAL.ListIndex = -1
   ' CBO_COD_SUPER_COORD = ""
  '  CBO_FILIAL = ""
  '  CBO_COD_SUPER_COORD.SetFocus
'End Sub
Private Sub CMDEXCLUIR_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    StatusBar1.SimpleText = "Excluir"
End Sub


'Private Sub CMDGRAVAR_Click()
'    Dim OBJ_CHECAR As Object
'    Dim S_SELECT1
'    Dim S_SQL1
'    If CBO_COD_SUPER_COORD <> "" Then
'        If CBO_FILIAL <> "" Then
'            S_SELECT1 = "SELECT * " & _
'                        "FROM   INTERNET.R_FIL_SUPER_COORD " & _
 '                       "WHERE  COD_SUPER_COORD = " & Val(CBO_COD_SUPER_COORD) & " AND " & _
 '                               "COD_FILIAL = " & Val(CBO_FILIAL)
 '           Set OBJ_CHECAR = BANCO.DBCREATEDYNASET(S_SELECT1, 0&)
 '           If OBJ_CHECAR.EOF Then
 '               S_SQL1 = "INSERT INTO INTERNET.R_FIL_SUPER_COORD VALUES(" & Val(CBO_COD_SUPER_COORD) & ", " & Val(CBO_FILIAL) & "); commit;"
 '               BANCO.DBEXECUTESQL S_SQL1
 '           Else
 '               MsgBox "Relacionamento j� existe!"
 '               Exit Sub
 '           End If
 '       Else
 '           MsgBox "Escolha a Filial!"
 '           Exit Sub
 '       End If
 '   Else
 '       MsgBox "Escolha o C�digo!"
 '       Exit Sub
 '   End If
  '  CBO_COD_SUPER_COORD.ListIndex = -1
 '   CBO_FILIAL.ListIndex = -1
 '   CBO_COD_SUPER_COORD = ""
 '   CBO_FILIAL = ""
 '   CBO_COD_SUPER_COORD.SetFocus
'End Sub
Private Sub CMDGRAVAR_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    StatusBar1.SimpleText = "Gravar"
End Sub


Private Sub cmdRetirar_Click()
Dim i As Integer
Dim j As Integer
Dim S_SQL1 As String

    For i = 0 To lstFiliaisAtribuidas.ListCount - 1 'adicionar item
        If lstFiliaisAtribuidas.Selected(i) = True Then
            lstFiliaisDisponiveis.AddItem lstFiliaisAtribuidas.List(i)
            S_SQL1 = "DELETE INTERNET.R_FIL_SUPER_COORD WHERE COD_SUPER_COORD = " & Val(CBO_COD_SUPER_COORD) & " AND COD_FILIAL = " & Val(lstFiliaisAtribuidas)
            BANCO.DBEXECUTESQL S_SQL1
           lstFiliaisDisponiveis.ItemData(Me.lstFiliaisDisponiveis.NewIndex) = Me.lstFiliaisAtribuidas.ItemData(i)
        End If
    Next
    For j = lstFiliaisAtribuidas.ListCount - 1 To 0 Step -1 'remover item
        If lstFiliaisAtribuidas.Selected(j) = True Then
            lstFiliaisAtribuidas.RemoveItem (j)
        End If
    Next
End Sub

Private Sub CMDVOLTAR_Click()
    Unload Me
End Sub
Private Sub CMDVOLTAR_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    StatusBar1.SimpleText = "Voltar"
End Sub



Private Sub TXT_COD_SUPER_COORD_Change()
    If FRMCADASTRO.TXT_COD_SUPER_COORD <> "" Then
        If Not IsNumeric(FRMCADASTRO.TXT_COD_SUPER_COORD) Then
            SendKeys "{BACKSPACE}"
            Exit Sub
        End If
    End If
End Sub


Private Sub Form_Activate()
    Dim S_SELECT1
    Dim S_SELECT2
    Dim OBJ_FILIAL As Object
    Dim OBJ_COORD As Object
    Screen.MousePointer = vbHourglass
   lstFiliaisDisponiveis.Clear
   lstFiliaisAtribuidas.Clear
    CBO_COD_SUPER_COORD.Clear
    StatusBar1.SimpleText = "Aguarde... Carregando Coordenadores/Supervisores..."
    S_SELECT1 = _
        "SELECT " & _
            "COD_FILIAL, " & _
            "NOME_FILIAL " & _
        "FROM " & _
            "FILIAL " & _
        "ORDER BY " & _
            "COD_FILIAL"
    Set OBJ_FILIAL = BANCO.DBCREATEDYNASET(S_SELECT1, 0&)
    While Not OBJ_FILIAL.EOF
       ' CBO_FILIAL.AddItem OBJ_FILIAL!cod_filial & "-" & OBJ_FILIAL!NOME_FILIAL
        OBJ_FILIAL.MoveNext
    Wend
    StatusBar1.SimpleText = "Aguarde... Carregando Filiais..."
    S_SELECT2 = _
        "SELECT " & _
            "COD_SUPER_COORD, " & _
            "NOME " & _
        "FROM " & _
            "INTERNET.SUPER_COORD " & _
        "ORDER BY " & _
            "COD_SUPER_COORD"
    Set OBJ_COORD = BANCO.DBCREATEDYNASET(S_SELECT2, 0&)
    While Not OBJ_COORD.EOF
        CBO_COD_SUPER_COORD.AddItem OBJ_COORD!COD_SUPER_COORD & "-" & OBJ_COORD!NOME
        OBJ_COORD.MoveNext
    Wend
    StatusBar1.SimpleText = ""
    Screen.MousePointer = vbDefault
End Sub

Private Sub List1_Click()

End Sub

