VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.MDIForm MDICAD580 
   BackColor       =   &H8000000C&
   Caption         =   "CAD580 - Cadastro de Supervisor / Coordenador"
   ClientHeight    =   5805
   ClientLeft      =   1230
   ClientTop       =   2820
   ClientWidth     =   6510
   LinkTopic       =   "MDIForm1"
   WindowState     =   2  'Maximized
   Begin VB.Timer Timer1 
      Interval        =   3000
      Left            =   1080
      Top             =   72
   End
   Begin Threed.SSPanel SSPanel1 
      Align           =   3  'Align Left
      Height          =   5400
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   975
      _Version        =   65536
      _ExtentX        =   1720
      _ExtentY        =   9525
      _StockProps     =   15
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin Threed.SSCommand SSCommand1 
         Height          =   615
         Left            =   180
         TabIndex        =   1
         Top             =   2970
         Visible         =   0   'False
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   1085
         _StockProps     =   78
         BevelWidth      =   1
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "Mdcad580.frx":0000
      End
      Begin Threed.SSCommand CMDRELACIONAMENTO 
         Height          =   615
         Left            =   180
         TabIndex        =   2
         Top             =   2280
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   1085
         _StockProps     =   78
         BevelWidth      =   1
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "Mdcad580.frx":031A
      End
      Begin Threed.SSCommand CMDCADASTROS 
         Height          =   615
         Left            =   180
         TabIndex        =   0
         Top             =   960
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   1085
         _StockProps     =   78
         BevelWidth      =   1
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "Mdcad580.frx":0634
      End
      Begin Threed.SSCommand CMDSAIR 
         Height          =   615
         Left            =   180
         TabIndex        =   4
         Top             =   4680
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   1085
         _StockProps     =   78
         BevelWidth      =   1
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "Mdcad580.frx":094E
      End
      Begin Threed.SSCommand CMDSOBRE 
         Height          =   615
         Left            =   180
         TabIndex        =   3
         Top             =   3900
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   1085
         _StockProps     =   78
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   4.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BevelWidth      =   1
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "Mdcad580.frx":0C68
      End
      Begin Threed.SSCommand cmdRelacionamentoGerentes 
         Height          =   615
         Left            =   180
         TabIndex        =   7
         ToolTipText     =   "Gerentes"
         Top             =   1620
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   1085
         _StockProps     =   78
         BevelWidth      =   1
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "Mdcad580.frx":0F82
      End
      Begin VB.Image Image1 
         Appearance      =   0  'Flat
         Height          =   735
         Left            =   60
         Picture         =   "Mdcad580.frx":129C
         Stretch         =   -1  'True
         Top             =   60
         Width           =   855
      End
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   405
      Left            =   0
      TabIndex        =   6
      Top             =   5400
      Width           =   6510
      _ExtentX        =   11483
      _ExtentY        =   714
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   10.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Menu MNU_CADASTRO 
      Caption         =   "&Cadastro"
      Begin VB.Menu CADASTRO_COORD 
         Caption         =   "Cooredenadoras"
      End
      Begin VB.Menu CADASTRO_GERENTE 
         Caption         =   "Gerente"
      End
   End
   Begin VB.Menu MNU_RELACIONAMENTO 
      Caption         =   "&Relacionamento"
   End
   Begin VB.Menu MNU_SOBRE 
      Caption         =   "S&obre"
   End
   Begin VB.Menu MNU_SAIR 
      Caption         =   "&Sair"
   End
End
Attribute VB_Name = "MDICAD580"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CADASTRO_COORD_Click()
    FRMCADASTRO.Show 1
End Sub

Private Sub CADASTRO_GERENTE_Click()
    FRMCADGER.Show 1
End Sub


Private Sub CMDCADASTROS_Click()
    CADASTRO_COORD_Click
End Sub

Private Sub CMDCADASTROS_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    StatusBar1.SimpleText = "Cadastro de Coordenadoras"
End Sub


Private Sub CMDRELACIONAMENTO_Click()
    MNU_RELACIONAMENTO_Click
End Sub

Private Sub CMDRELACIONAMENTO_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    StatusBar1.SimpleText = "Relacionamento com Filial"
End Sub


Private Sub cmdRelacionamentoGerentes_Click()
    frmRelacionamentoGerente.Show
End Sub

Private Sub CMDSAIR_Click()
    End
End Sub

Private Sub CMDSAIR_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    StatusBar1.SimpleText = "Sair"
End Sub


Private Sub CMDSOBRE_Click()
    MNU_SOBRE_Click
End Sub
Private Sub CMDSOBRE_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    StatusBar1.SimpleText = "Sobre"
End Sub


Private Sub MDIForm_Load()
    Set SESSAO = CreateObject("oracleinprocserver.xorasession")
    Set BANCO = SESSAO.OpenDatabase("PRODUCAO.CAL.COM.BR", "CAD580/PROD", 0&)
    'Set BANCO = SESSAO.OpenDatabase("SDPK_TESTE", "PRODUCAO/DPK", 0&)
End Sub


Private Sub MNU_RELACIONAMENTO_Click()
    FRMRELACIONAMENTO.Show 1
End Sub

Private Sub MNU_SAIR_Click()
    End
End Sub
Private Sub MNU_SOBRE_Click()
    FRMSOBRE.Show 1
End Sub

Private Sub SSCommand1_Click()
    CADASTRO_GERENTE_Click
End Sub
Private Sub SSCommand1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    StatusBar1.SimpleText = "Cadastro de Gerente"
End Sub
Private Sub Timer1_Timer()
    StatusBar1.SimpleText = ""
End Sub


