VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form frmRelacionamentoGerente 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Relacionamento de Gerentes/Coordenadores"
   ClientHeight    =   8430
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7515
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8430
   ScaleWidth      =   7515
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox TXT_EMAIL 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   315
      Left            =   1950
      MaxLength       =   50
      TabIndex        =   19
      Top             =   780
      Width           =   4815
   End
   Begin VB.Frame Frame1 
      Height          =   1125
      Left            =   990
      TabIndex        =   5
      Top             =   120
      Width           =   6375
      Begin VB.ComboBox CBO_COD_SUPER_COORD 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmRelacionamentoGerente.frx":0000
         Left            =   960
         List            =   "frmRelacionamentoGerente.frx":0002
         TabIndex        =   6
         Top             =   240
         Width           =   4815
      End
      Begin VB.Label Label4 
         Caption         =   "E-mail"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   20
         Top             =   720
         Width           =   735
      End
      Begin VB.Label Label1 
         Caption         =   "C�digo"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   300
         Width           =   735
      End
   End
   Begin VB.Frame Frame2 
      Height          =   975
      Left            =   3300
      TabIndex        =   2
      Top             =   4020
      Width           =   1935
      Begin Threed.SSCommand cmdRetirar 
         Height          =   615
         Left            =   1080
         TabIndex        =   3
         Top             =   240
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   1085
         _StockProps     =   78
         ForeColor       =   -2147483638
         BevelWidth      =   1
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "frmRelacionamentoGerente.frx":0004
      End
      Begin Threed.SSCommand cmdEnviar 
         Height          =   615
         Left            =   240
         TabIndex        =   4
         Top             =   240
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   1085
         _StockProps     =   78
         ForeColor       =   -2147483638
         BevelWidth      =   1
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "frmRelacionamentoGerente.frx":031E
      End
   End
   Begin VB.ListBox lstFiliaisDisponiveis 
      Height          =   2205
      ItemData        =   "frmRelacionamentoGerente.frx":0638
      Left            =   990
      List            =   "frmRelacionamentoGerente.frx":063F
      Sorted          =   -1  'True
      TabIndex        =   1
      Top             =   1680
      Width           =   6375
   End
   Begin VB.ListBox lstFiliaisAtribuidas 
      Height          =   2205
      Left            =   990
      Sorted          =   -1  'True
      TabIndex        =   0
      Top             =   5100
      Width           =   6375
   End
   Begin Threed.SSPanel SSPanel1 
      Align           =   3  'Align Left
      Height          =   8010
      Left            =   0
      TabIndex        =   8
      Top             =   0
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   14129
      _StockProps     =   15
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderWidth     =   1
      RoundedCorners  =   0   'False
      Alignment       =   1
      Begin Threed.SSCommand CMDCANCELAR 
         Height          =   615
         Left            =   120
         TabIndex        =   9
         Top             =   120
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   1085
         _StockProps     =   78
         ForeColor       =   -2147483638
         BevelWidth      =   1
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "frmRelacionamentoGerente.frx":065A
      End
      Begin Threed.SSCommand CMDGRAVAR 
         Height          =   615
         Left            =   120
         TabIndex        =   10
         Top             =   6360
         Visible         =   0   'False
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   1085
         _StockProps     =   78
         ForeColor       =   -2147483638
         BevelWidth      =   1
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "frmRelacionamentoGerente.frx":0974
      End
      Begin Threed.SSCommand CMDEXCLUIR 
         Height          =   615
         Left            =   120
         TabIndex        =   11
         Top             =   7080
         Visible         =   0   'False
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   1085
         _StockProps     =   78
         ForeColor       =   -2147483638
         BevelWidth      =   1
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "frmRelacionamentoGerente.frx":0DC6
      End
      Begin Threed.SSCommand CMDVOLTAR 
         Height          =   615
         Left            =   120
         TabIndex        =   12
         Top             =   960
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   1085
         _StockProps     =   78
         ForeColor       =   -2147483638
         BevelWidth      =   1
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "frmRelacionamentoGerente.frx":10E0
      End
   End
   Begin Threed.SSCommand SSCommand3 
      Height          =   615
      Left            =   0
      TabIndex        =   13
      Top             =   0
      Width           =   615
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   1085
      _StockProps     =   78
      ForeColor       =   -2147483638
      BevelWidth      =   1
      RoundedCorners  =   0   'False
      Outline         =   0   'False
      Picture         =   "frmRelacionamentoGerente.frx":13FA
   End
   Begin Threed.SSCommand SSCommand2 
      Height          =   615
      Left            =   0
      TabIndex        =   14
      Top             =   0
      Width           =   615
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   1085
      _StockProps     =   78
      ForeColor       =   -2147483638
      BevelWidth      =   1
      RoundedCorners  =   0   'False
      Outline         =   0   'False
      Picture         =   "frmRelacionamentoGerente.frx":184C
   End
   Begin Threed.SSCommand SSCommand1 
      Height          =   615
      Left            =   0
      TabIndex        =   15
      Top             =   0
      Width           =   615
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   1085
      _StockProps     =   78
      ForeColor       =   -2147483638
      BevelWidth      =   1
      RoundedCorners  =   0   'False
      Outline         =   0   'False
      Picture         =   "frmRelacionamentoGerente.frx":1C9E
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   420
      Left            =   0
      TabIndex        =   16
      Top             =   8010
      Width           =   7515
      _ExtentX        =   13256
      _ExtentY        =   741
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   10.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "Filiais Dispon�veis"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   990
      TabIndex        =   18
      Top             =   1440
      Width           =   1785
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      Caption         =   "Filiais Atribu�das"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   990
      TabIndex        =   17
      Top             =   4860
      Width           =   1650
   End
End
Attribute VB_Name = "frmRelacionamentoGerente"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Sub CBO_COD_SUPER_COORD_Click()
    
    If CBO_COD_SUPER_COORD.ListIndex = -1 Then
       TXT_EMAIL = ""
       lstFiliaisAtribuidas.Clear
       lstFiliaisDisponiveis.Clear
    End If
    
    Dim obj_checar2 As Object
    S_SELECT1 = "SELECT A.E_MAIL " & _
                "FROM   CONTROLE_BASES.GERENCIA A " & _
                "WHERE  A.COD_GERENTE = " & Val(CBO_COD_SUPER_COORD)
                
    
    Set obj_checar2 = BANCO.DBCREATEDYNASET(S_SELECT1, 0&)
    If Not obj_checar2.EOF Then
        TXT_EMAIL = "" & obj_checar2!E_MAIL
    Else
        TXT_EMAIL = ""
    End If
    
    Preencher_Atribuidas
    Preencher_Disponiveis
    
End Sub

Private Sub CMDCANCELAR_Click()
    CBO_COD_SUPER_COORD.ListIndex = -1
    CBO_COD_SUPER_COORD.SetFocus
End Sub

Private Sub CMDCANCELAR_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    StatusBar1.SimpleText = "Cancelar"
End Sub

Private Sub cmdEnviar_Click()
    Dim i As Integer
    Dim j As Integer
    Dim S_SQL1 As String
    
    For i = 0 To lstFiliaisDisponiveis.ListCount - 1 'adicionar item na lista
        If lstFiliaisDisponiveis.Selected(i) = True Then
            lstFiliaisAtribuidas.AddItem lstFiliaisDisponiveis.List(i)
            lstFiliaisAtribuidas.ItemData(lstFiliaisAtribuidas.NewIndex) = lstFiliaisDisponiveis.ItemData(i)
            S_SQL1 = "INSERT INTO INTERNET.GERENTE VALUES(" & Val(CBO_COD_SUPER_COORD) & ", " & Val(lstFiliaisDisponiveis) & ")"
            BANCO.DBEXECUTESQL S_SQL1
        End If
    Next
    For j = lstFiliaisDisponiveis.ListCount - 1 To 0 Step -1 'remover item da lista
        If lstFiliaisDisponiveis.Selected(j) = True Then
            lstFiliaisDisponiveis.RemoveItem (j)
        End If
    Next
End Sub


Private Sub CMDEXCLUIR_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    StatusBar1.SimpleText = "Excluir"
End Sub


Private Sub CMDGRAVAR_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    StatusBar1.SimpleText = "Gravar"
End Sub


Private Sub cmdRetirar_Click()
Dim i As Integer
Dim j As Integer
Dim S_SQL1 As String

    For i = 0 To lstFiliaisAtribuidas.ListCount - 1 'adicionar item
        If lstFiliaisAtribuidas.Selected(i) = True Then
            lstFiliaisDisponiveis.AddItem lstFiliaisAtribuidas.List(i)
            S_SQL1 = "DELETE INTERNET.gerente WHERE COD_GERENTE = " & Val(CBO_COD_SUPER_COORD) & " AND COD_FILIAL = " & Val(lstFiliaisAtribuidas)
            BANCO.DBEXECUTESQL S_SQL1
           lstFiliaisDisponiveis.ItemData(Me.lstFiliaisDisponiveis.NewIndex) = Me.lstFiliaisAtribuidas.ItemData(i)
        End If
    Next
    For j = lstFiliaisAtribuidas.ListCount - 1 To 0 Step -1 'remover item
        If lstFiliaisAtribuidas.Selected(j) = True Then
            lstFiliaisAtribuidas.RemoveItem (j)
        End If
    Next
End Sub

Private Sub CMDVOLTAR_Click()
    Unload Me
End Sub
Private Sub CMDVOLTAR_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    StatusBar1.SimpleText = "Voltar"
End Sub



Private Sub TXT_COD_SUPER_COORD_Change()
    If FRMCADASTRO.TXT_COD_SUPER_COORD <> "" Then
        If Not IsNumeric(FRMCADASTRO.TXT_COD_SUPER_COORD) Then
            SendKeys "{BACKSPACE}"
            Exit Sub
        End If
    End If
End Sub


Private Sub Form_Load()
    Dim S_SELECT1
    Dim S_SELECT2
    Dim OBJ_FILIAL As Object
    Dim OBJ_COORD As Object
    Screen.MousePointer = vbHourglass
    
    lstFiliaisAtribuidas.Clear
    lstFiliaisDisponiveis.Clear
    
    CBO_COD_SUPER_COORD.Clear
    
'    StatusBar1.SimpleText = "Aguarde... Carregando Coordenadores/Supervisores..."
'    S_SELECT1 = _
'        "SELECT " & _
'            "COD_FILIAL, " & _
'            "NOME_FILIAL " & _
'        "FROM " & _
'            "FILIAL " & _
'        "WHERE " & _
'            "TP_FILIAL = 'R' " & _
'        "ORDER BY " & _
'            "COD_FILIAL"
'    Set OBJ_FILIAL = BANCO.DBCREATEDYNASET(S_SELECT1, 0&)
'    While Not OBJ_FILIAL.EOF
'        lstFiliaisAtribuidas.AddItem OBJ_FILIAL!cod_filial & "-" & OBJ_FILIAL!NOME_FILIAL
'        OBJ_FILIAL.MoveNext
'    Wend
    StatusBar1.SimpleText = "Aguarde... Carregando Filiais..."
    S_SELECT2 = _
        "SELECT " & _
            "COD_REPRES, " & _
            "NOME_REPRES " & _
        "FROM " & _
            "REPRESENTANTE " & _
        "ORDER BY " & _
            "COD_REPRES"
    Set OBJ_COORD = BANCO.DBCREATEDYNASET(S_SELECT2, 0&)
    While Not OBJ_COORD.EOF
        CBO_COD_SUPER_COORD.AddItem OBJ_COORD!COD_REPRES & "-" & OBJ_COORD!NOME_REPRES
        OBJ_COORD.MoveNext
    Wend
    StatusBar1.SimpleText = ""
    Screen.MousePointer = vbDefault
End Sub


Private Sub TXT_EMAIL_LostFocus()
    Dim n_validar_email As Byte
    n_validar_email = InStr(1, TXT_EMAIL, "@")
    If n_validar_email = 0 Then
        MsgBox "Email inv�lido!"
        TXT_EMAIL.SetFocus
        Exit Sub
    End If
End Sub

Sub Preencher_Atribuidas()
    Dim vSql As String
    Dim vObj As Object
    Dim i As Integer
    
    'Alteracao solicitada pela Adriana - 22/02/06
    vSql = "SELECT F.COD_FILIAL, F.NOME_FILIAL " & _
           " FROM   FILIAL F, INTERNET.GERENTE G " & _
           " WHERE  F.COD_FILIAL = G.COD_FILIAL and " & _
           " g.cod_gerente = " & Val(CBO_COD_SUPER_COORD) & _
           " ORDER BY COD_FILIAL "
'           " F.TP_FILIAL = 'R' and "
                                
    Set vObj = BANCO.DBCREATEDYNASET(vSql, 0&)
                                
    lstFiliaisAtribuidas.Clear
    
    For i = 1 To vObj.RecordCount
        lstFiliaisAtribuidas.AddItem Format(vObj!cod_filial, "0000") & " - " & vObj!NOME_FILIAL
        vObj.MoveNext
    Next
End Sub

Sub Preencher_Disponiveis()
    Dim vSql As String
    Dim vObj As Object
    Dim i As Integer
    
'Alterado por Eduardo Relvas - 21/02/2006
'    vSql = " SELECT F.COD_FILIAL, F.NOME_FILIAL " & _
'           " FROM   FILIAL F " & _
'           " WHERE  F.COD_FILIAL NOT IN (SELECT COD_FILIAL FROM INTERNET.GERENTE WHERE cod_gerente = " & Val(CBO_COD_SUPER_COORD) & ")" & _
'           "        ORDER BY COD_FILIAL "
'           '" and    F.tp_filial = 'R' ORDER BY COD_FILIAL "
    
    'SELECT 2 CONTEM AS INFORMA��ES SOBRE A LISTA DE FILIAIS DISPON�VEIS
    vSql = "select cod_filial, nome_filial " & _
           "from filial " & _
           "where cod_filial not in (" & _
           "select cod_filial " & _
           "from internet.Gerente " & _
           "where cod_gerente = " & Val(CBO_COD_SUPER_COORD) & ")" & _
           "order by cod_filial"
                                
                                
    Set vObj = BANCO.DBCREATEDYNASET(vSql, 0&)
                                
    lstFiliaisDisponiveis.Clear
    
    For i = 1 To vObj.RecordCount
        lstFiliaisDisponiveis.AddItem Format(vObj!cod_filial, "0000") & " - " & vObj!NOME_FILIAL
        vObj.MoveNext
    Next
End Sub

