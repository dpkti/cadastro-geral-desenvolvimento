VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form frmCad210 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "CAD 210 - ALTERA��O DE PESOS E QTD. M�NIMA DE VENDA"
   ClientHeight    =   7155
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11070
   Icon            =   "frmCad210.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   477
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   738
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame framePesquisar 
      Appearance      =   0  'Flat
      Caption         =   "PESQUISAR"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   5055
      Left            =   480
      TabIndex        =   13
      Top             =   1440
      Width           =   5985
      Begin VB.TextBox txtCodFabrica 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   240
         TabIndex        =   2
         ToolTipText     =   "Digite o c�digo de f�brica do item"
         Top             =   1920
         Width           =   3090
      End
      Begin VB.TextBox txtCodDPK 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1560
         TabIndex        =   3
         ToolTipText     =   "Digite o c�digo DPK do item"
         Top             =   3240
         Width           =   1770
      End
      Begin VB.ComboBox cmbFornecedor 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         ItemData        =   "frmCad210.frx":23D2
         Left            =   240
         List            =   "frmCad210.frx":23D4
         TabIndex        =   1
         ToolTipText     =   "Digite o nome, o CNPJ, o c�digo ou a sigla do fornecedor desejado"
         Top             =   960
         Width           =   4485
      End
      Begin VB.Label lblDescItem 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   240
         TabIndex        =   19
         Top             =   4440
         Width           =   4560
      End
      Begin VB.Label lblDescricao 
         Appearance      =   0  'Flat
         Caption         =   "Descri��o"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   240
         TabIndex        =   18
         Top             =   3960
         Width           =   1050
      End
      Begin VB.Label lblCodDPK 
         Appearance      =   0  'Flat
         Caption         =   "C�digo DPK"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   240
         TabIndex        =   17
         Top             =   3240
         Width           =   1410
      End
      Begin VB.Label lblFornecedor 
         Appearance      =   0  'Flat
         Caption         =   "Fornecedor"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   240
         TabIndex        =   16
         Top             =   480
         Width           =   1050
      End
      Begin VB.Label lblCodFabrica 
         Appearance      =   0  'Flat
         Caption         =   "C�digo de F�brica"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   240
         TabIndex        =   15
         Top             =   1560
         Width           =   2010
      End
      Begin VB.Label lblCodFornecedor 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   1800
         TabIndex        =   14
         Top             =   480
         Width           =   960
      End
   End
   Begin VB.Frame framePesoQtde 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1815
      Left            =   7320
      TabIndex        =   10
      Top             =   2400
      Width           =   3105
      Begin VB.TextBox txtQtdeMin 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1560
         TabIndex        =   5
         Top             =   1080
         Width           =   930
      End
      Begin VB.TextBox txtPeso 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1560
         TabIndex        =   4
         Top             =   360
         Width           =   930
      End
      Begin VB.Label lblQtdeMin 
         Appearance      =   0  'Flat
         Caption         =   "Qtde M�n. Venda"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   405
         Left            =   240
         TabIndex        =   12
         Top             =   1080
         Width           =   1290
      End
      Begin VB.Label lblPeso 
         Appearance      =   0  'Flat
         Caption         =   "Peso"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   240
         TabIndex        =   11
         Top             =   480
         Width           =   570
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   6825
      Width           =   11070
      _ExtentX        =   19526
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   19024
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   0
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   1080
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   7
      TabStop         =   0   'False
      ToolTipText     =   "Voltar para p�gina inicial"
      Top             =   120
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCad210.frx":23D6
      PICN            =   "frmCad210.frx":23F2
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdGravar 
      Height          =   1005
      Left            =   7680
      TabIndex        =   8
      TabStop         =   0   'False
      ToolTipText     =   "Gravar altera��o"
      Top             =   0
      Width           =   1005
      _ExtentX        =   1773
      _ExtentY        =   1773
      BTYPE           =   3
      TX              =   "Gravar"
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCad210.frx":30CC
      PICN            =   "frmCad210.frx":30E8
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdAlterar 
      Height          =   1005
      Left            =   8880
      TabIndex        =   9
      TabStop         =   0   'False
      ToolTipText     =   "Atualizar dados"
      Top             =   0
      Width           =   1005
      _ExtentX        =   1773
      _ExtentY        =   1773
      BTYPE           =   3
      TX              =   "Atualizar"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCad210.frx":3DC2
      PICN            =   "frmCad210.frx":3DDE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdLimpar 
      Height          =   1005
      Left            =   6480
      TabIndex        =   20
      TabStop         =   0   'False
      ToolTipText     =   "Limpar campos"
      Top             =   0
      Width           =   1005
      _ExtentX        =   1773
      _ExtentY        =   1773
      BTYPE           =   3
      TX              =   "Limpar"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCad210.frx":4AB8
      PICN            =   "frmCad210.frx":4AD4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmCad210"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim vObj As Object
Dim vVerifica_Digitos As Integer
Dim vSituacao_Item As Integer
Dim vSituacao_CodFabrica As Integer

Private Sub cmbFornecedor_Change()
    lblCodFornecedor = ""
    DESABILITAR_ALTERACAO
    cmdGravar.Enabled = False
    txtCodFabrica = ""
    txtCodDPK = ""
    lblDescItem = ""
    txtPeso = ""
    txtQtdeMin = ""
End Sub

Private Sub cmbFornecedor_Click()
    lblCodFornecedor = Mid(cmbFornecedor, 1, 3)
    txtCodFabrica = ""
    txtCodDPK = ""
    lblDescItem = ""
    txtPeso = ""
    txtQtdeMin = ""
End Sub

Private Sub cmbFornecedor_KeyPress(KeyAscii As Integer)
    
 KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
     
    If KeyAscii = 13 Then
        cmbFornecedor_LostFocus
        cmbFornecedor.SetFocus
    End If
End Sub

Private Sub cmbFornecedor_LostFocus()
'Dim vForn As String

'    vForn = cmbFornecedor
    
'    cmbFornecedor.Clear
    
'    cmbFornecedor.Text = vForn
    
    If Trim(cmbFornecedor) <> "" And Trim(lblCodFornecedor) = "" Then
        PESQUISA_FORNECEDOR
    End If
End Sub

Private Sub cmdAlterar_Click()
    If txtCodDPK = "" Then
        Exit Sub
    Else
        cmdGravar.Enabled = True
        If cmbFornecedor <> "" Then
            HABILITAR_ALTERACAO
        End If
    End If
    
End Sub

Private Sub cmdPesquisarFornec_Click()
    cmbFornecedor.Enabled = True
End Sub

Private Sub cmdGravar_Click()
       
    If VALIDA_ALTERACAO = True Then
        If lblCodFornecedor <> "" Then
            vBanco.Parameters.Remove "NOVO_PESO"
            vBanco.Parameters.Add "NOVO_PESO", txtPeso, 1
            
            vBanco.Parameters.Remove "NOVA_QTDEMIN"
            vBanco.Parameters.Add "NOVA_QTDEMIN", txtQtdeMin, 1
            
            vBanco.Parameters.Remove "COD_FORNECEDOR"
            vBanco.Parameters.Add "COD_FORNECEDOR", lblCodFornecedor, 1
            
            vBanco.Parameters.Remove "COD_FABRICA"
            vBanco.Parameters.Add "COD_FABRICA", txtCodFabrica, 1
            
            vErro = vVB_Generica_001.ExecutaPl(vBanco, "PRODUCAO.PCK_CAD210.PR_UPDATE_PESO_QTDEMINVDA(:NOVO_PESO,:NOVA_QTDEMIN,:COD_FORNECEDOR,:COD_FABRICA)")
            
            If vErro = "" Then
                If vVB_Generica_001.Perguntar("Deseja realmente alterar o documento?") = 6 Then
                    vVB_Generica_001.Informar ("Altera��o realizada com sucesso!")
                    DESABILITAR_ALTERACAO
                    HABILITAR_PESQUISA
                Else
                    cmdGravar.Enabled = False
                    DESABILITAR_ALTERACAO
                    Exit Sub
                End If
            Else
                MsgBox vErro
            End If
        End If
        cmdGravar.Enabled = False
    End If
    
End Sub

Private Sub cmdLimpar_Click()

    txtCodDPK = ""
    lblCodFornecedor = ""
    txtCodFabrica = ""
    lblDescItem = ""
    txtPeso = ""
    txtQtdeMin = ""
    cmbFornecedor.Clear
    
End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub

Private Sub Form_Load()
        
    Me.Top = 0
    Me.Left = 0
    
End Sub

Sub PESQUISA_FORNECEDOR()

    Criar_Cursor vBanco.Parameters, "vCursor"
    vBanco.Parameters.Remove "CODERRO": vBanco.Parameters.Add "CODERRO", 0, 2
      
    
    VERIFICAR_DIGITOS (cmbFornecedor)
    
    Select Case vVerifica_Digitos
    
        Case 0
            vBanco.Parameters.Remove "FORNECEDOR"
            vBanco.Parameters.Add "FORNECEDOR", Val(cmbFornecedor), 1
            
            vErro = vVB_Generica_001.ExecutaPl(vBanco, "PRODUCAO.PCK_CAD210.PR_SELECT_COD_FORNEC(:VCURSOR,:FORNECEDOR,:CODERRO)")
            If vErro <> "" Then
                MsgBox vErro
            Else
                Set vObj = vBanco.Parameters("VCURSOR").Value
                If vObj.EOF Then
                    vVB_Generica_001.Informar ("N�o existe nenhum fornecedor com esse c�digo!")
                    cmbFornecedor.SetFocus
                Else
'                    PREENCHER_FORNECEDOR
                    cmbFornecedor.Clear
                    For i = 1 To vObj.RecordCount
                        cmbFornecedor.AddItem Format(vObj.Fields(1).Value, "000") & " - " & vObj.Fields(0).Value 'traz para o combo todos os fornecedores encontrados na busca.
                        'cmbFornecedor.ItemData(cmbFornecedor.NewIndex) = vObj.Fields(1).Value
                        vObj.MoveNext
                    Next
                    cmbFornecedor.ListIndex = 0
                End If
            End If
        
        
        Case 1
            
            vBanco.Parameters.Remove "FORNECEDOR"
            vBanco.Parameters.Add "FORNECEDOR", Val(cmbFornecedor), 1
            
            vErro = vVB_Generica_001.ExecutaPl(vBanco, "PRODUCAO.PCK_CAD210.PR_SELECT_CNPJ_FORNEC(:VCURSOR,:FORNECEDOR,:CODERRO)")
            
            If vErro <> "" Then
                MsgBox vErro
            Else
                Set vObj = vBanco.Parameters("VCURSOR").Value
                If vObj.EOF Then
                    vVB_Generica_001.Informar ("N�o existe nenhum fornecedor com esse CNPJ!")
                    cmbFornecedor.SetFocus
                Else
                    'PREENCHER_FORNECEDOR
                    cmbFornecedor.Clear
                    For i = 1 To vObj.RecordCount
                        cmbFornecedor.AddItem Format(vObj.Fields(1).Value, "000") & " - " & vObj.Fields(0).Value 'traz para o combo todos os fornecedores encontrados na busca.
                        vObj.MoveNext
                    Next
                    cmbFornecedor.ListIndex = 0
                End If
            End If
      
        
        Case 2
            
            vBanco.Parameters.Remove "FORNECEDOR"
            vBanco.Parameters.Add "FORNECEDOR", cmbFornecedor, 1
            
            vErro = vVB_Generica_001.ExecutaPl(vBanco, "PRODUCAO.PCK_CAD210.PR_SELECT_NOME_FORNEC(:VCURSOR,:FORNECEDOR,:CODERRO)")
            
            If vErro <> "" Then
                MsgBox vErro
            Else
                Set vObj = vBanco.Parameters("VCURSOR").Value
                If vObj.EOF Then
                    vVB_Generica_001.Informar ("N�o existe nenhum fornecedor com esse nome!")
                End If
                
                If vObj.RecordCount = 1 Then
                    PREENCHER_FORNECEDOR
                End If
                
                If vObj.RecordCount > 1 Then
                    vVB_Generica_001.Informar ("Encontrei mais de um fornecedor! Selecione um.")
                    cmbFornecedor.Clear
                    For i = 1 To vObj.RecordCount
                        cmbFornecedor.AddItem Format(vObj.Fields(1).Value, "000") & " - " & vObj.Fields(0).Value 'traz para o combo todos os fornecedores encontrados na busca.
                        'cmbFornecedor.ItemData(cmbFornecedor.NewIndex) = vObj.Fields(1).Value
                        vObj.MoveNext
                    Next
                    cmbFornecedor.ListIndex = 0
                    
                End If
            End If
    End Select
            
End Sub

Sub PESQUISA_COD_DPK()
          
    lblCodFornecedor = ""
    txtCodFabrica = ""
    lblDescItem = ""
    txtPeso = ""
    txtQtdeMin = ""
    cmbFornecedor.Clear
    
    Criar_Cursor vBanco.Parameters, "vCursor"
    
    vBanco.Parameters.Remove "COD_DPK"
    vBanco.Parameters.Add "COD_DPK", txtCodDPK, 1
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, "PRODUCAO.PCK_CAD210.PR_SELECT_COD_DPK(:VCURSOR,:COD_DPK)")
    
    If vErro <> "" Then
        MsgBox vErro
    Else
        Set vObj = vBanco.Parameters("VCURSOR").Value
        VERIFICA_SIT_ITEM
        Select Case vSituacao_Item
            
            Case 0
                If vObj.EOF Then
                    vVB_Generica_001.Informar ("N�o existe item com esse c�digo!")
                    txtCodDPK = ""
                    Exit Sub
                Else
                    PREENCHER
                End If
                
            Case 8
                vVB_Generica_001.Informar ("Esse item foi substitu�do!")
                cmdLimpar_Click
                Exit Sub
                
            Case 9
                vVB_Generica_001.Informar ("Item desativado!")
                cmdLimpar_Click
                Exit Sub
                
            End Select
        
    End If
End Sub

Function VERIFICAR_DIGITOS(dado As String) As Integer

    If IsNumeric(dado) Then 'SE FOR UM N�MERO
    
        If Len(dado) <= 3 Then '� C�DIGO_FORNEC
            vVerifica_Digitos = 0
        Else ' � CNPJ
            vVerifica_Digitos = 1
        End If
        
    Else ' SE FOR STRING
        vVerifica_Digitos = 2
        
    End If
    
End Function

Sub HABILITAR_PESQUISA()
    txtCodDPK.Enabled = True
    txtCodFabrica.Enabled = True
    cmbFornecedor.Enabled = True
End Sub

Sub HABILITAR_ALTERACAO()
    txtPeso.Enabled = True
    txtQtdeMin.Enabled = True
End Sub

Sub DESABILITAR_PESQUISA()
    txtCodDPK.Enabled = False
    txtCodFabrica.Enabled = False
    cmbFornecedor.Enabled = False
End Sub

Sub DESABILITAR_ALTERACAO()
    txtPeso.Enabled = False
    txtQtdeMin.Enabled = False
End Sub

Sub PREENCHER() 'PREENCHE A TELA COM OS DADOS ENCONTRADOS NO ORACLE

    If cmbFornecedor <> "" Then
        txtCodFabrica.Text = vObj("COD_FABRICA")
        txtCodDPK.Text = vObj("COD_DPK")
        lblDescItem.Caption = vObj("DESC_ITEM")
        txtPeso.Text = vObj("PESO")
        txtQtdeMin.Text = vObj("QTD_MINVDA")
    Else
        cmbFornecedor.Text = vObj("NOME_FORNEC")
        lblCodFornecedor.Caption = vObj("COD_FORNECEDOR")
        txtCodFabrica.Text = vObj("COD_FABRICA")
        txtCodDPK.Text = vObj("COD_DPK")
        lblDescItem.Caption = vObj("DESC_ITEM")
        txtPeso.Text = vObj("PESO")
        txtQtdeMin.Text = vObj("QTD_MINVDA")
    End If
   
End Sub

Sub PREENCHER_FORNECEDOR()

    cmbFornecedor.Text = vObj("NOME_FORNEC")
    lblCodFornecedor.Caption = vObj("COD_FORNECEDOR")
    
End Sub

Private Sub txtCodDPK_Change()

    DESABILITAR_ALTERACAO
    cmdGravar.Enabled = False
    
End Sub

Private Sub txtCodDPK_KeyPress(KeyAscii As Integer)

    If KeyAscii = 13 Then
        txtCodDPK_LostFocus
    End If
    
    KeyAscii = vVB_Generica_001.Numero(KeyAscii)
    
End Sub

Private Sub txtCodDPK_LostFocus()

    If Trim(txtCodDPK) <> "" Then
        PESQUISA_COD_DPK
    End If
    
End Sub

Private Sub txtCodFabrica_Change()

    DESABILITAR_ALTERACAO
    cmdGravar.Enabled = False
    
End Sub

Private Sub txtCodFabrica_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
    
    If KeyAscii = 13 Then
        txtCodFabrica_LostFocus
    End If
    
End Sub

Private Sub txtCodFabrica_LostFocus()
      
    SITUACAO_COD_FABRICA
    
    Select Case vSituacao_CodFabrica
    
    Case 0
        vVB_Generica_001.Informar ("O campo 'FORNECEDOR' deve ser informado!")
        txtCodFabrica = ""
        cmbFornecedor.SetFocus
    Case 1
        Exit Sub
    Case 2
        Exit Sub
    Case 3
               
        Criar_Cursor vBanco.Parameters, "vCursor"
        
        vBanco.Parameters.Remove "COD_FABRICA"
        vBanco.Parameters.Add "COD_FABRICA", txtCodFabrica, 1
        
        vBanco.Parameters.Remove "COD_FORNEC"
        vBanco.Parameters.Add "COD_FORNEC", lblCodFornecedor, 1
        
        vErro = vVB_Generica_001.ExecutaPl(vBanco, "PRODUCAO.PCK_CAD210.PR_SELECT_COD_FABRICA(:COD_FABRICA,:COD_FORNEC,:VCURSOR)")
        
        If vErro <> "" Then
            MsgBox vErro
        Else
            Set vObj = vBanco.Parameters("VCURSOR").Value
            VERIFICA_SIT_ITEM
            Select Case vSituacao_Item
            
            Case 0
                If vObj.EOF Then
                    vVB_Generica_001.Informar ("N�o existe nenhum item desse fornecedor com esse c�digo de f�brica!")
                    txtPeso = ""
                    txtQtdeMin = ""
                    txtCodFabrica = ""
                Else
                    PREENCHER
                End If
                
            Case 8
                vVB_Generica_001.Informar ("Esse item foi substitu�do!")
                cmdLimpar_Click
                Exit Sub
                
            Case 9
                vVB_Generica_001.Informar ("Item desativado!")
                cmdLimpar_Click
                Exit Sub
                
            End Select
        End If
        
    End Select
End Sub

Sub SITUACAO_COD_FABRICA()

    If Trim(cmbFornecedor.Text) = "" And Trim(txtCodFabrica) <> "" Then
        vSituacao_CodFabrica = 0
    End If
    
    If Trim(cmbFornecedor.Text) = "" And Trim(txtCodFabrica) = "" Then
        vSituacao_CodFabrica = 1
    End If
    
    If Trim(cmbFornecedor.Text) <> "" And Trim(txtCodFabrica) = "" Then
        vSituacao_CodFabrica = 2
    End If
    
    If Trim(cmbFornecedor.Text) <> "" And Trim(txtCodFabrica) <> "" Then
        vSituacao_CodFabrica = 3
    End If

End Sub

Function VALIDA_ALTERACAO() As Boolean

    If txtPeso = "" Then
        vVB_Generica_001.Informar ("O valor do campo 'PESO' deve ser informado!")
        txtPeso.SetFocus
        VALIDA_ALTERACAO = False
        Exit Function
    End If

    If txtQtdeMin = "" Then
        vVB_Generica_001.Informar ("O valor do campo 'QTDE M�NIMA' deve ser informado!")
        txtQtdeMin.SetFocus
        VALIDA_ALTERACAO = False
        Exit Function
    End If
    
    VALIDA_ALTERACAO = True
    
End Function

Function VERIFICA_SIT_ITEM()

    If vObj("SITUACAO") = 0 Then
        vSituacao_Item = 0
    End If
    
    If vObj("SITUACAO") = 8 Then
        vSituacao_Item = 8
    End If
    
    If vObj("SITUACAO") = 9 Then
        vSituacao_Item = 9
    End If
    
End Function

