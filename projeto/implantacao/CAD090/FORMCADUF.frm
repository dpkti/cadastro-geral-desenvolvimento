VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form FORMCADUF 
   BackColor       =   &H8000000C&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "CADASTRO DE UF:"
   ClientHeight    =   5010
   ClientLeft      =   2850
   ClientTop       =   2115
   ClientWidth     =   6150
   FillColor       =   &H00FFFFFF&
   ForeColor       =   &H8000000A&
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5010
   ScaleWidth      =   6150
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   405
      Left            =   0
      TabIndex        =   13
      Top             =   4605
      Width           =   6150
      _ExtentX        =   10848
      _ExtentY        =   714
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   10795
         EndProperty
      EndProperty
   End
   Begin VB.Frame fraManutencao 
      Caption         =   "Cadastro de UF"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   3765
      Left            =   60
      TabIndex        =   8
      Top             =   750
      Visible         =   0   'False
      Width           =   6015
      Begin VB.TextBox txtCodUf 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   240
         MaxLength       =   2
         TabIndex        =   1
         Top             =   855
         Width           =   495
      End
      Begin VB.TextBox txtDescUf 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   240
         MaxLength       =   20
         TabIndex        =   2
         Top             =   1680
         Width           =   3435
      End
      Begin VB.TextBox txtDtAliq 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   240
         TabIndex        =   3
         Top             =   2490
         Width           =   1035
      End
      Begin VB.ComboBox cboMacro 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         ItemData        =   "FORMCADUF.frx":0000
         Left            =   240
         List            =   "FORMCADUF.frx":0013
         TabIndex        =   4
         Top             =   3210
         Width           =   855
      End
      Begin VB.Label lblCodigo 
         Caption         =   "C�digo:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   240
         TabIndex        =   12
         Top             =   510
         Width           =   1095
      End
      Begin VB.Label lblDesc 
         Caption         =   "Descri��o:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   240
         TabIndex        =   11
         Top             =   1335
         Width           =   1485
      End
      Begin VB.Label lblDtAliq 
         Caption         =   "Data Aliq.:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   240
         TabIndex        =   10
         Top             =   2130
         Width           =   1575
      End
      Begin VB.Label lblMacro 
         Caption         =   "Macro:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   240
         TabIndex        =   9
         Top             =   2910
         Width           =   945
      End
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   8760
      Top             =   1800
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FORMCADUF.frx":002B
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FORMCADUF.frx":0347
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FORMCADUF.frx":079B
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FORMCADUF.frx":0AB7
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FORMCADUF.frx":0F0B
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FORMCADUF.frx":1227
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FORMCADUF.frx":197B
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   660
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6150
      _ExtentX        =   10848
      _ExtentY        =   1164
      ButtonWidth     =   1032
      ButtonHeight    =   1005
      Appearance      =   1
      ImageList       =   "ImageList1"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   6
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "TBCadastro"
            Object.ToolTipText     =   "CADASTRAR UF:"
            ImageIndex      =   2
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "TBSobre"
            Object.ToolTipText     =   "INFORMA��ES SOBRE O PROGRAMA:"
            ImageIndex      =   6
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "TBSair"
            Object.ToolTipText     =   "SAIR DO PROGRAMA:"
            ImageIndex      =   5
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Toolbar Toolbar2 
      Align           =   1  'Align Top
      Height          =   660
      Left            =   0
      TabIndex        =   5
      Top             =   660
      Visible         =   0   'False
      Width           =   6150
      _ExtentX        =   10848
      _ExtentY        =   1164
      ButtonWidth     =   1032
      ButtonHeight    =   1005
      Appearance      =   1
      ImageList       =   "ImageList1"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   10
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "TBConsultar"
            Object.ToolTipText     =   "CONSULTAR UF�s:"
            ImageIndex      =   3
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "TBImprimir"
            Object.ToolTipText     =   "IMPRIMIR:"
            ImageIndex      =   1
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "TBConfirma"
            Object.ToolTipText     =   "CONFIRMA DADOS:"
            ImageIndex      =   4
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "TBCancela"
            Object.ToolTipText     =   "CANCELA:"
            ImageIndex      =   7
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "TBVoltar"
            Object.ToolTipText     =   "VOLTAR:"
            ImageIndex      =   5
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Toolbar Toolbar3 
      Align           =   1  'Align Top
      Height          =   660
      Left            =   0
      TabIndex        =   6
      Top             =   1320
      Visible         =   0   'False
      Width           =   6150
      _ExtentX        =   10848
      _ExtentY        =   1164
      ButtonWidth     =   1032
      ButtonHeight    =   1005
      Appearance      =   1
      ImageList       =   "ImageList1"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   2
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "TBVoltar2"
            Object.ToolTipText     =   "SAIR:"
            ImageIndex      =   5
         EndProperty
      EndProperty
   End
   Begin MSFlexGridLib.MSFlexGrid Grid1 
      Height          =   3720
      Left            =   60
      TabIndex        =   7
      Top             =   750
      Visible         =   0   'False
      Width           =   6030
      _ExtentX        =   10636
      _ExtentY        =   6562
      _Version        =   393216
      Cols            =   4
      FixedCols       =   0
      BackColor       =   -2147483624
      BackColorBkg    =   -2147483645
      FormatString    =   "<Cod. UF          |<Descri��o                                |<Data Aliq.           |<Macro          "
   End
   Begin VB.Menu Cadastro 
      Caption         =   "&Cadastro"
      Begin VB.Menu CadastrarUF 
         Caption         =   "&Cadastrar UF"
      End
   End
   Begin VB.Menu Sobre 
      Caption         =   "&Sobre"
   End
   Begin VB.Menu Sair1 
      Caption         =   "S&air"
   End
   Begin VB.Menu Consulta 
      Caption         =   "&Consulta"
   End
   Begin VB.Menu Imprimir 
      Caption         =   "&Imprimir"
   End
   Begin VB.Menu Confirma 
      Caption         =   "&Confirma"
   End
   Begin VB.Menu Cancela 
      Caption         =   "C&ancela"
   End
   Begin VB.Menu Sair2 
      Caption         =   "&Sair"
   End
   Begin VB.Menu Sair3 
      Caption         =   "&Sair"
   End
   Begin VB.Menu Popup1 
      Caption         =   "RightButton"
      Visible         =   0   'False
      Begin VB.Menu PopupConfirma 
         Caption         =   "Confirmar"
      End
      Begin VB.Menu PopupCancelar 
         Caption         =   "Cancelar"
      End
   End
End
Attribute VB_Name = "FORMCADUF"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Sub Checa_Campos()
    fl_erro = False  'variavel de controle de erro
    
    If Trim(txtCodUf.Text) = "" Then
        MsgBox "Deve-se Digitar um C�digo de UF ", vbCritical, MSG_TIT
        txtCodUf.SetFocus
        fl_erro = True
        Exit Sub
    ElseIf Trim(txtDescUf.Text) = "" Then
        MsgBox "Deve-se Digitar uma Descri��o de UF ", vbCritical, MSG_TIT
        txtDescUf.SetFocus
        fl_erro = True
        Exit Sub
    ElseIf Trim(txtDtAliq.Text) = "" Then
        MsgBox "Deve-se Digitar uma Data para a Aliquota de UF ", vbCritical, MSG_TIT
        txtDtAliq.SetFocus
        fl_erro = True
        Exit Sub
    ElseIf Trim(cboMacro.Text) = " " Then
        MsgBox "Deve-se Escolher uma Macro Para UF ", vbCritical, MSG_TIT
        cboMacro.SetFocus
        fl_erro = True
        Exit Sub
    End If
End Sub

Sub Limpa_Campos()
    txtCodUf.Text = ""
    txtDescUf.Text = ""
    txtDtAliq.Text = ""
    cboMacro.Text = " "
    txtCodUf.Enabled = True
    txtCodUf.SetFocus
End Sub

Private Sub CadastrarUF_Click() 'menu
    Toolbar1.Visible = False
    Toolbar2.Visible = True
    fraManutencao.Visible = True
    
    'MENUS:
    
        '1� BLOCO
    Cadastro.Visible = False
    Sobre.Visible = False
    Sair1.Visible = False
        '2�bloco
    Consulta.Visible = True
    Imprimir.Visible = True
    Confirma.Visible = True
    Cancela.Visible = True
    Sair2.Visible = True
        '3� bloco
    Sair3.Visible = False

'FIM DO TRATAMENTO DE MENUS

   
End Sub

Private Sub Cancela_Click() 'menu
    Limpa_Campos   ' limpa campos da tela
    txtCodUf.Enabled = True
    fraManutencao.Caption = "Cadastro de UF"
    txtCodUf.SetFocus
        
End Sub

Private Sub cbomacro_LostFocus()
    FORMCADUF.Refresh
End Sub

Private Sub Confirma_Click()    'menu
    
    Checa_Campos   ' confere se os campos sao validos
    
        If fl_erro = True Then
            Exit Sub
        End If
        
        db.Parameters.Remove "Cod_ErrorPl"
        db.Parameters.Remove "Txt_ErrorPl"
        db.Parameters.Remove "COD_UF"
        db.Parameters.Remove "DESC_UF"
        db.Parameters.Remove "DT_ALIQ_INTERNA"
        db.Parameters.Remove "MACRO"
        db.Parameters.Add "Cod_ErrorPl", 0, 2
        db.Parameters.Add "Txt_ErrorPl", "", 2
        db.Parameters.Add "COD_UF", txtCodUf.Text, 1
        db.Parameters.Add "DESC_UF", txtDescUf.Text, 1
        db.Parameters.Add "DT_ALIQ_INTERNA", txtDtAliq.Text, 1
        db.Parameters.Add "MACRO", cboMacro.Text, 1
        CmdSql = "BEGIN "
            
        If fl_alteracao = True Then  ' CASO SEJA ALTERACAO
            CmdSql = CmdSql & "UPDATE UF SET " & _
                     "DESC_UF = :DESC_UF, " & _
                     "DT_ALIQ_INTERNA = to_date(:DT_ALIQ_INTERNA,'DD/MM/RR'), " & _
                     "MACRO = :MACRO WHERE COD_UF = :COD_UF; "
            CmdSql = CmdSql & "Commit; " & _
                     "Exception " & _
                     "When Others then " & _
                     "RollBack; " & _
                     ":Cod_ErrorPl := SQLCODE; " & _
                     ":Txt_ErrorPl := SQLERRM; " & _
                     "End;"
            
            Dim r As String
            r = MsgBox("Confirma Altera��o?", vbYesNo + vbQuestion)
                
           'SE N�O CONFIRMA ALTERA��O, LIMPA CAMPOS E SAI
            If r = vbNo Then
                Limpa_Campos
                Exit Sub
            Else
               'MsgBox (CmdSql)
                Screen.MousePointer = 11
                db.ExecuteSQL CmdSql
            End If
            
            If Val(db.Parameters("Cod_ErrorPl")) <> 0 Then
                Screen.MousePointer = 0
                MsgBox "Erro - " & db.Parameters("Txt_ErrorPl") & ". Favor avisar Depto. de Sistemas!", vbExclamation, MSG_TIT
            Else
                Screen.MousePointer = 0
                MsgBox "Altera��o OK!", vbExclamation, MSG_TIT
                Limpa_Campos
            End If
        Else    'caso seja inclusao
            CmdSql = CmdSql & " INSERT INTO UF (COD_UF, DESC_UF, " & _
                     "DT_ALIQ_INTERNA, MACRO)  VALUES(:COD_UF, :DESC_UF, " & _
                     "to_date(:DT_ALIQ_INTERNA,'DD/MM/RR'), :MACRO);"
            CmdSql = CmdSql & "Commit; " & _
                     "Exception " & _
                     "When Others then " & _
                     "RollBack; " & _
                     ":Cod_ErrorPl := SQLCODE; " & _
                     ":Txt_ErrorPl := SQLERRM; " & _
                     "End;"
                              
            r = MsgBox("Confirma Inclus�o?", vbYesNo + vbQuestion)
                
           'SE N�O CONFIRMA ALTERA��O, LIMPA CAMPOS E SAI
            If r = vbNo Then
                Limpa_Campos
                Exit Sub
            Else
               'MsgBox (CmdSql)
                Screen.MousePointer = 11
                db.ExecuteSQL CmdSql
            End If
            
            If Val(db.Parameters("Cod_ErrorPl")) <> 0 Then
                Screen.MousePointer = 0
                'MsgBox "Erro - " & db.Parameters("Txt_ErrorPl") & ". Favor avisar Depto. de Sistemas!", vbExclamation, MSG_TIT
            Else
                Screen.MousePointer = 0
                MsgBox "INCLUS�O OK!", vbExclamation, MSG_TIT
                Limpa_Campos
            End If
                              
        End If
        
        db.Parameters.Remove "Cod_ErrorPl"
        db.Parameters.Remove "Txt_ErrorPl"
        db.Parameters.Remove "COD_UF"
        db.Parameters.Remove "DESC_UF"
        db.Parameters.Remove "DT_ALIQ_INTERNA"
        db.Parameters.Remove "MACRO"
End Sub

Private Sub fraManutencao_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)

If Button = vbRightButton Then
    FORMCADUF.PopupMenu Popup1
End If

End Sub

Private Sub Grid1_DblClick()
    
    Grid1.Col = 0   ' caso de dois clicks em algum codigo ele volta para a tela de manutencao
    txtCodUf.Text = Grid1.Text
    
    Toolbar1.Visible = False
    Toolbar3.Visible = False    'deixa os campos referentes  a consulta
    Grid1.Visible = False       'invisiveis
    fraManutencao.Visible = True
    Toolbar2.Visible = True
    'MENUS:
    
        '1� BLOCO
    Cadastro.Visible = False
    Sobre.Visible = False
    Sair1.Visible = False
        '2�bloco
    Consulta.Visible = True
    Imprimir.Visible = True
    Confirma.Visible = True
    Cancela.Visible = True
    Sair2.Visible = True
        '3� bloco
    Sair3.Visible = False

'FIM DO TRATAMENTO DE MENUS
    
    FORMCADUF.Caption = "Manuten��o de UF"
    txtCodUf_LostFocus
End Sub

Private Sub Form_Load()
    
    On Error GoTo TRATA_ERRO
    
    MSG_TIT = "Cadastro de UF"
    
    If App.PrevInstance = True Then
        MsgBox "J� existe uma inst�ncia do programa no ar.", vbExclamation, MSG_TIT
        End
    End If

    'posicionar tela
    'WindowState = 2
   
    '//////////////////////////////////////////
    '///  ESTABELECE CONEx�O COM O ORACLE  ///
    '//////////////////////////////////////////

   Set dbOra = CreateObject("oracleinproCServer.xorasession")
   Set db = dbOra.OpenDatabase("PRODUCAO", "CAD090/PROD", 0&)
   'Set db = dbOra.OpenDatabase("DESENV", "PRODUCAO/DES", 0&)

    '8 -> PARA UMA LINHA
    '0 -> PARA VARIAS LINHAS
    
    
'MENUS:

        '2�bloco
    Consulta.Visible = False
    Imprimir.Visible = False
    Confirma.Visible = False
    Cancela.Visible = False
    Sair2.Visible = False
        '3� bloco
    Sair3.Visible = False

'FIM DO TRATAMENTO DE MENUS

Exit Sub
   
TRATA_ERRO:
    Call Process_Line_Errors


End Sub

Private Sub Imprimir_Click()    'menu
            
        txtCodUf_LostFocus   ' executa o lost focus do codigo
            
        If fl_alteracao = True Then   ' caso tenha algum codigo valido
            
            FORMCADUF.PrintForm   'imprime o formulario
        Else  ' caso o codigo nao seja valido
                MsgBox "Para Imprimir e Necess�rio Digitar um C�digo V�lido", vbCritical, MSG_TIT
        End If
        
End Sub

Private Sub Consulta_Click()    'menu
    Dim Pesquisa As String
    
   'caixa para se digitar parte da string para a busca por descricao
    Pesquisa = UCase(InputBox("Digite Descri��o da UF ou Parte da Mesma"))
    Pesquisa = "%" + Pesquisa + "%"
    
    If Trim(Pesquisa) = "" Then   'caso nao se tenha digitado nada
        Exit Sub
    Else
        Screen.MousePointer = 11
        
        db.Parameters.Remove "DESC_UF"
        db.Parameters.Add "DESC_UF", Pesquisa, 1
        
        CmdSql = "SELECT * FROM UF WHERE DESC_UF LIKE :DESC_UF ORDER BY DESC_UF"
        
        Set db_CONS = db.dbcreatedynaset(CmdSql, 0&)
        db.Parameters.Remove "DESC_UF"
       
        If db_CONS.EOF = True Then
            Screen.MousePointer = 0
            MsgBox "N�o Exite Nenhuma UF com Essa Descri��o", vbExclamation, MSG_TIT
        Else  'caso encontre algum codigo valido
            
            fraManutencao.Visible = False  'deixa os campos referentes a consulta visiveis
            Toolbar2.Visible = False
            Toolbar3.Visible = True
            Grid1.Visible = True
            
            'MENUS:
    
                    '1� BLOCO
                Cadastro.Visible = False
                Sobre.Visible = False
                Sair1.Visible = False
                    '2�bloco
                Imprimir.Visible = False
                Confirma.Visible = False
                Cancela.Visible = False
                Sair2.Visible = False
                    '3� bloco
                Sair3.Visible = True
            'FIM DO TRATAMENTO DE MENUS

            'monta o grid na tela
            fraManutencao.Caption = "Consulta UF" ''
            Grid1.Rows = db_CONS.recordcount + 1
            
            Dim I As Integer
            For I = 1 To db_CONS.recordcount Step 1 'carrega o grid com as informacoes do select
                Grid1.Row = I
                Grid1.Col = 0
                Grid1.Text = CStr(db_CONS!COD_UF)
                Grid1.Col = 1
                Grid1.Text = CStr(db_CONS!DESC_UF)
                Grid1.Col = 2
                Grid1.Text = CStr(db_CONS!DT_ALIQ_INTERNA)
                Grid1.Col = 3
                Grid1.Text = CStr(db_CONS!MACRO)
                db_CONS.MoveNext
            Next
        End If
        
    End If
    Screen.MousePointer = 0
End Sub

Private Sub PopupCancelar_Click()
Call Cancela_Click
End Sub

Private Sub PopupConfirma_Click()
Call Confirma_Click
End Sub

Private Sub Sair1_Click()   'menu
    End
End Sub

Private Sub Sair2_Click()   'menu
    Toolbar1.Visible = True
    fraManutencao.Visible = False
    Toolbar2.Visible = False
    Toolbar3.Visible = False
    
    'MENUS:
    
        '1� BLOCO
    Cadastro.Visible = True
    Sobre.Visible = True
    Sair1.Visible = True
        '2�bloco
    Consulta.Visible = False
    Imprimir.Visible = False
    Confirma.Visible = False
    Cancela.Visible = False
    Sair2.Visible = False
        '3� bloco
    Sair3.Visible = False

'FIM DO TRATAMENTO DE MENUS

End Sub

Private Sub Sair3_Click()   'menu
    Toolbar1.Visible = False
    Toolbar3.Visible = False    'deixa os campos referentes  a consulta
    Grid1.Visible = False       'invisiveis
    fraManutencao.Visible = True
    fraManutencao.Caption = ""
    Toolbar2.Visible = True
    FORMCADUF.Caption = "Manuten��o de UF"
    
    'MENUS:
    
        '1� BLOCO
    Cadastro.Visible = False
    Sobre.Visible = False
    Sair1.Visible = False
        '2�bloco
    Consulta.Visible = True
    Imprimir.Visible = True
    Confirma.Visible = True
    Cancela.Visible = True
    Sair2.Visible = True
        '3� bloco
    Sair3.Visible = False

'FIM DO TRATAMENTO DE MENUS

End Sub

Private Sub Sobre_Click()   'menu
    FORMSOBRE.Show vbModal
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case Button.Key
    
        Case "TBCadastro"
            Call CadastrarUF_Click
        
        Case "TBSobre"
            Call Sobre_Click
            
        Case "TBSair"

            Call Sair1_Click
              
    End Select
End Sub

Private Sub Toolbar2_ButtonClick(ByVal Button As MSComctlLib.Button)
    
    Select Case Button.Key
    
        Case "TBConsultar"
        
           Call Consulta_Click
        
        Case "TBImprimir"
        
            Call Imprimir_Click
            
        Case "TBConfirma"
        
            Call Confirma_Click
        
        Case "TBCancela"
            
            Call Cancela_Click
        
        Case "TBVoltar"
        
            Call Sair2_Click
        
        End Select
        
    
End Sub

Private Sub Toolbar3_ButtonClick(ByVal Button As MSComctlLib.Button)
        
       Select Case Button.Key
        
            Case "TBVoltar2"
                
                Call Sair3_Click
                
        End Select
                       
End Sub

Private Sub txtCodUf_GotFocus()

txtCodUf.SelStart = 0
txtCodUf.SelLength = 5

End Sub

Private Sub txtCodUf_KeyPress(KeyAscii As Integer)
    
    KeyAscii = Maiusculo(KeyAscii)
    
End Sub

Private Sub txtCodUf_LostFocus()

If Trim(txtCodUf.Text) <> "" Then   'caso tenha se digitado alguma coisa
    
    db.Parameters.Remove "COD_UF"
    db.Parameters.Add "COD_UF", txtCodUf.Text, 1
    
    CmdSql = "select * from UF WHERE COD_UF = :COD_UF"
    
    Set db_CONS = db.dbcreatedynaset(CmdSql, 0&)
    
    db.Parameters.Remove "COD_UF"
    
    If Not db_CONS.EOF Then    'caso tenha achado algum trata-se de uma alteracao
        txtCodUf.Enabled = False
        fl_alteracao = True
        fraManutencao.Caption = "ALTERA��O"
        txtDescUf.Text = CStr(db_CONS!DESC_UF)
        txtDtAliq.Text = CStr(db_CONS!DT_ALIQ_INTERNA)
        cboMacro.Text = CStr(db_CONS!MACRO)
    Else    '  caso nao tenha achado  trata-se de uma inclusao
        fl_alteracao = False
        fraManutencao.Caption = "INCLUS�O"
        txtDescUf.Text = ""
        txtDtAliq.Text = ""
        cboMacro.Text = " "
    End If

End If

End Sub

Private Sub txtDescUf_GotFocus()

txtDescUf.SelStart = 0
txtDescUf.SelLength = 20

End Sub

Private Sub txtDescUf_KeyPress(KeyAscii As Integer)
    KeyAscii = Maiusculo(KeyAscii)
End Sub

Private Sub txtDtAliq_KeyPress(KeyAscii As Integer)
    Data KeyAscii, txtDtAliq
End Sub
