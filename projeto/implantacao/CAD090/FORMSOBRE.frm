VERSION 5.00
Begin VB.Form FORMSOBRE 
   Appearance      =   0  'Flat
   BackColor       =   &H80000000&
   Caption         =   "INFORMAÇÕES SOBRE O PROGRAMA:"
   ClientHeight    =   3795
   ClientLeft      =   3750
   ClientTop       =   2730
   ClientWidth     =   3390
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   ScaleHeight     =   3795
   ScaleWidth      =   3390
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command1 
      Caption         =   "OK!"
      Height          =   495
      Left            =   1073
      TabIndex        =   7
      ToolTipText     =   "SAIR DAS INFORMAÇÕES:"
      Top             =   3240
      Width           =   1245
   End
   Begin VB.PictureBox Picture1 
      Height          =   1455
      Left            =   833
      Picture         =   "FORMSOBRE.frx":0000
      ScaleHeight     =   1395
      ScaleWidth      =   1665
      TabIndex        =   0
      Top             =   60
      Width           =   1725
   End
   Begin VB.Label Label6 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "supsist@dpk.com.br"
      BeginProperty Font 
         Name            =   "Lucida Console"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   60
      TabIndex        =   6
      Top             =   2880
      Width           =   3285
   End
   Begin VB.Label Label5 
      Caption         =   "CONTATO:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   165
      Left            =   60
      TabIndex        =   5
      Top             =   2670
      Width           =   945
   End
   Begin VB.Label Label4 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "RENATO"
      BeginProperty Font 
         Name            =   "Lucida Console"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   60
      TabIndex        =   4
      Top             =   2340
      Width           =   3285
   End
   Begin VB.Label Label3 
      Caption         =   "RESPONSÁVEL:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   165
      Left            =   60
      TabIndex        =   3
      Top             =   2130
      Width           =   1395
   End
   Begin VB.Label Label2 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "CAD090-CADASTRO DE UF"
      BeginProperty Font 
         Name            =   "Lucida Console"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   60
      TabIndex        =   2
      Top             =   1800
      Width           =   3285
   End
   Begin VB.Label Label1 
      Caption         =   "PROGRAMA:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   165
      Left            =   60
      TabIndex        =   1
      Top             =   1590
      Width           =   1125
   End
   Begin VB.Menu Sair 
      Caption         =   "&Sair"
   End
End
Attribute VB_Name = "FORMSOBRE"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Command1_Click()
    Unload Me
End Sub

Private Sub Sair_Click()
    Unload Me
End Sub
