VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.MDIForm MDIForm1 
   BackColor       =   &H8000000C&
   Caption         =   "CAD820 - CADASTRO DE SENHA PARA ACESSO A REFERENCIAS COMERCIAIS VIA INTERNET"
   ClientHeight    =   5940
   ClientLeft      =   2355
   ClientTop       =   2640
   ClientWidth     =   6690
   LinkTopic       =   "MDIForm1"
   LockControls    =   -1  'True
   WindowState     =   2  'Maximized
   Begin Threed.SSPanel SSPanel1 
      Align           =   1  'Align Top
      Height          =   645
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6690
      _Version        =   65536
      _ExtentX        =   11800
      _ExtentY        =   1138
      _StockProps     =   15
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin Threed.SSCommand SSCommand3 
         Height          =   525
         Left            =   825
         TabIndex        =   4
         Top             =   60
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   926
         _StockProps     =   78
         ForeColor       =   -2147483630
         Picture         =   "MDIForm1.frx":0000
      End
      Begin Threed.SSCommand sssobre 
         Height          =   525
         Left            =   1530
         TabIndex        =   3
         Top             =   60
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   926
         _StockProps     =   78
         Picture         =   "MDIForm1.frx":031A
      End
      Begin Threed.SSCommand SSCommand2 
         Height          =   525
         Left            =   2220
         TabIndex        =   2
         Top             =   60
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   926
         _StockProps     =   78
         Picture         =   "MDIForm1.frx":0634
      End
      Begin Threed.SSCommand SSCommand1 
         Height          =   525
         Left            =   120
         TabIndex        =   1
         Top             =   60
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   926
         _StockProps     =   78
         Picture         =   "MDIForm1.frx":094E
      End
   End
End
Attribute VB_Name = "MDIForm1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub MDIForm_Load()
Dim lstrsql
If App.PrevInstance Then
    MsgBox "J� EXISTE UMA INST�NCIA DO PROGRAMA NO AR.", , MSG_TIT
    End
End If

Set DBORA = CreateObject("oracleinproCServer.xorasession")
'Set DB = DBORA.OpenDatabase("desenv", "PRODUCAO/des", 0&)
Set DB = DBORA.OpenDatabase("producao", "CAD820/prod", 0&)

End Sub


Private Sub SSCommand1_Click()
   FrmCad.Show
   FrmCad.Label1.Visible = True
   FrmCad.Label2.Visible = True
   FrmCad.Label3.Visible = True
   FrmCad.Label4.Visible = True
   FrmCad.Label5.Visible = True
   FrmCad.Label6.Visible = True
   FrmCad.Label7.Visible = True
   FrmCad.Label8.Visible = True
   FrmCad.Label9.Visible = True
   FrmCad.TXTCGC.Visible = True
   FrmCad.TXTRAZAO.Visible = True
   FrmCad.TXTENDERECO.Visible = True
   FrmCad.CBOUF.Visible = True
   FrmCad.TXTDDD.Visible = True
   FrmCad.TXTFONE.Visible = True
   FrmCad.TXTCONTATO.Visible = True
   FrmCad.TXTEMAIL.Visible = True
   FrmCad.CBOSITUACAO.Visible = True
End Sub

Private Sub SSCommand2_Click()
End
End Sub


Private Sub SSCommand3_Click()
FrmCons.Show

'Alterada a ordena��o, conf. solicita��o da Ros�ngela/Cr�dito em 30/04/07 - Aleth�a
'         " order by cgc "

strsql = " select * from " & _
         " internet.usuario_refer " & _
         " order by RAZAO_SOCIAL "


Set ObjSel = DB.dbcreatedynaset(strsql, 0&)

If Not ObjSel.EOF Then
   FrmCons.Grd_Cons.Row = 0
   FrmCons.Grd_Cons.Col = 0
   FrmCons.Grd_Cons.ColWidth(0) = 1400
   FrmCons.Grd_Cons.FixedAlignment(0) = 2
   FrmCons.Grd_Cons.Text = "CGC"
   FrmCons.Grd_Cons.Col = 1
   FrmCons.Grd_Cons.ColWidth(1) = 4000
   FrmCons.Grd_Cons.FixedAlignment(1) = 2
   FrmCons.Grd_Cons.Text = "Raz�o Social"
   FrmCons.Grd_Cons.Col = 2
   FrmCons.Grd_Cons.ColWidth(2) = 4000
   FrmCons.Grd_Cons.FixedAlignment(2) = 2
   FrmCons.Grd_Cons.Text = "E-Mail"
   FrmCons.Grd_Cons.Col = 3
   FrmCons.Grd_Cons.ColWidth(3) = 1000
   FrmCons.Grd_Cons.FixedAlignment(3) = 2
   FrmCons.Grd_Cons.Text = "Situacao"
   FrmCons.Grd_Cons.Col = 4
   FrmCons.Grd_Cons.ColWidth(4) = 1000
   FrmCons.Grd_Cons.FixedAlignment(4) = 2
   FrmCons.Grd_Cons.Text = "Senha"
End If

Do While Not ObjSel.EOF
   DoEvents
   senha = ObjSel.fields("Senha")
   If Len(senha) > 0 Then
      Call DESCRIPTOGRAFAR(senha, SENHA_DES)
      DoEvents
   End If
   FrmCons.Grd_Cons.AddItem ObjSel.fields("cgc") & Chr(9) & ObjSel.fields("razao_social") & _
   Chr(9) & ObjSel.fields("e_mail") & Chr(9) & ObjSel.fields("situacao") & _
   Chr(9) & SENHA_DES
       
   ObjSel.MoveNext
Loop
If ObjSel.EOF Then
   FrmCons.Grd_Cons.RemoveItem 1
End If

FrmCons.Grd_Cons.Visible = True
End Sub

Private Sub sssobre_Click()
FrmSobre.Show
End Sub


