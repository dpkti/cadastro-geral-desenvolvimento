VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Begin VB.Form FrmCons 
   Caption         =   "CONSULTA"
   ClientHeight    =   5940
   ClientLeft      =   495
   ClientTop       =   1590
   ClientWidth     =   10905
   LinkTopic       =   "Form1"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5940
   ScaleWidth      =   10905
   Begin Threed.SSCommand SSCommand3 
      Height          =   525
      Left            =   450
      TabIndex        =   1
      Top             =   5325
      Width           =   615
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   926
      _StockProps     =   78
      Picture         =   "FrmCons.frx":0000
   End
   Begin MSGrid.Grid Grd_Cons 
      Height          =   4275
      Left            =   390
      TabIndex        =   0
      Top             =   675
      Visible         =   0   'False
      Width           =   10035
      _Version        =   65536
      _ExtentX        =   17701
      _ExtentY        =   7541
      _StockProps     =   77
      BackColor       =   16777215
      Cols            =   5
   End
End
Attribute VB_Name = "FrmCons"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Grd_Cons_Click()
On Error GoTo Trata_Erro
    
    Grd_Cons.Col = 0
    CGC = Grd_Cons.Text
    strsql = " Select * from " & _
             " Internet.usuario_refer " & _
             " Where cgc = '" & CGC & "'"
    
    Set ObjSel = DB.dbcreatedynaset(strsql, 0&)

    If Not ObjSel.EOF Then
       FrmCad.Show
       FrmCad.TXTCGC = ObjSel!CGC
       FrmCad.TXTRAZAO = ObjSel!RAZAO_SOCIAL
       FrmCad.TXTENDERECO = ObjSel!endereco
       FrmCad.TXTDDD = ObjSel!ddd
       FrmCad.TXTFONE = ObjSel!fone
       FrmCad.TXTCONTATO = ObjSel!contato
       FrmCad.TXTEMAIL = ObjSel!e_mail
       FrmCad.CBOSITUACAO = IIf(ObjSel!situacao = 0, "0 - Ativo", "9 - Desativado")
       If Len(ObjSel!senha) > 0 Then
          FrmCad.txtSenha.Visible = True
          FrmCad.lblSenha.Visible = True
          Call DESCRIPTOGRAFAR(ObjSel!senha, SENHA_DES)
          FrmCad.txtSenha = SENHA_DES
       Else
          FrmCad.txtSenha.Visible = False
          FrmCad.lblSenha.Visible = False
       End If
       
       
       'Busca Uf
       Cod_Cid = ObjSel!Cod_Cidade
       Sql = "Select cod_uf,nome_cidade from cidade " & _
             " Where cod_cidade= " & Left(Cod_Cid, 4)
       Set ObjSel = DB.dbcreatedynaset(Sql, 0&)
       FrmCad.CBOUF = ObjSel!cod_uf
       FrmCad.CBOCIDADE = ObjSel!Nome_Cidade
    End If


Exit Sub

Trata_Erro:
If Err <> 0 Then

   MsgBox Err & " - " & Err.Description & Chr(10) & _
          " ENTRE EM CONTATO COM O DEPTO SUPORTE ", vbExclamation, "ATEN��O"
   Resume Next
   
End If


End Sub

Private Sub SSCommand3_Click()
Unload Me
End Sub


