VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmCad 
   Caption         =   "CADASTRO"
   ClientHeight    =   5940
   ClientLeft      =   1560
   ClientTop       =   1515
   ClientWidth     =   9240
   ClipControls    =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5940
   ScaleWidth      =   9240
   Begin VB.ComboBox CBOSITUACAO 
      ForeColor       =   &H000000C0&
      Height          =   315
      Left            =   1755
      TabIndex        =   17
      Top             =   3570
      Width           =   2070
   End
   Begin VB.TextBox TXTSENHA 
      BackColor       =   &H00C0C0FF&
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   300
      Left            =   4305
      MaxLength       =   12
      TabIndex        =   21
      Top             =   210
      Width           =   1080
   End
   Begin VB.ComboBox CBOCIDADE 
      Height          =   315
      Left            =   1755
      TabIndex        =   12
      Top             =   1890
      Width           =   3090
   End
   Begin VB.ComboBox CBOUF 
      Height          =   315
      Left            =   1770
      TabIndex        =   11
      Top             =   1485
      Width           =   975
   End
   Begin VB.TextBox TXTEMAIL 
      Height          =   315
      Left            =   1755
      MaxLength       =   40
      TabIndex        =   16
      Top             =   3150
      Width           =   4380
   End
   Begin VB.TextBox TXTCONTATO 
      Height          =   285
      Left            =   1755
      MaxLength       =   15
      TabIndex        =   15
      Top             =   2745
      Width           =   1815
   End
   Begin VB.TextBox TXTFONE 
      Height          =   300
      Left            =   2745
      MaxLength       =   9
      TabIndex        =   14
      Top             =   2310
      Width           =   1665
   End
   Begin VB.TextBox TXTDDD 
      Height          =   285
      Left            =   1755
      MaxLength       =   4
      TabIndex        =   13
      Top             =   2325
      Width           =   780
   End
   Begin VB.TextBox TXTENDERECO 
      Height          =   300
      Left            =   1755
      MaxLength       =   33
      TabIndex        =   10
      Top             =   1020
      Width           =   3660
   End
   Begin VB.TextBox TXTRAZAO 
      Height          =   285
      Left            =   1755
      MaxLength       =   30
      TabIndex        =   9
      Top             =   615
      Width           =   3270
   End
   Begin VB.TextBox TXTCGC 
      Height          =   300
      Left            =   1785
      MaxLength       =   14
      TabIndex        =   8
      Top             =   225
      Width           =   1515
   End
   Begin Threed.SSCommand SSCommand5 
      Height          =   525
      Left            =   945
      TabIndex        =   24
      Top             =   5175
      Width           =   615
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   926
      _StockProps     =   78
      Picture         =   "frmcad.frx":0000
   End
   Begin Threed.SSCommand SSCommand1 
      Height          =   525
      Left            =   225
      TabIndex        =   19
      Top             =   5175
      Width           =   615
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   926
      _StockProps     =   78
      Picture         =   "frmcad.frx":031A
   End
   Begin Threed.SSCommand SSCommand3 
      Height          =   525
      Left            =   2490
      TabIndex        =   18
      Top             =   5190
      Width           =   615
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   926
      _StockProps     =   78
      Picture         =   "frmcad.frx":0634
   End
   Begin VB.Label Label10 
      Caption         =   "Situa��o = 0 --> Tem Acesso a Internet,       Situa��o = 9 --> Cancela o Acesso a Internet"
      ForeColor       =   &H00800000&
      Height          =   465
      Left            =   4440
      TabIndex        =   23
      Top             =   3540
      Width           =   3450
   End
   Begin VB.Label Label9 
      Caption         =   "SITUA��O"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   90
      TabIndex        =   22
      Top             =   3660
      Width           =   1215
   End
   Begin VB.Label LBLSENHA 
      Caption         =   "SENHA"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   285
      Left            =   3465
      TabIndex        =   20
      Top             =   300
      Width           =   765
   End
   Begin VB.Label Label8 
      Caption         =   "E-MAIL"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   270
      Left            =   135
      TabIndex        =   7
      Top             =   3240
      Width           =   870
   End
   Begin VB.Label Label7 
      Caption         =   "CONTATO"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   240
      Left            =   120
      TabIndex        =   6
      Top             =   2835
      Width           =   1095
   End
   Begin VB.Label Label6 
      Caption         =   "FONE"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   120
      TabIndex        =   5
      Top             =   2400
      Width           =   1095
   End
   Begin VB.Label Label5 
      Caption         =   "CIDADE"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   105
      TabIndex        =   4
      Top             =   1950
      Width           =   840
   End
   Begin VB.Label Label4 
      Caption         =   "UF"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   210
      Left            =   135
      TabIndex        =   3
      Top             =   1485
      Width           =   495
   End
   Begin VB.Label Label3 
      Caption         =   "ENDERE�O"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   1080
      Width           =   1230
   End
   Begin VB.Label Label2 
      Caption         =   "RAZ�O SOCIAL"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   105
      TabIndex        =   1
      Top             =   660
      Width           =   1515
   End
   Begin VB.Label Label1 
      Caption         =   "CGC"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   375
      Left            =   105
      TabIndex        =   0
      Top             =   255
      Width           =   615
   End
End
Attribute VB_Name = "FrmCad"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CBOCIDADE_GotFocus()
If Len(CBOUF) > 0 Then
   strsql = " Select cod_cidade, nome_cidade " & _
          " From cidade " & _
          " Where cod_uf = '" & CBOUF & "' " & _
          " Order by nome_cidade "

   Set ObjSel = DB.dbcreatedynaset(strsql, 0&)
   CBOCIDADE.Clear
   While Not ObjSel.EOF
         CBOCIDADE.AddItem Format(ObjSel.fields("cod_cidade"), "0000") & "-" & ObjSel.fields("nome_cidade")
         ObjSel.MoveNext
   Wend
End If
FrmCad.Refresh
End Sub


Private Sub CBOSITUACAO_GotFocus()
CBOSITUACAO.AddItem "0 - Ativo"
CBOSITUACAO.AddItem "9 - Desativado"

End Sub


Private Sub CBOUF_GotFocus()
'CARREGANDO COMBO DE UF
lstrsql = "SELECT cod_uf FROM uf "

Set ObjSel = DB.dbcreatedynaset(lstrsql, 0&)
CBOUF.Clear

CBOSITUACAO.Clear
While Not ObjSel.EOF
    CBOUF.AddItem ObjSel.fields("cod_uf")
    ObjSel.MoveNext
Wend
FrmCad.Refresh
End Sub


Private Sub Form_Load()
 TXTSENHA.Visible = False
 LBLSENHA.Visible = False
End Sub


Private Sub GERA_SENHA(senha)

    senha = Left("000000", 6 - Len(senha)) & senha
    s = Len(senha)
    a = 0
    ret = ""
    For i = 0 To s - 1
        a = a + (i + 2) * Mid(senha, s - i, 1)
        ret = ((11 - (a Mod 11)) Mod 10) & ret
    Next

'Rotina para Gera��o de Senha
senha = ""
senha = ret
TXTSENHA = UCase(senha)
Call CRIPTOGRAFAR(senha, SENHA_CRI)


'Gravar a Senha na Tabela de Usu�rio

Sql = "BEGIN Update Internet.Usuario_Refer " & _
      " Set senha = :senha " & _
      " Where cgc = :cgc; " & _
      " COMMIT;" & _
      " EXCEPTION WHEN OTHERS" & _
      " THEN ROLLBACK; " & _
      " :cod_errora := SQLCODE;" & _
      " :txt_errora := SQLERRM;" & _
      " END;"

      
DB.Parameters.Remove "SENHA"
DB.Parameters.Add "SENHA", SENHA_CRI, 1
DB.Parameters.Remove "CGC"
DB.Parameters.Add "CGC", TXTCGC, 1
      
DB.ExecuteSQL Sql
   
If Val(DB.Parameters("cod_errora")) <> 0 Then
   MsgBox DB.Parameters("COD_ERRORA") & DB.Parameters("TXT_ERRORA")
   MsgBox " Problemas com a Grava��o da Senha! "
End If
  

'***
'Mostra Senha Gerada na tela

MsgBox " A senha do Usu�rio: " & CGC & " � :" & TXTSENHA, vbInformation, "SENHA GERADA"
TXTSENHA.Visible = True
LBLSENHA.Visible = True


End Sub
Private Sub Grd_Cons_Click()
Grd_Cons.Col = 0
CGC = Grd_Cons.Text
    
    strsql = " Select * from " & _
             " Internet.usuario_refer " & _
             " Where cgc = '" & CGC & "'"
    
    Set ObjSel = DB.dbcreatedynaset(strsql, 0&)

    If Not ObjSel.EOF Then
       Label1.Visible = True
       Label2.Visible = True
       Label3.Visible = True
       Label4.Visible = True
       Label5.Visible = True
       Label6.Visible = True
       Label7.Visible = True
       Label8.Visible = True
       Label9.Visible = True
       TXTCGC.Visible = True
       TXTRAZAO.Visible = True
       TXTENDERECO.Visible = True
       CBOUF.Visible = True
       CBOCIDADE.Visible = True
       TXTDDD.Visible = True
       TXTFONE.Visible = True
       TXTCONTATO.Visible = True
       TXTEMAIL.Visible = True
       CBOSITUACAO.Visible = True
       LBLSENHA.Visible = True
       TXTSENHA.Visible = True
        
       Grd_Cons.Visible = False
       TXTCGC = CGC
       TXTRAZAO = ObjSel!RAZAO_SOCIAL
       TXTENDERECO = ObjSel!endereco
       CBOCIDADE = ObjSel!Cod_Cidade
       TXTDDD = ObjSel!ddd
       TXTFONE = ObjSel!fone
       TXTCONTATO = ObjSel!contato
       TXTEMAIL = ObjSel!e_mail
       CBOSITUACAO = IIf(ObjSel!situacao = 0, "0 - Ativo", "9 - Desativado")
       If Len(ObjSel!senha) > 0 Then
          TXTSENHA.Visible = True
          LBLSENHA.Visible = True
          TXTSENHA = ObjSel!senha
       Else
          TXTSENHA.Visible = False
          LBLSENHA.Visible = False
       End If
       'Busca Uf
    End If

Exit Sub

Trata_Erro:
If Err <> 0 Then

   MsgBox Err & " - " & Err.Description & Chr(10) & _
          " ENTRE EM CONTATO COM O DEPTO SUPORTE ", vbExclamation, "ATEN��O"
   Resume Next
   
End If



End Sub
'TI-5697
Private Sub SSCommand1_Click()

On Error GoTo Trata_Erro

If Len(TXTCGC) > 0 Then
    strsql = " select * from " & _
             " internet.usuario_refer " & _
             " where cgc= '" & TXTCGC & "'"
             
    Set ObjSel = DB.dbcreatedynaset(strsql, 0&)
             
    If ObjSel.EOF Then
    
       strsql = " BEGIN Insert into " & _
                " Internet.usuario_refer " & _
                " values(:cgc,:razao,:ender,:cid,:ddd,:fone,:cont,:email,:sit,producao.seq_senha.nextval);" & _
                " COMMIT;" & _
                " EXCEPTION WHEN OTHERS" & _
                " THEN ROLLBACK; " & _
                " :cod_errora := SQLCODE;" & _
                " :txt_errora := SQLERRM;" & _
                " END;"
                 
        DB.Parameters.Remove "razao"
        DB.Parameters.Add "razao", TXTRAZAO, 1
        DB.Parameters.Remove "ender"
        DB.Parameters.Add "ender", TXTENDERECO, 1
        DB.Parameters.Remove "cid"
        DB.Parameters.Add "cid", Val(CBOCIDADE), 1
        DB.Parameters.Remove "ddd"
        DB.Parameters.Add "ddd", Val(TXTDDD), 1
        DB.Parameters.Remove "fone"
        DB.Parameters.Add "fone", Val(TXTFONE), 1
        DB.Parameters.Remove "cont"
        DB.Parameters.Add "cont", TXTCONTATO, 1
        DB.Parameters.Remove "email"
        DB.Parameters.Add "email", TXTEMAIL, 1
        DB.Parameters.Remove "cgc"
        DB.Parameters.Add "cgc", Val(TXTCGC), 1
        DB.Parameters.Remove "sit"
        DB.Parameters.Add "sit", Val(CBOSITUACAO), 1
        DB.Parameters.Remove "senha"
        DB.Parameters.Add "senha", "", 1
        DB.Parameters.Remove "cod_errora"
        DB.Parameters.Add "cod_errora", 0, 2
        DB.Parameters.Remove "txt_errora"
        DB.Parameters.Add "txt_errora", "", 2
             
        DB.ExecuteSQL strsql
        
        If Val(DB.Parameters("cod_errora")) <> 0 Then
           MsgBox DB.Parameters("COD_ERRORA") & DB.Parameters("TXT_ERRORA")
           MsgBox " Problemas com a Inclus�o "
        Else
            'Buscar Sequence para gera��o de Senha
            Sql = "Select senha from  internet.usuario_refer where cgc= " & TXTCGC
            Set ObjSel = DB.dbcreatedynaset(Sql, 0&)
            senha = ObjSel!senha

            Call GERA_SENHA(senha)
        End If
        
    Else
       strsql = " BEGIN Update internet.usuario_refer " & _
                " set razao_social = :razao, endereco = :ender, cod_cidade = :cid, ddd = :ddd, " & _
                " fone = :fone, contato = :cont, e_mail=:email, situacao=:sit " & _
                " where cgc=:cgc; " & _
                " COMMIT;" & _
                " END;"
            
        DB.Parameters.Remove "razao"
        DB.Parameters.Add "razao", TXTRAZAO, 1
        DB.Parameters.Remove "ender"
        DB.Parameters.Add "ender", TXTENDERECO, 1
        DB.Parameters.Remove "cid"
        DB.Parameters.Add "cid", Val(CBOCIDADE), 1
        DB.Parameters.Remove "ddd"
        DB.Parameters.Add "ddd", Val(TXTDDD), 1
        DB.Parameters.Remove "fone"
        DB.Parameters.Add "fone", Val(TXTFONE), 1
        DB.Parameters.Remove "cont"
        DB.Parameters.Add "cont", TXTCONTATO, 1
        DB.Parameters.Remove "email"
        DB.Parameters.Add "email", TXTEMAIL, 1
        DB.Parameters.Remove "cgc"
        DB.Parameters.Add "cgc", Val(TXTCGC), 1
        DB.Parameters.Remove "sit"
        DB.Parameters.Add "sit", Val(CBOSITUACAO), 1
        
'        DB.Parameters.Remove "cod_errora"
'        DB.Parameters.Add "cod_errora", 0, 2
'        DB.Parameters.Remove "txt_errora"
'        DB.Parameters.Add "txt_errora", "", 2
        
        DB.ExecuteSQL strsql
        
        'If DB.Parameters("cod_errora") <> 0 Then
         '  MsgBox DB.Parameters("COD_ERRORA") & DB.Parameters("TXT_ERRORA")
         '  MsgBox " Problemas com a Atualiza��o / Inclus�o "
        'Else
           MsgBox "Atualiza��o OK!"
        'End If
       
    End If
    
    'Unload Me
End If

Exit Sub

Trata_Erro:
   MsgBox Err & " - " & Err.Description & Chr(10) & _
          " Problemas com a Atualiza��o / Inclus�o ", vbExclamation, "ATEN��O"
End Sub
'FIM TI-5697
Private Sub SSCommand2_Click()
strsql = " select * from " & _
         " internet.usuario_refer " & _
         " order by cgc "
         
Set ObjSel = DB.dbcreatedynaset(strsql, 0&)

If Not ObjSel.EOF Then
   Grd_Cons.Row = 0
   Grd_Cons.Col = 0
   Grd_Cons.ColWidth(0) = 1400
   Grd_Cons.FixedAlignment(0) = 2
   Grd_Cons.Text = "CGC"
   Grd_Cons.Col = 1
   Grd_Cons.ColWidth(1) = 4000
   Grd_Cons.FixedAlignment(1) = 2
   Grd_Cons.Text = "Raz�o Social"
   Grd_Cons.Col = 2
   Grd_Cons.ColWidth(2) = 4000
   Grd_Cons.FixedAlignment(2) = 2
   Grd_Cons.Text = "E-Mail"
   Grd_Cons.Col = 3
   Grd_Cons.ColWidth(3) = 1000
   Grd_Cons.FixedAlignment(3) = 2
   Grd_Cons.Text = "Situacao"
   Grd_Cons.Col = 4
   Grd_Cons.ColWidth(4) = 1000
   Grd_Cons.FixedAlignment(4) = 2
   Grd_Cons.Text = "Senha"
End If

Do While Not ObjSel.EOF
   DoEvents
   senha = ObjSel.fields("Senha")
   If Len(senha) > 0 Then
      Call DESCRIPTOGRAFAR(senha, SENHA_DES)
      DoEvents
   End If
   Grd_Cons.AddItem ObjSel.fields("cgc") & Chr(9) & ObjSel.fields("razao_social") & _
   Chr(9) & ObjSel.fields("e_mail") & Chr(9) & ObjSel.fields("situacao") & _
   Chr(9) & SENHA_DES
       
   ObjSel.MoveNext
Loop
FrmCons.Show
End Sub

Private Sub SSCommand3_Click()
   
 Unload Me

End Sub


Private Sub SSCommand5_Click()
TXTCGC = ""
TXTRAZAO = ""
TXTENDERECO = ""
CBOUF.Clear
CBOCIDADE.Clear
TXTDDD = ""
TXTFONE = ""
TXTCONTATO = ""
TXTEMAIL = ""
txtsituacao = ""
TXTSENHA = ""
TXTSENHA.Visible = False
LBLSENHA.Visible = False
End Sub

Private Sub TXTCGC_LostFocus()
On Error GoTo Trata_Erro
If Trim(Len(TXTCGC)) <= 0 Then
   TXTCGC.SetFocus
End If

If Len(TXTCGC) > 0 Then
    If Not fCGC(TXTCGC) Then
       MsgBox "CGC INV�LIDO!!!", vbCritical, "Aten��o"
       TXTCGC = ""
       TXTCGC.SetFocus
       Exit Sub
    End If
    strsql = " Select * from " & _
             " Internet.usuario_refer " & _
             " Where cgc = '" & TXTCGC & "'"
    
    Set ObjSel = DB.dbcreatedynaset(strsql, 0&)

    If Not ObjSel.EOF Then
       TXTRAZAO = ObjSel!RAZAO_SOCIAL
       TXTENDERECO = ObjSel!endereco
       TXTDDD = ObjSel!ddd
       TXTFONE = ObjSel!fone
       TXTCONTATO = ObjSel!contato
       TXTEMAIL = IIf(IsNull(ObjSel!e_mail), "", ObjSel!e_mail)
       CBOSITUACAO = IIf(ObjSel!situacao = 0, "0 - Ativo", "9 - Desativado")
       If Len(ObjSel!senha) > 0 Then
          TXTSENHA.Visible = True
          LBLSENHA.Visible = True
          TXTSENHA = ObjSel!senha
          Call DESCRIPTOGRAFAR(TXTSENHA, SENHA_DES)
          TXTSENHA = SENHA_DES
       Else
          TXTSENHA.Visible = False
          LBLSENHA.Visible = False
       End If
       'Busca Uf
       Cod_Cid = ObjSel!Cod_Cidade
       Sql = "Select cod_uf , nome_cidade from cidade " & _
             " Where cod_cidade= " & Left(Cod_Cid, 4)
       Set ObjSel = DB.dbcreatedynaset(Sql, 0&)
       CBOCIDADE = Format(Cod_Cid, "0000") & " - " & ObjSel!Nome_Cidade
       CBOUF = ObjSel!cod_uf
      'SSCommand1.Enabled = False
    End If
Else
SSCommand1.Enabled = True
End If

Exit Sub

Trata_Erro:
If Err <> 0 Then

   MsgBox Err & " - " & Err.Description & Chr(10) & _
          " ENTRE EM CONTATO COM O DEPTO SUPORTE ", vbExclamation, "ATEN��O"
   Resume Next
   
End If


End Sub


Private Sub TXTCONTATO_KeyPress(KeyAscii As Integer)
KeyAscii = Maiusculo(KeyAscii)
End Sub


Private Sub TXTDDD_KeyPress(KeyAscii As Integer)
KeyAscii = Numerico(KeyAscii)
End Sub


Private Sub TXTEMAIL_KeyPress(KeyAscii As Integer)
KeyAscii = Maiusculo(KeyAscii)
End Sub


Private Sub TXTENDERECO_KeyPress(KeyAscii As Integer)
KeyAscii = Maiusculo(KeyAscii)
End Sub


Private Sub TXTRAZAO_KeyPress(KeyAscii As Integer)
KeyAscii = Maiusculo(KeyAscii)
End Sub


