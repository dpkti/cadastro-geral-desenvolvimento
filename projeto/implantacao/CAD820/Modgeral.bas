Attribute VB_Name = "Module1"
Option Explicit
Public DB As Object
Public orasession As Object
Public DBORA As Object

Public Fl_Cliente_Novo
Public Cliente_SCI
Public Linha_Produto

Public Fl_Invalida
Public Fl_Tare
Public Fl_Ja_Relacionou
Public ObjSel As Object

Function fCGC(lstrCGC)
    Dim i, j
    Dim liResultado As Integer
    Dim llSoma As Long
    Dim llResto As Long
    Dim llDigito As Long
    
    lstrCGC = Format(lstrCGC, "00000000000000") '14
    llSoma = 0
    j = 2
    For i = 12 To 1 Step -1
        llSoma = llSoma + (j * Val(Mid(lstrCGC, i, 1)))
        j = j + 1
        If j = 10 Then
            j = 2
        End If
    Next

    llResto = Int(llSoma / 11) * 11
    llResto = llSoma - llResto

    If llResto = 0 Or llResto = 1 Then
        llDigito = 0
    Else
        llDigito = 11 - llResto
    End If

    liResultado = llDigito * 10

    llSoma = 0
    j = 2
    For i = 13 To 1 Step -1
        llSoma = llSoma + (j * Val(Mid(lstrCGC, i, 1)))
        j = j + 1
        If j = 10 Then
            j = 2
        End If
    Next

    llResto = Int(llSoma / 11) * 11
    llResto = llSoma - llResto

    If llResto = 0 Or llResto = 1 Then
        llDigito = 0
    Else
        llDigito = 11 - llResto
    End If

    liResultado = liResultado + llDigito
    liResultado = Format(liResultado, "00")

    fCGC = IIf(liResultado = Mid(lstrCGC, 13, 2), True, False)
    
End Function





Sub Data(ByRef KeyAscii, ByRef txtCampo)
    On Error GoTo TrataErro

    Dim bTam As Byte    'tamanho do campo JA digitado
    Dim strData As String
    Dim bKey As Byte
    bTam = Len(txtCampo.Text)
    
    If KeyAscii = 8 And bTam > 0 Then 'backspace
        If Mid$(txtCampo.Text, bTam) = "/" Then
            txtCampo.Text = Mid$(txtCampo.Text, 1, bTam - 2)
        Else
            txtCampo.Text = Mid$(txtCampo.Text, 1, bTam - 1)
        End If
            
    ElseIf Chr$(KeyAscii) >= "0" And Chr$(KeyAscii) <= "9" Then
        If bTam = 1 Then
            strData = txtCampo.Text & Chr$(KeyAscii)
            If CInt(strData) < 1 Or CInt(strData > 31) Then
                Beep
            Else
                txtCampo.Text = txtCampo.Text & Chr$(KeyAscii) & "/"
            End If
            
        ElseIf bTam = 4 Then
            strData = Mid$(txtCampo.Text, 4) & Chr$(KeyAscii)
            If CInt(strData) < 1 Or CInt(strData > 12) Then
                Beep
            Else
                txtCampo.Text = txtCampo.Text & Chr$(KeyAscii) & "/"
            End If
        
        ElseIf bTam = 7 Then
            strData = Mid$(txtCampo.Text, 1, 2)     'dia
            If CInt(strData) < 1 Or CInt(strData > 31) Then
                Beep
            Else
                strData = Mid$(txtCampo.Text, 4, 2)     'mes
                If CInt(strData) < 1 Or CInt(strData > 12) Then
                    Beep
                Else
                    strData = txtCampo.Text & Chr$(KeyAscii)
                    strData = Left(strData, 6) & IIf(Val(Right(strData, 2)) > 50, "19", "20") & Right(strData, 2)
                    If Not IsDate(CDate(strData)) Then
                        Beep
                    Else
                        txtCampo.Text = Format(strData, "dd/mm/yy")
                    End If
                End If
            End If
            
        ElseIf bTam < 8 Then
            bKey = KeyAscii
            
        Else
            bKey = 0
        End If
    Else
        Beep
    End If
    
    SendKeys "{END}"
    KeyAscii = bKey
    Exit Sub
    
TrataErro:

    If Err.Number = 13 Then
        MsgBox "Data Inv�lida " & strData, vbCritical, "Aten��o"
        KeyAscii = 0
        Beep
        Err.Clear
    End If

End Sub




Sub Process_Line_Errors(ByRef Sql)
    Dim iFnum As Integer
        
    If Err.Number = 3186 Or Err.Number = 3188 Or Err.Number = 3260 Then
        Resume
    Else
        MsgBox "Ocorreu o erro: " & Err.Number & " -" & Err.Description & ". Ligue para o departamento de sistemas"
        Screen.MousePointer = vbDefault
        End
    End If
    Exit Sub

Handler_Process_Line_Errors:
    DoEvents
    Resume Next

End Sub

Function FmtBR(ByVal Valor) As String

    Dim Temp As String
    Dim i As Integer
    
    Temp = Trim(Valor)
        
    For i = 1 To Len(Temp)
        If Mid$(Temp, i, 1) = "," Then
            Mid$(Temp, i, 1) = "."
        End If
    Next i

    FmtBR = Temp

End Function

Function iRetornaDigito(ByVal iOrigem As Long) As Integer

    Dim iSoma As Integer
    Dim gResto As Single
    Dim iTam As Integer
    Dim iFator As Integer
    Dim iContador As Integer
    Dim sOrigem As String


    iTam = Len(Trim$(Str$(iOrigem)))
    iFator = 2
    
    sOrigem = Trim$(Str$(iOrigem))
    
    
    For iContador = iTam To 1 Step -1
        iSoma = iSoma + Val(Mid$(sOrigem, iContador, 1)) * iFator
        iFator = iFator + 1
    Next iContador
    
    
    gResto = iSoma Mod 11
    
    If gResto = 0 Then
        iRetornaDigito = 1
    ElseIf gResto = 1 Then
        iRetornaDigito = 0
    Else
        iRetornaDigito = 11 - gResto
    End If

End Function









Function Maiusculo(KeyAscii As Integer) As Integer
    If Chr$(KeyAscii) = "'" Or Chr$(KeyAscii) = "�" Or Chr$(KeyAscii) = "`" Then
        KeyAscii = 0
        Beep
    Else
        KeyAscii = Asc(UCase(Chr$(KeyAscii)))
    End If
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    Maiusculo = KeyAscii
End Function

Function Numerico(ByVal KeyAscii As Integer) As Integer
    If KeyAscii = 8 Then    'BACKSPACE
        Numerico = KeyAscii
        Exit Function
    End If
    If Chr(KeyAscii) < "0" Or Chr(KeyAscii) > "9" Then
        KeyAscii = 0
        Beep
    End If
    Numerico = KeyAscii
End Function

Function Texto(ByVal KeyAscii As Integer) As Integer
    If KeyAscii = 8 Then    'BACKSPACE
        Texto = KeyAscii
        Exit Function
    End If
    
    If Chr$(KeyAscii) = "'" Or Chr$(KeyAscii) = "�" Or Chr$(KeyAscii) = "`" Then
        KeyAscii = 0
        Beep
    Else
        KeyAscii = Asc(UCase(Chr$(KeyAscii)))
    End If
    
    Texto = KeyAscii
End Function

Function Valor(ByVal KeyAscii As Integer, strCampo As String) As Integer
    If KeyAscii = 8 Then    'BACKSPACE
        Valor = KeyAscii
        Exit Function
    End If
    If Chr$(KeyAscii) = "," Then
        KeyAscii = Asc(".")
    End If
    If Chr$(KeyAscii) = "," Or Chr$(KeyAscii) = "." Then
        If InStr(strCampo, ",") > 0 Or InStr(strCampo, ".") > 0 Then
            KeyAscii = 0
            Beep
        End If
    Else
        If Chr$(KeyAscii) < "0" Or Chr$(KeyAscii) > "9" Then
            KeyAscii = 0
            Beep
        End If
    End If
    
    Valor = KeyAscii
End Function


