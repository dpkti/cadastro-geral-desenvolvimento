VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form frmCadastro 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cadastro de Par�metros de Ressuprimento"
   ClientHeight    =   7380
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10320
   Icon            =   "frmCadastro.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   492
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   688
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   7050
      Width           =   10320
      _ExtentX        =   18203
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   18150
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   810
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadastro.frx":23D2
      PICN            =   "frmCadastro.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin TabDlg.SSTab stbParametros 
      Height          =   6090
      Left            =   90
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   900
      Width           =   10170
      _ExtentX        =   17939
      _ExtentY        =   10742
      _Version        =   393216
      Tabs            =   5
      TabsPerRow      =   5
      TabHeight       =   617
      ShowFocusRect   =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Itens"
      TabPicture(0)   =   "frmCadastro.frx":30C8
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Label93"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Label94"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Label95"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Label96"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "cmdLimparI"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "cmdImportarI"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "cmdGravarI"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "FrameI1"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).ControlCount=   8
      TabCaption(1)   =   "Fornecedores"
      TabPicture(1)   =   "frmCadastro.frx":30E4
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "cmdImportarF"
      Tab(1).Control(1)=   "cmdGravarF"
      Tab(1).Control(2)=   "cmdLimparF"
      Tab(1).Control(3)=   "cmdGravarF2"
      Tab(1).Control(4)=   "FrameF1"
      Tab(1).Control(5)=   "FrameF2"
      Tab(1).Control(6)=   "FrameF6"
      Tab(1).ControlCount=   7
      TabCaption(2)   =   "Itens do Fornecedor"
      TabPicture(2)   =   "frmCadastro.frx":3100
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "cmdLimparIF"
      Tab(2).Control(0).Enabled=   0   'False
      Tab(2).Control(1)=   "cmdGravarIF"
      Tab(2).Control(1).Enabled=   0   'False
      Tab(2).Control(2)=   "FrameIF"
      Tab(2).Control(2).Enabled=   0   'False
      Tab(2).ControlCount=   3
      TabCaption(3)   =   "Layout"
      TabPicture(3)   =   "frmCadastro.frx":311C
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "FrameLayout"
      Tab(3).ControlCount=   1
      TabCaption(4)   =   "Consulta Bloqueios"
      TabPicture(4)   =   "frmCadastro.frx":3138
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "FrameB_Bloq"
      Tab(4).Control(1)=   "FrameB"
      Tab(4).Control(2)=   "cmdConsultarB"
      Tab(4).Control(3)=   "cmdLimparB"
      Tab(4).ControlCount=   4
      Begin VB.Frame FrameB_Bloq 
         Caption         =   "Bloqueios"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   2490
         Left            =   -74865
         TabIndex        =   110
         Top             =   2700
         Width           =   8670
         Begin MSFlexGridLib.MSFlexGrid mfgBloqueios 
            Height          =   2085
            Left            =   135
            TabIndex        =   111
            Top             =   270
            Width           =   8380
            _ExtentX        =   14790
            _ExtentY        =   3678
            _Version        =   393216
            FixedCols       =   0
            BackColorBkg    =   -2147483633
            AllowBigSelection=   0   'False
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame FrameB 
         Caption         =   "Dados"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   2100
         Left            =   -74865
         TabIndex        =   99
         Top             =   518
         Width           =   8655
         Begin VB.ComboBox cmbLojaB 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            ItemData        =   "frmCadastro.frx":3154
            Left            =   1260
            List            =   "frmCadastro.frx":3156
            Style           =   2  'Dropdown List
            TabIndex        =   101
            Top             =   315
            Width           =   2280
         End
         Begin VB.ComboBox cmbFornecedorB 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            ItemData        =   "frmCadastro.frx":3158
            Left            =   1260
            List            =   "frmCadastro.frx":315A
            Style           =   2  'Dropdown List
            TabIndex        =   103
            Top             =   720
            Width           =   5070
         End
         Begin VB.ComboBox cmbGrupoB 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            ItemData        =   "frmCadastro.frx":315C
            Left            =   1260
            List            =   "frmCadastro.frx":315E
            Style           =   2  'Dropdown List
            TabIndex        =   105
            Top             =   1125
            Width           =   3315
         End
         Begin VB.ComboBox cmbSubGrupoB 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            ItemData        =   "frmCadastro.frx":3160
            Left            =   1260
            List            =   "frmCadastro.frx":3162
            Style           =   2  'Dropdown List
            TabIndex        =   107
            Top             =   1530
            Width           =   3315
         End
         Begin MSMask.MaskEdBox txtDPKB 
            Height          =   330
            Left            =   7470
            TabIndex        =   109
            Top             =   720
            Width           =   870
            _ExtentX        =   1535
            _ExtentY        =   582
            _Version        =   393216
            MaxLength       =   5
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            PromptChar      =   "�"
         End
         Begin VB.Label lblDPKB 
            Appearance      =   0  'Flat
            Caption         =   "DPK:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   6975
            TabIndex        =   108
            Top             =   765
            Width           =   495
         End
         Begin VB.Label lblLojaB 
            Appearance      =   0  'Flat
            Caption         =   "Loja:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   135
            TabIndex        =   100
            Top             =   360
            Width           =   660
         End
         Begin VB.Label lblFornB 
            Appearance      =   0  'Flat
            Caption         =   "Fornecedor:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   135
            TabIndex        =   102
            Top             =   765
            Width           =   1065
         End
         Begin VB.Label lblGrupoB 
            Appearance      =   0  'Flat
            Caption         =   "Grupo:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   135
            TabIndex        =   104
            Top             =   1170
            Width           =   660
         End
         Begin VB.Label lblSubGrupoB 
            Appearance      =   0  'Flat
            Caption         =   "Subrupo:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   135
            TabIndex        =   106
            Top             =   1575
            Width           =   855
         End
      End
      Begin VB.Frame FrameF6 
         Caption         =   "Importa��o de Arquivo"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   1810
         Left            =   -74865
         TabIndex        =   72
         Top             =   3398
         Width           =   8670
         Begin MSFlexGridLib.MSFlexGrid mfgImportacao 
            Height          =   1410
            Left            =   135
            TabIndex        =   73
            Top             =   270
            Width           =   8370
            _ExtentX        =   14764
            _ExtentY        =   2487
            _Version        =   393216
            FixedCols       =   0
            BackColorBkg    =   -2147483633
            AllowBigSelection=   0   'False
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame FrameF2 
         Caption         =   "Quantidades"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   2745
         Left            =   -69915
         TabIndex        =   36
         Top             =   518
         Width           =   3705
         Begin VB.Frame FrameF5 
            Caption         =   "Prog.Transf."
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   2415
            Left            =   2475
            TabIndex        =   59
            Top             =   215
            Width           =   1095
            Begin MSMask.MaskEdBox txtAP 
               Height          =   330
               Left            =   405
               TabIndex        =   61
               Top             =   315
               Width           =   495
               _ExtentX        =   873
               _ExtentY        =   582
               _Version        =   393216
               MaxLength       =   3
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               PromptChar      =   "�"
            End
            Begin MSMask.MaskEdBox txtBP 
               Height          =   330
               Left            =   405
               TabIndex        =   63
               Top             =   720
               Width           =   495
               _ExtentX        =   873
               _ExtentY        =   582
               _Version        =   393216
               MaxLength       =   3
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               PromptChar      =   "�"
            End
            Begin MSMask.MaskEdBox txtCP 
               Height          =   330
               Left            =   405
               TabIndex        =   65
               Top             =   1125
               Width           =   495
               _ExtentX        =   873
               _ExtentY        =   582
               _Version        =   393216
               MaxLength       =   3
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               PromptChar      =   "�"
            End
            Begin MSMask.MaskEdBox txtDP 
               Height          =   330
               Left            =   405
               TabIndex        =   67
               Top             =   1530
               Width           =   495
               _ExtentX        =   873
               _ExtentY        =   582
               _Version        =   393216
               MaxLength       =   3
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               PromptChar      =   "�"
            End
            Begin MSMask.MaskEdBox txtEP 
               Height          =   330
               Left            =   405
               TabIndex        =   69
               Top             =   1935
               Width           =   495
               _ExtentX        =   873
               _ExtentY        =   582
               _Version        =   393216
               MaxLength       =   3
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               PromptChar      =   "�"
            End
            Begin VB.Label lblEP 
               Appearance      =   0  'Flat
               Caption         =   "E"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   285
               Left            =   225
               TabIndex        =   68
               Top             =   1980
               Width           =   210
            End
            Begin VB.Label lblDP 
               Appearance      =   0  'Flat
               Caption         =   "D"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   285
               Left            =   225
               TabIndex        =   66
               Top             =   1575
               Width           =   210
            End
            Begin VB.Label lblCP 
               Appearance      =   0  'Flat
               Caption         =   "C"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   285
               Left            =   225
               TabIndex        =   64
               Top             =   1170
               Width           =   210
            End
            Begin VB.Label lblBP 
               Appearance      =   0  'Flat
               Caption         =   "B"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   285
               Left            =   225
               TabIndex        =   62
               Top             =   765
               Width           =   210
            End
            Begin VB.Label lblAP 
               Appearance      =   0  'Flat
               Caption         =   "A"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   285
               Left            =   225
               TabIndex        =   60
               Top             =   360
               Width           =   210
            End
         End
         Begin VB.Frame FrameF4 
            Caption         =   "Min.Transf."
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   2415
            Left            =   1305
            TabIndex        =   48
            Top             =   215
            Width           =   1095
            Begin MSMask.MaskEdBox txtAM 
               Height          =   330
               Left            =   405
               TabIndex        =   50
               Top             =   315
               Width           =   495
               _ExtentX        =   873
               _ExtentY        =   582
               _Version        =   393216
               MaxLength       =   3
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               PromptChar      =   "�"
            End
            Begin MSMask.MaskEdBox txtBM 
               Height          =   330
               Left            =   405
               TabIndex        =   52
               Top             =   720
               Width           =   495
               _ExtentX        =   873
               _ExtentY        =   582
               _Version        =   393216
               MaxLength       =   3
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               PromptChar      =   "�"
            End
            Begin MSMask.MaskEdBox txtCM 
               Height          =   330
               Left            =   405
               TabIndex        =   54
               Top             =   1125
               Width           =   495
               _ExtentX        =   873
               _ExtentY        =   582
               _Version        =   393216
               MaxLength       =   3
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               PromptChar      =   "�"
            End
            Begin MSMask.MaskEdBox txtDM 
               Height          =   330
               Left            =   405
               TabIndex        =   56
               Top             =   1530
               Width           =   495
               _ExtentX        =   873
               _ExtentY        =   582
               _Version        =   393216
               MaxLength       =   3
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               PromptChar      =   "�"
            End
            Begin MSMask.MaskEdBox txtEM 
               Height          =   330
               Left            =   405
               TabIndex        =   58
               Top             =   1935
               Width           =   495
               _ExtentX        =   873
               _ExtentY        =   582
               _Version        =   393216
               MaxLength       =   3
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               PromptChar      =   "�"
            End
            Begin VB.Label lblEM 
               Appearance      =   0  'Flat
               Caption         =   "E"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   285
               Left            =   225
               TabIndex        =   57
               Top             =   1980
               Width           =   210
            End
            Begin VB.Label lblDM 
               Appearance      =   0  'Flat
               Caption         =   "D"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   285
               Left            =   225
               TabIndex        =   55
               Top             =   1575
               Width           =   210
            End
            Begin VB.Label lblCM 
               Appearance      =   0  'Flat
               Caption         =   "C"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   285
               Left            =   225
               TabIndex        =   53
               Top             =   1170
               Width           =   210
            End
            Begin VB.Label lblBM 
               Appearance      =   0  'Flat
               Caption         =   "B"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   285
               Left            =   225
               TabIndex        =   51
               Top             =   765
               Width           =   210
            End
            Begin VB.Label lblAM 
               Appearance      =   0  'Flat
               Caption         =   "A"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   285
               Left            =   225
               TabIndex        =   49
               Top             =   360
               Width           =   210
            End
         End
         Begin VB.Frame FrameF3 
            Caption         =   "Excesso"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   2415
            Left            =   135
            TabIndex        =   37
            Top             =   215
            Width           =   1095
            Begin MSMask.MaskEdBox txtAE 
               Height          =   330
               Left            =   405
               TabIndex        =   39
               Top             =   315
               Width           =   495
               _ExtentX        =   873
               _ExtentY        =   582
               _Version        =   393216
               MaxLength       =   3
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               PromptChar      =   "�"
            End
            Begin MSMask.MaskEdBox txtBE 
               Height          =   330
               Left            =   405
               TabIndex        =   41
               Top             =   720
               Width           =   495
               _ExtentX        =   873
               _ExtentY        =   582
               _Version        =   393216
               MaxLength       =   3
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               PromptChar      =   "�"
            End
            Begin MSMask.MaskEdBox txtCE 
               Height          =   330
               Left            =   405
               TabIndex        =   43
               Top             =   1125
               Width           =   495
               _ExtentX        =   873
               _ExtentY        =   582
               _Version        =   393216
               MaxLength       =   3
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               PromptChar      =   "�"
            End
            Begin MSMask.MaskEdBox txtDE 
               Height          =   330
               Left            =   405
               TabIndex        =   45
               Top             =   1530
               Width           =   495
               _ExtentX        =   873
               _ExtentY        =   582
               _Version        =   393216
               MaxLength       =   3
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               PromptChar      =   "�"
            End
            Begin MSMask.MaskEdBox txtEE 
               Height          =   330
               Left            =   405
               TabIndex        =   47
               Top             =   1935
               Width           =   495
               _ExtentX        =   873
               _ExtentY        =   582
               _Version        =   393216
               MaxLength       =   3
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Arial"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               PromptChar      =   "�"
            End
            Begin VB.Label lblEE 
               Appearance      =   0  'Flat
               Caption         =   "E"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   285
               Left            =   225
               TabIndex        =   46
               Top             =   1980
               Width           =   210
            End
            Begin VB.Label lblDE 
               Appearance      =   0  'Flat
               Caption         =   "D"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   285
               Left            =   225
               TabIndex        =   44
               Top             =   1575
               Width           =   210
            End
            Begin VB.Label lblCE 
               Appearance      =   0  'Flat
               Caption         =   "C"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   285
               Left            =   225
               TabIndex        =   42
               Top             =   1170
               Width           =   210
            End
            Begin VB.Label lblBE 
               Appearance      =   0  'Flat
               Caption         =   "B"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   285
               Left            =   225
               TabIndex        =   40
               Top             =   765
               Width           =   210
            End
            Begin VB.Label lblAE 
               Appearance      =   0  'Flat
               Caption         =   "A"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   285
               Left            =   225
               TabIndex        =   38
               Top             =   360
               Width           =   210
            End
         End
      End
      Begin VB.Frame FrameF1 
         Caption         =   "Dados"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   1965
         Left            =   -74865
         TabIndex        =   27
         Top             =   518
         Width           =   4875
         Begin VB.ComboBox cmbFornecedorF 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            ItemData        =   "frmCadastro.frx":3164
            Left            =   1170
            List            =   "frmCadastro.frx":3166
            Style           =   2  'Dropdown List
            TabIndex        =   31
            Top             =   720
            Width           =   3585
         End
         Begin VB.ComboBox cmbLojaF 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            ItemData        =   "frmCadastro.frx":3168
            Left            =   1170
            List            =   "frmCadastro.frx":316A
            Style           =   2  'Dropdown List
            TabIndex        =   29
            Top             =   315
            Width           =   2280
         End
         Begin MSMask.MaskEdBox txtDiasF 
            Height          =   330
            Left            =   2115
            TabIndex        =   33
            Top             =   1125
            Width           =   690
            _ExtentX        =   1217
            _ExtentY        =   582
            _Version        =   393216
            MaxLength       =   4
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            PromptChar      =   "�"
         End
         Begin MSMask.MaskEdBox txtCalculoF 
            Height          =   330
            Left            =   2115
            TabIndex        =   35
            Top             =   1530
            Width           =   330
            _ExtentX        =   582
            _ExtentY        =   582
            _Version        =   393216
            MaxLength       =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            PromptChar      =   "�"
         End
         Begin VB.Label lblCalculoF 
            Appearance      =   0  'Flat
            Caption         =   "Flag C�LCULO:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   135
            TabIndex        =   34
            Top             =   1575
            Width           =   1665
         End
         Begin VB.Label lblDiasF 
            Appearance      =   0  'Flat
            Caption         =   "N� Dias Prim. Entrada:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   135
            TabIndex        =   32
            Top             =   1170
            Width           =   1890
         End
         Begin VB.Label lblFornecedorF 
            Appearance      =   0  'Flat
            Caption         =   "Fornecedor:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   135
            TabIndex        =   30
            Top             =   765
            Width           =   1065
         End
         Begin VB.Label lblLojaF 
            Appearance      =   0  'Flat
            Caption         =   "Loja:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   135
            TabIndex        =   28
            Top             =   360
            Width           =   660
         End
      End
      Begin VB.Frame FrameI1 
         Caption         =   "Dados"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   4665
         Left            =   135
         TabIndex        =   4
         Top             =   518
         Width           =   6945
         Begin VB.ComboBox cmbLojaI 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            ItemData        =   "frmCadastro.frx":316C
            Left            =   1260
            List            =   "frmCadastro.frx":316E
            Style           =   2  'Dropdown List
            TabIndex        =   6
            Top             =   315
            Width           =   2280
         End
         Begin VB.ComboBox cmbFornecedorI 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            ItemData        =   "frmCadastro.frx":3170
            Left            =   1260
            List            =   "frmCadastro.frx":3172
            Style           =   2  'Dropdown List
            TabIndex        =   8
            Top             =   745
            Width           =   4690
         End
         Begin MSMask.MaskEdBox txtCompraI 
            Height          =   330
            Left            =   2115
            TabIndex        =   14
            Top             =   1980
            Width           =   330
            _ExtentX        =   582
            _ExtentY        =   582
            _Version        =   393216
            MaxLength       =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            PromptChar      =   "�"
         End
         Begin MSMask.MaskEdBox txtTransfI 
            Height          =   330
            Left            =   2115
            TabIndex        =   16
            Top             =   2385
            Width           =   330
            _ExtentX        =   582
            _ExtentY        =   582
            _Version        =   393216
            MaxLength       =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            PromptChar      =   "�"
         End
         Begin MSMask.MaskEdBox txtCodFabricaI 
            Height          =   330
            Left            =   1260
            TabIndex        =   10
            Top             =   1170
            Width           =   2085
            _ExtentX        =   3678
            _ExtentY        =   582
            _Version        =   393216
            MaxLength       =   19
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            PromptChar      =   "�"
         End
         Begin MSMask.MaskEdBox txtDPKI 
            Height          =   330
            Left            =   1260
            TabIndex        =   12
            Top             =   1575
            Width           =   870
            _ExtentX        =   1535
            _ExtentY        =   582
            _Version        =   393216
            MaxLength       =   5
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            PromptChar      =   "�"
         End
         Begin MSFlexGridLib.MSFlexGrid mfgItens 
            Height          =   1740
            Left            =   135
            TabIndex        =   23
            Top             =   2790
            Width           =   6660
            _ExtentX        =   11748
            _ExtentY        =   3069
            _Version        =   393216
            FixedCols       =   0
            BackColorBkg    =   -2147483633
            AllowBigSelection=   0   'False
            ScrollBars      =   2
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin MSMask.MaskEdBox txtCasadaI 
            Height          =   330
            Left            =   5670
            TabIndex        =   18
            Top             =   1575
            Width           =   330
            _ExtentX        =   582
            _ExtentY        =   582
            _Version        =   393216
            MaxLength       =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            PromptChar      =   "�"
         End
         Begin MSMask.MaskEdBox txtEstoqueI 
            Height          =   330
            Left            =   5670
            TabIndex        =   20
            Top             =   1980
            Width           =   330
            _ExtentX        =   582
            _ExtentY        =   582
            _Version        =   393216
            MaxLength       =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            PromptChar      =   "�"
         End
         Begin MSMask.MaskEdBox txtDtLiberacaoI 
            Height          =   330
            Left            =   4770
            TabIndex        =   22
            Top             =   2385
            Width           =   1230
            _ExtentX        =   2170
            _ExtentY        =   582
            _Version        =   393216
            MaxLength       =   8
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            PromptChar      =   "�"
         End
         Begin VB.Label Label3 
            Appearance      =   0  'Flat
            Caption         =   "Flag Bloq. VDA CASADA:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   3375
            TabIndex        =   17
            Top             =   1620
            Width           =   2070
         End
         Begin VB.Label Label2 
            Appearance      =   0  'Flat
            Caption         =   "Flag Bloq. AUM. ESTOQUE:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   3375
            TabIndex        =   19
            Top             =   2025
            Width           =   2250
         End
         Begin VB.Label Label1 
            Appearance      =   0  'Flat
            Caption         =   "Data Libera��o:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   3375
            TabIndex        =   21
            Top             =   2430
            Width           =   1305
         End
         Begin VB.Label lblLojaI 
            Appearance      =   0  'Flat
            Caption         =   "Loja:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   135
            TabIndex        =   5
            Top             =   360
            Width           =   660
         End
         Begin VB.Label lblFornecedorI 
            Appearance      =   0  'Flat
            Caption         =   "Fornecedor:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   135
            TabIndex        =   7
            Top             =   775
            Width           =   1065
         End
         Begin VB.Label lblCodFabricaI 
            Appearance      =   0  'Flat
            Caption         =   "C�d. F�brica:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   135
            TabIndex        =   9
            Top             =   1215
            Width           =   1155
         End
         Begin VB.Label lblDPKI 
            Appearance      =   0  'Flat
            Caption         =   "DPK:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   135
            TabIndex        =   11
            Top             =   1620
            Width           =   855
         End
         Begin VB.Label lblCompraI 
            Appearance      =   0  'Flat
            Caption         =   "Flag COMPRA:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   135
            TabIndex        =   13
            Top             =   2025
            Width           =   1440
         End
         Begin VB.Label lblTransfI 
            Appearance      =   0  'Flat
            Caption         =   "Flag TRANSFER�NCIA:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   135
            TabIndex        =   15
            Top             =   2430
            Width           =   1935
         End
      End
      Begin VB.Frame FrameIF 
         Caption         =   "Dados"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   3225
         Left            =   -74865
         TabIndex        =   76
         Top             =   518
         Width           =   6495
         Begin VB.ComboBox cmbSubgrupo 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            ItemData        =   "frmCadastro.frx":3174
            Left            =   1260
            List            =   "frmCadastro.frx":3176
            Style           =   2  'Dropdown List
            TabIndex        =   84
            Top             =   1530
            Width           =   3315
         End
         Begin VB.ComboBox cmbGrupo 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            ItemData        =   "frmCadastro.frx":3178
            Left            =   1260
            List            =   "frmCadastro.frx":317A
            Style           =   2  'Dropdown List
            TabIndex        =   82
            Top             =   1125
            Width           =   3315
         End
         Begin VB.ComboBox cmbFornecedor 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            ItemData        =   "frmCadastro.frx":317C
            Left            =   1260
            List            =   "frmCadastro.frx":317E
            Style           =   2  'Dropdown List
            TabIndex        =   80
            Top             =   720
            Width           =   5070
         End
         Begin VB.ComboBox cmbLoja 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            ItemData        =   "frmCadastro.frx":3180
            Left            =   1260
            List            =   "frmCadastro.frx":3182
            Style           =   2  'Dropdown List
            TabIndex        =   78
            Top             =   315
            Width           =   2280
         End
         Begin MSMask.MaskEdBox txtCompra 
            Height          =   330
            Left            =   2115
            TabIndex        =   86
            Top             =   1935
            Width           =   330
            _ExtentX        =   582
            _ExtentY        =   582
            _Version        =   393216
            MaxLength       =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            PromptChar      =   "�"
         End
         Begin MSMask.MaskEdBox txtTransf 
            Height          =   330
            Left            =   2115
            TabIndex        =   88
            Top             =   2340
            Width           =   330
            _ExtentX        =   582
            _ExtentY        =   582
            _Version        =   393216
            MaxLength       =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            PromptChar      =   "�"
         End
         Begin MSMask.MaskEdBox txtCasada 
            Height          =   330
            Left            =   5445
            TabIndex        =   90
            Top             =   1935
            Width           =   330
            _ExtentX        =   582
            _ExtentY        =   582
            _Version        =   393216
            MaxLength       =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            PromptChar      =   "�"
         End
         Begin MSMask.MaskEdBox txtEstoque 
            Height          =   330
            Left            =   5445
            TabIndex        =   92
            Top             =   2340
            Width           =   330
            _ExtentX        =   582
            _ExtentY        =   582
            _Version        =   393216
            MaxLength       =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            PromptChar      =   "�"
         End
         Begin MSMask.MaskEdBox txtDtLiberacao 
            Height          =   330
            Left            =   4545
            TabIndex        =   94
            Top             =   2745
            Width           =   1230
            _ExtentX        =   2170
            _ExtentY        =   582
            _Version        =   393216
            MaxLength       =   8
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            PromptChar      =   "�"
         End
         Begin VB.Label lblDtLiberacao 
            Appearance      =   0  'Flat
            Caption         =   "Data Libera��o:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   3150
            TabIndex        =   93
            Top             =   2790
            Width           =   1305
         End
         Begin VB.Label lblEstoque 
            Appearance      =   0  'Flat
            Caption         =   "Flag Bloq. AUM. ESTOQUE:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   3150
            TabIndex        =   91
            Top             =   2385
            Width           =   2250
         End
         Begin VB.Label lblCasada 
            Appearance      =   0  'Flat
            Caption         =   "Flag Bloq. VDA CASADA:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   3150
            TabIndex        =   89
            Top             =   1980
            Width           =   2070
         End
         Begin VB.Label lblTransf 
            Appearance      =   0  'Flat
            Caption         =   "Flag TRANSFER�NCIA:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   135
            TabIndex        =   87
            Top             =   2385
            Width           =   1935
         End
         Begin VB.Label lblCompra 
            Appearance      =   0  'Flat
            Caption         =   "Flag COMPRA:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   135
            TabIndex        =   85
            Top             =   1980
            Width           =   1440
         End
         Begin VB.Label lblSubgrupo 
            Appearance      =   0  'Flat
            Caption         =   "Subrupo:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   135
            TabIndex        =   83
            Top             =   1575
            Width           =   855
         End
         Begin VB.Label lblGrupo 
            Appearance      =   0  'Flat
            Caption         =   "Grupo:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   135
            TabIndex        =   81
            Top             =   1170
            Width           =   660
         End
         Begin VB.Label lblFornecedor 
            Appearance      =   0  'Flat
            Caption         =   "Fornecedor:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   135
            TabIndex        =   79
            Top             =   765
            Width           =   1065
         End
         Begin VB.Label lblLoja 
            Appearance      =   0  'Flat
            Caption         =   "Loja:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   135
            TabIndex        =   77
            Top             =   360
            Width           =   660
         End
      End
      Begin VB.Frame FrameLayout 
         Caption         =   "Layout do Arquivo de Itens:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   2790
         Left            =   -74865
         TabIndex        =   97
         Top             =   518
         Width           =   5565
         Begin VB.Label lblLayout 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   2265
            Left            =   135
            TabIndex        =   98
            Top             =   360
            Width           =   5280
         End
      End
      Begin Bot�o.cmd cmdGravarI 
         Height          =   690
         Left            =   1665
         TabIndex        =   26
         TabStop         =   0   'False
         ToolTipText     =   "Gravar"
         Top             =   5288
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadastro.frx":3184
         PICN            =   "frmCadastro.frx":31A0
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdImportarI 
         Height          =   690
         Left            =   900
         TabIndex        =   25
         TabStop         =   0   'False
         ToolTipText     =   "Importar Arquivo de Itens"
         Top             =   5288
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadastro.frx":3E7A
         PICN            =   "frmCadastro.frx":3E96
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdLimparI 
         Height          =   690
         Left            =   135
         TabIndex        =   24
         TabStop         =   0   'False
         ToolTipText     =   "Limpar campos"
         Top             =   5288
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadastro.frx":4B70
         PICN            =   "frmCadastro.frx":4B8C
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdGravarF2 
         Height          =   690
         Left            =   -74100
         TabIndex        =   71
         TabStop         =   0   'False
         ToolTipText     =   "Gravar"
         Top             =   2543
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadastro.frx":5866
         PICN            =   "frmCadastro.frx":5882
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdGravarIF 
         Height          =   690
         Left            =   -74085
         TabIndex        =   96
         TabStop         =   0   'False
         ToolTipText     =   "Gravar"
         Top             =   3803
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadastro.frx":655C
         PICN            =   "frmCadastro.frx":6578
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdLimparIF 
         Height          =   690
         Left            =   -74865
         TabIndex        =   95
         TabStop         =   0   'False
         ToolTipText     =   "Limpar campos"
         Top             =   3803
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadastro.frx":7252
         PICN            =   "frmCadastro.frx":726E
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdLimparF 
         Height          =   690
         Left            =   -74865
         TabIndex        =   70
         TabStop         =   0   'False
         ToolTipText     =   "Limpar campos"
         Top             =   2543
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadastro.frx":7F48
         PICN            =   "frmCadastro.frx":7F64
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdGravarF 
         Height          =   690
         Left            =   -74085
         TabIndex        =   75
         TabStop         =   0   'False
         ToolTipText     =   "Gravar"
         Top             =   5288
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadastro.frx":8C3E
         PICN            =   "frmCadastro.frx":8C5A
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdImportarF 
         Height          =   690
         Left            =   -74865
         TabIndex        =   74
         TabStop         =   0   'False
         ToolTipText     =   "Importar"
         Top             =   5288
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadastro.frx":9934
         PICN            =   "frmCadastro.frx":9950
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdConsultarB 
         Height          =   690
         Left            =   -74085
         TabIndex        =   113
         TabStop         =   0   'False
         ToolTipText     =   "Consultar"
         Top             =   5288
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadastro.frx":A62A
         PICN            =   "frmCadastro.frx":A646
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdLimparB 
         Height          =   690
         Left            =   -74865
         TabIndex        =   112
         TabStop         =   0   'False
         ToolTipText     =   "Limpar campos"
         Top             =   5288
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadastro.frx":B320
         PICN            =   "frmCadastro.frx":B33C
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label Label96 
         Caption         =   "99"
         BeginProperty Font 
            Name            =   "MS Serif"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   135
         Left            =   10485
         TabIndex        =   117
         Top             =   2857
         Width           =   300
      End
      Begin VB.Label Label95 
         Caption         =   "999"
         BeginProperty Font 
            Name            =   "MS Serif"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   135
         Left            =   10620
         TabIndex        =   116
         Top             =   1597
         Width           =   300
      End
      Begin VB.Label Label94 
         Caption         =   "99"
         BeginProperty Font 
            Name            =   "MS Serif"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   135
         Left            =   10485
         TabIndex        =   115
         Top             =   1967
         Width           =   255
      End
      Begin VB.Label Label93 
         Caption         =   "999"
         BeginProperty Font 
            Name            =   "MS Serif"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   135
         Left            =   10620
         TabIndex        =   114
         Top             =   2497
         Width           =   345
      End
   End
End
Attribute VB_Name = "frmCadastro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmbFornecedor_Click()

    If Trim(cmbFornecedor) <> "" Then
        cmbSubgrupo.Clear
        cmbSubgrupo.AddItem "0"
        'carrega combo Grupo
        cmbGrupo.Clear
        cmbGrupo.AddItem "0"
        Call vVB_Generica_001.ExcluiBind(vBanco)
        vBanco.Parameters.Add "vCursor", 0, 3
        vBanco.Parameters("vCursor").ServerType = 102
        vBanco.Parameters("vCursor").DynasetOption = &H2&
        vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
        vBanco.Parameters.Add "vCodForn", Mid(cmbFornecedor, 1, 3), 1
        vSql = "PRODUCAO.PCK_CAD920.PR_SEL_GRUPO(:vCursor,:vCodForn)"
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        If vErro <> "" Then
            vVB_Generica_001.ProcessaErro (vErro & ". Erro ao ler Grupo.")
            Exit Sub
        Else
            Set vObjOracle = vBanco.Parameters("vCursor").Value
        End If
        If vObjOracle.EOF Then
            Exit Sub
        Else
            While Not vObjOracle.EOF
                cmbGrupo.AddItem Format(vObjOracle!COD_GRUPO, "00") & " - " & vObjOracle!DESC_GRUPO
                vObjOracle.MoveNext
            Wend
        End If
    End If

End Sub

Private Sub cmbFornecedorB_Click()

    If Trim(cmbFornecedorB) <> "" Then
        cmbSubGrupoB.Clear
        cmbSubGrupoB.AddItem "0"
        'carrega combo Grupo
        cmbGrupoB.Clear
        cmbGrupoB.AddItem "0"
                
        txtDPKB = ""
        'define grid
        mfgBloqueios.Rows = 2
        mfgBloqueios.Cols = 7
        mfgBloqueios.Row = 1
        'limpa linha
        For i = 0 To mfgBloqueios.Cols - 1
            mfgBloqueios.Col = i
            mfgBloqueios.Text = ""
        Next i
        
        Call vVB_Generica_001.ExcluiBind(vBanco)
        vBanco.Parameters.Add "vCursor", 0, 3
        vBanco.Parameters("vCursor").ServerType = 102
        vBanco.Parameters("vCursor").DynasetOption = &H2&
        vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
        vBanco.Parameters.Add "vCodForn", Mid(cmbFornecedorB, 1, 3), 1
        vSql = "PRODUCAO.PCK_CAD920.PR_SEL_GRUPO(:vCursor,:vCodForn)"
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        If vErro <> "" Then
            vVB_Generica_001.ProcessaErro (vErro & ". Erro ao ler Grupo.")
            Exit Sub
        Else
            Set vObjOracle = vBanco.Parameters("vCursor").Value
        End If
        If vObjOracle.EOF Then
            Exit Sub
        Else
            While Not vObjOracle.EOF
                cmbGrupoB.AddItem Format(vObjOracle!COD_GRUPO, "00") & " - " & vObjOracle!DESC_GRUPO
                vObjOracle.MoveNext
            Wend
        End If
    End If

End Sub

Private Sub cmbFornecedorF_Click()

    If Trim(cmbLojaF) <> "" And Trim(cmbFornecedorF) <> "" Then
        'limpa campos
        txtDiasF = ""
        txtCalculoF = ""
        txtAE = ""
        txtBE = ""
        txtCE = ""
        txtDE = ""
        txtEE = ""
        txtAM = ""
        txtBM = ""
        txtCM = ""
        txtDM = ""
        txtEM = ""
        txtAP = ""
        txtBP = ""
        txtCP = ""
        txtDP = ""
        txtEP = ""
        'carregar dados
        Call Aguardar
        Call vVB_Generica_001.ExcluiBind(vBanco)
        vBanco.Parameters.Add "vCursor", 0, 3
        vBanco.Parameters("vCursor").ServerType = 102
        vBanco.Parameters("vCursor").DynasetOption = &H2&
        vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
        vBanco.Parameters.Add "vCodLoja", Mid(cmbLojaF, 1, 2), 1
        vBanco.Parameters.Add "vCodForn", Mid(cmbFornecedorF, 1, 3), 1
        vSql = "PRODUCAO.PCK_CAD920.PR_SEL_DADOS_FORN(:vCursor,:vCodLoja,:vCodForn)"
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        Unload frmAguardar
        If vErro <> "" Then
            vVB_Generica_001.ProcessaErro (vErro & ". Erro ao verificar dados Fornecedor.")
            Exit Sub
        Else
            Set vObjOracle = vBanco.Parameters("vCursor").Value
        End If
        If Not vObjOracle.EOF Then
            txtDiasF = Format(vObjOracle!NUM_DIAS_PRIM_ENTRADA, "0000")
            txtCalculoF = vObjOracle!Fl_Calculo
            txtAE = vObjOracle!QTD_EXCESSO_A
            txtBE = vObjOracle!QTD_EXCESSO_B
            txtCE = vObjOracle!QTD_EXCESSO_C
            txtDE = vObjOracle!QTD_EXCESSO_D
            txtEE = vObjOracle!QTD_EXCESSO_E
            txtAM = vObjOracle!QTD_MIN_TRANSF_A
            txtBM = vObjOracle!QTD_MIN_TRANSF_B
            txtCM = vObjOracle!QTD_MIN_TRANSF_C
            txtDM = vObjOracle!QTD_MIN_TRANSF_D
            txtEM = vObjOracle!QTD_MIN_TRANSF_E
            txtAP = vObjOracle!QTD_PROG_TRANSF_A
            txtBP = vObjOracle!QTD_PROG_TRANSF_B
            txtCP = vObjOracle!QTD_PROG_TRANSF_C
            txtDP = vObjOracle!QTD_PROG_TRANSF_D
            txtEP = vObjOracle!QTD_PROG_TRANSF_E
            V_FL_CALCULO = vObjOracle!Fl_Calculo
            V_FL_CALCULO_ANTERIOR = vObjOracle!Fl_Calculo
            txtDiasF.SetFocus
        Else
            V_FL_CALCULO = ""
            V_FL_CALCULO_ANTERIOR = ""
            txtDiasF.SetFocus
        End If
    End If

End Sub

Private Sub cmbGrupo_Click()

    If Trim(cmbFornecedor) <> "" And Trim(cmbGrupo) <> "" Then
        'carrega combo Grupo
        cmbSubgrupo.Clear
        cmbSubgrupo.AddItem "0"
        Call vVB_Generica_001.ExcluiBind(vBanco)
        vBanco.Parameters.Add "vCursor", 0, 3
        vBanco.Parameters("vCursor").ServerType = 102
        vBanco.Parameters("vCursor").DynasetOption = &H2&
        vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
        vBanco.Parameters.Add "vCodGrupo", Mid(cmbGrupo, 1, 2), 1
        vBanco.Parameters.Add "vCodForn", Mid(cmbFornecedor, 1, 3), 1
        vSql = "PRODUCAO.PCK_CAD920.PR_SEL_SUBGRUPO(:vCursor,:vCodGrupo,:vCodForn)"
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        If vErro <> "" Then
            vVB_Generica_001.ProcessaErro (vErro & ". Erro ao ler Subgrupo.")
            Exit Sub
        Else
            Set vObjOracle = vBanco.Parameters("vCursor").Value
        End If
        If vObjOracle.EOF Then
            Exit Sub
        Else
            While Not vObjOracle.EOF
                cmbSubgrupo.AddItem Format(vObjOracle!COD_SUBGRUPO, "00") & " - " & vObjOracle!DESC_SUBGRUPO
                vObjOracle.MoveNext
            Wend
        End If
    End If

End Sub

Private Sub cmbGrupoB_Click()

    If Trim(cmbFornecedorB) <> "" And Trim(cmbGrupoB) <> "" Then
        'carrega combo Grupo
        cmbSubGrupoB.Clear
        cmbSubGrupoB.AddItem "0"
        
        txtDPKB = ""
        'define grid
        mfgBloqueios.Rows = 2
        mfgBloqueios.Cols = 7
        mfgBloqueios.Row = 1
        'limpa linha
        For i = 0 To mfgBloqueios.Cols - 1
            mfgBloqueios.Col = i
            mfgBloqueios.Text = ""
        Next i
        
        Call vVB_Generica_001.ExcluiBind(vBanco)
        vBanco.Parameters.Add "vCursor", 0, 3
        vBanco.Parameters("vCursor").ServerType = 102
        vBanco.Parameters("vCursor").DynasetOption = &H2&
        vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
        vBanco.Parameters.Add "vCodGrupo", Mid(cmbGrupoB, 1, 2), 1
        vBanco.Parameters.Add "vCodForn", Mid(cmbFornecedorB, 1, 3), 1
        vSql = "PRODUCAO.PCK_CAD920.PR_SEL_SUBGRUPO(:vCursor,:vCodGrupo,:vCodForn)"
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        If vErro <> "" Then
            vVB_Generica_001.ProcessaErro (vErro & ". Erro ao ler Subgrupo.")
            Exit Sub
        Else
            Set vObjOracle = vBanco.Parameters("vCursor").Value
        End If
        If vObjOracle.EOF Then
            Exit Sub
        Else
            While Not vObjOracle.EOF
                cmbSubGrupoB.AddItem Format(vObjOracle!COD_SUBGRUPO, "00") & " - " & vObjOracle!DESC_SUBGRUPO
                vObjOracle.MoveNext
            Wend
        End If
    End If

End Sub

Private Sub cmbLojaF_Click()

    cmbFornecedorF.ListIndex = -1
    txtDiasF = ""
    txtCalculoF = ""
    txtAE = ""
    txtBE = ""
    txtCE = ""
    txtDE = ""
    txtEE = ""
    txtAM = ""
    txtBM = ""
    txtCM = ""
    txtDM = ""
    txtEM = ""
    txtAP = ""
    txtBP = ""
    txtCP = ""
    txtDP = ""
    txtEP = ""

End Sub

Private Sub cmbSubGrupoB_Click()

    txtDPKB = ""
    'define grid
    mfgBloqueios.Rows = 2
    mfgBloqueios.Cols = 7
    mfgBloqueios.Row = 1
    'limpa linha
    For i = 0 To mfgBloqueios.Cols - 1
        mfgBloqueios.Col = i
        mfgBloqueios.Text = ""
    Next i

End Sub

Private Sub cmdConsultarB_Click()

    'verifica campos
    If Trim(cmbLojaB) = "" Then
        Call vVB_Generica_001.Informar("Selecione uma Loja.")
        cmbLojaB.SetFocus
        Exit Sub
    End If
    If Trim(cmbFornecedorB) = "" And Trim(txtDPKB) = "" Then
        Call vVB_Generica_001.Informar("Selecione um Fornecedor ou digite o valor DPK.")
        cmbFornecedorB.SetFocus
        Exit Sub
    End If
    If Trim(cmbFornecedorB) <> "" And Trim(cmbGrupoB) = "" Then
        Call vVB_Generica_001.Informar("Selecione o Grupo.")
        cmbGrupoB.SetFocus
        Exit Sub
    End If
    If Trim(cmbFornecedorB) <> "" And Trim(cmbGrupoB) <> "" And Trim(cmbSubGrupoB) = "" Then
        Call vVB_Generica_001.Informar("Selecione o Subgrupo.")
        cmbSubGrupoB.SetFocus
        Exit Sub
    End If

    'carrega grid
    Call Aguardar
    Call vVB_Generica_001.ExcluiBind(vBanco)
    vBanco.Parameters.Add "vCursor", 0, 3
    vBanco.Parameters("vCursor").ServerType = 102
    vBanco.Parameters("vCursor").DynasetOption = &H2&
    vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
    vBanco.Parameters.Add "vLOJA", Mid(cmbLojaB, 1, 2), 1
    vBanco.Parameters.Add "vDPK", IIf(Trim(txtDPKB) = "", "0", txtDPKB), 1
    vBanco.Parameters.Add "vFORN", IIf(Trim(Mid(cmbFornecedorB, 1, 3)) = "", "0", Mid(cmbFornecedorB, 1, 3)), 1
    vBanco.Parameters.Add "vGRUPO", IIf(Trim(Mid(cmbGrupoB, 1, 2)) = "", "0", Mid(cmbGrupoB, 1, 2)), 1
    vBanco.Parameters.Add "vSUBGRUPO", IIf(Trim(Mid(cmbSubGrupoB, 1, 2)) = "", "0", Mid(cmbSubGrupoB, 1, 2)), 1
    vSql = "PRODUCAO.PCK_CAD920.PR_CON_BLOQ(:vCursor,:vLOJA,:vDPK,:vFORN,:vGRUPO,:vSUBGRUPO)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    Unload frmAguardar
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro & ". Erro ao consultar bloqueios.")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("vCursor").Value
    End If
    If Not vObjOracle.EOF Then
        'define grid
        mfgBloqueios.Rows = 2
        mfgBloqueios.Cols = 7
        mfgBloqueios.Row = 1
        'limpa linha
        For i = 0 To mfgBloqueios.Cols - 1
            mfgBloqueios.Col = i
            mfgBloqueios.Text = ""
        Next i
        'encontra dados
        With mfgBloqueios
            .Row = 1
            While Not vObjOracle.EOF
                .Col = 0
                .Text = Format(vObjOracle!Cod_Loja, "00")
                .Col = 1
                .Text = vObjOracle!Cod_Dpk
                .Col = 2
                .Text = vObjOracle!Fl_Compra
                .Col = 3
                .Text = vObjOracle!Fl_Transf
                .Col = 4
                .Text = IIf(IsNull(vObjOracle!Fl_Bloq_VD_Casada), "", vObjOracle!Fl_Bloq_VD_Casada)
                .Col = 5
                .Text = IIf(IsNull(vObjOracle!Fl_Bloq_Aum_Estoq), "", vObjOracle!Fl_Bloq_Aum_Estoq)
                .Col = 6
                .Text = IIf(IsNull(vObjOracle!Dt_Liberacao), "", vObjOracle!Dt_Liberacao)
                 vObjOracle.MoveNext
                 .Rows = .Rows + 1
                 .Row = .Rows - 1
                .Refresh
            Wend
            .Rows = .Rows - 1
        End With
    Else
        'define grid
        mfgBloqueios.Rows = 2
        mfgBloqueios.Cols = 7
        mfgBloqueios.Row = 1
        'limpa linha
        For i = 0 To mfgBloqueios.Cols - 1
            mfgBloqueios.Col = i
            mfgBloqueios.Text = ""
        Next i
        Call vVB_Generica_001.Informar("N�o existem bloqueios para o filtro informado.")
        Call cmdLimparB_Click
    End If

End Sub

Private Sub cmdGravarF_Click()

Dim vLoja As Long
Dim vForn As Long
Dim vExcA As Long, vExcB As Long, vExcC As Long, vExcD As Long, vExcE As Long
Dim vMinA As Long, vMinB As Long, vMinC As Long, vMinD As Long, vMinE As Long
Dim vTraA As Long, vTraB As Long, vTraC As Long, vTraD As Long, vTraE As Long
Dim vDias As Long
Dim vFlCalc As String
Dim vFlCalcAnt As String
Dim i As Long
Dim vQtdObs As Long
Dim vRegGravado As Long
Dim vQtdRegGrid As Long

    'Verifica se o grid possui informa��es para serem gravadas
    mfgImportacao.Row = 1
    mfgImportacao.Col = 1
    If Trim(mfgImportacao) = "" Then
        Call vVB_Generica_001.Informar("N�o h� dados para grava��o.")
        Exit Sub
    End If
    
    'Verifica se h� linhas sem observa��o
    vQtdRegGrid = mfgImportacao.Rows - 1
    vQtdObs = 0
    For i = 1 To vQtdRegGrid
        mfgImportacao.Row = i
        mfgImportacao.Col = 19
        If Trim(mfgImportacao) <> "" Then
            vQtdObs = vQtdObs + 1
        End If
    Next i
    If vQtdObs = vQtdRegGrid Then
        Call vVB_Generica_001.Informar("Nenhuma linha importada poder� ser cadastrada. Verifique as observa��es.")
        Exit Sub
    End If
    
    'confirma altera��o
    If vVB_Generica_001.Perguntar("Confirma gravar os dados?") = 7 Then
        Exit Sub
    End If
    
    'Grava as linhas do grid que n�o tiverem criticas (Observa��es)
    vRegGravado = 0
    For i = 1 To vQtdRegGrid
        mfgImportacao.Row = i
        mfgImportacao.Col = 19
        If Trim(mfgImportacao) = "" Then
            With mfgImportacao
                .Col = 0:  vLoja = Val(Trim(.Text))
                .Col = 1:  vForn = Val(Trim(.Text))
                .Col = 2:  vExcA = Val(Trim(.Text))
                .Col = 3:  vExcB = Val(Trim(.Text))
                .Col = 4:  vExcC = Val(Trim(.Text))
                .Col = 5:  vExcD = Val(Trim(.Text))
                .Col = 6:  vExcE = Val(Trim(.Text))
                .Col = 7:  vMinA = Val(Trim(.Text))
                .Col = 8:  vMinB = Val(Trim(.Text))
                .Col = 9:  vMinC = Val(Trim(.Text))
                .Col = 10: vMinD = Val(Trim(.Text))
                .Col = 11: vMinE = Val(Trim(.Text))
                .Col = 12: vTraA = Val(Trim(.Text))
                .Col = 13: vTraB = Val(Trim(.Text))
                .Col = 14: vTraC = Val(Trim(.Text))
                .Col = 15: vTraD = Val(Trim(.Text))
                .Col = 16: vTraE = Val(Trim(.Text))
                .Col = 17: vDias = Val(Trim(.Text))
                .Col = 18: vFlCalc = Trim(.Text)
                .Col = 20: vFlCalcAnt = Trim(.Text)
                'atualiza banco
                Call vVB_Generica_001.ExcluiBind(vBanco)
                vBanco.Parameters.Add ("vLOJA"), vLoja, 1
                vBanco.Parameters.Add ("vFORNECEDOR"), vForn, 1
                vBanco.Parameters.Add ("vEXCA"), vExcA, 1
                vBanco.Parameters.Add ("vEXCB"), vExcB, 1
                vBanco.Parameters.Add ("vEXCC"), vExcC, 1
                vBanco.Parameters.Add ("vEXCD"), vExcD, 1
                vBanco.Parameters.Add ("vEXCE"), vExcE, 1
                vBanco.Parameters.Add ("vMINA"), vMinA, 1
                vBanco.Parameters.Add ("vMINB"), vMinB, 1
                vBanco.Parameters.Add ("vMINC"), vMinC, 1
                vBanco.Parameters.Add ("vMIND"), vMinD, 1
                vBanco.Parameters.Add ("vMINE"), vMinE, 1
                vBanco.Parameters.Add ("vPROGA"), vTraA, 1
                vBanco.Parameters.Add ("vPROGB"), vTraB, 1
                vBanco.Parameters.Add ("vPROGC"), vTraC, 1
                vBanco.Parameters.Add ("vPROGD"), vTraD, 1
                vBanco.Parameters.Add ("vPROGE"), vTraE, 1
                vBanco.Parameters.Add ("vDIAS"), vDias, 1
                vBanco.Parameters.Add ("vFLAG"), UCase(vFlCalc), 1
                vSql = "PRODUCAO.PCK_CAD920.PR_UPD_IMP(:vLOJA,:vFORNECEDOR,:vEXCA,:vEXCB,:vEXCC,:vEXCD,:vEXCE,:vMINA,:vMINB,:vMINC,:vMIND,:vMINE,:vPROGA,:vPROGB,:vPROGC,:vPROGD,:vPROGE,:vDIAS,:vFLAG)"
                vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
                If vErro <> "" Then
                    vVB_Generica_001.ProcessaErro (vErro & ". Erro Grava��o.")
                    Exit Sub
                End If
                vRegGravado = vRegGravado + 1
            End With
        End If
    Next i
    
    'msg usu�rio
    Call vVB_Generica_001.Informar("Foram atualizadas " & vRegGravado & " linhas, de " & vQtdRegGrid & " importadas.")
    
    'define grid
    mfgImportacao.Rows = 2
    mfgImportacao.Cols = 21
    mfgImportacao.Row = 1
    
    'limpa linha
    For i = 0 To mfgImportacao.Cols - 1
        mfgImportacao.Col = i
        mfgImportacao.Text = ""
    Next i
    
End Sub

Private Sub cmdGravarF2_Click()

    If Trim(cmbLojaF) = "" Then
        Call vVB_Generica_001.Informar("Selecione uma Loja.")
        cmbLojaF.SetFocus
        Exit Sub
    End If
    If Trim(cmbFornecedorF) = "" Then
        Call vVB_Generica_001.Informar("Selecione um Fornecedor.")
        cmbFornecedorF.SetFocus
        Exit Sub
    End If
    If Trim(txtDiasF) = "" Then
        Call vVB_Generica_001.Informar("Informe o N� de Dias.")
        txtDiasF.SetFocus
        Exit Sub
    End If
    If Trim(txtCalculoF) = "" Then
        Call vVB_Generica_001.Informar("Informe o Flag C�LCULO.")
        txtCalculoF.SetFocus
        Exit Sub
    End If
    If Trim(txtAE) = "" Or Trim(txtBE) = "" Or Trim(txtCE) = "" Or Trim(txtDE) = "" Or Trim(txtEE) = "" Then
        Call vVB_Generica_001.Informar("Digite os Excessos.")
        txtAE.SetFocus
        Exit Sub
    End If
    If Trim(txtAM) = "" Or Trim(txtBM) = "" Or Trim(txtCM) = "" Or Trim(txtDM) = "" Or Trim(txtEM) = "" Then
        Call vVB_Generica_001.Informar("Digite os M�nimos de Transfer�ncia.")
        txtAM.SetFocus
        Exit Sub
    End If
    If Trim(txtAP) = "" Or Trim(txtBP) = "" Or Trim(txtCP) = "" Or Trim(txtDP) = "" Or Trim(txtEP) = "" Then
        Call vVB_Generica_001.Informar("Digite as Programa��es de Transfer�ncia.")
        txtAP.SetFocus
        Exit Sub
    End If

    'confirma inclus�o
    If vVB_Generica_001.Perguntar("Confirma informa��es?") = 7 Then
        Call cmdLimparF_Click
        Exit Sub
    End If
    
    'atualiza banco
    Call Aguardar
    Call vVB_Generica_001.ExcluiBind(vBanco)
    vBanco.Parameters.Add ("vLOJA"), Mid(cmbLojaF, 1, 2), 1
    vBanco.Parameters.Add ("vFORNECEDOR"), Mid(cmbFornecedorF, 1, 3), 1
    vBanco.Parameters.Add ("vEXCA"), txtAE, 1
    vBanco.Parameters.Add ("vEXCB"), txtBE, 1
    vBanco.Parameters.Add ("vEXCC"), txtCE, 1
    vBanco.Parameters.Add ("vEXCD"), txtDE, 1
    vBanco.Parameters.Add ("vEXCE"), txtEE, 1
    vBanco.Parameters.Add ("vMINA"), txtAM, 1
    vBanco.Parameters.Add ("vMINB"), txtBM, 1
    vBanco.Parameters.Add ("vMINC"), txtCM, 1
    vBanco.Parameters.Add ("vMIND"), txtDM, 1
    vBanco.Parameters.Add ("vMINE"), txtEM, 1
    vBanco.Parameters.Add ("vPROGA"), txtAP, 1
    vBanco.Parameters.Add ("vPROGB"), txtBP, 1
    vBanco.Parameters.Add ("vPROGC"), txtCP, 1
    vBanco.Parameters.Add ("vPROGD"), txtDP, 1
    vBanco.Parameters.Add ("vPROGE"), txtEP, 1
    vBanco.Parameters.Add ("vDIAS"), txtDiasF, 1
    vBanco.Parameters.Add ("vFLAG"), txtCalculoF, 1
    vSql = "PRODUCAO.PCK_CAD920.PR_UPD_IMP(:vLOJA,:vFORNECEDOR,:vEXCA,:vEXCB,:vEXCC,:vEXCD,:vEXCE,:vMINA,:vMINB,:vMINC,:vMIND,:vMINE,:vPROGA,:vPROGB,:vPROGC,:vPROGD,:vPROGE,:vDIAS,:vFLAG)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    Unload frmAguardar
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro & ". Erro Inclus�o.")
        Call cmdLimparF_Click
        Exit Sub
    End If
    
    'registra log
    If (Mid(cmbLojaF, 1, 2) <> 99 And (V_FL_CALCULO <> V_FL_CALCULO_ANTERIOR)) Or (Mid(cmbLojaF, 1, 2) = 99) Then
        V_SQL = "INSERT INTO log_ressuprimento " & _
                "VALUES ('" & NOME_USUARIO & "', " & _
                        "'" & Now & "', " & _
                        "'" & V_FL_CALCULO & "', " & _
                        "' ', " & _
                        "' ', " & _
                        "0, " & _
                        Mid(cmbLojaF, 1, 2) & ", " & _
                        Mid(cmbFornecedorF, 1, 3) & ", " & _
                        "0)"
        V_ACCESS.Execute V_SQL, dbFailOnError
    End If
        
    'atualiza tela
    Call vVB_Generica_001.Informar("Altera��es efetuadas com sucesso.")
    Call cmdLimparF_Click

End Sub

Private Sub cmdGravarI_Click()

    'verifica campos
    If Trim(cmbLojaI) = "" Then
        Call vVB_Generica_001.Informar("Selecione uma Loja.")
        cmbLojaI.SetFocus
        Exit Sub
    End If
    If Trim(txtDPKI) = "" Then
        Call vVB_Generica_001.Informar("Digite o valor DPK.")
        txtDPKI.SetFocus
        Exit Sub
    End If
    If Trim(txtCompraI) = "" Then
        Call vVB_Generica_001.Informar("Digite o valor do Flag COMPRA.")
        txtCompraI.SetFocus
        Exit Sub
    End If
    If Trim(txtTransfI) = "" Then
        Call vVB_Generica_001.Informar("Digite o valor do Flag TRANSFER�NCIA.")
        txtTransfI.SetFocus
        Exit Sub
    End If
    If Trim(txtCasadaI) = "" Then
        Call vVB_Generica_001.Informar("Digite o Flag Bloq. VDA CASADA.")
        txtCasadaI.SetFocus
        Exit Sub
    End If
    If Trim(txtEstoqueI) = "" Then
        Call vVB_Generica_001.Informar("Digite o Flag Bloq. AUM. ESTOQUE.")
        txtEstoqueI.SetFocus
        Exit Sub
    End If
    If (txtCasadaI = "S" And Trim(txtDtLiberacaoI) = "") Or (txtEstoqueI = "S" And Trim(txtDtLiberacaoI) = "") Then
        Call vVB_Generica_001.Informar("Digite a Data de Libera��o.")
        txtDtLiberacaoI.SetFocus
        Exit Sub
    End If
            
    'confirma inclus�o
    If vVB_Generica_001.Perguntar("Confirma informa��es?") = 7 Then
        Call cmdLimparI_Click
        Exit Sub
    End If
    
    'atualiza banco
    Call Aguardar
    Call vVB_Generica_001.ExcluiBind(vBanco)
    vBanco.Parameters.Add "vLOJA", Mid(cmbLojaI, 1, 2), 1
    vBanco.Parameters.Add "vDPK", txtDPKI, 1
    vBanco.Parameters.Add "vFLCOMPRA", txtCompraI, 1
    vBanco.Parameters.Add "vFLTRANSF", txtTransfI, 1
    vBanco.Parameters.Add "vFLCASADA", txtCasadaI, 1
    vBanco.Parameters.Add "vFLESTOQUE", txtEstoqueI, 1
    vBanco.Parameters.Add "vDTLIBERACAO", IIf(Trim(txtDtLiberacaoI) = "", Null, txtDtLiberacaoI), 1
    vSql = "PRODUCAO.PCK_CAD920.PR_INC_ITENS(:vLOJA,:vDPK,:vFLCOMPRA,:vFLTRANSF,:vFLCASADA,:vFLESTOQUE,:vDTLIBERACAO)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    Unload frmAguardar
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro & ". Erro Altera��o.")
        Call cmdLimparI_Click
        Exit Sub
    End If
    
    'grava log
    If Mid(cmbLojaI, 1, 2) <> 99 Then
        'Se foi alterada a flag de compra, registra no log
        If V_FL_COMPRA <> V_FL_COMPRA_ANTERIOR Then
            V_SQL = "INSERT INTO log_ressuprimento " & _
                    "VALUES ('" & NOME_USUARIO & "', " & _
                            "'" & Now & "', " & _
                            "' ', " & _
                            "'" & V_FL_COMPRA & "', " & _
                            "' ', " & _
                            txtDPKI & ", " & _
                            Mid(cmbLojaI, 1, 2) & ", " & _
                            "0, " & _
                            "0)"
            V_ACCESS.Execute V_SQL, dbFailOnError
        End If
        'Se foi alterada a flag de transfer�ncia, registra no log
        If V_FL_TRANSF <> V_FL_TRANSF_ANTERIOR Then
            V_SQL = "INSERT INTO log_ressuprimento " & _
                    "VALUES ('" & NOME_USUARIO & "', " & _
                            "'" & Now & "', " & _
                            "' ', " & _
                            "' ', " & _
                            "'" & V_FL_TRANSF & "', " & _
                            txtDPKI & ", " & _
                            Mid(cmbLojaI, 1, 2) & ", " & _
                            "0, " & _
                            "0)"
            V_ACCESS.Execute V_SQL, dbFailOnError
        End If
    Else
        'Se for todos os cds, grava todo o tipo de log pois n�o tenho par�metro anterior p/ comparar
        V_SQL = "INSERT INTO log_ressuprimento " & _
                "VALUES ('" & NOME_USUARIO & "', " & _
                        "'" & Now & "', " & _
                        "' ', " & _
                        "'" & V_FL_COMPRA & "', " & _
                        "'" & V_FL_TRANSF & "', " & _
                        txtDPKI & ", " & _
                        Mid(cmbLojaI, 1, 2) & ", " & _
                        "0, " & _
                        "0)"
        V_ACCESS.Execute V_SQL, dbFailOnError
    End If
    
    'atualiza tela
    Call vVB_Generica_001.Informar("Altera��o efetuada com sucesso.")
    Call cmdLimparI_Click

End Sub

Private Sub cmdGravarIF_Click()

    'verifica campos
    If Trim(cmbLoja) = "" Then
        Call vVB_Generica_001.Informar("Selecione uma Loja.")
        cmbLoja.SetFocus
        Exit Sub
    End If
    If Trim(cmbFornecedor) = "" Then
        Call vVB_Generica_001.Informar("Selecione um Fornecedor.")
        cmbFornecedor.SetFocus
        Exit Sub
    End If
    If Trim(cmbGrupo) = "" Then
        Call vVB_Generica_001.Informar("Selecione um Grupo.")
        cmbGrupo.SetFocus
        Exit Sub
    End If
    If Trim(cmbSubgrupo) = "" Then
        Call vVB_Generica_001.Informar("Selecione um Subgrupo.")
        cmbSubgrupo.SetFocus
        Exit Sub
    End If
    If Trim(txtCompra) = "" Then
        Call vVB_Generica_001.Informar("Digite o Flag COMPRA.")
        txtCompra.SetFocus
        Exit Sub
    End If
    If Trim(txtTransf) = "" Then
        Call vVB_Generica_001.Informar("Digite o Flag TRANSFER�NCIA.")
        txtTransf.SetFocus
        Exit Sub
    End If
    If Trim(txtCasada) = "" Then
        Call vVB_Generica_001.Informar("Digite o Flag Bloq. VDA CASADA.")
        txtCasada.SetFocus
        Exit Sub
    End If
    If Trim(txtEstoque) = "" Then
        Call vVB_Generica_001.Informar("Digite o Flag Bloq. AUM. ESTOQUE.")
        txtEstoque.SetFocus
        Exit Sub
    End If
    If (txtCasada = "S" And Trim(txtDtLiberacao) = "") Or (txtEstoque = "S" And Trim(txtDtLiberacao) = "") Then
        Call vVB_Generica_001.Informar("Digite a Data de Libera��o.")
        txtDtLiberacao.SetFocus
        Exit Sub
    End If
            
    'confirma inclus�o
    If vVB_Generica_001.Perguntar("Confirma informa��es?") = 7 Then
        Call cmdLimparIF_Click
        Exit Sub
    End If
    
    'atualiza banco
    Call Aguardar
    Call vVB_Generica_001.ExcluiBind(vBanco)
    vBanco.Parameters.Add "vLOJA", Mid(cmbLoja, 1, 2), 1
    vBanco.Parameters.Add "vFORN", Mid(cmbFornecedor, 1, 3), 1
    vBanco.Parameters.Add "vGRUPO", Mid(cmbGrupo, 1, 2), 1
    vBanco.Parameters.Add "vSUBGRUPO", Mid(cmbSubgrupo, 1, 2), 1
    vBanco.Parameters.Add "vFLCOMPRA", txtCompra, 1
    vBanco.Parameters.Add "vFLTRANSF", txtTransf, 1
    vBanco.Parameters.Add "vFLCASADA", txtCasada, 1
    vBanco.Parameters.Add "vFLESTOQUE", txtEstoque, 1
    vBanco.Parameters.Add "vDTLIBERACAO", IIf(Trim(txtDtLiberacao) = "", Null, txtDtLiberacao), 1
    vSql = "PRODUCAO.PCK_CAD920.PR_INC_ITENS_FORN(:vLOJA,:vFORN,:vGRUPO,:vSUBGRUPO,:vFLCOMPRA,:vFLTRANSF,:vFLCASADA,:vFLESTOQUE,:vDTLIBERACAO)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    Unload frmAguardar
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro & ". Erro 'Inclus�o'.")
        Call cmdLimparIF_Click
        Exit Sub
    End If
    
    'registro no log
    V_SQL = "INSERT INTO log_ressuprimento VALUES " & _
            "('" & NOME_USUARIO & "'," & _
            "'" & Now & "'," & _
            "' ', " & _
            "'" & UCase(txtCompra) & "', " & _
            "'" & UCase(txtTransf) & "', " & _
            "0, " & _
            Mid(cmbLoja, 1, 2) & ", " & _
            Mid(cmbFornecedor, 1, 3) & ", " & _
            "0) "
    V_ACCESS.Execute V_SQL, dbFailOnError
    
    'atualiza tela
    Call vVB_Generica_001.Informar("Altera��es efetuadas com sucesso.")
    Call cmdLimparIF_Click

End Sub

Private Sub cmdImportarF_Click()

Dim vLoja As String, vForn As String
Dim vEX_A As String, vEX_B As String, vEX_C As String, vEX_D As String, vEX_E As String
Dim vMT_A As String, vMT_B As String, vMT_C As String, vMT_D As String, vMT_E As String
Dim vPT_A As String, vPT_B As String, vPT_C As String, vPT_D As String, vPT_E As String
Dim v1ENTR As String, vFlCalc As String, vFlCalcAnt As String
Dim vIM_A As Long, vIM_B As Long, vIM_C As Long, vIM_D As Long, vIM_E As Long
Dim i As Long
Dim vFl_Ok As Boolean
Dim v_arquivo As String

    frmImportar.Show 1
    
    If vArquivo <> "" Then
    
        'confirma importa��o
        If vVB_Generica_001.Perguntar("Confirma importa��o?") = 7 Then
            Exit Sub
        End If
        
        'prepara grid
        'define grid
        mfgImportacao.Rows = 2
        mfgImportacao.Cols = 21
        mfgImportacao.Row = 1
        'limpa linha
        For i = 0 To mfgImportacao.Cols - 1
            mfgImportacao.Col = i
            mfgImportacao.Text = ""
        Next i
        
        'l� arquivo
        Open vArquivo For Input As #1
        i = mfgImportacao.Rows - 1
        Do While Not EOF(1)
            
            With mfgImportacao
            
                'L� registro - linha - do arquivo, joga na vari�vel  e seta ponteiro para o pr�ximo
                Line Input #1, v_arquivo
                If Len(v_arquivo) <> 55 Then
                    Call vVB_Generica_001.Informar("Arquivo inv�lido.")
                    Close #1
                    Exit Sub
                End If
                
                'Carregando vari�veis com as informa��es do arquivo
                vLoja = Mid(v_arquivo, 1, 2)
                vForn = Mid(v_arquivo, 3, 3)
                vEX_A = IIf(Trim(Mid(v_arquivo, 6, 3)) = "", "0", Mid(v_arquivo, 6, 3))
                vEX_B = IIf(Trim(Mid(v_arquivo, 9, 3)) = "", "0", Mid(v_arquivo, 9, 3))
                vEX_C = IIf(Trim(Mid(v_arquivo, 12, 3)) = "", "0", Mid(v_arquivo, 12, 3))
                vEX_D = IIf(Trim(Mid(v_arquivo, 15, 3)) = "", "0", Mid(v_arquivo, 15, 3))
                vEX_E = IIf(Trim(Mid(v_arquivo, 18, 3)) = "", "0", Mid(v_arquivo, 18, 3))
                vMT_A = IIf(Trim(Mid(v_arquivo, 21, 3)) = "", "0", Mid(v_arquivo, 21, 3))
                vMT_B = IIf(Trim(Mid(v_arquivo, 24, 3)) = "", "0", Mid(v_arquivo, 24, 3))
                vMT_C = IIf(Trim(Mid(v_arquivo, 27, 3)) = "", "0", Mid(v_arquivo, 27, 3))
                vMT_D = IIf(Trim(Mid(v_arquivo, 30, 3)) = "", "0", Mid(v_arquivo, 30, 3))
                vMT_E = IIf(Trim(Mid(v_arquivo, 33, 3)) = "", "0", Mid(v_arquivo, 33, 3))
                vPT_A = IIf(Trim(Mid(v_arquivo, 36, 3)) = "", "0", Mid(v_arquivo, 36, 3))
                vPT_B = IIf(Trim(Mid(v_arquivo, 39, 3)) = "", "0", Mid(v_arquivo, 39, 3))
                vPT_C = IIf(Trim(Mid(v_arquivo, 42, 3)) = "", "0", Mid(v_arquivo, 42, 3))
                vPT_D = IIf(Trim(Mid(v_arquivo, 45, 3)) = "", "0", Mid(v_arquivo, 45, 3))
                vPT_E = IIf(Trim(Mid(v_arquivo, 48, 3)) = "", "0", Mid(v_arquivo, 48, 3))
                v1ENTR = Mid(v_arquivo, 51, 4)
                vFlCalc = Mid(v_arquivo, 55, 1)
                stbBarra.Panels(1).Text = "Importando arquivo... Loja: " & Format(Val(vLoja), "00") & " Fornecedor: " & Format(Val(vForn), "000")
                                 
                'Verifica qual o valor da flag de c�lculo que est� valendo no registro da tabela
                Call vVB_Generica_001.ExcluiBind(vBanco)
                vBanco.Parameters.Add "vCursor", 0, 3
                vBanco.Parameters("vCursor").ServerType = 102
                vBanco.Parameters("vCursor").DynasetOption = &H2&
                vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
                vBanco.Parameters.Add "vLOJA", vLoja, 1
                vBanco.Parameters.Add "vFORN", vForn, 1
                vSql = "PRODUCAO.PCK_CAD920.PR_VER_IMP_1(:vCursor,:vLOJA,:vFORN)"
                vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
                If vErro <> "" Then
                    vVB_Generica_001.ProcessaErro (vErro & ". Erro Importa��o 1.")
                    Exit Sub
                Else
                    Set vObjOracle = vBanco.Parameters("vCursor").Value
                End If
                If Not vObjOracle.EOF Then
                    vFlCalcAnt = vObjOracle!Fl_Calculo.Value
                Else
                    vFlCalcAnt = ""
                End If
                
                'Carregando grid com os dados do arquivos
                .Rows = i + 1
                .Row = i
                .Col = 0:  .Text = vLoja    'Loja
                .Col = 1:  .Text = vForn    'Fornecedor
                .Col = 2:  .Text = vEX_A    'Excesso A
                .Col = 3:  .Text = vEX_B    'Excesso B
                .Col = 4:  .Text = vEX_C    'Excesso C
                .Col = 5:  .Text = vEX_D    'Excesso D
                .Col = 6:  .Text = vEX_E    'Excesso E
                .Col = 7:  .Text = vMT_A    'Min.Transf. A
                .Col = 8:  .Text = vMT_B    'Min.Transf. B
                .Col = 9:  .Text = vMT_C    'Min.Transf. C
                .Col = 10: .Text = vMT_D    'Min.Transf. D
                .Col = 11: .Text = vMT_E    'Min.Transf. E
                .Col = 12: .Text = vPT_A    'Prog.Transf. A
                .Col = 13: .Text = vPT_B    'Prog.Transf. B
                .Col = 14: .Text = vPT_C    'Prog.Transf. C
                .Col = 15: .Text = vPT_D    'Prog.Transf. D
                .Col = 16: .Text = vPT_E    'Prog.Transf. E
                .Col = 17: .Text = v1ENTR   'No.dias primeira entrada
                .Col = 18: .Text = vFlCalc  'Flag C�lculo
                .Col = 20: .Text = vFlCalcAnt  'Flag C�lculo Anterior
                
                vFl_Ok = True
                'valida dados do arquivo
                If Trim(vLoja) = "" Then
                    .Col = 19: .Text = "Loja inv�lida."
                    vFl_Ok = False
                ElseIf Trim(vForn) = "" Or Val(vForn) = 0 Then
                    .Col = 19: .Text = "Fornecedor inv�lido."
                    vFl_Ok = False
                ElseIf Trim(v1ENTR) = "" Then
                    .Col = 19: .Text = "Dt. 1a. Entrada inv�lida."
                    vFl_Ok = False
                ElseIf Trim(vFlCalc) = "" Or (Trim(vFlCalc) <> "S" And Trim(vFlCalc) <> "N") Then
                    .Col = 19: .Text = "Flag de C�lculo invalido."
                    vFl_Ok = False
                End If
                
                If vFl_Ok = True Then
                    'valida c�digo de loja
                    Call vVB_Generica_001.ExcluiBind(vBanco)
                    vBanco.Parameters.Add "vCursor", 0, 3
                    vBanco.Parameters("vCursor").ServerType = 102
                    vBanco.Parameters("vCursor").DynasetOption = &H2&
                    vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
                    vBanco.Parameters.Add "vLOJA", vLoja, 1
                    vSql = "PRODUCAO.PCK_CAD920.PR_VER_IMP_2(:vCursor,:vLOJA)"
                    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
                    If vErro <> "" Then
                        vVB_Generica_001.ProcessaErro (vErro & ". Erro Importa��o 2.")
                        Exit Sub
                    Else
                        Set vObjOracle = vBanco.Parameters("vCursor").Value
                    End If
                    If (vObjOracle!Qtd.Value = 0 And Val(vLoja) <> 99) Then
                        .Col = 19: .Text = "Loja inexistente."
                        vFl_Ok = False
                    End If
                    If vFl_Ok = True Then
                        'Valida se pode usar loja 99 - Todas
                        If Val(vLoja) = 99 Then
                            Call vVB_Generica_001.ExcluiBind(vBanco)
                            vBanco.Parameters.Add "vCursor", 0, 3
                            vBanco.Parameters("vCursor").ServerType = 102
                            vBanco.Parameters("vCursor").DynasetOption = &H2&
                            vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
                            vBanco.Parameters.Add "vFORN", vForn, 1
                            vSql = "PRODUCAO.PCK_CAD920.PR_VER_IMP_3(:vCursor,:vFORN)"
                            vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
                            If vErro <> "" Then
                                vVB_Generica_001.ProcessaErro (vErro & ". Erro Importa��o 3.")
                                Exit Sub
                            Else
                                Set vObjOracle = vBanco.Parameters("vCursor").Value
                            End If
                            'Se n�o houver registro para este fornecedor, a inclus�o tem que ser feita loja a loja
                            If vObjOracle!Qtd = 0 Then
                                .Col = 19: .Text = "Loja 99 inv�lida para este Fornecedor."
                                vFl_Ok = False
                            End If
                        End If
                        If vFl_Ok = True Then
                            'valida c�digo de fornecedor
                            Call vVB_Generica_001.ExcluiBind(vBanco)
                            vBanco.Parameters.Add "vCursor", 0, 3
                            vBanco.Parameters("vCursor").ServerType = 102
                            vBanco.Parameters("vCursor").DynasetOption = &H2&
                            vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
                            vBanco.Parameters.Add "vFORN", vForn, 1
                            vSql = "PRODUCAO.PCK_CAD920.PR_VER_IMP_4(:vCursor,:vFORN)"
                            vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
                            If vErro <> "" Then
                                vVB_Generica_001.ProcessaErro (vErro & ". Erro Importa��o 4.")
                                Exit Sub
                            Else
                                Set vObjOracle = vBanco.Parameters("vCursor").Value
                            End If
                           If vObjOracle!Qtd.Value = 0 Then
                                .Col = 19: .Text = "Fornecedor inexistente ou desativado!"
                                vFl_Ok = False
                            End If
                        End If
                    End If
                End If
                
                i = i + 1
            End With
        Loop
        
        Call vVB_Generica_001.Informar("Importa��o efetuada com sucesso.")
        Close #1
        stbBarra.Panels(1).Text = ""
        
    End If

End Sub

Private Sub cmdImportarI_Click()

Dim V_LOJA As String
Dim V_DPK As String
Dim V_FL_COMPRA As String
Dim V_FL_TRANSF As String
Dim V_FL_CASADA As String
Dim V_FL_ESTOQUE As String
Dim V_FL_DATA As String

    Call cmdLimparI_Click

    frmImportar.Show 1
    
    If vArquivo <> "" Then
        'confirma importa��o
        If vVB_Generica_001.Perguntar("Confirma importa��o?") = 7 Then
            Exit Sub
        End If
        Open vArquivo For Input As #1
        While Not EOF(1)
            Input #1, V_LOJA, V_DPK, V_FL_COMPRA, V_FL_TRANSF, V_FL_CASADA, V_FL_ESTOQUE, V_FL_DATA
            'verifica se o padr�o do arquivo est� correto
            If (Len(V_LOJA) <> 2) Or (Len(V_DPK) <> 5) Or (Len(V_FL_COMPRA) <> 1) Or (Len(V_FL_TRANSF) <> 1) Or (V_FL_COMPRA <> "S" And V_FL_COMPRA <> "N") Or (V_FL_TRANSF <> "S" And V_FL_TRANSF <> "N") Or (V_FL_CASADA <> "S" And V_FL_CASADA <> "N") Or (V_FL_ESTOQUE <> "S" And V_FL_ESTOQUE <> "N") Or (Len(V_FL_DATA) <> 6) Then
                Call vVB_Generica_001.Informar("Arquivo inv�lido.")
                Close #1
                Exit Sub
            End If
            stbBarra.Panels(1).Text = "Importando arquivo... Loja: " & Val(V_LOJA) & " DPK: " & Val(V_DPK)
            'atualiza banco
            Call Aguardar
            Call vVB_Generica_001.ExcluiBind(vBanco)
            vBanco.Parameters.Add "vLOJA", Val(V_LOJA), 1
            vBanco.Parameters.Add "vDPK", Val(V_DPK), 1
            vBanco.Parameters.Add "vFLCOMPRA", V_FL_COMPRA, 1
            vBanco.Parameters.Add "vFLTRANSF", V_FL_TRANSF, 1
            vBanco.Parameters.Add "vFLCASADA", V_FL_CASADA, 1
            vBanco.Parameters.Add "vFLESTOQUE", V_FL_ESTOQUE, 1
            vBanco.Parameters.Add "vDTLIBERACAO", Mid(V_FL_DATA, 1, 2) & "/" & Mid(V_FL_DATA, 3, 2) & "/" & Mid(V_FL_DATA, 5, 2), 1
            vSql = "PRODUCAO.PCK_CAD920.PR_UPD_IMP_I(:vLOJA,:vDPK,:vFLCOMPRA,:vFLTRANSF,:vFLCASADA,:vFLESTOQUE,:vDTLIBERACAO)"
            vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
            Unload frmAguardar
            If vErro <> "" Then
                vVB_Generica_001.ProcessaErro (vErro & ". Erro Importa��o.")
                Exit Sub
            End If
            stbBarra.Panels(1).Text = "Atualizando Loja: " & Val(V_LOJA) & " DPK: " & Val(V_DPK)
        Wend
        stbBarra.Panels(1).Text = ""
        Close #1
        Call vVB_Generica_001.Informar("Importa��o efetuada com sucesso.")
    End If

End Sub

Private Sub cmdLimparB_Click()

    cmbLojaB.ListIndex = -1
    cmbFornecedorB.ListIndex = -1
    cmbGrupoB.ListIndex = -1
    cmbGrupoB.Clear
    cmbSubGrupoB.ListIndex = -1
    cmbSubGrupoB.Clear
    txtDPKB = ""
    
    'define grid
    mfgBloqueios.Rows = 2
    mfgBloqueios.Cols = 7
    mfgBloqueios.Row = 1
    'limpa linha
    For i = 0 To mfgBloqueios.Cols - 1
        mfgBloqueios.Col = i
        mfgBloqueios.Text = ""
    Next i
    
    cmbLojaB.SetFocus

End Sub

Private Sub cmdLimparF_Click()

    cmbLojaF.ListIndex = -1
    cmbFornecedorF.ListIndex = -1
    txtDiasF = ""
    txtCalculoF = ""
    txtAE = ""
    txtBE = ""
    txtCE = ""
    txtDE = ""
    txtEE = ""
    txtAM = ""
    txtBM = ""
    txtCM = ""
    txtDM = ""
    txtEM = ""
    txtAP = ""
    txtBP = ""
    txtCP = ""
    txtDP = ""
    txtEP = ""
    cmbLojaF.SetFocus

End Sub

Private Sub cmdLimparI_Click()

    cmbLojaI.ListIndex = -1
    cmbFornecedorI.ListIndex = -1
    txtCodFabricaI = ""
    txtDPKI = ""
    txtCompraI = ""
    txtTransfI = ""
    V_FL_COMPRA = ""
    V_FL_TRANSF = ""
    
    txtCasadaI = ""
    txtEstoqueI = ""
    txtDtLiberacaoI = ""
    
    'define grid
    mfgItens.Rows = 2
    mfgItens.Cols = 6
    mfgItens.Row = 1
    'limpa linha
    For i = 0 To mfgItens.Cols - 1
        mfgItens.Col = i
        mfgItens.Text = ""
    Next i
    
    cmbLojaI.SetFocus

End Sub

Private Sub cmdLimparIF_Click()

    cmbLoja.ListIndex = -1
    cmbFornecedor.ListIndex = -1
    cmbGrupo.ListIndex = -1
    cmbSubgrupo.ListIndex = -1
    txtCompra = ""
    txtTransf = ""
    txtCasada = ""
    txtEstoque = ""
    txtDtLiberacao = ""
    cmbLoja.SetFocus

End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub

Private Sub Form_GotFocus()

    stbParametros.Tab = 0

End Sub

Private Sub Form_Load()

Dim i As Integer
    
    Me.Top = 0
    Me.Left = 0
    
    'Itens ---------------------------------
    'define grid
    mfgItens.Rows = 2
    mfgItens.Cols = 6
    mfgItens.Row = 1
    
    'limpa linha
    For i = 0 To mfgItens.Cols - 1
        mfgItens.Col = i
        mfgItens.Text = ""
    Next i
    
    'cabe�alho e alinhamento coluna
    mfgItens.Row = 0
    
    mfgItens.Col = 0
    mfgItens.Text = "Qtde. Excesso"
    mfgItens.FixedAlignment(0) = 4
    mfgItens.ColAlignment(0) = 1
    mfgItens.ColWidth(0) = 1400
    
    mfgItens.Col = 1
    mfgItens.Text = "Qtde. Reserva"
    mfgItens.FixedAlignment(1) = 4
    mfgItens.ColAlignment(1) = 1
    mfgItens.ColWidth(1) = 1400
    
    mfgItens.Col = 2
    mfgItens.Text = "Nec. Transf."
    mfgItens.FixedAlignment(2) = 4
    mfgItens.ColAlignment(2) = 1
    mfgItens.ColWidth(2) = 1200
    
    mfgItens.Col = 3
    mfgItens.Text = "Nec. Im."
    mfgItens.FixedAlignment(3) = 4
    mfgItens.ColAlignment(3) = 1
    mfgItens.ColWidth(3) = 800
    
    mfgItens.Col = 4
    mfgItens.Text = "Nec. 30"
    mfgItens.FixedAlignment(4) = 4
    mfgItens.ColAlignment(4) = 1
    mfgItens.ColWidth(4) = 800
    
    mfgItens.Col = 5
    mfgItens.Text = "Nec. 60"
    mfgItens.FixedAlignment(5) = 4
    mfgItens.ColAlignment(5) = 1
    mfgItens.ColWidth(5) = 800
    
    'carrega combo Loja
    cmbLojaI.Clear
    Call vVB_Generica_001.ExcluiBind(vBanco)
    vBanco.Parameters.Add "vCursor", 0, 3
    vBanco.Parameters("vCursor").ServerType = 102
    vBanco.Parameters("vCursor").DynasetOption = &H2&
    vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
    vSql = "PRODUCAO.PCK_CAD920.PR_SEL_LOJAS(:vCursor)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro & ". Erro ao ler Lojas.")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("vCursor").Value
    End If
    While Not vObjOracle.EOF
        cmbLojaI.AddItem Format(vObjOracle!Cod_Loja, "00") & " - " & vObjOracle!Nome_Fantasia
        vObjOracle.MoveNext
    Wend
    cmbLojaI.AddItem "99 - TODOS"
    
    'carrega combo Fornecedor
    cmbFornecedorI.Clear
    Call vVB_Generica_001.ExcluiBind(vBanco)
    vBanco.Parameters.Add "vCursor", 0, 3
    vBanco.Parameters("vCursor").ServerType = 102
    vBanco.Parameters("vCursor").DynasetOption = &H2&
    vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
    vSql = "PRODUCAO.PCK_CAD920.PR_SEL_FORN(:vCursor)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro & ". Erro ao ler Fornecedores.")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("vCursor").Value
    End If
    While Not vObjOracle.EOF
        cmbFornecedorI.AddItem Format(vObjOracle!cod_fornecedor, "000") & " - " & vObjOracle!nome_fornec
        vObjOracle.MoveNext
    Wend
    
    'Fornecedores ---------------------------------
    'define grid
    mfgImportacao.Rows = 2
    mfgImportacao.Cols = 21
    mfgImportacao.Row = 1
    
    'limpa linha
    For i = 0 To mfgImportacao.Cols - 1
        mfgImportacao.Col = i
        mfgImportacao.Text = ""
    Next i
    
    'cabe�alho e alinhamento coluna
    With mfgImportacao
        .Row = 0
        .ColWidth(0) = 550:  .Col = 0:  .ColAlignment(0) = 4: .Text = "Loja"
        .ColWidth(1) = 550:  .Col = 1:  .ColAlignment(1) = 4: .Text = "Forn"
        .ColWidth(2) = 800:  .Col = 2:  .ColAlignment(2) = 4: .Text = "Exc A"
        .ColWidth(3) = 800:  .Col = 3:  .ColAlignment(3) = 4: .Text = "Exc B"
        .ColWidth(4) = 800:  .Col = 4:  .ColAlignment(4) = 4: .Text = "Exc C"
        .ColWidth(5) = 800:  .Col = 5:  .ColAlignment(5) = 4: .Text = "Exc D"
        .ColWidth(6) = 800:  .Col = 6:  .ColAlignment(6) = 4: .Text = "Exc E"
        .ColWidth(7) = 800:  .Col = 7:  .ColAlignment(7) = 4: .Text = "Min.Tr.A"
        .ColWidth(8) = 800:  .Col = 8:  .ColAlignment(8) = 4: .Text = "Min.Tr.B"
        .ColWidth(9) = 800:  .Col = 9:  .ColAlignment(9) = 4: .Text = "Min.Tr.C"
        .ColWidth(10) = 800: .Col = 10: .ColAlignment(10) = 4: .Text = "Min.Tr.D"
        .ColWidth(11) = 800: .Col = 11: .ColAlignment(11) = 4: .Text = "Min.Tr.E"
        .ColWidth(12) = 800: .Col = 12: .ColAlignment(12) = 4: .Text = "Prg.Tr.A"
        .ColWidth(13) = 800: .Col = 13: .ColAlignment(13) = 4: .Text = "Prg.Tr.B"
        .ColWidth(14) = 800: .Col = 14: .ColAlignment(14) = 4: .Text = "Prg.Tr.C"
        .ColWidth(15) = 800: .Col = 15: .ColAlignment(15) = 4: .Text = "Prg.Tr.D"
        .ColWidth(16) = 800: .Col = 16: .ColAlignment(16) = 4: .Text = "Prg.Tr.E"
        .ColWidth(17) = 600: .Col = 17: .ColAlignment(17) = 4: .Text = "Dias"
        .ColWidth(18) = 800: .Col = 18: .ColAlignment(18) = 4: .Text = "Fl.Calc."
        .ColWidth(19) = 10000: .Col = 19: .Text = "Observa��o"
        .ColWidth(20) = 800: .Col = 20: .ColAlignment(18) = 2: .Text = "Fl.Ant."
    End With
    
    'carrega combo Loja
    cmbLojaF.Clear
    Call vVB_Generica_001.ExcluiBind(vBanco)
    vBanco.Parameters.Add "vCursor", 0, 3
    vBanco.Parameters("vCursor").ServerType = 102
    vBanco.Parameters("vCursor").DynasetOption = &H2&
    vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
    vSql = "PRODUCAO.PCK_CAD920.PR_SEL_LOJAS(:vCursor)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro & ". Erro ao ler Lojas.")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("vCursor").Value
    End If
    While Not vObjOracle.EOF
        cmbLojaF.AddItem Format(vObjOracle!Cod_Loja, "00") & " - " & vObjOracle!Nome_Fantasia
        vObjOracle.MoveNext
    Wend
    
    'carrega combo Fornecedor
    cmbFornecedorF.Clear
    Call vVB_Generica_001.ExcluiBind(vBanco)
    vBanco.Parameters.Add "vCursor", 0, 3
    vBanco.Parameters("vCursor").ServerType = 102
    vBanco.Parameters("vCursor").DynasetOption = &H2&
    vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
    vSql = "PRODUCAO.PCK_CAD920.PR_SEL_FORN(:vCursor)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro & ". Erro ao ler Fornecedores.")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("vCursor").Value
    End If
    While Not vObjOracle.EOF
        cmbFornecedorF.AddItem Format(vObjOracle!cod_fornecedor, "000") & " - " & vObjOracle!nome_fornec
        vObjOracle.MoveNext
    Wend
    
    'Itens do Fornecedor ---------------------------------
    'carrega combo Loja
    cmbLoja.Clear
    Call vVB_Generica_001.ExcluiBind(vBanco)
    vBanco.Parameters.Add "vCursor", 0, 3
    vBanco.Parameters("vCursor").ServerType = 102
    vBanco.Parameters("vCursor").DynasetOption = &H2&
    vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
    vSql = "PRODUCAO.PCK_CAD920.PR_SEL_LOJAS(:vCursor)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro & ". Erro ao ler Lojas.")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("vCursor").Value
    End If
    While Not vObjOracle.EOF
        cmbLoja.AddItem Format(vObjOracle!Cod_Loja, "00") & " - " & vObjOracle!Nome_Fantasia
        vObjOracle.MoveNext
    Wend
    cmbLoja.AddItem "99 - TODOS"
    
    'carrega combo Fornecedor
    cmbFornecedor.Clear
    Call vVB_Generica_001.ExcluiBind(vBanco)
    vBanco.Parameters.Add "vCursor", 0, 3
    vBanco.Parameters("vCursor").ServerType = 102
    vBanco.Parameters("vCursor").DynasetOption = &H2&
    vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
    vSql = "PRODUCAO.PCK_CAD920.PR_SEL_FORN(:vCursor)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro & ". Erro ao ler Fornecedores.")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("vCursor").Value
    End If
    While Not vObjOracle.EOF
        cmbFornecedor.AddItem Format(vObjOracle!cod_fornecedor, "000") & " - " & vObjOracle!nome_fornec
        vObjOracle.MoveNext
    Wend
    cmbFornecedor.AddItem "099 - TODOS"
    
    'Layout ---------------------------------
    lblLayout.Caption = "LL,DDDDD,C,T,V,A,DD/MM/AA" & Chr(13) & Chr(13) & _
                    "LL - Loja" & Chr(13) & _
                    "DDDDD - DPK" & Chr(13) & _
                    "C - Flag de Compra (S/N)" & Chr(13) & _
                    "T - Flag de Transfer�ncia (S/N)" & Chr(13) & _
                    "V - Flag de Bloqueio por Venda Casada (S/N)" & Chr(13) & _
                    "A - Flag de Bloqueio por Aumento de Estoque (S/N)" & Chr(13) & _
                    "DDMMAA - Data de Libera��o dos bloqueios VC e AE"
                    
    'Consulta Bloqueios ---------------------------------
    'define grid
    mfgBloqueios.Rows = 2
    mfgBloqueios.Cols = 7
    mfgBloqueios.Row = 1
    
    'limpa linha
    For i = 0 To mfgBloqueios.Cols - 1
        mfgBloqueios.Col = i
        mfgBloqueios.Text = ""
    Next i
        
    'cabe�alho e alinhamento coluna
    mfgBloqueios.Row = 0
    
    mfgBloqueios.Col = 0
    mfgBloqueios.Text = "LOJA"
    mfgBloqueios.FixedAlignment(0) = 4
    mfgBloqueios.ColAlignment(0) = 4
    mfgBloqueios.ColWidth(0) = 800
    
    mfgBloqueios.Col = 1
    mfgBloqueios.Text = "DPK"
    mfgBloqueios.FixedAlignment(1) = 4
    mfgBloqueios.ColAlignment(1) = 4
    mfgBloqueios.ColWidth(1) = 1000
    
    mfgBloqueios.Col = 2
    mfgBloqueios.Text = "FL COMPRA"
    mfgBloqueios.FixedAlignment(2) = 4
    mfgBloqueios.ColAlignment(2) = 4
    mfgBloqueios.ColWidth(2) = 1200
    
    mfgBloqueios.Col = 3
    mfgBloqueios.Text = "FL TRANSF."
    mfgBloqueios.FixedAlignment(3) = 4
    mfgBloqueios.ColAlignment(3) = 4
    mfgBloqueios.ColWidth(3) = 1200
    
    mfgBloqueios.Col = 4
    mfgBloqueios.Text = "BLOQ. VC"
    mfgBloqueios.FixedAlignment(4) = 4
    mfgBloqueios.ColAlignment(4) = 4
    mfgBloqueios.ColWidth(4) = 1200
    
    mfgBloqueios.Col = 5
    mfgBloqueios.Text = "BLOQ. AE"
    mfgBloqueios.FixedAlignment(5) = 4
    mfgBloqueios.ColAlignment(5) = 4
    mfgBloqueios.ColWidth(5) = 1200
    
    mfgBloqueios.Col = 6
    mfgBloqueios.Text = "DT. LIBER."
    mfgBloqueios.FixedAlignment(6) = 4
    mfgBloqueios.ColAlignment(6) = 4
    mfgBloqueios.ColWidth(6) = 1400
    
    'carrega combo Loja
    cmbLojaB.Clear
    Call vVB_Generica_001.ExcluiBind(vBanco)
    vBanco.Parameters.Add "vCursor", 0, 3
    vBanco.Parameters("vCursor").ServerType = 102
    vBanco.Parameters("vCursor").DynasetOption = &H2&
    vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
    vSql = "PRODUCAO.PCK_CAD920.PR_SEL_LOJAS(:vCursor)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro & ". Erro ao ler Lojas.")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("vCursor").Value
    End If
    While Not vObjOracle.EOF
        cmbLojaB.AddItem Format(vObjOracle!Cod_Loja, "00") & " - " & vObjOracle!Nome_Fantasia
        vObjOracle.MoveNext
    Wend
    cmbLojaB.AddItem "99 - TODOS"
    
    'carrega combo Fornecedor
    cmbFornecedorB.Clear
    Call vVB_Generica_001.ExcluiBind(vBanco)
    vBanco.Parameters.Add "vCursor", 0, 3
    vBanco.Parameters("vCursor").ServerType = 102
    vBanco.Parameters("vCursor").DynasetOption = &H2&
    vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
    vSql = "PRODUCAO.PCK_CAD920.PR_SEL_FORN(:vCursor)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro & ". Erro ao ler Fornecedores.")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("vCursor").Value
    End If
    While Not vObjOracle.EOF
        cmbFornecedorB.AddItem Format(vObjOracle!cod_fornecedor, "000") & " - " & vObjOracle!nome_fornec
        vObjOracle.MoveNext
    Wend
        
End Sub

Private Sub stbParametros_Click(PreviousTab As Integer)

    'limpa tela, para cada Tab clicada
    If stbParametros.Tab = 0 Then
        cmbLojaI.ListIndex = -1
        cmbFornecedorI.ListIndex = -1
        txtCodFabricaI = ""
        txtDPKI = ""
        txtCompraI = ""
        txtTransfI = ""
        txtCasadaI = ""
        txtEstoqueI = ""
        txtDtLiberacaoI = ""
        'define grid
        mfgItens.Rows = 2
        mfgItens.Cols = 6
        mfgItens.Row = 1
        'limpa linha
        For i = 0 To mfgItens.Cols - 1
            mfgItens.Col = i
            mfgItens.Text = ""
        Next i
        V_FL_COMPRA = ""
        V_FL_TRANSF = ""
        cmbLojaI.SetFocus
    End If
    If stbParametros.Tab = 1 Then
        cmbLojaF.ListIndex = -1
        cmbFornecedorF.ListIndex = -1
        txtDiasF = ""
        txtCalculoF = ""
        txtAE = ""
        txtBE = ""
        txtCE = ""
        txtDE = ""
        txtEE = ""
        txtAM = ""
        txtBM = ""
        txtCM = ""
        txtDM = ""
        txtEM = ""
        txtAP = ""
        txtBP = ""
        txtCP = ""
        txtDP = ""
        txtEP = ""
        cmbLojaF.SetFocus
    End If
    If stbParametros.Tab = 2 Then
        cmbLoja.ListIndex = -1
        cmbFornecedor.ListIndex = -1
        cmbGrupo.ListIndex = -1
        cmbSubgrupo.ListIndex = -1
        txtCompra = ""
        txtTransf = ""
        cmbLoja.SetFocus
    End If
    If stbParametros.Tab = 4 Then
        cmbLojaB.ListIndex = -1
        cmbFornecedorB.ListIndex = -1
        cmbGrupoB.ListIndex = -1
        cmbSubGrupoB.ListIndex = -1
        txtDPKB = ""
        'define grid
        mfgBloqueios.Rows = 2
        mfgBloqueios.Cols = 7
        mfgBloqueios.Row = 1
        'limpa linha
        For i = 0 To mfgBloqueios.Cols - 1
            mfgBloqueios.Col = i
            mfgBloqueios.Text = ""
        Next i
        cmbLojaB.SetFocus
    End If

End Sub

Private Sub txtCalculoF_Change()

    V_FL_CALCULO = txtCalculoF

End Sub

Private Sub txtCasadaI_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
    
End Sub

Private Sub txtCasadaI_LostFocus()

    If Trim(txtCasadaI) <> "" Then
        If txtCasadaI <> "S" And txtCasadaI <> "N" Then
            Call vVB_Generica_001.Informar("Flag Bloq. VDA CASADA deve ser 'S' ou 'N'.")
            txtCasadaI = ""
            txtCasadaI.SetFocus
        End If
    End If

End Sub

Private Sub txtDPKB_KeyDown(KeyCode As Integer, Shift As Integer)

    cmbFornecedorB.ListIndex = -1
    cmbGrupoB.ListIndex = -1
    cmbSubGrupoB.ListIndex = -1
    
    'define grid
    mfgBloqueios.Rows = 2
    mfgBloqueios.Cols = 7
    mfgBloqueios.Row = 1
    'limpa linha
    For i = 0 To mfgBloqueios.Cols - 1
        mfgBloqueios.Col = i
        mfgBloqueios.Text = ""
    Next i

End Sub

Private Sub txtEstoqueI_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
    
End Sub

Private Sub txtEstoqueI_LostFocus()

    If Trim(txtEstoqueI) <> "" Then
        If txtEstoqueI <> "S" And txtEstoqueI <> "N" Then
            Call vVB_Generica_001.Informar("Flag Bloq. AUM. ESTOQUE deve ser 'S' ou 'N'.")
            txtEstoqueI = ""
            txtEstoqueI.SetFocus
        End If
    End If

End Sub

Private Sub txtDtLiberacaoI_KeyPress(KeyAscii As Integer)

    Call vVB_Generica_001.Data(KeyAscii, txtDtLiberacaoI)

End Sub

Private Sub txtDtLiberacaoI_LostFocus()

    If Trim(txtDtLiberacaoI) <> "" Then
        If CDate(txtDtLiberacaoI) < CDate(Date) Then
            MsgBox "Data de Libera��o deve ser maior que a data de hoje.", vbInformation, "COM430"
            txtDtLiberacaoI = ""
            txtDtLiberacaoI.SetFocus
            Exit Sub
        End If
    End If

End Sub

Private Sub txtCodFabricaI_GotFocus()

    'verifica campos
    If Trim(cmbLojaI) = "" Then
        Call vVB_Generica_001.Informar("Selecione uma Loja.")
        cmbLojaI.SetFocus
        Exit Sub
    End If
    If Trim(cmbFornecedorI) = "" Then
        Call vVB_Generica_001.Informar("Selecione um Fornecedor.")
        cmbFornecedorI.SetFocus
        Exit Sub
    End If
    
End Sub

Private Sub txtCodFabricaI_KeyDown(KeyCode As Integer, Shift As Integer)

    txtDPKI = ""
    txtDPKI.Enabled = False
    txtCompraI = ""
    txtTransfI = ""
    V_FL_COMPRA = ""
    V_FL_TRANSF = ""
    txtCasadaI = ""
    txtEstoqueI = ""
    txtDtLiberacaoI = ""
    'define grid
    mfgItens.Rows = 2
    mfgItens.Cols = 6
    mfgItens.Row = 1
    'limpa linha
    For i = 0 To mfgItens.Cols - 1
        mfgItens.Col = i
        mfgItens.Text = ""
    Next i
        
End Sub

Private Sub txtCodFabricaI_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
    
End Sub

Private Sub txtCodFabricaI_LostFocus()

    If Trim(txtCodFabricaI) <> "" And Trim(TXTDPK) = "" Then
        TXTDPK = ""
        'verifica DPK
        Call Aguardar
        Call vVB_Generica_001.ExcluiBind(vBanco)
        vBanco.Parameters.Add "vCursor", 0, 3
        vBanco.Parameters("vCursor").ServerType = 102
        vBanco.Parameters("vCursor").DynasetOption = &H2&
        vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
        vBanco.Parameters.Add "vForn", Mid(cmbFornecedorI, 1, 3), 1
        vBanco.Parameters.Add "vFabr", txtCodFabricaI, 1
        vSql = "PRODUCAO.PCK_CAD920.PR_VER_DPK(:vCursor,:vForn,:vFabr)"
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        Unload frmAguardar
        If vErro <> "" Then
            vVB_Generica_001.ProcessaErro (vErro & ". Erro ao verificar DPK.")
            Exit Sub
        Else
            Set vObjOracle = vBanco.Parameters("vCursor").Value
        End If
        If vObjOracle.EOF Then
            Call vVB_Generica_001.Informar("Item n�o cadastrado, desativado ou substitu�do.")
            txtCodFabricaI = ""
            txtCodFabricaI.SetFocus
            txtDPKI.Enabled = True
            Exit Sub
        Else
            txtDPKI = vObjOracle!Cod_Dpk
            txtDPKI.Enabled = True
            Call txtDPKI_LostFocus
        End If
    End If

End Sub

Private Sub txtCompra_Change()

    V_Fl_Compra2 = txtCompra

End Sub

Private Sub txtCompraI_Change()

    V_FL_COMPRA = txtCompraI

End Sub

Private Sub txtDPKI_KeyDown(KeyCode As Integer, Shift As Integer)
    
    txtCodFabricaI = ""
    txtCodFabricaI.Enabled = False
    txtCompraI = ""
    txtTransfI = ""
    V_FL_COMPRA = ""
    V_FL_TRANSF = ""
    txtCasadaI = ""
    txtEstoqueI = ""
    txtDtLiberacaoI = ""
    'define grid
    mfgItens.Rows = 2
    mfgItens.Cols = 6
    mfgItens.Row = 1
    'limpa linha
    For i = 0 To mfgItens.Cols - 1
        mfgItens.Col = i
        mfgItens.Text = ""
    Next i
    
End Sub

Private Sub txtDPKI_GotFocus()

    'verifica campos
    If Trim(cmbLojaI) = "" Then
        Call vVB_Generica_001.Informar("Selecione uma Loja.")
        cmbLojaI.SetFocus
        Exit Sub
    End If
        
End Sub

Private Sub txtDPKI_LostFocus()

    If Trim(txtDPKI) <> "" And (Trim(txtCompraI) = "" Or Trim(txtTransfI) = "") Then
        'verifica DPK
        Call Aguardar
        Call vVB_Generica_001.ExcluiBind(vBanco)
        vBanco.Parameters.Add "vCursor", 0, 3
        vBanco.Parameters("vCursor").ServerType = 102
        vBanco.Parameters("vCursor").DynasetOption = &H2&
        vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
        vBanco.Parameters.Add "vDPK", txtDPKI, 1
        vSql = "PRODUCAO.PCK_CAD920.PR_VER_DPK_2(:vCursor,:vDPK)"
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        Unload frmAguardar
        If vErro <> "" Then
            vVB_Generica_001.ProcessaErro (vErro & ". Erro ao verificar DPK 2.")
            Exit Sub
        Else
            Set vObjOracle = vBanco.Parameters("vCursor").Value
        End If
        If vObjOracle.EOF Then
            Call vVB_Generica_001.Informar("Item n�o cadastrado, desativado ou substitu�do.")
            txtDPKI = ""
            txtDPKI.SetFocus
            txtCodFabricaI.Enabled = True
            Exit Sub
        Else
            For i = 0 To cmbFornecedorI.ListCount - 1
                If cmbFornecedorI.List(i) = Format(CStr(vObjOracle!Forn), "000") & " - " & CStr(vObjOracle!nome_fornec) Then
                    cmbFornecedorI.ListIndex = i
                    Exit For
                End If
            Next
            txtCodFabricaI.Enabled = True
            txtCodFabricaI = vObjOracle!Fabr
        End If
        'verifica DPK - carrega grid
        Call Aguardar
        Call vVB_Generica_001.ExcluiBind(vBanco)
        vBanco.Parameters.Add "vCursor", 0, 3
        vBanco.Parameters("vCursor").ServerType = 102
        vBanco.Parameters("vCursor").DynasetOption = &H2&
        vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
        vBanco.Parameters.Add "vLOJA", Mid(cmbLojaI, 1, 2), 1
        vBanco.Parameters.Add "vDPK", txtDPKI, 1
        vSql = "PRODUCAO.PCK_CAD920.PR_VER_DPK_3(:vCursor,:vLOJA,:vDPK)"
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        Unload frmAguardar
        If vErro <> "" Then
            vVB_Generica_001.ProcessaErro (vErro & ". Erro ao verificar DPK 3.")
            Exit Sub
        Else
            Set vObjOracle = vBanco.Parameters("vCursor").Value
        End If
        If Not vObjOracle.EOF Then
            'encontra dados
            txtCompraI = IIf(Val(cmbLojaI) <> 99, CStr(vObjOracle!Fl_Compra), "")
            txtTransfI = IIf(Val(cmbLojaI) <> 99, CStr(vObjOracle!Fl_Transf), "")
            
            txtCasadaI = IIf(Val(cmbLojaI) <> 99, CStr(vObjOracle!Fl_Bloq_VD_Casada), "")
            txtEstoqueI = IIf(Val(cmbLojaI) <> 99, CStr(vObjOracle!Fl_Bloq_Aum_Estoq), "")
            txtDtLiberacaoI = IIf(Val(cmbLojaI) <> 99, IIf(IsNull(vObjOracle!Dt_Liberacao), "", (vObjOracle!Dt_Liberacao)), "")
            
            V_FL_COMPRA_ANTERIOR = IIf(Val(cmbLojaI) <> 99, CStr(vObjOracle!Fl_Compra), "")
            V_FL_COMPRA = IIf(Val(cmbLojaI) <> 99, CStr(vObjOracle!Fl_Compra), "")
            V_FL_TRANSF_ANTERIOR = IIf(Val(cmbLojaI) <> 99, CStr(vObjOracle!Fl_Transf), "")
            V_FL_TRANSF = IIf(Val(cmbLojaI) <> 99, CStr(vObjOracle!Fl_Transf), "")
            'define grid
            mfgItens.Rows = 2
            mfgItens.Cols = 6
            mfgItens.Row = 1
            'limpa linha
            For i = 0 To mfgItens.Cols - 1
                mfgItens.Col = i
                mfgItens.Text = ""
            Next i
            mfgItens.Row = 1
            mfgItens.Col = 0
            mfgItens.Text = IIf(Val(cmbLojaI) <> 99, CStr(vObjOracle!QTD_EXCESSO), "")
            mfgItens.Col = 1
            mfgItens.Text = IIf(Val(cmbLojaI) <> 99, CStr(vObjOracle!QTD_RESERVA), "")
            mfgItens.Col = 2
            mfgItens.Text = IIf(Val(cmbLojaI) <> 99, CStr(vObjOracle!QTD_NEC_TRANSF), "")
            mfgItens.Col = 3
            mfgItens.Text = IIf(Val(cmbLojaI) <> 99, CStr(vObjOracle!QTD_NEC_COMPRA_IM), "")
            mfgItens.Col = 4
            mfgItens.Text = IIf(Val(cmbLojaI) <> 99, CStr(vObjOracle!QTD_NEC_COMPRA_30), "")
            mfgItens.Col = 5
            mfgItens.Text = IIf(Val(cmbLojaI) <> 99, CStr(vObjOracle!QTD_NEC_COMPRA_60), "")
        Else
            'n�o encontra dados
            txtCompraI = ""
            txtTransfI = ""
            V_FL_COMPRA_ANTERIOR = ""
            V_FL_COMPRA = ""
            V_FL_TRANSF_ANTERIOR = ""
            V_FL_TRANSF = ""
            txtCasadaI = ""
            txtEstoqueI = ""
            txtDtLiberacaoI = ""
            'define grid
            mfgItens.Rows = 2
            mfgItens.Cols = 6
            mfgItens.Row = 1
            'limpa linha
            For i = 0 To mfgItens.Cols - 1
                mfgItens.Col = i
                mfgItens.Text = ""
            Next i
        End If
    End If

End Sub

Private Sub txtTransf_Change()

    V_Fl_Transf2 = txtTransf

End Sub

Private Sub txtTransfI_Change()

    V_FL_TRANSF = txtTransfI

End Sub

Private Sub txtCompra_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
    
End Sub

Private Sub txtCompra_LostFocus()

    If Trim(txtCompra) <> "" Then
        If txtCompra <> "S" And txtCompra <> "N" Then
            Call vVB_Generica_001.Informar("Flag COMPRA deve ser 'S' ou 'N'.")
            txtCompra = ""
            txtCompra.SetFocus
        End If
    End If

End Sub

Private Sub txtTransf_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
    
End Sub

Private Sub txtTransf_LostFocus()

    If Trim(txtTransf) <> "" Then
        If txtTransf <> "S" And txtTransf <> "N" Then
            Call vVB_Generica_001.Informar("Flag TRANSFER�NCIA deve ser 'S' ou 'N'.")
            txtTransf = ""
            txtTransf.SetFocus
        End If
    End If

End Sub

Private Sub txtCasada_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
    
End Sub

Private Sub txtCasada_LostFocus()

    If Trim(txtCasada) <> "" Then
        If txtCasada <> "S" And txtCasada <> "N" Then
            Call vVB_Generica_001.Informar("Flag Bloq. VDA CASADA deve ser 'S' ou 'N'.")
            txtCasada = ""
            txtCasada.SetFocus
        End If
    End If

End Sub

Private Sub txtEstoque_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
    
End Sub

Private Sub txtEstoque_LostFocus()

    If Trim(txtEstoque) <> "" Then
        If txtEstoque <> "S" And txtEstoque <> "N" Then
            Call vVB_Generica_001.Informar("Flag Bloq. AUM. ESTOQUE deve ser 'S' ou 'N'.")
            txtEstoque = ""
            txtEstoque.SetFocus
        End If
    End If

End Sub

Private Sub txtDtLiberacao_KeyPress(KeyAscii As Integer)

    Call vVB_Generica_001.Data(KeyAscii, txtDtLiberacao)

End Sub

Private Sub txtDtLiberacao_LostFocus()

    If Trim(txtDtLiberacao) <> "" Then
        If CDate(txtDtLiberacao) < CDate(Date) Then
            MsgBox "Data de Libera��o deve ser maior que a data de hoje", vbInformation, "COM430"
            txtDtLiberacao = ""
            txtDtLiberacao.SetFocus
            Exit Sub
        End If
    End If

End Sub

Private Sub txtCompraI_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
    
End Sub

Private Sub txtCompraI_LostFocus()

    If Trim(txtCompraI) <> "" Then
        If txtCompraI <> "S" And txtCompraI <> "N" Then
            Call vVB_Generica_001.Informar("Flag COMPRA deve ser 'S' ou 'N'.")
            txtCompraI = ""
            txtCompraI.SetFocus
        End If
    End If

End Sub

Private Sub txtTransfI_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
    
End Sub

Private Sub txtTransfI_LostFocus()

    If Trim(txtTransfI) <> "" Then
        If txtTransfI <> "S" And txtTransfI <> "N" Then
            Call vVB_Generica_001.Informar("Flag TRANSFER�NCIA deve ser 'S' ou 'N'.")
            txtTransfI = ""
            txtTransfI.SetFocus
        End If
    End If

End Sub

Private Sub txtDPKI_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtDPKI)

End Sub

Private Sub txtCalculoF_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
    
End Sub

Private Sub txtCalculoF_LostFocus()

    If Trim(txtCalculoF) <> "" Then
        If txtCalculoF <> "S" And txtCalculoF <> "N" Then
            Call vVB_Generica_001.Informar("Flag C�LCULO deve ser 'S' ou 'N'.")
            txtCalculoF = ""
            txtCalculoF.SetFocus
        End If
    End If

End Sub

Private Sub txtDiasF_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtDiasF)

End Sub

Private Sub txtAE_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtAE)

End Sub

Private Sub txtBE_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtBE)

End Sub

Private Sub txtCE_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtCE)

End Sub

Private Sub txtDE_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtDE)

End Sub

Private Sub txtEE_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtEE)

End Sub

Private Sub txtAM_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtAM)

End Sub

Private Sub txtBM_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtBM)

End Sub

Private Sub txtCM_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtCM)

End Sub

Private Sub txtDM_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtDM)

End Sub

Private Sub txtEM_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtEM)

End Sub

Private Sub txtAP_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtAP)

End Sub

Private Sub txtBP_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtBP)

End Sub

Private Sub txtCP_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtCP)

End Sub

Private Sub txtDP_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtDP)

End Sub

Private Sub txtEP_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtEP)

End Sub

Private Sub txtDPKB_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtDPKB)

End Sub
