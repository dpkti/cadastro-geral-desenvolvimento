create or replace package producao.PCK_CAD920 is

  TYPE TP_CURSOR IS REF CURSOR;

  Procedure PR_SEL_LOJAS (cDados IN OUT TP_CURSOR);

  Procedure PR_SEL_FORN (cDados IN OUT TP_CURSOR);

  Procedure PR_SEL_DADOS_FORN (cDados IN OUT TP_CURSOR,
                               cCodLoja IN ressuprimento.parametro_transF.cod_loja%type,
                               cCodForn IN ressuprimento.parametro_transF.cod_fornecedor%type);

  Procedure PR_SEL_GRUPO (cDados IN OUT TP_CURSOR,
                          cCodForn IN FORNECEDOR.COD_FORNECEDOR%type);

  Procedure PR_SEL_SUBGRUPO (cDados IN OUT TP_CURSOR,
                             cCodGrupo IN ITEM_CADASTRO.COD_GRUPO%type,
                             cCodForn IN FORNECEDOR.COD_FORNECEDOR%type);

  Procedure PR_UPD_IMP (cLoja IN ressuprimento.parametro_transf.cod_loja%type,
                        cForn IN ressuprimento.parametro_transf.cod_fornecedor%type,
                        cExcA IN ressuprimento.parametro_transf.qtd_excesso_a%type,
                        cExcB IN ressuprimento.parametro_transf.qtd_excesso_b%type,
                        cExcC IN ressuprimento.parametro_transf.qtd_excesso_c%type,
                        cExcD IN ressuprimento.parametro_transf.qtd_excesso_d%type,
                        cExcE IN ressuprimento.parametro_transf.qtd_excesso_e%type,
                        cMinA IN ressuprimento.parametro_transf.qtd_min_transf_a%type,
                        cMinB IN ressuprimento.parametro_transf.qtd_min_transf_b%type,
                        cMinC IN ressuprimento.parametro_transf.qtd_min_transf_c%type,
                        cMinD IN ressuprimento.parametro_transf.qtd_min_transf_d%type,
                        cMinE IN ressuprimento.parametro_transf.qtd_excesso_e%type,
                        cTraA IN ressuprimento.parametro_transf.qtd_prog_transf_a%type,
                        cTraB IN ressuprimento.parametro_transf.qtd_prog_transf_b%type,
                        cTraC IN ressuprimento.parametro_transf.qtd_prog_transf_c%type,
                        cTraD IN ressuprimento.parametro_transf.qtd_prog_transf_d%type,
                        cTraE IN ressuprimento.parametro_transf.qtd_prog_transf_e%type,
                        cDias IN ressuprimento.parametro_transf.num_dias_prim_entrada%type,
                        cFLAG IN ressuprimento.parametro_transf.fl_calculo%type);

  Procedure PR_INC_ITENS (cLOJA IN Item_estoque.Cod_Loja%type,
                          cDPK IN Item_estoque.Cod_Dpk%type,
                          cFLCOMPRA IN ressuprimento.item_qtde_excesso.fl_compra%type,
                          cFLTRANSF IN ressuprimento.item_qtde_excesso.fl_transf%type,
                          cFLCASADA IN ressuprimento.item_qtde_excesso.fl_bloq_vd_casada%type,
                          cFLESTOQUE IN ressuprimento.item_qtde_excesso.fl_bloq_aum_estoq%type,
                          cDTLIBERACAO IN string);

  Procedure PR_INC_ITENS_FORN (cLOJA IN Item_estoque.Cod_Loja%type,
                               cFORN IN Item_Cadastro.Cod_Fornecedor%type,
                               cGrupo In Item_Cadastro.Cod_Grupo%type,
                               cSubgrupo In Item_Cadastro.Cod_Subgrupo%type,
                               cFLCOMPRA IN ressuprimento.item_qtde_excesso.fl_compra%type,
                               cFLTRANSF IN ressuprimento.item_qtde_excesso.fl_transf%type,
                               cFLCASADA IN ressuprimento.item_qtde_excesso.fl_bloq_vd_casada%type,
                               cFLESTOQUE IN ressuprimento.item_qtde_excesso.fl_bloq_aum_estoq%type,
                               cDTLIBERACAO IN STRING);

  Procedure PR_VER_DPK (cDados IN OUT TP_CURSOR,
                        cFORN IN Item_Cadastro.Cod_Fornecedor%type,
                        cFABR IN Item_Cadastro.Cod_Fabrica%type);

  Procedure PR_VER_DPK_2 (cDados IN OUT TP_CURSOR,
                          cDPK IN Item_Cadastro.Cod_Dpk%type);

  Procedure PR_VER_DPK_3 (cDados IN OUT TP_CURSOR,
                          cLOJA IN ressuprimento.item_qtde_excesso.cod_loja%type,
                          cDPK IN ressuprimento.item_qtde_excesso.cod_dpk%type);

  Procedure PR_UPD_IMP_I (cLOJA IN ressuprimento.item_qtde_excesso.cod_loja%type,
                        cDPK IN ressuprimento.item_qtde_excesso.cod_dpk%type,
                        cFLCOMPRA IN ressuprimento.item_qtde_excesso.fl_compra%type,
                        cFLTRANSF IN ressuprimento.item_qtde_excesso.fl_transf%type,
                        cFLCASADA IN ressuprimento.item_qtde_excesso.fl_bloq_vd_casada%type,
                        cFLESTOQUE IN ressuprimento.item_qtde_excesso.fl_bloq_aum_estoq%type,
                        cDTLIBERACAO IN string);

  Procedure PR_VER_IMP_1 (cDados IN OUT TP_CURSOR,
                          cLOJA IN ressuprimento.parametro_transf.cod_loja%type,
                          cFORN IN ressuprimento.parametro_transf.cod_fornecedor%type);

  Procedure PR_VER_IMP_2 (cDados IN OUT TP_CURSOR,
                          cLOJA IN loja.cod_loja%type);

  Procedure PR_VER_IMP_3 (cDados IN OUT TP_CURSOR,
                          cFORN IN ressuprimento.parametro_transf.cod_fornecedor%type);

  Procedure PR_VER_IMP_4 (cDados IN OUT TP_CURSOR,
                          cFORN IN FORNECEDOR.COD_FORNECEDOR%type);

  Procedure PR_CON_BLOQ (cDados IN OUT TP_CURSOR,
                         cLOJA IN Item_estoque.Cod_Loja%type,
                         cDPK IN Item_Cadastro.Cod_Dpk%type,
                         cFORN IN Item_Cadastro.Cod_Fornecedor%type,
                         cGrupo In Item_Cadastro.Cod_Grupo%type,
                         cSubgrupo In Item_Cadastro.Cod_Subgrupo%type);

end PCK_CAD920;
/
create or replace package body producao.PCK_CAD920 is

  -- 1) Combo Lojas
  Procedure PR_SEL_LOJAS (cDados IN OUT TP_CURSOR) is
  Begin
       OPEN cDados FOR
       SELECT COD_LOJA,
              NOME_FANTASIA
       FROM   LOJA
       ORDER BY COD_LOJA;
  End;

  -- 2) Fornecedores
  Procedure PR_SEL_FORN (cDados IN OUT TP_CURSOR) is
  Begin
       OPEN cDados FOR
       Select cod_fornecedor, nome_fornec, divisao, classificacao
       From   FORNECEDOR
       Where  situacao = 0
       Order By cod_fornecedor;
  End;

  -- 3) Dados Fornecedor
  Procedure PR_SEL_DADOS_FORN (cDados IN OUT TP_CURSOR,
                               cCodLoja IN ressuprimento.parametro_transF.cod_loja%type,
                               cCodForn IN ressuprimento.parametro_transF.cod_fornecedor%type) is
  Begin
       OPEN cDados FOR
       SELECT QTD_EXCESSO_A,
              QTD_EXCESSO_B,
              QTD_EXCESSO_C,
              QTD_EXCESSO_D,
              QTD_EXCESSO_E,
              QTD_MIN_TRANSF_A,
              QTD_MIN_TRANSF_B,
              QTD_MIN_TRANSF_C,
              QTD_MIN_TRANSF_D,
              QTD_MIN_TRANSF_E,
              QTD_PROG_TRANSF_A,
              QTD_PROG_TRANSF_B,
              QTD_PROG_TRANSF_C,
              QTD_PROG_TRANSF_D,
              QTD_PROG_TRANSF_E,
              NUM_DIAS_PRIM_ENTRADA,
              FL_CALCULO
       FROM   ressuprimento.parametro_transF
       WHERE  COD_LOJA = cCodLoja AND
              COD_FORNECEDOR = cCodForn;
  End;

  -- 5) Grupo
  Procedure PR_SEL_GRUPO (cDados IN OUT TP_CURSOR,
                          cCodForn IN FORNECEDOR.COD_FORNECEDOR%type) is
  Begin
       OPEN cDados FOR
       SELECT Distinct G.COD_GRUPO,
              G.DESC_GRUPO
       FROM   GRUPO G, ITEM_CADASTRO C, FORNECEDOR F
       WHERE  C.Cod_Grupo = G.Cod_Grupo and
              C.Cod_Fornecedor = F.Cod_Fornecedor and
              F.Cod_Fornecedor = cCodForn
       ORDER BY G.COD_GRUPO;
  End;

  -- 6) SubGrupo
  Procedure PR_SEL_SUBGRUPO (cDados IN OUT TP_CURSOR,
                             cCodGrupo IN ITEM_CADASTRO.COD_GRUPO%type,
                             cCodForn IN FORNECEDOR.COD_FORNECEDOR%type) is
  Begin
       OPEN cDados FOR
       Select distinct s.COD_SUBGRUPO, s.DESC_SUBGRUPO
       From   SUBGRUPO s, ITEM_CADASTRO c, FORNECEDOR f
       Where  f.cod_fornecedor = cCodForn and
              c.COD_GRUPO = cCodGrupo and
              f.COD_FORNECEDOR = c.COD_FORNECEDOR and
              c.COD_GRUPO = s.COD_GRUPO
       Order by s.cod_subgrupo;
  End;

  -- 7) Gravar Importação
  Procedure PR_UPD_IMP (cLoja IN ressuprimento.parametro_transf.cod_loja%type,
                        cForn IN ressuprimento.parametro_transf.cod_fornecedor%type,
                        cExcA IN ressuprimento.parametro_transf.qtd_excesso_a%type,
                        cExcB IN ressuprimento.parametro_transf.qtd_excesso_b%type,
                        cExcC IN ressuprimento.parametro_transf.qtd_excesso_c%type,
                        cExcD IN ressuprimento.parametro_transf.qtd_excesso_d%type,
                        cExcE IN ressuprimento.parametro_transf.qtd_excesso_e%type,
                        cMinA IN ressuprimento.parametro_transf.qtd_min_transf_a%type,
                        cMinB IN ressuprimento.parametro_transf.qtd_min_transf_b%type,
                        cMinC IN ressuprimento.parametro_transf.qtd_min_transf_c%type,
                        cMinD IN ressuprimento.parametro_transf.qtd_min_transf_d%type,
                        cMinE IN ressuprimento.parametro_transf.qtd_excesso_e%type,
                        cTraA IN ressuprimento.parametro_transf.qtd_prog_transf_a%type,
                        cTraB IN ressuprimento.parametro_transf.qtd_prog_transf_b%type,
                        cTraC IN ressuprimento.parametro_transf.qtd_prog_transf_c%type,
                        cTraD IN ressuprimento.parametro_transf.qtd_prog_transf_d%type,
                        cTraE IN ressuprimento.parametro_transf.qtd_prog_transf_e%type,
                        cDias IN ressuprimento.parametro_transf.num_dias_prim_entrada%type,
                        cFLAG IN ressuprimento.parametro_transf.fl_calculo%type) is
  Begin
       If cLoja = 99 Then
          UPDATE ressuprimento.parametro_transf
          SET    QTD_EXCESSO_A = cEXCA, QTD_EXCESSO_B = cEXCB,
                 QTD_EXCESSO_C = cEXCC, QTD_EXCESSO_D = cEXCD,
                 QTD_EXCESSO_E = cEXCE,
                 QTD_MIN_TRANSF_A = cMINA,
                 QTD_MIN_TRANSF_B = cMINB,
                 QTD_MIN_TRANSF_C = cMINC,
                 QTD_MIN_TRANSF_D = cMIND,
                 QTD_MIN_TRANSF_E = cMINE,
                 QTD_PROG_TRANSF_A = cTRAA,
                 QTD_PROG_TRANSF_B = cTRAB,
                 QTD_PROG_TRANSF_C = cTRAC,
                 QTD_PROG_TRANSF_D = cTRAD,
                 QTD_PROG_TRANSF_E = cTRAE,
                 NUM_DIAS_PRIM_ENTRADA = cDIAS,
                 FL_CALCULO = cFLAG
          WHERE  COD_FORNECEDOR = cFORN;
       Else
          UPDATE ressuprimento.parametro_transf
          SET    QTD_EXCESSO_A = cEXCA, QTD_EXCESSO_B = cEXCB,
                 QTD_EXCESSO_C = cEXCC, QTD_EXCESSO_D = cEXCD,
                 QTD_EXCESSO_E = cEXCE,
                 QTD_MIN_TRANSF_A = cMINA,
                 QTD_MIN_TRANSF_B = cMINB,
                 QTD_MIN_TRANSF_C = cMINC,
                 QTD_MIN_TRANSF_D = cMIND,
                 QTD_MIN_TRANSF_E = cMINE,
                 QTD_PROG_TRANSF_A = cTRAA,
                 QTD_PROG_TRANSF_B = cTRAB,
                 QTD_PROG_TRANSF_C = cTRAC,
                 QTD_PROG_TRANSF_D = cTRAD,
                 QTD_PROG_TRANSF_E = cTRAE,
                 NUM_DIAS_PRIM_ENTRADA = cDIAS,
                 FL_CALCULO = cFLAG
          WHERE  COD_FORNECEDOR = cFORN and
                 COD_LOJA = cLOJA;

          IF SQL%NOTFOUND THEN
            INSERT INTO ressuprimento.parametro_transf
                   (COD_LOJA, COD_FORNECEDOR, QTD_EXCESSO_A, QTD_EXCESSO_B,
                    QTD_EXCESSO_C, QTD_EXCESSO_D, QTD_EXCESSO_E,
                    QTD_MIN_TRANSF_A, QTD_MIN_TRANSF_B, QTD_MIN_TRANSF_C,
                    QTD_MIN_TRANSF_D, QTD_MIN_TRANSF_E, QTD_PROG_TRANSF_A,
                    QTD_PROG_TRANSF_B, QTD_PROG_TRANSF_C, QTD_PROG_TRANSF_D,
                    QTD_PROG_TRANSF_E, NUM_DIAS_PRIM_ENTRADA, FL_CALCULO)
            VALUES (cLOJA, cFORN,
                    cEXCA, cEXCB, cEXCC, cEXCD, cEXCE,
                    cMINA, cMINB, cMINC, cMIND, cMINE,
                    cTRAA, cTRAB, cTRAC, cTRAD, cTRAE,
                    cDIAS, cFLAG);
          End If;
       End If;

       Commit;

       Exception
          When Others then
          RollBack;
  End;

  -- 8) Inclui Itens
  Procedure PR_INC_ITENS (cLOJA IN Item_estoque.Cod_Loja%type,
                          cDPK IN Item_estoque.Cod_Dpk%type,
                          cFLCOMPRA IN ressuprimento.item_qtde_excesso.fl_compra%type,
                          cFLTRANSF IN ressuprimento.item_qtde_excesso.fl_transf%type,
                          cFLCASADA IN ressuprimento.item_qtde_excesso.fl_bloq_vd_casada%type,
                          cFLESTOQUE IN ressuprimento.item_qtde_excesso.fl_bloq_aum_estoq%type,
                          cDTLIBERACAO IN string) is
  Begin
       If cLOJA <> 99 Then
           UPDATE ressuprimento.item_qtde_excesso
           SET    FL_COMPRA = cFLCOMPRA,
                  FL_TRANSF = cFLTRANSF,
                  fl_bloq_vd_casada = cFLCASADA,
                  fl_bloq_aum_estoq = cFLESTOQUE,
                  dt_liberacao = TO_DATE(cDTLIBERACAO,'DD/MM/RR')
           WHERE  (Cod_Loja, Cod_Dpk) In
                  (Select E.Cod_Loja, E.Cod_Dpk
                   From   Item_estoque E
                   Where  E.Situacao = 0 and
                          E.Cod_Loja = cLOJA and
                          E.Cod_Dpk = cDPK);
       Else
           UPDATE ressuprimento.item_qtde_excesso
           SET    FL_COMPRA = cFLCOMPRA,
                  FL_TRANSF = cFLTRANSF,
                  fl_bloq_vd_casada = cFLCASADA,
                  fl_bloq_aum_estoq = cFLESTOQUE,
                  dt_liberacao = TO_DATE(cDTLIBERACAO,'DD/MM/RR')
           WHERE  (Cod_Loja, Cod_Dpk) In
                  (Select E.Cod_Loja, E.Cod_Dpk
                   From   Item_estoque E
                   Where  E.Situacao = 0 and
                          E.Cod_Dpk = cDPK);
       End If;

       Commit;

       Exception
          When Others then
          RollBack;
  End;

  -- 9) Inclui Itens do Fornecedor
  Procedure PR_INC_ITENS_FORN (cLOJA IN Item_estoque.Cod_Loja%type,
                               cFORN IN Item_Cadastro.Cod_Fornecedor%type,
                               cGrupo In Item_Cadastro.Cod_Grupo%type,
                               cSubgrupo In Item_Cadastro.Cod_Subgrupo%type,
                               cFLCOMPRA IN ressuprimento.item_qtde_excesso.fl_compra%type,
                               cFLTRANSF IN ressuprimento.item_qtde_excesso.fl_transf%type,
                               cFLCASADA IN ressuprimento.item_qtde_excesso.fl_bloq_vd_casada%type,
                               cFLESTOQUE IN ressuprimento.item_qtde_excesso.fl_bloq_aum_estoq%type,
                               cDTLIBERACAO IN STRING) is
  Begin
       If cGrupo = 0 And cSubgrupo = 0 Then
            If cLOJA <> 99 Then
               If cFORN <> 99 Then
                   UPDATE ressuprimento.item_qtde_excesso
                   SET    FL_COMPRA = cFLCOMPRA,
                          FL_TRANSF = cFLTRANSF,
                          fl_bloq_vd_casada = cFLCASADA,
                          fl_bloq_aum_estoq = cFLESTOQUE,
                          dt_liberacao = TO_DATE(cDTLIBERACAO,'DD/MM/RR')
                   WHERE  (Cod_Loja, Cod_Dpk) In
                          (Select E.Cod_Loja, E.Cod_Dpk
                           From   Item_Estoque E, Item_Cadastro C
                           Where  E.Cod_Dpk = C.Cod_Dpk and
                                  E.Cod_Loja = cLOJA and
                                  E.Situacao = 0 and
                                  C.Cod_Fornecedor = cFORN);
               Else
                   UPDATE ressuprimento.item_qtde_excesso
                   SET    FL_COMPRA = cFLCOMPRA,
                          FL_TRANSF = cFLTRANSF,
                          fl_bloq_vd_casada = cFLCASADA,
                          fl_bloq_aum_estoq = cFLESTOQUE,
                          dt_liberacao = TO_DATE(cDTLIBERACAO,'DD/MM/RR')
                   WHERE  (Cod_Loja, Cod_Dpk) In
                          (Select E.Cod_Loja, E.Cod_Dpk
                           From   Item_Estoque E, Item_Cadastro C
                           Where  E.Cod_Dpk = C.Cod_Dpk and
                                  E.Cod_Loja = cLOJA and
                                  E.Situacao = 0);
               End If;
            Else
               If cFORN <> 99 Then
                   UPDATE ressuprimento.item_qtde_excesso
                   SET    FL_COMPRA = cFLCOMPRA,
                          FL_TRANSF = cFLTRANSF,
                          fl_bloq_vd_casada = cFLCASADA,
                          fl_bloq_aum_estoq = cFLESTOQUE,
                          dt_liberacao = TO_DATE(cDTLIBERACAO,'DD/MM/RR')
                   WHERE  (Cod_Loja, Cod_Dpk) In
                          (Select E.Cod_Loja, E.Cod_Dpk
                           From   Item_Estoque E, Item_Cadastro C
                           Where  E.Cod_Dpk = C.Cod_Dpk and
                                  E.Situacao = 0 and
                                  E.Cod_Loja Not IN (2, 6, 11, 12, 13, 16, 17, 18) and
                                  C.Cod_Fornecedor = cFORN);
               Else
                   UPDATE ressuprimento.item_qtde_excesso
                   SET    FL_COMPRA = cFLCOMPRA,
                          FL_TRANSF = cFLTRANSF,
                          fl_bloq_vd_casada = cFLCASADA,
                          fl_bloq_aum_estoq = cFLESTOQUE,
                          dt_liberacao = TO_DATE(cDTLIBERACAO,'DD/MM/RR')
                   WHERE  (Cod_Loja, Cod_Dpk) In
                          (Select E.Cod_Loja, E.Cod_Dpk
                           From   Item_Estoque E, Item_Cadastro C
                           Where  E.Cod_Dpk = C.Cod_Dpk and
                                  E.Situacao = 0 and
                                  E.Cod_Loja Not IN (2, 6, 11, 12, 13, 16, 17, 18));

               End If;
            End If;
       End If;
       If cGrupo <> 0 And cSubgrupo = 0 Then
            If cLOJA <> 99 Then
               If cFORN <> 99 Then
                   UPDATE ressuprimento.item_qtde_excesso
                   SET    FL_COMPRA = cFLCOMPRA,
                          FL_TRANSF = cFLTRANSF,
                          fl_bloq_vd_casada = cFLCASADA,
                          fl_bloq_aum_estoq = cFLESTOQUE,
                          dt_liberacao = TO_DATE(cDTLIBERACAO,'DD/MM/RR')
                   WHERE  (Cod_Loja, Cod_Dpk) In
                          (Select E.Cod_Loja, E.Cod_Dpk
                           From   Item_Estoque E, Item_Cadastro C
                           Where  E.Cod_Dpk = C.Cod_Dpk and
                                  E.Cod_Loja = cLOJA and
                                  E.Situacao = 0 and
                                  C.Cod_Fornecedor = cFORN and
                                  C.Cod_Grupo = cGRUPO);
               Else
                   UPDATE ressuprimento.item_qtde_excesso
                   SET    FL_COMPRA = cFLCOMPRA,
                          FL_TRANSF = cFLTRANSF,
                          fl_bloq_vd_casada = cFLCASADA,
                          fl_bloq_aum_estoq = cFLESTOQUE,
                          dt_liberacao = TO_DATE(cDTLIBERACAO,'DD/MM/RR')
                   WHERE  (Cod_Loja, Cod_Dpk) In
                          (Select E.Cod_Loja, E.Cod_Dpk
                           From   Item_Estoque E, Item_Cadastro C
                           Where  E.Cod_Dpk = C.Cod_Dpk and
                                  E.Cod_Loja = cLOJA and
                                  E.Situacao = 0 and
                                  C.Cod_Grupo = cGRUPO);
               End If;
            Else
               If cFORN <> 99 Then
                   UPDATE ressuprimento.item_qtde_excesso
                   SET    FL_COMPRA = cFLCOMPRA,
                          FL_TRANSF = cFLTRANSF,
                          fl_bloq_vd_casada = cFLCASADA,
                          fl_bloq_aum_estoq = cFLESTOQUE,
                          dt_liberacao = TO_DATE(cDTLIBERACAO,'DD/MM/RR')
                   WHERE  (Cod_Loja, Cod_Dpk) In
                          (Select E.Cod_Loja, E.Cod_Dpk
                           From   Item_Estoque E, Item_Cadastro C
                           Where  E.Cod_Dpk = C.Cod_Dpk and
                                  E.Situacao = 0 and
                                  E.Cod_Loja Not IN (2, 6, 11, 12, 13, 16, 17, 18) and
                                  C.Cod_Fornecedor = cFORN and
                                  C.Cod_Grupo = cGRUPO);
               Else
                   UPDATE ressuprimento.item_qtde_excesso
                   SET    FL_COMPRA = cFLCOMPRA,
                          FL_TRANSF = cFLTRANSF,
                          fl_bloq_vd_casada = cFLCASADA,
                          fl_bloq_aum_estoq = cFLESTOQUE,
                          dt_liberacao = TO_DATE(cDTLIBERACAO,'DD/MM/RR')
                   WHERE  (Cod_Loja, Cod_Dpk) In
                          (Select E.Cod_Loja, E.Cod_Dpk
                           From   Item_Estoque E, Item_Cadastro C
                           Where  E.Cod_Dpk = C.Cod_Dpk and
                                  E.Situacao = 0 and
                                  E.Cod_Loja Not IN (2, 6, 11, 12, 13, 16, 17, 18) and
                                  C.Cod_Grupo = cGRUPO);
               End If;
            End If;
       End If;
       If cGrupo <> 0 And cSubgrupo <> 0 Then
            If cLOJA <> 99 Then
                If cFORN <> 99 Then
                    UPDATE ressuprimento.item_qtde_excesso
                    SET    FL_COMPRA = cFLCOMPRA,
                           FL_TRANSF = cFLTRANSF,
                           fl_bloq_vd_casada = cFLCASADA,
                           fl_bloq_aum_estoq = cFLESTOQUE,
                           dt_liberacao = TO_DATE(cDTLIBERACAO,'DD/MM/RR')
                    WHERE  (Cod_Loja, Cod_Dpk) In
                           (Select E.Cod_Loja, E.Cod_Dpk
                            From Item_Estoque E, Item_Cadastro C
                            Where E.Cod_Dpk = C.Cod_Dpk and
                                  E.Cod_Loja = cLOJA and
                                  E.Situacao = 0 and
                                  C.Cod_Fornecedor = cFORN and
                                  C.Cod_Grupo = cGRUPO and
                                  C.Cod_Subgrupo = cSUBGRUPO);
                Else
                    UPDATE ressuprimento.item_qtde_excesso
                    SET    FL_COMPRA = cFLCOMPRA,
                           FL_TRANSF = cFLTRANSF,
                           fl_bloq_vd_casada = cFLCASADA,
                           fl_bloq_aum_estoq = cFLESTOQUE,
                           dt_liberacao = TO_DATE(cDTLIBERACAO,'DD/MM/RR')
                    WHERE  (Cod_Loja, Cod_Dpk) In
                           (Select E.Cod_Loja, E.Cod_Dpk
                            From Item_Estoque E, Item_Cadastro C
                            Where E.Cod_Dpk = C.Cod_Dpk and
                                  E.Cod_Loja = cLOJA and
                                  E.Situacao = 0 and
                                  C.Cod_Grupo = cGRUPO and
                                  C.Cod_Subgrupo = cSUBGRUPO);
                End If;
            Else
                If cFORN <> 99 Then
                    UPDATE ressuprimento.item_qtde_excesso
                    SET    FL_COMPRA = cFLCOMPRA,
                           FL_TRANSF = cFLTRANSF,
                           fl_bloq_vd_casada = cFLCASADA,
                           fl_bloq_aum_estoq = cFLESTOQUE,
                           dt_liberacao = TO_DATE(cDTLIBERACAO,'DD/MM/RR')
                    WHERE  (Cod_Loja, Cod_Dpk) In
                           (Select E.Cod_Loja, E.Cod_Dpk
                            From   Item_Estoque E, Item_Cadastro C
                            Where  E.Cod_Dpk = C.Cod_Dpk and
                                   E.Situacao = 0 and
                                   E.Cod_Loja Not IN (2, 6, 11, 12, 13, 16, 17, 18) and
                                   C.Cod_Fornecedor = cFORN and
                                   C.Cod_Grupo = cGRUPO and
                                   C.Cod_Subgrupo = cSUBGRUPO);
                Else
                    UPDATE ressuprimento.item_qtde_excesso
                    SET    FL_COMPRA = cFLCOMPRA,
                           FL_TRANSF = cFLTRANSF,
                           fl_bloq_vd_casada = cFLCASADA,
                           fl_bloq_aum_estoq = cFLESTOQUE,
                           dt_liberacao = TO_DATE(cDTLIBERACAO,'DD/MM/RR')
                    WHERE  (Cod_Loja, Cod_Dpk) In
                           (Select E.Cod_Loja, E.Cod_Dpk
                            From   Item_Estoque E, Item_Cadastro C
                            Where  E.Cod_Dpk = C.Cod_Dpk and
                                   E.Situacao = 0 and
                                   E.Cod_Loja Not IN (2, 6, 11, 12, 13, 16, 17, 18) and
                                   C.Cod_Grupo = cGRUPO and
                                   C.Cod_Subgrupo = cSUBGRUPO);
                End If;
            End If;
       End If;

       Commit;

       Exception
          When Others then
          RollBack;
  End;

  -- 11) Verifica DPK
  Procedure PR_VER_DPK (cDados IN OUT TP_CURSOR,
                        cFORN IN Item_Cadastro.Cod_Fornecedor%type,
                        cFABR IN Item_Cadastro.Cod_Fabrica%type) is
  Begin
       OPEN cDados FOR
       Select C.Cod_Dpk
       From   Item_Estoque E, Item_Cadastro C
       Where  E.Cod_Dpk = C.Cod_Dpk and
              E.Situacao = 0 and
              E.Cod_LOja = 1 and
              C.Cod_Fornecedor = cFORN and
              C.Cod_Fabrica = cFABR;
  End;

  -- 12) Verifica DPK 2
  Procedure PR_VER_DPK_2 (cDados IN OUT TP_CURSOR,
                          cDPK IN Item_Cadastro.Cod_Dpk%type) is
  Begin
       OPEN cDados FOR
       Select C.Cod_Fornecedor Forn, F.NOME_FORNEC, C.Cod_Fabrica Fabr
       From   Item_Estoque E, Item_Cadastro C, Fornecedor F
       Where  E.Cod_Dpk = C.Cod_Dpk and
              C.Cod_Fornecedor = F.Cod_Fornecedor and
              E.Situacao = 0 and
              E.Cod_LOja = 1 and
              C.Cod_Dpk = cDPK;
  End;

  -- 13) Verifica DPK 3
  Procedure PR_VER_DPK_3 (cDados IN OUT TP_CURSOR,
                          cLOJA IN ressuprimento.item_qtde_excesso.cod_loja%type,
                          cDPK IN ressuprimento.item_qtde_excesso.cod_dpk%type) is
  Begin
       If cLOJA <> 99 Then
           OPEN cDados FOR
           SELECT FL_COMPRA, FL_TRANSF, QTD_EXCESSO, QTD_RESERVA,
                  QTD_NEC_TRANSF, QTD_NEC_COMPRA_IM, QTD_NEC_COMPRA_30,
                  QTD_NEC_COMPRA_60,
                  nvl(fl_bloq_vd_casada,' ') fl_bloq_vd_casada,
                  nvl(fl_bloq_aum_estoq,' ') fl_bloq_aum_estoq,
                  to_char(dt_liberacao,'DD/MM/YY') dt_liberacao
           FROM   ressuprimento.item_qtde_excesso
           WHERE  COD_DPK = cDPK and
                  COD_LOJA = cLOJA;
       Else
           OPEN cDados FOR
           SELECT FL_COMPRA, FL_TRANSF, QTD_EXCESSO, QTD_RESERVA,
                  QTD_NEC_TRANSF, QTD_NEC_COMPRA_IM, QTD_NEC_COMPRA_30,
                  QTD_NEC_COMPRA_60,
                  nvl(fl_bloq_vd_casada,' ') fl_bloq_vd_casada,
                  nvl(fl_bloq_aum_estoq,' ') fl_bloq_aum_estoq,
                  to_char(dt_liberacao,'DD/MM/YY') dt_liberacao
           FROM   ressuprimento.item_qtde_excesso
           WHERE  COD_DPK = cDPK;
       End If;
  End;

  -- 14) Importação
  Procedure PR_UPD_IMP_I (cLOJA IN ressuprimento.item_qtde_excesso.cod_loja%type,
                        cDPK IN ressuprimento.item_qtde_excesso.cod_dpk%type,
                        cFLCOMPRA IN ressuprimento.item_qtde_excesso.fl_compra%type,
                        cFLTRANSF IN ressuprimento.item_qtde_excesso.fl_transf%type,
                        cFLCASADA IN ressuprimento.item_qtde_excesso.fl_bloq_vd_casada%type,
                        cFLESTOQUE IN ressuprimento.item_qtde_excesso.fl_bloq_aum_estoq%type,
                        cDTLIBERACAO IN string) is
  Begin
       UPDATE ressuprimento.item_qtde_excesso
       SET    FL_COMPRA = cFLCOMPRA,
              FL_TRANSF = cFLTRANSF,
              fl_bloq_vd_casada = cFLCASADA,
              fl_bloq_aum_estoq = cFLESTOQUE,
              dt_liberacao = TO_DATE(cDTLIBERACAO,'DD/MM/RR')
       WHERE  COD_LOJA = cLOJA AND
              COD_DPK = cDPK;

       Commit;

       Exception
          When Others then
          RollBack;
  End;

  -- 15) Verifica Importação 1
  Procedure PR_VER_IMP_1 (cDados IN OUT TP_CURSOR,
                          cLOJA IN ressuprimento.parametro_transf.cod_loja%type,
                          cFORN IN ressuprimento.parametro_transf.cod_fornecedor%type) is
  Begin
       OPEN cDados FOR
       Select Fl_Calculo
       From   ressuprimento.parametro_transf
       Where  Cod_Fornecedor = cFORN and
              Cod_Loja = cLOJA;
  End;

  -- 16) Verifica Importação 2
  Procedure PR_VER_IMP_2 (cDados IN OUT TP_CURSOR,
                          cLOJA IN loja.cod_loja%type) is
  Begin
       OPEN cDados FOR
       Select Count(*) Qtd
       From LOJA
       Where Cod_Loja = cLOJA;
  End;

  -- 17) Verifica Importação 3
  Procedure PR_VER_IMP_3 (cDados IN OUT TP_CURSOR,
                          cFORN IN ressuprimento.parametro_transf.cod_fornecedor%type) is
  Begin
       OPEN cDados FOR
       Select Count(*) Qtd
       From ressuprimento.parametro_transf
       Where cod_fornecedor = cFORN;
  End;

  -- 18) Verifica Importação 4
  Procedure PR_VER_IMP_4 (cDados IN OUT TP_CURSOR,
                          cFORN IN FORNECEDOR.COD_FORNECEDOR%type) is
  Begin
       OPEN cDados FOR
       Select Count(*) Qtd
       From FORNECEDOR
       Where situacao = 0 and
             cod_fornecedor = cFORN;
  End;

  -- 19) Consulta Bloqueios
  Procedure PR_CON_BLOQ (cDados IN OUT TP_CURSOR,
                         cLOJA IN Item_estoque.Cod_Loja%type,
                         cDPK IN Item_Cadastro.Cod_Dpk%type,
                         cFORN IN Item_Cadastro.Cod_Fornecedor%type,
                         cGrupo In Item_Cadastro.Cod_Grupo%type,
                         cSubgrupo In Item_Cadastro.Cod_Subgrupo%type) is
  Begin
           If cLOJA = 99 Then
               If cDPK = 0 Then
                   If cGRUPO = 0 Then
                       OPEN cDados FOR
                       Select IQE.cod_loja, IQE.cod_dpk, IQE.fl_compra, IQE.fl_transf,
                              IQE.fl_bloq_vd_casada, IQE.fl_bloq_aum_estoq, IQE.dt_liberacao
                       From   ressuprimento.Item_Qtde_Excesso IQE,
                              Producao.Item_Estoque IE,
                              Producao.Item_Cadastro IC
                       Where  IQE.Cod_Loja = IE.Cod_Loja and
                              IQE.Cod_Dpk  = IE.Cod_Dpk and
                              IE.Cod_Dpk   = IC.Cod_Dpk and
                              IE.Situacao = 0 and
                              IC.Cod_Fornecedor+0 = cFORN
                       Order by IE.Cod_Loja, IC.Cod_Fornecedor, IC.Cod_Grupo,
                                IC.Cod_Subgrupo, IC.Cod_Dpk;
                   Else
                       If cSUBGRUPO = 0 Then
                           OPEN cDados FOR
                           Select IQE.cod_loja, IQE.cod_dpk, IQE.fl_compra, IQE.fl_transf,
                                  IQE.fl_bloq_vd_casada, IQE.fl_bloq_aum_estoq, IQE.dt_liberacao
                           From   ressuprimento.Item_Qtde_Excesso IQE,
                                  Producao.Item_Estoque IE,
                                  Producao.Item_Cadastro IC
                           Where  IQE.Cod_Loja = IE.Cod_Loja and
                                  IQE.Cod_Dpk  = IE.Cod_Dpk and
                                  IE.Cod_Dpk   = IC.Cod_Dpk and
                                  IE.Situacao = 0 and
                                  IC.Cod_Fornecedor+0 = cFORN and
                                  IC.Cod_Grupo = cGRUPO
                           Order by IE.Cod_Loja, IC.Cod_Fornecedor, IC.Cod_Grupo,
                                    IC.Cod_Subgrupo, IC.Cod_Dpk;
                       Else
                           OPEN cDados FOR
                           Select IQE.cod_loja, IQE.cod_dpk, IQE.fl_compra, IQE.fl_transf,
                                  IQE.fl_bloq_vd_casada, IQE.fl_bloq_aum_estoq, IQE.dt_liberacao
                           From   ressuprimento.Item_Qtde_Excesso IQE,
                                  Producao.Item_Estoque IE,
                                  Producao.Item_Cadastro IC
                           Where  IQE.Cod_Loja = IE.Cod_Loja and
                                  IQE.Cod_Dpk  = IE.Cod_Dpk and
                                  IE.Cod_Dpk   = IC.Cod_Dpk and
                                  IE.Situacao = 0 and
                                  IC.Cod_Fornecedor+0 = cFORN and
                                  IC.Cod_Grupo = cGRUPO and
                                  IC.Cod_SubGrupo = cSUBGRUPO
                           Order by IE.Cod_Loja, IC.Cod_Fornecedor, IC.Cod_Grupo,
                                    IC.Cod_Subgrupo, IC.Cod_Dpk;
                       End If;
                   End If;
               Else
                   OPEN cDados FOR
                   Select IQE.cod_loja, IQE.cod_dpk, IQE.fl_compra, IQE.fl_transf,
                          IQE.fl_bloq_vd_casada, IQE.fl_bloq_aum_estoq, IQE.dt_liberacao
                   From   ressuprimento.Item_Qtde_Excesso IQE,
                          Producao.Item_Estoque IE,
                          Producao.Item_Cadastro IC
                   Where  IQE.Cod_Loja = IE.Cod_Loja and
                          IQE.Cod_Dpk  = IE.Cod_Dpk and
                          IE.Cod_Dpk   = IC.Cod_Dpk and
                          IE.Situacao = 0 and
                          IC.Cod_Dpk = cDPK
                   Order by IE.Cod_Loja, IC.Cod_Fornecedor, IC.Cod_Grupo,
                            IC.Cod_Subgrupo, IC.Cod_Dpk;
               End If;
           Else
               If cDPK = 0 Then
                   If cGRUPO = 0 Then
                       OPEN cDados FOR
                       Select IQE.cod_loja, IQE.cod_dpk, IQE.fl_compra, IQE.fl_transf,
                              IQE.fl_bloq_vd_casada, IQE.fl_bloq_aum_estoq, IQE.dt_liberacao
                       From   ressuprimento.Item_Qtde_Excesso IQE,
                              Producao.Item_Estoque IE,
                              Producao.Item_Cadastro IC
                       Where  IQE.Cod_Loja = IE.Cod_Loja and
                              IQE.Cod_Dpk  = IE.Cod_Dpk and
                              IE.Cod_Dpk   = IC.Cod_Dpk and
                              IE.Situacao = 0 and
                              IE.Cod_Loja = cLOJA and
                              IC.Cod_Fornecedor+0 = cFORN
                       Order by IE.Cod_Loja, IC.Cod_Fornecedor, IC.Cod_Grupo,
                                IC.Cod_Subgrupo, IC.Cod_Dpk;
                   Else
                       If cSUBGRUPO = 0 Then
                           OPEN cDados FOR
                           Select IQE.cod_loja, IQE.cod_dpk, IQE.fl_compra, IQE.fl_transf,
                                  IQE.fl_bloq_vd_casada, IQE.fl_bloq_aum_estoq, IQE.dt_liberacao
                           From   ressuprimento.Item_Qtde_Excesso IQE,
                                  Producao.Item_Estoque IE,
                                  Producao.Item_Cadastro IC
                           Where  IQE.Cod_Loja = IE.Cod_Loja and
                                  IQE.Cod_Dpk  = IE.Cod_Dpk and
                                  IE.Cod_Dpk   = IC.Cod_Dpk and
                                  IE.Situacao = 0 and
                                  IE.Cod_Loja = cLOJA and
                                  IC.Cod_Fornecedor+0 = cFORN and
                                  IC.Cod_Grupo = cGRUPO
                           Order by IE.Cod_Loja, IC.Cod_Fornecedor, IC.Cod_Grupo,
                                    IC.Cod_Subgrupo, IC.Cod_Dpk;
                       Else
                           OPEN cDados FOR
                           Select IQE.cod_loja, IQE.cod_dpk, IQE.fl_compra, IQE.fl_transf,
                                  IQE.fl_bloq_vd_casada, IQE.fl_bloq_aum_estoq, IQE.dt_liberacao
                           From   ressuprimento.Item_Qtde_Excesso IQE,
                                  Producao.Item_Estoque IE,
                                  Producao.Item_Cadastro IC
                           Where  IQE.Cod_Loja = IE.Cod_Loja and
                                  IQE.Cod_Dpk  = IE.Cod_Dpk and
                                  IE.Cod_Dpk   = IC.Cod_Dpk and
                                  IE.Situacao = 0 and
                                  IE.Cod_Loja = cLOJA and
                                  IC.Cod_Fornecedor+0 = cFORN and
                                  IC.Cod_Grupo = cGRUPO and
                                  IC.Cod_SubGrupo = cSUBGRUPO
                           Order by IE.Cod_Loja, IC.Cod_Fornecedor, IC.Cod_Grupo,
                                    IC.Cod_Subgrupo, IC.Cod_Dpk;
                       End If;
                   End If;
               Else
                   OPEN cDados FOR
                   Select IQE.cod_loja, IQE.cod_dpk, IQE.fl_compra, IQE.fl_transf,
                          IQE.fl_bloq_vd_casada, IQE.fl_bloq_aum_estoq, IQE.dt_liberacao
                   From   ressuprimento.Item_Qtde_Excesso IQE,
                          Producao.Item_Estoque IE,
                          Producao.Item_Cadastro IC
                   Where  IQE.Cod_Loja = IE.Cod_Loja and
                          IQE.Cod_Dpk  = IE.Cod_Dpk and
                          IE.Cod_Dpk   = IC.Cod_Dpk and
                          IE.Situacao = 0 and
                          IE.Cod_Loja = cLOJA and
                          IC.Cod_Dpk = cDPK
                   Order by IE.Cod_Loja, IC.Cod_Fornecedor, IC.Cod_Grupo,
                            IC.Cod_Subgrupo, IC.Cod_Dpk;
               End If;
           End If;
  End;

end PCK_CAD920;
/
