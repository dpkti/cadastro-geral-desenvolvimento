Attribute VB_Name = "ModGeral"
Option Explicit

Declare Function GetUserName Lib "advapi32.dll" Alias "GetUserNameA" (ByVal lpBuffer As String, nSize As Long) As Long

Public V_FL_COMPRA
Public V_FL_TRANSF
Public V_Fl_Compra2
Public V_Fl_Transf2
Public V_FL_CALCULO
Public V_FL_COMPRA_ANTERIOR
Public V_FL_TRANSF_ANTERIOR
Public V_FL_CALCULO_ANTERIOR

Public vArquivo As String

Public V_USUARIO As String
Public V_ACCESS As Object
Public V_SQL As String

Function NOME_USUARIO() As String

Dim V_USER_ID As String
Dim V_BUFFER As Long
Dim V_RET As Long
Dim i

    V_USER_ID = String(25, 0)
    V_BUFFER = 25
    V_USUARIO = ""
    V_RET = GetUserName(V_USER_ID, V_BUFFER)
    For i = 1 To Len(V_USER_ID)
        If Asc(Mid(V_USER_ID, i, 1)) <> 0 Then
            V_USUARIO = V_USUARIO & Mid(V_USER_ID, i, 1)
        End If
    Next
    V_USUARIO = UCase(V_USUARIO)
    If V_RET Then
        NOME_USUARIO$ = V_USUARIO
    End If

End Function
