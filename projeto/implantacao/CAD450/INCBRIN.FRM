VERSION 4.00
Begin VB.Form frmInclusao 
   Caption         =   "Inclus�o de Brindes"
   ClientHeight    =   4710
   ClientLeft      =   300
   ClientTop       =   1725
   ClientWidth     =   9000
   Height          =   5115
   Left            =   240
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   540
   ScaleWidth      =   540
   Top             =   1380
   Width           =   9120
   Begin Threed.SSFrame SSFrame5 
      Height          =   4560
      Left            =   75
      TabIndex        =   14
      Top             =   45
      Width           =   8835
      _version        =   65536
      _extentx        =   15584
      _extenty        =   8043
      _stockprops     =   14
      shadowstyle     =   1
      Begin Threed.SSFrame SSFrame1 
         Height          =   1170
         Left            =   2280
         TabIndex        =   15
         Top             =   120
         Width           =   5100
         _version        =   65536
         _extentx        =   8996
         _extenty        =   2064
         _stockprops     =   14
         Begin VB.TextBox txtSubcampanha 
            Alignment       =   2  'Center
            Enabled         =   0   'False
            BeginProperty Font 
               name            =   "MS Sans Serif"
               charset         =   1
               weight          =   700
               size            =   8.25
               underline       =   0   'False
               italic          =   0   'False
               strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   330
            Left            =   3840
            MaxLength       =   9
            TabIndex        =   2
            Text            =   "999999999"
            Top             =   510
            Width           =   1095
         End
         Begin VB.TextBox txtCampanha 
            Alignment       =   2  'Center
            Enabled         =   0   'False
            BeginProperty Font 
               name            =   "MS Sans Serif"
               charset         =   1
               weight          =   700
               size            =   8.25
               underline       =   0   'False
               italic          =   0   'False
               strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   330
            Left            =   1170
            MaxLength       =   9
            TabIndex        =   1
            Text            =   "999999999"
            Top             =   510
            Width           =   1095
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "Subcampanha"
            Enabled         =   0   'False
            BeginProperty Font 
               name            =   "MS Sans Serif"
               charset         =   1
               weight          =   700
               size            =   8.25
               underline       =   0   'False
               italic          =   0   'False
               strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   2550
            TabIndex        =   17
            Top             =   510
            Width           =   1215
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            Caption         =   "Campanha"
            Enabled         =   0   'False
            BeginProperty Font 
               name            =   "MS Sans Serif"
               charset         =   1
               weight          =   700
               size            =   8.25
               underline       =   0   'False
               italic          =   0   'False
               strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   180
            TabIndex        =   16
            Top             =   510
            Width           =   900
         End
      End
      Begin Threed.SSFrame SSFrame2 
         Height          =   1170
         Left            =   225
         TabIndex        =   18
         Top             =   120
         Width           =   1980
         _version        =   65536
         _extentx        =   3493
         _extenty        =   2064
         _stockprops     =   14
         caption         =   "C�digo do Brinde"
         forecolor       =   8388608
         BeginProperty font {FB8F0823-0164-101B-84ED-08002B2EC713} 
            name            =   "MS Sans Serif"
            charset         =   1
            weight          =   700
            size            =   8.25
            underline       =   0   'False
            italic          =   0   'False
            strikethrough   =   0   'False
         EndProperty
         alignment       =   2
         Begin VB.Label lblCodBrinde 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "999999999"
            BeginProperty Font 
               name            =   "MS Sans Serif"
               charset         =   1
               weight          =   700
               size            =   8.25
               underline       =   0   'False
               italic          =   0   'False
               strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   330
            Left            =   380
            TabIndex        =   0
            Top             =   510
            Width           =   1200
         End
      End
      Begin Threed.SSFrame SSFrame3 
         Height          =   3105
         Left            =   225
         TabIndex        =   19
         Top             =   1335
         Width           =   7155
         _version        =   65536
         _extentx        =   12621
         _extenty        =   5477
         _stockprops     =   14
         Begin VB.TextBox txtCodCliente 
            Alignment       =   2  'Center
            BeginProperty Font 
               name            =   "MS Sans Serif"
               charset         =   1
               weight          =   700
               size            =   8.25
               underline       =   0   'False
               italic          =   0   'False
               strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   330
            Left            =   930
            MaxLength       =   6
            TabIndex        =   5
            Text            =   "999999"
            Top             =   1000
            Width           =   780
         End
         Begin VB.TextBox txtDtEntrada 
            Alignment       =   2  'Center
            BeginProperty Font 
               name            =   "MS Sans Serif"
               charset         =   1
               weight          =   700
               size            =   8.25
               underline       =   0   'False
               italic          =   0   'False
               strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   330
            Left            =   5715
            MaxLength       =   10
            TabIndex        =   4
            Text            =   "99/99/9999"
            Top             =   300
            Width           =   1170
         End
         Begin VB.TextBox txtCodDPK 
            Alignment       =   2  'Center
            BeginProperty Font 
               name            =   "MS Sans Serif"
               charset         =   1
               weight          =   700
               size            =   8.25
               underline       =   0   'False
               italic          =   0   'False
               strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   330
            Left            =   1155
            MaxLength       =   5
            TabIndex        =   7
            Text            =   "99999"
            Top             =   1500
            Width           =   675
         End
         Begin VB.ComboBox cboDeposito 
            BeginProperty Font 
               name            =   "MS Sans Serif"
               charset         =   1
               weight          =   700
               size            =   8.25
               underline       =   0   'False
               italic          =   0   'False
               strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   300
            ItemData        =   "INCBRIN.frx":0000
            Left            =   1095
            List            =   "INCBRIN.frx":0002
            TabIndex        =   3
            Text            =   "99 - XXXXXXXXXX"
            Top             =   300
            Width           =   2160
         End
         Begin VB.TextBox txtQtdBrinde 
            Alignment       =   2  'Center
            BeginProperty Font 
               name            =   "MS Sans Serif"
               charset         =   1
               weight          =   700
               size            =   8.25
               underline       =   0   'False
               italic          =   0   'False
               strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   330
            Left            =   1665
            MaxLength       =   6
            TabIndex        =   9
            Text            =   "999999"
            Top             =   2215
            Width           =   780
         End
         Begin VB.ComboBox cboTpBrinde 
            BeginProperty Font 
               name            =   "MS Sans Serif"
               charset         =   1
               weight          =   700
               size            =   8.25
               underline       =   0   'False
               italic          =   0   'False
               strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   300
            ItemData        =   "INCBRIN.frx":0004
            Left            =   4920
            List            =   "INCBRIN.frx":000E
            TabIndex        =   10
            Text            =   "X - XXXXXXXXX"
            Top             =   2215
            Width           =   1935
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Cliente"
            BeginProperty Font 
               name            =   "MS Sans Serif"
               charset         =   1
               weight          =   700
               size            =   8.25
               underline       =   0   'False
               italic          =   0   'False
               strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   200
            TabIndex        =   25
            Top             =   1000
            Width           =   600
         End
         Begin VB.Label lblNomeCliente 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
            BeginProperty Font 
               name            =   "MS Sans Serif"
               charset         =   1
               weight          =   700
               size            =   8.25
               underline       =   0   'False
               italic          =   0   'False
               strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   330
            Left            =   1755
            TabIndex        =   6
            Top             =   1000
            Width           =   3825
         End
         Begin VB.Label Label4 
            AutoSize        =   -1  'True
            Caption         =   "Data Entrada"
            BeginProperty Font 
               name            =   "MS Sans Serif"
               charset         =   1
               weight          =   700
               size            =   8.25
               underline       =   0   'False
               italic          =   0   'False
               strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   4470
            TabIndex        =   24
            Top             =   300
            Width           =   1140
         End
         Begin VB.Label Label6 
            AutoSize        =   -1  'True
            Caption         =   "Dep�sito"
            BeginProperty Font 
               name            =   "MS Sans Serif"
               charset         =   1
               weight          =   700
               size            =   8.25
               underline       =   0   'False
               italic          =   0   'False
               strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   200
            TabIndex        =   23
            Top             =   300
            Width           =   765
         End
         Begin VB.Label Label7 
            AutoSize        =   -1  'True
            Caption         =   "C�d. DPK"
            BeginProperty Font 
               name            =   "MS Sans Serif"
               charset         =   1
               weight          =   700
               size            =   8.25
               underline       =   0   'False
               italic          =   0   'False
               strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   200
            TabIndex        =   22
            Top             =   1515
            Width           =   840
         End
         Begin VB.Label Label8 
            AutoSize        =   -1  'True
            Caption         =   "Tipo de Brinde"
            BeginProperty Font 
               name            =   "MS Sans Serif"
               charset         =   1
               weight          =   700
               size            =   8.25
               underline       =   0   'False
               italic          =   0   'False
               strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   3570
            TabIndex        =   21
            Top             =   2215
            Width           =   1260
         End
         Begin VB.Label Label9 
            AutoSize        =   -1  'True
            Caption         =   "Qtd. de Brindes"
            BeginProperty Font 
               name            =   "MS Sans Serif"
               charset         =   1
               weight          =   700
               size            =   8.25
               underline       =   0   'False
               italic          =   0   'False
               strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   210
            TabIndex        =   20
            Top             =   2215
            Width           =   1335
         End
         Begin VB.Label lblDescDPK 
            Alignment       =   2  'Center
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
            BeginProperty Font 
               name            =   "MS Sans Serif"
               charset         =   1
               weight          =   700
               size            =   8.25
               underline       =   0   'False
               italic          =   0   'False
               strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   330
            Left            =   1890
            TabIndex        =   8
            Top             =   1500
            Width           =   4980
         End
      End
      Begin Threed.SSFrame SSFrame4 
         Height          =   2760
         Left            =   7470
         TabIndex        =   26
         Top             =   1665
         Width           =   1125
         _version        =   65536
         _extentx        =   1984
         _extenty        =   4868
         _stockprops     =   14
         Begin Threed.SSCommand cmdOK 
            Height          =   510
            Left            =   290
            TabIndex        =   11
            Top             =   180
            Width           =   510
            _version        =   65536
            _extentx        =   900
            _extenty        =   900
            _stockprops     =   78
            forecolor       =   -2147483640
            picture         =   "INCBRIN.frx":002D
         End
         Begin Threed.SSCommand cmdCancelar 
            Height          =   510
            Left            =   290
            TabIndex        =   12
            Top             =   1035
            Width           =   510
            _version        =   65536
            _extentx        =   900
            _extenty        =   900
            _stockprops     =   78
            forecolor       =   -2147483640
            picture         =   "INCBRIN.frx":0347
         End
         Begin Threed.SSCommand cmdVoltar 
            Height          =   510
            Left            =   290
            TabIndex        =   13
            Top             =   1890
            Width           =   510
            _version        =   65536
            _extentx        =   900
            _extenty        =   900
            _stockprops     =   78
            forecolor       =   -2147483640
            picture         =   "INCBRIN.frx":0661
         End
         Begin VB.Label lblHelp 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H0000FFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Cancelar"
            ForeColor       =   &H80000008&
            Height          =   225
            Left            =   220
            TabIndex        =   27
            Top             =   1575
            Visible         =   0   'False
            Width           =   660
         End
      End
   End
End
Attribute VB_Name = "frmInclusao"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Option Explicit

Sub Limpa_Campos()
    lblCodBrinde = ""
    txtCampanha = ""
    txtSubcampanha = ""
    txtDtEntrada = ""
    txtCodCliente = ""
    lblNomeCliente = ""
    cboDeposito = ""
    txtCodDPK = ""
    lblDescDPK = ""
    txtQtdBrinde = ""
    cboTpBrinde = ""
End Sub

Private Sub cmdCancelar_Click()
    Limpa_Campos
    txtDtEntrada = DataReal
End Sub

Private Sub cmdCancelar_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp.Visible = True
    lblHelp.Left = 220
    lblHelp.Top = 1575
    lblHelp = "Cancelar"
    cmdCancelar.SetFocus
End Sub


Private Sub cmdOK_Click()

    'Declara��o de vari�veis locais � subrotina
    Dim R As Integer                        'Recebe a confirma��o de inclus�o

    'Verifica se todos os campos est�o devidamente preenchidos
    If cboDeposito = "" Then
        MsgBox "Escolha um dep�sito para a inclus�o do brinde", , MSG_TIT
        Exit Sub
    ElseIf txtDtEntrada = "" Then
        MsgBox "Digite a data de entrada do brinde", , MSG_TIT
        Exit Sub
    ElseIf txtCodCliente = "" Then
        MsgBox "Digite o c�digo do cliente", , MSG_TIT
        Exit Sub
    ElseIf txtCodDPK = "" Then
        MsgBox "Digite o c�digo DPK", , MSG_TIT
        Exit Sub
    ElseIf Val(txtQtdBrinde) = 0 Then
        MsgBox "Digite a quantidade de brindes", , MSG_TIT
        Exit Sub
    ElseIf cboTpBrinde = "" Then
        MsgBox "Escolha o tipo do brinde", , MSG_TIT
        Exit Sub
    End If
    
    
    'Verifica a sequ�ncia do cadastro de brindes e transfere dado para o campo do c�digo do brinde
    CmdSql = "Select Autolog.Seq_Brinde.NextVal From Dual"
    
    Set db_CONS_NextVal = db.DbCreateDynaset(CmdSql, 8&)
    
    lblCodBrinde = Format(db_CONS_NextVal!NextVal, "000000000")
    
    
    'Inicializa vari�veis de bind para a inclus�o
    db.Parameters.Add "CODBRI", lblCodBrinde, 1
    db.Parameters.Add "CODCLI", txtCodCliente, 1
    db.Parameters.Add "DTENTRADA", txtDtEntrada, 1
    db.Parameters.Add "CODDPK", txtCodDPK, 1
    db.Parameters.Add "QTDBRI", txtQtdBrinde, 1
    db.Parameters.Add "LOJA", Mid(cboDeposito, 1, 2), 1
    db.Parameters.Add "TPBRI", Mid(cboTpBrinde, 1, 1), 1
    db.Parameters.Add "Cod_ErrorPl", 0, 2
    db.Parameters.Add "Txt_ErrorPl", "", 2
    
    
    'Inicializa comandos para a inclus�o
    CmdPl = "Begin "
    
    CmdPl = CmdPl & "Insert into Autolog.Brinde " & _
                                    "Values(:CODBRI,Null,Null,:CODCLI," & _
                                    "To_Date(:DTENTRADA,'dd/mm/yyyy')," & _
                                    "Null,:CODDPK,:QTDBRI,:LOJA,Null,Null,:TPBRI); "

    CmdPl = CmdPl & "Commit; " & _
                                    "Exception " & _
                                    "When Others then " & _
                                    "RollBack; " & _
                                    ":Cod_ErrorPl := SQLCODE; " & _
                                    ":Txt_ErrorPl := SQLERRM; " & _
                                    "End;"

    R = MsgBox("Confirma Inclus�o?", vbYesNo, MSG_TIT)
    
    
    'Confirmada a inclus�o, executa o comando Pl-Sql
    If R = vbNo Then
        Limpa_Campos
        GoTo Fim
    Else
        db.ExecuteSQL CmdPl
    End If
    
    
    'No caso de erro, exibe mensagem
    If db.Parameters("Cod_ErrorPl") <> 0 Then
        MsgBox "Erro - " & db.Parameters("Txt_ErrorPl") & ". Favor avisar Depto. de Sistemas!", 48, MSG_TIT
    Else
        MsgBox "Inclus�o OK!" & Chr(13) & "Brinde no. " & Str(Val(lblCodBrinde)), , MSG_TIT
    End If
    
    
    'Limpa os campos e transporta data real para o campo de data de entrada
    Limpa_Campos
    txtDtEntrada = DataReal


'Rotina que remove todas as vari�veis de bind ap�s a inclus�o
Fim:
    db.Parameters.Remove "CODBRI"
    db.Parameters.Remove "CODCLI"
    db.Parameters.Remove "DTENTRADA"
    db.Parameters.Remove "CODDPK"
    db.Parameters.Remove "QTDBRI"
    db.Parameters.Remove "LOJA"
    db.Parameters.Remove "TPBRI"
    db.Parameters.Remove "Cod_ErrorPl"
    db.Parameters.Remove "Txt_ErrorPl"
    Exit Sub
    
    
Trata_Erro:
    Call Process_Line_Errors

End Sub

Private Sub cmdOK_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp.Visible = True
    lblHelp.Left = 210
    lblHelp.Top = 720
    lblHelp = "Confirmar"
    cmdOK.SetFocus
End Sub


Private Sub cmdVoltar_Click()
    Unload Me
End Sub

Private Sub cmdVoltar_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp.Visible = True
    lblHelp.Left = 80
    lblHelp.Top = 2430
    lblHelp = "Sair p/ Menu"
    cmdVoltar.SetFocus
End Sub


Private Sub Form_Load()

    'Declara vari�veis locais � subrotina
    Dim I As Integer
        
    On Error GoTo Trata_Erro
    
    'Alinha formul�rio
    frmInclusao.Height = 5115
    frmInclusao.Left = 240
    frmInclusao.Top = 1380
    frmInclusao.Width = 9120
    
    'Limpa todos os campos
    Limpa_Campos
    
    ' Ao entrar no form de Inclus�o, faz select para trazer as lojas - dep�sito - e colocar na ComboBox
    CmdSql = "Select Cod_Loja, Nome_Fantasia From Loja Order By Cod_Loja"
        
    Set db_CONS = db.CreateDynaset(CmdSql, 0&)
        
    If db_CONS.RecordCount = 0 Then
        MsgBox "Problema na consulta dos Dep�sitos. " & Chr(13) & "Chame o analista respons�vel.", , MSG_TIT
        Exit Sub
    End If
        
    'Transporta para a ComboBox, todas as lojas encontradas (C�digo e Nome Fantasia)
    For I = 1 To db_CONS.RecordCount
        cboDeposito.AddItem Format(db_CONS!Cod_Loja, "00") & " - " & db_CONS!Nome_Fantasia
        db_CONS.MoveNext
    Next I
        
    'Campo de data de entrada - cadastro do brinde - recebe a data real, mas permite altera��o
    txtDtEntrada = DataReal

Exit Sub

Trata_Erro:
    Call Process_Line_Errors

End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp.Visible = False
End Sub


Private Sub SSFrame4_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp.Visible = False
End Sub


Private Sub SSFrame5_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    lblHelp.Visible = False

End Sub


Private Sub txtCampanha_KeyPress(KeyAscii As Integer)
    KeyAscii = Numerico(KeyAscii)
End Sub


Private Sub txtCodCliente_DblClick()

    'Declara��o de vari�veis locais � subrotina
    Dim vRec As String
    
    On Error GoTo Trata_Erro
    
    'Limpa dados referentes ao cliente
    txtCodCliente = ""
    lblNomeCliente = ""
    
    vRec = Format(InputBox("Digite parte do nome do cliente.", MSG_TIT), ">")
    
    If vRec = "" Then
        Exit Sub
    End If
    
    'Nomeia origem do grid, para depois transferir as informa��es para os campos corretos
    ORIG_GRID = "Consulta_Cliente"
    
    'Traz todos os cliente ativos para serem apresentados no grid
    CmdSql = "Select Cod_cliente, Nome_Cliente From Cliente "
    CmdSql = CmdSql & "Where Nome_Cliente like '" & vRec & "'"
    CmdSql = CmdSql & " And Situacao = 0 Order by Cod_Cliente"
    
    ' Chama function que constr�i o grid
    Call CONS_GRID(500, 1200, 7000, 2500, 2, "700,6000", _
                   "C�d.,Cliente", CmdSql, frmGrid.grdPadrao)
                         
    ' Mostra o grid que o select trouxe
    If db_CONS.RecordCount <> 0 Then
        frmGrid.Caption = "Consulta Clientes"
        frmGrid.Show vbModal
        Call txtCodCliente_LostFocus
    End If
    
    Exit Sub
    
Trata_Erro:
        Call Process_Line_Errors

End Sub


Private Sub txtCodCliente_KeyPress(KeyAscii As Integer)
    KeyAscii = Numerico(KeyAscii)
End Sub


Private Sub txtCodCliente_LostFocus()

    On Error GoTo Trata_Erro
    
    'Se campo de c�digo de cliente, estiver em branco, sai desta rotina
    If txtCodCliente = "" Then
        Exit Sub
    End If

    'Inicializa vari�veis de bind
    db.Parameters.Add "CODCLI", txtCodCliente, 1
    
    'Procura os dados (c�digo e nome) do cliente digitado
    CmdSql = "Select Nome_Cliente From Cliente Where Cod_Cliente = :CODCLI And Situacao = 0"
    
    Set db_CONS = db.DbCreateDynaset(CmdSql, 0&)
    
    'Remove vari�vel de bind
    db.Parameters.Remove "CODCLI"
    
    'Se select n�o trouxer nada, � porque n�o existe cliente ou est� desativado. Sen�o, transfere dados para os campos devidos.
    If db_CONS.RecordCount = 0 Then
        MsgBox "C�digo inexistente ou cliente desativado.", , MSG_TIT
        txtCodCliente = ""
        lblNomeCliente = ""
        txtCodCliente.SetFocus
    Else
        lblNomeCliente = db_CONS!Nome_Cliente
    End If
    
    Exit Sub
    
Trata_Erro:
        Call Process_Line_Errors

End Sub


Private Sub txtCodDPK_DblClick()
    
    'Declara��o de vari�veis locais � subrotina
    Dim vRec As String
    
    On Error GoTo Trata_Erro
    
    'Limpa dados referentes ao DPK
    txtCodDPK = ""
    lblDescDPK = ""
    
    vRec = Format(InputBox("Digite parte da descri��o do DPK.", MSG_TIT), ">")
    
    If vRec = "" Then
        Exit Sub
    End If
    
    'Nomeia origem do grid, para depois transferir as informa��es para os campos corretos
    ORIG_GRID = "Consulta_DPK"
    
    'Traz todos os DPKs ativos para serem apresentados no grid
    CmdSql = "Select C.Cod_Dpk, C.Desc_Item " & _
                      "From Item_Estoque E, Item_Cadastro C, Autolog.R_Item_Endereco R " & _
                      "Where E.Cod_Dpk = C.Cod_Dpk And C.Cod_Dpk = R.Cod_Dpk " & _
                      "And E.Situacao = 0 And C.Desc_Item like '" & vRec & "'" & _
                      "Order by C.Cod_Dpk"
    
    ' Chama function que constr�i o grid
    Call CONS_GRID(500, 700, 8000, 2500, 2, "700,7000", "DPK,Descri��o", CmdSql, frmGrid.grdPadrao)
    
    ' Mostra o grid que o select trouxe
    If db_CONS.RecordCount <> 0 Then
        frmGrid.Caption = "Consulta DPK"
        frmGrid.Show vbModal
        Call txtCodDPK_LostFocus
    End If
    
    Exit Sub
    
Trata_Erro:
        Call Process_Line_Errors

End Sub


Private Sub txtCodDPK_KeyPress(KeyAscii As Integer)
    KeyAscii = Numerico(KeyAscii)
End Sub

Private Sub txtCodDPK_LostFocus()
    
    On Error GoTo Trata_Erro
    
    'Se campo de c�digo de DPK, estiver em branco, sai desta rotina
    If txtCodDPK = "" Then
        Exit Sub
    End If
    
    'Inicializa vari�veis de bind
    db.Parameters.Add "CODDPK", txtCodDPK, 1
    
    'Procura os dados (c�digo e nome) do cliente digitado
    CmdSql = "Select C.Desc_Item From Item_Estoque E, Item_Cadastro C, Autolog.R_Item_Endereco R " & _
                      "Where E.Cod_Dpk = C.Cod_Dpk And E.Cod_Dpk = :CODDPK " & _
                      "And E.Cod_Dpk = R.Cod_Dpk And E.Situacao = 0"
    
    Set db_CONS = db.DbCreateDynaset(CmdSql, 0&)
    
    'Remove vari�vel de bind
    db.Parameters.Remove "CODDPK"
    
    'Se select n�o trouxer nada, � porque n�o existe cliente ou est� desativado. Sen�o, transfere dados para os campos devidos.
    If db_CONS.RecordCount = 0 Then
        MsgBox "C�digo inexistente ou DPK desativado.", , MSG_TIT
        txtCodDPK = ""
        lblDescDPK = ""
        txtCodDPK.SetFocus
    Else
        lblDescDPK = db_CONS!Desc_Item
    End If
    
    Exit Sub
    
Trata_Erro:
        Call Process_Line_Errors

End Sub


Private Sub txtDtEntrada_KeyPress(KeyAscii As Integer)
    DATA_YYYY KeyAscii, txtDtEntrada
End Sub


Private Sub txtQtdBrinde_KeyPress(KeyAscii As Integer)
    KeyAscii = Numerico(KeyAscii)
End Sub


Private Sub txtSubcampanha_KeyPress(KeyAscii As Integer)
    KeyAscii = Numerico(KeyAscii)
End Sub


