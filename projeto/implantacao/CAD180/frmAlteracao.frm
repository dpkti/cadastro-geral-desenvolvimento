VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form frmAlteracao 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Altera��o de Pre�os"
   ClientHeight    =   6795
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10080
   Icon            =   "frmAlteracao.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   453
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   672
   Begin VB.TextBox txtForn 
      BackColor       =   &H8000000F&
      Height          =   285
      Left            =   2070
      Locked          =   -1  'True
      TabIndex        =   23
      Top             =   945
      Width           =   2940
   End
   Begin VB.TextBox txtGrupo 
      BackColor       =   &H8000000F&
      Height          =   285
      Left            =   3825
      Locked          =   -1  'True
      TabIndex        =   22
      Top             =   1935
      Visible         =   0   'False
      Width           =   2040
   End
   Begin VB.TextBox Text7 
      BackColor       =   &H8000000F&
      Height          =   285
      Left            =   6075
      Locked          =   -1  'True
      TabIndex        =   21
      Top             =   1605
      Visible         =   0   'False
      Width           =   1005
   End
   Begin VB.TextBox txtCdDpk 
      Height          =   285
      Left            =   3960
      TabIndex        =   20
      Top             =   1275
      Width           =   1050
   End
   Begin VB.TextBox txtSubGr 
      BackColor       =   &H8000000F&
      Height          =   285
      Left            =   7065
      Locked          =   -1  'True
      TabIndex        =   19
      Top             =   1935
      Visible         =   0   'False
      Width           =   2040
   End
   Begin VB.TextBox txtLinha 
      BackColor       =   &H8000000F&
      Height          =   285
      Left            =   990
      Locked          =   -1  'True
      TabIndex        =   18
      Top             =   1935
      Visible         =   0   'False
      Width           =   2040
   End
   Begin VB.TextBox txtDesc 
      BackColor       =   &H8000000F&
      Height          =   285
      Left            =   990
      Locked          =   -1  'True
      TabIndex        =   17
      Top             =   1605
      Width           =   4020
   End
   Begin VB.TextBox txtFabrica 
      Height          =   285
      Left            =   990
      TabIndex        =   16
      Top             =   1275
      Width           =   2040
   End
   Begin VB.TextBox txtCdForn 
      Height          =   285
      Left            =   990
      TabIndex        =   11
      Top             =   945
      Width           =   1005
   End
   Begin VB.Frame fraTabelas 
      Caption         =   "Tabelas"
      Height          =   4020
      Left            =   45
      TabIndex        =   10
      Top             =   2385
      Visible         =   0   'False
      Width           =   9960
      Begin VB.CheckBox chkCD99 
         Caption         =   "Assinalar CD 99 - Geral"
         Height          =   285
         Left            =   180
         TabIndex        =   38
         Top             =   3555
         Width           =   1995
      End
      Begin VB.CheckBox chkOnline 
         Caption         =   "Atualizar Online"
         Height          =   285
         Left            =   180
         TabIndex        =   37
         Top             =   3240
         Width           =   1995
      End
      Begin VB.TextBox txtPrNormal1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00008000&
         Height          =   285
         Left            =   3510
         TabIndex        =   36
         Text            =   "0.00"
         Top             =   3195
         Visible         =   0   'False
         Width           =   1380
      End
      Begin VB.TextBox txtPrOferta1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000C0&
         Height          =   285
         Left            =   5050
         TabIndex        =   35
         Text            =   "0.00"
         Top             =   3195
         Visible         =   0   'False
         Width           =   1380
      End
      Begin VB.TextBox txtPrSP1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   285
         Left            =   6590
         TabIndex        =   34
         Text            =   "0.00"
         Top             =   3195
         Visible         =   0   'False
         Width           =   1380
      End
      Begin VB.TextBox txtPrecoVDR 
         BackColor       =   &H0000FF00&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   8130
         TabIndex        =   33
         Text            =   "0.00"
         Top             =   3195
         Visible         =   0   'False
         Width           =   1380
      End
      Begin MSGrid.Grid grdLojas 
         Height          =   2400
         Left            =   135
         TabIndex        =   24
         Top             =   675
         Width           =   9660
         _Version        =   65536
         _ExtentX        =   17039
         _ExtentY        =   4233
         _StockProps     =   77
         ForeColor       =   0
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Cols            =   7
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "NORMAL"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000C000&
         Height          =   240
         Left            =   3510
         TabIndex        =   32
         Top             =   180
         Width           =   840
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         Caption         =   "OFERT�O"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   240
         Left            =   5025
         TabIndex        =   31
         Top             =   180
         Width           =   930
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "SUP.PROM."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   240
         Left            =   6525
         TabIndex        =   30
         Top             =   180
         Width           =   1110
      End
      Begin VB.Label lblVDR 
         AutoSize        =   -1  'True
         Caption         =   "VDR"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000FF00&
         Height          =   240
         Left            =   8085
         TabIndex        =   29
         Top             =   180
         Width           =   405
      End
      Begin VB.Label lblNormal1 
         Caption         =   "Label3"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00008000&
         Height          =   255
         Left            =   3510
         TabIndex        =   28
         Top             =   405
         Width           =   855
      End
      Begin VB.Label lblOferta1 
         Caption         =   "Label3"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000C0&
         Height          =   300
         Left            =   5040
         TabIndex        =   27
         Top             =   405
         Width           =   855
      End
      Begin VB.Label lblSP1 
         Caption         =   "Label3"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   6540
         TabIndex        =   26
         Top             =   405
         Width           =   855
      End
      Begin VB.Label lblPrecoVDR 
         BackStyle       =   0  'Transparent
         Caption         =   "Label3"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000FF00&
         Height          =   255
         Left            =   8100
         TabIndex        =   25
         Top             =   405
         Width           =   855
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   6465
      Width           =   10080
      _ExtentX        =   17780
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   17277
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      Top             =   810
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   0
      MPTR            =   1
      MICON           =   "frmAlteracao.frx":08CA
      PICN            =   "frmAlteracao.frx":08E6
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdGravar 
      Height          =   690
      Left            =   810
      TabIndex        =   3
      TabStop         =   0   'False
      ToolTipText     =   "Gravar Altera��es"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   0
      MPTR            =   1
      MICON           =   "frmAlteracao.frx":15C0
      PICN            =   "frmAlteracao.frx":15DC
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdPesquisar 
      Height          =   690
      Left            =   1575
      TabIndex        =   4
      TabStop         =   0   'False
      ToolTipText     =   "Consultar Informa��es"
      Top             =   60
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   0
      MPTR            =   1
      MICON           =   "frmAlteracao.frx":22B6
      PICN            =   "frmAlteracao.frx":22D2
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdCancelar 
      Height          =   690
      Left            =   2340
      TabIndex        =   5
      TabStop         =   0   'False
      ToolTipText     =   "Limpar Campos"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   0
      MPTR            =   1
      MICON           =   "frmAlteracao.frx":2FAC
      PICN            =   "frmAlteracao.frx":2FC8
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label lblFornec 
      AutoSize        =   -1  'True
      Caption         =   "Fornecedor"
      Height          =   195
      Left            =   90
      TabIndex        =   15
      Top             =   990
      Width           =   810
   End
   Begin VB.Label lblFabrica 
      AutoSize        =   -1  'True
      Caption         =   "F�brica"
      Height          =   195
      Left            =   375
      TabIndex        =   14
      Top             =   1320
      Width           =   525
   End
   Begin VB.Label lblDesc 
      AutoSize        =   -1  'True
      Caption         =   "Descri��o"
      Height          =   195
      Left            =   180
      TabIndex        =   13
      Top             =   1650
      Width           =   720
   End
   Begin VB.Label lblLinha 
      AutoSize        =   -1  'True
      Caption         =   "Linha"
      Height          =   195
      Left            =   510
      TabIndex        =   12
      Top             =   1980
      Visible         =   0   'False
      Width           =   390
   End
   Begin VB.Label lblSubGrupo 
      AutoSize        =   -1  'True
      Caption         =   "SubGrupo"
      Height          =   195
      Left            =   6255
      TabIndex        =   9
      Top             =   1980
      Visible         =   0   'False
      Width           =   720
   End
   Begin VB.Label lblGrupo 
      AutoSize        =   -1  'True
      Caption         =   "Grupo"
      Height          =   195
      Left            =   3375
      TabIndex        =   8
      Top             =   1980
      Visible         =   0   'False
      Width           =   435
   End
   Begin VB.Label lblUnid 
      AutoSize        =   -1  'True
      Caption         =   "Unidade"
      Height          =   195
      Left            =   5355
      TabIndex        =   7
      Top             =   1650
      Visible         =   0   'False
      Width           =   600
   End
   Begin VB.Label lblCodDpk 
      AutoSize        =   -1  'True
      Caption         =   "C�d. DPK"
      Height          =   195
      Left            =   3150
      TabIndex        =   6
      Top             =   1320
      Width           =   705
   End
End
Attribute VB_Name = "frmAlteracao"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Objetos
Dim frnFornecedor As New Fornecedor
Dim prdProduto As New Produto

' Variaveis
Private ds As Object
Dim mDiv As String
Dim mstrCdForn As String
Dim mstrCdDpk As String
Private vFl_Geral As Boolean
Private vCont As Integer
Private vGuardaLin As Integer
Private vGuardaCol As Integer
Private vCD_P As Integer

Private Sub cmdSair_Click()

    Unload Me

End Sub

Sub LIMPAR_CAMPOS()
'Limpa campos
txtPrNormal1.Visible = False
txtPrOferta1.Visible = False
txtPrSP1.Visible = False
txtPrecoVDR.Visible = False
fraTabelas.Visible = False
If Trim(txtCdDpk) <> "" Then
    txtCdForn = ""
    txtFabrica = ""
ElseIf Trim(txtCdDpk) = "" And (Trim(txtCdForn) <> "" And Trim(txtFabrica) <> "") Then
    txtCdDpk = ""
End If
txtDesc = ""
txtLinha = ""
txtGrupo = ""
txtSubGr = ""
Text7 = ""

End Sub

Private Sub cmdCancelar_Click()
    
    'Limpa Todos os Campos(TextBox) do Form
    Dim Control As Object
    
    For Each Control In Me.Controls
      If TypeName(Control) = "TextBox" Then
        Control.Text = ""
      End If
    Next Control
    
    txtPrNormal1.Visible = False
    txtPrOferta1.Visible = False
    txtPrSP1.Visible = False
    txtPrecoVDR.Visible = False
    
    ' Esconde a Tabela de Pre�os
    fraTabelas.Visible = False
    
End Sub

Private Sub cmdGravar_Click()
  
  Dim vCDSOnline As String
  Dim vFl_Online As Boolean
  
  Dim vPR_NOR As Double, vPR_OF As Double, vPR_SP As Double
  Dim vPR_VDR As Double
  
  Dim strFl_Online As String
  Dim strFl_Geral As String
  
  Dim I As Integer
  
  Dim vLoja As Integer
  
  Dim IP As String, Timeout As Integer, CDLJ As String, Success As Long
  
  'Verifica se foi digitado um DPK para consultar/gravar pre�o
  If mstrCdDpk = "" Then
      MsgBox "� necess�rio digitar um DPK!", vbCritical, "CAD180 - ATEN��O!"
      Exit Sub
  End If
       
  'Verifica se existem CDs marcados para gravar pre�o
  VERIFICA_DEPOSITO
  If vCont = 0 Then
      MsgBox "N�o h� nenhum CD marcado para gravar pre�o!", vbCritical, "CAD180"
      Exit Sub
  End If
  
  If MsgBox("Confirma a Atualiza��o ? " & Chr(13) & _
                "Somente ser�o atualizados os CDs marcados com 'SIM'!!!!", vbYesNo + vbQuestion, "Aten��o") = vbNo Then
      Exit Sub
  End If
  
  Screen.MousePointer = 11
  
  If vFl_Geral Then
  
      strFl_Geral = "S"
  
      vCDSOnline = ""
      vFl_Online = False
      
      strFl_Online = "N"
      
      If chkOnline.Value = 1 Then
      
          strFl_Online = "S"
      
          vFl_Online = True
          
          Set OraParameters = vBanco.Parameters
          Criar_Cursor OraParameters, "vCursor"
          
          OraParameters.Remove "LOJA_PADRAO":     OraParameters.Add "LOJA_PADRAO", DEPOSITO_DEFAULT, 1
          OraParameters.Remove "COD_LOJA":     OraParameters.Add "COD_LOJA", 0, 1
          OraParameters.Remove "ERRO":            OraParameters.Add "ERRO", 0, 2
          
          
          vSql = "PRODUCAO.PCK_CAD180.PR_CDS_IP(:LOJA_PADRAO, :COD_LOJA, :ERRO, :vCursor)"
          If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
              Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
              Exit Sub
          Else
              Set vObjOracle = OraParameters("vCursor").Value
          End If
      
          If vObjOracle.EOF Then
              MsgBox "N�o existem CDs cadastrados na tabela DEPOSITO_VISAO! " & Chr(13) & _
                    "Favor avisar o analista respons�vel!", vbCritical, "CAD180 - ATEN��O!)"
              Screen.MousePointer = 0
              Exit Sub
          End If
                
          
          While Not vObjOracle.EOF
              
              IP = vObjOracle("IP")
              Timeout = vObjOracle("TIME_OUT")
              CDLJ = vObjOracle("Cod_Loja")
              Success = Ping(IP, CDLJ, ECHO)
      
              If InStr(GetStatusCode(Success), ("ip success")) = 0 Or _
                  ECHO.RoundTripTime > Timeout Then
                  MsgBox "CD " & CDLJ & " sem comunica��o!" & Chr(13) & _
                      "Por favor, tente novamente mais tarde. " & Chr(13) & _
                      "Caso contr�rio a atualiza��o ser� feita somente � noite!!", vbInformation, "CAD180 - ATEN��O!"
              Else
                  vCDSOnline = vCDSOnline & ", " & CStr(CDLJ)
              End If
            
              vObjOracle.MoveNext
          Wend
          
          vCDSOnline = Mid(vCDSOnline, 3)
          
      End If
      
      grdLojas.Row = 1
      grdLojas.Col = 3
      vPR_NOR = CDbl(grdLojas.Text)
      grdLojas.Col = 4
      vPR_OF = CDbl(grdLojas.Text)
      grdLojas.Col = 5
      vPR_SP = CDbl(grdLojas.Text)
      
      If lblPrecoVDR.Visible = True Then
          grdLojas.Col = 6
          vPR_VDR = CDbl(grdLojas.Text)
      Else
          vPR_VDR = 0
      End If
      
      Set OraParameters = vBanco.Parameters
      
      OraParameters.Remove "COD_DPK":  OraParameters.Add "COD_DPK", CLng(mstrCdDpk), 1
      OraParameters.Remove "COD_LOJAS":  OraParameters.Add "COD_LOJAS", vCDSOnline, 1
      OraParameters.Remove "COD_LOJA":  OraParameters.Add "COD_LOJA", 0, 1
      OraParameters.Remove "FL_GERAL":  OraParameters.Add "FL_GERAL", strFl_Geral, 1
      OraParameters.Remove "FL_ONLINE":  OraParameters.Add "FL_ONLINE", strFl_Online, 1
      OraParameters.Remove "PR_VENDA":  OraParameters.Add "PR_VENDA", vPR_NOR, 1
      OraParameters.Remove "PR_OF":  OraParameters.Add "PR_OF", vPR_OF, 1
      OraParameters.Remove "PR_SP":  OraParameters.Add "PR_SP", vPR_SP, 1
      OraParameters.Remove "PR_VDR":  OraParameters.Add "PR_VDR", vPR_VDR, 1
      OraParameters.Remove "COD_ERRO":  OraParameters.Add "COD_ERRO", 0, 2
      OraParameters.Remove "TXT_ERRO":  OraParameters.Add "TXT_ERRO", Null, 2
      
      vSql = "PRODUCAO.PCK_CAD180.PR_GRAVA(:COD_DPK, :COD_LOJAS, :COD_LOJA, :FL_GERAL, :FL_ONLINE, :PR_VENDA, :PR_OF, :PR_SP, :PR_VDR, :COD_ERRO, :TXT_ERRO)"
      
      If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
          Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
          Exit Sub
      End If
            

  Else ' vFl_Geral = False
  
      strFl_Geral = "N"
      
      If (chkOnline.Value = 1) Then
     
          Set OraParameters = vBanco.Parameters
          Criar_Cursor OraParameters, "vCursor"
          
          OraParameters.Remove "LOJA_PADRAO":     OraParameters.Add "LOJA_PADRAO", DEPOSITO_DEFAULT, 1
          OraParameters.Remove "COD_LOJA":     OraParameters.Add "COD_LOJA", 0, 1
          OraParameters.Remove "ERRO":            OraParameters.Add "ERRO", 0, 2
          
          
          vSql = "PRODUCAO.PCK_CAD180.PR_CDS_IP(:LOJA_PADRAO, :COD_LOJA, :ERRO, :vCursor)"
          If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
              Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
              Exit Sub
          Else
              Set vObjOracle = OraParameters("vCursor").Value
          End If
      
          If vObjOracle.EOF Then
              MsgBox "N�o existem CDs cadastrados na tabela DEPOSITO_VISAO! " & Chr(13) & _
                    "Favor avisar o analista respons�vel!", vbCritical, "CAD180 - ATEN��O!)"
              Screen.MousePointer = 0
              Exit Sub
          End If
                
          While Not vObjOracle.EOF
              
              IP = vObjOracle("IP")
              Timeout = vObjOracle("TIME_OUT")
              CDLJ = vObjOracle("Cod_Loja")
              Success = Ping(IP, CDLJ, ECHO)
      
              If InStr(GetStatusCode(Success), ("ip success")) = 0 Or _
                  ECHO.RoundTripTime > Timeout Then
                  MsgBox "CD " & CDLJ & " sem comunica��o!" & Chr(13) & _
                      "Por favor, tente novamente mais tarde. " & Chr(13) & _
                      "Caso contr�rio a atualiza��o ser� feita somente � noite!!", vbInformation, "CAD180 - ATEN��O!"
              Else
                  vCDSOnline = vCDSOnline & ", " & CStr(CDLJ)
              End If
            
              vObjOracle.MoveNext
          Wend
          
          vCDSOnline = Mid(vCDSOnline, 3)
      
      Else
          vCDSOnline = ""
      End If
      
      For I = 2 To (grdLojas.Rows - 1)
    
        grdLojas.Row = I
        
        'Verifica se o CD est� marcado para atualiza��o
        grdLojas.Col = 1
        If grdLojas.Text = "SIM" Then
            
            'Pega o c�digo da loja a ter o pre�o alterado
            grdLojas.Col = 0
            vLoja = Val(Mid(grdLojas.Text, 1, 2))
            
            'Pega os pre�os NOR, OF e SP
            grdLojas.Col = 3
            vPR_NOR = CDbl(grdLojas.Text)
            grdLojas.Col = 4
            vPR_OF = CDbl(grdLojas.Text)
            grdLojas.Col = 5
            vPR_SP = CDbl(grdLojas.Text)
      
            If lblPrecoVDR.Visible = True Then
                grdLojas.Col = 6
                vPR_VDR = CDbl(grdLojas.Text)
            Else
                vPR_VDR = 0
            End If
            
            'Alterado aqui pois qdo. alterava o pre�o de venda em Cps. n�o disparava para os outros CDs e
            'ent�o estes CDs n�o conseguiam vender por Campinas, pelo pre�o atualizado.
            'If (chkOnline.Value = 1) And (vLoja <> 1) Then
            If (chkOnline.Value = 1) Then
                strFl_Online = "S"
            'Else
            '    strFl_Online = "N"
            End If
            
        End If
        
        Set OraParameters = vBanco.Parameters
        
        OraParameters.Remove "COD_DPK":  OraParameters.Add "COD_DPK", CLng(mstrCdDpk), 1
        OraParameters.Remove "COD_LOJAS":  OraParameters.Add "COD_LOJAS", vCDSOnline, 1
        OraParameters.Remove "COD_LOJA":  OraParameters.Add "COD_LOJA", vLoja, 1
        OraParameters.Remove "FL_GERAL":  OraParameters.Add "FL_GERAL", strFl_Geral, 1
        OraParameters.Remove "FL_ONLINE":  OraParameters.Add "FL_ONLINE", strFl_Online, 1
        OraParameters.Remove "PR_VENDA":  OraParameters.Add "PR_VENDA", vPR_NOR, 1
        OraParameters.Remove "PR_OF":  OraParameters.Add "PR_OF", vPR_OF, 1
        OraParameters.Remove "PR_SP":  OraParameters.Add "PR_SP", vPR_SP, 1
        OraParameters.Remove "PR_VDR":  OraParameters.Add "PR_VDR", vPR_VDR, 1
        OraParameters.Remove "COD_ERRO":  OraParameters.Add "COD_ERRO", 0, 2
        OraParameters.Remove "TXT_ERRO":  OraParameters.Add "TXT_ERRO", Null, 2
        
        vSql = "PRODUCAO.PCK_CAD180.PR_GRAVA(:COD_DPK, :COD_LOJAS, :COD_LOJA, :FL_GERAL, :FL_ONLINE, :PR_VENDA, :PR_OF, :PR_SP, :PR_VDR, :COD_ERRO, :TXT_ERRO)"
        
        If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
            Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
            Exit Sub
        Else
            If OraParameters("COD_ERRO").Value <> 0 Then
                Call vVB_Generica_001.Informar("Ocorreu um erro no processamento do CD " & CStr(vLoja) & Chr(13) & CStr(OraParameters("COD_ERRO").Value) & " - " & OraParameters("TXT_ERRO").Value)
            'Else
            '    MsgBox "ATUALIZA��O NO DEP�SITO: " & vLoja & " OK!" & Chr(13) & "Assinalado CD's Espec�ficos", vbExclamation, "CAD080 - ATEN��O!"
            End If
        End If
          
        
      Next I
  
  End If
  
  MsgBox "ATUALIZA��ES EFETUADAS COM SUCESSO!!", vbInformation, "CAD180 - ATEN��O!"
  Screen.MousePointer = 0
  
  
End Sub

Private Sub cmdPesquisar_Click()
Dim I As Integer


'Verifica se existem CDs marcados para gravar pre�o
VERIFICA_DEPOSITO

'If vCont = 0 Then
'    MsgBox "N�o h� nenhum CD marcado para gravar pre�o!", vbCritical, "CAD180"
'    Exit Sub
'End If

'fraTabelas.Visible = False

'Limpa campos de informa��es do grid
grdLojas.Cols = 7
grdLojas.Col = 6
grdLojas.ColWidth(6) = 1500
grdLojas.ColAlignment(6) = 2
grdLojas.Row = 0
grdLojas.Text = "   Pre�o VDR  "

For I = 1 To grdLojas.Rows - 1
    grdLojas.Row = I
    grdLojas.Col = 2
    grdLojas.Text = ""
    grdLojas.Col = 3
    grdLojas.Text = "0.00"
    grdLojas.Col = 4
    grdLojas.Text = "0.00"
    grdLojas.Col = 5
    grdLojas.Text = "0.00"
    grdLojas.Col = 6
    grdLojas.Text = "0.00"
Next I

If Trim(txtCdDpk) = "" And (Trim(txtCdForn.Text) = "" Or Trim(txtFabrica.Text) = "") Then
    MsgBox "Por favor, digite o item a ser consultado!", vbCritical, "CAD180 - ATEN��O!"
    Exit Sub
End If

CONS_ITEM

End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    
    Me.Top = 0
    Me.Left = 0
    
End Sub


Private Sub grdLojas_Click()
  chkOnline.Value = 0
End Sub

Private Sub grdLojas_DblClick()
Dim vCont2 As Integer
Dim I As Integer

'Esconde todos os campos de altera��o de pre�o
txtPrNormal1.Text = "0.00"
txtPrNormal1.Visible = False
txtPrOferta1.Text = "0.00"
txtPrOferta1.Visible = False
txtPrSP1.Text = "0.00"
txtPrSP1.Visible = False
txtPrecoVDR.Text = "0.00"
txtPrecoVDR.Visible = False

'CONTROLA CDS ASSINALADOS, PARA QUE O USU�RIO N�O ASSINALE 99-TODOS E MAIS ALGUM CD

'grdLojas.Col = vGuardaCol
'grdLojas.Row = vGuardaLin

'Se tiver dado duplo clique na coluna de marca
'de altera��o, alterna as  op��es "SIM" e "N�O"
If grdLojas.Col = 1 Then

    'grdLojas.Row = vGuardaLin

    If grdLojas.Text = "SIM" Then
        
        grdLojas.Text = "N�O"
        
        If grdLojas.Row = 1 Then
            vFl_Geral = False
        End If
    
    Else
        
        'Se tiver assinalando outro CD, diferente de 99 e 99 j� estiver assinalado, avisa usu�rio
        If vFl_Geral = True And grdLojas.Row <> 1 Then
            MsgBox "Aten��o! CD 99 - GERAL j� foi assinalado!", vbCritical, "CAD180"
        Else
            
            'Se estiver tentando assinalar o CD 99 para "SIM", verifica se existem outros CDs assinalados
            If grdLojas.Row = 1 Then
                vCont2 = 0
                For I = 2 To grdLojas.Rows - 1
                    grdLojas.Row = I
                    grdLojas.Col = 1
                    If grdLojas.Text = "SIM" Then
                        vCont2 = vCont2 + 1
                    End If
                Next I
                grdLojas.Row = 1
                If vCont2 > 0 Then
                    MsgBox "Aten��o! Existem outros CD's j� assinalados!", vbCritical, "CAD180"
                    grdLojas.Text = "N�O"
                Else
                    grdLojas.Text = "SIM"
                End If
            Else
                grdLojas.Text = "SIM"
            End If
            
        End If
    End If

    'Se tiver assinalado a loja 99-vai guardar numa vari�vel
    If grdLojas.Row = 1 Then
        grdLojas.Col = 1
        If grdLojas.Text = "SIM" Then
            vFl_Geral = True
        End If
    End If
    
    Call cmdPesquisar_Click

'Se o duplo clique for dado nas colunas de pre�o (Norma, OF, SP ou VDR)
ElseIf grdLojas.Col = 3 Or grdLojas.Col = 4 Or grdLojas.Col = 5 Or grdLojas.Col = 6 Then

    vGuardaLin = grdLojas.Row
    vGuardaCol = grdLojas.Col
    
    'Vai alterar somente para os CDs que estiverem assinalados
    grdLojas.Col = 1
    If grdLojas.Text = "SIM" Then
    
        If vGuardaCol = 3 Then
            grdLojas.Col = 3
            txtPrNormal1.Visible = True
            txtPrNormal1.Text = grdLojas.Text
        ElseIf vGuardaCol = 4 Then
            grdLojas.Col = 4
            txtPrOferta1.Visible = True
            txtPrOferta1.Text = grdLojas.Text
        ElseIf vGuardaCol = 5 Then
            grdLojas.Col = 5
            txtPrSP1.Visible = True
            txtPrSP1.Text = grdLojas.Text
        ElseIf vGuardaCol = 6 Then
            grdLojas.Col = 6
            txtPrecoVDR.Visible = True
            txtPrecoVDR.Text = grdLojas.Text
        End If
    Else
    
        grdLojas.Col = vGuardaCol
        grdLojas.Row = vGuardaLin
    
    End If


End If
End Sub


Private Sub txtcddpk_Change()
txtPrecoVDR = "0.00"
End Sub

Private Sub txtcddpk_Click()
SendKeys "{HOME}+{END}"
End Sub

Private Sub txtcddpk_GotFocus()
LIMPAR_CAMPOS
SendKeys "{HOME}+{END}"
End Sub


Private Sub txtcddpk_KeyPress(KeyAscii As Integer)
  KeyAscii = Numerico(KeyAscii)
End Sub

Private Sub txtcddpk_LostFocus()
Dim I As Integer

If Trim(txtCdDpk.Text) <> "" And Trim(txtFabrica.Text) <> "" Then
    txtFabrica.Text = ""
End If

If Trim(txtCdDpk.Text) <> "" And Trim(txtFabrica.Text) = "" Then

    'Vai verificar dados do item no CD Campinas
    If Not prdProduto.BuscarCodDPK(CLng(txtCdDpk.Text), 1) Then
      Exit Sub
    End If
    
    txtFabrica.Text = prdProduto.CodFabr
    txtCdForn.Text = prdProduto.CodForn
    mstrCdForn = Trim(txtCdForn.Text)
    'txtCdForn_LostFocus
    txtForn.Text = prdProduto.Sigla
    mDiv = prdProduto.Divisao
    mstrCdDpk = Trim(txtCdDpk.Text)
    
    txtFabrica_LostFocus
        
Else
    txtCdDpk.Enabled = True
End If

End Sub

Private Sub txtCdForn_Click()
    cmdCancelar_Click
End Sub

Private Sub txtCdForn_GotFocus()
    cmdCancelar_Click
End Sub

Private Sub txtCdForn_KeyPress(KeyAscii As Integer)
    KeyAscii = Numerico(KeyAscii)
End Sub

Private Sub txtCdForn_LostFocus()
  
  If Trim(txtCdForn.Text) <> "" Then
      
      If frnFornecedor.Buscar(CLng(txtCdForn.Text)) Then
          txtForn.Text = frnFornecedor.Sigla
          mDiv = frnFornecedor.Divisao
      End If
      
  Else
      txtForn.Text = ""
  End If
  
  mstrCdForn = Trim(txtCdForn.Text)

End Sub


Private Sub txtFabrica_Click()
    txtCdDpk.Text = ""
    mstrCdDpk = ""
    fraTabelas.Visible = False
End Sub

Private Sub txtFabrica_GotFocus()
    txtCdDpk.Text = ""
    mstrCdDpk = ""
    fraTabelas.Visible = False
End Sub

Private Sub txtFabrica_KeyPress(KeyAscii As Integer)
    KeyAscii = Texto(KeyAscii)
End Sub

Private Sub txtFabrica_LostFocus()
Dim I As Integer
Dim vLoja As Integer, vAux As Byte


Monta_Grid

If Trim(txtFabrica.Text) <> "" Then
 
    If prdProduto.BuscarCodFabr(1, txtFabrica.Text, CLng(mstrCdForn)) Then
        
        If Trim(txtCdDpk.Text) = "" Then
            
            txtDesc.Text = prdProduto.Descricao
            txtCdDpk.Text = prdProduto.CodDPK
            mstrCdDpk = Trim(txtCdDpk.Text)
            'txtCdDpk.Enabled = False
        
        End If
        
    Else
    
        Exit Sub
        
    End If
        
    'CD a CD - Espec�fico
    If vFl_Geral = False Then
    
        'Set ds = prdProduto.ListarCDSCodFabr(Trim(txtFabrica.Text), CLng(mstrCdForn))
        Set ds = prdProduto.ListarCDSCodFabr(txtFabrica.Text, CLng(mstrCdForn))
    
        txtDesc.Text = prdProduto.Descricao
        
        'Varre o grid comparando com a loja lida do select
        While Not ds.EOF
            
            vLoja = Val(ds("Cod_Loja"))
            vAux = 0
            I = 1
            
            Do While (vAux = 0) And (I <= grdLojas.Rows - 1)
                
                grdLojas.Row = I
                grdLojas.Col = 0
                
                If Val(Mid(grdLojas.Text, 1, 2)) = vLoja Then
                    grdLojas.Col = 1
                    grdLojas.Text = "SIM"
                    vAux = 1
                End If
                I = I + 1
            
            Loop
            ds.MoveNext
            
        Wend
        
        'Verifica novamente, ap�s verifica��o do item, se ainda existem CDs marcados para gravar pre�o
        VERIFICA_DEPOSITO
        
        If vCont = 0 Then
            MsgBox "N�o h� nenhum CD marcado para gravar pre�o!", vbCritical, "CAD180"
            Exit Sub
        End If
    
    End If
        
    'CONS_ITEM
        
Else
    txtCdDpk.Enabled = True

End If

chkCD99.Value = 0

End Sub


Sub VERIFICA_DEPOSITO()
    
    Dim I As Integer
    
    'Verifica se existe mais de um CD marcado para transfer�ncia
    'Se houver, n�o deixa fazer on-line
    vCont = 0
    
    vGuardaCol = grdLojas.Col
    vGuardaLin = grdLojas.Row
    
    For I = 1 To grdLojas.Rows - 1
        grdLojas.Row = I
        grdLojas.Col = 1
        If grdLojas.Text = "SIM" Then
            vCont = vCont + 1
        End If
    Next I
    
    grdLojas.Col = vGuardaCol
    grdLojas.Row = vGuardaLin

End Sub


Sub Monta_Grid()
    
    Dim I As Integer

    'On Error GoTo Trata_Erro

    '/////////////////////////
    'MONTA O GRID
    '/////////////////////////
    grdLojas.Visible = True
    grdLojas.Cols = 7
    grdLojas.Row = 0
    
    grdLojas.Col = 0
    grdLojas.ColWidth(0) = 2000
    grdLojas.Text = "          CD"
        
    grdLojas.Col = 1
    grdLojas.ColWidth(1) = 800
    grdLojas.ColAlignment(1) = 2
    grdLojas.Text = " Altera?"
    
    grdLojas.Col = 2
    grdLojas.ColWidth(2) = 500
    grdLojas.ColAlignment(2) = 2
    grdLojas.Text = "Cat."
    
    grdLojas.Col = 3
    grdLojas.ColWidth(3) = 1500
    grdLojas.ColAlignment(3) = 2
    grdLojas.Text = " Pre�o Normal"
    
    grdLojas.Col = 4
    grdLojas.ColWidth(4) = 1500
    grdLojas.ColAlignment(4) = 2
    grdLojas.Text = "   Pre�o OF  "
    
    grdLojas.Col = 5
    grdLojas.ColWidth(5) = 1500
    grdLojas.ColAlignment(5) = 2
    grdLojas.Text = "   Pre�o SP   "
    
    grdLojas.Col = 6
    grdLojas.ColWidth(6) = 1500
    grdLojas.ColAlignment(6) = 2
    grdLojas.Text = "   Pre�o VDR  "
    
    Set OraParameters = vBanco.Parameters
    Criar_Cursor OraParameters, "vCursor"
    OraParameters.Remove "ERRO":            OraParameters.Add "ERRO", 0, 2
    vSql = "PRODUCAO.PCK_CAD180.PR_LOJAS_VIS(:ERRO,:vCursor)"
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
        Exit Sub
    Else
        Set vObjOracle = OraParameters("vCursor").Value
    End If

    If vObjOracle.EOF Then
        Call vVB_Generica_001.Informar("Lojas n�o encontradas!")
        Screen.MousePointer = 0
        Exit Sub
    End If
    
    DEPOSITO_DEFAULT = Format(vObjOracle("DEPOSITO_DEFAULT"), "00")
    
    'Na primeira linha, vai colocar 99 - TODOS
    grdLojas.Rows = 2
    grdLojas.Row = 1
    grdLojas.Col = 0
    grdLojas.Text = "99 - GERAL"
    grdLojas.Col = 1
    'grdLojas.Text = "SIM"
    grdLojas.Text = "N�O"
    grdLojas.Col = 2
    grdLojas.Text = ""
    grdLojas.Col = 3
    grdLojas.Text = "0.00"
    grdLojas.Col = 4
    grdLojas.Text = "0.00"
    grdLojas.Col = 5
    grdLojas.Text = "0.00"
    grdLojas.Col = 6
    grdLojas.Text = "0.00"
    
    ' vFl_Geral = True
    vFl_Geral = False
    I = 1
    
    'Carrega o grid, primeiro com as informa��es dos CD's e com os pre�os zerados
    While Not vObjOracle.EOF
        
        I = I + 1
        grdLojas.Rows = I + 1
        grdLojas.Row = I
        grdLojas.Col = 0
        grdLojas.Text = Format(vObjOracle("CDLOJA"), "00") & " - " & vObjOracle("NMFANTA")
        grdLojas.Col = 1
        'grdLojas.Text = "SIM"
        grdLojas.Text = "N�O"
        grdLojas.Col = 2
        grdLojas.Text = ""
        grdLojas.Col = 3
        grdLojas.Text = "0.00"
        grdLojas.Col = 4
        grdLojas.Text = "0.00"
        grdLojas.Col = 5
        grdLojas.Text = "0.00"
        grdLojas.Col = 6
        grdLojas.Text = "0.00"
        
        vObjOracle.MoveNext
    
    Wend

Exit Sub

End Sub

Private Sub CONS_ITEM()
    
    Dim I As Integer
    Dim vCd As String

    Screen.MousePointer = 11
    
    DoEvents
    
    '-------------------------------------------
    'PROCURA NOME DA TABELA VDR
    '-------------------------------------------
    Set OraParameters = vBanco.Parameters
    
    OraParameters.Remove "DIV":   OraParameters.Add "DIV", mDiv, 1
    OraParameters.Remove "TAB_VDR":  OraParameters.Add "TAB_VDR", Null, 2
    OraParameters.Remove "TAB_NORMAL1":  OraParameters.Add "TAB_NORMAL1", Null, 2
    OraParameters.Remove "TAB_OFERTA1":  OraParameters.Add "TAB_OFERTA1", Null, 2
    OraParameters.Remove "TAB_SP1":  OraParameters.Add "TAB_SP1", Null, 2
    OraParameters.Remove "ERRO":  OraParameters.Add "ERRO", 0, 2
    
    vSql = "PRODUCAO.PCK_CAD180.PR_NOME_TABELAS(:DIV,:TAB_VDR,:TAB_NORMAL1,:TAB_OFERTA1,:TAB_SP1,:ERRO)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
        Exit Sub
    End If
    
    If OraParameters("ERRO").Value <> 0 Then
        Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
        Exit Sub
    End If
        
    lblPrecoVDR.Caption = OraParameters("TAB_VDR").Value
    
    lblNormal1.Caption = OraParameters("TAB_NORMAL1").Value
    lblOferta1.Caption = OraParameters("TAB_OFERTA1").Value
    lblSP1.Caption = OraParameters("TAB_SP1").Value
   
    ' ### CONTINUAR ### '
       
    '---------------------------------------------------
    'Seleciona pre�os de acordo com CDS
    'Traz a categoria do item por CD assinalado
    '-------------------------------------------
        
    'Se estiver assinalado CD 99-GERAL, traz o pre�o do CD CPS
    If vFl_Geral = True Then
               
        'PRE�OS PARA CD GERAL
        If Not prdProduto.BuscarPreco(CLng(mstrCdDpk), 0) Then
            Exit Sub
        End If
        
        grdLojas.Row = 1
        grdLojas.Col = 3
        grdLojas.Text = Format(prdProduto.PrecoVenda, "0.00")
        grdLojas.Col = 4
        grdLojas.Text = Format(prdProduto.PrecoOF, "0.00")
        grdLojas.Col = 5
        grdLojas.Text = Format(prdProduto.PrecoSP, "0.00")
        grdLojas.Col = 6
        grdLojas.Text = Format(prdProduto.PrecoVDR, "0.00")
    
        If Not prdProduto.BuscarCategoria(CLng(mstrCdDpk), 0) Then
            Exit Sub
        End If
        
        grdLojas.Col = 2
        grdLojas.Text = prdProduto.Categoria
    
    
    'Se n�o for CD 99, vai pegar pre�o por CD assinalado
    Else
    
        'Varre todo o grid para pegar os pre�os, de acordo com os CDs
        For I = 2 To grdLojas.Rows - 1
        
            grdLojas.Row = I
            grdLojas.Col = 1
            
            If grdLojas.Text = "SIM" Then
                
                grdLojas.Col = 0
                vCD_P = Mid(grdLojas.Text, 1, 2)
                
                If prdProduto.BuscarPreco(CLng(mstrCdDpk), vCD_P) Then
                    grdLojas.Row = I
                    grdLojas.Col = 3
                    grdLojas.Text = Format(prdProduto.PrecoVenda, "0.00")
                    grdLojas.Col = 4
                    grdLojas.Text = Format(prdProduto.PrecoOF, "0.00")
                    grdLojas.Col = 5
                    grdLojas.Text = Format(prdProduto.PrecoSP, "0.00")
                    'tratamento para n�o dar invalid row value
                    If lblPrecoVDR.Visible = True Then
                    'If grdLojas.Cols = 7 Then
                        grdLojas.Col = 6
                        grdLojas.Text = Format(prdProduto.PrecoVDR, "0.00")
                    End If
                Else
                    MsgBox "Item desativado ou N�o existe pre�o cadastrado para CD " & vCD_P & "!" & Chr(13) & _
                           "CD " & vCD_P & " ser� desmarcado!", vbInformation, "CAD180 - ATEN��O!"
                    Screen.MousePointer = 0
                    grdLojas.Row = I
                    grdLojas.Col = 1
                    grdLojas.Text = "N�O"
                 End If
                
                'CATEGORIA POR CD ASSINALADO
                If Not prdProduto.BuscarCategoria(CLng(mstrCdDpk), vCD_P) Then
                    Exit Sub
                End If
                
                grdLojas.Col = 2
                grdLojas.Text = prdProduto.Categoria

            End If
        
        Next I
    End If
    
    '-------------- VERIFICA SE � VDR ------------------------------
    If Trim(txtFabrica.Text) <> "" Then
        'If prdProduto.BuscarCodFabr(1, Trim(txtFabrica.Text), CLng(mstrCdForn)) Then
        If prdProduto.BuscarCodFabr(1, txtFabrica.Text, CLng(mstrCdForn)) Then
        
            If Trim(txtCdDpk.Text) = "" Then
            
                txtDesc.Text = prdProduto.Descricao
                txtCdDpk.Text = prdProduto.CodDPK
                mstrCdDpk = Trim(txtCdDpk.Text)
                'txtCdDpk.Enabled = False
            
            End If
            
        Else
        
            Exit Sub
            
        End If
    ElseIf Trim(txtCdDpk.Text) <> "" Then
                mstrCdDpk = Trim(txtCdDpk.Text)
    End If
    
    
    If prdProduto.VerificaVDR(CLng(mstrCdDpk)) Then
        lblPrecoVDR.Visible = True
        lblVDR.Visible = True
    Else
        lblPrecoVDR.Visible = False
        lblVDR.Visible = False
        grdLojas.Cols = 6
    End If
    
    fraTabelas.Visible = True
    
    Screen.MousePointer = 1

End Sub

Private Sub chkCD99_Click()
    
    Dim I As Integer
    
    If chkCD99.Value = 1 Then
        I = 1
        For I = 1 To grdLojas.Rows - 1
            
            grdLojas.Col = 0
            grdLojas.Row = I
            If grdLojas.Text = "99 - GERAL" Then
                grdLojas.Col = 1
                grdLojas.Text = "SIM"
            Else
                grdLojas.Col = 1
                grdLojas.Text = "N�O"
            End If
        
        Next I
        vFl_Geral = True
        Call cmdPesquisar_Click
    End If
    
    If chkCD99.Value = 0 Then
        I = 1
        For I = 1 To grdLojas.Rows - 1
            
            grdLojas.Col = 0
            grdLojas.Row = I
            If grdLojas.Text = "99 - GERAL" Then
                grdLojas.Col = 1
                grdLojas.Text = "N�O"
            Else
                grdLojas.Col = 1
                grdLojas.Text = "SIM"
            End If
        
        Next I
        vFl_Geral = False
        Call cmdPesquisar_Click
    End If

End Sub

Private Sub chkONLINE_Click()
  
  VERIFICA_DEPOSITO
  
  If vCont = 0 And chkOnline.Value = 1 Then
      MsgBox "N�o h� nenhum CD marcado para gravar pre�o!", vbCritical, "CAD180"
      
      chkOnline.Value = 0
      Exit Sub
  End If

End Sub
Private Sub txtPrecoVDR_Click()
SendKeys "{HOME}+{END}"

End Sub


Private Sub txtPrecoVDR_LostFocus()
  
    txtPrecoVDR.Text = FmtBR(txtPrecoVDR)
    
    If txtPrecoVDR < 0 Then
        MsgBox "Pre�o negativo!"
        txtPrecoVDR.SetFocus
    End If

    grdLojas.Row = vGuardaLin
    grdLojas.Col = vGuardaCol
    grdLojas.Text = Format(txtPrecoVDR, "###,##0.00")

    txtPrecoVDR.Text = "0.00"
    txtPrecoVDR.Visible = False

End Sub


Private Sub txtPrNormal1_Click()
SendKeys "{HOME}+{END}"
End Sub


Private Sub txtPrNormal1_GotFocus()
SendKeys "{HOME}+{END}"
End Sub





Private Sub txtPrNormal1_LostFocus()
    
    txtPrNormal1.Text = FmtBR(txtPrNormal1)
    
    If txtPrNormal1 < 0 Then
        MsgBox "Pre�o negativo!"
        txtPrNormal1.SetFocus
    End If

    grdLojas.Row = vGuardaLin
    grdLojas.Col = vGuardaCol
    grdLojas.Text = Format(txtPrNormal1, "###,##0.00")
    
    txtPrNormal1.Text = "0.00"
    txtPrNormal1.Visible = False


End Sub

Private Sub txtPrOferta1_Click()
SendKeys "{HOME}+{END}"
End Sub


Private Sub txtPrOferta1_GotFocus()
SendKeys "{HOME}+{END}"
End Sub




Private Sub txtPrOferta1_LostFocus()
  
    txtPrOferta1.Text = FmtBR(txtPrOferta1)
    
    If txtPrOferta1 < 0 Then
        MsgBox "Pre�o negativo!"
        txtPrOferta1.SetFocus
    End If

    grdLojas.Row = vGuardaLin
    grdLojas.Col = vGuardaCol
    grdLojas.Text = Format(txtPrOferta1, "###,##0.00")

    txtPrOferta1.Text = "0.00"
    txtPrOferta1.Visible = False

End Sub

Private Sub txtPrSP1_Click()
SendKeys "{HOME}+{END}"
End Sub


Private Sub txtPrSP1_GotFocus()
SendKeys "{HOME}+{END}"
End Sub






Private Sub txtPrSP1_LostFocus()
  
    txtPrSP1.Text = FmtBR(txtPrSP1)
    
    If txtPrSP1 < 0 Then
        MsgBox "Pre�o negativo!"
        txtPrSP1.SetFocus
    End If

    grdLojas.Row = vGuardaLin
    grdLojas.Col = vGuardaCol
    grdLojas.Text = Format(txtPrSP1, "###,##0.00")

    txtPrSP1.Text = "0.00"
    txtPrSP1.Visible = False


End Sub
