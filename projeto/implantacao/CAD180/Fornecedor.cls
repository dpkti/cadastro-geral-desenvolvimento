VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Fornecedor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private CodForn As Long
Private SiglaForn As String
Private DivForn As String

Public Property Get Codigo() As Long
    Codigo = CodForn
End Property

Public Property Get Sigla() As String
    Sigla = SiglaForn
End Property

Public Property Get Divisao() As String
    Divisao = DivForn
End Property

Public Function Buscar(ByVal Codigo As Long) As Boolean
        
    'Selecionar os c�digos de f�brica que dever�o entrar na consulta
    Set OraParameters = vBanco.Parameters
    Criar_Cursor OraParameters, "vCursor"
    OraParameters.Remove "COD_FORNECEDOR":  OraParameters.Add "COD_FORNECEDOR", Codigo, 1
    OraParameters.Remove "ERRO":            OraParameters.Add "ERRO", 0, 2
    vSql = "PRODUCAO.PCK_CAD180.PR_FORNECEDOR(:COD_FORNECEDOR,:ERRO,:vCursor)"
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
        Exit Function
    Else
        Set vObjOracle = OraParameters("vCursor").Value
    End If

    If vObjOracle.EOF Then
        Call vVB_Generica_001.Informar("Fornecedor n�o encontrado!")
        Buscar = False
        Screen.MousePointer = 0
        Exit Function
    End If
    
    SiglaForn = vObjOracle("Sigla")
    DivForn = vObjOracle("Divisao")
        
    Buscar = True

End Function
