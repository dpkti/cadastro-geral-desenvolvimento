Attribute VB_Name = "modFuncoes"
Option Explicit
Public OraParameters As Object
Public DEPOSITO_DEFAULT As Integer
Public ECHO As ICMP_ECHO_REPLY

Function Numerico(ByVal KeyAscii As Integer) As Integer
    If KeyAscii = 8 Then    'BACKSPACE
        Numerico = KeyAscii
        Exit Function
    End If
    If Chr(KeyAscii) < "0" Or Chr(KeyAscii) > "9" Then
        KeyAscii = 0
        Beep
    End If
    Numerico = KeyAscii
End Function

Public Sub Criar_Cursor(Banco As Object, pNomeCursor As String)
    Banco.Remove pNomeCursor
    Banco.Add pNomeCursor, 0, 3
    Banco(pNomeCursor).serverType = 102
    Banco(pNomeCursor).DynasetOption = &H2&
    Banco(pNomeCursor).DynasetCacheParams 256, 16, 20, 2000, 0
End Sub

Function Texto(ByVal KeyAscii As Integer) As Integer
    If KeyAscii = 8 Then    'BACKSPACE
        Texto = KeyAscii
        Exit Function
    End If
    
    If Chr$(KeyAscii) = "'" Or Chr$(KeyAscii) = "," Or Chr$(KeyAscii) = ";" Then
        KeyAscii = 0
        Beep
    Else
        KeyAscii = Asc(UCase(Chr$(KeyAscii)))
    End If
    
    Texto = KeyAscii
End Function

Function FmtBR(ByVal Valor) As String

    Dim Temp As String
    Dim I As Integer
    
    Temp = Trim(Valor)
        
    For I = 1 To Len(Temp)
        If Mid$(Temp, I, 1) = "," Then
            Mid$(Temp, I, 1) = "."
        End If
    Next I

    FmtBR = Temp

End Function

