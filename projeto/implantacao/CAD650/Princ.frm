VERSION 4.00
Begin VB.Form frmPrinc 
   Caption         =   "Cad650 - Cadastro de Informa��es - NF Sa�da"
   ClientHeight    =   4605
   ClientLeft      =   2370
   ClientTop       =   2550
   ClientWidth     =   6810
   ForeColor       =   &H00404040&
   Height          =   5295
   Icon            =   "Princ.frx":0000
   Left            =   2310
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   4605
   ScaleWidth      =   6810
   Top             =   1920
   Width           =   6930
   Begin VB.ComboBox cmbCodLoja 
      Height          =   315
      Left            =   1800
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   420
      Width           =   855
   End
   Begin VB.TextBox txtSubSerie 
      Enabled         =   0   'False
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   1680
      MaxLength       =   2
      TabIndex        =   12
      Top             =   2460
      Width           =   1215
   End
   Begin VB.TextBox txtNumSerie 
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   5100
      MaxLength       =   3
      TabIndex        =   10
      Text            =   "AC"
      Top             =   1800
      Width           =   1215
   End
   Begin VB.TextBox txtPAIDF 
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   5100
      MaxLength       =   10
      TabIndex        =   14
      Text            =   "0200005138"
      Top             =   2460
      Width           =   1215
   End
   Begin VB.TextBox txtNumSelo 
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   1680
      MaxLength       =   10
      TabIndex        =   8
      Top             =   1800
      Width           =   1215
   End
   Begin VB.TextBox txtNumForm 
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   5100
      MaxLength       =   10
      TabIndex        =   6
      Top             =   1140
      Width           =   1215
   End
   Begin VB.TextBox txtNumNF 
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   1680
      MaxLength       =   6
      TabIndex        =   4
      Top             =   1170
      Width           =   1215
   End
   Begin Threed.SSCommand sscmdExcluir 
      Height          =   495
      Left            =   3495
      TabIndex        =   21
      Top             =   3345
      Width           =   495
      _Version        =   65536
      _ExtentX        =   873
      _ExtentY        =   873
      _StockProps     =   78
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "Princ.frx":014A
   End
   Begin VB.Label lblLoja 
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   2760
      TabIndex        =   2
      Top             =   540
      Width           =   2295
   End
   Begin Threed.SSCommand sscmdPesquisa 
      Height          =   495
      Left            =   945
      TabIndex        =   15
      Top             =   3345
      Width           =   495
      _Version        =   65536
      _ExtentX        =   873
      _ExtentY        =   873
      _StockProps     =   78
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "Princ.frx":0464
   End
   Begin Threed.SSCommand sscmdSair 
      Height          =   495
      Left            =   4500
      TabIndex        =   18
      Top             =   3345
      Width           =   495
      _Version        =   65536
      _ExtentX        =   873
      _ExtentY        =   873
      _StockProps     =   78
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "Princ.frx":08B6
   End
   Begin Threed.SSCommand sscmdGravar 
      Height          =   495
      Left            =   1785
      TabIndex        =   16
      Top             =   3345
      Width           =   495
      _Version        =   65536
      _ExtentX        =   873
      _ExtentY        =   873
      _StockProps     =   78
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "Princ.frx":0BD0
   End
   Begin ComctlLib.StatusBar Status 
      Align           =   2  'Align Bottom
      Height          =   390
      Left            =   0
      TabIndex        =   19
      Top             =   4215
      Width           =   6810
      _ExtentX        =   12012
      _ExtentY        =   688
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
         NumPanels       =   2
         BeginProperty Panel1 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
            Alignment       =   1
            Object.Width           =   9084
            MinWidth        =   9084
            TextSave        =   ""
            Key             =   ""
            Object.Tag             =   ""
         EndProperty
         BeginProperty Panel2 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
            Style           =   6
            Alignment       =   1
            TextSave        =   "20/06/05"
            Key             =   ""
            Object.Tag             =   ""
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin Threed.SSCommand sscmdCancelar 
      Height          =   495
      Left            =   2625
      TabIndex        =   17
      Top             =   3345
      Width           =   495
      _Version        =   65536
      _ExtentX        =   873
      _ExtentY        =   873
      _StockProps     =   78
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "Princ.frx":0EEA
   End
   Begin VB.Label Label7 
      Alignment       =   1  'Right Justify
      Caption         =   "Subs�rie"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   840
      TabIndex        =   11
      Top             =   2550
      Width           =   735
   End
   Begin VB.Label Label6 
      Alignment       =   1  'Right Justify
      Caption         =   "S�rie"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   4260
      TabIndex        =   9
      Top             =   1890
      Width           =   735
   End
   Begin VB.Label Label5 
      Alignment       =   1  'Right Justify
      Caption         =   "PAIDF n�"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   4140
      TabIndex        =   13
      Top             =   2520
      Width           =   855
   End
   Begin VB.Label Label4 
      Alignment       =   1  'Right Justify
      Caption         =   "Selo Fiscal n�"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   240
      TabIndex        =   7
      Top             =   1890
      Width           =   1335
   End
   Begin VB.Label Label3 
      Alignment       =   1  'Right Justify
      Caption         =   "Formul�rio n�"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   3780
      TabIndex        =   5
      Top             =   1230
      Width           =   1215
   End
   Begin VB.Label Label2 
      Alignment       =   1  'Right Justify
      Caption         =   "NF Sa�da n�"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   480
      TabIndex        =   3
      Top             =   1260
      Width           =   1095
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "C�digo da Loja"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   360
      TabIndex        =   0
      Top             =   540
      Width           =   1335
   End
   Begin Threed.SSCommand sscmdAlterar 
      Height          =   495
      Left            =   1785
      TabIndex        =   20
      Top             =   3345
      Width           =   495
      _Version        =   65536
      _ExtentX        =   873
      _ExtentY        =   873
      _StockProps     =   78
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "Princ.frx":1204
   End
   Begin VB.Menu mnuArquivo 
      Caption         =   "&Sair"
   End
   Begin VB.Menu mnuSobre 
      Caption         =   "S&obre"
   End
End
Attribute VB_Name = "frmPrinc"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Private Sub SSCommand1_Click()

End Sub


Private Sub cmbCodLoja_Click()
    cmd = "select nome_fantasia from loja where cod_loja= " & cmbCodLoja.Text & " "
    
    Set oradynaset = oradatabase.dbCreateDynaset(cmd, 0&)
    
    lblLoja.Caption = oradynaset!nome_fantasia

End Sub


Private Sub Form_Load()
    Dim cmd As String
    Dim form As String  'D�gitos do form
    Dim selo As String  'D�gitos do selo
    Dim vUSER As String


   'ESTABELECE CONEX�O COM O ORACLE
    Set orasession = CreateObject("oracleinproCServer.xorasession")
   'VERS�O DO EXECUT�VEL EXISTENTE NO CD FORTALEZA DEVE CONECTAR DESTA FORMA:
   Set oradatabase = orasession.OpenDatabase("PRODUCAO", "CAD650/PROD", 0&)
   'VERS�O DO EXECUT�VEL EXISTENTE NO CD CAMPINAS DEVE CONECTAR DESTA FORMA:
    'Set oradatabase = orasession.OpenDatabase("CD05", "CAD650/PROD", 0&)
   
   'Set oradatabase = orasession.OpenDatabase("DESENV", "PRODUCAO/DES", 0&)
   'Set oradatabase = orasession.OpenDatabase("SDPK", "CAD650/PROD", 0&)


    sscmdAlterar.Visible = False
    sscmdGravar.Visible = True
    cmbCodLoja.Enabled = True
    txtNumNF.Enabled = True
    txtNumForm.Enabled = True
    
    cmd = "select COD_LOJA from DEPOSITO_VISAO where NOME_PROGRAMA='CAD650'"
    
    Set oradynaset = oradatabase.dbCreateDynaset(cmd, 0&)

    Do While Not oradynaset.EOF
        cmbCodLoja.AddItem oradynaset!COD_LOJA
        oradynaset.MoveNext
    Loop
    
End Sub

Private Sub sscmdCancela_Click()
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Status.Panels(1) = ""
End Sub

Private Sub mnuArquivo_Click()
    End
End Sub

Private Sub mnuSobre_Click()
    frmSobre.Show vbModal
End Sub

Private Sub scmdExcluir_Click()

End Sub

Private Sub sscmdAlterar_Click()
    
    'Consistencia para n�o gravar n�mero de selo ou formul�rio errado
    'If Len(txtNumForm.Text) < 4 Or Len(txtNumSelo.Text) < 4 Then
    '    MsgBox "Verifique se digitou todos os n�meros do selo e formul�rio."
    '    Exit Sub
    'Else
    '    form = Right(txtNumForm.Text, 4)
    '    selo = Right(txtNumSelo.Text, 4)
    '    If form <> selo Then
    '        MsgBox "Verifique o n�mero do Selo e Formul�rio. Os 4 �ltimos d�gitos n�o s�o iguais."
    '        txtNumSelo.SetFocus
    '        Exit Sub
    '    End If
    'End If
    
    
    If txtNumSerie.Text = "" Then
        MsgBox "Este campo n�o pode ser nulo."
    Else
        cmd = "update NUM_SEGURANCA " & _
              "set SERIE = '" & txtNumSerie.Text & "', " & _
              "NUM_SELO_FISCAL =" & txtNumSelo.Text & ", " & _
              "PAIDF =" & txtPAIDF.Text & " " & _
              "where COD_LOJA =" & cmbCodLoja.Text & " And NUM_NF = " & txtNumNF.Text & ""
    
        oradatabase.dbexecutesql cmd
            
        MsgBox "Altera��o efetuada!", 64, Me.Caption
    End If
    
    txtNumNF.Text = ""
    txtNumSerie.Text = "AC"
    txtSubSerie.Text = ""
    txtNumForm.Text = ""
    txtNumSelo.Text = ""
    txtPAIDF.Text = "0200005138"
    lblLoja.Caption = ""
    sscmdAlterar.Visible = False
    sscmdGravar.Visible = True
    cmbCodLoja.Enabled = True
    txtNumNF.Enabled = True
    txtNumForm.Enabled = True
    cmbCodLoja.SetFocus
    
End Sub

Private Sub sscmdAlterar_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Status.Panels(1) = "Alterar os dados do formul�rio"
End Sub


Private Sub sscmdCancelar_Click()
    
    txtNumNF.Text = ""
    txtNumSerie.Text = "AC"
    txtSubSerie.Text = ""
    txtNumForm.Text = ""
    txtNumSelo.Text = ""
    txtPAIDF.Text = "0200005138"
    lblLoja.Caption = ""
    sscmdAlterar.Visible = False
    sscmdGravar.Visible = True
    cmbCodLoja.Enabled = True
    txtNumNF.Enabled = True
    txtNumForm.Enabled = True
    cmbCodLoja.SetFocus

End Sub

Private Sub sscmdCancelar_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Status.Panels(1) = "Limpar os dados do formul�rio"
End Sub


Private Sub sscmdExcluir_Click()

    'Consist�ncia dos campos
    If cmbCodLoja = "" Or Val(cmbCodLoja) = 0 Then
        MsgBox "Favor preencher C�digo de Loja", vbCritical, "Aten��o!"
        cmbCodLoja.SetFocus
        Exit Sub
    End If
    If Trim(txtNumNF) = "" Then
        MsgBox "Favor preencher N�mero de NF", vbCritical, "Aten��o!"
        txtNumNF.SetFocus
        Exit Sub
    End If
    If Trim(txtNumForm) = "" Then
        MsgBox "Favor preencher N�mero de Formul�rio", vbCritical, "Aten��o!"
        txtNumForm.SetFocus
        Exit Sub
    End If

    'Verifica exist�ncia do registro
    oradatabase.Parameters.Remove "LOJA": oradatabase.Parameters.Add "LOJA", cmbCodLoja, 1
    oradatabase.Parameters.Remove "NF": oradatabase.Parameters.Add "NF", txtNumNF, 1
    oradatabase.Parameters.Remove "FORM": oradatabase.Parameters.Add "FORM", txtNumForm, 1
    oradatabase.Parameters.Remove "TXT_ERRORA": oradatabase.Parameters.Add "TXT_ERRORA", "", 2
    oradatabase.Parameters.Remove "COD_ERRORA": oradatabase.Parameters.Add "COD_ERRORA", 0, 1
   'linha de tratamento de erro para a nova OLE
    oradatabase.Parameters("cod_errora").serverType = ORATYPE_NUMBER
   'linha de tratamento de erro para a nova OLE
    oradatabase.Parameters("txt_errora").serverType = ORATYPE_STRING
    
    cmd = "Select Count(*) TotReg " & _
          "From NUM_SEGURANCA " & _
          "Where Cod_Loja = :LOJA " & _
                "And Num_Nf = :NF " & _
                "And Num_Formulario_Continuo = :FORM"
    
    Set oradynaset = oradatabase.dbCreateDynaset(cmd, 0&)
    
    If oradynaset!TotReg = 0 Then
        MsgBox "Selo Fiscal n�o cadastrado!", 64, Me.Caption
        Exit Sub
    End If

   'Se encontrou registro, faz a exclus�o, conforme confirma��o do usu�rio
    If MsgBox("Confirma exclus�o?", vbYesNo, Me.Caption) = vbYes Then
    
        cmd = "Delete NUM_SEGURANCA " & _
               "Where Cod_Loja = :LOJA " & _
                 "And Num_Nf = :NF " & _
                 "And Num_Formulario_Continuo = :FORM; "
    
        cmd = "Begin " & cmd & " COMMIT; EXCEPTION WHEN OTHERS THEN ROLLBACK;" & _
                        " :cod_errora := SQLCODE; :txt_errora := SQLERRM; END; "
                
        oradatabase.executesql (cmd)
                
               
        VErro = IIf((oradatabase.Parameters("COD_ERRORa").Value = Null Or oradatabase.Parameters("cod_errora").Value = ""), 0, oradatabase.Parameters("cod_errora").Value)
        If Val(VErro) <> 0 Or Trim(oradatabase.Parameters("Txt_Errora")) <> "" Then
             MsgBox "Erro na exclus�o do selo fiscal!" & Chr(10) & _
                    "Erro: " & oradatabase.Parameters("cod_errora").Value & " - " & _
                    oradatabase.Parameters("txt_errora").Value, vbCritical, Me.Caption
        ElseIf Val(VErro) <> 0 Then
             MsgBox "Erro na exclus�o do selo fiscal!" & Chr(10) & _
                   "Erro: " & Err.Number & " - " & Err.Description, vbCritical, Me.Caption
        Else
            
            MsgBox "Exclus�o Efetuada com sucesso!", 64, Me.Caption
            Call sscmdCancelar_Click
        End If
    
    End If

End Sub

Private Sub sscmdExcluir_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
        Status.Panels(1) = "Exclui registro de selo fiscal"
End Sub


Private Sub sscmdGravar_Click()
On Error GoTo Erro
Dim Posicao As Integer

    'Eduardo 09/06/05
    If Trim(txtNumForm) = "" Then
        MsgBox "Campo N� Formul�rio obrigat�rio.", vbInformation, "Aten��o"
        txtNumForm.SetFocus
        Exit Sub
    End If
    
    If txtNumSerie.Text = "" Then
        MsgBox "Este campo n�o pode ser nulo."
    Else
        oradatabase.Parameters.Remove "COD_LOJA"
        oradatabase.Parameters.Add "COD_LOJA", cmbCodLoja.Text, 1
        oradatabase.Parameters.Remove "NUM_NF"
        oradatabase.Parameters.Add "NUM_NF", txtNumNF.Text, 1
        oradatabase.Parameters.Remove "SERIE"
        oradatabase.Parameters.Add "SERIE", txtNumSerie.Text, 1
        oradatabase.Parameters.Remove "NUM_FORM"
        oradatabase.Parameters.Add "NUM_FORM", txtNumForm.Text, 1
        oradatabase.Parameters.Remove "NUM_SELO"
        oradatabase.Parameters.Add "NUM_SELO", txtNumSelo.Text, 1
        oradatabase.Parameters.Remove "PAIDF"
        oradatabase.Parameters.Add "PAIDF", txtPAIDF.Text, 1
        
        cmd = "insert into num_seguranca (COD_LOJA, NUM_NF, SERIE, " & _
              "NUM_FORMULARIO_CONTINUO, NUM_SELO_FISCAL, PAIDF) " & _
              "values (:COD_LOJA, :NUM_NF, :SERIE, :NUM_FORM, " & _
              ":NUM_SELO, :PAIDF) "
    
        oradatabase.dbexecutesql cmd
    
        MsgBox "Inclus�o Efetuada!", 64, Me.Caption
    End If
    
    txtNumNF.Text = ""
    txtNumSerie.Text = "AC"
    txtSubSerie.Text = ""
    txtNumForm.Text = ""
    txtNumSelo.Text = ""
    txtPAIDF.Text = "0200005138"
    lblLoja.Caption = ""
    cmbCodLoja.SetFocus

Exit Sub
 
Erro:
   Posicao = 0
   Posicao = InStr(Err.Description, "ORA-00001")
   If Posicao > 0 Then
      MsgBox "N�mero de formul�rio ou selo fiscal j� existente para esta loja e s�rie!", 16, Me.Caption
   Else
      MsgBox Err.Description & " - " & CStr(Err), 16, Me.Caption
   End If
     
End Sub

Private Sub sscmdGravar_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Status.Panels(1) = "Gravar os dados da nota"
End Sub


Private Sub sscmdPesquisa_Click()
    cmd = " select * from NUM_SEGURANCA " & _
          " where COD_LOJA=" & cmbCodLoja.Text & " and NUM_NF=" & txtNumNF.Text & " "
    
    Set oradynaset = oradatabase.dbCreateDynaset(cmd, 0&)
    
    If oradynaset.EOF Then
        MsgBox "Nota Fiscal n�o encontrada para esta loja", 64, Me.Caption
        cmbCodLoja.SetFocus
    Else
        'cmbCodLoja.Text = oradynaset!COD_LOJA
        'txtNumNF.Text = oradynaset!NUM_NF
        txtNumSerie.Text = oradynaset!SERIE
        txtNumForm.Text = oradynaset!NUM_FORMULARIO_CONTINUO
        txtNumSelo.Text = oradynaset!NUM_SELO_FISCAL
        txtPAIDF.Text = oradynaset!PAIDF
        sscmdAlterar.Visible = True
        sscmdGravar.Visible = False
        cmbCodLoja.Enabled = False
        txtNumNF.Enabled = False
        txtNumForm.Enabled = False
    End If
    
End Sub

Private Sub sscmdPesquisa_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
        Status.Panels(1) = "Consulta os dados da nota e loja escolhidas"
End Sub


Private Sub sscmdSair_Click()
    End
End Sub


Private Sub sscmdSair_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Status.Panels(1) = "Finalizar o sistema"
End Sub






Private Sub txtCodLoja_Change()

End Sub

Private Sub txtNumNF_LostFocus()

    Dim resp As Integer

    'Verifica se existe a nota digitada para este cd
    cmd = " select COD_LOJA_NOTA, NUM_NOTA from PEDNOTA_VENDA " & _
          " where COD_LOJA_NOTA=" & cmbCodLoja.Text & " and NUM_NOTA=" & txtNumNF.Text & " "
    
    Set oradynaset = oradatabase.dbCreateDynaset(cmd, 0&)

    If oradynaset.EOF Then
        MsgBox "Nota n�o existente para esta loja", 64, Me.Caption
        cmbCodLoja.SetFocus
    
    'Comentado pois o n�mero de s�rie n�o � da nota fiscal e sim do selo fiscal que ser� digitado
    'Else
    '    'traz o n�mero de s�rie automaticamente de acordo com a loja em quest�o
    '
    '    cmd = " select serie from loja_serie where cod_loja= " & cmbCodLoja.Text & " and " & _
    '          " tipo_nf='S'"
    '
    '    Set oradynaset = oradatabase.dbCreateDynaset(cmd, 0&)
    '
    '    If Not oradynaset.EOF Then
    '        txtNumSerie.Text = oradynaset!SERIE
    '    Else
    '        MsgBox "Loja n�o encontrada!"
    '        cmbCodLoja.SetFocus
    '    End If
    End If
    
    
End Sub

Private Sub txtNumSelo_LostFocus()
    
    'Vai fazer consist�ncia dos campos, somente na hora de efetuar a transa��o
    
    'If Len(txtNumForm.Text) < 4 Or Len(txtNumSelo.Text) < 4 Then
    '    MsgBox "Verifique se digitou todos os n�meros do selo e formul�rio."
    'Else
    '    form = Right(txtNumForm.Text, 4)
    '    selo = Right(txtNumSelo.Text, 4)
    '    If form <> selo Then
    '        MsgBox "Verifique o n�mero do Selo e Formul�rio. Os 4 �ltimos d�gitos n�o s�o iguais."
    '        Exit Sub
    '    End If
    'End If

    'De acordo com Josinal, aumentou a qtd. de d�gitos do selo e somente os 2 �ltimos ser�o iguais ao formul�rio
    If Len(txtNumForm.Text) < 4 Or Len(txtNumSelo.Text) < 4 Then
        MsgBox "Verifique se digitou todos os n�meros do selo e formul�rio."
    Else
        form = Right(txtNumForm.Text, 2)
        selo = Right(txtNumSelo.Text, 2)
        If form <> selo Then
            MsgBox "Verifique o n�mero do Selo e Formul�rio. Os 2 �ltimos d�gitos n�o s�o iguais."
        End If
    End If


End Sub


Private Sub txtNumSerie_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Status.Panels(1) = "N�mero de s�rie do Selo Fiscal"
End Sub


Private Sub txtPAIDF_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Status.Panels(1) = "Este n� se encontra no canto inferior esquerdo da nota"
End Sub


Private Sub txtSubSerie_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Status.Panels(1) = "N�o preencher subs�rie"
End Sub


