VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmConsulta 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Consulta"
   ClientHeight    =   6960
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7230
   Icon            =   "frmConsulta.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   464
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   482
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fra 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   5535
      Left            =   60
      TabIndex        =   3
      Top             =   960
      Width           =   7095
      Begin VB.Frame Frame1 
         Caption         =   "Consultas"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   1215
         Left            =   90
         TabIndex        =   5
         Top             =   150
         Width           =   6915
         Begin VB.OptionButton optAutoZ 
            Appearance      =   0  'Flat
            Caption         =   "AUTOZ"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   5550
            TabIndex        =   13
            Top             =   780
            Width           =   1020
         End
         Begin VB.OptionButton optDPK 
            Appearance      =   0  'Flat
            Caption         =   "DPK"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   5550
            TabIndex        =   12
            Top             =   360
            Width           =   780
         End
         Begin VB.OptionButton optMensal 
            Appearance      =   0  'Flat
            Caption         =   "M - MENSAL"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   3840
            TabIndex        =   11
            Top             =   780
            Width           =   1320
         End
         Begin VB.OptionButton optDiario 
            Appearance      =   0  'Flat
            Caption         =   "D - DI�RIO"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   270
            Left            =   3840
            TabIndex        =   10
            Top             =   360
            Width           =   1200
         End
         Begin VB.OptionButton optSubst 
            Appearance      =   0  'Flat
            Caption         =   "4 - SUBST. TRIBUT�RIA"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   360
            Left            =   1890
            TabIndex        =   9
            Top             =   720
            Width           =   1350
         End
         Begin VB.OptionButton optIPI 
            Appearance      =   0  'Flat
            Caption         =   "3 - IPI"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   1890
            TabIndex        =   8
            Top             =   360
            Width           =   1050
         End
         Begin VB.OptionButton optICMS 
            Appearance      =   0  'Flat
            Caption         =   "2 - ICMS"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   300
            TabIndex        =   7
            Top             =   780
            Width           =   1050
         End
         Begin VB.OptionButton optNormal 
            Appearance      =   0  'Flat
            Caption         =   "1 - Normal"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   300
            TabIndex        =   6
            Top             =   360
            Width           =   1170
         End
         Begin VB.Line Line1 
            X1              =   3510
            X2              =   3510
            Y1              =   270
            Y2              =   1110
         End
      End
      Begin MSFlexGridLib.MSFlexGrid mfgConsulta 
         Height          =   3930
         Left            =   90
         TabIndex        =   4
         ToolTipText     =   "Duplo Clique p/ Alterar"
         Top             =   1470
         Visible         =   0   'False
         Width           =   6870
         _ExtentX        =   12118
         _ExtentY        =   6932
         _Version        =   393216
         BackColorBkg    =   -2147483633
         HighLight       =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   6630
      Width           =   7230
      _ExtentX        =   12753
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   12700
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      Top             =   810
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsulta.frx":23D2
      PICN            =   "frmConsulta.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdConfirmar 
      Height          =   690
      Left            =   6180
      TabIndex        =   14
      TabStop         =   0   'False
      ToolTipText     =   "Sobre"
      Top             =   60
      Visible         =   0   'False
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsulta.frx":30C8
      PICN            =   "frmConsulta.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmConsulta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim tam As Integer

Private Sub cmdConfirmar_Click()

    Call Aguardar
    mfgConsulta.Visible = False
    Call vVB_Generica_001.ExcluiBind(vBanco)
    Call Criar_Cursor(vBanco, "vCursor")
    If optNormal.Value = True Then
        vBanco.Parameters.Add "vTipoCont", 1, 1
        vBanco.Parameters.Add "vTipoLanc", "1", 1
        vBanco.Parameters.Add "vDivisao", "1", 1
        vBanco.Parameters.Add "vFlag", 1, 1
        
    ElseIf optSubst.Value = True Then
        vBanco.Parameters.Add "vTipoCont", 4, 1
        vBanco.Parameters.Add "vTipoLanc", "1", 1
        vBanco.Parameters.Add "vDivisao", "1", 1
        vBanco.Parameters.Add "vFlag", 1, 1
     
    ElseIf optICMS.Value = True Then
        vBanco.Parameters.Add "vTipoCont", 2, 1
        vBanco.Parameters.Add "vTipoLanc", "1", 1
        vBanco.Parameters.Add "vDivisao", "1", 1
        vBanco.Parameters.Add "vFlag", 1, 1
    
    ElseIf optIPI.Value = True Then
        vBanco.Parameters.Add "vTipoCont", 3, 1
        vBanco.Parameters.Add "vTipoLanc", "1", 1
        vBanco.Parameters.Add "vDivisao", "1", 1
        vBanco.Parameters.Add "vFlag", 1, 1
    
    ElseIf optDiario.Value = True Then
        vBanco.Parameters.Add "vTipoLanc", "D", 1
        vBanco.Parameters.Add "vTipoCont", 1, 1
        vBanco.Parameters.Add "vDivisao", "1", 1
        vBanco.Parameters.Add "vFlag", 2, 1
    
    ElseIf optMensal.Value = True Then
        vBanco.Parameters.Add "vTipoLanc", "M", 1
        vBanco.Parameters.Add "vTipoCont", 1, 1
        vBanco.Parameters.Add "vDivisao", "1", 1
        vBanco.Parameters.Add "vFlag", 2, 1
    
    ElseIf optDPK.Value = True Then
        vBanco.Parameters.Add "vDivisao", "D", 1
        vBanco.Parameters.Add "vTipoLanc", "1", 1
        vBanco.Parameters.Add "vTipoCont", 1, 1
        vBanco.Parameters.Add "vFlag", 3, 1
    
    ElseIf optAutoZ.Value = True Then
        vBanco.Parameters.Add "vDivisao", "Z", 1
        vBanco.Parameters.Add "vTipoLanc", "1", 1
        vBanco.Parameters.Add "vTipoCont", 1, 1
        vBanco.Parameters.Add "vFlag", 3, 1
            
    End If
    
    vSql = "producao.PCK_CAD670.pr_select(:vCursor,:vTipoLanc,:vTipoCont,:vDivisao,:vFlag)"
    Unload frmAguardar
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.ExcluiBind(vBanco)
        End
    Else
        Set vObjOracle = vParameters("vCursor").Value
    End If
    If Not vObjOracle.EOF Then
        Call vVB_Generica_001.CarregaGridTabela(mfgConsulta, vObjOracle, 7)
        mfgConsulta.Visible = True
    Else
        Call vVB_Generica_001.Informar("Nenhum dado encontrado!!")
    End If

End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    
    Me.Top = 0
    Me.Left = 0
    
End Sub

Private Sub mfgConsulta_DblClick()

    With mfgConsulta
    If .Text = "" Then
        Exit Sub
    End If
    Call Aguardar
    Load frmCadastro
    Call frmCadastro.Form_Activate
    FG = 0
    .Col = 1
    Call vVB_Generica_001.ExcluiBind(vBanco)
    Set vObjOracle = vVB_Generica_001.TabelaNaturezaOperacao(vBanco, .Text)
    frmCadastro.txtNOP.Text = vObjOracle.Fields(1)
'    For i = 0 To frmCadastro.cboNOP.ListCount - 1
'        frmCadastro.cboNOP.ListIndex = i
'        If Left(frmCadastro.cboNOP.Text, 3) = .Text Then
'            j = i
'            Exit For
'        End If
'    Next i
    .Col = 3
    For i = 0 To frmCadastro.cboTpContabil.ListCount - 1
        frmCadastro.cboTpContabil.ListIndex = i
        If Left(frmCadastro.cboTpContabil.Text, 1) = .Text Then
            Exit For
        End If
    Next
    .Col = 4
    frmCadastro.txtContaDebito.Text = .Text
    .Col = 5
    frmCadastro.txtContaCredito.Text = .Text
    .Col = 6
    For i = 0 To frmCadastro.cboLancamento.ListCount - 1
        frmCadastro.cboLancamento.ListIndex = i
        If Left(frmCadastro.cboLancamento.Text, 1) = .Text Then
            Exit For
        End If
    Next
    .Col = 2
    Unload frmAguardar
    frmCadastro.Show
    For i = 0 To frmCadastro.cboDivisao.ListCount - 1
        frmCadastro.cboDivisao.ListIndex = i
        FG = 2
        If Left(frmCadastro.cboDivisao.Text, 1) = .Text Then
            Exit For
        End If
    Next
    Unload Me
    Call frmCadastro.txtContaDebito_LostFocus
    Call frmCadastro.txtContaCredito_LostFocus
    End With
    FG = 1
    If frmCadastro.cboDivisao.Enabled = True Then
        Call frmCadastro.Trava_Destrava_Combo
    End If
    frmCadastro.cboLancamento.SetFocus
    
End Sub

Private Sub optAutoZ_Click()

    Call cmdConfirmar_Click
    
End Sub

Private Sub optDiario_Click()

    Call cmdConfirmar_Click
    
End Sub

Private Sub optDPK_Click()

    Call cmdConfirmar_Click
    
End Sub

Private Sub optICMS_Click()

    Call cmdConfirmar_Click
    
End Sub

Private Sub optIPI_Click()

    Call cmdConfirmar_Click
    
End Sub

Private Sub optMensal_Click()

    Call cmdConfirmar_Click
    
End Sub

Private Sub optNormal_Click()

    Call cmdConfirmar_Click
    
End Sub

Private Sub optSubst_Click()

    Call cmdConfirmar_Click
    
End Sub
