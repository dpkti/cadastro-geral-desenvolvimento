create or replace package producao.PCK_CAD670 is

  -- Author  : CARLOS.MOURA
  -- Created : 24/9/2004 15:14:58
  -- Purpose : 
  
  type tp_cursor is ref cursor;
  
  PROCEDURE pr_insert_update_cad(pm_natureza in contatab.nop_contabilizacao.cod_natureza%type,
                                 pm_divisao in contatab.nop_contabilizacao.divisao%type,
                                 pm_contab in contatab.nop_contabilizacao.tipo_contab%type,
                                 pm_debito in contatab.nop_contabilizacao.num_conta_debito%type,
                                 pm_credito in contatab.nop_contabilizacao.num_conta_credito%type,
                                 pm_lancamento in contatab.nop_contabilizacao.tipo_lancto%type);
                                 
  PROCEDURE pr_select_desc_conta(p_cursor in out tp_cursor,
                                 pm_conta in number);    
                                 
  PROCEDURE pr_select(p_cursor in out tp_cursor,
                      pm_lancamento in contatab.nop_contabilizacao.tipo_lancto%type,
                      pm_contab in contatab.nop_contabilizacao.tipo_contab%type,
                      pm_divisao in contatab.nop_contabilizacao.divisao%type,     
                      pm_flag in number);   
                      
  PROCEDURE pr_select_cadastro( p_cursor in out tp_cursor,
                                pm_natureza in contatab.nop_contabilizacao.cod_natureza%type,
                                pm_divisao in contatab.nop_contabilizacao.divisao%type,
                                pm_contab in contatab.nop_contabilizacao.tipo_contab%type);                                                            
                                  
  PROCEDURE pr_sel_contas(p_cursor in out tp_cursor);

end ;
/
create or replace package body producao.PCK_CAD670 is

  PROCEDURE pr_insert_update_cad(pm_natureza in contatab.nop_contabilizacao.cod_natureza%type,
                                 pm_divisao in contatab.nop_contabilizacao.divisao%type,
                                 pm_contab in contatab.nop_contabilizacao.tipo_contab%type,
                                 pm_debito in contatab.nop_contabilizacao.num_conta_debito%type,
                                 pm_credito in contatab.nop_contabilizacao.num_conta_credito%type,
                                 pm_lancamento in contatab.nop_contabilizacao.tipo_lancto%type)
                                 
  is 
  begin
  
    Update   contatab.nop_contabilizacao set 
             num_conta_debito = pm_debito,
             num_conta_credito = pm_credito,
             tipo_lancto=pm_lancamento
             where cod_natureza = pm_natureza and 
             divisao = pm_divisao and 
             tipo_contab = pm_contab;
             
             If sql%notfound then
                 Insert into contatab.nop_contabilizacao
                 (cod_natureza,divisao, tipo_contab, num_conta_debito, num_conta_credito, tipo_lancto) 
                 values (pm_natureza, pm_divisao, pm_contab, pm_debito,
                         pm_credito, pm_lancamento);
             end if;
             
             commit;
             
  exception
 
     When others then 
         Rollback; 
        
  end pr_insert_update_cad;
  
  PROCEDURE pr_select_desc_conta(p_cursor in out tp_cursor,
                                 pm_conta in number)
  is
  begin
  open p_cursor for
    Select desc_conta 
    from contatab.plano_contas 
    where num_conta =pm_conta;  
  
  end pr_select_desc_conta;   
  
  PROCEDURE pr_select(p_cursor in out tp_cursor,
                      pm_lancamento in contatab.nop_contabilizacao.tipo_lancto%type,
                      pm_contab in contatab.nop_contabilizacao.tipo_contab%type,
                      pm_divisao in contatab.nop_contabilizacao.divisao%type,     
                      pm_flag in number)                           
    
  is
  begin
      if pm_flag=1 then
        open p_cursor for
            select cod_natureza, divisao, tipo_contab,num_conta_debito,num_conta_credito, 
               tipo_lancto from contatab.nop_contabilizacao
               where tipo_contab = pm_contab
               order by cod_natureza; 
               
      elsif pm_flag=2 then
        open p_cursor for
            select cod_natureza, divisao, tipo_contab,num_conta_debito,num_conta_credito, 
               tipo_lancto from contatab.nop_contabilizacao
               where tipo_lancto = pm_lancamento
               order by cod_natureza; 
      else 
        open p_cursor for
            select cod_natureza, divisao, tipo_contab,num_conta_debito,num_conta_credito, 
               tipo_lancto from contatab.nop_contabilizacao
               where divisao = pm_divisao
               order by cod_natureza;
               
      end if;    
  
  end pr_select;
  
  PROCEDURE pr_select_cadastro( p_cursor in out tp_cursor,
                                pm_natureza in contatab.nop_contabilizacao.cod_natureza%type,
                                pm_divisao in contatab.nop_contabilizacao.divisao%type,
                                pm_contab in contatab.nop_contabilizacao.tipo_contab%type)
                                
  is 
  begin
  open p_cursor for 
      select * from contatab.nop_contabilizacao
      where cod_natureza=pm_natureza and 
      divisao=pm_divisao and 
      tipo_contab=pm_contab;
      
  end pr_select_cadastro;
  
  PROCEDURE pr_sel_contas(p_cursor in out tp_cursor)
  is
  begin
  open p_cursor for 
      select NUM_CONTA Conta, DESC_CONTA Descri��o
      from contatab.plano_contas;
   
  end pr_sel_contas;
             
end ;
/
