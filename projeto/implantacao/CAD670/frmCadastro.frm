VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form frmCadastro 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cadastro"
   ClientHeight    =   5010
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6525
   Icon            =   "frmCadastro.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   334
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   435
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fra 
      Caption         =   "Dados"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   3615
      Left            =   60
      TabIndex        =   14
      Top             =   960
      Width           =   6405
      Begin VB.TextBox txtNOP 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1380
         TabIndex        =   21
         TabStop         =   0   'False
         Top             =   330
         Width           =   4320
      End
      Begin VB.TextBox txtDescCredito 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2910
         TabIndex        =   8
         Top             =   3030
         Width           =   2790
      End
      Begin VB.TextBox txtDescDebito 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2910
         TabIndex        =   6
         Top             =   2490
         Width           =   2790
      End
      Begin VB.TextBox txtContaCredito 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1380
         MaxLength       =   12
         TabIndex        =   7
         Top             =   3030
         Width           =   1440
      End
      Begin VB.TextBox txtContaDebito 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1380
         MaxLength       =   12
         TabIndex        =   5
         Top             =   2490
         Width           =   1440
      End
      Begin VB.TextBox txtLivro 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3420
         TabIndex        =   2
         Top             =   877
         Width           =   2280
      End
      Begin VB.ComboBox cboTpContabil 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         ItemData        =   "frmCadastro.frx":23D2
         Left            =   1380
         List            =   "frmCadastro.frx":23E2
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   1410
         Width           =   3285
      End
      Begin VB.ComboBox cboLancamento 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         ItemData        =   "frmCadastro.frx":2422
         Left            =   1380
         List            =   "frmCadastro.frx":242C
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   1950
         Width           =   1635
      End
      Begin VB.ComboBox cboDivisao 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         ItemData        =   "frmCadastro.frx":2448
         Left            =   1380
         List            =   "frmCadastro.frx":2452
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   870
         Width           =   1965
      End
      Begin Bot�o.cmd cmdCodNOP 
         Height          =   360
         Left            =   5850
         TabIndex        =   0
         ToolTipText     =   "Selecionar C�d da NOP"
         Top             =   310
         Width           =   450
         _ExtentX        =   794
         _ExtentY        =   635
         BTYPE           =   3
         TX              =   "..."
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadastro.frx":246A
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdCtaCred 
         Height          =   360
         Left            =   5850
         TabIndex        =   22
         ToolTipText     =   "Selecionar Conta"
         Top             =   3015
         Width           =   450
         _ExtentX        =   794
         _ExtentY        =   635
         BTYPE           =   3
         TX              =   "..."
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadastro.frx":2486
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdCtaDeb 
         Height          =   360
         Left            =   5850
         TabIndex        =   23
         ToolTipText     =   "Selecionar Conta"
         Top             =   2475
         Width           =   450
         _ExtentX        =   794
         _ExtentY        =   635
         BTYPE           =   3
         TX              =   "..."
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadastro.frx":24A2
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label Label5 
         Appearance      =   0  'Flat
         Caption         =   "Lan�amento"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   300
         TabIndex        =   20
         Top             =   2010
         Width           =   1230
      End
      Begin VB.Label Label4 
         Appearance      =   0  'Flat
         Caption         =   "Conta Cr�dito"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   180
         TabIndex        =   19
         Top             =   3120
         Width           =   1230
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         Caption         =   "Conta D�bito"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   210
         TabIndex        =   18
         Top             =   2550
         Width           =   1230
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         Caption         =   "Tipo Cont�bil"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   210
         TabIndex        =   17
         Top             =   1470
         Width           =   1230
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         Caption         =   "Divis�o"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   750
         TabIndex        =   16
         Top             =   930
         Width           =   1230
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         Caption         =   "C�d. da NOP."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   210
         TabIndex        =   15
         Top             =   390
         Width           =   1230
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   9
      Top             =   4680
      Width           =   6525
      _ExtentX        =   11509
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   11456
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   10
      TabStop         =   0   'False
      Top             =   810
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   11
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadastro.frx":24BE
      PICN            =   "frmCadastro.frx":24DA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdLimpar 
      Height          =   690
      Left            =   840
      TabIndex        =   12
      TabStop         =   0   'False
      ToolTipText     =   "Limpar Campos"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadastro.frx":31B4
      PICN            =   "frmCadastro.frx":31D0
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdConfirmar 
      Height          =   690
      Left            =   1650
      TabIndex        =   13
      TabStop         =   0   'False
      ToolTipText     =   "Gravar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadastro.frx":3AAA
      PICN            =   "frmCadastro.frx":3AC6
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmCadastro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim vControl

Private Function Verifica_Campos() As Boolean

    For Each vControl In Me.Controls
        If TypeOf vControl Is TextBox _
        Or TypeOf vControl Is ComboBox Then
            If vControl.Name <> txtDescDebito.Name And _
            vControl.Name <> txtDescCredito.Name Then
                If Trim(vControl.Text) = "" Then
                    Verifica_Campos = False
                    Exit Function
                End If
            End If
        End If
    Next
    Verifica_Campos = True
        
End Function
Public Sub verifica_formulario()
    
    fl = 0
    If frmCadastro.txtNOP.Text = "" Then
        Call vVB_Generica_001.Informar("Preencha o Campo com o C�digo de Opera��o!")
        frmCadastro.txtNOP.SetFocus
        fl = 1
    
    ElseIf frmCadastro.cboDivisao.Text = "" Then
        Call vVB_Generica_001.Informar("Preencha o Campo com a Divis�o!")
        frmCadastro.cboDivisao.SetFocus
        fl = 1
    
    ElseIf frmCadastro.cboTpContabil.Text = "" Then
        Call vVB_Generica_001.Informar("Preencha o Campo com o Tipo Cont�bil!")
        frmCadastro.cboTpContabil.SetFocus
        fl = 1
    ElseIf frmCadastro.txtContaDebito.Text = "" Then
        Call vVB_Generica_001.Informar("Preencha o Campo com a Conta D�bito!")
        frmCadastro.txtContaDebito.SetFocus
        fl = 1
    ElseIf frmCadastro.txtContaCredito.Text = "" Then
        Call vVB_Generica_001.Informar("Preencha o Campo com a Conta Cr�dito!")
        frmCadastro.txtContaCredito.SetFocus
        fl = 1
    ElseIf frmCadastro.cboLancamento.Text = "" Then
        Call vVB_Generica_001.Informar("Preencha o Campo com o Tipo de Lan�amento!")
        frmCadastro.cboLancamento.SetFocus
        fl = 1
    End If
    
End Sub

Public Sub Trava_Destrava_Combo()

    For Each vControl In frmCadastro.Controls
        If TypeOf vControl Is ComboBox _
        And vControl.Name <> "cboLancamento" Then
            If vControl.Enabled = True Then
                vControl.Enabled = False
            Else
                vControl.Enabled = True
            End If
        End If
    Next
        
End Sub

Private Sub cboDivisao_Click()

    Call cboTpContabil_Click

End Sub

Private Sub cboDivisao_KeyPress(KeyAscii As Integer)

    If KeyAscii = 13 Then
        cboTpContabil.SetFocus
    End If

End Sub

Private Sub cboLancamento_KeyPress(KeyAscii As Integer)

    If KeyAscii = 13 Then
        txtContaDebito.SetFocus
    End If

End Sub


'Private Sub cboNOP_Click()
'
'    If Trim(cboNOP) = "" Then
'        Exit Sub
'    End If
'    Set vObjOracle = vVB_Generica_001.TabelaNaturezaOperacao(vBanco, Mid(cboNOP.Text, 1, 3))
'    If vObjOracle!fl_livro = "E" Then
'        txtLivro.Text = vObjOracle!fl_livro & " - Entrada"
'    ElseIf vObjOracle!fl_livro = "S" Then
'        txtLivro.Text = vObjOracle!fl_livro & " - Sa�da"
'    Else
'        txtLivro.Text = vObjOracle!fl_livro & " - N�o Contabiliza"
'    End If
'    Call cboTpContabil_Click
'
'End Sub

Private Sub cboNOP_KeyPress(KeyAscii As Integer)

    If KeyAscii = 13 Then
        cboDivisao.SetFocus
    End If
    
End Sub

Private Sub cboTpContabil_Click()

    If FG = 0 Then
        Exit Sub
    End If
    If Trim(cboTpContabil) <> "" And Trim(cboDivisao) <> "" _
    And Trim(txtNOP) <> "" Then 'And Trim(cboLancamento) <> ""
        Call vVB_Generica_001.ExcluiBind(vBanco)
        Call Criar_Cursor(vBanco, "vCursor")
        vBanco.Parameters.Add "vNatureza", Mid(txtNOP.Text, 1, 3), 1
        vBanco.Parameters.Add "vDivisao", Mid(cboDivisao.Text, 1, 1), 1
        vBanco.Parameters.Add "vTipo", Val(Mid(cboTpContabil.Text, 1, 1)), 1
        vSql = "producao.PCK_CAD670.pr_select_cadastro(:vCursor,:vNatureza,:vDivisao,:vTipo)"
        If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
            End
        Else
            Set vObjOracle = vParameters("vCursor").Value
        End If
        If Not vObjOracle.EOF Then
            If FG <> 2 Then
                If vVB_Generica_001.Perguntar("Cadastro j� existente.Deseja fazer altera��es?") = 7 Then
                    Call cmdLimpar_Click
                    Exit Sub
                End If
            End If
            Call Trava_Destrava_Combo
            For i = 1 To 1
                cboLancamento.ListIndex = IIf(vObjOracle!TIPO_LANCTO = "D", 0, 1)
                txtContaDebito.Text = vObjOracle!NUM_CONTA_DEBITO
                txtContaCredito.Text = vObjOracle!NUM_CONTA_CREDITO
                Call txtContaDebito_LostFocus
                Call txtContaCredito_LostFocus
            Next i
        End If
    End If
    
End Sub

Private Sub cboTpContabil_KeyPress(KeyAscii As Integer)

    If KeyAscii = 13 Then
        cboLancamento.SetFocus
    End If

End Sub

Private Sub cmdCodNOP_Click()

    If cboDivisao.Enabled = False Then
        Call cmdLimpar_Click
    End If
    frmConsNOP.Show 1
    
End Sub

Private Sub cmdCodNOP_KeyPress(KeyAscii As Integer)

    If KeyAscii = 13 Then
        Call cmdCodNOP_Click
    End If
    
End Sub

Private Sub cmdConfirmar_Click()

    Call Verifica_Campos
    If Verifica_Campos = False Then
        Call vVB_Generica_001.Informar("Campo(s) em Branco!!")
        Exit Sub
    End If
    If vVB_Generica_001.Perguntar("Confirma os Dados?") = 7 Then
        Exit Sub
    End If
    If fl = 1 Then
        Exit Sub
    End If
    Call vVB_Generica_001.ExcluiBind(vBanco)
    vBanco.Parameters.Add "vNatureza", Mid(txtNOP.Text, 1, 3), 1
    vBanco.Parameters.Add "vDivisao", Mid(cboDivisao.Text, 1, 1), 1
    vBanco.Parameters.Add "vTipo", Val(Mid(cboTpContabil.Text, 1, 1)), 1
    vBanco.Parameters.Add "vDebito", Val(txtContaDebito), 1
    vBanco.Parameters.Add "vCredito", Val(txtContaCredito), 1
    vBanco.Parameters.Add "vLancamento", Mid(cboLancamento.Text, 1, 1), 1
    vSql = "producao.PCK_CAD670.pr_insert_update_cad(:vNatureza,:vDivisao,:vTipo,:vDebito,:vCredito,:vLancamento)"
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        End
    Else
        Call vVB_Generica_001.Informar("Grava��o Realizada com Sucesso!")
        Call Trava_Destrava_Combo
    End If
    Call cmdLimpar_Click
    If cboDivisao.Enabled = False Then
        Call Trava_Destrava_Combo
    End If
    
End Sub

Private Sub cmdCtaCred_Click()

    flDebito = False
    frmContas.Show 1
    
End Sub

Private Sub cmdCtaDeb_Click()

    flDebito = True
    frmContas.Show 1
    txtContaCredito.SetFocus
    
End Sub

Private Sub cmdLimpar_Click()

    txtNOP.Text = ""
    cboDivisao.ListIndex = -1
    txtLivro.Text = ""
    cboTpContabil.ListIndex = -1
    txtContaDebito.Text = ""
    txtContaCredito.Text = ""
    txtDescCredito.Text = ""
    txtDescDebito.Text = ""
    cboLancamento.ListIndex = -1
    If cboDivisao.Enabled = False Then
        Call Trava_Destrava_Combo
    End If
    cmdCodNOP.SetFocus
    
End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub

Public Sub Form_Activate()

    '------Carrega Natureza de Opera��o--------
'    If Trim(txtNOP) = "" Then
'        Call vVB_Generica_001.ExcluiBind(vBanco)
'        Set vObjOracle = vVB_Generica_001.TabelaNaturezaOperacao(vBanco)
'        Call vVB_Generica_001.PreencheComboList(vObjOracle, cboNOP, "NAT. OPERA��O")
    '    ------Carrega Divis�o--------------
    '    Set vObjOracle = Nothing
    '    Call vVB_Generica_001.ExcluiBind(vBanco)
    '    Set vObjOracle = vVB_Generica_001.TabelaDivisao(vBanco)
    '    Call vVB_Generica_001.PreencheComboList(vObjOracle, cboDivisao, "DIVIS�O", "DESC. DIVIS�O")
    'End If

End Sub

Private Sub Form_Load()
    
    Me.Top = 0
    Me.Left = 0
    
End Sub

Private Sub txtContaCredito_Change()

    txtDescCredito = ""
    
End Sub

Private Sub txtContaCredito_GotFocus()

    Call vVB_Generica_001.SelecionaCampo(txtContaCredito)
    
End Sub

Private Sub txtContaCredito_KeyPress(KeyAscii As Integer)

    If KeyAscii = 13 Then
        Call txtContaCredito_LostFocus
        If Trim(txtDescCredito) <> "" Then
            Call cmdConfirmar_Click
        End If
    End If
    KeyAscii = vVB_Generica_001.Numero(KeyAscii)

End Sub

Public Sub txtContaCredito_LostFocus()

    If Trim(txtContaCredito) = "" Then
        Exit Sub
    End If
    Call vVB_Generica_001.ExcluiBind(vBanco)
    Call Criar_Cursor(vBanco, "vCursor")
    vBanco.Parameters.Add "vCredito", Val(txtContaCredito), 1
    vSql = "producao.PCK_CAD670.pr_select_desc_conta(:vCursor,:vCredito)"
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.ExcluiBind(vBanco)
        End
    Else
        Set vObjOracle = vParameters("vCursor").Value
    End If
    If vObjOracle.RecordCount = 0 Then
        Call vVB_Generica_001.Informar("Este n�mero de Conta n�o � v�lido.Tente digitar novamente!")
        txtContaCredito.Text = ""
        txtContaCredito.SetFocus
        txtDescCredito.Text = ""
    Else
        txtDescCredito.Text = vObjOracle!desc_conta
    End If
    
End Sub

Private Sub txtContaDebito_Change()

    txtDescDebito = ""
    
End Sub

Private Sub txtContaDebito_GotFocus()

    Call vVB_Generica_001.SelecionaCampo(txtContaDebito)
    
End Sub

Private Sub txtContaDebito_KeyPress(KeyAscii As Integer)

    If KeyAscii = 13 Then
        txtContaCredito.SetFocus
    End If
    KeyAscii = vVB_Generica_001.Numero(KeyAscii)

End Sub

Public Sub txtContaDebito_LostFocus()

    If Trim(txtContaDebito) = "" Then
        Exit Sub
    End If
    Call vVB_Generica_001.ExcluiBind(vBanco)
    Call Criar_Cursor(vBanco, "vCursor")
    vBanco.Parameters.Add "vDebito", Val(txtContaDebito), 1
    vSql = "producao.PCK_CAD670.pr_select_desc_conta(:vCursor,:vDebito)"
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.ExcluiBind(vBanco)
        End
    Else
        Set vObjOracle = vParameters("vCursor").Value
    End If
    If vObjOracle.RecordCount = 0 Then
        Call vVB_Generica_001.Informar("Este n�mero de Conta n�o � v�lido.Tente digitar novamente!")
        txtContaDebito.Text = ""
        txtContaDebito.SetFocus
        txtDescDebito.Text = ""
    Else
        txtDescDebito.Text = vObjOracle!desc_conta
    End If
    
End Sub

Private Sub txtNOP_Change()

    If Trim(txtNOP) = "" Then
        Exit Sub
    End If
    Set vObjOracle = vVB_Generica_001.TabelaNaturezaOperacao(vBanco, Mid(txtNOP.Text, 1, 3))
    If vObjOracle!fl_livro = "E" Then
        txtLivro.Text = vObjOracle!fl_livro & " - Entrada"
    ElseIf vObjOracle!fl_livro = "S" Then
        txtLivro.Text = vObjOracle!fl_livro & " - Sa�da"
    Else
        txtLivro.Text = vObjOracle!fl_livro & " - N�o Contabiliza"
    End If
    Call cboTpContabil_Click
    
End Sub
