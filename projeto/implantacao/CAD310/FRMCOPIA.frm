VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FRMCOPIA 
   Caption         =   "CAD310 - Duplica��o de UF"
   ClientHeight    =   3090
   ClientLeft      =   3750
   ClientTop       =   3645
   ClientWidth     =   5955
   LinkTopic       =   "Form1"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   3090
   ScaleWidth      =   5955
   Begin VB.Frame Frame1 
      Height          =   2244
      Left            =   144
      TabIndex        =   0
      Top             =   144
      Width           =   5628
      Begin VB.ComboBox CBODESTINO 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   10.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   3960
         TabIndex        =   5
         Top             =   288
         Width           =   948
      End
      Begin VB.ComboBox CBOORIGEM 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   10.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1872
         TabIndex        =   4
         Top             =   288
         Width           =   948
      End
      Begin Threed.SSPanel SSOBS 
         Height          =   810
         Left            =   150
         TabIndex        =   3
         Top             =   1290
         Width           =   5340
         _Version        =   65536
         _ExtentX        =   9419
         _ExtentY        =   1418
         _StockProps     =   15
         ForeColor       =   16711680
         BackColor       =   12632256
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   10.42
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Alignment       =   0
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         Caption         =   "para"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   10.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   228
         Left            =   2880
         TabIndex        =   2
         Top             =   360
         Width           =   1020
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Caption         =   "Copiar de"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   10.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   228
         Left            =   576
         TabIndex        =   1
         Top             =   360
         Width           =   1236
      End
   End
   Begin Threed.SSCommand SSCommand2 
      Height          =   516
      Left            =   5256
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   2520
      Width           =   516
      _Version        =   65536
      _ExtentX        =   910
      _ExtentY        =   910
      _StockProps     =   78
      ForeColor       =   -2147483640
      Picture         =   "FRMCOPIA.frx":0000
   End
   Begin Threed.SSCommand SSCommand1 
      Height          =   516
      Left            =   4680
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   2520
      Width           =   516
      _Version        =   65536
      _ExtentX        =   910
      _ExtentY        =   910
      _StockProps     =   78
      ForeColor       =   -2147483640
      Picture         =   "FRMCOPIA.frx":031A
   End
   Begin VB.Label LBL_COPIANDO 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   228
      Left            =   144
      TabIndex        =   8
      Top             =   2664
      Width           =   4332
   End
End
Attribute VB_Name = "FRMCOPIA"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Sub ATUALIZA_UF()
    CBOORIGEM.Clear
    CBODESTINO.Clear
    S_SELECT1 = "SELECT DISTINCT COD_UF_ORIGEM FROM VENDAS.UF_DPK"
    S_SELECT2 = "SELECT DISTINCT COD_UF FROM UF"
    Set OBJORIGEM = oradatabase.dbCreateDynaset(S_SELECT1, 0&)
    Set OBJDESTINO = oradatabase.dbCreateDynaset(S_SELECT2, 0&)
    While Not OBJORIGEM.EOF
        CBOORIGEM.AddItem OBJORIGEM.Fields(0)
        OBJORIGEM.MoveNext
    Wend
    While Not OBJDESTINO.EOF
        CBODESTINO.AddItem OBJDESTINO.Fields(0)
        OBJDESTINO.MoveNext
    Wend
End Sub

Private Sub SSPanel1_Click()

End Sub


Private Sub Form_Load()
    Dim OBJORIGEM As Object
    Dim OBJDESTINO As Object
    SSOBS.Caption = " OBS.: " & Chr(13) & _
                    " 1) Os registros existentes n�o ser�o apagados " & Chr(13) & _
                    " 2) Os registros duplicados ser�o sobrepostos"
    ATUALIZA_UF
End Sub
Private Sub SSCommand1_Click()
    
    Dim OBJ_ORIGEM As Object, OBJ_CHECAR As Object
    
    If CBOORIGEM = CBODESTINO Then
        MsgBox "Origem n�o pode ser igual a Destino!"
        Exit Sub
    End If
    
    A = MsgBox("Confirma duplica��o de " & CBOORIGEM & " para " & CBODESTINO & "?", 36, "CAD310")
    
    If A = 6 Then
        
        FRMCOPIA.MousePointer = 11
        frmManual.MousePointer = 11
        
        oradatabase.Parameters.Remove "CBOORI": oradatabase.Parameters.Add "CBOORI", CBOORIGEM, 1
        oradatabase.Parameters.Remove "CBODES": oradatabase.Parameters.Add "CBODES", CBODESTINO, 1
        oradatabase.Parameters.Remove "USU":    oradatabase.Parameters.Add "USU", vLog_Usu, 1
        
        LBL_COPIANDO.Caption = "Copiando dados de " & CBOORIGEM & " para " & CBODESTINO & " ! Aguarde..."
        Me.Refresh
        S_SQL1 = "BEGIN PRODUCAO.PCK_CAD310.PR_COPIA_DESC(:CBOORI,:CBODES,:USU); " & _
                 "COMMIT; " & _
                 "EXCEPTION WHEN OTHERS THEN ROLLBACK; " & _
                 ":cod_errora := SQLCODE;:txt_errora := SQLERRM; END;"
        
        With oradatabase.Parameters
            .Remove "cod_errora":       .Add "cod_errora", 0, 2
            .Remove "txt_errora":       .Add "txt_errora", "", 2
        End With
        
        oradatabase.ExecuteSQL S_SQL1
        
        'VERIFICA ERROS
        If IIf(IsNull(oradatabase.Parameters("cod_errora")), 0, Val(oradatabase.Parameters("cod_errora"))) <> 0 Then
            MsgBox oradatabase.Parameters("txt_errora") & ".Ligue para o Depto de Sistemas"
            Exit Sub
        Else
            LBL_COPIANDO.Caption = ""
            MsgBox "Duplica��o de " & CBOORIGEM & " para " & CBODESTINO & " conclu�da com sucesso!"
            ATUALIZA_UF
            Me.MousePointer = 0
            frmManual.MousePointer = 0
        End If
    
    Else
        Exit Sub
    End If

End Sub
Private Sub SSCommand2_Click()
    FRMCOPIA.Hide
End Sub


