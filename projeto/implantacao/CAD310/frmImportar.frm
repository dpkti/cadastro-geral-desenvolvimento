VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Begin VB.Form frmImportar 
   Caption         =   "Importar Arquivo"
   ClientHeight    =   6165
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8655
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   6165
   ScaleWidth      =   8655
   Begin Threed.SSFrame SSFrame2 
      Height          =   5895
      Left            =   90
      TabIndex        =   0
      Top             =   60
      Width           =   8460
      _Version        =   65536
      _ExtentX        =   14922
      _ExtentY        =   10398
      _StockProps     =   14
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin VB.CommandButton cmdVoltar 
         Height          =   630
         Left            =   7515
         Picture         =   "frmImportar.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   8
         ToolTipText     =   "Voltar"
         Top             =   4950
         Width           =   690
      End
      Begin VB.CommandButton cmdGravar 
         Height          =   630
         Left            =   6255
         Picture         =   "frmImportar.frx":030A
         Style           =   1  'Graphical
         TabIndex        =   7
         ToolTipText     =   "Gravar"
         Top             =   4950
         Width           =   690
      End
      Begin VB.CommandButton cmdgeraLog 
         Height          =   630
         Left            =   5370
         Picture         =   "frmImportar.frx":0BD4
         Style           =   1  'Graphical
         TabIndex        =   6
         ToolTipText     =   "Gera Arq.LOG"
         Top             =   4950
         Width           =   690
      End
      Begin MSGrid.Grid grdArquivo 
         Height          =   2940
         Left            =   285
         TabIndex        =   4
         Top             =   1815
         Width           =   7950
         _Version        =   65536
         _ExtentX        =   14023
         _ExtentY        =   5186
         _StockProps     =   77
         BackColor       =   16777215
         Cols            =   6
      End
      Begin Threed.SSFrame SSFrame1 
         Height          =   1305
         Left            =   255
         TabIndex        =   1
         Top             =   255
         Width           =   7980
         _Version        =   65536
         _ExtentX        =   14076
         _ExtentY        =   2302
         _StockProps     =   14
         Caption         =   "Arquivo"
         ForeColor       =   -2147483635
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Begin VB.CommandButton cmdLayout 
            Height          =   630
            Left            =   7110
            Picture         =   "frmImportar.frx":0EDE
            Style           =   1  'Graphical
            TabIndex        =   9
            ToolTipText     =   "Lay-out do Arquivo"
            Top             =   255
            Width           =   690
         End
         Begin Threed.SSCommand scmdImportar 
            Height          =   660
            Left            =   375
            TabIndex        =   2
            ToolTipText     =   "Abrir Arquivo"
            Top             =   420
            Width           =   690
            _Version        =   65536
            _ExtentX        =   1217
            _ExtentY        =   1164
            _StockProps     =   78
            Picture         =   "frmImportar.frx":11E8
         End
         Begin MSComDlg.CommonDialog controle 
            Left            =   120
            Top             =   270
            _ExtentX        =   847
            _ExtentY        =   847
            _Version        =   393216
            DefaultExt      =   "*.prn"
            InitDir         =   "c:\arquivos-alethea\cad310"
         End
         Begin VB.Label lblArquivo 
            BorderStyle     =   1  'Fixed Single
            Height          =   330
            Left            =   1170
            TabIndex        =   3
            Top             =   750
            Width           =   5310
         End
      End
      Begin VB.Label lblMsg 
         AutoSize        =   -1  'True
         Caption         =   "Mensagem"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   195
         Left            =   345
         TabIndex        =   5
         Top             =   5175
         Visible         =   0   'False
         Width           =   915
      End
   End
End
Attribute VB_Name = "frmImportar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdgeraLog_Click()

Dim I As Integer        'Ponteiro de linha do grid
Dim J As Long           'Ponteiro de caracter no campo observa��o
Dim vTamObs As Long     'Guarda a qtd. de caracteres da coluna Observa��o
Dim vObs As String      'Guarda o conte�do da coluna de Observa��es
Dim vUFOri As String     'Guarda o c�digo do fornecedor para impress�o
Dim vUFDes As String     'Guarda o c�digo de f�brica para impress�o
Dim vDPK As String      'Guarda o c�digo DPK para impress�o
Dim vSinal As String      'Guarda o c�digo DPK para impress�o
    
    
If MsgBox("Confirma gera��o do arquivo de log?", vbYesNo, "CAD310") = vbYes Then

    'Gera arquivo de log com o nome LODDMMAA.TXT
    Open "C:\TEMP\LogCAD310_" & Mid(vDt_Cad, 1, 2) & _
                        Mid(vDt_Cad, 4, 2) & Mid(vDt_Cad, 7, 2) & ".txt" For Output As #1
    'Open "C:\arquivos-alethea\CAD310\LOG\Lo" & Mid(vDt_Cad, 1, 2) & _
                        Mid(vDt_Cad, 4, 2) & Mid(vDt_Cad, 7, 2) & ".txt" For Output As #1
    
    Print #1, "LOG DO ARQUIVO: " & vNomeArq
    Print #1, ""
   'Print #1, "DPK   Forn  Fabrica             Observa��es"
   'Print #1, "----- ----  ------------------- ------------------------------------------------------"
    
    'carlosj 09/08/2012 CiT - [ISA 192]
    'Print #1, "UF Ori UF Des DPK   Desconto Observa��es"
    'Print #1, "------ ------ ----- -------- ---------------------------------------------------"
    Print #1, "UF Ori UF Des DPK                Desconto Observa��es"
    Print #1, "------ ------ ------------------ -------- ---------------------------------------------------"
    
    
    'Pega linha a linha do grid para gravar as que cont�m campo de Observa��o <> ""
    For I = 1 To grdArquivo.Rows - 1
        
        grdArquivo.Row = I
        
        grdArquivo.Col = 1
        vUFOri = Trim(grdArquivo.Text)      'Pega a UF Origem
        grdArquivo.Col = 2
        vUFDes = Trim(grdArquivo.Text)      'Pega a UF Destino
        grdArquivo.Col = 3
        vDPK = Trim(grdArquivo.Text)        'Pega DPK
        grdArquivo.Col = 4
        vDesc = Trim(grdArquivo.Text)       'Pega desconto
        grdArquivo.Col = 5
        vObs = Trim(grdArquivo.Text)        'Pega as Observa��es
        
        'Se a coluna de Observa��o n�o estiver em branco
        If vObs <> "" Then
            
            'Zero ponteiro que vai me indicar que caracter no campo obs. estou lendo
            J = 0
            'Pego o tamanho da observa��o para esta linha
            vTamObs = CLng(Len(vObs))
            
            'Se o campo de observa��o contiver mais de 49 caracteres, o arquivo de log ter�
            'que conter mais de 1 linha de mensagens para aquele item
            'Conto 49 pois antes da Obs., a 1a. linha ir� conter o C�d.DPK - C�d.Forn - C�d.F�br. (31 caracteres)
            'E para a impress�o correta do arquivo s� pode ter 80 colunas
            
            'Se for menor ou igual a 49, gravo a linha no arquivo normalmente
            If vTamObs <= 51 Then
            'carlosj 09/08/2012 CiT - [ISA 192] (na terceira linha houve modificacao)
'                Print #1, String(6 - Len(vUFOri), " ") & vUFOri & " " & _
'                          String(6 - Len(vUFDes), " ") & vUFDes & " " & _
'                          String(5 - Len(vDPK), " ") & vDPK & " " & _
'                          String(8 - Len(vDesc), " ") & vDesc & " " & _
'                          vObs


                Print #1, String(6 - Len(vUFOri), " ") & vUFOri & " " & _
                          String(6 - Len(vUFDes), " ") & vUFDes & " " & _
                          String(18 - Len(vDPK), " ") & vDPK & " " & _
                          String(8 - Len(vDesc), " ") & vDesc & " " & _
                          vObs
            'Sen�o
            Else
                'Imprimo os primeiros 49 caracteres
                'carlosj 09/08/2012 CiT - [ISA 192] (na terceira linha houve modificacao)
'                Print #1, String(6 - Len(vUFOri), " ") & vUFOri & " " & _
'                          String(6 - Len(vUFDes), " ") & vUFDes & " " & _
'                          String(5 - Len(vDPK), " ") & vDPK & " " & _
'                          String(8 - Len(vDesc), " ") & vDesc & " " & _
'                          Mid(vObs, 1, 51)

                Print #1, String(6 - Len(vUFOri), " ") & vUFOri & " " & _
                          String(6 - Len(vUFDes), " ") & vUFDes & " " & _
                          String(18 - Len(vDPK), " ") & vDPK & " " & _
                          String(8 - Len(vDesc), " ") & vDesc & " " & _
                          Mid(vObs, 1, 51)

                'Tamanho agora vai ser igual ao tamanho original - 49
                vTamObs = vTamObs - 51
                'Indico de que caracter vou come�ar a ler no cpo. Observa��o
                J = J + 52
                
                'Enquanto o tamanho for maior que 0
                Do While vTamObs > 0
                    'Se tamanho maior que 49, imprime os pr�ximos 49 caracteres e atualiza tam.e ponteiro
                    If vTamObs > 49 Then
                        Print #1, Space(29) & Mid(vObs, J, 51)
                        vTamObs = vTamObs - 51
                        J = J + 52
                    'Se tamanho menor que 49, imprime os caracteres que faltam
                    Else
                        Print #1, Space(29) & Mid(vObs, J, vTamObs)
                        vTamObs = 0
                    End If
                Loop
            End If
            
        End If
            
    Next I

    Print #1, ""
    Print #1, "***************** FIM DO ARQUIVO DE LOG ***********************"
    
    Close #1

    MsgBox "Arquivo gerado com sucesso!", vbInformation, "Aten��o!"


End If

End Sub

Private Sub cmdGravar_Click()

Dim vObs As String            'Guarda o conte�do da coluna de Observa��es
Dim I As Integer              'Ponteiro de linha do grid
Dim vReg_Gravado As Long      'Guarda a qtd. de itens gravados
Dim vCommit As Long           'Guarda a qtd. de itens para commitar de 20 em 20 nos CD's
Dim vLinSemObs As Long        'Guarda a qtd.de linhas sem observa��o
Dim cmdPlSql As String        'Comando SQL da LOJA_CONEXAO


If MsgBox("Confirma cadastro dos descontos?", vbYesNo, "CAD310") = vbYes Then

    'Primeiro vai percorrer todo o grid p/ ver se tem itens a serem gravados
    cmdPlSql = ""
    vLinSemObs = 0
    
    For I = 1 To grdArquivo.Rows - 1
        grdArquivo.Row = I
        grdArquivo.Col = 5
        vObs = grdArquivo.Text
        'Se a coluna de Observa��o n�o contiver nenhuma mensagem de inconsist�ncia vai gravar
        If vObs = "" Then
            vLinSemObs = vLinSemObs + 1
        End If
    Next I
    
    If vLinSemObs = 0 Then
        MsgBox "Nenhuma linha de desconto poder� ser cadastrada!", vbCritical, "Aten��o!"
        Exit Sub
    End If
    
    'Se houverem itens a serem gravados, vai iniciar todo o processo
    'Zera contador de linhas gravadas
    vReg_Gravado = 0
    vCommit = 0
    
    oradatabase.Parameters.Remove "cod_errora": oradatabase.Parameters.Add "cod_errora", 0, 2
    oradatabase.Parameters.Remove "txt_errora": oradatabase.Parameters.Add "txt_errora", "", 2

    'Pega linha a linha do grid para gravar as que cont�m campo de Observa��o = ""
    For I = 1 To grdArquivo.Rows - 1
        
        grdArquivo.Row = I
        grdArquivo.Col = 5
        vObs = Trim(grdArquivo.Text)
        
        'Se a coluna de Observa��o n�o contiver nenhuma mensagem de inconsist�ncia vai gravar
        If vObs = "" Then
            
            '//////////////////////////////////////////////
            'Vai alimentar as vari�veis normais e de bind
            '/////////////////////////////////////////////
            'Limpa vari�veis auxiliares de grava��o
            cmdpl = ""
    
            grdArquivo.Col = 1
            oradatabase.Parameters.Remove "UFORI": oradatabase.Parameters.Add "UFORI", Trim(grdArquivo.Text), 1
            grdArquivo.Col = 2
            oradatabase.Parameters.Remove "UFDES": oradatabase.Parameters.Add "UFDES", Trim(grdArquivo.Text), 1
            grdArquivo.Col = 3
            oradatabase.Parameters.Remove "DPK": oradatabase.Parameters.Add "DPK", CDbl(Trim(grdArquivo.Text)), 1
            grdArquivo.Col = 4
            oradatabase.Parameters.Remove "DESC": oradatabase.Parameters.Add "DESC", CDbl(Trim(grdArquivo.Text)), 1
            
            cmdPlSql = cmdPlSql & _
                      " Update VENDAS.UF_DPK " & _
                          "Set pc_desc = :DESC" & " " & _
                        "Where cod_dpk = :DPK" & " " & _
                          "And cod_uf_origem = :UFORI" & " " & _
                          "And cod_uf_destino = :UFDES" & "; " & _
                    " If SQL%NOTFOUND Then " & _
                        "Insert Into VENDAS.UF_DPK " & _
                              " (cod_uf_origem, cod_uf_destino, cod_dpk, pc_desc) " & _
                       " Values (:UFORI" & ", :UFDES" & ", " & _
                                ":DPK" & ", :DESC" & ");" & _
                    " End If;"
            
            vCommit = vCommit + 1
            vReg_Gravado = vReg_Gravado + 1
                
                
            '///////////////////////////////////////////////////////////////////////////////////////
            'Verifica as informa��es deste item, para gravar o LOG
            vLog_FL_DIF_ICM = "": vLog_TP_ALT = "": vLog_UFORI = "": vLog_UFDES = "": vLog_DPK = 0
            vLog_DESC_ANT = 0: vLog_DESC_NOVO = 0: strLOG = ""
            
            'Verifica a flag de diferen�a de icms no cadastro de fornecedor espec�fico
            lstrSQL = "Select Nvl(E.FL_Dif_Icms,'NULO') FL_Dif_Icms " & _
                        "From Producao.Fornecedor_Especifico E, Producao.Item_Cadastro C " & _
                       "Where E.Cod_Fornecedor in (0, C.Cod_Fornecedor) " & _
                         "And E.Cod_Grupo in (0, C.Cod_Grupo) " & _
                         "And E.Cod_SubGrupo in (0, C.Cod_SubGrupo) " & _
                         "And E.Cod_Dpk in (0, C.Cod_Dpk) " & _
                         "And C.Cod_Dpk = :DPK " & _
                         "Order by E.Cod_Fornecedor, E.Cod_Grupo, E.Cod_Subgrupo, E.Cod_Dpk "
            Set objDesc2 = oradatabase.dbCreateDynaset(lstrSQL, 0&)
            If objDesc2.EOF Then
                vLog_FL_DIF_ICM = "NULO"
            Else
                vLog_FL_DIF_ICM = objDesc2!FL_Dif_Icms
            End If
            'Seleciona o percentual de desconto atual do item na Vendas.Uf_Dpk
            lstrSQL = "Select * " & _
                        "From Vendas.Uf_Dpk " & _
                       "Where Cod_Dpk = :DPK " & _
                         "And Cod_Uf_Origem = :UFORI " & _
                         "And Cod_Uf_Destino = :UFDES"
            Set objDesc2 = oradatabase.dbCreateDynaset(lstrSQL, 0&)
            If objDesc2.EOF Then
                grdArquivo.Col = 1
                vLog_UFORI = Trim(grdArquivo.Text)
                grdArquivo.Col = 2
                vLog_UFDES = Trim(grdArquivo.Text)
                grdArquivo.Col = 3
                vLog_DPK = CLng(grdArquivo.Text)
                vLog_DESC_ANT = 0
                grdArquivo.Col = 4
                vLog_DESC_NOVO = CDbl(grdArquivo.Text)
                vLog_TP_ALT = "I"
            Else
                vLog_UFORI = objDesc2!Cod_Uf_Origem
                vLog_UFDES = objDesc2!Cod_Uf_Destino
                vLog_DPK = objDesc2!Cod_Dpk
                vLog_DESC_ANT = objDesc2!Pc_Desc
                grdArquivo.Col = 4
                vLog_DESC_NOVO = CDbl(grdArquivo.Text)
                vLog_TP_ALT = "A"
            End If
            'Prepara binds para gravar log
            oradatabase.Parameters.Remove "UF_ORI":  oradatabase.Parameters.Add "UF_ORI", vLog_UFORI, 1
            oradatabase.Parameters.Remove "UF_DES":  oradatabase.Parameters.Add "UF_DES", vLog_UFDES, 1
            oradatabase.Parameters.Remove "COD_DPK":    oradatabase.Parameters.Add "COD_DPK", vLog_DPK, 1
            oradatabase.Parameters.Remove "DESCANT": oradatabase.Parameters.Add "DESCANT", vLog_DESC_ANT, 1
            oradatabase.Parameters.Remove "DESCATU":   oradatabase.Parameters.Add "DESCATU", vLog_DESC_NOVO, 1
            oradatabase.Parameters.Remove "FLDIF":  oradatabase.Parameters.Add "FLDIF", vLog_FL_DIF_ICM, 1
            oradatabase.Parameters.Remove "TPALT":  oradatabase.Parameters.Add "TPALT", vLog_TP_ALT, 1
            oradatabase.Parameters.Remove "USU":    oradatabase.Parameters.Add "USU", vLog_Usu, 1
            strLOG = "Insert into Vendas.Log_Uf_Dpk " & _
                     "(Cod_Uf_Origem, Cod_Uf_Destino, Cod_Dpk, Pc_Desc_Ant, Pc_Desc, Fl_Dif_Icms, Tp_Alteracao, " & _
                     " Dt_Alteracao, Usuario) Values " & _
                     "(:UF_ORI, :UF_DES, :COD_DPK, :DESCANT, :DESCATU, :FLDIF, :TPALT, " & _
                     " To_Date(To_Char(SYSDATE,'dd/mm/rr hh24:mi:ss'),'dd/mm/rr hh24:mi:ss'), :USU); "
            strLOG = "BEGIN " & strLOG & _
                            " COMMIT;EXCEPTION WHEN OTHERS THEN ROLLBACK;" & _
                            " :cod_errora := SQLCODE;:txt_errora := SQLERRM; " & _
                         "END;"
            oradatabase.ExecuteSQL strLOG
            'VERIFICA ERROS
            If IIf(IsNull(oradatabase.Parameters("cod_errora")), 0, Val(oradatabase.Parameters("cod_errora"))) <> 0 Then
                MsgBox oradatabase.Parameters("txt_errora") & " - Erro na gera��o do Log para esta altera��o!"
            End If
            '///////////////////////////////////////////////////////////////////////////////////////
                
                
                
                
                
            cmdPlSql = "BEGIN " & cmdPlSql & _
                            " COMMIT;EXCEPTION WHEN OTHERS THEN ROLLBACK;" & _
                            " :cod_errora := SQLCODE;:txt_errora := SQLERRM; " & _
                         "END;"
            oradatabase.ExecuteSQL cmdPlSql
            
            'VERIFICA ERROS
            vErro = IIf(oradatabase.Parameters("cod_errora") = Null Or oradatabase.Parameters("cod_errora") = "", 0, oradatabase.Parameters("cod_errora"))
            If vErro <> 0 Then
                MsgBox oradatabase.Parameters("txt_errora") & ".Ligue para o Suporte a Sistemas!"
                Exit Sub
            End If
                            
                            
                            
            cmdPlSql = ""
            vCommit = 0
        
        End If
    Next I
        
    cmdPlSql = "BEGIN " & cmdPlSql & _
                    " COMMIT;EXCEPTION WHEN OTHERS THEN ROLLBACK;" & _
                    " :cod_errora := SQLCODE;:txt_errora := SQLERRM; " & _
                 "END;"
    oradatabase.ExecuteSQL cmdPlSql
    
    'VERIFICA ERROS
    vErro = IIf(oradatabase.Parameters("cod_errora") = Null Or oradatabase.Parameters("cod_errora") = "", 0, oradatabase.Parameters("cod_errora"))
    If vErro <> 0 Then
        MsgBox oradatabase.Parameters("txt_errora") & ".Ligue para o Suporte a Sistemas!"
        Exit Sub
    Else
        MsgBox "Descontos de " & vReg_Gravado & " itens Gravados com Sucesso!", vbInformation, "CAD310 - Aten��o!"
    End If

End If

End Sub

Private Sub cmdLayout_Click()
    frmLayOut.Show 1
End Sub

Private Sub cmdVoltar_Click()
Unload Me
End Sub


Private Sub Command1_Click()

End Sub

Private Sub scmdImportar_Click()
    
    'Vari�veis locais
    Dim I As Integer              'Ponteiro / Contador
    Dim vStrLinhaArq As String    'Guarda o peda�o (Coluna) do arquivo que est� sendo lido
    Dim vDPK As Double            'Guarda o c�digo DPK se o item j� estiver cadastrado
    Dim vUFOri As String          'Guarda a UF Origem vindo do arquivo
    Dim vUFDes As String          'Guarda a UF Destino vindo do arquivo
    Dim vDesc As Double           'Guarda o desconto de UF por item vindo do arquivo
    Dim vSinal As String          'Guarda o sinal do desconto
    Dim vSit_Dpk As Integer       'Guarda a situa��o do DPK vindo do arquivo
    Dim vFl_DPK_OK As Boolean     'Indica se DPK est� Ok ou n�o (se n�o estiver, nem continua demais valida��es
    Dim vNomeArq As String
    Dim varLinhaArquivo As String   'Recebe a linha lida do arquivo texto
    Dim Sql As String
    Dim objValida As Object
    
    'Indica que o controle ser� usado para abrir o arquivo
    controle.ShowOpen
    lblArquivo.Caption = controle.FileName
    vNomeArq = lblArquivo

    'Verifica se foi indicado o diret�rio e nome do arquivo a ser lido
    If lblArquivo.Caption = "" Then
        MsgBox "Indique o arquivo a ser aberto", , MSG_TIT
        Exit Sub
    End If


    '/////////////////////////
    'MONTA O GRID
    '/////////////////////////
    With grdArquivo
        
        .Visible = True
        .Row = 0
        
        .Col = 0: .ColWidth(0) = 450: .Text = "Reg"
        
        .Col = 1: .ColWidth(1) = 1050: .ColAlignment(1) = 2
        .Text = "  UF Origem"
        
        .Col = 2: .ColWidth(2) = 1050: .ColAlignment(2) = 2
        .Text = "  UF Destino"
        
        'carlosj 09/08/2012 CiT - [ISA 192]
        '.Col = 3: .ColWidth(3) = 600: .ColAlignment(3) = 2
        '.Text = "  Dpk"
        .Col = 3: .ColWidth(3) = 850: .ColAlignment(3) = 2
        .Text = "  Dpk"
        
        .Col = 4: .ColWidth(4) = 850: .ColAlignment(4) = 2
        .Text = " Desconto"
        
        .Col = 5: .ColWidth(5) = 10000: .ColAlignment(5) = 0
        .Text = "Observa��o"
    
    End With
    
    '//////////////////////////
    'LEITURA DO ARQUIVO
    '//////////////////////////
    Open vNomeArq For Input As #1
    
    'Inicializa contadores de linhas do grid
    I = 0
    lblMsg.Visible = True
    
    'L� enquanto n�o for final de arquivo
    Do While Not EOF(1)
    
        With grdArquivo
    
            Screen.MousePointer = 11
            I = I + 1
            
            lblMsg = "CARREGANDO ...." & I
            lblMsg.Refresh
            
            'Inicializa flags e vari�veis
            vFl_DPK_OK = True
            
            vDPK = 0
            vUFOri = ""
            vUFDes = ""
            vDesc = 0
            vSinal = ""
            vStrLinhaArq = ""
            
            
            'L� registro - linha - do arquivo, joga na vari�vel  e seta ponteiro para o pr�ximo
            Line Input #1, varLinhaArquivo
            'Se linha lida for em branco, sai do loop
            If Mid(varLinhaArquivo, 1, 28) = "" Then
                Exit Do
            End If
            
            'Qtd. Linhas do grid ser� correspondente � qtd. de linhas lidas do arquivo texto
            .Rows = I + 1
            .Row = I
                    
            '1a Coluna vai receber a numera��o de registros lidos do arquivo
            .Col = 0: .Text = Format(I, "0000")
            
            'Carrega vari�veis com os dados vindos do arquivo
            vUFOri = Trim(Mid(varLinhaArquivo, 1, 2))           'UF ORIGEM
            vUFDes = Trim(Mid(varLinhaArquivo, 3, 2))           'UF DESTINO
            'carlosj 09/08/2012 CiT - [ISA 192]
'            vDPK = Trim(Mid(varLinhaArquivo, 5, 5))             'DPK
'            vDesc = CDbl(Trim(Mid(varLinhaArquivo, 10, 5)))     'VALOR DO DESCONTO
'            vSinal = Trim(Mid(varLinhaArquivo, 15, 1))          'SINAL DO DESCONTO
            vDPK = Trim(Mid(varLinhaArquivo, 5, 18))             'DPK
            vDesc = CDbl(Trim(Mid(varLinhaArquivo, 23, 5)))     'VALOR DO DESCONTO
            vSinal = Trim(Mid(varLinhaArquivo, 28, 1))          'SINAL DO DESCONTO
           
            'Carregar grid com os dados, independente de estarem corretos ou n�o
            With grdArquivo
                .Col = 1: .Text = vUFOri
                .Col = 2: .Text = vUFDes
                .Col = 3: .Text = vDPK
                .Col = 4: .Text = IIf(vSinal = "", "+", vSinal) & Format(vDesc, "00.00")
            End With
            
            'Valida dados originais do arquivo
            vFl_DPK_OK = True
            
            If vUFOri = "" Then
                .Col = 5: .Text = "UF Origem Inv�lido!"
                vFl_DPK_OK = False
            ElseIf vUFDes = "" Then
                .Col = 5: .Text = "UF Destino Inv�lido!"
                vFl_DPK_OK = False
            ElseIf Val(vDPK) = 0 Then
                .Col = 5: .Text = "DPK Inv�lido!"
                vFl_DPK_OK = False
            ElseIf vSinal <> "+" And vSinal <> "-" And vSinal <> "" Then
                .Col = 5: .Text = "Sinal Inv�lido!"
                vFl_DPK_OK = False
            End If
            
            'Se dados originais estiverem ok, Valida campos - UF
            If vFl_DPK_OK = True Then
            
                'oradatabase.Parameters.Remove "UFORI": oradatabase.Parameters.Add "UFORI", vUFOri, 1
                'Sql = "Select Count(*) Qtd From Producao.UF Where Cod_Uf = :UFORI "
                'Set objValida = oradatabase.CreateDynaset(Sql, 0&)
                'If objValida!qtd.Value = 0 Then
                '    .Col = 5: .Text = "UF Origem Inv�lida!"
                '    vFl_DPK_OK = False
                'End If
                'If vFl_DPK_OK = True Then
                    'oradatabase.Parameters.Remove "UFDES": oradatabase.Parameters.Add "UFDES", vUFDes, 1
                    'Sql = "Select Count(*) Qtd From Producao.UF Where Cod_Uf = :UFORI "
                    'Set objValida = oradatabase.CreateDynaset(Sql, 0&)
                    'If objValida!qtd.Value = 0 Then
                    '    .Col = 5: .Text = "UF Origem Inv�lida!"
                    '    vFl_DPK_OK = False
                    'End If
                    
                    'Vou validar as duas UFs de uma vez para agilizar o processo de importa��o
                    oradatabase.Parameters.Remove "UFORI": oradatabase.Parameters.Add "UFORI", vUFOri, 1
                    oradatabase.Parameters.Remove "UFDES": oradatabase.Parameters.Add "UFDES", vUFDes, 1
                    Sql = "Select Count(*) Qtd " & _
                            "From Producao.UF A, Producao.UF B " & _
                           "Where A.Cod_Uf = :UFORI " & _
                             "And B.Cod_Uf = :UFDES "
                    Set objValida = oradatabase.CreateDynaset(Sql, 0&)
                    If objValida!Qtd.Value = 0 Then
                        .Col = 5: .Text = "UF Origem/Destino � Inv�lida!"
                        vFl_DPK_OK = False
                    End If
                
                    If vFl_DPK_OK = True Then
                        
                        'Valida DPK
                        oradatabase.Parameters.Remove "DPK": oradatabase.Parameters.Add "DPK", vDPK, 1
                        Sql = "Select IC.Cod_Dpk " & _
                                "From Producao.Item_Estoque IE, Producao.Item_Cadastro IC " & _
                               "Where IE.Cod_Dpk = IC.Cod_Dpk " & _
                                 "And IE.Situacao = 0 " & _
                                 "And IE.Cod_Loja = 1 " & _
                                 "And IC.Cod_Dpk = :DPK "
                        
                        Set objValida = oradatabase.CreateDynaset(Sql, 0&)
                        If objValida.EOF Then
                            .Col = 5: .Text = "DPK Inexistente ou Desativado!"
                            vFl_DPK_OK = False
                        End If
                    End If
                'End If
            End If
        End With
    Loop
            
    MsgBox "Arquivo Lido!", vbInformation, "Aten��o!"
    lblMsg = I & " Linhas Lidas do Arquivo!"
    lblMsg.Refresh
    
    Close #1    ' Close file.
    Screen.MousePointer = 0
        
    Exit Sub
        
        
Trata_Erro:
    
    If Err.Number = 53 Then
        MsgBox "Arquivo de Controle n�o Encontrado. ", , MSG_TIT
    Else
        Call Process_Line_Errors(Err.Number)
    End If


End Sub


