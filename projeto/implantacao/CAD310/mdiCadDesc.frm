VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.MDIForm mdiCadDesc 
   BackColor       =   &H8000000C&
   Caption         =   "CAD310 - Cadastro de Desconto de UF por Item"
   ClientHeight    =   6585
   ClientLeft      =   165
   ClientTop       =   3300
   ClientWidth     =   8850
   LinkTopic       =   "MDIForm1"
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin Threed.SSPanel SSPanel1 
      Align           =   1  'Align Top
      Height          =   795
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   8850
      _Version        =   65536
      _ExtentX        =   15610
      _ExtentY        =   1402
      _StockProps     =   15
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin VB.CommandButton cmdSair 
         Height          =   600
         Left            =   6630
         Picture         =   "mdiCadDesc.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   75
         Width           =   675
      End
      Begin VB.CommandButton cmdSobre 
         DownPicture     =   "mdiCadDesc.frx":030A
         Height          =   600
         Left            =   5685
         Picture         =   "mdiCadDesc.frx":0614
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   75
         Width           =   675
      End
      Begin VB.CommandButton cmdImport 
         Height          =   600
         Left            =   1065
         Picture         =   "mdiCadDesc.frx":091E
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   90
         Width           =   675
      End
      Begin VB.CommandButton cmdManual 
         Height          =   600
         Left            =   135
         Picture         =   "mdiCadDesc.frx":0C28
         Style           =   1  'Graphical
         TabIndex        =   1
         Top             =   90
         Width           =   675
      End
   End
   Begin VB.Menu mnuCadastrar 
      Caption         =   "&Cadastrar"
      Begin VB.Menu mnuManual 
         Caption         =   "&Manual"
      End
      Begin VB.Menu mnuImport 
         Caption         =   "&Import.Arquivo"
      End
   End
   Begin VB.Menu mnuSobre 
      Caption         =   "So&bre"
   End
   Begin VB.Menu mnuSair 
      Caption         =   "&Sair"
   End
End
Attribute VB_Name = "mdiCadDesc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdImport_Click()
    frmImportar.Show 1
End Sub

Private Sub cmdManual_Click()
    frmManual.Show 1
End Sub

Private Sub cmdSair_Click()
End
End Sub


Private Sub cmdSobre_Click()
    frmSobre.Show 1
End Sub


Private Sub MDIForm_Load()
Dim lstrSQL As String, Sql As String, strLogin As String
Dim lstrConeccao As String
Dim objSQL As Object, ss As Object
    
On Error GoTo TrataErro
glCod_Transp = 0
    
If App.PrevInstance Then
  MsgBox "J� EXISTE UMA INST�NCIA DO PROGRAMA NO AR", 0, "Aten��o"
  End
End If
    

dlgLogin.Show 1
    
    
'Conexao oracle
'Set orasession = CreateObject("oracleinprocserver.xorasession")
'Set oradatabase = orasession.OpenDatabase("PRODUCAO", "CAD_UFDPK/PROD", 0&)
'Set oradatabase = orasession.OpenDatabase("sdpk_teste", "PRODUCAO/dpk", 0&)

'----------------- DATA DE HOJE ---------------------
lstrSQL = "SELECT to_char(dt_faturamento, 'DD/MM/RR'), " & _
                " to_char(dt_faturamento, 'DD/MM/YYYY') Dt_Cad " & _
            "FROM datas"
Set objSQL = oradatabase.dbCreateDynaset(lstrSQL, 0&)

gstrDataFaturamento = objSQL.Fields(0)
vDt_Cad = Format(objSQL!Dt_Cad.Value, "dd/mm/yy")


Exit Sub

TrataErro:
    If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Or Err = 75 Then
       Resume
    Else
       Call Process_Line_Errors(Sql)
    End If


End Sub


Private Sub mnuImport_Click()
Call cmdImport_Click
End Sub

Private Sub mnuManual_Click()

Call cmdManual_Click

End Sub


