VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form frmLayOut 
   Caption         =   "Lay-Out do Arquivo para Cadastro de Desconto de UF Por Item"
   ClientHeight    =   6360
   ClientLeft      =   1470
   ClientTop       =   1035
   ClientWidth     =   8370
   LinkTopic       =   "Form1"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6360
   ScaleWidth      =   8370
   Begin VB.Frame Frame1 
      Height          =   5595
      Left            =   150
      TabIndex        =   0
      Top             =   -45
      Width           =   8115
      Begin VB.Label Label13 
         AutoSize        =   -1  'True
         Caption         =   "    Dois d�gitos � esquerda, ponto decimal e duas casas deciamis. (00.00)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   645
         TabIndex        =   18
         Top             =   3690
         Width           =   6285
      End
      Begin VB.Label Label22 
         AutoSize        =   -1  'True
         Caption         =   "Sinal                                          ALFANUM�RICO (1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   645
         TabIndex        =   17
         Top             =   1800
         Width           =   4680
      End
      Begin VB.Label Label17 
         AutoSize        =   -1  'True
         Caption         =   "    Se campo estiver em branco, ser� atribuido sinal de + (Positivo)."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   645
         TabIndex        =   16
         Top             =   4395
         Width           =   5745
      End
      Begin VB.Label Label11 
         AutoSize        =   -1  'True
         Caption         =   "3) Campo ""Sinal"" dever� vir com + (Positivo) ou - (Negativo)."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   645
         TabIndex        =   15
         Top             =   4200
         Width           =   5205
      End
      Begin VB.Line Line3 
         X1              =   600
         X2              =   7480
         Y1              =   3180
         Y2              =   3180
      End
      Begin VB.Label Label16 
         AutoSize        =   -1  'True
         Caption         =   "Desconto                                    NUM�RICO (4,2)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   645
         TabIndex        =   14
         Top             =   1560
         Width           =   4425
      End
      Begin VB.Label Label14 
         AutoSize        =   -1  'True
         Caption         =   "4) Fazer arquivos de, no m�ximo 100 linhas."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   645
         TabIndex        =   13
         Top             =   4650
         Width           =   3750
      End
      Begin VB.Label Label12 
         AutoSize        =   -1  'True
         Caption         =   "2) Campos Alfanum�ricos dever�o vir com espa�os em branco � direita."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   645
         TabIndex        =   11
         Top             =   3945
         Width           =   6090
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         Caption         =   "1) Campos Num�ricos dever�o vir com zeros � esquerda."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   645
         TabIndex        =   10
         Top             =   3495
         Width           =   4845
      End
      Begin VB.Label Label9 
         AutoSize        =   -1  'True
         Caption         =   "Observa��es:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000040C0&
         Height          =   195
         Left            =   1035
         TabIndex        =   9
         Top             =   3255
         Width           =   1185
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         Caption         =   "MGMG00000000000006885904.77"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   615
         TabIndex        =   8
         Top             =   2865
         Width           =   2835
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "MGMG00000000000006885804.60"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   615
         TabIndex        =   7
         Top             =   2655
         Width           =   2835
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         Caption         =   "MGMG00000000000006886104.57"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   615
         TabIndex        =   6
         Top             =   2445
         Width           =   2835
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Exemplo de Arquivo:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000040C0&
         Height          =   195
         Left            =   1110
         TabIndex        =   5
         Top             =   2205
         Width           =   1755
      End
      Begin VB.Line Line2 
         X1              =   600
         X2              =   7480
         Y1              =   2130
         Y2              =   2130
      End
      Begin VB.Line Line1 
         X1              =   600
         X2              =   7480
         Y1              =   705
         Y2              =   705
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Campo                                  Tipo de Dados"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000040C0&
         Height          =   195
         Left            =   1035
         TabIndex        =   4
         Top             =   465
         Width           =   3870
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "C�d.DPK                                     NUM�RICO (18)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   645
         TabIndex        =   3
         Top             =   1320
         Width           =   4380
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "UF Destino                                  ALFANUM�RICO (2)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   645
         TabIndex        =   2
         Top             =   1080
         Width           =   4725
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "UF Origem                                   ALFANUM�RICO (2)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   645
         TabIndex        =   1
         Top             =   855
         Width           =   4725
      End
   End
   Begin Threed.SSCommand cmdVoltar 
      Height          =   615
      Left            =   7635
      TabIndex        =   12
      Top             =   5655
      Width           =   630
      _Version        =   65536
      _ExtentX        =   1111
      _ExtentY        =   1085
      _StockProps     =   78
      Picture         =   "frmLayOu.frx":0000
   End
End
Attribute VB_Name = "frmLayOut"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdVoltar_Click()
    Unload Me
End Sub


