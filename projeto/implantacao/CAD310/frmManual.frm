VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmManual 
   Caption         =   "CAD310 - Cadastro Manual "
   ClientHeight    =   8670
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11595
   LinkTopic       =   "Form1"
   ScaleHeight     =   8670
   ScaleWidth      =   11595
   WindowState     =   2  'Maximized
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   8160
      Top             =   7920
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton cmdDuplUF 
      DisabledPicture =   "frmManual.frx":0000
      Height          =   555
      Left            =   10650
      Picture         =   "frmManual.frx":030A
      Style           =   1  'Graphical
      TabIndex        =   45
      ToolTipText     =   "Duplicar UF"
      Top             =   4965
      Width           =   750
   End
   Begin VB.CommandButton cmdConsultar 
      DisabledPicture =   "frmManual.frx":0614
      Height          =   555
      Left            =   10650
      Picture         =   "frmManual.frx":091E
      Style           =   1  'Graphical
      TabIndex        =   8
      ToolTipText     =   "Consultar Descontos"
      Top             =   5850
      Width           =   750
   End
   Begin VB.CommandButton cmdLimpar 
      DisabledPicture =   "frmManual.frx":11E8
      Height          =   555
      Left            =   10665
      Picture         =   "frmManual.frx":14F2
      Style           =   1  'Graphical
      TabIndex        =   7
      ToolTipText     =   "Limpar Campos"
      Top             =   6780
      Width           =   750
   End
   Begin VB.CommandButton cmdVoltar 
      DisabledPicture =   "frmManual.frx":1DBC
      Height          =   555
      Left            =   10665
      Picture         =   "frmManual.frx":20C6
      Style           =   1  'Graphical
      TabIndex        =   6
      ToolTipText     =   "Voltar"
      Top             =   7680
      Width           =   750
   End
   Begin Threed.SSFrame fraCadastro 
      Height          =   8475
      Left            =   45
      TabIndex        =   9
      Top             =   90
      Width           =   9390
      _Version        =   65536
      _ExtentX        =   16563
      _ExtentY        =   14949
      _StockProps     =   14
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin Threed.SSPanel spnlUF 
         Height          =   4020
         Left            =   75
         TabIndex        =   10
         Top             =   4380
         Width           =   7710
         _Version        =   65536
         _ExtentX        =   13600
         _ExtentY        =   7091
         _StockProps     =   15
         Caption         =   " "
         BackColor       =   12632256
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Begin VB.ComboBox cboUfOrigem 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   315
            Left            =   1125
            Sorted          =   -1  'True
            TabIndex        =   18
            Top             =   360
            Width           =   705
         End
         Begin VB.CommandButton cmdCarregaUF 
            DisabledPicture =   "frmManual.frx":23D0
            Height          =   555
            Left            =   1995
            Picture         =   "frmManual.frx":26DA
            Style           =   1  'Graphical
            TabIndex        =   17
            ToolTipText     =   "Pesquisar Descontos da UF"
            Top             =   360
            Width           =   750
         End
         Begin Threed.SSPanel SSPanel1 
            Height          =   3570
            Left            =   3300
            TabIndex        =   11
            Top             =   360
            Width           =   4275
            _Version        =   65536
            _ExtentX        =   7541
            _ExtentY        =   6297
            _StockProps     =   15
            BackColor       =   12632256
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Begin VB.TextBox txtDesconto 
               BackColor       =   &H00FFFFFF&
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00FF0000&
               Height          =   315
               Left            =   2040
               MaxLength       =   6
               TabIndex        =   13
               Top             =   3135
               Width           =   915
            End
            Begin VB.CommandButton cmdGravarEspec 
               DisabledPicture =   "frmManual.frx":29E4
               Height          =   555
               Left            =   3390
               Picture         =   "frmManual.frx":2CEE
               Style           =   1  'Graphical
               TabIndex        =   12
               ToolTipText     =   "Gravar Descontos Espec�ficos"
               Top             =   2520
               Width           =   750
            End
            Begin MSGrid.Grid grdUF 
               Height          =   2775
               Left            =   120
               TabIndex        =   14
               Top             =   315
               Width           =   3195
               _Version        =   65536
               _ExtentX        =   5636
               _ExtentY        =   4895
               _StockProps     =   77
               BackColor       =   16777215
               Cols            =   4
               FixedCols       =   0
            End
            Begin VB.Label lblDesconto 
               AutoSize        =   -1  'True
               Caption         =   "Desconto"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00800000&
               Height          =   195
               Left            =   1140
               TabIndex        =   16
               Top             =   3210
               Width           =   870
               WordWrap        =   -1  'True
            End
            Begin VB.Label Label11 
               AutoSize        =   -1  'True
               Caption         =   "UF Destino"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00800000&
               Height          =   195
               Left            =   120
               TabIndex        =   15
               Top             =   105
               Width           =   960
            End
         End
         Begin Threed.SSPanel SSPanel3 
            Height          =   750
            Left            =   240
            TabIndex        =   19
            Top             =   2745
            Width           =   2175
            _Version        =   65536
            _ExtentX        =   3836
            _ExtentY        =   1323
            _StockProps     =   15
            ForeColor       =   -2147483640
            BackColor       =   12632256
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Alignment       =   6
            Begin VB.TextBox txtDescontoUnico 
               BackColor       =   &H00FFFFFF&
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00FF0000&
               Height          =   315
               Left            =   120
               MaxLength       =   6
               TabIndex        =   21
               Top             =   315
               Width           =   915
            End
            Begin VB.CommandButton cmdGravarUnico 
               DisabledPicture =   "frmManual.frx":35B8
               Height          =   555
               Left            =   1230
               Picture         =   "frmManual.frx":38C2
               Style           =   1  'Graphical
               TabIndex        =   20
               ToolTipText     =   "Gravar Desconto �nico"
               Top             =   105
               Width           =   750
            End
         End
         Begin VB.Label Label10 
            AutoSize        =   -1  'True
            Caption         =   "UF Origem"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   135
            TabIndex        =   24
            Top             =   465
            Width           =   900
         End
         Begin VB.Label Label9 
            Alignment       =   2  'Center
            AutoSize        =   -1  'True
            Caption         =   "Cadastro de Desconto Espec�fico por UF"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   3450
            TabIndex        =   23
            Top             =   105
            Width           =   3735
            WordWrap        =   -1  'True
         End
         Begin VB.Label Label14 
            Alignment       =   2  'Center
            AutoSize        =   -1  'True
            Caption         =   "Cadastro de Desconto �nico por UF"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   480
            Left            =   225
            TabIndex        =   22
            Top             =   2310
            Width           =   2205
            WordWrap        =   -1  'True
         End
      End
      Begin Threed.SSPanel spnlGrupo 
         Height          =   1890
         Left            =   75
         TabIndex        =   25
         Top             =   2415
         Width           =   4815
         _Version        =   65536
         _ExtentX        =   8493
         _ExtentY        =   3334
         _StockProps     =   15
         BackColor       =   12632256
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Begin VB.ComboBox cboGrupo 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   315
            Left            =   1080
            Sorted          =   -1  'True
            TabIndex        =   1
            Top             =   120
            Width           =   3525
         End
         Begin MSGrid.Grid grdSubGrupo 
            Height          =   1290
            Left            =   1080
            TabIndex        =   26
            Top             =   525
            Width           =   3525
            _Version        =   65536
            _ExtentX        =   6218
            _ExtentY        =   2275
            _StockProps     =   77
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            FixedCols       =   0
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Grupo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   105
            TabIndex        =   28
            Top             =   120
            Width           =   525
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "SubGrupo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   105
            TabIndex        =   27
            Top             =   495
            Width           =   855
         End
      End
      Begin Threed.SSPanel spnlForn 
         Height          =   2160
         Left            =   90
         TabIndex        =   29
         Top             =   195
         Width           =   9180
         _Version        =   65536
         _ExtentX        =   16192
         _ExtentY        =   3810
         _StockProps     =   15
         BackColor       =   12632256
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Begin VB.ComboBox cboFornecedor 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   315
            Left            =   1950
            Sorted          =   -1  'True
            TabIndex        =   0
            Top             =   135
            Width           =   4215
         End
         Begin VB.TextBox txtCod_DPK 
            BackColor       =   &H00FFFFFF&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   315
            Left            =   7425
            MaxLength       =   9
            TabIndex        =   3
            Top             =   135
            Width           =   1275
         End
         Begin VB.ComboBox cboCod_Fabrica 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   315
            Left            =   1770
            Sorted          =   -1  'True
            TabIndex        =   2
            Top             =   525
            Width           =   4395
         End
         Begin VB.Label lblDesc_Item 
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   315
            Left            =   1170
            TabIndex        =   5
            Top             =   915
            Width           =   4995
         End
         Begin VB.Label lblCod_Tributacao 
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   315
            Left            =   2115
            TabIndex        =   37
            Top             =   1695
            Width           =   2595
         End
         Begin VB.Label lblCod_Tributacao_IPI 
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   315
            Left            =   2115
            TabIndex        =   36
            Top             =   1305
            Width           =   2595
         End
         Begin VB.Label Label8 
            AutoSize        =   -1  'True
            Caption         =   "C�digo Tributa��o ICM"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   105
            TabIndex        =   35
            Top             =   1695
            Width           =   1980
         End
         Begin VB.Label Label15 
            AutoSize        =   -1  'True
            Caption         =   "C�digo Tributa��o IPI"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   105
            TabIndex        =   34
            Top             =   1305
            Width           =   1875
         End
         Begin VB.Label Label4 
            AutoSize        =   -1  'True
            Caption         =   "Fornecedor"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   135
            TabIndex        =   33
            Top             =   135
            Width           =   975
         End
         Begin VB.Label lblCod_Fornecedor 
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   315
            Left            =   1185
            TabIndex        =   4
            Top             =   135
            Width           =   735
         End
         Begin VB.Label Label5 
            AutoSize        =   -1  'True
            Caption         =   "Descri��o"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   105
            TabIndex        =   32
            Top             =   915
            Width           =   870
         End
         Begin VB.Label Label6 
            AutoSize        =   -1  'True
            Caption         =   "C�digo de F�brica"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   105
            TabIndex        =   31
            Top             =   525
            Width           =   1560
         End
         Begin VB.Label Label7 
            AutoSize        =   -1  'True
            Caption         =   "Cod. DPK"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   6495
            TabIndex        =   30
            Top             =   135
            Width           =   840
         End
      End
      Begin Threed.SSPanel spnlLinha 
         Height          =   1890
         Left            =   4950
         TabIndex        =   38
         Top             =   2415
         Width           =   4320
         _Version        =   65536
         _ExtentX        =   7620
         _ExtentY        =   3334
         _StockProps     =   15
         BackColor       =   12632256
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Begin MSGrid.Grid grdLinha 
            Height          =   1485
            Left            =   240
            TabIndex        =   39
            Top             =   285
            Width           =   3840
            _Version        =   65536
            _ExtentX        =   6773
            _ExtentY        =   2619
            _StockProps     =   77
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            FixedCols       =   0
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            Caption         =   "Linha"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   270
            TabIndex        =   40
            Top             =   60
            Width           =   480
         End
      End
   End
   Begin Threed.SSFrame fraConsulta 
      Height          =   7650
      Left            =   75
      TabIndex        =   41
      Top             =   180
      Width           =   9375
      _Version        =   65536
      _ExtentX        =   16536
      _ExtentY        =   13494
      _StockProps     =   14
      Caption         =   "Consulta Descontos"
      ForeColor       =   12582912
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin VB.CommandButton cmdImprimir 
         DisabledPicture =   "frmManual.frx":418C
         Height          =   555
         Left            =   8310
         Picture         =   "frmManual.frx":4496
         Style           =   1  'Graphical
         TabIndex        =   44
         ToolTipText     =   "Imprimir Consulta"
         Top             =   6000
         Width           =   750
      End
      Begin VB.CommandButton cmdVoltarCons 
         DisabledPicture =   "frmManual.frx":4D60
         Height          =   555
         Left            =   8310
         Picture         =   "frmManual.frx":506A
         Style           =   1  'Graphical
         TabIndex        =   43
         ToolTipText     =   "Voltar para Cadastro"
         Top             =   6885
         Width           =   750
      End
      Begin MSGrid.Grid grdConsulta 
         Height          =   6900
         Left            =   150
         TabIndex        =   42
         Top             =   510
         Width           =   7815
         _Version        =   65536
         _ExtentX        =   13785
         _ExtentY        =   12171
         _StockProps     =   77
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
End
Attribute VB_Name = "frmManual"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private vFl_Gr As Boolean    'Vai indicar se o usu�rio escolheu algum grupo
Private vFl_SubGr As Boolean 'Vai indicar se o usu�rio escolheu algum subgrupo
Private vFl_Lin As Boolean   'Vai indicar se o usu�rio escolheu alguma linha
Private vGuarda_Lin As Integer
Private vStr_SubGr As String, vStr_Lin As String
Private vFl_Preenchimento As Boolean    'Vai indicar se campos est�o preenchidos para formar filtro de consulta
Sub pVerifica_Filtros()
Dim I As Integer
Dim vAux As Byte
    

'------------------------------------------------------------------------
'Verifica se alguma linha foi marcada, para setar flag de filtro
'------------------------------------------------------------------------
grdLinha.Col = 1
vAux = 0
vStr_Lin = ""
vFl_Lin = False
For I = 1 To grdLinha.Rows - 1
    grdLinha.Row = I
    grdLinha.Col = 1
    If Trim(grdLinha.Text) = "X" Then
        vFl_Lin = True
        grdLinha.Col = 0
        If vAux = 0 Then
            vStr_Lin = "("
            vStr_Lin = vStr_Lin & Mid(Trim(grdLinha.Text), 1, 2)
            vAux = 1
        Else
            vStr_Lin = vStr_Lin & ", " & Mid(Trim(grdLinha.Text), 1, 2)
        End If
    End If
    If (I = grdLinha.Rows - 1) And vStr_Lin <> "" Then
        vStr_Lin = vStr_Lin & ")"
    End If
Next I

   
'------------------------------------------------------------------------
'Verifica Grupo
'------------------------------------------------------------------------
vFl_Gr = False
If Trim(cboGrupo.Text) <> "" Then
    vFl_Gr = True
End If

'------------------------------------------------------------------------
'Verifica se algum subgrupo foi marcado, para setar flag de filtro
'------------------------------------------------------------------------
grdSubGrupo.Col = 1
vAux = 0
vStr_SubGr = ""
vFl_SubGr = False
For I = 1 To grdSubGrupo.Rows - 1
    grdSubGrupo.Row = I
    grdSubGrupo.Col = 1
    If Trim(grdSubGrupo.Text) = "X" Then
        grdSubGrupo.Col = 0
        vFl_SubGr = True
        If vAux = 0 Then
            vStr_SubGr = "("
            vStr_SubGr = vStr_SubGr & Mid(Trim(grdSubGrupo.Text), 1, 2)
            vAux = 1
        Else
            vStr_SubGr = vStr_SubGr & ", " & Mid(Trim(grdSubGrupo.Text), 1, 2)
        End If
    End If
    If (I = grdSubGrupo.Rows - 1) And vStr_SubGr <> "" Then
        vStr_SubGr = vStr_SubGr & ")"
    End If
Next I

'------------------------------------------------------------------------
'Se n�o foi digitado um DPK ou Forn/F�brica, precisa pelo menos estar com fornecedor digitado
'------------------------------------------------------------------------
If (Trim(txtCod_DPK.Text) = "" And Trim(cboFornecedor.Text) = "" And vStr_Lin = "") Or _
    (vFl_Gr = True And Trim(cboFornecedor.Text) = "") Then
    MsgBox "� preciso escolher DPK, Fornecedor ou Linha(s)", vbCritical, "CAD310 - Aten��o!"
    vFl_Preenchimento = False
    cboFornecedor.SetFocus
End If


End Sub
Sub pLimpar()
    
    lblCod_Fornecedor = ""
    cboFornecedor.Clear
    txtCod_DPK = ""
    txtCod_DPK.Enabled = True
    cboCod_Fabrica.Clear
    lblDesc_Item = ""
    lblCod_Tributacao_IPI = ""
    lblCod_Tributacao = ""
    txtUF = ""
    lblICM = ""
    txtDescontoUnico = ""
    txtDesconto = ""
    
    With grdUF '------- MONTA GRID --------
        .Rows = 2
        .Row = 1
        For iColuna = 0 To .Cols - 1
            .Col = iColuna:   .Text = ""
        Next iColuna
    End With

    'Deixa painel de grupo e linha, habilitados
    spnlGrupo.Visible = True
    spnlLinha.Visible = True

End Sub

Sub pCarregaDados()

Dim lstrSQL  As String, objItem     As Object, objUF As Object
Dim objBanco As Object, objEndereco As Object, liitem As Integer
Dim objEDI As Object, lstrUF As String
Dim objDesc As Object

lstrWHERE = ""

If Trim(txtCod_DPK) = "" Then
    If lblCod_Fornecedor = "" Then
        Screen.MousePointer = vbDefault
        'Deixa painel de grupo e linha, habilitados
        spnlGrupo.Visible = True
        spnlLinha.Visible = True
        Exit Sub
    End If
End If

If Trim(cboCod_Fabrica) = "" And Trim(txtCod_DPK) = "" Then
    Screen.MousePointer = vbDefault
    'Deixa painel de grupo e linha, habilitados
    spnlGrupo.Visible = True
    spnlLinha.Visible = True
    cboUfOrigem.SetFocus
    Exit Sub
End If

Screen.MousePointer = vbHourglass

lstrSQL = "SELECT c.Cod_fabrica, c.Cod_Dpk, c.Desc_Item, e.situacao, c.desc_item," & _
                " c.Cod_Tributacao, c.Cod_Tributacao_Ipi, f.cod_fornecedor, f.sigla " & _
                " ,c.cod_grupo, c.cod_subgrupo " & _
           " FROM item_cadastro c , fornecedor f, item_estoque e " & _
          " WHERE f.cod_fornecedor = c.cod_fornecedor " & _
            " AND e.cod_loja = 1 " & _
            " AND e.cod_dpk = c.cod_dpk " & _
            " AND e.situacao = 0 "

If Trim(cboCod_Fabrica) <> "" Then
    
    If Left(cboCod_Fabrica, 1) = "%" Or Right(cboCod_Fabrica, 1) = "%" Then
        lstrSQL = lstrSQL & " AND c.cod_fabrica LIKE :cod_fabrica and f.cod_fornecedor=:cod_fornecedor "
    Else
        lstrSQL = lstrSQL & " AND c.cod_fabrica = :cod_fabrica and f.cod_fornecedor=:cod_fornecedor "
    End If
    
    oradatabase.Parameters.Remove "cod_fornecedor": oradatabase.Parameters.Add "cod_fornecedor", lblCod_Fornecedor, 1
    oradatabase.Parameters.Remove "cod_fabrica": oradatabase.Parameters.Add "cod_fabrica", cboCod_Fabrica, 1

Else
    
    lstrSQL = lstrSQL & " AND c.cod_dpk = :cod_dpk"
    
    oradatabase.Parameters.Remove "cod_dpk": oradatabase.Parameters.Add "cod_dpk", txtCod_DPK, 1

End If

On Error Resume Next

sspMsg = "PESQUISANDO . . ."

Set objItem = oradatabase.dbCreateDynaset(lstrSQL, 0&)

If objItem.EOF Then
    
    Screen.MousePointer = vbDefault
    
    If Trim(txtCod_DPK) <> "" Then
        MsgBox "Item n�o encontrado !", vbInformation, "Aten��o"
    End If
    Exit Sub

End If

If objItem.RecordCount > 1 Then
    
    cboCod_Fabrica.Clear
    
    While Not objItem.EOF
        sspMsg = "PESQUISANDO . . ." & cboCod_Fabrica.ListCount
        cboCod_Fabrica.AddItem objItem.Fields("cod_fabrica") & Space(19 - Len(objItem.Fields("cod_fabrica"))) & "-" & objItem.Fields("Desc_Item")
        objItem.MoveNext
    Wend
    
    MsgBox "Foram selecionados v�rios C�digos de F�brica. Seleciona um !!!", vbInformation, "Aten��o"

Else
    
    Call pLimpar
    
    On Error Resume Next
    
    lblCod_Fornecedor = objItem.Fields("cod_fornecedor")
    cboFornecedor = objItem.Fields("sigla")
    liInclusao = False
    cboCod_Fabrica = objItem.Fields("cod_fabrica")
    txtCod_DPK = objItem.Fields("cod_dpk")
    txtCod_DPK.Enabled = False
    lblDesc_Item = objItem.Fields("desc_item")
    
    Select Case objItem.Fields("cod_tributacao")
        Case 0: lblCod_Tributacao = "0 - Tributado Integralmente"
        Case 1: lblCod_Tributacao = "1 - Subst. Tribut�ria"
        Case 4: lblCod_Tributacao = "4 - Isento"
        Case 8: lblCod_Tributacao = "8 - Aliq. Reduzida"
    End Select
    
    Select Case objItem.Fields("cod_tributacao_ipi")
        Case 0: lblCod_Tributacao_IPI = "0 - N�o Incide"
        Case 1: lblCod_Tributacao_IPI = "1 - Tributado"
    End Select
    
    lstrSQL = "SELECT cod_uf " & _
                "FROM uf "
    
    lstrUF = cboUfOrigem
    
    cboUfOrigem.Clear
    Set objUF = oradatabase.dbCreateDynaset(lstrSQL, 0&)
    
    While Not objUF.EOF
        cboUfOrigem.AddItem objUF.Fields(0)
        objUF.MoveNext
    Wend
    
    cboUfOrigem = lstrUF

End If

'Deixa painel de grupo e linha, desabilitados
cboGrupo.Clear
Call LIMPA_GRID(grdSubGrupo)
Call LIMPA_GRID(grdLinha)
spnlGrupo.Visible = False
spnlLinha.Visible = False

Screen.MousePointer = vbDefault

End Sub

Sub pCarregaComboFornecedor(lcboFornecedor As ComboBox, lstrParametro As String, lblCod_Fornecedor As Label)
Dim lstrSQL      As String
Dim lstrWHERE    As String
Dim objFornecedor    As Object
Dim lstrFornecedor   As String

lstrWHERE = ""
lcboFornecedor.Clear
lstrParametro = UCase(lstrParametro)

'NOME OU CODIGO
If Trim(lstrParametro) <> "" Then
    
    If IsNumeric(Trim(lstrParametro)) Then
        
        If Len(Trim(lstrParametro)) > 3 Then ' cgc=14
            oradatabase.Parameters.Remove "cgc": oradatabase.Parameters.Add "cgc", lstrParametro, 1
            lstrWHERE = lstrWHERE & " cgc=:cgc"
        Else ' codigo=3
            oradatabase.Parameters.Remove "cod_Fornecedor": oradatabase.Parameters.Add "cod_Fornecedor", lstrParametro, 1
            lstrWHERE = lstrWHERE & " cod_Fornecedor =:cod_Fornecedor "
        End If
    
    Else
        
        'SE NAO FOR NUMERICO E NAO TIVER '%'
        If Right(lstrParametro, 1) <> "%" And Left(lstrParametro, 1) <> "%" Then
            lcboFornecedor.Text = lstrParametro
            Exit Sub
        End If
        
        oradatabase.Parameters.Remove "sigla": oradatabase.Parameters.Add "sigla", lstrParametro, 1
        lstrWHERE = lstrWHERE & " sigla" & IIf(Left(lstrParametro, 1) = "%" Or Right(lstrParametro, 1) = "%", " LIKE ", "=") & " :sigla "
    
    End If
End If

lstrSQL = "SELECT cod_Fornecedor, sigla, divisao, classificacao " & _
            "FROM Fornecedor"
lstrSQL = lstrSQL & IIf(lstrWHERE = "", "", " WHERE " & lstrWHERE & " AND (classificacao='C' or classificacao= 'A')and Situacao = 0")

Set objFornecedor = oradatabase.dbCreateDynaset(lstrSQL, 0&)

lcboFornecedor.Clear
Screen.MousePointer = vbHourglass

While Not objFornecedor.EOF
    'sspMsg.Caption = "PESQUISANDO FORNECEDORES . . . " & lcboFornecedor.ListCount + 1
    lcboFornecedor.AddItem objFornecedor.Fields("sigla").Value & " - " & Format(objFornecedor.Fields("cod_Fornecedor").Value, "000")
    lstrFornecedor = objFornecedor.Fields("sigla").Value & " - " & Format(objFornecedor.Fields("cod_Fornecedor").Value, "000")
    objFornecedor.MoveNext
Wend

Screen.MousePointer = vbDefault

If objFornecedor.RecordCount <= 1 Then
    If objFornecedor.RecordCount = 0 Then
        MsgBox "Fornecedor n�o Encontrado ou classificacao diferente de A e C!", vbInformation, "Aten��o"
        lcboFornecedor.Text = Left(lstrParametro, 30)
    Else
        objFornecedor.MoveFirst
        lcboFornecedor.Text = objFornecedor.Fields("sigla")
        lblCod_Fornecedor = objFornecedor.Fields("cod_fornecedor")
    End If
Else
    MsgBox "Encontrei mais de um Fornecedor." & Chr(10) & "Selecione um !", vbInformation, "Aten��o"
End If

End Sub


Private Sub cboCod_Fabrica_Change()
    If cboCod_Fabrica <> "" Then
        txtCod_DPK.Enabled = False
    Else
        txtCod_DPK.Enabled = True
    End If
End Sub

Private Sub cboCod_Fabrica_Click()
    cboCod_Fabrica = Trim(Left(cboCod_Fabrica, 19))
End Sub


Private Sub cboCod_Fabrica_KeyPress(KeyAscii As Integer)
    If Len(cboCod_Fabrica) >= 19 And InStr(1, "/ABCDEFGHIJLMNOPQRSTUVW -.;,*XZY1234567890", Chr(KeyAscii)) <> 0 Then
        KeyAscii = 0
    End If
    KeyAscii = Maiusculo(KeyAscii)
End Sub

Private Sub cboCod_Fabrica_LostFocus()
    On Error Resume Next
    cboCod_Fabrica = UCase(Left(cboCod_Fabrica, 19))
    Call pCarregaDados

End Sub

Private Sub cboFornecedor_Click()
Dim lstrAuxiliar As String

lstrAuxiliar = Right(cboFornecedor, 3)
cboFornecedor = Left(cboFornecedor, Len(cboFornecedor) - 6)
lblCod_Fornecedor = lstrAuxiliar

End Sub

Private Sub cboFornecedor_GotFocus()
    Call pLimpar

End Sub


Private Sub cboFornecedor_LostFocus()

cboFornecedor = UCase(Left(cboFornecedor, 30))

If cboFornecedor <> "" Then
        Call pCarregaComboFornecedor(cboFornecedor, cboFornecedor.Text, lblCod_Fornecedor)
End If

'Vai carregar a combo de grupo, depois que escolher o fornecedor - Alethea - 05/05/06
oradatabase.Parameters.Remove "CODFORN": oradatabase.Parameters.Add "CODFORN", Val(lblCod_Fornecedor), 1
lstrSQL = "SELECT DISTINCT C.COD_GRUPO, G.DESC_GRUPO " & _
            "FROM PRODUCAO.ITEM_ESTOQUE E, PRODUCAO.ITEM_CADASTRO C, PRODUCAO.GRUPO G " & _
           "WHERE E.COD_DPK = C.COD_DPK " & _
             "AND C.COD_GRUPO = G.COD_GRUPO " & _
             "AND E.SITUACAO = 0 " & _
             "AND E.COD_LOJA = 1 "
If Val(lblCod_Fornecedor) <> 0 Then
    lstrSQL = lstrSQL & "AND C.COD_FORNECEDOR = :CODFORN "
End If
lstrSQL = lstrSQL & "ORDER BY C.COD_GRUPO"

Set objGrupo = oradatabase.dbCreateDynaset(lstrSQL, 0&)

cboGrupo.Clear
cboGrupo.AddItem "00 - TODOS"
While Not objGrupo.EOF
    cboGrupo.AddItem Format(objGrupo.Fields(0).Value, "00") & " - " & _
                     objGrupo.Fields(1).Value
    objGrupo.MoveNext
Wend


'Vai carregar o grid de linha, depois que escolher o fornecedor - Alethea - 05/05/06
oradatabase.Parameters.Remove "CODFORN": oradatabase.Parameters.Add "CODFORN", Val(lblCod_Fornecedor), 1
lstrSQL = "SELECT DISTINCT C.COD_LINHA, L.DESC_LINHA " & _
            "FROM PRODUCAO.ITEM_ESTOQUE E, PRODUCAO.ITEM_CADASTRO C, PRODUCAO.LINHA L " & _
           "WHERE E.COD_DPK = C.COD_DPK " & _
             "AND C.COD_LINHA = L.COD_LINHA " & _
             "AND E.SITUACAO = 0 " & _
             "AND E.COD_LOJA = 1 "
If Val(lblCod_Fornecedor) <> 0 Then
    lstrSQL = lstrSQL & "AND C.COD_FORNECEDOR = :CODFORN "
End If
lstrSQL = lstrSQL & "ORDER BY C.COD_LINHA"
'lstrSQL = "Select * " & _
'            "From Producao.Linha " & _
'    "Order By Cod_Linha"
Set objLinha = oradatabase.dbCreateDynaset(lstrSQL, 0&)

Call LIMPA_GRID(grdLinha)

If objLinha.EOF Then
    MsgBox "N�o existem linhas cadastradas!", vbInformation, "CAD310 - Aten��o!"
Else
    'Monta Estrutura Grid Linha
    With grdLinha
        .Cols = 2
        .Rows = 2
        .Row = 1
        
        For I = 0 To .Cols - 1
            .Col = I: .Text = ""
        Next I
        
        .Row = 0
        .Col = 0: .Text = "Linha"
        .FixedAlignment(0) = 2:
        .ColAlignment(0) = 0
        .ColWidth(0) = 2600
        
        .Col = 1: .Text = "Marcar"
        .FixedAlignment(1) = 2:
        .ColAlignment(1) = 2
        .ColWidth(1) = 800
    End With
    
    For I = 1 To objLinha.RecordCount
        grdLinha.Rows = I + 1
        grdLinha.Row = I
        
        grdLinha.Col = 0
        grdLinha.Text = Format(objLinha!Cod_Linha.Value, "00") & " - " & objLinha!Desc_Linha.Value
        
        objLinha.MoveNext
    Next I

End If




End Sub

Private Sub cboGrupo_Click()
Dim cmdSQL As String
Dim objSubGr As Object
Dim I As Integer

'Monta Estrutura do Grid de SubGrupo
Call LIMPA_GRID(grdSubGrupo)
With grdSubGrupo
    .Cols = 2
    .Row = 0
    .Col = 0: .Text = "SubGrupo": .ColWidth(0) = 2600: .FixedAlignment(0) = 2
    .Col = 1: .Text = "Marcar": .ColWidth(1) = 600: .FixedAlignment(1) = 2
    .ColAlignment(1) = 2
End With

If Trim(cboGrupo.Text) = "00 - TODOS" Then
    
    grdSubGrupo.Col = 0
    grdSubGrupo.Row = 1
    grdSubGrupo.Text = "00 - TODOS"
    grdSubGrupo.Enabled = False
   
Else

    grdSubGrupo.Enabled = True
    
    'ALTERAR ESTE FILTRO PARA TRAZER DE ACORDO COM ITENS CADASTRADOS NO NIVEL FORNECEDOR E GRUPO ESCOLHIDOS - ALETHEA - 05/05/06
    oradatabase.Parameters.Remove "CODGRUPO": oradatabase.Parameters.Add "CODGRUPO", Val(cboGrupo), 1
    oradatabase.Parameters.Remove "CODFORN":  oradatabase.Parameters.Add "CODFORN", Val(lblCod_Fornecedor), 1
    
    'cmdSQL = "Select * " & _
               "From Producao.Subgrupo " & _
              "Where Cod_Grupo = :CODGRUPO " & _
           "Order By Cod_SubGrupo"
    
    cmdSQL = "SELECT DISTINCT C.COD_SUBGRUPO, S.DESC_SUBGRUPO " & _
               "FROM ITEM_ESTOQUE E, ITEM_CADASTRO C, SUBGRUPO S, GRUPO G " & _
              "WHERE E.COD_DPK = C.COD_DPK " & _
                "AND C.COD_GRUPO = S.COD_GRUPO " & _
                "AND C.COD_GRUPO = G.COD_GRUPO " & _
                "AND C.COD_SUBGRUPO = S.COD_SUBGRUPO " & _
                "AND E.SITUACAO = 0 " & _
                "AND E.COD_LOJA = 1 " & _
                "AND C.COD_GRUPO = :CODGRUPO " & _
                "AND C.COD_FORNECEDOR = :CODFORN " & _
                "ORDER BY C.COD_SUBGRUPO "
    
    Set objSubGr = oradatabase.dbCreateDynaset(cmdSQL, 0&)
    
    If objSubGr.EOF Then
        MsgBox "N�o existem subgrupos cadastrados para este grupo!", vbInformation, "CAD310 - Aten��o!"
        Exit Sub
    End If
    
    'Primeira linha do grid, ser� de SubGrupo TODOS
    'grdSubGrupo.Row = 1
    'grdSubGrupo.Text = "00 - TODOS"
    
    grdSubGrupo.Col = 0
    For I = 1 To objSubGr.RecordCount
        grdSubGrupo.Rows = I + 1
        grdSubGrupo.Row = I
        grdSubGrupo.Text = Format(objSubGr!Cod_SubGrupo.Value, "00") & _
                            " - " & objSubGr!Desc_SubGrupo.Value
        objSubGr.MoveNext
    Next I

End If
End Sub


Private Sub cmd_Click()

End Sub

Private Sub cmdCarregaUF_Click()
Dim liLinha As Integer
Dim objUF As Object
Dim lstrSQL As String
    
If Trim(cboUfOrigem) = "" Then
    Exit Sub
End If
    
   
If Trim(txtCod_DPK) <> "" Then
    
    Call pVerifica_Filtros
    
    oradatabase.Parameters.Remove "cod_uf_origem": oradatabase.Parameters.Add "cod_uf_origem", cboUfOrigem, 1
    oradatabase.Parameters.Remove "cod_dpk": oradatabase.Parameters.Add "cod_dpk", txtCod_DPK, 1
    
    lstrSQL = "SELECT Decode(ud.pc_desc,null,' ','X'), uf.cod_uf , " & _
                    " uod.pc_icm , Nvl(ud.pc_desc,0) desconto " & _
                "FROM uf, uf_origem_destino uod, vendas.uf_dpk ud " & _
               "WHERE (uod.cod_uf_destino(+) = uf.cod_uf " & _
                 "AND uod.cod_uf_origem(+) = :cod_uf_origem ) " & _
                 "AND (ud.cod_uf_destino(+) = uf.cod_uf " & _
                 "AND ud.cod_uf_origem(+) = :cod_uf_origem " & _
                 "AND ud.cod_dpk(+) = :cod_dpk) " & _
            "ORDER BY uf.cod_uf "
        
    Set objUF = oradatabase.dbCreateDynaset(lstrSQL, 0&)
        
    Screen.MousePointer = vbHourglass
    Call LIMPA_GRID(grdUF)
    
    If objUF.EOF Then
        MsgBox "N�o existem descontos cadastrados para esta UF Origem!", vbInformation, "CAD310 - Aten��o"
    Else
        liLinha = 0
        While Not objUF.EOF
            liLinha = liLinha + 1
            'sspMsg.Caption = "PESQUISANDO DESCONTOS . . ." & liLinha
            grdUF.Rows = liLinha + 1
            grdUF.Row = liLinha
            grdUF.Col = 0:    grdUF.Text = objUF.Fields(0)
            grdUF.Col = 1:    grdUF.Text = objUF.Fields("cod_uf")
            grdUF.Col = 2:    grdUF.Text = Format(objUF.Fields("pc_icm").Value, "0.00")
            grdUF.Col = 3:    grdUF.Text = Format(objUF.Fields("desconto").Value, "0.00")
            objUF.MoveNext
        Wend
    End If

'Se campo de DPK estiver em branco, � porque o cadastro ser� mais gen�rico, respeitando os filtros
Else

    Call pVerifica_Filtros
        
    oradatabase.Parameters.Remove "cod_uf_origem": oradatabase.Parameters.Add "cod_uf_origem", cboUfOrigem, 1
    
    lstrSQL = "SELECT ' ', uf.cod_uf, uod.pc_icm, 0 Desconto " & _
                "FROM Producao.UF, Producao.UF_Origem_Destino uod " & _
               "WHERE (uod.cod_uf_destino(+) = uf.cod_uf " & _
                 "AND uod.cod_uf_origem(+) = :cod_uf_origem) " & _
            "ORDER BY uf.cod_uf"
        
    Set objUF = oradatabase.dbCreateDynaset(lstrSQL, 0&)
        
    Screen.MousePointer = vbHourglass
    Call LIMPA_GRID(grdUF)
    
    If objUF.EOF Then
        MsgBox "N�o existem descontos cadastrados para esta UF Origem!", vbInformation, "CAD310 - Aten��o"
    Else
        liLinha = 0
        While Not objUF.EOF
            liLinha = liLinha + 1
            grdUF.Rows = liLinha + 1
            grdUF.Row = liLinha
            grdUF.Col = 0:    grdUF.Text = objUF.Fields(0)
            grdUF.Col = 1:    grdUF.Text = objUF.Fields("cod_uf")
            grdUF.Col = 2:    grdUF.Text = Format(objUF.Fields("pc_icm").Value, "0.00")
            grdUF.Col = 3:    grdUF.Text = Format(objUF.Fields("desconto").Value, "0.00")
            objUF.MoveNext
        Wend
    End If

End If

Screen.MousePointer = vbDefault
End Sub


Private Sub cmdConsultar_Click()
Dim lstrWHERE As String, cmdSQL As String
Dim objItens As Object
Dim J As Long
    
lstrWHERE = ""
    
    If Trim(txtCod_DPK) <> "" Then
        
        oradatabase.Parameters.Remove "COD_DPK": oradatabase.Parameters.Add "COD_DPK", txtCod_DPK.Text, 1
        lstrWHERE = lstrWHERE & _
                    "And C.Cod_Dpk = :COD_DPK "
    End If
        
    vFl_Preenchimento = True
    Call pVerifica_Filtros

    If vFl_Preenchimento = False Then
        MsgBox "� preciso escolher algum dos filtros!", vbCritical, "CAD310 - Aten��o!"
        Exit Sub
    End If
    
    'De acordo com filtros escolhidos, vou montar a cl�usula Where
    If Trim(cboFornecedor.Text) <> "" Then
        oradatabase.Parameters.Remove "CODFORN": oradatabase.Parameters.Add "CODFORN", Val(lblCod_Fornecedor.Caption), 1
        lstrWHERE = lstrWHERE & _
                    "And C.Cod_Fornecedor = :CODFORN "
    End If
    If Trim(cboGrupo.Text) <> "00 - TODOS" And Trim(cboGrupo.Text) <> "" Then
        oradatabase.Parameters.Remove "CODGRUPO": oradatabase.Parameters.Add "CODGRUPO", Val(cboGrupo.Text), 1
        lstrWHERE = lstrWHERE & _
            " AND C.Cod_Grupo = :CODGRUPO "
    End If
    If vFl_SubGr = True And Trim(vStr_SubGr) <> "" Then
        lstrWHERE = lstrWHERE & _
            " AND C.Cod_SubGrupo in " & vStr_SubGr
    End If
    If vFl_Lin = True And Trim(vStr_Lin) <> "" Then
        lstrWHERE = lstrWHERE & _
            " AND C.Cod_Linha in " & vStr_Lin
    End If
    If cboUfOrigem.Text <> "" Then
        lstrWHERE = lstrWHERE & _
            " AND U.Cod_Uf_Origem = '" & CStr(Trim(cboUfOrigem.Text)) & "'"
    End If
          
      
    'Vou selecionar todos os descontos dos DPKs que respeitarem os filtros
    cmdSQL = "Select U.Cod_Uf_Origem UFORI, " & _
                   " U.Cod_Uf_Destino UFDES, " & _
                   " U.Cod_Dpk DPK, " & _
                   " U.Pc_Desc PCDESC, " & _
                   " UOD.Pc_Icm PCICM, " & _
                   " C.Cod_Tributacao TRIBICM, " & _
                   " C.Cod_Tributacao_Ipi TRIBIPI "
    cmdSQL = cmdSQL & _
               "From Producao.Item_Cadastro C, " & _
                   " Producao.Item_Estoque E, " & _
                   " Vendas.Uf_Dpk U, " & _
                   " Producao.Uf_Origem_Destino UOD " & _
              "Where E.Situacao = 0 " & _
                "And E.Cod_Loja = 1 " & _
                "And E.Cod_Dpk = C.Cod_Dpk " & _
                "And C.Cod_Dpk = U.Cod_Dpk " & _
                "And UOD.Cod_Uf_Origem(+) = U.Cod_Uf_Origem " & _
                "And UOD.Cod_Uf_Destino(+) = U.Cod_Uf_Destino "
    cmdSQL = cmdSQL & lstrWHERE & _
        " Order By U.Cod_Dpk, U.Cod_Uf_Origem, U.Cod_Uf_Destino"

    
    Set objItens = oradatabase.dbCreateDynaset(cmdSQL, 0&)
        
    If objItens.EOF Then
        MsgBox "N�o existem itens cadastrados para estes filtros!", vbInformation, "CAD310 - Aten��o!"
        Exit Sub
    End If
    
    fraCadastro.Visible = False
    fraConsulta.Visible = True
    
    With grdConsulta
        .Cols = 7
        .Row = 0
        .Col = 0: .Text = "UF Origem":  .ColWidth(0) = 1000: .FixedAlignment(0) = 2
        '.ColAlignment(0) = 2
        .Col = 1: .Text = "UF Destino": .ColWidth(1) = 1000: .FixedAlignment(1) = 2
        .ColAlignment(1) = 2
        'carlosj 08/08/2012 CiT - [ISA 192]
        '.Col = 2: .Text = "DPK":        .ColWidth(2) = 550:  .FixedAlignment(2) = 2
         .Col = 2: .Text = "DPK":        .ColWidth(2) = 1100:  .FixedAlignment(2) = 2
        .ColAlignment(2) = 0
        .Col = 3: .Text = "Desconto":   .ColWidth(3) = 1000: .FixedAlignment(3) = 2
        .ColAlignment(3) = 1
        .Col = 4: .Text = "Pc.ICM":     .ColWidth(4) = 1000: .FixedAlignment(4) = 2
        .ColAlignment(4) = 1
        .Col = 5: .Text = "Cod.Trib.ICM":     .ColWidth(5) = 1000: .FixedAlignment(5) = 2
        .ColAlignment(5) = 1
        .Col = 6: .Text = "Cod.Trib.IPI":     .ColWidth(6) = 1000: .FixedAlignment(6) = 2
        .ColAlignment(6) = 1
    End With
    
    Dim resposta As String
    
    
    If objItens.RecordCount >= 16352 Then
    
        resposta = MsgBox("O resultado n�o pode ser exibido, pois a consulta ultrapassou o limite de 16352 linhas. Deseja exportar para arquivo ?", vbYesNo, "Exportar")
        If resposta = vbYes Then
            ExportarArquivo objItens
            Exit Sub
        Else
            Exit Sub
        End If
    
    
    End If
    
    
    
    For J = 1 To objItens.RecordCount
               
        With grdConsulta
            .Rows = J + 1
            .Row = J
            .Col = 0
            .Text = objItens!UFORI.Value
            .Col = 1
            .Text = objItens!UFDES.Value
            .Col = 2
            'carlosj 08/08/2012 CiT - [ISA 192]
            .Text = Format(objItens!DPK.Value, "000000000")
            .Col = 3
            .Text = Format(objItens!PCDESC.Value, "#0.00")
            .Col = 4
            .Text = Format(objItens!PCICM.Value, "#0.00")
            .Col = 5
            .Text = Format(objItens!TRIBICM.Value, "#0.00")
            .Col = 6
            .Text = Format(objItens!TRIBIPI.Value, "#0.00")
        End With

        objItens.MoveNext

    Next J
    
        resposta = MsgBox("Deseja exportar o resultado em arquivo ?", vbYesNo, "Exportar")
        If resposta = vbYes Then
            objItens.MoveFirst
            ExportarArquivo objItens
        End If

End Sub


Private Sub ExportarArquivo(objItens As Object)
Dim intX As Double
Dim strConteudo As String
Dim vArq As Integer

vArq = FreeFile
Dim strArquivo
strArquivo = "C:\CAD310" & Format(Now, "DDMMYYYHHMMSS") & ".TXT"
'vArq = FreeFile

Open strArquivo For Output As #vArq

Print #vArq, "UF Origem;UF Destino;DPK;Desconto;Pc.ICM;Cod.Trib.ICM;Cod.Trib.IPI"

For intX = 1 To objItens.RecordCount
   'carlosj 08/08/2012 CiT - [ISA 192]
   ' Print #vArq, objItens!UFORI.Value & ";" & objItens!UFDES.Value & ";" & Format(objItens!DPK.Value, "00000") & ";" & Format(objItens!PCDESC.Value, "#0.00") & ";" & Format(objItens!PCICM.Value, "#0.00") & ";" & Format(objItens!TRIBICM.Value, "#0.00") & ";" & Format(objItens!TRIBIPI.Value, "#0.00")
     Print #vArq, objItens!UFORI.Value & ";" & objItens!UFDES.Value & ";" & Format(objItens!DPK.Value, "000000000000000000") & ";" & Format(objItens!PCDESC.Value, "#0.00") & ";" & Format(objItens!PCICM.Value, "#0.00") & ";" & Format(objItens!TRIBICM.Value, "#0.00") & ";" & Format(objItens!TRIBIPI.Value, "#0.00")
    objItens.MoveNext
Next
    
    Close #1
    MsgBox ("Arquivo gerado com sucesso:" & strArquivo)

End Sub


Private Sub cmdDuplUF_Click()
    FRMCOPIA.Show 1
End Sub

Private Sub cmdGravarEspec_Click()

Dim lstrUPDATE As String, lstrSQL As String
Dim lstrWHERE As String, cmdSQL As String
Dim liLinha As Integer, liColuna As Integer
Dim objDesc As Object, objItens As Object
Dim vMsg As String, J As Long

If Trim(cboUfOrigem) = "" Then
    MsgBox "UF Origem inv�lido!", vbCritical, "Aten��o"
    Exit Sub
End If

liLinha = 0

'Pede confirma��o do usu�rio para efetuar transa��o
If Trim(txtCod_DPK.Text) <> "" Then
    vMsg = "Confirma a Altera��o dos DESCONTOS ESPEC�FICOS?" & Chr(13) & _
           "Todas as UFs Destino (marcadas) para o DPK " & txtCod_DPK & " ser�o alteradas."
Else
    If vFl_Gr = True Or vFl_SubGr = True Or vFl_Lin = True Then
        vMsg = "Confirma a Altera��o dos DESCONTOS ESPEC�FICOS?" & Chr(13) & _
               "Todas as UFs Destino de todos os DPKs que respeitarem os filtros acima ser�o alteradas."
    Else
        MsgBox "N�o existem itens selecionados para serem alterados!", vbInformation, "CAD310 - Aten��o!"
        Exit Sub
    End If
End If
    
    
'Se usu�rio confirmou a altera��o dos descontos espec�ficos
If MsgBox(vMsg, vbYesNo + vbQuestion, "Aten��o") = vbYes Then

    'Vari�veis de bind que ser�o gravadas para todos os registros
    With oradatabase.Parameters
        .Remove "cod_uf_origem": .Add "cod_uf_origem", CStr(cboUfOrigem.Text), 1
        .Remove "cod_errora":    .Add "cod_errora", 0, 2
        .Remove "txt_errora":    .Add "txt_errora", "", 2
    End With
    
    
    'Se foi escolhido um DPK espec�fico, vou alterar o desconto para todas as UFs destino, mas somente para este DPK
    If Trim(txtCod_DPK.Text) <> "" Then

        oradatabase.Parameters.Remove "cod_dpk": oradatabase.Parameters.Add "cod_dpk", Val(txtCod_DPK.Text), 1
        
        'Seleciona todas as linhas, varrendo grid, para gravar Desconto x UF Destino
        liLinha = 0
        For J = 1 To grdUF.Rows - 1
        
            grdUF.Row = J
            
            'Vai gravar somente as linhas do grid que estiverem marcadas com um "X"
            grdUF.Col = 0
            If Trim(grdUF.Text) = "X" Then
           
                'Prepara vari�veis de bind
                With oradatabase.Parameters
                    grdUF.Col = 3
                    .Remove "pc_desc" & liLinha: .Add "pc_desc" & liLinha, Val(grdUF.Text), 1
                    grdUF.Col = 1
                    .Remove "cod_uf_destino" & liLinha: .Add "cod_uf_destino" & liLinha, CStr(Trim(grdUF.Text)), 1
                End With
            
                lstrUPDATE = lstrUPDATE & _
                    " UPDATE vendas.uf_dpk " & _
                        "SET pc_desc = :pc_desc" & liLinha & " " & _
                      "WHERE cod_dpk = :cod_dpk " & _
                        "AND cod_uf_origem = :cod_uf_origem " & _
                        "AND cod_uf_destino = :cod_uf_destino" & liLinha & "; " & _
                " IF SQL%NOTFOUND THEN " & _
                    " INSERT INTO vendas.uf_dpk " & _
                           " (cod_uf_origem, cod_uf_destino, cod_dpk, pc_desc) " & _
                    " VALUES (:cod_uf_origem,:cod_uf_destino" & liLinha & ",:cod_dpk,:pc_desc" & liLinha & ");" & _
                " END IF;"
            
            
                '///////////////////////////////////////////////////////////////////////////////////////
                'Verifica as informa��es deste item, para gravar o LOG
                vLog_FL_DIF_ICM = "": vLog_TP_ALT = "": vLog_UFORI = "": vLog_UFDES = "": vLog_DPK = 0
                vLog_DESC_ANT = 0: vLog_DESC_NOVO = 0: strLOG = ""
                'Verifica a flag de diferen�a de icms no cadastro de fornecedor espec�fico
                lstrSQL = "Select Nvl(E.FL_Dif_Icms,'NULO') FL_Dif_Icms " & _
                            "From Producao.Fornecedor_Especifico E, Producao.Item_Cadastro C " & _
                           "Where E.Cod_Fornecedor in (0, C.Cod_Fornecedor) " & _
                             "And E.Cod_Grupo in (0, C.Cod_Grupo) " & _
                             "And E.Cod_SubGrupo in (0, C.Cod_SubGrupo) " & _
                             "And E.Cod_Dpk in (0, C.Cod_Dpk) " & _
                             "And C.Cod_Dpk = :COD_DPK " & _
                             "Order by E.Cod_Fornecedor, E.Cod_Grupo, E.Cod_Subgrupo, E.Cod_Dpk "
                Set objDesc2 = oradatabase.dbCreateDynaset(lstrSQL, 0&)
                If objDesc2.EOF Then
                    vLog_FL_DIF_ICM = "NULO"
                Else
                    vLog_FL_DIF_ICM = objDesc2!FL_Dif_Icms
                End If
                'Seleciona o percentual de desconto atual do item na Vendas.Uf_Dpk
                lstrSQL = "Select * " & _
                            "From Vendas.Uf_Dpk " & _
                           "Where Cod_Dpk = :COD_DPK " & _
                             "And Cod_Uf_Origem = :COD_UF_ORIGEM " & _
                             "And Cod_Uf_Destino = :COD_UF_DESTINO" & liLinha
                Set objDesc2 = oradatabase.dbCreateDynaset(lstrSQL, 0&)
                If objDesc2.EOF Then
                    vLog_UFORI = CStr(cboUfOrigem.Text)
                    grdUF.Col = 1
                    vLog_UFDES = CStr(Trim(grdUF.Text))
                    vLog_DPK = Val(txtCod_DPK.Text)
                    vLog_DESC_ANT = 0
                    grdUF.Col = 3
                    vLog_DESC_NOVO = Val(grdUF.Text)
                    vLog_TP_ALT = "I"
                Else
                    vLog_UFORI = objDesc2!Cod_Uf_Origem
                    vLog_UFDES = objDesc2!Cod_Uf_Destino
                    vLog_DPK = objDesc2!Cod_Dpk
                    vLog_DESC_ANT = objDesc2!Pc_Desc
                    grdUF.Col = 3
                    vLog_DESC_NOVO = Val(grdUF.Text)
                    vLog_TP_ALT = "A"
                End If
                'Prepara binds para gravar log
                oradatabase.Parameters.Remove "UFORI":  oradatabase.Parameters.Add "UFORI", vLog_UFORI, 1
                oradatabase.Parameters.Remove "UFDES":  oradatabase.Parameters.Add "UFDES", vLog_UFDES, 1
                oradatabase.Parameters.Remove "DPK":    oradatabase.Parameters.Add "DPK", vLog_DPK, 1
                oradatabase.Parameters.Remove "DESCANT": oradatabase.Parameters.Add "DESCANT", vLog_DESC_ANT, 1
                oradatabase.Parameters.Remove "DESC":   oradatabase.Parameters.Add "DESC", vLog_DESC_NOVO, 1
                oradatabase.Parameters.Remove "FLDIF":  oradatabase.Parameters.Add "FLDIF", vLog_FL_DIF_ICM, 1
                oradatabase.Parameters.Remove "TPALT":  oradatabase.Parameters.Add "TPALT", vLog_TP_ALT, 1
                oradatabase.Parameters.Remove "USU":    oradatabase.Parameters.Add "USU", vLog_Usu, 1
                strLOG = "Insert into Vendas.Log_Uf_Dpk " & _
                         "(Cod_Uf_Origem, Cod_Uf_Destino, Cod_Dpk, Pc_Desc_Ant, Pc_Desc, Fl_Dif_Icms, Tp_Alteracao, " & _
                         " Dt_Alteracao, Usuario) Values " & _
                         "(:UFORI, :UFDES, :DPK, :DESCANT, :DESC, :FLDIF, :TPALT, " & _
                         " To_Date(To_Char(SYSDATE,'dd/mm/rr hh24:mi:ss'),'dd/mm/rr hh24:mi:ss'), :USU); "
                strLOG = "BEGIN " & strLOG & _
                                " COMMIT;EXCEPTION WHEN OTHERS THEN ROLLBACK;" & _
                                " :cod_errora := SQLCODE;:txt_errora := SQLERRM; " & _
                             "END;"
                oradatabase.ExecuteSQL strLOG
                'VERIFICA ERROS
                If IIf(IsNull(oradatabase.Parameters("cod_errora")), 0, Val(oradatabase.Parameters("cod_errora"))) <> 0 Then
                    MsgBox oradatabase.Parameters("txt_errora") & " - Erro na gera��o do Log para esta altera��o!"
                End If
                '///////////////////////////////////////////////////////////////////////////////////////
            
            
                liLinha = liLinha + 1
            
            End If
        
        Next J
        
        lstrUPDATE = "BEGIN " & lstrUPDATE & _
                        " COMMIT;EXCEPTION WHEN OTHERS THEN ROLLBACK;" & _
                        " :cod_errora := SQLCODE;:txt_errora := SQLERRM; " & _
                     "END;"
        oradatabase.ExecuteSQL lstrUPDATE
        
        'VERIFICA ERROS
        If IIf(IsNull(oradatabase.Parameters("cod_errora")), 0, Val(oradatabase.Parameters("cod_errora"))) <> 0 Then
            MsgBox oradatabase.Parameters("txt_errora") & ".Ligue para o Depto de Sistemas"
            Exit Sub
        Else
            MsgBox "Desconto Espec�fico Gravado com sucesso !", vbInformation, "Aten��o"
            txtDesconto.Text = ""
            Call cmdCarregaUF_Click
        End If

    
    'Mas Se foi escolhido um filtro, vou alterar o desconto para todas as UFs destino, mas somente os DPKs que respeitarem os filtros
    ElseIf vFl_Gr = True Or vFl_SubGr = True Or vFl_Lin = True Then

        lstrWHERE = ""
        
        'De acordo com filtros escolhidos, vou montar a cl�usula Where
        If Trim(cboFornecedor.Text) <> "" Then
            oradatabase.Parameters.Remove "CODFORN": oradatabase.Parameters.Add "CODFORN", Val(lblCod_Fornecedor.Caption), 1
            lstrWHERE = lstrWHERE & _
                        "And C.Cod_Fornecedor = :CODFORN "
        End If
        If Trim(cboGrupo.Text) <> "00 - TODOS" And Trim(cboGrupo.Text) <> "" Then
            oradatabase.Parameters.Remove "CODGRUPO": oradatabase.Parameters.Add "CODGRUPO", Val(cboGrupo.Text), 1
            lstrWHERE = lstrWHERE & _
                " AND C.Cod_Grupo = :CODGRUPO "
        End If
        If vFl_SubGr = True And Trim(vStr_SubGr) <> "" Then
            lstrWHERE = lstrWHERE & _
                " AND C.Cod_SubGrupo in " & vStr_SubGr
        End If
        If vFl_Lin = True And Trim(vStr_Lin) <> "" Then
            lstrWHERE = lstrWHERE & _
                " AND C.Cod_Linha in " & vStr_Lin
        End If
        
          
        'Vou selecionar todos os DPKs que respeitarem os filtros
        cmdSQL = "Select C.Cod_DPK " & _
                   "From Producao.Item_Estoque E, " & _
                       " Producao.Item_Cadastro C " & _
                  "Where E.Cod_Dpk = C.Cod_Dpk " & _
                    "And E.Cod_Loja = 1 " & _
                    "And E.Situacao = 0 "
        cmdSQL = cmdSQL & lstrWHERE
        
        Set objItens = oradatabase.dbCreateDynaset(cmdSQL, 0&)
            
        If objItens.EOF Then
            MsgBox "N�o existem itens cadastrados para estes filtros!", vbInformation, "CAD310 - Aten��o!"
            Exit Sub
        End If
            
        
        While Not objItens.EOF
            
            lstrUPDATE = ""
            
            oradatabase.Parameters.Remove "cod_dpk": oradatabase.Parameters.Add "cod_dpk", Val(objItens!Cod_Dpk.Value), 1
            
            liLinha = 0
            
            'Seleciona todas as linhas, varrendo grid, para gravar Desconto x UF Destino
            For J = 1 To grdUF.Rows - 1
            
                grdUF.Row = J
                
                'Vai gravar somente as linhas do grid que estiverem marcadas com um "X"
                grdUF.Col = 0
                If Trim(grdUF.Text) = "X" Then
               
                    'Prepara vari�veis de bind
                    With oradatabase.Parameters
                        grdUF.Col = 3
                        .Remove "pc_desc" & liLinha: .Add "pc_desc" & liLinha, Val(grdUF.Text), 1
                        grdUF.Col = 1
                        .Remove "cod_uf_destino" & liLinha: .Add "cod_uf_destino" & liLinha, CStr(Trim(grdUF.Text)), 1
                    End With
            
                    lstrUPDATE = lstrUPDATE & _
                        " UPDATE vendas.uf_dpk " & _
                            "SET pc_desc = :pc_desc" & liLinha & " " & _
                          "WHERE cod_dpk = :cod_dpk " & _
                            "AND cod_uf_origem = :cod_uf_origem " & _
                            "AND cod_uf_destino = :cod_uf_destino" & liLinha & "; " & _
                    " IF SQL%NOTFOUND THEN " & _
                        " INSERT INTO vendas.uf_dpk " & _
                               " (cod_uf_origem, cod_uf_destino, cod_dpk, pc_desc) " & _
                        " VALUES (:cod_uf_origem,:cod_uf_destino" & liLinha & ",:cod_dpk,:pc_desc" & liLinha & ");" & _
                    " END IF;"
                    
                    '///////////////////////////////////////////////////////////////////////////////////////
                    'Verifica as informa��es deste item, para gravar o LOG
                    vLog_FL_DIF_ICM = "": vLog_TP_ALT = "": vLog_UFORI = "": vLog_UFDES = "": vLog_DPK = 0
                    vLog_DESC_ANT = 0: vLog_DESC_NOVO = 0: strLOG = ""
                    'Verifica a flag de diferen�a de icms no cadastro de fornecedor espec�fico
                    lstrSQL = "Select Nvl(E.FL_Dif_Icms,'NULO') FL_Dif_Icms " & _
                                "From Producao.Fornecedor_Especifico E, Producao.Item_Cadastro C " & _
                               "Where E.Cod_Fornecedor in (0, C.Cod_Fornecedor) " & _
                                 "And E.Cod_Grupo in (0, C.Cod_Grupo) " & _
                                 "And E.Cod_SubGrupo in (0, C.Cod_SubGrupo) " & _
                                 "And E.Cod_Dpk in (0, C.Cod_Dpk) " & _
                                 "And C.Cod_Dpk = :COD_DPK " & _
                                 "Order by E.Cod_Fornecedor, E.Cod_Grupo, E.Cod_Subgrupo, E.Cod_Dpk "
                    Set objDesc2 = oradatabase.dbCreateDynaset(lstrSQL, 0&)
                    If objDesc2.EOF Then
                        vLog_FL_DIF_ICM = "NULO"
                    Else
                        vLog_FL_DIF_ICM = objDesc2!FL_Dif_Icms
                    End If
                    'Seleciona o percentual de desconto atual do item na Vendas.Uf_Dpk
                    lstrSQL = "Select * " & _
                                "From Vendas.Uf_Dpk " & _
                               "Where Cod_Dpk = :COD_DPK " & _
                                 "And Cod_Uf_Origem = :COD_UF_ORIGEM " & _
                                 "And Cod_Uf_Destino = :COD_UF_DESTINO" & liLinha
                    Set objDesc2 = oradatabase.dbCreateDynaset(lstrSQL, 0&)
                    If objDesc2.EOF Then
                        vLog_UFORI = CStr(cboUfOrigem.Text)
                        grdUF.Col = 1
                        vLog_UFDES = CStr(Trim(grdUF.Text))
                        vLog_DPK = Val(objItens!Cod_Dpk.Value)
                        vLog_DESC_ANT = 0
                        grdUF.Col = 3
                        vLog_DESC_NOVO = Val(grdUF.Text)
                        vLog_TP_ALT = "I"
                    Else
                        vLog_UFORI = objDesc2!Cod_Uf_Origem
                        vLog_UFDES = objDesc2!Cod_Uf_Destino
                        vLog_DPK = objDesc2!Cod_Dpk
                        vLog_DESC_ANT = objDesc2!Pc_Desc
                        grdUF.Col = 3
                        vLog_DESC_NOVO = Val(grdUF.Text)
                        vLog_TP_ALT = "A"
                    End If
                    'Prepara binds para gravar log
                    oradatabase.Parameters.Remove "UFORI":  oradatabase.Parameters.Add "UFORI", vLog_UFORI, 1
                    oradatabase.Parameters.Remove "UFDES":  oradatabase.Parameters.Add "UFDES", vLog_UFDES, 1
                    oradatabase.Parameters.Remove "DPK":    oradatabase.Parameters.Add "DPK", vLog_DPK, 1
                    oradatabase.Parameters.Remove "DESCANT": oradatabase.Parameters.Add "DESCANT", vLog_DESC_ANT, 1
                    oradatabase.Parameters.Remove "DESC":   oradatabase.Parameters.Add "DESC", vLog_DESC_NOVO, 1
                    oradatabase.Parameters.Remove "FLDIF":  oradatabase.Parameters.Add "FLDIF", vLog_FL_DIF_ICM, 1
                    oradatabase.Parameters.Remove "TPALT":  oradatabase.Parameters.Add "TPALT", vLog_TP_ALT, 1
                    oradatabase.Parameters.Remove "USU":    oradatabase.Parameters.Add "USU", vLog_Usu, 1
                    strLOG = "Insert into Vendas.Log_Uf_Dpk " & _
                             "(Cod_Uf_Origem, Cod_Uf_Destino, Cod_Dpk, Pc_Desc_Ant, Pc_Desc, Fl_Dif_Icms, Tp_Alteracao, " & _
                             " Dt_Alteracao, Usuario) Values " & _
                             "(:UFORI, :UFDES, :DPK, :DESCANT, :DESC, :FLDIF, :TPALT, " & _
                             " To_Date(To_Char(SYSDATE,'dd/mm/rr hh24:mi:ss'),'dd/mm/rr hh24:mi:ss'), :USU); "
                    strLOG = "BEGIN " & strLOG & _
                                    " COMMIT;EXCEPTION WHEN OTHERS THEN ROLLBACK;" & _
                                    " :cod_errora := SQLCODE;:txt_errora := SQLERRM; " & _
                                 "END;"
                    oradatabase.ExecuteSQL strLOG
                    'VERIFICA ERROS
                    If IIf(IsNull(oradatabase.Parameters("cod_errora")), 0, Val(oradatabase.Parameters("cod_errora"))) <> 0 Then
                        MsgBox oradatabase.Parameters("txt_errora") & " - Erro na gera��o do Log para esta altera��o!"
                    End If
                    '///////////////////////////////////////////////////////////////////////////////////////
                    
                    
                    liLinha = liLinha + 1
            
                End If
            
            Next J
        
            lstrUPDATE = "BEGIN " & lstrUPDATE & _
                            " COMMIT;EXCEPTION WHEN OTHERS THEN ROLLBACK;" & _
                            " :cod_errora := SQLCODE;:txt_errora := SQLERRM; " & _
                         "END;"
            'Vai executar transa��o que altera desconto geral para todos DPKs que atendem filtros, para UF destino que est� sendo lida
            oradatabase.ExecuteSQL lstrUPDATE
        
            'VERIFICA ERROS
            If IIf(IsNull(oradatabase.Parameters("cod_errora")), 0, Val(oradatabase.Parameters("cod_errora"))) <> 0 Then
                MsgBox oradatabase.Parameters("txt_errora") & ".Ligue para o Depto de Sistemas"
                Exit Sub
            'Else
            '    MsgBox "Desconto Espec�fico Gravado com sucesso em todas as UFs Destino, " & Chr(13) & _
                        "para o DPK " & Format(objItens!Cod_Dpk.Value, "00000") & "!", vbInformation, "CAD310 - Aten��o!"
            End If

            'Le pr�ximo item a ser alterado
            objItens.MoveNext
        
        Wend

        MsgBox "Desconto Espec�fico Gravado com sucesso em todas as UFs Destino, " & Chr(13) & _
               "para todos os DPKs!", vbInformation, "CAD310 - Aten��o!"
        txtDesconto.Text = ""
        Call cmdCarregaUF_Click

    End If

End If

End Sub

Private Sub cmdGravarUnico_Click()
Dim lstrUPDATE As String, lstrSQL As String
Dim lstrWHERE As String, cmdSQL As String
Dim liLinha As Integer, liColuna As Integer
Dim objDesc As Object, objDesc2 As Object, objItens As Object
Dim vMsg As String

If Trim(cboUfOrigem) = "" Then
    MsgBox "UF Origem inv�lido!", vbCritical, "Aten��o"
    Exit Sub
End If

If Trim(txtDescontoUnico) = "" Then
    Exit Sub
End If

If CSng(txtDescontoUnico) > 100 Then
    MsgBox "Desconto deve ser menor que 100 !", vbCritical, "Aten��o"
    Exit Sub
End If

liLinha = 0


If Trim(txtCod_DPK.Text) <> "" Then
    vMsg = "Confirma a Altera��o do DESCONTO GERAL ?" & Chr(13) & _
           "Todas as UFs Destino para o DPK " & txtCod_DPK & " ser�o alteradas."
Else
    If vFl_Gr = True Or vFl_SubGr = True Or vFl_Lin = True Then
        vMsg = "Confirma a Altera��o do DESCONTO GERAL ?" & Chr(13) & _
               "Todas as UFs Destino de todos os DPKs que respeitarem os filtros acima ser�o alteradas."
    Else
        MsgBox "N�o existem itens selecionados para serem alterados!", vbInformation, "CAD310 - Aten��o!"
        Exit Sub
    End If
End If
    
    
'Se usu�rio confirmou a altera��o geral do desconto
If MsgBox(vMsg, vbYesNo + vbQuestion, "Aten��o") = vbYes Then

    
    'Vari�veis de bind que ser�o gravadas para todos os registros
    With oradatabase.Parameters
        .Remove "pc_desc":       .Add "pc_desc", CDbl(txtDescontoUnico.Text), 1
        .Remove "cod_uf_origem": .Add "cod_uf_origem", CStr(cboUfOrigem.Text), 1
        .Remove "cod_errora":    .Add "cod_errora", 0, 2
        .Remove "txt_errora":    .Add "txt_errora", "", 2
    End With
    
    
    'Se foi escolhido um DPK espec�fico, vou alterar o desconto para todas as UFs destino, mas somente para este DPK
    If Trim(txtCod_DPK.Text) <> "" Then

        oradatabase.Parameters.Remove "cod_dpk": oradatabase.Parameters.Add "cod_dpk", Val(txtCod_DPK.Text), 1
        
        'Seleciona todas as UFs, uma a uma, para grav�-las como UF Destino
        lstrSQL = "Select Cod_Uf " & _
                    "From Producao.UF"
        Set objDesc = oradatabase.dbCreateDynaset(lstrSQL, 0&)
        
        liLinha = 0
        
       
        While Not objDesc.EOF
            
            oradatabase.Parameters.Remove "cod_uf_destino" & liLinha: oradatabase.Parameters.Add "cod_uf_destino" & liLinha, CStr(objDesc!cod_uf), 1
            
            '///////////////////////////////////////////////////////////////////////////////////////
            'Verifica as informa��es deste item, para gravar o log
            vLog_FL_DIF_ICM = "": vLog_TP_ALT = "": vLog_UFORI = "": vLog_UFDES = "": vLog_DPK = 0
            vLog_DESC_ANT = 0: vLog_DESC_NOVO = 0: strLOG = ""
            'Verifica a flag de diferen�a de icms no cadastro de fornecedor espec�fico
            lstrSQL = "Select Nvl(E.FL_Dif_Icms,'NULO') FL_Dif_Icms " & _
                        "From Producao.Fornecedor_Especifico E, Producao.Item_Cadastro C " & _
                       "Where E.Cod_Fornecedor in (0, C.Cod_Fornecedor) " & _
                         "And E.Cod_Grupo in (0, C.Cod_Grupo) " & _
                         "And E.Cod_SubGrupo in (0, C.Cod_SubGrupo) " & _
                         "And E.Cod_Dpk in (0, C.Cod_Dpk) " & _
                         "And C.Cod_Dpk = :COD_DPK " & _
                         "Order by E.Cod_Fornecedor, E.Cod_Grupo, E.Cod_Subgrupo, E.Cod_Dpk "
            Set objDesc2 = oradatabase.dbCreateDynaset(lstrSQL, 0&)
            If objDesc2.EOF Then
                vLog_FL_DIF_ICM = "NULO"
            Else
                vLog_FL_DIF_ICM = objDesc2!FL_Dif_Icms
            End If
            'Seleciona o percentual de desconto atual do item na Vendas.Uf_Dpk
            lstrSQL = "Select * " & _
                        "From Vendas.Uf_Dpk " & _
                       "Where Cod_Dpk = :COD_DPK " & _
                         "And Cod_Uf_Origem = :COD_UF_ORIGEM " & _
                         "And Cod_Uf_Destino = :COD_UF_DESTINO" & liLinha
            Set objDesc2 = oradatabase.dbCreateDynaset(lstrSQL, 0&)
            If objDesc2.EOF Then
                vLog_UFORI = CStr(cboUfOrigem.Text)
                vLog_UFDES = CStr(objDesc!cod_uf)
                vLog_DPK = Val(txtCod_DPK.Text)
                vLog_DESC_ANT = 0
                vLog_DESC_NOVO = CDbl(txtDescontoUnico.Text)
                vLog_TP_ALT = "I"
            Else
                vLog_UFORI = objDesc2!Cod_Uf_Origem
                vLog_UFDES = objDesc2!Cod_Uf_Destino
                vLog_DPK = objDesc2!Cod_Dpk
                vLog_DESC_ANT = objDesc2!Pc_Desc
                vLog_DESC_NOVO = CDbl(txtDescontoUnico.Text)
                vLog_TP_ALT = "A"
            End If
            'Prepara binds para gravar log
            oradatabase.Parameters.Remove "UFORI":  oradatabase.Parameters.Add "UFORI", vLog_UFORI, 1
            oradatabase.Parameters.Remove "UFDES":  oradatabase.Parameters.Add "UFDES", vLog_UFDES, 1
            oradatabase.Parameters.Remove "DPK":    oradatabase.Parameters.Add "DPK", vLog_DPK, 1
            oradatabase.Parameters.Remove "DESCANT": oradatabase.Parameters.Add "DESCANT", vLog_DESC_ANT, 1
            oradatabase.Parameters.Remove "DESC":   oradatabase.Parameters.Add "DESC", vLog_DESC_NOVO, 1
            oradatabase.Parameters.Remove "FLDIF":  oradatabase.Parameters.Add "FLDIF", vLog_FL_DIF_ICM, 1
            oradatabase.Parameters.Remove "TPALT":  oradatabase.Parameters.Add "TPALT", vLog_TP_ALT, 1
            oradatabase.Parameters.Remove "USU":    oradatabase.Parameters.Add "USU", vLog_Usu, 1
            strLOG = "Insert into Vendas.Log_Uf_Dpk " & _
                     "(Cod_Uf_Origem, Cod_Uf_Destino, Cod_Dpk, Pc_Desc_Ant, Pc_Desc, Fl_Dif_Icms, Tp_Alteracao, " & _
                     " Dt_Alteracao, Usuario) Values " & _
                     "(:UFORI, :UFDES, :DPK, :DESCANT, :DESC, :FLDIF, :TPALT, " & _
                     " To_Date(To_Char(SYSDATE,'dd/mm/rr hh24:mi:ss'),'dd/mm/rr hh24:mi:ss'), :USU); "
            strLOG = "BEGIN " & strLOG & _
                            " COMMIT;EXCEPTION WHEN OTHERS THEN ROLLBACK;" & _
                            " :cod_errora := SQLCODE;:txt_errora := SQLERRM; " & _
                         "END;"
            oradatabase.ExecuteSQL strLOG
            'VERIFICA ERROS
            If IIf(IsNull(oradatabase.Parameters("cod_errora")), 0, Val(oradatabase.Parameters("cod_errora"))) <> 0 Then
                MsgBox oradatabase.Parameters("txt_errora") & " - Erro na gera��o do Log para esta altera��o!"
            End If
            '///////////////////////////////////////////////////////////////////////////////////////
            
            
            lstrUPDATE = lstrUPDATE & _
                " UPDATE vendas.uf_dpk " & _
                    "SET pc_desc = :pc_desc " & _
                  "WHERE cod_dpk = :cod_dpk " & _
                    "AND cod_uf_origem = :cod_uf_origem " & _
                    "AND cod_uf_destino = :cod_uf_destino" & liLinha & "; " & _
            " IF SQL%NOTFOUND THEN " & _
                " INSERT INTO vendas.uf_dpk " & _
                       " (cod_uf_origem, cod_uf_destino, cod_dpk, pc_desc) " & _
                " VALUES (:cod_uf_origem,:cod_uf_destino" & liLinha & ",:cod_dpk,:pc_desc);" & _
            " END IF;"
            
            liLinha = liLinha + 1
            objDesc.MoveNext
        
        Wend
        
        lstrUPDATE = "BEGIN " & lstrUPDATE & _
                        " COMMIT;EXCEPTION WHEN OTHERS THEN ROLLBACK;" & _
                        " :cod_errora := SQLCODE;:txt_errora := SQLERRM; " & _
                     "END;"
        oradatabase.ExecuteSQL lstrUPDATE
        
        'VERIFICA ERROS
        If IIf(IsNull(oradatabase.Parameters("cod_errora")), 0, Val(oradatabase.Parameters("cod_errora"))) <> 0 Then
            MsgBox oradatabase.Parameters("txt_errora") & ".Ligue para o Depto de Sistemas"
            Exit Sub
        Else
            MsgBox "Desconto Geral Gravado com sucesso !", vbInformation, "Aten��o"
            txtDescontoUnico.Text = ""
            Call cmdCarregaUF_Click
        End If

    
    'Mas Se foi escolhido um filtro, vou alterar o desconto para todas as UFs destino, mas somente os DPKs que respeitarem os filtros
    ElseIf vFl_Gr = True Or vFl_SubGr = True Or vFl_Lin = True Then

        lstrWHERE = ""
        
        'De acordo com filtros escolhidos, vou montar a cl�usula Where
        If Trim(cboFornecedor.Text) <> "" Then
            oradatabase.Parameters.Remove "CODFORN": oradatabase.Parameters.Add "CODFORN", Val(lblCod_Fornecedor.Caption), 1
            lstrWHERE = lstrWHERE & _
                        "And C.Cod_Fornecedor = :CODFORN "
        End If
        If Trim(cboGrupo.Text) <> "00 - TODOS" And Trim(cboGrupo.Text) <> "" Then
            oradatabase.Parameters.Remove "CODGRUPO": oradatabase.Parameters.Add "CODGRUPO", Val(cboGrupo.Text), 1
            lstrWHERE = lstrWHERE & _
                " AND C.Cod_Grupo = :CODGRUPO "
        End If
        If vFl_SubGr = True And Trim(vStr_SubGr) <> "" Then
            lstrWHERE = lstrWHERE & _
                " AND C.Cod_SubGrupo in " & vStr_SubGr
        End If
        If vFl_Lin = True And Trim(vStr_Lin) <> "" Then
            lstrWHERE = lstrWHERE & _
                " AND C.Cod_Linha in " & vStr_Lin
        End If
        
          
        'Vou selecionar todos os DPKs que respeitarem os filtros
        cmdSQL = "Select C.Cod_DPK " & _
                   "From Producao.Item_Cadastro C, " & _
                       " Producao.Item_Estoque E " & _
                  "Where E.Situacao = 0 " & _
                    "And E.Cod_Loja = 1 " & _
                    "And E.Cod_Dpk = C.Cod_Dpk "
        cmdSQL = cmdSQL & lstrWHERE
        
        Set objItens = oradatabase.dbCreateDynaset(cmdSQL, 0&)
            
        If objItens.EOF Then
            MsgBox "N�o existem itens cadastrados para estes filtros!", vbInformation, "CAD310 - Aten��o!"
            Exit Sub
        End If
            
        
        While Not objItens.EOF
            
            lstrUPDATE = ""
            
            oradatabase.Parameters.Remove "cod_dpk": oradatabase.Parameters.Add "cod_dpk", Val(objItens!Cod_Dpk.Value), 1
            
            'Vou trazer todas as UFs, para serem gravadas como UF Destino
            lstrSQL = "Select Cod_Uf " & _
                        "From Producao.UF"
            Set objDesc = oradatabase.dbCreateDynaset(lstrSQL, 0&)
            
            liLinha = 0
            
            While Not objDesc.EOF
                
                oradatabase.Parameters.Remove "cod_uf_destino" & liLinha: oradatabase.Parameters.Add "cod_uf_destino" & liLinha, CStr(objDesc!cod_uf), 1
                
                lstrUPDATE = lstrUPDATE & _
                    " UPDATE vendas.uf_dpk " & _
                        "SET pc_desc = :pc_desc " & _
                      "WHERE cod_dpk = :cod_dpk " & _
                        "AND cod_uf_origem = :cod_uf_origem " & _
                        "AND cod_uf_destino = :cod_uf_destino" & liLinha & "; " & _
                " IF SQL%NOTFOUND THEN " & _
                    " INSERT INTO vendas.uf_dpk " & _
                           " (cod_uf_origem, cod_uf_destino, cod_dpk, pc_desc) " & _
                    " VALUES (:cod_uf_origem,:cod_uf_destino" & liLinha & ",:cod_dpk,:pc_desc);" & _
                " END IF;"
                
                liLinha = liLinha + 1
                objDesc.MoveNext
            
            Wend
        
            lstrUPDATE = "BEGIN " & lstrUPDATE & _
                            " COMMIT;EXCEPTION WHEN OTHERS THEN ROLLBACK;" & _
                            " :cod_errora := SQLCODE;:txt_errora := SQLERRM; " & _
                         "END;"
            'Vai executar transa��o que altera desconto geral para todos DPKs que atendem filtros, para UF destino que est� sendo lida
            oradatabase.ExecuteSQL lstrUPDATE
        
            'VERIFICA ERROS
            If IIf(IsNull(oradatabase.Parameters("cod_errora")), 0, Val(oradatabase.Parameters("cod_errora"))) <> 0 Then
                MsgBox oradatabase.Parameters("txt_errora") & ".Ligue para o Depto de Sistemas"
                Exit Sub
            'Else
            '    MsgBox "Desconto Geral Gravado com sucesso em todas as UFs Destino, " & Chr(13) & _
                        "para o DPK " & Format(objItens!Cod_Dpk.Value, "00000") & "!", vbInformation, "CAD310 - Aten��o!"
            End If

            'Le pr�ximo item a ser alterado
            objItens.MoveNext
        
        Wend

        MsgBox "Desconto Geral Gravado com sucesso em todas as UFs Destino, " & Chr(13) & _
               "para todos os DPKs!", vbInformation, "CAD310 - Aten��o!"
        txtDescontoUnico.Text = ""
        Call cmdCarregaUF_Click

    End If

End If

End Sub



Private Sub cmdImprimir_Click()
Dim J As Long
Dim vCont As Integer, vPag As Integer
Dim vUFOri As String, vUfDest As String, vDPK As String
Dim vDesc As String, vPcIcm As String, vTribIcm As String
Dim vTribIpi As String

'Verifica se o grid cont�m informa��es
grdConsulta.Row = 1
grdConsulta.Col = 0
If Trim(grdConsulta.Text) = "" Then
    MsgBox "N�o h� dados para serem impressos!", vbInformation, "CAD310 - Aten��o!"
    Exit Sub
End If

Screen.MousePointer = 11
vCont = 0
vPag = 0
For J = 1 To grdConsulta.Rows - 1

    grdConsulta.Row = J
    grdConsulta.Col = 0

    'Verifica se � 1o.registro ou se precisa mudar de p�gina
    If J = 1 Then
        GoSub CABEC_1
    Else
        If vCont = 65 Then
            While vCont < 69
                'carlosj 08/08/2012 CiT - [ISA 192]
                'Printer.Print "|       |        |       |          |        |             |             |"
                Printer.Print "|       |        |                   |          |        |             |             |"
                vCont = vCont + 1
            Wend
            If vCont = 69 Then
                vPag = Format(vPag + 1, "000")
                Printer.Print "|                                                                          Pag.: " & Space(4 - Len(vPag)) & vPag & " |"
                'carlosj 08/08/2012 CiT - [ISA 192]1
                'Printer.Print "+=======+========+=======+==========+========+=============+=============+"
                 Printer.Print "+=======+========+===================+==========+========+=============+=============+"
            End If
            Printer.NewPage
            vCont = 0
            GoSub CABEC_1
        End If
    End If

    grdConsulta.Col = 0
    vUFOri = Trim(grdConsulta.Text)
    grdConsulta.Col = 1
    vUfDest = Trim(grdConsulta.Text)
    grdConsulta.Col = 2
    'carlosj 08/08/2012 CiT - [ISA 192]
    'vDPK = Format(Trim(grdConsulta.Text), "00000")
    vDPK = Format(Trim(grdConsulta.Text), "000000000000000000")
    grdConsulta.Col = 3
    vDesc = Trim(grdConsulta.Text)
    grdConsulta.Col = 4
    vPcIcm = Trim(grdConsulta.Text)
    grdConsulta.Col = 5
    vTribIcm = Trim(grdConsulta.Text)
    grdConsulta.Col = 6
    vTribIpi = Trim(grdConsulta.Text)
    
    Printer.FontSize = 9
    Printer.Print "|" & Space(2) & vUFOri & Space(3) & _
                  "|" & Space(3) & vUfDest & Space(3) & _
                  "|" & Space(18 - Len(vDPK)) & vDPK & Space(1) & _
                  "|" & Space(7 - Len(vDesc)) & vDesc & Space(3) & _
                  "|" & Space(7 - Len(vPcIcm)) & vPcIcm & Space(1) & _
                  "|" & Space(9 - Len(vTribIcm)) & vTribIcm & Space(4) & _
                  "|" & Space(9 - Len(vTribIpi)) & vTribIpi & Space(4) & "|"

    vCont = vCont + 1

Next J

'Finaliza lay-out
If vCont <> 0 Then
    While vCont < 69
       'carlosj 08/08/2012 CiT - [ISA 192]
       ' Printer.Print "|       |        |       |          |        |             |             |"
        Printer.Print "|       |        |                   |          |        |             |             |"
        vCont = vCont + 1
    Wend
    If vCont = 69 Then
        vPag = Format(vPag + 1, "000")
        Printer.Print "|                                                                          Pag.: " & Space(4 - Len(vPag)) & vPag & " |"
        'carlosj 08/08/2012 CiT - [ISA 192]2
        'Printer.Print "+=======+========+=======+==========+========+=============+=============+"
        Printer.Print "+=======+========+===================+==========+========+=============+=============+"
    End If
End If
Printer.EndDoc
Screen.MousePointer = 0
MsgBox "Impress�o finalizada!", vbInformation, "CAD310 - Aten��o!"

Exit Sub

CABEC_1:
    Printer.FontName = "Courier New"
    Printer.FontSize = 9
    Printer.Print " "
    Printer.FontBold = True
    Printer.Print "               CAD310 - CADASTRO DE DESCONTO DE UF POR ITEM "
    Printer.Print " "
    '
    Printer.Print "+=======+========+===================+==========+========+=============+=============+"
    Printer.Print "|UF ORI |UF DEST |        DPK        | DESCONTO | PC_ICM |COD.TRIB.ICM |COD.TRIB.IPI |"
    Printer.Print "+=======+========+===================+==========+========+=============+=============+"
    Printer.FontBold = False
    Printer.Print "|       |        |                   |          |        |             |             |"
    vCont = 7
    Return


End Sub

Private Sub cmdLimpar_Click()
    Call pLimpar
    Call Form_Load
    Call LIMPA_GRID(grdSubGrupo)
End Sub

Private Sub cmdVoltar_Click()

    Unload Me
End Sub

Private Sub cmdVoltarCons_Click()
    Call LIMPA_GRID(grdConsulta)
    fraCadastro.Visible = True
    fraConsulta.Visible = False
End Sub

Private Sub Form_Load()
Dim lstrSQL As String, lstrUF As String
Dim objUF As Object, objGrupo As Object, objLinha As Object
Dim I As Integer

'Deixa painel de grupo e linha, habilitados
spnlGrupo.Visible = True
spnlLinha.Visible = True
txtDesconto.Visible = False
lblDesconto.Visible = False
vFl_Gr = False
vFl_SubGr = False
vFl_Lin = False
vFl_Todos = True
fraCadastro.Visible = True
fraConsulta.Visible = False

'--------------------------------------------COMBO DE UF ORIGEM
lstrSQL = "Select Cod_Uf " & _
            "From Producao.UF "
Set objUF = oradatabase.dbCreateDynaset(lstrSQL, 0&)

lstrUF = cboUfOrigem
cboUfOrigem.Clear

While Not objUF.EOF
    cboUfOrigem.AddItem objUF.Fields(0).Value
    objUF.MoveNext
Wend
cboUfOrigem = lstrUF


'--------------------------------------------COMBO DE GRUPO
'Vai carregar a combo de grupo, depois que escolher o fornecedor - Alethea - 05/05/06
'lstrSQL = "Select Cod_Grupo, Desc_Grupo " & _
            "From Producao.GRUPO " & _
        "Order By Cod_Grupo"
'Set objGrupo = oradatabase.dbCreateDynaset(lstrSQL, 0&)
'cboGrupo.Clear
'cboGrupo.AddItem "00 - TODOS"
'While Not objGrupo.EOF
'    cboGrupo.AddItem Format(objGrupo.Fields(0).Value, "00") & " - " & _
                     objGrupo.Fields(1).Value
'    objGrupo.MoveNext
'Wend



'Monta Estrutura do Grid de UF Destino
With grdUF
    .Cols = 4
    .Row = 0
    .Col = 0: .Text = "": .ColWidth(0) = 300: .FixedAlignment(0) = 2
    .Col = 1: .Text = "UF": .ColWidth(1) = 500: .FixedAlignment(1) = 2
    .ColAlignment(1) = 2
    .Col = 2: .Text = "Pc.ICM": .ColWidth(2) = 1000: .FixedAlignment(2) = 2
    .ColAlignment(2) = 1
    .Col = 3: .Text = "Desconto": .ColWidth(3) = 1000: .FixedAlignment(3) = 2
    .ColAlignment(3) = 1
End With

'Monta Estrutura e Preenche Grid de Linha
'Vai carregar o grid de linha, depois que escolher o fornecedor - Alethea - 05/05/06
'lstrSQL = "Select * " & _
            "From Producao.Linha " & _
    "Order By Cod_Linha"
'Set objLinha = oradatabase.dbCreateDynaset(lstrSQL, 0&)
'Call LIMPA_GRID(grdLinha)
'If objLinha.EOF Then
'    MsgBox "N�o existem linhas cadastradas!", vbInformation, "CAD310 - Aten��o!"
'Else
'    'Monta Estrutura Grid Linha
'    With grdLinha
'        .Cols = 2
'        .Rows = 2
'        .Row = 1
'        For I = 0 To .Cols - 1
'            .Col = I: .Text = ""
'        Next I
'        .Row = 0
'        .Col = 0: .Text = "Linha"
'        .FixedAlignment(0) = 2:
'        .ColAlignment(0) = 0
'        .ColWidth(0) = 2600
'        .Col = 1: .Text = "Marcar"
'        .FixedAlignment(1) = 2:
'        .ColAlignment(1) = 2
'        .ColWidth(1) = 800
'    End With
'    For I = 1 To objLinha.RecordCount
'        grdLinha.Rows = I + 1
'        grdLinha.Row = I
'        grdLinha.Col = 0
'        grdLinha.Text = Format(objLinha!Cod_Linha.Value, "00") & " - " & objLinha!Desc_Linha.Value
'        objLinha.MoveNext
'    Next I
'End If

End Sub





Private Sub grdLinha_DblClick()

    If grdLinha.Col = 1 Then
        If grdLinha.Text = "X" Then
            grdLinha.Text = ""
        Else
            grdLinha.Text = "X"
        End If
    End If

End Sub


Private Sub grdSubGrupo_DblClick()
    If grdSubGrupo.Col = 1 Then
        If grdSubGrupo.Text = "X" Then
            grdSubGrupo.Text = ""
        Else
            grdSubGrupo.Text = "X"
        End If
    End If

End Sub





Private Sub grdUF_DblClick()
    If grdUF.Col = 3 Then
        vGuarda_Lin = grdUF.Row
        txtDesconto.Visible = True
        lblDesconto.Visible = True
        txtDesconto.SetFocus
    End If
End Sub

Private Sub txtCod_DPK_KeyPress(KeyAscii As Integer)
KeyAscii = Numerico(KeyAscii)

End Sub


Private Sub txtCod_DPK_LostFocus()

On Error Resume Next

If lblCod_Fornecedor = "" Then
    Call pCarregaDados
End If

End Sub


Private Sub txtDesconto_KeyPress(KeyAscii As Integer)
KeyAscii = Valor_Negativo(KeyAscii, txtDesconto)

End Sub

Private Sub txtDesconto_LostFocus()

'Valida desconto
If Trim(txtDesconto) <> "" Then
    If CSng(txtDesconto) > 100 Then
        MsgBox "Desconto deve ser menor que 100 !", vbCritical, "Aten��o"
        txtDesconto.SetFocus
    End If
End If

'Grava no grid, o novo desconto (somente no grid, por enquanto)

grdUF.Row = vGuarda_Lin
grdUF.Col = 0
grdUF.Text = "X"
grdUF.Col = 3
grdUF.Text = Format(txtDesconto.Text, "#0.00")
grdUF.Refresh

txtDesconto.Visible = False
lblDesconto.Visible = False

grdUF.SetFocus

End Sub


Private Sub txtDescontoUnico_KeyPress(KeyAscii As Integer)
KeyAscii = Valor_Negativo(KeyAscii, txtDesconto)

End Sub


