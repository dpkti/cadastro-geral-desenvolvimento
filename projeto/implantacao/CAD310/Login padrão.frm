VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form dlgLogin 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Acesso"
   ClientHeight    =   2130
   ClientLeft      =   5475
   ClientTop       =   3480
   ClientWidth     =   3210
   ControlBox      =   0   'False
   Icon            =   "Login padr�o.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   142
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   214
   ShowInTaskbar   =   0   'False
   Begin VB.Frame fra 
      Caption         =   "Informa��es do usu�rio"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1140
      Left            =   45
      TabIndex        =   5
      Top             =   945
      Width           =   3120
      Begin VB.TextBox txtSenha 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Wingdings"
            Size            =   9
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   330
         IMEMode         =   3  'DISABLE
         Left            =   855
         PasswordChar    =   "l"
         TabIndex        =   1
         Top             =   675
         Width           =   2130
      End
      Begin VB.TextBox txtUsuario 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   855
         TabIndex        =   0
         Top             =   270
         Width           =   2130
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         Caption         =   "Senha:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   135
         TabIndex        =   7
         Top             =   720
         Width           =   690
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         Caption         =   "Usu�rio:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   135
         TabIndex        =   6
         Top             =   315
         Width           =   690
      End
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   2
      Top             =   810
      Width           =   3120
      _ExtentX        =   5503
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   690
      Left            =   855
      TabIndex        =   3
      TabStop         =   0   'False
      ToolTipText     =   "Sair"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Login padr�o.frx":000C
      PICN            =   "Login padr�o.frx":0028
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdConectar 
      Height          =   690
      Left            =   45
      TabIndex        =   4
      TabStop         =   0   'False
      ToolTipText     =   "Conectar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Login padr�o.frx":0902
      PICN            =   "Login padr�o.frx":091E
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "dlgLogin"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdConectar_Click()

    Dim vAuxSenha As String, vSenha As String
    
    Screen.MousePointer = 11
    
    '************************************
    'PRODUCAO
    '************************************
    'Conexao oracle
    Set orasession = CreateObject("oracleinprocserver.xorasession")
    Set oradatabase = orasession.OpenDatabase("PRODUCAO", "CAD_UFDPK/PROD", 0&)
    'Set oradatabase = orasession.OpenDatabase("sdpk_teste", "PRODUCAO/dpk", 0&)
    
    '1o. Vai trazer dados do banco, de acordo com login de intranet passado pelo usu�rio
    Set OraParameters = oradatabase.Parameters
    Criar_Cursor OraParameters, "PM_CURSOR1"
    OraParameters.Remove "LOGIN": OraParameters.Add "LOGIN", txtUsuario, 1
    OraParameters.Remove "vCODErro": OraParameters.Add "vCODErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    
    vErro = vVB_Generica_001.ExecutaPl(oradatabase, "PRODUCAO.PCK_CAD310.Pr_Login_User (:PM_CURSOR1, :LOGIN, :vCODErro, :vMsgErro)")
    If vErro = "" Then
        Set vObjOracle = OraParameters("PM_CURSOR1").Value
    Else
        Exit Sub
    End If

    If vObjOracle.EOF Then
        Call vVB_Generica_001.Informar("Uus�rio n�o encontrado!")
        Screen.MousePointer = 0
        Exit Sub
    End If

    'Se houve sucesso no login, vai pegar a senha que veio do select acima e descriptografar
    vAuxSenha = vObjOracle!SENHA
    vCodUser_Intranet = vObjOracle!Cod_Usuario
    vUser_Intranet = vObjOracle!Login
    vLog_Usu = vCodUser_Intranet & " - " & vUser_Intranet
        
    Screen.MousePointer = 0
    If vAuxSenha <> CStr(UCase(txtSenha)) Then
        Call vVB_Generica_001.Informar("Senha n�o confere!")
        Screen.MousePointer = 0
        Exit Sub
    End If
    
    Unload Me

End Sub

Private Sub cmdSair_Click()

    If vVB_Generica_001.Sair = 6 Then
    
        Set vObjOracle = Nothing
        Set vBanco = Nothing
        Set vSessao = Nothing
        
        End
    
    End If

End Sub
Private Sub Form_Load()
    
    Me.Top = 0
    Me.Left = 0
    
End Sub

Private Sub txtSenha_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
    Call cmdConectar_Click
End If

End Sub


