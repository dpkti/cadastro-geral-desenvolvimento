VERSION 4.00
Begin VB.Form frmCons 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Consulta"
   ClientHeight    =   3945
   ClientLeft      =   1140
   ClientTop       =   1515
   ClientWidth     =   8220
   Height          =   4350
   Left            =   1080
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3945
   ScaleWidth      =   8220
   Top             =   1170
   Width           =   8340
   Begin VB.Label Label1 
      Caption         =   "duplo click para excluir"
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   360
      TabIndex        =   1
      Top             =   3600
      Width           =   3255
   End
   Begin MSGrid.Grid grdCon 
      Height          =   3015
      Left            =   360
      TabIndex        =   0
      Top             =   480
      Width           =   7575
      _Version        =   65536
      _ExtentX        =   13361
      _ExtentY        =   5318
      _StockProps     =   77
      ForeColor       =   8388608
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmCons"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
   Dim i As Long
   'mouse
    Screen.MousePointer = vbHourglass
    
    'montar SQL
    SQL = "Select to_char(a.cod_loja,'00')||'-'||b.nome_fantasia loja,"
    SQL = SQL & " to_char(a.cod_fornecedor,'0000')||'-'||C.sigla forn,"
    SQL = SQL & " to_char(nvl(a.cod_grupo,0),'00')||'-'||d.desc_grupo grupo"
    SQL = SQL & " From fornecedor_destaque a, loja b,"
    SQL = SQL & " fornecedor c, grupo d"
    SQL = SQL & " Where"
    SQL = SQL & " a.cod_grupo = d.cod_grupo(+) and"
    SQL = SQL & " a.cod_fornecedor = C.cod_fornecedor and"
    SQL = SQL & " a.cod_loja=b.cod_loja"
    SQL = SQL & " Order by loja, forn, grupo"
    
    'criar consulta
    Set ss = oradatabase.dbcreatedynaset(SQL, 0&)
    
    If ss.EOF And ss.BOF Then
        Screen.MousePointer = vbDefault
        MsgBox "N�o ha FORNECEDOR EM DESTAQUE cadastrado", vbInformation, "Aten��o"
        Unload Me
        Exit Sub
    End If
    
    'carrega dados
    
    With frmCons.grdCon
        .Cols = 3
        .Rows = ss.RecordCount + 1
        .ColWidth(0) = 2000
        .ColWidth(1) = 2500
        .ColWidth(2) = 3000
        
        .Row = 0
        .Col = 0
        .Text = "LOJA"
        .Col = 1
        .Text = "FORNECEDOR"
        .Col = 2
        .Text = "GRUPO"
        
        ss.MoveFirst
        For i = 1 To .Rows - 1
            .Row = i
            
            .Col = 0
            .Text = Trim(ss!LOJA)
            .Col = 1
            .Text = Trim(ss!FORN)
            .Col = 2
            .Text = ss!GRUPO
            
            ss.MoveNext
        Next
        .Row = 1
    End With
    
            
    'mouse
    Screen.MousePointer = vbDefault
        
    Exit Sub
End Sub


Private Sub grdCon_DblClick()
  Dim lngCod_Loja As Long
  Dim lngForn As Long
  Dim lngGrupo As Long
  
  grdCon.Col = 0
  lngCod_Loja = Mid(Trim(grdCon.Text), 1, 2)
  grdCon.Col = 1
  lngForn = Mid(Trim(grdCon.Text), 1, 4)
  grdCon.Col = 2
  lngGrupo = Mid(Trim(grdCon.Text), 1, 2)

   
  SQL = "BEGIN"
  SQL = SQL & " Delete from fornecedor_destaque "
  SQL = SQL & " where "
  SQL = SQL & " cod_grupo = :grupo and"
  SQL = SQL & " cod_fornecedor = :forn "
  SQL = SQL & " AND cod_loja = :loja;"
  SQL = SQL & " COMMIT;"
  SQL = SQL & "EXCEPTION"
  SQL = SQL & " WHEN OTHERS THEN"
  SQL = SQL & " ROLLBACK;"
  SQL = SQL & " :cod_errora := SQLCODE;"
  SQL = SQL & " :txt_errora := SQLERRM;"
  SQL = SQL & "END;"
    
  oradatabase.Parameters.Remove "loja"
  oradatabase.Parameters.Remove "FORN"
  oradatabase.Parameters.Remove "grupo"
  oradatabase.Parameters.Remove "cod_errora"
  oradatabase.Parameters.Remove "txt_errora"
     
  oradatabase.Parameters.Add "loja", lngCod_Loja, 1
  oradatabase.Parameters.Add "FORN", lngForn, 1
  oradatabase.Parameters.Add "grupo", lngGrupo, 1
  oradatabase.Parameters.Add "cod_errora", 0, 2
  oradatabase.Parameters.Add "txt_errora", "", 2
  
   
  oradatabase.ExecuteSQL SQL
            
  If oradatabase.Parameters("cod_errora") <> 0 Then
    MsgBox oradatabase.Parameters("txt_errora") & ".Ligue para o Depto de Sistemas"
    Exit Sub
  Else
    MsgBox "Exclus�o OK ", vbInformation, "Aten��o"
    Form_Load
  End If




End Sub

