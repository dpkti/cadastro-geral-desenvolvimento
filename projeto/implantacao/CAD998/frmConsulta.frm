VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form frmConsulta 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Consulta montadoras"
   ClientHeight    =   5415
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8820
   Icon            =   "frmConsulta.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5415
   ScaleWidth      =   8820
   Begin VB.TextBox txtCidade 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   6120
      MaxLength       =   10
      TabIndex        =   7
      Top             =   1560
      Width           =   2535
   End
   Begin VB.TextBox txtNome 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1680
      MaxLength       =   10
      TabIndex        =   6
      Top             =   1560
      Width           =   3375
   End
   Begin VB.TextBox txtCNPJ 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   6120
      MaxLength       =   10
      TabIndex        =   5
      Top             =   1110
      Width           =   2535
   End
   Begin VB.TextBox txtCodigo 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1680
      MaxLength       =   10
      TabIndex        =   4
      Top             =   1110
      Width           =   1815
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   0
      TabIndex        =   9
      Top             =   885
      Width           =   6450
      _ExtentX        =   11377
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Cancel          =   -1  'True
      Height          =   690
      Left            =   45
      TabIndex        =   0
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   120
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsulta.frx":1242
      PICN            =   "frmConsulta.frx":125E
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdAdiciona 
      Height          =   690
      Left            =   1980
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Adicionar"
      Top             =   120
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsulta.frx":1F38
      PICN            =   "frmConsulta.frx":1F54
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdExclui 
      Height          =   690
      Left            =   2760
      TabIndex        =   3
      TabStop         =   0   'False
      ToolTipText     =   "Excluir"
      Top             =   120
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsulta.frx":2C2E
      PICN            =   "frmConsulta.frx":2C4A
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdPesquisa 
      Default         =   -1  'True
      Height          =   690
      Left            =   1200
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisar"
      Top             =   120
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsulta.frx":3924
      PICN            =   "frmConsulta.frx":3940
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSFlexGridLib.MSFlexGrid mfgCons 
      Height          =   3255
      Left            =   120
      TabIndex        =   8
      Top             =   2040
      Width           =   8520
      _ExtentX        =   15028
      _ExtentY        =   5741
      _Version        =   393216
      BackColorBkg    =   -2147483633
      AllowUserResizing=   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      Caption         =   "Cidade"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   5280
      TabIndex        =   13
      Top             =   1605
      Width           =   735
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      Caption         =   "Nome cliente"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   180
      TabIndex        =   12
      Top             =   1605
      Width           =   1335
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      Caption         =   "CNPJ"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   5280
      TabIndex        =   11
      Top             =   1155
      Width           =   735
   End
   Begin VB.Label lbl 
      Appearance      =   0  'Flat
      Caption         =   "C�d. cliente"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   180
      TabIndex        =   10
      Top             =   1155
      Width           =   1335
   End
End
Attribute VB_Name = "frmConsulta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdAdiciona_Click()
    frmClientes.Show vbModal
    cmdPesquisa_Click
End Sub

Private Sub cmdExclui_Click()
    If mfgCons.TextMatrix(mfgCons.Row, 1) = Empty Then
        MsgBox "Selecione um cliente para ser removido da lista de mntadoras"
        Exit Sub
    End If

    If MsgBox("O cliente selecionado ser� removida da lista de montadoras. Deseja continuar?", vbYesNo) = vbYes Then
        
        vSql = "PRODUCAO.PCK_CAD998.pr_DEL_MONTADORA(:PM_CODIGO,:PM_CODERRO,:PM_TXTERRO)"
         
        vBanco.Parameters.Remove "PM_CODIGO"
        vBanco.Parameters.Add "PM_CODIGO", mfgCons.TextMatrix(mfgCons.Row, 1), 1
         
        vBanco.Parameters.Remove "PM_CODERRO"
        vBanco.Parameters.Add "PM_CODERRO", 0, 2
        vBanco.Parameters.Remove "PM_TXTERRO"
        vBanco.Parameters.Add "PM_TXTERRO", "", 2
              
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
         
        vErro = IIf(vBanco.Parameters("PM_CODERRO") = "", 0, vBanco.Parameters("PM_CODERRO"))
         
        If vErro <> 0 Then
            MsgBox vBanco.Parameters("PM_TXTERRO") & ".Ligue para o Depto de Sistemas"
            Screen.MousePointer = 0
            Exit Sub
        Else
                                  
            cmdPesquisa_Click
            Screen.MousePointer = 0
            MsgBox "Exclus�o OK ", vbInformation, "Aten��o"
            'Call LIMPA_TELA
            
        End If
        
    End If
End Sub

Private Sub cmdPesquisa_Click()
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
    
    vBanco.Parameters.Remove "PM_CODIGO"
    vBanco.Parameters.Add "PM_CODIGO", LimpaStr(txtCodigo.Text), 1
    vBanco.Parameters.Remove "PM_CNPJ"
    vBanco.Parameters.Add "PM_CNPJ", LimpaStr(txtCNPJ.Text), 1
    vBanco.Parameters.Remove "PM_NOME"
    vBanco.Parameters.Add "PM_NOME", LimpaStr(Trim(txtNome.Text)), 1
    vBanco.Parameters.Remove "PM_CIDADE"
    vBanco.Parameters.Add "PM_CIDADE", LimpaStr(Trim(txtCidade.Text)), 1
   
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "PRODUCAO.PCK_CAD998.pr_CON_MONTADORAS(:PM_CURSOR1, :PM_CODIGO, :PM_CNPJ, :PM_NOME, :PM_CIDADE, :PM_CODERRO, :PM_TXTERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
    
    If vObjOracle.EOF Then
        MsgBox "Nenhum cliente do tipo montadora encontrado.", , "Aten��o!"
        Exit Sub
    Else
        vVB_Generica_001.CarregaGridTabela mfgCons, vObjOracle, 6
        mfgCons.Row = 1
    End If

End Sub

Private Sub cmdVoltar_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    cmdPesquisa_Click
End Sub

Private Sub txtCNPJ_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Numero(KeyAscii)
End Sub

Private Sub txtCodigo_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Numero(KeyAscii)
End Sub

Private Sub txtCodigo_LostFocus()
    txtCodigo.Text = LimpaStr(txtCodigo.Text)
End Sub

Private Sub txtCNPJ_LostFocus()
    txtCNPJ.Text = LimpaStr(txtCNPJ.Text)
End Sub

Private Sub txtNome_LostFocus()
    txtNome.Text = LimpaStr(txtNome.Text)
End Sub

Private Sub txtCidade_LostFocus()
    txtCidade.Text = LimpaStr(txtCidade.Text)
End Sub
