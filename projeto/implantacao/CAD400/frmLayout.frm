VERSION 5.00
Begin VB.Form frmLayout 
   BorderStyle     =   1  'Fixed Single
   Caption         =   ":: Layout para importa��o de dados ::"
   ClientHeight    =   2295
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3900
   Icon            =   "frmLayout.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2295
   ScaleWidth      =   3900
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame2 
      Caption         =   "Exemplo do dados no Arquivo:"
      Height          =   585
      Left            =   600
      TabIndex        =   13
      Top             =   1500
      Width           =   2745
      Begin VB.Label Label15 
         AutoSize        =   -1  'True
         Caption         =   "1"
         ForeColor       =   &H0000FFFF&
         Height          =   195
         Left            =   2370
         TabIndex        =   16
         Top             =   240
         Width           =   90
      End
      Begin VB.Label Label14 
         AutoSize        =   -1  'True
         Caption         =   "000000000000070199"
         ForeColor       =   &H000000FF&
         Height          =   195
         Left            =   720
         TabIndex        =   15
         Top             =   240
         Width           =   1620
      End
      Begin VB.Label Label13 
         AutoSize        =   -1  'True
         Caption         =   "01"
         ForeColor       =   &H00FF0000&
         Height          =   195
         Left            =   480
         TabIndex        =   14
         Top             =   240
         Width           =   180
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Layout do Arquivo:"
      Height          =   1305
      Left            =   240
      TabIndex        =   0
      Top             =   90
      Width           =   3525
      Begin VB.Label Label12 
         AutoSize        =   -1  'True
         Caption         =   "08"
         ForeColor       =   &H0000FFFF&
         Height          =   195
         Left            =   2670
         TabIndex        =   12
         Top             =   960
         Width           =   180
      End
      Begin VB.Label Label11 
         AutoSize        =   -1  'True
         Caption         =   "03  a  20"
         ForeColor       =   &H000000FF&
         Height          =   195
         Left            =   2670
         TabIndex        =   11
         Top             =   720
         Width           =   630
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         Caption         =   "01  a  02"
         ForeColor       =   &H00FF0000&
         Height          =   195
         Left            =   2670
         TabIndex        =   10
         Top             =   480
         Width           =   630
      End
      Begin VB.Label Label9 
         AutoSize        =   -1  'True
         Caption         =   "Posi��es"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   2670
         TabIndex        =   9
         Top             =   240
         Width           =   780
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         Caption         =   "02"
         ForeColor       =   &H00FF0000&
         Height          =   195
         Left            =   1860
         TabIndex        =   8
         Top             =   480
         Width           =   180
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "18"
         ForeColor       =   &H000000FF&
         Height          =   195
         Left            =   1860
         TabIndex        =   7
         Top             =   720
         Width           =   180
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         Caption         =   "01"
         ForeColor       =   &H0000FFFF&
         Height          =   195
         Left            =   1860
         TabIndex        =   6
         Top             =   960
         Width           =   180
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Tamanho"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1770
         TabIndex        =   5
         Top             =   240
         Width           =   795
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "C�digo da Tributa��o"
         ForeColor       =   &H0000FFFF&
         Height          =   195
         Left            =   120
         TabIndex        =   4
         Top             =   960
         Width           =   1530
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "C�digo do DPK"
         ForeColor       =   &H000000FF&
         Height          =   195
         Left            =   120
         TabIndex        =   3
         Top             =   720
         Width           =   1095
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "C�digo da Loja"
         ForeColor       =   &H00FF0000&
         Height          =   195
         Left            =   120
         TabIndex        =   2
         Top             =   480
         Width           =   1065
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Nome do Campo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   1395
      End
   End
End
Attribute VB_Name = "frmLayout"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
