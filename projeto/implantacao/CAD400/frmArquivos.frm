VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{44FDCC3D-6464-11D4-889B-00105AB0D2BD}#1.0#0"; "3DLINES.OCX"
Begin VB.Form frmArquivos 
   ClientHeight    =   6240
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8190
   Icon            =   "frmArquivos.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   6240
   ScaleWidth      =   8190
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame1 
      Caption         =   "Gravar Log:"
      Height          =   1125
      Left            =   30
      TabIndex        =   9
      Top             =   4770
      Width           =   8085
      Begin VB.TextBox txtCaminhoLog 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         Height          =   285
         Left            =   990
         TabIndex        =   11
         Top             =   210
         Width           =   6735
      End
      Begin VB.CommandButton cmdCaminhoLog 
         Caption         =   "..."
         Height          =   285
         Left            =   7740
         TabIndex        =   10
         ToolTipText     =   "Indique o caminho do arquivo"
         Top             =   210
         Width           =   285
      End
      Begin Bot�o.cmd cmdGravaLog 
         Height          =   540
         Left            =   7560
         TabIndex        =   13
         TabStop         =   0   'False
         ToolTipText     =   "Gravar Log"
         Top             =   510
         Width           =   465
         _ExtentX        =   820
         _ExtentY        =   953
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   2
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmArquivos.frx":08CA
         PICN            =   "frmArquivos.frx":08E6
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label lblNotificacao 
         AutoSize        =   -1  'True
         Caption         =   "#"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   210
         TabIndex        =   14
         Top             =   570
         Visible         =   0   'False
         Width           =   195
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Localiza��o:"
         Height          =   195
         Left            =   60
         TabIndex        =   12
         Top             =   270
         Width           =   900
      End
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   270
      Left            =   0
      TabIndex        =   8
      Top             =   5970
      Width           =   8190
      _ExtentX        =   14446
      _ExtentY        =   476
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   335308
            MinWidth        =   335308
         EndProperty
      EndProperty
      OLEDropMode     =   1
   End
   Begin MSComctlLib.ListView lsvSubTrib 
      Height          =   2925
      Left            =   30
      TabIndex        =   7
      Top             =   1290
      Width           =   8115
      _ExtentX        =   14314
      _ExtentY        =   5159
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   6
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Qtde"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Cod_loja"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Cod_DPK"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "Cod_Tributa��o"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Text            =   "Observa��o"
         Object.Width           =   4127
      EndProperty
   End
   Begin VB.CommandButton cmdCaminho 
      Caption         =   "..."
      Height          =   285
      Left            =   7860
      TabIndex        =   6
      ToolTipText     =   "Indique o caminho do arquivo"
      Top             =   960
      Width           =   285
   End
   Begin VB.TextBox txtCaminho 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      Height          =   285
      Left            =   960
      TabIndex        =   5
      Top             =   960
      Width           =   6885
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   3180
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin Lines3D.Line3D Line3D1 
      Height          =   195
      Left            =   -180
      Top             =   810
      Width           =   8325
      _ExtentX        =   14684
      _ExtentY        =   344
   End
   Begin Bot�o.cmd cmdGravar 
      Height          =   810
      Left            =   2190
      TabIndex        =   0
      TabStop         =   0   'False
      ToolTipText     =   "Gravar informa��es"
      Top             =   30
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1429
      BTYPE           =   3
      TX              =   "Gravar"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   2
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmArquivos.frx":15C0
      PICN            =   "frmArquivos.frx":15DC
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdLocaliza 
      Height          =   810
      Left            =   30
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Consultar informa��es"
      Top             =   30
      Width           =   1020
      _ExtentX        =   1799
      _ExtentY        =   1429
      BTYPE           =   3
      TX              =   "Ler Arquivo"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   2
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmArquivos.frx":22B6
      PICN            =   "frmArquivos.frx":22D2
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdLog 
      Height          =   810
      Left            =   1110
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Visualizar"
      Top             =   30
      Width           =   1035
      _ExtentX        =   1826
      _ExtentY        =   1429
      BTYPE           =   3
      TX              =   "Gerar Log"
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   2
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmArquivos.frx":2FAC
      PICN            =   "frmArquivos.frx":2FC8
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdExcel 
      Height          =   810
      Left            =   7410
      TabIndex        =   3
      TabStop         =   0   'False
      ToolTipText     =   "Gravar informa��es"
      Top             =   30
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1429
      BTYPE           =   3
      TX              =   "Voltar"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   2
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmArquivos.frx":3CA2
      PICN            =   "frmArquivos.frx":3CBE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdLayout 
      Height          =   300
      Left            =   2970
      TabIndex        =   16
      TabStop         =   0   'False
      Top             =   540
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   529
      BTYPE           =   14
      TX              =   "Layout"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   2
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmArquivos.frx":4998
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label lblGrava 
      AutoSize        =   -1  'True
      Caption         =   "#"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   3030
      TabIndex        =   15
      Top             =   120
      Visible         =   0   'False
      Width           =   105
   End
   Begin VB.Label lblCaminho 
      AutoSize        =   -1  'True
      Caption         =   "Localiza��o:"
      Height          =   195
      Left            =   30
      TabIndex        =   4
      Top             =   1020
      Width           =   900
   End
End
Attribute VB_Name = "frmArquivos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim vNomeArq As String
Dim lItem As ListItem
Dim vLinhaArquivo As String
Dim Sql As String
Dim Ds  As Object

Private Sub cmdCaminho_Click()
1         On Error GoTo TrataErro
          
2         CommonDialog1.Filter = "Text (*.txt)|*.txt"
3         CommonDialog1.ShowOpen
4         vNomeArq = UCase(CommonDialog1.FileTitle)
5         txtCaminho.Text = CommonDialog1.FileName
6         lsvSubTrib.ListItems.Clear
TrataErro:
7         If Err.Number <> 0 Then
8             MsgBox "Rotina cmdCaminho_Click" & vbCrLf & "Numero: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & _
              vbCrLf & "Linha: " & Erl, vbInformation, "Aten��o"
9             Exit Sub
10        End If
End Sub

Private Sub cmdCaminhoLog_Click()
          Dim vNomeArqLog As String
          
1         On Error GoTo TrataErro
          
2         CommonDialog1.Filter = "Text (*.txt)|*.txt"
3         CommonDialog1.ShowOpen
4         txtCaminhoLog.Text = CommonDialog1.FileName
5         vNomeArqLog = UCase(CommonDialog1.FileTitle)
          
TrataErro:
6         If Err.Number <> 0 Then
7             MsgBox "Rotina cmdCaminhoLog_Click" & vbCrLf & "Numero: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & _
              vbCrLf & "Linha: " & Erl, vbInformation, "Aten��o"
8             Exit Sub
9         End If
End Sub

Private Sub cmdExcel_Click()
    Unload Me
End Sub

Private Sub cmdGravaLog_Click()
          Dim vFso As FileSystemObject
          Dim vFile As TextStream
          Dim i As Long
          
1         On Error GoTo TrataErro
          
2         If txtCaminhoLog.Text <> "" Then
3             For i = 1 To lsvSubTrib.ListItems.Count
4                 If UCase(lsvSubTrib.ListItems(i).SubItems(5) <> UCase("OK")) Then
5                     Set vFso = New FileSystemObject
6                     Set vFile = vFso.OpenTextFile(txtCaminhoLog.Text, ForAppending, True, TristateUseDefault)
7                     vFile.WriteLine Format(lsvSubTrib.ListItems(i).SubItems(2), "00") & Format(lsvSubTrib.ListItems(i).SubItems(3), "00000") & lsvSubTrib.ListItems(i).SubItems(4) & " - " & Mid(lsvSubTrib.ListItems(i).SubItems(5), 7, Len(lsvSubTrib.ListItems(i).SubItems(5)))
8                     vFile.Close
9                 End If
10            Next
11            Set vFile = Nothing
12            Set vFso = Nothing
13            lblNotificacao.Caption = "Arquivo de log gerado com sucesso."
14            lblNotificacao.Visible = True
15        End If
          
TrataErro:
16        If Err.Number <> 0 Then
17            MsgBox "Rotina cmdGravaLog_Click" & vbCrLf & "Numero: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & _
              vbCrLf & "Linha: " & Erl, vbInformation, "Aten��o"
18            Exit Sub
19        End If
End Sub

Private Sub cmdGravar_Click()
          Dim i As Long
1         On Error GoTo TrataErro
          
2         If txtCaminho.Text <> "" Then
3             For i = 1 To lsvSubTrib.ListItems.Count
4                If UCase(lsvSubTrib.ListItems(i).SubItems(5) = UCase("OK")) Then
5                     Sql = "Select cod_loja, cod_dpk, cod_tributacao from compras.subst_compras_aux "
6                     Sql = Sql & "Where "
7                     Sql = Sql & "cod_loja = " & lsvSubTrib.ListItems(i).SubItems(2) & " and "
8                     Sql = Sql & "cod_dpk = " & lsvSubTrib.ListItems(i).SubItems(3) & " and "
9                     Sql = Sql & "cod_tributacao = " & lsvSubTrib.ListItems(i).SubItems(4)
10                    Set Ds = FCAD400.Db.CreateDynaset(Sql, 8&)
11                    If Ds.EOF = False Then
12                        Sql = "Update Compras.Subst_compras_aux set Cod_tributacao = " & lsvSubTrib.ListItems(i).SubItems(4)
13                        Sql = Sql & " Where "
14                        Sql = Sql & "cod_loja = " & lsvSubTrib.ListItems(i).SubItems(2) & " and "
15                        Sql = Sql & "cod_dpk = " & lsvSubTrib.ListItems(i).SubItems(3)
16                    Else
17                        Sql = "Insert into Compras.Subst_Compras_aux(Cod_Loja, Cod_dpk, Cod_Tributacao "
18                        Sql = Sql & ") values ("
19                        Sql = Sql & lsvSubTrib.ListItems(i).SubItems(2) & ", "
20                        Sql = Sql & lsvSubTrib.ListItems(i).SubItems(3) & ", "
21                        Sql = Sql & lsvSubTrib.ListItems(i).SubItems(4) & ")"
22                    End If
23                    FCAD400.Db.ExecuteSQL ("Begin " & Sql & "; Commit; End;")
24                End If
25            Next
26            lblGrava.Caption = "Importa��o realizada com sucesso."
27            lblGrava.Visible = True
28        End If

TrataErro:
29        If Err.Number <> 0 Then
30            MsgBox "Rotina cmdGravar_Click." & vbCrLf & "Numero: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & _
              vbCrLf & "Linha: " & Erl, vbInformation, "Aten��o"
31            Exit Sub
32        End If
End Sub

Private Sub cmdLayout_Click()
    frmLayout.Show 1
End Sub

Private Sub cmdLocaliza_Click()
          Dim fso As New FileSystemObject
          Dim vArq As TextStream
          Dim vcount As String
1         On Error GoTo TrataErro
          
2         vcount = 0
          
          'Limpar o ListView
3         lsvSubTrib.ListItems.Clear
4         If txtCaminho.Text <> "" Then
5             Set vArq = fso.OpenTextFile(txtCaminho.Text, ForReading, True)
'6             Me.MousePointer = vbHourglass
7             Do While vArq.AtEndOfStream = False
8                 vLinhaArquivo = vArq.ReadLine
9                 vcount = vcount + 1
                    'eoliveira - CiT - 10/08/2012
                    'Alterar tamanho do c�digo DPK
                    
10                Sql = "Select cod_loja, cod_dpk From producao.item_estoque where cod_dpk = " & Trim(Mid(vLinhaArquivo, 3, 18)) & " and cod_loja = " & Trim(Mid(vLinhaArquivo, 1, 2)) & " and situacao = 0"
11                Set Ds = FCAD400.Db.CreateDynaset(Sql, 8&)
12                If Ds.EOF = False Then
13                    DoEvents
14                    If Val(Trim(Mid(vLinhaArquivo, 8, 1))) = 0 Or _
                      Val(Trim(Mid(vLinhaArquivo, 8, 1))) = 1 Or _
                      Val(Trim(Mid(vLinhaArquivo, 8, 1))) = 3 Or _
                      Val(Trim(Mid(vLinhaArquivo, 8, 1))) = 6 Or _
                      Val(Trim(Mid(vLinhaArquivo, 8, 1))) = 8 Or _
                      Val(Trim(Mid(vLinhaArquivo, 8, 1))) = 9 Then
15                        Set lItem = lsvSubTrib.ListItems.Add
16                        lItem = ""
17                        lItem.SubItems(1) = lsvSubTrib.ListItems.Count
18                        lItem.SubItems(2) = Trim(Mid(vLinhaArquivo, 1, 2))
                          'eoliveira - CiT - 10/08/2012
                          'Alterar tamanho do c�digo DPK
19                        lItem.SubItems(3) = Trim(Mid(vLinhaArquivo, 3, 18))
20                        lItem.SubItems(4) = Trim(Mid(vLinhaArquivo, 21, 1))
21                        lItem.SubItems(5) = "OK"
22                        lItem.EnsureVisible
23                        lsvSubTrib.Refresh
                          'lsvSubTrib.ListItems.Item(vcount + 1).EnsureVisible
24                    Else
25                        preencheErro vLinhaArquivo, "Cod_Tributa��o"
26                    End If
27                Else
28                    preencheErro vLinhaArquivo, "Cod_DPK / Loja"
29                End If
30                StatusBar1.Panels(1).Text = "Registros: " & vcount
31            Loop
32            cmdLog.Enabled = True
33        End If
'34        Me.MousePointer = vbNormal
          
TrataErro:
35        If Err.Number = 440 Then
36            MsgBox "Arquivo invalido para esta opera��o.", vbInformation, ":: Notifica��o ::"
37        ElseIf Err.Number <> 0 Then
38            MsgBox "Rotina cmdLocaliza." & vbCrLf & "Numero: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & _
              vbCrLf & "Linha: " & Erl, vbInformation, "Aten��o"
39            Exit Sub
40        End If
End Sub

Sub preencheErro(pArq As String, pMotivo As String)

1         On Error GoTo TrataErro

2         Set lItem = lsvSubTrib.ListItems.Add
3         lItem = ""
4         lItem.SubItems(1) = lsvSubTrib.ListItems.Count
5         lItem.ListSubItems(1).ForeColor = &HFF&
6         lItem.ListSubItems(1).Bold = True
7         lItem.SubItems(2) = Trim(Mid(vLinhaArquivo, 1, 2))
8         lItem.ListSubItems(2).ForeColor = &HFF&
9         lItem.ListSubItems(2).Bold = True
          'eoliveira - CiT - 10/08/2012
          'Alterar tamanho do c�digo DPK
10        lItem.SubItems(3) = Trim(Mid(vLinhaArquivo, 3, 18))
11        lItem.ListSubItems(3).ForeColor = &HFF&
12        lItem.ListSubItems(3).Bold = True
13        lItem.SubItems(4) = Trim(Mid(vLinhaArquivo, 21, 1))
14        lItem.ListSubItems(4).ForeColor = &HFF&
15        lItem.ListSubItems(4).Bold = True
16        lItem.SubItems(5) = "ERRO: " & pMotivo
17        lItem.ListSubItems(5).ForeColor = &HFF&
18        lItem.ListSubItems(5).Bold = True
          
TrataErro:
19        If Err.Number <> 0 Then
20            MsgBox "Rotina preencheErro" & vbCrLf & "Numero: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & _
              vbCrLf & "Linha: " & Erl, vbInformation, "Aten��o"
21            Exit Sub
22        End If
End Sub

Private Sub cmdLog_Click()
1         Me.Height = 6645
End Sub

Private Sub Form_Load()
1         Me.Caption = "CAD400 - Substitui��o tributaria - Vers�o: " & App.Major & "." & App.Minor & "." & App.Revision
2         Me.Height = 4920
End Sub
