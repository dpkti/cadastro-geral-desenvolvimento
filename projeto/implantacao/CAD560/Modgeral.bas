Attribute VB_Name = "Module1"
Option Explicit
Global oradatabase As Object
Global orasession As Object
Global oradynaset As Object

Public linha4

'VARIÁVEIS DE ERRO
Public VErro As Variant
Global Const ORATYPE_NUMBER = 2
Global Const ORATYPE_STRING = 5





Sub Process_Line_Errors(ByRef Sql)
    Dim iFnum As Integer
        
    If Err.Number = 3186 Or Err.Number = 3188 Or Err.Number = 3260 Then
        Resume
    Else
        MsgBox "Ocorreu o erro: " & Err.Number & " -" & Err.Description & ". Ligue para o departamento de sistemas"
        Screen.MousePointer = vbDefault
        End
    End If
    Exit Sub

Handler_Process_Line_Errors:
    DoEvents
    Resume Next

End Sub



Function Maiusculo(KeyAscii As Integer) As Integer
    If Chr$(KeyAscii) = "'" Or Chr$(KeyAscii) = "´" Or Chr$(KeyAscii) = "`" Then
        KeyAscii = 0
        Beep
    Else
        KeyAscii = Asc(UCase(Chr$(KeyAscii)))
    End If
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    Maiusculo = KeyAscii
End Function


Function Texto(ByVal KeyAscii As Integer) As Integer
    If KeyAscii = 8 Then    'BACKSPACE
        Texto = KeyAscii
        Exit Function
    End If
    
    If Chr$(KeyAscii) = "'" Or Chr$(KeyAscii) = "´" Or Chr$(KeyAscii) = "`" Then
        KeyAscii = 0
        Beep
    Else
        KeyAscii = Asc(UCase(Chr$(KeyAscii)))
    End If
    
    Texto = KeyAscii
End Function

Function Valor(ByVal KeyAscii As Integer, strCampo As String) As Integer
    If KeyAscii = 8 Then    'BACKSPACE
        Valor = KeyAscii
        Exit Function
    End If
    If Chr$(KeyAscii) = "," Then
        KeyAscii = Asc(".")
    End If
    If Chr$(KeyAscii) = "," Or Chr$(KeyAscii) = "." Then
        If InStr(strCampo, ",") > 0 Or InStr(strCampo, ".") > 0 Then
            KeyAscii = 0
            Beep
        End If
    Else
        If Chr$(KeyAscii) < "0" Or Chr$(KeyAscii) > "9" Then
            KeyAscii = 0
            Beep
        End If
    End If
    
    Valor = KeyAscii
End Function


