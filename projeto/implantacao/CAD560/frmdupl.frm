VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form frmDuplica 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Duplica - UF TIPO CLIENTE"
   ClientHeight    =   1605
   ClientLeft      =   1665
   ClientTop       =   2820
   ClientWidth     =   5445
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   1605
   ScaleWidth      =   5445
   Begin VB.ComboBox cboPARA 
      Height          =   315
      Left            =   4680
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   240
      Width           =   675
   End
   Begin VB.ComboBox cboDE 
      Height          =   315
      Left            =   2940
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   240
      Width           =   675
   End
   Begin Threed.SSCommand cmdSair 
      Height          =   495
      Left            =   3000
      TabIndex        =   5
      Top             =   960
      Width           =   495
      _Version        =   65536
      _ExtentX        =   873
      _ExtentY        =   873
      _StockProps     =   78
      Picture         =   "frmdupl.frx":0000
   End
   Begin Threed.SSCommand cmdGravar 
      Height          =   495
      Left            =   1800
      TabIndex        =   4
      Top             =   960
      Width           =   495
      _Version        =   65536
      _ExtentX        =   873
      _ExtentY        =   873
      _StockProps     =   78
      Picture         =   "frmdupl.frx":031A
   End
   Begin VB.Label Label2 
      Caption         =   "para"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   4080
      TabIndex        =   3
      Top             =   360
      Width           =   555
   End
   Begin VB.Label Label1 
      Caption         =   "Copiar o cadastro de UF TIPO CLIENTE da origem "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   2655
   End
End
Attribute VB_Name = "frmDuplica"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdGravar_Click()
If Trim(cboDE) = "" Or Trim(cboPARA) = "" Then
    MsgBox "Dados incompletos!", vbCritical, "Aten��o"
    Exit Sub
End If
If Trim(cboDE) = Trim(cboPARA) Then
    MsgBox "N�o posso copiar os dados de " & cboDE & " para ele mesmo!", vbCritical, "Aten��o"
    Exit Sub
End If 'PRINTER
If MsgBox("Deseja duplicar o cadastro de UF TIPO CLIENTE do estado de " & cboDE & " para " & cboPARA & " ?", vbYesNo + vbQuestion, "Aten��o") = vbYes Then
    lstrSQL = " begin PRODUCAO.PR_DUPLICA_UF_TPCLIENTE(:de,:para); Commit; " & _
              " Exception When Others then RollBack;  :Cod_ErrorPl := SQLCODE; " & _
              " :Txt_ErrorPl := SQLERRM;  End; "
    oradatabase.Parameters.Remove "Cod_ErrorPl"
    oradatabase.Parameters.Remove "Txt_ErrorPl"
    oradatabase.Parameters.Remove "de"
    oradatabase.Parameters.Remove "para"
    oradatabase.Parameters.Add "Cod_ErrorPl", 0, 2
    oradatabase.Parameters.Add "Txt_ErrorPl", "", 2
   'linha de tratamento de erro para a nova OLE
    oradatabase.Parameters("cod_errorpl").serverType = ORATYPE_NUMBER
   'linha de tratamento de erro para a nova OLE
    oradatabase.Parameters("txt_errorpl").serverType = ORATYPE_STRING
    oradatabase.Parameters.Add "de", cboDE, 1
    oradatabase.Parameters.Add "para", cboPARA, 1
    Screen.MousePointer = vbHourglass
    oradatabase.ExecuteSQL lstrSQL
    Screen.MousePointer = vbDefault
    VErro = IIf((oradatabase.Parameters("COD_ERRORPL").Value = Null Or oradatabase.Parameters("cod_errorpl").Value = ""), 0, oradatabase.Parameters("cod_errorpl").Value)
    If Val(VErro) <> 0 Then
        MsgBox "Erro - " & oradatabase.Parameters("Txt_ErrorPl") & ". Favor avisar Depto. de Sistemas!", vbExclamation
        Exit Sub
    Else
        MsgBox "Duplica��o OK!", vbCritical, "Aten��o"
    End If
End If
End Sub

Private Sub cmdSair_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Dim lstrSQL As String
    Dim objSQL As Object
    lstrSQL = " SELECT cod_uf FROM uf  "
    Set objSQL = oradatabase.dbcreatedynaset(lstrSQL, 4&)
    While Not objSQL.EOF
        cboDE.AddItem objSQL!cod_uf
        cboPARA.AddItem objSQL!cod_uf
        objSQL.MoveNext
    Wend
    Set objSQL = Nothing
End Sub


