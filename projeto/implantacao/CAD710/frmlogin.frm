VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form dlgLogin 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Acesso"
   ClientHeight    =   2130
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3210
   ControlBox      =   0   'False
   Icon            =   "frmlogin.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   142
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   214
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fra 
      Caption         =   "Login Usu�rio Intranet"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1140
      Left            =   45
      TabIndex        =   5
      Top             =   945
      Width           =   3120
      Begin VB.TextBox txtSenha 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Wingdings"
            Size            =   9
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   330
         IMEMode         =   3  'DISABLE
         Left            =   855
         PasswordChar    =   "l"
         TabIndex        =   1
         Top             =   675
         Width           =   2130
      End
      Begin VB.TextBox txtUsuario 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   855
         TabIndex        =   0
         Top             =   270
         Width           =   2130
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         Caption         =   "Senha:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   135
         TabIndex        =   7
         Top             =   720
         Width           =   690
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         Caption         =   "Usu�rio:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   135
         TabIndex        =   6
         Top             =   315
         Width           =   690
      End
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   4
      Top             =   810
      Width           =   3120
      _ExtentX        =   5503
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   690
      Left            =   855
      TabIndex        =   3
      TabStop         =   0   'False
      ToolTipText     =   "Sair"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmlogin.frx":000C
      PICN            =   "frmlogin.frx":0028
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdConectar 
      Height          =   690
      Left            =   45
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Conectar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmlogin.frx":0D02
      PICN            =   "frmlogin.frx":0D1E
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "dlgLogin"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim vTentativas As Integer
Private Sub cmdConectar_Click()
    
    If Trim(txtUsuario) = "" Then Exit Sub
    If Trim(txtSenha) = "" Then Exit Sub

    vUsuario = vvB_Seguranca_001.ValidaLogin(vBanco, txtUsuario, txtSenha)
    If vUsuario.Permissao = 0 Then
        vTentativas = vTentativas + 1
        If vTentativas > 3 Then
            Call vVB_Generica_001.Informar("N�mero de tentativas excedida. O sistema ser� finalizado")
            Set vObjOracle = Nothing
            Set vBanco = Nothing
            Set vSessao = Nothing
            End
        Else
            Call vVB_Generica_001.Informar("Login inv�lido")
            txtUsuario.SetFocus
            Exit Sub
        End If
    End If
        
    Set vObjOracle = Nothing
    Set vBanco = Nothing
    Set vSessao = Nothing
    Unload Me
    mdiCAD710.Show
End Sub

Private Sub cmdSair_Click()
    Set vObjOracle = Nothing
    Set vBanco = Nothing
    Set vSessao = Nothing
    End
End Sub

Private Sub Form_Load()
    
    'INCLUIR OS ARQUIVOS: (PROJECT -> ADD FILE OU CTRL+D)
    'H:\Usr\Padroes\VB6\Forms\Aguardar.frm
    'H:\Usr\Padroes\VB6\Forms\Logo.frm
    'H:\Usr\Padroes\VB6\M�dulos\M�dulo padr�o.bas
    
    If App.PrevInstance Then
        Call vVB_Generica_001.Informar("J� existe outra inst�ncia do programa!")
        End
    End If
    
    Me.Top = 0
    Me.Left = 0

    '<BANCO> = BANCO DE DADOS A SER CONECTADO
    '<USU�RIO> = USU�RIO DE CONEX�O COM O ORACLE
    'vErro = vVB_Generica_001.ConectaOracle("DESENV", "PRODUCAO", False, Me)
    vErro = vVB_Generica_001.ConectaOracle("PRODUCAO", "CAD710", True, mdiCAD710)

    If vErro <> "" Then
        Call vVB_Generica_001.ProcessaErro(vErro)
        End
    End If
    
    vCd = vVB_Generica_001.vCd
    vTipoCD = vVB_Generica_001.vTipoCD
    Set vSessao = vVB_Generica_001.vSessao
    Set vBanco = vVB_Generica_001.vBanco
    
    'PARA SISTEMAS QUE FOREM CONECTAR NOS CD'S, DESCOMENTAR A LINHA
    'ABAIXO E INCLUIR O FORM ABAIXO (PROJECT -> ADD FORM)
    'Conex�o CD 's
    'dlgConexao.Show 1
    
End Sub

Private Sub txtSenha_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        Call cmdConectar_Click
    End If
End Sub

Private Sub txtUsuario_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
End Sub



