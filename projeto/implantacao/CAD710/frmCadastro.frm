VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form frmCadastro 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cadastro de Ocorr�ncias Banc�rias"
   ClientHeight    =   4020
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8325
   Icon            =   "frmCadastro.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4020
   ScaleWidth      =   8325
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraMotivos 
      Appearance      =   0  'Flat
      Caption         =   "Motivos da Rejei��o"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1275
      Left            =   90
      TabIndex        =   8
      Top             =   2640
      Width           =   8115
      Begin VB.TextBox txtCodigo 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1980
         MaxLength       =   4
         TabIndex        =   2
         Top             =   405
         Width           =   690
      End
      Begin VB.TextBox txtDescricao 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1980
         MaxLength       =   40
         TabIndex        =   3
         Top             =   810
         Width           =   6000
      End
      Begin VB.Label lblCodigo 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "C�digo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   135
         TabIndex        =   10
         Top             =   405
         Width           =   555
      End
      Begin VB.Label lblDescricao 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Descri��o"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   135
         TabIndex        =   9
         Top             =   810
         Width           =   765
      End
   End
   Begin VB.Frame fraBanco 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1410
      Left            =   90
      TabIndex        =   7
      Top             =   1080
      Width           =   5865
      Begin VB.ComboBox cmbCodBanco 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1980
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   0
         Top             =   315
         Width           =   3750
      End
      Begin VB.TextBox txtCodOcorrencia 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1980
         MaxLength       =   2
         TabIndex        =   1
         Top             =   810
         Width           =   690
      End
      Begin VB.Label lblCodbanco 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "C�digo do Banco"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   90
         TabIndex        =   12
         Top             =   450
         Width           =   1380
      End
      Begin VB.Label lblCodOcorrencia 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "C�digo da Ocorr�ncia"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   90
         TabIndex        =   11
         Top             =   930
         Width           =   1740
      End
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   0
      TabIndex        =   13
      TabStop         =   0   'False
      Top             =   885
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   120
      TabIndex        =   6
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   120
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadastro.frx":23D2
      PICN            =   "frmCadastro.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdLimpar 
      Height          =   690
      Left            =   960
      TabIndex        =   5
      TabStop         =   0   'False
      ToolTipText     =   "Limpar Campos"
      Top             =   120
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadastro.frx":30C8
      PICN            =   "frmCadastro.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdConfirmar 
      Height          =   690
      Left            =   1800
      TabIndex        =   4
      TabStop         =   0   'False
      ToolTipText     =   "Gravar"
      Top             =   120
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadastro.frx":39BE
      PICN            =   "frmCadastro.frx":39DA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmCadastro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Sub cmbCodBanco_Change()
    txtCodigo.Text = ""
    txtCodOcorrencia.Text = ""
    txtDescricao.Text = ""
End Sub
Private Sub cmbCodBanco_Click()
    txtCodigo.Text = ""
    txtCodOcorrencia.Text = ""
    txtDescricao.Text = ""
End Sub
Private Sub cmdConfirmar_Click()
    If cmbCodBanco.Text = "" Then
        Call vVB_Generica_001.Informar("Informe o Banco")
        cmbCodBanco.SetFocus
    Else
        If Trim(txtCodOcorrencia.Text) = "" Then
            Call vVB_Generica_001.Informar("Informe o C�digo da Ocorr�ncia")
            txtCodOcorrencia.SetFocus
        Else
            If Trim(txtCodigo.Text) = "" Then
                Call vVB_Generica_001.Informar("Informe o C�digo da Rejei��o")
                txtCodigo.SetFocus
            Else
                If Trim(txtDescricao.Text) = "" Then
                    Call vVB_Generica_001.Informar("Preencha o campo descri��o")
                    txtDescricao.SetFocus
                Else
                    If vVB_Generica_001.Perguntar("Deseja realmente gravar as informa��es na base ?") = 6 Then
                        vBanco.Parameters.Add "pBANCO", CInt(cmbCodBanco.ItemData(cmbCodBanco.ListIndex)), 1
                        vBanco.Parameters.Add "pOCORRENCIA", txtCodOcorrencia.Text, 1
                        vBanco.Parameters.Add "pREJEICAO", txtCodigo.Text, 1
                        vBanco.Parameters.Add "pDESCRICAO", txtDescricao.Text, 1
                        vBanco.Parameters.Add "pERRO", " ", 2
                        vBanco.Parameters.Add "pDESC_ERRO", " ", 2

                        vSql = "PRODUCAO.PCK_CAD710.pr_GravaCadastro(:pBANCO, :pOCORRENCIA, :pREJEICAO, :pDESCRICAO)"
                        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
                        If vErro <> "" Then
                            vVB_Generica_001.ProcessaErro (vErro)
                        End If
                        Call vVB_Generica_001.ExcluiBind(vBanco)
                    End If
                End If
            End If
        End If
    End If
End Sub
Private Sub cmdLimpar_Click()
    cmbCodBanco.ListIndex = -1
    txtCodOcorrencia.Text = ""
    txtCodigo.Text = ""
    txtDescricao.Text = ""
End Sub
Private Sub cmdVoltar_Click()
     Unload Me
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        SendKeys "{TAB}"
    End If
End Sub
Private Sub Form_Load()
    vBanco.Parameters.Add "cBUSCABANCO", 0, 3
    vBanco.Parameters("cBUSCABANCO").ServerType = 102
    vBanco.Parameters("cBUSCABANCO").DynasetOption = &H2&
    vBanco.Parameters("cBUSCABANCO").DynasetCacheParams 256, 16, 20, 2000, 0
    
    vSql = "PRODUCAO.PCK_CAD710.pr_BuscaBanco(:cBUSCABANCO)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro)
    Else
        Set vObjOracle = vBanco.Parameters("cBUSCABANCO").Value
    End If
    Call vVB_Generica_001.ExcluiBind(vBanco)
    
    If vObjOracle.RecordCount <> 0 Then
        While Not vObjOracle.EOF
            cmbCodBanco.AddItem Format(vObjOracle!cod_banco, "000") & " - " & vObjOracle!sigla
            cmbCodBanco.ItemData(cmbCodBanco.NewIndex) = vObjOracle!cod_banco
            vObjOracle.MoveNext
        Wend
        cmbCodBanco.ListIndex = -1
    End If
End Sub
Private Sub txtCodigo_GotFocus()
    txtCodigo.SelStart = 0
    txtCodigo.SelLength = Len(txtCodigo.Text)
End Sub

Private Sub txtCodigo_KeyPress(KeyAscii As Integer)
    If Verifica_Numeros(KeyAscii) = False Then
        Call vVB_Generica_001.Informar("Este campo pode conter apenas valores num�ricos")
        txtCodigo.SetFocus
        KeyAscii = 8
    ElseIf KeyAscii = 46 Or KeyAscii = 44 Then
        Call vVB_Generica_001.Informar("Este campo aceita apenas n�meros inteiros")
        txtCodigo.SetFocus
        KeyAscii = 8
    End If
End Sub
Private Sub txtCodigo_LostFocus()
    txtCodigo.Text = Format(txtCodigo.Text, "0000")
End Sub
Private Sub txtCodOcorrencia_GotFocus()
    Call vVB_Generica_001.SelecionaCampo(txtCodOcorrencia)
End Sub
Private Sub txtCodOcorrencia_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Numero(KeyAscii)
End Sub

Private Sub txtCodOcorrencia_LostFocus()
    txtCodOcorrencia.Text = Format(txtCodOcorrencia, "00")
    txtCodigo.Text = ""
    txtDescricao.Text = ""
End Sub
Private Sub txtDescricao_GotFocus()
    If cmbCodBanco.Text = "" Then
        Call vVB_Generica_001.Informar("Informe o Banco")
        cmbCodBanco.SetFocus
    Else
        If Trim(txtCodOcorrencia.Text) = "" Then
            Call vVB_Generica_001.Informar("Informe o C�digo da Ocorr�ncia")
            txtCodOcorrencia.SetFocus
        Else
            If Trim(txtCodigo.Text) = "" Then
                Call vVB_Generica_001.Informar("Informe o C�digo da Rejei��o")
                txtCodigo.SetFocus
            Else
                vBanco.Parameters.Add "cOCORRENCIA", 0, 3
                vBanco.Parameters("cOCORRENCIA").ServerType = 102
                vBanco.Parameters("cOCORRENCIA").DynasetOption = &H2&
                vBanco.Parameters("cOCORRENCIA").DynasetCacheParams 256, 16, 20, 2000, 0
                vBanco.Parameters.Add "pBANCO", CInt(cmbCodBanco.ItemData(cmbCodBanco.ListIndex)), 1
                vBanco.Parameters.Add "pOCORRENCIA", CInt(txtCodOcorrencia.Text), 1
                vBanco.Parameters.Add "pREJEICAO", CInt(txtCodigo.Text), 1
                
                vSql = "PRODUCAO.PCK_CAD710.pr_BuscaBancoOcorrencia(:cOCORRENCIA, :pBANCO, :pOCORRENCIA, :pREJEICAO)"
                vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
                If vErro <> "" Then
                    vVB_Generica_001.ProcessaErro (vErro)
                Else
                    Set vObjOracle = vBanco.Parameters("cOCORRENCIA").Value
                End If
                Call vVB_Generica_001.ExcluiBind(vBanco)
               
                If vObjOracle.RecordCount <> 0 Then
                    txtDescricao.Text = vObjOracle!descr_rejeicao
                    fraBanco.Caption = "Altera��o"
                    txtDescricao.SelStart = 0
                    txtDescricao.SelLength = Len(txtDescricao.Text)
                Else
                    fraBanco.Caption = "Inclus�o"
                    txtDescricao.Text = ""
                End If
            End If
        End If
    End If
End Sub
Private Sub txtDescricao_LostFocus()
    txtDescricao.Text = UCase(txtDescricao.Text)
End Sub
