Attribute VB_Name = "Variaveis"
Option Explicit
Public vSessao As Object                            'SESS�O DO ORACLE
Public vBanco As Object                             'BANCO DO ORACLE
Public vNomeBanco As String                         'NOME DO BANCO DO ARQUIVO .INI
Public vObjOracle As Object                         'OBJETO DO ORACLE
Public vCd As Integer                               'C�DIGO DO CD CONECTADO
Public vTipoCD As String                            'TIPO DO CD CONECTADO (U, M)
Public vLinhaArquivo                                'LEITURA DE LINHA DE ARQUIVO
Public vResposta                                    'GUARDA A ESCOLHA DO USU�RIO DA FUN��O PERGUNTA
Public vUsuarioBanco As String                      'USU�RIO DO BANCO
Public vUsuarioRede As String                       'USU�RIO DA REDE
Public vNomeSistema As String                       'NOME DO SISTEMA
Public vDescricaoSistema As String                  'DESCRI��O DO SISTEMA
Public vAnalistaResponsavel As String               'ANALISTA RESPONS�VEL PELO SISTEMA
Public vAnalistaBackup As String                    'ANALISTA BACKUP
Public vVB_Generica_001 As New clsVB_Generica_001
Public vVB_Venda_001 As New clsVB_Venda_001
Public vVB_PAAC_001 As New clsVB_PAAC_001
Public vVB_Cobranca_001 As New clsVB_Cobranca_001
Public vVB_Autolog_001 As New clsVB_Autolog_001
Public vvB_Seguranca_001 As New clsVB_Seguranca_001
Public vErro As String
Public vSelect As String
Public vSql As String

Global vUsuario As VB_Seguranca_001.Usuario

Sub Aguardar()

    frmAguardar.Show
    frmAguardar.Refresh

End Sub

Sub DefinirTelaSobre()

    vNomeSistema = UCase(App.Title)
    vDescricaoSistema = vObjOracle.Fields(0)
    vAnalistaResponsavel = vObjOracle.Fields(1)
    vAnalistaBackup = vObjOracle.Fields(2)
    
    frmSobre.lblNomeSistema = vNomeSistema & " - " & vDescricaoSistema
    frmSobre.lblResponsavel = UCase(vAnalistaResponsavel)
    frmSobre.lblBackup = UCase(vAnalistaBackup)
    
End Sub
Function Verifica_Numeros(KeyAscii As Integer) As Boolean
    If KeyAscii = 46 Then
        KeyAscii = 44
    Else
        If (KeyAscii < 48 Or KeyAscii > 57) And KeyAscii <> 8 And KeyAscii <> 13 And KeyAscii <> 44 Then
            Verifica_Numeros = False
            Exit Function
        End If
    End If
    Verifica_Numeros = True
End Function



