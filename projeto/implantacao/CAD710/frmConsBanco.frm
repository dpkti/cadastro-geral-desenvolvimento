VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmConsBanco 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Consulta de Ocorr�ncias por Banco"
   ClientHeight    =   5655
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9300
   Icon            =   "frmConsBanco.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   377
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   620
   StartUpPosition =   2  'CenterScreen
   Begin VB.ComboBox cmbBanco 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   1755
      Sorted          =   -1  'True
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   1050
      Width           =   4110
   End
   Begin Bot�o.cmd cmdFechar 
      Height          =   690
      Left            =   120
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Fechar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsBanco.frx":23D2
      PICN            =   "frmConsBanco.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSFlexGridLib.MSFlexGrid mfgConsulta 
      Height          =   4020
      Left            =   135
      TabIndex        =   2
      Top             =   1500
      Width           =   9015
      _ExtentX        =   15901
      _ExtentY        =   7091
      _Version        =   393216
      BackColorBkg    =   -2147483633
      HighLight       =   0
      ScrollBars      =   2
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   0
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   840
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin VB.Label lblCodBanco 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "C�digo do Banco"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   210
      Left            =   180
      TabIndex        =   3
      Top             =   1185
      Width           =   1380
   End
End
Attribute VB_Name = "frmConsBanco"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Sub cmbBanco_Click()
    
    'Call Aguardar
    vBanco.Parameters.Add "cOCORRENCIA", 0, 3
    vBanco.Parameters("cOCORRENCIA").ServerType = 102
    vBanco.Parameters("cOCORRENCIA").DynasetOption = &H2&
    vBanco.Parameters("cOCORRENCIA").DynasetCacheParams 256, 16, 20, 2000, 0
    vBanco.Parameters.Add "pBANCO", CInt(cmbBanco.ItemData(cmbBanco.ListIndex)), 1
    
    vSql = "PRODUCAO.PCK_CAD710.pr_BuscaOcorrencia(:cOCORRENCIA, :pBANCO)"
    'Unload frmAguardar
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro)
    Else
        Set vObjOracle = vBanco.Parameters("cOCORRENCIA").Value
    End If
 
    Call vVB_Generica_001.ExcluiBind(vBanco)
        
    If vObjOracle.RecordCount <> 0 Then
        Call vVB_Generica_001.CarregaGridTabela(mfgConsulta, vObjOracle, 4)
    Else
        Call vVB_Generica_001.Informar("N�o existem ocorr�ncias cadastradas para este banco")
    End If
End Sub
Private Sub cmdFechar_Click()
    Unload Me
End Sub
Private Sub Form_Load()
    vBanco.Parameters.Add "cBUSCABANCO", 0, 3
    vBanco.Parameters("cBUSCABANCO").ServerType = 102
    vBanco.Parameters("cBUSCABANCO").DynasetOption = &H2&
    vBanco.Parameters("cBUSCABANCO").DynasetCacheParams 256, 16, 20, 2000, 0
    
    vSql = "PRODUCAO.PCK_CAD710.pr_BuscaBanco(:cBUSCABANCO)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro)
    Else
        Set vObjOracle = vBanco.Parameters("cBUSCABANCO").Value
    End If
    Call vVB_Generica_001.ExcluiBind(vBanco)
    
    If vObjOracle.RecordCount <> 0 Then
        While Not vObjOracle.EOF
            cmbBanco.AddItem Format(vObjOracle!cod_banco, "000") & " - " & vObjOracle!sigla
            cmbBanco.ItemData(cmbBanco.NewIndex) = vObjOracle!cod_banco
            vObjOracle.MoveNext
        Wend
    End If
End Sub


