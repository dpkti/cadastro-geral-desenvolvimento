VERSION 4.00
Begin VB.Form frmMain 
   Caption         =   "CADASTRO DE TAXAS"
   ClientHeight    =   6915
   ClientLeft      =   1710
   ClientTop       =   1485
   ClientWidth     =   6765
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Height          =   7320
   Icon            =   "MAIN.frx":0000
   Left            =   1650
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   6915
   ScaleWidth      =   6765
   Top             =   1140
   Width           =   6885
   WindowState     =   2  'Maximized
   Begin VB.Timer Timer1 
      Interval        =   1
      Left            =   2265
      Top             =   240
   End
   Begin Threed.SSPanel pnlPrincipal 
      Height          =   2400
      Left            =   120
      TabIndex        =   0
      Top             =   1080
      Width           =   9375
      _version        =   65536
      _extentx        =   16536
      _extenty        =   4233
      _stockprops     =   15
      backcolor       =   12632256
      bevelwidth      =   2
      Begin VB.Label Label2 
         Caption         =   "Consultar"
         BeginProperty Font 
            name            =   "Times New Roman"
            charset         =   0
            weight          =   700
            size            =   9.75
            underline       =   0   'False
            italic          =   -1  'True
            strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   225
         Left            =   3540
         TabIndex        =   18
         Top             =   975
         Width           =   1065
      End
      Begin Threed.SSCommand SSCommand1 
         Height          =   750
         Left            =   3555
         TabIndex        =   17
         Top             =   1305
         Width           =   1170
         _version        =   65536
         _extentx        =   2064
         _extenty        =   1323
         _stockprops     =   78
      End
      Begin VB.Label Label1 
         BackColor       =   &H00808080&
         Caption         =   "Label1"
         Height          =   705
         Left            =   3615
         TabIndex        =   16
         Top             =   1425
         Width           =   1200
      End
      Begin Threed.SSCommand cmdcONSULTA 
         Height          =   750
         Index           =   0
         Left            =   1845
         TabIndex        =   7
         Top             =   1260
         Width           =   1170
         _version        =   65536
         _extentx        =   2064
         _extenty        =   1323
         _stockprops     =   78
         picture         =   "MAIN.frx":030A
      End
      Begin VB.Label Label7 
         BackColor       =   &H00808080&
         Height          =   735
         Index           =   0
         Left            =   1935
         TabIndex        =   6
         Top             =   1380
         Width           =   1170
      End
      Begin VB.Label lblCONSULTA 
         AutoSize        =   -1  'True
         Caption         =   "Alterar"
         BeginProperty Font 
            name            =   "Times New Roman"
            charset         =   1
            weight          =   700
            size            =   9.75
            underline       =   0   'False
            italic          =   -1  'True
            strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   1875
         TabIndex        =   5
         Top             =   975
         Width           =   600
      End
      Begin Threed.SSCommand CMDVENDA 
         Height          =   735
         Left            =   165
         TabIndex        =   4
         Top             =   1275
         Width           =   1170
         _version        =   65536
         _extentx        =   2064
         _extenty        =   1296
         _stockprops     =   78
         picture         =   "MAIN.frx":0624
      End
      Begin VB.Label Label5 
         BackColor       =   &H00808080&
         Height          =   735
         Left            =   255
         TabIndex        =   3
         Top             =   1380
         Width           =   1170
      End
      Begin VB.Label lblVENDA 
         AutoSize        =   -1  'True
         Caption         =   "Incluir"
         BeginProperty Font 
            name            =   "Times New Roman"
            charset         =   1
            weight          =   700
            size            =   9.75
            underline       =   0   'False
            italic          =   -1  'True
            strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   180
         TabIndex        =   2
         Top             =   975
         Width           =   570
      End
      Begin VB.Label lblMenu_principal 
         AutoSize        =   -1  'True
         Caption         =   "Menu Principal"
         BeginProperty Font 
            name            =   "Times New Roman"
            charset         =   1
            weight          =   700
            size            =   24
            underline       =   -1  'True
            italic          =   -1  'True
            strikethrough   =   0   'False
         EndProperty
         Height          =   540
         Left            =   270
         TabIndex        =   1
         Top             =   135
         Width           =   3165
      End
   End
   Begin Threed.SSPanel pnlAuxiliar 
      Height          =   2400
      Left            =   120
      TabIndex        =   8
      Top             =   3630
      Width           =   9375
      _version        =   65536
      _extentx        =   16536
      _extenty        =   4233
      _stockprops     =   15
      backcolor       =   12632256
      bevelwidth      =   2
      Begin VB.Label lblSair 
         AutoSize        =   -1  'True
         Caption         =   "Sair"
         BeginProperty Font 
            name            =   "Times New Roman"
            charset         =   1
            weight          =   700
            size            =   9.75
            underline       =   0   'False
            italic          =   -1  'True
            strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   570
         TabIndex        =   12
         Top             =   915
         Width           =   345
      End
      Begin Threed.SSCommand cmdSair 
         Height          =   735
         Left            =   240
         TabIndex        =   11
         Top             =   1215
         Width           =   1170
         _version        =   65536
         _extentx        =   2064
         _extenty        =   1296
         _stockprops     =   78
         picture         =   "MAIN.frx":093E
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         BackColor       =   &H00808080&
         ForeColor       =   &H80000008&
         Height          =   780
         Left            =   345
         TabIndex        =   10
         Top             =   1320
         Width           =   1155
      End
      Begin VB.Label lblMenu_Auxiliar 
         AutoSize        =   -1  'True
         Caption         =   "Menu Auxiliar"
         BeginProperty Font 
            name            =   "Times New Roman"
            charset         =   1
            weight          =   700
            size            =   24
            underline       =   -1  'True
            italic          =   -1  'True
            strikethrough   =   0   'False
         EndProperty
         Height          =   540
         Left            =   210
         TabIndex        =   9
         Top             =   135
         Width           =   2925
      End
   End
   Begin Threed.SSPanel SSPanel1 
      Height          =   435
      Left            =   0
      TabIndex        =   13
      Top             =   6480
      Width           =   9615
      _version        =   65536
      _extentx        =   16960
      _extenty        =   767
      _stockprops     =   15
      backcolor       =   12632256
      bevelinner      =   1
      Begin VB.Label Label6 
         BeginProperty Font 
            name            =   "MS Sans Serif"
            charset         =   1
            weight          =   700
            size            =   8.25
            underline       =   0   'False
            italic          =   0   'False
            strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   210
         TabIndex        =   15
         Top             =   105
         Width           =   840
      End
      Begin VB.Label Label4 
         BeginProperty Font 
            name            =   "MS Sans Serif"
            charset         =   1
            weight          =   700
            size            =   8.25
            underline       =   0   'False
            italic          =   0   'False
            strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   225
         Left            =   8865
         TabIndex        =   14
         Top             =   105
         Width           =   630
      End
   End
   Begin VB.Image imgDPK 
      Height          =   645
      Left            =   180
      Picture         =   "MAIN.frx":0C58
      Top             =   135
      Width           =   1725
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Option Explicit
Public tipo_menu As Integer
Sub MudaCor()

     lblVENDA.ForeColor = 8421504
    lblCONSULTA.ForeColor = 8421504
    'LBLGERENCIAL.ForeColor = 8421504
    lblSair.ForeColor = 8421504
    
End Sub















Private Sub cmdCancela_Click()
frmCancela.Show vbModal

End Sub

Private Sub cmdCancela_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
  MudaCor
    lblCancela.ForeColor = 8388608
    cmdCancela.SetFocus
End Sub





Private Sub cmdReimp_Click()
End Sub

Private Sub cmdReimp_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
  MudaCor
    lblVENDA.ForeColor = 8388608
    CMDVENDA.SetFocus
End Sub


Private Sub CMDAJUDA_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
   MudaCor
    LBLAJUDA.ForeColor = 8388608
    CMDAJUDA.SetFocus

End Sub


Private Sub cmdcONSULTA_Click()
    frmprox.Show vbModal

End Sub

Private Sub cmdcONSULTA_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    MudaCor
    lblCONSULTA.ForeColor = 8388608
    cmdcONSULTA.SetFocus
End Sub


Private Sub cmdRelat_Click()
frmDtRel.Show vbModal

End Sub


Private Sub cmdRelat_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
End Sub


Private Sub cmdRemove_click()

Dim mens As String
Dim cmd As String

 Screen.MousePointer = 0
  mens = "Deleta recibos at� " & (dt_imp - 1)
  If MsgBox(mens, 4, "Relat�rio de Recibos") = vbYes Then
     Screen.MousePointer = 11
     cmd = "Delete from recibo " & _
            "where data <='" & (dt_imp - 1) & "'"
       
      DAcs.Execute cmd
      
      cmd = "Delete from cheque " & _
            "where data <= '" & (dt_imp - 1) & "'"
      
      DAcs.Execute cmd
      
      Screen.MousePointer = 0
   End If
End Sub

Private Sub cmdRemove_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
 MudaCor
    lblemite.ForeColor = 8388608
    cmdEmissao.SetFocus
End Sub


Private Sub cmdEmissao_Click(Index As Integer)

End Sub

Private Sub CMDGERENCIAL_Click()
    FRMGERENCIAL.Show vbModal
End Sub

Private Sub CMDINFO_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    MudaCor
    LBLINFO.ForeColor = 8388608
    CMDINFO.SetFocus

End Sub




Private Sub CMDROTINA_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    MudaCor
    LBLROTINA.ForeColor = 8388608
    CMDROTINA.SetFocus

End Sub


Private Sub cmdSair_Click()
    
    End
End Sub

Private Sub cmdSair_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

    MudaCor
    lblSair.ForeColor = 8388608
    cmdSair.SetFocus

End Sub






Private Sub CMDUTIL_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    MudaCor
    LBLUTIL.ForeColor = 8388608
    CMDUTIL.SetFocus
End Sub


Private Sub CMDVENDA_Click()
    frmsem_atual.Show
End Sub

Private Sub CMDVENDA_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    MudaCor
    lblVENDA.ForeColor = 8388608
    CMDVENDA.SetFocus
End Sub


Private Sub Command1_Click()
'commomdialog1.ShowPrinter

End Sub

Private Sub Form_Load()
Dim cmd As String
Label6.Caption = Format(Date, "short date")

If App.PrevInstance Then
   MsgBox "Exite mais de uma inst�ncia aberta"
   End
End If
    'ESTABELECE CONEX�O COM O ORACLE,
Set orasession = CreateObject("oracleinproCServer.xorasession")
'Set oradatabase = orasession.OpenDatabase("PRODUCAO", "PRODUCAO/DPK", 0&)
Set oradatabase = orasession.OpenDatabase("DESENV", "DESENV/DPK", 0&)

'Set DAcs = OpenDatabase("f:\sistemas\oracle\rec520\vb\RECIBO.mdb", False, False)
'Set DAcs = OpenDatabase("h:\dados\RECIBO.mdb", False, False)

'
End Sub


   
   


Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

    MudaCor

End Sub






Private Sub Label11_Click()
End Sub

Private Sub Label5_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    MudaCor
End Sub


Private Sub Label7_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    MudaCor
End Sub



Private Sub pnlAuxiliar_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

    MudaCor
    lblMenu_principal.ForeColor = 8421504
    lbLmenu_Auxiliar.ForeColor = 0

End Sub


Private Sub pnlPrincipal_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    MudaCor
    lblMenu_principal.ForeColor = 0
    lbLmenu_Auxiliar.ForeColor = 8421504
End Sub




Private Sub SSCommand1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    MudaCor
    LBLROTINA.ForeColor = 8388608
    CMDROTINA.SetFocus
End Sub


'Private Sub Timer1_Timer()
'Label4.Caption = Format(Time, "HH:MM")
'End Sub


