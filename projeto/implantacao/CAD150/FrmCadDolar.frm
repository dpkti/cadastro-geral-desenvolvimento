VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form FrmCadDolar 
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cadastro de D�lar"
   ClientHeight    =   6630
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5460
   Icon            =   "FrmCadDolar.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   442
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   364
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Caption         =   "Cota��o do Dia"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   4425
      Left            =   30
      TabIndex        =   41
      Top             =   1770
      Width           =   5385
      Begin VB.TextBox txtDia 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   31
         Left            =   4080
         MaxLength       =   12
         TabIndex        =   32
         Top             =   3900
         Width           =   1170
      End
      Begin VB.TextBox txtDia 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   30
         Left            =   4080
         MaxLength       =   12
         TabIndex        =   31
         Top             =   3540
         Width           =   1170
      End
      Begin VB.TextBox txtDia 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   29
         Left            =   4080
         MaxLength       =   12
         TabIndex        =   30
         Top             =   3180
         Width           =   1170
      End
      Begin VB.TextBox txtDia 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   28
         Left            =   4080
         MaxLength       =   12
         TabIndex        =   29
         Top             =   2820
         Width           =   1170
      End
      Begin VB.TextBox txtDia 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   27
         Left            =   4080
         MaxLength       =   12
         TabIndex        =   28
         Top             =   2460
         Width           =   1170
      End
      Begin VB.TextBox txtDia 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   26
         Left            =   4080
         MaxLength       =   12
         TabIndex        =   27
         Top             =   2100
         Width           =   1170
      End
      Begin VB.TextBox txtDia 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   25
         Left            =   4080
         MaxLength       =   12
         TabIndex        =   26
         Top             =   1740
         Width           =   1170
      End
      Begin VB.TextBox txtDia 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   24
         Left            =   4080
         MaxLength       =   12
         TabIndex        =   25
         Top             =   1380
         Width           =   1170
      End
      Begin VB.TextBox txtDia 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   23
         Left            =   4080
         MaxLength       =   12
         TabIndex        =   24
         Top             =   1020
         Width           =   1170
      End
      Begin VB.TextBox txtDia 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   22
         Left            =   4080
         MaxLength       =   12
         TabIndex        =   23
         Top             =   660
         Width           =   1170
      End
      Begin VB.TextBox txtDia 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   21
         Left            =   4080
         MaxLength       =   12
         TabIndex        =   22
         Top             =   300
         Width           =   1170
      End
      Begin VB.TextBox txtDia 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   17
         Left            =   2280
         MaxLength       =   12
         TabIndex        =   18
         Top             =   2460
         Width           =   1170
      End
      Begin VB.TextBox txtDia 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   16
         Left            =   2280
         MaxLength       =   12
         TabIndex        =   17
         Top             =   2100
         Width           =   1170
      End
      Begin VB.TextBox txtDia 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   15
         Left            =   2280
         MaxLength       =   12
         TabIndex        =   16
         Top             =   1740
         Width           =   1170
      End
      Begin VB.TextBox txtDia 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   13
         Left            =   2280
         MaxLength       =   12
         TabIndex        =   14
         Top             =   1020
         Width           =   1170
      End
      Begin VB.TextBox txtDia 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   12
         Left            =   2280
         MaxLength       =   12
         TabIndex        =   13
         Top             =   660
         Width           =   1170
      End
      Begin VB.TextBox txtDia 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   11
         Left            =   2280
         MaxLength       =   12
         TabIndex        =   12
         Top             =   300
         Width           =   1170
      End
      Begin VB.TextBox txtDia 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   20
         Left            =   2280
         MaxLength       =   12
         TabIndex        =   21
         Top             =   3540
         Width           =   1170
      End
      Begin VB.TextBox txtDia 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   19
         Left            =   2280
         MaxLength       =   12
         TabIndex        =   20
         Top             =   3180
         Width           =   1170
      End
      Begin VB.TextBox txtDia 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   18
         Left            =   2280
         MaxLength       =   12
         TabIndex        =   19
         Top             =   2820
         Width           =   1170
      End
      Begin VB.TextBox txtDia 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   14
         Left            =   2280
         MaxLength       =   12
         TabIndex        =   15
         Top             =   1380
         Width           =   1170
      End
      Begin VB.TextBox txtDia 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   10
         Left            =   510
         MaxLength       =   12
         TabIndex        =   11
         Top             =   3540
         Width           =   1170
      End
      Begin VB.TextBox txtDia 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   9
         Left            =   510
         MaxLength       =   12
         TabIndex        =   10
         Top             =   3180
         Width           =   1170
      End
      Begin VB.TextBox txtDia 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   8
         Left            =   510
         MaxLength       =   12
         TabIndex        =   9
         Top             =   2820
         Width           =   1170
      End
      Begin VB.TextBox txtDia 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   7
         Left            =   510
         MaxLength       =   12
         TabIndex        =   8
         Top             =   2460
         Width           =   1170
      End
      Begin VB.TextBox txtDia 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   6
         Left            =   510
         MaxLength       =   12
         TabIndex        =   7
         Top             =   2100
         Width           =   1170
      End
      Begin VB.TextBox txtDia 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   5
         Left            =   510
         MaxLength       =   12
         TabIndex        =   6
         Top             =   1740
         Width           =   1170
      End
      Begin VB.TextBox txtDia 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   4
         Left            =   510
         MaxLength       =   12
         TabIndex        =   5
         Top             =   1380
         Width           =   1170
      End
      Begin VB.TextBox txtDia 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   3
         Left            =   510
         MaxLength       =   12
         TabIndex        =   4
         Top             =   1020
         Width           =   1170
      End
      Begin VB.TextBox txtDia 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   2
         Left            =   510
         MaxLength       =   12
         TabIndex        =   3
         Top             =   660
         Width           =   1170
      End
      Begin VB.TextBox txtDia 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   1
         Left            =   510
         MaxLength       =   12
         TabIndex        =   2
         Top             =   300
         Width           =   1170
      End
      Begin VB.Label lbldia 
         Appearance      =   0  'Flat
         Caption         =   "31"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   31
         Left            =   3690
         TabIndex        =   72
         Top             =   3930
         Width           =   300
      End
      Begin VB.Label lbldia 
         Appearance      =   0  'Flat
         Caption         =   "30"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   30
         Left            =   3690
         TabIndex        =   71
         Top             =   3570
         Width           =   300
      End
      Begin VB.Label lbldia 
         Appearance      =   0  'Flat
         Caption         =   "29"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   29
         Left            =   3690
         TabIndex        =   70
         Top             =   3210
         Width           =   300
      End
      Begin VB.Label lbldia 
         Appearance      =   0  'Flat
         Caption         =   "28"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   28
         Left            =   3690
         TabIndex        =   69
         Top             =   2850
         Width           =   300
      End
      Begin VB.Label lbldia 
         Appearance      =   0  'Flat
         Caption         =   "27"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   27
         Left            =   3690
         TabIndex        =   68
         Top             =   2490
         Width           =   300
      End
      Begin VB.Label lbldia 
         Appearance      =   0  'Flat
         Caption         =   "26"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   26
         Left            =   3690
         TabIndex        =   67
         Top             =   2130
         Width           =   300
      End
      Begin VB.Label lbldia 
         Appearance      =   0  'Flat
         Caption         =   "25"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   25
         Left            =   3690
         TabIndex        =   66
         Top             =   1770
         Width           =   300
      End
      Begin VB.Label lbldia 
         Appearance      =   0  'Flat
         Caption         =   "24"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   24
         Left            =   3690
         TabIndex        =   65
         Top             =   1410
         Width           =   300
      End
      Begin VB.Label lbldia 
         Appearance      =   0  'Flat
         Caption         =   "23"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   23
         Left            =   3690
         TabIndex        =   64
         Top             =   1050
         Width           =   300
      End
      Begin VB.Label lbldia 
         Appearance      =   0  'Flat
         Caption         =   "22"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   22
         Left            =   3690
         TabIndex        =   63
         Top             =   690
         Width           =   300
      End
      Begin VB.Label lbldia 
         Appearance      =   0  'Flat
         Caption         =   "21"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   21
         Left            =   3690
         TabIndex        =   62
         Top             =   330
         Width           =   300
      End
      Begin VB.Label lbldia 
         Appearance      =   0  'Flat
         Caption         =   "20"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   20
         Left            =   1890
         TabIndex        =   61
         Top             =   3570
         Width           =   300
      End
      Begin VB.Label lbldia 
         Appearance      =   0  'Flat
         Caption         =   "19"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   19
         Left            =   1890
         TabIndex        =   60
         Top             =   3210
         Width           =   300
      End
      Begin VB.Label lbldia 
         Appearance      =   0  'Flat
         Caption         =   "18"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   18
         Left            =   1890
         TabIndex        =   59
         Top             =   2850
         Width           =   300
      End
      Begin VB.Label lbldia 
         Appearance      =   0  'Flat
         Caption         =   "17"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   17
         Left            =   1890
         TabIndex        =   58
         Top             =   2490
         Width           =   300
      End
      Begin VB.Label lbldia 
         Appearance      =   0  'Flat
         Caption         =   "16"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   16
         Left            =   1890
         TabIndex        =   57
         Top             =   2130
         Width           =   300
      End
      Begin VB.Label lbldia 
         Appearance      =   0  'Flat
         Caption         =   "15"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   15
         Left            =   1890
         TabIndex        =   56
         Top             =   1770
         Width           =   300
      End
      Begin VB.Label lbldia 
         Appearance      =   0  'Flat
         Caption         =   "14"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   14
         Left            =   1890
         TabIndex        =   55
         Top             =   1410
         Width           =   300
      End
      Begin VB.Label lbldia 
         Appearance      =   0  'Flat
         Caption         =   "13"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   13
         Left            =   1890
         TabIndex        =   54
         Top             =   1050
         Width           =   300
      End
      Begin VB.Label lbldia 
         Appearance      =   0  'Flat
         Caption         =   "12"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   12
         Left            =   1890
         TabIndex        =   53
         Top             =   690
         Width           =   300
      End
      Begin VB.Label lbldia 
         Appearance      =   0  'Flat
         Caption         =   "11"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   11
         Left            =   1890
         TabIndex        =   52
         Top             =   330
         Width           =   300
      End
      Begin VB.Label lbldia 
         Appearance      =   0  'Flat
         Caption         =   "10"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   10
         Left            =   120
         TabIndex        =   51
         Top             =   3600
         Width           =   300
      End
      Begin VB.Label lbldia 
         Appearance      =   0  'Flat
         Caption         =   "09"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   9
         Left            =   120
         TabIndex        =   50
         Top             =   3240
         Width           =   300
      End
      Begin VB.Label lbldia 
         Appearance      =   0  'Flat
         Caption         =   "08"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   8
         Left            =   120
         TabIndex        =   49
         Top             =   2880
         Width           =   300
      End
      Begin VB.Label lbldia 
         Appearance      =   0  'Flat
         Caption         =   "07"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   7
         Left            =   120
         TabIndex        =   48
         Top             =   2520
         Width           =   300
      End
      Begin VB.Label lbldia 
         Appearance      =   0  'Flat
         Caption         =   "06"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   6
         Left            =   120
         TabIndex        =   47
         Top             =   2160
         Width           =   300
      End
      Begin VB.Label lbldia 
         Appearance      =   0  'Flat
         Caption         =   "05"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   5
         Left            =   120
         TabIndex        =   46
         Top             =   1800
         Width           =   300
      End
      Begin VB.Label lbldia 
         Appearance      =   0  'Flat
         Caption         =   "04"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   4
         Left            =   120
         TabIndex        =   45
         Top             =   1440
         Width           =   300
      End
      Begin VB.Label lbldia 
         Appearance      =   0  'Flat
         Caption         =   "03"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   3
         Left            =   120
         TabIndex        =   44
         Top             =   1080
         Width           =   300
      End
      Begin VB.Label lbldia 
         Appearance      =   0  'Flat
         Caption         =   "02"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   2
         Left            =   120
         TabIndex        =   43
         Top             =   720
         Width           =   300
      End
      Begin VB.Label lbldia 
         Appearance      =   0  'Flat
         Caption         =   "01"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   1
         Left            =   120
         TabIndex        =   42
         Top             =   360
         Width           =   300
      End
   End
   Begin VB.Frame fra 
      Caption         =   "Data"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   735
      Left            =   30
      TabIndex        =   38
      Top             =   930
      Width           =   5385
      Begin VB.TextBox txtAno 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   570
         MaxLength       =   4
         TabIndex        =   0
         Top             =   270
         Width           =   510
      End
      Begin VB.ComboBox cboMes 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         ItemData        =   "FrmCadDolar.frx":23D2
         Left            =   3480
         List            =   "FrmCadDolar.frx":23FA
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   240
         Width           =   1725
      End
      Begin VB.Label lblTpAno 
         Appearance      =   0  'Flat
         Caption         =   "lblTpAno"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   225
         Left            =   1470
         TabIndex        =   73
         Top             =   300
         Visible         =   0   'False
         Width           =   540
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         Caption         =   "M�s"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   3060
         TabIndex        =   40
         Top             =   300
         Width           =   690
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         Caption         =   "Ano"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   0
         Left            =   150
         TabIndex        =   39
         Top             =   330
         Width           =   690
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   33
      Top             =   6300
      Width           =   5460
      _ExtentX        =   9631
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   9578
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   34
      TabStop         =   0   'False
      Top             =   810
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   35
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "FrmCadDolar.frx":2463
      PICN            =   "FrmCadDolar.frx":247F
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdOK 
      Height          =   690
      Left            =   810
      TabIndex        =   36
      TabStop         =   0   'False
      ToolTipText     =   "Confirmar Dados"
      Top             =   30
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "FrmCadDolar.frx":3159
      PICN            =   "FrmCadDolar.frx":3175
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdCancel 
      Height          =   690
      Left            =   1590
      TabIndex        =   37
      TabStop         =   0   'False
      ToolTipText     =   "Limpar Campos"
      Top             =   30
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "FrmCadDolar.frx":3E4F
      PICN            =   "FrmCadDolar.frx":3E6B
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "FrmCadDolar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim FlagPrimeiraVez As Boolean
Dim Flag_Erro As Boolean

Sub TravaDestravaTexto()

    On Error GoTo TrataErro
    
    For Each vControl In Me.Controls
        If TypeOf vControl Is TextBox And vControl.Name <> txtAno.Name Then
            If vControl.Enabled = True Then
                vControl.Enabled = False
            Else
                vControl.Enabled = True
            End If
        End If
    Next
    
    Exit Sub
    
TrataErro:
    If Err.Number = 340 Then
        Load vControl
        Resume Next
    End If
    
End Sub

Sub VISIVEL()
'ESTA SUB-ROTINA TORNA VIS�VEL (DEFAULT) OS
'CAMPOS QUE EVENTUALMENTE SER�O ALTERADOS, DE
'ACORDO COM OS DIAS DO RESPECTIVO M�S.

    txtDia(29).Visible = True
    txtDia(29).Text = ""
    lbldia(29).Visible = True
    txtDia(30).Visible = True
    txtDia(30).Text = ""
    lbldia(30).Visible = True
    
End Sub

Sub MOSTRARDIASDOMES()

    On Error GoTo TrataErro
    
    Call VISIVEL
    '0=Jan  2=Mar  4=Mai  6=Jul  7=Ago  9=Out  11=Dez
    If cboMes.ListIndex = 0 Or _
       cboMes.ListIndex = 2 Or _
       cboMes.ListIndex = 4 Or _
       cboMes.ListIndex = 6 Or _
       cboMes.ListIndex = 7 Or _
       cboMes.ListIndex = 9 Or _
       cboMes.ListIndex = 11 Then
       txtDia(31).Visible = True
       txtDia(31).Text = ""
       lbldia(31).Visible = True
    '3=Abr  5=Jun  8=Set  10=Nov
    ElseIf cboMes.ListIndex = 3 Or _
           cboMes.ListIndex = 5 Or _
           cboMes.ListIndex = 8 Or _
           cboMes.ListIndex = 10 Then
           txtDia(31).Visible = False
           lbldia(31).Visible = False
    'Fev Normal
    ElseIf cboMes.ListIndex = 1 And _
           lblTpAno = "Normal" Then
           txtDia(29).Visible = False
           lbldia(29).Visible = False
           txtDia(30).Visible = False
           lbldia(30).Visible = False
           txtDia(31).Visible = False
           lbldia(31).Visible = False
    'Fev Bissexto
    Else
           txtDia(30).Visible = False
           lbldia(30).Visible = False
           txtDia(31).Visible = False  'aqui d� o erro
           lbldia(31).Visible = False
    End If

    Exit Sub
    
TrataErro:

    If Err.Number = 340 Then
        Load txtDia(i)
        Resume Next
    End If
    
End Sub

Sub LIMPA_EXIBE()

    'ESTA SUB-ROTINA LIMPA TODAS AS TEXTBOX, EXCETO
    'A txtAno, PARA A ENTRADA DE NOVOS DADOS.
    
    For Each CurControl In Controls
        If TypeOf CurControl Is TextBox Then
            If CurControl <> txtAno Then
                CurControl.Text = ""
            End If
        End If
    Next

End Sub

Sub EXIBE_VALORES()

    'ESTA SUB-ROTINA PEGA MES E ANO DIGITADOS E PESQUI-
    'SA NO ORACLE A EXIST�NCIA DE VALORES, FAZENDO AS
    'CONSIST�NCIAS DE ACORDO COM A VARI�VEL "FL_MODO", QUE
    'INDICA SE � INCLUSAO(FALSE) OU ALTERA��O(TRUE).
    
    Dim CMD As String
    Dim MES As String
    Dim ANO As String
    Dim DATA As String
    Dim x As Integer
    
    If txtDia(1).Enabled = False Then
        Call TravaDestravaTexto
    End If
    
    'RETORNA ESTADO NORMAL DO M�S PARA PODER PESQUISAR
    'NO ORACLE.
    
    x = cboMes.ListIndex + 1
    
    If cboMes.Text = "Janeiro" Then
       MES = "01"
    ElseIf cboMes.Text = "Fevereiro" Then: MES = "02"
    ElseIf cboMes.Text = "Mar�o" Then: MES = "03"
    ElseIf cboMes.Text = "Abril" Then: MES = "04"
    ElseIf cboMes.Text = "Maio" Then: MES = "05"
    ElseIf cboMes.Text = "Junho" Then: MES = "06"
    ElseIf cboMes.Text = "Julho" Then: MES = "07"
    ElseIf cboMes.Text = "Agosto" Then: MES = "08"
    ElseIf cboMes.Text = "Setembro" Then: MES = "09"
    ElseIf cboMes.Text = "Outubro" Then: MES = "10"
    ElseIf cboMes.Text = "Novembro" Then: MES = "11"
    Else:  MES = "12"
    End If
    
    ANO = txtAno
    DATA = MES & "-" & ANO
    
    'INICIALIZA VARIAVEIS PARA PL-SQL
    Call vVB_Generica_001.ExcluiBind(vBanco)
    Set vObjOracle = Nothing
    Call Criar_Cursor(vBanco, "vCursor")
    vBanco.Parameters.Add "DATA", DATA, 1
    vSql = "PRODUCAO.PCK_CAD150.pr_select_dolar_mensal(:vCursor,:DATA)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    If vErro <> "" Then
        Call vVB_Generica_001.ProcessaErro(vErro)
        Exit Sub
    Else
        Set vObjOracle = vParameters("vCursor").Value
    End If
    
    'SE N�O TROUXER NADA DO ORACLE
    'E FOR ALTERA��O(TRUE) : ENVIA MSG. DE ERRO E SAI DO SUB.
    'SE FOR INCLUS�O(FALSE): SAI NORMAL DA ROTINA E DEIXA USUARIO
    '                        ENTRAR COM OS VALORES DO D�LAR.
    '******
    'SE TROUXER DADOS DO ORACLE
    'E FOR INCLUS�O(FALSE) : ENVIA MSG. DE ERRO E SAI DO SUB.
    'SE FOR ALTERA��O(TRUE): EXIBE DADOS E DEIXA FAZER ALTERA��O
    Flag_Erro = False
    If vObjOracle.RecordCount = 0 Then
        If flModo = True Then
            'If FlagPrimeiraVez = False Then
            Call vVB_Generica_001.Informar("N�o existem valores de d�lar para esta data!")
            Call TravaDestravaTexto
            cboMes.ListIndex = -1
            'End If
            'FlagPrimeiraVez = False
            Flag_Erro = True
            'cboMes.SetFocus
            'txtAno.SetFocus
        End If
        Exit Sub
    Else
        If flModo = False Then
            'If FlagPrimeiraVez = False Then
            Call vVB_Generica_001.Informar("J� existem valores de d�lar para esta data!")
            Call TravaDestravaTexto
            cboMes.ListIndex = -1
            'cboMes.SetFocus
            'End If
            'FlagPrimeiraVez = False
            Flag_Erro = True
            'txtAno.SetFocus
            Exit Sub
        End If
    
        'ATRIBUI AOS CAMPOS, OS VALORES TRAZIDOS DO ORA-
        'CLE, PARA ALTERA��O.
        For i = 1 To 31
            
            If vObjOracle.EOF Then
                Exit Sub
            End If
            
            txtDia(i) = Format(vObjOracle.Fields("VALOR_USS"), "#####0.000000")
            vObjOracle.MoveNext
        
        Next i
        
    End If
   
End Sub

Private Sub cboMes_Click()

    If Trim(cboMes) = "" Then
        Exit Sub
    End If
    If Trim(txtAno) = "" Then
        Call vVB_Generica_001.Informar("Digite o ano!")
        txtAno.SetFocus
        Exit Sub
    ElseIf Len(txtAno) <> 4 Then
        Call vVB_Generica_001.Informar("O ano deve ter 4 d�gitos!")
        txtAno = ""
        txtAno.SetFocus
        Exit Sub
    ElseIf txtAno.Text = "0000" Then
        Call vVB_Generica_001.Informar("Ano inv�lido!")
        txtAno = ""
        txtAno.SetFocus
        Exit Sub
    End If
    Call MOSTRARDIASDOMES
    Call LIMPA_EXIBE
    Call EXIBE_VALORES
    
End Sub


Private Sub cmdCancel_Click()

    Call Limpa(FrmCadDolar)
    txtAno = Year(Now)
    If BISSEXTO(txtAno) = True Then
        lblTpAno = "Bissexto"
    Else
        lblTpAno = "Normal"
    End If
    For i = 1 To 31
        txtDia(i).Enabled = False
    Next i
    cboMes.SetFocus
    
End Sub

Private Sub cmdOK_Click()

    Dim MES As String   'VARI�VEL QUE GUARDA O M�S NO FORMATO A SER GRAVADO
    Dim ANO As String   'VARI�VEL ONDE SER� ARMAZENADO OS 2 �LTIMOS NO.DO ANO
    Dim DATA As String  'VARI�VEL ONDE SER� MONTADA A DATA PARA GRAVAR
    Dim DATAREAL As String  'VARI�VEL QUE GUARDA A DATA CORRENTE
    Dim VALOR As Double 'VARI�VEL ONDE SER� MONTADO O VALOR A SER GRAVADO
    Dim DIAS As Integer 'VARI�VEL QUE VAI GUARDAR A QTD. DE DIAS DO M�S

    If Trim(txtAno) = "" Then
        Call vVB_Generica_001.Informar("Digite o ano!")
        txtAno.SetFocus
        Exit Sub
    ElseIf Len(txtAno) <> 4 Then
        Call vVB_Generica_001.Informar("O ano deve ter 4 d�gitos!")
        txtAno = ""
        txtAno.SetFocus
        Exit Sub
    ElseIf txtAno.Text = "0000" Then
        Call vVB_Generica_001.Informar("Ano inv�lido!")
        txtAno = ""
        txtAno.SetFocus
        Exit Sub
    ElseIf cboMes.Text = "" Then
        Call vVB_Generica_001.Informar("Selecione o m�s!")
        cboMes.SetFocus
        Exit Sub
    End If
    
    If Flag_Erro = True Then
       If flModo = False Then
           Call vVB_Generica_001.Informar("Inclus�o inv�lida. Verifique!")
           Exit Sub
       Else
           Call vVB_Generica_001.Informar("Altera��o inv�lida. Verifique!")
           Exit Sub
        End If
    End If
    If VERIF_CAMPO(FrmCadDolar) = False Then
        'txtDia(i).SetFocus
        Exit Sub
    End If
    Set vObjOracle = vVB_Generica_001.TabelaDatas(vBanco)
    DATAREAL = Format(vObjOracle.Fields(1), "DD-MM-YYYY")
    If vVB_Generica_001.Perguntar("Confirma os Dados?") = 7 Then
        Exit Sub
    End If
    'ATRIBUI � VARI�VEL DIAS, A QTD. DE DIAS QUE O M�S POSSUI
    If cboMes.Text = "Fevereiro" And lblTpAno.Caption = "Normal" Then
        DIAS = 28  '28 DIAS
    ElseIf cboMes.Text = "Fevereiro" And lblTpAno.Caption = "Bissexto" Then
        DIAS = 29  '29 DIAS
    ElseIf cboMes.Text = "Abril" Or cboMes.Text = "Junho" Or _
        cboMes.Text = "Setembro" Or cboMes.Text = "Novembro" Then
        DIAS = 30  '30 DIAS
    Else
        DIAS = 31  '31 DIAS
    End If
   
    'Atribui valores de mes e ano p/ acessar ORACLE
    MES = Format(cboMes.ListIndex + 1, "00")
    ANO = txtAno.Text
    
    'PARA TODOS OS DIAS DO M�S - IN�CIO 01 - VAI GRAVAR VALOR E DATA
    
    If flModo = False Then 'INCLUS�O
    
        For i = 1 To DIAS
            DATA = lbldia(i) & "-" & MES & "-" & ANO
            VALOR = Val(Format(txtDia(i), "#####0.000000"))
            If CDate(DATA) < CDate(DATAREAL) Then
                If Right(DATA, 7) <> Right(DATAREAL, 7) Then 'm�s diferente do atual
                    Call vVB_Generica_001.Informar("A data n�o pode ser anterior � atual!!")
                    txtAno.SetFocus
                    Exit Sub
                End If
            End If
            Call vVB_Generica_001.ExcluiBind(vBanco)
            vBanco.Parameters.Add "DATA", DATA, 1
            vBanco.Parameters.Add "VALOR", VALOR, 1
            vSql = "PRODUCAO.PCK_CAD150.pr_insert_dolar(:DATA,:VALOR)"
            vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
            If vErro <> "" Then
                Call vVB_Generica_001.ProcessaErro(vErro)
                Exit Sub
            End If
            If i = DIAS Then
                Call vVB_Generica_001.Informar("Inclus�o OK!")
                Exit For
            End If
        Next i
    Else 'ALTERA��O
        For i = 1 To DIAS
            DATA = lbldia(i) & "-" & MES & "-" & ANO
            VALOR = Val(Format(txtDia(i), "#####0.000000"))
            If CDate(DATA) < CDate(DATAREAL) Then
                If Right(DATA, 7) <> Right(DATAREAL, 7) Then 'm�s diferente do atual
                    Call vVB_Generica_001.Informar("A data n�o pode ser anterior � atual!!")
                    txtAno.SetFocus
                    Exit Sub
                End If
            End If
            Call vVB_Generica_001.ExcluiBind(vBanco)
            vBanco.Parameters.Add "DATA", DATA, 1
            vBanco.Parameters.Add "VALOR", VALOR, 1
            vSql = "PRODUCAO.PCK_CAD150.pr_update_dolar(:DATA,:VALOR)"
            vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
            If vErro <> "" Then
                Call vVB_Generica_001.ProcessaErro(vErro)
                Exit Sub
            End If
            If i = DIAS Then
                Call vVB_Generica_001.Informar("Altera��o OK!")
                Exit For
            End If
        Next i
    End If
    
    Call LIMPA_EXIBE
    Call vVB_Generica_001.ExcluiBind(vBanco)
    
End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub

Private Sub Form_Activate()

    txtAno = Year(Now)
    
End Sub

Private Sub Form_Load()
    
    On Error GoTo TrataErro
    
    Me.Top = 0
    Me.Left = 0
    'FLAG P/ EVITAR MENSAGEM NA CHAMADA DE INCLUSAO OU ALTERACAO
    FlagPrimeiraVez = True
    'FLAG P/ DETECTAR SE INCLUSAO JA EXISTE OU ALTERACAO NAO
    Flag_Erro = True
    Call Limpa(FrmCadDolar)
    If flModo = False Then
        FrmCadDolar.Caption = "Inclus�o de D�lar Di�rio"
    Else
        FrmCadDolar.Caption = "Altera��o de D�lar Di�rio"
    End If
    If BISSEXTO(txtAno) = True Then
        lblTpAno = "Bissexto"
    Else
        lblTpAno = "Normal"
    End If
    For i = 1 To 31
        txtDia(i).Enabled = False
    Next i
    
    Exit Sub
    
TrataErro: 'criado p/ evitar bug no array de txt
    
    If Err.Number = 340 Then
        Load txtDia(i)
        Resume Next
    End If
    
End Sub

Private Sub txtAno_Change()

    If Len(txtAno) = 4 Then
        cboMes.SetFocus
    End If
    Call LIMPA_EXIBE
    
End Sub

Private Sub txtAno_GotFocus()

    Call vVB_Generica_001.SelecionaCampo(txtAno)
    
End Sub

Private Sub txtAno_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Numero(KeyAscii)
    
End Sub


Private Sub txtAno_LostFocus()

    If Trim(txtAno.Text) = "" Then
        Exit Sub
    End If
    
    If Trim(txtAno) <> "" And Len(txtAno) < 4 Then
        Call vVB_Generica_001.Informar("O ano deve ter 4 d�gitos!")
        txtAno = ""
        txtAno.SetFocus
        Exit Sub
    End If
    
    If BISSEXTO(txtAno.Text) = True Then
        lblTpAno.Caption = "Bissexto"
    Else
        lblTpAno.Caption = "Normal"
    End If
    
    cboMes.SetFocus
    
End Sub

Private Sub txtDia_Change(Index As Integer)

    If Len(txtDia(Index)) = 12 Then
        If Index = 31 Then
            Call cmdOK_Click
        ElseIf Index = 28 And cboMes.ListIndex = 1 And _
            lblTpAno = "Normal" Then
            Call cmdOK_Click
        ElseIf Index = 29 And cboMes.ListIndex = 1 And _
            lblTpAno = "Bissexto" Then
            Call cmdOK_Click
'        ElseIf Index = 30 And cboMes.ListIndex = 3 Or _
'        cboMes.ListIndex = 5 Or cboMes.ListIndex = 8 _
'        Or cboMes.ListIndex = 10 Then
'            Call cmdOK_Click
        ElseIf Index = 30 Then
            If cboMes.ListIndex = 3 Or cboMes.ListIndex = 5 Or _
            cboMes.ListIndex = 8 Or cboMes.ListIndex = 10 Then
                Call cmdOK_Click
            End If
        Else
            txtDia(Index + 1).SetFocus
        End If
    End If
    
End Sub

Private Sub txtDia_GotFocus(Index As Integer)

    Call vVB_Generica_001.SelecionaCampo(txtDia(Index))

End Sub

Private Sub txtDia_KeyPress(Index As Integer, KeyAscii As Integer)

    If KeyAscii = 13 Then
        If Index = 31 Then
            Call cmdOK_Click
        ElseIf Index = 28 And cboMes.ListIndex = 1 And _
            lblTpAno = "Normal" Then
            Call cmdOK_Click
        ElseIf Index = 29 And cboMes.ListIndex = 1 And _
            lblTpAno = "Bissexto" Then
            Call cmdOK_Click
        ElseIf Index = 30 Then
            If cboMes.ListIndex = 3 Or cboMes.ListIndex = 5 _
            Or cboMes.ListIndex = 8 Or cboMes.ListIndex = 10 Then
                Call cmdOK_Click
            End If
        Else
            txtDia(Index + 1).SetFocus
        End If
    End If
    KeyAscii = vVB_Generica_001.VALOR(KeyAscii, txtDia(Index).Text)
    If Chr$(KeyAscii) = "," Then
        KeyAscii = Asc(".")
    End If

End Sub

Private Sub txtDia_LostFocus(Index As Integer)

    If Val(txtDia(Index)) >= 100000 Then
        Call vVB_Generica_001.Informar("Valor deve ser menor que 100000!")
        txtDia(Index) = ""
        txtDia(Index).SetFocus
        Exit Sub
    End If
    txtDia(Index).Text = Format(txtDia(Index).Text, "#####0.000000")
    
End Sub
