Attribute VB_Name = "Module1"
Option Explicit
Public vParameters As Object
Public Controls
Public CurControl As Control
Public flModo As Boolean
Public vControl As Variant
Sub Criar_Cursor(Banco As Object, pNomeCursor As String)

    vParameters.Remove pNomeCursor
    vParameters.Add pNomeCursor, 0, 3
    vParameters(pNomeCursor).ServerType = 102
    vParameters(pNomeCursor).DynasetOption = &H2&
    vParameters(pNomeCursor).DynasetCacheParams 256, 16, 20, 2000, 0
    
End Sub

Sub Limpa(frm As Form)

    For Each CurControl In frm.Controls
        If TypeOf CurControl Is TextBox Or TypeOf _
            CurControl Is ComboBox Or _
                TypeOf CurControl Is Label Then
            If CurControl.Enabled = False Then
                CurControl.Enabled = True
            End If
            If CurControl.Visible = False Then
                If CurControl <> frm.lblTpAno Then
                    CurControl.Visible = True
                End If
            End If
            If TypeOf CurControl Is TextBox Then
                CurControl.Text = ""
            ElseIf TypeOf CurControl Is ComboBox Then
                    CurControl.ListIndex = -1
            End If
        End If
    Next
    FrmCadDolar.txtAno = ""
    
End Sub

Function VERIF_CAMPO(frm As Form) As Boolean
'ESTA SUB-ROTINA VERIFICA A EXIST�NCIA DE CAMPOS EM
'BRANCO OU COM VALOR INV�LIDO, E SE HOUVER, ENVIA MSG. DE ALERTA.

    For Each CurControl In frm.Controls

        If TypeOf CurControl Is TextBox Then
            If CurControl <> frm.txtAno Then

                If CurControl.Visible = False Then

                ElseIf CurControl.Text = "" Or _
                Val(CurControl) = 0 Then
                   Call vVB_Generica_001.Informar("Valores n�o podem estar zerados, nem em branco. Caso voc� n�o tenha " & _
                           "todos os valores, repita o �ltimo para os demais dias!!")
                    VERIF_CAMPO = False
                    CurControl.SetFocus
                    Exit Function
                ElseIf Val(CurControl) >= 100000 Then
                    Call vVB_Generica_001.Informar("Valor deve ser menor que 100000!")
                    VERIF_CAMPO = False
                    CurControl.SetFocus
                    Exit Function
                End If

            End If

        End If

    Next
    
    VERIF_CAMPO = True

End Function

Function BISSEXTO(ANO As String) As Boolean

    ANO = Val(Mid(ANO, 3, 2))

    'EXEMPLO: ANO = 97
    For i = 0 To 100 Step 4
        
        If i = 100 Then
            
            If Val(Mid(Str(i), 3, 2)) = ANO Then
                BISSEXTO = True
                Exit Function
            Else
                BISSEXTO = False
                Exit Function
            End If
        
        ElseIf i = ANO Then
            
            BISSEXTO = True
            Exit Function
        
        ElseIf i > ANO Then
            
            BISSEXTO = False
            Exit Function
        
        End If
        
    Next i

End Function




