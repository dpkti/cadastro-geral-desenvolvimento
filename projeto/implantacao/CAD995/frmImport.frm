VERSION 5.00
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "msmask32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "Msflxgrd.ocx"
Begin VB.Form frmImport 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Importa��o de Classifica��o"
   ClientHeight    =   6705
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7185
   Icon            =   "frmImport.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   447
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   479
   Begin VB.Frame Frame1 
      Caption         =   "Dados do Arquivo"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   4065
      Left            =   45
      TabIndex        =   6
      Top             =   2250
      Width           =   7080
      Begin Bot�o.cmd cmdImp 
         Height          =   420
         Left            =   5850
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   3555
         Width           =   1140
         _ExtentX        =   2011
         _ExtentY        =   741
         BTYPE           =   3
         TX              =   "Importar"
         ENAB            =   0   'False
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmImport.frx":23D2
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MSFlexGridLib.MSFlexGrid mfgArq 
         Height          =   3120
         Left            =   135
         TabIndex        =   8
         Top             =   360
         Width           =   6810
         _ExtentX        =   12012
         _ExtentY        =   5503
         _Version        =   393216
         Cols            =   6
         BackColorBkg    =   -2147483633
         AllowUserResizing=   1
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lbMsg 
         BeginProperty Font 
            Name            =   "Arial Black"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000FFFF&
         Height          =   420
         Left            =   180
         TabIndex        =   9
         Top             =   3555
         Width           =   5325
      End
   End
   Begin VB.Frame fra 
      Caption         =   "Arquivo para Leitura"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1230
      Left            =   45
      TabIndex        =   3
      Top             =   945
      Width           =   7080
      Begin Bot�o.cmd cmd1 
         Height          =   690
         Left            =   135
         TabIndex        =   4
         TabStop         =   0   'False
         ToolTipText     =   "Abrir"
         Top             =   360
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmImport.frx":23EE
         PICN            =   "frmImport.frx":240A
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MSMask.MaskEdBox txtArquivo 
         Height          =   330
         Left            =   1035
         TabIndex        =   5
         Top             =   585
         Width           =   5730
         _ExtentX        =   10107
         _ExtentY        =   582
         _Version        =   393216
         Appearance      =   0
         MaxLength       =   14
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   "###,###,##0.00"
         PromptChar      =   "�"
      End
   End
   Begin MSComDlg.CommonDialog CMMDIALOG 
      Left            =   6660
      Top             =   180
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   6375
      Width           =   7185
      _ExtentX        =   12674
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   12621
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      Top             =   810
      Width           =   7080
      _ExtentX        =   12488
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmImport.frx":30E4
      PICN            =   "frmImport.frx":3100
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmImport"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public FLERRO As Integer

Private Sub cmd1_Click()
On Error GoTo Trata_erro

    Dim xl As New Excel.Application
    'Dim xlB As Excel.Workbook
    Dim Fim, ErroC As Integer
    Dim Celula As String


    CMMDIALOG.ShowOpen
    
    strcaminho = CMMDIALOG.FileName
    
    If InStr(1, strcaminho, ".xls") = 0 Then
        MsgBox "O arquivo de importa��o deve ser em Excel", , "Aten��o!"
        Exit Sub
    End If
    
    txtArquivo.Text = strcaminho
    
    Screen.MousePointer = 11
    
    'Verifica se foi indicado o diret�rio e nome do arquivo a ser lido
    If strcaminho = "" Then
        MsgBox "Indique o arquivo a ser aberto", , MSG_TIT
        Screen.MousePointer = 0
        Exit Sub
    End If

    xl.Workbooks.Add strcaminho
    
    'xlB.Activate
    'xl.Visible = True
    'xl.Visible = True
    lbMsg.Caption = ""
    
    If xl.Cells(5, 1).Value = "" Then
        MsgBox "O Arquivo n�o esta no formato correto", vbInformation, "Aten��o"
        Screen.MousePointer = 0
        Exit Sub
    ElseIf xl.Cells(2, 1).Value = "" Then
        
        Fim = 1
    
    Else
        
        xl.Cells(5, 1).Select
        xl.ActiveCell.End(xlDown).Select

        Fim = xl.ActiveCell.Row
        
    End If
    
    xl.Cells(5, 1).Select
    
    FLERRO = 0
    
    Dim vCNPJ As Double
    Dim msgErro As String
    Dim nmCliente As String
    Dim CodCliente As String
    Dim TPACAO As String
    
    For i = 5 To Fim
         
        vCNPJ = xl.Cells(i, 2).Value
        
        vBanco.Parameters.Remove "PM_CNPJ"
        vBanco.Parameters.Add "PM_CNPJ", vCNPJ, 1
        
        vBanco.Parameters.Remove "PM_CURSOR1"
        vBanco.Parameters.Add "PM_CURSOR1", 0, 3
        vBanco.Parameters("PM_CURSOR1").ServerType = 102
        vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
        vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
        
        vBanco.Parameters.Remove "PM_CODERRO"
        vBanco.Parameters.Add "PM_CODERRO", 0, 2
        vBanco.Parameters.Remove "PM_TXTERRO"
        vBanco.Parameters.Add "PM_TXTERRO", "", 2
        
        vSql = "PRODUCAO.PCK_CAD995.pr_CON_CLIENTE(:PM_CNPJ,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
        
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        
        Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
        
        If vObjOracle.EOF Then
            msgErro = "N�O ENCONTRADO"
            nmCliente = ""
            CodCliente = ""
            TPACAO = ""
        Else
            nmCliente = vObjOracle("NOME_CLIENTE")
            CodCliente = vObjOracle("COD_CLIENTE")
            
            If vObjOracle("Situacao") <> "0" Then
                msgErro = "DESATIVADO"
            Else
                TPACAO = "I"
                msgErro = ""
            End If
            
        End If
        
        mfgArq.Row = i - 4
        mfgArq.Col = 1
        mfgArq.Text = vCNPJ
        
        mfgArq.Col = 2
        mfgArq.Text = CodCliente
        
        mfgArq.Col = 3
        mfgArq.Text = nmCliente
        
        mfgArq.Col = 4
        mfgArq.Text = TPACAO
        
        mfgArq.Col = 5
        mfgArq.Text = msgErro
            
        If msgErro <> "" Then
            FLERRO = 1
            lbMsg.Caption = "* Registros com problemas"
            
            With mfgArq
                For X = 1 To 5
                    .Row = i - 4
                    .Col = X
                    .CellBackColor = vbYellow
                Next
            End With
        End If
            
        mfgArq.Rows = i - 2

    Next
    
    xl.Workbooks.Close
    
    Set xl = Nothing
    Screen.MousePointer = vbNormal
    
    cmdImp.Enabled = True
    
Trata_erro:
    If Err.Number = -2147417851 Then
        Resume Next
    ElseIf Err.Number <> 0 Then
        MsgBox Err.Number & vbCrLf & Err.Description & " - " & i
        Screen.MousePointer = vbNormal
    End If
End Sub

Private Sub cmdImp_Click()

    If FLERRO = 1 Then
        If MsgBox("Os registro marcados em AMARELO, n�o ser�o inseridos. Deseja continuar?", vbYesNo) = vbNo Then
             Exit Sub
        End If
    End If
    
    Call SUBGRAVACLASS

End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    
    Me.Top = 2000
    Me.Left = 2000
    
    mfgArq.ColWidth(0) = 200
    
    With mfgArq
        .Row = 0
        
        .Col = 1
        .Text = "CNPJ"
        .ColWidth(.Col) = 1000
        
        .Col = 2
        .Text = "COD"
        .ColWidth(.Col) = 800
        
        .Col = 3
        .Text = "NOME"
        .ColWidth(.Col) = 2200
        
        .Col = 4
        .Text = "A�AO"
        .ColWidth(.Col) = 400
        
        .Col = 5
        .Text = "ERRO"
        .ColWidth(.Col) = 2000
        
    End With
    
End Sub


Sub SUBGRAVACLASS()

Dim ArrDados(14) As String
Dim vCor As String
Dim CODCLI As String
Dim TPACAO As String

    Screen.MousePointer = 0
    
    vSql = "PRODUCAO.PCK_CAD995.pr_DEL_CLIENTE_INDENT(:PM_CODERRO,:PM_TXTERRO)"
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    vErro = IIf(vBanco.Parameters("PM_CODERRO") = "", 0, vBanco.Parameters("PM_CODERRO"))
    If vErro <> 0 Then
        MsgBox vBanco.Parameters("PM_TXTERRO") & ".Ligue para o Depto de Sistemas"
        Screen.MousePointer = 0
        Exit Sub
    End If
  
    vSql = "PRODUCAO.PCK_CAD995.pr_ACT_CLIENTE_INDENT(:PM_CODCLI,:PM_ACT,:PM_CODERRO,:PM_TXTERRO)"
     

    For i = 1 To mfgArq.Rows - 1
    
        With mfgArq
            .Row = i
            .Col = 1
            vCor = .CellBackColor
            
            .Col = 2
            CODCLI = .Text
            
            .Col = 4
            TPACAO = .Text
            
        End With
        
        If vCor <> 65535 Then
                            
                vBanco.Parameters.Remove "PM_CODCLI"
                vBanco.Parameters.Add "PM_CODCLI", CODCLI, 1
                vBanco.Parameters.Remove "PM_ACT"
                vBanco.Parameters.Add "PM_ACT", TPACAO, 1
     
                vBanco.Parameters.Remove "PM_CODERRO"
                vBanco.Parameters.Add "PM_CODERRO", 0, 2
                vBanco.Parameters.Remove "PM_TXTERRO"
                vBanco.Parameters.Add "PM_TXTERRO", "", 2
          
                vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
     
                vErro = IIf(vBanco.Parameters("PM_CODERRO") = "", 0, vBanco.Parameters("PM_CODERRO"))
     
                If vErro <> 0 Then
                    MsgBox vBanco.Parameters("PM_TXTERRO") & ".Ligue para o Depto de Sistemas"
                    Screen.MousePointer = 0
                    Exit Sub
                End If
                
        End If
    Next
    
    MsgBox "Inclus�o/Altera��o OK ", vbInformation, "Aten��o"
    Screen.MousePointer = 0
    cmdImp.Enabled = False
    
    Exit Sub

End Sub
