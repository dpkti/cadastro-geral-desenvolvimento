VERSION 5.00
Begin VB.Form frmMascara 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cadastro de M�scara:"
   ClientHeight    =   4425
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3810
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4425
   ScaleWidth      =   3810
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraCentro 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   4305
      Left            =   45
      TabIndex        =   5
      Top             =   60
      Width           =   3735
      Begin VB.ComboBox cmbCodSegmento 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   308
         Style           =   2  'Dropdown List
         TabIndex        =   11
         Top             =   2460
         Width           =   3195
      End
      Begin VB.ComboBox cmbCNPJ 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   308
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   1830
         Width           =   3195
      End
      Begin VB.CommandButton cmdOK 
         Caption         =   "OK"
         Height          =   375
         Left            =   1276
         TabIndex        =   8
         ToolTipText     =   "Confirma m�scara"
         Top             =   3810
         Width           =   1245
      End
      Begin VB.Label lblCodIdentificador 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "9"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   308
         TabIndex        =   10
         Top             =   1230
         Width           =   135
      End
      Begin VB.Label lblMascaraTitle 
         AutoSize        =   -1  'True
         Caption         =   "M�scara:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   165
         Left            =   1598
         TabIndex        =   7
         Top             =   3090
         Width           =   795
      End
      Begin VB.Label lblMascara 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   675
         TabIndex        =   6
         Top             =   3300
         Width           =   2460
      End
      Begin VB.Label lblCodCentroCusto 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "80800"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   308
         TabIndex        =   4
         Top             =   660
         Width           =   555
      End
      Begin VB.Label lblCentroCusto 
         AutoSize        =   -1  'True
         Caption         =   "C�digo do Centro de Custo:"
         Height          =   195
         Left            =   308
         TabIndex        =   3
         Top             =   450
         Width           =   1950
      End
      Begin VB.Label lblCodigoIdentificador 
         AutoSize        =   -1  'True
         Caption         =   "C�digo Identificador:"
         Height          =   195
         Left            =   308
         TabIndex        =   2
         Top             =   1020
         Width           =   1455
      End
      Begin VB.Label lblCNPJ 
         AutoSize        =   -1  'True
         Caption         =   "CNPJ:"
         Height          =   195
         Left            =   308
         TabIndex        =   1
         Top             =   1650
         Width           =   450
      End
      Begin VB.Label lblCodSegmento 
         AutoSize        =   -1  'True
         Caption         =   "C�digo do Segmento:"
         Height          =   195
         Left            =   308
         TabIndex        =   0
         Top             =   2250
         Width           =   1530
      End
   End
End
Attribute VB_Name = "frmMascara"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim vString As String

Private Sub cmbCNPJ_Click()

    If cmbCNPJ.Text = "" Then
        vString = "    "
    Else
        vString = Format(cmbCNPJ.ItemData(cmbCNPJ.ListIndex), "0000")
    End If
    
    If Len(vString) < 4 Then
        vString = vString & Space(4 - Len(vString))
    End If

    lblMascara.Caption = Mid(lblMascara.Caption, 1, 2) & vString & Mid(lblMascara.Caption, 7)


End Sub

Private Sub cmbCodSegmento_Click()

    If cmbCodSegmento.Text = "" Then
        vString = "   "
    Else
        vString = Format(cmbCodSegmento.ItemData(cmbCodSegmento.ListIndex), "000")
    End If
    
    If Len(vString) < 3 Then
        vString = vString & Space(3 - Len(vString))
    End If
    
    lblMascara.Caption = Mid(lblMascara.Caption, 1, 7) & vString & Mid(lblMascara.Caption, 11)

End Sub

Private Sub cmdOK_Click()

    
    If cmbCNPJ.Text = "" Then
        Call MsgBox("Selecione o CNPJ!", vbExclamation, "Aten��o!")
        cmbCNPJ.SetFocus
        Exit Sub
    End If
    
    If cmbCodSegmento.Text = "" Then
        Call MsgBox("Selecione o C�digo do Segmento!", vbExclamation, "Aten��o!")
        cmbCodSegmento.SetFocus
        Exit Sub
    End If
    
    If InStr(1, lblMascara.Caption, " ") Then
        Call MsgBox("A m�scara do centro de custo n�o pode conter espa�os em branco!" & vbCrLf & "Por favor reveja o cadastro!", vbCritical, "Aten��o!")
        Exit Sub
    End If
    
    vMascara.cod_centro_custo = lblCodCentroCusto.Caption
    vMascara.cod_identificador = lblCodIdentificador.Caption
    vMascara.cnpj = cmbCNPJ.ItemData(cmbCNPJ.ListIndex)
    vMascara.cod_segmento = cmbCodSegmento.ItemData(cmbCodSegmento.ListIndex)
    vMascara.mascara = lblMascara.Caption
    
    frmCentro.lblMascara.Caption = lblMascara.Caption
    Unload Me
        
End Sub

Private Sub Form_Load()

    Dim i As Byte

    lblCodCentroCusto.Caption = frmCentro.txtCodigo.Text
    
    Call cmbCNPJ_onLoad
    Call cmbCodSegmento_onLoad
    
    If frmCentro.lblMascara.Caption = "" Then
    
        lblMascara.Caption = "9.    .   ." & frmCentro.txtCodigo
        
    Else
    
        For i = 0 To cmbCNPJ.ListCount - 1
        
            If Format(cmbCNPJ.ItemData(i), "0000") = Mid(frmCentro.lblMascara.Caption, 3, 4) Then
                cmbCNPJ.ListIndex = i
                Exit For
            End If
            
        Next
        
        For i = 0 To cmbCodSegmento.ListCount - 1
        
            If Format(cmbCodSegmento.ItemData(i), "000") = Mid(frmCentro.lblMascara.Caption, 8, 3) Then
                cmbCodSegmento.ListIndex = i
                Exit For
            End If
        
        Next
        
        lblMascara = frmCentro.lblMascara.Caption
        
        'cmbCNPJ.Text = Mid(frmCentro.lblMascara.Caption, 3, 4)
        'txtCodSegmento = Mid(frmCentro.lblMascara.Caption, 8, 3)
    
    End If

End Sub

Private Sub cmbCNPJ_LostFocus()

'    If cmbCNPJ.Text = "" Then
'        vString = "    "
'    Else
'        vString = Format(cmbCNPJ.ItemData(cmbCNPJ.ListIndex), "0000")
'    End If
    
'    If Len(vString) < 4 Then
'        vString = vString & Space(4 - Len(vString))
'    End If

'    lblMascara.Caption = Mid(lblMascara.Caption, 1, 2) & vString & Mid(lblMascara.Caption, 7)

End Sub

Private Sub cmbCodSegmento_LostFocus()

'    If cmbCodSegmento.Text = "" Then
'        vString = "   "
'    Else
'        vString = Format(cmbCodSegmento.ItemData(cmbCodSegmento.ListIndex), "000")
'    End If
    
'    If Len(vString) < 3 Then
'        vString = vString & Space(3 - Len(vString))
'    End If
    
'    lblMascara.Caption = Mid(lblMascara.Caption, 1, 7) & vString & Mid(lblMascara.Caption, 11)

End Sub

Public Sub cmbCNPJ_onLoad()

    Dim vRecordSet As New ADODB.Recordset

    Call fCNPJ(1, 0, vRecordSet)
    
    While Not vRecordSet.EOF
    
        cmbCNPJ.AddItem (vRecordSet!cod_loja & "-" & vRecordSet!nome_fantasia & "-" & vRecordSet!cnpj)
        cmbCNPJ.ItemData(cmbCNPJ.NewIndex) = vRecordSet!cnpj
        vRecordSet.MoveNext
    
    Wend

End Sub

Public Sub cmbCodSegmento_onLoad()

    cmbCodSegmento.AddItem ("001 - Administrativo")
    cmbCodSegmento.ItemData(cmbCodSegmento.NewIndex) = "001"
    cmbCodSegmento.AddItem ("002 - Comercial/Regionais")
    cmbCodSegmento.ItemData(cmbCodSegmento.NewIndex) = "002"
    cmbCodSegmento.AddItem ("003 - Centros de Distribui��o")
    cmbCodSegmento.ItemData(cmbCodSegmento.NewIndex) = "003"

End Sub

