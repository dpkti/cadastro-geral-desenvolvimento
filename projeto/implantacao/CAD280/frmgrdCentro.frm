VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MsFlxGrd.ocx"
Begin VB.Form frmgrdCentro 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Consulta de Dados: ""Centros de Custo"""
   ClientHeight    =   5295
   ClientLeft      =   2820
   ClientTop       =   1830
   ClientWidth     =   7695
   Icon            =   "frmgrdCentro.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5295
   ScaleWidth      =   7695
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdVoltar 
      Caption         =   "Voltar"
      Height          =   525
      Left            =   3975
      TabIndex        =   2
      ToolTipText     =   "Volta para tela anterior"
      Top             =   4680
      Width           =   1245
   End
   Begin VB.CommandButton cmdImprimir 
      Caption         =   "Imprimir"
      Height          =   525
      Left            =   2565
      TabIndex        =   1
      ToolTipText     =   "Imprime conte�do da tabela acima"
      Top             =   4680
      Width           =   1245
   End
   Begin MSFlexGridLib.MSFlexGrid grdCentro 
      Height          =   4185
      Left            =   30
      TabIndex        =   0
      Top             =   60
      Width           =   7635
      _ExtentX        =   13467
      _ExtentY        =   7382
      _Version        =   393216
      Cols            =   4
      FixedCols       =   0
      BackColor       =   16777215
      HighLight       =   2
      SelectionMode   =   1
      FormatString    =   "<C�digo   |<C�digo Antigo   |<Descri��o            |<M�scara               "
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblRegistros 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Registros: 1000"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   225
      Left            =   6060
      TabIndex        =   3
      Top             =   4260
      Width           =   1635
   End
End
Attribute VB_Name = "frmgrdCentro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdImprimir_Click()

    Dim vTexto As String
    Dim i As Integer
    
    If grdCentro.Rows = 0 Then
    End If

    Printer.KillDoc
    Printer.FontName = "Courier New"
    Printer.FontSize = 8
    
    Printer.FontBold = False
    vTexto = "Rela��o de Centros de Custos " & Now
    Printer.Print vTexto
    
    vTexto = "Par�metros da Pesquisa: '" & vParametro & "'"
    Printer.Print vTexto
    
    Printer.Print ""
    
    vTexto = "COD_CENTRO_CUSTO     CODIGO_ANTIGO     DESCRICAO                MASCARA              "
    Printer.Print vTexto
    
    With grdCentro
    
        For i = 1 To .Rows - 1
        
            .Row = i
            
            .Col = 0
            vTexto = .Text & Space(21 - Len(.Text))
            
            .Col = 1
            vTexto = vTexto & .Text & Space(18 - Len(.Text))
            
            .Col = 2
            vTexto = vTexto & .Text & Space(25 - Len(.Text))
            
            .Col = 3
            vTexto = vTexto & .Text & Space(18 - Len(.Text))
            
            Printer.Print vTexto
        
        Next
        
    End With
    
    Printer.Print ""
    
    vTexto = "Total de �tens: " & grdCentro.Rows - 1
    
    Printer.EndDoc
    
    Call MsgBox("Impress�o conclu�da!", vbInformation + vbOKOnly, "Aten��o!")
    
End Sub

Private Sub cmdPesquisar_Click()

    Call Carrega_Grid

End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub

Private Sub grdCentro_Click()

    If grdCentro.MouseRow = 0 Then
    
        Call MfgSort(Me.grdCentro)
        
    End If

End Sub

Private Sub grdCentro_DblClick()

'    Alexsandro Ci&T - 19/10/12
'    [SAF-511] - Remover Tratamento para Faixa de Centros de Custos
'    grdCentro.Col = 4
'    If grdCentro.Text = "DPA" Then
'        Call MsgBox("O centro de custo selecionado n�o pertence � DPK e n�o poder� ser alterado!" & vbCrLf & vbCrLf & "-Faixa de Centros de Custos DPK: 80.000 a 99.999", vbCritical + vbOKOnly, "Aten��o!")
'        Exit Sub
'    End If

    grdCentro.Col = 0
    frmCentro.txtCodigo.Text = grdCentro.Text
    grdCentro.Col = 1
    frmCentro.lblCodAntigo.Caption = grdCentro.Text
    grdCentro.Col = 2
    frmCentro.txtDescrCentro.Text = grdCentro.Text
    grdCentro.Col = 3
    frmCentro.lblMascara.Caption = grdCentro.Text
    frmCentro.fraCentro.Caption = "ALTERA��O / CONSULTA"
    Unload Me
    
End Sub

Private Sub mnuQtde_Elementos_Click()
    MsgBox "Quantidade de elementos: " & elem, vbOKOnly, MSG_TIT
End Sub

Private Sub Voltar_Click()  'menu
    Unload Me
End Sub
