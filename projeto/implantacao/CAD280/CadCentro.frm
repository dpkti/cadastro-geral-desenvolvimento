VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmCentro 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cadastro de Centro de Custo:"
   ClientHeight    =   4485
   ClientLeft      =   4050
   ClientTop       =   2430
   ClientWidth     =   4860
   Icon            =   "CadCentro.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4485
   ScaleWidth      =   4860
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdAlterarMascara 
      Caption         =   "..."
      Height          =   285
      Left            =   2880
      TabIndex        =   7
      ToolTipText     =   "Alterar M�scara ..."
      Top             =   3780
      Width           =   255
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   4230
      Top             =   30
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CadCentro.frx":0CCA
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CadCentro.frx":19A4
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CadCentro.frx":267E
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CadCentro.frx":3358
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "CadCentro.frx":4032
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   660
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   4860
      _ExtentX        =   8573
      _ExtentY        =   1164
      ButtonWidth     =   1032
      ButtonHeight    =   1005
      Appearance      =   1
      ImageList       =   "ImageList1"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   8
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "TBConfirma"
            Object.ToolTipText     =   "Confirma Dados:"
            ImageIndex      =   2
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   4
            Object.Width           =   100
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "TBCancela"
            Object.ToolTipText     =   "Cancela:"
            ImageIndex      =   5
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "TBPesquisa"
            Object.ToolTipText     =   "Consulta Centro de Custo:"
            ImageIndex      =   1
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "TBVoltar"
            Object.ToolTipText     =   "Voltar:"
            ImageIndex      =   4
         EndProperty
      EndProperty
      BorderStyle     =   1
   End
   Begin VB.Frame fraCentro 
      BeginProperty Font 
         Name            =   "Microsoft Sans Serif"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   3525
      Left            =   143
      TabIndex        =   0
      Top             =   810
      Width           =   4575
      Begin VB.TextBox txtDescrCentro 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   180
         MaxLength       =   20
         TabIndex        =   4
         Top             =   2190
         Width           =   2475
      End
      Begin VB.TextBox txtCodigo 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   180
         MaxLength       =   8
         TabIndex        =   3
         ToolTipText     =   "Clique Duas Vezes para Pesquisar:"
         Top             =   750
         Width           =   1095
      End
      Begin VB.Label lblCodAntigo 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   180
         TabIndex        =   10
         Top             =   1470
         Width           =   1095
      End
      Begin VB.Label lblAntigo 
         AutoSize        =   -1  'True
         Caption         =   "C�digo Antigo:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   210
         TabIndex        =   9
         Top             =   1140
         Width           =   1320
      End
      Begin VB.Label lblMascara 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   180
         TabIndex        =   8
         Top             =   2970
         Width           =   2475
      End
      Begin VB.Label lblMascaraTitle 
         AutoSize        =   -1  'True
         Caption         =   "M�scara:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   6
         Top             =   2670
         Width           =   840
      End
      Begin VB.Label lblDescCentro 
         AutoSize        =   -1  'True
         Caption         =   "Descri��o do Centro de Custo:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   180
         TabIndex        =   2
         Top             =   1860
         Width           =   2730
      End
      Begin VB.Label lblCodigo 
         AutoSize        =   -1  'True
         Caption         =   "C�digo do Centro de Custo:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   210
         TabIndex        =   1
         Top             =   420
         Width           =   2460
      End
   End
End
Attribute VB_Name = "frmCentro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Sub limpa_campos()

    txtCodigo.Text = ""
    lblCodAntigo.Caption = ""
    txtDescrCentro.Text = ""
    lblMascara.Caption = ""
    fraCentro.Caption = ""
    vMascara.cnpj = ""
    vMascara.cod_centro_custo = 0
    vMascara.cod_identificador = ""
    vMascara.cod_segmento = ""
    vMascara.mascara = ""

End Sub

Private Sub mnuCancela_Click()
    
    limpa_campos
    fraCentro.Caption = ""
    txtCodigo.SetFocus
    
End Sub

Private Sub cmdAlterarMascara_Click()

    If txtCodigo.Text = "" Then
        Call MsgBox("Digite o C�digo do Centro de Custo!", vbExclamation, "Aten��o!")
        txtCodigo.SetFocus
        Exit Sub
    End If
    
    frmMascara.Show 1

End Sub

Private Sub txtCodigo_GotFocus()

txtCodigo.SelStart = 0
' CI&T - 19/10/2012 SVALIM
' [SAF-510] [DEV] CAD280 - Altera��o em CadCentro.frm
'txtCodigo.SelLength = 6
txtCodigo.SelLength = 8

End Sub

Private Sub txtDescrCentro_GotFocus()

txtDescrCentro.SelStart = 0
txtDescrCentro.SelLength = 21

End Sub

Private Sub txtDescrCentro_KeyPress(KeyAscii As Integer)
    
    KeyAscii = Maiusculo(KeyAscii)
    
End Sub

Private Sub txtCodigo_KeyPress(KeyAscii As Integer)
    
    KeyAscii = Numerico(KeyAscii)
    
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)


    Select Case Button.Key

        Case "TBConfirma" 'bot�o de confirma��o de dados
        
            If fraCentro.Caption = "" Then 'Indica se os dados j� foram carregados
                Call txtCodigo_LostFocus
                Exit Sub
            End If

            If txtCodigo.Text = "" Then
                MSGB = MsgBox("O campo 'Centro de Custo' N�o Pode Estar em Branco", vbCritical + vbOKOnly, MSG_TIT)
                txtCodigo.SetFocus
                Exit Sub
            ' CI&T - 19/10/2012 SVALIM
            ' [SAF-510] [DEV] CAD280 - Altera��o em CadCentro.frm
            'ElseIf Len(txtCodigo) < 5 Then
            ElseIf Len(txtCodigo) < 8 Then
                Call MsgBox("O c�digo do centro de custo n�o pode ser formado por menos de oito digitos!", vbCritical + vbOKOnly, "Aten��o!")
                txtCodigo.SetFocus
                Exit Sub
            'ElseIf Val(txtCodigo.Text) < 80000 Or Val(txtCodigo.Text) > 99999 Then
            '    Call MsgBox("C�digo do centro de Custo fora da faixa permitida para a DPK!" & vbCrLf & vbCrLf & "-Faixa Permitida: 80.000 a 99.999", vbCritical + vbOKOnly, "Aten��o!")
            '    txtCodigo.SetFocus
            '    Exit Sub
            ElseIf Trim(txtDescrCentro.Text) = "" Then
                MSGB = MsgBox("O campo 'Descri��o do Centro de Custo' N�o Pode Estar em Branco", vbCritical + vbOKOnly, MSG_TIT)
                txtDescrCentro.SetFocus
                Exit Sub
            ElseIf Len(Trim(txtDescrCentro.Text)) < 3 Then
                Call MsgBox("O campo 'Descri��o do Centro de Custo' n�o pode ser formado por menos de 3 caracteres!", vbCritical + vbOKOnly, "Aten��o")
                txtDescrCentro.SetFocus
                Exit Sub
            ElseIf lblMascara.Caption = "" Then
                Call MsgBox("A m�scara n�o pode ficar em branco!", vbCritical + vbOKOnly, "Aten��o!")
                frmMascara.Show 1
                Exit Sub
            ElseIf InStr(1, lblMascara.Caption, " ") > 0 Then
                Call MsgBox("N�o pode haver espa�os em branco na m�scara!", vbCritical + vbOKOnly, "Aten��o!")
                frmMascara.Show 1
                Exit Sub
            End If

            If FL_INAL = 0 Then   'inclusao dos dados no oracle
 
                r = MsgBox("Confirma Inclus�o?", vbYesNo + vbQuestion, MSG_TIT)

                ' SE N�O CONFIRMA INCLUS�O, LIMPA CAMPOS E SAI
                If r = vbNo Then
                    MSGB = ""
                    limpa_campos
                    fraCentro.Caption = ""
                    txtCodigo.SetFocus
                    Exit Sub
                Else
                    Call sCC("I", CDbl(txtCodigo.Text), txtDescrCentro.Text)
                    Call sMascara("I", CDbl(txtCodigo.Text), vMascara.cod_identificador, Format(vMascara.cnpj, "0000"), Format(vMascara.cod_segmento, "000"), vMascara.mascara)
                End If
                
                MsgBox "Inclus�o OK!", vbOKOnly + vbInformation, MSG_TIT
                limpa_campos
                fraCentro.Caption = ""
                
            Else ' alteracao no oracle

                r = MsgBox("Confirma Altera��o?", vbYesNo + vbQuestion, MSG_TIT)

                If r = vbNo Then    ' SE N�O CONFIRMA ALTERA��O, LIMPA CAMPOS E SAI
                    MSGB = ""
                    limpa_campos
                    fraCentro.Caption = ""
                    txtCodigo.SetFocus
                    Exit Sub
                Else
                    Screen.MousePointer = 11
                    Call sCC("U", CDbl(txtCodigo.Text), txtDescrCentro.Text)
                    Call sMascara("U", CDbl(txtCodigo.Text), vMascara.cod_identificador, Format(vMascara.cnpj, "0000"), Format(vMascara.cod_segmento, "000"), vMascara.mascara)
                    Screen.MousePointer = 0
                End If

                MsgBox "Altera��o OK!", vbOKOnly + vbInformation, MSG_TIT
                limpa_campos
                txtCodigo.SetFocus

            End If


        Case "TBCancela"
            limpa_campos
            fraCentro.Caption = ""
            txtCodigo.SetFocus

        Case "TBPesquisa"

            'On Error GoTo TRATA_ERRO:
            
            limpa_campos

            Call Carrega_Grid

            Exit Sub

TRATA_ERRO:
            Call Process_Line_Errors

        Case "TBVoltar"

            Unload Me

    End Select



End Sub

Private Sub txtCodigo_LostFocus()

    Dim vRecordSet As New ADODB.Recordset

    If txtCodigo.Text = "" Then
        Exit Sub
    End If
    ' CI&T - 19/10/2012 SVALIM
    ' [SAF-510] [DEV] CAD280 - Altera��o em CadCentro.frm
    'If Val(txtCodigo.Text) < 80000 Or Val(txtCodigo.Text) > 99999 Then
    '    Call MsgBox("C�digo do centro de Custo fora da faixa permitida para a DPK!" & vbCrLf & vbCrLf & "Faixa Permitida: 80.000 a 99.999", vbCritical + vbOKOnly, "Aten��o!")
    '    txtCodigo.SetFocus
    '    Exit Sub
    'End If
    
    Call fSelect(txtCodigo.Text, vRecordSet)
    
    If Not vRecordSet.EOF Then
          
        FL_INAL = 1
        fraCentro.Caption = "ALTERA��O / CONSULTA"
        
        If IsNull(vRecordSet!COD_CENTRO_CUSTO_ANTIGO) = True Then
            lblCodAntigo.Caption = "-"
        Else
            lblCodAntigo.Caption = Trim(CStr(vRecordSet!COD_CENTRO_CUSTO_ANTIGO))
        End If

        txtDescrCentro = vRecordSet!DESC_CENTRO_CUSTO
        lblMascara.Caption = vRecordSet!mascara
        
        vMascara.cod_centro_custo = vRecordSet("cod_centro_custo")
        vMascara.cod_identificador = vRecordSet("cod_identificador")
        vMascara.cnpj = vRecordSet("cnpj")
        vMascara.cod_segmento = vRecordSet("cod_segmento")
        vMascara.mascara = vRecordSet("mascara")
        
    Else
    
        If txtCodigo <> "" Then
            FL_INAL = 0
            fraCentro.Caption = "INCLUS�O"
            txtDescrCentro.Text = ""
            lblMascara.Caption = ""
            lblCodAntigo.Caption = "-"
        End If
        ' CI&T - 19/10/2012 SVALIM
        ' [SAF-510] [DEV] CAD280 - Altera��o em CadCentro.frm
        'If Len(txtCodigo.Text) < 5 Then
        If Len(txtCodigo.Text) < 8 Then
            Call MsgBox("O c�digo do centro de custo n�o pode ser formado por menos de oito digitos!", vbCritical + vbOKOnly, "Aten��o!")
        End If
        
    End If
    
    Exit Sub
    
TRATA_ERRO:
        Call Process_Line_Errors
    
    End Sub
    
Private Sub txtCodigo_DblClick()
    
    On Error GoTo TRATA_ERRO:
    
    Call Carrega_Grid
    
    Exit Sub
        
TRATA_ERRO:
        Call Process_Line_Errors
    End Sub
    
        
