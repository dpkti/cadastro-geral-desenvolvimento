Attribute VB_Name = "MOD_PAD"
Option Explicit

'VARIAVEIS DE BANCO DE DADOS
Public vConnection As New ADODB.Connection

Global cod_errora As Integer    'Cod. Erro PL-SQL
Global txt_errora As String     'Texto Erro PL-SQL
Global CmdSql As String         'String para o SQL
Global CmdPl As String          'String para o PL-SQL

'VARIAVEIS AUXILIARES
Public DataFatur As String      'Data do Faturamento
Public DataReal As String       'Data do Faturamento
Public MSG_TIT As String        'T�tulo MsgBox
Public VL_COMBO As String       'Valor do Combo Box
Public FL_INAL As Integer       'Flag de Inclus�o=0/Altera��o=1
Public pbDEPOSITO As Byte       'C�digo do Dep�sito
Public STR_RET As String        'String de Retorno

'PADRAO PARA MENSAGEM
Const ctnormal = 1
Const cterro = 2

'VARIAVEIS PARA A FUNCAO DO EXTENSO
Dim Unidade(20) As String
Dim Dezena(8)  As String
Dim Centena(9) As String

Public elem As Integer

Type tpMascara
    cod_centro_custo As Double
    cod_identificador As String
    cnpj As String
    cod_segmento As String
    mascara As String
End Type

Dim i As Double

Public vMascara As tpMascara

Public vParametro As String
    
Sub MMAA(ByRef KeyAscii, ByRef txtCampo)
    On Error GoTo TrataErro

    Dim bTam As Byte    'tamanho do campo JA digitado
    Dim strData As String
    Dim bKey As Byte
    bTam = Len(txtCampo.Text)
    
    If KeyAscii = 8 And bTam > 0 Then 'backspace
        If Mid$(txtCampo.Text, bTam) = "/" Then
            txtCampo.Text = Mid$(txtCampo.Text, 1, bTam - 2)
        Else
            txtCampo.Text = Mid$(txtCampo.Text, 1, bTam - 1)
        End If
            
    ElseIf Chr$(KeyAscii) >= "0" And Chr$(KeyAscii) <= "9" Then
        If bTam = 1 Then
            strData = txtCampo.Text & Chr$(KeyAscii)
            If CInt(strData) < 1 Or CInt(strData > 12) Then
                Beep
            Else
                txtCampo.Text = txtCampo.Text & Chr$(KeyAscii) & "/"
            End If
        
        ElseIf bTam = 5 Then
            strData = Mid$(txtCampo.Text, 1, 2)     'mes
            If CInt(strData) < 1 Or CInt(strData > 12) Then
               Beep
            Else
               strData = txtCampo.Text & Chr$(KeyAscii)
               If Not IsDate(CDate(strData)) Then
                  Beep
               Else
                  txtCampo.Text = strData
               End If
            End If
            
        ElseIf bTam < 5 Then
            bKey = KeyAscii
            
        Else
            bKey = 0
        End If
    Else
        Beep
    End If
    
    SendKeys "{END}"
    KeyAscii = bKey
    Exit Sub
    
TrataErro:

    If Err.Number = 13 Then
        MsgBox strData, vbInformation, "Data Inv�lida"
        KeyAscii = 0
        Beep
        Err.Clear
    End If

End Sub

Function Def_cent(strAux As String, strUnitario As String, strVarios As String) As String
    
    Dim iVL As Integer
    Dim strRetorno As String
    Dim iOriginal As Integer

    If CInt(strAux) = 0 Then
        Def_cent = ""
        Exit Function
    End If

    iOriginal = CInt(strAux)

    If iOriginal > 99 Then
        strAux = Mid$(strAux, 1, 3)
        If strAux = "100" Then
            strRetorno = "CEM"
        Else
            strRetorno = Centena(CInt(Mid$(strAux, 1, 1)) - 1)
        End If
        strAux = Mid$(strAux, 2, 2)
    Else
        If Len(strAux) > 2 Then
            strAux = Mid$(strAux, 2, 2)
        End If
    End If
    
    iVL = CInt(strAux)
    If iVL > 0 And iVL < 20 Then
        If Len(strRetorno) > 0 Then
            strRetorno = strRetorno & " E " & Unidade(iVL)
        Else
            strRetorno = Unidade(iVL)
        End If
    ElseIf iVL > 19 Then
        iVL = CInt(Mid$(strAux, 1, 1))
        If Len(strRetorno) > 0 Then
            strRetorno = strRetorno & " E " & Dezena(iVL - 2)
        Else
            strRetorno = Dezena(iVL - 2)
        End If
        iVL = CInt(Mid$(strAux, 2, 1))
        If iVL > 0 Then
            strRetorno = strRetorno & " E " & Unidade(iVL)
        End If
    End If

    If strUnitario <> "" Then
        If iOriginal = 1 Then
            strRetorno = strRetorno & " " & strUnitario
        Else
            strRetorno = strRetorno & " " & strVarios
        End If
    End If

    Def_cent = strRetorno

End Function

Function Extenso(Valor As Double) As String

    Dim strdecimal As String
    Dim strInteiro As String
    Dim strAuxPrep As String
    Dim inteiro As Currency
    Dim decimall As Integer
    Dim i As Integer
    Dim j As Integer
    Dim strAux As String
    Dim strBilhao As String
    Dim strMILhao As String
    Dim strMIL As String
    Dim strCem As String
    
    'call Carregar dados
    Unidade(0) = "ZERO"
    Unidade(1) = "UM"
    Unidade(2) = "DOIS"
    Unidade(3) = "TR�S"
    Unidade(4) = "QUATRO"
    Unidade(5) = "CINCO"
    Unidade(6) = "SEIS"
    Unidade(7) = "SETE"
    Unidade(8) = "OITO"
    Unidade(9) = "NOVE"
    Unidade(10) = "DEZ"
    Unidade(11) = "ONZE"
    Unidade(12) = "DOZE"
    Unidade(13) = "TREZE"
    Unidade(14) = "QUATORZE"
    Unidade(15) = "QUINZE"
    Unidade(16) = "DEZESSEIS"
    Unidade(17) = "DEZESSETE"
    Unidade(18) = "DEZOITO"
    Unidade(19) = "DEZENOVE"
    
    Dezena(0) = "VINTE"
    Dezena(1) = "TRINTA"
    Dezena(2) = "QUARENTA"
    Dezena(3) = "CINQUENTA"
    Dezena(4) = "SESSENTA"
    Dezena(5) = "SETENTA"
    Dezena(6) = "OITENTA"
    Dezena(7) = "NOVENTA"

    Centena(0) = "CENTO"
    Centena(1) = "DUZENTOS"
    Centena(2) = "TREZENTOS"
    Centena(3) = "QUATROCENTOS"
    Centena(4) = "QUINHENTOS"
    Centena(5) = "SEISCENTOS"
    Centena(6) = "SETECENTOS"
    Centena(7) = "OITOCENTOS"
    Centena(8) = "NOVECENTOS"

    'separar valor
    inteiro = Int(Valor)
    strAux = CStr(Valor)
    i = InStr(1, strAux, ".")
    If i > 0 Then
        strAux = Mid$(strAux, i + 1)
        If Len(strAux) = 1 Then
            strAux = strAux & "0"
        ElseIf Len(strAux) > 2 Then
            strAux = Mid$(strAux, 1, 2)
        End If
        decimall = CInt(strAux)
    Else
        decimall = 0
    End If
    
    'definir valor decimall
    strdecimal = Def_cent(CStr(decimall), "CENTAVO", "CENTAVOS")
    If decimall = 0 Then
        strdecimal = ""
    End If
    
    'definir valor inteiro
    i = Len(CStr(inteiro))
    j = 1
    strBilhao = "N"
    strMILhao = "N"
    strMIL = "N"
    strCem = "N"
    strAuxPrep = ""
    If i > 9 Then  'BILHAO
        strBilhao = "S"
        strAux = Mid$(CStr(inteiro), j, i - 9)
        strInteiro = Def_cent(strAux, "BILH�O", "BILH�ES")
        j = j + i - 9
        i = 9
    End If

    If i > 6 Then  'MILHAO
        strAux = Mid$(CStr(inteiro), j, i - 6)
        If CInt(strAux) > 0 Then
            If Len(strInteiro) > 0 Then
                strInteiro = strInteiro & " , " & Def_cent(strAux, "MILH�O", "MILH�ES")
            Else
                strInteiro = Def_cent(strAux, "MILH�O", "MILH�ES")
            End If
            strMILhao = "S"
        End If
        If j > 1 Then
            strAuxPrep = strAux
        End If
        j = j + i - 6
        i = 6
    End If

    If i > 3 Then  'MIL
        strAux = Mid$(CStr(inteiro), j, i - 3)
        If CInt(strAux) > 0 Then
            If Len(strInteiro) > 0 Then
                If strBilhao = "S" And strMILhao = "N" Then
                    strInteiro = strInteiro & " E " & Def_cent(strAux, "MIL", "MIL")
                Else
                    strInteiro = strInteiro & " , " & Def_cent(strAux, "MIL", "MIL")
                End If
            Else
                strInteiro = Def_cent(strAux, "MIL", "MIL")
            End If
            strMIL = "S"
        End If
        If j > 1 Then
            strAuxPrep = strAuxPrep & strAux
        End If
        j = j + i - 3
        i = 3
    End If

    strAux = Mid$(CStr(inteiro), j, i)
    If CInt(strAux) > 0 Then
        If Len(strInteiro) > 0 Then
            If (strBilhao = "S" And strMIL = "N") Or (strMILhao = "S" And strMIL = "N") Then
                strInteiro = strInteiro & " E " & Def_cent(strAux, "", "")
            Else
                strInteiro = strInteiro & " , " & Def_cent(strAux, "", "")
            End If
        Else
            strInteiro = Def_cent(strAux, "", "")
        End If
    End If

    If inteiro = 1 Then
        strInteiro = strInteiro & " REAL"
    Else
        If strAuxPrep <> "" Then
            If CLng(strAuxPrep) = 0 Then
                strAuxPrep = " DE"
            Else
                strAuxPrep = ""
            End If
        End If
        strInteiro = strInteiro & strAuxPrep & " REAIS"
    End If

    'definir valor de retorno
    If inteiro = 0 And decimall = 0 Then
        Extenso = ""
    ElseIf inteiro = 0 Then
        Extenso = strdecimal
    Else
        If decimall = 0 Then
            Extenso = strInteiro
        Else
            Extenso = strInteiro & " E " & strdecimal
        End If
    End If

End Function


Sub Data(ByRef KeyAscii, ByRef txtCampo)

    'On Error GoTo TrataErro

    Dim bTam As Byte    'tamanho do campo JA digitado
    Dim strData As String
    Dim bKey As Byte
    bTam = Len(txtCampo.Text)
    
    If KeyAscii = 8 And bTam > 0 Then 'backspace
        If Mid$(txtCampo.Text, bTam) = "/" Then
            txtCampo.Text = Mid$(txtCampo.Text, 1, bTam - 2)
        Else
            txtCampo.Text = Mid$(txtCampo.Text, 1, bTam - 1)
        End If
            
    ElseIf Chr$(KeyAscii) >= "0" And Chr$(KeyAscii) <= "9" Then
        If bTam = 1 Then
            strData = txtCampo.Text & Chr$(KeyAscii)
            If CInt(strData) < 1 Or CInt(strData > 31) Then
                Beep
            Else
                txtCampo.Text = txtCampo.Text & Chr$(KeyAscii) & "/"
            End If
            
        ElseIf bTam = 4 Then
            strData = Mid$(txtCampo.Text, 4) & Chr$(KeyAscii)
            If CInt(strData) < 1 Or CInt(strData > 12) Then
                Beep
            Else
                txtCampo.Text = txtCampo.Text & Chr$(KeyAscii) & "/"
            End If
        
        ElseIf bTam = 7 Then
            strData = Mid$(txtCampo.Text, 1, 2)     'dia
            If CInt(strData) < 1 Or CInt(strData > 31) Then
                Beep
            Else
                strData = Mid$(txtCampo.Text, 4, 2)     'mes
                If CInt(strData) < 1 Or CInt(strData > 12) Then
                    Beep
                Else
                    strData = txtCampo.Text & Chr$(KeyAscii)
                    If Not IsDate(CDate(strData)) Then
                        Beep
                    Else
                        txtCampo.Text = strData
                    End If
                End If
            End If
            
        ElseIf bTam < 8 Then
            bKey = KeyAscii
            
        Else
            bKey = 0
        End If
    Else
        Beep
    End If
    
    SendKeys "{END}"
    KeyAscii = bKey
    Exit Sub
    
TrataErro:

    If Err.Number = 13 Then
        MsgBox strData, vbInformation, "Data Inv�lida"
        KeyAscii = 0
        Beep
        Err.Clear
    End If

End Sub





Function CONV_VALOR(strValor As String) As String
    Dim i As Byte
    i = InStr(strValor, ",")
    Do Until i = 0
        Mid(strValor, i, 1) = "."
        i = InStr(strValor, ",")
    Loop
    CONV_VALOR = strValor
End Function



Sub Process_Line_Errors()
    
    Dim iFnum As Integer
    'On Error GoTo Handler_Process_Line_Errors
                
    If Err.Number = 3186 Or Err.Number = 3188 Or Err.Number = 3260 Or Err.Number = 3197 Then
        Resume
    Else
       'iFnum = FreeFile
       'Open strPathUsuario & "ERRORLOG.TXT" For Append As #iFnum
       'Print #iFnum, ""
       'Print #iFnum, App.ProductName & " " & Date & " " & Time
       'Print #iFnum, "  Erro: " & Err.Number & " - " & Err.Description
       'Print #iFnum, "  " & SQL
       'Close #iFnum
    
        MsgBox "Ocorreu o erro: " & Err.Number & " -" & Err.Description & ". Ligue para o departamento de sistemas"
         
        'cursor
        
        
         
         Screen.MousePointer = vbDefault
        'MsgBox "Erro na aplica��o, ligue para o departamento de sistemas", vbCritical, "A T E N � � O"
    
        'para a aplicacao
        End

    End If

    Exit Sub

Handler_Process_Line_Errors:
    DoEvents
    Resume Next

End Sub

Function FmtBR(ByVal Valor) As String

    Dim Temp As String
    Dim i As Integer
    
    Temp = Trim(Valor)
        
    For i = 1 To Len(Temp)
        If Mid$(Temp, i, 1) = "," Then
            Mid$(Temp, i, 1) = "."
        End If
    Next i

    FmtBR = Temp

End Function


Function iRetornaDigito(ByVal iOrigem As Long) As Integer

    Dim iSoma As Integer
    Dim gResto As Single
    Dim iTam As Integer
    Dim iFator As Integer
    Dim iContador As Integer
    Dim sOrigem As String

    iTam = Len(Trim$(Str$(iOrigem)))
    iFator = 2
    
    sOrigem = Trim$(Str$(iOrigem))
    
    For iContador = iTam To 1 Step -1
        iSoma = iSoma + Val(Mid$(sOrigem, iContador, 1)) * iFator
        iFator = iFator + 1
    Next iContador
    
    gResto = iSoma Mod 11
    
    If gResto = 0 Then
        iRetornaDigito = 1
    ElseIf gResto = 1 Then
        iRetornaDigito = 0
    Else
        iRetornaDigito = 11 - gResto
    End If

End Function


Function Maiusculo(KeyAscii As Integer) As Integer
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    Maiusculo = KeyAscii
End Function

Function Numerico(ByVal KeyAscii As Integer) As Integer
    If KeyAscii = 8 Then    'BACKSPACE
        Numerico = KeyAscii
        Exit Function
    End If
    If Chr(KeyAscii) < "0" Or Chr(KeyAscii) > "9" Then
        KeyAscii = 0
    End If
    Numerico = KeyAscii
End Function

Function Texto(ByVal KeyAscii As Integer) As Integer
    If KeyAscii = 8 Then    'BACKSPACE
        Texto = KeyAscii
        Exit Function
    End If
    
    If Chr$(KeyAscii) = "'" Or Chr$(KeyAscii) = "," Or Chr$(KeyAscii) = ";" Then
        KeyAscii = 0
        Beep
    Else
        KeyAscii = Asc(UCase(Chr$(KeyAscii)))
    End If
    
    Texto = KeyAscii
End Function

Function Valor(ByVal KeyAscii As Integer, strCampo As String) As Integer
    If KeyAscii = 8 Then    'BACKSPACE
        Valor = KeyAscii
        Exit Function
    End If
    
    If Chr$(KeyAscii) = "," Or Chr$(KeyAscii) = "." Then
        If InStr(strCampo, ",") > 0 Or InStr(strCampo, ".") > 0 Then
            KeyAscii = 0
            Beep
        End If
    Else
        If Chr$(KeyAscii) < "0" Or Chr$(KeyAscii) > "9" Then
            KeyAscii = 0
            Beep
        End If
    End If
    
   Valor = KeyAscii


End Function

Sub Hora(ByRef KeyAscii, ByRef txtCampo)

    On Error GoTo TrataErro

    Dim bTam As Byte    'tamanho do campo JA digitado
    Dim strData As String
    Dim bKey As Byte
    bTam = Len(txtCampo.Text)
    
    If Chr$(KeyAscii) >= "0" And Chr$(KeyAscii) <= "9" Then
    
        If bTam = 0 Then
            If Chr$(KeyAscii) = "0" Or Chr$(KeyAscii) = "1" Or Chr$(KeyAscii) = "2" Then
                bKey = KeyAscii
            Else
                Beep
                bKey = 0
            End If
                
        ElseIf bTam = 1 Then
            strData = txtCampo
            If Mid(strData, 1, 1) = "1" Or Mid(strData, 1, 1) = "0" Then
                txtCampo.Text = strData & Chr$(KeyAscii) & ":"
                
            ElseIf Mid(strData, 1, 1) = "2" And Chr$(KeyAscii) <= "3" Then
                txtCampo.Text = strData & Chr$(KeyAscii) & ":"
                
            Else
                Beep
                bKey = 0
                
            End If
            
        ElseIf bTam = 3 Then
            strData = txtCampo
            If Chr$(KeyAscii) < 6 Then
                bKey = KeyAscii
            Else
                Beep
                bKey = 0
            End If
            
        
        ElseIf bTam = 4 Then
            bKey = KeyAscii
            
        Else
            bKey = 0
            
        End If
        
    End If
      
    SendKeys "{END}"
    KeyAscii = bKey
    Exit Sub
    
TrataErro:

    If Err.Number = 13 Then
        MsgBox strData, vbInformation, "Hora Inv�lida"
        KeyAscii = 0
        Beep
        Err.Clear
    End If


End Sub

Public Sub sCC(vOpcao As String, vCodCC As Double, vDescCC As String)

    Dim vCommand As New ADODB.Command
    Dim pmOpcao As New ADODB.Parameter
    Dim pmCod_CC As New ADODB.Parameter
    Dim pmDesc_CC As New ADODB.Parameter
    Dim pmErro As New ADODB.Parameter
    Dim pmDescErro As New ADODB.Parameter
    
    vCommand.ActiveConnection = vConnection
    vCommand.CommandText = "{call producao.pck_cad280.pr_CC(?,?,?,?,?)}"
    vCommand.CommandType = adCmdText
    
    Set pmOpcao = vCommand.CreateParameter("pmOpcao", 200, 1, 1, vOpcao)
    'Alex Ci&T - 25/10/12
    Set pmCod_CC = vCommand.CreateParameter("pmCod_CC", 131, 1, 8, vCodCC)
    Set pmDesc_CC = vCommand.CreateParameter("pmDesc_CC", 200, 1, 20, vDescCC)
    Set pmErro = vCommand.CreateParameter("pmErro", 131, 2, 10, 0)
    Set pmDescErro = vCommand.CreateParameter("pmDescErro", 200, 1, 100, "")
    
    vCommand.Parameters.Append pmOpcao
    vCommand.Parameters.Append pmCod_CC
    vCommand.Parameters.Append pmDesc_CC
    vCommand.Parameters.Append pmErro
    vCommand.Parameters.Append pmDescErro
    
    vCommand.Execute
    
    If vCommand.Parameters("pmErro").Value <> 0 Then
        MsgBox ("Erro: " & vCommand.Parameters("pmDescErro").Value)
        End
    End If

End Sub

Public Sub sMascara(vOpcao As String, vCodCC As Double, vCodIdentificador As String, vCNPJ As String, vCodSegmento As String, vMascara As String)

    Dim vCommand As New ADODB.Command
    Dim pmOpcao As New ADODB.Parameter
    Dim pmCod_CC As New ADODB.Parameter
    Dim pmCodIdentificador As New ADODB.Parameter
    Dim pmCNPJ As New ADODB.Parameter
    Dim pmCodSegmento As New ADODB.Parameter
    Dim pmMascara As New ADODB.Parameter
    Dim pmErro As New ADODB.Parameter
    Dim pmDescErro As New ADODB.Parameter
    
    vCommand.ActiveConnection = vConnection
    vCommand.CommandText = "{call producao.pck_cad280.pr_Mascara(?,?,?,?,?,?,?,?)}"
    vCommand.CommandType = adCmdText
    
    Set pmOpcao = vCommand.CreateParameter("pmOpcao", 200, 1, 1, vOpcao)
    Set pmCod_CC = vCommand.CreateParameter("pmCod_CC", 131, 1, 5, vCodCC)
    Set pmCodIdentificador = vCommand.CreateParameter("pmCodIdentificador", 200, 1, 1, vCodIdentificador)
    Set pmCNPJ = vCommand.CreateParameter("pmCNPJ", 200, 1, 4, vCNPJ)
    Set pmCodSegmento = vCommand.CreateParameter("pmCodSegmento", 200, 1, 3, vCodSegmento)
    'CIT Douglasr - SAF-553 - considerar 19 posicicoes ao inves de 16
    Set pmMascara = vCommand.CreateParameter("pmMascara", 200, 1, 19, vMascara)
    Set pmErro = vCommand.CreateParameter("pmErro", 131, 2, 10, 0)
    Set pmDescErro = vCommand.CreateParameter("pmDescErro", 200, 1, 100, "")
    
    vCommand.Parameters.Append pmOpcao
    vCommand.Parameters.Append pmCod_CC
    vCommand.Parameters.Append pmCodIdentificador
    vCommand.Parameters.Append pmCNPJ
    vCommand.Parameters.Append pmCodSegmento
    vCommand.Parameters.Append pmMascara
    vCommand.Parameters.Append pmErro
    vCommand.Parameters.Append pmDescErro
    
    vCommand.Execute
    
    If vCommand.Parameters("pmErro").Value <> 0 Then
        MsgBox ("Erro: " & vCommand.Parameters("pmDescErro").Value)
        End
    End If

End Sub

Public Sub fCNPJ(vOpcao As Byte, vCNPJ As String, ByRef vRecordSet As ADODB.Recordset)

    Dim vCommand As New ADODB.Command
    Dim pmOpcao As New ADODB.Parameter
    Dim pmCNPJ As New ADODB.Parameter
    
    vCommand.ActiveConnection = vConnection
    vCommand.CommandText = "{call producao.pck_cad280.pr_cnpj({resultset 0, pm_cursor},?,?)}"
    vCommand.CommandType = adCmdText
    
    Set pmOpcao = vCommand.CreateParameter("pmOpcao", 131, 1, 1, vOpcao)
    Set pmCNPJ = vCommand.CreateParameter("pmCNPJ", 131, 1, 4, vCNPJ)
    
    vCommand.Parameters.Append pmOpcao
    vCommand.Parameters.Append pmCNPJ
    
    Set vRecordSet = vCommand.Execute()
          
End Sub


Public Sub fSelect(vPesquisa As String, ByRef vRecordSet As ADODB.Recordset)

    Dim vCommand As New ADODB.Command
    Dim pmCod_CC As New ADODB.Parameter
    Dim pmDesc_CC As New ADODB.Parameter
    Dim pmErro As New ADODB.Parameter
    Dim pmDescErro As New ADODB.Parameter
    Dim vDBL As Double
    Dim vStr As String
    
    vCommand.ActiveConnection = vConnection
    vCommand.CommandText = "{call producao.pck_cad280.pr_select({resultset 0, pm_cursor},?,?,?,?)}"
    vCommand.CommandType = adCmdText
    
    If IsNumeric(vPesquisa) Then
        vDBL = CDbl(vPesquisa)
        vStr = ""
    Else
        vDBL = 0
        vStr = CStr(vPesquisa)
    End If
    
    Set pmCod_CC = vCommand.CreateParameter("pmCod_CC", 131, 1, 5, vDBL)
    Set pmDesc_CC = vCommand.CreateParameter("pmDesc_CC", 200, 1, 20, vStr)
    Set pmErro = vCommand.CreateParameter("pmErro", 131, 2, 10, 0)
    Set pmDescErro = vCommand.CreateParameter("pmDescErro", 200, 1, 100, "")
    
    vCommand.Parameters.Append pmCod_CC
    vCommand.Parameters.Append pmDesc_CC
    vCommand.Parameters.Append pmErro
    vCommand.Parameters.Append pmDescErro
    
    Set vRecordSet = vCommand.Execute()
    
    If vCommand.Parameters("pmErro").Value <> 0 Then
        MsgBox ("Erro: " & vCommand.Parameters("pmDescErro").Value)
        End
    End If
          
End Sub

Public Sub ConectaDB()

    'frmMain.stbBarra.Panels(1).Text = "Banco: SDPKT"
    frmMain.stbBarra.Panels(1).Text = "Banco: Produ��o"
    
    vConnection.ConnectionTimeout = 30
    vConnection.CommandTimeout = 60
    vConnection.CursorLocation = 3
    'vConnection.ConnectionString = "Provider=MSDAORA.1;Data Source=SDPKT;Password=PROD;User ID=CAD280;Persist Security Info=False"
    vConnection.ConnectionString = "Provider=MSDAORA.1;Data Source=producao;Password=PROD;User ID=CAD280;Persist Security Info=False"
    vConnection.Open

End Sub

Sub Carrega_Grid()
 
    Dim RET As String
    Dim CmdSql As String
    Dim vRecordSet As New ADODB.Recordset
    
    RET = UCase(InputBox("Digite o Centro de Custo ou parte do nome:", MSG_TIT, "%"))
    
    If RET = "" Then
        'Call MsgBox("Par�metro de pesquisa inv�lido!", vbCritical + vbOKOnly, "Aten��o!")
        Exit Sub
    End If
    
    vParametro = CStr(RET)
    
    Call fSelect(RET, vRecordSet)

    Screen.MousePointer = 11
    
    If vRecordSet.EOF = True Then
        Screen.MousePointer = 0
        MsgBox "Centro de Custo Inexistente!", 48, MSG_TIT
        Exit Sub
    End If
    
    elem = vRecordSet.RecordCount
    frmgrdCentro.lblRegistros.Caption = "Registros: " & Format(elem, "0000")

'----> TRATAMENTO DO GRID

    'Alexsandro CI&T - 19/10/12
    '[SAF-522] - Remover tratamento para exibi��o da coluna "Empresa" na grid grdCentro .
    'frmgrdCentro.grdCentro.ColWidth(4) = 0
    
    frmgrdCentro.grdCentro.Rows = vRecordSet.RecordCount + 1 'n� de linha s do grid:(Record set +1)
        
    For i = 1 To vRecordSet.RecordCount
    
        frmgrdCentro.grdCentro.Row = i
        frmgrdCentro.grdCentro.Col = 0
        frmgrdCentro.grdCentro.Text = CStr(vRecordSet!cod_centro_custo)
        
        frmgrdCentro.grdCentro.Col = 1
        If IsNull(vRecordSet!COD_CENTRO_CUSTO_ANTIGO) = True Then
            frmgrdCentro.grdCentro.Text = "-"
        Else
            frmgrdCentro.grdCentro.Text = Trim(CStr(vRecordSet!COD_CENTRO_CUSTO_ANTIGO))
        End If
        
        frmgrdCentro.grdCentro.Col = 2
        frmgrdCentro.grdCentro.Text = CStr(vRecordSet!DESC_CENTRO_CUSTO)
        frmgrdCentro.grdCentro.Col = 3
        frmgrdCentro.grdCentro.Text = CStr(vRecordSet!mascara)
        
        'Alexsandro CI&T - 19/10/12
        '[SAF-522] - Remover tratamento para exibi��o da coluna "Empresa" na grid grdCentro.
        'frmgrdCentro.grdCentro.Col = 4
        'frmgrdCentro.grdCentro.Text = CStr(vRecordSet!fl_Empresa)
        
        vRecordSet.MoveNext
        
    Next i
      
    frmgrdCentro.Caption = "Centro de Custo"
          
    Screen.MousePointer = 0
    SendKeys "^{HOME}"
    frmgrdCentro.Show 1
    
    Exit Sub
    
TRAT_ERRO:

    Screen.MousePointer = 0
    MsgBox Err.Number & " - " & Err.Description, , MSG_TIT
    Exit Sub
    
End Sub

Sub MfgSort(vMfg As MSFlexGrid)
    
    Dim vType As String
    
    With vMfg
    
    If Left(.TextMatrix(0, .MouseCol), 1) = ">" Then
        .TextMatrix(0, .MouseCol) = "< " & Mid(.TextMatrix(0, .MouseCol), 3)
        vType = "A"
        GoTo label_sort
    ElseIf Left(.TextMatrix(0, .MouseCol), 1) = "<" Then
        .TextMatrix(0, .MouseCol) = "> " & Mid(.TextMatrix(0, .MouseCol), 3)
        vType = "D"
        GoTo label_sort
    Else
        .FormatString = .FormatString
        .TextMatrix(0, .MouseCol) = "> " & .TextMatrix(0, .MouseCol)
        vType = "D"
        GoTo label_sort
    End If
    
label_sort:

    If IsNumeric(.Text) Then
    
        If InStr(1, .Text, ".") > 0 Then
    
            For i = 1 To vMfg.Rows - 1
            
                .Row = i
                .Text = Format(.Text, "0.00")
            
            Next
        
        End If
    
        If vType = "A" Then
            .Sort = flexSortGenericAscending
        Else
            .Sort = flexSortNumericDescending
        End If
        
        If InStr(1, .Text, ".") > 0 Then
        
          For i = 1 To vMfg.Rows - 1
        
              .Row = i
              .Text = Format(.Text, "standard")
          
          Next
          
        End If

    
    Else
    
        If vType = "A" Then
            .Sort = flexSortStringAscending
        Else
            .Sort = flexSortStringDescending
        End If
    
    End If
    
    'Alexsandro CI&T - 19/10/12
    '[SAF-522] - Remover tratamento para exibi��o da coluna "Empresa" na grid grdCentro.
    '--------------------
    'Esconde a coluna que mostra a qual empresa o cc pertence
    '.ColWidth(4) = 0
    '--------------------
    
    End With

End Sub


