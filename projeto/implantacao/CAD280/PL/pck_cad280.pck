create or replace package pck_cad280 is

    -- Author  : RENATOH
    -- Created : 13/12/06 09:04:21
    -- Purpose : cadastro de Centro de Custo
    
    type tp_cursor is ref cursor;

procedure pr_CC
(
    pm_opcao in varchar2,
    pm_cod_centro_custo in number,
    pm_desc_centro_custo in varchar2,
    pm_erro out number,
    pm_desc_erro out varchar2
);

procedure pr_Mascara
(
    pm_opcao in varchar2,
    pm_cod_centro_custo in number,
    pm_cod_identificador in varchar2,
    pm_cnpj in varchar2,
    pm_cod_segmento in varchar2,
    pm_mascara in varchar2,
    pm_erro out number,
    pm_desc_erro out varchar2
);

procedure pr_select
(
    pm_cursor in out tp_cursor,
    pm_cod_centro_custo in number,
    pm_desc_centro_custo in varchar2,
    pm_erro out number,
    pm_desc_erro out varchar2
);

procedure pr_cnpj
(
    pm_cursor in out tp_cursor,
    pm_opcao in number,
    pm_cnpj in number
);

end pck_cad280;
/
create or replace package body pck_cad280 is

procedure pr_CC
(
    pm_opcao in varchar2,
    pm_cod_centro_custo in number,
    pm_desc_centro_custo in varchar2,
    pm_erro out number,
    pm_desc_erro out varchar2
)
is
begin

    if pm_opcao = 'I' then

        insert into contatab.centro_custo(cod_centro_custo, desc_centro_custo)
        values(pm_cod_centro_custo,pm_desc_centro_custo);

    elsif pm_opcao = 'U' then

        update contatab.centro_custo
        set desc_centro_custo = pm_desc_centro_custo
        where cod_centro_custo = pm_cod_centro_custo;

    end if;

    commit;

    exception 
    when others then
    rollback;
    pm_erro := sqlcode;
    pm_desc_erro := sqlerrm;

end pr_CC;

procedure pr_Mascara
(
    pm_opcao in varchar2,
    pm_cod_centro_custo in number,
    pm_cod_identificador in varchar2,
    pm_cnpj in varchar2,
    pm_cod_segmento in varchar2,
    pm_mascara in varchar2,
    pm_erro out number,
    pm_desc_erro out varchar2
)
is
begin

    if pm_opcao = 'I' then

        insert into contatab.r_centro_custo_mascara(cod_centro_custo,cod_identificador,cnpj,cod_segmento,mascara)
        values(pm_cod_centro_custo,pm_cod_identificador,pm_cnpj,pm_cod_segmento,pm_mascara);

    elsif pm_opcao = 'U' then

        update contatab.r_centro_custo_mascara
        set cod_identificador = pm_cod_identificador, cnpj = pm_cnpj, cod_segmento = pm_cod_segmento, mascara = pm_mascara
        where cod_centro_custo = pm_cod_centro_custo;
            
    end if;

    commit;

    exception
    when others then
    rollback;
    pm_erro := sqlcode;
    pm_desc_erro := sqlerrm;

end pr_Mascara;

procedure pr_select
(
    pm_cursor in out tp_cursor,
    pm_cod_centro_custo in number,
    pm_desc_centro_custo in varchar2,
    pm_erro out number,
    pm_desc_erro out varchar2
)
is

    vSelect varchar2(5000);

begin

    vSelect := 'select a.*, b.*,case when a.cod_centro_custo > 80000 and a.cod_centro_custo < 99999 then ''DPK'' else ''DPA'' end fl_empresa
                from contatab.centro_custo a, contatab.r_centro_custo_mascara b ';

    if pm_cod_centro_custo > 0 then

        vSelect := vSelect ||  'where a.cod_centro_custo like ''%' || pm_cod_centro_custo || '%''';

    elsif pm_desc_centro_custo is not null then

        vSelect := vSelect ||  'where a.desc_centro_custo like ' || chr(39) || '%' || pm_desc_centro_custo || '%' || chr(39);

    end if;

    vSelect := vSelect  || ' and b.cod_centro_custo = a.cod_centro_custo order by a.cod_centro_custo';

    open pm_cursor for vSelect;

end pr_select;

procedure pr_cnpj
(
    pm_cursor in out tp_cursor,
    pm_opcao in number,
    pm_cnpj in number
)
is
begin

    if pm_opcao = 1 then --seleciona os dados de todas as lojas

        open pm_cursor for
        select trim(to_char(cod_loja,'00')) cod_loja,nome_fantasia,cgc,trim(substr(cgc,9,4)) cnpj
        from loja
        where cod_loja <> 6 order by cod_loja;

    elsif pm_opcao = 2 then --seleciona o dado de uma loja específica

        open pm_cursor for
        select trim(to_char(cod_loja,'00')),nome_fantasia,cgc,trim(substr(cgc,9,4)) cnpj
        from loja
        where trim(substr(cgc,9,4)) = pm_cnpj;
    
    end if;

end pr_cnpj;

end pck_cad280;
/
