Attribute VB_Name = "MOD_RENA"
Sub Carrega_Grid()

 
    Dim RET As String
    Dim CmdSql As String
    
    
    
    RET = UCase(InputBox("Digite o Centro de Custo ou parte do nome:", MSG_TIT, ""))
    
    If RET = "" Then
        Exit Sub
    End If
    
    db.Parameters.Remove "DESC_CENTRO_CUSTO"
    db.Parameters.Add "DESC_CENTRO_CUSTO", RET, 1
    
    CmdSql = "Select COD_CENTRO_CUSTO, DESC_CENTRO_CUSTO " & _
             "From CONTATAB.CENTRO_CUSTO " & _
             "Where DESC_CENTRO_CUSTO like :DESC_CENTRO_CUSTO " & _
             "Order by DESC_CENTRO_CUSTO"
        
    Screen.MousePointer = 11
    
    Set db_CONS = db.CreateDynaset(CmdSql, 0&)
    
    db.Parameters.Remove "DESC_CENTRO_CUSTO"
    
    If db_CONS.EOF = True Then
        Screen.MousePointer = 0
        MsgBox "Centro de Custo Inexistente!", 48, MSG_TIT
        Exit Sub
        
    End If

'----> TRATAMENTO DO GRID
    
    frmgrdCentro.grdCentro.Rows = db_CONS.RecordCount + 1
    
    frmgrdCentro.grdCentro.Row = 0
    frmgrdCentro.grdCentro.Col = 0
    frmgrdCentro.grdCentro.Text = "Cod"
    frmgrdCentro.grdCentro.Col = 1
    frmgrdCentro.grdCentro.Text = "Descri��o"
        
    
    frmgrdCentro.grdCentro.ColWidth(0) = 3 * 175
    frmgrdCentro.grdCentro.ColWidth(1) = 30 * 175
    
    For I = 0 To 1
        frmgrdCentro.grdCentro.Col = I
        frmgrdCentro.grdCentro.FixedAlignment(I) = 2
    Next I
    
    For I = 1 To db_CONS.RecordCount
        frmgrdCentro.grdCentro.Row = I
        frmgrdCentro.grdCentro.Col = 0
        frmgrdCentro.grdCentro.Text = CStr(db_CONS!COD_CENTRO_CUSTO)
        frmgrdCentro.grdCentro.Col = 1
        frmgrdCentro.grdCentro.Text = CStr(db_CONS!DESC_CENTRO_CUSTO)
        db_CONS.MoveNext
        
    Next I
      
    frmgrdCentro.Caption = "Centro de Custo"
'    frmGrdCentro.Top = 1536
'    frmGrdCentro.Left = 2988
'    frmgrdCentro.Height = 6384
'    frmgrdCentro.Width = 5604
          
    Screen.MousePointer = 0
    frmgrdCentro.grdCentro.Col = 0
    frmgrdCentro.grdCentro.Row = 0
    frmgrdCentro.Show 1
    SendKeys "+{END}"
    
    Exit Sub
    
TRAT_ERRO:

    Screen.MousePointer = 0
    MsgBox Err.Number & " - " & Err.Description, , MSG_TIT
    Exit Sub
End Sub




