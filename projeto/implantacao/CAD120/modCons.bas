Attribute VB_Name = "modCons"
Option Explicit
Public lsRET_FUNC As String
Public MSG_TIT
Public vCodFabrAnterior As String   'Guarda c�digo de f�brica anterior
Public psRet_Grid As String
Public intColRet As Integer
Public DataReal As Date, DataFatur As Date

Public OraParameters As Object

Public Function CONS_DPK(pCOD_DPK As String, _
                         pCOD_LOJA As String) As String

    Dim lsPED_COMP As String

    Screen.MousePointer = 11

    '''''''''''''
    
    Set OraParameters = vBanco.Parameters
    
    Criar_Cursor OraParameters, "vCursor"
    
    OraParameters.Remove "COD_DPK":     OraParameters.Add "COD_DPK", pCOD_DPK, 1
    OraParameters.Remove "COD_LOJA":    OraParameters.Add "COD_LOJA", pCOD_LOJA, 1
    OraParameters.Remove "PED_COMP":    OraParameters.Add "PED_COMP", 0, 2
    OraParameters.Remove "ERRO":        OraParameters.Add "ERRO", 0, 2
    
    vSql = "Producao.PCK_CAD120.PR_CONS_DPK(:COD_DPK, :COD_LOJA, :PED_COMP, :ERRO, :vCursor)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
        Exit Function
    Else
        Set vObjOracle = OraParameters("vCursor").Value
    End If

    If vObjOracle.EOF Then
        Call vVB_Generica_001.Informar("Nenhuma informa��o encontrada!")
        Exit Function
    End If
    
    lsPED_COMP = OraParameters("PED_COMP").Value
    
    ''''''''''''''
    
    Screen.MousePointer = 0

    If vObjOracle.RecordCount = 0 Then
        MsgBox "Item n�o Cadastrado."
        'Call LIMPAR_CAMPOS
        Exit Function
    End If

    'Esta vari�vel ser� usada quando for substitui��o de c�digo de f�brica
    vCodFabrAnterior = vObjOracle!cod_fabrica

    CONS_DPK = vObjOracle!situacao & "|" & Format(vObjOracle!cod_fornecedor, "000") & "|" & _
               vObjOracle!cod_fabrica & Space(19 - Len(vObjOracle!cod_fabrica)) & "|" & _
               vObjOracle!DESC_ITEM & Space(40 - Len(vObjOracle!DESC_ITEM)) & "|" & _
               vObjOracle!DESC_LINHA & Space(20 - Len(vObjOracle!DESC_LINHA)) & "|" & _
               vObjOracle!DESC_GRUPO & Space(20 - Len(vObjOracle!DESC_GRUPO)) & "|" & _
               vObjOracle!DESC_SUBGRUPO & Space(20 - Len(vObjOracle!DESC_SUBGRUPO)) & "|" & _
               Format(vObjOracle!qtd_atual, "00000000.0") & "|" & _
               Format(vObjOracle!qtd_reserv, "00000000.0") & "|" & _
               lsPED_COMP & "|" & vObjOracle!MASCARADO & Space(19 - Len(vObjOracle!MASCARADO)) & "|" & _
               Format(vObjOracle!Min_estoque, "00000000.0")
'0|570|W990050            |TESTE 1                                 |LEVE                |DIVERSOS            |DIVERSOS            |00000000.0|00000000.0|N|W990050            |00000001.0
'0|505|6783               |BOMBA DE COMBUST.                       |LEVE                |MOTOR               |BOMBA COMBUSTIVEL   |00000000.0|00000000.0|N|6783               |00000001.0


End Function


Public Function CONS_FORN(pCOD_FORN As String) As String

    Screen.MousePointer = 11

    '''''''''''''
    
    Set OraParameters = vBanco.Parameters
    
    Criar_Cursor OraParameters, "vCursor"
    
    OraParameters.Remove "COD_FORNEC":  OraParameters.Add "COD_FORNEC", pCOD_FORN, 1
    OraParameters.Remove "ERRO":        OraParameters.Add "ERRO", 0, 2
    
    vSql = "Producao.PCK_CAD120.PR_CONS_FORN(:COD_FORNEC, :ERRO, :vCursor)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
        Exit Function
    Else
        Set vObjOracle = OraParameters("vCursor").Value
    End If

    If vObjOracle.EOF Then
        Call vVB_Generica_001.Informar("Fornecedor n�o Cadastrado!")
        Exit Function
    End If
    
    ''''''''''''''
    
    Screen.MousePointer = 0

    CONS_FORN = vObjOracle!situacao & "|" & _
                vObjOracle!sigla & Space(10 - Len(vObjOracle!sigla)) & "|" & _
                vObjOracle!fl_edi & "|" & _
                vObjOracle!CLASSIFICACAO

End Function



Public Function CONS_FABR(pCOD_FORN As String, _
                          pCOD_FABR As String, _
                          pCOD_LOJA As String) As String

    Screen.MousePointer = 11

    '''''''''''''
    
    Set OraParameters = vBanco.Parameters
    
    Criar_Cursor OraParameters, "vCursor"
    
    OraParameters.Remove "COD_FORNEC":    OraParameters.Add "COD_FORNEC", pCOD_FORN, 1
    OraParameters.Remove "COD_FABRICA":   OraParameters.Add "COD_FABRICA", Trim(pCOD_FABR), 1
    OraParameters.Remove "COD_LOJA":      OraParameters.Add "COD_LOJA", pCOD_LOJA, 1
    OraParameters.Remove "ERRO":          OraParameters.Add "ERRO", 0, 2
    
    vSql = "Producao.PCK_CAD120.PR_CONS_FABRIC(:COD_FORNEC, :COD_FABRICA, :COD_LOJA, :ERRO, :vCursor)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
        Exit Function
    Else
        Set vObjOracle = OraParameters("vCursor").Value
    End If

    If vObjOracle.EOF Then
        CONS_FABR = ""
    Else
        CONS_FABR = vObjOracle!Cod_DPK
    End If

End Function

Public Function CONS_EDI(pCOD_FORN As String, _
                          pCOD_FABR As String) As String

    Screen.MousePointer = 11

    '''''''''''''
    
    Set OraParameters = vBanco.Parameters
    
    Criar_Cursor OraParameters, "vCursor"
    
    OraParameters.Remove "COD_FORN":     OraParameters.Add "COD_FORN", pCOD_FORN, 1
    OraParameters.Remove "COD_EDI":    OraParameters.Add "COD_EDI", pCOD_FABR, 1
    OraParameters.Remove "COD_DPK":    OraParameters.Add "COD_DPK", 0, 2
    OraParameters.Remove "ERRO":        OraParameters.Add "ERRO", 0, 2
    
    vSql = "Producao.PCK_CAD120.PR_CONS_EDI(:COD_FORN, :COD_EDI, :COD_DPK, :ERRO)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
        Exit Function
    End If

    ''''''''''''''

    If OraParameters("COD_DPK") = 0 Then
        CONS_EDI = ""
    Else
        CONS_EDI = OraParameters("COD_DPK")
    End If
    
    Screen.MousePointer = 0

End Function



Public Sub Criar_Cursor(Banco As Object, pNomeCursor As String)
    Banco.Remove pNomeCursor
    Banco.Add pNomeCursor, 0, 3
    Banco(pNomeCursor).serverType = 102
    Banco(pNomeCursor).DynasetOption = &H2&
    Banco(pNomeCursor).DynasetCacheParams 256, 16, 20, 2000, 0
End Sub

Public Sub CONS_GRID(POS_TOP As Integer, _
                     POS_LEFT As Integer, _
                     POS_WIDTH As Integer, _
                     POS_HEIGHT As Integer, _
                     NUM_COL As Byte, _
                     TAM_COL As String, _
                     CABEC_COL As String, _
                     db_CONS As Object, _
                     NOME_GRID As Object, _
                     colRet As Integer)
            
    Dim CONT_L As Integer
    Dim i As Byte
    Dim C_TAM_STR As Byte
    Dim F_TAM_STR As Byte
    Dim C_CAB_STR As Byte
    Dim F_CAB_STR As Byte
    Dim ULT_POS_TAM As Byte
    Dim ULT_POS_CAB As Byte
    Dim TIPO As Integer
    
    intColRet = colRet
   
   'POS_TOP    - Posicao do Topo do Grid
   'POS_LEFT   - Posicao da Coluna a Esquerda
   'POS_WIDTH  - Posicao da Largura
   'POS_HEIGHT - Posicao da Altura
   'NUM_COL    - Numero de Colunas
   'TAM_COL    - Tamanho das Colunas
   'CABEC_COL  - Cabe�alho das Colunas
   'FRM        - Nome do Form
   'SEL        - String que contem o Select

    Screen.MousePointer = 11
           
   'Executa o Select de Consulta
    'Set db_CONS = db.dbCreateDynaset(SEL, 0&)

    psRet_Grid = ""
                    
    If db_CONS.RecordCount = 0 Then
        Screen.MousePointer = 0
        MsgBox "Registro n�o encontrado.", , "CONSULTA"
        psRet_Grid = ""
        'pbRET_GRID = False
        Exit Sub
    Else
        'pboFL_ESCOLHA_GRID = False
        'pbRET_GRID = True
    End If

'    NOME_GRID.Top = POS_TOP
'    NOME_GRID.Left = POS_LEFT
    NOME_GRID.Width = POS_WIDTH
    NOME_GRID.Height = POS_HEIGHT
    
    NOME_GRID.Rows = db_CONS.RecordCount + 1
    NOME_GRID.Cols = NUM_COL
    NOME_GRID.FontBold = True

    C_TAM_STR = 0
    F_TAM_STR = 0
    C_CAB_STR = 0
    F_CAB_STR = 0
    ULT_POS_TAM = 0
    ULT_POS_CAB = 0

    For i = 0 To NUM_COL - 1
        NOME_GRID.Row = 0

        If i < NUM_COL - 1 Then
            ULT_POS_TAM = InStr(F_TAM_STR + 1, TAM_COL, ",")
            C_TAM_STR = ULT_POS_TAM - F_TAM_STR
            ULT_POS_CAB = InStr(F_CAB_STR + 1, CABEC_COL, ",")
            C_CAB_STR = ULT_POS_CAB - F_CAB_STR
        Else
            C_TAM_STR = Len(TAM_COL) - F_TAM_STR + 1
            C_CAB_STR = Len(CABEC_COL) - F_CAB_STR + 1
        End If

       'Define o Tamanho das Colunas
        NOME_GRID.ColWidth(i) = Mid(TAM_COL, F_TAM_STR + 1, C_TAM_STR - 1)

       'Define o Cabe�alho das Colunas
        NOME_GRID.Col = i
        NOME_GRID.Text = Mid(CABEC_COL, F_CAB_STR + 1, C_CAB_STR - 1)

        F_TAM_STR = ULT_POS_TAM
        F_CAB_STR = ULT_POS_CAB
    Next

   'Monta o Conteudo do Grid
    For CONT_L = 1 To db_CONS.RecordCount
        NOME_GRID.Row = CONT_L
        NOME_GRID.Col = 0
        NOME_GRID.Text = db_CONS(0)
        If NUM_COL = 2 Then
            NOME_GRID.Col = 1
            NOME_GRID.Text = db_CONS(1)
        ElseIf NUM_COL = 3 Then
            NOME_GRID.Col = 1
            NOME_GRID.Text = db_CONS(1)
            NOME_GRID.Col = 2
            NOME_GRID.Text = db_CONS(2)
        ElseIf NUM_COL = 4 Then
            NOME_GRID.Col = 1
            NOME_GRID.Text = db_CONS(1)
            NOME_GRID.Col = 2
            NOME_GRID.Text = db_CONS(2)
            NOME_GRID.Col = 3
            NOME_GRID.Text = db_CONS(3)
        ElseIf NUM_COL = 5 Then
            NOME_GRID.Col = 1
            NOME_GRID.Text = db_CONS(1)
            NOME_GRID.Col = 2
            NOME_GRID.Text = db_CONS(2)
            NOME_GRID.Col = 3
            NOME_GRID.Text = db_CONS(3)
            NOME_GRID.Col = 4
            NOME_GRID.Text = db_CONS(4)
        ElseIf NUM_COL = 6 Then
            NOME_GRID.Col = 1
            NOME_GRID.Text = db_CONS(1)
            NOME_GRID.Col = 2
            NOME_GRID.Text = db_CONS(2)
            NOME_GRID.Col = 3
            NOME_GRID.Text = db_CONS(3)
            NOME_GRID.Col = 4
            NOME_GRID.Text = db_CONS(4)
            NOME_GRID.Col = 5
            NOME_GRID.Text = db_CONS(5)
        ElseIf NUM_COL = 7 Then
            NOME_GRID.Col = 1
            NOME_GRID.Text = db_CONS(1)
            NOME_GRID.Col = 2
            NOME_GRID.Text = db_CONS(2)
            NOME_GRID.Col = 3
            NOME_GRID.Text = db_CONS(3)
            NOME_GRID.Col = 4
            NOME_GRID.Text = db_CONS(4)
            NOME_GRID.Col = 5
            NOME_GRID.Text = db_CONS(5)
            NOME_GRID.Col = 6
            NOME_GRID.Text = db_CONS(6)
        ElseIf NUM_COL = 8 Then
            NOME_GRID.Col = 1
            NOME_GRID.Text = db_CONS(1)
            NOME_GRID.Col = 2
            NOME_GRID.Text = db_CONS(2)
            NOME_GRID.Col = 3
            NOME_GRID.Text = db_CONS(3)
            NOME_GRID.Col = 4
            NOME_GRID.Text = db_CONS(4)
            NOME_GRID.Col = 5
            NOME_GRID.Text = db_CONS(5)
            NOME_GRID.Col = 6
            NOME_GRID.Text = db_CONS(6)
            NOME_GRID.Col = 7
            NOME_GRID.Text = db_CONS(7)
        End If

        db_CONS.dbMoveNext
    Next

    NOME_GRID.Visible = True
    NOME_GRID.Row = 1
    SendKeys "^+{HOME}"
    Screen.MousePointer = 0
End Sub

Public Function Bloco_Acerto(intCOD_DPK_1 As Long, intCOD_DPK_2 As Long, strFORN_CLASS As String, strFORN_EDI As String) As Boolean

    '''''''''''''
    
    Set OraParameters = vBanco.Parameters
    
    OraParameters.Remove "COD_DPK_1":  OraParameters.Add "COD_DPK_1", intCOD_DPK_1, 1
    OraParameters.Remove "COD_DPK_2":  OraParameters.Add "COD_DPK_2", intCOD_DPK_2, 1
    OraParameters.Remove "FORN_CLASS": OraParameters.Add "FORN_CLASS", strFORN_CLASS, 1
    OraParameters.Remove "FORN_EDI":   OraParameters.Add "FORN_EDI", strFORN_EDI, 1
    OraParameters.Remove "COD_ERRO":   OraParameters.Add "COD_ERRO", 0, 2
    OraParameters.Remove "TXT_ERRO":   OraParameters.Add "TXT_ERRO", Null, 2
    
    vSql = "Producao.PCK_CAD120.PR_ACERTO(:COD_DPK_1, :COD_DPK_2, :FORN_CLASS, :FORN_EDI, :COD_ERRO, :TXT_ERRO)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
        Bloco_Acerto = False
        Exit Function
    End If

     
    ''''''''''''''

    If OraParameters("COD_ERRO") <> 0 Then
        MsgBox "Ocorreram os seguintes erros no acerto: " & vbCrLf & Replace(CStr(OraParameters("TXT_ERRO")), "#", vbCrLf), vbOKOnly, "Erros no Acerto"
        'MsgBox "Ocorreram os seguintes erros no acerto: " & OraParameters("TXT_ERRO"), vbOKOnly, "Erros no Acerto"
        Bloco_Acerto = False
        Exit Function
    End If

    Bloco_Acerto = True
    
End Function

Public Function Acerta_Equivalencias(ByRef vCodDPKAnterior, ByRef vCodDPKAtual) As Boolean

    '''''''''''''
    
    Set OraParameters = vBanco.Parameters
    
    OraParameters.Remove "COD_DPK_ANT":  OraParameters.Add "COD_DPK_ANT", vCodDPKAnterior, 1
    OraParameters.Remove "COD_DPK_ATU":  OraParameters.Add "COD_DPK_ATU", vCodDPKAtual, 1
    OraParameters.Remove "COD_ERRO":   OraParameters.Add "COD_ERRO", 0, 2
    OraParameters.Remove "TXT_ERRO":   OraParameters.Add "TXT_ERRO", Null, 2
    
    vSql = "Producao.PCK_CAD120.PR_AC_EQUIV(:COD_DPK_ANT, :COD_DPK_ATU, :COD_ERRO, :TXT_ERRO)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
        Acerta_Equivalencias = False
        Exit Function
    End If

    
    ''''''''''''''

    If OraParameters("COD_ERRO") <> 0 Then
        Screen.MousePointer = 0
        MsgBox OraParameters("COD_ERRO") & " - " & OraParameters("TXT_ERRO") & vbCrLf & _
                "Erro ao acertar equival�ncias! Ligue p/ Depto.Sistemas."
        Acerta_Equivalencias = False
        Exit Function
    End If

    Acerta_Equivalencias = True

End Function


Public Function Acerta_DtPriEnt_Acerto(ByRef vCodDPKAnterior, ByRef vCodDPKAtual) As Boolean

    '''''''''''''
    
    Set OraParameters = vBanco.Parameters
    
    OraParameters.Remove "COD_DPK_ANT":  OraParameters.Add "COD_DPK_ANT", vCodDPKAnterior, 1
    OraParameters.Remove "COD_DPK_ATU":  OraParameters.Add "COD_DPK_ATU", vCodDPKAtual, 1
    OraParameters.Remove "COD_ERRO":   OraParameters.Add "COD_ERRO", 0, 2
    OraParameters.Remove "TXT_ERRO":   OraParameters.Add "TXT_ERRO", Null, 2
    
    vSql = "Producao.PCK_CAD120.PR_AC_DTPRIENT(:COD_DPK_ANT, :COD_DPK_ATU, :COD_ERRO, :TXT_ERRO)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
        Acerta_DtPriEnt_Acerto = False
        Exit Function
    End If

    
    ''''''''''''''

    If OraParameters("COD_ERRO") <> 0 Then
        Screen.MousePointer = 0
        
        If CInt(OraParameters("COD_ERRO")) = 1 Then
            MsgBox Replace(OraParameters("TXT_ERRO"), "#", vbCrLf)
            Acerta_DtPriEnt_Acerto = True
            Exit Function
        Else
            MsgBox OraParameters("COD_ERRO") & " - " & OraParameters("TXT_ERRO") & vbCrLf & _
                    "Erro ao acertar data de primeira entrada! Ligue p/ Depto.Sistemas."
            Acerta_DtPriEnt_Acerto = False
            Exit Function
        End If
    End If

    Acerta_DtPriEnt_Acerto = True

End Function


Public Function Acerta_Fotos(ByRef vCodDPKAnterior, ByRef vCodDPKAtual)

    '''''''''''''
    
    Set OraParameters = vBanco.Parameters
    
    OraParameters.Remove "COD_DPK_ANT":   OraParameters.Add "COD_DPK_ANT", vCodDPKAnterior, 1
    OraParameters.Remove "COD_DPK_ATU":   OraParameters.Add "COD_DPK_ATU", vCodDPKAtual, 1
    OraParameters.Remove "FOTOS_ANT":   OraParameters.Add "FOTOS_ANT", 0, 2
    OraParameters.Remove "FOTOS_ATU":   OraParameters.Add "FOTOS_ATU", 0, 2
    OraParameters.Remove "COD_ERRO":      OraParameters.Add "COD_ERRO", 0, 2
    
    vSql = "Producao.PCK_CAD120.PR_BUSCA_FOTOS(:COD_DPK_ANT, :COD_DPK_ATU, :FOTOS_ANT, :FOTOS_ATU, :COD_ERRO)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
        Acerta_Fotos = False
        Exit Function
    End If

    ''''''''''''''
    
    Dim fl_Foto_DPK_Ant As Boolean, fl_Foto_DPK_Atu As Boolean, fl_Mostra_Msg As Boolean
    Dim vMsg As String
    
    ' Verifica se o item anterior tem foto
    If CInt(OraParameters("FOTOS_ANT")) = 0 Then
        fl_Foto_DPK_Ant = False
    Else
        fl_Foto_DPK_Ant = True
    End If
    
    ' Verifica se o item atual tem foto
    If CInt(OraParameters("FOTOS_ATU")) = 0 Then
        fl_Foto_DPK_Atu = False
    Else
        fl_Foto_DPK_Atu = True
    End If

    'Se tanto o item anterior, como o atual, j� tiverem foto cadastrada
    'Perguntar ao usu�rio se quer acertar foto
    If fl_Foto_DPK_Ant = True And fl_Foto_DPK_Atu = True Then
        
        vMsg = "DPK Anterior e Atual j� possuem foto cadastrada." & Chr(13) & _
               "Deseja atualizar foto do DPK Atual, para que receba mesma foto do DPK Anterior?"
        fl_Mostra_Msg = True
    
    'Se nem o item anterior e nem o atual, tiverem foto cadastrada
    'Neste caso, n�o ser� perguntado ao usu�rio se quer acertar foto, apenas dar� mensagem sobre situa��o das foto
    ElseIf fl_Foto_DPK_Ant = False And fl_Foto_DPK_Atu = False Then
        
        vMsg = "Nem DPK Anterior, nem DPK Atual possuem foto cadastrada."
        fl_Mostra_Msg = False
    
    'Se o item anterior possui foto cadastrada, mas o item atual n�o
    'Perguntar ao usu�rio se quer acertar foto
    ElseIf fl_Foto_DPK_Ant = True And fl_Foto_DPK_Atu = False Then
    
        vMsg = "DPK Anterior possui foto cadastrada mas DPK Atual n�o." & Chr(13) & _
               "Deseja atualizar foto do DPK Atual, para que receba mesma foto do DPK Anterior?"
        fl_Mostra_Msg = True
    
    'Se o item anterior n�o possui foto cadastrada, mas o item atual sim
    ElseIf fl_Foto_DPK_Ant = False And fl_Foto_DPK_Atu = True Then
    
        'vMsg = "DPK Atual j� possui foto cadastrada mas DPK Anterior n�o." & Chr(13) & _
               "Deseja atualizar foto do DPK Atual, para que n�o receba nenhuma foto (como est� no DPK Anterior)?" & _
               "'SIM' - Atualiza Foto / 'N�O' - Mant�m fotos como est�o"
        vMsg = "DPK Atual j� possui foto cadastrada."
        fl_Mostra_Msg = False
    
    End If
    
    'Se as fotos estiverem numa situa��o em que n�o podem ser atualizadas, vou somente mostrar a mensagem
    If fl_Mostra_Msg = False Then
        MsgBox vMsg, vbInformation, "Acerto de Fotos"
    Else
        'Vou perguntar se devo atualizar ou n�o: 6=sim, 7=n�o
        If MsgBox(vMsg, vbYesNo, "Acerto de Fotos") = vbNo Then
            Acerta_Fotos = True
            Exit Function
        Else
                
            '''''''''''''
            
            Set OraParameters = vBanco.Parameters
            
            OraParameters.Remove "COD_DPK_ANT":  OraParameters.Add "COD_DPK_ANT", vCodDPKAnterior, 1
            OraParameters.Remove "COD_DPK_ATU":  OraParameters.Add "COD_DPK_ATU", vCodDPKAtual, 1
            OraParameters.Remove "COD_ERRO":   OraParameters.Add "COD_ERRO", 0, 2
            OraParameters.Remove "TXT_ERRO":   OraParameters.Add "TXT_ERRO", Null, 2
            
            vSql = "Producao.PCK_CAD120.PR_AC_FOTOS(:COD_DPK_ANT, :COD_DPK_ATU, :COD_ERRO, :TXT_ERRO)"
            
            If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
                Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
                Acerta_Fotos = False
                Exit Function
            End If
     
            
            ''''''''''''''
    
            If OraParameters("COD_ERRO") <> 0 Then
                Screen.MousePointer = 0
                MsgBox OraParameters("COD_ERRO") & " - " & OraParameters("TXT_ERRO") & vbCrLf & _
                        "Erro ao acertar as fotos! Ligue p/ Depto.Sistemas."
                Acerta_Fotos = False
                Exit Function
            End If
    
            
        End If
        
    End If
    
    Acerta_Fotos = True

End Function

Public Function Bloco_Substituicao(tmpCOD_DPK_1 As Long, tmpCOD_FORN As Long, tmpCOD_FABR As String, _
                                  tmpCOD_MASC As String, tmpSEQ_TAB As Long, tmpCOD_EDI As String, _
                                  tmpDT_REAL As Date, tmpMIN_EST As Double, tmpFORN_CLASS As String, _
                                  tmpFORN_EDI As String) As Boolean

    '''''''''''''
    
    Set OraParameters = vBanco.Parameters
    
    OraParameters.Remove "COD_DPK_1":   OraParameters.Add "COD_DPK_1", tmpCOD_DPK_1, 1
    OraParameters.Remove "COD_FORN":    OraParameters.Add "COD_FORN", tmpCOD_FORN, 1
    OraParameters.Remove "COD_FABR":    OraParameters.Add "COD_FABR", tmpCOD_FABR, 1
    OraParameters.Remove "COD_MASC":    OraParameters.Add "COD_MASC", tmpCOD_MASC, 1
    OraParameters.Remove "SEQ_TAB":     OraParameters.Add "SEQ_TAB", tmpSEQ_TAB, 1
    OraParameters.Remove "COD_EDI":     OraParameters.Add "COD_EDI", tmpCOD_EDI, 1
    OraParameters.Remove "DT_REAL":     OraParameters.Add "DT_REAL", tmpDT_REAL, 1
    OraParameters.Remove "MIN_EST":     OraParameters.Add "MIN_EST", tmpMIN_EST, 1
    OraParameters.Remove "FORN_CLASS":  OraParameters.Add "FORN_CLASS", tmpFORN_CLASS, 1
    OraParameters.Remove "FORN_EDI":    OraParameters.Add "FORN_EDI", tmpFORN_EDI, 1
    OraParameters.Remove "RET_DPK":     OraParameters.Add "RET_DPK", 0, 2
    OraParameters.Remove "COD_ERRO":    OraParameters.Add "COD_ERRO", 0, 2
    OraParameters.Remove "TXT_ERRO":    OraParameters.Add "TXT_ERRO", Null, 2
    
    
    vSql = "Producao.PCK_CAD120.PR_SUBSTITUICAO(:COD_DPK_1, :COD_FORN, :COD_FABR, :COD_MASC, :SEQ_TAB, :COD_EDI, :DT_REAL, :MIN_EST, :FORN_CLASS, :FORN_EDI, :RET_DPK, :COD_ERRO, :TXT_ERRO)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
        Bloco_Substituicao = False
        Screen.MousePointer = 0
        Exit Function
    End If
    
    If OraParameters("COD_ERRO").Value <> 0 Then
        Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
        Bloco_Substituicao = False
        Screen.MousePointer = 0
        Exit Function
    End If
    
    ''''''''''''''
    
    Bloco_Substituicao = True
    
End Function



Public Function Bloco_Volta(tmpCOD_DPK_1 As Long, tmpCOD_FORN As Long, tmpCUEMA As String, _
                                  tmpSEQ_TAB As Long, tmpCOD_EDI As String, _
                                  tmpDT_REAL As Date, tmpFORN_CLASS As String, _
                                  tmpFORN_EDI As String) As Boolean

    '''''''''''''
    
    Set OraParameters = vBanco.Parameters
    
    OraParameters.Remove "COD_DPK_1":   OraParameters.Add "COD_DPK_1", tmpCOD_DPK_1, 1
    OraParameters.Remove "COD_FORN":    OraParameters.Add "COD_FORN", tmpCOD_FORN, 1
    OraParameters.Remove "CUEMA":    OraParameters.Add "CUEMA", Val(tmpCUEMA), 1
    OraParameters.Remove "SEQ_TAB":     OraParameters.Add "SEQ_TAB", tmpSEQ_TAB, 1
    OraParameters.Remove "COD_EDI":     OraParameters.Add "COD_EDI", tmpCOD_EDI, 1
    OraParameters.Remove "DT_REAL":     OraParameters.Add "DT_REAL", tmpDT_REAL, 1
    OraParameters.Remove "FORN_CLASS":  OraParameters.Add "FORN_CLASS", tmpFORN_CLASS, 1
    OraParameters.Remove "FORN_EDI":    OraParameters.Add "FORN_EDI", tmpFORN_EDI, 1
    OraParameters.Remove "COD_ERRO":    OraParameters.Add "COD_ERRO", 0, 2
    OraParameters.Remove "TXT_ERRO":    OraParameters.Add "TXT_ERRO", Null, 2
    
    
    vSql = "Producao.PCK_CAD120.PR_VOLTA(:COD_DPK_1, :COD_FORN, :CUEMA, :SEQ_TAB, :COD_EDI, :DT_REAL, :FORN_CLASS, :FORN_EDI, :COD_ERRO, :TXT_ERRO)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
        Bloco_Volta = False
        Screen.MousePointer = 0
        Exit Function
    End If
    
    If OraParameters("COD_ERRO").Value <> 0 Then
        Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
        Bloco_Volta = False
        Screen.MousePointer = 0
        Exit Function
    End If
    
    ''''''''''''''
    
    Bloco_Volta = True
    
End Function

