VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form frmSubstituicao 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Substitui��o de Itens"
   ClientHeight    =   5685
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8250
   Icon            =   "frmSubst.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   379
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   550
   Begin VB.Frame fraNovo 
      Caption         =   "Item Novo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1815
      Left            =   90
      TabIndex        =   27
      Top             =   3510
      Visible         =   0   'False
      Width           =   8070
      Begin VB.Frame fraDados_Novo 
         Height          =   1050
         Left            =   90
         TabIndex        =   30
         Top             =   675
         Visible         =   0   'False
         Width           =   7890
         Begin VB.TextBox txtSEQ_TAB_Novo 
            Height          =   285
            Left            =   5175
            TabIndex        =   11
            Top             =   270
            Width           =   2220
         End
         Begin VB.TextBox txtCOD_EDI_Novo 
            Height          =   285
            Left            =   1485
            TabIndex        =   12
            Top             =   630
            Width           =   5910
         End
         Begin VB.TextBox txtCOD_MASCARADO_Novo 
            Height          =   285
            Left            =   1485
            TabIndex        =   10
            Top             =   270
            Width           =   2355
         End
         Begin VB.Label lblSeq_Tab_Novo 
            AutoSize        =   -1  'True
            Caption         =   "Sequencia Tab."
            Height          =   195
            Left            =   3960
            TabIndex        =   33
            Top             =   315
            Width           =   1140
         End
         Begin VB.Label lblCOD_EDI_Novo 
            AutoSize        =   -1  'True
            Caption         =   "C�d. F�brica EDI"
            Height          =   195
            Left            =   135
            TabIndex        =   32
            Top             =   675
            Width           =   1215
         End
         Begin VB.Label lblCOD_MASCARADO_Novo 
            AutoSize        =   -1  'True
            Caption         =   "C�d. Mascarado"
            Height          =   195
            Left            =   135
            TabIndex        =   31
            Top             =   315
            Width           =   1170
         End
      End
      Begin VB.TextBox txtCOD_FORNEC_Novo 
         Height          =   315
         Left            =   1095
         TabIndex        =   8
         Top             =   315
         Width           =   700
      End
      Begin VB.TextBox txtCOD_FABRIC_Novo 
         Height          =   315
         Left            =   5280
         TabIndex        =   9
         Top             =   315
         Width           =   2500
      End
      Begin VB.Label lblSIGLA_FORN_Novo 
         Height          =   195
         Left            =   1980
         TabIndex        =   34
         Top             =   375
         Width           =   2160
      End
      Begin VB.Label lblCOD_FORNEC_Novo 
         AutoSize        =   -1  'True
         Caption         =   "Fornecedor"
         Height          =   195
         Left            =   135
         TabIndex        =   29
         Top             =   375
         Width           =   810
      End
      Begin VB.Label lblCOD_FABRICA_Novo 
         AutoSize        =   -1  'True
         Caption         =   "C�d. Fabrica"
         Height          =   195
         Left            =   4245
         TabIndex        =   28
         Top             =   375
         Width           =   900
      End
   End
   Begin VB.Frame fraAntigo 
      Caption         =   "Item Antigo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2535
      Left            =   90
      TabIndex        =   18
      Top             =   945
      Width           =   8070
      Begin VB.ComboBox cboFILIAL 
         Height          =   315
         Left            =   1125
         Style           =   2  'Dropdown List
         TabIndex        =   0
         Top             =   360
         Width           =   2265
      End
      Begin VB.TextBox txtCOD_DPK 
         Height          =   315
         Left            =   1125
         TabIndex        =   1
         Top             =   765
         Width           =   1140
      End
      Begin VB.TextBox txtDESC_ITEM 
         Height          =   315
         Left            =   2385
         Locked          =   -1  'True
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   765
         Width           =   5415
      End
      Begin VB.TextBox txtCOD_FORNEC 
         Height          =   315
         Left            =   1125
         TabIndex        =   3
         Top             =   1170
         Width           =   700
      End
      Begin VB.TextBox txtCOD_FABRIC 
         Height          =   315
         Left            =   5310
         TabIndex        =   4
         Top             =   1170
         Width           =   2500
      End
      Begin VB.TextBox txtLIN 
         Height          =   315
         Left            =   1125
         Locked          =   -1  'True
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   1575
         Width           =   2265
      End
      Begin VB.TextBox txtGRP 
         Height          =   315
         Left            =   5310
         Locked          =   -1  'True
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   1575
         Width           =   2500
      End
      Begin VB.TextBox txtSUBGP 
         Height          =   315
         Left            =   5310
         Locked          =   -1  'True
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   1980
         Width           =   2500
      End
      Begin VB.Label lblDeposito 
         AutoSize        =   -1  'True
         Caption         =   "Dep�sito"
         Height          =   195
         Left            =   345
         TabIndex        =   26
         Top             =   420
         Width           =   630
      End
      Begin VB.Label lblCodDPK 
         AutoSize        =   -1  'True
         Caption         =   "C�d. DPK"
         Height          =   195
         Left            =   270
         TabIndex        =   25
         Top             =   825
         Width           =   705
      End
      Begin VB.Label lblFornec 
         AutoSize        =   -1  'True
         Caption         =   "Fornecedor"
         Height          =   195
         Left            =   165
         TabIndex        =   24
         Top             =   1230
         Width           =   810
      End
      Begin VB.Label lblLinha 
         AutoSize        =   -1  'True
         Caption         =   "Linha"
         Height          =   195
         Left            =   585
         TabIndex        =   23
         Top             =   1635
         Width           =   390
      End
      Begin VB.Label lblCodFabrica 
         AutoSize        =   -1  'True
         Caption         =   "C�d. Fabrica"
         Height          =   195
         Left            =   4275
         TabIndex        =   22
         Top             =   1230
         Width           =   900
      End
      Begin VB.Label lblGrupo 
         AutoSize        =   -1  'True
         Caption         =   "Grupo"
         Height          =   195
         Left            =   4740
         TabIndex        =   21
         Top             =   1635
         Width           =   435
      End
      Begin VB.Label lblSubGrupo 
         AutoSize        =   -1  'True
         Caption         =   "Subgrupo"
         Height          =   195
         Left            =   4485
         TabIndex        =   20
         Top             =   2040
         Width           =   690
      End
      Begin VB.Label lblSIGLA_FORN 
         Height          =   195
         Left            =   1980
         TabIndex        =   19
         Top             =   1230
         Width           =   2160
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   13
      Top             =   5355
      Width           =   8250
      _ExtentX        =   14552
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   14049
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   14
      Top             =   810
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   15
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmSubst.frx":23D2
      PICN            =   "frmSubst.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdOK 
      Height          =   690
      Left            =   810
      TabIndex        =   16
      TabStop         =   0   'False
      ToolTipText     =   "Confimar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmSubst.frx":30C8
      PICN            =   "frmSubst.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdCancela 
      Height          =   690
      Left            =   1575
      TabIndex        =   17
      TabStop         =   0   'False
      ToolTipText     =   "Cancelar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmSubst.frx":3DBE
      PICN            =   "frmSubst.frx":3DDA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmSubstituicao"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim lsFL_EDI As String
Dim psFORN_EDI As String
Dim psFORN_CLASS As String
Dim ldQTD_MINEST As Double


Private Sub cmdCancela_Click()

    txtCOD_DPK.Text = ""
    txtCOD_FORNEC.Text = ""
    txtCOD_FABRIC.Text = ""
    lblSIGLA_FORN.Caption = ""
    txtDESC_ITEM.Text = ""
    txtLIN.Text = ""
    txtGRP.Text = ""
    txtSUBGP.Text = ""
    txtCOD_FORNEC_Novo.Text = ""
    lblSIGLA_FORN_Novo.Caption = ""
    txtCOD_FABRIC_Novo.Text = ""
    txtCOD_MASCARADO_Novo.Text = ""
    fraNovo.Visible = False
    txtCOD_DPK.SetFocus
    txtCOD_EDI_Novo.Text = ""

End Sub

Private Sub cmdOK_Click()

    If Trim(txtCOD_DPK.Text) = "" Then
        MsgBox "Entre com o Item Antigo.", , MSG_TIT
        txtCOD_DPK.SetFocus
        Exit Sub
    ElseIf Trim(txtCOD_FORNEC_Novo.Text) = "" Or Trim(txtCOD_FABRIC_Novo.Text) = "" Then
        MsgBox "Entre com o Item Novo.", , MSG_TIT
        txtCOD_FORNEC_Novo.SetFocus
        Exit Sub
    ElseIf Trim(txtCOD_MASCARADO_Novo.Text) = "" Then
        MsgBox "Entre com o C�digo Mascarado.", , MSG_TIT
        txtCOD_MASCARADO_Novo.SetFocus
        Exit Sub
    ElseIf Trim(txtSEQ_TAB_Novo.Text) = "" Then
        MsgBox "Entre com a Sequ�ncia da Tabela.", , MSG_TIT
        txtSEQ_TAB_Novo.SetFocus
        Exit Sub
    ElseIf txtCOD_EDI_Novo.Visible = True And Trim(txtCOD_EDI_Novo.Text) = "" Then
        MsgBox "Entre com o C�digo de F�brica EDI.", , MSG_TIT
        txtCOD_EDI_Novo.SetFocus
        Exit Sub
    End If
    
    If vVB_Generica_001.Perguntar("Confirma Substitui��o?") = vbYes Then
    
      If Not Bloco_Substituicao(txtCOD_DPK.Text, txtCOD_FORNEC_Novo.Text, txtCOD_FABRIC_Novo.Text, _
                                txtCOD_MASCARADO_Novo.Text, txtSEQ_TAB_Novo.Text, txtCOD_EDI_Novo.Text, _
                                DataReal, ldQTD_MINEST, psFORN_CLASS, psFORN_EDI) Then
          Exit Sub
      End If
      
      Dim RET_DPK As Long
      
      RET_DPK = OraParameters("RET_DPK").Value
      
      Dim vCodDPKAnterior As Long, vCodDPKAtual As Long
      
      vCodDPKAnterior = Trim(txtCOD_DPK.Text)
      vCodDPKAtual = Trim(RET_DPK)
      
      If Not Acerta_Equivalencias(vCodDPKAnterior, vCodDPKAtual) Then
          Exit Sub
      End If

      If Not Acerta_DtPriEnt_Acerto(vCodDPKAnterior, vCodDPKAtual) Then
          Exit Sub
      End If

      If Not Acerta_Fotos(vCodDPKAnterior, vCodDPKAtual) Then
          Exit Sub
      End If

      vVB_Generica_001.Informar ("Substitui��o OK. Novo C�d.DPK = " & RET_DPK)

      cmdCancela_Click
      
    End If

End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    
    Me.Top = 0
    Me.Left = 0
    
    cboFILIAL.Clear
    cboFILIAL.AddItem "99 - TODOS"
    cboFILIAL.ListIndex = 0
    
End Sub

Private Sub txtCOD_DPK_KeyPress(KeyAscii As Integer)
    If Chr(KeyAscii) < "0" Or Chr(KeyAscii) > "9" Then
       KeyAscii = 0
       Beep
    End If
End Sub

Private Sub txtCOD_DPK_LostFocus()

    Dim ldQTD_ATUAL As Double
    Dim ldQTD_RESERV As Double
    
    Dim lsPED_COM As String
    
    If Trim(txtCOD_DPK) <> "" Then

        lsRET_FUNC = CONS_DPK(txtCOD_DPK, Str(vCd))
        If lsRET_FUNC = "" Then
            cmdCancela_Click
            txtCOD_DPK.SetFocus
            Exit Sub
        ElseIf Mid(lsRET_FUNC, 1, 1) <> 0 Then
            MsgBox "Item Desativado/Fora de Linha.", , MSG_TIT
            cmdCancela_Click
            txtCOD_DPK.SetFocus
            Exit Sub
        End If
        txtCOD_FABRIC.Text = Mid(lsRET_FUNC, 7, 19)
        txtDESC_ITEM.Text = Mid(lsRET_FUNC, 27, 40)
        txtLIN.Text = Mid(lsRET_FUNC, 68, 20)
        txtGRP.Text = Mid(lsRET_FUNC, 89, 20)
        txtSUBGP.Text = Mid(lsRET_FUNC, 110, 20)
        ldQTD_ATUAL = CDbl(Mid(lsRET_FUNC, 131, 10))
        ldQTD_RESERV = CDbl(Mid(lsRET_FUNC, 142, 10))
        lsPED_COM = Mid(lsRET_FUNC, 153, 1)
        ldQTD_MINEST = CDbl(Mid(lsRET_FUNC, 175, 10))

        txtCOD_FORNEC.Text = Mid(lsRET_FUNC, 3, 3)
        'txtCOD_FORNEC_LostFocus

        If ldQTD_ATUAL <> 0 Or ldQTD_RESERV <> 0 Then
            MsgBox "Item com saldo em Estoque ou Reservado.", , MSG_TIT
            cmdCancela_Click
            txtCOD_DPK.SetFocus
            Exit Sub
        ElseIf lsPED_COM = "S" Then
            MsgBox "Item possui pend�ncia na F�brica. Imposs�vel fazer Acerto.", , MSG_TIT
            cmdCancela_Click
            txtCOD_DPK.SetFocus
            Exit Sub
        End If

        Screen.MousePointer = 11
        
        '//////////////////////////////////////
        '///  VERIFICA OS OUTROS DEPOSITOS  ///
        '//////////////////////////////////////
        
        Set OraParameters = vBanco.Parameters
        
        Criar_Cursor OraParameters, "vCursor"
        
        OraParameters.Remove "COD_DPK":     OraParameters.Add "COD_DPK", Trim(txtCOD_DPK.Text), 1
        OraParameters.Remove "TOT":         OraParameters.Add "TOT", 0, 2
        OraParameters.Remove "PED":         OraParameters.Add "PED", 0, 2
        OraParameters.Remove "ERRO":        OraParameters.Add "ERRO", 0, 2
        
        vSql = "Producao.PCK_CAD120.PR_CONS_DPK_DEP(:COD_DPK, :TOT, :PED, :ERRO)"
        
        If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
            Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
            Exit Sub
        End If
    
        If Val(OraParameters("TOT").Value) > 0 Then
            MsgBox "Item com saldo em Estoque ou Reservado para Outros Dep�sitos.", vbCritical, MSG_TIT
            cmdCancela_Click
            txtCOD_DPK.SetFocus
            Screen.MousePointer = 0
            Exit Sub
        End If
                        
        If Val(OraParameters("PED").Value) > 0 Then
            MsgBox "Item possui pend�ncia na F�brica para Outros Dep�sitos.", , MSG_TIT
            cmdCancela_Click
            txtCOD_DPK.SetFocus
            Screen.MousePointer = 0
            Exit Sub
        End If

        Screen.MousePointer = 0
    
        fraNovo.Visible = True
        txtCOD_FORNEC_Novo.SetFocus
         
    End If
    
End Sub

Private Sub txtCOD_EDI_Novo_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub txtCOD_EDI_Novo_LostFocus()
    Dim lsCOD_EDI As String
    
    If Trim(txtCOD_EDI_Novo.Text) = "" Then
       Exit Sub
    End If

    lsCOD_EDI = CONS_EDI(txtCOD_FORNEC_Novo.Text, txtCOD_EDI_Novo.Text)
    If lsCOD_EDI <> "" Then
        MsgBox "Fornecedor/F�brica EDI j� Cadastrado com DPK " & lsCOD_EDI, , MSG_TIT
        txtCOD_EDI_Novo.SetFocus
        Exit Sub
    End If

    Screen.MousePointer = 0
End Sub

Private Sub txtCOD_FABRIC_DblClick()

    If txtCOD_FORNEC.Text = "" Then
        Exit Sub
    ElseIf txtCOD_FABRIC.Text <> "" And InStr(txtCOD_FABRIC.Text, "%") = 0 Then
        Exit Sub
    End If

    Screen.MousePointer = 11
    
    '''''''''''
    
    Set OraParameters = vBanco.Parameters
    
    Criar_Cursor OraParameters, "vCursor"
    
    OraParameters.Remove "COD_FORNEC":     OraParameters.Add "COD_FORNEC", Trim(txtCOD_FORNEC.Text), 1
    If InStr(txtCOD_FABRIC.Text, "%") <> 0 Then
      OraParameters.Remove "COD_FABRICA":    OraParameters.Add "COD_FABRICA", Trim(txtCOD_FABRIC.Text), 1
    Else
      OraParameters.Remove "COD_FABRICA":    OraParameters.Add "COD_FABRICA", Null, 1
    End If
    OraParameters.Remove "COD_LOJA":       OraParameters.Add "COD_LOJA", vCd, 1
    OraParameters.Remove "ERRO":           OraParameters.Add "ERRO", 0, 2
    
    vSql = "Producao.PCK_CAD120.PR_FABRICA(:COD_FORNEC, :COD_FABRICA, :COD_LOJA, :ERRO, :vCursor)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
        Exit Sub
    Else
        Set vObjOracle = OraParameters("vCursor").Value
    End If
    
       
    '''''''''''
    
    Call CONS_GRID(300, 400, 7000, 3100, 3, "1900,3990,800", _
                   "C�d.F�brica,Descri��o,C�d.DPK", vObjOracle, frmGrid_Cons.grid_Padrao, 2)

    Screen.MousePointer = 0
    
    frmGrid_Cons.Height = 4845
    frmGrid_Cons.Width = 7305
    frmGrid_Cons.Top = 1905
    frmGrid_Cons.Left = 720
    frmGrid_Cons.Caption = "Consulta Item"
    frmGrid_Cons.Show vbModal

    If psRet_Grid <> "" Then
        txtCOD_DPK.Text = psRet_Grid
    End If
    txtCOD_DPK_LostFocus
    txtCOD_DPK.SetFocus

End Sub

Private Sub txtCOD_FABRIC_LostFocus()
    If (Trim(txtCOD_FABRIC.Text) <> "" And InStr(txtCOD_FABRIC.Text, "%") = 0) And _
       Trim(txtCOD_FORNEC.Text) <> "" Then

        txtCOD_DPK = CONS_FABR(txtCOD_FORNEC.Text, txtCOD_FABRIC.Text, Str(vCd))
        If txtCOD_DPK.Text = "" Then
            MsgBox "Item n�o Cadastrado.", , MSG_TIT
            txtCOD_FABRIC.SetFocus
            Exit Sub
        End If
        txtCOD_DPK_LostFocus

    End If

End Sub

Private Sub txtCOD_FABRIC_Novo_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub txtCOD_FABRIC_Novo_LostFocus()

    Dim Cod_DPK_Novo As String

    If (Trim(txtCOD_FABRIC_Novo.Text) <> "" And InStr(txtCOD_FABRIC_Novo.Text, "%") = 0) And _
       Trim(txtCOD_FORNEC_Novo.Text) <> "" Then

        Cod_DPK_Novo = CONS_FABR(txtCOD_FORNEC_Novo.Text, txtCOD_FABRIC_Novo.Text, Str(vCd))
        If Cod_DPK_Novo <> "" Then
            MsgBox "Item j� Cadastrado.", , MSG_TIT
            txtCOD_FABRIC_Novo.SetFocus
            Exit Sub
        End If
        
        'Verifica se o novo fabrica digitado j� existe na tabela R_FABRICA_DPK (hist�rico de fabricas)
        'Se existir, relaciona com a ITEM_CADASTRO (pelo c�d.dpk) e verifica
        'se o fornecedor do DPK existente na R_FABRICA_DPK � o mesmo fornecedor
        'do novo item que est� sendo substituido
        'Se for, d� mensagem ao usu�rio, dizendo que para este fornecedor j� existiu um
        'item com este mesmo c�d. f�brica que ele est� tentando substituir e portanto
        'agora ele se refere a outro c�digo de f�brica
        
        '''''''''''
        
        Set OraParameters = vBanco.Parameters
        
        Criar_Cursor OraParameters, "vCursor"
        
        OraParameters.Remove "COD_FORN":      OraParameters.Add "COD_FORN", Trim(txtCOD_FORNEC_Novo.Text), 1
        OraParameters.Remove "COD_FABRICA":   OraParameters.Add "COD_FABRICA", Trim(txtCOD_FABRIC_Novo.Text), 1
        OraParameters.Remove "ERRO":          OraParameters.Add "ERRO", 0, 2
        
        vSql = "Producao.PCK_CAD120.PR_CONS_FABR_NOVO(:COD_FORN, :COD_FABRICA, :ERRO, :vCursor)"
        
        If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
            Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
            Exit Sub
        Else
            Set vObjOracle = OraParameters("vCursor").Value
        End If
        
           
        '''''''''''
        
        If Not vObjOracle.EOF Then
            MsgBox "Para este fornecedor, j� existiu um item com este mesmo c�digo de f�brica " & Chr(13) & _
                   "portando ele n�o poder� ser utilizado na substitui��o." & Chr(13) & _
                   "O item deste fornecedor que antes possuia este c�d. de f�brica era o " & vObjOracle!Cod_DPK & Chr(13) & _
                   "e agora ele possui o c�d. f�brica " & vObjOracle!cod_fabrica & ". Favor verificar!", , MSG_TIT
            txtCOD_FABRIC_Novo.SetFocus
            Exit Sub
        End If
        
        'Se estiver tudo ok
        fraDados_Novo.Visible = True
        txtCOD_MASCARADO_Novo.SetFocus
        Screen.MousePointer = 0

    End If
  

End Sub

Private Sub txtCOD_FORNEC_DblClick()
    If txtCOD_FORNEC.Text <> "" Then
        Exit Sub
    End If

    '''''''''''
    
    Set OraParameters = vBanco.Parameters
    
    Criar_Cursor OraParameters, "vCursor"
    
    OraParameters.Remove "ERRO":           OraParameters.Add "ERRO", 0, 2
    
    vSql = "Producao.PCK_CAD120.PR_FORNEC(:ERRO, :vCursor)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
        Exit Sub
    Else
        Set vObjOracle = OraParameters("vCursor").Value
    End If
    
       
    '''''''''''
        
    Call CONS_GRID(300, 400, 3000, 2660, 2, "800,1900", _
                   "C�d.,Sigla", vObjOracle, frmGrid_Cons.grid_Padrao, 0)

    frmGrid_Cons.Height = 4395
    frmGrid_Cons.Width = 3300
    frmGrid_Cons.Top = 1770
    frmGrid_Cons.Left = 2805
    frmGrid_Cons.Caption = "Consulta Fornecedor"
    frmGrid_Cons.Show vbModal
    
    If psRet_Grid <> "" Then
        txtCOD_FORNEC.Text = psRet_Grid
    End If
    
    txtCOD_FORNEC_LostFocus
    txtCOD_FABRIC.SetFocus
End Sub

Private Sub txtCOD_FORNEC_KeyPress(KeyAscii As Integer)
    If Chr(KeyAscii) < "0" Or Chr(KeyAscii) > "9" Then
       KeyAscii = 0
       Beep
    End If
End Sub

Private Sub txtCOD_FORNEC_LostFocus()
    
    If Trim(txtCOD_FORNEC.Text) <> "" Then

        lsRET_FUNC = CONS_FORN(txtCOD_FORNEC.Text)
        If lsRET_FUNC = "" Then
            txtCOD_FORNEC.SetFocus
            Exit Sub
        ElseIf Mid(lsRET_FUNC, 1, 1) <> 0 Then
            MsgBox "Fornecedor Desativado/Fora de Linha.", , MSG_TIT
            txtCOD_FORNEC.SetFocus
            Exit Sub
        End If
        lblSIGLA_FORN.Caption = Mid(lsRET_FUNC, 3, 10)
        lsFL_EDI = Mid(lsRET_FUNC, 14, 1)
        psFORN_EDI = lsFL_EDI
        psFORN_CLASS = Mid(lsRET_FUNC, 16, 1)
    End If

End Sub



Private Sub txtCOD_FORNEC_Novo_DblClick()
    If txtCOD_FORNEC_Novo.Text <> "" Then
        Exit Sub
    End If

    '''''''''''
    
    Set OraParameters = vBanco.Parameters
    
    Criar_Cursor OraParameters, "vCursor"
    
    OraParameters.Remove "ERRO":           OraParameters.Add "ERRO", 0, 2
    
    vSql = "Producao.PCK_CAD120.PR_FORNEC(:ERRO, :vCursor)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
        Exit Sub
    Else
        Set vObjOracle = OraParameters("vCursor").Value
    End If
    
       
    '''''''''''
        
    Call CONS_GRID(300, 400, 3000, 2660, 2, "800,1900", _
                   "C�d.,Sigla", vObjOracle, frmGrid_Cons.grid_Padrao, 0)

    frmGrid_Cons.Height = 4395
    frmGrid_Cons.Width = 3300
    frmGrid_Cons.Top = 1770
    frmGrid_Cons.Left = 2805
    frmGrid_Cons.Caption = "Consulta Fornecedor"
    frmGrid_Cons.Show vbModal
    
    If psRet_Grid <> "" Then
        txtCOD_FORNEC_Novo.Text = psRet_Grid
    End If
    
    txtCOD_FORNEC_Novo_LostFocus
    txtCOD_FABRIC_Novo.SetFocus
End Sub

Private Sub txtCOD_FORNEC_Novo_LostFocus()
    
    Dim lsFL_EDI As String
    
    If Trim(txtCOD_FORNEC_Novo.Text) <> "" Then

        lsRET_FUNC = CONS_FORN(txtCOD_FORNEC_Novo.Text)
        If lsRET_FUNC = "" Then
            txtCOD_FORNEC_Novo.SetFocus
            Exit Sub
        ElseIf Mid(lsRET_FUNC, 1, 1) <> 0 Then
            MsgBox "Fornecedor Desativado/Fora de Linha.", , MSG_TIT
            txtCOD_FORNEC_Novo.SetFocus
            Exit Sub
        End If
        lblSIGLA_FORN_Novo.Caption = Mid(lsRET_FUNC, 3, 10)
    End If
    
    lsFL_EDI = Mid(lsRET_FUNC, 14, 1)
    psFORN_EDI = lsFL_EDI
    psFORN_CLASS = Mid(lsRET_FUNC, 16, 1)
    
    If lsFL_EDI <> "S" Then
        lblCOD_EDI_Novo.Visible = False
        txtCOD_EDI_Novo.Visible = False
    Else
        lblCOD_EDI_Novo.Visible = True
        txtCOD_EDI_Novo.Visible = True
    End If
    
  
End Sub



Private Sub txtCOD_MASCARADO_Novo_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub


Private Sub txtSEQ_TAB_Novo_DblClick()

    '''''''''''
    
    Set OraParameters = vBanco.Parameters
    
    Criar_Cursor OraParameters, "vCursor"
    
    OraParameters.Remove "COD_FORN":       OraParameters.Add "COD_FORN", txtCOD_FORNEC_Novo.Text, 1
    OraParameters.Remove "COD_LOJA":       OraParameters.Add "COD_LOJA", vCd, 1
    OraParameters.Remove "ERRO":           OraParameters.Add "ERRO", 0, 2

    
    vSql = "Producao.PCK_CAD120.PR_CONS_SEQ(:COD_FORN, :COD_LOJA, :ERRO, :vCursor)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
        Exit Sub
    Else
        Set vObjOracle = OraParameters("vCursor").Value
    End If
    
       
    '''''''''''
    
        
    Call CONS_GRID(300, 300, 3300, 2860, 2, "2000,1000", _
                   "C�d.F�brica,Sequ�ncia", vObjOracle, frmGrid_Cons.grid_Padrao, 0)

    frmGrid_Cons.Height = 4600
    frmGrid_Cons.Width = 3550
    frmGrid_Cons.Top = 1700
    frmGrid_Cons.Left = 2805
    frmGrid_Cons.Caption = "Consulta Sequ�ncia"
    frmGrid_Cons.Show vbModal
    
    txtSEQ_TAB_Novo.SetFocus

End Sub

Private Sub txtSEQ_TAB_Novo_KeyPress(KeyAscii As Integer)
    If Chr(KeyAscii) < "0" Or Chr(KeyAscii) > "9" Then
       KeyAscii = 0
       Beep
    End If
End Sub
