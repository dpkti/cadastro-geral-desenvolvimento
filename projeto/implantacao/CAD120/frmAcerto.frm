VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form frmAcerto 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Acerto Item"
   ClientHeight    =   5685
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8250
   Icon            =   "frmAcerto.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   379
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   550
   Begin VB.Frame fraNovo 
      Caption         =   "Item Novo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1725
      Left            =   90
      TabIndex        =   6
      Top             =   3510
      Visible         =   0   'False
      Width           =   8070
      Begin VB.TextBox txtCOD_MASCARADO 
         Height          =   315
         Left            =   5280
         Locked          =   -1  'True
         TabIndex        =   30
         TabStop         =   0   'False
         Top             =   1170
         Width           =   2500
      End
      Begin VB.TextBox txtCOD_FABRIC_Novo 
         Height          =   315
         Left            =   5280
         TabIndex        =   29
         Top             =   765
         Width           =   2500
      End
      Begin VB.TextBox txtCOD_FORNEC_Novo 
         Height          =   315
         Left            =   1095
         TabIndex        =   28
         Top             =   765
         Width           =   700
      End
      Begin VB.TextBox txtDESC_ITEM_Novo 
         Height          =   315
         Left            =   2355
         Locked          =   -1  'True
         TabIndex        =   27
         TabStop         =   0   'False
         Top             =   360
         Width           =   5415
      End
      Begin VB.TextBox txtCOD_DPK_Novo 
         Height          =   315
         Left            =   1095
         TabIndex        =   26
         Top             =   360
         Width           =   1140
      End
      Begin VB.Label lblSIGLA_FORN_Novo 
         Height          =   195
         Left            =   1890
         TabIndex        =   32
         Top             =   825
         Width           =   2160
      End
      Begin VB.Label lblCodMascarado 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "C�d. Mascarado"
         Height          =   195
         Left            =   3975
         TabIndex        =   25
         Top             =   1230
         Width           =   1170
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "C�d. Fabrica"
         Height          =   195
         Left            =   4245
         TabIndex        =   24
         Top             =   825
         Width           =   900
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Fornecedor"
         Height          =   195
         Left            =   135
         TabIndex        =   23
         Top             =   825
         Width           =   810
      End
      Begin VB.Label lblCOD_DPK_Novo 
         AutoSize        =   -1  'True
         Caption         =   "C�d. DPK"
         Height          =   195
         Left            =   240
         TabIndex        =   22
         Top             =   420
         Width           =   705
      End
   End
   Begin VB.Frame fraAntigo 
      Caption         =   "Item Antigo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2535
      Left            =   90
      TabIndex        =   5
      Top             =   945
      Width           =   8070
      Begin VB.TextBox txtSUBGP 
         Height          =   315
         Left            =   5310
         Locked          =   -1  'True
         TabIndex        =   21
         TabStop         =   0   'False
         Top             =   1980
         Width           =   2500
      End
      Begin VB.TextBox txtGRP 
         Height          =   315
         Left            =   5310
         Locked          =   -1  'True
         TabIndex        =   20
         TabStop         =   0   'False
         Top             =   1575
         Width           =   2500
      End
      Begin VB.TextBox txtLIN 
         Height          =   315
         Left            =   1125
         Locked          =   -1  'True
         TabIndex        =   19
         TabStop         =   0   'False
         Top             =   1575
         Width           =   2265
      End
      Begin VB.TextBox txtCOD_FABRIC 
         Height          =   315
         Left            =   5310
         TabIndex        =   18
         Top             =   1170
         Width           =   2500
      End
      Begin VB.TextBox txtCOD_FORNEC 
         Height          =   315
         Left            =   1125
         TabIndex        =   17
         Top             =   1170
         Width           =   700
      End
      Begin VB.TextBox txtDESC_ITEM 
         Height          =   315
         Left            =   2385
         Locked          =   -1  'True
         TabIndex        =   16
         TabStop         =   0   'False
         Top             =   765
         Width           =   5415
      End
      Begin VB.TextBox txtCOD_DPK 
         Height          =   315
         Left            =   1125
         TabIndex        =   15
         Top             =   765
         Width           =   1140
      End
      Begin VB.ComboBox cboFILIAL 
         Height          =   315
         Left            =   1125
         Style           =   2  'Dropdown List
         TabIndex        =   14
         Top             =   360
         Width           =   2265
      End
      Begin VB.Label lblSIGLA_FORN 
         Height          =   195
         Left            =   1980
         TabIndex        =   31
         Top             =   1230
         Width           =   2160
      End
      Begin VB.Label lblSubGrupo 
         AutoSize        =   -1  'True
         Caption         =   "Subgrupo"
         Height          =   195
         Left            =   4485
         TabIndex        =   13
         Top             =   2040
         Width           =   690
      End
      Begin VB.Label lblGrupo 
         AutoSize        =   -1  'True
         Caption         =   "Grupo"
         Height          =   195
         Left            =   4740
         TabIndex        =   12
         Top             =   1635
         Width           =   435
      End
      Begin VB.Label lblCodFabrica 
         AutoSize        =   -1  'True
         Caption         =   "C�d. Fabrica"
         Height          =   195
         Left            =   4275
         TabIndex        =   11
         Top             =   1230
         Width           =   900
      End
      Begin VB.Label lblLinha 
         AutoSize        =   -1  'True
         Caption         =   "Linha"
         Height          =   195
         Left            =   585
         TabIndex        =   10
         Top             =   1635
         Width           =   390
      End
      Begin VB.Label lblFornec 
         AutoSize        =   -1  'True
         Caption         =   "Fornecedor"
         Height          =   195
         Left            =   165
         TabIndex        =   9
         Top             =   1230
         Width           =   810
      End
      Begin VB.Label lblCodDPK 
         AutoSize        =   -1  'True
         Caption         =   "C�d. DPK"
         Height          =   195
         Left            =   270
         TabIndex        =   8
         Top             =   825
         Width           =   705
      End
      Begin VB.Label lblDeposito 
         AutoSize        =   -1  'True
         Caption         =   "Dep�sito"
         Height          =   195
         Left            =   345
         TabIndex        =   7
         Top             =   420
         Width           =   630
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   5355
      Width           =   8250
      _ExtentX        =   14552
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   14049
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      Top             =   810
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmAcerto.frx":23D2
      PICN            =   "frmAcerto.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdOK 
      Height          =   690
      Left            =   810
      TabIndex        =   3
      TabStop         =   0   'False
      ToolTipText     =   "Confimar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmAcerto.frx":30C8
      PICN            =   "frmAcerto.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdCancela 
      Height          =   690
      Left            =   1575
      TabIndex        =   4
      TabStop         =   0   'False
      ToolTipText     =   "Cancelar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmAcerto.frx":3DBE
      PICN            =   "frmAcerto.frx":3DDA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmAcerto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim lsFL_EDI As String
Dim psFORN_EDI As String
Dim psFORN_CLASS As String



Private Sub cmdCancela_Click()
    txtCOD_DPK.Text = ""
    txtCOD_FORNEC.Text = ""
    txtCOD_FABRIC.Text = ""
    lblSIGLA_FORN.Caption = ""
    txtDESC_ITEM.Text = ""
    txtLIN.Text = ""
    txtGRP.Text = ""
    txtSUBGP.Text = ""
    txtCOD_DPK_Novo.Text = ""
    txtCOD_FORNEC_Novo.Text = ""
    lblSIGLA_FORN_Novo.Caption = ""
    txtCOD_FABRIC_Novo.Text = ""
    txtDESC_ITEM_Novo.Text = ""
    txtCOD_MASCARADO.Text = ""
    fraNovo.Visible = False
    txtCOD_DPK.SetFocus
End Sub

Private Sub cmdOK_Click()
    If Trim(txtCOD_DPK.Text) = "" Then
        MsgBox "Entre com o Item Antigo.", , MSG_TIT
        txtCOD_DPK.SetFocus
        Exit Sub
    ElseIf Trim(txtCOD_DPK_Novo.Text) = "" Then
        MsgBox "Entre com o Item Novo.", , MSG_TIT
        txtCOD_DPK_Novo.SetFocus
        Exit Sub
    End If
  
    If vVB_Generica_001.Perguntar("Confirma Acerto?") = vbYes Then
        If Not Bloco_Acerto(CLng(txtCOD_DPK.Text), CLng(txtCOD_DPK_Novo.Text), psFORN_CLASS, psFORN_EDI) Then
            Exit Sub
        End If
        
        Dim vCodDPKAnterior As Long
        Dim vCodDPKAtual As Long
        
        vCodDPKAnterior = CLng(Trim(txtCOD_DPK.Text))
        vCodDPKAtual = CLng(Trim(txtCOD_DPK_Novo.Text))

        If Not Acerta_Equivalencias(vCodDPKAnterior, vCodDPKAtual) Then
            Exit Sub
        End If

        If Not Acerta_DtPriEnt_Acerto(vCodDPKAnterior, vCodDPKAtual) Then
            Exit Sub
        End If

        If Not Acerta_Fotos(vCodDPKAnterior, vCodDPKAtual) Then
            Exit Sub
        End If
        
        vVB_Generica_001.Informar ("Acerto de Item OK!")
        
        cmdCancela_Click
        
    End If
End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    
    Me.Top = 0
    Me.Left = 0
    
    cboFILIAL.Clear
    cboFILIAL.AddItem "99 - TODOS"
    cboFILIAL.ListIndex = 0
      
    'vFl_BloqAcerto = False
    
End Sub

Private Sub txtCOD_DPK_KeyPress(KeyAscii As Integer)
    If Chr(KeyAscii) < "0" Or Chr(KeyAscii) > "9" Then
       KeyAscii = 0
       Beep
    End If
End Sub

Private Sub txtCOD_DPK_LostFocus()

    Dim ldQTD_ATUAL As Double
    Dim ldQTD_RESERV As Double
    Dim lsPED_COM As String
    
    If Trim(txtCOD_DPK) <> "" Then

        lsRET_FUNC = CONS_DPK(txtCOD_DPK, Str(vCd))
        If lsRET_FUNC = "" Then
            cmdCancela_Click
            txtCOD_DPK.SetFocus
            Exit Sub
        ElseIf Mid(lsRET_FUNC, 1, 1) <> 0 Then
            MsgBox "Item Desativado/Fora de Linha.", , MSG_TIT
            cmdCancela_Click
            txtCOD_DPK.SetFocus
            Exit Sub
        End If
        txtCOD_FABRIC.Text = Mid(lsRET_FUNC, 7, 19)
        txtDESC_ITEM.Text = Mid(lsRET_FUNC, 27, 40)
        txtLIN.Text = Mid(lsRET_FUNC, 68, 20)
        txtGRP.Text = Mid(lsRET_FUNC, 89, 20)
        txtSUBGP.Text = Mid(lsRET_FUNC, 110, 20)
        ldQTD_ATUAL = CDbl(Mid(lsRET_FUNC, 131, 10))
        ldQTD_RESERV = CDbl(Mid(lsRET_FUNC, 142, 10))
        lsPED_COM = Mid(lsRET_FUNC, 153, 1)

        txtCOD_FORNEC.Text = Mid(lsRET_FUNC, 3, 3)
        txtCOD_FORNEC_LostFocus

        If ldQTD_ATUAL <> 0 Or ldQTD_RESERV <> 0 Then
            MsgBox "Item com saldo em Estoque ou Reservado.", , MSG_TIT
            cmdCancela_Click
            txtCOD_DPK.SetFocus
            Exit Sub
        ElseIf lsPED_COM = "S" Then
            MsgBox "Item possui pend�ncia na F�brica. Imposs�vel fazer Acerto.", , MSG_TIT
            cmdCancela_Click
            txtCOD_DPK.SetFocus
            Exit Sub
        End If

        Screen.MousePointer = 11
        
        '//////////////////////////////////////
        '///  VERIFICA OS OUTROS DEPOSITOS  ///
        '//////////////////////////////////////
        
        Set OraParameters = vBanco.Parameters
        
        Criar_Cursor OraParameters, "vCursor"
        
        OraParameters.Remove "COD_DPK":     OraParameters.Add "COD_DPK", Trim(txtCOD_DPK.Text), 1
        OraParameters.Remove "TOT":         OraParameters.Add "TOT", 0, 2
        OraParameters.Remove "PED":         OraParameters.Add "PED", 0, 2
        OraParameters.Remove "ERRO":        OraParameters.Add "ERRO", 0, 2
        
        vSql = "Producao.PCK_CAD120.PR_CONS_DPK_DEP(:COD_DPK, :TOT, :PED, :ERRO)"
        
        If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
            Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
            Exit Sub
        End If
    
        If Val(OraParameters("TOT").Value) > 0 Then
            MsgBox "Item com saldo em Estoque ou Reservado para Outros Dep�sitos.", vbCritical, MSG_TIT
            cmdCancela_Click
            txtCOD_DPK.SetFocus
            Screen.MousePointer = 0
            Exit Sub
        End If
                        
        If Val(OraParameters("PED").Value) > 0 Then
            MsgBox "Item possui pend�ncia na F�brica para Outros Dep�sitos.", , MSG_TIT
            cmdCancela_Click
            txtCOD_DPK.SetFocus
            Screen.MousePointer = 0
            Exit Sub
        End If

        Screen.MousePointer = 0
    
        fraNovo.Visible = True
        txtCOD_DPK_Novo.SetFocus
         
    End If
    
    
End Sub




Private Sub txtCOD_DPK_Novo_KeyPress(KeyAscii As Integer)
    If Chr(KeyAscii) < "0" Or Chr(KeyAscii) > "9" Then
       KeyAscii = 0
       Beep
    End If
End Sub

Private Sub txtCOD_DPK_Novo_LostFocus()
    If Trim(txtCOD_DPK_Novo.Text) <> "" Then

        lsRET_FUNC = CONS_DPK(txtCOD_DPK_Novo.Text, Str(vCd))
        Screen.MousePointer = 0
        If lsRET_FUNC = "" Then
            txtCOD_DPK_Novo.SetFocus
            Exit Sub
        ElseIf Mid(lsRET_FUNC, 1, 1) <> 0 Then
            MsgBox "Item Desativado/Fora de Linha.", , MSG_TIT
            txtCOD_DPK_Novo.SetFocus
            Exit Sub
        End If
        txtCOD_FABRIC_Novo.Text = Mid(lsRET_FUNC, 7, 19)
        txtDESC_ITEM_Novo.Text = Mid(lsRET_FUNC, 27, 40)
        txtCOD_MASCARADO.Text = Mid(lsRET_FUNC, 155, 19)

        txtCOD_FORNEC_Novo.Text = Mid(lsRET_FUNC, 3, 3)
        'txtCOD_FORNEC_Novo_LostFocus

        If Trim(txtCOD_DPK_Novo.Text) = Trim(txtCOD_DPK.Text) Then
            MsgBox "Item igual ao Antigo. Imposs�vel fazer Acerto.", , MSG_TIT
            GoTo limpa_var
        End If

    End If

    Exit Sub

limpa_var:

txtCOD_DPK_Novo.Text = ""
txtCOD_FORNEC_Novo.Text = ""
lblSIGLA_FORN_Novo.Caption = ""
txtCOD_FABRIC_Novo.Text = ""
txtDESC_ITEM_Novo.Text = ""
txtCOD_MASCARADO.Text = ""
txtCOD_DPK_Novo.SetFocus


End Sub

Private Sub txtCOD_FABRIC_DblClick()

    If txtCOD_FORNEC.Text = "" Then
        Exit Sub
    ElseIf txtCOD_FABRIC.Text <> "" And InStr(txtCOD_FABRIC.Text, "%") = 0 Then
        Exit Sub
    End If

    Screen.MousePointer = 11
    
    '''''''''''
    
    Set OraParameters = vBanco.Parameters
    
    Criar_Cursor OraParameters, "vCursor"
    
    OraParameters.Remove "COD_FORNEC":     OraParameters.Add "COD_FORNEC", Trim(txtCOD_FORNEC.Text), 1
    If InStr(txtCOD_FABRIC.Text, "%") <> 0 Then
      OraParameters.Remove "COD_FABRICA":    OraParameters.Add "COD_FABRICA", Trim(txtCOD_FABRIC.Text), 1
    Else
      OraParameters.Remove "COD_FABRICA":    OraParameters.Add "COD_FABRICA", Null, 1
    End If
    OraParameters.Remove "COD_LOJA":       OraParameters.Add "COD_LOJA", vCd, 1
    OraParameters.Remove "ERRO":           OraParameters.Add "ERRO", 0, 2
    
    vSql = "Producao.PCK_CAD120.PR_FABRICA(:COD_FORNEC, :COD_FABRICA, :COD_LOJA, :ERRO, :vCursor)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
        Exit Sub
    Else
        Set vObjOracle = OraParameters("vCursor").Value
    End If
    
       
    '''''''''''
    
    Call CONS_GRID(300, 400, 7000, 3100, 3, "1900,3990,800", _
                   "C�d.F�brica,Descri��o,C�d.DPK", vObjOracle, frmGrid_Cons.grid_Padrao, 2)

    Screen.MousePointer = 0
    
    frmGrid_Cons.Height = 4845
    frmGrid_Cons.Width = 7305
    frmGrid_Cons.Top = 1905
    frmGrid_Cons.Left = 720
    frmGrid_Cons.Caption = "Consulta Item"
    frmGrid_Cons.Show vbModal

    If psRet_Grid <> "" Then
        txtCOD_DPK.Text = psRet_Grid
    End If
    txtCOD_DPK_LostFocus
    txtCOD_DPK.SetFocus

End Sub

Private Sub txtCOD_FABRIC_LostFocus()
    If (Trim(txtCOD_FABRIC.Text) <> "" And InStr(txtCOD_FABRIC.Text, "%") = 0) And _
       Trim(txtCOD_FORNEC.Text) <> "" Then

        txtCOD_DPK = CONS_FABR(txtCOD_FORNEC.Text, txtCOD_FABRIC.Text, Str(vCd))
        If txtCOD_DPK.Text = "" Then
            MsgBox "Item n�o Cadastrado.", , MSG_TIT
            txtCOD_FABRIC.SetFocus
            Exit Sub
        End If
        txtCOD_DPK_LostFocus

    End If

End Sub



Private Sub txtCOD_FABRIC_Novo_DblClick()

    If txtCOD_FORNEC_Novo.Text = "" Then
        Exit Sub
    ElseIf txtCOD_FABRIC_Novo.Text <> "" And InStr(txtCOD_FABRIC_Novo.Text, "%") = 0 Then
        Exit Sub
    End If

    Screen.MousePointer = 11
    
    '''''''''''
    
    Set OraParameters = vBanco.Parameters
    
    Criar_Cursor OraParameters, "vCursor"
    
    OraParameters.Remove "COD_FORNEC":     OraParameters.Add "COD_FORNEC", Trim(txtCOD_FORNEC_Novo.Text), 1
    If InStr(txtCOD_FABRIC_Novo.Text, "%") <> 0 Then
      OraParameters.Remove "COD_FABRICA":    OraParameters.Add "COD_FABRICA", Trim(txtCOD_FABRIC_Novo.Text), 1
    Else
      OraParameters.Remove "COD_FABRICA":    OraParameters.Add "COD_FABRICA", Null, 1
    End If
    OraParameters.Remove "COD_LOJA":       OraParameters.Add "COD_LOJA", vCd, 1
    OraParameters.Remove "ERRO":           OraParameters.Add "ERRO", 0, 2
    
    vSql = "Producao.PCK_CAD120.PR_FABRICA(:COD_FORNEC, :COD_FABRICA, :COD_LOJA, :ERRO, :vCursor)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
        Exit Sub
    Else
        Set vObjOracle = OraParameters("vCursor").Value
    End If
    
       
    '''''''''''
    
    Call CONS_GRID(300, 400, 7000, 3100, 3, "1900,3990,800", _
                   "C�d.F�brica,Descri��o,C�d.DPK", vObjOracle, frmGrid_Cons.grid_Padrao, 2)

    Screen.MousePointer = 0
    
    frmGrid_Cons.Height = 4845
    frmGrid_Cons.Width = 7305
    frmGrid_Cons.Top = 1905
    frmGrid_Cons.Left = 720
    frmGrid_Cons.Caption = "Consulta Item"
    frmGrid_Cons.Show vbModal

    If psRet_Grid <> "" Then
        txtCOD_DPK_Novo.Text = psRet_Grid
    End If
    txtCOD_DPK_Novo_LostFocus
    txtCOD_DPK_Novo.SetFocus

End Sub

Private Sub txtCOD_FABRIC_Novo_LostFocus()
    If (Trim(txtCOD_FABRIC_Novo.Text) <> "" And InStr(txtCOD_FABRIC_Novo.Text, "%") = 0) And _
       Trim(txtCOD_FORNEC_Novo.Text) <> "" Then

        txtCOD_DPK_Novo.Text = CONS_FABR(txtCOD_FORNEC_Novo.Text, txtCOD_FABRIC_Novo.Text, Str(vCd))
        If txtCOD_DPK_Novo.Text = "" Then
            MsgBox "Item n�o Cadastrado.", , MSG_TIT
            txtCOD_FABRIC_Novo.SetFocus
            Exit Sub
        End If
        txtCOD_DPK_Novo_LostFocus

    End If
End Sub

Private Sub txtCOD_FORNEC_DblClick()
    If txtCOD_FORNEC.Text <> "" Then
        Exit Sub
    End If

    '''''''''''
    
    Set OraParameters = vBanco.Parameters
    
    Criar_Cursor OraParameters, "vCursor"
    
    OraParameters.Remove "ERRO":           OraParameters.Add "ERRO", 0, 2
    
    vSql = "Producao.PCK_CAD120.PR_FORNEC(:ERRO, :vCursor)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
        Exit Sub
    Else
        Set vObjOracle = OraParameters("vCursor").Value
    End If
    
       
    '''''''''''
        
    Call CONS_GRID(300, 400, 3000, 2660, 2, "800,1900", _
                   "C�d.,Sigla", vObjOracle, frmGrid_Cons.grid_Padrao, 0)

    frmGrid_Cons.Height = 4395
    frmGrid_Cons.Width = 3300
    frmGrid_Cons.Top = 1770
    frmGrid_Cons.Left = 2805
    frmGrid_Cons.Caption = "Consulta Fornecedor"
    frmGrid_Cons.Show vbModal
    
    If psRet_Grid <> "" Then
        txtCOD_FORNEC.Text = psRet_Grid
    End If
    
    txtCOD_FORNEC_LostFocus
    txtCOD_FABRIC.SetFocus
End Sub

Private Sub txtCOD_FORNEC_KeyPress(KeyAscii As Integer)
    If Chr(KeyAscii) < "0" Or Chr(KeyAscii) > "9" Then
       KeyAscii = 0
       Beep
    End If
End Sub

Private Sub txtCOD_FORNEC_LostFocus()
    
    If Trim(txtCOD_FORNEC.Text) <> "" Then

        lsRET_FUNC = CONS_FORN(txtCOD_FORNEC.Text)
        If lsRET_FUNC = "" Then
            txtCOD_FORNEC.SetFocus
            Exit Sub
        ElseIf Mid(lsRET_FUNC, 1, 1) <> 0 Then
            MsgBox "Fornecedor Desativado/Fora de Linha.", , MSG_TIT
            txtCOD_FORNEC.SetFocus
            Exit Sub
        End If
        lblSIGLA_FORN.Caption = Mid(lsRET_FUNC, 3, 10)
        lsFL_EDI = Mid(lsRET_FUNC, 14, 1)
        psFORN_EDI = lsFL_EDI
        psFORN_CLASS = Mid(lsRET_FUNC, 16, 1)
    End If

End Sub


Private Sub txtCOD_FORNEC_Novo_DblClick()
    If txtCOD_FORNEC_Novo.Text <> "" Then
        Exit Sub
    End If

    '''''''''''
    
    Set OraParameters = vBanco.Parameters
    
    Criar_Cursor OraParameters, "vCursor"
    
    OraParameters.Remove "ERRO":           OraParameters.Add "ERRO", 0, 2
    
    vSql = "Producao.PCK_CAD120.PR_FORNEC(:ERRO, :vCursor)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
        Exit Sub
    Else
        Set vObjOracle = OraParameters("vCursor").Value
    End If
    
       
    '''''''''''
        
    Call CONS_GRID(300, 400, 3000, 2660, 2, "800,1900", _
                   "C�d.,Sigla", vObjOracle, frmGrid_Cons.grid_Padrao, 0)

    frmGrid_Cons.Height = 4395
    frmGrid_Cons.Width = 3300
    frmGrid_Cons.Top = 1770
    frmGrid_Cons.Left = 2805
    frmGrid_Cons.Caption = "Consulta Fornecedor"
    frmGrid_Cons.Show vbModal
    
    If psRet_Grid <> "" Then
        txtCOD_FORNEC_Novo.Text = psRet_Grid
    End If
    
    txtCOD_FORNEC_Novo_LostFocus
    txtCOD_FABRIC_Novo.SetFocus
End Sub

Private Sub txtCOD_FORNEC_Novo_KeyPress(KeyAscii As Integer)
    If Chr(KeyAscii) < "0" Or Chr(KeyAscii) > "9" Then
       KeyAscii = 0
       Beep
    End If
End Sub

Private Sub txtCOD_FORNEC_Novo_LostFocus()
    
    If Trim(txtCOD_FORNEC_Novo.Text) <> "" Then

        lsRET_FUNC = CONS_FORN(txtCOD_FORNEC_Novo.Text)
        If lsRET_FUNC = "" Then
            txtCOD_FORNEC_Novo.SetFocus
            Exit Sub
        ElseIf Mid(lsRET_FUNC, 1, 1) <> 0 Then
            MsgBox "Fornecedor Desativado/Fora de Linha.", , MSG_TIT
            txtCOD_FORNEC_Novo.SetFocus
            Exit Sub
        End If
        lblSIGLA_FORN_Novo.Caption = Mid(lsRET_FUNC, 3, 10)
    End If
End Sub
