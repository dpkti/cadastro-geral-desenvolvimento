VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form frmLimite 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cadastro de Limite de Cr�dito"
   ClientHeight    =   6285
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7905
   Icon            =   "frmLimite.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   419
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   527
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   5955
      Width           =   7905
      _ExtentX        =   13944
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   13891
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      Top             =   810
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmLimite.frx":23D2
      PICN            =   "frmLimite.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin TabDlg.SSTab SSTabCadLim 
      Height          =   5010
      Left            =   90
      TabIndex        =   3
      Top             =   900
      Width           =   7755
      _ExtentX        =   13679
      _ExtentY        =   8837
      _Version        =   393216
      Tabs            =   4
      Tab             =   2
      TabsPerRow      =   4
      TabHeight       =   794
      ShowFocusRect   =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Consultar"
      TabPicture(0)   =   "frmLimite.frx":30C8
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "mfgConsultar"
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Alterar"
      TabPicture(1)   =   "frmLimite.frx":30E4
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "lblNota(1)"
      Tab(1).Control(1)=   "lblInicial(1)"
      Tab(1).Control(2)=   "lblFinal(1)"
      Tab(1).Control(3)=   "lblLim(1)"
      Tab(1).Control(4)=   "mfgAlterar"
      Tab(1).Control(5)=   "CmdGravarA"
      Tab(1).Control(6)=   "cmdCancelA"
      Tab(1).Control(7)=   "txtInicA"
      Tab(1).Control(8)=   "txtFinalA"
      Tab(1).Control(9)=   "txtLimiteA"
      Tab(1).Control(10)=   "txtNotaA"
      Tab(1).ControlCount=   11
      TabCaption(2)   =   "Incluir"
      TabPicture(2)   =   "frmLimite.frx":3100
      Tab(2).ControlEnabled=   -1  'True
      Tab(2).Control(0)=   "lblLim(0)"
      Tab(2).Control(0).Enabled=   0   'False
      Tab(2).Control(1)=   "lblFinal(0)"
      Tab(2).Control(1).Enabled=   0   'False
      Tab(2).Control(2)=   "lblInicial(0)"
      Tab(2).Control(2).Enabled=   0   'False
      Tab(2).Control(3)=   "lblNota(0)"
      Tab(2).Control(3).Enabled=   0   'False
      Tab(2).Control(4)=   "CmdGravarI"
      Tab(2).Control(4).Enabled=   0   'False
      Tab(2).Control(5)=   "cmdCancelI"
      Tab(2).Control(5).Enabled=   0   'False
      Tab(2).Control(6)=   "mfgIncluir"
      Tab(2).Control(6).Enabled=   0   'False
      Tab(2).Control(7)=   "txtNotaI"
      Tab(2).Control(7).Enabled=   0   'False
      Tab(2).Control(8)=   "txtLimiteI"
      Tab(2).Control(8).Enabled=   0   'False
      Tab(2).Control(9)=   "txtFinalI"
      Tab(2).Control(9).Enabled=   0   'False
      Tab(2).Control(10)=   "txtInicI"
      Tab(2).Control(10).Enabled=   0   'False
      Tab(2).ControlCount=   11
      TabCaption(3)   =   "Excluir"
      TabPicture(3)   =   "frmLimite.frx":311C
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "Label4"
      Tab(3).Control(1)=   "mfgExcluir"
      Tab(3).ControlCount=   2
      Begin VB.TextBox txtInicA 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   -73650
         MaxLength       =   18
         TabIndex        =   11
         Top             =   4500
         Width           =   1230
      End
      Begin VB.TextBox txtFinalA 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   -72165
         MaxLength       =   18
         TabIndex        =   10
         Top             =   4500
         Width           =   1230
      End
      Begin VB.TextBox txtLimiteA 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   -70680
         MaxLength       =   18
         TabIndex        =   9
         Top             =   4500
         Width           =   1230
      End
      Begin VB.TextBox txtInicI 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1350
         MaxLength       =   18
         TabIndex        =   8
         Top             =   4500
         Width           =   1230
      End
      Begin VB.TextBox txtFinalI 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2835
         MaxLength       =   18
         TabIndex        =   7
         Top             =   4500
         Width           =   1230
      End
      Begin VB.TextBox txtLimiteI 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   4320
         MaxLength       =   18
         TabIndex        =   6
         Top             =   4500
         Width           =   1230
      End
      Begin VB.TextBox txtNotaA 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   -74640
         MaxLength       =   18
         TabIndex        =   5
         Top             =   4500
         Width           =   690
      End
      Begin VB.TextBox txtNotaI 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   360
         MaxLength       =   18
         TabIndex        =   4
         Top             =   4500
         Width           =   690
      End
      Begin MSFlexGridLib.MSFlexGrid mfgConsultar 
         Height          =   3300
         Left            =   -74640
         TabIndex        =   12
         Top             =   810
         Width           =   5415
         _ExtentX        =   9551
         _ExtentY        =   5821
         _Version        =   393216
         Cols            =   5
         BackColorBkg    =   -2147483633
         AllowBigSelection=   0   'False
         FocusRect       =   2
         ScrollBars      =   2
         MergeCells      =   2
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSFlexGridLib.MSFlexGrid mfgIncluir 
         Height          =   3300
         Left            =   360
         TabIndex        =   13
         Top             =   810
         Width           =   5415
         _ExtentX        =   9551
         _ExtentY        =   5821
         _Version        =   393216
         Cols            =   5
         BackColorBkg    =   -2147483633
         FocusRect       =   2
         HighLight       =   0
         ScrollBars      =   2
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSFlexGridLib.MSFlexGrid mfgExcluir 
         Height          =   3300
         Left            =   -74640
         TabIndex        =   14
         Top             =   810
         Width           =   5415
         _ExtentX        =   9551
         _ExtentY        =   5821
         _Version        =   393216
         Cols            =   5
         BackColorBkg    =   -2147483633
         FocusRect       =   2
         HighLight       =   0
         ScrollBars      =   2
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin Bot�o.cmd cmdCancelI 
         Height          =   690
         Left            =   6885
         TabIndex        =   15
         TabStop         =   0   'False
         ToolTipText     =   "Limpar Campos"
         Top             =   4185
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmLimite.frx":3138
         PICN            =   "frmLimite.frx":3154
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd CmdGravarI 
         Height          =   690
         Left            =   6120
         TabIndex        =   16
         TabStop         =   0   'False
         ToolTipText     =   "Gravar"
         Top             =   4185
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmLimite.frx":3E2E
         PICN            =   "frmLimite.frx":3E4A
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdCancelA 
         Height          =   690
         Left            =   -68115
         TabIndex        =   17
         TabStop         =   0   'False
         ToolTipText     =   "Limpar Campos"
         Top             =   4185
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmLimite.frx":4B24
         PICN            =   "frmLimite.frx":4B40
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd CmdGravarA 
         Default         =   -1  'True
         Height          =   690
         Left            =   -68880
         TabIndex        =   18
         TabStop         =   0   'False
         ToolTipText     =   "Gravar"
         Top             =   4185
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmLimite.frx":581A
         PICN            =   "frmLimite.frx":5836
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MSFlexGridLib.MSFlexGrid mfgAlterar 
         Height          =   3300
         Left            =   -74640
         TabIndex        =   19
         Top             =   810
         Width           =   5415
         _ExtentX        =   9551
         _ExtentY        =   5821
         _Version        =   393216
         Cols            =   5
         BackColorBkg    =   -2147483633
         FocusRect       =   2
         HighLight       =   0
         ScrollBars      =   2
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblNota 
         Appearance      =   0  'Flat
         Caption         =   "Nota"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   0
         Left            =   360
         TabIndex        =   28
         Top             =   4230
         Width           =   690
      End
      Begin VB.Label lblInicial 
         Appearance      =   0  'Flat
         Caption         =   "Nota Inicial"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   0
         Left            =   1350
         TabIndex        =   27
         Top             =   4230
         Width           =   1230
      End
      Begin VB.Label lblFinal 
         Appearance      =   0  'Flat
         Caption         =   "Nota Final"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   0
         Left            =   2835
         TabIndex        =   26
         Top             =   4230
         Width           =   1185
      End
      Begin VB.Label lblLim 
         Appearance      =   0  'Flat
         Caption         =   "Limite"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   0
         Left            =   4320
         TabIndex        =   25
         Top             =   4230
         Width           =   690
      End
      Begin VB.Label lblNota 
         Appearance      =   0  'Flat
         Caption         =   "Nota"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   1
         Left            =   -74640
         TabIndex        =   24
         Top             =   4230
         Width           =   690
      End
      Begin VB.Label lblInicial 
         Appearance      =   0  'Flat
         Caption         =   "Nota Inicial"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   1
         Left            =   -73650
         TabIndex        =   23
         Top             =   4230
         Width           =   1230
      End
      Begin VB.Label lblFinal 
         Appearance      =   0  'Flat
         Caption         =   "Nota Final"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   1
         Left            =   -72165
         TabIndex        =   22
         Top             =   4230
         Width           =   1185
      End
      Begin VB.Label lblLim 
         Appearance      =   0  'Flat
         Caption         =   "Limite"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Index           =   1
         Left            =   -70680
         TabIndex        =   21
         Top             =   4230
         Width           =   690
      End
      Begin VB.Label Label4 
         Caption         =   "Duplo click na linha para excluir o Limite de Cr�dito desejado"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   255
         Left            =   -74640
         TabIndex        =   20
         Top             =   4275
         Width           =   5415
      End
   End
End
Attribute VB_Name = "frmLimite"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    
    Me.Top = 0
    Me.Left = 0
    
    'Carregar Grids
    Call Carrega_Grid
    
    'set focus para a primeira tab
    SSTabCadLim.Tab = 0
    
End Sub

Private Sub cmdCancelA_Click()
    
    txtNotaA = ""
    txtInicA = ""
    txtInicA.SetFocus
    txtFinalA = ""
    txtLimiteA = ""
    
End Sub

Private Sub cmdCancelI_Click()
    
    txtNotaI = ""
    txtNotaI.SetFocus
    txtInicI = ""
    txtFinalI = ""
    txtLimiteI = ""
    
End Sub

Private Sub mfgAlterar_Click()

Dim vLinha As Integer
'Dim i As Integer
'Dim j As Integer
    
    vLinha = mfgAlterar.Row
    mfgAlterar.Col = 1
    txtNotaA.Text = mfgAlterar.Text
    vLinha = mfgAlterar.Row
    mfgAlterar.Col = 2
    txtInicA.Text = mfgAlterar.Text
    vLinha = mfgAlterar.Row
    mfgAlterar.Col = 3
    txtFinalA.Text = mfgAlterar.Text
    vLinha = mfgAlterar.Row
    mfgAlterar.Col = 4
    txtLimiteA.Text = mfgAlterar.Text
    
'    For j = 1 To mfgAlterar.Rows - 1
'        For i = 1 To mfgAlterar.Cols - 1
'           mfgAlterar.Col = i
'           mfgAlterar.Row = j
'           If mfgAlterar.CellForeColor = &H80000014 Then
'                mfgAlterar.CellBackColor = &H80000014
'                mfgAlterar.CellForeColor = &H80000012
'           End If
'        Next i
'    Next j
        
'    For i = 1 To mfgAlterar.Cols - 1
'       mfgAlterar.Col = i
'       mfgAlterar.Row = vLinha
'       mfgAlterar.CellBackColor = &H8000000D
'       mfgAlterar.CellForeColor = &H80000014
'    Next i
    
End Sub

Private Sub txtInicI_KeyPress(KeyAscii As Integer)
   
   KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtInicI)
   If Chr$(KeyAscii) = "," Then
        KeyAscii = Asc(".")
   End If

End Sub

Private Sub txtFinalI_KeyPress(KeyAscii As Integer)
   
   KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtFinalI)
   If Chr$(KeyAscii) = "," Then
        KeyAscii = Asc(".")
   End If

End Sub

Private Sub txtLimiteI_KeyPress(KeyAscii As Integer)
   
   KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtLimiteI)
   If Chr$(KeyAscii) = "," Then
        KeyAscii = Asc(".")
   End If

End Sub

Private Sub CmdGravarI_Click()

Dim i As Integer
Dim vNota As String
Dim vInic As Double
Dim vFinal As Double
Dim vLimite As Double
    
    'verifica se campos est�o preenchidos
    If Trim(txtNotaI.Text) = "" Then
        Call vVB_Generica_001.Informar("� necess�rio informar o valor Nota!")
        txtNotaI.SetFocus
        Exit Sub
    ElseIf Trim(txtInicI.Text) = "" Then
        Call vVB_Generica_001.Informar("� necess�rio informar o valor Nota Inicial!")
        txtInicI.SetFocus
        Exit Sub
    ElseIf Trim(txtFinalI.Text) = "" Then
        Call vVB_Generica_001.Informar("� necess�rio informar o valor Nota Final!")
        txtFinalI.SetFocus
        Exit Sub
    ElseIf Trim(txtLimiteI.Text) = "" Then
        Call vVB_Generica_001.Informar("� necess�rio informar o valor Limite!")
        txtLimiteI.SetFocus
        Exit Sub
    End If
   
    vNota = UCase(Trim(txtNotaI.Text))
    vInic = Format(Trim(txtInicI.Text), "###.0")
    vFinal = Format(Trim(txtFinalI.Text), "###.0")
    vLimite = Format(Trim(txtLimiteI.Text), "###.0")
    
    'Verifica se Nota > que 1 caracter
    If Len(vNota) > 1 Then
        Call vVB_Generica_001.Informar("O campo Nota deve conter no m�ximo 1 caracter!")
        txtNotaI.Text = ""
        txtNotaI.SetFocus
        Exit Sub
    End If
    
    'Verificar se Nota j� existe
    For i = 1 To mfgIncluir.Rows - 1
        mfgIncluir.Col = 1
        mfgIncluir.Row = i
        If mfgIncluir.Text = vNota Then
             Call vVB_Generica_001.Informar("Limite de Cr�dito j� existe!")
             Call cmdCancelI_Click
             Exit Sub
        End If
    Next i
    
    'verifica se campos tem 3 casas inteiras e 1 decimal (Oracle: Number(4,1))
    If CDbl(Trim(txtInicI.Text)) > 999.9 Then
        Call vVB_Generica_001.Informar("Valor inv�lido para o campo Nota Inicial!")
        txtInicI.SetFocus
        Exit Sub
    End If
    
    If CDbl(Trim(txtFinalI.Text)) > 999.9 Then
        Call vVB_Generica_001.Informar("Valor inv�lido para o campo Nota Final!")
        txtFinalI.SetFocus
        Exit Sub
    End If

    If CDbl(Trim(txtLimiteI.Text)) > 999.9 Then
        Call vVB_Generica_001.Informar("Valor inv�lido para o campo Limite!")
        txtLimiteI.SetFocus
        Exit Sub
    End If
    
    'confirma inclus�o
    If vVB_Generica_001.Perguntar("Confirma Inclus�o?") = 7 Then
        Call cmdCancelI_Click
        Exit Sub
    End If
   
    'envia par�metros para o banco e executa INSERT
    Call vVB_Generica_001.ExcluiBind(vBanco)
    vBanco.Parameters.Add "vNOTA", vNota, 1
    vBanco.Parameters.Add "vINIC", vInic, 1
    vBanco.Parameters.Add "vFINAL", vFinal, 1
    vBanco.Parameters.Add "vLIMITE", vLimite, 1
  
    vSql = "PRODUCAO.PCK_CAD540.PR_INS_LIMCRED(:vNOTA, :vINIC, :vFINAL, :vLIMITE)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro)
        Exit Sub
    End If
    
    'atualiza tela
    Call Carrega_Grid
    Call vVB_Generica_001.Informar("Inclus�o do Limite de Cr�dito '" & vNota & "' efetuada com sucesso!")
    Call cmdCancelI_Click

End Sub

Private Sub mfgExcluir_DblClick()

Dim vNota As String
Dim vLinha As Integer
Dim i As Integer

    'guarda valor da Nota selecionada
    mfgExcluir.Col = 1
    vLinha = mfgExcluir.Row
    vNota = mfgExcluir.Text
    
    'Seleciona a linha (em azul)
    For i = 1 To mfgExcluir.Cols - 1
       mfgExcluir.Col = i
       mfgExcluir.CellBackColor = &H8000000D
       mfgExcluir.CellForeColor = &H80000014
    Next i
         
    'confirma exclus�o
    If vVB_Generica_001.Perguntar("Confirma exclus�o do Limite de Cr�dito '" & vNota & "' ?") = 7 Then
        Call cmdCancelA_Click
        For i = 1 To mfgExcluir.Cols - 1
           mfgExcluir.Col = i
           mfgExcluir.CellBackColor = &H80000014
           mfgExcluir.CellForeColor = &H80000012
        Next i
        Exit Sub
    End If
            
    'envia par�metros para o banco e executa comando DELETE
    vBanco.Parameters.Remove "V_NOTA"
    vBanco.Parameters.Add "V_NOTA", vNota, 1
     
    vSql = "PRODUCAO.PCK_CAD540.PR_DEL_LIMCRED(:V_NOTA)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro)
        Exit Sub
    End If
    
    'atualiza tela
    For i = 1 To mfgExcluir.Cols - 1
        mfgExcluir.Col = i
        mfgExcluir.CellBackColor = &H80000014
        mfgExcluir.CellForeColor = &H80000012
    Next i
    
    Call Carrega_Grid
    Call vVB_Generica_001.Informar("Exclus�o do Limite de Cr�dito '" & vNota & "' efetuada com sucesso!")

End Sub

Private Sub txtInicA_KeyPress(KeyAscii As Integer)
   
   KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtInicA)
   If Chr$(KeyAscii) = "," Then
        KeyAscii = Asc(".")
   End If

End Sub

Private Sub txtFinalA_KeyPress(KeyAscii As Integer)
   
   KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtFinalA)
   If Chr$(KeyAscii) = "," Then
        KeyAscii = Asc(".")
   End If

End Sub

Private Sub txtLimiteA_KeyPress(KeyAscii As Integer)
   
   KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtLimiteA)
   If Chr$(KeyAscii) = "," Then
        KeyAscii = Asc(".")
   End If

End Sub

Private Sub CmdGravarA_Click()

Dim vNota As String
Dim vInic As Double
Dim vFinal As Double
Dim vLimite As Double
    
    'verifica se campos est�o preenchidos
    If Trim(txtNotaA.Text) = "" Then
        Call vVB_Generica_001.Informar("� necess�rio selecionar um Limite de Cr�dito!")
        Call cmdCancelA_Click
        Exit Sub
    ElseIf Trim(txtInicA.Text) = "" Then
        Call vVB_Generica_001.Informar("� necess�rio informar o valor Nota Inicial!")
        txtInicA.SetFocus
        Exit Sub
    ElseIf Trim(txtFinalA.Text) = "" Then
        Call vVB_Generica_001.Informar("� necess�rio informar o valor Nota Final!")
        txtFinalA.SetFocus
        Exit Sub
    ElseIf Trim(txtLimiteA.Text) = "" Then
        Call vVB_Generica_001.Informar("� necess�rio informar o valor Limite!")
        txtLimiteA.SetFocus
        Exit Sub
    End If
    
    'verifica campos num�ricos
    If CDbl(Trim(txtInicA.Text)) > 999.9 Then
        Call vVB_Generica_001.Informar("Valor inv�lido para o campo Nota Inicial!")
        txtInicA.SetFocus
        Exit Sub
    End If
    
    If CDbl(Trim(txtFinalA.Text)) > 999.9 Then
        Call vVB_Generica_001.Informar("Valor inv�lido para o campo Nota Final!")
        txtFinalA.SetFocus
        Exit Sub
    End If

    If CDbl(Trim(txtLimiteA.Text)) > 999.9 Then
        Call vVB_Generica_001.Informar("Valor inv�lido para o campo Limite!")
        txtLimiteA.SetFocus
        Exit Sub
    End If
    
    vNota = Trim(txtNotaA.Text)
    
    'confirma altera��o
    If vVB_Generica_001.Perguntar("Confirma altera��o do Limite '" & vNota & "' ?") = 7 Then
        Call cmdCancelA_Click
        Exit Sub
    End If
        
    vInic = Format(Trim(txtInicA.Text), "###.0")
    vFinal = Format(Trim(txtFinalA.Text), "###.0")
    vLimite = Format(Trim(txtLimiteA.Text), "###.0")
    
    'envia par�metros para o banco e executa comando UPDATE
    Call vVB_Generica_001.ExcluiBind(vBanco)
    vBanco.Parameters.Add "vNOTA", vNota, 1
    vBanco.Parameters.Add "vINIC", vInic, 1
    vBanco.Parameters.Add "vFINAL", vFinal, 1
    vBanco.Parameters.Add "vLIMITE", vLimite, 1
  
    vSql = "PRODUCAO.PCK_CAD540.PR_ALT_LIMCRED(:vNOTA, :vINIC, :vFINAL, :vLIMITE)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro)
        Exit Sub
    End If
    
    'atualiza tela
    Call Carrega_Grid
    Call vVB_Generica_001.Informar("Altera��o do Limite de Cr�dito '" & vNota & "' efetuada com sucesso!")
    Call cmdCancelA_Click
    
End Sub

Private Sub SSTabCadLim_Click(previoustab As Integer)
    
    'limpa campos qdo usu�rio muda de tab
    txtNotaA.Text = ""
    txtInicA.Text = ""
    txtFinalA.Text = ""
    txtLimiteA.Text = ""
    txtNotaI.Text = ""
    txtInicI.Text = ""
    txtFinalI.Text = ""
    txtLimiteI.Text = ""
    
    If SSTabCadLim.Tab = 2 Then
        txtNotaI.SetFocus
    End If
    If SSTabCadLim.Tab = 1 Then
        txtInicA.SetFocus
    End If

End Sub

Private Sub Carrega_Grid()

    Call Aguardar
    
    'envia par�metros para o banco e executa comando SELECT
    Call vVB_Generica_001.ExcluiBind(vBanco)
    vBanco.Parameters.Remove "vCursor"
    vBanco.Parameters.Add "vCursor", 0, 3
    vBanco.Parameters("vCursor").ServerType = 102
    vBanco.Parameters("vCursor").DynasetOption = &H2&
    vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
        
    vSql = "PRODUCAO.PCK_CAD540.PR_SEL_LIMCRED(:vCursor)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    Unload frmAguardar
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro)
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("vCursor").Value
    End If
    
    'preenche grid
    If vObjOracle.EOF = True Then  'n�o achou
        Call vVB_Generica_001.Informar("Limites de Cr�dito inexistentes!")
        Exit Sub
    Else
        Call vVB_Generica_001.CarregaGridTabela(mfgConsultar, vObjOracle, 5)
        Call vVB_Generica_001.CarregaGridTabela(mfgAlterar, vObjOracle, 5)
        Call vVB_Generica_001.CarregaGridTabela(mfgIncluir, vObjOracle, 5)
        Call vVB_Generica_001.CarregaGridTabela(mfgExcluir, vObjOracle, 5)
    End If

End Sub

Private Sub txtNotaI_KeyPress(KeyAscii As Integer)

    'coloca letra mai�scula para o campo
    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)

End Sub

