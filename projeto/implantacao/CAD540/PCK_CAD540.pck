create or replace package PRODUCAO.PCK_CAD540 is

  TYPE TP_CURSOR IS REF CURSOR;

  Procedure PR_SEL_LIMCRED (cLimCred IN OUT TP_CURSOR);
  
  Procedure PR_INS_LIMCRED (pNota  IN PONT_CREDITO.NOTA%TYPE, 
                            pNotaInic IN PONT_CREDITO.PC_NOTA_INIC%TYPE,
                            pNotaFinal IN PONT_CREDITO.PC_NOTA_FINAL%TYPE,
                            pLimite IN PONT_CREDITO.PC_LIMITE%TYPE);
                            
  Procedure PR_ALT_LIMCRED (pNota  IN PONT_CREDITO.NOTA%TYPE, 
                            pNotaInic IN PONT_CREDITO.PC_NOTA_INIC%TYPE,
                            pNotaFinal IN PONT_CREDITO.PC_NOTA_FINAL%TYPE,
                            pLimite IN PONT_CREDITO.PC_LIMITE%TYPE);   
                            
  Procedure PR_DEL_LIMCRED (pNota  IN PONT_CREDITO.NOTA%TYPE);                                                   
  
end PCK_CAD540;
/
create or replace package body PRODUCAO.PCK_CAD540 is
  
  --Consulta
  Procedure PR_SEL_LIMCRED (cLimCred IN OUT TP_CURSOR) is
    Begin
       OPEN cLimCred FOR
          SELECT   NOTA, 
                   TO_CHAR(PC_NOTA_INIC,'9990.9') AS PC_NOTA_INIC, 
                   TO_CHAR(PC_NOTA_FINAL,'9990.9') AS PC_NOTA_FINAL, 
                   TO_CHAR(PC_LIMITE,'9990.9') AS PC_LIMITE 
          FROM     PONT_CREDITO
          ORDER BY NOTA;          
  End;
  
  --Insere
  Procedure PR_INS_LIMCRED (pNota  IN PONT_CREDITO.NOTA%TYPE, 
                            pNotaInic IN PONT_CREDITO.PC_NOTA_INIC%TYPE,
                            pNotaFinal IN PONT_CREDITO.PC_NOTA_FINAL%TYPE,
                            pLimite IN PONT_CREDITO.PC_LIMITE%TYPE) is
     Begin                            
       INSERT INTO PONT_CREDITO
       VALUES (pNota, pNotaInic, pNotaFinal,pLimite);

     Commit; 
  End;
  
  --Altera
  Procedure PR_ALT_LIMCRED (pNota  IN PONT_CREDITO.NOTA%TYPE, 
                            pNotaInic IN PONT_CREDITO.PC_NOTA_INIC%TYPE,
                            pNotaFinal IN PONT_CREDITO.PC_NOTA_FINAL%TYPE,
                            pLimite IN PONT_CREDITO.PC_LIMITE%TYPE) is
     Begin                            
       UPDATE PONT_CREDITO
       SET PC_NOTA_INIC = pNotaInic, 
           PC_NOTA_FINAL = pNotaFinal, 
           PC_LIMITE = pLimite
       WHERE NOTA = pNota;
       
     Commit;            
  End;
  
  --Deleta
  Procedure PR_DEL_LIMCRED (pNota  IN PONT_CREDITO.NOTA%TYPE) is
     Begin                            
       DELETE FROM PONT_CREDITO
       WHERE NOTA = pNota;
       
     Commit; 
  End;  
  
end PCK_CAD540;
/
