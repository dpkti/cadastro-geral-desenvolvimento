VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Begin VB.Form frmLogin 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Login"
   ClientHeight    =   1575
   ClientLeft      =   2835
   ClientTop       =   3480
   ClientWidth     =   2640
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   930.562
   ScaleMode       =   0  'User
   ScaleWidth      =   2478.818
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtUserName 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Height          =   345
      Left            =   1020
      TabIndex        =   1
      Top             =   30
      Width           =   1515
   End
   Begin VB.TextBox txtPassword 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Height          =   345
      IMEMode         =   3  'DISABLE
      Left            =   1020
      PasswordChar    =   "*"
      TabIndex        =   2
      Top             =   420
      Width           =   1515
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   1110
      TabIndex        =   3
      TabStop         =   0   'False
      ToolTipText     =   "Conectar"
      Top             =   810
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmLogin.frx":0000
      PICN            =   "frmLogin.frx":001C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdCancel 
      Height          =   690
      Left            =   1860
      TabIndex        =   4
      TabStop         =   0   'False
      ToolTipText     =   "Sair"
      Top             =   810
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmLogin.frx":08F6
      PICN            =   "frmLogin.frx":0912
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label lblLabels 
      AutoSize        =   -1  'True
      Caption         =   "&User Name:"
      Height          =   195
      Index           =   0
      Left            =   105
      TabIndex        =   0
      Top             =   150
      Width           =   840
   End
   Begin VB.Label lblLabels 
      AutoSize        =   -1  'True
      Caption         =   "&Password:"
      Height          =   195
      Index           =   1
      Left            =   105
      TabIndex        =   5
      Top             =   540
      Width           =   735
   End
End
Attribute VB_Name = "frmLogin"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdCancel_Click()
    End
End Sub

Private Sub cmdVoltar_Click()
    
    If txtUserName <> "" And txtPassword <> "" Then
        
        vLogin = txtUserName
        vSenha = txtPassword
        
        If Get_Permissao = False Then Exit Sub
        
        Unload Me
        
        frmVda231.Show
        
    Else
        MsgBox "Invalid Password, try again!", , "Login"
        txtPassword.SetFocus
        SendKeys "{Home}+{End}"
    End If
End Sub

Private Sub Form_Load()
    frmLogo.Show 1
End Sub
Function Get_Permissao() As Boolean

    Dim vrstPermissao As Object
    Dim lItem As ListItem
    Dim vColunas
    
    vErro = vVB_Generica_001.ConectaOracle("PRODUCAO", "VDA020", False, frmVda231)
    Set vBanco = vVB_Generica_001.vBanco
    
    Set vrstPermissao = vBanco.CreateDynaset("Select vl_parametro from Helpdesk.parametros where cod_software=1069 and nome_parametro='PERMISSAO' and vl_parametro like '" & vLogin & ";" & vSenha & "%'", 0&)

    If vrstPermissao.RecordCount <= 0 Then
        MsgBox "Senha ou Login Inv�lido.", vbInformation, "Aten��o"
        txtUserName.SetFocus
        Exit Function
    End If
    
    vColunas = Split(vrstPermissao!VL_PARAMETRO, ";")
        
    If UCase(vColunas(0)) <> UCase(vLogin) Or UCase(vColunas(1)) <> UCase(vSenha) Then
        MsgBox "Senha ou Login Inv�lido.", vbInformation, "Aten��o"
        txtUserName.SetFocus
        Get_Permissao = False
        Exit Function
    End If
    
    vPabx = IIf(UCase(vColunas(2)) <> "", True, False)
    vLigAuto = IIf(UCase(vColunas(3)) <> "", True, False)
    vTLMKTEspecifico = IIf(UCase(vColunas(4)) <> "", True, False)
    vTempo = IIf(UCase(vColunas(5)) <> "", True, False)
    'vLinks = IIf(UCase(vColunas(6)) <> "", True, False)
    
    Get_Permissao = True
    
    vrstPermissao.Close
    Set vrstPermissao = Nothing
End Function
Private Sub txtPassword_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub txtUserName_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub
