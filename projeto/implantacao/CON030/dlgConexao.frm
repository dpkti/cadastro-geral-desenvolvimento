VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form dlgConexao 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Conex�o"
   ClientHeight    =   1725
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3900
   ControlBox      =   0   'False
   Icon            =   "dlgConexao.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   115
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   260
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fra 
      Caption         =   "Dep�sito"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   735
      Left            =   45
      TabIndex        =   3
      Top             =   945
      Width           =   3795
      Begin VB.ComboBox cmbLoja 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   945
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   270
         Width           =   2715
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         Caption         =   "Dep�sito:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   135
         TabIndex        =   4
         Top             =   315
         Width           =   780
      End
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   0
      Top             =   810
      Width           =   3840
      _ExtentX        =   6773
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   690
      Left            =   855
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Sair"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "dlgConexao.frx":000C
      PICN            =   "dlgConexao.frx":0028
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdConectar 
      Height          =   690
      Left            =   30
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Conectar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "dlgConexao.frx":0D02
      PICN            =   "dlgConexao.frx":0D1E
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "dlgConexao"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public vConexao As String

Private Sub cmbLoja_Click()
    frmVda231.lblDeposito.Caption = Me.cmbLoja
End Sub

Private Sub cmdConectar_Click()

    If cmbLoja <> "" Then
        
        If vCd = Val(cmbLoja) Then
            vConexao = "PRODUCAO"
        ElseIf InStr(1, vCDDPK, Format(vCd, "00")) > 0 Then
            vConexao = "DEP" & Format(vCd, "00") & "."
        Else
            vConexao = "CD" & CStr(Format(Val(cmbLoja), "00"))
        End If
        
        If vErro <> "" Then
    
            Call vVB_Generica_001.ProcessaErro(vErro)
            End
    
        End If
    
        Select Case Val(cmbLoja)
            Case InStr(1, vCDDPK, Format(Val(cmbLoja), "00")) > 0
                vErro = vVB_Generica_001.ConectaOracle("CDDPK", "vda020", False, frmVda231)
                frmVda231.vDEP = "DEP" & CStr(Format(Val(cmbLoja), "00"))
                frmVda231.vNumDep = Val(cmbLoja)
            
            Case 1
                vErro = vVB_Generica_001.ConectaOracle("PRODUCAO", "vda020", False, frmVda231)
                frmVda231.vDEP = "PRODUCAO"
                frmVda231.vNumDep = 1
            
            Case Else
                vErro = vVB_Generica_001.ConectaOracle(vConexao, "vda020", False, frmVda231)
                frmVda231.vDEP = "PRODUCAO"
                frmVda231.vNumDep = Val(Me.cmbLoja)
            
        End Select
        
        vCd = vVB_Generica_001.vCd
        vTipoCD = vVB_Generica_001.vTipoCD
        Set vSessao = vVB_Generica_001.vSessao
        Set vBanco = vVB_Generica_001.vBanco
        
        If frmVersoes.vVersoes = True Then
            frmVersoes.Caption = "Conectado em : " & Me.cmbLoja
        End If
                
        Unload Me

        frmVda231.lstCidadesSemDDD.Tag = "Carregando"
        If frmVersoes.vVersoes = False Then
            frmVda231.Carregar_Parametros
            frmVda231.Carregar_Cidades
            frmVda231.Cidades_Selecionadas
            frmVda231.carregar_lsv_Canal
            frmVda231.lstCidadesSemDDD.Tag = ""
        End If
    Else
    
        Call vVB_Generica_001.Informar("Escolha o dep�sito")
        Exit Sub
    
    End If

End Sub
Private Sub cmdSair_Click()

    If vVB_Generica_001.Sair = 6 Then
    
        Set vObjOracle = Nothing
        Set vBanco = Nothing
        Set vSessao = Nothing
        
        End
    
    End If

End Sub

Private Sub Form_Load()

    vErro = vVB_Generica_001.ConectaOracle("PRODUCAO", "vda020", False, frmVda231)

    If vErro <> "" Then

        Call vVB_Generica_001.ProcessaErro(vErro)
        End

    End If

    vCd = vVB_Generica_001.vCd
    vTipoCD = vVB_Generica_001.vTipoCD
    Set vSessao = vVB_Generica_001.vSessao
    Set vBanco = vVB_Generica_001.vBanco


    Set vObjOracle = vVB_Generica_001.TabelaLoja(vBanco)
    
    Call vVB_Generica_001.PreencheComboList(vObjOracle, cmbLoja, "DEP�SITO")

End Sub

Private Sub Form_Unload(Cancel As Integer)
    Preencher_Lsv
End Sub

Sub Preencher_Lsv()
    Dim vObj As Object
    Dim litem As ListItem
    
    frmVersoes.lsvVersoes.ListItems.Clear
    
    Set vObj = vBanco.CreateDynaset("Select * from v$session where UPPER(PROGRAM) like 'VDA230%'", 0&)
    
    For i = 1 To vObj.RecordCount
        Set litem = frmVersoes.lsvVersoes.ListItems.Add
        litem = vObj!UserName
        litem.SubItems(1) = vObj!program
        vObj.MoveNext
    Next
    
    frmVersoes.lblLigacaoAutomatica = IIf(frmVda231.Pegar_VL_Parametro("LIGACAOAUTOMATICA") = 1, "Sim", "N�o")
    
    If frmVersoes.lblLigacaoAutomatica = "N�o" Then
        frmVersoes.lblLigacaoAutomatica = IIf(frmVda231.Pegar_VL_Parametro("LigacaoAutomatica") = 1, "Sim", "N�o")
    End If
    
    
End Sub
