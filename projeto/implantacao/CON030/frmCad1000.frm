VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form frmVda231 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "VDA231- Parametros"
   ClientHeight    =   8640
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8430
   Icon            =   "frmCad1000.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   8640
   ScaleWidth      =   8430
   StartUpPosition =   2  'CenterScreen
   Begin VB.ListBox lstDepositosOK 
      Appearance      =   0  'Flat
      Height          =   2565
      Left            =   4830
      TabIndex        =   31
      Top             =   5520
      Width           =   3555
   End
   Begin VB.Frame Frame2 
      Caption         =   "Canal de Voz"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   3735
      Left            =   90
      TabIndex        =   23
      Top             =   4410
      Width           =   4665
      Begin VB.TextBox txtDeposito 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   150
         TabIndex        =   24
         Top             =   390
         Width           =   2625
      End
      Begin VB.TextBox txtNumero 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2820
         TabIndex        =   26
         Top             =   390
         Width           =   1755
      End
      Begin MSComctlLib.ListView lsvCanalVoz 
         Height          =   2475
         Left            =   120
         TabIndex        =   28
         Top             =   1170
         Width           =   4425
         _ExtentX        =   7805
         _ExtentY        =   4366
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Dep�sito"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Canal de Voz"
            Object.Width           =   2540
         EndProperty
      End
      Begin Bot�o.cmd cmdExcl 
         Height          =   390
         Left            =   4110
         TabIndex        =   29
         TabStop         =   0   'False
         ToolTipText     =   "Voltar"
         Top             =   720
         Width           =   450
         _ExtentX        =   794
         _ExtentY        =   688
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCad1000.frx":23D2
         PICN            =   "frmCad1000.frx":23EE
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdAdd 
         Height          =   390
         Left            =   3540
         TabIndex        =   30
         TabStop         =   0   'False
         ToolTipText     =   "Voltar"
         Top             =   720
         Width           =   450
         _ExtentX        =   794
         _ExtentY        =   688
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCad1000.frx":2800
         PICN            =   "frmCad1000.frx":281C
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label Label9 
         AutoSize        =   -1  'True
         Caption         =   "Dep�sito:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   150
         TabIndex        =   27
         Top             =   180
         Width           =   825
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "N�mero:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   2820
         TabIndex        =   25
         Top             =   180
         Width           =   720
      End
   End
   Begin VB.TextBox txtSistema 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   480
      Left            =   1980
      TabIndex        =   17
      Text            =   "VDA230"
      Top             =   510
      Width           =   1305
   End
   Begin VB.Frame Frame1 
      Caption         =   "Par�metros do PABX"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   2265
      Left            =   90
      TabIndex        =   6
      Top             =   1980
      Width           =   3075
      Begin VB.ComboBox cboConectado 
         Appearance      =   0  'Flat
         Height          =   315
         ItemData        =   "frmCad1000.frx":2CB6
         Left            =   135
         List            =   "frmCad1000.frx":2CC0
         Style           =   2  'Dropdown List
         TabIndex        =   11
         Top             =   1740
         Width           =   735
      End
      Begin VB.TextBox txtIP4000 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   105
         TabIndex        =   10
         Top             =   540
         Width           =   1635
      End
      Begin VB.TextBox txtIP3000 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   105
         TabIndex        =   9
         Top             =   1080
         Width           =   1635
      End
      Begin VB.TextBox txtPorta4000 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1815
         TabIndex        =   8
         Top             =   540
         Width           =   975
      End
      Begin VB.TextBox txtPorta3000 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1815
         TabIndex        =   7
         Top             =   1080
         Width           =   975
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "IP HP4000"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   90
         TabIndex        =   16
         Top             =   330
         Width           =   930
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "IP HP3000"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   90
         TabIndex        =   15
         Top             =   870
         Width           =   930
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Porta"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   1800
         TabIndex        =   14
         Top             =   330
         Width           =   465
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "Porta"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   1800
         TabIndex        =   13
         Top             =   870
         Width           =   465
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         Caption         =   "Trabalhar Conectado"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   120
         TabIndex        =   12
         Top             =   1500
         Width           =   1800
      End
   End
   Begin VB.ListBox lstCidadesSemDDD 
      Appearance      =   0  'Flat
      Height          =   2055
      Left            =   3375
      Style           =   1  'Checkbox
      TabIndex        =   3
      Top             =   2190
      Width           =   4875
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   2
      Top             =   1125
      Width           =   8235
      _ExtentX        =   14526
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   8310
      Width           =   8430
      _ExtentX        =   14870
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   2646
            MinWidth        =   2646
            Object.ToolTipText     =   "Usu�rio da rede"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   2381
            MinWidth        =   2381
            Object.ToolTipText     =   "Usu�rio do banco de dados"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   3528
            MinWidth        =   3528
            Object.ToolTipText     =   "Banco de dados conectado"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            Alignment       =   1
            Object.Width           =   2381
            MinWidth        =   2381
            TextSave        =   "07/07/05"
            Object.ToolTipText     =   "Data"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   1
            Object.Width           =   1587
            MinWidth        =   1587
            TextSave        =   "17:48"
            Object.ToolTipText     =   "Hora"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   1005
      Left            =   7380
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Sair do sistema"
      Top             =   60
      Width           =   945
      _ExtentX        =   1667
      _ExtentY        =   1773
      BTYPE           =   3
      TX              =   "Sair"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCad1000.frx":2CCE
      PICN            =   "frmCad1000.frx":2CEA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdSalvar 
      Height          =   1005
      Left            =   6420
      TabIndex        =   4
      TabStop         =   0   'False
      ToolTipText     =   "Salvar"
      Top             =   60
      Width           =   945
      _ExtentX        =   1667
      _ExtentY        =   1773
      BTYPE           =   3
      TX              =   "Gravar"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCad1000.frx":39C4
      PICN            =   "frmCad1000.frx":39E0
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdConectar 
      Height          =   1005
      Left            =   5460
      TabIndex        =   22
      TabStop         =   0   'False
      ToolTipText     =   "Conectar"
      Top             =   60
      Width           =   945
      _ExtentX        =   1667
      _ExtentY        =   1773
      BTYPE           =   3
      TX              =   "Conectar"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCad1000.frx":46BA
      PICN            =   "frmCad1000.frx":46D6
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label10 
      AutoSize        =   -1  'True
      Caption         =   "Dep�sitos Atualizados:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   4830
      TabIndex        =   32
      Top             =   5310
      Width           =   1950
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Qtd Cidades Selecionadas:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   5430
      TabIndex        =   21
      Top             =   4290
      Width           =   2310
   End
   Begin VB.Label lblQtd 
      AutoSize        =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   240
      Left            =   7770
      TabIndex        =   20
      Top             =   4260
      Width           =   465
   End
   Begin VB.Label lblDeposito 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   20.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   555
      Left            =   120
      TabIndex        =   19
      Top             =   1260
      Width           =   8130
   End
   Begin VB.Label Label6 
      AutoSize        =   -1  'True
      Caption         =   "Nome Sistema:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   1980
      TabIndex        =   18
      Top             =   270
      Width           =   1275
   End
   Begin VB.Label lbl1 
      AutoSize        =   -1  'True
      Caption         =   "Cidades Sem DDD para Dep�sito Atual:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   3375
      TabIndex        =   5
      Top             =   1980
      Width           =   3405
   End
   Begin VB.Image Image1 
      Height          =   1185
      Left            =   -90
      Picture         =   "frmCad1000.frx":4FB0
      Stretch         =   -1  'True
      ToolTipText     =   "Acessar a Intranet"
      Top             =   -45
      Width           =   1455
   End
End
Attribute VB_Name = "frmVda231"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const ORATYPE_CURSOR = 102
Const ORADYN_NO_BLANKSTRIP = &H2&
Const ORAPARM_INPUT = 1              'CONSTANTE DE BIND INPUT
Const ORAPARM_OUTPUT = 2             'CONSTANTE DE BIND OUTPUT
Const ORAPARM_BOTH = 3               'CONSTANTE DE BIND INPUT/OUTPUT
Const ORATYPE_NUMBER = 2

Public vDEP As String
Public vNumDep As Byte
Public vIsCDDPK As Boolean

Private Sub cmdAdd_Click()
    Gravar_Canal "CV-" & txtDeposito, txtNumero
    carregar_lsv_Canal
    txtDeposito = ""
    txtNumero = ""
End Sub

Private Sub cmdConectar_Click()
    dlgConexao.Show 1
End Sub

Private Sub cmdExcl_Click()
    If txtDeposito = "" Or txtNumero = "" Then
        MsgBox "Selecione um item da lista.", vbInformation, "Aten��o"
        Exit Sub
    End If
    
    Deletar_Canal "CV-" & txtDeposito, txtNumero

    MsgBox "Item excluido com sucesso !!", vbInformation, "Aten��o"
    
    carregar_lsv_Canal
    
End Sub

Private Sub cmdSair_Click()

    If vVB_Generica_001.Sair = 6 Then
    
        Set vObjOracle = Nothing
        Set vBanco = Nothing
        Set vSessao = Nothing
        
        End
    
    End If

End Sub
Private Sub cmdSobre_Click()

    frmSobre.Show 1

End Sub
Private Sub Form_Load()

    frmLogo.Show 1
    
    Me.Top = 0
    Me.Left = 0
    
End Sub


Private Sub cmdSalvar_Click()
    Gravar "IP4000", txtIP4000
    Gravar "PORTA4000", txtPorta4000
    Gravar "IP3000", txtIP3000
    Gravar "PORTA3000", txtPorta3000
    Gravar "LIGACAOAUTOMATICA", IIf(UCase(cboConectado) = "SIM", 1, 0)
End Sub

Sub Gravar(pNomeParam As String, pValorParam As String)
    Dim vOwner As String
    
    'If Val(vObjOracle.Fields(0)) = 0 Or Val(vObjOracle.Fields(0)) = 6 Or Val(vObjOracle.Fields(0)) = 15 Th
    
    vBanco.Parameters.Remove "Cod_Soft": vBanco.Parameters.Add "Cod_Soft", Pegar_Cod_Sistema, 1
    vBanco.Parameters.Remove "Nome_Param": vBanco.Parameters.Add "Nome_Param", pNomeParam, 1
    vBanco.Parameters.Remove "VL_PARAM": vBanco.Parameters.Add "VL_PARAM", pValorParam, 1
    vBanco.Parameters.Remove "Dep": vBanco.Parameters.Add "DEP", vOwner, 1
    vBanco.Parameters.Remove "Txt_Erro": vBanco.Parameters.Add "Txt_Erro", "", 2

    If vVB_Generica_001.ExecutaPl(vBanco, vDEP & ".PCK_VDA231.Pr_Salvar_Parametros(:Nome_Param, :VL_PARAM, :Cod_Soft, :DEP, :Txt_Erro)") <> "" Then
        MsgBox "Descri��o do Erro:" & vErro
        Exit Sub
    End If

End Sub

Sub Deletar(pNomeParam As String, pValorParam As String)
    Dim vOwner As String
    
    If InStr(1, "02, 11, 12, 13, 16, 17, 18, 19", Format(CStr(Val(Right(vDEP, 2))), "00")) Then
       vErro = vVB_Generica_001.ConectaOracle("CDDPK", "vda020", False, frmVda231)
       vOwner = "DEP" & Format(Val(Right(vDEP, 2)), "00") & "."
       'vDEP = vOwner
       Set vSessao = vVB_Generica_001.vSessao
       Set vBanco = vVB_Generica_001.vBanco
    Else
        If Val(vObjOracle.Fields(0)) = 1 Then
           vErro = vVB_Generica_001.ConectaOracle("PRODUCAO", "vda020", False, frmVda231)
        Else
           vErro = vVB_Generica_001.ConectaOracle("CD" & Format(CStr(Val(vObjOracle.Fields(0))), "00"), "vda020", False, frmVda231)
        End If
        Set vSessao = vVB_Generica_001.vSessao
        Set vBanco = vVB_Generica_001.vBanco
        vOwner = "HELPDESK."
        'vDEP = "PRODUCAO."
    End If

    vBanco.Parameters.Remove "Cod_Soft": vBanco.Parameters.Add "Cod_Soft", Pegar_Cod_Sistema, 1
    vBanco.Parameters.Remove "Nome_Param": vBanco.Parameters.Add "Nome_Param", pNomeParam, 1
    'vBanco.Parameters.Remove "VL_PARAM": vBanco.Parameters.Add "VL_PARAM", pValorParam, 1
    vBanco.Parameters.Remove "Dep": vBanco.Parameters.Add "DEP", vOwner, 1
    vBanco.Parameters.Remove "Txt_Erro": vBanco.Parameters.Add "Txt_Erro", "", 2

    vVB_Generica_001.ExecutaPl vBanco, vDEP & ".PCK_VDA231.Pr_Deletar_Parametros(:Nome_Param, :Cod_Soft, :DEP, :Txt_Erro)"

    If Trim(vBanco.Parameters("Txt_Erro")) <> "" Then
        MsgBox "Descri��o do Erro:" & vBanco.Parameters("Txt_Erro")
        Exit Sub
    End If
End Sub


Public Sub Carregar_Parametros()
    
    Dim vIP4000 As String
    Dim vPorta4000 As String
    Dim vIP3000 As String
    Dim vPorta3000 As String
    Dim vConectado As String
    
    vIP4000 = "" & Pegar_VL_Parametro("IP4000")
    vPorta4000 = "" & Pegar_VL_Parametro("PORTA4000")
    vIP3000 = "" & Pegar_VL_Parametro("IP3000")
    vPorta3000 = "" & Pegar_VL_Parametro("PORTA3000")
    vConectado = IIf(Pegar_VL_Parametro("LIGACAOAUTOMATICA") = 1, "Sim", "N�o")
    
    If vConectado = "N�o" Then
        vConectado = IIf(Pegar_VL_Parametro("LigacaoAutomatica") = 1, "Sim", "N�o")
    End If
    
    txtIP4000 = vIP4000
    txtPorta4000 = vPorta4000
    txtIP3000 = vIP3000
    txtPorta3000 = vPorta3000
    cboConectado = vConectado

End Sub

Private Sub lstCidadesSemDDD_ItemCheck(Item As Integer)
    If lstCidadesSemDDD.Tag = "Carregando" Then Exit Sub
    If lstCidadesSemDDD.Selected(Item) = True Then
        Gravar lstCidadesSemDDD.List(Item), lstCidadesSemDDD.ItemData(Item)
    Else
        Deletar lstCidadesSemDDD.List(Item), lstCidadesSemDDD.ItemData(Item)
    End If
    lblQtd.Caption = lstCidadesSemDDD.SelCount
End Sub

Private Sub lsvCanalVoz_Click()
    txtDeposito = lsvCanalVoz.SelectedItem
    txtNumero = lsvCanalVoz.SelectedItem.SubItems(1)
End Sub

Private Sub txtDeposito_KeyPress(KeyAscii As Integer)
    If KeyAscii <> 8 And KeyAscii <> 13 Then
        txtDeposito = txtDeposito & UCase(Chr(KeyAscii))
        KeyAscii = 0
    End If
End Sub

Private Sub txtIP3000_Change()
    txtIP4000 = ""
    txtPorta4000 = ""
End Sub

Private Sub txtIP4000_Change()
    txtIP3000 = ""
    txtPorta3000 = ""
End Sub

Public Sub Carregar_Cidades()

    Dim vObjCidades As Object
    
    lstCidadesSemDDD.Clear
    
    vBanco.Parameters.Remove "CodLojaCidade"
    vBanco.Parameters.Add "CodLojaCidade", vNumDep, 1
    Criar_Cursor
    vErro = vVB_Generica_001.ExecutaPl(vBanco, "Producao.PCK_VDA231.Pr_Select_Cidades_Loja(:vCursor, :CodLojaCidade)")

    If vErro <> "" Then
        MsgBox vErro
        Exit Sub
    Else
        Set vObjCidades = vBanco.Parameters("vCursor").Value
    End If

    For i = 1 To vObjCidades.RecordCount
        lstCidadesSemDDD.AddItem vObjCidades("Nome_cidade")
        lstCidadesSemDDD.ItemData(lstCidadesSemDDD.NewIndex) = vObjCidades("Cod_cidade")
        vObjCidades.MoveNext
    Next

End Sub

Public Sub Cidades_Selecionadas()

    Dim vOwner As String
    Dim vObjCidades As Object
    Dim ii As Integer
    
    If vDEP = "PRODUCAO" Then
        vOwner = "HELPDESK."
    Else
        vOwner = vDEP & "."
    End If

    vBanco.Parameters.Remove "Cod_Soft": vBanco.Parameters.Add "Cod_Soft", Pegar_Cod_Sistema, 1
    vBanco.Parameters.Remove "Dep": vBanco.Parameters.Add "DEP", vOwner, 1
    Criar_Cursor

    vErro = vVB_Generica_001.ExecutaPl(vBanco, "PRODUCAO.PCK_VDA231.Pr_Select_Parametros_Cidades(:vCursor, :DEP, :Cod_Soft)")

    If Trim(vErro) <> "" Then
        MsgBox "Descri��o do Erro:" & vErro
        Exit Sub
    Else
        Set vObjCidades = vBanco.Parameters("vCursor").Value
    End If
    
    For i = 0 To lstCidadesSemDDD.ListCount - 1
        vObjCidades.MoveFirst
        For ii = 1 To vObjCidades.RecordCount
            If UCase(lstCidadesSemDDD.List(i)) = UCase(vObjCidades("Nome_parametro")) Then
               lstCidadesSemDDD.Selected(i) = True
               Exit For
            End If
            vObjCidades.MoveNext
        Next
        
    Next
    
    lblQtd.Caption = lstCidadesSemDDD.SelCount

End Sub

Public Function Pegar_VL_Parametro(NOME_PARAMETRO As String)
    
    Dim vOwner As String
    
    If vDEP = "PRODUCAO" Then
        vOwner = "HELPDESK."
    Else
        vOwner = vDEP & "."
    End If
    
    Criar_Cursor

    vBanco.Parameters.Remove "Cod_Sistema"
    vBanco.Parameters.Add "Cod_Sistema", Pegar_Cod_Sistema, 1

    vBanco.Parameters.Remove "Nome_Parametro"
    vBanco.Parameters.Add "Nome_Parametro", NOME_PARAMETRO, 1

    vBanco.Parameters.Remove "DEP"
    vBanco.Parameters.Add "DEP", vOwner, 1

    vErro = vVB_Generica_001.ExecutaPl(vBanco, vDEP & ".PCK_VDA231.PR_SELECT_VL_PARAMETRO(:vCursor, :Cod_Sistema, :Nome_Parametro, :DEP)")

    If vErro = "" Then
        Set vObjOracle = vBanco.Parameters("vCursor").Value
        Pegar_VL_Parametro = vObjOracle("VL_PARAMETRO")
    End If
End Function

Public Function Pegar_Cod_Sistema()

    vBanco.Parameters.Remove "Nome_Software"
    vBanco.Parameters.Add "Nome_Software", txtSistema, 1

    vBanco.Parameters.Remove "Cod_Software"
    vBanco.Parameters.Add "Cod_Software", 0, 2

    vVB_Generica_001.ExecutaPl vBanco, "PRODUCAO.PCK_VDA231.PR_SELECT_COD_SOFTWARE(:Nome_Software, :Cod_Software)"

    Pegar_Cod_Sistema = vBanco.Parameters("Cod_Software")

    vBanco.Parameters.Remove "Cod_Software"

End Function

Public Sub Criar_Cursor()
    vBanco.Parameters.Remove "vCursor"
    vBanco.Parameters.Add "vCursor", 0, 2
    vBanco.Parameters("vCursor").serverType = ORATYPE_CURSOR
    vBanco.Parameters("vCursor").DynasetOption = ORADYN_NO_BLANKSTRIP
    vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
End Sub

Sub carregar_lsv_Canal()

    Dim vOwner As String
    Dim vObjCidades As Object
    Dim Litem As ListItem
    
    If vIsCDDPK = True Then
       vOwner = vDEP & "."
    Else
       vOwner = "HELPDESK."
    End If
    
    vBanco.Parameters.Remove "Cod_Soft": vBanco.Parameters.Add "Cod_Soft", 1069, 1
    vBanco.Parameters.Remove "Dep": vBanco.Parameters.Add "DEP", vOwner, 1
    Criar_Cursor

    vErro = vVB_Generica_001.ExecutaPl(vBanco, "PRODUCAO.PCK_VDA231.Pr_Select_Parametros_Cidades(:vCursor, :DEP, :Cod_Soft)")

    If Trim(vErro) <> "" Then
        MsgBox "Sub: Carregar_lsv_Canal" & vbCrLf & "Descri��o:" & vErro, , "Aten�ao"
        Exit Sub
    Else
        Set vObjCidades = vBanco.Parameters("vCursor").Value
    End If
    
    lsvCanalVoz.ListItems.Clear
    
    For i = 1 To vObjCidades.RecordCount
        
        If Left(vObjCidades("NOME_PARAMETRO"), 3) = "CV-" Then
            Set Litem = lsvCanalVoz.ListItems.Add
            Litem = Mid(vObjCidades!NOME_PARAMETRO, 4)
            Litem.SubItems(1) = vObjCidades!vl_parametro
        End If
        vObjCidades.MoveNext
    
    Next

End Sub

Sub Gravar_Canal(pNomeParam As String, pValorParam As String)
    
    Dim vOwner As String
    
    Set vObjOracle = vVB_Generica_001.TabelaLoja(vBanco)
    
    For i = 1 To vObjOracle.RecordCount
        If Val(vObjOracle.Fields(0)) = 0 Or Val(vObjOracle.Fields(0)) = 6 Or Val(vObjOracle.Fields(0)) = 15 Then GoTo Proximo
        
        If InStr(1, "02, 11, 12, 13, 16, 17, 18, 19", Format(CStr(Val(vObjOracle.Fields(0))), "00")) Then
           vErro = vVB_Generica_001.ConectaOracle("CDDPK", "vda020", False, frmVda231)
           vOwner = "DEP" & Format(Val(vObjOracle.Fields(0)), "00") & "."
           vDEP = vOwner
           Set vSessao = vVB_Generica_001.vSessao
           Set vBanco = vVB_Generica_001.vBanco
        Else
            If Val(vObjOracle.Fields(0)) = 1 Then
               vErro = vVB_Generica_001.ConectaOracle("PRODUCAO", "vda020", False, frmVda231)
            Else
               vErro = vVB_Generica_001.ConectaOracle("CD" & Format(CStr(Val(vObjOracle.Fields(0))), "00"), "vda020", False, frmVda231)
            End If
            Set vSessao = vVB_Generica_001.vSessao
            Set vBanco = vVB_Generica_001.vBanco
            vOwner = "HELPDESK."
            vDEP = "PRODUCAO."
        End If
        
        vBanco.Parameters.Remove "Cod_Soft": vBanco.Parameters.Add "Cod_Soft", Pegar_Cod_Sistema, 1
        vBanco.Parameters.Remove "Nome_Param": vBanco.Parameters.Add "Nome_Param", pNomeParam, 1
        vBanco.Parameters.Remove "VL_PARAM": vBanco.Parameters.Add "VL_PARAM", pValorParam, 1
        vBanco.Parameters.Remove "Dep": vBanco.Parameters.Add "DEP", vOwner, 1
        vBanco.Parameters.Remove "Txt_Erro": vBanco.Parameters.Add "Txt_Erro", "", 2
    
        If vVB_Generica_001.ExecutaPl(vBanco, vDEP & "PCK_VDA231.Pr_Salvar_Parametros(:Nome_Param, :VL_PARAM, :Cod_Soft, :DEP, :Txt_Erro)") <> "" Then
            lstDepositosOK.AddItem vObjOracle.Fields(0) & " - Erro"
        Else
            lstDepositosOK.AddItem vObjOracle.Fields(0) & " - OK"
        End If
Proximo:
        vObjOracle.MoveNext
    Next


End Sub


Sub Deletar_Canal(pNomeParam As String, pValorParam As String)

    Dim vOwner As String
    
    Set vObjOracle = vVB_Generica_001.TabelaLoja(vBanco)
    
    For i = 1 To vObjOracle.RecordCount
        If Val(vObjOracle.Fields(0)) = 0 Or Val(vObjOracle.Fields(0)) = 6 Or Val(vObjOracle.Fields(0)) = 15 Then GoTo Proximo
        
        If InStr(1, "02, 11, 12, 13, 16, 17, 18, 19", Format(CStr(Val(vObjOracle.Fields(0))), "00")) Then
           vErro = vVB_Generica_001.ConectaOracle("CDDPK", "vda020", False, frmVda231)
           vOwner = "DEP" & Format(Val(vObjOracle.Fields(0)), "00") & "."
           vDEP = vOwner
           Set vSessao = vVB_Generica_001.vSessao
           Set vBanco = vVB_Generica_001.vBanco
        Else
            If Val(vObjOracle.Fields(0)) = 1 Then
               vErro = vVB_Generica_001.ConectaOracle("PRODUCAO", "vda020", False, frmVda231)
            Else
               vErro = vVB_Generica_001.ConectaOracle("CD" & Format(CStr(Val(vObjOracle.Fields(0))), "00"), "vda020", False, frmVda231)
            End If
            Set vSessao = vVB_Generica_001.vSessao
            Set vBanco = vVB_Generica_001.vBanco
            vOwner = "HELPDESK."
            vDEP = "PRODUCAO."
        End If
        
        vBanco.Parameters.Remove "Cod_Soft": vBanco.Parameters.Add "Cod_Soft", Pegar_Cod_Sistema, 1
        vBanco.Parameters.Remove "Nome_Param": vBanco.Parameters.Add "Nome_Param", pNomeParam, 1
        vBanco.Parameters.Remove "VL_PARAM": vBanco.Parameters.Add "VL_PARAM", pValorParam, 1
        vBanco.Parameters.Remove "Dep": vBanco.Parameters.Add "DEP", vOwner, 1
        vBanco.Parameters.Remove "Txt_Erro": vBanco.Parameters.Add "Txt_Erro", "", 2

        If vVB_Generica_001.ExecutaPl(vBanco, vDEP & "PCK_VDA231.Pr_Deletar_Parametros(:Nome_Param, :Cod_Soft, :DEP, :Txt_Erro)") <> "" Then
            lstDepositosOK.AddItem vObjOracle.Fields(0) & " - Erro"
        Else
            lstDepositosOK.AddItem vObjOracle.Fields(0) & " - OK"
        End If
Proximo:
        vObjOracle.MoveNext
    Next

End Sub

Private Sub txtNumero_KeyPress(KeyAscii As Integer)
    If KeyAscii < 47 Or KeyAscii > 58 Then
        If KeyAscii <> 8 Then
            KeyAscii = 0
        End If
    End If
End Sub
