VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form frmVersoes 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Nome da tela"
   ClientHeight    =   7260
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5805
   Icon            =   "frmVersoes.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   484
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   387
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox txtCaminho 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   60
      TabIndex        =   8
      Text            =   "Z:\oracle\sistemas\vb\32bits\VDA230.EXE"
      Top             =   1110
      Width           =   5475
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   1560
      Top             =   60
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSComctlLib.ListView lsvVersoes 
      Height          =   5595
      Left            =   60
      TabIndex        =   2
      Top             =   1530
      Width           =   5715
      _ExtentX        =   10081
      _ExtentY        =   9869
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Usu�rio"
         Object.Width           =   2646
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Programa"
         Object.Width           =   2646
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   0
      Top             =   810
      Width           =   5685
      _ExtentX        =   10028
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmVersoes.frx":23D2
      PICN            =   "frmVersoes.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdConectar 
      Height          =   690
      Left            =   810
      TabIndex        =   3
      TabStop         =   0   'False
      ToolTipText     =   "Conectar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmVersoes.frx":30C8
      PICN            =   "frmVersoes.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdCaminho 
      Height          =   270
      Left            =   5550
      TabIndex        =   10
      TabStop         =   0   'False
      ToolTipText     =   "Caminho"
      Top             =   1110
      Width           =   240
      _ExtentX        =   423
      _ExtentY        =   476
      BTYPE           =   3
      TX              =   "..."
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmVersoes.frx":39BE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "Caminho do Arquivo:"
      Height          =   195
      Left            =   90
      TabIndex        =   9
      Top             =   900
      Width           =   1470
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      Caption         =   "Vers�o do Sistema:"
      Height          =   195
      Left            =   2970
      TabIndex        =   7
      Top             =   270
      Width           =   1365
   End
   Begin VB.Label lblVersao 
      AutoSize        =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   195
      Left            =   4530
      TabIndex        =   6
      Top             =   270
      Width           =   75
   End
   Begin VB.Label lblLigacaoAutomatica 
      AutoSize        =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   195
      Left            =   4530
      TabIndex        =   5
      Top             =   30
      Width           =   75
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Liga��o Autom�tica: "
      Height          =   195
      Left            =   2970
      TabIndex        =   4
      Top             =   30
      Width           =   1500
   End
End
Attribute VB_Name = "frmVersoes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public vVersoes As Boolean


Private Sub cmdCaminho_Click()
'   Dim strFile As String
    Dim udtFileInfo As FILEINFO
    
'    With CommonDialog1
'        .Filter = frmVda231.txtSistema & ".EXE"
'        .ShowOpen
'        strFile = .FileName
'        If Err.Number = cdlCancel Or strFile = "" Then Exit Sub
'    End With

    Me.txtCaminho = "Z:\Oracle\sistemas\vb\32bits\VDA230.EXE"

    If GetFileVersionInformation(txtCaminho, udtFileInfo) = eNoVersion Then
        MsgBox "No version available For this file", vbInformation
        Exit Sub
    End If
    
    lblVersao = udtFileInfo.ProductVersion
End Sub

Private Sub cmdConectar_Click()
    On Error Resume Next

    Dim udtFileInfo As FILEINFO
    
    Desconectar
    
    vVersoes = True
    dlgConexao.Show 1
    
    Desconectar
    
    Sleep (5000)
    
    fConectar frmVda231.vNumDep, True
    
    If GetFileVersionInformation(txtCaminho, udtFileInfo) = eNoVersion Then
        MsgBox "No version available For this file", vbInformation
        Exit Sub
    End If
    
    lblVersao = udtFileInfo.ProductVersion
    
End Sub

Private Sub cmdVoltar_Click()
    
    vVersoes = False
    
    Unload Me

End Sub
Private Sub Form_Load()
   
   Me.Top = (Screen.Height - Me.Height) / 2
   Me.Left = (Screen.Width - Me.Width) / 2
    
End Sub

Function fConectar(pCodLoja As Integer, pRetornarMensagemFalha As Boolean) As Boolean

    On Error GoTo Trata_Erro

    Dim vChecaDiretorio
    Dim vIP As String
    Dim vCompartilhamento As String
    Dim vUsuario As String
    Dim vSenha As String
    
    Dim vBanco_Prod As Object
    
    vErro = vVB_Generica_001.ConectaOracle("PRODUCAO", "VDA020", False, Me)

    If vErro = "" Then
        Set vBanco_Prod = vVB_Generica_001.vBanco
    End If

    fConectar = False

    Call vVB_Generica_001.ExcluiBind(vBanco)

    vBanco_Prod.Parameters.Add "pAcao", 1, 1
    vBanco_Prod.Parameters.Add "pCodLoja", Val(pCodLoja), 1
    vBanco_Prod.Parameters.Add "pIP", "", 1
    vBanco_Prod.Parameters.Add "pCompartilhamento", "", 1
    vBanco_Prod.Parameters.Add "pUsuario", "", 1
    vBanco_Prod.Parameters.Add "pSenha", "", 1

    vBanco_Prod.Parameters.Add "pRetorno1", 0, 2
    vBanco_Prod.Parameters("pRetorno1").ServerType = 102
    vBanco_Prod.Parameters("pRetorno1").DynasetOption = &H2&
    vBanco_Prod.Parameters("pRetorno1").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco_Prod.Parameters.Add "pCodErro", 0, 2
    vBanco_Prod.Parameters.Add "pMsgErro", "", 2

    vSql = _
            "PRODUCAO.PCK_CON001.PRCADASTRO(" & _
            ":pAcao, " & _
            ":pCodLoja, " & _
            ":pIP, " & _
            ":pCompartilhamento, " & _
            ":pUsuario, " & _
            ":pSenha, " & _
            ":pRetorno1, " & _
            ":pCodErro, " & _
            ":pMsgErro)"

    vBanco_Prod.ExecuteSQL "Begin " & vSql & "; End;"

    If vBanco_Prod.Parameters("pCodErro") <> 0 Then

        vErro = "Ocorreu um erro" & Chr(13) & _
                "C�d. do erro: " & CStr(vBanco_Prod.Parameters("pCodErro")) & Chr(13) & _
                "Msg. de erro: " & CStr(vBanco_Prod.Parameters("pMsgErro"))

        Call vVB_Generica_001.ProcessaErro(vErro)
        Exit Function

    End If

    Set vObjOracle = vBanco_Prod.Parameters("pRetorno1").Value

    If Not vObjOracle.EOF Then

        vIP = CStr(vObjOracle.Fields(0))
        vCompartilhamento = CStr(vObjOracle.Fields(1))
        vUsuario = CStr(vObjOracle.Fields(2))
        vSenha = CStr(vObjOracle.Fields(3))
        
        theNetResource.lpRemoteName = "\\" & vIP & vCompartilhamento
        UserName = vUsuario
        UserPassword = vSenha
        theNetResource.dwType = RESOURCETYPE_DISK

        Dim Result&
        Result = WNetAddConnection2(theNetResource, UserPassword, UserName, 0)
        
        'vComando = "NET USE /PERSISTENT:NO Z: \\" & vIP & vCompartilhamento & " /USER:" & vUsuario & " " & vSenha
        'ExecutarEsperar vComando
        'Call Sleep(TEMPO_ESPERA)

        vChecaDiretorio = Dir("Z:\", vbDirectory)

        If vChecaDiretorio = "" Then

            If pRetornarMensagemFalha = True Then

                Call vVB_Generica_001.Informar("N�o foi poss�vel conectar no dep�sito com as informa��es digitadas.")

            End If

            Exit Function

        Else

            Open "Z:\ORACLE\DADOS\32BITS\CD.TXT" For Input As #1
            Line Input #1, vLinhaArquivo
            Close #1

            If Val(Mid(vLinhaArquivo, 1, 2)) <> pCodLoja Then

                If pRetornarMensagemFalha = True Then

                    Call vVB_Generica_001.Informar("N�o foi poss�vel conectar no dep�sito com as informa��es digitadas.")

                End If

                Exit Function

            End If

            fConectar = True

        End If
        
    End If
Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "C�digo:" & Err.Number & vbCrLf & "Descri��o: " & Err.Description
    End If
End Function

