VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form frmPermissao 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Permiss�es"
   ClientHeight    =   5805
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5550
   Icon            =   "frmPermissao.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5805
   ScaleWidth      =   5550
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ListView lsvPermissoes 
      Height          =   2955
      Left            =   60
      TabIndex        =   13
      Top             =   2790
      Width           =   5475
      _ExtentX        =   9657
      _ExtentY        =   5212
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   6
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Login"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Senha"
         Object.Width           =   1411
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   2
         SubItemIndex    =   2
         Text            =   "PABX"
         Object.Width           =   1411
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   2
         SubItemIndex    =   3
         Text            =   "Lig.Auto/NF"
         Object.Width           =   1940
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   2
         SubItemIndex    =   4
         Text            =   "Tlmkt"
         Object.Width           =   1411
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   2
         SubItemIndex    =   5
         Text            =   "Tempo"
         Object.Width           =   1411
      EndProperty
   End
   Begin VB.Frame Frame1 
      Caption         =   "Permiss�es"
      Height          =   1305
      Left            =   60
      TabIndex        =   10
      Top             =   930
      Width           =   5415
      Begin VB.CheckBox Check1 
         Appearance      =   0  'Flat
         Caption         =   "Parametros PABX"
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   90
         TabIndex        =   0
         Tag             =   "P"
         Top             =   210
         Width           =   1665
      End
      Begin VB.CheckBox Check2 
         Appearance      =   0  'Flat
         Caption         =   "Ligacao Automatica / End NF"
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   90
         TabIndex        =   1
         Tag             =   "L"
         Top             =   450
         Width           =   2505
      End
      Begin VB.CheckBox Check3 
         Appearance      =   0  'Flat
         Caption         =   "Tirar TLMKT da Ligacao Automatica"
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   90
         TabIndex        =   2
         Tag             =   "T"
         Top             =   690
         Width           =   2895
      End
      Begin VB.CheckBox Check4 
         Appearance      =   0  'Flat
         Caption         =   "Tempo para Marcar Liga��o"
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   90
         TabIndex        =   3
         Tag             =   "M"
         Top             =   930
         Width           =   2505
      End
      Begin VB.TextBox txtLogin 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   3570
         TabIndex        =   4
         Top             =   360
         Width           =   1785
      End
      Begin VB.TextBox txtSenha 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   3570
         MaxLength       =   4
         TabIndex        =   5
         Top             =   900
         Width           =   1035
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Login:"
         Height          =   195
         Left            =   3570
         TabIndex        =   12
         Top             =   150
         Width           =   435
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Senha:"
         Height          =   195
         Left            =   3570
         TabIndex        =   11
         Top             =   690
         Width           =   510
      End
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   8
      Top             =   810
      Width           =   5475
      _ExtentX        =   9657
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   9
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmPermissao.frx":23D2
      PICN            =   "frmPermissao.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdSalvar 
      Height          =   495
      Left            =   90
      TabIndex        =   6
      TabStop         =   0   'False
      ToolTipText     =   "Salvar"
      Top             =   2280
      Width           =   495
      _ExtentX        =   873
      _ExtentY        =   873
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmPermissao.frx":30C8
      PICN            =   "frmPermissao.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdExcluirEmail 
      Height          =   495
      Left            =   600
      TabIndex        =   7
      TabStop         =   0   'False
      ToolTipText     =   "Excluir"
      Top             =   2280
      Width           =   495
      _ExtentX        =   873
      _ExtentY        =   873
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmPermissao.frx":367E
      PICN            =   "frmPermissao.frx":369A
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmPermissao"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdExcluirEmail_Click()
    If Me.lsvPermissoes.SelectedItem <> "" Then
        If MsgBox("Confirma exclus�o ?", vbQuestion + vbYesNo, "Aten��o") = vbYes Then
            vBanco.ExecuteSQL "BEGIN DELETE FROM HELPDESK.PARAMETROS WHERE COD_SOFTWARE = 1069 AND NOME_PARAMETRO = 'PERMISSAO' AND VL_PARAMETRO = '" & txtLogin & ";" & txtSenha & ";" & IIf(Check1.Value = 1, Check1.Tag, "") & ";" & IIf(Check2.Value = 1, Check2.Tag, "") & ";" & IIf(Check3.Value = 1, Check3.Tag, "") & ";" & IIf(Check4.Value = 1, Check4.Tag, "") & ";" & IIf(Check5.Value = 1, Check5.Tag, "") & "'; COMMIT; END;"
            Preencher_Permissoes
            txtLogin = ""
            txtSenha = ""
            Check1.Value = 0
            Check2.Value = 0
            Check3.Value = 0
            Check4.Value = 0
            Check5.Value = 0
        End If
    Else
        MsgBox "Selecione um item da lista.", vbInformation, "Aten��o"
    End If
End Sub

Private Sub cmdSalvar_Click()
    If txtLogin = "" Then
        MsgBox "Informe o Login.", vbInformation, "Aten��o"
        txtLogin.SetFocus
        Exit Sub
    End If
    
    vBanco.ExecuteSQL "BEGIN DELETE FROM HELPDESK.PARAMETROS WHERE COD_SOFTWARE = 1069 AND NOME_PARAMETRO='PERMISSAO' AND VL_PARAMETRO LIKE '" & txtLogin & ";" & txtSenha & "%'; COMMIT; END;"
    vBanco.ExecuteSQL "BEGIN INSERT INTO HELPDESK.PARAMETROS VALUES (1069, 'PERMISSAO','" & txtLogin & ";" & txtSenha & ";" & IIf(Check1.Value = 1, Check1.Tag, "") & ";" & IIf(Check2.Value = 1, Check2.Tag, "") & ";" & IIf(Check3.Value = 1, Check3.Tag, "") & ";" & IIf(Check4.Value = 1, Check4.Tag, "") & "'); COMMIT; END;"
    
    Preencher_Permissoes
    
    txtLogin = ""
    txtSenha = ""
    Check1.Value = 0
    Check2.Value = 0
    Check3.Value = 0
    Check4.Value = 0
    
End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub

Private Sub lstPermissao_Click()
    txtLogin = Left(lstPermissao, InStr(1, lstPermissao, "/") - 1)
    senha = Right(lstPermissao, InStr(1, lstPermissao, "/") + 1)
    If Right(lstPermissao, 7) <> "" Then
        If InStr(1, lstPermissao, "1") > 0 Then
            Check1.Value = 1
        ElseIf InStr(1, lstPermissao, "2") > 0 Then
            Check2.Value = 1
        ElseIf InStr(1, lstPermissao, "3") > 0 Then
            Check3.Value = 1
        ElseIf InStr(1, lstPermissao, "4") > 0 Then
            Check4.Value = 1
        End If
    End If
            
End Sub

Sub Preencher_Permissoes()

    Dim vrstPermissao As Object
    Dim lItem As ListItem
    Dim vColunas
    
    Set vrstPermissao = vBanco.CreateDynaset("Select vl_parametro from Helpdesk.parametros where cod_software=1069 and nome_parametro='PERMISSAO'", 0&)

    lsvPermissoes.ListItems.Clear

    For i = 1 To vrstPermissao.RecordCount
            
        vColunas = Split(vrstPermissao!vl_parametro, ";")
        
        Set lItem = lsvPermissoes.ListItems.Add
        lItem = vColunas(0)
        lItem.SubItems(1) = vColunas(1)
        lItem.SubItems(2) = IIf(vColunas(2) = "", "", "X")
        lItem.SubItems(3) = IIf(vColunas(3) = "", "", "X")
        lItem.SubItems(4) = IIf(vColunas(4) = "", "", "X")
        lItem.SubItems(5) = IIf(vColunas(5) = "", "", "X")

        vrstPermissao.MoveNext
    Next

    vrstPermissao.Close
    Set vrstPermissao = Nothing

End Sub


Private Sub Form_Load()
    Preencher_Permissoes
End Sub

Private Sub lsvPermissoes_ItemClick(ByVal Item As MSComctlLib.ListItem)
    
    txtLogin = ""
    txtSenha = ""
    Check1.Value = 0
    Check2.Value = 0
    Check3.Value = 0
    Check4.Value = 0
    
    txtLogin = lsvPermissoes.SelectedItem
    txtSenha = lsvPermissoes.SelectedItem.SubItems(1)
    If lsvPermissoes.SelectedItem.SubItems(2) = "X" Then Me.Check1.Value = 1
    If lsvPermissoes.SelectedItem.SubItems(3) = "X" Then Me.Check2.Value = 1
    If lsvPermissoes.SelectedItem.SubItems(4) = "X" Then Me.Check3.Value = 1
    If lsvPermissoes.SelectedItem.SubItems(5) = "X" Then Me.Check4.Value = 1
    
End Sub

Private Sub txtLogin_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub txtSenha_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub
