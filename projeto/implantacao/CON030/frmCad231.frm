VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmVda231 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "CON030- Parametros"
   ClientHeight    =   8835
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9120
   Icon            =   "frmCad231.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   8835
   ScaleWidth      =   9120
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame FraBotoes 
      Height          =   1215
      Left            =   1410
      TabIndex        =   61
      Top             =   0
      Width           =   6675
      Begin VB.TextBox txtSistema 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   480
         Left            =   60
         TabIndex        =   62
         Text            =   "VDA230"
         Top             =   600
         Width           =   1305
      End
      Begin Bot�o.cmd cmdSalvar 
         Height          =   1005
         Left            =   5670
         TabIndex        =   63
         TabStop         =   0   'False
         ToolTipText     =   "Salvar"
         Top             =   150
         Width           =   945
         _ExtentX        =   1667
         _ExtentY        =   1773
         BTYPE           =   3
         TX              =   "Gravar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCad231.frx":23D2
         PICN            =   "frmCad231.frx":23EE
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdConectar 
         Height          =   1005
         Left            =   3690
         TabIndex        =   64
         TabStop         =   0   'False
         ToolTipText     =   "Conectar"
         Top             =   150
         Width           =   945
         _ExtentX        =   1667
         _ExtentY        =   1773
         BTYPE           =   3
         TX              =   "Conectar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCad231.frx":30C8
         PICN            =   "frmCad231.frx":30E4
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdVersoes 
         Height          =   1005
         Left            =   4680
         TabIndex        =   65
         TabStop         =   0   'False
         ToolTipText     =   "Vers�o sendo utilizada"
         Top             =   150
         Width           =   945
         _ExtentX        =   1667
         _ExtentY        =   1773
         BTYPE           =   3
         TX              =   "Vers�es"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCad231.frx":39BE
         PICN            =   "frmCad231.frx":39DA
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdEmail 
         Height          =   1005
         Left            =   2700
         TabIndex        =   66
         Top             =   150
         Width           =   945
         _ExtentX        =   1667
         _ExtentY        =   1773
         BTYPE           =   3
         TX              =   "Email"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCad231.frx":46B4
         PICN            =   "frmCad231.frx":46D0
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdPermissao 
         Height          =   1005
         Left            =   1410
         TabIndex        =   67
         Top             =   150
         Width           =   1245
         _ExtentX        =   2196
         _ExtentY        =   1773
         BTYPE           =   3
         TX              =   "Permiss�es"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCad231.frx":53AA
         PICN            =   "frmCad231.frx":53C6
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         Caption         =   "Nome Sistema:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   60
         TabIndex        =   68
         Top             =   360
         Width           =   1275
      End
   End
   Begin TabDlg.SSTab TabParam 
      Height          =   6885
      Left            =   60
      TabIndex        =   2
      Top             =   1470
      Width           =   9015
      _ExtentX        =   15901
      _ExtentY        =   12144
      _Version        =   393216
      Tabs            =   8
      Tab             =   7
      TabsPerRow      =   2
      TabHeight       =   520
      TabCaption(0)   =   "Parametros PABX"
      TabPicture(0)   =   "frmCad231.frx":60A0
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "lstCidadesSemDDD"
      Tab(0).Control(1)=   "Frame1"
      Tab(0).Control(2)=   "Frame2"
      Tab(0).Control(3)=   "lstDepositosOK"
      Tab(0).Control(4)=   "txtCaminhoArquivoTexto"
      Tab(0).Control(5)=   "cmdGerar"
      Tab(0).Control(6)=   "lblDeposito"
      Tab(0).Control(7)=   "lbl1"
      Tab(0).Control(8)=   "lblQtd"
      Tab(0).Control(9)=   "Label1"
      Tab(0).Control(10)=   "Label10"
      Tab(0).Control(11)=   "Label11"
      Tab(0).ControlCount=   12
      TabCaption(1)   =   "Liga��o Auto / Endereco NF"
      TabPicture(1)   =   "frmCad231.frx":60BC
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame3"
      Tab(1).Control(1)=   "FraLigacao"
      Tab(1).Control(2)=   "cboCds"
      Tab(1).Control(3)=   "Label12"
      Tab(1).ControlCount=   4
      TabCaption(2)   =   "Tirar Tlmkt da Ligacao Auto"
      TabPicture(2)   =   "frmCad231.frx":60D8
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Frame4"
      Tab(2).ControlCount=   1
      TabCaption(3)   =   "Tempo para Marcar Liga��o"
      TabPicture(3)   =   "frmCad231.frx":60F4
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "Label15"
      Tab(3).Control(1)=   "Label16"
      Tab(3).Control(2)=   "Label17"
      Tab(3).Control(3)=   "Label18"
      Tab(3).Control(4)=   "cmdSalvarTempo"
      Tab(3).Control(5)=   "txtTempo"
      Tab(3).Control(6)=   "cboCdTempo"
      Tab(3).Control(7)=   "LSTTEMPOS"
      Tab(3).ControlCount=   8
      TabCaption(4)   =   "Habilitar/Desabilitar LINK Producao"
      TabPicture(4)   =   "frmCad231.frx":6110
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "Frame5"
      Tab(4).ControlCount=   1
      TabCaption(5)   =   "Habilitar/Desabilitar LINK IDPK"
      TabPicture(5)   =   "frmCad231.frx":612C
      Tab(5).ControlEnabled=   0   'False
      Tab(5).Control(0)=   "Frame6"
      Tab(5).ControlCount=   1
      TabCaption(6)   =   "Promtor receber Roteiro"
      TabPicture(6)   =   "frmCad231.frx":6148
      Tab(6).ControlEnabled=   0   'False
      Tab(6).Control(0)=   "cmdAdicionarPromotor"
      Tab(6).Control(1)=   "txtCodPromotor"
      Tab(6).Control(2)=   "lstPromotores"
      Tab(6).Control(3)=   "cmdExcluirPromotor"
      Tab(6).Control(4)=   "cmdGravarPromotores"
      Tab(6).Control(5)=   "Label21"
      Tab(6).ControlCount=   6
      TabCaption(7)   =   "CDs que n�o imprimem a s�rie na NF"
      TabPicture(7)   =   "frmCad231.frx":6164
      Tab(7).ControlEnabled=   -1  'True
      Tab(7).Control(0)=   "lblStatusSerie"
      Tab(7).Control(0).Enabled=   0   'False
      Tab(7).Control(1)=   "Frame9"
      Tab(7).Control(1).Enabled=   0   'False
      Tab(7).ControlCount=   2
      Begin VB.Frame Frame9 
         Caption         =   "N�o Imprimir a S�rie na NF para os CDs Selecionados"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   5415
         Left            =   90
         TabIndex        =   96
         Top             =   1320
         Width           =   4815
         Begin Bot�o.cmd cmdSalvarSerie 
            Height          =   555
            Left            =   3060
            TabIndex        =   98
            Top             =   4740
            Width           =   1665
            _ExtentX        =   2937
            _ExtentY        =   979
            BTYPE           =   3
            TX              =   "Salvar"
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   -1  'True
            BCOL            =   13160660
            BCOLO           =   13160660
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "frmCad231.frx":6180
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   0
            NGREY           =   0   'False
            FX              =   0
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin VB.ListBox lstSerie 
            Appearance      =   0  'Flat
            Height          =   4980
            Left            =   60
            Style           =   1  'Checkbox
            TabIndex        =   97
            Top             =   270
            Width           =   2265
         End
      End
      Begin Bot�o.cmd cmdAdicionarPromotor 
         Height          =   375
         Left            =   -73620
         TabIndex        =   93
         Top             =   1440
         Width           =   375
         _ExtentX        =   661
         _ExtentY        =   661
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCad231.frx":619C
         PICN            =   "frmCad231.frx":61B8
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.TextBox txtCodPromotor 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   -74880
         TabIndex        =   91
         Top             =   1470
         Width           =   1215
      End
      Begin VB.ListBox lstPromotores 
         Appearance      =   0  'Flat
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1046
            SubFormatType   =   1
         EndProperty
         Height          =   4905
         Left            =   -74880
         Sorted          =   -1  'True
         TabIndex        =   90
         Top             =   1830
         Width           =   1215
      End
      Begin VB.Frame Frame6 
         Height          =   4995
         Left            =   -74910
         TabIndex        =   80
         Top             =   1320
         Width           =   8565
         Begin VB.ComboBox CboCdsLinkIDPK 
            Height          =   315
            Left            =   60
            Style           =   2  'Dropdown List
            TabIndex        =   87
            Top             =   420
            Width           =   3375
         End
         Begin VB.Frame Frame8 
            Caption         =   "Ativar ou Desativar o Link para Sistema VDA230 - Telemarketing"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   2865
            Left            =   60
            TabIndex        =   81
            Top             =   810
            Width           =   8445
            Begin VB.ListBox lstIDPK 
               Appearance      =   0  'Flat
               Height          =   2280
               Left            =   90
               Style           =   1  'Checkbox
               TabIndex        =   82
               Top             =   240
               Width           =   2175
            End
            Begin Bot�o.cmd cmdAtivarIDPK 
               Height          =   555
               Left            =   2310
               TabIndex        =   83
               Top             =   1500
               Width           =   1785
               _ExtentX        =   3149
               _ExtentY        =   979
               BTYPE           =   3
               TX              =   "ATIVAR"
               ENAB            =   0   'False
               BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               COLTYPE         =   1
               FOCUSR          =   -1  'True
               BCOL            =   13160660
               BCOLO           =   13160660
               FCOL            =   0
               FCOLO           =   0
               MCOL            =   12632256
               MPTR            =   1
               MICON           =   "frmCad231.frx":6574
               UMCOL           =   -1  'True
               SOFT            =   0   'False
               PICPOS          =   0
               NGREY           =   0   'False
               FX              =   0
               HAND            =   0   'False
               CHECK           =   0   'False
               VALUE           =   0   'False
            End
            Begin Bot�o.cmd cmdDesativarIDPK 
               Height          =   555
               Left            =   4470
               TabIndex        =   84
               Top             =   1500
               Width           =   1785
               _ExtentX        =   3149
               _ExtentY        =   979
               BTYPE           =   3
               TX              =   "DESATIVAR"
               ENAB            =   0   'False
               BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               COLTYPE         =   1
               FOCUSR          =   -1  'True
               BCOL            =   13160660
               BCOLO           =   13160660
               FCOL            =   0
               FCOLO           =   0
               MCOL            =   12632256
               MPTR            =   1
               MICON           =   "frmCad231.frx":6590
               UMCOL           =   -1  'True
               SOFT            =   0   'False
               PICPOS          =   0
               NGREY           =   0   'False
               FX              =   0
               HAND            =   0   'False
               CHECK           =   0   'False
               VALUE           =   0   'False
            End
            Begin Bot�o.cmd cmdInserirIDPK 
               Height          =   555
               Left            =   6570
               TabIndex        =   85
               Top             =   1500
               Width           =   1785
               _ExtentX        =   3149
               _ExtentY        =   979
               BTYPE           =   3
               TX              =   "INSERIR"
               ENAB            =   0   'False
               BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               COLTYPE         =   1
               FOCUSR          =   -1  'True
               BCOL            =   13160660
               BCOLO           =   13160660
               FCOL            =   0
               FCOLO           =   0
               MCOL            =   12632256
               MPTR            =   1
               MICON           =   "frmCad231.frx":65AC
               UMCOL           =   -1  'True
               SOFT            =   0   'False
               PICPOS          =   0
               NGREY           =   0   'False
               FX              =   0
               HAND            =   0   'False
               CHECK           =   0   'False
               VALUE           =   0   'False
            End
            Begin VB.Label lblMensagemLinkIDPK 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   1215
               Left            =   2310
               TabIndex        =   86
               Top             =   240
               Width           =   6045
            End
         End
         Begin VB.Label Label23 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Os Dep�sitos que est�o marcados significa que est�o com o LINK Ativo"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   735
            Left            =   150
            TabIndex        =   89
            Top             =   4050
            Width           =   8205
         End
         Begin VB.Label Label22 
            AutoSize        =   -1  'True
            Caption         =   "CDs:"
            Height          =   195
            Left            =   30
            TabIndex        =   88
            Top             =   210
            Width           =   345
         End
      End
      Begin VB.Frame Frame5 
         Height          =   4995
         Left            =   -74940
         TabIndex        =   70
         Top             =   1290
         Width           =   8565
         Begin VB.Frame Frame7 
            Caption         =   "Ativar ou Desativar o Link para Sistema VDA230 - Telemarketing"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   2865
            Left            =   60
            TabIndex        =   72
            Top             =   810
            Width           =   8445
            Begin VB.ListBox lstLink 
               Appearance      =   0  'Flat
               Height          =   2280
               Left            =   90
               Style           =   1  'Checkbox
               TabIndex        =   73
               Top             =   240
               Width           =   2175
            End
            Begin Bot�o.cmd cmdAtivarLink 
               Height          =   555
               Left            =   2310
               TabIndex        =   74
               Top             =   1500
               Width           =   1755
               _ExtentX        =   3096
               _ExtentY        =   979
               BTYPE           =   3
               TX              =   "ATIVAR"
               ENAB            =   0   'False
               BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               COLTYPE         =   1
               FOCUSR          =   -1  'True
               BCOL            =   13160660
               BCOLO           =   13160660
               FCOL            =   0
               FCOLO           =   0
               MCOL            =   12632256
               MPTR            =   1
               MICON           =   "frmCad231.frx":65C8
               UMCOL           =   -1  'True
               SOFT            =   0   'False
               PICPOS          =   0
               NGREY           =   0   'False
               FX              =   0
               HAND            =   0   'False
               CHECK           =   0   'False
               VALUE           =   0   'False
            End
            Begin Bot�o.cmd cmdDesativarLink 
               Height          =   555
               Left            =   4470
               TabIndex        =   75
               Top             =   1500
               Width           =   1755
               _ExtentX        =   3096
               _ExtentY        =   979
               BTYPE           =   3
               TX              =   "DESATIVAR"
               ENAB            =   0   'False
               BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               COLTYPE         =   1
               FOCUSR          =   -1  'True
               BCOL            =   13160660
               BCOLO           =   13160660
               FCOL            =   0
               FCOLO           =   0
               MCOL            =   12632256
               MPTR            =   1
               MICON           =   "frmCad231.frx":65E4
               UMCOL           =   -1  'True
               SOFT            =   0   'False
               PICPOS          =   0
               NGREY           =   0   'False
               FX              =   0
               HAND            =   0   'False
               CHECK           =   0   'False
               VALUE           =   0   'False
            End
            Begin Bot�o.cmd cmdInserirLink 
               Height          =   555
               Left            =   6600
               TabIndex        =   76
               Top             =   1500
               Width           =   1755
               _ExtentX        =   3096
               _ExtentY        =   979
               BTYPE           =   3
               TX              =   "INSERIR"
               ENAB            =   0   'False
               BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               COLTYPE         =   1
               FOCUSR          =   -1  'True
               BCOL            =   13160660
               BCOLO           =   13160660
               FCOL            =   0
               FCOLO           =   0
               MCOL            =   12632256
               MPTR            =   1
               MICON           =   "frmCad231.frx":6600
               UMCOL           =   -1  'True
               SOFT            =   0   'False
               PICPOS          =   0
               NGREY           =   0   'False
               FX              =   0
               HAND            =   0   'False
               CHECK           =   0   'False
               VALUE           =   0   'False
            End
            Begin VB.Label lblMensagemLink 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   1215
               Left            =   2310
               TabIndex        =   77
               Top             =   240
               Width           =   6045
            End
         End
         Begin VB.ComboBox cboCdsLink 
            Height          =   315
            Left            =   60
            Style           =   2  'Dropdown List
            TabIndex        =   71
            Top             =   420
            Width           =   3375
         End
         Begin VB.Label Label20 
            AutoSize        =   -1  'True
            Caption         =   "CDs:"
            Height          =   195
            Left            =   30
            TabIndex        =   79
            Top             =   210
            Width           =   345
         End
         Begin VB.Label Label19 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Os Dep�sitos que est�o marcados significa que est�o com o LINK Ativo"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   735
            Left            =   150
            TabIndex        =   78
            Top             =   4050
            Width           =   8205
         End
      End
      Begin VB.Frame Frame4 
         Height          =   2295
         Left            =   -74880
         TabIndex        =   54
         Top             =   1350
         Width           =   3705
         Begin VB.ComboBox cboCDsRamal 
            Height          =   315
            Left            =   60
            Style           =   2  'Dropdown List
            TabIndex        =   56
            Top             =   390
            Width           =   3375
         End
         Begin VB.TextBox txtTLMKT 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   90
            TabIndex        =   55
            Top             =   1170
            Width           =   1305
         End
         Begin Bot�o.cmd cmdAtivar 
            Height          =   555
            Left            =   60
            TabIndex        =   57
            Top             =   1590
            Width           =   1545
            _ExtentX        =   2725
            _ExtentY        =   979
            BTYPE           =   3
            TX              =   "Ativar"
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   -1  'True
            BCOL            =   13160660
            BCOLO           =   13160660
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "frmCad231.frx":661C
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   0
            NGREY           =   0   'False
            FX              =   0
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin Bot�o.cmd cmdDesativar 
            Height          =   555
            Left            =   1920
            TabIndex        =   58
            Top             =   1590
            Width           =   1545
            _ExtentX        =   2725
            _ExtentY        =   979
            BTYPE           =   3
            TX              =   "Desativar"
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   -1  'True
            BCOL            =   13160660
            BCOLO           =   13160660
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "frmCad231.frx":6638
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   0
            NGREY           =   0   'False
            FX              =   0
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin VB.Label Label13 
            AutoSize        =   -1  'True
            Caption         =   "CDs:"
            Height          =   195
            Left            =   60
            TabIndex        =   60
            Top             =   180
            Width           =   345
         End
         Begin VB.Label Label14 
            Caption         =   "C�digo do TLMKT:"
            Height          =   225
            Left            =   90
            TabIndex        =   59
            Top             =   960
            Width           =   1365
         End
      End
      Begin VB.ListBox LSTTEMPOS 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4710
         Left            =   -71310
         TabIndex        =   51
         Top             =   1620
         Width           =   4725
      End
      Begin VB.ComboBox cboCdTempo 
         Height          =   315
         Left            =   -74790
         Style           =   2  'Dropdown List
         TabIndex        =   47
         Top             =   1620
         Width           =   3375
      End
      Begin VB.TextBox txtTempo 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   -74790
         TabIndex        =   46
         Top             =   2580
         Width           =   765
      End
      Begin VB.Frame Frame3 
         Caption         =   "Imprimir Endere�o na NF de Sa�da"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2835
         Left            =   -74880
         TabIndex        =   37
         Top             =   4530
         Width           =   8295
         Begin VB.ListBox lstEnderecoNF 
            Appearance      =   0  'Flat
            Height          =   2505
            Left            =   60
            Style           =   1  'Checkbox
            TabIndex        =   45
            Top             =   270
            Width           =   2265
         End
         Begin Bot�o.cmd cmdStatusNFativar 
            Height          =   555
            Left            =   2400
            TabIndex        =   38
            Top             =   1530
            Width           =   1785
            _ExtentX        =   3149
            _ExtentY        =   979
            BTYPE           =   3
            TX              =   "ATIVAR"
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   -1  'True
            BCOL            =   13160660
            BCOLO           =   13160660
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "frmCad231.frx":6654
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   0
            NGREY           =   0   'False
            FX              =   0
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin Bot�o.cmd cmdStatusNFDesAtivar 
            Height          =   555
            Left            =   4410
            TabIndex        =   41
            Top             =   1530
            Width           =   1785
            _ExtentX        =   3149
            _ExtentY        =   979
            BTYPE           =   3
            TX              =   "DESATIVAR"
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   -1  'True
            BCOL            =   13160660
            BCOLO           =   13160660
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "frmCad231.frx":6670
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   0
            NGREY           =   0   'False
            FX              =   0
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin Bot�o.cmd cmdStatusNFAdicionar 
            Height          =   555
            Left            =   6420
            TabIndex        =   43
            Top             =   1530
            Width           =   1785
            _ExtentX        =   3149
            _ExtentY        =   979
            BTYPE           =   3
            TX              =   "INSERIR"
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   -1  'True
            BCOL            =   13160660
            BCOLO           =   13160660
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "frmCad231.frx":668C
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   0
            NGREY           =   0   'False
            FX              =   0
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin VB.Label lblStatusNF 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   1155
            Left            =   2400
            TabIndex        =   39
            Top             =   300
            Width           =   5805
         End
      End
      Begin VB.Frame FraLigacao 
         Caption         =   "Liga��o Autom�tica"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2595
         Left            =   -74880
         TabIndex        =   34
         Top             =   1860
         Width           =   8235
         Begin VB.ListBox lstLigAuto 
            Appearance      =   0  'Flat
            Height          =   2280
            Left            =   60
            Style           =   1  'Checkbox
            TabIndex        =   44
            Top             =   240
            Width           =   2325
         End
         Begin Bot�o.cmd cmdStatusLigAutoAtivar 
            Height          =   555
            Left            =   2430
            TabIndex        =   35
            Top             =   1140
            Width           =   1755
            _ExtentX        =   3096
            _ExtentY        =   979
            BTYPE           =   3
            TX              =   "ATIVAR"
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   -1  'True
            BCOL            =   13160660
            BCOLO           =   13160660
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "frmCad231.frx":66A8
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   0
            NGREY           =   0   'False
            FX              =   0
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin Bot�o.cmd cmdStatusLigAutoDesativar 
            Height          =   555
            Left            =   4440
            TabIndex        =   40
            Top             =   1140
            Width           =   1755
            _ExtentX        =   3096
            _ExtentY        =   979
            BTYPE           =   3
            TX              =   "DESATIVAR"
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   -1  'True
            BCOL            =   13160660
            BCOLO           =   13160660
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "frmCad231.frx":66C4
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   0
            NGREY           =   0   'False
            FX              =   0
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin Bot�o.cmd cmdStatusLigAutoAdicionar 
            Height          =   555
            Left            =   6390
            TabIndex        =   42
            Top             =   1140
            Width           =   1755
            _ExtentX        =   3096
            _ExtentY        =   979
            BTYPE           =   3
            TX              =   "INSERIR"
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   -1  'True
            BCOL            =   13160660
            BCOLO           =   13160660
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "frmCad231.frx":66E0
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   0
            NGREY           =   0   'False
            FX              =   0
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin VB.Label lblStatus 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   795
            Left            =   2430
            TabIndex        =   36
            Top             =   270
            Width           =   5715
         End
      End
      Begin VB.ComboBox cboCds 
         Height          =   315
         Left            =   -74880
         Style           =   2  'Dropdown List
         TabIndex        =   32
         Top             =   1500
         Width           =   3375
      End
      Begin VB.ListBox lstCidadesSemDDD 
         Appearance      =   0  'Flat
         Height          =   2055
         Left            =   -71625
         Style           =   1  'Checkbox
         TabIndex        =   24
         Top             =   2190
         Width           =   4875
      End
      Begin VB.Frame Frame1 
         Caption         =   "Par�metros do PABX"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   2265
         Left            =   -74910
         TabIndex        =   13
         Top             =   1980
         Width           =   3075
         Begin VB.TextBox txtPorta3000 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   1815
            TabIndex        =   18
            Top             =   1080
            Width           =   975
         End
         Begin VB.TextBox txtPorta4000 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   1815
            TabIndex        =   17
            Top             =   540
            Width           =   975
         End
         Begin VB.TextBox txtIP3000 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   105
            TabIndex        =   16
            Top             =   1080
            Width           =   1635
         End
         Begin VB.TextBox txtIP4000 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   105
            TabIndex        =   15
            Top             =   540
            Width           =   1635
         End
         Begin VB.ComboBox cboConectado 
            Appearance      =   0  'Flat
            Height          =   315
            ItemData        =   "frmCad231.frx":66FC
            Left            =   135
            List            =   "frmCad231.frx":6706
            Style           =   2  'Dropdown List
            TabIndex        =   14
            Top             =   1740
            Width           =   735
         End
         Begin VB.Label Label8 
            AutoSize        =   -1  'True
            Caption         =   "Trabalhar Conectado"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   120
            TabIndex        =   23
            Top             =   1500
            Width           =   1800
         End
         Begin VB.Label Label7 
            AutoSize        =   -1  'True
            Caption         =   "Porta"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   1800
            TabIndex        =   22
            Top             =   870
            Width           =   465
         End
         Begin VB.Label Label5 
            AutoSize        =   -1  'True
            Caption         =   "Porta"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   1800
            TabIndex        =   21
            Top             =   330
            Width           =   465
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "IP HP3000"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   90
            TabIndex        =   20
            Top             =   870
            Width           =   930
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            Caption         =   "IP HP4000"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   90
            TabIndex        =   19
            Top             =   330
            Width           =   930
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Canal de Voz"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   2535
         Left            =   -74910
         TabIndex        =   5
         Top             =   4410
         Visible         =   0   'False
         Width           =   4665
         Begin VB.TextBox txtNumero 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   2820
            TabIndex        =   7
            Top             =   390
            Width           =   1755
         End
         Begin VB.TextBox txtDeposito 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   150
            TabIndex        =   6
            Top             =   390
            Width           =   2625
         End
         Begin MSComctlLib.ListView lsvCanalVoz 
            Height          =   1215
            Left            =   120
            TabIndex        =   8
            Top             =   1170
            Width           =   4425
            _ExtentX        =   7805
            _ExtentY        =   2143
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   0
            NumItems        =   2
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "Dep�sito"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Text            =   "Canal de Voz"
               Object.Width           =   2540
            EndProperty
         End
         Begin Bot�o.cmd cmdExcl 
            Height          =   390
            Left            =   4110
            TabIndex        =   9
            TabStop         =   0   'False
            ToolTipText     =   "Voltar"
            Top             =   720
            Width           =   450
            _ExtentX        =   794
            _ExtentY        =   688
            BTYPE           =   3
            TX              =   ""
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   0   'False
            BCOL            =   16777215
            BCOLO           =   12640511
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "frmCad231.frx":6714
            PICN            =   "frmCad231.frx":6730
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   2
            NGREY           =   0   'False
            FX              =   3
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin Bot�o.cmd cmdAdd 
            Height          =   390
            Left            =   3540
            TabIndex        =   10
            TabStop         =   0   'False
            ToolTipText     =   "Voltar"
            Top             =   720
            Width           =   450
            _ExtentX        =   794
            _ExtentY        =   688
            BTYPE           =   3
            TX              =   ""
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   0   'False
            BCOL            =   16777215
            BCOLO           =   12640511
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "frmCad231.frx":6B42
            PICN            =   "frmCad231.frx":6B5E
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   2
            NGREY           =   0   'False
            FX              =   3
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin VB.Label Label4 
            AutoSize        =   -1  'True
            Caption         =   "N�mero:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   2820
            TabIndex        =   12
            Top             =   180
            Width           =   720
         End
         Begin VB.Label Label9 
            AutoSize        =   -1  'True
            Caption         =   "Dep�sito:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   150
            TabIndex        =   11
            Top             =   180
            Width           =   825
         End
      End
      Begin VB.ListBox lstDepositosOK 
         Appearance      =   0  'Flat
         Height          =   1200
         Left            =   -70170
         TabIndex        =   4
         Top             =   5520
         Visible         =   0   'False
         Width           =   3555
      End
      Begin VB.TextBox txtCaminhoArquivoTexto 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   -70170
         TabIndex        =   3
         Top             =   4920
         Visible         =   0   'False
         Width           =   3075
      End
      Begin Bot�o.cmd cmdGerar 
         Height          =   390
         Left            =   -67050
         TabIndex        =   25
         TabStop         =   0   'False
         ToolTipText     =   "Atualizar Ramais com Arquivo Texto"
         Top             =   4860
         Visible         =   0   'False
         Width           =   450
         _ExtentX        =   794
         _ExtentY        =   688
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCad231.frx":6FF8
         PICN            =   "frmCad231.frx":7014
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdSalvarTempo 
         Height          =   465
         Left            =   -74790
         TabIndex        =   48
         Top             =   2970
         Width           =   1545
         _ExtentX        =   2725
         _ExtentY        =   820
         BTYPE           =   3
         TX              =   "Salvar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCad231.frx":74AE
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdExcluirPromotor 
         Height          =   375
         Left            =   -73200
         TabIndex        =   94
         ToolTipText     =   "Excluir Promotor Selecionado"
         Top             =   1440
         Width           =   375
         _ExtentX        =   661
         _ExtentY        =   661
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCad231.frx":74CA
         PICN            =   "frmCad231.frx":74E6
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdGravarPromotores 
         Height          =   375
         Left            =   -73620
         TabIndex        =   95
         ToolTipText     =   "Gravar Altera��es dos Promotores"
         Top             =   2040
         Width           =   1965
         _ExtentX        =   3466
         _ExtentY        =   661
         BTYPE           =   3
         TX              =   "Gravar Altera��es"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCad231.frx":78B3
         PICN            =   "frmCad231.frx":78CF
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label lblStatusSerie 
         Appearance      =   0  'Flat
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   4950
         TabIndex        =   99
         Top             =   6090
         Width           =   3435
      End
      Begin VB.Label Label21 
         Appearance      =   0  'Flat
         Caption         =   "Promotor"
         ForeColor       =   &H80000008&
         Height          =   225
         Left            =   -74880
         TabIndex        =   92
         Top             =   1260
         Width           =   705
      End
      Begin VB.Label Label18 
         AutoSize        =   -1  'True
         Caption         =   "Tempo em Segundos:"
         Height          =   195
         Left            =   -68190
         TabIndex        =   53
         Top             =   1410
         Width           =   1560
      End
      Begin VB.Label Label17 
         AutoSize        =   -1  'True
         Caption         =   "CDs:"
         Height          =   195
         Left            =   -71310
         TabIndex        =   52
         Top             =   1410
         Width           =   435
      End
      Begin VB.Label Label16 
         AutoSize        =   -1  'True
         Caption         =   "CDs:"
         Height          =   195
         Left            =   -74790
         TabIndex        =   50
         Top             =   1410
         Width           =   345
      End
      Begin VB.Label Label15 
         Caption         =   "Tempo para Marcar Liga��o em Segundos:"
         Height          =   405
         Left            =   -74790
         TabIndex        =   49
         Top             =   2130
         Width           =   1695
         WordWrap        =   -1  'True
      End
      Begin VB.Label Label12 
         AutoSize        =   -1  'True
         Caption         =   "CDs:"
         Height          =   195
         Left            =   -74910
         TabIndex        =   33
         Top             =   1290
         Width           =   345
      End
      Begin VB.Label lblDeposito 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   20.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000C0&
         Height          =   555
         Left            =   -74940
         TabIndex        =   31
         Top             =   1410
         Width           =   8370
      End
      Begin VB.Label lbl1 
         AutoSize        =   -1  'True
         Caption         =   "Cidades Sem DDD para Dep�sito Atual:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   -71625
         TabIndex        =   30
         Top             =   1980
         Width           =   3405
      End
      Begin VB.Label lblQtd 
         AutoSize        =   -1  'True
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000C0&
         Height          =   240
         Left            =   -67170
         TabIndex        =   29
         Top             =   4290
         Width           =   465
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Qtd Cidades Selecionadas:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   -69570
         TabIndex        =   28
         Top             =   4290
         Width           =   2310
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         Caption         =   "Dep�sitos Atualizados:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   -70170
         TabIndex        =   27
         Top             =   5310
         Visible         =   0   'False
         Width           =   1950
      End
      Begin VB.Label Label11 
         AutoSize        =   -1  'True
         Caption         =   "Caminho Arquivo Texto:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   -70170
         TabIndex        =   26
         Top             =   4710
         Visible         =   0   'False
         Width           =   2040
      End
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      Top             =   1335
      Width           =   9075
      _ExtentX        =   16007
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   8505
      Width           =   9120
      _ExtentX        =   16087
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   3387
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   2646
            MinWidth        =   2646
            Object.ToolTipText     =   "Usu�rio da rede"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   2381
            MinWidth        =   2381
            Object.ToolTipText     =   "Usu�rio do banco de dados"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   3528
            MinWidth        =   3528
            Object.ToolTipText     =   "Banco de dados conectado"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            Alignment       =   1
            Object.Width           =   2381
            MinWidth        =   2381
            TextSave        =   "21/12/09"
            Object.ToolTipText     =   "Data"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   1
            Object.Width           =   1587
            MinWidth        =   1587
            TextSave        =   "17:07"
            Object.ToolTipText     =   "Hora"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   1005
      Left            =   8130
      TabIndex        =   69
      TabStop         =   0   'False
      ToolTipText     =   "Sair do sistema"
      Top             =   150
      Width           =   945
      _ExtentX        =   1667
      _ExtentY        =   1773
      BTYPE           =   3
      TX              =   "Sair"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCad231.frx":7CC4
      PICN            =   "frmCad231.frx":7CE0
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Image Image1 
      Height          =   1185
      Left            =   -90
      Picture         =   "frmCad231.frx":89BA
      Stretch         =   -1  'True
      ToolTipText     =   "Acessar a Intranet"
      Top             =   -45
      Width           =   1455
   End
End
Attribute VB_Name = "frmVda231"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Declare Function GetUserName Lib _
        "advapi32.dll" Alias "GetUserNameA" _
        (ByVal lpbuffer As String, nSize _
        As Long) As Long


Const ORATYPE_CURSOR = 102
Const ORADYN_NO_BLANKSTRIP = &H2&
Const ORAPARM_INPUT = 1              'CONSTANTE DE BIND INPUT
Const ORAPARM_OUTPUT = 2             'CONSTANTE DE BIND OUTPUT
Const ORAPARM_BOTH = 3               'CONSTANTE DE BIND INPUT/OUTPUT
Const ORATYPE_NUMBER = 2

Public vDEP As String
Public vNumDep As Byte
Dim vLoad As Boolean

Private Sub cboCds_Click()
    Dim vDBLink As String
    Dim vrstnf As Object
    
    lblStatus.Caption = ""
    lblStatus.BackColor = &H8000000F
    cmdStatusLigAutoAtivar.Enabled = False
    cmdStatusLigAutoDesativar.Enabled = False
    cmdStatusLigAutoAdicionar.Enabled = False
    
    lblStatusNF.Caption = ""
    lblStatusNF.BackColor = &H8000000F
    cmdStatusNFativar.Enabled = False
    cmdStatusNFDesAtivar.Enabled = False
    cmdStatusNFAdicionar.Enabled = False
    
    If Val(cboCds) = 6 Or Val(cboCds) = 2 Or Val(cboCds) = 20 Then Exit Sub
    
    If InStr(1, vCDDPK, Format(Val(cboCds), "00")) > 0 Then
        Set vObjOracle = vBanco.CreateDynaset("SELECT VL_PARAMETRO FROM DEP" & Format(Val(cboCds), "00") & ".PARAMETROS@lnk_CDdpk.CAL.COM.BR WHERE COD_SOFTWARE =1069 and UPPER(NOME_PARAMETRO) ='LIGACAOAUTOMATICA'", 0&)
        Set vrstnf = vBanco.CreateDynaset("SELECT VL_PARAMETRO FROM DEP" & Format(Val(cboCds), "00") & ".PARAMETROS@lnk_CDdpk.CAL.COM.BR WHERE COD_SOFTWARE =1062 and NOME_PARAMETRO ='ENDERECO_NF'", 0&)
    Else
        Select Case Val(cboCds)
            Case 1
                vDBLink = ""
            Case Else
                vDBLink = "@LNK_CD" & Format(Val(cboCds), "00") & ".cal.com.br"
        End Select
        
        Set vObjOracle = vBanco.CreateDynaset("SELECT VL_PARAMETRO FROM HELPDESK.PARAMETROS" & vDBLink & " WHERE COD_SOFTWARE =1069 and UPPER(NOME_PARAMETRO) ='LIGACAOAUTOMATICA'", 0&)
        Set vrstnf = vBanco.CreateDynaset("SELECT VL_PARAMETRO FROM HELPDESK.PARAMETROS" & vDBLink & " WHERE COD_SOFTWARE =1062 and NOME_PARAMETRO ='ENDERECO_NF'", 0&)
    
    End If
    
    If vObjOracle.EOF = False Then
        If Val(vObjOracle!vl_parametro) = 0 Then
            lblStatus.Caption = "Liga��o Autom�tica est� DESATIVADA" & vbCrLf & "Para ATIVAR clique no bot�o abaixo."
            cmdStatusLigAutoAtivar.Enabled = True
            cmdStatusLigAutoDesativar.Enabled = False
            cmdStatusLigAutoAdicionar.Enabled = False
            lblStatus.BackColor = &H8080FF
        Else
            lblStatus.Caption = "Liga��o Autom�tica est� ATIVADA" & vbCrLf & "Para DESATIVAR clique no bot�o abaixo."
            cmdStatusLigAutoDesativar.Enabled = True
            cmdStatusLigAutoAtivar.Enabled = False
            cmdStatusLigAutoAdicionar.Enabled = False
            lblStatus.BackColor = &HC0FFC0
        End If
    Else
        cmdStatusLigAutoDesativar.Enabled = False
        cmdStatusLigAutoAtivar.Enabled = False
        cmdStatusLigAutoAdicionar.Enabled = True
    End If
    
    'ENDERECO NAS NF
    If vrstnf.EOF = False Then
        If Val(vrstnf!vl_parametro) = 0 Then
            lblStatusNF.Caption = "O Endere�o N�O est� sendo impresso nas Notas deste CD." & vbCrLf & "Para come�ar a Imprimir o Endere�o na NF clique no bot�o abaixo."
            cmdStatusNFativar.Enabled = True
            cmdStatusNFDesAtivar.Enabled = False
            lblStatusNF.BackColor = &H8080FF
        Else
            lblStatusNF.Caption = "O Endere�o est� sendo impresso para as Notas deste CD." & vbCrLf & "Para parar de Imprimir o Endere�o na NF clique no bot�o abaixo."
            cmdStatusNFDesAtivar.Enabled = True
            cmdStatusNFativar.Enabled = False
            lblStatusNF.BackColor = &HC0FFC0
        End If
    Else
        lblStatusNF.Caption = "N�o existe o par�metro neste banco." & vbCrLf & "Para Adicionar clique no bot�o abaixo."
        cmdStatusNFAdicionar.Enabled = True
        cmdStatusNFDesAtivar.Enabled = False
        cmdStatusNFativar.Enabled = False
    End If
    
End Sub

Private Sub cboCdsLink_Click()
    Dim vDBLink As String
    
    Me.lblMensagemLink.Caption = "Aguarde..."
    
    Me.Refresh
    
    For i = 0 To cboCdsLink.ListCount - 1
        If Val(cboCdsLink) = 2 Or Val(cboCdsLink) = 6 Or Val(cboCdsLink) = 20 Then GoTo proximo
        If InStr(1, vCDDPK, Format(Val(cboCdsLink), "00")) > 0 Then
            Set vObjOracle = vBanco.CreateDynaset("SELECT * FROM DEP" & Format(Val(cboCdsLink), "00") & ".PARAMETROS@lnk_CDdpk.CAL.COM.BR WHERE COD_SOFTWARE =1069 and NOME_PARAMETRO ='LINK_CPS'", 0&)
        Else
            Select Case Val(cboCdsLink)
                Case 1
                    vDBLink = ""
                Case Else
                    vDBLink = "@LNK_CD" & Format(Val(cboCdsLink), "00") & ".cal.com.br"
            End Select
            
            Set vObjOracle = vBanco.CreateDynaset("SELECT vl_parametro FROM HELPDESK.PARAMETROS" & vDBLink & " WHERE COD_SOFTWARE =1069 and UPPER(NOME_PARAMETRO) like 'LINK_CPS'", 0&)
        End If
        If vObjOracle.EOF = False Then
            cmdInserirLink.Enabled = False
            If Val(vObjOracle!vl_parametro) = "0" Then
                lblMensagemLink.Caption = "O Link est� Desativado." & vbCrLf & "Para reativ�-lo clieque no bot�o ATIVAR."
                cmdAtivarLink.Enabled = True
                cmdDesativarLink.Enabled = False
            Else
                lblMensagemLink.Caption = "O Link est� Ativado." & vbCrLf & "Para desativ�-lo clieque no bot�o DESATIVAR."
                cmdDesativarLink.Enabled = True
                cmdAtivarLink.Enabled = False
            End If
        Else
            lblMensagemLink.Caption = "Adicione o registro na tabela Parametros." & vbCrLf & "Sempre que o registro � adicionado ele � adicionado como Ativado."
            cmdDesativarLink.Enabled = False
            cmdAtivarLink.Enabled = False
            cmdInserirLink.Enabled = True
        End If

proximo:
    Next
    
End Sub

Private Sub CboCdsLinkIDPK_Click()
    Dim vDBLink As String
    
    Me.lblMensagemLinkIDPK.Caption = "Aguarde..."
    
    Me.Refresh
    
    For i = 0 To CboCdsLinkIDPK.ListCount - 1
        If Val(CboCdsLinkIDPK) = 2 Or Val(CboCdsLinkIDPK) = 6 Or Val(CboCdsLinkIDPK) = 20 Then GoTo proximo
        If InStr(1, vCDDPK, Format(Val(CboCdsLinkIDPK), "00")) > 0 Then
            Set vObjOracle = vBanco.CreateDynaset("SELECT * FROM DEP" & Format(Val(CboCdsLinkIDPK), "00") & ".PARAMETROS@lnk_CDdpk.CAL.COM.BR WHERE COD_SOFTWARE =1069 and NOME_PARAMETRO ='LINK_IDPK'", 0&)
        Else
            Select Case Val(CboCdsLinkIDPK)
                Case 1
                    vDBLink = ""
                Case Else
                    vDBLink = "@LNK_CD" & Format(Val(CboCdsLinkIDPK), "00") & ".cal.com.br"
            End Select
            
            Set vObjOracle = vBanco.CreateDynaset("SELECT * FROM HELPDESK.PARAMETROS" & vDBLink & " WHERE COD_SOFTWARE =1069 and UPPER(NOME_PARAMETRO) like 'LINK_IDPK'", 0&)
        End If
        If vObjOracle.EOF = False Then
            cmdInserirIDPK.Enabled = False
            If Val(vObjOracle!vl_parametro) = "0" Then
                lblMensagemLinkIDPK.Caption = "O Link est� Desativado." & vbCrLf & "Para reativ�-lo clieque no bot�o ATIVAR."
                cmdAtivarIDPK.Enabled = True
                cmdDesativarIDPK.Enabled = False
            Else
                lblMensagemLinkIDPK.Caption = "O Link est� Ativado." & vbCrLf & "Para desativ�-lo clieque no bot�o DESATIVAR."
                cmdDesativarIDPK.Enabled = True
                cmdAtivarIDPK.Enabled = False
            End If
        Else
            lblMensagemLinkIDPK.Caption = "Adicione o registro na tabela Parametros." & vbCrLf & "Sempre que o registro � adicionado ele � adicionado como Ativado."
            cmdDesativarIDPK.Enabled = False
            cmdAtivarIDPK.Enabled = False
            cmdInserirIDPK.Enabled = True
        End If

proximo:
    Next
End Sub

Private Sub cboCdTempo_Click()
    Dim vDBLink As String
    
    txtTempo = ""
    
    For i = 0 To cboCdTempo.ListCount - 1
        If Val(Me.cboCdTempo) = 2 Or Val(cboCdTempo) = 6 Or Val(cboCdTempo) = 20 Then GoTo proximo
        If InStr(1, vCDDPK, Format(Val(cboCdTempo), "00")) > 0 Then
            Set vObjOracle = vBanco.CreateDynaset("SELECT VL_PARAMETRO FROM DEP" & Format(Val(cboCdTempo), "00") & ".PARAMETROS@lnk_CDdpk.CAL.COM.BR WHERE COD_SOFTWARE =1069 and NOME_PARAMETRO ='TEMPOLIGACAO'", 0&)
        Else
            Select Case Val(cboCdTempo)
                Case 1
                    vDBLink = ""
                Case Else
                    vDBLink = "@LNK_CD" & Format(Val(cboCdTempo), "00") & ".cal.com.br"
            End Select
            
            Set vObjOracle = vBanco.CreateDynaset("SELECT VL_PARAMETRO FROM HELPDESK.PARAMETROS" & vDBLink & " WHERE COD_SOFTWARE =1069 and UPPER(NOME_PARAMETRO) ='TEMPOLIGACAO'", 0&)
        End If
    
        If vObjOracle.EOF = False Then
            txtTempo = Val(vObjOracle!vl_parametro)
            Exit For
        End If
proximo:
    Next
End Sub

Private Sub cmdAdd_Click()
    
    Gravar_Canal "CV-" & txtDeposito, txtNumero
    carregar_lsv_Canal
    txtDeposito = ""
    txtNumero = ""
End Sub

Private Sub cmdAdicionarPromotor_Click()

    If Val(Me.txtCodPromotor) = 0 Then
        MsgBox "Informe o c�digo do promotor.", vbInformation, "Aten��o"
        Me.txtCodPromotor.SetFocus
        Exit Sub
    End If

    For i = 0 To Me.lstPromotores.ListCount - 1
    
        If Val(txtCodPromotor) = Val(Me.lstPromotores.List(i)) Then
            MsgBox "C�digo de promotor j� cadastrado.", vbInformation, "Aten��o"
            Exit Sub
        End If
    
    Next
    
    lstPromotores.AddItem Val(txtCodPromotor)
    txtCodPromotor = ""

End Sub

Private Sub cmdAtivar_Click()
    If txtTLMKT = "" Then
        MsgBox "Informe o codigo do TLMKT.", vbInformation, "Aten��o"
        txtTLMKT.SetFocus
        Exit Sub
    End If
    
    Status_TLMKT_Ligacao_AUTO 1
End Sub

Private Sub cmdAtivarIDPK_Click()
    If lblMensagemLinkIDPK = "" Then Exit Sub
    
    Me.MousePointer = vbHourglass
    
    Status_LINK 1, "LINK_IDPK", Val(CboCdsLinkIDPK)
    
    Enviar_Email "ATIVAR", "LINK"
    
    CboCdsLinkIDPK_Click
    
    Me.MousePointer = vbNormal
End Sub

Private Sub cmdAtivarLink_Click()
    If lblMensagemLink = "" Then Exit Sub
    
    Me.MousePointer = vbHourglass
    
    Status_LINK 1, "LINK_CPS", Val(cboCdsLink)
    
    Enviar_Email "ATIVAR", "LINK"
    
    cboCdsLink_Click
    
    Me.MousePointer = vbNormal
    
End Sub

Private Sub cmdConectar_Click()
    dlgConexao.Show 1
End Sub

Private Sub cmdDesativar_Click()
    If txtTLMKT = "" Then
        MsgBox "Informe o codigo do TLMKT.", vbInformation, "Aten��o"
        txtTLMKT.SetFocus
        Exit Sub
    End If
    
    Status_TLMKT_Ligacao_AUTO 2
End Sub

Private Sub cmdDesativarIDPK_Click()
    If lblMensagemLinkIDPK = "" Then Exit Sub
    
    Me.MousePointer = vbHourglass
    Status_LINK 2, "LINK_IDPK", Val(CboCdsLinkIDPK)
    Enviar_Email "DESATIVAR", "LINK"
    CboCdsLinkIDPK_Click
    Me.MousePointer = vbNormal

End Sub

Private Sub cmdDesativarLink_Click()
    If lblMensagemLink = "" Then Exit Sub
    
    Me.MousePointer = vbHourglass
    Status_LINK 2, "LINK_CPS", Val(cboCdsLink)
    Enviar_Email "DESATIVAR", "LINK"
    cboCdsLink_Click
    Me.MousePointer = vbNormal
End Sub

Private Sub cmdEmail_Click()
    frmEnviarEmail.Show 1
End Sub

Private Sub cmdExcl_Click()
    If txtDeposito = "" Or txtNumero = "" Then
        MsgBox "Selecione um item da lista.", vbInformation, "Aten��o"
        Exit Sub
    End If
    
    Deletar_Canal "CV-" & txtDeposito, txtNumero

    MsgBox "Item excluido com sucesso !!", vbInformation, "Aten��o"
    
    carregar_lsv_Canal
    
End Sub

Private Sub cmdExcluirPromotor_Click()

    If Me.lstPromotores.SelCount = 0 Then
        MsgBox "Selecione um promotor da lista.", vbInformation, "Aten��o"
        Exit Sub
    End If
    
    Me.lstPromotores.RemoveItem lstPromotores.ListIndex

End Sub

Private Sub cmdGerar_Click()
    
    On Error GoTo trata_erro

    Dim vArq As Integer
    Dim vlinha As String
    Dim vCampo() As String
    Dim vOwner As String
    
    vArq = FreeFile
    
    Set vObjOracle = vVB_Generica_001.TabelaLoja(vBanco)
    
    For i = 1 To vObjOracle.RecordCount
        
        Open txtCaminhoArquivoTexto For Input As #vArq
        
        If Val(vObjOracle.Fields(0)) = 0 Or Val(vObjOracle.Fields(0)) = 6 Then GoTo proximo
        
        If InStr(1, vCDDPK, Format(CStr(Val(vObjOracle.Fields(0))), "00")) Then
           vErro = vVB_Generica_001.ConectaOracle("CDDPK", "vda020", False, frmVda231)
           vOwner = "PRODUCAO."
           vDEP = "DEP" & Format(CStr(Val(vObjOracle.Fields(0))), "00") & "."
           Set vSessao = vVB_Generica_001.vSessao
           Set vBanco = vVB_Generica_001.vBanco
        Else
            If Val(vObjOracle.Fields(0)) = 1 Then
               vErro = vVB_Generica_001.ConectaOracle("PRODUCAO", "vda020", False, frmVda231)
            Else
               vErro = vVB_Generica_001.ConectaOracle("CD" & Format(CStr(Val(vObjOracle.Fields(0))), "00"), "vda020", False, frmVda231)
            End If
            Set vSessao = vVB_Generica_001.vSessao
            Set vBanco = vVB_Generica_001.vBanco
            vOwner = "HELPDESK."
            vDEP = "PRODUCAO."
        End If
        
        While Not EOF(vArq)
        
            Line Input #vArq, vlinha
            
            vCampo = Split(vlinha, ";")
            
            vBanco.Parameters.Remove "Cod_Soft": vBanco.Parameters.Add "Cod_Soft", 1069, 1
            vBanco.Parameters.Remove "Nome_Param": vBanco.Parameters.Add "Nome_Param", Trim(vCampo(0)), 1
            vBanco.Parameters.Remove "VL_PARAM": vBanco.Parameters.Add "VL_PARAM", Trim(vCampo(1)), 1
            vBanco.Parameters.Remove "Dep": vBanco.Parameters.Add "DEP", vDEP, 1
            vBanco.Parameters.Remove "Txt_Erro": vBanco.Parameters.Add "Txt_Erro", "", 2
        
            If vVB_Generica_001.ExecutaPl(vBanco, vOwner & "PCK_VDA231.Pr_Salvar_Parametros(:Nome_Param, :VL_PARAM, :Cod_Soft, :DEP, :Txt_Erro)") <> "" Then
                lstDepositosOK.AddItem vObjOracle.Fields(0) & " - Erro"
            Else
                lstDepositosOK.AddItem vObjOracle.Fields(0) & " - OK"
            End If
            
        Wend
        
        Close #vArq
        
proximo:
        vObjOracle.MoveNext
    Next
    
trata_erro:
    If Err.Number = 55 Then
        Close vArq
        Resume
    ElseIf Err.Number <> 0 Then
        MsgBox "C�digo:" & Err.Number & vbCrLf & "Descricao:" & Err.Description
    End If
    
End Sub

Private Sub cmdGravarPromotores_Click()
    Dim vLista As String
    Dim vPromotores(1, 20) As String
    Dim vPos As Byte
    Dim vArq As Integer
    
    If MsgBox("Confirma Grava��o ?", vbQuestion + vbYesNo) = vbYes Then
    
        vBanco.ExecuteSQL "BEGIN DELETE FROM HELPDESK.PARAMETROS WHERE NOME_PARAMETRO LIKE 'PROMOTOR_%' AND COD_SOFTWARE = 508; commit;end;"
        
        For i = 0 To lstPromotores.ListCount - 1
        
            If Trim(vLista) = "" Then
                vLista = lstPromotores.List(i)
            Else
                vLista = vLista & ";" & lstPromotores.List(i)
            End If
        
            If Len(Trim(vLista)) >= 146 Then
               vPromotores(0, vPos) = "PROMOTOR_" & vPos + 1
               vPromotores(1, vPos) = vLista
               vLista = ""
               vPos = vPos + 1
            End If
        
        Next
        If vLista <> "" Then
            vPromotores(0, vPos) = "PROMOTOR_" & vPos + 1
            vPromotores(1, vPos) = vLista
        End If
    
        Salvar_Posicao "GRAVAR"
        
        For i = 0 To 20
            If vPromotores(0, i) = "" Then
                Exit For
            End If
            
            vBanco.ExecuteSQL "BEGIN UPDATE HELPDESK.PARAMETROS SET VL_PARAMETRO='" & vPromotores(1, i) & "' WHERE NOME_PARAMETRO ='" & vPromotores(0, i) & "' AND COD_SOFTWARE = 508; IF SQL%NOTFOUND THEN INSERT INTO HELPDESK.PARAMETROS VALUES (508, '" & vPromotores(0, i) & "','" & vPromotores(1, i) & "'); end if; commit;end;"
        Next
        
        MsgBox "Grava��o efetuada com sucesso !", vbInformation, "Aten��o"
        
    End If

End Sub

Private Sub cmdInserirIDPK_Click()
    Me.MousePointer = vbHourglass
    Status_LINK 3, "LINK_IDPK", Val(CboCdsLinkIDPK)
    CboCdsLinkIDPK_Click
    Me.MousePointer = vbNormal
End Sub

Private Sub cmdInserirLink_Click()
    Me.MousePointer = vbHourglass
    Status_LINK 3, "LINK_CPS", Val(cboCdsLink)
    cboCdsLink_Click
    Me.MousePointer = vbNormal
End Sub

Private Sub cmdPermissao_Click()
    frmPermissao.Show 1
End Sub

Private Sub cmdSair_Click()
    
    Desconectar
    
    If vVB_Generica_001.Sair = 6 Then
    
        Set vObjOracle = Nothing
        Set vBanco = Nothing
        Set vSessao = Nothing
        
        End
    
    End If

End Sub
Private Sub cmdSobre_Click()

    frmSobre.Show 1

End Sub
'1-Ativar
'2-Desativar
'3-Adicionar
Sub Status_Ligacao(pAcao As Byte)
    Dim vDBLink As String

    If Val(cboCds) = 6 Or Val(cboCds) = 2 Or Val(cboCds) = 20 Then Exit Sub
    
    If MsgBox(IIf(pAcao = 3, "Confirma ?", "Confirma a altera��o do Par�metro ?"), vbQuestion + vbYesNo, "Aten��o") = vbNo Then
        Exit Sub
    End If
    If pAcao = 3 Then 'Inserir
        If InStr(1, vCDDPK, Format(Val(cboCds), "00")) > 0 Then
            vBanco.ExecuteSQL "BEGIN INSERT INTO DEP" & Format(Val(cboCds), "00") & ".PARAMETROS@lnk_CDdpk.CAL.COM.BR VALUES (1069,'LIGACAOAUTOMATICA','1'); COMMIT; END;"
        Else
           Select Case Val(cboCds)
                Case 1
                    vDBLink = ""
                Case Else
                    vDBLink = "@LNK_CD" & Format(Val(cboCds), "00") & ".cal.com.br"
            End Select
            vBanco.ExecuteSQL "BEGIN INSERT INTO HELPDESK.PARAMETROS" & vDBLink & " VALUES (1069,'LIGACAOAUTOMATICA','1'); COMMIT; END;"
        End If
    Else
        If InStr(1, vCDDPK, Format(Val(cboCds), "00")) > 0 Then
            vBanco.ExecuteSQL "BEGIN UPDATE DEP" & Format(Val(cboCds), "00") & ".PARAMETROS@lnk_CDdpk.CAL.COM.BR SET VL_PARAMETRO='" & IIf(pAcao = 1, 1, 0) & "' WHERE COD_SOFTWARE =1069 and upper(NOME_PARAMETRO) ='LIGACAOAUTOMATICA'; COMMIT; END;"
        Else
           Select Case Val(cboCds)
                Case 1
                    vDBLink = ""
                Case Else
                    vDBLink = "@LNK_CD" & Format(Val(cboCds), "00") & ".cal.com.br"
            End Select
            vBanco.ExecuteSQL "BEGIN UPDATE HELPDESK.PARAMETROS" & vDBLink & " SET VL_PARAMETRO = '" & IIf(pAcao = 1, 1, 0) & "' WHERE COD_SOFTWARE =1069 and UPPER(NOME_PARAMETRO) ='LIGACAOAUTOMATICA'; COMMIT; END;"
        End If
    End If
End Sub

'1-Ativar
'2-Desativar
'3-Adicionar
Sub Status_NF(pAcao As Byte)
    
    Dim vDBLink As String
    
    If Val(cboCds) = 6 Or Val(cboCds) = 2 Or Val(cboCds) = 20 Then Exit Sub
    
    If MsgBox(IIf(pAcao = 3, "Confirma ?", "Confirma a altera��o do Par�metro ?"), vbQuestion + vbYesNo, "Aten��o") = vbNo Then
        Exit Sub
    End If
    
    If pAcao = 3 Then 'Inserir
        If InStr(1, vCDDPK, Format(Val(cboCds), "00")) > 0 Then
            vBanco.ExecuteSQL "BEGIN INSERT INTO DEP" & Format(Val(cboCds), "00") & ".PARAMETROS@lnk_CDdpk.CAL.COM.BR VALUES(1062, 'ENDERECO_NF','0'); COMMIT; END;"
        Else
            Select Case Val(cboCds)
                Case 1
                    vDBLink = ""
                Case Else
                    vDBLink = "@LNK_CD" & Format(Val(cboCds), "00") & ".cal.com.br"
            End Select
            vBanco.ExecuteSQL "BEGIN INSERT INTO HELPDESK.PARAMETROS" & vDBLink & " VALUES(1062, 'ENDERECO_NF','0'); COMMIT; END;"
        End If
    Else
        If InStr(1, vCDDPK, Format(Val(cboCds), "00")) > 0 Then
            vBanco.ExecuteSQL "BEGIN UPDATE DEP" & Format(Val(cboCds), "00") & ".PARAMETROS@lnk_CDdpk.CAL.COM.BR SET VL_PARAMETRO='" & IIf(pAcao = 2, 0, 1) & "' WHERE COD_SOFTWARE =1062 and upper(NOME_PARAMETRO) ='ENDERECO_NF'; COMMIT; END;"
        Else
            Select Case Val(cboCds)
                Case 1
                    vDBLink = ""
                Case Else
                    vDBLink = "@LNK_CD" & Format(Val(cboCds), "00") & ".cal.com.br"
            End Select
            vBanco.ExecuteSQL "BEGIN UPDATE HELPDESK.PARAMETROS" & vDBLink & " SET VL_PARAMETRO = '" & IIf(pAcao = 2, 0, 1) & "' WHERE COD_SOFTWARE =1062 and UPPER(NOME_PARAMETRO) ='ENDERECO_NF'; COMMIT; END;"
        End If
    End If
End Sub

Private Sub cmdSalvarSerie_Click()
    Dim i As Integer
    Dim vSerie As String
    Dim vDBLink As String
    
    For i = 0 To lstSerie.ListCount - 1
        If Val(lstSerie.List(i)) <> 2 And Val(lstSerie.List(i)) <> 17 And Val(lstSerie.List(i)) <> 6 Then
        
        Me.lblStatusSerie = "Atualizando CD: " & lstSerie.List(i)
        Me.lblStatusSerie.Refresh
        
        If lstSerie.Selected(i) = True Then
            If InStr(1, vCDDPK, Format(Val(lstSerie.List(i)), "00")) > 0 Then
                vBanco.ExecuteSQL "BEGIN DELETE FROM DEP" & Format(Val(lstSerie.List(i)), "00") & ".PARAMETROS@lnk_CDdpk.CAL.COM.BR WHERE COD_SOFTWARE = 1062 AND NOME_PARAMETRO = 'NAO_IMPRIMIR_SERIE'; COMMIT; END;"
                vBanco.ExecuteSQL "BEGIN INSERT INTO DEP" & Format(Val(lstSerie.List(i)), "00") & ".PARAMETROS@lnk_CDdpk.CAL.COM.BR VALUES(1062, 'NAO_IMPRIMIR_SERIE','" & Val(lstSerie.List(i)) & "'); COMMIT; END;"
            Else
                Select Case Val(lstSerie.List(i))
                    Case 1
                        vDBLink = ""
                    Case Else
                        vDBLink = "@LNK_CD" & Format(Val(Me.lstSerie.List(i)), "00") & ".cal.com.br"
                End Select
                vBanco.ExecuteSQL "BEGIN DELETE FROM HELPDESK.PARAMETROS" & vDBLink & " WHERE COD_SOFTWARE = 1062 AND NOME_PARAMETRO = 'NAO_IMPRIMIR_SERIE'; COMMIT; END;"
                vBanco.ExecuteSQL "BEGIN INSERT INTO HELPDESK.PARAMETROS" & vDBLink & " VALUES(1062, 'NAO_IMPRIMIR_SERIE','" & Val(lstSerie.List(i)) & "'); COMMIT; END;"
            End If
        Else
            If InStr(1, vCDDPK, Format(Val(lstSerie.List(i)), "00")) > 0 Then
                vBanco.ExecuteSQL "BEGIN DELETE FROM DEP" & Format(Val(lstSerie.List(i)), "00") & ".PARAMETROS@lnk_CDdpk.CAL.COM.BR WHERE COD_SOFTWARE = 1062 AND NOME_PARAMETRO = 'NAO_IMPRIMIR_SERIE'; COMMIT; END;"
            Else
                Select Case Val(lstSerie.List(i))
                    Case 1
                        vDBLink = ""
                    Case Else
                        vDBLink = "@LNK_CD" & Format(Val(Me.lstSerie.List(i)), "00") & ".cal.com.br"
                End Select
                vBanco.ExecuteSQL "BEGIN DELETE FROM HELPDESK.PARAMETROS" & vDBLink & " WHERE COD_SOFTWARE = 1062 AND NOME_PARAMETRO = 'NAO_IMPRIMIR_SERIE'; COMMIT; END;"
            End If
        End If
    End If
    Next
    Me.lblStatusSerie = ""
End Sub

Private Sub cmdSalvarTempo_Click()
    Dim vDBLink As String

    If Val(cboCdTempo) = 6 Or Val(cboCdTempo) = 2 Or Val(cboCdTempo) = 20 Then Exit Sub
    
    If MsgBox("Confirma ?", vbQuestion + vbYesNo, "Aten��o") = vbYes Then
        If InStr(1, vCDDPK, Format(Val(cboCdTempo), "00")) > 0 Then
            vBanco.ExecuteSQL "BEGIN DELETE FROM DEP" & Format(Val(cboCdTempo), "00") & ".PARAMETROS@lnk_CDdpk.CAL.COM.BR WHERE COD_SOFTWARE=1069 AND NOME_PARAMETRO='TEMPOLIGACAO'; COMMIT; END;"
            vBanco.ExecuteSQL "BEGIN INSERT INTO DEP" & Format(Val(cboCdTempo), "00") & ".PARAMETROS@lnk_CDdpk.CAL.COM.BR VALUES (1069,'TEMPOLIGACAO','" & txtTempo & "'); COMMIT; END;"
            For i = 1 To Me.LSTTEMPOS.ListCount - 1
                If Val(cboCdTempo) = Val(LSTTEMPOS.List(i)) Then
                    LSTTEMPOS.List(i) = Trim(LSTTEMPOS.List(i)) & Space(30 - Len(Me.LSTTEMPOS.List(i))) & " - " & txtTempo
                    Exit For
                End If
            Next
        Else
           Select Case Val(cboCdTempo)
                Case 1
                    vDBLink = ""
                Case Else
                    vDBLink = "@LNK_CD" & Format(Val(cboCdTempo), "00") & ".cal.com.br"
            End Select
            vBanco.ExecuteSQL "BEGIN DELETE FROM HELPDESK.PARAMETROS" & vDBLink & " WHERE COD_SOFTWARE=1069 AND NOME_PARAMETRO='TEMPOLIGACAO'; COMMIT; END;"
            vBanco.ExecuteSQL "BEGIN INSERT INTO HELPDESK.PARAMETROS" & vDBLink & " VALUES (1069,'TEMPOLIGACAO','" & txtTempo & "'); COMMIT; END;"
            For i = 0 To LSTTEMPOS.ListCount - 1
                If Val(cboCdTempo) = Val(LSTTEMPOS.List(i)) Then
                    LSTTEMPOS.List(i) = Left(Trim(LSTTEMPOS.List(i)), 15) & Space(15) & " - " & txtTempo
                    Exit For
                End If
            Next
        
        End If
    End If
End Sub

Private Sub cmdStatusLigAutoAdicionar_Click()
    Status_Ligacao 3
End Sub

Private Sub cmdStatusLigAutoAtivar_Click()
    If lblStatus = "" Then Exit Sub
    
    Status_Ligacao 1
    
    Enviar_Email "ATIVAR", "LIGACAO AUTOMATICA"
    
End Sub

Private Sub cmdStatusLigAutoDesativar_Click()
    If lblStatus = "" Then Exit Sub
    Status_Ligacao 2
    
    Enviar_Email "DESATIVAR", "LIGACAO AUTOMATICA"
End Sub

Private Sub cmdStatusNFAdicionar_Click()
    Status_NF 3
End Sub

Private Sub cmdStatusNFativar_Click()
    Status_NF 1
End Sub

Private Sub cmdStatusNFDesAtivar_Click()
    Status_NF 2
End Sub

Private Sub cmdVersoes_Click()
    frmVersoes.Show 1
End Sub

Private Sub Form_Load()
    Dim vObj As Object
    
    frmVda231.Visible = True
    
    theNetResource.lpLocalName = "Z:"
    
    Desconectar
    
    Set vObj = vBanco.CreateDynaset("SELECT * FROM PRODUCAO.TIPO_BANCO_CD WHERE TP_BANCO ='M'", &O0)
    
    For i = 1 To vObj.RecordCount
        If vCDDPK = "" Then
            vCDDPK = vObj!cod_loja
        Else
            vCDDPK = vCDDPK & ", " & vObj!cod_loja
        End If
        vObj.MoveNext
    Next
    
    Set vObj = Nothing
    
    Preencher_Cbos
    
    TabParam.TabVisible(0) = vPabx 'UCase(vColunas(2)) = "P" Or UCase(vColunas(3)) = "P" Or UCase(vColunas(4)) = "P" Or UCase(vColunas(5)) = "P"
    FraBotoes.Visible = vPabx And vLigAuto
    TabParam.TabVisible(1) = vLigAuto 'vUCase(vColunas(2)) = "P" Or UCase(vColunas(3)) = "P" Or UCase(vColunas(4)) = "P" Or UCase(vColunas(5)) = "P"
    TabParam.TabVisible(2) = vTLMKTEspecifico 'UCase(vColunas(2)) = "P" Or UCase(vColunas(3)) = "P" Or UCase(vColunas(4)) = "P" Or UCase(vColunas(5)) = "P"
    TabParam.TabVisible(3) = vTempo 'UCase(vColunas(2)) = "P" Or UCase(vColunas(3)) = "P" Or UCase(vColunas(4)) = "P" Or UCase(vColunas(5)) = "P"
    TabParam.TabVisible(4) = False 'vLinks 'UCase(vColunas(2)) = "P" Or UCase(vColunas(3)) = "P" Or UCase(vColunas(4)) = "P" Or UCase(vColunas(5)) = "P"
    TabParam.TabVisible(5) = False 'vLinks 'UCase(vColunas(2)) = "P" Or UCase(vColunas(3)) = "P" Or UCase(vColunas(4)) = "P" Or UCase(vColunas(5)) = "P"
    
    CarregarPromotores
    
End Sub



Private Sub cmdSalvar_Click()
    Gravar "IP4000", txtIP4000
    Gravar "PORTA4000", txtPorta4000
    Gravar "IP3000", txtIP3000
    Gravar "PORTA3000", txtPorta3000
    Gravar "LIGACAOAUTOMATICA", IIf(UCase(cboConectado) = "SIM", 1, 0)
End Sub

Sub Gravar(pNomeParam As String, pValorParam As String)
    Dim vOwner As String
    
    'If Val(vObjOracle.Fields(0)) = 0 Or Val(vObjOracle.Fields(0)) = 6 Or Val(vObjOracle.Fields(0)) = 15 Th
    
    vBanco.Parameters.Remove "Cod_Soft": vBanco.Parameters.Add "Cod_Soft", Pegar_Cod_Sistema, 1
    vBanco.Parameters.Remove "Nome_Param": vBanco.Parameters.Add "Nome_Param", pNomeParam, 1
    vBanco.Parameters.Remove "VL_PARAM": vBanco.Parameters.Add "VL_PARAM", pValorParam, 1
    vBanco.Parameters.Remove "Dep": vBanco.Parameters.Add "DEP", vOwner, 1
    vBanco.Parameters.Remove "Txt_Erro": vBanco.Parameters.Add "Txt_Erro", "", 2

    If vVB_Generica_001.ExecutaPl(vBanco, vDEP & ".PCK_VDA231.Pr_Salvar_Parametros(:Nome_Param, :VL_PARAM, :Cod_Soft, :DEP, :Txt_Erro)") <> "" Then
        MsgBox "Descri��o do Erro:" & vErro
        Exit Sub
    End If

End Sub

Sub Deletar(pNomeParam As String, pValorParam As String)
    Dim vOwner As String
    
    If InStr(1, vCDDPK, Format(CStr(Val(Right(vDEP, 2))), "00")) Then
       vErro = vVB_Generica_001.ConectaOracle("CDDPK", "vda020", False, frmVda231)
       vOwner = "DEP" & Format(Val(Right(vDEP, 2)), "00") & "."
       'vDEP = vOwner
       Set vSessao = vVB_Generica_001.vSessao
       Set vBanco = vVB_Generica_001.vBanco
    Else
        If Val(vObjOracle.Fields(0)) = 1 Then
           vErro = vVB_Generica_001.ConectaOracle("PRODUCAO", "vda020", False, frmVda231)
        Else
           vErro = vVB_Generica_001.ConectaOracle("CD" & Format(CStr(Val(vObjOracle.Fields(0))), "00"), "vda020", False, frmVda231)
        End If
        Set vSessao = vVB_Generica_001.vSessao
        Set vBanco = vVB_Generica_001.vBanco
        vOwner = "HELPDESK."
        'vDEP = "PRODUCAO."
    End If

    vBanco.Parameters.Remove "Cod_Soft": vBanco.Parameters.Add "Cod_Soft", Pegar_Cod_Sistema, 1
    vBanco.Parameters.Remove "Nome_Param": vBanco.Parameters.Add "Nome_Param", pNomeParam, 1
    'vBanco.Parameters.Remove "VL_PARAM": vBanco.Parameters.Add "VL_PARAM", pValorParam, 1
    vBanco.Parameters.Remove "Dep": vBanco.Parameters.Add "DEP", vOwner, 1
    vBanco.Parameters.Remove "Txt_Erro": vBanco.Parameters.Add "Txt_Erro", "", 2

    vVB_Generica_001.ExecutaPl vBanco, vDEP & ".PCK_VDA231.Pr_Deletar_Parametros(:Nome_Param, :Cod_Soft, :DEP, :Txt_Erro)"

    If Trim(vBanco.Parameters("Txt_Erro")) <> "" Then
        MsgBox "Descri��o do Erro:" & vBanco.Parameters("Txt_Erro")
        Exit Sub
    End If
End Sub


Public Sub Carregar_Parametros()
    
    Dim vIP4000 As String
    Dim vPorta4000 As String
    Dim vIP3000 As String
    Dim vPorta3000 As String
    Dim vConectado As String
    
    vIP4000 = "" & Pegar_VL_Parametro("IP4000")
    vPorta4000 = "" & Pegar_VL_Parametro("PORTA4000")
    vIP3000 = "" & Pegar_VL_Parametro("IP3000")
    vPorta3000 = "" & Pegar_VL_Parametro("PORTA3000")
    vConectado = IIf(Pegar_VL_Parametro("LIGACAOAUTOMATICA") = 1, "Sim", "N�o")
    
    If vConectado = "N�o" Then
        vConectado = IIf(Pegar_VL_Parametro("LigacaoAutomatica") = 1, "Sim", "N�o")
    End If
    
    txtIP4000 = vIP4000
    txtPorta4000 = vPorta4000
    txtIP3000 = vIP3000
    txtPorta3000 = vPorta3000
    cboConectado = vConectado

End Sub


Private Sub lstCidadesSemDDD_ItemCheck(Item As Integer)
    If lstCidadesSemDDD.Tag = "Carregando" Then Exit Sub
    If lstCidadesSemDDD.Selected(Item) = True Then
        Gravar lstCidadesSemDDD.List(Item), lstCidadesSemDDD.ItemData(Item)
    Else
        Deletar lstCidadesSemDDD.List(Item), lstCidadesSemDDD.ItemData(Item)
    End If
    lblQtd.Caption = lstCidadesSemDDD.SelCount
End Sub

Private Sub lsvCanalVoz_Click()
    txtDeposito = lsvCanalVoz.SelectedItem
    txtNumero = lsvCanalVoz.SelectedItem.SubItems(1)
End Sub

Private Sub Preencher_Cbos()
    
    Dim vrstnf As Object
    Dim vDBLink As String
    Dim vRstTempo As Object
    Dim vrstLink As Object
    Dim vRstLinkIdpk As Object
    Dim vRstSerie As Object
    Dim vSerie() As String
    Dim zz As Integer
    Dim ii As Integer
        
    vLoad = True
    
    Screen.MousePointer = vbHourglass
    
    stbBarra.Panels(1).Text = "Preenchendo combos..."
    
    Set vObjOracle = vVB_Generica_001.TabelaLoja(vBanco)
    Call vVB_Generica_001.PreencheComboList(vObjOracle, cboCds, "DEP�SITO")
    Call vVB_Generica_001.PreencheComboList(vObjOracle, cboCdsLink, "DEP�SITO")
    Call vVB_Generica_001.PreencheComboList(vObjOracle, CboCdsLinkIDPK, "DEP�SITO")
    Call vVB_Generica_001.PreencheComboList(vObjOracle, cboCDsRamal, "DEP�SITO")
    Call vVB_Generica_001.PreencheComboList(vObjOracle, lstLigAuto, "DEP�SITO")
    Call vVB_Generica_001.PreencheComboList(vObjOracle, lstLink, "DEP�SITO")
    Call vVB_Generica_001.PreencheComboList(vObjOracle, lstIDPK, "DEP�SITO")
    Call vVB_Generica_001.PreencheComboList(vObjOracle, lstEnderecoNF, "DEP�SITO")
    Call vVB_Generica_001.PreencheComboList(vObjOracle, cboCdTempo, "DEP�SITO")
    Call vVB_Generica_001.PreencheComboList(vObjOracle, LSTTEMPOS, "DEP�SITO")
    Call vVB_Generica_001.PreencheComboList(vObjOracle, lstSerie, "DEP�SITO")
    
    'Verificar quais os CDS que est�o imprimindo o Endereco de NF
    For i = 0 To cboCds.ListCount - 1
    
        stbBarra.Panels(1).Text = "Verificando CD: " & Val(cboCds.List(i))
    
        If Val(cboCds.List(i)) = 2 Or Val(cboCds.List(i)) = 6 Or Val(cboCds.List(i)) = 17 Then GoTo proximo
        If InStr(1, vCDDPK, Format(Val(cboCds.List(i)), "00")) > 0 Then
            Set vObjOracle = vBanco.CreateDynaset("SELECT VL_PARAMETRO FROM DEP" & Format(Val(cboCds.List(i)), "00") & ".PARAMETROS@lnk_CDdpk.CAL.COM.BR WHERE COD_SOFTWARE =1069 and NOME_PARAMETRO ='LIGACAOAUTOMATICA'", 0&)
            Set vrstnf = vBanco.CreateDynaset("SELECT VL_PARAMETRO FROM DEP" & Format(Val(cboCds.List(i)), "00") & ".PARAMETROS@lnk_CDdpk.CAL.COM.BR WHERE COD_SOFTWARE =1062 and NOME_PARAMETRO ='ENDERECO_NF'", 0&)
            Set vRstTempo = vBanco.CreateDynaset("SELECT VL_PARAMETRO FROM DEP" & Format(Val(cboCds.List(i)), "00") & ".PARAMETROS@lnk_CDdpk.CAL.COM.BR WHERE COD_SOFTWARE =1069 and NOME_PARAMETRO ='TEMPOLIGACAO'", 0&)
            Set vrstLink = vBanco.CreateDynaset("SELECT VL_PARAMETRO FROM DEP" & Format(Val(cboCdsLink.List(i)), "00") & ".PARAMETROS@lnk_CDdpk.CAL.COM.BR WHERE COD_SOFTWARE =1069 and NOME_PARAMETRO like 'LINK_CPS'", 0&)
            Set vRstLinkIdpk = vBanco.CreateDynaset("SELECT VL_PARAMETRO FROM DEP" & Format(Val(CboCdsLinkIDPK.List(i)), "00") & ".PARAMETROS@lnk_CDdpk.CAL.COM.BR WHERE COD_SOFTWARE =1069 and NOME_PARAMETRO like 'LINK_IDPK'", 0&)
            Set vRstSerie = vBanco.CreateDynaset("SELECT VL_PARAMETRO FROM DEP" & Format(Val(CboCdsLinkIDPK.List(i)), "00") & ".PARAMETROS@lnk_CDdpk.CAL.COM.BR WHERE COD_SOFTWARE =1062 and NOME_PARAMETRO = 'NAO_IMPRIMIR_SERIE'", 0&)
        
        Else
            Select Case Val(cboCds.List(i))
                Case 1
                    vDBLink = ""
                Case Else
                    vDBLink = "@LNK_CD" & Format(Val(cboCds.List(i)), "00") & ".cal.com.br"
            End Select
            
            If fTesteConexao(vDBLink) = False Then GoTo proximo
            
            Set vObjOracle = vBanco.CreateDynaset("SELECT VL_PARAMETRO FROM HELPDESK.PARAMETROS" & vDBLink & " WHERE COD_SOFTWARE =1069 and UPPER(NOME_PARAMETRO) ='LIGACAOAUTOMATICA'", 0&)
            Set vrstnf = vBanco.CreateDynaset("SELECT VL_PARAMETRO FROM HELPDESK.PARAMETROS" & vDBLink & " WHERE COD_SOFTWARE =1062 and NOME_PARAMETRO ='ENDERECO_NF'", 0&)
            Set vRstTempo = vBanco.CreateDynaset("SELECT VL_PARAMETRO FROM HELPDESK.PARAMETROS" & vDBLink & " WHERE COD_SOFTWARE =1069 and NOME_PARAMETRO ='TEMPOLIGACAO'", 0&)
            Set vrstLink = vBanco.CreateDynaset("SELECT VL_PARAMETRO FROM HELPDESK.PARAMETROS" & vDBLink & " WHERE COD_SOFTWARE =1069 and NOME_PARAMETRO like 'LINK_CPS'", 0&)
            Set vRstLinkIdpk = vBanco.CreateDynaset("SELECT VL_PARAMETRO FROM HELPDESK.PARAMETROS" & vDBLink & " WHERE COD_SOFTWARE =1069 and NOME_PARAMETRO like 'LINK_IDPK'", 0&)
            Set vRstSerie = vBanco.CreateDynaset("SELECT VL_PARAMETRO FROM HELPDESK.PARAMETROS" & vDBLink & " WHERE COD_SOFTWARE =1062 and NOME_PARAMETRO = 'NAO_IMPRIMIR_SERIE'", 0&)
        End If
    
        If vObjOracle.EOF = False Then lstLigAuto.Selected(i) = Val(vObjOracle!vl_parametro)
        If vrstnf.EOF = False Then lstEnderecoNF.Selected(i) = Val(vrstnf!vl_parametro)
        If vRstTempo.EOF = False Then LSTTEMPOS.List(i) = Left(Trim(LSTTEMPOS.List(i)), 15) & Space(15 - Len(Left(LSTTEMPOS.List(i), 15))) & Space(15) & " - " & vRstTempo!vl_parametro
        If vrstLink.EOF = False Then lstLink.Selected(i) = Val(vrstLink!vl_parametro)
        If vRstLinkIdpk.EOF = False Then lstIDPK.Selected(i) = Val(vRstLinkIdpk!vl_parametro)
        
        If vRstSerie.EOF = False Then Me.lstSerie.Selected(i) = Val(vRstSerie!vl_parametro)
        
proximo:
    
    Next
    
    stbBarra.Panels(1).Text = ""
    
    Screen.MousePointer = vbNormal
    vLoad = False
    
End Sub



Private Sub txtDeposito_KeyPress(KeyAscii As Integer)
    If KeyAscii <> 8 And KeyAscii <> 13 Then
        txtDeposito = txtDeposito & UCase(Chr(KeyAscii))
        KeyAscii = 0
    End If
End Sub

Private Sub txtIP3000_Change()
    txtIP4000 = ""
    txtPorta4000 = ""
End Sub

Private Sub txtIP4000_Change()
    txtIP3000 = ""
    txtPorta3000 = ""
End Sub

Public Sub Carregar_Cidades()

    Dim vObjCidades As Object
    
    lstCidadesSemDDD.Clear
    
    vBanco.Parameters.Remove "CodLojaCidade"
    vBanco.Parameters.Add "CodLojaCidade", vNumDep, 1
    Criar_Cursor
    vErro = vVB_Generica_001.ExecutaPl(vBanco, "Producao.PCK_VDA231.Pr_Select_Cidades_Loja(:vCursor, :CodLojaCidade)")

    If vErro <> "" Then
        MsgBox vErro
        Exit Sub
    Else
        Set vObjCidades = vBanco.Parameters("vCursor").Value
    End If

    For i = 1 To vObjCidades.RecordCount
        lstCidadesSemDDD.AddItem vObjCidades("Nome_cidade")
        lstCidadesSemDDD.ItemData(lstCidadesSemDDD.NewIndex) = vObjCidades("Cod_cidade")
        vObjCidades.MoveNext
    Next

End Sub

Public Sub Cidades_Selecionadas()

    Dim vOwner As String
    Dim vObjCidades As Object
    Dim ii As Integer
    
    If vDEP = "PRODUCAO" Then
        vOwner = "HELPDESK."
    Else
        vOwner = vDEP & "."
    End If

    vBanco.Parameters.Remove "Cod_Soft": vBanco.Parameters.Add "Cod_Soft", Pegar_Cod_Sistema, 1
    vBanco.Parameters.Remove "Dep": vBanco.Parameters.Add "DEP", vOwner, 1
    Criar_Cursor

    vErro = vVB_Generica_001.ExecutaPl(vBanco, "PRODUCAO.PCK_VDA231.Pr_Select_Parametros_Cidades(:vCursor, :DEP, :Cod_Soft)")

    If Trim(vErro) <> "" Then
        MsgBox "Descri��o do Erro:" & vErro
        Exit Sub
    Else
        Set vObjCidades = vBanco.Parameters("vCursor").Value
    End If
    
    For i = 0 To lstCidadesSemDDD.ListCount - 1
        vObjCidades.MoveFirst
        For ii = 1 To vObjCidades.RecordCount
            If UCase(lstCidadesSemDDD.List(i)) = UCase(vObjCidades("Nome_parametro")) Then
               lstCidadesSemDDD.Selected(i) = True
               Exit For
            End If
            vObjCidades.MoveNext
        Next
        
    Next
    
    lblQtd.Caption = lstCidadesSemDDD.SelCount

End Sub

Public Function Pegar_VL_Parametro(NOME_PARAMETRO As String)
    
    Dim vOwner As String
    
    If vDEP = "PRODUCAO" Or vDEP = "" Then
        vOwner = "HELPDESK."
        If vDEP = "" Then vDEP = "PRODUCAO"
    Else
        vOwner = vDEP & "."
    End If
    
    Criar_Cursor

    vBanco.Parameters.Remove "Cod_Sistema"
    vBanco.Parameters.Add "Cod_Sistema", Pegar_Cod_Sistema, 1

    vBanco.Parameters.Remove "Nome_Parametro"
    vBanco.Parameters.Add "Nome_Parametro", NOME_PARAMETRO, 1

    vBanco.Parameters.Remove "DEP"
    vBanco.Parameters.Add "DEP", vOwner, 1

    vErro = vVB_Generica_001.ExecutaPl(vBanco, vDEP & ".PCK_VDA231.PR_SELECT_VL_PARAMETRO(:vCursor, :Cod_Sistema, :Nome_Parametro, :DEP)")

    If vErro = "" Then
        Set vObjOracle = vBanco.Parameters("vCursor").Value
        Pegar_VL_Parametro = vObjOracle("VL_PARAMETRO")
    End If
End Function

Public Function Pegar_Cod_Sistema()

    vBanco.Parameters.Remove "Nome_Software"
    vBanco.Parameters.Add "Nome_Software", txtSistema, 1

    vBanco.Parameters.Remove "Cod_Software"
    vBanco.Parameters.Add "Cod_Software", 0, 2

    vVB_Generica_001.ExecutaPl vBanco, "PRODUCAO.PCK_VDA231.PR_SELECT_COD_SOFTWARE(:Nome_Software, :Cod_Software)"

    Pegar_Cod_Sistema = vBanco.Parameters("Cod_Software")

    vBanco.Parameters.Remove "Cod_Software"

End Function

Public Sub Criar_Cursor()
    vBanco.Parameters.Remove "vCursor"
    vBanco.Parameters.Add "vCursor", 0, 2
    vBanco.Parameters("vCursor").ServerType = ORATYPE_CURSOR
    vBanco.Parameters("vCursor").DynasetOption = ORADYN_NO_BLANKSTRIP
    vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
End Sub

Sub carregar_lsv_Canal()

    Dim vOwner As String
    Dim vObjCidades As Object
    Dim lItem As ListItem
    
    If vDEP = "PRODUCAO." Or vDEP = "PRODUCAO" Then
       vOwner = "HELPDESK."
    Else
       vOwner = vDEP & "."
    End If
    
    vBanco.Parameters.Remove "Cod_Soft": vBanco.Parameters.Add "Cod_Soft", 1069, 1
    vBanco.Parameters.Remove "Dep": vBanco.Parameters.Add "DEP", vOwner, 1
    Criar_Cursor

    vErro = vVB_Generica_001.ExecutaPl(vBanco, "PRODUCAO.PCK_VDA231.Pr_Select_Parametros_Cidades(:vCursor, :DEP, :Cod_Soft)")

    If Trim(vErro) <> "" Then
        MsgBox "Sub: Carregar_lsv_Canal" & vbCrLf & "Descri��o:" & vErro, , "Aten�ao"
        Exit Sub
    Else
        Set vObjCidades = vBanco.Parameters("vCursor").Value
    End If
    
    lsvCanalVoz.ListItems.Clear
    
    For i = 1 To vObjCidades.RecordCount
        
        If Left(vObjCidades("NOME_PARAMETRO"), 3) = "CV-" Then
            Set lItem = lsvCanalVoz.ListItems.Add
            lItem = Mid(vObjCidades!NOME_PARAMETRO, 4)
            lItem.SubItems(1) = vObjCidades!vl_parametro
        End If
        vObjCidades.MoveNext
    
    Next

End Sub

Sub Gravar_Canal(pNomeParam As String, pValorParam As String)
    
    Dim vOwner As String
    
    Set vObjOracle = vVB_Generica_001.TabelaLoja(vBanco)
    
    lstDepositosOK.Clear
    
    For i = 1 To vObjOracle.RecordCount
        If Val(vObjOracle.Fields(0)) = 0 Or Val(vObjOracle.Fields(0)) = 6 Then GoTo proximo
        
        If InStr(1, vCDDPK, Format(CStr(Val(vObjOracle.Fields(0))), "00")) Then
           vErro = vVB_Generica_001.ConectaOracle("CDDPK", "vda020", False, frmVda231)
           vOwner = "PRODUCAO."
           vDEP = vOwner
           Set vSessao = vVB_Generica_001.vSessao
           Set vBanco = vVB_Generica_001.vBanco
        Else
            If Val(vObjOracle.Fields(0)) = 1 Then
               vErro = vVB_Generica_001.ConectaOracle("PRODUCAO", "vda020", False, frmVda231)
            Else
               vErro = vVB_Generica_001.ConectaOracle("CD" & Format(CStr(Val(vObjOracle.Fields(0))), "00"), "vda020", False, frmVda231)
            End If
            Set vSessao = vVB_Generica_001.vSessao
            Set vBanco = vVB_Generica_001.vBanco
            vOwner = "HELPDESK."
            vDEP = "PRODUCAO."
        End If
        
        vBanco.Parameters.Remove "Cod_Soft": vBanco.Parameters.Add "Cod_Soft", Pegar_Cod_Sistema, 1
        vBanco.Parameters.Remove "Nome_Param": vBanco.Parameters.Add "Nome_Param", pNomeParam, 1
        vBanco.Parameters.Remove "VL_PARAM": vBanco.Parameters.Add "VL_PARAM", pValorParam, 1
        vBanco.Parameters.Remove "Dep": vBanco.Parameters.Add "DEP", vOwner, 1
        vBanco.Parameters.Remove "Txt_Erro": vBanco.Parameters.Add "Txt_Erro", "", 2
    
        If vVB_Generica_001.ExecutaPl(vBanco, vDEP & "PCK_VDA231.Pr_Salvar_Parametros(:Nome_Param, :VL_PARAM, :Cod_Soft, :DEP, :Txt_Erro)") <> "" Then
            lstDepositosOK.AddItem vObjOracle.Fields(0) & " - Erro"
        Else
            lstDepositosOK.AddItem vObjOracle.Fields(0) & " - OK"
        End If
proximo:
        vObjOracle.MoveNext
    Next

    'Voltar conectar no banco inicial
    If Val(lblDeposito) = 0 Or Val(lblDeposito) = 6 Then Exit Sub
    
    If InStr(1, vCDDPK, Format(Val(lblDeposito), "00")) Then
       vErro = vVB_Generica_001.ConectaOracle("CDDPK", "vda020", False, frmVda231)
       vOwner = "PRODUCAO."
       vDEP = vOwner
       Set vSessao = vVB_Generica_001.vSessao
       Set vBanco = vVB_Generica_001.vBanco
    Else
        If Val(lblDeposito) = 1 Then
           vErro = vVB_Generica_001.ConectaOracle("PRODUCAO", "vda020", False, frmVda231)
        Else
           vErro = vVB_Generica_001.ConectaOracle("CD" & Format(CStr(Val(lblDeposito)), "00"), "vda020", False, frmVda231)
        End If
        Set vSessao = vVB_Generica_001.vSessao
        Set vBanco = vVB_Generica_001.vBanco
        vOwner = "HELPDESK."
        vDEP = "PRODUCAO."
    End If

End Sub


Sub Deletar_Canal(pNomeParam As String, pValorParam As String)

    Dim vOwner As String
    
    Set vObjOracle = vVB_Generica_001.TabelaLoja(vBanco)
    
    For i = 1 To vObjOracle.RecordCount
        If Val(vObjOracle.Fields(0)) = 0 Or Val(vObjOracle.Fields(0)) = 6 Then GoTo proximo
        
        If InStr(1, vCDDPK, Format(CStr(Val(vObjOracle.Fields(0))), "00")) Then
           vErro = vVB_Generica_001.ConectaOracle("CDDPK", "vda020", False, frmVda231)
           vOwner = "DEP" & Format(Val(vObjOracle.Fields(0)), "00") & "."
           vDEP = vOwner
           Set vSessao = vVB_Generica_001.vSessao
           Set vBanco = vVB_Generica_001.vBanco
        Else
            If Val(vObjOracle.Fields(0)) = 1 Then
               vErro = vVB_Generica_001.ConectaOracle("PRODUCAO", "vda020", False, frmVda231)
            Else
               vErro = vVB_Generica_001.ConectaOracle("CD" & Format(CStr(Val(vObjOracle.Fields(0))), "00"), "vda020", False, frmVda231)
            End If
            Set vSessao = vVB_Generica_001.vSessao
            Set vBanco = vVB_Generica_001.vBanco
            vOwner = "HELPDESK."
            vDEP = "PRODUCAO."
        End If
        
        vBanco.Parameters.Remove "Cod_Soft": vBanco.Parameters.Add "Cod_Soft", Pegar_Cod_Sistema, 1
        vBanco.Parameters.Remove "Nome_Param": vBanco.Parameters.Add "Nome_Param", pNomeParam, 1
        vBanco.Parameters.Remove "VL_PARAM": vBanco.Parameters.Add "VL_PARAM", pValorParam, 1
        vBanco.Parameters.Remove "Dep": vBanco.Parameters.Add "DEP", vOwner, 1
        vBanco.Parameters.Remove "Txt_Erro": vBanco.Parameters.Add "Txt_Erro", "", 2

        If vVB_Generica_001.ExecutaPl(vBanco, vDEP & "PCK_VDA231.Pr_Deletar_Parametros(:Nome_Param, :Cod_Soft, :DEP, :Txt_Erro)") <> "" Then
            lstDepositosOK.AddItem vObjOracle.Fields(0) & " - Erro"
        Else
            lstDepositosOK.AddItem vObjOracle.Fields(0) & " - OK"
        End If
proximo:
        vObjOracle.MoveNext
    Next

End Sub

Private Sub txtNumero_KeyPress(KeyAscii As Integer)
    If KeyAscii < 47 Or KeyAscii > 58 Then
        If KeyAscii <> 8 Then
            KeyAscii = 0
        End If
    End If
End Sub

Sub Status_TLMKT_Ligacao_AUTO(pAcao As Byte)
    Dim vDBLink As String

    If Val(cboCDsRamal) = 6 Or Val(cboCDsRamal) = 2 Or Val(cboCDsRamal) = 20 Then Exit Sub
    
    If MsgBox("Confirma ?", vbQuestion + vbYesNo, "Aten��o") = vbNo Then
        Exit Sub
    End If
    If InStr(1, vCDDPK, Format(Val(cboCDsRamal), "00")) > 0 Then
        If pAcao = 1 Or pAcao = 2 Then
            vBanco.ExecuteSQL "BEGIN DELETE FROM DEP" & Format(Val(cboCDsRamal), "00") & ".PARAMETROS@lnk_CDdpk.CAL.COM.BR WHERE VL_PARAMETRO='" & txtTLMKT & "' AND COD_SOFTWARE =1069 and upper(NOME_PARAMETRO) ='USUARIO'; COMMIT; END;"
        End If
        If pAcao = 1 Then
            vBanco.ExecuteSQL "BEGIN INSERT INTO DEP" & Format(Val(cboCDsRamal), "00") & ".PARAMETROS@lnk_CDdpk.CAL.COM.BR VALUES (1069, 'USUARIO','" & txtTLMKT & "'); COMMIT; END;"
        End If
    Else
       Select Case Val(cboCDsRamal)
            Case 1
                vDBLink = ""
            Case Else
                vDBLink = "@LNK_CD" & Format(Val(cboCDsRamal), "00") & ".cal.com.br"
        End Select
        If pAcao = 1 Then
            vBanco.ExecuteSQL "BEGIN DELETE FROM HELPDESK.PARAMETROS" & vDBLink & " WHERE VL_PARAMETRO='" & txtTLMKT & "' AND COD_SOFTWARE =1069 and upper(NOME_PARAMETRO) ='USUARIO'; COMMIT; END;"
        End If
        If pAcao = 2 Or pAcao = 3 Then
            vBanco.ExecuteSQL "BEGIN DELETE FROM HELPDESK.PARAMETROS" & vDBLink & " WHERE VL_PARAMETRO='" & txtTLMKT & "' AND COD_SOFTWARE =1069 and upper(NOME_PARAMETRO) ='USUARIO'; COMMIT; END;"
            vBanco.ExecuteSQL "BEGIN INSERT INTO HELPDESK.PARAMETROS" & vDBLink & " VALUES (1069,'USUARIO','" & txtTLMKT & "'); COMMIT; END;"
        End If
    End If
End Sub

Private Sub txtTempo_KeyPress(KeyAscii As Integer)
    If InStr(1, "0123456789", Chr(KeyAscii)) = 0 And KeyAscii <> 8 Then KeyAscii = 0
End Sub

Sub Enviar_Email(pTexto As String, pQual As String)
        
    Dim vrstEmail As Object
    Dim vNomeusuario As String
    
    vNomeusuario = BuscaUSERNAME
    
    Set vrstEmail = vBanco.CreateDynaset("Select vl_parametro from Helpdesk.parametros where cod_software=1069 and nome_parametro='EMAIL'", 0&)
        
    For i = 1 To vrstEmail.RecordCount
        
        vBanco.Parameters.Remove "Para"
        vBanco.Parameters.Add "Para", vrstEmail!vl_parametro, 1
    
        vBanco.Parameters.Remove "Titulo"
        vBanco.Parameters.Add "Titulo", pQual, 1
        
        vBanco.Parameters.Remove "Conteudo"
        If UCase(pQual) <> UCase("Ligacao Automatica") Then
            vBanco.Parameters.Add "Conteudo", "Link com Problemas: " & pTexto & vbCrLf & "CD: " & cboCdsLink & vbCrLf & "Data e Hora: " & Now & vbCrLf & "Usuario: " & vNomeusuario, 1
        Else
            vBanco.Parameters.Add "Conteudo", "Ligacao Automatica: " & pTexto & vbCrLf & "CD: " & cboCds & vbCrLf & "Data e Hora: " & Now & vbCrLf & "Usuario: " & vNomeusuario, 1
        End If
        
        vBanco.ExecuteSQL "BEGIN INTRANET.ENVIA_EMAIL (:para, :titulo, :conteudo); END;"
    
        vrstEmail.MoveNext
    
    Next
    
    vrstEmail.Close
    Set vrstEmail = Nothing
End Sub


Function BuscaUSERNAME() As String

    On Error GoTo TrataErro

    Dim sBuffer As String
    Dim lSize As Long

    sBuffer = Space$(255)
    lSize = Len(sBuffer)
    GetUserName sBuffer, lSize

    If lSize > 0 Then
        BuscaUSERNAME = Left$(sBuffer, lSize - 1)
    End If
    Exit Function

TrataErro:
    BuscaUSERNAME = "Houve Erro"


End Function


'1-Ativar
'2-Desativar
'3-Adicionar
Sub Status_LINK(pAcao As Byte, pParametro As String, pCD As Byte)
    Dim vDBLink As String

    If pCD = 6 Or pCD = 2 Or pCD = 20 Then Exit Sub
    
    If MsgBox(IIf(pAcao = 3, "Confirma ?", "Confirma a altera��o do Par�metro ?"), vbQuestion + vbYesNo, "Aten��o") = vbNo Then
        Exit Sub
    End If
    If pAcao = 3 Then 'Inserir
        If InStr(1, vCDDPK, Format(pCD, "00")) > 0 Then
           vBanco.ExecuteSQL "BEGIN INSERT INTO DEP" & Format(pCD, "00") & ".PARAMETROS@lnk_CDdpk.CAL.COM.BR VALUES (1069,'" & pParametro & "','1'); COMMIT; END;"
        Else
           Select Case pCD
                Case 1
                    vDBLink = ""
                Case Else
                    vDBLink = "@LNK_CD" & Format(pCD, "00") & ".cal.com.br"
            End Select
            vBanco.ExecuteSQL "BEGIN INSERT INTO HELPDESK.PARAMETROS" & vDBLink & " VALUES (1069,'" & pParametro & "','1'); COMMIT; END;"
        End If
    Else
        If InStr(1, vCDDPK, Format(pCD, "00")) > 0 Then
           vBanco.ExecuteSQL "BEGIN UPDATE DEP" & Format(pCD, "00") & ".PARAMETROS@lnk_CDdpk.CAL.COM.BR SET VL_PARAMETRO='" & IIf(pAcao = 1, 1, 0) & "' WHERE COD_SOFTWARE =1069 and upper(NOME_PARAMETRO) ='" & pParametro & "'; COMMIT; END;"
        Else
           Select Case pCD
                Case 1
                    vDBLink = ""
                Case Else
                    vDBLink = "@LNK_CD" & Format(pCD, "00") & ".cal.com.br"
            End Select
            vBanco.ExecuteSQL "BEGIN UPDATE HELPDESK.PARAMETROS" & vDBLink & " SET VL_PARAMETRO = '" & IIf(pAcao = 1, 1, 0) & "' WHERE COD_SOFTWARE =1069 and UPPER(NOME_PARAMETRO) ='" & pParametro & "'; COMMIT; END;"
        End If
    End If
End Sub


Function fTesteConexao(pLink As String) As Boolean
    On Error GoTo trata_erro
    
    Dim vObj As Object
    
    Set vObj = vBanco.CreateDynaset("SELECT VL_PARAMETRO FROM HELPDESK.PARAMETROS" & pLink & " WHERE COD_SOFTWARE =1069 and UPPER(NOME_PARAMETRO) ='LIGACAOAUTOMATICA'", 0&)

trata_erro:
    If Err.Number > 0 Then
        fTesteConexao = False
    Else
        fTesteConexao = True
    End If
End Function

Sub CarregarPromotores()
    Dim vObj As Object
    Dim vStr() As String
    Dim i As Integer
    Dim ii As Integer
    
    Set vObj = vBanco.CreateDynaset("Select * from helpdesk.parametros where nome_parametro like '%PROMOTOR_%' AND COD_SOFTWARE = 508", 0&)
    
    For i = 1 To vObj.RecordCount
        vStr = Split(vObj!vl_parametro, ";")
    
        For ii = 0 To UBound(vStr())
            Me.lstPromotores.AddItem Val(vStr(ii))
        Next
        vObj.MoveNext
    Next
    
    Set vObj = Nothing
    
    Salvar_Posicao "LOAD"
    
    
End Sub

Sub Salvar_Posicao(pLocal As String)
    Dim vLista As String
    Dim vPromotores(1, 20) As String
    Dim vPos As Byte
    Dim vArq As Integer
    
    For i = 0 To lstPromotores.ListCount - 1
    
        If Trim(vLista) = "" Then
            vLista = lstPromotores.List(i)
        Else
            vLista = vLista & ";" & lstPromotores.List(i)
        End If
    
        If Len(Trim(vLista)) >= 146 Then
           vPromotores(0, vPos) = "PROMOTOR_" & vPos + 1
           vPromotores(1, vPos) = vLista
           vLista = ""
           vPos = vPos + 1
        End If
    
    Next
    If vLista <> "" Then
        vPromotores(0, vPos) = "PROMOTOR_" & vPos + 1
        vPromotores(1, vPos) = vLista
    End If
    
    'Salvar Posicao antes de gravar.
    'Se o arquivo j� existir, apenas adicionar linhas
    'Senao criar o arquivo
    vArq = FreeFile
    If Dir(App.Path & "\Parametros_Promotores.txt", vbArchive) = "" Then
        Open App.Path & "\Parametros_Promotores.txt" For Output As #vArq
    Else
        Open App.Path & "\Parametros_Promotores.txt" For Append As #vArq
    End If
    
    Print #vArq, pLocal & " - " & Now
    
    For i = 0 To 20

        If vPromotores(0, i) = "" Then
            Exit For
        End If
        Print #vArq, vPromotores(0, i) & " - " & vPromotores(1, i)
    Next
    Close #vArq
    
End Sub
