create or replace package PRODUCAO.PCK_VDA231 is

  -- Author  : EDUARDO.RELVAS
  -- Created : 05-mai-05 15:40:30
  -- Contralar Parametros em Geral
  
  TYPE tp_cursor IS REF CURSOR;
  vSql varchar2(4000);  
  
  Procedure Pr_Select_Cidades_Loja (
                                    PM_CURSOR IN OUT TP_CURSOR,
                                    PM_COD_LOJA IN NUMBER
                                   );
  Procedure Pr_Select_Parametros_Cidades (
                                          PM_CURSOR IN OUT TP_CURSOR,
                                          PM_DEP IN CHAR,
                                          PM_COD_SOFT Number
                                         );
  Procedure Pr_Select_Depositos (
                                PM_CURSOR IN OUT TP_CURSOR
                                );
  
  PROCEDURE PR_SALVAR_PARAMETROS(PM_NOME_PARAM 	IN 	VARCHAR2,
                               PM_VALOR_PARAM 	IN 	VARCHAR2,
                               PM_COD_SOFT 		  IN 	HELPDESK.SOFTWARE.COD_SOFTWARE%TYPE,
                               PM_DEP           IN  Varchar2,
                               PM_TXTERRO 		  OUT	VARCHAR2);
  
  PROCEDURE PR_DELETAR_PARAMETROS(PM_NOME_PARAM 	IN 	VARCHAR2,
                                  PM_COD_SOFT 		  IN 	HELPDESK.SOFTWARE.COD_SOFTWARE%TYPE,
                                  PM_DEP           IN  Varchar2,
                                  PM_TXTERRO 		  OUT	VARCHAR2);
  PROCEDURE PR_SELECT_COD_SOFTWARE (
                                   PM_NOME_SISTEMA in HELPDESK.SOFTWARE.NOME_SOFTWARE%TYPE,
					                         PM_COD_SISTEMA  OUT Number
                                   );
  PROCEDURE PR_SELECT_VL_PARAMETRO (
                                   PM_CURSOR1 IN OUT TP_CURSOR,
                                   PM_COD_SISTEMA IN NUMBER,
					                         PM_NOME_PARAM  IN VARCHAR2,
                                   PM_DEP         IN VARCHAR2
                                   );
end PCK_VDA231;
/
create or replace package body PRODUCAO.PCK_VDA231 is

  Procedure Pr_Select_Cidades_Loja (
                                    PM_CURSOR IN OUT TP_CURSOR,
                                    PM_COD_LOJA IN NUMBER
                                   ) is
     vUF char(2):='';
     BEGIN                                   
        SELECT B.Cod_Uf 
        INTO   vUF 
        FROM   Loja A, cidade B
        WHERE  A.Cod_Cidade = B.Cod_Cidade 
           and A.Cod_Loja = PM_COD_LOJA;

        OPEN PM_CURSOR FOR
            Select COD_CIDADE, NOME_CIDADE
            FROM   CIDADE
            where  cod_uf = vUF
            ORDER BY NOME_CIDADE;
  End;
 
  Procedure Pr_Select_Parametros_Cidades (
                                          PM_CURSOR IN OUT TP_CURSOR,
                                          PM_DEP IN CHAR,
                                          PM_COD_SOFT IN Number
                                         ) is
    BEGIN

          vSql := 'SELECT * FROM ' || PM_DEP || 'PARAMETROS ';
          vSql := vSql || ' WHERE COD_SOFTWARE = ' || PM_COD_SOFT; 
           
         OPEN PM_CURSOR FOR
              vSql;
  End;
  
  Procedure Pr_Select_Depositos (
                                PM_CURSOR IN OUT TP_CURSOR
                                ) is
    BEGIN                                
      OPEN PM_CURSOR FOR
           Select * from Loja;
  End; 

  PROCEDURE PR_SALVAR_PARAMETROS(PM_NOME_PARAM 	IN 	VARCHAR2,
                               PM_VALOR_PARAM 	IN 	VARCHAR2,
                               PM_COD_SOFT 		  IN 	HELPDESK.SOFTWARE.COD_SOFTWARE%TYPE,
                               PM_DEP           IN  Varchar2,
                               PM_TXTERRO 		  OUT	VARCHAR2) IS
      vSql varchar2(4000):='';                         
      BEGIN
           vSql := 'DELETE ' || PM_DEP || 'PARAMETROS ';
           vSql := vSql || ' WHERE COD_SOFTWARE = ' || PM_COD_SOFT || ' AND '; 
           vSql := vSql || ' NOME_PARAMETRO = ' || chr(39) || PM_NOME_PARAM || chr(39);
           
           Execute Immediate vSql;
                   
           vSql := 'INSERT INTO ' || PM_DEP || 'PARAMETROS 
                    VALUES (' || PM_COD_SOFT || ',' || chr(39) || PM_NOME_PARAM || chr(39) || ',' || chr(39) || PM_VALOR_PARAM || chr(39) ||')';
            
           Execute Immediate vSql;

           commit;
      
           EXCEPTION
           WHEN OTHERS THEN PM_TXTERRO := SQLERRM;

  End;
  
  PROCEDURE PR_DELETAR_PARAMETROS(PM_NOME_PARAM 	IN 	VARCHAR2,
                                  PM_COD_SOFT 		  IN 	HELPDESK.SOFTWARE.COD_SOFTWARE%TYPE,
                                  PM_DEP           IN  Varchar2,
                                  PM_TXTERRO 		  OUT	VARCHAR2) IS
      vSql varchar2(4000):='';                         
      BEGIN
           vSql := 'DELETE ' || PM_DEP || 'PARAMETROS ';
           vSql := vSql || ' WHERE COD_SOFTWARE = ' || PM_COD_SOFT || ' AND '; 
           vSql := vSql || ' NOME_PARAMETRO = ' || chr(39) || PM_NOME_PARAM || chr(39);
           
           Execute Immediate vSql;
                   
           commit;
      
           EXCEPTION
           WHEN OTHERS THEN PM_TXTERRO := SQLERRM;

  End;  
  PROCEDURE PR_SELECT_COD_SOFTWARE (
                                   PM_NOME_SISTEMA in HELPDESK.SOFTWARE.NOME_SOFTWARE%TYPE,
					                         PM_COD_SISTEMA  OUT Number
                                   ) is
    BEGIN
         SELECT COD_SOFTWARE
         INTO   PM_COD_SISTEMA
         FROM   HELPDESK.SOFTWARE
         WHERE  NOME_SOFTWARE = PM_NOME_SISTEMA;

  END PR_SELECT_COD_SOFTWARE;

  PROCEDURE PR_SELECT_VL_PARAMETRO (
                                   PM_CURSOR1 IN OUT TP_CURSOR,
                                   PM_COD_SISTEMA IN NUMBER,
					                         PM_NOME_PARAM  IN VARCHAR2,
                                   PM_DEP         IN VARCHAR2
                                   ) is
    vSql VARCHAR2(4000) :='';

    BEGIN
    
           vSql := 'SELECT * FROM ' || PM_DEP || 'PARAMETROS ';
           vSql := vSql || 'WHERE COD_SOFTWARE = ' || PM_COD_SISTEMA || ' AND ';
           vSql := vSql || 'NOME_PARAMETRO = ' || chr(39) || PM_NOME_PARAM || chr(39);

           OPEN PM_CURSOR1 FOR
                vSql;              

  END PR_SELECT_VL_PARAMETRO;

  
end PCK_VDA231;
/
