VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form frmEnviarEmail 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Enviar Email para"
   ClientHeight    =   5415
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5595
   Icon            =   "frmEnviarEmail.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   361
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   373
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame2 
      Caption         =   "Enviar Email para:"
      Height          =   4455
      Left            =   30
      TabIndex        =   2
      Top             =   900
      Width           =   5535
      Begin VB.ListBox lstEmail 
         Appearance      =   0  'Flat
         Height          =   2955
         Left            =   60
         TabIndex        =   4
         Top             =   1410
         Width           =   5415
      End
      Begin VB.TextBox txtEmail 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   90
         TabIndex        =   3
         Top             =   480
         Width           =   5385
      End
      Begin Bot�o.cmd cmdSalvar 
         Height          =   495
         Left            =   60
         TabIndex        =   5
         TabStop         =   0   'False
         ToolTipText     =   "Salvar"
         Top             =   810
         Width           =   495
         _ExtentX        =   873
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmEnviarEmail.frx":23D2
         PICN            =   "frmEnviarEmail.frx":23EE
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdExcluirEmail 
         Height          =   495
         Left            =   600
         TabIndex        =   6
         TabStop         =   0   'False
         ToolTipText     =   "Excluir"
         Top             =   810
         Width           =   495
         _ExtentX        =   873
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmEnviarEmail.frx":2988
         PICN            =   "frmEnviarEmail.frx":29A4
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label Label11 
         AutoSize        =   -1  'True
         Caption         =   "E-mail:"
         Height          =   195
         Left            =   90
         TabIndex        =   7
         Top             =   270
         Width           =   465
      End
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   0
      Top             =   810
      Width           =   4875
      _ExtentX        =   8599
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmEnviarEmail.frx":2F3E
      PICN            =   "frmEnviarEmail.frx":2F5A
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmEnviarEmail"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdSalvar_Click()
    If txtEmail = "" Then
        MsgBox "Informe o E-mail.", vbInformation, "Aten��o"
        txtEmail.SetFocus
        Exit Sub
    End If
    vBanco.ExecuteSQL "BEGIN INSERT INTO HELPDESK.PARAMETROS VALUES (1069, 'EMAIL','" & txtEmail & "'); COMMIT; END;"
    
    If Check1.Value + Check2.Value + Check3.Value + Check4.Value >= 1 Then
        vBanco.ExecuteSQL "BEGIN DELETE FROM HELPDESK.PARAMETROS WHERE COD_SOFTWARE = 1069 AND NOME_PARAMETRO = 'PERMISSAO_' AND '" & txtLogin & ";" & txtSenha & ":" & Check1.Value + Check2.Value + Check3.Value + Check4.Value & "'); COMMIT; END;"
        vBanco.ExecuteSQL "BEGIN INSERT INTO HELPDESK.PARAMETROS VALUES (1069, 'PERMISSAO','" & txtLogin & ";" & txtSenha & ";" & Check1.Value + Check2.Value + Check3.Value + Check4.Value & "'); COMMIT; END;"
    End If
    
    Preencher_Email
    
    txtEmail = ""
    
End Sub
Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub cmdExcluirEmail_Click()
    If Me.txtEmail = "" Then
        txtEmail.SetFocus
        MsgBox "Informe o E-mail a ser exclu�do.", vbInformation, "Aten��o"
        Exit Sub
    End If
    
    vBanco.ExecuteSQL "BEGIN DELETE FROM HELPDESK.PARAMETROS WHERE COD_SOFTWARE = 1069 AND NOME_PARAMETRO='EMAIL' AND VL_PARAMETRO='" & txtEmail & "'; COMMIT; END;"
    
    Preencher_Email
    
    txtEmail = ""

End Sub

Private Sub Form_Load()

    Preencher_Email

End Sub

Sub Preencher_Email()

    Dim vrstEmail As Object
    
    Set vrstEmail = vBanco.CreateDynaset("Select vl_parametro from Helpdesk.parametros where cod_software=1069 and nome_parametro='EMAIL'", 0&)

    lstEmail.Clear

    For i = 1 To vrstEmail.RecordCount
        lstEmail.AddItem vrstEmail!vl_parametro
        vrstEmail.MoveNext
    Next

    vrstEmail.Close
    Set vrstEmail = Nothing

End Sub

Private Sub lstEmail_Click()
    Me.txtEmail = Me.lstEmail
End Sub

