VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form frmParametros 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Parametros"
   ClientHeight    =   4980
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4575
   ControlBox      =   0   'False
   Icon            =   "frmParametros.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   332
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   305
   StartUpPosition =   2  'CenterScreen
   Begin VB.ListBox lstCidadesSemDDD 
      Appearance      =   0  'Flat
      Height          =   2505
      Left            =   60
      Style           =   1  'Checkbox
      TabIndex        =   13
      Top             =   2400
      Width           =   4485
   End
   Begin VB.TextBox txtPorta3000 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   3510
      TabIndex        =   3
      Top             =   1290
      Width           =   975
   End
   Begin VB.TextBox txtPorta4000 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   3510
      TabIndex        =   1
      Top             =   900
      Width           =   975
   End
   Begin VB.TextBox txtIP3000 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1050
      TabIndex        =   2
      Top             =   1290
      Width           =   1635
   End
   Begin VB.TextBox txtIP4000 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   1050
      TabIndex        =   0
      Top             =   900
      Width           =   1635
   End
   Begin VB.ComboBox cboConectado 
      Appearance      =   0  'Flat
      Height          =   315
      ItemData        =   "frmParametros.frx":23D2
      Left            =   1920
      List            =   "frmParametros.frx":23DC
      Style           =   2  'Dropdown List
      TabIndex        =   4
      Top             =   1650
      Width           =   735
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   5
      Top             =   810
      Width           =   4485
      _ExtentX        =   7911
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   6
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmParametros.frx":23EA
      PICN            =   "frmParametros.frx":2406
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdSalvar 
      Height          =   690
      Left            =   3810
      TabIndex        =   12
      TabStop         =   0   'False
      ToolTipText     =   "Salvar"
      Top             =   60
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmParametros.frx":30E0
      PICN            =   "frmParametros.frx":30FC
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Cidades Sem DDD para Dep�sito Atual:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   60
      TabIndex        =   14
      Top             =   2190
      Width           =   3375
   End
   Begin VB.Label Label8 
      AutoSize        =   -1  'True
      Caption         =   "Trabalhar Conectado"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   90
      TabIndex        =   11
      Top             =   1740
      Width           =   1800
   End
   Begin VB.Label Label7 
      AutoSize        =   -1  'True
      Caption         =   "Porta"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   3000
      TabIndex        =   10
      Top             =   1320
      Width           =   465
   End
   Begin VB.Label Label5 
      AutoSize        =   -1  'True
      Caption         =   "Porta"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   3000
      TabIndex        =   9
      Top             =   930
      Width           =   465
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "IP HP3000"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   90
      TabIndex        =   8
      Top             =   1320
      Width           =   930
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      Caption         =   "IP HP4000"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   90
      TabIndex        =   7
      Top             =   930
      Width           =   930
   End
End
Attribute VB_Name = "frmParametros"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdSalvar_Click()
    Gravar "IP4000", txtIP4000
    Gravar "PORTA4000", txtPorta4000
    Gravar "IP3000", txtIP3000
    Gravar "PORTA3000", txtPorta3000
    Gravar "LIGACAOAUTOMATICA", IIf(UCase(cboConectado) = "SIM", 1, 0)
End Sub

Private Sub cmdVoltar_Click()

    Pegar_Valores_Parametros

    Unload Me

End Sub

Private Sub Form_Load()
    lstCidadesSemDDD.Tag = "Carregando"
    Carregar_Parametros
    Carregar_Cidades
    Cidades_Selecionadas
    lstCidadesSemDDD.Tag = ""
End Sub

Sub Gravar(pNomeParam As String, pValorParam As String)
    Dim vOwner As String
    
    If lngCD = 1 Then
        vOwner = "HELPDESK."
    Else
        vOwner = strTabela_Banco
    End If

    vBanco.Parameters.Remove "Cod_Soft": vBanco.Parameters.Add "Cod_Soft", Pegar_Codigo_Sistema, 1
    vBanco.Parameters.Remove "Nome_Param": vBanco.Parameters.Add "Nome_Param", pNomeParam, 1
    vBanco.Parameters.Remove "VL_PARAM": vBanco.Parameters.Add "VL_PARAM", pValorParam, 1
    vBanco.Parameters.Remove "Dep": vBanco.Parameters.Add "DEP", vOwner, 1
    vBanco.Parameters.Remove "Txt_Erro": vBanco.Parameters.Add "Txt_Erro", "", 2

    vVB_Generica_001.ExecutaPl vBanco, "PRODUCAO.PCK_VDA230.Pr_Salvar_Parametros(:Nome_Param, :VL_PARAM, :Cod_Soft, :DEP, :Txt_Erro)"

    If Trim(vBanco.Parameters("Txt_Erro")) <> "" Then
        MsgBox "Descri��o do Erro:" & vBanco.Parameters("Txt_Erro")
        Exit Sub
    End If
End Sub

Sub Deletar(pNomeParam As String, pValorParam As String)
    Dim vOwner As String
    
    If lngCD = 1 Then
        vOwner = "HELPDESK."
    Else
        vOwner = strTabela_Banco
    End If

    vBanco.Parameters.Remove "Cod_Soft": vBanco.Parameters.Add "Cod_Soft", Pegar_Codigo_Sistema, 1
    vBanco.Parameters.Remove "Nome_Param": vBanco.Parameters.Add "Nome_Param", pNomeParam, 1
    vBanco.Parameters.Remove "VL_PARAM": vBanco.Parameters.Add "VL_PARAM", pValorParam, 1
    vBanco.Parameters.Remove "Dep": vBanco.Parameters.Add "DEP", vOwner, 1
    vBanco.Parameters.Remove "Txt_Erro": vBanco.Parameters.Add "Txt_Erro", "", 2

    vVB_Generica_001.ExecutaPl vBanco, "PRODUCAO.PCK_VDA230.Pr_Deletar_Parametros(:Nome_Param, :VL_PARAM, :Cod_Soft, :DEP, :Txt_Erro)"

    If Trim(vBanco.Parameters("Txt_Erro")) <> "" Then
        MsgBox "Descri��o do Erro:" & vBanco.Parameters("Txt_Erro")
        Exit Sub
    End If
End Sub


Sub Carregar_Parametros()

    txtIP4000 = IIf(IsNull(Pegar_VL_Parametro("IP4000")), "", Pegar_VL_Parametro("IP4000"))
    txtPorta4000 = IIf(IsNull(Pegar_VL_Parametro("PORTA4000")), "", Pegar_VL_Parametro("PORTA4000"))
    txtIP3000 = IIf(IsNull(Pegar_VL_Parametro("IP3000")), "", Pegar_VL_Parametro("IP3000"))
    txtPorta3000 = IIf(IsNull(Pegar_VL_Parametro("PORTA3000")), "", Pegar_VL_Parametro("PORTA3000"))
    cboConectado = IIf(Pegar_VL_Parametro("LIGACAOAUTOMATICA") = 1, "Sim", "N�o")

End Sub


Private Sub lstCidadesSemDDD_ItemCheck(Item As Integer)
    If lstCidadesSemDDD.Tag = "Carregando" Then Exit Sub
    If lstCidadesSemDDD.Selected(Item) = True Then
        Gravar lstCidadesSemDDD.List(Item), lstCidadesSemDDD.ItemData(Item)
    Else
        Deletar lstCidadesSemDDD.List(Item), lstCidadesSemDDD.ItemData(Item)
    End If
End Sub

Private Sub txtIP3000_Change()
    txtIP4000 = ""
    txtPorta4000 = ""
End Sub

Private Sub txtIP4000_Change()
    txtIP3000 = ""
    txtPorta3000 = ""
End Sub

Sub Carregar_Cidades()

    Dim vObjCidades As Object
    
    vBanco.Parameters.Remove "CodLojaCidade"
    vBanco.Parameters.Add "CodLojaCidade", lngCD, 1
    Criar_Cursor
    vErro = vVB_Generica_001.ExecutaPl(vBanco, "Producao.PCK_VDA230.PR_SELECT_CIDADES_LOJA(:vCursor, :CodLojaCidade)")

    If vErro <> "" Then
        MsgBox vErro
        Exit Sub
    Else
        Set vObjCidades = vBanco.Parameters("vCursor").Value
    End If

    For i = 1 To vObjCidades.RecordCount
        lstCidadesSemDDD.AddItem vObjCidades("Nome_cidade")
        lstCidadesSemDDD.ItemData(lstCidadesSemDDD.NewIndex) = vObjCidades("Cod_cidade")
        vObjCidades.MoveNext
    Next

End Sub

Sub Cidades_Selecionadas()

    Dim vOwner As String
    Dim vObjCidades As Object
    
    If lngCD = 1 Then
        vOwner = "HELPDESK."
    Else
        vOwner = strTabela_Banco
    End If

    vBanco.Parameters.Remove "Cod_Soft": vBanco.Parameters.Add "Cod_Soft", Pegar_Codigo_Sistema, 1
    vBanco.Parameters.Remove "Dep": vBanco.Parameters.Add "DEP", vOwner, 1
    Criar_Cursor

    vErro = vVB_Generica_001.ExecutaPl(vBanco, "PRODUCAO.PCK_VDA230.Pr_Select_Parametros_Cidades(:vCursor, :DEP, :Cod_Soft)")

    If Trim(vErro) <> "" Then
        MsgBox "Descri��o do Erro:" & vBanco.Parameters("Txt_Erro")
        Exit Sub
    Else
        Set vObjCidades = vBanco.Parameters("vCursor").Value
    End If
    
    For i = 0 To lstCidadesSemDDD.ListCount - 1
        
        For ii = 1 To vObjCidades.RecordCount
            If UCase(lstCidadesSemDDD.List(i)) = UCase(vObjCidades("Nome_parametro")) Then
               lstCidadesSemDDD.Selected(i) = True
               Exit For
            End If
            vObjCidades.MoveNext
        Next
        
    Next

End Sub
