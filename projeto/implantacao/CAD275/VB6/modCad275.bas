Attribute VB_Name = "modCad275"
Public RC_Geral As Object
Public vCodNat As String
Public vTipoOp As String
Public vCtaCred1 As String
Public vCtaCred2 As String
Public vCtaDeb As String

Public Sub Retorna_Cursor(Nome_Cursor As String, Nome_PCK As String, Optional Param1 As String, Optional Param2 As String, Optional Param3 As String, Optional Param4 As String, Optional Param5 As String)
    Dim vSql As String
'Cursor
    vBanco.Parameters.Remove Nome_Cursor
    vBanco.Parameters.Add Nome_Cursor, 0, 3
    vBanco.Parameters(Nome_Cursor).ServerType = 102
    vBanco.Parameters(Nome_Cursor).DynasetOption = &H2&
    vBanco.Parameters(Nome_Cursor).DynasetCacheParams 256, 16, 20, 2000, 0
    If Param1 <> "" Then vBanco.Parameters.Add "pParam1", Param1, 1
    If Param2 <> "" Then vBanco.Parameters.Add "pParam2", Param2, 1
    If Param3 <> "" Then vBanco.Parameters.Add "pParam3", Param3, 1
    If Param4 <> "" Then vBanco.Parameters.Add "pParam4", Param4, 1
    If Param5 <> "" Then vBanco.Parameters.Add "pParam5", Param5, 1
    
    vSql = _
        "Begin " & Nome_PCK & _
            "( :" & Nome_Cursor
        If Param1 <> "" Then vSql = vSql & ",:pParam1"
        If Param2 <> "" Then vSql = vSql & ",:pParam2"
        If Param3 <> "" Then vSql = vSql & ",:pParam3"
        If Param4 <> "" Then vSql = vSql & ",:pParam4"
        If Param5 <> "" Then vSql = vSql & ",:pParam5"
    vSql = vSql & "); END;"
    
    vBanco.ExecuteSQL vSql
    Set RC_Geral = vBanco.Parameters(Nome_Cursor).Value
    
    vBanco.Parameters.Remove Nome_Cursor
    If Param1 <> "" Then vBanco.Parameters.Remove "pParam1"
    If Param2 <> "" Then vBanco.Parameters.Remove "pParam2"
    If Param3 <> "" Then vBanco.Parameters.Remove "pParam3"
    If Param4 <> "" Then vBanco.Parameters.Remove "pParam4"
    If Param5 <> "" Then vBanco.Parameters.Remove "pParam5"
     
End Sub

