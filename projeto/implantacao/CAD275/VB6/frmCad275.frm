VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmCad275 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BackColor       =   &H80000004&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cadastro de contabiliza��o do cr�dito suspenso."
   ClientHeight    =   4920
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6990
   Icon            =   "frmCad275.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   328
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   466
   StartUpPosition =   2  'CenterScreen
   Begin Bot�o.cmd cmdSAIR 
      Height          =   690
      Left            =   120
      TabIndex        =   13
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   120
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCad275.frx":23D2
      PICN            =   "frmCad275.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   0
      TabIndex        =   33
      TabStop         =   0   'False
      Top             =   960
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin TabDlg.SSTab stbDIVIS�O 
      Height          =   3705
      Left            =   0
      TabIndex        =   14
      Top             =   1080
      Width           =   6855
      _ExtentX        =   12091
      _ExtentY        =   6535
      _Version        =   393216
      Tab             =   1
      TabHeight       =   617
      TabMaxWidth     =   2999
      ShowFocusRect   =   0   'False
      BackColor       =   -2147483644
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Consulta"
      TabPicture(0)   =   "frmCad275.frx":30C8
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "grdCO_GERAL"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "grdCO_NAT_OP"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "SSPanel1"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).ControlCount=   3
      TabCaption(1)   =   "Inclus�o"
      TabPicture(1)   =   "frmCad275.frx":30E4
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "lblIDESCRICAO"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "lblCOD_NATUREZA"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "lblTP_NATUREZA"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "lblCONTA_CREDITO1"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).Control(4)=   "lblCONTA_CREDITO2"
      Tab(1).Control(4).Enabled=   0   'False
      Tab(1).Control(5)=   "lblONTA_DEBITO"
      Tab(1).Control(5).Enabled=   0   'False
      Tab(1).Control(6)=   "cmdINSERT"
      Tab(1).Control(6).Enabled=   0   'False
      Tab(1).Control(7)=   "cmdIBUSCA"
      Tab(1).Control(7).Enabled=   0   'False
      Tab(1).Control(8)=   "txtICOD_NATUREZA"
      Tab(1).Control(8).Enabled=   0   'False
      Tab(1).Control(9)=   "txtICONTA_CREDITO1"
      Tab(1).Control(9).Enabled=   0   'False
      Tab(1).Control(10)=   "txtICONTA_CREDITO2"
      Tab(1).Control(10).Enabled=   0   'False
      Tab(1).Control(11)=   "txtICONTA_DEBITO"
      Tab(1).Control(11).Enabled=   0   'False
      Tab(1).Control(12)=   "cmbITIPO_OPERACAO"
      Tab(1).Control(12).Enabled=   0   'False
      Tab(1).ControlCount=   13
      TabCaption(2)   =   "Altera��o"
      TabPicture(2)   =   "frmCad275.frx":3100
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Label4"
      Tab(2).Control(1)=   "Label5"
      Tab(2).Control(2)=   "Label6"
      Tab(2).Control(3)=   "Label7"
      Tab(2).Control(4)=   "lblUDESCRICAO"
      Tab(2).Control(5)=   "Label3"
      Tab(2).Control(6)=   "cmdUPDATE"
      Tab(2).Control(7)=   "txtUCONTA_DEBITO"
      Tab(2).Control(8)=   "txtUCONTA_CREDITO2"
      Tab(2).Control(9)=   "txtUCONTA_CREDITO1"
      Tab(2).Control(10)=   "txtUCOD_NATUREZA"
      Tab(2).Control(11)=   "txtUTIPO_OPERACAO"
      Tab(2).ControlCount=   12
      Begin VB.TextBox txtUTIPO_OPERACAO 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   -72465
         Locked          =   -1  'True
         TabIndex        =   30
         Top             =   1320
         Width           =   2535
      End
      Begin VB.ComboBox cmbITIPO_OPERACAO 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         ItemData        =   "frmCad275.frx":311C
         Left            =   2535
         List            =   "frmCad275.frx":3126
         TabIndex        =   1
         Top             =   1305
         Width           =   2655
      End
      Begin VB.TextBox txtUCOD_NATUREZA 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   -72465
         Locked          =   -1  'True
         MaxLength       =   3
         TabIndex        =   25
         Top             =   840
         Width           =   615
      End
      Begin VB.TextBox txtUCONTA_CREDITO1 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   -72465
         MaxLength       =   12
         TabIndex        =   7
         Top             =   1800
         Width           =   1455
      End
      Begin VB.TextBox txtUCONTA_CREDITO2 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   -72465
         MaxLength       =   12
         TabIndex        =   8
         Top             =   2280
         Width           =   1455
      End
      Begin VB.TextBox txtUCONTA_DEBITO 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   -72465
         MaxLength       =   12
         TabIndex        =   9
         Top             =   2760
         Width           =   1455
      End
      Begin VB.TextBox txtICONTA_DEBITO 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2535
         MaxLength       =   12
         TabIndex        =   4
         Top             =   2760
         Width           =   1455
      End
      Begin VB.TextBox txtICONTA_CREDITO2 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2535
         MaxLength       =   12
         TabIndex        =   3
         Top             =   2280
         Width           =   1455
      End
      Begin VB.TextBox txtICONTA_CREDITO1 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2535
         MaxLength       =   12
         TabIndex        =   2
         Top             =   1800
         Width           =   1455
      End
      Begin VB.TextBox txtICOD_NATUREZA 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   2535
         MaxLength       =   3
         TabIndex        =   0
         Top             =   840
         Width           =   615
      End
      Begin Threed.SSPanel SSPanel1 
         Height          =   840
         Left            =   -70350
         TabIndex        =   20
         Top             =   600
         Width           =   2055
         _Version        =   65536
         _ExtentX        =   3625
         _ExtentY        =   1482
         _StockProps     =   15
         BackColor       =   -2147483638
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BevelOuter      =   0
         Begin VB.OptionButton optCO_NAT_OP 
            Appearance      =   0  'Flat
            BackColor       =   &H8000000B&
            Caption         =   "Natureza de opera��o"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   0
            TabIndex        =   11
            Top             =   0
            Value           =   -1  'True
            Width           =   2130
         End
         Begin VB.OptionButton optCO_GERAL 
            Appearance      =   0  'Flat
            BackColor       =   &H8000000B&
            Caption         =   "Geral"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   0
            TabIndex        =   12
            Top             =   360
            Width           =   810
         End
      End
      Begin Bot�o.cmd cmdIBUSCA 
         Height          =   300
         Left            =   3180
         TabIndex        =   6
         TabStop         =   0   'False
         ToolTipText     =   "Busca"
         Top             =   855
         Width           =   300
         _ExtentX        =   529
         _ExtentY        =   529
         BTYPE           =   3
         TX              =   "..."
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCad275.frx":3156
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdUPDATE 
         Height          =   660
         Left            =   -69120
         TabIndex        =   10
         TabStop         =   0   'False
         Tag             =   "0"
         ToolTipText     =   "Alterar"
         Top             =   2760
         Width           =   675
         _ExtentX        =   1191
         _ExtentY        =   1164
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCad275.frx":3172
         PICN            =   "frmCad275.frx":318E
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdINSERT 
         Height          =   660
         Left            =   5880
         TabIndex        =   5
         TabStop         =   0   'False
         Tag             =   "1"
         ToolTipText     =   "Incluir"
         Top             =   2760
         Width           =   675
         _ExtentX        =   1191
         _ExtentY        =   1164
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCad275.frx":3E68
         PICN            =   "frmCad275.frx":3E84
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MSFlexGridLib.MSFlexGrid grdCO_NAT_OP 
         Height          =   1425
         Left            =   -74880
         TabIndex        =   35
         Top             =   480
         Width           =   4425
         _ExtentX        =   7805
         _ExtentY        =   2514
         _Version        =   393216
         BackColorBkg    =   -2147483633
         HighLight       =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSFlexGridLib.MSFlexGrid grdCO_GERAL 
         Height          =   1425
         Left            =   -74880
         TabIndex        =   36
         Top             =   2040
         Width           =   6585
         _ExtentX        =   11615
         _ExtentY        =   2514
         _Version        =   393216
         BackColorBkg    =   -2147483633
         HighLight       =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label3 
         BackColor       =   &H8000000B&
         Caption         =   "Conta cr�dito_ 2"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   -74730
         TabIndex        =   34
         Top             =   2280
         Width           =   1935
      End
      Begin VB.Label lblUDESCRICAO 
         Height          =   255
         Left            =   -71775
         TabIndex        =   32
         Top             =   885
         Width           =   2895
      End
      Begin VB.Label Label7 
         BackColor       =   &H8000000B&
         Caption         =   "Conta debito"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   -74730
         TabIndex        =   29
         Top             =   2760
         Width           =   1935
      End
      Begin VB.Label Label6 
         BackColor       =   &H8000000B&
         Caption         =   "C�digo da Nat. Opera��o"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   -74730
         TabIndex        =   28
         Top             =   840
         Width           =   2295
      End
      Begin VB.Label Label5 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000B&
         Caption         =   "Tipo da opera��o"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   -74730
         TabIndex        =   27
         Top             =   1350
         Width           =   1920
      End
      Begin VB.Label Label4 
         BackColor       =   &H8000000B&
         Caption         =   "Conta cr�dito_1"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   -74730
         TabIndex        =   26
         Top             =   1800
         Width           =   1935
      End
      Begin VB.Label lblONTA_DEBITO 
         BackColor       =   &H8000000B&
         Caption         =   "Conta debito"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   270
         TabIndex        =   19
         Top             =   2760
         Width           =   1935
      End
      Begin VB.Label lblCONTA_CREDITO2 
         BackColor       =   &H8000000B&
         Caption         =   "Conta cr�dito_ 2"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   270
         TabIndex        =   18
         Top             =   2280
         Width           =   1935
      End
      Begin VB.Label lblCONTA_CREDITO1 
         BackColor       =   &H8000000B&
         Caption         =   "Conta cr�dito_1"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   270
         TabIndex        =   17
         Top             =   1800
         Width           =   1935
      End
      Begin VB.Label lblTP_NATUREZA 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000B&
         Caption         =   "Tipo da opera��o"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   270
         TabIndex        =   16
         Top             =   1350
         Width           =   1920
      End
      Begin VB.Label lblCOD_NATUREZA 
         BackColor       =   &H8000000B&
         Caption         =   "C�digo da Nat. Opera��o"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   270
         TabIndex        =   15
         Top             =   840
         Width           =   2295
      End
      Begin VB.Label lblIDESCRICAO 
         Height          =   255
         Left            =   3570
         TabIndex        =   31
         Top             =   870
         Width           =   2895
      End
   End
   Begin Threed.SSPanel splBUSCA 
      Height          =   1695
      Left            =   2160
      TabIndex        =   21
      Top             =   1680
      Visible         =   0   'False
      Width           =   3975
      _Version        =   65536
      _ExtentX        =   7011
      _ExtentY        =   2990
      _StockProps     =   15
      BackColor       =   13160660
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin VB.ComboBox cboB_NATUREZA 
         Height          =   1155
         ItemData        =   "frmCad275.frx":4B5E
         Left            =   120
         List            =   "frmCad275.frx":4B60
         Style           =   1  'Simple Combo
         TabIndex        =   22
         Top             =   360
         Width           =   3735
      End
      Begin Bot�o.cmd cmdF_BUSCA 
         Height          =   195
         Left            =   3750
         TabIndex        =   24
         TabStop         =   0   'False
         ToolTipText     =   "Sair"
         Top             =   30
         Width           =   180
         _ExtentX        =   318
         _ExtentY        =   344
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   -2147483646
         BCOLO           =   -2147483646
         FCOL            =   -2147483646
         FCOLO           =   -2147483646
         MCOL            =   -2147483646
         MPTR            =   1
         MICON           =   "frmCad275.frx":4B62
         PICN            =   "frmCad275.frx":4B7E
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label Label1 
         BackColor       =   &H8000000D&
         Height          =   255
         Left            =   0
         TabIndex        =   23
         Top             =   0
         Width           =   3975
      End
   End
End
Attribute VB_Name = "frmCad275"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim DESCRICAO As String
Dim vCOD_NATUREZA As String
Dim COD(), DESC() As String
Dim X As Integer
Private Sub LOC_NATUREZA(CODIGO As String)
    Dim RCODIGO
    RCODIGO = CODIGO
    DESCRICAO = ""
    For X = 0 To UBound(COD())
        If COD(X) = RCODIGO Then
            DESCRICAO = DESC(X)
            Exit For
        End If
    Next X
End Sub
Private Sub INSERT()
    Dim vTP_OPERACAO As String * 2
    vBanco.Parameters.Remove "pCOD_NATUREZA"
    vBanco.Parameters.Remove "pTP_NATUREZA"
    vBanco.Parameters.Remove "pCONTA_CREDITO_1"
    vBanco.Parameters.Remove "pCONTA_CREDITO_2"
    vBanco.Parameters.Remove "pCONTA_DEBITO"
    vBanco.Parameters.Remove "pTXTERRO"
    
    If cmbITIPO_OPERACAO.Text = "DE - Dentro do estado" Then vTP_OPERACAO = "DE"
    If cmbITIPO_OPERACAO.Text = "FE - Fora do estado" Then vTP_OPERACAO = "FE"
    
    vBanco.Parameters.Add "pCOD_NATUREZA", txtICOD_NATUREZA.Text, 1
    vBanco.Parameters.Add "pTP_NATUREZA", vTP_OPERACAO, 1
    vBanco.Parameters.Add "pCONTA_CREDITO_1", txtICONTA_CREDITO1.Text, 1
    If Trim(txtICONTA_CREDITO2.Text) = "" Then vBanco.Parameters.Add "pCONTA_CREDITO_2", Null, 1
    If Trim(txtICONTA_CREDITO2.Text) <> "" Then vBanco.Parameters.Add "pCONTA_CREDITO_2", txtICONTA_CREDITO2.Text, 1
    vBanco.Parameters.Add "pCONTA_DEBITO", txtICONTA_DEBITO.Text, 1
    vBanco.Parameters.Add "pTXTERRO", "", 2
End Sub
Private Sub Update()
    Dim vTP_OPERACAO As String * 2

    Call vVB_Generica_001.ExcluiBind(vBanco)
    vTP_OPERACAO = txtUTIPO_OPERACAO.Text
    vBanco.Parameters.Add "pCOD_NATUREZA", txtUCOD_NATUREZA.Text, 1
    vBanco.Parameters.Add "pTP_NATUREZA", vTP_OPERACAO, 1
    vBanco.Parameters.Add "pCONTA_CREDITO_1", txtUCONTA_CREDITO1.Text, 1
    If Trim(txtUCONTA_CREDITO2.Text) = "" Then vBanco.Parameters.Add "pCONTA_CREDITO_2", Null, 1
    If Trim(txtUCONTA_CREDITO2.Text) <> "" Then vBanco.Parameters.Add "pCONTA_CREDITO_2", txtUCONTA_CREDITO2.Text, 1
    vBanco.Parameters.Add "pCONTA_DEBITO", txtUCONTA_DEBITO.Text, 1
    vBanco.Parameters.Add "pTXTERRO", "", 2
End Sub

Private Sub VerificaConta(texto As TextBox)

    If Trim(texto) = "" Then Exit Sub
    Call vVB_Generica_001.ExcluiBind(vBanco)
    vBanco.Parameters.Add "cCta", 0, 3
    vBanco.Parameters("cCta").ServerType = 102
    vBanco.Parameters("cCta").DynasetOption = &H2&
    vBanco.Parameters("cCta").DynasetCacheParams 256, 16, 20, 2000, 0
    vBanco.Parameters.Add "vCta", texto, 1
    vSql = "PRODUCAO.PCK_CAD275.PR_SELECT_DESC_CONTA(:cCta,:vCta)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    If vErro <> "" Then
        Call vVB_Generica_001.ProcessaErro(vErro)
        Exit Sub
    End If
    Set vObjOracle = vBanco.Parameters("cCta").Value
    If vObjOracle.RecordCount = 0 Then
        Call vVB_Generica_001.Informar("Conta n�o cadastrada.")
        texto = ""
        texto.SetFocus
        Exit Sub
    End If
    
End Sub

Private Sub cboB_NATUREZA_Click()
    txtICOD_NATUREZA.Text = COD(cboB_NATUREZA.ListIndex)
    lblIDESCRICAO.Caption = DESC(cboB_NATUREZA.ListIndex)
    splBUSCA.Visible = False
    txtICOD_NATUREZA.SetFocus
End Sub

Private Sub cboB_NATUREZA_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)

End Sub

Private Sub cmdF_BUSCA_Click()
    cboB_NATUREZA.Text = ""
    splBUSCA.Visible = False
End Sub
Private Sub cmdIBUSCA_Click()
    splBUSCA.Visible = True
    cboB_NATUREZA.SetFocus
End Sub
Private Sub cmdINSERT_Click()
    If Trim(txtICOD_NATUREZA.Text) = "" Then
        Call vVB_Generica_001.Informar("Selecione o cod. natureza!")
        txtICOD_NATUREZA.SetFocus
        Exit Sub
    ElseIf cmbITIPO_OPERACAO.Text <> "DE - Dentro do estado" Then
        If cmbITIPO_OPERACAO.Text <> "FE - Fora do estado" Then
            Call vVB_Generica_001.Informar("Tipo da opera��o inv�lido!")
            Exit Sub
        End If
    ElseIf Trim(txtICONTA_CREDITO1.Text) = "" Then
        Call vVB_Generica_001.Informar("Digite a conta cr�dito!")
        Exit Sub
    ElseIf Trim(txtICONTA_DEBITO.Text) = "" Then
        Call vVB_Generica_001.Informar("Digite a conta d�bito!")
        Exit Sub
    End If
    
    Call INSERT
    vSessao.BeginTrans
    vBanco.ExecuteSQL "BEGIN PRODUCAO.PCK_CAD275.PR_INCLUIR(:pCOD_NATUREZA, :pTP_NATUREZA, " & _
                        ":pCONTA_CREDITO_1, :pCONTA_CREDITO_2, :pCONTA_DEBITO, :pTXTERRO); END;"
    If vBanco.Parameters("pTXTERRO") <> "" Then
        Call vVB_Generica_001.Informar(vBanco.Parameters("pTXTERRO"))
        vSessao.Rollback
        Exit Sub
    End If
    If vVB_Generica_001.Perguntar("Incluir registro?") = 7 Then
        vSessao.Rollback
    Else
        vSessao.CommitTrans
        Call vVB_Generica_001.Informar("Inclus�o realizada com sucesso.")
        txtICOD_NATUREZA.Text = ""
        cmbITIPO_OPERACAO.Text = ""
        txtICONTA_CREDITO1.Text = ""
        txtICONTA_CREDITO2.Text = ""
        txtICONTA_DEBITO.Text = ""
        lblIDESCRICAO.Caption = ""
        txtICOD_NATUREZA.SetFocus
    End If
End Sub
Private Sub cmdSair_Click()
    Unload Me
End Sub
Private Sub cmdUPDATE_Click()
    If Trim(txtUCOD_NATUREZA.Text) = "" Then
        Call vVB_Generica_001.Informar("Selecione o c�d. natureza.")
        Exit Sub
    ElseIf Trim(txtUCONTA_CREDITO1.Text) = "" Then
        Call vVB_Generica_001.Informar("Selecione a conta cr�dito.")
        Exit Sub
    ElseIf Trim(txtUCONTA_DEBITO.Text) = "" Then
        Call vVB_Generica_001.Informar("Selecione a conta d�bito.")
        Exit Sub
    ElseIf vCodNat = (txtUCOD_NATUREZA) And vTipoOp = txtUTIPO_OPERACAO And _
    vCtaCred1 = txtUCONTA_CREDITO1 And vCtaCred2 = txtUCONTA_CREDITO2 And vCtaDeb = txtUCONTA_DEBITO Then
        Call vVB_Generica_001.Informar("Nenhum campo foi alterado.")
        Exit Sub
    End If
    Call Update
    vSessao.BeginTrans
    vBanco.ExecuteSQL "BEGIN PRODUCAO.PCK_CAD275.PR_ATUALIZAR(:pCOD_NATUREZA, :pTP_NATUREZA, " & _
                        ":pCONTA_CREDITO_1, :pCONTA_CREDITO_2, :pCONTA_DEBITO, :pTXTERRO); END;"
    If vBanco.Parameters("pTXTERRO") <> "" Then
        Call vVB_Generica_001.Informar(vBanco.Parameters("pTXTERRO"))
        vSessao.Rollback
        Exit Sub
    End If
    If vVB_Generica_001.Perguntar("Atualizar registro?") = 7 Then
        vSessao.Rollback
    Else
        vSessao.CommitTrans
        Call vVB_Generica_001.Informar("Altera��o realizada com sucesso.")
        txtUCOD_NATUREZA = ""
        lblUDESCRICAO = ""
        txtUTIPO_OPERACAO = ""
        txtUCONTA_CREDITO1 = ""
        txtUCONTA_CREDITO2 = ""
        txtUCONTA_DEBITO = ""
        stbDIVIS�O.Tab = 0
    End If
    
End Sub
Private Sub Form_Load()
    Me.Top = 50
    Me.Left = 50
    Me.Width = 7125
    Me.Height = 5325
    
    vCOD_NATUREZA = "NULL"
    DESCRICAO = ""
    Call optCO_NAT_OP_Click
    
    Set vObjOracle = vVB_Generica_001.TabelaNaturezaOperacao(vBanco, , , , "S")
    Call vVB_Generica_001.ExcluiBind(vBanco)
    
    stbDIVIS�O.Tab = 0
    cboB_NATUREZA.Clear
    If vObjOracle.RecordCount = 0 Then Exit Sub
    ReDim COD(vObjOracle.RecordCount), DESC(vObjOracle.RecordCount)
    Call vVB_Generica_001.PreencheComboList(vObjOracle, cboB_NATUREZA, "C�D. NATUREZA", "DESC_ABREVIADA")
    vObjOracle.MoveFirst
    For X = 0 To vObjOracle.RecordCount - 1
        COD(X) = vObjOracle.Fields("C�D. NATUREZA")
        DESC(X) = vObjOracle!DESC_ABREVIADA
        vObjOracle.MoveNext
    Next X
    
End Sub
Private Sub grdCO_GERAL_DblClick()
    
    With grdCO_GERAL
        .Col = 1
        If .Text = "" Then Exit Sub
        stbDIVIS�O.Tab = 2
        txtUCOD_NATUREZA.Text = .Text
        vCodNat = .Text
        .Col = 2
        If .Text = "DE" Then
            txtUTIPO_OPERACAO.Text = "DE - Dentro do estado"
        Else
            txtUTIPO_OPERACAO.Text = "FE - Fora do estado"
        End If
        vTipoOp = txtUTIPO_OPERACAO
        .Col = 3
        txtUCONTA_CREDITO1.Text = .Text
        vCtaCred1 = .Text
        .Col = 4
        txtUCONTA_CREDITO2.Text = .Text
        vCtaCred2 = .Text
        .Col = 5
        txtUCONTA_DEBITO.Text = .Text
        vCtaDeb = .Text
    End With
    txtUCONTA_CREDITO1.SetFocus
End Sub
Private Sub grdCO_NAT_OP_DblClick()
    vCOD_NATUREZA = Left(grdCO_NAT_OP.Text, 3)
    grdCO_NAT_OP.Col = 1
    If Trim(grdCO_NAT_OP.Text) = "" Then Exit Sub
    optCO_GERAL_Click
End Sub

Private Sub optCO_GERAL_Click()
    If vCOD_NATUREZA = "NULL" Then
        Call vVB_Generica_001.LimpaFlexGridComTitulo(grdCO_NAT_OP)
    End If
   
    vBanco.Parameters.Add "cCRED_SUSP", 0, 3
    vBanco.Parameters("cCRED_SUSP").ServerType = 102
    vBanco.Parameters("cCRED_SUSP").DynasetOption = &H2&
    vBanco.Parameters("cCRED_SUSP").DynasetCacheParams 256, 16, 20, 2000, 0
    vBanco.Parameters.Add "pCOD_NAT", vCOD_NATUREZA, 1
    
    vCOD_NATUREZA = "NULL"
    vSql = "PRODUCAO.PCK_CAD275.PR_SELECT_CRED_SUSP(:cCRED_SUSP, :pCOD_NAT)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro)
    Else
        Set vObjOracle = vBanco.Parameters("cCRED_SUSP").Value
    End If
 
    Call vVB_Generica_001.ExcluiBind(vBanco)
        
    If vObjOracle.RecordCount <> 0 Then
        Call vVB_Generica_001.CarregaGridTabela(grdCO_GERAL, vObjOracle, 6)
    Else
        Call vVB_Generica_001.Informar("N�o existem ocorr�ncias cadastradas para este banco")
    End If
    grdCO_GERAL.Col = 1
    grdCO_GERAL.Sort = 1
    
End Sub
Private Sub optCO_NAT_OP_Click()

    Call vVB_Generica_001.LimpaFlexGridComTitulo(grdCO_GERAL)
    
    Set vObjOracle = vVB_Generica_001.TabelaNaturezaOperacao(vBanco, , , , "S")
    
'    vBanco.Parameters.Add "cNATUREZA", 0, 3
'    vBanco.Parameters("cNATUREZA").ServerType = 102
'    vBanco.Parameters("cNATUREZA").DynasetOption = &H2&
'    vBanco.Parameters("cNATUREZA").DynasetCacheParams 256, 16, 20, 2000, 0
'    vSql = "PRODUCAO.PCK_CAD275.PR_SELECT_NATUREZA(:cNATUREZA)"
'    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
'    If vErro <> "" Then
'        vVB_Generica_001.ProcessaErro (vErro)
'    Else
'        Set vObjOracle = vBanco.Parameters("cNATUREZA").Value
'    End If

    Call vVB_Generica_001.ExcluiBind(vBanco)
        
    If vObjOracle.RecordCount <> 0 Then
        Call vVB_Generica_001.CarregaGridTabela(grdCO_NAT_OP, vObjOracle, 3)
    Else
        Call vVB_Generica_001.Informar("N�o existem ocorr�ncias cadastradas para este banco")
    End If
End Sub

Private Sub stbDIVIS�O_Click(PreviousTab As Integer)
    splBUSCA.Visible = False
    txtICOD_NATUREZA.Text = ""
    cmbITIPO_OPERACAO.Text = ""
    txtICONTA_CREDITO1.Text = ""
    txtICONTA_CREDITO2.Text = ""
    txtICONTA_DEBITO.Text = ""
    lblUDESCRICAO.Caption = ""
    
    txtUCOD_NATUREZA.Text = ""
    txtUTIPO_OPERACAO.Text = ""
    txtUCONTA_CREDITO1.Text = ""
    txtUCONTA_CREDITO2.Text = ""
    txtUCONTA_DEBITO.Text = ""
    lblIDESCRICAO.Caption = ""
    
    If stbDIVIS�O.Tab = 0 Then
        Call vVB_Generica_001.LimpaFlexGridComTitulo(grdCO_GERAL)
        optCO_NAT_OP.Value = True
    End If
    If stbDIVIS�O.Tab = 1 Then txtICOD_NATUREZA.SetFocus
    If stbDIVIS�O.Tab = 2 Then txtUCONTA_CREDITO1.SetFocus
    
End Sub
Private Sub txtICOD_NATUREZA_LostFocus()
    If Trim(txtICOD_NATUREZA.Text) = "" Then
        lblIDESCRICAO.Caption = ""
        Exit Sub
    End If
    lblIDESCRICAO.Caption = ""
    LOC_NATUREZA txtICOD_NATUREZA.Text
    If DESCRICAO = "" Then
        Call vVB_Generica_001.Informar("C�digo n�o encontrado.")
        txtICOD_NATUREZA.SetFocus
    Else
        lblIDESCRICAO.Caption = DESCRICAO
    End If
    DESCRICAO = ""
    
End Sub

Private Sub txtICONTA_CREDITO1_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Numero(KeyAscii)

End Sub

Private Sub txtICONTA_CREDITO1_LostFocus()

    Call VerificaConta(txtICONTA_CREDITO1)

End Sub

Private Sub txtICONTA_CREDITO2_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Numero(KeyAscii)
    
End Sub

Private Sub txtICONTA_CREDITO2_LostFocus()

    Call VerificaConta(txtICONTA_CREDITO2)
    
End Sub

Private Sub txtICONTA_DEBITO_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Numero(KeyAscii)
    
End Sub

Private Sub txtICONTA_DEBITO_LostFocus()

    Call VerificaConta(txtICONTA_DEBITO)

End Sub

Private Sub txtUCOD_NATUREZA_Change()

    If Trim(txtUCOD_NATUREZA.Text) = "" Then Exit Sub
    lblUDESCRICAO.Caption = ""
    LOC_NATUREZA txtUCOD_NATUREZA.Text
    If DESCRICAO = "" Then
        Call vVB_Generica_001.Informar("C�digo n�o encontrado.")
        txtUCOD_NATUREZA.SetFocus
    Else
        lblUDESCRICAO.Caption = DESCRICAO
    End If
    DESCRICAO = ""
    
End Sub

Private Sub txtUCONTA_CREDITO1_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Numero(KeyAscii)
    
End Sub

Private Sub txtUCONTA_CREDITO1_LostFocus()

    Call VerificaConta(txtUCONTA_CREDITO1)
    
End Sub

Private Sub txtUCONTA_CREDITO2_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Numero(KeyAscii)
    
End Sub

Private Sub txtUCONTA_CREDITO2_LostFocus()

    Call VerificaConta(txtUCONTA_CREDITO2)

End Sub

Private Sub txtUCONTA_DEBITO_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Numero(KeyAscii)
    
End Sub

Private Sub txtUCONTA_DEBITO_LostFocus()

    Call VerificaConta(txtUCONTA_DEBITO)

End Sub
