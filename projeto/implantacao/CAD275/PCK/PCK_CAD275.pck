create or replace package PCK_CAD275 is

  -- Author  : CONSULTOR17
  -- Created : 29/09/04 08:47:08
  -- Purpose : Cadastro de Cr�dito Suspenso
  
  Type TP_CURSOR IS REF CURSOR;
  
  PROCEDURE PR_SELECT_NATUREZA (cNATUREZA IN OUT TP_CURSOR);
  
  PROCEDURE PR_SELECT_CRED_SUSP (cCRED_SUSP IN OUT TP_CURSOR, 
                                 pCOD_NATUREZA IN VARCHAR2);    

  PROCEDURE PR_ATUALIZAR (pCOD_NATUREZA IN FISCAL.CRED_SUSP_CONTABILIZACAO.COD_NATUREZA%TYpe,
                          pTP_OPERACAO IN FISCAL.CRED_SUSP_CONTABILIZACAO.TP_OPERACAO%TYpe,
                          pCONTA_CREDITO_1 IN FISCAL.CRED_SUSP_CONTABILIZACAO.CONTA_CREDITO_1%TYpe,
                          pCONTA_CREDITO_2 IN FISCAL.CRED_SUSP_CONTABILIZACAO.CONTA_CREDITO_2%TYpe,
                          pCONTA_DEBITO IN FISCAL.CRED_SUSP_CONTABILIZACAO.CONTA_DEBITO%TYpe,
                          pTXTERRO         OUT Varchar2); 
       
  PROCEDURE PR_INCLUIR (pCOD_NATUREZA    IN FISCAL.CRED_SUSP_CONTABILIZACAO.COD_NATUREZA%TYpe,
                        pTP_OPERACAO     IN FISCAL.CRED_SUSP_CONTABILIZACAO.TP_OPERACAO%TYpe,
                        pCONTA_CREDITO_1 IN FISCAL.CRED_SUSP_CONTABILIZACAO.CONTA_CREDITO_1%TYpe,
                        pCONTA_CREDITO_2 IN FISCAL.CRED_SUSP_CONTABILIZACAO.CONTA_CREDITO_2%TYpe,
                        pCONTA_DEBITO    IN FISCAL.CRED_SUSP_CONTABILIZACAO.CONTA_DEBITO%TYpe,
                        pTXTERRO         OUT Varchar2);
                                                    
end PCK_CAD275;
/
create or replace package body PCK_CAD275 is

  PROCEDURE PR_SELECT_NATUREZA (cNATUREZA IN OUT TP_CURSOR ) IS
  BEGIN
      OPEN cNATUREZA FOR
      SELECT COD_NATUREZA, DESC_ABREVIADA  
      FROM PRODUCAO.NATUREZA_OPERACAO
      WHERE FL_ESTOQUE = 'S'
      ORDER BY DESC_ABREVIADA;
  END PR_SELECT_NATUREZA;   
-------------------------------------------------------------------
  PROCEDURE PR_SELECT_CRED_SUSP (cCRED_SUSP IN OUT TP_CURSOR, 
                                 pCOD_NATUREZA IN VARCHAR2) IS    
  BEGIN
      IF pCOD_NATUREZA = 'NULL' THEN 
         OPEN cCRED_SUSP FOR
         SELECT FISCAL.CRED_SUSP_CONTABILIZACAO.COD_NATUREZA, 
                FISCAL.CRED_SUSP_CONTABILIZACAO.TP_OPERACAO,
                FISCAL.CRED_SUSP_CONTABILIZACAO.CONTA_CREDITO_1,
                FISCAL.CRED_SUSP_CONTABILIZACAO.CONTA_CREDITO_2,
                FISCAL.CRED_SUSP_CONTABILIZACAO.CONTA_DEBITO
         FROM PRODUCAO.NATUREZA_OPERACAO, FISCAL.CRED_SUSP_CONTABILIZACAO
         WHERE PRODUCAO.NATUREZA_OPERACAO.FL_ESTOQUE ='S' AND FISCAL.CRED_SUSP_CONTABILIZACAO.COD_NATUREZA = PRODUCAO.NATUREZA_OPERACAO.COD_NATUREZA;
      ELSE
         OPEN cCRED_SUSP FOR
         SELECT *  
         FROM FISCAL.CRED_SUSP_CONTABILIZACAO
         WHERE COD_NATUREZA = pCOD_NATUREZA;
      END IF;     
  END PR_SELECT_CRED_SUSP;  
-------------------------------------------------------------------
  PROCEDURE PR_ATUALIZAR (pCOD_NATUREZA    IN FISCAL.CRED_SUSP_CONTABILIZACAO.COD_NATUREZA%TYpe,
                          pTP_OPERACAO     IN FISCAL.CRED_SUSP_CONTABILIZACAO.TP_OPERACAO%TYpe,
                          pCONTA_CREDITO_1 IN FISCAL.CRED_SUSP_CONTABILIZACAO.CONTA_CREDITO_1%TYpe,
                          pCONTA_CREDITO_2 IN FISCAL.CRED_SUSP_CONTABILIZACAO.CONTA_CREDITO_2%TYpe,
                          pCONTA_DEBITO    IN FISCAL.CRED_SUSP_CONTABILIZACAO.CONTA_DEBITO%TYpe,
                          pTxtErro         OUT Varchar2) IS
  vFLAG INT(2);
  BEGIN
       vFLAG := 0;
       pTXTERRO := '';
       SELECT COUNT(NUM_CONTA) INTO vFLAG FROM CONTATAB.PLANO_CONTAS
       WHERE NUM_CONTA = pCONTA_CREDITO_1 OR NUM_CONTA = pCONTA_CREDITO_2 OR NUM_CONTA = pCONTA_DEBITO;
       IF vFLAG = 3 OR vFLAG = 2 AND pCONTA_CREDITO_2 IS NULL THEN
          UPDATE FISCAL.CRED_SUSP_CONTABILIZACAO
          SET CONTA_CREDITO_1 = pCONTA_CREDITO_1, 
              CONTA_CREDITO_2 = pCONTA_CREDITO_2,
              CONTA_DEBITO = pCONTA_DEBITO
          WHERE COD_NATUREZA = pCOD_NATUREZA AND TP_OPERACAO = pTP_OPERACAO;   
       ELSE
           pTXTERRO := 'Conta cont�bil n�o cadastrada.';       
       END IF;   
  END PR_ATUALIZAR;
-------------------------------------------------------------------  
  PROCEDURE PR_INCLUIR (pCOD_NATUREZA    IN FISCAL.CRED_SUSP_CONTABILIZACAO.COD_NATUREZA%TYpe,
                        pTP_OPERACAO     IN FISCAL.CRED_SUSP_CONTABILIZACAO.TP_OPERACAO%TYpe,
                        pCONTA_CREDITO_1 IN FISCAL.CRED_SUSP_CONTABILIZACAO.CONTA_CREDITO_1%TYpe,
                        pCONTA_CREDITO_2 IN FISCAL.CRED_SUSP_CONTABILIZACAO.CONTA_CREDITO_2%TYpe,
                        pCONTA_DEBITO    IN FISCAL.CRED_SUSP_CONTABILIZACAO.CONTA_DEBITO%TYpe,
                        pTXTERRO         OUT Varchar2) IS
  vFLAG INT(2);
  BEGIN
       vFLAG := 0;
       pTXTERRO := '';
       SELECT COUNT(COD_NATUREZA) INTO vFLAG FROM FISCAL.CRED_SUSP_CONTABILIZACAO
       WHERE COD_NATUREZA = pCOD_NATUREZA AND TP_OPERACAO = pTP_OPERACAO;
       IF vFLAG = 1 THEN
          pTXTERRO := 'Contabilizacao j� cadastrada.';   
       ELSE
           SELECT COUNT(NUM_CONTA) INTO vFLAG FROM CONTATAB.PLANO_CONTAS
           WHERE NUM_CONTA = pCONTA_CREDITO_1 OR NUM_CONTA = pCONTA_CREDITO_2 OR NUM_CONTA = pCONTA_DEBITO;
           IF vFLAG = 3 OR vFLAG = 2 AND pCONTA_CREDITO_2 IS NULL THEN
              INSERT INTO FISCAL.CRED_SUSP_CONTABILIZACAO(COD_NATUREZA, 
                                                          TP_OPERACAO,
                                                          CONTA_CREDITO_1,
                                                          CONTA_CREDITO_2,
                                                          CONTA_DEBITO)
              VALUES(pCOD_NATUREZA, 
                     pTP_OPERACAO,
                     pCONTA_CREDITO_1,
                     pCONTA_CREDITO_2,
                     pCONTA_DEBITO);                                                      
           ELSE
               pTXTERRO := 'Conta cont�bil n�o cadastrada.';       
           END IF;
       END IF;  
  END PR_INCLUIR;  
-------------------------------------------------------------------   

end PCK_CAD275;
/
