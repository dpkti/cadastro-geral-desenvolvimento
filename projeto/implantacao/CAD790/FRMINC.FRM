VERSION 4.00
Begin VB.Form frmInclui 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "INCLUS�O"
   ClientHeight    =   3150
   ClientLeft      =   1230
   ClientTop       =   2025
   ClientWidth     =   6690
   Height          =   3555
   Left            =   1170
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3150
   ScaleWidth      =   6690
   Top             =   1680
   Width           =   6810
   Begin VB.CommandButton Command3 
      Caption         =   "SAIR"
      Height          =   615
      Left            =   4680
      TabIndex        =   5
      Top             =   1680
      Width           =   1815
   End
   Begin VB.CommandButton Command2 
      Caption         =   "EXCLUIR"
      Height          =   615
      Left            =   2520
      TabIndex        =   4
      Top             =   1680
      Width           =   1815
   End
   Begin VB.CommandButton Command1 
      Caption         =   "INCLUIR"
      Height          =   615
      Left            =   360
      TabIndex        =   3
      Top             =   1680
      Width           =   1815
   End
   Begin VB.TextBox txtCod_Cliente 
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   1440
      MaxLength       =   6
      TabIndex        =   1
      Top             =   360
      Width           =   975
   End
   Begin VB.Label lblUF 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   5160
      TabIndex        =   9
      Top             =   720
      Width           =   375
   End
   Begin VB.Label lblCidade 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   2520
      TabIndex        =   8
      Top             =   720
      Width           =   2535
   End
   Begin VB.Label Label3 
      Caption         =   "TIPO CLIENTE"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   360
      TabIndex        =   7
      Top             =   1200
      Width           =   1455
   End
   Begin VB.Label lblTipo 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   2520
      TabIndex        =   6
      Top             =   1080
      Width           =   3975
   End
   Begin VB.Label lblCliente 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   2520
      TabIndex        =   2
      Top             =   360
      Width           =   3975
   End
   Begin VB.Label Label1 
      Caption         =   "CLIENTE"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   360
      TabIndex        =   0
      Top             =   360
      Width           =   975
   End
End
Attribute VB_Name = "frmInclui"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Option Explicit

Private Sub Command1_Click()

  If txtCod_Cliente <> "" Then
  
    SQL = "Select cod_cliente"
    SQL = SQL & " From cliente_despesa"
    SQL = SQL & " Where cod_cliente = :cod"

    oradatabase.Parameters.Remove "cod"
    oradatabase.Parameters.Add "cod", txtCod_Cliente, 1
    Set ss = oradatabase.dbcreatedynaset(SQL, 8&)
    
    If ss.EOF And ss.BOF Then
       SQL = "BEGIN"
       SQL = SQL & " Insert into cliente_despesa "
       SQL = SQL & " values(:cod);"
       SQL = SQL & " COMMIT;"
       SQL = SQL & "EXCEPTION"
       SQL = SQL & " WHEN OTHERS THEN"
       SQL = SQL & " ROLLBACK;"
       SQL = SQL & " :cod_errora := SQLCODE;"
       SQL = SQL & " :txt_errora := SQLERRM;"
       SQL = SQL & "END;"
    
       oradatabase.Parameters.Remove "cod"
       oradatabase.Parameters.Remove "cod_errora"
       oradatabase.Parameters.Remove "txt_errora"
     
       oradatabase.Parameters.Add "cod", txtCod_Cliente, 1
       oradatabase.Parameters.Add "cod_errora", 0, 2
       oradatabase.Parameters.Add "txt_errora", "", 2
  
       oradatabase.ExecuteSQL SQL
            
       If Val(oradatabase.Parameters("cod_errora")) <> 0 Then
         MsgBox oradatabase.Parameters("txt_errora") & ".Ligue para o Depto de Sistemas"
         Limpa
         Exit Sub
       Else
         Limpa
       End If
       
  Else
    MsgBox "Cliente j� cadastrado", vbExclamation, "Aten��o"
    Limpa
    Exit Sub
  End If
 End If


End Sub

Private Sub Command2_Click()
  
  If txtCod_Cliente <> "" Then
       SQL = "BEGIN"
       SQL = SQL & " Delete from cliente_despesa "
       SQL = SQL & " where cod_cliente = :cod;"
       SQL = SQL & " COMMIT;"
       SQL = SQL & "EXCEPTION"
       SQL = SQL & " WHEN OTHERS THEN"
       SQL = SQL & " ROLLBACK;"
       SQL = SQL & " :cod_errora := SQLCODE;"
       SQL = SQL & " :txt_errora := SQLERRM;"
       SQL = SQL & "END;"
    
       oradatabase.Parameters.Remove "cod"
       oradatabase.Parameters.Remove "cod_errora"
       oradatabase.Parameters.Remove "txt_errora"
     
       oradatabase.Parameters.Add "cod", txtCod_Cliente, 1
       oradatabase.Parameters.Add "cod_errora", 0, 2
       oradatabase.Parameters.Add "txt_errora", "", 2
  
       oradatabase.ExecuteSQL SQL
            
       If Val(oradatabase.Parameters("cod_errora")) <> 0 Then
         MsgBox oradatabase.Parameters("txt_errora") & ".Ligue para o Depto de Sistemas"
         Limpa
         Exit Sub
       Else
         Limpa
       End If
       
 End If

End Sub

Private Sub Command3_Click()
  Unload Me
End Sub

Private Sub txtCod_Cliente_Change()
  If txtCod_Cliente = "" Then
    lblCliente = ""
    lblTipo = ""
  End If
End Sub

Private Sub txtCod_Cliente_KeyPress(KeyAscii As Integer)
  KeyAscii = Numerico(KeyAscii)
End Sub


Private Sub txtCod_Cliente_LostFocus()
  
  If txtCod_Cliente <> "" Then
    SQL = "Select a.nome_cliente, b.desc_tipo_cli,"
    SQL = SQL & " c.nome_cidade,c.cod_uf"
    SQL = SQL & " from cliente a, tipo_cliente b,"
    SQL = SQL & " cidade c"
    SQL = SQL & " where a.cod_cliente = :cli and"
    SQL = SQL & " a.cod_cidade = c.cod_cidade and"
    SQL = SQL & " a.cod_tipo_cliente = b.cod_tipo_cli"
  
    oradatabase.Parameters.Remove "cli"
    oradatabase.Parameters.Add "cli", txtCod_Cliente, 1
  
    Set ss = oradatabase.dbcreatedynaset(SQL, 8&)
  
    If ss.EOF And ss.BOF Then
      MsgBox "CLIENTE N�O CADASTRADO", vbExclamation, "ATEN��O"
      Limpa
      Exit Sub
    Else
      lblCliente = ss!nome_cliente
      lblTipo = ss!desc_tipo_cli
      lblCidade = ss!nome_cidade
      lblUF = ss!cod_uf
    End If
  End If
  
End Sub



Public Sub Limpa()
   txtCod_Cliente = ""
   lblCliente = ""
   lblTipo = ""
   lblCidade = ""
   lblUF = ""
   txtCod_Cliente.SetFocus
End Sub
