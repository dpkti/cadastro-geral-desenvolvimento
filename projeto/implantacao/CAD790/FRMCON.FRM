VERSION 4.00
Begin VB.Form frmCon 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Consulta"
   ClientHeight    =   5940
   ClientLeft      =   1140
   ClientTop       =   1515
   ClientWidth     =   6690
   Height          =   6345
   Left            =   1080
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5940
   ScaleWidth      =   6690
   Top             =   1170
   Width           =   6810
   Begin VB.CommandButton Command1 
      Caption         =   "SAIR"
      Height          =   615
      Left            =   2880
      TabIndex        =   1
      Top             =   4680
      Width           =   1215
   End
   Begin VB.ListBox lstCliente 
      ForeColor       =   &H00800000&
      Height          =   2400
      Left            =   840
      TabIndex        =   0
      Top             =   1440
      Width           =   4695
   End
   Begin VB.Label lblTotal 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   375
      Left            =   1800
      TabIndex        =   4
      Top             =   3960
      Width           =   1095
   End
   Begin VB.Label Label2 
      Caption         =   "TOTAL "
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   255
      Left            =   840
      TabIndex        =   3
      Top             =   3960
      Width           =   735
   End
   Begin VB.Label Label1 
      Caption         =   "LISTA DE CLIENTES"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   375
      Left            =   960
      TabIndex        =   2
      Top             =   1080
      Width           =   2295
   End
End
Attribute VB_Name = "frmCon"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Option Explicit

Private Sub Command1_Click()
  Unload Me
End Sub

Private Sub Form_Load()
  Dim I As Long

  On Error GoTo Trata_Erro

  SQL = "Select TO_CHAR(a.cod_cliente,'000000')||' - '||b.nome_cliente cliente"
  SQL = SQL & " From cliente_despesa a,"
  SQL = SQL & " cliente b"
  SQL = SQL & " Where a.cod_cliente=b.cod_cliente"
  SQL = SQL & " Order by b.nome_cliente"

  Set ss = oradatabase.dbcreatedynaset(SQL, 0&)
  
  If ss.EOF And ss.BOF Then
    MsgBox "N�o h� cliente cadastrado", vbExclamation, "Aten��o"
    Unload Me
  Else
    For I = 1 To ss.recordcount
      lstCliente.AddItem ss!cliente
      ss.MoveNext
    Next
    lblTotal = ss.recordcount
  End If
  Exit Sub
  
Trata_Erro:
  If Err = 364 Then
    Resume Next
  End If
  
End Sub


