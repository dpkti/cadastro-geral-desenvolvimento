VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form frmComparativo 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Gerar Planilha Comparativa"
   ClientHeight    =   2925
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6180
   Icon            =   "frmComparativo.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   195
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   412
   Begin VB.Frame fra 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1590
      Left            =   45
      TabIndex        =   4
      Top             =   945
      Width           =   6090
      Begin VB.ComboBox cboUF_Origem 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1170
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   450
         Width           =   1905
      End
      Begin VB.ComboBox cboUF_Destino 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1170
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   990
         Width           =   1905
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "UF Origem:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   225
         TabIndex        =   8
         Top             =   540
         Width           =   915
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "UF Destino:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   180
         TabIndex        =   7
         Top             =   1080
         Width           =   945
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   2595
      Width           =   6180
      _ExtentX        =   10901
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   10848
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      Top             =   810
      Width           =   6090
      _ExtentX        =   10742
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmComparativo.frx":23D2
      PICN            =   "frmComparativo.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd4 
      Height          =   690
      Left            =   900
      TabIndex        =   3
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmComparativo.frx":30C8
      PICN            =   "frmComparativo.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmComparativo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False



Private Sub cmd4_Click()

    If cboUF_Origem.Text = "" And cboUF_Destino.Text = "" Then
        MsgBox "Seleciona pelo menos um UF", , "Aten��o"
        cboUF_Origem.SetFocus
        Exit Sub
    End If

    Screen.MousePointer = 11
    stbBarra.Panels(1).Text = "Gerando Relat�rio. Aguarde..."
    DoEvents
    
    vBanco.Parameters.Remove "PM_UFORIG"
    vBanco.Parameters.Add "PM_UFORIG", Mid(Trim(cboUF_Origem.Text), 1, 2), 1

    vBanco.Parameters.Remove "PM_UFDEST"
    vBanco.Parameters.Add "PM_UFDEST", Mid(Trim(cboUF_Destino.Text), 1, 2), 1
    
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").serverType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 17, 20, 2000, 0
    
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "PRODUCAO.PCK_CAD510.PR_CON_COMPARATIVO(:PM_UFORIG,:PM_UFDEST,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
    
    vErro = vVB_Generica_001.ExecutaPL(vBanco, vSql)

    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
    
    If vObjOracle.EOF Then
        MsgBox "Nenhum comparativo para a UF selecionada", , "Aten��o"
        Exit Sub
    End If
    
    PlanilhaExcel False
    
    Screen.MousePointer = 0
    stbBarra.Panels(1).Text = ""
    DoEvents
    
End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    
    Me.Top = 1500
    Me.Left = 300
    
    Set vObjOracle = vVB_Generica_001.TabelaUF(vBanco)
    
    Call vVB_Generica_001.PreencheComboList(vObjOracle, cboUF_Origem, "UF", "DESC. UF")
    
    Call vVB_Generica_001.PreencheComboList(vObjOracle, cboUF_Destino, "UF", "DESC. UF")
    
End Sub



Sub PlanilhaExcel(ByVal Imprimir As Boolean)

    On Error GoTo TrataErro
    
    Dim x As Integer, y As Integer, Z As Integer
    
    Dim EApp As Excel.Application
    Dim EwkB As Excel.Workbook
    Dim EwkS As Excel.Worksheet
    
    Screen.MousePointer = 11
    DoEvents
    
    On Error Resume Next
    
    Set EApp = CreateObject("Excel.Application")
    Set EwkB = EApp.Workbooks.Add
    Set EwkS = EwkB.Sheets(1)
    
        
    ' Esconde o Excel
    'EApp.Application.Visible = True

    ' Configura Margens da P�gina
    'EApp.ActiveSheet.PageSetup.PrintArea = ""
    
    With EApp.ActiveSheet.PageSetup
        '.LeftHeader = "INDICE DE INFLA��O DE COMPRA"
        '.CenterHeader = cmbFornecedor.Text
        '.RightHeader = "&D"
        '.LeftFooter = "Comercial Automotiva Ltda. Confidencial"
        '.CenterFooter = "Depto. Controladoria"
        .RightFooter = "P�gina &P/&N"
        .LeftMargin = 10 ''Application.InchesToPoints(0.393700787401575)
        .RightMargin = 10 ''Application.InchesToPoints(0.393700787401575)
        .TopMargin = 10 ''Application.InchesToPoints(0.393700787401575)
        .BottomMargin = 10 ''Application.InchesToPoints(0.393700787401575)
        '.HeaderMargin = Application.InchesToPoints(0)
        '.FooterMargin = Application.InchesToPoints(0)
        .PrintHeadings = False
        .PrintGridlines = False
        .PrintComments = xlPrintNoComments
        .PrintQuality = 600
        .CenterHorizontally = False
        .CenterVertically = False
        '.Orientation = xlPortrait
        .Draft = False
        .PaperSize = xlPaperA4
        .FirstPageNumber = xlAutomatic
        .Order = xlDownThenOver
        .BlackAndWhite = False
        '.Zoom = 65
    End With
    
'    EApp.Columns(1).ColumnWidth = 15
'    EApp.Columns(2).ColumnWidth = 18
'    EApp.Columns(3).ColumnWidth = 40
'
'    EApp.Cells(1, 1).Value = "INFORME DE RENDIMENTO DO ANO " & frmConsulta.txtAno.Text
'    EApp.Range(EApp.Cells(1, 1), EApp.Cells(1, 5)).Select
'    With EApp.Selection
'        .HorizontalAlignment = xlCenter
'        .VerticalAlignment = xlBottom
'        .WrapText = False
'        .Orientation = 1
'        .ShrinkToFit = False
'        .MergeCells = True
'    End With
'    EApp.Selection.Merge
'
'    EApp.Cells(1, 1).Font.Bold = True
'
'    EApp.Cells(4, 1).Value = "COD. REPRES"
'    EApp.Cells(4, 1).Font.Bold = True
'
'    EApp.Cells(4, 2).Value = "CGC"
'    EApp.Cells(4, 2).Font.Bold = True
'
'    EApp.Cells(4, 3).Value = "RAZ�O SOCIAL"
'    EApp.Cells(4, 3).Font.Bold = True
'
'
'
'    x = 1
'    For I = 4 To 28 Step 2
'        EApp.Cells(3, I).Value = UCase(MonthName(x))
'        EApp.Range(EApp.Cells(3, I), EApp.Cells(3, I + 1)).Select
'        EApp.Selection.Merge
'        EApp.Selection.HorizontalAlignment = xlCenter
'        EApp.Cells(3, I).Font.Bold = True
'        x = x + 1
'    Next
'
'    For I = 4 To 27 Step 2
'        EApp.Cells(4, I).Value = "VALOR"
'        EApp.Cells(4, I + 1).Value = "IR"
'        EApp.Cells(4, I).HorizontalAlignment = xlCenter
'        EApp.Cells(4, I + 1).HorizontalAlignment = xlCenter
'        EApp.Cells(4, I).Font.Bold = True
'        EApp.Cells(4, I + 1).Font.Bold = True
'    Next

    ' Gera Dados na P�gina e Formata C�lulas
    Z = 1
    vObjOracle.MoveFirst
    
    col = vObjOracle.Fields.Count

    For y = 0 To col
        EApp.Cells(Z, y + 1).Value = vObjOracle.Fields(y).Name
    Next

    Z = 2

    While Not vObjOracle.EOF
        For y = 0 To 26
            EApp.Cells(Z, y + 1).Value = vObjOracle(y)
           'EApp.Cells(Z, y + 1).Font.Bold = True
        Next
        Z = Z + 1
        vObjOracle.MoveNext
    Wend
    
    
    If Not Imprimir Then
        ' Exibe Excel
        EApp.Application.Visible = True
    Else
        EApp.ActiveWindow.SelectedSheets.PrintOut Copies:=1
        EApp.ActiveWorkbook.Close (False)
        EApp.Quit
    End If
    
    ' Volta Cursor do Mouse
    Screen.MousePointer = 1
    
    ' Desliga o Excel do Programa
    Set EwkS = Nothing
    Set EwkB = Nothing
    Set EApp = Nothing
    
    Exit Sub
    
TrataErro:
    
       Screen.MousePointer = 1
       MsgBox "Ocorreu erro: " & Err.Number & " - " & Err.Description, 16
       Set EwkS = Nothing
       Set EwkB = Nothing
       Set EApp = Nothing

   
End Sub
