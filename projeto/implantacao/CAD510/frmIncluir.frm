VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form frmIncluir 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Inclus�o"
   ClientHeight    =   7470
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7380
   Icon            =   "frmIncluir.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   498
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   492
   Begin VB.ComboBox cboTP_Relatorio 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   2520
      Style           =   2  'Dropdown List
      TabIndex        =   39
      Top             =   6450
      Width           =   2685
   End
   Begin VB.TextBox txtClass 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   2025
      MaxLength       =   10
      TabIndex        =   8
      Top             =   1035
      Width           =   1230
   End
   Begin VB.ComboBox cboOrigem 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      ItemData        =   "frmIncluir.frx":23D2
      Left            =   2025
      List            =   "frmIncluir.frx":23D4
      Style           =   2  'Dropdown List
      TabIndex        =   9
      Top             =   1440
      Width           =   1725
   End
   Begin VB.ComboBox cboDestino 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   2040
      Style           =   2  'Dropdown List
      TabIndex        =   10
      Top             =   1920
      Width           =   1725
   End
   Begin VB.Frame fra 
      Caption         =   "TRIBUTA��O DE ICMS"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   3090
      Left            =   90
      TabIndex        =   28
      Top             =   2385
      Width           =   3810
      Begin VB.ComboBox cboTrib_ReduzICMS 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1620
         Style           =   2  'Dropdown List
         TabIndex        =   45
         Top             =   2460
         Width           =   1725
      End
      Begin VB.ComboBox cboTrib_Montadora 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1635
         Style           =   2  'Dropdown List
         TabIndex        =   42
         Top             =   2040
         Width           =   1725
      End
      Begin VB.ComboBox cboTrib_Rev 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1635
         Style           =   2  'Dropdown List
         TabIndex        =   11
         Top             =   270
         Width           =   1725
      End
      Begin VB.ComboBox cboTrib_Ins 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1635
         Style           =   2  'Dropdown List
         TabIndex        =   12
         Top             =   720
         Width           =   1725
      End
      Begin VB.ComboBox cboTrib_Ise 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1635
         Style           =   2  'Dropdown List
         TabIndex        =   13
         Top             =   1170
         Width           =   1725
      End
      Begin VB.ComboBox cboTrib_Tare 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1635
         Style           =   2  'Dropdown List
         TabIndex        =   14
         Top             =   1620
         Width           =   1725
      End
      Begin VB.Label Label13 
         Appearance      =   0  'Flat
         Caption         =   "REDUZ ICMS"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   180
         TabIndex        =   44
         Top             =   2520
         Width           =   1185
      End
      Begin VB.Label Label11 
         Appearance      =   0  'Flat
         Caption         =   "MONTADORA"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   180
         TabIndex        =   43
         Top             =   2085
         Width           =   1185
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         Caption         =   "REVENDEDOR"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   180
         TabIndex        =   32
         Top             =   315
         Width           =   1185
      End
      Begin VB.Label Label14 
         Appearance      =   0  'Flat
         Caption         =   "INSCRITO"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   180
         TabIndex        =   31
         Top             =   765
         Width           =   1185
      End
      Begin VB.Label ISENTO 
         Appearance      =   0  'Flat
         Caption         =   "ISENTO"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   180
         TabIndex        =   30
         Top             =   1215
         Width           =   1185
      End
      Begin VB.Label Label15 
         Appearance      =   0  'Flat
         Caption         =   "TARE"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   180
         TabIndex        =   29
         Top             =   1665
         Width           =   1185
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "FATOR"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   3090
      Left            =   4110
      TabIndex        =   23
      Top             =   2385
      Width           =   3180
      Begin VB.TextBox txtFat_Montadora 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1680
         MaxLength       =   6
         TabIndex        =   40
         Top             =   2040
         Width           =   1230
      End
      Begin VB.TextBox txtFat_rev 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1680
         MaxLength       =   6
         TabIndex        =   15
         Top             =   270
         Width           =   1230
      End
      Begin VB.TextBox txtFat_Ins 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1680
         MaxLength       =   6
         TabIndex        =   16
         Top             =   720
         Width           =   1230
      End
      Begin VB.TextBox txtFat_Ise 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1680
         MaxLength       =   6
         TabIndex        =   17
         Top             =   1170
         Width           =   1230
      End
      Begin VB.TextBox txtFat_Tare 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1680
         MaxLength       =   6
         TabIndex        =   18
         Top             =   1620
         Width           =   1230
      End
      Begin VB.Label Label10 
         Appearance      =   0  'Flat
         Caption         =   "MONTADORA"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   180
         TabIndex        =   41
         Top             =   2085
         Width           =   1185
      End
      Begin VB.Label Label4 
         Appearance      =   0  'Flat
         Caption         =   "TARE"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   180
         TabIndex        =   27
         Top             =   1665
         Width           =   1185
      End
      Begin VB.Label Label5 
         Appearance      =   0  'Flat
         Caption         =   "ISENTO"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   180
         TabIndex        =   26
         Top             =   1215
         Width           =   1185
      End
      Begin VB.Label Label6 
         Appearance      =   0  'Flat
         Caption         =   "INSCRITO"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   180
         TabIndex        =   25
         Top             =   765
         Width           =   1185
      End
      Begin VB.Label Label12 
         Appearance      =   0  'Flat
         Caption         =   "REVENDEDOR"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   180
         TabIndex        =   24
         Top             =   315
         Width           =   1185
      End
   End
   Begin VB.ComboBox cboCred 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   2520
      Style           =   2  'Dropdown List
      TabIndex        =   19
      Top             =   5985
      Width           =   1725
   End
   Begin VB.TextBox txtMg_Lucro 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   6390
      MaxLength       =   7
      TabIndex        =   20
      Top             =   5985
      Width           =   855
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   7140
      Width           =   7380
      _ExtentX        =   13018
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   12965
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      Top             =   810
      Width           =   7410
      _ExtentX        =   13070
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   90
      TabIndex        =   22
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmIncluir.frx":23D6
      PICN            =   "frmIncluir.frx":23F2
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd1 
      Height          =   690
      Left            =   1215
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Gravar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmIncluir.frx":30CC
      PICN            =   "frmIncluir.frx":30E8
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd2 
      Height          =   690
      Left            =   1980
      TabIndex        =   21
      TabStop         =   0   'False
      ToolTipText     =   "Cancelar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmIncluir.frx":3DC2
      PICN            =   "frmIncluir.frx":3DDE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd3 
      Height          =   690
      Left            =   2745
      TabIndex        =   3
      TabStop         =   0   'False
      ToolTipText     =   "Copiar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmIncluir.frx":46B8
      PICN            =   "frmIncluir.frx":46D4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd4 
      Height          =   690
      Left            =   4275
      TabIndex        =   5
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmIncluir.frx":53AE
      PICN            =   "frmIncluir.frx":53CA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd5 
      Height          =   690
      Left            =   5040
      TabIndex        =   6
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisar UF"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmIncluir.frx":60A4
      PICN            =   "frmIncluir.frx":60C0
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd6 
      Height          =   690
      Left            =   5805
      TabIndex        =   7
      TabStop         =   0   'False
      ToolTipText     =   "Importar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmIncluir.frx":6D9A
      PICN            =   "frmIncluir.frx":6DB6
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd7 
      Height          =   690
      Left            =   3510
      TabIndex        =   4
      TabStop         =   0   'False
      ToolTipText     =   "Copiar UF"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmIncluir.frx":7A90
      PICN            =   "frmIncluir.frx":7AAC
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdExcel 
      Height          =   690
      Left            =   6600
      TabIndex        =   46
      TabStop         =   0   'False
      ToolTipText     =   "Exportar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmIncluir.frx":8786
      PICN            =   "frmIncluir.frx":87A2
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label9 
      Appearance      =   0  'Flat
      Caption         =   "Tipo"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   90
      TabIndex        =   38
      Top             =   6510
      Width           =   405
   End
   Begin VB.Label lbl 
      Appearance      =   0  'Flat
      Caption         =   "Classifica��o Fiscal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   225
      TabIndex        =   37
      Top             =   1080
      Width           =   1815
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      Caption         =   "UF Origem"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   225
      TabIndex        =   36
      Top             =   1485
      Width           =   1815
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      Caption         =   "UF Destino"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   225
      TabIndex        =   35
      Top             =   1935
      Width           =   1815
   End
   Begin VB.Label Label7 
      Appearance      =   0  'Flat
      Caption         =   "Relat�rio Cr�dito Suspenso"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   90
      TabIndex        =   34
      Top             =   6030
      Width           =   2445
   End
   Begin VB.Label Label8 
      Appearance      =   0  'Flat
      Caption         =   "Mg Lucro"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   5310
      TabIndex        =   33
      Top             =   6030
      Width           =   975
   End
End
Attribute VB_Name = "frmIncluir"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cboCred_Change()
 If cboCred.DataChanged Then
  If cboCred = "N�O" Then
    cboTP_Relatorio.Text = "N-NENHUM"
    cboTP_Relatorio.Enabled = False
  Else
    cboTP_Relatorio.Enabled = True
  End If
 End If
End Sub

Private Sub cboCred_LostFocus()
  If cboCred = "N�O" Then
    cboTP_Relatorio.Text = "N-NENHUM"
    cboTP_Relatorio.Enabled = False
  Else
    cboTP_Relatorio.Enabled = True
  End If
End Sub



Private Sub cmd1_Click()

    On Error GoTo TrataErro
    Screen.MousePointer = 11
    If txtClass = "" Or cboOrigem = "" Or cboDestino = "" Or _
        cboTrib_Rev = "" Or cboTrib_Ins = "" Or cboTrib_Ise = "" Or cboTrib_Tare = "" Or _
        cboTrib_Montadora = "" Or txtFat_rev = "" Or txtFat_Ins = "" Or txtFat_Ise = "" Or _
        txtFat_Tare = "" Or txtFat_Montadora = "" Or cboCred = "" Or txtMg_Lucro = "" Or _
        cboTP_Relatorio = "" Then
        MsgBox "Para gravar � preciso preencher todos os campos", vbExclamation, "Aten��o"
        Screen.MousePointer = 0
        Exit Sub
    End If
    
    If Mid(cboCred, 1, 1) = "N" And Mid(cboTP_Relatorio, 1, 1) <> "N" Then
       MsgBox "Credito Suspenso for N�O, ent�o o tipo dever� ser N�O", vbExclamation, "Aten��o"
        Screen.MousePointer = 0
        Exit Sub
    End If
    
    If Mid(cboCred, 1, 1) <> "N" And Mid(cboTP_Relatorio, 1, 1) = "N" Then
       MsgBox "Credito Suspenso diferente de N�O, ent�o o tipo dever� ser diferente de N�O", vbExclamation, "Aten��o"
        Screen.MousePointer = 0
        Exit Sub
    End If

   'Leandro - 29/05/09xx
    vSql = "PRODUCAO.PCK_CAD510.pr_INC_CLASS(:PM_CLASS,:PM_UFORIG,:PM_UFDEST, " & _
            ":PM_TRIBREV,:PM_TRIBINS,:PM_TRIBISE,:PM_TRIBTARE,:PM_TRIBMONTADORA,:PM_FATREV,:PM_FATINS," & _
            ":PM_FATISE,:PM_FATTARE,:PM_FATMONTADORA,:PM_CRED,:PM_MARG,:PM_TPREL,:PM_CODERRO,:PM_TXTERRO,:PM_REDICMS)"
    '
    
    vBanco.Parameters.Remove "PM_CLASS"
    vBanco.Parameters.Add "PM_CLASS", txtClass.Text, 1
    vBanco.Parameters.Remove "PM_UFORIG"
    vBanco.Parameters.Add "PM_UFORIG", Mid(Trim(cboOrigem), 1, 2), 1
    vBanco.Parameters.Remove "PM_UFDEST"
    vBanco.Parameters.Add "PM_UFDEST", Mid(Trim(cboDestino), 1, 2), 1
    vBanco.Parameters.Remove "PM_TRIBREV"
    vBanco.Parameters.Add "PM_TRIBREV", Trim(cboTrib_Rev), 1
    vBanco.Parameters.Remove "PM_TRIBINS"
    vBanco.Parameters.Add "PM_TRIBINS", Trim(cboTrib_Ins), 1
    vBanco.Parameters.Remove "PM_TRIBISE"
    vBanco.Parameters.Add "PM_TRIBISE", Trim(cboTrib_Ise), 1
    vBanco.Parameters.Remove "PM_TRIBTARE"
    vBanco.Parameters.Add "PM_TRIBTARE", Trim(cboTrib_Tare), 1
    vBanco.Parameters.Remove "PM_TRIBMONTADORA"
    vBanco.Parameters.Add "PM_TRIBMONTADORA", Trim(cboTrib_Montadora), 1
    vBanco.Parameters.Remove "PM_FATREV"
    vBanco.Parameters.Add "PM_FATREV", Trim(txtFat_rev), 1
    vBanco.Parameters.Remove "PM_FATINS"
    vBanco.Parameters.Add "PM_FATINS", Trim(txtFat_Ins), 1
    vBanco.Parameters.Remove "PM_FATISE"
    vBanco.Parameters.Add "PM_FATISE", Trim(txtFat_Ise), 1
    vBanco.Parameters.Remove "PM_FATTARE"
    vBanco.Parameters.Add "PM_FATTARE", Trim(txtFat_Tare), 1
    vBanco.Parameters.Remove "PM_FATMONTADORA"
    vBanco.Parameters.Add "PM_FATMONTADORA", Trim(txtFat_Montadora), 1
    vBanco.Parameters.Remove "PM_CRED"
    vBanco.Parameters.Add "PM_CRED", Mid(Trim(cboCred), 1, 1), 1
    vBanco.Parameters.Remove "PM_MARG"
    vBanco.Parameters.Add "PM_MARG", Trim(Replace(txtMg_Lucro, ",", ".")), 1
    vBanco.Parameters.Remove "PM_TPREL"
    vBanco.Parameters.Add "PM_TPREL", Mid(Trim(cboTP_Relatorio), 1, 1), 1
    
   'Leandro - 29/05/09
    vBanco.Parameters.Remove "PM_REDICMS"
    vBanco.Parameters.Add "PM_REDICMS", Mid(Trim(cboTrib_ReduzICMS), 1, 1), 1
   '------------------
    
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
          
    vErro = vVB_Generica_001.ExecutaPL(vBanco, vSql)
     
    vErro = IIf(vBanco.Parameters("PM_CODERRO") = "", 0, vBanco.Parameters("PM_CODERRO"))
     
    If vErro <> 0 Then
        MsgBox vBanco.Parameters("PM_TXTERRO") & ".Ligue para o Depto de Sistemas"
        Screen.MousePointer = 0
        Exit Sub
    Else
      
        'strMsg = "DESEJA GRAVAR TRIBUTA��O 6, NA ENTRADA DA NF NO CD DESTINO PARA ESTA"
        'strMsg = strMsg & " CLASSIFICA��O(ANTECIPA��O TRIBUTARIA) ? "
       ' strResposta = MsgBox(strMsg, vbYesNo, "ATEN��O")
        
        'If strResposta = vbYes Then
      
         '   vBanco.Parameters.Remove "PM_ACT"
         '   vBanco.Parameters.Add "PM_ACT", "I", 1
      
          '  vSql = "PRODUCAO.PCK_CAD510.pr_ACT_CLASS6(:PM_CLASS,:PM_UFORIG,:PM_UFDEST,:PM_ACT,:PM_CODERRO,:PM_TXTERRO)"
     
           ' vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        
            'vErro = IIf(vBanco.Parameters("PM_CODERRO") = "", 0, vBanco.Parameters("PM_CODERRO"))
     
    '        If vErro <> 0 Then
     '           MsgBox vBanco.Parameters("PM_TXTERRO") & ".Ligue para o Depto de Sistemas"
      '          Screen.MousePointer = 0
       '         Exit Sub
        '    End If
        'End If
            
        MsgBox "Inclus�o/Altera��o OK ", vbInformation, "Aten��o"
        Screen.MousePointer = 0
        Call LIMPA_TELA
        
    End If
    Screen.MousePointer = 0
    
    
    If bOpcao = 2 Then
        frmPesquisa.Show 1
    End If
 
  Exit Sub
  
TrataErro:
   If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
     Resume
   Else
     'Call Process_Line_Errors(SQL)
   End If
   


End Sub
Private Sub cmd1_MouseOut()
    stbBarra.Panels(1).Text = ""
End Sub

Private Sub cmd1_MouseOver()
    stbBarra.Panels(1).Text = "Grava Classifica��o"
End Sub




Private Sub cmd2_Click()
    Call LIMPA_TELA
End Sub

Private Sub cmd2_MouseOut()
        stbBarra.Panels(1).Text = ""
End Sub

Private Sub cmd2_MouseOver()
        stbBarra.Panels(1).Text = "Limpa os Campos"
End Sub



Private Sub cmd3_Click()
    frmCopia.Show 1
End Sub
Private Sub cmd3_MouseOut()
    stbBarra.Panels(1).Text = ""
End Sub

Private Sub cmd3_MouseOver()
    stbBarra.Panels(1).Text = "Copiar Informa��es de uma classifica��o para outra"
End Sub



Private Sub cmd5_Click()
  vOpcao = 2
  If txtClass.Text = "" Or cboOrigem = "" Then
    MsgBox "Para executar a consulta digite a classifica��o Fiscal e a UF Origem", vbInformation, "Aten��o"
    Exit Sub
  End If
    
  frmPesquisa.Show
End Sub
Private Sub cmd5_MouseOut()
    stbBarra.Panels(1).Text = ""
End Sub
Private Sub cmd5_MouseOver()
    stbBarra.Panels(1).Text = "Pesquisa por classifica��o e UF origem"
End Sub



Private Sub cmd4_Click()
  vOpcao = 1
  If txtClass.Text = "" Then
    MsgBox "Para executar a consulta preencha a classifica��o Fiscal", vbInformation, "Aten��o"
    Exit Sub
  End If
     
  frmPesquisa.Show
End Sub
Private Sub cmd4_MouseOut()
    stbBarra.Panels(1).Text = ""
End Sub
Private Sub cmd4_MouseOver()
    stbBarra.Panels(1).Text = "Pesquisa por classifica��o"
End Sub


Private Sub cmd6_Click()
    frmImport.Show 1
End Sub
Private Sub cmd6_MouseOut()
    stbBarra.Panels(1).Text = ""
End Sub

Private Sub cmd6_MouseOver()
    stbBarra.Panels(1).Text = "Importar Classifica��o de Arquivo"
End Sub



Private Sub cmd7_Click()

    frmCopia2.Show 1

End Sub

Private Sub cmd7_MouseOut()
    stbBarra.Panels(1).Text = ""
End Sub

Private Sub cmd7_MouseOver()
    stbBarra.Panels(1).Text = "Copiar todas classifica��o de uma UF origem para outra"
End Sub

Private Sub cmdExcel_Click()
    frmExportacao.Show
End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub

Private Sub Form_Load()
    
    Me.Top = 0
    Me.Left = 0
    
    
    Set vObjOracle = vVB_Generica_001.TabelaUF(vBanco)
    
    Call vVB_Generica_001.PreencheComboList(vObjOracle, cboOrigem, "UF", "DESC. UF")
    
    Call vVB_Generica_001.PreencheComboList(vObjOracle, cboDestino, "UF", "DESC. UF")
    cboDestino.AddItem "SU - SUFRAMA"
  '------------------------------
  
  'preenche combo tributacao revendedor
      cboTrib_Rev.AddItem "0"
      cboTrib_Rev.AddItem "1"
      cboTrib_Rev.AddItem "3"
      cboTrib_Rev.AddItem "6"
      cboTrib_Rev.AddItem "8"
  '-------------------------------
  
  'preenche combo tributacao inscrito
      cboTrib_Ins.AddItem "0"
      cboTrib_Ins.AddItem "1"
      cboTrib_Ins.AddItem "3"
      cboTrib_Ins.AddItem "6"
      cboTrib_Ins.AddItem "8"
  '-------------------------------
      
   'preenche combo tributacao insento
      cboTrib_Ise.AddItem "0"
      cboTrib_Ise.AddItem "1"
      cboTrib_Ise.AddItem "3"
      cboTrib_Ise.AddItem "6"
      cboTrib_Ise.AddItem "8"
  '-------------------------------
  
   'preenche combo tributacao TARE
      cboTrib_Tare.AddItem "0"
      cboTrib_Tare.AddItem "1"
      cboTrib_Tare.AddItem "3"
      cboTrib_Tare.AddItem "6"
      cboTrib_Tare.AddItem "8"
  '-------------------------------
      
   'preenche combo tributacao MONTADORA
      cboTrib_Montadora.AddItem "0"
      cboTrib_Montadora.AddItem "1"
      cboTrib_Montadora.AddItem "3"
      cboTrib_Montadora.AddItem "6"
      cboTrib_Montadora.AddItem "8"
  
  '-------------------------------
      
   'preenche combo credito suspenso
      cboCred.AddItem "SIM"
      cboCred.AddItem "N�O"
      
   'preenche combo tipo relatorio
      cboTP_Relatorio.AddItem "N-NENHUM"
      cboTP_Relatorio.AddItem "A-AUTO PE�AS"
      cboTP_Relatorio.AddItem "C-ACESS�RIOS"
      cboTP_Relatorio.AddItem "D-DERIVADO DE PETR�LEO"
         
      
  '------------------------------
  
   'Leandro 28/05/09 - preenche combo redu��o de ICMS
      cboTrib_ReduzICMS.AddItem "N"
      cboTrib_ReduzICMS.AddItem "S"
    
    
End Sub


Private Sub txtClass_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Numero(KeyAscii)

End Sub

Private Sub txtFat_Montadora_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtFat_Montadora)
End Sub

Private Sub txtFat_Montadora_LostFocus()
    Celula = txtFat_Montadora.Text
    ErroC = 0
    If Celula <> "" Then
        PosVal = InStr(1, Celula, ",")
        If PosVal > 2 Then
            ErroC = 1
        ElseIf (Len(Celula) - PosVal) > 6 Then
            ErroC = 1
        End If
    End If
    If ErroC = 1 Then
        MsgBox "Valor Incorreto", vbInformation, "Aten��o"
        txtFat_Montadora.SetFocus
    End If
End Sub

Private Sub txtFat_rev_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtFat_rev)

End Sub

Private Sub txtFat_Ins_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtFat_Ins)

End Sub

Private Sub txtFat_Ise_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtFat_Ise)

End Sub

Private Sub txtFat_rev_LostFocus()
    Celula = txtFat_rev.Text
    ErroC = 0
    If Celula <> "" Then
        PosVal = InStr(1, Celula, ",")
        If PosVal > 2 Then
            ErroC = 1
        ElseIf (Len(Celula) - PosVal) > 6 Then
            ErroC = 1
        End If
    End If
    If ErroC = 1 Then
        MsgBox "Valor Incorreto", vbInformation, "Aten��o"
        txtFat_rev.SetFocus
    End If
End Sub

Private Sub txtFat_Ins_LostFocus()
    Celula = txtFat_Ins.Text
    ErroC = 0
    If Celula <> "" Then
        PosVal = InStr(1, Celula, ",")
        If PosVal > 2 Then
            ErroC = 1
        ElseIf (Len(Celula) - PosVal) > 6 Then
            ErroC = 1
        End If
    End If
    If ErroC = 1 Then
        MsgBox "Valor Incorreto", vbInformation, "Aten��o"
        txtFat_Ins.SetFocus
    End If
End Sub

Private Sub txtFat_Ise_LostFocus()
    Celula = txtFat_Ise.Text
    ErroC = 0
    If Celula <> "" Then
        PosVal = InStr(1, Celula, ",")
        If PosVal > 2 Then
            ErroC = 1
        ElseIf (Len(Celula) - PosVal) > 6 Then
            ErroC = 1
        End If
    End If
    If ErroC = 1 Then
        MsgBox "Valor Incorreto", vbInformation, "Aten��o"
        txtFat_Ise.SetFocus
    End If
End Sub

Private Sub txtFat_Tare_LostFocus()
    Celula = txtFat_Tare.Text
    ErroC = 0
    If Celula <> "" Then
        PosVal = InStr(1, Celula, ",")
        If PosVal > 2 Then
            ErroC = 1
        ElseIf (Len(Celula) - PosVal) > 6 Then
            ErroC = 1
        End If
    End If
    If ErroC = 1 Then
        MsgBox "Valor Incorreto", vbInformation, "Aten��o"
        txtFat_Tare.SetFocus
    End If
End Sub


Private Sub txtMg_Lucro_LostFocus()
    Celula = txtMg_Lucro.Text
    ErroC = 0
    If Celula <> "" Then
        PosVal = InStr(1, Celula, ",")
        If PosVal > 3 Then
            ErroC = 1
        ElseIf (Len(Celula) - PosVal) > 2 Then
            ErroC = 1
        End If
    End If
    If ErroC = 1 Then
        MsgBox "Valor Incorreto", vbInformation, "Aten��o"
        txtMg_Lucro.SetFocus
    End If
End Sub

Private Sub txtFat_tare_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtFat_Tare)

End Sub


Private Sub txtMg_Lucro_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtMg_Lucro)

End Sub
