VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmAntec 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "ANTECIPA��O TRIBUTARIA"
   ClientHeight    =   7065
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9555
   ControlBox      =   0   'False
   Icon            =   "frmAntec.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   471
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   637
   Begin VB.TextBox txtMg_Lucro 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1800
      MaxLength       =   7
      TabIndex        =   6
      Top             =   2280
      Width           =   855
   End
   Begin VB.ComboBox cboDestino 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   1755
      Style           =   2  'Dropdown List
      TabIndex        =   5
      Top             =   1845
      Width           =   1725
   End
   Begin VB.ComboBox cboOrigem 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   1755
      Style           =   2  'Dropdown List
      TabIndex        =   4
      Top             =   1395
      Width           =   1725
   End
   Begin VB.TextBox txtClass 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1755
      MaxLength       =   10
      TabIndex        =   3
      Top             =   990
      Width           =   1230
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   6735
      Width           =   9555
      _ExtentX        =   16854
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   16801
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      Top             =   810
      Width           =   9375
      _ExtentX        =   16536
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmAntec.frx":23D2
      PICN            =   "frmAntec.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd1 
      Height          =   690
      Left            =   3060
      TabIndex        =   7
      TabStop         =   0   'False
      ToolTipText     =   "Gravar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmAntec.frx":30C8
      PICN            =   "frmAntec.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSFlexGridLib.MSFlexGrid mfgCons 
      Height          =   3210
      Left            =   90
      TabIndex        =   17
      Top             =   2760
      Width           =   9390
      _ExtentX        =   16563
      _ExtentY        =   5662
      _Version        =   393216
      BackColorBkg    =   -2147483633
      AllowUserResizing=   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin Bot�o.cmd cmd4 
      Height          =   690
      Left            =   3825
      TabIndex        =   8
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmAntec.frx":3DBE
      PICN            =   "frmAntec.frx":3DDA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd7 
      Height          =   690
      Left            =   4590
      TabIndex        =   9
      TabStop         =   0   'False
      ToolTipText     =   "Copiar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmAntec.frx":4AB4
      PICN            =   "frmAntec.frx":4AD0
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd6 
      Height          =   690
      Left            =   5355
      TabIndex        =   10
      TabStop         =   0   'False
      ToolTipText     =   "Importar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmAntec.frx":57AA
      PICN            =   "frmAntec.frx":57C6
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd2 
      Height          =   690
      Left            =   6885
      TabIndex        =   12
      TabStop         =   0   'False
      ToolTipText     =   "Deleter em Lote"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmAntec.frx":64A0
      PICN            =   "frmAntec.frx":64BC
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdExcel 
      Height          =   690
      Left            =   6120
      TabIndex        =   11
      TabStop         =   0   'False
      ToolTipText     =   "Exportar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmAntec.frx":6D96
      PICN            =   "frmAntec.frx":6DB2
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label8 
      Appearance      =   0  'Flat
      Caption         =   "Mg Lucro"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   120
      TabIndex        =   18
      Top             =   2280
      Width           =   975
   End
   Begin VB.Label Label4 
      Caption         =   "Duplo click para excluir a classifica��o da antecipa��o tributaria"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   120
      TabIndex        =   16
      Top             =   6360
      Width           =   5595
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      Caption         =   "UF Destino"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   90
      TabIndex        =   15
      Top             =   1890
      Width           =   1815
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      Caption         =   "UF Origem"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   90
      TabIndex        =   14
      Top             =   1440
      Width           =   1815
   End
   Begin VB.Label lbl 
      Appearance      =   0  'Flat
      Caption         =   "Classifica��o Fiscal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   90
      TabIndex        =   13
      Top             =   1035
      Width           =   1815
   End
End
Attribute VB_Name = "frmAntec"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmd1_Click()

On Error GoTo TrataErro

     Screen.MousePointer = 11
     If txtClass = "" Or cboOrigem = "" Or cboDestino = "" Or txtMg_Lucro = "" Then
        MsgBox "Para gravar � preciso preencher todos os campos", vbExclamation, "Aten��o"
        Screen.MousePointer = 0
        Exit Sub
     End If
     
      strMsg = "CONFIRMA A GRAVA��O DESTA"
      strMsg = strMsg & " CLASSIFICA��O COMO ANTECIPA��O TRIBUTARIA PARA ESTA UF ORIGEM / UF DESTINO ? "
      strResposta = MsgBox(strMsg, vbYesNo, "ATEN��O")
      If strResposta = vbYes Then
      
        vBanco.Parameters.Remove "PM_CLASS"
        vBanco.Parameters.Add "PM_CLASS", txtClass.Text, 1
        vBanco.Parameters.Remove "PM_UFORIG"
        vBanco.Parameters.Add "PM_UFORIG", Mid(Trim(cboOrigem), 1, 2), 1
        vBanco.Parameters.Remove "PM_UFDEST"
        vBanco.Parameters.Add "PM_UFDEST", Mid(Trim(cboDestino), 1, 2), 1
        
        vBanco.Parameters.Remove "PM_MARGEM"
        vBanco.Parameters.Add "PM_MARGEM", txtMg_Lucro, 1
        
        vBanco.Parameters.Remove "PM_ACT"
        vBanco.Parameters.Add "PM_ACT", "I", 1
     
        vBanco.Parameters.Remove "PM_CODERRO"
        vBanco.Parameters.Add "PM_CODERRO", 0, 2
        vBanco.Parameters.Remove "PM_TXTERRO"
        vBanco.Parameters.Add "PM_TXTERRO", "", 2
      
        vSql = "PRODUCAO.PCK_CAD510.pr_ACT_CLASS6(:PM_CLASS,:PM_UFORIG,:PM_UFDEST,:PM_ACT,:PM_MARGEM,:PM_CODERRO,:PM_TXTERRO)"
     
        vErro = vVB_Generica_001.ExecutaPL(vBanco, vSql)
        
        vErro = IIf(vBanco.Parameters("PM_CODERRO") = "", 0, vBanco.Parameters("PM_CODERRO"))
     
       If vErro <> 0 Then
          MsgBox vBanco.Parameters("PM_TXTERRO") & ".Ligue para o Depto de Sistemas"
          Screen.MousePointer = 0
          Exit Sub
        End If
     End If
            
       MsgBox "Inclus�o/Altera��o OK ", vbInformation, "Aten��o"
     '  Call SSCommand4_Click
       Screen.MousePointer = 0
Exit Sub

TrataErro:
  'Call Process_Line_Errors(SQL)
End Sub
Private Sub cmd1_MouseOut()
    stbBarra.Panels(1).Text = ""
End Sub

Private Sub cmd1_MouseOver()
    stbBarra.Panels(1).Text = "Grava Antecipa��o Tributaria"
End Sub




Private Sub cmd2_Click()
    frmDeleteAntec.Show 1
End Sub

Private Sub cmd4_Click()

    'On Error GoTo TrataErro
    If cboDestino = "" Then
        MsgBox "Para fazer a consulta solecione a UF destino", vbInformation, "Aten��o"
        Exit Sub
    End If
  
    MsgBox "Para consultar as classifica��es cadastradas como Antecipa��o Tribut�ria basta " & _
     " informar a UF DESTINO", vbInformation, "Aten��o"
    'cboOrigem.Text = ""
    txtClass.Text = ""
    txtMg_Lucro.Text = ""
  
    Screen.MousePointer = 11

    vBanco.Parameters.Remove "PM_UFDEST"
    vBanco.Parameters.Add "PM_UFDEST", Mid(Trim(cboDestino), 1, 2), 1
   
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").serverType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    
    'Leandro -29/06/09
    'vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 17, 20, 2000, 0
     vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 17, 20, 2000, 0
   
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "PRODUCAO.PCK_CAD510.pr_CON_CLASS_ANTEC(:PM_UFDEST,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"


    vErro = vVB_Generica_001.ExecutaPL(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
    
    If vObjOracle.EOF And vObjOracle.BOF Then
        Screen.MousePointer = vbDefault
        MsgBox "N�o ha tratamento antecipa��o tributaria cadastrado", vbInformation, "Aten��o"
        Exit Sub
    End If
    
    vVB_Generica_001.CarregaGridTabela mfgCons, vObjOracle, 6
    
    'mouse
    Screen.MousePointer = vbDefault
    Exit Sub


End Sub
Private Sub cmd4_MouseOut()
    stbBarra.Panels(1).Text = ""
End Sub
Private Sub cmd4_MouseOver()
    stbBarra.Panels(1).Text = "Pesquisar Antecipa��o Tributaria"
End Sub




Private Sub cmd6_Click()
    frmImportAntec.Show 1
End Sub
Private Sub cmd6_MouseOut()
    stbBarra.Panels(1).Text = ""
End Sub
Private Sub cmd6_MouseOver()
    stbBarra.Panels(1).Text = "Importar Antecipa��o de Arquivo"
End Sub



Private Sub cmd7_Click()
  frmCP_Antec.Show 1
End Sub
Private Sub cmd7_MouseOut()
    stbBarra.Panels(1).Text = ""
End Sub
Private Sub cmd7_MouseOver()
    stbBarra.Panels(1).Text = "Copiar Antecipa��o Tributaria"
End Sub



Private Sub cmdExcel_Click()

    If mfgCons.Rows > 2 Then
        PlanilhaExcel False
    Else
        MsgBox "Realiza uma pesquisa antes de exportar os dados para o excel.", vbInformation, "Aten��o"
        Exit Sub
    End If
    
End Sub

Private Sub cmdVoltar_Click()
    Unload Me
End Sub
Private Sub Form_Load()
    
    Me.Top = 0
    Me.Left = 0
    
    Set vObjOracle = vVB_Generica_001.TabelaUF(vBanco)
    
    Call vVB_Generica_001.PreencheComboList(vObjOracle, cboOrigem, "UF", "DESC. UF")
    
    Call vVB_Generica_001.PreencheComboList(vObjOracle, cboDestino, "UF", "DESC. UF")
    cboDestino.AddItem "SU - SUFRAMA"
    
End Sub



Private Sub mfgCons_DblClick()

    Dim strClass As String
    Dim StrOrigem As String
    Dim strDestino As String
    Dim strMG_Lucro As String
    

    mfgCons.Col = 1
    strClass = mfgCons.Text
    mfgCons.Col = 2
    StrOrigem = mfgCons.Text
    mfgCons.Col = 3
    strDestino = mfgCons.Text
    mfgCons.Col = 5
    strMG_Lucro = mfgCons.Text

    vBanco.Parameters.Remove "PM_CLASS"
    vBanco.Parameters.Add "PM_CLASS", strClass, 1
    vBanco.Parameters.Remove "PM_UFORIG"
    vBanco.Parameters.Add "PM_UFORIG", StrOrigem, 1
    vBanco.Parameters.Remove "PM_UFDEST"
    vBanco.Parameters.Add "PM_UFDEST", strDestino, 1
     vBanco.Parameters.Remove "PM_MARGEM"
    vBanco.Parameters.Add "PM_MARGEM", strMG_Lucro, 1
    
    vBanco.Parameters.Remove "PM_ACT"
    vBanco.Parameters.Add "PM_ACT", "D", 1
    
   
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
      
    vSql = "PRODUCAO.PCK_CAD510.pr_ACT_CLASS6(:PM_CLASS,:PM_UFORIG,:PM_UFDEST,:PM_ACT,:PM_MARGEM,:PM_CODERRO,:PM_TXTERRO)"
     
    vErro = vVB_Generica_001.ExecutaPL(vBanco, vSql)
        
    vErro = IIf(vBanco.Parameters("PM_CODERRO") = "", 0, vBanco.Parameters("PM_CODERRO"))
     
    If vErro <> 0 Then
        MsgBox vBanco.Parameters("PM_TXTERRO") & ".Ligue para o Depto de Sistemas"
        Screen.MousePointer = 0
        Exit Sub
    End If
     
            
    MsgBox "Exclus�o OK ", vbInformation, "Aten��o"
    Call cmd4_Click
    Screen.MousePointer = 0

End Sub

Private Sub txtClass_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Numero(KeyAscii)

End Sub

Private Sub txtMg_Lucro_KeyPress(KeyAscii As Integer)
   KeyAscii = Valor(KeyAscii, txtMg_Lucro.Text)
End Sub

Private Sub txtMg_Lucro_LostFocus()
    txtMg_Lucro = FmtBR(txtMg_Lucro)
    Celula = txtMg_Lucro.Text
    ErroC = 0
    If Celula <> "" Then
        PosVal = InStr(1, Celula, ".")
        If PosVal > 3 Then
            ErroC = 1
        ElseIf (Len(Celula) - PosVal) > 2 Then
            ErroC = 1
        End If
    End If
    If ErroC = 1 Then
        MsgBox "Valor Incorreto", vbInformation, "Aten��o"
        txtMg_Lucro.SetFocus
    End If

End Sub

Sub PlanilhaExcel(ByVal Imprimir As Boolean)

    On Error GoTo TrataErro
    
    Dim X As Integer, y As Integer, Z As Integer
    
    Dim EApp As Excel.Application
    Dim EwkB As Excel.Workbook
    Dim EwkS As Excel.Worksheet
    
    Screen.MousePointer = 11
    DoEvents
    
    On Error Resume Next
    
    Set EApp = CreateObject("Excel.Application")
    Set EwkB = EApp.Workbooks.Add
    Set EwkS = EwkB.Sheets(1)
    
    ' Esconde o Excel
    'EApp.Application.Visible = True

    ' Configura Margens da P�gina
    'EApp.ActiveSheet.PageSetup.PrintArea = ""
    
    With EApp.ActiveSheet.PageSetup
        '.LeftHeader = "INDICE DE INFLA��O DE COMPRA"
        '.CenterHeader = cmbFornecedor.Text
        '.RightHeader = "&D"
        '.LeftFooter = "Comercial Automotiva Ltda. Confidencial"
        '.CenterFooter = "Depto. Controladoria"
        .RightFooter = "P�gina &P/&N"
        .LeftMargin = 10 ''Application.InchesToPoints(0.393700787401575)
        .RightMargin = 10 ''Application.InchesToPoints(0.393700787401575)
        .TopMargin = 10 ''Application.InchesToPoints(0.393700787401575)
        .BottomMargin = 10 ''Application.InchesToPoints(0.393700787401575)
        '.HeaderMargin = Application.InchesToPoints(0)
        '.FooterMargin = Application.InchesToPoints(0)
        .PrintHeadings = False
        .PrintGridlines = False
        .PrintComments = xlPrintNoComments
        .PrintQuality = 600
        .CenterHorizontally = False
        .CenterVertically = False
        '.Orientation = xlPortrait
        .Draft = False
        .PaperSize = xlPaperA4
        .FirstPageNumber = xlAutomatic
        .Order = xlDownThenOver
        .BlackAndWhite = False
        '.Zoom = 65
    End With
    
'    For y = 0 To Col
'        EApp.Cells(Z, y + 1).Value = vObjOracle.Fields(y).Name
'    Next

    With mfgCons
        Z = 1
        While Z < .Rows
            For y = 1 To 16
                EApp.Cells(Z, y).Value = .TextMatrix(Z, y)
            Next
            Z = Z + 1
        Wend
    End With
    
    
    If Not Imprimir Then
        ' Exibe Excel
        EApp.Application.Visible = True
    Else
        EApp.ActiveWindow.SelectedSheets.PrintOut Copies:=1
        EApp.ActiveWorkbook.Close (False)
        EApp.Quit
    End If
    
    ' Volta Cursor do Mouse
    Screen.MousePointer = 1
    
    ' Desliga o Excel do Programa
    Set EwkS = Nothing
    Set EwkB = Nothing
    Set EApp = Nothing
    
    Exit Sub
    
TrataErro:
    
       Screen.MousePointer = 1
       MsgBox "Ocorreu erro: " & Err.Number & " - " & Err.Description, 16
       Set EwkS = Nothing
       Set EwkB = Nothing
       Set EApp = Nothing

End Sub
