VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmConv52 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "CLASSIFICA��O CONV�NIO 52"
   ClientHeight    =   6705
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5820
   ControlBox      =   0   'False
   Icon            =   "frmConv52.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   447
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   388
   Begin VB.TextBox txtClass 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1755
      MaxLength       =   10
      TabIndex        =   3
      Top             =   1080
      Width           =   1230
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   6375
      Width           =   5820
      _ExtentX        =   10266
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   10213
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      Top             =   810
      Width           =   5730
      _ExtentX        =   10107
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConv52.frx":23D2
      PICN            =   "frmConv52.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd1 
      Height          =   690
      Left            =   855
      TabIndex        =   4
      TabStop         =   0   'False
      ToolTipText     =   "Gravar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConv52.frx":30C8
      PICN            =   "frmConv52.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSFlexGridLib.MSFlexGrid mfgCons 
      Height          =   4245
      Left            =   90
      TabIndex        =   7
      Top             =   1635
      Width           =   5655
      _ExtentX        =   9975
      _ExtentY        =   7488
      _Version        =   393216
      BackColorBkg    =   -2147483633
      AllowUserResizing=   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin Bot�o.cmd cmd6 
      Height          =   690
      Left            =   1620
      TabIndex        =   8
      TabStop         =   0   'False
      ToolTipText     =   "Importar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConv52.frx":3DBE
      PICN            =   "frmConv52.frx":3DDA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd2 
      Height          =   690
      Left            =   5040
      TabIndex        =   9
      TabStop         =   0   'False
      ToolTipText     =   "Fornecedores"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConv52.frx":4AB4
      PICN            =   "frmConv52.frx":4AD0
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label4 
      Alignment       =   2  'Center
      Caption         =   "Duplo click para excluir a classifica��o"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   120
      TabIndex        =   6
      Top             =   5985
      Width           =   5595
   End
   Begin VB.Label lbl 
      Appearance      =   0  'Flat
      Caption         =   "Classifica��o Fiscal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   90
      TabIndex        =   5
      Top             =   1125
      Width           =   1815
   End
End
Attribute VB_Name = "frmConv52"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmd1_Click()

On Error GoTo TrataErro

     Screen.MousePointer = 11
     If txtClass = "" Then
        MsgBox "Entre com a classifica��o", vbExclamation, "Aten��o"
        Screen.MousePointer = 0
        Exit Sub
     End If
     
      strMsg = "CONFIRMA A GRAVA��O DESTA"
      strMsg = strMsg & " CLASSIFICA��O PARA O CONV�NIO 52 ? "
      strResposta = MsgBox(strMsg, vbYesNo, "ATEN��O")
      If strResposta = vbYes Then
      
        vBanco.Parameters.Remove "PM_CLASS"
        vBanco.Parameters.Add "PM_CLASS", txtClass.Text, 1
        
        vBanco.Parameters.Remove "PM_ACT"
        vBanco.Parameters.Add "PM_ACT", "I", 1
     
        vBanco.Parameters.Remove "PM_CODERRO"
        vBanco.Parameters.Add "PM_CODERRO", 0, 2
        vBanco.Parameters.Remove "PM_TXTERRO"
        vBanco.Parameters.Add "PM_TXTERRO", "", 2
      
        vSql = "PRODUCAO.PCK_CAD510.PR_ACT_CONV52(:PM_CLASS,:PM_ACT,:PM_CODERRO,:PM_TXTERRO)"
     
        vErro = vVB_Generica_001.ExecutaPL(vBanco, vSql)
        
        vErro = IIf(vBanco.Parameters("PM_CODERRO") = "", 0, vBanco.Parameters("PM_CODERRO"))
     
       If vErro <> 0 Then
          MsgBox vBanco.Parameters("PM_TXTERRO") & ".Ligue para o Depto de Sistemas"
          Screen.MousePointer = 0
          Exit Sub
        End If
     End If
            
       MsgBox "Inclus�o OK ", vbInformation, "Aten��o"
       
       Call Form_Load
       
       Screen.MousePointer = 0
Exit Sub

TrataErro:
  'Call Process_Line_Errors(SQL)
End Sub
Private Sub cmd1_MouseOut()
    stbBarra.Panels(1).Text = ""
End Sub

Private Sub cmd1_MouseOver()
    stbBarra.Panels(1).Text = "Grava Antecipa��o Tributaria"
End Sub

Private Sub cmd2_Click()
    frmFormec.Show 1
End Sub

Private Sub cmd6_Click()
    frmImportConv52.Show 1
End Sub
Private Sub cmd6_MouseOut()
    stbBarra.Panels(1).Text = ""
End Sub
Private Sub cmd6_MouseOver()
    stbBarra.Panels(1).Text = "Importar Antecipa��o de Arquivo"
End Sub


Private Sub cmdVoltar_Click()
    Unload Me
End Sub
Public Sub Form_Load()
    
    Me.Top = 0
    Me.Left = 0
   
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").serverType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 17, 20, 2000, 0
   
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "PRODUCAO.PCK_CAD510.PR_CON_CONV52(:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
    
    vErro = vVB_Generica_001.ExecutaPL(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
    
    If Not vObjOracle.EOF Then
        vVB_Generica_001.CarregaGridTabela mfgCons, vObjOracle, 2
    End If
    
End Sub


Private Sub mfgCons_DblClick()

    Dim strClass As String
    Dim StrOrigem As String
    Dim strDestino As String
    Dim strMG_Lucro As String

    mfgCons.col = 1
    strClass = mfgCons.Text

    vBanco.Parameters.Remove "PM_CLASS"
    vBanco.Parameters.Add "PM_CLASS", strClass, 1
    
    vBanco.Parameters.Remove "PM_ACT"
    vBanco.Parameters.Add "PM_ACT", "D", 1
    
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
      
    vSql = "PRODUCAO.PCK_CAD510.PR_ACT_CONV52(:PM_CLASS,:PM_ACT,:PM_CODERRO,:PM_TXTERRO)"
     
    vErro = vVB_Generica_001.ExecutaPL(vBanco, vSql)
        
    vErro = IIf(vBanco.Parameters("PM_CODERRO") = "", 0, vBanco.Parameters("PM_CODERRO"))
     
    If vErro <> 0 Then
        MsgBox vBanco.Parameters("PM_TXTERRO") & ".Ligue para o Depto de Sistemas"
        Screen.MousePointer = 0
        Exit Sub
    End If
            
    MsgBox "Exclus�o OK ", vbInformation, "Aten��o"
    Call Form_Load
    Screen.MousePointer = 0

End Sub

Private Sub txtClass_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Numero(KeyAscii)
End Sub
