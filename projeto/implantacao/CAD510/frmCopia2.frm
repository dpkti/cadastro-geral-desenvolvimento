VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmCopia2 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Copia todas as Classifica��es de uma UF de origem para outra"
   ClientHeight    =   2415
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5610
   ControlBox      =   0   'False
   Icon            =   "frmCopia2.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   161
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   374
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fra 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1320
      Left            =   90
      TabIndex        =   2
      Top             =   945
      Width           =   5370
      Begin VB.ComboBox cboUF_Destino 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1350
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   810
         Width           =   1905
      End
      Begin VB.ComboBox cboUF_Origem 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1350
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   315
         Width           =   1905
      End
      Begin Bot�o.cmd cmd1 
         Height          =   420
         Left            =   4005
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   540
         Width           =   960
         _ExtentX        =   1693
         _ExtentY        =   741
         BTYPE           =   3
         TX              =   "OK"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCopia2.frx":000C
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         Caption         =   "UF Destino"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   225
         TabIndex        =   5
         Top             =   855
         Width           =   1590
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         Caption         =   "UF Original"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   225
         TabIndex        =   4
         Top             =   360
         Width           =   1590
      End
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   0
      Top             =   810
      Width           =   5460
      _ExtentX        =   9631
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCopia2.frx":0028
      PICN            =   "frmCopia2.frx":0044
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmCopia2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmd1_Click()

    Dim resposta As String
    Dim MSG As String
  
    On Error GoTo TrataErro
  
    If cboUF_Origem = "" Or cboUF_Destino = "" Then
        MsgBox "Para fazer a c�pia � necess�rio entrar com a UF Origem e UF Destino", vbExclamation, "ATEN��O"
        Exit Sub
    End If

    If Mid(cboUF_Origem, 1, 2) = Mid(cboUF_Destino, 1, 2) Then
        MsgBox "N�o � poss�vel copiar uma UF para ela mesma", vbExclamation, "ATEN��O"
        Exit Sub
    End If
    Screen.MousePointer = 11
     
    vBanco.Parameters.Remove "PM_UFORIG"
    vBanco.Parameters.Add "PM_UFORIG", Mid(cboUF_Origem, 1, 2), 1
    vBanco.Parameters.Remove "PM_UFDEST"
    vBanco.Parameters.Add "PM_UFDEST", Mid(cboUF_Destino, 1, 2), 1
   
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "PRODUCAO.PCK_CAD510.pr_COP_CLASS_UF(:PM_UFORIG,:PM_UFDEST,:PM_CODERRO,:PM_TXTERRO)"
          
    
    MSG = "TEM CERTEZA QUE DESEJA COPIAR AS CLASSIFICA��ES DA UF: " & Mid(cboUF_Origem, 1, 2)
    MSG = MSG & " PARA A UF: " & Mid(cboUF_Destino, 1, 2) & " ?????"
    resposta = MsgBox(MSG, vbYesNo, "ATEN��O")
     
    If resposta = vbYes Then
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    Else
        Screen.MousePointer = 0
        Exit Sub
    End If
      
    vErro = IIf(vBanco.Parameters("PM_CODERRO") = "", 0, vBanco.Parameters("PM_CODERRO"))
    If vErro <> 0 Then
        Screen.MousePointer = 0
        If vErro Like "*ORA-00001*" Then
            SQL = "N�o foi poss�vel fazer a c�pia, pois a classifica��o "
            SQL = SQL & " destino j� esta cadastrada. Consulte-a"
            MsgBox SQL, vbExclamation, "Aten��o"
            Exit Sub
        End If
        MsgBox vBanco.Parameters("PM_TXTERRO") & ".Ligue para o Depto de Sistemas"
        Exit Sub
    End If

    'MsgBox "C�pia OK ", vbInformation, "Aten��o"
    'Unload Me

    MsgBox "C�pia completada com sucesso", vbInformation, "ATEN��O"
    Screen.MousePointer = 0
    Exit Sub
     
TrataErro:
   If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
     Resume
   Else
    ' Call Process_Line_Errors(SQL)
   End If

End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    
    Me.Top = 0
    Me.Left = 0
    
    Set vObjOracle = vVB_Generica_001.TabelaUF(vBanco)
    
    Call vVB_Generica_001.PreencheComboList(vObjOracle, frmCopia2.cboUF_Origem, "UF", "DESC. UF")
    
    Call vVB_Generica_001.PreencheComboList(vObjOracle, frmCopia2.cboUF_Destino, "UF", "DESC. UF")
    
End Sub
