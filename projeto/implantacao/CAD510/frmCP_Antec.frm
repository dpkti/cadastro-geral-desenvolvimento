VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form frmCP_Antec 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Copia Antecipa��o Tribut�ria"
   ClientHeight    =   4260
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6375
   ControlBox      =   0   'False
   Icon            =   "frmCP_Antec.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   284
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   425
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Height          =   2910
      Left            =   45
      TabIndex        =   2
      Top             =   945
      Width           =   6270
      Begin VB.TextBox txtMg_Lucro 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2040
         MaxLength       =   5
         TabIndex        =   13
         Top             =   2280
         Width           =   855
      End
      Begin VB.ComboBox cboOrigemCopia 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   2025
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   1755
         Width           =   2400
      End
      Begin VB.TextBox txtClass 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2025
         TabIndex        =   5
         Top             =   315
         Width           =   1230
      End
      Begin VB.ComboBox cboOrigem 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   2025
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   765
         Width           =   1725
      End
      Begin VB.ComboBox cboDestino 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   2025
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   1260
         Width           =   1725
      End
      Begin Bot�o.cmd cmd1 
         Height          =   420
         Left            =   4800
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   1620
         Width           =   960
         _ExtentX        =   1693
         _ExtentY        =   741
         BTYPE           =   3
         TX              =   "OK"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCP_Antec.frx":000C
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label Label8 
         Appearance      =   0  'Flat
         Caption         =   "Mg Lucro"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   240
         TabIndex        =   12
         Top             =   2280
         Width           =   975
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         Caption         =   "UF Origem para C�pia"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   135
         TabIndex        =   10
         Top             =   1800
         Width           =   1815
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         Caption         =   "Classifica��o Fiscal"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   135
         TabIndex        =   8
         Top             =   360
         Width           =   1815
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         Caption         =   "UF Origem"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   135
         TabIndex        =   7
         Top             =   810
         Width           =   1815
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         Caption         =   "UF Destino"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   135
         TabIndex        =   6
         Top             =   1305
         Width           =   1815
      End
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   0
      Top             =   810
      Width           =   6270
      _ExtentX        =   11060
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCP_Antec.frx":0028
      PICN            =   "frmCP_Antec.frx":0044
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmCP_Antec"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmd1_Click()


    Screen.MousePointer = 11
    If txtClass = "" Or cboOrigem = "" Or cboDestino = "" Or cboOrigemCopia = "" Or txtMg_Lucro = "" Then
        MsgBox "Para fazer a c�pia � preciso preencher todos os campos", vbExclamation, "Aten��o"
        Screen.MousePointer = 0
        Exit Sub
    End If
    
    
    vBanco.Parameters.Remove "PM_CLASS"
    vBanco.Parameters.Add "PM_CLASS", txtClass, 1
    vBanco.Parameters.Remove "PM_UFORIG"
    vBanco.Parameters.Add "PM_UFORIG", Mid(Trim(cboOrigem), 1, 2), 1
    vBanco.Parameters.Remove "PM_UFDEST"
    vBanco.Parameters.Add "PM_UFDEST", Mid(Trim(cboDestino), 1, 2), 1
   
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").serverType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    'Leandro -29/06/09
    'vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 17, 20, 2000, 0
     vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 17, 20, 2000, 0
    
   
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "PRODUCAO.PCK_CAD510.pr_CON_COUNT_CLASS(:PM_CLASS,:PM_UFDEST,:PM_UFORIG,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
    
    vErro = vVB_Generica_001.ExecutaPL(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
    
  
    If vObjOracle("TOTAL") = 0 Then
        MsgBox "N�o h� classifica��o cadastrada para esta UF ORIGEM e UF DESTINO", vbCritical, "ATEN��O"
        Screen.MousePointer = 0
        Exit Sub
    End If
  
    strMsg = "Existe(m) " & vObjOracle("TOTAL") & " registro(s) para esta "
    strMsg = strMsg & " classificacao, tem certeza que deseja copiar "
    If txtClass = "" Then
        strMsg = strMsg & " todas as classifica��es da UF destino " & Mid(Trim(cboDestino), 1, 2)
        strMsg = strMsg & " e UF origem " & Mid(Trim(cboOrigem), 1, 2)
        strMsg = strMsg & " para a UF origem " & cboOrigemCopia & "?"
    Else
        strMsg = strMsg & " a classifica��o " & txtClass
        strMsg = strMsg & "  da UF destino " & Mid(Trim(cboDestino), 1, 2)
        strMsg = strMsg & " e UF origem " & Mid(Trim(cboOrigem), 1, 2)
        strMsg = strMsg & " para a UF origem " & cboOrigemCopia & "?"
 
    End If
  
    strResposta = MsgBox(strMsg, vbYesNo + vbDefaultButton2, "Aten��o")
  
    If strResposta = vbNo Then
        Exit Sub
    Else
        If txtClass <> "" Then
            Call Copia_Class(txtClass, Mid(Trim(cboDestino), 1, 2), Mid(Trim(cboOrigem), 1, 2), _
                    Mid(Trim(cboOrigemCopia), 1, 1))
        Else
            Call Copia_UF(Mid(Trim(cboDestino), 1, 2), Mid(Trim(cboOrigem), 1, 2), _
                    Mid(Trim(cboOrigemCopia), 1, 1))
        End If
    End If
  
    txtClass.Text = ""
    cboDestino.ListIndex = -1
    cboOrigem.ListIndex = -1
    cboOrigemCopia.ListIndex = -1
   ' MsgBox "C�pia realizada, favor conferir se a MARGEM DE LUCRO est� OK.", vbCritical, "ATEN��O"
      

End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    
    Me.Top = 0
    Me.Left = 0
    
    Set vObjOracle = vVB_Generica_001.TabelaUF(vBanco)
    
    Call vVB_Generica_001.PreencheComboList(vObjOracle, cboOrigem, "UF", "DESC. UF")
    cboOrigem.AddItem "SU - SUFRAMA"
    
    Call vVB_Generica_001.PreencheComboList(vObjOracle, cboDestino, "UF", "DESC. UF")
    cboDestino.AddItem "SU - SUFRAMA"
    
    'preenche combo uf origem
    If cboOrigemCopia.ListCount = 0 Then
        cboOrigemCopia.AddItem "1-TODAS AS UFs"
        cboOrigemCopia.AddItem "2-SOMENTE UFs de CDs"
    End If
    
End Sub


Public Sub Copia_Class(Class As String, uf_dest As String, _
                       uf_orig As String, uf_copia As Integer)
Dim ss As Object
Dim i As Long
Dim strUF_Origem As String

    Screen.MousePointer = 11
    If uf_copia = 1 Then
  
        Set vObjOracle = vVB_Generica_001.TabelaUF(vBanco)
      
        For i = 1 To vObjOracle.RecordCount
        
            strUF_Origem = vObjOracle!UF
  
            If strUF_Origem <> uf_orig Then
            
                vBanco.Parameters.Remove "PM_CLASS"
                vBanco.Parameters.Add "PM_CLASS", Class, 1
                vBanco.Parameters.Remove "PM_UFORIG"
                vBanco.Parameters.Add "PM_UFORIG", strUF_Origem, 1
                vBanco.Parameters.Remove "PM_UFDEST"
                vBanco.Parameters.Add "PM_UFDEST", uf_dest, 1
                vBanco.Parameters.Remove "PM_MARGEM"
                vBanco.Parameters.Add "PM_MARGEM", txtMg_Lucro, 1
    
                vBanco.Parameters.Remove "PM_ACT"
                vBanco.Parameters.Add "PM_ACT", "D", 1
    
                vBanco.Parameters.Remove "PM_CODERRO"
                vBanco.Parameters.Add "PM_CODERRO", 0, 2
                vBanco.Parameters.Remove "PM_TXTERRO"
                vBanco.Parameters.Add "PM_TXTERRO", "", 2
      
                vSql = "PRODUCAO.PCK_CAD510.pr_ACT_CLASS6(:PM_CLASS,:PM_UFORIG,:PM_UFDEST,:PM_ACT,:PM_MARGEM,:PM_CODERRO,:PM_TXTERRO)"
     
                vErro = vVB_Generica_001.ExecutaPL(vBanco, vSql)
        
                vErro = IIf(vBanco.Parameters("PM_CODERRO") = "", 0, vBanco.Parameters("PM_CODERRO"))
     
                If vErro <> 0 Then
                    MsgBox vBanco.Parameters("PM_TXTERRO") & ".Ligue para o Depto de Sistemas"
                    Screen.MousePointer = 0
                    Exit Sub
                End If
            
            
                vBanco.Parameters.Remove "PM_ACT"
                vBanco.Parameters.Add "PM_ACT", "I", 1
            
                vSql = "PRODUCAO.PCK_CAD510.pr_ACT_CLASS6(:PM_CLASS,:PM_UFORIG,:PM_UFDEST,:PM_ACT,:PM_MARGEM,:PM_CODERRO,:PM_TXTERRO)"
     
                vErro = vVB_Generica_001.ExecutaPL(vBanco, vSql)
        
                vErro = IIf(vBanco.Parameters("PM_CODERRO") = "", 0, vBanco.Parameters("PM_CODERRO"))
     
                If vErro <> 0 Then
                    MsgBox vBanco.Parameters("PM_TXTERRO") & ".Ligue para o Depto de Sistemas"
                    Screen.MousePointer = 0
                    Exit Sub
                End If
                
            End If
            vObjOracle.MoveNext
        Next
     
    ElseIf uf_copia = 2 Then
    
        vBanco.Parameters.Remove "PM_CURSOR1"
        vBanco.Parameters.Add "PM_CURSOR1", 0, 3
        vBanco.Parameters("PM_CURSOR1").serverType = 102
        vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
        'Leandro - 29/06/09
        'vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 17, 20, 2000, 0
         vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 17, 20, 2000, 0
    
        vBanco.Parameters.Remove "PM_CODERRO"
        vBanco.Parameters.Add "PM_CODERRO", 0, 2
        vBanco.Parameters.Remove "PM_TXTERRO"
        vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
        vSql = "PRODUCAO.PCK_CAD510.pr_CON_LOJASUF(:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
    
        vErro = vVB_Generica_001.ExecutaPL(vBanco, vSql)
    
        Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
      
        For i = 1 To vObjOracle.RecordCount
        
            strUF_Origem = vObjOracle!cod_uf
        
            If strUF_Origem <> uf_orig Then
            
                vBanco.Parameters.Remove "PM_CLASS"
                vBanco.Parameters.Add "PM_CLASS", Class, 1
                vBanco.Parameters.Remove "PM_UFORIG"
                vBanco.Parameters.Add "PM_UFORIG", strUF_Origem, 1
                vBanco.Parameters.Remove "PM_UFDEST"
                vBanco.Parameters.Add "PM_UFDEST", uf_dest, 1
                vBanco.Parameters.Remove "PM_MARGEM"
                vBanco.Parameters.Add "PM_MARGEM", txtMg_Lucro, 1
    
    
                vBanco.Parameters.Remove "PM_ACT"
                vBanco.Parameters.Add "PM_ACT", "D", 1
    
                vBanco.Parameters.Remove "PM_CODERRO"
                vBanco.Parameters.Add "PM_CODERRO", 0, 2
                vBanco.Parameters.Remove "PM_TXTERRO"
                vBanco.Parameters.Add "PM_TXTERRO", "", 2
      
                vSql = "PRODUCAO.PCK_CAD510.pr_act_CLASS6(:PM_CLASS,:PM_UFORIG,:PM_UFDEST,:PM_ACT,:PM_MARGEM,:PM_CODERRO,:PM_TXTERRO)"
     
                vErro = vVB_Generica_001.ExecutaPL(vBanco, vSql)
        
                vErro = IIf(vBanco.Parameters("PM_CODERRO") = "", 0, vBanco.Parameters("PM_CODERRO"))
     
                If vErro <> 0 Then
                    MsgBox vBanco.Parameters("PM_TXTERRO") & ".Ligue para o Depto de Sistemas"
                    Screen.MousePointer = 0
                    Exit Sub
                End If
            
            
                vBanco.Parameters.Remove "PM_ACT"
                vBanco.Parameters.Add "PM_ACT", "I", 1
            
                vSql = "PRODUCAO.PCK_CAD510.pr_act_CLASS6(:PM_CLASS,:PM_UFORIG,:PM_UFDEST,:PM_ACT,0,:PM_CODERRO,:PM_TXTERRO)"
     
                vErro = vVB_Generica_001.ExecutaPL(vBanco, vSql)
        
                vErro = IIf(vBanco.Parameters("PM_CODERRO") = "", 0, vBanco.Parameters("PM_CODERRO"))
     
                If vErro <> 0 Then
                    MsgBox vBanco.Parameters("PM_TXTERRO") & ".Ligue para o Depto de Sistemas"
                    Screen.MousePointer = 0
                    Exit Sub
                End If
       End If
         vObjOracle.MoveNext
     Next
  End If
  MsgBox "Atualiza��o realizada com sucesso, FAVOR VERIFICAR A MARGEM DE LUCRO.", vbInformation, "Aten��o"
   Screen.MousePointer = 0
End Sub
Public Sub Copia_UF(uf_dest As String, _
                       uf_orig As String, uf_copia As Integer)
  Dim ss As Object
  Dim i As Long
  Dim strUF_Origem As String

    Screen.MousePointer = 11
    If uf_copia = 1 Then
  
        Set vObjOracle = vVB_Generica_001.TabelaUF(vBanco)
      
        For i = 1 To vObjOracle.RecordCount
            strUF_Origem = vObjOracle!cod_uf
  
            If strUF_Origem <> uf_orig Then
            
                vBanco.Parameters.Remove "PM_CLASS"
                vBanco.Parameters.Add "PM_CLASS", Class, 1
                vBanco.Parameters.Remove "PM_UFORIG"
                vBanco.Parameters.Add "PM_UFORIG", strUF_Origem, 1
                vBanco.Parameters.Remove "PM_UFORIGANT"
                vBanco.Parameters.Add "PM_UFORIGANT", uf_orig, 1
                vBanco.Parameters.Remove "PM_UFDEST"
                vBanco.Parameters.Add "PM_UFDEST", uf_dest, 1
                vBanco.Parameters.Remove "PM_MARGEM"
                vBanco.Parameters.Add "PM_MARGEM", txtMg_Lucro, 1
    
    
                vBanco.Parameters.Remove "PM_CODERRO"
                vBanco.Parameters.Add "PM_CODERRO", 0, 2
                vBanco.Parameters.Remove "PM_TXTERRO"
                vBanco.Parameters.Add "PM_TXTERRO", "", 2
      
                vSql = "PRODUCAO.PCK_CAD510.pr_act_CLASS6(:PM_CLASS,:PM_UFORIG,:PM_UFORIGANT,:PM_UFDEST,:PM_ACT,:PM_MARGEM,:PM_CODERRO,:PM_TXTERRO)"
     
                vErro = vVB_Generica_001.ExecutaPL(vBanco, vSql)
        
                vErro = IIf(vBanco.Parameters("PM_CODERRO") = "", 0, vBanco.Parameters("PM_CODERRO"))
     
                If vErro <> 0 Then
                    MsgBox vBanco.Parameters("PM_TXTERRO") & ".Ligue para o Depto de Sistemas"
                    Screen.MousePointer = 0
                    Exit Sub
                End If
        
            End If
            vObjOracle.MoveNext
        Next
     
    ElseIf uf_copia = 2 Then
    
        vBanco.Parameters.Remove "PM_CURSOR1"
        vBanco.Parameters.Add "PM_CURSOR1", 0, 3
        vBanco.Parameters("PM_CURSOR1").serverType = 102
        vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
        vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 17, 20, 2000, 0
    
        vBanco.Parameters.Remove "PM_CODERRO"
        vBanco.Parameters.Add "PM_CODERRO", 0, 2
        vBanco.Parameters.Remove "PM_TXTERRO"
        vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
        vSql = "PRODUCAO.PCK_CAD510.pr_CON_LOJASUF(:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
    
        vErro = vVB_Generica_001.ExecutaPL(vBanco, vSql)
    
        Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
   
        For i = 1 To vObjOracle.RecordCount
            strUF_Origem = vObjOracle!cod_uf
        
            If strUF_Origem <> uf_orig Then
            
            
                vBanco.Parameters.Remove "PM_CLASS"
                vBanco.Parameters.Add "PM_CLASS", Class, 1
                vBanco.Parameters.Remove "PM_UFORIG"
                vBanco.Parameters.Add "PM_UFORIG", strUF_Origem, 1
                vBanco.Parameters.Remove "PM_UFORIGANT"
                vBanco.Parameters.Add "PM_UFORIGANT", uf_orig, 1
                vBanco.Parameters.Remove "PM_UFDEST"
                vBanco.Parameters.Add "PM_UFDEST", uf_dest, 1
                  vBanco.Parameters.Remove "PM_MARGEM"
                vBanco.Parameters.Add "PM_MARGEM", txtMg_Lucro, 1
    
    
                vBanco.Parameters.Remove "PM_CODERRO"
                vBanco.Parameters.Add "PM_CODERRO", 0, 2
                vBanco.Parameters.Remove "PM_TXTERRO"
                vBanco.Parameters.Add "PM_TXTERRO", "", 2
      
                vSql = "PRODUCAO.PCK_CAD510.pr_act_CLASS6(:PM_CLASS,:PM_UFORIG,:PM_UFORIGANT,:PM_UFDEST,:PM_ACT,:PM_MARGEM,:PM_CODERRO,:PM_TXTERRO)"
     
                vErro = vVB_Generica_001.ExecutaPL(vBanco, vSql)
        
                vErro = IIf(vBanco.Parameters("PM_CODERRO") = "", 0, vBanco.Parameters("PM_CODERRO"))
     
                If vErro <> 0 Then
                    MsgBox vBanco.Parameters("PM_TXTERRO") & ".Ligue para o Depto de Sistemas"
                    Screen.MousePointer = 0
                    Exit Sub
                End If
        
            vObjOracle.MoveNext
       End If
     Next
 End If
  MsgBox "Atualiza��o realizada com sucesso", vbInformation, "Aten��o"
   Screen.MousePointer = 0
End Sub


Private Sub txtClass_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Numero(KeyAscii)
End Sub

Private Sub txtMg_Lucro_KeyPress(KeyAscii As Integer)
   KeyAscii = Valor(KeyAscii, txtMg_Lucro.Text)
End Sub

Private Sub txtMg_Lucro_LostFocus()
 txtMg_Lucro = FmtBR(txtMg_Lucro)
    Celula = txtMg_Lucro.Text
    ErroC = 0
    If Celula <> "" Then
        PosVal = InStr(1, Celula, ".")
        If PosVal > 3 Then
            ErroC = 1
        ElseIf (Len(Celula) - PosVal) > 2 Then
            ErroC = 1
        End If
    End If
    If ErroC = 1 Then
        MsgBox "Valor Incorreto", vbInformation, "Aten��o"
        txtMg_Lucro.SetFocus
    End If
End Sub
