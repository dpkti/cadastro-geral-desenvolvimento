--- PACKAGE DO SISTEMA VDA510
--- DATA: 03/04/2004
--- ANALISTA: FABIO GUIDOTTI
--- dir: F:\SISTEMAS\ORACLE\CAD510\VB\DESENV\PACKAGE\PCK_CAD510.SQL

CREATE OR REPLACE Package PRODUCAO.PCK_CAD510 is

	Type tp_Cursor is Ref Cursor;


	PROCEDURE pr_COP_CLASS_UF( PM_UFORIG  IN VARCHAR2,
							   PM_UFDEST  IN VARCHAR2,
			  				   PM_CODERRO OUT NUMBER,
			  				   PM_TXTERRO OUT VARCHAR2);


	PROCEDURE pr_COP_CLASS( PM_CLASSORIG IN NUMBER,
							PM_CLASSDEST IN NUMBER,
			  				PM_CODERRO   OUT NUMBER,
			  				PM_TXTERRO   OUT VARCHAR2);


	PROCEDURE pr_CON_CLASS( PM_CLASS   IN NUMBER,
							PM_UFORIG  IN VARCHAR2,
							PM_OPCAO   IN NUMBER,
							PM_CURSOR1 IN OUT TP_CURSOR,
			  				PM_CODERRO OUT NUMBER,
			  				PM_TXTERRO OUT VARCHAR2);


	PROCEDURE pr_INC_CLASS( PM_CLASS    IN NUMBER,
							PM_UFORIG   IN VARCHAR2,
							PM_UFDEST   IN VARCHAR2,
							PM_TRIBREV  IN NUMBER,
							PM_TRIBINS  IN NUMBER,
							PM_TRIBISE  IN NUMBER,
							PM_TRIBTARE IN NUMBER,
							PM_FATREV   IN NUMBER,
							PM_FATINS   IN NUMBER,
							PM_FATISE   IN NUMBER,
							PM_FATTARE  IN NUMBER,
							PM_CRED     IN VARCHAR2,
							PM_MARG     IN NUMBER,
							PM_TPREL     IN VARCHAR2,
			  				PM_CODERRO  OUT NUMBER,
			  				PM_TXTERRO  OUT VARCHAR2);


	PROCEDURE pr_ACT_CLASS6( PM_CLASS   IN NUMBER,
							 PM_UFORIG  IN VARCHAR2,
							 PM_UFDEST  IN VARCHAR2,
							 PM_ACT     IN CHAR,
							 PM_MARGEM  IN  NUMBER,
				  				 PM_CODERRO OUT NUMBER,
				  				 PM_TXTERRO OUT VARCHAR2);


	PROCEDURE pr_CON_CLASS_ANTEC( PM_UFDEST  IN VARCHAR2,
								  PM_CURSOR1 IN OUT TP_CURSOR,
								  PM_CODERRO OUT NUMBER,
								  PM_TXTERRO OUT VARCHAR2);


	PROCEDURE pr_CON_COUNT_CLASS( PM_CLASS   IN NUMBER,
								  PM_UFDEST  IN VARCHAR2,
								  PM_UFORIG  IN VARCHAR2,
								  PM_CURSOR1 IN OUT TP_CURSOR,
								  PM_CODERRO OUT NUMBER,
								  PM_TXTERRO OUT VARCHAR2);


	PROCEDURE pr_CON_LOJASUF( PM_CURSOR1 IN OUT TP_CURSOR,
							  PM_CODERRO OUT NUMBER,
							  PM_TXTERRO OUT VARCHAR2);


	PROCEDURE pr_ACT_CLASS6_II( PM_CLASS     IN NUMBER,
								PM_UFORIG    IN VARCHAR2,
								PM_UFORIGANT IN VARCHAR2,
								PM_UFDEST    IN VARCHAR2,
								PM_ACT       IN CHAR,
			  					PM_CODERRO   OUT NUMBER,
			  					PM_TXTERRO   OUT VARCHAR2);


	PROCEDURE pr_CON_CLASS_PIS( PM_CURSOR1 IN OUT TP_CURSOR,
								PM_CODERRO OUT NUMBER,
								PM_TXTERRO OUT VARCHAR2);
									

	PROCEDURE PR_ACT_CONV52( PM_CLASS    IN NUMBER,
							 PM_ACT      IN CHAR,
							 PM_CODERRO OUT NUMBER,
		  					 PM_TXTERRO OUT VARCHAR2);
			  					 
			  					 
	PROCEDURE PR_CON_CONV52( PM_CURSOR1 IN OUT TP_CURSOR,
							 PM_CODERRO OUT NUMBER,
							 PM_TXTERRO OUT VARCHAR2);
							 
							 
	PROCEDURE pr_CON_COMPARATIVO( PM_UFORIG  IN VARCHAR2,
								  PM_UFDEST  IN VARCHAR2,
								  PM_CURSOR1 IN OUT TP_CURSOR,
								  PM_CODERRO OUT NUMBER,
								  PM_TXTERRO OUT VARCHAR2);
									
	PROCEDURE PR_CON_FORNEC( PM_CURSOR1 IN OUT TP_CURSOR,
	  						 PM_CODERRO OUT NUMBER,
	  						 PM_TXTERRO OUT VARCHAR2);
	  							  
	PROCEDURE PR_CON_FORNEC_CONV( PM_CURSOR1 IN OUT TP_CURSOR,
	  							  PM_CODERRO OUT NUMBER,
	  							  PM_TXTERRO OUT VARCHAR2);
	  							  
	  							  
	PROCEDURE PR_ACT_FORNEC_CONV( PM_FORNEC  IN NUMBER,
								  PM_ACT     IN VARCHAR2,
	  							  PM_CODERRO OUT NUMBER,
	  							  PM_TXTERRO OUT VARCHAR2);

End PCK_CAD510;
---------------
/
CREATE OR REPLACE Package Body PRODUCAO.PCK_CAD510 is


	PROCEDURE pr_COP_CLASS_UF( PM_UFORIG  IN VARCHAR2,
							   PM_UFDEST  IN VARCHAR2,
			  				   PM_CODERRO OUT NUMBER,
			  				   PM_TXTERRO OUT VARCHAR2) AS
		BEGIN

		PM_CODERRO := 0;

		delete from compras.subst_tributaria
		where cod_uf_origem = PM_UFDEST;
		COMMIT;

		Insert into compras.subst_tributaria
		(Select class_fiscal,PM_UFDEST,
			cod_uf_destino,cod_trib_revendedor,
			cod_trib_inscrito,cod_trib_isento,
			fator_revendedor,fator_inscrito,
			fator_isento,fl_cred_suspenso,
			pc_margem_lucro,cod_trib_tare,fator_tare,
			TP_RELATORIO
		from compras.subst_tributaria
		Where cod_uf_origem = PM_UFORIG);
		COMMIT;


	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;
	end pr_COP_CLASS_UF;


	PROCEDURE pr_COP_CLASS( PM_CLASSORIG  IN NUMBER,
							PM_CLASSDEST  IN NUMBER,
			  				PM_CODERRO OUT NUMBER,
			  				PM_TXTERRO OUT VARCHAR2) AS
		BEGIN

		PM_CODERRO := 0;

		delete from compras.subst_tributaria
		where class_fiscal = PM_CLASSDEST;
		COMMIT;

		Insert into compras.subst_tributaria
		(Select PM_CLASSDEST,cod_uf_origem,
		cod_uf_destino,cod_trib_revendedor,
		cod_trib_inscrito,cod_trib_isento,
		fator_revendedor,fator_inscrito,
		fator_isento,fl_cred_suspenso,
		pc_margem_lucro,cod_trib_tare,fator_tare,TP_RELATORIO
		from compras.subst_tributaria
		Where class_fiscal = PM_CLASSORIG);
		COMMIT;


	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;

	end pr_COP_CLASS;


	PROCEDURE pr_CON_CLASS( PM_CLASS   IN NUMBER,
							PM_UFORIG  IN VARCHAR2,
							PM_OPCAO   IN NUMBER,
							PM_CURSOR1 IN OUT TP_CURSOR,
			  				PM_CODERRO OUT NUMBER,
			  				PM_TXTERRO OUT VARCHAR2) AS

		STRSQL varchar2(2000);

		BEGIN

		PM_CODERRO := 0;

		STRSQL := STRSQL || ' Select class_fiscal "CLASS. FISCAL",';
		STRSQL := STRSQL || ' cod_uf_origem "UF ORIGEM",';
		STRSQL := STRSQL || ' cod_uf_destino "UF DESTINO",';
		STRSQL := STRSQL || ' cod_trib_revendedor "TRIB. REVEND.",';
		STRSQL := STRSQL || ' cod_trib_inscrito "TRIB. INSCR.",';
		STRSQL := STRSQL || ' cod_trib_isento "TRIB. ISENTO",';
		STRSQL := STRSQL || ' cod_trib_tare "TRIB. TARE",';
		STRSQL := STRSQL || ' FATOR_REVENDEDOR "FAT. REVEND.",';
		STRSQL := STRSQL || ' fator_inscrito "FAT. INSCR.",';
		STRSQL := STRSQL || ' fator_isento "FAT. ISENTO",';
		STRSQL := STRSQL || ' fator_tare "FAT. TARE",';
		STRSQL := STRSQL || ' fl_cred_suspenso "CR�D. SUSPENSO",';
		STRSQL := STRSQL || ' pc_margem_lucro "% M. LUCRO",';
		STRSQL := STRSQL || ' TP_RELATORIO "TP.REL."';
		STRSQL := STRSQL || ' From compras.subst_tributaria';
		STRSQL := STRSQL || ' Where class_fiscal = ' || PM_CLASS || ' ';

		If PM_OPCAO = 2 Then
			STRSQL := STRSQL || ' and cod_uf_origem = ''' || PM_UFORIG || ''' ';
		End If;

		STRSQL := STRSQL || ' Order by class_fiscal,cod_uf_origem,';
		STRSQL := STRSQL || ' cod_uf_destino';

		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR
		STRSQL;

	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;

	end pr_CON_CLASS;


	PROCEDURE pr_INC_CLASS( PM_CLASS    IN NUMBER,
							PM_UFORIG   IN VARCHAR2,
							PM_UFDEST   IN VARCHAR2,
							PM_TRIBREV  IN NUMBER,
							PM_TRIBINS  IN NUMBER,
							PM_TRIBISE  IN NUMBER,
							PM_TRIBTARE IN NUMBER,
							PM_FATREV   IN NUMBER,
							PM_FATINS   IN NUMBER,
							PM_FATISE   IN NUMBER,
							PM_FATTARE  IN NUMBER,
							PM_CRED     IN VARCHAR2,
							PM_MARG     IN NUMBER,
							PM_TPREL     IN VARCHAR2,
			  				PM_CODERRO OUT NUMBER,
			  				PM_TXTERRO OUT VARCHAR2) AS

		BEGIN

		PM_CODERRO := 0;

		Update compras.subst_tributaria set
		cod_trib_revendedor = PM_TRIBREV,
		cod_trib_inscrito = PM_TRIBINS,
		cod_trib_isento = PM_TRIBISE,
		cod_trib_tare = PM_TRIBTARE,
		fator_revendedor = PM_FATREV,
		fator_inscrito = PM_FATINS,
		fator_isento = PM_FATISE,
		fator_tare = PM_FATTARE,
		fl_cred_suspenso = PM_CRED,
		pc_margem_lucro = PM_MARG,
		TP_RELATORIO = PM_TPREL
		where class_fiscal= PM_CLASS and
		cod_uf_origem = PM_UFORIG and
		cod_uf_destino = PM_UFDEST;
		--COMMIT;
		IF SQL%NOTFOUND THEN
			Insert into compras.subst_tributaria
			(class_fiscal,cod_uf_origem,cod_uf_destino,
			cod_trib_revendedor,cod_trib_inscrito,
			cod_trib_isento,fator_revendedor,
			fator_inscrito, fator_isento,fl_cred_suspenso,
			pc_margem_lucro,cod_trib_tare,fator_tare,TP_RELATORIO)
			values (PM_CLASS,PM_UFORIG,PM_UFDEST,
			PM_TRIBREV,PM_TRIBINS,PM_TRIBISE,
			PM_FATREV,PM_FATINS,PM_FATISE,PM_CRED,PM_MARG,PM_TRIBTARE,
			PM_FATTARE,PM_TPREL);
		--	COMMIT;
		END IF;
    	COMMIT;
	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;

	end pr_INC_CLASS;


	PROCEDURE pr_ACT_CLASS6( PM_CLASS    IN NUMBER,
							 PM_UFORIG   IN VARCHAR2,
							 PM_UFDEST   IN VARCHAR2,
							 PM_ACT      IN CHAR,
							 PM_MARGEM   IN NUMBER,
			  				 PM_CODERRO OUT NUMBER,
			  				 PM_TXTERRO OUT VARCHAR2) AS
			  				 
			VACT VARCHAR2(1);

		BEGIN

		PM_CODERRO := 0;

		IF PM_ACT = 'I' THEN
			VACT := 'U';
			
			Update compras.CLASS_ANTEC_ENTRADA set
			cod_trib = 6,
			PC_MARGEM_LUCRO = PM_MARGEM
			where class_fiscal= PM_CLASS and
			cod_uf_origem = PM_UFORIG and
			cod_uf_destino = PM_UFDEST;
			--COMMIT;
			IF SQL%NOTFOUND THEN
				Insert into compras.CLASS_ANTEC_ENTRADA
				(class_fiscal,cod_uf_origem,cod_uf_destino,
				cod_trib,PC_MARGEM_LUCRO)
				values (PM_CLASS,PM_UFORIG,PM_UFDEST,6,PM_MARGEM);
			--	COMMIT;
				VACT := 'I';
			END IF;
			--COMMIT;
		ELSIF PM_ACT = 'D' THEN
			VACT := 'D';
			Delete from  compras.CLASS_ANTEC_ENTRADA
			where class_fiscal= PM_CLASS and
			cod_uf_origem = PM_UFORIG and
			cod_uf_destino = PM_UFDEST;
			--COMMIT;
		END IF;
		
		
		INSERT INTO compras.log_class_antec_entrada
		VALUES
		((SELECT NVL(MAX(SEQUENCIA),0)+1 FROM compras.log_class_antec_entrada),
		  VACT,SYSDATE,PM_CLASS,PM_UFORIG,PM_UFDEST,6,PM_MARGEM);
		
		COMMIT;

	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;

	end pr_ACT_CLASS6;


	PROCEDURE pr_CON_CLASS_ANTEC( PM_UFDEST  IN VARCHAR2,
			   	      PM_CURSOR1 IN OUT TP_CURSOR,
	  			      PM_CODERRO OUT NUMBER,
	  			      PM_TXTERRO OUT VARCHAR2) AS

		BEGIN

		PM_CODERRO := 0;

		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR
		Select class_fiscal "CLASS. FISCAL",
		cod_uf_origem "UF ORIGEM",
		cod_uf_destino "UF DESTINO",
		cod_trib,
		PC_MARGEM_LUCRO "MG_LUCRO"
		From compras.class_antec_entrada
		Where cod_uf_destino = PM_UFDEST
		Order by class_fiscal,cod_uf_origem,
		cod_uf_destino;

	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;

	end pr_CON_CLASS_ANTEC;


	PROCEDURE pr_CON_COUNT_CLASS( PM_CLASS   IN NUMBER,
								  PM_UFDEST  IN VARCHAR2,
								  PM_UFORIG  IN VARCHAR2,
								  PM_CURSOR1 IN OUT TP_CURSOR,
								  PM_CODERRO OUT NUMBER,
								  PM_TXTERRO OUT VARCHAR2) AS

		STRSQL varchar2(2000);

		BEGIN

		PM_CODERRO := 0;

		STRSQL := STRSQL || ' Select count(*) TOTAL';
		STRSQL := STRSQL || ' From compras.class_antec_entrada';
		STRSQL := STRSQL || ' Where cod_uf_destino = ''' || PM_UFDEST || ''' ';
		STRSQL := STRSQL || ' and cod_uf_origem = ''' || PM_UFORIG || ''' ';

		If PM_CLASS <> 0 Then
			STRSQL := STRSQL || ' and class_fiscal = ' || PM_CLASS || ' ';
		End If;

		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR
		STRSQL;

	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;

	end pr_CON_COUNT_CLASS;


	PROCEDURE pr_CON_LOJASUF( PM_CURSOR1 IN OUT TP_CURSOR,
							  PM_CODERRO OUT NUMBER,
							  PM_TXTERRO OUT VARCHAR2) AS
		BEGIN

		PM_CODERRO := 0;

		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR
		select b.cod_uf cod_uf
		from PRODUCAO.loja a, PRODUCAO.cidade b
		Where a.cod_cidade = b.cod_cidade
		group by b.cod_uf
		order by b.cod_uf;

	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;

	end pr_CON_LOJASUF;


	PROCEDURE pr_ACT_CLASS6_II( PM_CLASS     IN NUMBER,
				    PM_UFORIG    IN VARCHAR2,
				    PM_UFORIGANT IN VARCHAR2,
	  			    PM_UFDEST    IN VARCHAR2,
   				    PM_ACT       IN CHAR,
  				    PM_CODERRO   OUT NUMBER,
  				    PM_TXTERRO   OUT VARCHAR2) AS

		BEGIN

		PM_CODERRO := 0;

		DELETE FROM compras.CLASS_ANTEC_ENTRADA
		where
		cod_uf_origem = PM_UFORIG and
		cod_uf_destino = PM_UFDEST ;
		COMMIT;
		Insert into compras.CLASS_ANTEC_ENTRADA
		(class_fiscal,cod_uf_origem,cod_uf_destino,cod_trib,PC_MARGEM_LUCRO)
		(Select class_fiscal,PM_UFORIG,PM_UFDEST,'6',PC_MARGEM_LUCRO
		From compras.class_antec_entrada
		Where cod_uf_destino = PM_UFDEST and
		cod_uf_origem = PM_UFORIGANT);
		COMMIT;

	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;

	end pr_ACT_CLASS6_II;


	PROCEDURE pr_CON_CLASS_PIS( PM_CURSOR1 IN OUT TP_CURSOR,
								PM_CODERRO OUT NUMBER,
								PM_TXTERRO OUT VARCHAR2) AS
		BEGIN

		PM_CODERRO := 0;

		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR
		Select class_fiscal "CLASS. FISCAL"
		From producao.class_fiscal_pis_cofins
		Order by class_fiscal;

	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;

	end pr_CON_CLASS_PIS;
	
	
	
	PROCEDURE PR_ACT_CONV52( PM_CLASS    IN NUMBER,
							 PM_ACT      IN CHAR,
							 PM_CODERRO OUT NUMBER,
			  				 PM_TXTERRO OUT VARCHAR2) AS
		BEGIN

		PM_CODERRO := 0;

		IF PM_ACT = 'I' THEN
			
			Insert into compras.REDBASE_CONV52
			(class_fiscal) values (PM_CLASS);

		ELSIF PM_ACT = 'D' THEN
		
			Delete from  compras.REDBASE_CONV52
			where class_fiscal= PM_CLASS ;

		END IF;
		
		
		INSERT INTO compras.LOG_REDBASE_CONV52
		VALUES
		((SELECT NVL(MAX(SEQUENCIA),0)+1 FROM compras.LOG_REDBASE_CONV52),
		  SYSDATE,PM_ACT,PM_CLASS,PM_CLASS);
		
		COMMIT;

	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;

	end PR_ACT_CONV52;
	
	
	PROCEDURE pr_CON_CONV52( PM_CURSOR1 IN OUT TP_CURSOR,
							 PM_CODERRO OUT NUMBER,
							 PM_TXTERRO OUT VARCHAR2) AS
		BEGIN

		PM_CODERRO := 0;

		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR
		SELECT * from compras.REDBASE_CONV52;

	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;

	end pr_CON_CONV52;
	
	
	PROCEDURE pr_CON_COMPARATIVO( PM_UFORIG  IN VARCHAR2,
									PM_UFDEST  IN VARCHAR2,
									PM_CURSOR1 IN OUT TP_CURSOR,
									PM_CODERRO OUT NUMBER,
									PM_TXTERRO OUT VARCHAR2) AS
			
				STRSQL VARCHAR2(1000);
			
			BEGIN
	
			PM_CODERRO := 0;
			
			STRSQL := 'select A.*,
					B.CLASS_FISCAL CLASS_FISCAL_ANTEC,
					B.COD_UF_ORIGEM UF_ORIGEM_ANTEC,
					B.COD_UF_DESTINO UF_DESTINO_ANTEC,
					B.PC_MARGEM_LUCRO PC_MARGEM_ANTEC
					from compras.subst_tributaria A,
					COMPRAS.CLASS_ANTEC_ENTRADA B
					where ';
					
					IF PM_UFORIG IS NOT NULL THEN
						STRSQL := STRSQL || ' A.cod_uf_origem = ''' || PM_UFORIG ||''' AND';
					END IF;
					
					IF PM_UFDEST IS NOT NULL THEN
						STRSQL := STRSQL || ' A.COD_UF_DESTINO = ''' || PM_UFDEST ||''' AND';
					END IF;
					
					STRSQL := STRSQL || ' A.CLASS_FISCAL = B.CLASS_FISCAL(+) AND
								A.COD_UF_DESTINO = B.COD_UF_DESTINO(+) AND
								A.COD_UF_ORIGEM=B.COD_UF_ORIGEM(+)
								ORDER BY A.CLASS_FISCAL,A.COD_UF_ORIGEM,A.COD_UF_DESTINO';
	
			-- Abrindo Cursor
			OPEN PM_CURSOR1 FOR
			STRSQL;
	
		EXCEPTION
			WHEN OTHERS THEN
			ROLLBACK;
			PM_CODERRO := SQLCODE;
			PM_TXTERRO := SQLERRM;
	
	end pr_CON_COMPARATIVO;
	
	
	PROCEDURE PR_CON_FORNEC_DISP( PM_CURSOR1 IN OUT TP_CURSOR,
	  							  PM_CODERRO OUT NUMBER,
	  							  PM_TXTERRO OUT VARCHAR2) AS

		BEGIN

		PM_CODERRO := 0;

		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR
		select a.cod_fornecedor, a.nome_fornec, a.sigla
		from producao.fornecedor a
		where 
		a.situacao = 0 and
		a.cod_fornecedor not in 
		(select b.cod_fornecedor from compras.forn_conv52_sf04 b);

	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;

	end PR_CON_FORNEC_DISP;
	
	
	PROCEDURE PR_CON_FORNEC( PM_CURSOR1 IN OUT TP_CURSOR,
	  						 PM_CODERRO OUT NUMBER,
	  						 PM_TXTERRO OUT VARCHAR2) AS

		BEGIN

		PM_CODERRO := 0;

		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR
		select a.cod_fornecedor, a.nome_fornec, a.sigla
		from producao.fornecedor a
		where 
		a.situacao = 0 and
		a.cod_fornecedor not in 
		(select b.cod_fornecedor from compras.forn_conv52_sf04 b)
		ORDER BY A.NOME_FORNEC;

	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;

	END PR_CON_FORNEC;
	
	
	PROCEDURE PR_CON_FORNEC_CONV( PM_CURSOR1 IN OUT TP_CURSOR,
	  							  PM_CODERRO OUT NUMBER,
	  							  PM_TXTERRO OUT VARCHAR2) AS

		BEGIN

		PM_CODERRO := 0;

		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR
		select a.cod_fornecedor, a.nome_fornec, a.sigla
		from producao.fornecedor a, compras.forn_conv52_sf04 b
		where 
		A.COD_FORNECEDOR = B.COD_FORNECEDOR
		ORDER BY A.NOME_FORNEC;

	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;

	end PR_CON_FORNEC_CONV;
	
	
	PROCEDURE PR_ACT_FORNEC_CONV( PM_FORNEC  IN NUMBER,
								  PM_ACT     IN VARCHAR2,
	  							  PM_CODERRO OUT NUMBER,
	  							  PM_TXTERRO OUT VARCHAR2) AS

		BEGIN

		PM_CODERRO := 0;

		IF PM_ACT = 'I' THEN
		
			INSERT INTO COMPRAS.FORN_CONV52_SF04 VALUES (PM_FORNEC);
		
		ELSIF PM_ACT = 'D' THEN
		
			DELETE FROM COMPRAS.FORN_CONV52_SF04
			WHERE COD_FORNECEDOR = PM_FORNEC;
			
		END IF;
		
		INSERT INTO COMPRAS.LOG_FORN_CONV52_SF04 VALUES
		((SELECT NVL(MAX(SEQUENCIA),0)+1 FROM COMPRAS.LOG_FORN_CONV52_SF04),
		SYSDATE,PM_ACT,PM_FORNEC,NULL);
		
		COMMIT;

	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;

	end PR_ACT_FORNEC_CONV;
	

End PCK_CAD510;
---------------
/
