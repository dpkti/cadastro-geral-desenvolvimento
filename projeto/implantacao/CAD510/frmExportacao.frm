VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form frmExportacao 
   Caption         =   "Exporta��o"
   ClientHeight    =   3810
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   4770
   LinkTopic       =   "Exporta��o"
   LockControls    =   -1  'True
   ScaleHeight     =   3810
   ScaleWidth      =   4770
   Begin VB.ComboBox cboDestino 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      ItemData        =   "frmExportacao.frx":0000
      Left            =   45
      List            =   "frmExportacao.frx":0002
      Style           =   2  'Dropdown List
      TabIndex        =   8
      Top             =   1860
      Width           =   2955
   End
   Begin VB.ComboBox cboOrigem 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      ItemData        =   "frmExportacao.frx":0004
      Left            =   60
      List            =   "frmExportacao.frx":0006
      Style           =   2  'Dropdown List
      TabIndex        =   7
      Top             =   1200
      Width           =   2925
   End
   Begin VB.ListBox lstClass 
      Height          =   1620
      Left            =   3090
      TabIndex        =   1
      Top             =   1170
      Width           =   1605
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   0
      Top             =   3555
      Width           =   4770
      _ExtentX        =   8414
      _ExtentY        =   450
      SimpleText      =   "Todos os itens da lista ser�o cadastrados"
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   7885
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin Bot�o.cmd cmd_importar 
      Height          =   675
      Left            =   3480
      TabIndex        =   2
      ToolTipText     =   "Importar Dados"
      Top             =   2820
      Width           =   915
      _ExtentX        =   1614
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   14215660
      BCOLO           =   14215660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmExportacao.frx":0008
      PICN            =   "frmExportacao.frx":0024
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSComDlg.CommonDialog CMMDIALOG 
      Left            =   2940
      Top             =   60
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   30
      TabIndex        =   6
      Top             =   780
      Width           =   4920
      _ExtentX        =   8678
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmd_voltar 
      Height          =   690
      Left            =   60
      TabIndex        =   9
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   60
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmExportacao.frx":0CFE
      PICN            =   "frmExportacao.frx":0D1A
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd_Cadastrar 
      Height          =   690
      Left            =   855
      TabIndex        =   10
      TabStop         =   0   'False
      ToolTipText     =   "Gravar"
      Top             =   60
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmExportacao.frx":19F4
      PICN            =   "frmExportacao.frx":1A10
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      Caption         =   "UF Destino:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   60
      TabIndex        =   5
      Top             =   1620
      Width           =   945
   End
   Begin VB.Label lblOrigem 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      Caption         =   "UF Origem:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   60
      TabIndex        =   4
      Top             =   960
      Width           =   915
   End
   Begin VB.Label lbl_ClassificacaoFiscal 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      Caption         =   "Classifica��o Fiscal:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   3120
      TabIndex        =   3
      Top             =   930
      Width           =   1485
   End
End
Attribute VB_Name = "frmExportacao"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmd_Cadastrar_Click()
    
    If cboOrigem.ListIndex = -1 And cboDestino.ListIndex = -1 Then
        MsgBox "Escolha pelo menos uma UF.", vbExclamation + vbOKOnly, "Aten��o!"
        Exit Sub
    End If
    
    StatusBar1.Panels(1).Text = "Exportando Dados..."
    ExportarClassificacao
    
End Sub

Private Sub cmd_importar_Click()
On Error GoTo Trata_erro

    Dim xl As New Excel.Application, FLERRO As Integer
    'Dim xlB As Excel.Workbook
    Dim Fim, ErroC As Integer, i As Long, j As Long, x As Integer
    Dim Celula, strcaminho As String
    Dim mat_import(13)

    StatusBar1.Panels(1).Text = ""
    
    CMMDIALOG.ShowOpen
    
    strcaminho = CMMDIALOG.FileName
    
    If InStr(1, strcaminho, ".xls") = 0 Then
        MsgBox "O arquivo de importa��o deve ser em Excel", , "Aten��o!"
        Exit Sub
    End If
       
    Screen.MousePointer = 11
    
    'Verifica se foi indicado o diret�rio e nome do arquivo a ser lido
    If strcaminho = "" Then
        MsgBox "Indique o arquivo a ser aberto"
        Screen.MousePointer = 0
        Exit Sub
    End If
    
    xl.Workbooks.Add strcaminho

    If xl.Cells(1, 1).Value = "" Then

        MsgBox "Primeira linha do Arquivo n�o poder estar em branco", vbInformation, "Aten��o"
        Screen.MousePointer = 0
        Exit Sub

    ElseIf xl.Cells(2, 1).Value = "" Then

        Fim = 1

    Else

        xl.Cells(1, 1).Select
        xl.ActiveCell.End(xlDown).Select

        Fim = xl.ActiveCell.Row

    End If

    xl.Cells(1, 1).Select

    FLERRO = 0
    
    'I = Linha
    'J = Coluna
    
    lstClass.Clear
    
    For i = 1 To Fim
        ErroC = 0
           
        Celula = xl.Cells(i, 1).Value
        Celula = Trim(UCase(Celula))
        
        If Not IsNumeric(Celula) Then
            ErroC = 1
            MsgBox "Valor:" & Celula & " da Col:1 Lin:" & i & "  n�o existe no cadastro. Favor Verificar e Importar a Planilha Corrigida.", vbExclamation + vbOKOnly, "Aten��o!"
            Exit For
        End If
        
        lstClass.AddItem Format(Celula, "0000000000")
        
    Next

    If ErroC = 1 Then
        lstClass.Clear
    End If

    Set xl = Nothing
    
    
    Screen.MousePointer = vbNormal
    Exit Sub
    
Trata_erro:
    If Err.Number <> 0 Then
        MsgBox "Sub: cmdImportar_Click" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl
        Screen.MousePointer = vbNormal

    End If
End Sub


Private Sub cmd_voltar_Click()
    Unload Me
End Sub

Private Sub Form_Load()

    StatusBar1.Panels(1).Text = ""
    
    Me.Top = (Screen.Height - Me.Height) / 2
    Me.Left = (Screen.Width - Me.Width) / 2
    
    Set vObjOracle = vVB_Generica_001.TabelaUF(vBanco)
    
    Call vVB_Generica_001.PreencheComboList(vObjOracle, cboOrigem, "UF", "DESC. UF")
    
    Call vVB_Generica_001.PreencheComboList(vObjOracle, cboDestino, "UF", "DESC. UF")
    
End Sub

Sub ExportarClassificacao()
    Dim vSql As String
    Dim i As Long
    Dim vClass_fiscal As Long
    Dim vOrigem As String, vDestino As String

    On Error GoTo Trata_erro
    
    Screen.MousePointer = 11
    
    vOrigem = Mid(Trim(cboOrigem.Text), 1, 2)
    vDestino = Mid(Trim(cboDestino.Text), 1, 2)
    
    vSql = "PRODUCAO.PCK_CAD510.pr_exportacao(:pm_tipo,:pm_classfiscal,:pm_origem,:pm_destino)"
    
    'Devem Existir Valores Carregados
    If lstClass.ListCount < 1 Then

        'Setando Parametros
        vBanco.Parameters.Remove "pm_tipo"
        vBanco.Parameters.Add "pm_tipo", 1, 1
        
        vBanco.Parameters.Remove "pm_classfiscal"
        vBanco.Parameters.Add "pm_classfiscal", Null, 1
        
        vBanco.Parameters.Remove "pm_origem"
        vBanco.Parameters.Add "pm_origem", vOrigem, 1
        
        vBanco.Parameters.Remove "pm_destino"
        vBanco.Parameters.Add "pm_destino", vDestino, 1
                
        Call vVB_Generica_001.ExecutaPL(vBanco, vSql)
        
       Exit Sub
       
    End If
     
    For i = 0 To lstClass.ListCount - 1
    
        vClass_fiscal = Val(lstClass.List(i))
               
        'Setando Parametros
        vBanco.Parameters.Remove "pm_tipo"
        vBanco.Parameters.Add "pm_tipo", 2, 1
        
        vBanco.Parameters.Remove "pm_classfiscal"
        vBanco.Parameters.Add "pm_classfiscal", vClass_fiscal, 1
        
        vBanco.Parameters.Remove "pm_origem"
        vBanco.Parameters.Add "pm_origem", vOrigem, 1
        
        vBanco.Parameters.Remove "pm_destino"
        vBanco.Parameters.Add "pm_destino", vDestino, 1
                
        Call vVB_Generica_001.ExecutaPL(vBanco, vSql)
        
    Next
    
    CarregaExcel vOrigem, vDestino
    LimpaCampos
    
    StatusBar1.Panels(1).Text = "Dados exportados com sucesso."
    
    Screen.MousePointer = vbNormal
    
Trata_erro:
    If Err.Number <> 0 Then
        Screen.MousePointer = vbNormal
        MsgBox "Sub: ExportarClassificacao" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Sub

Sub CarregaExcel(pOrigem As String, pDestino As String)

    On Error GoTo Trata_erro
    
    Dim i As Long
    Dim vSql As String
    Dim EApp As Excel.Application
    Dim EwkB As Excel.Workbook
    Dim EwkS As Excel.Worksheet
    
    Set EApp = CreateObject("Excel.Application")
    Set EwkB = EApp.Workbooks.Add
    Set EwkS = EwkB.Sheets(1)
    
    With EApp.ActiveSheet.PageSetup
        .RightFooter = "P�gina &P/&N"
        .LeftMargin = 10 ''Application.InchesToPoints(0.393700787401575)
        .RightMargin = 10 ''Application.InchesToPoints(0.393700787401575)
        .TopMargin = 10 ''Application.InchesToPoints(0.393700787401575)
        .BottomMargin = 10 ''Application.InchesToPoints(0.393700787401575)
        .PrintHeadings = False
        .PrintGridlines = False
        .PrintComments = xlPrintNoComments
        .PrintQuality = 600
        .CenterHorizontally = False
        .CenterVertically = False
        .Draft = False
        .PaperSize = xlPaperA4
        .FirstPageNumber = xlAutomatic
        .Order = xlDownThenOver
        .BlackAndWhite = False
    End With
    
    'Carrega os dados no excel
    vBanco.Parameters.Remove "PM_CURSOR"
    vBanco.Parameters.Add "PM_CURSOR", 0, 3
    vBanco.Parameters("PM_CURSOR").serverType = 102
    vBanco.Parameters("PM_CURSOR").DynasetOption = &H2&
    
    'Leandro -29/06/09
    vBanco.Parameters("PM_CURSOR").DynasetCacheParams 256, 17, 20, 2000, 0
   
    vBanco.Parameters.Remove "PM_origem"
    vBanco.Parameters.Add "PM_origem", pOrigem, 1
    vBanco.Parameters.Remove "pm_destino"
    vBanco.Parameters.Add "pm_destino", pDestino, 1
    
    vSql = "PRODUCAO.PCK_CAD510.pr_carrega_excel(:PM_CURSOR,:PM_origem,:pm_destino)"


    vErro = vVB_Generica_001.ExecutaPL(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR").Value
           
    EApp.Cells(1, 1).Value = "CLASS. FISCAL"
    EApp.Cells(1, 2).Value = "UF ORIGEM"
    EApp.Cells(1, 3).Value = "UF DESTINO"
    EApp.Cells(1, 4).Value = "TRIB. REVEND"
    EApp.Cells(1, 5).Value = "TRIB. INSCRITO"
    EApp.Cells(1, 6).Value = "TRIB. ISENTO"
    EApp.Cells(1, 7).Value = "FATOR REVEND"
    EApp.Cells(1, 8).Value = "FATOR INSCRITO"
    EApp.Cells(1, 9).Value = "FATOR ISENTO"
    EApp.Cells(1, 10).Value = "CRED. SUSPENSO"
    EApp.Cells(1, 11).Value = "MG. LUCRO"
    EApp.Cells(1, 12).Value = "TRIB. TARE"
    EApp.Cells(1, 13).Value = "FATOR TARE"
    EApp.Cells(1, 14).Value = "TP.REL"
    EApp.Cells(1, 15).Value = "TRIB. MONTADORA"
    EApp.Cells(1, 16).Value = "FATOR MONTADORA"
    EApp.Cells(1, 17).Value = "RED.ICMS"
        
    i = 2
    
    While Not vObjOracle.EOF
        Screen.MousePointer = vbDefault
        
        EApp.Cells(i, 1).Value = vObjOracle("class_fiscal")
        EApp.Cells(i, 2).Value = vObjOracle("cod_uf_origem")
        EApp.Cells(i, 3).Value = vObjOracle("cod_uf_destino")
        EApp.Cells(i, 4).Value = vObjOracle("cod_trib_revendedor")
        EApp.Cells(i, 5).Value = vObjOracle("cod_trib_inscrito")
        EApp.Cells(i, 6).Value = vObjOracle("cod_trib_isento")
        EApp.Cells(i, 7).Value = vObjOracle("fator_revendedor")
        EApp.Cells(i, 8).Value = vObjOracle("fator_inscrito")
        EApp.Cells(i, 9).Value = vObjOracle("fator_isento")
        EApp.Cells(i, 10).Value = vObjOracle("fl_cred_suspenso")
        EApp.Cells(i, 11).Value = vObjOracle("pc_margem_lucro")
        EApp.Cells(i, 12).Value = vObjOracle("cod_trib_tare")
        EApp.Cells(i, 13).Value = vObjOracle("fator_tare")
        EApp.Cells(i, 14).Value = vObjOracle("tp_relatorio")
        EApp.Cells(i, 15).Value = vObjOracle("cod_trib_montadora")
        EApp.Cells(i, 16).Value = vObjOracle("fator_montadora")
        EApp.Cells(i, 17).Value = vObjOracle("fl_red_icms")
        
        i = i + 1
        vObjOracle.MoveNext
    Wend
    
    ' Exibe Excel
    EApp.Application.Visible = True
    
    ' Desliga o Excel do Programa
    Set EwkS = Nothing
    Set EwkB = Nothing
    Set EApp = Nothing
    
    Exit Sub
    
Trata_erro:
    
       Screen.MousePointer = vbNormal
       MsgBox "Ocorreu erro: " & Err.Number & " - " & Err.Description, 16
       Set EwkS = Nothing
       Set EwkB = Nothing
       Set EApp = Nothing
End Sub

Private Sub LimpaCampos()

    cboOrigem.ListIndex = -1
    cboDestino.ListIndex = -1
    lstClass.Clear
    
End Sub
