VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmRegimePE 
   Caption         =   "Classifica��o Regime Especial PE"
   ClientHeight    =   6270
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   5790
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   6270
   ScaleWidth      =   5790
   Begin VB.TextBox txtFator 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   4500
      MaxLength       =   5
      TabIndex        =   4
      Top             =   1035
      Width           =   1230
   End
   Begin VB.TextBox txtClass 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1755
      MaxLength       =   10
      TabIndex        =   3
      Top             =   1035
      Width           =   1230
   End
   Begin pkGradientControl.pkGradient pkGradient 
      Height          =   30
      Left            =   45
      TabIndex        =   6
      Top             =   765
      Width           =   5730
      _ExtentX        =   10107
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   0
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   0
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmRegimePE.frx":0000
      PICN            =   "frmRegimePE.frx":001C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdSalvar 
      Height          =   690
      Left            =   835
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Gravar"
      Top             =   0
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmRegimePE.frx":0CF6
      PICN            =   "frmRegimePE.frx":0D12
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSFlexGridLib.MSFlexGrid mfgCons 
      Height          =   4245
      Left            =   90
      TabIndex        =   5
      Top             =   1590
      Width           =   5655
      _ExtentX        =   9975
      _ExtentY        =   7488
      _Version        =   393216
      Cols            =   3
      BackColorBkg    =   -2147483633
      AllowUserResizing=   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin Bot�o.cmd cmdImportar 
      Height          =   690
      Left            =   1620
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Importar"
      Top             =   0
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmRegimePE.frx":19EC
      PICN            =   "frmRegimePE.frx":1A08
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      Caption         =   "Fator"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   3840
      TabIndex        =   9
      Top             =   1080
      Width           =   615
   End
   Begin VB.Label lbl 
      Appearance      =   0  'Flat
      Caption         =   "Classifica��o Fiscal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   90
      TabIndex        =   8
      Top             =   1080
      Width           =   1815
   End
   Begin VB.Label Label4 
      Alignment       =   2  'Center
      Caption         =   "Duplo click para excluir a classifica��o"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   5940
      Width           =   5595
   End
End
Attribute VB_Name = "frmRegimePE"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : frmRegimePE
' Author    : C.SAMUEL.OLIVEIRA
' Date      : 07/10/15
' Purpose   : TI-3030
'---------------------------------------------------------------------------------------

Option Explicit

Dim vSql As String
Dim vErro As String

Private Sub cmdImportar_Click()
    frmImportRegimePE.Show 1
End Sub
Private Sub cmdVoltar_Click()
    Unload Me
End Sub

Private Sub cmdSalvar_Click()

On Error GoTo TrataErro

    Screen.MousePointer = 11
     
    If txtClass.Text = "" Then
        MsgBox "Preencha o campo Classifica��o Fiscal!", vbExclamation, "Aten��o"
        Screen.MousePointer = 0
        txtClass.SetFocus
        Exit Sub
    End If

    If txtFator.Text = "" Then
        MsgBox "Preencha o campo Fator!", vbExclamation, "Aten��o"
        Screen.MousePointer = 0
        txtFator.SetFocus
        Exit Sub
    End If
    
    If Len(txtFator.Text) > 2 And InStr(1, txtFator.Text, ".") = 0 Then
        MsgBox "Preencha o campo Fator no formato (99.99)", vbExclamation, "Aten��o"
        Screen.MousePointer = 0
        txtFator.SetFocus
        Exit Sub
    End If
    
    If MsgBox("Confirma grava��o dessa classifica��o fiscal para o Regime de PE?", vbYesNo, "Aten��o") = vbNo Then Exit Sub
       
    vSql = AtualizarClassFiscal(txtClass.Text, txtFator.Text, 1)
    
    vErro = vVB_Generica_001.ExecutaPL(vBanco, vSql)
    
    vErro = IIf(vBanco.Parameters("PM_CODERRO") = "", 0, vBanco.Parameters("PM_CODERRO"))
    
    If vErro <> 0 Then
        MsgBox vBanco.Parameters("PM_TXTERRO") & ".Ligue para o Depto de Sistemas"
        Screen.MousePointer = 0
        Exit Sub
    End If
    
    Call Form_Load
    
    Screen.MousePointer = 0
       
    Exit Sub
    
TrataErro:
    If Err.Number <> 0 Then
        MsgBox "Sub cmdSalvar_Click " & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Sub

Public Sub Form_Load()

On Error GoTo TrataErro

    Me.Top = 1600
    Me.Left = 100
    
    txtClass.Text = ""
    txtFator.Text = ""
    
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").serverType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 17, 20, 2000, 0
   
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "PRODUCAO.PCK_CAD510.PR_SEL_NCM_REGIME_ESPECIAL_PE(:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
    
    vErro = vVB_Generica_001.ExecutaPL(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
    
    vErro = IIf(vBanco.Parameters("PM_CODERRO") = "", 0, vBanco.Parameters("PM_CODERRO"))
    
    If vErro <> 0 Then
        MsgBox vBanco.Parameters("PM_TXTERRO") & ".Ligue para o Depto de Sistemas"
        Screen.MousePointer = 0
        Exit Sub
    End If
    
    If Not vObjOracle.EOF And Not vObjOracle.BOF Then
        vVB_Generica_001.CarregaGridTabela mfgCons, vObjOracle, 3
    End If
    
    Exit Sub
    
TrataErro:
    If Err.Number <> 0 Then
        MsgBox "Sub Form_Load " & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Sub


Private Sub mfgCons_DblClick()

On Error GoTo TrataErro

Dim vClass As String, vFator As Double

    If MsgBox("Comfirma exclus�o da classifica��o fiscal do Regime Especial PE?", vbYesNo, "ATEN��O") = vbNo Then
        Exit Sub
    End If

    mfgCons.Col = 1
    vClass = mfgCons.Text
    
    mfgCons.Col = 2
    vFator = mfgCons.Text
    
    vSql = AtualizarClassFiscal(vClass, vFator, 0)
    
    vErro = vVB_Generica_001.ExecutaPL(vBanco, vSql)
    
    vErro = IIf(vBanco.Parameters("PM_CODERRO") = "", 0, vBanco.Parameters("PM_CODERRO"))
    
    If vErro <> 0 Then
        MsgBox vBanco.Parameters("PM_TXTERRO") & ".Ligue para o Depto de Sistemas"
        Screen.MousePointer = 0
        Exit Sub
    End If
    
    Call Form_Load

TrataErro:
    If Err.Number <> 0 Then
        MsgBox "Sub mfgCons_DblClick " & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
    
End Sub

Private Sub txtFator_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtFator)

End Sub

Private Sub txtClass_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtClass)

End Sub

Private Sub txtFator_LostFocus()

    txtFator.Text = Replace(txtFator.Text, ",", ".")

End Sub


Public Function AtualizarClassFiscal(pClassFiscal As String, pFator As Double, pSituacao As Byte) As String
    
    AtualizarClassFiscal = ""
    
    vBanco.Parameters.Remove "PM_CLASS"
    vBanco.Parameters.Add "PM_CLASS", pClassFiscal, 1
    
    vBanco.Parameters.Remove "PM_FATOR"
    vBanco.Parameters.Add "PM_FATOR", pFator, 1
    
    vBanco.Parameters.Remove "PM_SITUACAO"
    vBanco.Parameters.Add "PM_SITUACAO", pSituacao, 1
    
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    AtualizarClassFiscal = "PRODUCAO.PCK_CAD510.PR_UPD_NCM_REGIME_ESPECIAL_PE(:PM_CLASS,:PM_FATOR,:PM_SITUACAO,:PM_CODERRO,:PM_TXTERRO)"
    
End Function

