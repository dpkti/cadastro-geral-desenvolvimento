VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmImportRegimePE 
   Caption         =   "Importar Classifica��o Regime Especial PE"
   ClientHeight    =   6330
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   7170
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   6330
   ScaleWidth      =   7170
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame fra 
      Caption         =   "Arquivo para Leitura"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1230
      Left            =   45
      TabIndex        =   4
      Top             =   900
      Width           =   7080
      Begin VB.TextBox txtArquivo 
         Appearance      =   0  'Flat
         Height          =   375
         Left            =   960
         Locked          =   -1  'True
         TabIndex        =   8
         Top             =   600
         Width           =   6015
      End
      Begin Bot�o.cmd cmdArq 
         Height          =   690
         Left            =   135
         TabIndex        =   5
         TabStop         =   0   'False
         ToolTipText     =   "Abrir"
         Top             =   360
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmImportRegimePE.frx":0000
         PICN            =   "frmImportRegimePE.frx":001C
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Dados do Arquivo"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   4065
      Left            =   45
      TabIndex        =   0
      Top             =   2205
      Width           =   7110
      Begin Bot�o.cmd cmdImp 
         Height          =   420
         Left            =   5850
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   3555
         Width           =   1140
         _ExtentX        =   2011
         _ExtentY        =   741
         BTYPE           =   3
         TX              =   "Importar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmImportRegimePE.frx":0CF6
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MSFlexGridLib.MSFlexGrid mfgArq 
         Height          =   3120
         Left            =   120
         TabIndex        =   2
         Top             =   360
         Width           =   6855
         _ExtentX        =   12091
         _ExtentY        =   5503
         _Version        =   393216
         Cols            =   3
         BackColorBkg    =   -2147483633
         AllowUserResizing=   1
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lbMsg 
         BeginProperty Font 
            Name            =   "Arial Black"
            Size            =   12
            Charset         =   0
            Weight          =   900
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   375
         Left            =   180
         TabIndex        =   3
         Top             =   3600
         Width           =   5325
      End
   End
   Begin MSComDlg.CommonDialog CMMDIALOG 
      Left            =   6660
      Top             =   135
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin pkGradientControl.pkGradient pkGradient 
      Height          =   30
      Left            =   45
      TabIndex        =   6
      Top             =   765
      Width           =   7080
      _ExtentX        =   12488
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   7
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   0
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmImportRegimePE.frx":0D12
      PICN            =   "frmImportRegimePE.frx":0D2E
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmImportRegimePE"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : frmImportRegimePE
' Author    : C.SAMUEL.OLIVEIRA
' Date      : 07/10/15
' Purpose   : TI-3030
'---------------------------------------------------------------------------------------

Option Explicit
Public FLERRO As Integer

Private Sub cmdArq_Click()

On Error GoTo TrataErro

    Dim xl As New Excel.Application
    Dim Fim, ErroC As Integer
    Dim Celula, strUF As String
    Dim vNomeArquivo As String
        
    CMMDIALOG.ShowOpen
    
    vNomeArquivo = CMMDIALOG.FileName
    
    If InStr(1, UCase(vNomeArquivo), ".XLS") = 0 Then
        MsgBox "O arquivo de importa��o deve ser em Excel", vbInformation, "Aten��o!"
        Exit Sub
    End If
    
    txtArquivo.Text = vNomeArquivo
    
    Screen.MousePointer = 11
    
    'Verifica se foi indicado o diret�rio e nome do arquivo a ser lido
    If vNomeArquivo = "" Then
        MsgBox "Indique o arquivo a ser aberto", vbInformation, "Aten��o!"
        Screen.MousePointer = 0
        Exit Sub
    End If

    xl.Workbooks.Add vNomeArquivo
    
    lbMsg.Caption = ""
    
    If xl.Cells(1, 1).Value = "" Or xl.Cells(1, 2).Value = "" Then
    
        MsgBox "Primeira linha do Arquivo n�o poder estar em branco", vbInformation, "Aten��o"
        Screen.MousePointer = 0
        Exit Sub
        
    ElseIf xl.Cells(2, 1).Value = "" Or xl.Cells(2, 2).Value = "" Then
        
        Fim = 1
    
    Else
        
        xl.Cells(1, 2).Select
        xl.ActiveCell.End(xlDown).Select

        Fim = xl.ActiveCell.Row
        
    End If
    
    xl.Cells(1, 2).Select
    
    FLERRO = 0
    
    For i = 1 To Fim
        ErroC = 0
    
        With mfgArq
        
            Celula = xl.Cells(i, 1).Value
            Celula = Trim(UCase(Celula))
            
            If Not IsNumeric(Celula) Then
                ErroC = 1
            Else
                If Len(Celula) > 10 Then
                    ErroC = 1
                End If
            End If
                
            .Rows = i + 1
            
            .Row = i
            .Col = 1
            .Text = UCase(Celula)
            
            Celula = xl.Cells(i, 2).Value
            Celula = Trim(UCase(Celula))
            
            If Not IsNumeric(Celula) Then
                ErroC = 1
            Else
                If Len(Celula) > 5 Then
                    ErroC = 1
                End If
            End If
            
            .Col = 2
            .Text = UCase(Celula)
            
        End With
        
        If ErroC = 1 Then
            FLERRO = 1
            lbMsg.Caption = "* H� registros com problemas"
            
            With mfgArq
                .Row = i
                .Col = 1
                .CellBackColor = vbYellow
                .Col = 2
                .CellBackColor = vbYellow
            End With
        End If

    Next
    
    Set xl = Nothing
    Screen.MousePointer = vbNormal
    
    cmdImp.Enabled = True
    
TrataErro:
    If Err.Number = -2147417851 Then
        Resume Next
    ElseIf Err.Number <> 0 Then
        MsgBox Err.Number & vbCrLf & Err.Description
        Screen.MousePointer = vbNormal
    End If
End Sub

Private Sub cmdImp_Click()

    If FLERRO = 1 Then
        If MsgBox("Os registro marcados em AMARELO, n�o ser�o inseridos. Deseja continuar?", vbYesNo) = vbNo Then
             Exit Sub
        End If
    End If
    
    SalvarClass
    
End Sub

Private Sub cmdVoltar_Click()
    frmRegimePE.Form_Load

    Unload Me
End Sub

Private Sub Form_Load()
    Me.Top = 2000
    Me.Left = 2000
    
End Sub

Private Sub SalvarClass()

Dim vCor As String
Dim vClass As String, vFator As Double
On Error GoTo TrataErro
    Screen.MousePointer = 11

    For i = 1 To mfgArq.Rows - 1
    
        With mfgArq
            .Row = i
            .Col = 1
            vCor = .CellBackColor
        End With
        
        If vCor <> 65535 Then
                       
            mfgArq.Col = 1
            vClass = mfgArq.Text
            
            mfgArq.Col = 2
            vFator = mfgArq.Text
    
            vSql = frmRegimePE.AtualizarClassFiscal(vClass, vFator, 1)
            
            vErro = vVB_Generica_001.ExecutaPL(vBanco, vSql)
            
            vErro = IIf(vBanco.Parameters("PM_CODERRO") = "", 0, vBanco.Parameters("PM_CODERRO"))
            
            If vErro <> 0 Then
                MsgBox vBanco.Parameters("PM_TXTERRO") & ".Ligue para o Depto de Sistemas"
                Screen.MousePointer = 0
                Exit Sub
            End If
    
        End If
    Next
    
    MsgBox "Inclus�o OK ", vbInformation, "Aten��o"
    Screen.MousePointer = 0
    cmdImp.Enabled = False
    
    Exit Sub
TrataErro:
    If Err.Number = -2147417851 Then
        Resume Next
    ElseIf Err.Number <> 0 Then
        MsgBox Err.Number & vbCrLf & Err.Description
        Screen.MousePointer = vbNormal
    End If
End Sub
