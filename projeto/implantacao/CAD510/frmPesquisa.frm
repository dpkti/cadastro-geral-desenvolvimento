VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmPesquisa 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Pesquisa"
   ClientHeight    =   4635
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8715
   ControlBox      =   0   'False
   Icon            =   "frmPesquisa.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   309
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   581
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   4305
      Width           =   8715
      _ExtentX        =   15372
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   15319
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      Top             =   810
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmPesquisa.frx":23D2
      PICN            =   "frmPesquisa.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSFlexGridLib.MSFlexGrid mfgCons 
      Height          =   3255
      Left            =   90
      TabIndex        =   3
      Top             =   945
      Width           =   8520
      _ExtentX        =   15028
      _ExtentY        =   5741
      _Version        =   393216
      BackColorBkg    =   -2147483633
      AllowUserResizing=   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin Bot�o.cmd cmdExcel 
      Height          =   690
      Left            =   840
      TabIndex        =   4
      TabStop         =   0   'False
      ToolTipText     =   "Exportar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmPesquisa.frx":30C8
      PICN            =   "frmPesquisa.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmPesquisa"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdSair_Click()

    Unload Me

End Sub

Private Sub cmdExcel_Click()
    PlanilhaExcel False
End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()

    Dim vUFOrig As String
    
    Me.Top = 0
    Me.Left = 0
    
    If vOpcao = 2 Then
        vUFOrig = Mid(frmIncluir.cboOrigem, 1, 2)
    Else
        vUFOrig = "X"
    End If
    
    vBanco.Parameters.Remove "PM_CLASS"
    vBanco.Parameters.Add "PM_CLASS", Val(frmIncluir.txtClass), 1
    vBanco.Parameters.Remove "PM_UFORIG"
    vBanco.Parameters.Add "PM_UFORIG", vUFOrig, 1
    vBanco.Parameters.Remove "PM_OPCAO"
    vBanco.Parameters.Add "PM_OPCAO", vOpcao, 1
   
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").serverType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    'Leandro - 29/06/09
    'vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 17, 20, 2000, 0
     vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 17, 20, 2000, 0
   
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
   
    vSql = "PRODUCAO.PCK_CAD510.pr_CON_CLASS(:PM_CLASS,:PM_UFORIG,:PM_OPCAO,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
    
    vErro = vVB_Generica_001.ExecutaPL(vBanco, vSql)
    ExecutaPL vBanco, vSql
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
    
    If vObjOracle.EOF Then
        MsgBox "Nenhuma Classifica��o encontrada!", , "Aten�ao"
        Exit Sub
    Else
        vVB_Generica_001.CarregaGridTabela mfgCons, vObjOracle, 18
    End If
End Sub

Private Sub mfgCons_DblClick()
  Dim tam As Long
  Dim i As Long

  mfgCons.Col = 1
  frmIncluir.txtClass = mfgCons.Text
  
  mfgCons.Col = 2
  tam = frmIncluir.cboOrigem.ListCount
  For i = 0 To tam - 1
    If Mid(Trim(frmIncluir.cboOrigem.List(i)), 1, 2) = mfgCons Then
      frmIncluir.cboOrigem = frmIncluir.cboOrigem.List(i)
      Exit For
      End If
  Next i
  
  mfgCons.Col = 3
  tam = frmIncluir.cboDestino.ListCount
  For i = 0 To tam - 1
    If Mid(Trim(frmIncluir.cboDestino.List(i)), 1, 2) = mfgCons Then
      frmIncluir.cboDestino = frmIncluir.cboDestino.List(i)
      Exit For
      End If
  Next i
  
  mfgCons.Col = 4
  frmIncluir.cboTrib_Rev = mfgCons.Text
  mfgCons.Col = 5
  frmIncluir.cboTrib_Ins = mfgCons.Text
  mfgCons.Col = 6
  frmIncluir.cboTrib_Ise = mfgCons.Text
  mfgCons.Col = 7
  frmIncluir.cboTrib_Tare = mfgCons.Text
  mfgCons.Col = 8
  frmIncluir.cboTrib_Montadora = mfgCons.Text
  mfgCons.Col = 9
  frmIncluir.txtFat_rev = mfgCons.Text
  mfgCons.Col = 10
  frmIncluir.txtFat_Ins = mfgCons.Text
  mfgCons.Col = 11
  frmIncluir.txtFat_Ise = mfgCons.Text
  mfgCons.Col = 12
  frmIncluir.txtFat_Tare = mfgCons.Text
  mfgCons.Col = 13
  frmIncluir.txtFat_Montadora = mfgCons.Text
  mfgCons.Col = 14
  If mfgCons.Text = "S" Then
    frmIncluir.cboCred.ListIndex = 0
  Else
    frmIncluir.cboCred.ListIndex = 1
  End If
  mfgCons.Col = 15
  frmIncluir.txtMg_Lucro = mfgCons.Text
  mfgCons.Col = 16
  If Mid(mfgCons.Text, 1, 2) = "N" Then
    frmIncluir.cboTP_Relatorio.ListIndex = 0
  ElseIf Mid(mfgCons.Text, 1, 2) = "A" Then
    frmIncluir.cboTP_Relatorio.ListIndex = 1
  ElseIf Mid(mfgCons.Text, 1, 2) = "C" Then
    frmIncluir.cboTP_Relatorio.ListIndex = 2
  Else
    frmIncluir.cboTP_Relatorio.ListIndex = 3
  End If
  
 'Leandro - 29/05/09
  mfgCons.Col = 17
  If Trim(mfgCons.Text) = "S" Then
       frmIncluir.cboTrib_ReduzICMS = "S"
  Else
       frmIncluir.cboTrib_ReduzICMS = "N"
  End If
  '------------------
  
  Unload Me
End Sub

Sub PlanilhaExcel(ByVal Imprimir As Boolean)

    On Error GoTo TrataErro
    
    Dim x As Integer, y As Integer, Z As Integer
    
    Dim EApp As Excel.Application
    Dim EwkB As Excel.Workbook
    Dim EwkS As Excel.Worksheet
    
    Screen.MousePointer = 11
    DoEvents
    
    On Error Resume Next
    
    Set EApp = CreateObject("Excel.Application")
    Set EwkB = EApp.Workbooks.Add
    Set EwkS = EwkB.Sheets(1)
    
    ' Esconde o Excel
    'EApp.Application.Visible = True

    ' Configura Margens da P�gina
    'EApp.ActiveSheet.PageSetup.PrintArea = ""
    
    With EApp.ActiveSheet.PageSetup
        '.LeftHeader = "INDICE DE INFLA��O DE COMPRA"
        '.CenterHeader = cmbFornecedor.Text
        '.RightHeader = "&D"
        '.LeftFooter = "Comercial Automotiva Ltda. Confidencial"
        '.CenterFooter = "Depto. Controladoria"
        .RightFooter = "P�gina &P/&N"
        .LeftMargin = 10 ''Application.InchesToPoints(0.393700787401575)
        .RightMargin = 10 ''Application.InchesToPoints(0.393700787401575)
        .TopMargin = 10 ''Application.InchesToPoints(0.393700787401575)
        .BottomMargin = 10 ''Application.InchesToPoints(0.393700787401575)
        '.HeaderMargin = Application.InchesToPoints(0)
        '.FooterMargin = Application.InchesToPoints(0)
        .PrintHeadings = False
        .PrintGridlines = False
        .PrintComments = xlPrintNoComments
        .PrintQuality = 600
        .CenterHorizontally = False
        .CenterVertically = False
        '.Orientation = xlPortrait
        .Draft = False
        .PaperSize = xlPaperA4
        .FirstPageNumber = xlAutomatic
        .Order = xlDownThenOver
        .BlackAndWhite = False
        '.Zoom = 65
    End With
    
'    For y = 0 To Col
'        EApp.Cells(Z, y + 1).Value = vObjOracle.Fields(y).Name
'    Next

    With mfgCons
        Z = 1
        While Z < .Rows
           'Leandro - 29/05/09
            For y = 1 To 17
                EApp.Cells(Z, y).Value = .TextMatrix(Z, y)
            Next
            Z = Z + 1
        Wend
    End With
    
    
    If Not Imprimir Then
        ' Exibe Excel
        EApp.Application.Visible = True
    Else
        EApp.ActiveWindow.SelectedSheets.PrintOut Copies:=1
        EApp.ActiveWorkbook.Close (False)
        EApp.Quit
    End If
    
    ' Volta Cursor do Mouse
    Screen.MousePointer = 1
    
    ' Desliga o Excel do Programa
    Set EwkS = Nothing
    Set EwkB = Nothing
    Set EApp = Nothing
    
    Exit Sub
    
TrataErro:
    
       Screen.MousePointer = 1
       MsgBox "Ocorreu erro: " & Err.Number & " - " & Err.Description, 16
       Set EwkS = Nothing
       Set EwkB = Nothing
       Set EApp = Nothing

End Sub


Function ExecutaPL( _
            pBanco As Object, _
            pSQL As String) As String
 
    On Error GoTo Erro
 
    If UCase(Left(pSQL, 6)) = "SELECT" Or _
            UCase(Left(pSQL, 6)) = "INSERT" Or _
            UCase(Left(pSQL, 6)) = "DELETE" Or _
            UCase(Left(pSQL, 6)) = "UPDATE" Then
 
        vErro = "N�o foi poss�vel executar o comando. N�o � permitido bloco an�nimo nos projetos."
        'Call ProcessaErro(vErro)
        ExecutaPL = vErro
        Exit Function
 
    End If
 
    Dim vPl As String
 
    vPl = _
            "Begin " & _
            pSQL & "; "
 
    pBanco.Parameters.Remove "p_Cod_Erro_Ora"
    pBanco.Parameters.Remove "p_Txt_Erro_Ora"
 
    pBanco.Parameters.Add "p_Cod_Erro_Ora", 0, 2
    pBanco.Parameters.Add "p_Txt_Erro_Ora", "", 2
 
    vPl = vPl & _
            "Exception " & _
            "When Others Then " & _
            "Rollback; " & _
            ":p_Cod_Erro_Ora := SqlCode; " & _
            ":p_Txt_Erro_Ora := SqlErrm; " & _
            "End;"
 
    pBanco.ExecuteSQL vPl
 
    If IsNull(pBanco.Parameters("p_Cod_Erro_Ora")) Or _
            pBanco.Parameters("p_Cod_Erro_Ora") = "" Or _
            pBanco.Parameters("p_Cod_Erro_Ora") = 0 Then
 
        ExecutaPL = ""
 
    Else
 
        ExecutaPL = CStr(pBanco.Parameters("p_Cod_Erro_Ora")) & " - " & CStr(pBanco.Parameters("p_Txt_Erro_Ora"))
 
    End If
 
    Exit Function
 
Erro:
 
    vErro = CStr(Err.Number) & " - " & CStr(Err.Description)
    ExecutaPL = vErro
    'Call ProcessaErro(vErro)
 
End Function


