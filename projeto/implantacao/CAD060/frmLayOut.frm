VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmLayOut 
   BackColor       =   &H00FFFFFF&
   Caption         =   "CAD060 - Lay-Out para Importa��o de Arquivos (Ativar/Desativar Cliente ou Alter.Mensagem)"
   ClientHeight    =   6705
   ClientLeft      =   120
   ClientTop       =   1320
   ClientWidth     =   11715
   LinkTopic       =   "Form1"
   ScaleHeight     =   6705
   ScaleWidth      =   11715
   Begin VB.Frame Frame2 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Ativar / Desativar Cliente"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3630
      Left            =   75
      TabIndex        =   6
      Top             =   15
      Width           =   8085
      Begin VB.TextBox Text3 
         BackColor       =   &H00C0E0FF&
         ForeColor       =   &H00C00000&
         Height          =   675
         Left            =   3405
         MultiLine       =   -1  'True
         TabIndex        =   10
         Text            =   "frmLayOut.frx":0000
         Top             =   2895
         Width           =   3975
      End
      Begin VB.TextBox Text1 
         BackColor       =   &H00C0E0FF&
         ForeColor       =   &H00C00000&
         Height          =   675
         Left            =   75
         MultiLine       =   -1  'True
         TabIndex        =   7
         Text            =   "frmLayOut.frx":0054
         Top             =   2895
         Width           =   3000
      End
      Begin MSFlexGridLib.MSFlexGrid grdflexSituacao 
         Height          =   2415
         Left            =   45
         TabIndex        =   8
         Top             =   240
         Width           =   7935
         _ExtentX        =   13996
         _ExtentY        =   4260
         _Version        =   393216
         Cols            =   6
         FixedCols       =   0
         BackColorBkg    =   14737632
         GridLines       =   2
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFFFFF&
         Caption         =   "Exemplo de Arquivo para Desativar:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   3420
         TabIndex        =   11
         Top             =   2670
         Width           =   3090
      End
      Begin VB.Label Label3 
         BackColor       =   &H00FFFFFF&
         Caption         =   "Exemplo de Arquivo para Ativar:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   90
         TabIndex        =   9
         Top             =   2670
         Width           =   2850
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Altera��o de Mensagem do Cliente"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2895
      Left            =   75
      TabIndex        =   2
      Top             =   3645
      Width           =   8070
      Begin VB.TextBox Text2 
         BackColor       =   &H00C0E0FF&
         ForeColor       =   &H00C00000&
         Height          =   675
         Left            =   75
         MultiLine       =   -1  'True
         TabIndex        =   3
         Text            =   "frmLayOut.frx":007C
         Top             =   2160
         Width           =   4620
      End
      Begin MSFlexGridLib.MSFlexGrid grdflexMensagem 
         Height          =   1665
         Left            =   45
         TabIndex        =   4
         Top             =   240
         Width           =   7905
         _ExtentX        =   13944
         _ExtentY        =   2937
         _Version        =   393216
         Cols            =   6
         FixedCols       =   0
         BackColorBkg    =   14737632
         GridLines       =   2
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label4 
         BackColor       =   &H00FFFFFF&
         Caption         =   "Exemplo de Arquivo para Alterar Mensagem do Cliente"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   90
         TabIndex        =   5
         Top             =   1935
         Width           =   4650
      End
   End
   Begin VB.TextBox txtObs 
      BackColor       =   &H00C0FFFF&
      Height          =   4215
      Left            =   8220
      MultiLine       =   -1  'True
      TabIndex        =   1
      Text            =   "frmLayOut.frx":00C1
      Top             =   120
      Width           =   3315
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   585
      Left            =   10965
      TabIndex        =   0
      ToolTipText     =   "Voltar"
      Top             =   5925
      Width           =   585
      _ExtentX        =   1032
      _ExtentY        =   1032
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmLayOut.frx":02D3
      PICN            =   "frmLayOut.frx":02EF
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmLayOut"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdVoltar_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    
    'Ocultar a barra de botoes do MDI
    mdiCAD060.SSPanel1.Visible = False
    
    Me.Top = mdiCAD060.Menu.Height * 2
    Me.Left = 50
    
    Me.Height = 7110

    With grdflexSituacao
        .Cols = 3
        .Rows = 7
        .ColWidth(0) = 2550:     .ColAlignment(0) = 0:   .TextMatrix(0, 0) = "Campo"
        .ColWidth(1) = 2550:     .ColAlignment(1) = 0:   .TextMatrix(0, 1) = "Tamanho"
        .ColWidth(2) = 2550:     .ColAlignment(2) = 0:   .TextMatrix(0, 2) = "Posi��o"
        .TextMatrix(1, 0) = "C�d.Cliente"
        .TextMatrix(1, 1) = "Numerico(6)"
        .TextMatrix(1, 2) = "01 a 06"
        .TextMatrix(2, 0) = "Situa��o"
        .TextMatrix(2, 1) = "Numerico(1)"
        .TextMatrix(2, 2) = "07 a 07"
        .TextMatrix(3, 0) = "C�d.Representante"
        .TextMatrix(3, 1) = "Numerico(4)"
        .TextMatrix(3, 2) = "08 a 11"
        .TextMatrix(4, 0) = "C�d.Mensagem"
        .TextMatrix(4, 1) = "Numerico(2)"
        .TextMatrix(4, 2) = "12 a 13"
        .TextMatrix(5, 0) = "Dt.Mensagem"
        .TextMatrix(5, 1) = "AlfaNumerico(8)"
        .TextMatrix(5, 2) = "14 a 21"
        .TextMatrix(6, 0) = "Respons�vel"
        .TextMatrix(6, 1) = "AlfaNumerico(12)"
        .TextMatrix(6, 2) = "22 a 33"
    End With
        
    With grdflexMensagem
        .Cols = 3
        .Rows = 5
        .ColWidth(0) = 2550:     .ColAlignment(0) = 0:   .TextMatrix(0, 0) = "Campo"
        .ColWidth(1) = 2550:     .ColAlignment(1) = 0:   .TextMatrix(0, 1) = "Tamanho"
        .ColWidth(2) = 2550:     .ColAlignment(2) = 0:   .TextMatrix(0, 2) = "Posi��o"
        .TextMatrix(1, 0) = "C�d.Cliente"
        .TextMatrix(1, 1) = "Numerico(6)"
        .TextMatrix(1, 2) = "01 a 06"
        .TextMatrix(2, 0) = "C�d.Mensagem"
        .TextMatrix(2, 1) = "Numerico(2)"
        .TextMatrix(2, 2) = "07 a 08"
        .TextMatrix(3, 0) = "Dt.Mensagem"
        .TextMatrix(3, 1) = "AlfaNumerico(8)"
        .TextMatrix(3, 2) = "09 a 16"
        .TextMatrix(4, 0) = "Respons�vel"
        .TextMatrix(4, 1) = "AlfaNumerico(12)"
        .TextMatrix(4, 2) = "17 a 28"
    End With
 
 End Sub


