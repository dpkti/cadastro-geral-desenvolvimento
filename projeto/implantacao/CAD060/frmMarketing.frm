VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Begin VB.Form frmMarketing 
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "CAD060 - Cadastro de Cliente - Dados de Marketing"
   ClientHeight    =   3930
   ClientLeft      =   45
   ClientTop       =   1545
   ClientWidth     =   10800
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3930
   ScaleWidth      =   10800
   Begin VB.Frame Frame6 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Cliente"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3300
      Left            =   30
      TabIndex        =   11
      Top             =   30
      Width           =   10755
      Begin VB.Frame Frame2 
         BackColor       =   &H00FFFFFF&
         Caption         =   "2� Contato"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2640
         Left            =   5475
         TabIndex        =   19
         Top             =   585
         Width           =   5235
         Begin VB.ComboBox cboCargo2 
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   990
            TabIndex        =   8
            Text            =   "99 - XXXXXXXXXXXXXXXXXXXX"
            Top             =   1110
            Width           =   2955
         End
         Begin VB.ComboBox cboTime2 
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   990
            TabIndex        =   10
            Text            =   "XXXX - XXXXXXXXXXXXXXXXXXXX"
            Top             =   2085
            Width           =   2955
         End
         Begin VB.ComboBox cboHobby2 
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   990
            TabIndex        =   9
            Text            =   "99 - XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
            Top             =   1605
            Width           =   3930
         End
         Begin VB.TextBox txtDt_Nasc2 
            Alignment       =   2  'Center
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   2070
            MaxLength       =   10
            TabIndex        =   7
            Text            =   "00/00/0000"
            Top             =   705
            Width           =   930
         End
         Begin VB.TextBox txtContato2 
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   975
            MaxLength       =   15
            TabIndex        =   6
            Text            =   "XXXXXXXXXXXXXXX"
            Top             =   300
            Width           =   2970
         End
         Begin VB.Label Label6 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "Time"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   270
            TabIndex        =   24
            Top             =   2070
            Width           =   420
         End
         Begin VB.Label Label7 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "Hobby"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   255
            TabIndex        =   23
            Top             =   1620
            Width           =   510
         End
         Begin VB.Label Label8 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "Cargo"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   255
            TabIndex        =   22
            Top             =   1110
            Width           =   495
         End
         Begin VB.Label Label9 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "Data de Nascimento"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   255
            TabIndex        =   21
            Top             =   705
            Width           =   1620
         End
         Begin VB.Label Label10 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "Nome"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   255
            TabIndex        =   20
            Top             =   300
            Width           =   480
         End
      End
      Begin VB.Frame Frame1 
         BackColor       =   &H00FFFFFF&
         Caption         =   "1� Contato"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2640
         Left            =   75
         TabIndex        =   13
         Top             =   585
         Width           =   5235
         Begin VB.TextBox txtContato1 
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   975
            MaxLength       =   15
            TabIndex        =   1
            Text            =   "XXXXXXXXXXXXXXX"
            Top             =   300
            Width           =   2970
         End
         Begin VB.TextBox txtdt_nasc1 
            Alignment       =   2  'Center
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   2070
            MaxLength       =   10
            TabIndex        =   2
            Text            =   "00/00/0000"
            Top             =   705
            Width           =   930
         End
         Begin VB.ComboBox cboHobby1 
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   990
            TabIndex        =   4
            Text            =   "99 - XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
            Top             =   1605
            Width           =   3930
         End
         Begin VB.ComboBox cboTime1 
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   990
            TabIndex        =   5
            Text            =   "XXXX - XXXXXXXXXXXXXXXXXXXX"
            Top             =   2085
            Width           =   2955
         End
         Begin VB.ComboBox cboCargo1 
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   990
            TabIndex        =   3
            Text            =   "99 - XXXXXXXXXXXXXXXXXXXX"
            Top             =   1110
            Width           =   2955
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "Nome"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   255
            TabIndex        =   18
            Top             =   300
            Width           =   480
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "Data de Nascimento"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   255
            TabIndex        =   17
            Top             =   705
            Width           =   1620
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "Cargo"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   255
            TabIndex        =   16
            Top             =   1110
            Width           =   495
         End
         Begin VB.Label Label4 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "Hobby"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   255
            TabIndex        =   15
            Top             =   1620
            Width           =   510
         End
         Begin VB.Label Label5 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "Time"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   270
            TabIndex        =   14
            Top             =   2070
            Width           =   420
         End
      End
      Begin VB.ComboBox cboCliente 
         BackColor       =   &H000040C0&
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         ItemData        =   "frmMarketing.frx":0000
         Left            =   1050
         List            =   "frmMarketing.frx":0002
         TabIndex        =   0
         Top             =   240
         Width           =   4095
      End
      Begin VB.TextBox txtCodCliente 
         Alignment       =   2  'Center
         BackColor       =   &H000040C0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Left            =   75
         Locked          =   -1  'True
         MaxLength       =   6
         TabIndex        =   12
         Top             =   240
         Width           =   975
      End
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   540
      Left            =   10200
      TabIndex        =   25
      ToolTipText     =   "Voltar"
      Top             =   3330
      Width           =   585
      _ExtentX        =   1032
      _ExtentY        =   953
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmMarketing.frx":0004
      PICN            =   "frmMarketing.frx":0020
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdCancelar 
      Height          =   540
      Left            =   9570
      TabIndex        =   26
      ToolTipText     =   "Cancelar"
      Top             =   3330
      Width           =   585
      _ExtentX        =   1032
      _ExtentY        =   953
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmMarketing.frx":079A
      PICN            =   "frmMarketing.frx":07B6
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdGravar 
      Height          =   540
      Left            =   8940
      TabIndex        =   27
      ToolTipText     =   "Gravar"
      Top             =   3330
      Width           =   585
      _ExtentX        =   1032
      _ExtentY        =   953
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmMarketing.frx":0C08
      PICN            =   "frmMarketing.frx":0C24
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmMarketing"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Private ValidaCpo As New Funcoes
Private vFl_Ativo As Boolean

Sub Desabilitar_Campos_Marketing()
    txtContato1.Enabled = False
    txtdt_nasc1.Enabled = False
    cboCargo1.Enabled = False
    cboHobby1.Enabled = False
    cboTime1.Enabled = False
    txtContato2.Enabled = False
    txtDt_Nasc2.Enabled = False
    cboCargo2.Enabled = False
    cboHobby2.Enabled = False
    cboTime2.Enabled = False
End Sub

Sub Habilitar_Campos_Marketing()
    txtContato1.Enabled = True
    txtdt_nasc1.Enabled = True
    cboCargo1.Enabled = True
    cboHobby1.Enabled = True
    cboTime1.Enabled = True
    txtContato2.Enabled = True
    txtDt_Nasc2.Enabled = True
    cboCargo2.Enabled = True
    cboHobby2.Enabled = True
    cboTime2.Enabled = True
End Sub

Sub Limpar_Campos_Marketing()
    txtCodCliente = ""
    cboCliente = ""
    txtContato1 = ""
    txtdt_nasc1 = ""
    cboCargo1 = ""
    cboHobby1 = ""
    cboTime1 = ""
    txtContato2 = ""
    txtDt_Nasc2 = ""
    cboCargo2 = ""
    cboHobby2 = ""
    cboTime2 = ""
End Sub

Sub pCarregaComboCliente(lcboCliente As ComboBox, lstrParametro As String)
    
    Dim lstrSQL      As String
    Dim lstrWHERE    As String
    Dim objCliente    As Object
    Dim lstrCliente   As String
    
    lstrWHERE = ""
    lcboCliente.Clear
    lstrParametro = UCase(lstrParametro)
    
    'NOME OU CODIGO
    
    If Trim(lstrParametro) <> "" Then
        
        If IsNumeric(Trim(lstrParametro)) Then
            
            If Len(Trim(lstrParametro)) > 6 Then ' cgc=14
                
                db.Parameters.Remove "CGC": db.Parameters.Add "CGC", lstrParametro, 1
                
                lstrWHERE = lstrWHERE & " cgc = :CGC"
                
                '---------- testa CGC / CIC -----------
                On Error Resume Next
                If Len(lstrParametro) <> 11 And Len(lstrParametro) <> 14 Then
                    MsgBox "CGC/CIC Inv�lido!!!", vbCritical, "Aten��o"
                    Exit Sub
                End If
                
                'Valida CGC ou CPF
                If Len(lstrParametro) > 11 Then
                    If Not vVB_Generica_001.ValidaCGC(lstrParametro) Then
                        MsgBox "CGC Inv�lido!!!", vbCritical, "Aten��o"
                        Exit Sub
                    End If
                Else
                    If Not vVB_Generica_001.ValidaCIC(lstrParametro) Then
                        MsgBox "CPF Inv�lido!!!", vbCritical, "Aten��o"
                        Exit Sub
                    End If
                End If
                
                cboCliente = ""
            
            Else ' codigo=6
                
                db.Parameters.Remove "CODCLI": db.Parameters.Add "CODCLI", Val(lstrParametro), 1
                lstrWHERE = lstrWHERE & " cod_cliente = :CODCLI "
            
            End If
        
        Else
            
            'SE NAO FOR NUMERICO E NAO TIVER '%'
            If Right(lstrParametro, 1) <> "%" And Left(lstrParametro, 1) <> "%" And Trim(txtCodCliente) = "" Then
                lcboCliente.Text = lstrParametro
                Exit Sub
            End If
            
            db.Parameters.Remove "NOME": db.Parameters.Add "NOME", lstrParametro, 1
            
            lstrWHERE = lstrWHERE & " nome_cliente " & _
                        IIf(Left(lstrParametro, 1) = "%" Or _
                           Right(lstrParametro, 1) = "%", " LIKE ", "=") & " :NOME "
        End If
    
    End If
    
   
    lstrSQL = "SELECT Cod_cliente, Nome_cliente " & _
                "FROM Producao.Cliente"
    
    lstrSQL = lstrSQL & IIf(lstrWHERE = "", "", " WHERE " & lstrWHERE)
    
    Set objCliente = db.dbcreatedynaset(lstrSQL, 0&)
    
    lcboCliente.Clear
    Screen.MousePointer = vbHourglass
    While Not objCliente.EOF
        'MDIForm1.sspMsg.Caption = "PESQUISANDO CLIENTES . . . " & lcboCliente.ListCount + 1
        lcboCliente.AddItem objCliente.Fields("nome_cliente") & " - " & Format(objCliente.Fields("cod_cliente"), "000000")
        lstrCliente = objCliente.Fields("nome_cliente") & " - " & Format(objCliente.Fields("cod_cliente"), "000000")
        objCliente.MoveNext
    Wend
    
    Screen.MousePointer = vbDefault
    If objCliente.RecordCount <= 1 Then
        If objCliente.RecordCount = 0 Then
            MsgBox "Cliente n�o Encontrado !", vbInformation, "Aten��o"
            If Not IsNumeric(lstrParametro) Then
                lcboCliente.Text = Left(lstrParametro, 30)
            Else
                lcboCliente.SetFocus
            End If
        Else
            objCliente.MoveFirst
            lcboCliente.Text = objCliente.Fields("nome_cliente")
            txtCodCliente = objCliente.Fields("cod_cliente")
            Call pVerSituacao
            If vFl_Ativo = False Then
                Exit Sub
            End If
            Call pCarregaDadosMkt
        End If
    Else
        MsgBox "Encontrei mais de um Cliente." & Chr(10) & "Selecione um !", vbInformation, "Aten��o"
    End If
End Sub

Sub pCarregaDadosMkt()
Dim objRecadastro As Object
                
                
    'Seleciona os dados de marketing j� cadastrados para este cliente
    Set OraParameters = db.Parameters
    OraParameters.Remove "CODCLI": OraParameters.Add "CODCLI", Val(txtCodCliente), 1
    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    Criar_Cursor OraParameters, "PM_CURSOR1"
    vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD060.Pr_Sel_DadosMktContato(:PM_CURSOR1, :CODCLI, :vErro, :vMsgErro)")
    If vErro = "" Then
        Set objRecadastro = OraParameters("PM_CURSOR1").Value
    Else
        Exit Sub
    End If

    txtContato1 = IIf(IsNull(objRecadastro!nome_contato1), "", objRecadastro!nome_contato1)
    txtContato2 = IIf(IsNull(objRecadastro!nome_contato2), "", objRecadastro!nome_contato2)
    txtdt_nasc1 = IIf(IsNull(objRecadastro!dt_nasc_contato1), "", objRecadastro!dt_nasc_contato1)
    txtDt_Nasc2 = IIf(IsNull(objRecadastro!dt_nasc_contato2), "", objRecadastro!dt_nasc_contato2)
    cboCargo1 = objRecadastro!cod_tipo_contato1 & " - " & objRecadastro!desc1
    cboCargo2 = objRecadastro!cod_tipo_contato2 & " - " & objRecadastro!desc2
    cboHobby1 = objRecadastro!cod_hobby_contato1 & " - " & objRecadastro!hobby1
    cboHobby2 = objRecadastro!cod_hobby_contato2 & " - " & objRecadastro!hobby2
    cboTime1 = objRecadastro!cod_time_contato1 & " - " & objRecadastro!time1
    cboTime2 = objRecadastro!cod_time_contato2 & " - " & objRecadastro!time2

End Sub

Sub pVerSituacao()

    vFl_Ativo = True
    
    'Verifica situa��o do cliente
    Set OraParameters = db.Parameters
    OraParameters.Remove "CODCLI": OraParameters.Add "CODCLI", Val(txtCodCliente), 1
    OraParameters.Remove "CGC": OraParameters.Add "CGC", 0, 1
    OraParameters.Remove "TP_CONS": OraParameters.Add "TP_CONS", 2, 1
    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    Criar_Cursor OraParameters, "PM_CURSOR1"
    vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD060.Pr_Verifica_Cliente(:PM_CURSOR1, :CODCLI, :CGC,:TP_CONS, :vErro, :vMsgErro)")
    If vErro <> "" Then
        Exit Sub
    End If
    Set rst = OraParameters("PM_CURSOR1").Value

    If rst.EOF Then
        MsgBox "Cliente n�o encontrado!", vbInformation, "CAD060 - Aten��o"
        vFl_Ativo = False
        Exit Sub
    ElseIf Val(rst!Situacao.Value) = 9 Then
        MsgBox "Cliente Desativado. Cadastro n�o pode ser feito!", vbInformation, "CAD060 - Aten��o"
        vFl_Ativo = False
        Limpar_Campos_Marketing
        Desabilitar_Campos_Marketing
        Exit Sub
    End If

End Sub

Private Sub cboCliente_Click()
    Dim vNomeCli As String, vVezes As Byte
        
    If Not IsNumeric(Right(cboCliente, 6)) Then Exit Sub
    
    txtCodCliente.Text = Right(cboCliente, 6)
    vNomeCli = Left(cboCliente, Len(cboCliente) - 9)
        
    'Verificar se cliente est� ativo
    Call pVerSituacao
    
    If vFl_Ativo = False Then
        Exit Sub
    End If
    
    Call pCarregaDadosMkt
    
    cboCliente.Clear
    cboCliente.AddItem vNomeCli
    cboCliente.ListIndex = 0

End Sub




Private Sub cboCliente_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)

End Sub


Private Sub cboCliente_LostFocus()
    cboCliente.Text = UCase(Left(cboCliente.Text, 30))
    
    If cboCliente <> "" Then
        If Trim(txtCodCliente) = "" Then
            Call pCarregaComboCliente(frmMarketing.cboCliente, cboCliente.Text)
        End If
    End If

End Sub

Private Sub cmdCancelar_Click()
    Limpar_Campos_Marketing
    Habilitar_Campos_Marketing
End Sub

Private Sub cmdGravar_Click()
Dim mSql As String
    
    'Muniz 10/02/10 valida branco txtContatos
    If Not ValidaBranco(txtContato1) Then
        MsgBox "Existe mais de um espa�o em branco no campo nome, 1o contato.", vbCritical, "CAD060 - Aten��o"
        txtContato1.SetFocus
        Exit Sub
    End If
    
    If Not ValidaBranco(txtContato2) Then
        MsgBox "Existe mais de um espa�o em branco no campo nome, 2o contato.", vbCritical, "CAD060 - Aten��o"
        txtContato2.SetFocus
        Exit Sub
    End If
    'fim Muniz
     
     
'//------------------------------------------------------------------------
'  Grava Dados de Recadastro de MKT - CAD780

    If Trim(txtCodCliente) = "" Then
        MsgBox "Cliente � campo obrigat�rio!", vbCritical, "CAD060 - Aten��o"
        cboCliente.SetFocus
        Exit Sub
    End If

    Set OraParameters = db.Parameters

    With OraParameters
        .Remove "cod_cliente":  .Add "cod_Cliente", txtCodCliente, 1
        .Remove "nome1":        .Add "nome1", txtContato1, 1
        .Remove "nome2":        .Add "nome2", txtContato2, 1
        .Remove "cod_contato1": .Add "cod_contato1", IIf(Len(cboCargo1) = 0, 0, Trim(Left(cboCargo1, 2))), 1
        .Remove "cod_contato2": .Add "cod_contato2", IIf(Len(cboCargo2) = 0, 0, Trim(Left(cboCargo2, 2))), 1
        .Remove "dt_nasc1":     .Add "dt_nasc1", txtdt_nasc1, 1
        .Remove "dt_nasc2":     .Add "dt_nasc2", txtDt_Nasc2, 1
        .Remove "cod_hobby1":   .Add "cod_hobby1", IIf(Len(cboHobby1) = 0, 0, Trim(Left(cboHobby1, 2))), 1
        .Remove "cod_hobby2":   .Add "cod_hobby2", IIf(Len(cboHobby2) = 0, 0, Trim(Left(cboHobby2, 2))), 1
        .Remove "cod_time1":    .Add "cod_time1", IIf(Len(cboTime1) = 0, 0, Trim(Left(cboTime1, 2))), 1
        .Remove "cod_time2":    .Add "cod_time2", IIf(Len(cboTime2) = 0, 0, Trim(Left(cboTime2, 2))), 1
        .Remove "vErro":        .Add "vErro", 0, 2
        .Remove "vMsgErro":     .Add "vMsgErro", "", 2
    End With
         
    mSql = "BEGIN " & _
                "PRODUCAO.PCK_CLIENTE.PR_GRAVA_RECADASTRO " & _
                "(:COD_CLIENTE, :NOME1, :NOME2, :COD_CONTATO1, :COD_CONTATO2, " & _
                " TO_DATE(:DT_NASC1,'DD/MM/RR'), TO_DATE(:DT_NASC2,'DD/MM/RR'), " & _
                " :COD_HOBBY1, :COD_HOBBY2, :COD_TIME1, :COD_TIME2); " & _
           "END;"
    db.ExecuteSQL mSql

    If Val(IIf(IsNull(db.Parameters("vErro")), 0, db.Parameters("vErro"))) <> 0 Then
        MsgBox "Erro " & db.Parameters("vErro") & "-" & db.Parameters("vMsgErro") & ". Avise o Depto.Suporte!", vbCritical, "CAD060 - Aten��o"
        Screen.MousePointer = vbNormal
        Exit Sub
    End If

    MsgBox "Informa��es de Marketing de Cliente Gravadas OK!"

'------------------------------------------------------------------------ //

End Sub

Private Sub cmdVoltar_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    
    Dim SEL_CARGO As String, SEL_hobby As String, SEL_time As String
    Dim objCargo As Object, objTime As Object, objHobby As Object

    'Ocultar a barra de botoes do MDI
    mdiCAD060.SSPanel1.Visible = False
    Me.Top = mdiCAD060.Menu.Height * 2
    Me.Left = 50
    
    Limpar_Campos_Marketing
    Habilitar_Campos_Marketing
    
    'Carregar combo de cargo
    Set OraParameters = db.Parameters
    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    Criar_Cursor OraParameters, "vCursor"
    'Vai fazer select * na tabela marketing.tipo_contato
    vErro = vVB_Generica_001.ExecutaPl(db, "Producao.PCK_CAD060.Pr_Sel_MktTpContato(:vCursor,:vErro,:vMsgErro)")
    If vErro <> "" Then
        Exit Sub
    End If
    
    Set objCargo = OraParameters("vCursor").Value
    vVB_Generica_001.PreencheComboList objCargo, cboCargo1, "Cod_Tipo_Contato", "Desc_Contato"
    objCargo.MoveFirst
    
    vVB_Generica_001.PreencheComboList objCargo, cboCargo2, "Cod_Tipo_Contato", "Desc_Contato"

    'Carregar combo de hobby
    'Vai fazer select * na tabela marketing.hobby_Cliente
    Criar_Cursor OraParameters, "vCursor"
    vErro = vVB_Generica_001.ExecutaPl(db, "Producao.PCK_CAD060.Pr_Sel_MktHobby(:vCursor,:vErro,:vMsgErro)")
    If vErro <> "" Then
        Exit Sub
    End If
    Set objHobby = OraParameters("vCursor").Value
    vVB_Generica_001.PreencheComboList objHobby, cboHobby1, "Cod_Hobby", "Desc_Hobby"
    objHobby.MoveFirst
    vVB_Generica_001.PreencheComboList objHobby, cboHobby2, "Cod_Hobby", "Desc_Hobby"

    'Carregar combo de time de futebol
    'Vai fazer select * na tabela marketing.time_Futebol
    Criar_Cursor OraParameters, "vCursor"
    vErro = vVB_Generica_001.ExecutaPl(db, "Producao.PCK_CAD060.Pr_Sel_MktTime(:vCursor,:vErro,:vMsgErro)")
    If vErro <> "" Then
        Exit Sub
    End If
    Set objTime = OraParameters("vCursor").Value
    
    vVB_Generica_001.PreencheComboList objTime, cboTime1, "Cod_Time", "Desc_Time"
    objTime.MoveFirst
    vVB_Generica_001.PreencheComboList objTime, cboTime2, "Cod_Time", "Desc_Time"


'SEL_CARGO = " Select * from MARKETING.tipo_contato"
'Set oradynaset = oradatabase.dbcreatedynaset(SEL_CARGO, 0&)
'cboCargo1.Clear
'Do While Not oradynaset.EOF
'   cboCargo1.AddItem oradynaset!cod_tipo_contato & " - " & oradynaset!desc_contato
'   oradynaset.MoveNext
'Loop
'cboCargo1.Refresh

'Carregar combo de hobby
'SEL_hobby = " Select * from MARKETING.hobby_cliente"
'Set oradynaset = oradatabase.dbcreatedynaset(SEL_hobby, 0&)
'cboHobby1.Clear
'Do While Not oradynaset.EOF
'   cboHobby1.AddItem oradynaset!cod_hobby & " - " & oradynaset!desc_hobby
'   oradynaset.MoveNext
'Loop
'cboHobby1.Refresh

'Carregar combo de time de futebol
'SEL_time = " Select * from MARKETING.time_futebol"
'Set oradynaset = oradatabase.dbcreatedynaset(SEL_time, 0&)
'cboTime1.Clear
'Do While Not oradynaset.EOF
'   cboTime1.AddItem oradynaset!cod_time & " - " & oradynaset!desc_time
'   oradynaset.MoveNext
'Loop
'cboTime1.Refresh


End Sub

Private Sub txtContato1_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
    KeyAscii = ValidaAlfaNumerico(KeyAscii)
End Sub

Private Sub txtContato2_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
    KeyAscii = ValidaAlfaNumerico(KeyAscii)
End Sub

Private Sub txtdt_nasc1_KeyPress(KeyAscii As Integer)
    vVB_Generica_001.Data KeyAscii, txtdt_nasc1
End Sub

Private Sub txtDt_Nasc2_KeyPress(KeyAscii As Integer)
    vVB_Generica_001.Data KeyAscii, txtDt_Nasc2
End Sub


