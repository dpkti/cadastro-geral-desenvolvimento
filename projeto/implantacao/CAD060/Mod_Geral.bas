Attribute VB_Name = "Mod_Geral"
Option Explicit

Public ECHO As ICMP_ECHO_REPLY
Public SUCCESS As Long

'sds nfe dpk
Public vCharEspeciais As String

Public Sub Valida_IE(CampoIE As Object, CampoUF As Object)

    '// --------------------------------------------
    ' Rotina para Valida��o de Inscri��o Estadual
    ' Para validar a Inscri��o ter que ser <> ISENTO
    '-------------------------------------------- //
    Dim lstrSQL
    Dim VALOR_IE
    
    Fl_Invalida = "N"
    Set OraParameters = db.Parameters
    
    If Trim(CampoIE.Text) = "ISENTA" Then
        CampoIE.Text = "ISENTO"
    End If
    
    If Trim(CampoIE.Text) <> "ISENTO" And Not IsNull(CampoIE) Then
        
        If CampoUF.Text <> "AC" And CampoUF.Text <> "RO" Then
            OraParameters.Remove "DT_IE": OraParameters.Add "DT_IE", "      ", 1
        Else
            If Len(CampoIE) = 9 And CampoUF.Text = "AC" Then
                OraParameters.Remove "DT_IE": OraParameters.Add "DT_IE", "101999", 1
            ElseIf Len(CampoIE) = 13 And CampoUF.Text = "AC" Then
                OraParameters.Remove "DT_IE": OraParameters.Add "DT_IE", "121999", 1
            End If
      
            If Len(CampoIE) = 9 And CampoUF.Text = "RO" Then
                OraParameters.Remove "DT_IE": OraParameters.Add "DT_IE", "062000", 1
            ElseIf Len(CampoIE) = 14 And CampoUF.Text = "RO" Then
                OraParameters.Remove "DT_IE": OraParameters.Add "DT_IE", "092000", 1
            End If
    
            If Len(CampoIE) > 14 Then
                MsgBox "INSCRI��O ESTADUAL INV�LIDA!", 16, "Aten��o!"
                Exit Sub
            End If
        End If
    
        '//---------------------------------------------------------------------
        'Procedure de Valida��o da Inscri��o Estadual desenvolvida pela Alethea
        '---------------------------------------------------------------------//
        
        Sql = "BEGIN PRODUCAO.PCK_IE.PR_IE(:UF,:DT_IE,:IE,:IE_VALIDA); END; "
                
        OraParameters.Remove "UF": OraParameters.Add "UF", CampoUF.Text, 1
        OraParameters.Remove "IE": OraParameters.Add "IE", CampoIE.Text, 1
        OraParameters.Remove "IE_VALIDA": OraParameters.Add "IE_VALIDA", 0, 3
        db.ExecuteSQL Sql
    
        VALOR_IE = OraParameters("IE_VALIDA")
    
        '// -----------------------------
        'Resultados de Retorno da Fun��o:
        '0 = IE ok
        '1 = IE n�o ok
        '------------------------------//
        
        If VALOR_IE = 1 Then
           Screen.MousePointer = 0
            'MsgBox "INSCRI��O ESTADUAL INV�LIDA!", 16, "Aten��o!"
            CampoIE.SetFocus
            Fl_Invalida = "S"
            Exit Sub
        Else
            Screen.MousePointer = 0
            Fl_Invalida = "N"
            'MsgBox "INSCRI��O ESTADUAL OK!!!!!", 16, "Aten��o!"
            Exit Sub
        End If
    
        Screen.MousePointer = 0
     
    End If

    Exit Sub

End Sub

Public Sub Criar_Cursor(Banco As Object, pNomeCursor As String)
    Banco.Remove pNomeCursor
    Banco.Add pNomeCursor, 0, 3
    Banco(pNomeCursor).ServerType = 102
    Banco(pNomeCursor).DynasetOption = &H2&
    Banco(pNomeCursor).DynasetCacheParams 256, 16, 20, 2000, 0
End Sub
  
Public Function fCharEspeciais()
    
    'teste vcd
    vCd = "01"
    
    OraParameters.Remove "PM_CD"
    OraParameters.Remove "PM_CARACTERES"

    OraParameters.Add "PM_CD", vCd, 1
    OraParameters.Add "PM_CARACTERES", "", 2
    
    db.ExecuteSQL "BEGIN PRODUCAO.PR_SELECT_CARACT_ESP_NFE(:PM_CD, :PM_CARACTERES); END;"

    fCharEspeciais = OraParameters("PM_CARACTERES").Value

End Function

Public Function ValidaBranco(pTXT As Object) As Boolean
        
    Dim vbranco As Byte
    Dim I  As Long
    
    'Atribui true na fun��o
    ValidaBranco = True
    
    'Valida se existe mais de um espa�o em branco nos campos
    For I = 1 To Len(pTXT)
        If Asc(Mid(pTXT, I, 1)) = 32 Then
            vbranco = vbranco + 1
        Else
            vbranco = 0
        End If
        If vbranco >= 2 Then
            ValidaBranco = False
            Exit Function
        End If
    Next
 
End Function


Function ValidaLetra(ByVal KeyAscii As Integer) As Integer

    On Error GoTo Trata_Erro
    
    If KeyAscii = 8 Or KeyAscii = 32 Or KeyAscii = 13 Then

        ValidaLetra = KeyAscii
        Exit Function

    End If

    If KeyAscii < 65 Or KeyAscii > 90 Then

        KeyAscii = 0
        
    End If

    ValidaLetra = KeyAscii
    
    Exit Function

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Function ValidaLetra" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description, "Aten��o", &H40000
    End If

End Function

Function ValidaNumero(ByVal KeyAscii As Integer) As Integer

On Error GoTo Trata_Erro

    If KeyAscii = 8 Or KeyAscii = 13 Then

        ValidaNumero = KeyAscii
        Exit Function

    End If

    If Chr(KeyAscii) < "0" Or Chr(KeyAscii) > "9" Then

        KeyAscii = 0

    End If

    ValidaNumero = KeyAscii

Exit Function

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Function ValidaNumero" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description, "Aten��o", &H40000
    End If
End Function

Function ValidaAlfaNumerico(ByVal KeyAscii As Integer) As Integer

On Error GoTo Trata_Erro

    If KeyAscii = 8 Or KeyAscii = 32 Or KeyAscii = 13 Then

        ValidaAlfaNumerico = KeyAscii
        Exit Function

    End If
    
    If InStr(1, vCharEspeciais, Chr(KeyAscii)) > 0 Then
        
        KeyAscii = 0

    End If

    ValidaAlfaNumerico = KeyAscii

Exit Function

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Function ValidaAlfaNumerico" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description, "Aten��o", &H40000
    End If
End Function

Public Function ValidaMaiuscula(pKeyascii As Integer) As Integer

    ValidaMaiuscula = Asc(UCase(Chr(pKeyascii)))

    Exit Function

End Function


Function ValidaTamanho(pTXT As TextBox, pTamanho As Integer) As Boolean
    
    'Atribui true na fun��o
    ValidaTamanho = True
    
    'Valida o tamnho do campo
    If Len(pTXT) < pTamanho Then
        ValidaTamanho = False
    End If
 
End Function

