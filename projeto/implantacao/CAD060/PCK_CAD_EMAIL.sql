create or replace package PCK_CAD_EMAIL is

  -- Author  : EDUARDO.RELVAS
  -- Created : 21-mar-06 09:42:18
  -- Purpose : Cadastro de Email a partir de outro sistema

  -- Public type declarations
  type TP_CURSOR is REF CURSOR;

  Procedure PR_SELECT_DATA_CADASTRO (
                                    PM_CURSOR IN OUT TP_CURSOR
                                    );
     Procedure PR_INCLUIR_EMAIL (
                                 PM_COD_CLIENTE  IN NUMBER,
                                 PM_SEQUENCIA    IN NUMBER,
                                 PM_EMAIL        IN VARCHAR2,
                                 PM_COD_DEPTO    IN NUMBER,
                                 PM_NOME_CONTATO IN VARCHAR2
                                 );

  Procedure PR_SELECT_DEPTO (
                              PM_CURSOR IN OUT TP_CURSOR
                              );
   Procedure PR_EXCLUIR_EMAIL (
                               PM_COD_CLIENTE IN NUMBER,
                               PM_SEQUENCIA   IN NUMBER
                              );
   Procedure PR_SELECT_EMAIL (
                              PM_CURSOR IN OUT TP_CURSOR,
                              PM_COD_CLIENTE IN Number
                             );
   Procedure PR_SELECT_NOME_CLIENTE (
                                    PM_CURSOR IN OUT TP_CURSOR,
                                    PM_COD_CLIENTE IN NUMBER
                                    );
  Procedure PR_ALTERAR_EMAIL (
                             PM_COD_CLIENTE IN NUMBER,
                             PM_SEQUENCIA IN NUMBER,
                             PM_EMAIL IN VARCHAR2,
                             PM_COD_DEPTO IN NUMBER,
                             PM_NOME_CONTATO IN VARCHAR2
                             );
  Procedure PR_RESEQUENCIAR (
                            PM_COD_CLIENTE IN NUMBER
                            );
  Procedure PR_SELECT_SEQUENCIA (PM_CURSOR IN OUT TP_CURSOR, PM_COD_CLIENTE IN NUMBER);
end PCK_CAD_EMAIL;
/
create or replace package body PCK_CAD_EMAIL is
     Procedure PR_SELECT_DATA_CADASTRO (PM_CURSOR IN OUT TP_CURSOR) is
        BEGIN
             OPEN PM_CURSOR FOR
                  SELECT dt_faturamento from datas;
     End;

     Procedure PR_INCLUIR_EMAIL (
                                 PM_COD_CLIENTE  IN NUMBER,
                                 PM_SEQUENCIA    IN NUMBER,
                                 PM_EMAIL        IN VARCHAR2,
                                 PM_COD_DEPTO    IN NUMBER,
                                 PM_NOME_CONTATO IN VARCHAR2
                                 ) IS
          BEGIN

            UPDATE R_CLIENTE_EMAIL
            SET    EMAIL = PM_EMAIL,
                   NOME_CONTATO = PM_NOME_CONTATO,
                   DT_EMAIL = TO_DATE(sysdate, 'DD/MM/RR HH24:MI:SS'),
                   COD_DEPTO = PM_COD_DEPTO
            WHERE  COD_CLIENTE = PM_COD_CLIENTE AND
                   SEQUENCIA = PM_SEQUENCIA;
            if SQL%NOTFOUND then
               INSERT INTO R_CLIENTE_EMAIL
               VALUES (PM_COD_CLIENTE,
                       PM_SEQUENCIA,
                       PM_EMAIL,
                       PM_COD_DEPTO,
                       PM_NOME_CONTATO,
                       to_date(sysdate,'DD-MON-RR HH24:MI:SS'));
             END IF;
             COMMIT;
      End;

   Procedure PR_SELECT_DEPTO (
                              PM_CURSOR IN OUT TP_CURSOR
                              ) is
      BEGIN
           OPEN PM_CURSOR FOR
                SELECT COD_DEPTO, DESCRICAO
                FROM   INTRANET.DEPARTAMENTO
                WHERE  COD_DEPTO IN (19,54) order by descricao;

   End;

   Procedure PR_EXCLUIR_EMAIL (
                               PM_COD_CLIENTE IN NUMBER,
                               PM_SEQUENCIA   IN NUMBER
                              ) is
      BEGIN
            DELETE
            FROM   R_CLIENTE_EMAIL
            WHERE  COD_CLIENTE = PM_COD_CLIENTE AND
                   SEQUENCIA = PM_SEQUENCIA;
            COMMIT;
   End;

   Procedure PR_SELECT_EMAIL (
                              PM_CURSOR IN OUT TP_CURSOR,
                              PM_COD_CLIENTE IN Number
                             ) is
      BEGIN
           OPEN PM_CURSOR FOR
              SELECT A.COD_CLIENTE, C.NOME_CLIENTE, A.SEQUENCIA, A.EMAIL, A.COD_DEPTO, B.DESCRICAO, A.NOME_CONTATO, A.DT_EMAIL
              FROM   R_CLIENTE_EMAIL A, INTRANET.DEPARTAMENTO B, CLIENTE C
              WHERE  A.COD_DEPTO = B.COD_DEPTO AND
                     A.COD_CLIENTE = C.COD_CLIENTE AND
                     A.COD_CLIENTE = PM_COD_CLIENTE;
   End;


   Procedure PR_SELECT_NOME_CLIENTE (
                                    PM_CURSOR IN OUT TP_CURSOR,
                                    PM_COD_CLIENTE IN NUMBER
                                    ) IS
      BEGIN
           OPEN PM_CURSOR FOR
                SELECT NOME_CLIENTE FROM CLIENTE WHERE COD_CLIENTE = PM_COD_CLIENTE;
   End;

  Procedure PR_ALTERAR_EMAIL (
                             PM_COD_CLIENTE IN NUMBER,
                             PM_SEQUENCIA IN NUMBER,
                             PM_EMAIL IN VARCHAR2,
                             PM_COD_DEPTO IN NUMBER,
                             PM_NOME_CONTATO IN VARCHAR2
                             ) is
     BEGIN
            UPDATE R_CLIENTE_EMAIL
            SET    EMAIL = PM_EMAIL,
                   NOME_CONTATO = PM_NOME_CONTATO,
                   DT_EMAIL = TO_DATE(sysdate, 'DD/MM/RR HH24:MI:SS'),
                   COD_DEPTO = PM_COD_DEPTO
            WHERE  COD_CLIENTE = PM_COD_CLIENTE AND
                   SEQUENCIA = PM_SEQUENCIA;

            COMMIT;
  END;

  Procedure PR_RESEQUENCIAR (
                            PM_COD_CLIENTE IN NUMBER
                            ) is

     vSeq Number(2):=1;
     vSeq2 Number(2):=0;

     CURSOR cCURSOR IS
            SELECT Sequencia FROM R_CLIENTE_EMAIL WHERE COD_CLIENTE = PM_COD_CLIENTE order by SEQUENCIA;

     BEGIN

--		      OPEN cCursor ;
			    FOR i IN cCursor LOOP

              UPDATE R_CLIENTE_EMAIL
              SET    SEQUENCIA = vSeq
              WHERE  COD_CLIENTE = PM_COD_CLIENTE AND
                     SEQUENCIA = i.sequencia;

              commit;

              vSeq := vSeq + 1;

          END LOOP ;
  End;

  Procedure PR_SELECT_SEQUENCIA (PM_CURSOR IN OUT TP_CURSOR, PM_COD_CLIENTE IN NUMBER) is
     BEGIN
          OPEN PM_CURSOR FOR
            SELECT max(SEQUENCIA) Seq
            FROM   R_CLIENTE_EMAIL
            WHERE  COD_CLIENTE = PM_COD_CLIENTE;
  End;

end PCK_CAD_EMAIL;
/
