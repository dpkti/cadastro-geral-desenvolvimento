PROCEDURE PR_GRAVA_CLIENTE(p_cgc	   IN     cliente.cgc%TYPE,
   		           p_tp_docto      IN     cliente.tp_docto%TYPE,
   		           p_nome_cliente  IN     cliente.nome_cliente%TYPE,
   		           p_endereco      IN     cliente.endereco%TYPE,
   		           p_cod_cidade    IN     cidade.cod_cidade%TYPE,
   		           p_bairro        IN     cliente.bairro%TYPE,
   		           p_ddd1          IN     cliente.ddd1%TYPE,
   		           p_fone1         IN     cliente.fone1%TYPE,
   		           p_cep           IN     cliente.cep%TYPE,
   		           p_inscr_estadual IN    cliente.inscr_estadual%TYPE,
   		           p_dt_cadastr    IN     cliente.dt_cadastr%TYPE,
   		           p_proprietario  IN     cliente.nome_proprietario%TYPE,
   		           p_fl_cons_final IN     cliente.fl_cons_final%TYPE,
   		           p_dt_fundacao   IN     cliente.dt_fundacao%TYPE,
   		           p_erro          OUT    NUMBER)


IS
   w_cod_cliente	cliente.cod_cliente%TYPE;
   w_SQL		varchar2(5000);

  BEGIN

    p_erro := 0;


    INSERT INTO cliente (Cod_cliente, Cgc, Tp_docto, Nome_cliente,Nome_contato,
        Classificacao, Status, caracteristica, Endereco, Cod_cidade, Bairro, Ddd1,
        Fone1, Ddd2, Fone2, Cep, Cxpostal, Telex, Fax, Inscr_estadual,Inscr_suframa,
        Dt_Cadastr,Cod_tipo_cliente, Cod_transp, Cod_repr_vend, Cod_repres_blau,
        Cod_banco, Tp_ordem_pgto,Situacao, Cod_mensagem, Nome_respons,
        Cod_ctr_protesto, Dt_fundacao, Propriedade, Cheque_sf, Cod_tipo_clblau,
        Nome_proprietario, Fl_Cons_Final,dt_mensagem )
        VALUES(Seq_cliente.NextVal,
        p_Cgc,p_Tp_docto,p_Nome_cliente,NULL,'X',1,
        'N',p_Endereco,p_Cod_cidade,p_Bairro,p_Ddd1,p_Fone1,0,0,
        p_Cep,0,0,0,p_Inscr_estadual,null,
        TO_DATE(p_Dt_Cadastr,'DD/MM/RR'),'15',999,NULL,
        0,999,null,0,0,null,null,TO_DATE(p_Dt_Cadastr,'DD/MM/RR'),'S',
        0,0,p_nome_cliente,'N',null);
     SELECT Seq_cliente.Currval INTO w_cod_cliente FROM dual;
     INSERT INTO clie_creden (cod_cliente,fl_creden) VALUES(w_cod_cliente,'N');
  



    EXCEPTION
    WHEN OTHERS THEN
    p_erro := SQLCODE;


END;
