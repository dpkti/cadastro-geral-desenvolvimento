VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{0F0877EF-2A93-4AE6-8BA8-4129832C32C3}#230.0#0"; "SMARTMENUXP.OCX"
Begin VB.MDIForm mdiCAD060 
   AutoShowChildren=   0   'False
   BackColor       =   &H00FFFFFF&
   Caption         =   "CAD060 - Cadastro/An�lise de Clientes"
   ClientHeight    =   5745
   ClientLeft      =   135
   ClientTop       =   1830
   ClientWidth     =   8520
   LinkTopic       =   "MDIForm1"
   WindowState     =   2  'Maximized
   Begin Threed.SSPanel SSPanel1 
      Align           =   1  'Align Top
      Height          =   645
      Left            =   0
      TabIndex        =   0
      Top             =   375
      Width           =   8520
      _Version        =   65536
      _ExtentX        =   15028
      _ExtentY        =   1138
      _StockProps     =   15
      BackColor       =   -2147483638
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BevelOuter      =   0
      Begin Bot�o.cmd cmdCadCliente 
         Height          =   600
         Left            =   30
         TabIndex        =   1
         ToolTipText     =   "Cadastro de Cliente"
         Top             =   30
         Width           =   600
         _ExtentX        =   1058
         _ExtentY        =   1058
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiCAD060.frx":0000
         PICN            =   "mdiCAD060.frx":001C
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   1
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdPreCadastro 
         Height          =   600
         Left            =   3360
         TabIndex        =   2
         ToolTipText     =   "An�lise Web - Cadastro"
         Top             =   30
         Width           =   600
         _ExtentX        =   1058
         _ExtentY        =   1058
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiCAD060.frx":0CF6
         PICN            =   "mdiCAD060.frx":0D12
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   1
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdLog 
         Height          =   600
         Left            =   6000
         TabIndex        =   3
         ToolTipText     =   "An�lise Web - Log"
         Top             =   30
         Width           =   600
         _ExtentX        =   1058
         _ExtentY        =   1058
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiCAD060.frx":19EC
         PICN            =   "mdiCAD060.frx":1A08
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   1
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdPreAlteracao 
         Height          =   600
         Left            =   4020
         TabIndex        =   4
         ToolTipText     =   "An�lise Web - Altera��o"
         Top             =   30
         Width           =   600
         _ExtentX        =   1058
         _ExtentY        =   1058
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiCAD060.frx":22E2
         PICN            =   "mdiCAD060.frx":22FE
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   1
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdPreRecadastro 
         Height          =   600
         Left            =   4680
         TabIndex        =   5
         ToolTipText     =   "An�lise Web - Recadastro"
         Top             =   30
         Width           =   600
         _ExtentX        =   1058
         _ExtentY        =   1058
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiCAD060.frx":2FD8
         PICN            =   "mdiCAD060.frx":2FF4
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   1
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdInformacoes 
         Height          =   600
         Left            =   10545
         TabIndex        =   6
         ToolTipText     =   "Informa��es do Sistema"
         Top             =   30
         Width           =   600
         _ExtentX        =   1058
         _ExtentY        =   1058
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiCAD060.frx":3CCE
         PICN            =   "mdiCAD060.frx":3CEA
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   1
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdSair 
         Height          =   600
         Left            =   11250
         TabIndex        =   7
         ToolTipText     =   "Sair do Sistema"
         Top             =   30
         Width           =   600
         _ExtentX        =   1058
         _ExtentY        =   1058
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiCAD060.frx":45C4
         PICN            =   "mdiCAD060.frx":45E0
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   1
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdAtivarDesativar 
         Height          =   600
         Left            =   690
         TabIndex        =   8
         ToolTipText     =   "Ativar/Desativar"
         Top             =   30
         Width           =   600
         _ExtentX        =   1058
         _ExtentY        =   1058
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiCAD060.frx":A87A
         PICN            =   "mdiCAD060.frx":A896
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   1
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdCadMotivos 
         Height          =   600
         Left            =   1350
         TabIndex        =   10
         ToolTipText     =   "Cadastro de Motivos de Recusa"
         Top             =   30
         Width           =   600
         _ExtentX        =   1058
         _ExtentY        =   1058
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiCAD060.frx":ABB0
         PICN            =   "mdiCAD060.frx":ABCC
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   1
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdConsulta 
         Height          =   600
         Left            =   5340
         TabIndex        =   11
         ToolTipText     =   "Consultar Solicita��es"
         Top             =   30
         Width           =   600
         _ExtentX        =   1058
         _ExtentY        =   1058
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiCAD060.frx":B8A6
         PICN            =   "mdiCAD060.frx":B8C2
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   1
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdResponsavel 
         Height          =   600
         Left            =   6675
         TabIndex        =   12
         ToolTipText     =   "Consulta/Manuten��o de Usu�rios Respons�veis"
         Top             =   30
         Width           =   600
         _ExtentX        =   1058
         _ExtentY        =   1058
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiCAD060.frx":BA1C
         PICN            =   "mdiCAD060.frx":BA38
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   1
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdCadVisaoUsuario 
         Height          =   600
         Left            =   2010
         TabIndex        =   13
         ToolTipText     =   "Cadastro de Visao Usu�rio X Clientes"
         Top             =   30
         Width           =   600
         _ExtentX        =   1058
         _ExtentY        =   1058
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiCAD060.frx":C712
         PICN            =   "mdiCAD060.frx":C72E
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   1
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
   End
   Begin VBSmartXPMenu.SmartMenuXP Menu 
      Align           =   1  'Align Top
      Height          =   375
      Left            =   0
      Top             =   0
      Width           =   855
      _ExtentX        =   1508
      _ExtentY        =   661
      BackColor       =   -2147483624
      FontForeColor   =   0
      CheckBackColor  =   16761024
      SelBackColor    =   16744576
      SelForeColor    =   16777215
      SeparatorColor  =   49344
      DisabledColor   =   12640511
      BorderStyle     =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   9
      Top             =   5415
      Width           =   8520
      _ExtentX        =   15028
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   2646
            MinWidth        =   2646
            Object.ToolTipText     =   "Usu�rio da rede"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   2381
            MinWidth        =   2381
            Object.ToolTipText     =   "Usu�rio do banco de dados"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   3528
            MinWidth        =   3528
            Object.ToolTipText     =   "Banco de dados conectado"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            Alignment       =   1
            Object.Width           =   2381
            MinWidth        =   2381
            TextSave        =   "10/05/2017"
            Object.ToolTipText     =   "Data"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   1
            Object.Width           =   1587
            MinWidth        =   1587
            TextSave        =   "08:49"
            Object.ToolTipText     =   "Hora"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "mdiCAD060"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'TI-5616
Private Declare Function GetSystemMenu Lib "user32" (ByVal hwnd As Long, ByVal bRevert As Long) As Long
Private Declare Function GetMenuItemCount Lib "user32" (ByVal hMenu As Long) As Long
Private Declare Function RemoveMenu Lib "user32" (ByVal hMenu As Long, ByVal nPosition As Long, ByVal wFlags As Long) As Long
Private Declare Function DrawMenuBar Lib "user32" (ByVal hwnd As Long) As Long
Private Const MF_BYPOSITION = &H400&
Private Const MF_DISABLED = &H2&
'TI-5616

Private Sub cmd1_Click()

End Sub

Private Sub cmdAtivarDesativar_Click()
    
    frmAtivarDesativar.Show vbModal
    vFl_Analise = False

End Sub

Private Sub cmdAtivarDesativar_MouseOver()
    stbBarra.Panels(1) = cmdAtivarDesativar.ToolTipText
End Sub

Private Sub cmdCadCliente_Click()
    mdiCAD060.stbBarra.Panels(1) = "Carregando informa��es, aguarde..."
    vFl_Consulta = False
    vFl_Analise = False
    frmCadCliente.Show vbModal
End Sub

Private Sub cmdCadCliente_MouseOver()
    stbBarra.Panels(1) = cmdCadCliente.ToolTipText
End Sub

Private Sub cmdCadMotivos_Click()
    frmCadastroMotivos.Show
End Sub

Private Sub cmdCadMotivos_MouseOver()
    stbBarra.Panels(1).Text = cmdCadMotivos.ToolTipText
End Sub

Private Sub cmdCadVisaoUsuario_Click()
    frmVisaoUsuario.Show
    vFl_Analise = False
End Sub

Private Sub cmdCadVisaoUsuario_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    stbBarra.Panels(1) = cmdCadVisaoUsuario.ToolTipText
End Sub


Private Sub cmdConsulta_Click()
    vFl_Consulta = True
    vFl_Tipo_Solicitacao = 0    'Consulta
    frmAnaliseWeb.Visible = False
    frmAnaliseWeb.Show vbModal
End Sub

Private Sub cmdConsulta_MouseOver()
    stbBarra.Panels(1).Text = cmdConsulta.ToolTipText

End Sub






Private Sub cmdInformacoes_Click()
    frmSobre.Show vbModal
End Sub

Private Sub cmdInformacoes_MouseOver()
    stbBarra.Panels(1).Text = cmdInformacoes.ToolTipText
End Sub

Private Sub cmdLog_Click()
Dim I As Integer
Dim vSenhaOK As Boolean

For I = 1 To 3
    If InputBox("Senha", "Acesso aos Logs") = "WWWWWW" Then
        vSenhaOK = True
        I = 3
        frmLogWeb.Show vbModal
    Else
        vSenhaOK = False
    End If
Next I

If vSenhaOK = False Then
    Exit Sub
End If

End Sub

Private Sub cmdLog_MouseOver()
    stbBarra.Panels(1).Text = cmdLog.ToolTipText
End Sub

Private Sub cmdPreAlteracao_Click()
    vFl_Consulta = False
    vFl_Tipo_Solicitacao = 1
    frmAnaliseWeb.Visible = False
    frmAnaliseWeb.Show vbModal
End Sub

Private Sub cmdPreAlteracao_MouseOver()
    stbBarra.Panels(1).Text = cmdPreAlteracao.ToolTipText
End Sub

Private Sub cmdPreCadastro_Click()
    vFl_Consulta = False
    vFl_Tipo_Solicitacao = 0
    frmAnaliseWeb.Visible = False
    frmAnaliseWeb.Show vbModal
End Sub


Private Sub cmdPreCadastro_MouseOver()
    stbBarra.Panels(1).Text = cmdPreCadastro.ToolTipText
End Sub

Private Sub cmdPreRecadastro_Click()
    vFl_Consulta = False
    vFl_Tipo_Solicitacao = 2
    frmAnaliseWeb.Visible = False
    frmAnaliseWeb.Show vbModal
End Sub

Private Sub cmdPreRecadastro_MouseOver()
    stbBarra.Panels(1).Text = cmdPreRecadastro.ToolTipText
End Sub

Private Sub cmdResponsavel_Click()
    frmResponsavel.Show vbModal
    vFl_Analise = False

End Sub

Private Sub cmdResponsavel_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    stbBarra.Panels(1) = cmdResponsavel.ToolTipText

End Sub


Private Sub cmdSair_Click()
    
    'Emerson - 04/11/15 - TI-2566 - Fechar sess�es ativas no Oracle...
    If (Not db Is Nothing) Or (Not db_INT Is Nothing) Then
    
        If vVB_Generica_001.Sair <> 6 Then
            Exit Sub
        End If
        
        If Not db Is Nothing Then
            Call vVB_Generica_001.ExcluiBind(db)
        End If
        
        If Not db_INT Is Nothing Then
            Call vVB_Generica_001.ExcluiBind(db_INT)
        End If
    End If
    
    Set vObjOracle = Nothing
   '------------------------------
    
    Set dbOra = Nothing
    Set dbOraInt = Nothing
    Set db = Nothing
    Set db_INT = Nothing
        
    End

End Sub


Private Sub cmdSair_MouseOver()
    Me.stbBarra.Panels(1).Text = cmdSair.ToolTipText
End Sub

Private Sub MDIForm_Activate()
           
     With Menu.MenuItems
    'Cadastro
        .Add 0, "Cadastros", , "&Cadastros"
        .Add "Cadastros", "Cadastro_Clientes", , "&Clientes", , vbShiftMask
        .Add "Cadastros", "Cadastro_MotivosRecusa", , "&Motivos de Recusa", , vbShiftMask
        .Add "Cadastros", "Cadastro_VisaoUsuario", , "&Visao do Usuario", , vbShiftMask
        .Add "Cadastros", "Dados_Marketing", , "&Dados Marketing", , vbShiftMask
        .Add "Cadastros", "SCI_Antigo", , "&SCI Antigo", , vbShiftMask
        .Add 0, "Ativar/Desativar", , "Ativar/&Desativar", , vbShiftMask
        .Add 0, "Analise_WEB", , "An�lise &WEB"
        .Add "Analise_WEB", "Pre_Cadastro", , "Pr�-&Cadastro", , vbShiftMask
        .Add "Analise_WEB", "Pre_Alteracao", , "Pr�-&Altera��o", , vbShiftMask
        .Add "Analise_WEB", "Pre_Recadastro", , "Pr�-&Recadastro", , vbShiftMask
        .Add "Analise_WEB", "Consulta", , "C&onsulta", , vbShiftMask
        .Add "Analise_WEB", "Consulta_Recusados", , "Co&nsulta Recusados", , vbShiftMask
        .Add "Analise_WEB", "Log", , "&Log", , vbShiftMask
        
        .Add 0, "Parametros", , "Par�metros"
        .Add 0, "Responsavel", , "Respons�vel"
        
        .Add 0, "Informacoes", , "&Informa��es", , vbShiftMask
        .Add 0, "Sair", , "&Sair", , vbShiftMask
    End With
    
    Sql = "SELECT TO_CHAR(dt_faturamento,'dd/mm/yy') FROM datas "
    Set db_CONS = db.dbcreatedynaset(Sql, 0&)
    vDtHoje = db_CONS.Fields(0).Value

    'stbBarra.Panels(3) = vUser_Intranet
    'stbBarra.Panels(4) = vDtHoje

End Sub

Private Sub MDIForm_Load()
    
    If App.PrevInstance Then
        MsgBox "J� EXISTE UMA INST�NCIA ABERTA"
        End
    End If
         
    frmLogo.Show 1
         
    dlgLogin.Show 1
         
    App.HelpFile = App.Path & "\CAD060.HLP"
    
    DisableX Me 'TI-5616
    
    'Pegar o codigo do sistema na tabela de HELPDESK.SOFTWARE usando o valor da Propriedade TITLE
    db.Parameters.Remove "Nome_Software"
    db.Parameters.Add "Nome_Software", UCase(App.Title), 1
    
    db.Parameters.Remove "Cod_Software"
    db.Parameters.Add "Cod_Software", 0, 2
    
    vVB_Generica_001.ExecutaPl db, "PRODUCAO.PCK_CAD060.PR_SELECT_COD_SOFTWARE(:Nome_Software, :Cod_Software)"
    
    CodigoSistema = db.Parameters("Cod_Software")
        
    db.Parameters.Remove "Cod_Software"
    
    vCharEspeciais = fCharEspeciais
    
End Sub

Private Sub Menu_Click(ByVal ID As Long)
    
    Select Case Menu.MenuItems.Key(ID)
        Case "Cadastro_Clientes"
            Call cmdCadCliente_Click
        Case "SCI_Antigo"
            'Shell "H:\ORACLE\SISTEMAS\VB\32BITS\CAD690.EXE ", 1
            
            'Shell "F:\SISTEMAS\ORACLE\CAD690\VB\PRODUCAO_20040420\cad690.exe ", 1
            Shell Pegar_VL_Parametro("CAMINHOSCIANTIGO") & "cad690.exe ", 1
            
            db.Parameters.Remove "Cod_Sistema"
            db.Parameters.Remove "Nome_parametro"
            
        Case "Ativar/Desativar"
            Call cmdAtivarDesativar_Click
        Case "Cadastro_MotivosRecusa"
            frmCadastroMotivos.Show
        Case "Cadastro_VisaoUsuario"
            frmVisaoUsuario.Show
        Case "Dados_Marketing"
            frmMarketing.Show
        Case "Pre_Cadastro"
            Call cmdPreCadastro_Click
        Case "Pre_Alteracao"
            Call cmdPreAlteracao_Click
        Case "Pre_Recadastro"
            Call cmdPreRecadastro_Click
        Case "Consulta"
            Call cmdConsulta_Click
        Case "Log"
            Call cmdLog_Click
        Case "Parametros"
            frmParametros.Show 1
        Case "Responsavel"
            frmResponsavel.Show 1
        Case "Informacoes"
            Call cmdInformacoes_Click
        Case "Sair"
            Call cmdSair_Click
    End Select

End Sub

'TI-5616
Public Sub DisableX(frm As Form)
    Dim hMenu As Long
    Dim nCount As Long
    hMenu = GetSystemMenu(frm.hwnd, 0)
    nCount = GetMenuItemCount(hMenu)
    Call RemoveMenu(hMenu, nCount - 1, MF_DISABLED Or MF_BYPOSITION)
    DrawMenuBar frm.hwnd
End Sub
'FIM TI-5616

