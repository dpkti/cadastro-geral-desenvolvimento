VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "COMCT332.OCX"
Object = "{EAB22AC0-30C1-11CF-A7EB-0000C05BAE0B}#1.1#0"; "SHDOCVW.DLL"
Begin VB.Form frmCEPS 
   Caption         =   "Pesquisa de CEP�s"
   ClientHeight    =   5700
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8655
   LinkTopic       =   "Form1"
   ScaleHeight     =   5700
   ScaleWidth      =   8655
   WindowState     =   2  'Maximized
   Begin SHDocVwCtl.WebBrowser browse 
      Height          =   3975
      Left            =   0
      TabIndex        =   4
      Top             =   1260
      Width           =   5685
      ExtentX         =   10028
      ExtentY         =   7011
      ViewMode        =   0
      Offline         =   0
      Silent          =   0
      RegisterAsBrowser=   0
      RegisterAsDropTarget=   1
      AutoArrange     =   0   'False
      NoClientEdge    =   0   'False
      AlignLeft       =   0   'False
      NoWebView       =   0   'False
      HideFileNames   =   0   'False
      SingleClick     =   0   'False
      SingleSelection =   0   'False
      NoFolders       =   0   'False
      Transparent     =   0   'False
      ViewID          =   "{0057D0E0-3573-11CF-AE69-08002B2E1262}"
      Location        =   ""
   End
   Begin ComCtl3.CoolBar CoolBar1 
      Align           =   1  'Align Top
      Height          =   1230
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   8655
      _ExtentX        =   15266
      _ExtentY        =   2170
      BandCount       =   2
      BackColor       =   14737632
      FixedOrder      =   -1  'True
      _CBWidth        =   8655
      _CBHeight       =   1230
      _Version        =   "6.7.9782"
      Child1          =   "Toolbar1"
      MinHeight1      =   780
      Width1          =   4950
      NewRow1         =   0   'False
      Child2          =   "cboSite"
      MinHeight2      =   360
      Width2          =   7755
      FixedBackground2=   0   'False
      NewRow2         =   -1  'True
      Begin VB.ComboBox cboSite 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   360
         ItemData        =   "frmCEPS.frx":0000
         Left            =   165
         List            =   "frmCEPS.frx":000A
         TabIndex        =   3
         Top             =   840
         Width           =   8400
      End
      Begin MSComctlLib.Toolbar Toolbar1 
         Height          =   780
         Left            =   30
         TabIndex        =   1
         Top             =   30
         Width           =   8535
         _ExtentX        =   15055
         _ExtentY        =   1376
         ButtonWidth     =   1244
         ButtonHeight    =   1376
         Style           =   1
         ImageList       =   "ImageList1"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   11
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "CEP"
               Key             =   "CEP"
               ImageIndex      =   3
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Correios"
               Key             =   "Correios"
               ImageIndex      =   3
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Parar"
               Key             =   "Parar"
               ImageIndex      =   1
            EndProperty
            BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Refresh"
               Key             =   "Refresh"
               ImageIndex      =   4
            EndProperty
            BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   4
            EndProperty
            BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Voltar"
               Key             =   "Voltar"
               ImageIndex      =   5
            EndProperty
            BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Pr�ximo"
               Key             =   "Pr�ximo"
               ImageIndex      =   6
            EndProperty
            BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   4
            EndProperty
            BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Sair"
               Key             =   "Sair"
               ImageIndex      =   2
            EndProperty
         EndProperty
         Begin Threed.SSPanel SSP1 
            Height          =   375
            Left            =   8145
            TabIndex        =   5
            Top             =   180
            Width           =   3480
            _Version        =   65536
            _ExtentX        =   6138
            _ExtentY        =   661
            _StockProps     =   15
            Caption         =   "SSPanel1"
            ForeColor       =   16777215
            BackColor       =   13160660
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            FloodType       =   1
            FloodColor      =   8388608
         End
         Begin MSComctlLib.ImageList ImageList1 
            Left            =   4275
            Top             =   180
            _ExtentX        =   1005
            _ExtentY        =   1005
            BackColor       =   -2147483643
            ImageWidth      =   32
            ImageHeight     =   32
            MaskColor       =   12632256
            _Version        =   393216
            BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
               NumListImages   =   6
               BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmCEPS.frx":0033
                  Key             =   ""
               EndProperty
               BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmCEPS.frx":5D95
                  Key             =   ""
               EndProperty
               BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmCEPS.frx":6A6F
                  Key             =   ""
               EndProperty
               BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmCEPS.frx":7749
                  Key             =   ""
               EndProperty
               BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmCEPS.frx":78A3
                  Key             =   ""
               EndProperty
               BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmCEPS.frx":7CF5
                  Key             =   ""
               EndProperty
            EndProperty
         End
         Begin VB.CommandButton Command1 
            BackColor       =   &H00E0E0E0&
            Caption         =   "Navigate"
            Default         =   -1  'True
            Height          =   315
            Left            =   6480
            TabIndex        =   2
            Top             =   0
            Visible         =   0   'False
            Width           =   1395
         End
      End
   End
End
Attribute VB_Name = "frmCEPS"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Browse_DownloadBegin()
    Me.MousePointer = vbHourglass
    SSP1.Visible = True
End Sub

Private Sub Browse_DownloadComplete()
    Me.MousePointer = vbNormal
    SSP1.Visible = False
End Sub

Private Sub Browse_ProgressChange(ByVal Progress As Long, ByVal ProgressMax As Long)
    Dim Percentual As Double
    Dim Valor1 As Double
    Dim Valor2 As Double
    
    If Progress > ProgressMax Then
        Valor1 = Progress
        Valor2 = ProgressMax
        Percentual = (100 * Valor2) / Valor1
    Else
        If ProgressMax = 0 Then
           Percentual = 100
        Else
            Valor1 = ProgressMax
            Valor2 = Progress
            Percentual = (100 * Valor2) / Valor1
        End If
    End If
    
    SSP1.FloodPercent = Percentual
    
End Sub

Private Sub Form_Load()
    SSP1.Visible = False
    Browse.Navigate "ABOUT:BLANK"
End Sub

Private Sub Form_Resize()
    On Error Resume Next
    Browse.Width = Me.Width - 200
    Browse.Height = Me.Height - (Toolbar1.Height)
    SSP1.Width = (Me.Width - SSP1.Left) - 300
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case UCase(Button.Key)
        Case "CEP"
            cboSite = "www.cep.com.br"
            Browse.Navigate cboSite.Text
        
        Case "CORREIOS"
            cboSite = "www.correios.com.br"
            Browse.Navigate cboSite.Text
        
        Case "PARAR"
            Browse.Stop
        
        Case "REFRESH"
            Browse.Refresh
        
        Case "VOLTAR"
            Browse.GoBack
            
        Case "PR�XIMO"
            Browse.GoForward
        
        Case "SAIR"
            Unload Me
            
    End Select
End Sub

