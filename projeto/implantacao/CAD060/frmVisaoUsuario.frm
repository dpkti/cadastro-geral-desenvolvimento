VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmVisaoUsuario 
   BackColor       =   &H00FFFFFF&
   Caption         =   "CAD060 - Cadastro Vis�o de Usu�rio X Clientes (Filiais)"
   ClientHeight    =   5655
   ClientLeft      =   270
   ClientTop       =   1560
   ClientWidth     =   8340
   LinkTopic       =   "Form1"
   ScaleHeight     =   5655
   ScaleWidth      =   8340
   Begin VB.Frame Frame8 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5475
      Left            =   75
      TabIndex        =   0
      Top             =   90
      Width           =   7275
      Begin VB.ComboBox cboUsuario 
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   195
         TabIndex        =   1
         Top             =   555
         Width           =   4815
      End
      Begin MSComctlLib.ListView lsvFiliais 
         Height          =   4245
         Left            =   180
         TabIndex        =   3
         Top             =   1035
         Width           =   6885
         _ExtentX        =   12144
         _ExtentY        =   7488
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   3
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Cod.Filial"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Nome Filial"
            Object.Width           =   7056
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Cod.Regional"
            Object.Width           =   2117
         EndProperty
      End
      Begin VB.Label Label2 
         BackColor       =   &H00FFFFFF&
         Caption         =   "Usu�rio que ter� vis�o das solicita��es feitas pelas filiais"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   225
         TabIndex        =   2
         Top             =   300
         Width           =   4890
      End
   End
   Begin Bot�o.cmd cmdVoltar2 
      Height          =   585
      Left            =   7560
      TabIndex        =   4
      ToolTipText     =   "Voltar"
      Top             =   4965
      Width           =   585
      _ExtentX        =   1032
      _ExtentY        =   1032
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmVisaoUsuario.frx":0000
      PICN            =   "frmVisaoUsuario.frx":001C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   1
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdGravar 
      Height          =   540
      Left            =   7560
      TabIndex        =   5
      ToolTipText     =   "Gravar vis�o do usu�rio"
      Top             =   4275
      Width           =   585
      _ExtentX        =   1032
      _ExtentY        =   953
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmVisaoUsuario.frx":0796
      PICN            =   "frmVisaoUsuario.frx":07B2
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmVisaoUsuario"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private vUsuarioVisao As Long

Private Sub cboUsuario_Click()
Dim I As Integer
    
    'Varre o listview para desmarcar as filiais, preparando para o pr�ximo usu�rio que for escolhido
    For I = 1 To lsvFiliais.ListItems.Count
        'Se o c�digo de filial lido da consulta de registros de filiais j� cadastrados para o usu�rio, marca o check
        If lsvFiliais.ListItems(I).Checked = True Then
            lsvFiliais.ListItems(I).Checked = False
        End If
    Next I
    
    Call cboUsuario_LostFocus

End Sub

Private Sub cboUsuario_LostFocus()
Dim vCodFilial As Long
Dim I As Integer
              
    Screen.MousePointer = 11
              
    vUsuarioVisao = Val(Mid(cboUsuario, InStr(cboUsuario, "-") + 2, 5))

    Set OraParameters = db_INT.Parameters
    Criar_Cursor OraParameters, "cCursor1"
    OraParameters.Remove "CODUSU":  OraParameters.Add "CODUSU", vUsuarioVisao, 1
    OraParameters.Remove "CODFIL":  OraParameters.Add "CODFIL", 0, 1
    OraParameters.Remove "TP_CONS": OraParameters.Add "TP_CONS", 0, 1
    OraParameters.Remove "vErro":   OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    
    vErro = vVB_Generica_001.ExecutaPl(db_INT, "INTRANET.PCK_CLIENTES_TESTE.PR_SEL_USU_VISAO(:cCursor1,:CODUSU,:CODFIL,:TP_CONS,:vErro,:vMsgErro)")
    If vErro <> "" Then
        Exit Sub
    Else
        Set db_CONS = OraParameters("cCursor1").Value
    End If

    'Faz o check no List View de acordo com os campos que vieram no select
    While Not db_CONS.EOF
        vCodFilial = Val(db_CONS!Cod_Filial.Value)
        'Varre o listview para marcar as filiais que j� est�o cadastradas para o usu�rio lido
        For I = 1 To lsvFiliais.ListItems.Count
            'Se o c�digo de filial lido da consulta de registros de filiais j� cadastrados para o usu�rio, marca o check
            If vCodFilial = lsvFiliais.ListItems(I) Then
                lsvFiliais.ListItems(I).Checked = True
            End If
        Next I
        db_CONS.movenext
    Wend

    Screen.MousePointer = 0


End Sub


Private Sub cmdGravar_Click()
Dim I As Integer, II As Integer
Dim vUpdate(13, 2) As String
    
    
If MsgBox("Confirma a vis�o destas filiais para o usu�rio escolhido?", vbQuestion + vbYesNo, "CAD060 - Cadastro de Clientes") = vbYes Then
    
    Screen.MousePointer = 11
    
    '--------------------------------------------------------------------------------------
    'Deleta todos os relacionamentos usuario X filial da tabela, para depois inserir tudo de novo
    '--------------------------------------------------------------------------------------
    Set OraParameters = db_INT.Parameters
    OraParameters.Remove "CODUSU": OraParameters.Add "CODUSU", vUsuarioVisao, 1
    OraParameters.Remove "CODFIL": OraParameters.Add "CODFIL", 0, 1
    OraParameters.Remove "TP_CONS": OraParameters.Add "TP_CONS", "D", 1
    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    
    db_INT.Executesql "BEGIN INTRANET.PCK_CLIENTES_TESTE.PR_USUARIO_VISAO_CLI" & _
                             "(:CODUSU,:CODFIL,:TP_CONS,:vErro,:vMsgErro); END;"
    
    If Val(IIf(IsNull(OraParameters("vErro")), 0, OraParameters("vErro"))) <> 0 Then
        Screen.MousePointer = 0
        MsgBox "Erro: " & Val(OraParameters("vErro").Value) & " - " & _
                       OraParameters("vMsgErro").Value & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
        Exit Sub
    End If
    
    '--------------------------------------------------------------------------------------
    'Insere todos os relacionamentos usuario X filial da tabela, para depois inserir tudo de novo
    '--------------------------------------------------------------------------------------
    Set OraParameters = db_INT.Parameters
    OraParameters.Remove "CODUSU": OraParameters.Add "CODUSU", vUsuarioVisao, 1
    OraParameters.Remove "TP_CONS": OraParameters.Add "TP_CONS", "I", 1
    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    
    For I = 1 To lsvFiliais.ListItems.Count
        If lsvFiliais.ListItems(I).Checked = True Then
            
            OraParameters.Remove "CODFIL": OraParameters.Add "CODFIL", lsvFiliais.ListItems(I), 1
            
            db_INT.Executesql "BEGIN INTRANET.PCK_CLIENTES_TESTE.PR_USUARIO_VISAO_CLI" & _
                                     "(:CODUSU,:CODFIL,:TP_CONS,:vErro,:vMsgErro); END;"
            
            If Val(IIf(IsNull(OraParameters("vErro")), 0, OraParameters("vErro"))) <> 0 Then
                Screen.MousePointer = 0
                MsgBox "Erro: " & Val(OraParameters("vErro").Value) & " - " & _
                               OraParameters("vMsgErro").Value & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
                Exit Sub
            End If
        End If
    Next I
              
    MsgBox "Vis�o das filiais atribu�das com sucesso para o usu�rio!", vbInformation, "CAD060 - Cadastro de Clientes"
    Screen.MousePointer = 0

End If

End Sub

Private Sub cmdVoltar2_Click()
    mdiCAD060.SSPanel1.Visible = True
    Unload Me
End Sub

Private Sub Form_Load()
Dim I As Long
Dim litem As ListItem
    
    Me.Top = mdiCAD060.Menu.Height * 4
    Me.Left = (mdiCAD060.Width - Me.Width) / 2
    
    Set OraParameters = db.Parameters
    OraParameters.Remove "vErro":       OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro":    OraParameters.Add "vMsgErro", "", 2
    Criar_Cursor OraParameters, "cCursor1"
    
    vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD060.PR_SELECT_FILIAL(:cCursor1,:vErro,:vMsgErro)")
    If vErro <> "" Then
        Exit Sub
    End If
    Set db_CONS = OraParameters("cCursor1").Value

    'Preencher o List View com os campos que sofreram altera��es
    While Not db_CONS.EOF
    'For I = 0 To db_CONS.RowCount - 1
        Set litem = lsvFiliais.ListItems.Add
        litem = IIf(IsNull(db_CONS.Fields(1).Value), "", db_CONS.Fields(1).Value)
        litem.SubItems(1) = IIf(IsNull(db_CONS.Fields(0).Value), "", db_CONS.Fields(0).Value)
        litem.SubItems(2) = IIf(IsNull(db_CONS.Fields(2).Value), "", db_CONS.Fields(2).Value)
        litem.Checked = False
        db_CONS.movenext
    Wend
    'Next I

    'Vai carregar combo com todos os usu�rios cadastrados na Intranet
    Call Carrega_Combo_Responsaveis("TODOS", cboUsuario)


End Sub


Private Sub lsvAlterados_BeforeLabelEdit(Cancel As Integer)

End Sub


