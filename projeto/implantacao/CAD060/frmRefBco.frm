VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmRefBco 
   BackColor       =   &H80000009&
   Caption         =   "Form2"
   ClientHeight    =   6180
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8835
   LinkTopic       =   "Form2"
   ScaleHeight     =   6180
   ScaleWidth      =   8835
   Begin Bot�o.cmd cmdAdicionar 
      Height          =   705
      Left            =   15
      TabIndex        =   17
      Top             =   0
      Width           =   765
      _ExtentX        =   1349
      _ExtentY        =   1244
      BTYPE           =   3
      TX              =   "Adicionar"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmRefBco.frx":0000
      PICN            =   "frmRefBco.frx":001C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   1335
      Left            =   30
      TabIndex        =   8
      Top             =   810
      Width           =   8760
      Begin VB.TextBox txtObs 
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   90
         MaxLength       =   80
         TabIndex        =   6
         Text            =   "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
         Top             =   900
         Width           =   8565
      End
      Begin VB.TextBox txtConceito 
         Alignment       =   2  'Center
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   5910
         MaxLength       =   5
         TabIndex        =   5
         Text            =   "XXXXX"
         Top             =   315
         Width           =   765
      End
      Begin VB.TextBox txtAgencia 
         Alignment       =   2  'Center
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3720
         MaxLength       =   5
         TabIndex        =   2
         Text            =   "00000"
         Top             =   315
         Width           =   675
      End
      Begin VB.TextBox txtBanco 
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   645
         MaxLength       =   15
         TabIndex        =   0
         Text            =   "XXXXXXXXXXXXXXX"
         Top             =   315
         Width           =   1740
      End
      Begin VB.TextBox txtSeq 
         Alignment       =   2  'Center
         BackColor       =   &H000040C0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   90
         Locked          =   -1  'True
         MaxLength       =   2
         TabIndex        =   9
         TabStop         =   0   'False
         Text            =   "00"
         Top             =   315
         Width           =   495
      End
      Begin VB.TextBox txtDt 
         Alignment       =   2  'Center
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2445
         MaxLength       =   10
         TabIndex        =   1
         Top             =   315
         Width           =   1215
      End
      Begin VB.TextBox txtFone 
         Alignment       =   2  'Center
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   4920
         MaxLength       =   9
         TabIndex        =   4
         Top             =   315
         Width           =   930
      End
      Begin VB.TextBox txtDDD 
         Alignment       =   2  'Center
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   4485
         MaxLength       =   4
         TabIndex        =   3
         Top             =   315
         Width           =   435
      End
      Begin VB.Label Label49 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Observa��o"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   75
         TabIndex        =   16
         Top             =   705
         Width           =   975
      End
      Begin VB.Label Label51 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Conceito"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   5910
         TabIndex        =   15
         Top             =   120
         Width           =   735
      End
      Begin VB.Label Label57 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Ag�ncia"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   3735
         TabIndex        =   14
         Top             =   120
         Width           =   660
      End
      Begin VB.Label Label58 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Banco"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   675
         TabIndex        =   13
         Top             =   120
         Width           =   495
      End
      Begin VB.Label Label59 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Seq."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   90
         TabIndex        =   12
         Top             =   120
         Width           =   360
      End
      Begin VB.Label Label56 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Dt Consulta"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   2475
         TabIndex        =   11
         Top             =   120
         Width           =   945
      End
      Begin VB.Label Label50 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "DDD/Fone"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   4500
         TabIndex        =   10
         Top             =   120
         Width           =   765
      End
   End
   Begin MSComctlLib.ListView lsvRefBanco 
      Height          =   4005
      Left            =   30
      TabIndex        =   7
      Top             =   2160
      Width           =   8775
      _ExtentX        =   15478
      _ExtentY        =   7064
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   8
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Seq"
         Object.Width           =   882
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Banco"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Dt Consulta"
         Object.Width           =   1940
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Ag�ncia"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "DDD"
         Object.Width           =   1058
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Text            =   "Fone"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Text            =   "Conceito"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   7
         Text            =   "Observa��es"
         Object.Width           =   5292
      EndProperty
   End
   Begin Bot�o.cmd cmdExcluir 
      Height          =   705
      Left            =   1590
      TabIndex        =   18
      Top             =   0
      Width           =   765
      _ExtentX        =   1349
      _ExtentY        =   1244
      BTYPE           =   3
      TX              =   "Excluir"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmRefBco.frx":0176
      PICN            =   "frmRefBco.frx":0192
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdGravar 
      Height          =   705
      Left            =   810
      TabIndex        =   19
      Top             =   -15
      Width           =   765
      _ExtentX        =   1349
      _ExtentY        =   1244
      BTYPE           =   3
      TX              =   "Gravar"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmRefBco.frx":02EC
      PICN            =   "frmRefBco.frx":0308
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmRefBco"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim rst As Object


Private Sub cmdAdicionar_Click()
    Limpar_Campos
    txtSeq = lsvRefBanco.ListItems(lsvRefBanco.ListItems.Count) + 1
    txtBanco.SetFocus
End Sub

Private Sub cmdGravar_Click()
    Dim litem As ListItem
    
    Set litem = lsvRefBanco.ListItems.Add
    litem = txtSeq
    litem.SubItems(1) = txtBanco
    litem.SubItems(2) = txtDt
    litem.SubItems(3) = txtAgencia
    litem.SubItems(4) = txtDDD
    litem.SubItems(5) = txtFone
    litem.SubItems(6) = txtConceito
    litem.SubItems(7) = txtObs

    'Gravar no banco
    
End Sub

Private Sub Form_Load()
    Conectar
    Limpar_Campos
    
    If db.Parameters("Tabela") = 1 Then
        Carregar_Referencias_Producao
        cmdAdicionar.Enabled = True
        cmdExcluir.Enabled = True
        cmdGravar.Enabled = True
    Else
        cmdAdicionar.Enabled = False
        cmdExcluir.Enabled = False
        cmdGravar.Enabled = False
        Carregar_Referencias_Intranet
    End If
End Sub

Private Sub lsvRefBanco_ItemClick(ByVal Item As MSComctlLib.ListItem)
    txtSeq = lsvRefBanco.SelectedItem
    txtBanco = lsvRefBanco.SelectedItem.SubItems(1)
    txtDt = lsvRefBanco.SelectedItem.SubItems(2)
    txtAgencia = lsvRefBanco.SelectedItem.SubItems(3)
    txtDDD = lsvRefBanco.SelectedItem.SubItems(4)
    txtFone = lsvRefBanco.SelectedItem.SubItems(5)
    txtConceito = lsvRefBanco.SelectedItem.SubItems(6)
    txtObs = lsvRefBanco.SelectedItem.SubItems(7)
End Sub

Sub Carregar_Referencias_Producao()
    Dim litem As ListItem
    Dim i As Integer
    
    For i = 1 To rst.RecordCount
        Set litem = lsvRefBanco.ListItems.Add
        litem = rst("Sequencia")
        litem.SubItems(1) = IIf(IsNull(rst("Banco")), "", rst("Banco"))
        litem.SubItems(2) = IIf(IsNull(rst("Dt_Consulta")), "", rst("Dt_Consulta"))
        litem.SubItems(3) = IIf(IsNull(rst("Agencia")), "", rst("Agencia"))
        litem.SubItems(4) = IIf(IsNull(rst("DDD")), "", rst("DDD"))
        litem.SubItems(5) = IIf(IsNull(rst("FONE")), "", rst("FONE"))
        litem.SubItems(6) = IIf(IsNull(rst("CONCEITO")), "", rst("CONCEITO"))
        litem.SubItems(7) = IIf(IsNull(rst("OBSERVACAO")), "", rst("OBSERVACAO"))
        rst.MoveNext
    Next
End Sub
'Sempre que for da tabela da Intranet.CLiente_Web
'Ser� apenas 1 registro
Sub Carregar_Referencias_Intranet()
    Dim litem As ListItem
    Dim i As Integer
    
    Set litem = lsvRefBanco.ListItems.Add
    litem = 1
    litem.SubItems(1) = IIf(IsNull(rst("Banco_ref_bco")), "", rst("Banco_ref_bco"))
    litem.SubItems(2) = ""
    litem.SubItems(3) = IIf(IsNull(rst("Agencia_ref_bco")), "", rst("Agencia_ref_bco"))
    litem.SubItems(4) = IIf(IsNull(rst("DDD_ref_bco")), "", rst("DDD_ref_bco"))
    litem.SubItems(5) = IIf(IsNull(rst("FONE_REF_BCO")), "", rst("FONE_REF_BCO"))
    litem.SubItems(6) = ""
    litem.SubItems(7) = ""
End Sub

Sub Conectar()
    Set dbOra = CreateObject("oracleinproCServer.xorasession")
    Set db = dbOra.OpenDatabase("IDPK", "INTRANET/PROD", 0&)
    
    'Se for producao deve-se usar o Codigo do Cliente e Tipo da Tabela igual a 1 -54157
    'Caso contr�rio usar CGC e Tipo da Tabela igual a 2 - 62588751000128
    db.Parameters.Remove "CODCLICGC": db.Parameters.Add "CODCLICGC", Val("54157"), 1
    db.Parameters.Remove "Tabela": db.Parameters.Add "Tabela", Val(1), 1

    Retorna_Cursor "cRefBco", "INTRANET.PCK_Clientes_Teste.PR_SELECT_REF_BANCO", "CODCLICGC", "TABELA"
    Set rst = Rst_Geral
    Rst_Geral.Close: Set Rst_Geral = Nothing

End Sub
Sub Limpar_Campos()
    Dim controle As Control
    For Each controle In Me.Controls
        If TypeOf controle Is TextBox Then
            controle = ""
        End If
    Next
End Sub

