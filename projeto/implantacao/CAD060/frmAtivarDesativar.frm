VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmAtivarDesativar 
   BackColor       =   &H00FFFFFF&
   Caption         =   "CAD060 - Ativar / Desativar Cliente"
   ClientHeight    =   7950
   ClientLeft      =   60
   ClientTop       =   1980
   ClientWidth     =   11880
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   530
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   792
   Begin VB.Frame fraSitAutomatico 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Importar Arquivo"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4155
      Left            =   90
      TabIndex        =   21
      Top             =   3300
      Visible         =   0   'False
      Width           =   11715
      Begin VB.Frame fraArquivo 
         BackColor       =   &H00FFFFFF&
         Height          =   870
         Left            =   2880
         TabIndex        =   23
         Top             =   255
         Width           =   8715
         Begin Bot�o.cmd cmdArquivo 
            Height          =   585
            Left            =   195
            TabIndex        =   11
            ToolTipText     =   "Importar Arquivo"
            Top             =   165
            Width           =   585
            _ExtentX        =   1032
            _ExtentY        =   1032
            BTYPE           =   3
            TX              =   ""
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   -1  'True
            BCOL            =   16777215
            BCOLO           =   16777215
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "frmAtivarDesativar.frx":0000
            PICN            =   "frmAtivarDesativar.frx":001C
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   2
            NGREY           =   0   'False
            FX              =   0
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin VB.Label lblArquivo 
            BackColor       =   &H00C0E0FF&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00404040&
            Height          =   330
            Left            =   1095
            TabIndex        =   24
            Top             =   435
            Width           =   7275
         End
      End
      Begin VB.Frame Frame3 
         BackColor       =   &H80000009&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   885
         Left            =   105
         TabIndex        =   22
         Top             =   240
         Width           =   2610
         Begin VB.OptionButton optTpArquivo 
            BackColor       =   &H00FFFFFF&
            Caption         =   "Altera��o de Mensagem"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   405
            Index           =   1
            Left            =   75
            TabIndex        =   10
            Top             =   450
            Width           =   2505
         End
         Begin VB.OptionButton optTpArquivo 
            BackColor       =   &H00FFFFFF&
            Caption         =   "Ativar / Desativar Cliente"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   465
            Index           =   0
            Left            =   75
            TabIndex        =   9
            Top             =   120
            Width           =   2490
         End
      End
      Begin MSFlexGridLib.MSFlexGrid grdflexArquivo 
         Height          =   2130
         Left            =   90
         TabIndex        =   25
         Top             =   1185
         Width           =   11505
         _ExtentX        =   20294
         _ExtentY        =   3757
         _Version        =   393216
         Cols            =   11
         BackColorBkg    =   14737632
         GridLines       =   2
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin Bot�o.cmd cmdGravar 
         Height          =   585
         Left            =   9495
         TabIndex        =   12
         ToolTipText     =   "Gravar"
         Top             =   3420
         Width           =   585
         _ExtentX        =   1032
         _ExtentY        =   1032
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmAtivarDesativar.frx":0CF6
         PICN            =   "frmAtivarDesativar.frx":0D12
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdGeraLog 
         Height          =   585
         Left            =   10245
         TabIndex        =   13
         ToolTipText     =   "Gera Arq. Log"
         Top             =   3420
         Width           =   585
         _ExtentX        =   1032
         _ExtentY        =   1032
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmAtivarDesativar.frx":15EC
         PICN            =   "frmAtivarDesativar.frx":1608
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdLayOut 
         Height          =   585
         Left            =   10980
         TabIndex        =   14
         ToolTipText     =   "Lay-Out"
         Top             =   3420
         Width           =   585
         _ExtentX        =   1032
         _ExtentY        =   1032
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmAtivarDesativar.frx":22E2
         PICN            =   "frmAtivarDesativar.frx":22FE
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label lblMsg 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "lblMsg"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   225
         Left            =   180
         TabIndex        =   26
         Top             =   3765
         Width           =   555
      End
   End
   Begin VB.Frame fraSitManual 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Ativar / Desativar - Modo Manual"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2505
      Left            =   105
      TabIndex        =   16
      Top             =   750
      Visible         =   0   'False
      Width           =   11700
      Begin VB.Frame fraMsg 
         BackColor       =   &H80000009&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   885
         Left            =   90
         TabIndex        =   31
         Top             =   840
         Width           =   8955
         Begin VB.ComboBox cboMsgCliente 
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   90
            TabIndex        =   2
            Text            =   "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX - 00"
            Top             =   405
            Width           =   5985
         End
         Begin VB.TextBox txtDtMsg 
            Alignment       =   2  'Center
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   6120
            MaxLength       =   10
            TabIndex        =   3
            Top             =   405
            Width           =   1215
         End
         Begin VB.TextBox txtResponsavel 
            Alignment       =   2  'Center
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   7365
            MaxLength       =   12
            TabIndex        =   4
            Top             =   405
            Width           =   1455
         End
         Begin VB.Label Label35 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "Mensagem Cliente"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   90
            TabIndex        =   34
            Top             =   210
            Width           =   1560
         End
         Begin VB.Label Label33 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "Dt Mensagem"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   6135
            TabIndex        =   33
            Top             =   210
            Width           =   1140
         End
         Begin VB.Label Label34 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "Respons�vel"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   7545
            TabIndex        =   32
            Top             =   210
            Width           =   1065
         End
      End
      Begin VB.Frame fraRepres 
         BackColor       =   &H80000009&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   825
         Left            =   90
         TabIndex        =   30
         Top             =   1635
         Width           =   8970
         Begin VB.TextBox txtUltRepres 
            Alignment       =   2  'Center
            BackColor       =   &H00C0E0FF&
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   105
            MaxLength       =   12
            TabIndex        =   5
            Top             =   375
            Width           =   1455
         End
         Begin VB.ComboBox cboRepresDPK 
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   1965
            TabIndex        =   6
            Top             =   390
            Width           =   6495
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "�lt.Representante"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   120
            TabIndex        =   36
            Top             =   180
            Width           =   1530
         End
         Begin VB.Label lblRepresDPK 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "Representante DPK"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   180
            Left            =   1995
            TabIndex        =   35
            Top             =   180
            Width           =   1590
         End
      End
      Begin VB.TextBox txtSituacao 
         Alignment       =   2  'Center
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   330
         Left            =   5430
         Locked          =   -1  'True
         MaxLength       =   14
         TabIndex        =   1
         Top             =   510
         Width           =   1695
      End
      Begin VB.PictureBox pictSituacao 
         Appearance      =   0  'Flat
         AutoRedraw      =   -1  'True
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   495
         Left            =   7125
         ScaleHeight     =   495
         ScaleWidth      =   495
         TabIndex        =   19
         Top             =   345
         Width           =   495
      End
      Begin VB.ComboBox cboCliente 
         BackColor       =   &H000040C0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Left            =   1035
         TabIndex        =   0
         Top             =   525
         Width           =   4095
      End
      Begin VB.TextBox txtCodCliente 
         Alignment       =   2  'Center
         BackColor       =   &H000040C0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Left            =   90
         Locked          =   -1  'True
         MaxLength       =   6
         TabIndex        =   17
         Top             =   525
         Width           =   975
      End
      Begin Bot�o.cmd cmdAtivarDesativar 
         Height          =   585
         Left            =   9630
         TabIndex        =   7
         ToolTipText     =   "Recusar"
         Top             =   255
         Width           =   1110
         _ExtentX        =   1958
         _ExtentY        =   1032
         BTYPE           =   3
         TX              =   "Desativar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmAtivarDesativar.frx":2FD8
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdCancelar 
         Height          =   585
         Left            =   10965
         TabIndex        =   8
         ToolTipText     =   "Cancelar"
         Top             =   240
         Width           =   585
         _ExtentX        =   1032
         _ExtentY        =   1032
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmAtivarDesativar.frx":2FF4
         PICN            =   "frmAtivarDesativar.frx":3010
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Situa��o Atual"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   5445
         TabIndex        =   20
         Top             =   330
         Width           =   1155
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFFFFF&
         Caption         =   "Cliente"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   120
         TabIndex        =   18
         Top             =   300
         Width           =   1335
      End
   End
   Begin Threed.SSPanel sspCadCli 
      Align           =   2  'Align Bottom
      Height          =   645
      Left            =   0
      TabIndex        =   27
      Top             =   7305
      Width           =   11880
      _Version        =   65536
      _ExtentX        =   20955
      _ExtentY        =   1138
      _StockProps     =   15
      ForeColor       =   8388608
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BevelOuter      =   0
      Alignment       =   0
      Begin Bot�o.cmd cmdVoltar 
         Height          =   585
         Left            =   11250
         TabIndex        =   15
         ToolTipText     =   "Voltar"
         Top             =   60
         Width           =   585
         _ExtentX        =   1032
         _ExtentY        =   1032
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmAtivarDesativar.frx":3462
         PICN            =   "frmAtivarDesativar.frx":347E
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MSComDlg.CommonDialog controle 
         Left            =   690
         Top             =   270
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
         InitDir         =   "C:\"
      End
      Begin MSComctlLib.ImageList imlFiguras 
         Left            =   135
         Top             =   150
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   32
         ImageHeight     =   32
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   5
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmAtivarDesativar.frx":3BF8
               Key             =   ""
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmAtivarDesativar.frx":3CEC
               Key             =   ""
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmAtivarDesativar.frx":3DE1
               Key             =   ""
            EndProperty
            BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmAtivarDesativar.frx":3ED6
               Key             =   ""
            EndProperty
            BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmAtivarDesativar.frx":41F0
               Key             =   ""
            EndProperty
         EndProperty
      End
   End
   Begin Bot�o.cmd cmdManual 
      Height          =   705
      Left            =   120
      TabIndex        =   28
      Top             =   0
      Width           =   765
      _ExtentX        =   1349
      _ExtentY        =   1244
      BTYPE           =   3
      TX              =   "Manual"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmAtivarDesativar.frx":450A
      PICN            =   "frmAtivarDesativar.frx":4526
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdAutomatico 
      Height          =   705
      Left            =   930
      TabIndex        =   29
      Top             =   0
      Width           =   765
      _ExtentX        =   1349
      _ExtentY        =   1244
      BTYPE           =   3
      TX              =   "Arquivo"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmAtivarDesativar.frx":5200
      PICN            =   "frmAtivarDesativar.frx":521C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmAtivarDesativar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private vSituacao As Integer
Dim vNomeArq As String
Dim varLinhaArquivo As String   'Recebe a linha lida do arquivo texto

Dim vFl_Arq_Situacao As Boolean     'Se true, importa��o de arquivo ativar/desativar. Se false, importa��o de arquivo de troca de mensagem
Dim I As Long, Y As Long
Dim vCodCli As String
Dim vNomeCli As String
Dim vSit As String
Dim vCodRepr As String
Dim vRepres As String
Dim vCodMsg As String
Dim vMsg As String
Dim vDtMsg As String
Dim vResp As String
Dim vFl_OK As Boolean
Dim vReprDefault As Long
'vxavier Ci&T 22/11/2012 - [ISA - 1027] - [form frmAtivarDesativar]Alterar o nome da vari�vel vTpCliAtivarDesativar para vSegCliAtivarDesativar
Dim vSegCliAtivarDesativar As String
Dim vCidadeAtivarDesativar As Long
Dim vStatus As Integer
Sub pCarregaComboRepresentante(lcboRepres As ComboBox, lstrParametro As String, lstrDivisao As String)
    
    Dim lstrSQL          As String
    Dim lstrWHERE        As String
    Dim objRepres        As Object
    Dim lstrRepres       As String
    Dim lstrSequencia    As String
    Dim lstrSQLSequencia As String
    
    On Error GoTo erros
    
    lstrSequencia = "0"
    lstrWHERE = ""
    lstrSQLSequencia = ""
    lcboRepres.Clear
    lstrParametro = UCase(lstrParametro)
    
    'NOME OU CODIGO
    'If IsNumeric(lstrParametro) Then
    '    If lstrDivisao <> "B" Then
    '        vReprDPKGravar = Fix(lstrParametro)
    '    End If
    'Else
    '    If lstrDivisao <> "B" Then
    '        vReprDPKGravar = ""
    '    End If
    'End If
    
    If Trim(lstrParametro) <> "" Then
        
        If IsNumeric(Trim(lstrParametro)) Then
            
            db.Parameters.Remove "CODREPR": db.Parameters.Add "CODREPR", Fix(lstrParametro), 1
            
            lstrWHERE = lstrWHERE & " cod_Repres =:CODREPR "
            
            If InStr(1, lstrParametro, ".") <> 0 Then ' se tem sequencia
                
                lstrSequencia = Mid(lstrParametro, InStr(1, lstrParametro, ".") + 1, Len(lstrParametro))
                
                '99 - Tipo Forn./Func.
                'vxavier Ci&T 22/11/2012 - [ISA - 1027] - [form frmAtivarDesativar]Alterar o nome da vari�vel vTpCliAtivarDesativar para vSegCliAtivarDesativar
                If vSegCliAtivarDesativar = "AG" Then
                    
                    If Val(lstrSequencia) = 0 Then
                        
                        'Em 14/12/04, solicitado para que somente representante tipo "R"
                        'seja relacionado ao cliente devido ao projeto de regionaliza�ao
                        'com exce��o do c�d.repres=599
                        'Comentado o trecho:
                        'AND (a.tipo in ('R','V','M','P'))
                        
                        'Em 23/12/04, alterado novamente para permitir representante
                        'do tipo M, A ou V, cuja filial for do tipo P ou T
                        
                        'Altera��o da consistencia conf.solic.Monica - Alethea - 28/11/06
                        '"AND (a.tipo = 'R' or a.cod_repres = 599 or "
                        lstrSQLSequencia = "SELECT a.cod_repres, a.nome_repres, " & _
                                                 " b.cod_regional, b.cod_filial " & _
                                             "FROM representante a, filial b " & _
                                            "WHERE a.cod_repres = :CODREPR " & _
                                              "AND a.seq_franquia = 0 " & _
                                              "AND b.cod_filial = a.cod_filial " & _
                                              "AND a.divisao = :DIVISAO " & _
                                              "AND a.situacao = 0 " & _
                                              "AND (a.tipo in ('R','P') or a.cod_repres = 599 or " & _
                                                  "(a.tipo in ('M','A','V') and " & _
                                                  " b.tp_filial in ('P','T'))) "
                   
                    Else
                        'Altera��o da consistencia conf.solic.Monica - Alethea - 28/11/06
                        '"AND (a.tipo = 'R' or a.cod_repres = 599 or "
                        lstrSQLSequencia = "SELECT a.cod_repres, a.nome_repres, " & _
                                                 " b.cod_regional, d.divisao, b.cod_filial " & _
                                             "FROM representante a , filial b , representante c, divisao d " & _
                                            "WHERE b.cod_franqueador = :COD_FRANQUEADOR " & _
                                              "AND a.cod_filial = b.cod_filial " & _
                                              "AND a.seq_franquia = :SEQ_FRANQUIA " & _
                                              "AND a.divisao = d.divisao " & _
                                              "AND a.situacao = 0 " & _
                                              "AND c.cod_repres = b.cod_franqueador " & _
                                              "AND c.seq_franquia = 0 " & _
                                              "AND (a.tipo in ('R','P') or a.cod_repres = 599 or " & _
                                                  "(a.tipo in ('M','A','V') and " & _
                                                  " b.tp_filial in ('P','T'))) " & _
                                              "AND a.seq_franquia = :seq_franquia "
                    End If
                
                ElseIf Val(lstrSequencia) <> 0 Then
                    
                    'Altera��o da consistencia conf.solic.Monica - Alethea - 28/11/06
                    '"AND (a.tipo = 'R' or a.cod_repres = 599 or "
                    lstrSQLSequencia = "SELECT a.cod_repres, a.nome_repres, b.cod_regional, " & _
                                             " d.divisao, b.cod_filial " & _
                                         "FROM representante a, filial b, " & _
                                             " representante c, divisao d " & _
                                        "WHERE b.cod_franqueador = :COD_FRANQUEADOR " & _
                                          "AND a.cod_filial = b.cod_filial " & _
                                          "AND a.seq_franquia = :SEQ_FRANQUIA " & _
                                          "AND a.divisao = d.divisao " & _
                                          "AND a.situacao = 0 " & _
                                          "AND c.cod_repres = b.cod_franqueador " & _
                                          "AND c.seq_franquia = 0 " & _
                                          "AND (a.tipo in ('R','P') or a.cod_repres = 599 or " & _
                                              "(a.tipo in ('M','A','V') and " & _
                                              " b.tp_filial in ('P','T'))) "
                Else
                    'Altera��o da consistencia conf.solic.Monica - Alethea - 28/11/06
                    '"AND (a.tipo = 'R' or a.cod_repres = 599 or "
                    lstrSQLSequencia = "SELECT a.cod_repres, a.nome_repres, b.cod_regional, " & _
                                             " b.cod_filial " & _
                                         "FROM representante a, filial b " & _
                                        "WHERE a.cod_repres = :CODREPR " & _
                                          "AND a.seq_franquia = 0 " & _
                                          "AND b.cod_filial = a.cod_filial " & _
                                          "AND a.divisao = :DIVISAO " & _
                                          "AND a.situacao = 0 " & _
                                          "AND (a.tipo in ('R','P') or a.cod_repres = 599 or " & _
                                                  "(a.tipo in ('M','A','V') and " & _
                                                  " b.tp_filial in ('P','T'))) "
                End If
                
                db.Parameters.Remove "cod_franqueador": db.Parameters.Add "cod_franqueador", Fix(lstrParametro), 1
                db.Parameters.Remove "seq_franquia": db.Parameters.Add "seq_franquia", lstrSequencia, 1
            End If
        Else
            db.Parameters.Remove "nome_Repres": db.Parameters.Add "nome_Repres", lstrParametro, 1
            
            lstrWHERE = lstrWHERE & " nome_Repres " & IIf(Left(lstrParametro, 1) = "%" Or Right(lstrParametro, 1) = "%", " LIKE ", "=") & " :nome_Repres "
        
        End If
    End If
    db.Parameters.Remove "divisao": db.Parameters.Add "divisao", lstrDivisao, 1
    
    If lstrSQLSequencia <> "" Then
        
        Screen.MousePointer = vbHourglass
        Set objRepres = db.dbcreatedynaset(lstrSQLSequencia, 0&)
        lcboRepres.Clear
        
        If Not objRepres.EOF Then     ' nome_repres                           cod_repres
            'If lstrDivisao <> "B" Then
            '    vReprDPKGravar = objRepres.fields(0).Value
            'End If
            
            'Altera��o da consistencia conf.solic.Monica - Alethea - 28/11/06
            '"AND (a.tipo = 'R' or a.cod_repres = 599 or "
            lstrSQL = "SELECT cod_Repres, nome_Repres, b.cod_regional, b.cod_filial " & _
                       " FROM Representante a, Filial b " & _
                       "WHERE cod_repres = :CODREPR " & _
                         "AND a.divisao = :DIVISAO " & _
                         "AND a.cod_filial = b.cod_filial " & _
                         "AND a.situacao = 0 " & _
                         "AND (a.tipo in ('R','P') or a.cod_repres = 599 or " & _
                             "(a.tipo in ('M','A','V') and " & _
                             " b.tp_filial in ('P','T'))) "

            
            Screen.MousePointer = vbHourglass
            db.Parameters.Remove "CODREPR": db.Parameters.Add "CODREPR", Fix(lstrParametro), 1
            Set objRepres = db.dbcreatedynaset(lstrSQL, 0&)
            Screen.MousePointer = vbDefault
            lcboRepres.Clear
            If Not objRepres.EOF Then
               lcboRepres.AddItem objRepres!nome_Repres.Value & " - " & Format(objRepres!cod_Repres.Value, "0000")
            End If
        
        End If
        
    Else
    
        'Altera��o da consistencia conf.solic.Monica - Alethea - 28/11/06
        '" AND (a.tipo = 'R' or a.cod_repres = 599 or "

        lstrSQL = "SELECT cod_Repres, nome_Repres, b.cod_regional, b.cod_filial " & _
                    "FROM Representante a, Filial b "
        lstrSQL = lstrSQL & _
                    IIf(lstrWHERE = "", "", " WHERE " & lstrWHERE & _
                    " AND a.divisao = :DIVISAO " & _
                    " AND situacao = 0 " & _
                    " AND a.cod_filial = b.cod_filial " & _
                    " AND (a.tipo in ('R','P') or a.cod_repres = 599 or " & _
                        "(a.tipo in ('M','A','V') and " & _
                        " b.tp_filial in ('P','T'))) " & _
                    " AND a.seq_franquia=0")
        
        Set objRepres = db.dbcreatedynaset(lstrSQL, 0&)
        
        lcboRepres.Clear
        Screen.MousePointer = vbHourglass
        
        While Not objRepres.EOF
            mdiCAD060.stbBarra.Panels(1) = "PESQUISANDO REPRESENTANTE . . . " & lcboRepres.ListCount
            lcboRepres.AddItem objRepres!nome_Repres.Value & " - " & Format(objRepres!cod_Repres.Value, "0000") & ".0"
            objRepres.MoveNext
        Wend
        mdiCAD060.stbBarra.Panels(1) = ""
    
    End If
    
    Screen.MousePointer = vbDefault
    
    If objRepres.RecordCount <= 1 Then
        If objRepres.RecordCount = 0 Then
            MsgBox "Representante n�o Encontrado !", vbInformation, "Aten��o"
            If lstrDivisao <> "B" Then
                lblRepresDPK = "Representante DPK"
                lblRepresDPK.Refresh
            '    vReprDPKGravar = ""
            End If
        Else
            
            lcboRepres.RemoveItem 0
            objRepres.MoveFirst
            lcboRepres.Text = objRepres.Fields("nome_Repres").Value & " - " & Format(objRepres!cod_Repres.Value, "0000") & ".0"
            
            If lstrDivisao <> "B" Then
                lblRepresDPK = "Representante DPK: " & Format(objRepres.Fields("cod_repres").Value, "0000") & "." & lstrSequencia
                lblRepresDPK.Refresh
                'If Trim(lstrSQLSequencia) = "" Then
                '    vReprDPKGravar = objRepres.fields("cod_repres").Value
                'End If
            End If
        End If
    Else
        MsgBox "Encontrei mais de um Representante" & Chr(10) & "Selecione um !", vbInformation, "Aten��o"
    End If
    
    'comentada rotina abaixo de validacao clientes DF - cristiano - 07/10/2008
    ''*******************************************************************************
    ''Para clientes do DF, verifica se Representante pertence a Regional 21,143 ou 186 ou a uma
    ''Filial que perten�a a estas Regionais
    'db.Parameters.Remove "CODCID": db.Parameters.Add "CODCID", Val(vCidadeAtivarDesativar), 1
    '
    'Sql = "Select cod_uf " & _
    '        "FROM Producao.Cidade " & _
    '       "WHERE cod_cidade = :CODCID"
    'Set objRepres = db.dbcreatedynaset(Sql, 0&)
    '
    ''Permitir que cliente de DF, que for OUTRAS SA�DAS, possa ter representante = 599
    'If objRepres!COD_UF = "DF" And vStatus = "0 - NORMAL" Then
    '
    '    db.Parameters.Remove "CODUF": db.Parameter.Add "CODUF", objRepres!COD_UF.Value, 1
    '    db.Parameters.Remove "CODREPR": db.Parameter.Add "CODREPR", Fix(lstrParametro), 1
    '    'Sql = " Select f.cod_filial, f.cod_regional " & _
    '            " from filial f, representante r " & _
    '           " where r.cod_repres =  :CODREPR " & _
    '             " and f.cod_filial = r.cod_filial  " & _
    '             " and ( f.cod_Filial = 21 or f.cod_regional = 21 ) "
    '
    '    Sql = " Select distinct f.cod_filial, f.cod_regional " & _
    '            " from filial f, representante r " & _
    '           " where r.cod_repres =  :CODREPR " & _
    '             " and f.cod_filial = r.cod_filial  " & _
    '             " and (f.cod_Filial in (21,143,186) or f.cod_regional in (21,143,186)) "
    '
    '
    '    Set objRepres = db.dbcreatedynaset(Sql, 0&)
    '
    '    If objRepres.EOF Then
    '       'MsgBox " Por causa do TARE (CD DF), este Cliente deve ser " & Chr(10) & _
    '              " cadastrado para um  Representante  que  perten�a" & Chr(10) & _
    '              " � Regional 21,143 ou 186 ", , "ATEN��O!!!!"
    '       MsgBox " Cliente status normal, do estado de 'DF' (venda limitada), deve ser " & Chr(10) & _
    '              " cadastrado para um  Representante  que  perten�a" & Chr(10) & _
    '              " � Regional 21,143 ou 186 ", , "ATEN��O!!!!"
    '       cboRepresDPK.SetFocus
    '       Exit Sub
    '    End If
    'End If
    ''*******************************************************************************
    
    Exit Sub
erros:
    If Err = 7 Then
        MsgBox "Consulta muito grande!", vbCritical, "Aten��o"
    End If
    Exit Sub
        
End Sub

Sub pAlteraMensagem(pCodCli As String, pCodMsg As String, pDtMsg As String, pResp As String)

    '----------------------------------------------------------------------------
    'Efetua Altera��o de Mensagem
    '----------------------------------------------------------------------------
    Set OraParameters = db.Parameters
    OraParameters.Remove "CODCLI":  OraParameters.Add "CODCLI", Val(pCodCli), 1
    OraParameters.Remove "CODMSG":  OraParameters.Add "CODMSG", IIf(Trim(pCodMsg) = "" Or IsNull(pCodMsg), Null, Val(pCodMsg)), 1
    OraParameters.Remove "DTMSG":   OraParameters.Add "DTMSG", IIf(Trim(pDtMsg) = "" Or IsNull(pDtMsg), Null, pDtMsg), 1
    OraParameters.Remove "RESP":    OraParameters.Add "RESP", IIf(Trim(pResp) = "" Or IsNull(pResp), Null, pResp), 1
    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
        
    db.ExecuteSQL "BEGIN PRODUCAO.PCK_Cliente.PR_ALTERA_MENSAGEM(:CODCLI,:CODMSG,To_Date(:DTMSG,'dd/mm/rr'),:RESP,:vErro,:vMsgErro); END;"
   
    If Val(IIf(IsNull(OraParameters("vErro")), 0, OraParameters("vErro"))) <> 0 Then
        MsgBox "Erro: " & Val(OraParameters("vErro").Value) & " - " & _
                          OraParameters("vMsgErro").Value & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
        vFl_OK = False
        Exit Sub
    End If

vFl_OK = True

End Sub

Sub pAtivarCliente(pCodCli As String, pSit As String, pCodRep As String, pCodMsg As String, pDtMsg As String, pResp As String)
Dim vLOJA As String
Dim objConexao As Object
Dim vCodMsg As Integer
    
    'EDUARDO - 30/05/2007
    'Pegar a UF do cliente no caso de reativacao de cliente, pois se a UF por SC, a mensagem devera ser 53
    
    Set objConexao = db.dbcreatedynaset("SELECT B.COD_UF FROM Producao.Cliente a, CIDADE B WHERE A.COD_CIDADE = B.COD_CIDADE AND COD_CLIENTE =" & pCodCli, 0&)
    If objConexao.EOF = False Then
        If objConexao!COD_UF = "SC" Then
            vCodMsg = 53
        Else
            vCodMsg = 0
        End If
    End If
    
    '----------------------------------------------------------------------------
    'Efetua Ativar
    '----------------------------------------------------------------------------
    Set OraParameters = db.Parameters
    OraParameters.Remove "CODCLI":  OraParameters.Add "CODCLI", Val(pCodCli), 1
    OraParameters.Remove "FLSIT":   OraParameters.Add "FLSIT", pSit, 1
    OraParameters.Remove "CODREP":  OraParameters.Add "CODREP", Val(pCodRep), 1
    'eduardo - 30/05/2007
    'OraParameters.Remove "CODMSG":  OraParameters.Add "CODMSG", 0, 1
    OraParameters.Remove "CODMSG":  OraParameters.Add "CODMSG", vCodMsg, 1
    OraParameters.Remove "DTMSG":   OraParameters.Add "DTMSG", "", 1
    OraParameters.Remove "RESP":    OraParameters.Add "RESP", "", 1
    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
        
    db.ExecuteSQL "BEGIN PRODUCAO.PCK_Cliente.PR_GRAVA_ATIVA_DESATIVA(:CODCLI,:FLSIT,:CODREP,:CODMSG,:DTMSG,:RESP,:vErro,:vMsgErro); END;"
   
    If Val(IIf(IsNull(OraParameters("vErro")), 0, OraParameters("vErro"))) <> 0 Then
        MsgBox "Erro: " & Val(OraParameters("vErro").Value) & " - " & _
                          OraParameters("vMsgErro").Value & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
        vFl_OK = False
        Exit Sub
    End If
    
       
    '----------------------------------------------------------------------------
    'Ativa on-line nos CDs
    '----------------------------------------------------------------------------
    If pSit = "A" Then   'Quer ativar
        
        'Seleciona Deposito_Visao
        Set OraParameters = db.Parameters
        OraParameters.Remove "NOMEPRG": OraParameters.Add "NOMEPRG", "CAD060", 1
        OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
        OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
        Criar_Cursor OraParameters, "pm_Cursor1"
        vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD060.Pr_Deposito_Visao(:pm_CURSOR1, :NOMEPRG, :vErro, :vMsgErro)")
        
        If vErro <> "" Then
            vFl_OK = False
            Exit Sub
        End If
        
        Set db_CONS = OraParameters("pm_Cursor1").Value
        
        If db_CONS.EOF Then
            MsgBox "N�o existem lojas cadastradas na tabela DEPOSITO_VISAO. " & Chr(13) & _
                   "O cliente somente ser� ativado ON-LINE em Campinas. " & Chr(13) & _
                   "Por favor, avise o suporte!", vbInformation, "CAD060 - Cadastro de Cliente"
        Else
        
            Do While Not db_CONS.EOF
            
                vLOJA = "DEP" & Format(db_CONS!Cod_Loja.Value, "00")
                
                'Verifica timeout na Loja_Conexao
                Set OraParameters = db.Parameters
                OraParameters.Remove "LOJA": OraParameters.Add "LOJA", Val(db_CONS!Loja.Value), 1
                OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
                OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
                Criar_Cursor OraParameters, "pm_Cursor1"
                vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD060.Pr_Loja_Conexao(:pm_CURSOR1, :CODLOJA, :vErro, :vMsgErro)")
                
                If vErro <> "" Then
                   vFl_OK = False
                   Exit Sub
                End If
                
                Set objConexao = OraParameters("pm_CURSOR1").Value
                        
                SUCCESS = Ping(objConexao!ip.Value, objConexao!Cod_Loja.Value, ECHO)
                
                'Sem Comunica��o
                If InStr(GetStatusCode(SUCCESS), ("ip success")) = 0 Or ECHO.RoundTripTime > objConexao!time_out.Value Then
                    
                    db_CONS.MoveNext
               
                'Comunica��o OK
                Else
        
                    'Vai alterar situa��o on-line no CD (somente vai ativar on-line, desativar n�o)
                     Set OraParameters = db.Parameters
                     OraParameters.Remove "CODCLI": OraParameters.Add "CODCLI", Val(pCodCli), 1
                     OraParameters.Remove "LOJA": OraParameters.Add "LOJA", Val(db_CONS!Loja.Value), 1
                     OraParameters.Remove "FLSIT": OraParameters.Add "FLSIT", 0, 1
                     OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
                     OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
                         
                     db.ExecuteSQL "BEGIN PRODUCAO.PCK_Cliente.PR_GRAVA_SIT_ONLINE(:CODCLI,:LOJA,:FLSIT,:vErro,:vMsgErro); END;"
                    
                     If Val(IIf(IsNull(OraParameters("vErro")), 0, OraParameters("vErro"))) <> 0 Then
                         MsgBox "Erro: " & Val(OraParameters("vErro").Value) & " - " & _
                                           OraParameters("vMsgErro").Value & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
                         vFl_OK = False
                         Exit Sub
                     End If
        
                End If
              
                db_CONS.MoveNext
        
            Loop
        
        End If
    
    End If

    vFl_OK = True

End Sub

Sub pCarregaComboCliente(lcboCliente As ComboBox, lstrParametro As String)
    
    Dim lstrSQL      As String
    Dim lstrWHERE    As String
    Dim objCliente    As Object
    Dim lstrCliente   As String
    
    lstrWHERE = ""
    lcboCliente.Clear
    lstrParametro = UCase(lstrParametro)
    
    'NOME OU CODIGO
    
    If Trim(lstrParametro) <> "" Then
        
        If IsNumeric(Trim(lstrParametro)) Then
            
            If Len(Trim(lstrParametro)) > 6 Then ' cgc=14
                
                db.Parameters.Remove "CGC": db.Parameters.Add "CGC", lstrParametro, 1
                
                lstrWHERE = lstrWHERE & " cgc = :CGC"
                
                '---------- testa CGC / CIC -----------
                On Error Resume Next
                If Len(lstrParametro) <> 11 And Len(lstrParametro) <> 14 Then
                    MsgBox "CGC/CIC Inv�lido!!!", vbCritical, "Aten��o"
                    Exit Sub
                End If
                
                'Valida CGC ou CPF
                If Len(lstrParametro) > 11 Then
                    If Not vVB_Generica_001.ValidaCGC(lstrParametro) Then
                        MsgBox "CGC Inv�lido!!!", vbCritical, "Aten��o"
                        Exit Sub
                    End If
                Else
                    If Not vVB_Generica_001.ValidaCIC(lstrParametro) Then
                        MsgBox "CPF Inv�lido!!!", vbCritical, "Aten��o"
                        Exit Sub
                    End If
                End If
                
                cboCliente = ""
            
            Else ' codigo=6
                
                db.Parameters.Remove "CODCLI": db.Parameters.Add "CODCLI", Val(lstrParametro), 1
                lstrWHERE = lstrWHERE & " cod_cliente = :CODCLI "
            
            End If
        
        Else
            
            'SE NAO FOR NUMERICO E NAO TIVER '%'
            If Right(lstrParametro, 1) <> "%" And Left(lstrParametro, 1) <> "%" And Trim(txtCodCliente) = "" Then
                lcboCliente.Text = lstrParametro
                Exit Sub
            End If
            
            db.Parameters.Remove "NOME": db.Parameters.Add "NOME", lstrParametro, 1
            
            lstrWHERE = lstrWHERE & " nome_cliente " & _
                        IIf(Left(lstrParametro, 1) = "%" Or _
                           Right(lstrParametro, 1) = "%", " LIKE ", "=") & " :NOME "
        End If
    
    End If
    
    lstrSQL = "SELECT c.cod_cliente, c.nome_cliente, c.cod_tipo_cliente, c.cod_cidade, s.cod_segmento " & _
                "FROM producao.cliente c " & _
                "LEFT JOIN producao.tipo_cliente t " & _
                "on c.cod_tipo_cliente = t.cod_tipo_cli " & _
                "INNER JOIN producao.segmento s " & _
                "on s.cod_segmento = t.cod_segmento"
    
    lstrSQL = lstrSQL & IIf(lstrWHERE = "", "", " WHERE " & lstrWHERE)
    
    Set objCliente = db.dbcreatedynaset(lstrSQL, 0&)
    
    lcboCliente.Clear
    Screen.MousePointer = vbHourglass
    While Not objCliente.EOF
        'MDIForm1.sspMsg.Caption = "PESQUISANDO CLIENTES . . . " & lcboCliente.ListCount + 1
        lcboCliente.AddItem objCliente.Fields("nome_cliente") & " - " & Format(objCliente.Fields("cod_cliente"), "000000")
        lstrCliente = objCliente.Fields("nome_cliente") & " - " & Format(objCliente.Fields("cod_cliente"), "000000")
        'vTpCliAtivarDesativar = objCliente.Fields("cod_tipo_cliente")
        'vxavier Ci&T 22/11/2012 - [ISA - 1027] - [form frmAtivarDesativar]Alterar o nome da vari�vel vTpCliAtivarDesativar para vSegCliAtivarDesativar.
        vSegCliAtivarDesativar = IIf(IsNull(objCliente.Fields("cod_segmento")), "", objCliente.Fields("cod_segmento"))
        vCidadeAtivarDesativar = objCliente.Fields("cod_cidade")
        objCliente.MoveNext
    Wend
    
    Screen.MousePointer = vbDefault
    If objCliente.RecordCount <= 1 Then
        If objCliente.RecordCount = 0 Then
            MsgBox "Cliente n�o Encontrado !", vbInformation, "Aten��o"
            Fl_Cliente_Novo = "S"
            Fl_Ja_Relacionou = "N"
            If Not IsNumeric(lstrParametro) Then
                lcboCliente.Text = Left(lstrParametro, 30)
            Else
                lcboCliente.SetFocus
            End If
        Else
            Fl_Cliente_Novo = "N"
            Fl_Ja_Relacionou = "S"
            'cboRepresentanteDPK.Enabled = False
            objCliente.MoveFirst
            lcboCliente.Text = objCliente.Fields("nome_cliente")
            txtCodCliente = objCliente.Fields("cod_cliente")
            Call pVerificaSituacao
            'cmdAtivarDesativar.SetFocus
        End If
    Else
        MsgBox "Encontrei mais de um Cliente." & Chr(10) & "Selecione um !", vbInformation, "Aten��o"
    End If
End Sub

Sub pDesativarCliente(pCodCli As String, pSit As String, pCodRep As String, pCodMsg As String, pDtMsg As String, pResp As String)

Dim vFl_Cli_Dupl_Aberto As Boolean

    '-------------------------------
    'Verifica se h� pedido em aberto
    '-------------------------------
    Set OraParameters = db.Parameters
    OraParameters.Remove "CODCLI": OraParameters.Add "CODCLI", Val(pCodCli), 1
    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    
    Criar_Cursor OraParameters, "pm_Cursor1"
    
    vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD060.Pr_Ver_Pedido_Aberto(:pm_CURSOR1, :CODCLI, :vErro, :vMsgErro)")
    
    Set db_CONS = OraParameters("pm_CURSOR1").Value
    
    If vErro <> "" Then
        vFl_OK = False
        Exit Sub
    End If
    
    vFl_Cli_Dupl_Aberto = False 'Seta default como usu�rio sem dupl pendente
    
    If Not db_CONS.EOF Then
      If db_CONS.Fields(0) > 0 Then
         'MsgBox "Cliente com PEDIDO em aberto !!! N�o � Permitido Desativar", vbCritical, "CAD060 - Cadastro de Clientes"
         vFl_Cli_Dupl_Aberto = True
         'vFl_OK = False
         'Exit Sub
      End If
    End If

    '----------------------------------------------------------------------------
    'Efetua Desativar
    '----------------------------------------------------------------------------
    Set OraParameters = db.Parameters
    OraParameters.Remove "CODCLI":  OraParameters.Add "CODCLI", Val(pCodCli), 1
    'Se o cliente n�o tiver duplicata pendente, desativa normalmente
    If vFl_Cli_Dupl_Aberto = False Then
        OraParameters.Remove "FLSIT":   OraParameters.Add "FLSIT", pSit, 1
    'Se o cliente tiver duplicata pendente, n�o desativar mas gravar a mensagem que vier no arquivo
    Else
        OraParameters.Remove "FLSIT":   OraParameters.Add "FLSIT", "M", 1
    End If
    OraParameters.Remove "CODREP":  OraParameters.Add "CODREP", 0, 1
    OraParameters.Remove "CODMSG":  OraParameters.Add "CODMSG", Val(pCodMsg), 1
    OraParameters.Remove "DTMSG":   OraParameters.Add "DTMSG", pDtMsg, 1
    OraParameters.Remove "RESP":    OraParameters.Add "RESP", pResp, 1
    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    
    db.ExecuteSQL "BEGIN PRODUCAO.PCK_Cliente.PR_GRAVA_ATIVA_DESATIVA(:CODCLI,:FLSIT,:CODREP,:CODMSG,To_Date(:DTMSG,'dd/mm/rr'),:RESP,:vErro,:vMsgErro); END;"
    
    If Val(IIf(IsNull(OraParameters("vErro")), 0, OraParameters("vErro"))) <> 0 Then
        MsgBox "Erro: " & Val(OraParameters("vErro").Value) & " - " & _
                          OraParameters("vMsgErro").Value & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
        vFl_OK = False
        Exit Sub
    End If

vFl_OK = True

End Sub

Sub pVerificaSituacao()
'Dim vVB_Generica_001 As New Funcoes
        
    'Verifica situa��o do cliente
    Set OraParameters = db.Parameters
    OraParameters.Remove "CODCLI": OraParameters.Add "CODCLI", Val(txtCodCliente), 1
    OraParameters.Remove "CGC": OraParameters.Add "CGC", 0, 1
    OraParameters.Remove "TP_CONS": OraParameters.Add "TP_CONS", 2, 1
    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    'OraParameters("vErro").ServerType = ORATYPE_NUMBER
    
    Criar_Cursor OraParameters, "PM_CURSOR1"
    vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD060.Pr_Verifica_Cliente(:PM_CURSOR1,:CODCLI, :CGC, :TP_CONS, :vErro, :vMsgErro)")
    
    If vErro = "" Then
        Set db_CONS = db.Parameters("PM_CURSOR1").Value
    Else
        Exit Sub
    End If
    
    If db_CONS.EOF Then
        MsgBox "Cliente n�o encontrado!", vbInformation, "CAD060 - Aten��o"
        Exit Sub
    End If

    txtSituacao = db_CONS!Situacao & " - " & IIf(Val(db_CONS!Situacao) = 0, "Ativo", "Desativado")
    vStatus = db_CONS!status
    
    pictSituacao.Visible = True
    pictSituacao.Picture = IIf(txtSituacao.Text = "0 - Ativo", imlFiguras.ListImages(5).Picture, imlFiguras.ListImages(4).Picture)
    
    cmdAtivarDesativar.Enabled = True
    If txtSituacao.Text = "0 - Ativo" Then
        cmdAtivarDesativar.Caption = "Desativar"
        vSituacao = 0
        fraRepres.Enabled = False
        fraMsg.Enabled = True
        Call Carrega_Combo_Mensagem("C", frmAtivarDesativar.cboMsgCliente)          'Par�metros: C=Cliente / F=Fiscal
    Else
        cmdAtivarDesativar.Caption = "Ativar"
        vSituacao = 9
        fraRepres.Enabled = True
        fraMsg.Enabled = False
        txtUltRepres.Enabled = True
        txtUltRepres = Format(db_CONS!Cod_Repr_Vend.Value, "0000")
        txtUltRepres.Enabled = False
        Call Carrega_Combo_Repres("D", frmAtivarDesativar.cboRepresDPK)             'Par�metros: D=DPK / B=BLAU
    End If

End Sub

Private Sub cboCliente_Click()
Dim vNomeCli As String
        
    If Not IsNumeric(Right(frmAtivarDesativar.cboCliente, 6)) Then Exit Sub
    
    frmAtivarDesativar.txtCodCliente.Text = Right(frmAtivarDesativar.cboCliente, 6)
    vNomeCli = Left(frmAtivarDesativar.cboCliente, Len(frmAtivarDesativar.cboCliente) - 9)
        
    Call pVerificaSituacao
        
    frmAtivarDesativar.cboCliente.Clear
    frmAtivarDesativar.cboCliente.AddItem vNomeCli
    frmAtivarDesativar.cboCliente.ListIndex = 0

End Sub


Private Sub cboCliente_KeyPress(KeyAscii As Integer)
    If Chr$(KeyAscii) = "'" Or Chr$(KeyAscii) = "�" Or Chr$(KeyAscii) = "`" Then
        KeyAscii = 0
        Beep
    Else
        KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
    End If

End Sub


Private Sub cboCliente_LostFocus()
    
    cboCliente.Text = UCase(Left(cboCliente.Text, 30))
    
    If cboCliente <> "" Then
        If Trim(txtCodCliente) = "" Then
            Call pCarregaComboCliente(frmAtivarDesativar.cboCliente, cboCliente.Text)
        End If
    End If

End Sub

Private Sub cboRepresDPK_Click()
    
    If Not IsNumeric(Mid(cboRepresDPK.Text, (InStr(1, cboRepresDPK.Text, " - ") + 3), Len(cboRepresDPK.Text))) Then
        Exit Sub
    End If
    
    lblRepresDPK.Caption = "Representante DPK: " & Mid(cboRepresDPK.Text, (InStr(1, cboRepresDPK.Text, " - ") + 3), Len(cboRepresDPK.Text))

End Sub


Private Sub cboRepresDPK_LostFocus()
Dim vAux1 As String, vAux2 As String

'lblRepresDPK = "Representante DPK: "
If IsNumeric(cboRepresDPK) Then
    vAux1 = cboRepresDPK
Else
    vAux1 = Mid(cboRepresDPK, InStr(1, cboRepresDPK, " - ") + 3, Len(cboRepresDPK))
End If
vAux2 = Mid(lblRepresDPK, InStr(1, lblRepresDPK, ":") + 2, Len(lblRepresDPK))


   If cboRepresDPK.Text <> "" Then
       If vAux1 <> vAux2 Then
          Call pCarregaComboRepresentante(cboRepresDPK, cboRepresDPK.Text, "D")
          lblRepresDPK.Caption = "Representante DPK: " & Mid(cboRepresDPK, InStr(1, cboRepresDPK, " - ") + 3, Len(cboRepresDPK))
       Else
          Call pCarregaComboRepresentante(cboRepresDPK, vAux1, "D")
          lblRepresDPK.Caption = "Representante DPK: " & Mid(cboRepresDPK, InStr(1, cboRepresDPK, " - ") + 3, Len(cboRepresDPK))
       End If
    Else
       lblRepresDPK.Caption = "Representante DPK"
    End If

End Sub


Private Sub cmdArquivo_Click()

'On Error GoTo Trata_Erro

'*****************************************************************************************
'Lay-out do Arquivo de Ativar/Desativar
'------------------------------------------
'CAMPO           TIPO           POSI��O
'--------------- -------------- -----------
'COD_CLIENTE     Numerico(6)    01 a 06
'SITUACAO        Numerico(1)    07 a 07
'COD_REPRES      Numerico(4)    08 a 11
'COD_MENSAGEM    Numerico(2)    12 a 13
'DT_MENSAGEM     Varchar(8)     14 a 21
'RESPONSAVEL     Varchar(12)    22 a 33

'Quando quiser ativar, somente preencher os campos de cod.cliente,situa��o e cod.repres
    '(os demais campos ser�o ignorados)
'Quando quiser desativar, o c�digo do representante dever� vir com zeros

'Exemplo de Arquivo para quando quiser "Desativar" Cliente:
'070269,9,0000,36,05/05/04,ALETHEA

'Exemplo de Arquivo para quando quiser "Ativar" Cliente:
'070269,0,1517
'*****************************************************************************************

'*****************************************************************************************
'Lay-out do Arquivo para Alterar C�d.Mensagem do Cliente
'------------------------------------------
'CAMPO           TIPO           POSI��O
'--------------- -------------- -----------
'COD_CLIENTE     Numerico(6)    01 a 06
'COD_MENSAGEM    Numerico(2)    07 a 08
'DT_MENSAGEM     Varchar(8)     09 a 16
'RESPONSAVEL     Varchar(12)    17 a 28

'Exemplo de Arquivo para quando quiser Alterar Mensagem do Cliente:
'070269,99,02/05/04,ALETHEA
'*****************************************************************************************

If optTpArquivo(0).Value = False And optTpArquivo(1).Value = False Then
    MsgBox "Por favor, assinale o tipo do arquivo a ser importado.", vbInformation, "CAD060 - Cadastro de Clientes"
    Exit Sub
End If

Screen.MousePointer = 11

If optTpArquivo(0).Value = True Then
    vFl_Arq_Situacao = True
Else
    vFl_Arq_Situacao = False
End If


'Call Limpa_FlexGrid(grdflexArquivo)

'Indica que o controle ser� usado para abrir o arquivo
controle.ShowOpen
lblArquivo.Caption = controle.FileName
vNomeArq = lblArquivo

'Verifica se foi indicado o diret�rio e nome do arquivo a ser lido
If lblArquivo.Caption = "" Then
    MsgBox "Indique o arquivo a ser aberto", vbInformation, "CAD060 - Aten��o"
    Screen.MousePointer = 0
    Exit Sub
End If


'Limpa Grid
For I = 0 To grdflexArquivo.Cols - 1
    grdflexArquivo.TextMatrix(1, I) = ""
Next I


grdflexArquivo.Rows = 2

If vFl_Arq_Situacao = True Then
    
    'Monta cabe�alho do grid para importa��o de arquivo Ativar/Desativar Cliente
    With grdflexArquivo
        .Cols = 11
        .ColWidth(0) = 550:     .ColAlignment(0) = 0:   .TextMatrix(0, 0) = "Reg."
        .ColWidth(1) = 750:     .ColAlignment(1) = 0:   .TextMatrix(0, 1) = "C�d."
        .ColWidth(2) = 2850:    .ColAlignment(2) = 0:   .TextMatrix(0, 2) = "Nome Cliente"
        .ColWidth(3) = 1250:    .ColAlignment(3) = 0:   .TextMatrix(0, 3) = "Situa��o"
        .ColWidth(4) = 550:     .ColAlignment(4) = 0:   .TextMatrix(0, 4) = "C�d."
        .ColWidth(5) = 2650:    .ColAlignment(5) = 0:   .TextMatrix(0, 5) = "Representante"
        .ColWidth(6) = 550:     .ColAlignment(6) = 0:   .TextMatrix(0, 6) = "C�d."
        .ColWidth(7) = 2650:    .ColAlignment(7) = 0:   .TextMatrix(0, 7) = "Mensagem"
        .ColWidth(8) = 2650:    .ColAlignment(8) = 0:   .TextMatrix(0, 8) = "Dt.Mensagem"
        .ColWidth(9) = 1550:    .ColAlignment(9) = 0:   .TextMatrix(0, 9) = "Respons�vel"
        .ColWidth(10) = 5000:   .ColAlignment(10) = 0:  .TextMatrix(0, 10) = "Observa��o"
    End With
Else
    'Monta cabe�alho do grid para importa��o de arquivo Altera��o da Mensagem do Cliente
    With grdflexArquivo
        .Cols = 8
        .ColWidth(0) = 550:     .ColAlignment(0) = 0:   .TextMatrix(0, 0) = "Reg."
        .ColWidth(1) = 750:     .ColAlignment(1) = 0:   .TextMatrix(0, 1) = "C�d."
        .ColWidth(2) = 2850:    .ColAlignment(2) = 0:   .TextMatrix(0, 2) = "Nome Cliente"
        .ColWidth(3) = 550:     .ColAlignment(3) = 0:   .TextMatrix(0, 3) = "C�d."
        .ColWidth(4) = 2650:    .ColAlignment(4) = 0:   .TextMatrix(0, 4) = "Mensagem"
        .ColWidth(5) = 2650:    .ColAlignment(5) = 0:   .TextMatrix(0, 5) = "Dt.Mensagem"
        .ColWidth(6) = 1550:    .ColAlignment(6) = 0:   .TextMatrix(0, 6) = "Respons�vel"
        .ColWidth(7) = 5000:    .ColAlignment(7) = 0:   .TextMatrix(0, 7) = "Observa��o"
    End With
End If


'//////////////////////////
'LEITURA DO ARQUIVO
'//////////////////////////
Open vNomeArq For Input As #1

'Inicializa contadores de linhas do grid
I = 0
lblMsg = ""
lblMsg.Visible = True

'L� enquanto n�o for final de arquivo
Do While Not EOF(1)

    I = I + 1
    
    lblMsg = "Importando Arquivo, aguarde ...." & I
    lblMsg.Refresh
    
    'Inicializa flags e vari�veis
    vCodCli = ""
    vNomeCli = ""
    vSit = ""
    vCodRepr = ""
    vRepres = ""
    vCodMsg = ""
    vMsg = ""
    vDtMsg = ""
    vResp = ""
    vFl_OK = True
    
    'L� registro - linha - do arquivo, joga na vari�vel  e seta ponteiro para o pr�ximo
    Line Input #1, varLinhaArquivo
    'Se linha lida for em branco, sai do loop
    If Mid(Trim(varLinhaArquivo), 1, 33) = "" Then
        Exit Do
    End If
    
    With grdflexArquivo
    
        'Qtd. Linhas do grid ser� correspondente � qtd. de linhas lidas do arquivo texto
        .Rows = I + 1
        .Row = I
        
        '1a Coluna vai receber a numera��o de registros lidos do arquivo
        .TextMatrix(I, 0) = Format(I, "0000")
        
        If vFl_Arq_Situacao = True Then
            'Atribui valores da linha do arquivo (ATivar/Desativar) para as vari�veis
            vCodCli = Trim(Mid(varLinhaArquivo, 1, 6))
            vSit = Trim(Mid(varLinhaArquivo, 7, 1))
            vCodRepr = Trim(Mid(varLinhaArquivo, 8, 4))
            vCodMsg = Trim(Mid(varLinhaArquivo, 12, 2))
            vDtMsg = Trim(Mid(varLinhaArquivo, 14, 8))
            vResp = UCase(Trim(Mid(varLinhaArquivo, 22, 12)))
            Y = 10   'Coluna de Observa��o
        Else
            'Atribui valores da linha do arquivo (Troca de Msg) para as vari�veis
            vCodCli = Trim(Mid(varLinhaArquivo, 1, 6))
            vCodMsg = Trim(Mid(varLinhaArquivo, 7, 2))
            vDtMsg = Trim(Mid(varLinhaArquivo, 9, 8))
            vResp = UCase(Trim(Mid(varLinhaArquivo, 17, 12)))
            Y = 7   'Coluna de Observa��o
        End If
        
        '-----------
        'Valida��es
        '-----------
        If vCodCli = "" Then
            .TextMatrix(I, Y) = "C�d.Cliente Inv�lido"
            vFl_OK = False
            
        'Se for arquivo de ativar/desativar, e n�o tiver o situa��o para que se quer mudar o cliente
        ElseIf vFl_Arq_Situacao = True And vSit = "" Then
            .TextMatrix(I, Y) = "Situa��o Inv�lida"
            vFl_OK = False
        
        'Se for arquivo de ativar/desativar, e situa��o for diferente de 0 ou 9
        ElseIf vFl_Arq_Situacao = True And (vSit <> "0" And vSit <> "9") Then
            .TextMatrix(I, Y) = "Situa��o Inv�lida"
            vFl_OK = False
        
        'Se for arquivo de ativar/desativar, se quiser ativar o cliente e o repres.=branco
        ElseIf vFl_Arq_Situacao = True And vSit = "0" And (vCodRepr = "" Or Val(vCodRepr) = 0) Then
            .TextMatrix(I, Y) = "C�d.Representante Inv�lido"
            vFl_OK = False
        
        'Se for arquivo de ativar/desativar, se quiser desativar o cliente e a mensagem = branco
        ElseIf vFl_Arq_Situacao = True And vSit = "9" And vCodMsg = "" Then
            .TextMatrix(I, Y) = "C�d.Mensagem Inv�lida"
            vFl_OK = False
        
        'Se for arquivo de ativar/desativar, se quiser desativar o cliente e a dt.mensagem = branco
        ElseIf vFl_Arq_Situacao = True And vSit = "9" And vDtMsg = "" Then
            .TextMatrix(I, Y) = "Dt.Mensagem Inv�lida"
            vFl_OK = False
        
        'Se for arquivo de ativar/desativar, se quiser desativar o cliente e a dt.mensagem = branco
        ElseIf vFl_Arq_Situacao = True And vSit = "9" And vResp = "" Then
            .TextMatrix(I, Y) = "Respons�vel Inv�lido"
            vFl_OK = False
        
        'Este caso n�o ser� mais inv�lido, pois poder� existir a op��o de retirar a mensagem - Conf.solic.Set/04
        'Se for arquivo de altera��o de mensagem
        'ElseIf vFl_Arq_Situacao = False And vCodMsg = "" Then
        '    .TextMatrix(i, Y) = "C�d.Mensagem Inv�lida"
        '    vFl_OK = False
        'Se for arquivo de altera��o de mensagem e a dt.mensagem = branco
        'ElseIf vFl_Arq_Situacao = False And vDtMsg = "" Then
        '    .TextMatrix(i, Y) = "Dt.Mensagem Inv�lida"
        '    vFl_OK = False
        'Se for arquivo de altera��o de mensagem e a dt.mensagem = branco
        'ElseIf vFl_Arq_Situacao = False And vResp = "" Then
        '    .TextMatrix(i, Y) = "Respons�vel Inv�lido"
        '    vFl_OK = False
        End If
        
        'Se a linha passar nas 1as. valida��es, continua a importa��o
        If vFl_OK = True Then
            
            'Valida c�digo do cliente, independente do tipo de arquivo
            Set OraParameters = db.Parameters
            OraParameters.Remove "CODCLI": OraParameters.Add "CODCLI", Val(vCodCli), 1
            OraParameters.Remove "CGC": OraParameters.Add "CGC", 0, 1
            OraParameters.Remove "TP_CONS": OraParameters.Add "TP_CONS", 2, 1
            OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
            OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
            
            Criar_Cursor OraParameters, "PM_CURSOR1"
            
            vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD060.Pr_Verifica_Cliente(:pm_CURSOR1, :CODCLI, :CGC, :TP_CONS, :vErro, :vMsgErro)")
            Set db_CONS = OraParameters("pm_CURSOR1").Value
            
            If vErro <> "" Then
                Exit Sub
            End If
            
            If db_CONS.EOF Then
                .TextMatrix(I, Y) = "C�d.Cliente Inv�lido"
                vFl_OK = False
            Else
            
                vNomeCli = Mid(db_CONS!nome_cliente.Value, 1, 25)
                
                If vFl_Arq_Situacao = True Then
                    If Val(vSit) = Val(db_CONS!Situacao) Then
                        .TextMatrix(I, Y) = "Cliente j� est� " & IIf(Val(vSit) = 0, "Ativo", "Desativado")
                        vFl_OK = False
                    End If
                End If
                
                If vFl_OK = True Then
                    
                    'Se quiser ativar cliente, vai verificar o c�digo do representante
                    If vFl_Arq_Situacao = True And vSit = "0" Then
                    
                        Set OraParameters = db.Parameters
                        OraParameters.Remove "CODCLI": OraParameters.Add "CODCLI", 0, 1
                        OraParameters.Remove "CODREP": OraParameters.Add "CODREP", Val(vCodRepr), 1
                        OraParameters.Remove "TP_CONS": OraParameters.Add "TP_CONS", 3, 1
                        OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
                        OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
                        
                        Criar_Cursor OraParameters, "PM_CURSOR1"
                            
                        vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD060.Pr_Verifica_Repres(:pm_CURSOR1, :CODCLI, :CODREP, :TP_CONS, :vErro, :vMsgErro)")
                        Set db_CONS = OraParameters("pm_CURSOR1").Value
    
                        If vErro <> "" Then
                            Exit Sub
                        End If
                        
                        If db_CONS.EOF Then
                            .TextMatrix(I, Y) = "C�d.Representante Inv�lido"
                            vFl_OK = False
                        Else
                            vRepres = db_CONS!Pseudonimo.Value
                        End If
                    
                    End If
                    
                    'Se quiser desativar cliente ou quiser importar arquivo de altera��o de mensagem
                    'vai verificar o c�digo da mensagem
                    If (vFl_Arq_Situacao = True And vSit = "9") Or _
                       (vFl_Arq_Situacao = False And Trim(vCodMsg) <> "") Then
                
                        If Val(vCodMsg) <> 0 Then
                
                            Set OraParameters = db.Parameters
                            OraParameters.Remove "CODMSG": OraParameters.Add "CODMSG", Val(vCodMsg), 1
                            OraParameters.Remove "TP_CONS": OraParameters.Add "TP_CONS", 0, 1
                            OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
                            OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
                            
                            Criar_Cursor OraParameters, "pm_Cursor1"
                            
                            vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD060.Pr_Select_Mensagem(:pm_CURSOR1, :CODMSG, :TP_CONS, :vErro, :vMsgErro)")
                            Set db_CONS = OraParameters("pm_Cursor1").Value
                                                            
                            If vErro <> "" Then
                                Exit Sub
                            End If
                            
                            If db_CONS.EOF Then
                                .TextMatrix(I, Y) = "C�d.Mensagem Inv�lida"
                                vFl_OK = False
                            Else
                                vMsg = db_CONS!Desc_Mens.Value
                            End If
                        
                        End If
                
                    End If
        
                End If
            End If
        
        End If
        
        'Alimenta o grid com as informa��es do arquivo, independente de estarem corretas ou n�o
        If vFl_Arq_Situacao = True Then
            .TextMatrix(I, 1) = vCodCli
            .TextMatrix(I, 2) = vNomeCli
            .TextMatrix(I, 3) = IIf(vSit = "0", "0 - Ativar", "9 - Desativar")
            .TextMatrix(I, 4) = IIf(vCodRepr = "0000", "", vCodRepr)
            .TextMatrix(I, 5) = vRepres
            .TextMatrix(I, 6) = vCodMsg
            .TextMatrix(I, 7) = vMsg
            .TextMatrix(I, 8) = vDtMsg
            .TextMatrix(I, 9) = vResp
        Else
            .TextMatrix(I, 1) = vCodCli
            .TextMatrix(I, 2) = vNomeCli
            .TextMatrix(I, 3) = vCodMsg
            .TextMatrix(I, 4) = vMsg
            .TextMatrix(I, 5) = vDtMsg
            .TextMatrix(I, 6) = vResp
        End If

    End With

'L� pr�xima linha
Loop


Screen.MousePointer = 0
MsgBox "Arquivo Lido!", vbInformation, "Aten��o!"
lblMsg = I & " Linhas Lidas do Arquivo!"
lblMsg.Refresh
Close #1    ' Close file.
Screen.MousePointer = 0
Exit Sub
        
Trata_Erro:
    If Err.Number = 53 Then
        MsgBox "Arquivo de Controle n�o Encontrado. ", vbCritical, "CAD060 - Cadastro de Cliente"
    Else
        Resume Next
    End If



End Sub

Private Sub cmdAtivarDesativar_Click()
Dim vFl_Situacao As String
Dim vLOJA As String
Dim objConexao As Object
Dim vMsg As String
Dim vrep As String


If MsgBox("Confirma " & IIf(vSituacao = 0, "DESATIVAR", "ATIVAR") & " o cliente ?", vbQuestion + vbYesNo, "CAD060 - Cadastro de Clientes") = vbYes Then
    
    '----------------------------------------------------------------------------
    'Se quiser desativar cliente, vai verificar se cliente tem pedidos em aberto
    '----------------------------------------------------------------------------
    If vSituacao = 0 Then   'Situacao atual do cliente � ativo - Quer desativar
    
        If cboMsgCliente.Text = "" Then
            MsgBox "Por favor, escolha a Mensagem a ser gravada para justificar a inatividade do cliente.", vbInformation, "CAD060 - Aten��o"
            cboMsgCliente.SetFocus
            Exit Sub
        End If
        If txtDtMsg.Text = "" Then
            MsgBox "Por favor, digite a Data da Mensagem a ser gravada para justificar a inatividade do cliente.", vbInformation, "CAD060 - Aten��o"
            txtDtMsg.SetFocus
            Exit Sub
        End If
        If txtResponsavel.Text = "" Then
            MsgBox "Por favor, digite o nome do respons�vel pela inatividade do cliente.", vbInformation, "CAD060 - Aten��o"
            txtResponsavel.SetFocus
            Exit Sub
        End If
        
        vFl_Situacao = "A"
        vMsg = IIf(IsNumeric(Right(cboMsgCliente, 2)), Right(cboMsgCliente, 2), "0")
        
        Call pDesativarCliente(txtCodCliente, vFl_Situacao, "0", vMsg, txtDtMsg, txtResponsavel)
    
        If vFl_OK = True Then
            MsgBox "Cliente Desativado com sucesso!", vbInformation, "CAD060 - Cadastro de Clientes"
        End If
            
    '----------------------------------------------------------------------------
    'Situa��o atual � desativado - Quer ativar o cliente
    '----------------------------------------------------------------------------
    Else
            
        If cboRepresDPK.Text = "" Then
            MsgBox "Por favor, escolha o Representante a ser gravado para ativar o cliente.", vbInformation, "CAD060 - Aten��o"
            cboRepresDPK.SetFocus
            Exit Sub
        End If
            
        vFl_Situacao = "D"
       
        'Alterado pois os representantes estavam aparecendo com dois h�fens e estava dando erro - 22/02/06 - Alethea
        'vrep = Mid(cboRepresDPK.Text, (InStr(1, cboRepresDPK.Text, " - ") + 3), Len(cboRepresDPK.Text))
        vrep = Mid(Right(cboRepresDPK.Text, 6), 1, 4)
        
        Call pAtivarCliente(txtCodCliente, vFl_Situacao, vrep, "0", "", "")
    
        If vFl_OK = True Then
            MsgBox "Cliente Ativado com sucesso!", vbInformation, "CAD060 - Cadastro de Clientes"
        End If
    
    End If
    


End If
    
End Sub

Private Sub cmdAutomatico_Click()
    fraSitAutomatico.Visible = True
    fraSitAutomatico.Top = 56
    fraSitManual.Visible = False
End Sub

Private Sub cmdCancelar_Click()

    txtCodCliente = ""
    cboCliente = ""
    txtSituacao = ""
    pictSituacao.Visible = False
    cmdAtivarDesativar.Enabled = False
    cboRepresDPK.Text = ""
    txtUltRepres.Text = ""
    cboMsgCliente.Text = ""
    txtDtMsg.Text = ""
    txtResponsavel.Text = ""
    fraRepres.Enabled = False
    fraMsg.Enabled = False

End Sub

Private Sub cmdGeraLog_Click()

    Dim j As Long           'Ponteiro de caracter no campo observa��o
    Dim Y As Integer
    Dim vTamObs As Long     'Guarda a qtd. de caracteres da coluna Observa��o
    Dim sOBS As String      'Guarda o conte�do da coluna de Observa��es
    Dim sCODCLI As String   'Guarda o c�digo do cliente para impress�o
    Dim sTP_ARQ As String
    Dim sLINHA As String
    Dim vArqLog As String
        
    If MsgBox("Confirma gera��o do arquivo de log?", vbYesNo, "CAD060 - Cadastro de Clientes") = vbYes Then
    
        If vFl_Arq_Situacao = True Then
            sTP_ARQ = "Ativar/Desativar Cliente"
            Y = 10
        Else
            sTP_ARQ = "Altera��o de Mensagem de Cliente"
            Y = 5
        End If
    
        'Gera arquivo de log com o nome LogCAD060_DDMMAA.TXT
        vArqLog = "C:\Windows\Temp\LogCAD060_" & Mid(vDtHoje, 1, 2) & Mid(vDtHoje, 4, 2) & Mid(vDtHoje, 7, 2) & ".txt"
        Open vArqLog For Output As #1
        
        Print #1, "Arquivo Importado: " & vNomeArq & " - " & sTP_ARQ
        Print #1, "Arquivo de Log...: " & vArqLog
        Print #1, ""
        Print #1, "|-----|-----------|------------------------------------------------------------|"
        Print #1, "|Linha|Cod.Cliente|               Observa��es de Inconsist�ncia                |"
        Print #1, "|-----|-----------|------------------------------------------------------------|"
        
        
        'Pega linha a linha do grid para gravar as que cont�m campo de Observa��o <> ""
        With grdflexArquivo
            
            For I = 1 To .Rows - 1
                
                sLINHA = Trim(.TextMatrix(I, 0))
                sCODCLI = Trim(.TextMatrix(I, 1))
                sOBS = Trim(.TextMatrix(I, Y))
                
                'Se a coluna de Observa��o n�o estiver em branco
                If sOBS <> "" Then
                    
                    'Zero ponteiro que vai me indicar que caracter no campo obs. estou lendo
                    j = 0
                    'Pego o tamanho da observa��o para esta linha
                    vTamObs = CLng(Len(sOBS))
                    
                    'Se o campo de observa��o contiver mais de 60 caracteres, o arquivo de log ter�
                    'que conter mais de 1 linha de mensagens para aquele item
                    'Conto 19 pois antes da Obs., a 1a. linha ir� conter outras informa��es (19 caracteres)
                    'E para a impress�o correta do arquivo s� pode ter 80 colunas
                    
                    'Se for menor ou igual a 60, gravo a linha no arquivo normalmente
                    If vTamObs <= 60 Then
                        Print #1, " " & _
                                  sLINHA & Space(5 - Len(sLINHA)) & "   " & _
                                  sCODCLI & Space(6 - Len(sCODCLI)) & "    " & _
                                  sOBS
                    'Sen�o
                    Else
                        'Imprimo os primeiros 60 caracteres
                        Print #1, " " & _
                                  sLINHA & Space(5 - Len(sLINHA)) & "   " & _
                                  sCODCLI & Space(6 - Len(sCODCLI)) & "    " & _
                                  Mid(sOBS, 1, 60)
                        
                        'Tamanho agora vai ser igual ao (tamanho original - 25)
                        vTamObs = vTamObs - 60
                        'Indico de que caracter vou come�ar a ler no cpo. Observa��o
                        j = j + 61
                        
                        'Enquanto o tamanho for maior que 0
                        Do While vTamObs > 0
                            'Se tamanho maior que 60, imprime os pr�ximos 60 caracteres e atualiza tam.e ponteiro
                            If vTamObs > 60 Then
                                Print #1, Space(19) & Mid(sOBS, j, 60)
                                vTamObs = vTamObs - 60
                                j = j + 61
                            'Se tamanho menor que 60, imprime os caracteres que faltam
                            Else
                                Print #1, Space(19) & Mid(sOBS, j, vTamObs)
                                vTamObs = 0
                            End If
                        Loop
                    End If
                    
                End If
                    
            Next I
        End With
    
        Print #1, ""
        Print #1, "***************** FIM DO ARQUIVO DE LOG ***********************"
        
        Close #1
    
        MsgBox "Arquivo gerado com sucesso!", vbInformation, "Aten��o!"
    
    End If

End Sub

Private Sub cmdGravar_Click()
Dim vFl_Situacao As String
    
'Verifica se o grid cont�m informa��es
If Trim(grdflexArquivo.TextMatrix(1, 0)) = "" Then
    MsgBox "N�o existem informa��es a serem gravadas!", vbInformation, "CAD060 - Cadastro de Cliente"
    Exit Sub
End If
    
'Dependendo do tipo do arquivo, a coluna de Observa��o ser� diferente
If vFl_Arq_Situacao = True Then
    Y = 10
Else
    Y = 7
End If

'Varre todo o grid
With grdflexArquivo
    
    For I = 1 To grdflexArquivo.Rows - 1
    
        'Se n�o tiver Observa��es de Inconsist�ncias, pega valores para gravar
        If Trim(.TextMatrix(I, Y)) = "" Then
            
            'Est� importando arquivo de ativar/desativar cliente
            If vFl_Arq_Situacao = True Then
                
                vCodCli = Trim(.TextMatrix(I, 1))
                vSit = Val(Mid(Trim(.TextMatrix(I, 3)), 1))
                vCodRepr = Trim(.TextMatrix(I, 4))
                vCodMsg = Trim(.TextMatrix(I, 6))
                vDtMsg = Trim(.TextMatrix(I, 8))
                vResp = Trim(.TextMatrix(I, 9))
                
                'Quer ativar o cliente
                If Val(vSit) = 0 Then
                    'O cliente est� desativado e quer ativ�-lo
                    vFl_Situacao = "D"
                    Call pAtivarCliente(vCodCli, vFl_Situacao, vCodRepr, "0", "", "")
                Else
                    'O cliente est� ativo e quer desativ�-lo
                    vFl_Situacao = "A"
                    Call pDesativarCliente(vCodCli, vFl_Situacao, "0", vCodMsg, vDtMsg, vResp)
                End If
            
            'Est� importando arquivo de altera��o de mensagem cliente
            Else
                vCodCli = Trim(.TextMatrix(I, 1))
                vCodMsg = Trim(.TextMatrix(I, 3))
                vDtMsg = Trim(.TextMatrix(I, 5))
                vResp = Trim(.TextMatrix(I, 6))
            
                Call pAlteraMensagem(vCodCli, vCodMsg, vDtMsg, vResp)
            
            End If
        End If

    Next I

End With
    
If vFl_OK = True Then
    MsgBox "Arquivo Importado, gravado com sucesso!", vbInformation, "CAD060 - Cadastro de Cliente"
End If

End Sub

Private Sub cmdLayOut_Click()

frmLayOut.Show 1
'*****************************************************************************************
'Lay-out do Arquivo de Ativar/Desativar
'------------------------------------------
'CAMPO           TIPO           POSI��O
'--------------- -------------- -----------
'COD_CLIENTE     Numerico(6)    01 a 06
'SITUACAO        Numerico(1)    07 a 07
'COD_REPRES      Numerico(4)    08 a 11
'COD_MENSAGEM    Numerico(2)    12 a 13
'DT_MENSAGEM     Varchar(8)     14 a 21
'RESPONSAVEL     Varchar(12)    22 a 33

'Quando quiser ativar, somente preencher os campos de cod.cliente,situa��o e cod.repres
    '(os demais campos ser�o ignorados)
'Quando quiser desativar, o c�digo do representante dever� vir com zeros

'Exemplo de Arquivo para quando quiser "Desativar" Cliente:
'070269,9,0000,36,05/05/04,ALETHEA

'Exemplo de Arquivo para quando quiser "Ativar" Cliente:
'070269,0,1517
'*****************************************************************************************

'*****************************************************************************************
'Lay-out do Arquivo para Alterar C�d.Mensagem do Cliente
'------------------------------------------
'CAMPO           TIPO           POSI��O
'--------------- -------------- -----------
'COD_CLIENTE     Numerico(6)    01 a 06
'COD_MENSAGEM    Numerico(2)    07 a 08

'Exemplo de Arquivo para quando quiser Alterar Mensagem do Cliente:
'070269,99
'*****************************************************************************************


End Sub

Private Sub cmdManual_Click()
    fraSitManual.Visible = True
    fraSitManual.Top = 56
    fraSitAutomatico.Visible = False
End Sub

Private Sub cmdVoltar_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    'Ocultar a barra de botoes do MDI
    mdiCAD060.SSPanel1.Visible = False
    
    Me.Top = mdiCAD060.Menu.Height * 2
    Me.Left = 50
    
    Me.Height = 6100
    
    'Call Alinha_Formulario(frmAtivarDesativar)
    
    Call cmdCancelar_Click
    lblMsg.Visible = False
    fraSitManual.Visible = True

End Sub

Private Sub Form_Unload(Cancel As Integer)
    mdiCAD060.SSPanel1.Visible = True
End Sub

Private Sub optTpArquivo_Click(Index As Integer)

    If optTpArquivo(0).Value = True Or optTpArquivo(1).Value = True Then
        fraArquivo.Visible = True
    Else
        fraArquivo.Visible = False
    End If
    
End Sub


Private Sub txtDtMsg_KeyPress(KeyAscii As Integer)
    vVB_Generica_001.Data KeyAscii, txtDtMsg
End Sub


Private Sub txtResponsavel_KeyPress(KeyAscii As Integer)
    If Chr$(KeyAscii) = "'" Or Chr$(KeyAscii) = "�" Or Chr$(KeyAscii) = "`" Then
        KeyAscii = 0
        Beep
    Else
        KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
    End If
End Sub


