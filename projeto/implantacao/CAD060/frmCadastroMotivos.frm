VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form frmCadastroMotivos 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cadastro de Motivos de Recusa"
   ClientHeight    =   4095
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6720
   Icon            =   "frmCadastroMotivos.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4095
   ScaleWidth      =   6720
   Begin VB.TextBox txtDescricao 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   765
      MaxLength       =   100
      TabIndex        =   4
      Top             =   1125
      Width           =   5865
   End
   Begin VB.TextBox txtCodigo 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   45
      Locked          =   -1  'True
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   1125
      Width           =   645
   End
   Begin MSComctlLib.ListView lsvMotivos 
      Height          =   2625
      Left            =   45
      TabIndex        =   2
      Top             =   1440
      Width           =   6585
      _ExtentX        =   11615
      _ExtentY        =   4630
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "C�digo"
         Object.Width           =   1235
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Descri��o do Motivo"
         Object.Width           =   8819
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   0
      Top             =   810
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   5940
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadastroMotivos.frx":23D2
      PICN            =   "frmCadastroMotivos.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdAdd 
      Height          =   690
      Left            =   90
      TabIndex        =   7
      TabStop         =   0   'False
      ToolTipText     =   "Adicionar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadastroMotivos.frx":30C8
      PICN            =   "frmCadastroMotivos.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdSalvar 
      Height          =   690
      Left            =   1680
      TabIndex        =   8
      TabStop         =   0   'False
      ToolTipText     =   "Salvar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadastroMotivos.frx":3DBE
      PICN            =   "frmCadastroMotivos.frx":3DDA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdExcluir 
      Height          =   690
      Left            =   3255
      TabIndex        =   9
      TabStop         =   0   'False
      ToolTipText     =   "Excluir"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadastroMotivos.frx":4AB4
      PICN            =   "frmCadastroMotivos.frx":4AD0
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdEditar 
      Height          =   690
      Left            =   885
      TabIndex        =   10
      TabStop         =   0   'False
      ToolTipText     =   "Editar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadastroMotivos.frx":57AA
      PICN            =   "frmCadastroMotivos.frx":57C6
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdDesfazer 
      Height          =   690
      Left            =   2460
      TabIndex        =   11
      TabStop         =   0   'False
      ToolTipText     =   "Desfazer"
      Top             =   30
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadastroMotivos.frx":64A0
      PICN            =   "frmCadastroMotivos.frx":64BC
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Descri��o"
      Height          =   195
      Left            =   765
      TabIndex        =   6
      Top             =   900
      Width           =   720
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "C�digo"
      Height          =   195
      Left            =   45
      TabIndex        =   5
      Top             =   900
      Width           =   495
   End
End
Attribute VB_Name = "frmCadastroMotivos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim vAcao As Byte '1-Adicionar, 2-Editar, 3-Excluir

Private Sub cmdAdd_Click()
   vAcao = 1
   
   Status_botoes True, False, True, False, True, False
   
   txtCodigo = ""
   txtDescricao = ""
   
   
    If lsvMotivos.ListItems.Count = 0 Then
        txtCodigo = 1
    Else
        txtCodigo = lsvMotivos.ListItems(lsvMotivos.ListItems.Count) + 1
    End If
   
   
   txtDescricao.Enabled = True
   txtDescricao.SetFocus

   lsvMotivos.Enabled = False

End Sub

Private Sub cmdDesfazer_Click()
    txtCodigo = ""
    txtDescricao = ""
        
    txtDescricao.Enabled = False
    
    Status_botoes True, True, False, False, False, False

    lsvMotivos.Enabled = True
End Sub

Private Sub cmdEditar_Click()
    vAcao = 2
    
    If txtCodigo = "" Then
        MsgBox "Selecione um item da lista.", vbInformation, "Aten��o"
        Exit Sub
    End If
    
    Status_botoes True, False, True, False, True, False
    lsvMotivos.Enabled = False
    txtDescricao.Enabled = True
    txtDescricao.SetFocus
End Sub

Private Sub cmdExcluir_Click()
    
    vAcao = 3
    If txtCodigo = "" Then
        MsgBox "Selecione um item da lista.", vbInformation, "Aten��o"
        Exit Sub
    End If
    If MsgBox("Confirma Exclus�o do item selecionado ?", vbQuestion + vbYesNo, "Aten��o") = vbYes Then
        'Deletar o registro selecioando
        cmdSalvar_Click
    End If
End Sub

Private Sub cmdSalvar_Click()
    
    If txtDescricao = "" Then
        MsgBox "Digite uma Descri��o.", vbInformation, "Aten��o"
        Exit Sub
    End If
    
    lsvMotivos.Enabled = False
    
    'Gravar os dados
    db.Parameters.Remove "Acao"
    db.Parameters.Add "Acao", vAcao, 1

    db.Parameters.Remove "Codigo"
    db.Parameters.Add "Codigo", txtCodigo, 1

    db.Parameters.Remove "Descricao"
    db.Parameters.Add "Descricao", txtDescricao, 1
    
    db.Parameters.Remove "CodErro"
    db.Parameters.Add "CodErro", 0, 2
    
    db.Parameters.Remove "TxtErro"
    db.Parameters.Add "TxtErro", "", 2

    db.Executesql "BEGIN PRODUCAO.PCK_CAD060.Pr_Ins_Upd_Del_MotivosRecusa(:Acao, :Codigo, :Descricao, :CodErro, :TxtErro); END;"
    
    If db.Parameters("CodErro") <> 0 Then
        MsgBox "Motivo n�o foi cadastrado." & vbCrLf & "C�digo do Erro: " & db.Parameters("CodErro") & vbCrLf & _
        "Descri��o: " & db.Parameters("TxtErro")
        Exit Sub
    End If
    
    txtCodigo = ""
    txtDescricao = ""
    txtDescricao.Enabled = False
    
    Status_botoes True, True, False, False, False, False
    
    Preencher_lsvMotivos
    
    lsvMotivos.Enabled = True
    
End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
        
    Me.Top = (mdiCAD060.Height - (Me.Height + mdiCAD060.SSPanel1.Height + mdiCAD060.Menu.Height + mdiCAD060.stbBarra.Height)) / 2
    Me.Left = (mdiCAD060.Width - Me.Width) / 2
    
    Preencher_lsvMotivos
    Status_botoes True, True, False, False, False, False
    txtDescricao.Enabled = False
        
End Sub

Private Sub Preencher_lsvMotivos()
    Dim litem As ListItem
    
    lsvMotivos.ListItems.Clear
    
    Criar_Cursor db.Parameters, "PM_CURSOR1"
    'vVB_Generica_001.Retorna_Cursor db, "pm_CURSOR1", "PRODUCAO.PCK_CAD060.Pr_Select_MotivosRecusa"
    vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD060.Pr_Select_MotivosRecusa(:PM_CURSOR1)")
    If vErro <> "" Then
        Exit Sub
    End If
    Set rst = db.Parameters("PM_CURSOR1").Value
    
    For I = 1 To rst.recordcount
        Set litem = lsvMotivos.ListItems.Add
        litem = rst("Cod_Motivo")
        litem.SubItems(1) = rst("Descricao")
        rst.movenext
    Next
    
    rst.Close
    Set rst = Nothing
End Sub

Private Sub Adicionar_lsvMotivos()
    
    Dim litem As ListItem
    
    Set litem = lsvMotivos.ListItems.Add
    litem = txtCodigo
    litem.SubItems(1) = txtDescricao
    
    txtCodigo = ""
    txtDescricao = ""
    
End Sub

Private Sub Status_botoes(Voltar As Boolean, Adicionar As Boolean, Salvar As Boolean, Editar As Boolean, Desfazer As Boolean, Excluir As Boolean)
    cmdVoltar.Enabled = Voltar
    cmdAdd.Enabled = Adicionar
    cmdSalvar.Enabled = Salvar
    cmdEditar.Enabled = Editar
    cmdDesfazer.Enabled = Desfazer
    cmdExcluir.Enabled = Excluir
End Sub

Private Sub lsvMotivos_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    lsvMotivos.Sorted = True
    lsvMotivos.SortKey = ColumnHeader.Index - 1
    lsvMotivos.SortOrder = lvwAscending
End Sub

Private Sub lsvMotivos_ItemClick(ByVal Item As MSComctlLib.ListItem)
    Status_botoes True, True, False, True, False, True
    
    txtCodigo = lsvMotivos.SelectedItem
    txtDescricao = lsvMotivos.SelectedItem.SubItems(1)

End Sub

Private Sub txtDescricao_KeyPress(KeyAscii As Integer)
    If Chr$(KeyAscii) = "'" Or Chr$(KeyAscii) = "�" Or Chr$(KeyAscii) = "`" Then
        KeyAscii = 0
        Beep
    Else
        KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
    End If
End Sub
