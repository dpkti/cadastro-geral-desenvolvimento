VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Begin VB.Form frmMotivoRecusa 
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "CAD060 - Motivo da Recusa"
   ClientHeight    =   2385
   ClientLeft      =   1545
   ClientTop       =   2175
   ClientWidth     =   8715
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2385
   ScaleWidth      =   8715
   Begin VB.ComboBox cboMotivoRecusa 
      BackColor       =   &H00C0E0FF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   330
      Left            =   150
      Style           =   2  'Dropdown List
      TabIndex        =   5
      Top             =   1275
      Width           =   8475
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   90
      TabIndex        =   0
      Top             =   90
      Width           =   8550
      Begin VB.Label Label3 
         BackColor       =   &H00FFFFFF&
         Caption         =   "CGC"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   6150
         TabIndex        =   4
         Top             =   165
         Width           =   1620
      End
      Begin VB.Label lblCGCRecusado 
         BackColor       =   &H00C0E0FF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   345
         Left            =   6135
         TabIndex        =   3
         Top             =   390
         Width           =   2325
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFFFFF&
         Caption         =   "Cliente"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   120
         TabIndex        =   2
         Top             =   165
         Width           =   1620
      End
      Begin VB.Label lblClienteRecusado 
         BackColor       =   &H00C0E0FF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   345
         Left            =   105
         TabIndex        =   1
         Top             =   390
         Width           =   5730
      End
   End
   Begin Bot�o.cmd cmdRecusar 
      Height          =   585
      Left            =   7440
      TabIndex        =   7
      ToolTipText     =   "Recusar Solicita��o"
      Top             =   1725
      Width           =   585
      _ExtentX        =   1032
      _ExtentY        =   1032
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmMotivoRecusa.frx":0000
      PICN            =   "frmMotivoRecusa.frx":001C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   585
      Left            =   8055
      TabIndex        =   8
      ToolTipText     =   "Voltar"
      Top             =   1725
      Width           =   585
      _ExtentX        =   1032
      _ExtentY        =   1032
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmMotivoRecusa.frx":08F6
      PICN            =   "frmMotivoRecusa.frx":0912
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label2 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Motivo da Recusa"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   165
      TabIndex        =   6
      Top             =   1035
      Width           =   1620
   End
End
Attribute VB_Name = "frmMotivoRecusa"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdRecusar_Click()
Dim vCodMotivoRecusa As Integer

'Verifica se o c�digo de motivo de recusa � v�lido
vCodMotivoRecusa = Val(Left(cboMotivoRecusa, 2))

'Antes de mais nada, � preciso escolher o motivo da recusa
If Trim(cboMotivoRecusa.Text) = "" Or Trim(cboMotivoRecusa.Text) = "0" Or _
   Trim(cboMotivoRecusa.Text) = 0 Or vCodMotivoRecusa = 0 Then
    MsgBox "Favor escoher o motivo da recusa da solicita��o do cliente", vbInformation, "CAD060 - Cadastro de Cliente"
    Exit Sub
End If
    
    
If MsgBox("Confirma recusa do cliente?", vbYesNo, "CAD060 - Cadastro de Cliente") = vbNo Then
    Exit Sub
End If

'Se solicita��o de inclus�o;altera��o/recadastro do cliente, tiver vindo pela web
If vFl_Analise = True Then

    'Pega a data e hora da altera��o
    Sql = "SELECT TO_CHAR(sysdate,'dd/mm/yy hh24:mi:ss') FROM dual "
    Set db_Hora = db.dbcreatedynaset(Sql, 0&)
    vDtHoraHoje = db_Hora.Fields(0).Value

    Set OraParameters = db_INT.Parameters
    OraParameters.Remove "CGC": OraParameters.Add "CGC", Val(vCGC_Analisado), 1
    OraParameters.Remove "TPSOL": OraParameters.Add "TPSOL", Val(vFl_Tipo_Solicitacao), 1
    'OraParameters.Remove "DTANAL": OraParameters.Add "DTANAL", vDtHoje, 1
    OraParameters.Remove "DTANAL": OraParameters.Add "DTANAL", vDtHoraHoje, 1
    OraParameters.Remove "SITUACAO": OraParameters.Add "SITUACAO", 9, 1
    OraParameters.Remove "CODUSER": OraParameters.Add "CODUSER", Val(vCodUser_Intranet), 1
    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    
    'Independente do tipo da solicita��o, vai alterar na Controle_Cliente_Web: Dt.Analise = Dt.Hoje, Situacao = 9 (Recusado), Cod_Usuario_Credito = vUser_Intranet
    db_INT.ExecuteSQL "BEGIN INTRANET.PCK_CLIENTES_TESTE.PR_ATU_PRECAD_CTRCLIWEB" & _
                             "(:CGC,:TPSOL,TO_DATE(:DTANAL,'DD/MM/RR HH24:MI:SS'),:SITUACAO," & _
                             " :CODUSER,:vErro,:vMsgErro); END;"
    If Val(IIf(IsNull(OraParameters("vErro")), 0, OraParameters("vErro"))) <> 0 Then
       MsgBox "Erro: " & Val(OraParameters("vErro").Value) & " - " & _
                       OraParameters("vMsgErro").Value & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
        Exit Sub
    End If
    
    
    'Gravar o c�digo do motivo da recusa, na tabela CLIENTE_WEB
    OraParameters.Remove "CODMOTIVO": OraParameters.Add "CODMOTIVO", vCodMotivoRecusa, 1
    'OraParameters.Remove "CODMOTIVO": OraParameters.Add "CODMOTIVO", Val(Left(cboMotivoRecusa, 2)), 1
    db_INT.ExecuteSQL "BEGIN INTRANET.PCK_CLIENTES_TESTE.PR_GRAVA_MOTIVORECUSA" & _
                             "(:CGC,:TPSOL,:CODMOTIVO,:vErro,:vMsgErro); END;"
    If Val(IIf(IsNull(db.Parameters("vErro")), 0, db.Parameters("vErro"))) <> 0 Then
        MsgBox "Erro " & db.Parameters("vErro") & "-" & db.Parameters("vMsgErro") & ". Avise o Depto.Suporte!", vbCritical, "CAD060 - Aten��o"
        Exit Sub
    End If
    
    'No caso de pr�-cadastro, n�o Deleta registro da Cliente_Web pois ele pode acertar diverg�ncia e recuperar as informa��es j� digitadas
    'A partir de Jan/05 solicitado para que no Recadastro Recusado, tb. n�o haja a perda das informa��es
    
    'Se for altera��o, vai deletar da Cliente_Web
    If (vFl_Tipo_Solicitacao <> 0) And (vFl_Tipo_Solicitacao <> 2) Then
        Set OraParameters = db_INT.Parameters
        OraParameters.Remove "CGC": OraParameters.Add "CGC", Val(vCGC_Analisado), 1
        OraParameters.Remove "TPSOL": OraParameters.Add "TPSOL", Val(vFl_Tipo_Solicitacao), 1
        OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
        OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
        db_INT.ExecuteSQL "BEGIN INTRANET.PCK_CLIENTES_TESTE.PR_DEL_CLIWEB" & _
                                 "(:CGC,:TPSOL,:vErro,:vMsgErro); END;"
        If Val(IIf(IsNull(db.Parameters("vErro")), 0, db.Parameters("vErro"))) <> 0 Then
            MsgBox "Erro " & db.Parameters("vErro") & "-" & db.Parameters("vMsgErro") & ". Avise o Depto.Suporte!", vbCritical, "CAD060 - Aten��o"
            Exit Sub
        End If
    End If

End If

'**************************************************************
'Sempre que finalizar uma an�lise - seja recusada ou aceita,
'enviar e-mail para o usu�rio que enviou a solicita��o da Intranet.Cliente_web
'**************************************************************
GoTo EnviarEmail
MsgBox "Recusa do Cliente gravada com sucesso!", vbInformation, "CAD060 - Cadastro de Cliente"
Unload Me

Exit Sub

'Enviar e-mail ao usu�rio solicitante, retornando a situa��o de sua solicita��o
EnviarEmail:

    Dim Texto As String, vAuxTexto As String
    'Seleciona C�d.Usu�rio da Controle_Cliente_Web e seu endereco de e-mail
    Set OraParameters = db_INT.Parameters
    OraParameters.Remove "CGC": OraParameters.Add "CGC", Val(vCGC_Analisado), 1
    OraParameters.Remove "TP_SOL": OraParameters.Add "TP_SOL", vFl_Tipo_Solicitacao, 1
    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    Criar_Cursor OraParameters, "PM_CURSOR1"
    vErro = vVB_Generica_001.ExecutaPl(db_INT, "Intranet.PCK_CLIENTES_TESTE.Pr_Sel_UserIntranet(:PM_CURSOR1, :CGC, :TP_SOL, :vErro, :vMsgErro)")
    If vErro = "" Then
        Set db_CONS = OraParameters("PM_CURSOR1").Value
    Else
        Exit Sub
    End If

    If vFl_Tipo_Solicitacao = 0 Then
        vAuxTexto = "Pr�-Cadastro"
    ElseIf vFl_Tipo_Solicitacao = 1 Then
        vAuxTexto = "Pr�-Altera��o"
        frmConfirmarAlteracoes.vFechar = True
    ElseIf vFl_Tipo_Solicitacao = 2 Then
        vAuxTexto = "Pr�-Recadastro"
        frmAltRecadastro.vFechar = True
    End If
    
    'Se a solicita��o de recadastro vier pela intranet (c�d.usu�rio=1905), ent�o vai mandar e-mail para o coordenador - 23/08/07
    Dim vEmail As String
    'Guarda o e-mail do usu�rio que vier na solicita��o
    If IsNull(db_CONS!E_Mail) Then
        vEmail = ""
    Else
        vEmail = Trim(db_CONS!E_Mail)
    End If
    
    'Se vier da intranet, pega e-mail do coordenador
    'If (vFl_Tipo_Solicitacao = 1 Or vFl_Tipo_Solicitacao = 2) And _
        Val(db_CONS!Cod_Usuario) = 1743 Then        'C�d.Usu�rio intranet no banco IDPKT
    
    If (vFl_Tipo_Solicitacao = 1 Or vFl_Tipo_Solicitacao = 2) And _
        Val(db_CONS!Cod_Usuario) = 1905 Then         'C�d.Usu�rio intranet no banco IDPK
        
        Dim vObjEmailDpkNet As Object
        Dim vAux As Byte
        
        Criar_Cursor OraParameters, "PM_CURSOR1"
        OraParameters.Remove "CGC": OraParameters.Add "CGC", Val(vCGC_Analisado), 1
        vErro = vVB_Generica_001.ExecutaPl(db_INT, "Intranet.PCK_CLIENTES_TESTE.PR_SELECT_EMAIL_COORD (:PM_CURSOR1, :CGC)")
        If vErro = "" Then
            Set vObjEmailDpkNet = OraParameters("PM_CURSOR1").Value
            If vObjEmailDpkNet.EOF = False Then
                vAux = 0
                While (vObjEmailDpkNet.EOF = False) And (vAux = 0)
                    If Not IsNull(vObjEmailDpkNet!EMail) Then
                        If Trim(vObjEmailDpkNet!EMail) <> "" Then
                            vEmail = Trim(vObjEmailDpkNet!EMail)
                            vAux = 1
                        End If
                    End If
                    vObjEmailDpkNet.MoveNext
                Wend
            End If
        Else
            Exit Sub
        End If
        
    End If
    
    'vEmail = "alethea@maxxipel.com.br"
    
    Texto = "Solicitacao de " & vAuxTexto & " para o CGC " & vCGC_Analisado & " foi recusada." & vbCrLf & vbCrLf & _
            "Data da Solicitacao: " & Format(db_CONS!Dt_Solicitacao, "DD/MM/YY HH:MM:SS") & vbCrLf & vbCrLf & _
            "Data da Analise    : " & Format(db_CONS!Dt_Analise, "DD/MM/YY HH:MM:SS") & vbCrLf & vbCrLf & _
            "Motivo da Recusa   : " & cboMotivoRecusa.Text & vbCrLf & vbCrLf & _
            "Favor nao responder esta mensagem." & vbCrLf & vbCrLf
    
    'vVB_Generica_001.EnviarEmail db_INT, db_CONS!E_Mail, vAuxTexto & " RECUSADO!!! - CGC " & vCGC_Analisado, Texto
    vVB_Generica_001.EnviarEmail db_INT, vEmail, vAuxTexto & " RECUSADO!!! - CGC " & vCGC_Analisado, Texto

    Unload Me

End Sub

Private Sub cmdVoltar_Click()
    If vFl_Tipo_Solicitacao = 1 Then
        frmConfirmarAlteracoes.vFechar = False
    ElseIf vFl_Tipo_Solicitacao = 2 Then
        frmAltRecadastro.vFechar = False
    End If
    
    Unload Me

End Sub

Private Sub Form_Load()
    Call Alinha_Formulario(frmMotivoRecusa)
    
    'Carrega campos com as informa��es b�sicas do cliente
    lblClienteRecusado.Caption = vNome_Analisado
    lblCGCRecusado.Caption = vCGC_Analisado
    
    'If vFl_Tipo_Solicitacao = 0 Then
    
    'Vai selecionar os motivos de recusa cadastrados
    Set OraParameters = db.Parameters
    Criar_Cursor OraParameters, "PM_CURSOR1"
    vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD060.Pr_Select_MotivosRecusa(:PM_CURSOR1)")
    If vErro <> "" Then
        Exit Sub
    End If
    Set db_CONS = OraParameters("PM_CURSOR1").Value
    
    If db_CONS.EOF Then
        MsgBox "Motivos de Recusa n�o cadastrados! Por favor, cadastre-os", vbInformation, "CAD060 - Cadastro de Cliente"
        Exit Sub
    End If
    
    cboMotivoRecusa.Clear
    While Not db_CONS.EOF
        cboMotivoRecusa.AddItem Format(db_CONS!Cod_Motivo.Value, "00") & " - " & db_CONS!Descricao.Value
        db_CONS.MoveNext
    Wend

End Sub


