VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.OCX"
Begin VB.Form frmAnaliseWeb 
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "CAD060 - An�lise de Clientes WEB"
   ClientHeight    =   6720
   ClientLeft      =   75
   ClientTop       =   360
   ClientWidth     =   11805
   FillColor       =   &H00800000&
   ForeColor       =   &H00000000&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6720
   ScaleWidth      =   11805
   Visible         =   0   'False
   Begin VB.CheckBox chkConsRecusados 
      BackColor       =   &H00C0FFFF&
      Caption         =   "Consultar Cadastros e Recadastros Recusados"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   7170
      TabIndex        =   9
      Top             =   90
      Width           =   4470
   End
   Begin VB.ComboBox cboFilial 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0E0FF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   720
      Style           =   2  'Dropdown List
      TabIndex        =   5
      Top             =   60
      Width           =   3975
   End
   Begin Threed.SSPanel SSPanel1 
      Align           =   2  'Align Bottom
      Height          =   675
      Left            =   0
      TabIndex        =   2
      Top             =   6045
      Width           =   11805
      _Version        =   65536
      _ExtentX        =   20823
      _ExtentY        =   1191
      _StockProps     =   15
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BevelOuter      =   0
      Begin Bot�o.cmd cmdVoltar 
         Height          =   855
         Left            =   14040
         TabIndex        =   3
         Top             =   120
         Width           =   855
         _ExtentX        =   1508
         _ExtentY        =   1508
         BTYPE           =   3
         TX              =   "Voltar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmAnaliseWeb.frx":0000
         PICN            =   "frmAnaliseWeb.frx":001C
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   1
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdVoltar2 
         Height          =   585
         Left            =   11145
         TabIndex        =   4
         ToolTipText     =   "Voltar"
         Top             =   45
         Width           =   585
         _ExtentX        =   1032
         _ExtentY        =   1032
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmAnaliseWeb.frx":0796
         PICN            =   "frmAnaliseWeb.frx":07B2
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   1
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MSComctlLib.ImageList imlFiguras2 
         Left            =   180
         Top             =   165
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   32
         ImageHeight     =   32
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   5
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmAnaliseWeb.frx":0F2C
               Key             =   ""
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmAnaliseWeb.frx":1020
               Key             =   ""
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmAnaliseWeb.frx":1115
               Key             =   ""
            EndProperty
            BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmAnaliseWeb.frx":120A
               Key             =   ""
            EndProperty
            BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmAnaliseWeb.frx":1524
               Key             =   ""
            EndProperty
         EndProperty
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00E0E0E0&
      Height          =   5520
      Left            =   45
      TabIndex        =   0
      Top             =   465
      Width           =   11895
      Begin MSFlexGridLib.MSFlexGrid grdflexAnalise 
         Height          =   4995
         Left            =   75
         TabIndex        =   1
         ToolTipText     =   "Duplo Clique sobre a linha, abrir� a solicita��o para an�lise"
         Top             =   180
         Width           =   11730
         _ExtentX        =   20690
         _ExtentY        =   8811
         _Version        =   393216
         Cols            =   6
         BackColorBkg    =   14737632
         GridLines       =   2
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblQtdFichas 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   6285
         TabIndex        =   8
         Top             =   5190
         Width           =   1335
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackColor       =   &H00E0E0E0&
         Caption         =   "Qtd. Solicita��es para esta filial: "
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   3390
         TabIndex        =   7
         Top             =   5205
         Width           =   2640
      End
   End
   Begin Bot�o.cmd cmdAtualizarCombo 
      Height          =   405
      Left            =   5145
      TabIndex        =   10
      ToolTipText     =   "Voltar"
      Top             =   15
      Width           =   1425
      _ExtentX        =   2514
      _ExtentY        =   714
      BTYPE           =   3
      TX              =   "Atualizar"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmAnaliseWeb.frx":183E
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   1
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label31 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      Caption         =   "Filiais"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   105
      TabIndex        =   6
      Top             =   105
      Width           =   465
   End
End
Attribute VB_Name = "frmAnaliseWeb"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private vGuardaLin As Integer, vGuardaCol As Integer    'Vai armazenar a linha e a coluna, de onde foi dado o duplo clique
Private vFl_Encontrou As Boolean

Sub Carrega_Combo_Filial(CampoTela As Object, pTP_CONS As Byte)
    
    Set OraParameters = db_INT.Parameters
    Criar_Cursor OraParameters, "vCursor"
    'pTP_CONS = 0 => ANALISE DE PRE-CADASTRO
    'pTP_CONS = 1 => ANALISE DE ALTERACAO
    'pTP_CONS = 2 => ANALISE DE RECADASTRO
    'pTP_CONS = 3 => CONSULTA DE PRE-CADASTRO E RECADASTRO
    OraParameters.Remove "TP_CONS": OraParameters.Add "TP_CONS", pTP_CONS, 1
    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
      
    'Vai fazer select na tabela filial, para o campo que eu levar como par�metro, para mostrar na combo
    'Em 03/08/06 foi alterada a procedure para trazer a informa��o filtrada - somente as regionais e que possuissem ficha em aberto - Alethea -
    'vErro = vVB_Generica_001.ExecutaPl(db, "Producao.PCK_CAD060.Pr_Select_Filial(:vCursor,:vErro,:vMsgErro)")
    vErro = vVB_Generica_001.ExecutaPl(db_INT, "INTRANET.PCK_Clientes_Teste.Pr_Select_Filial_2(:vCursor,:TP_CONS,:vErro,:vMsgErro)")
    If vErro <> "" Then
        Exit Sub
    End If
    
    Set db_CONS = OraParameters("vCursor").Value
    If Val(OraParameters("vErro").Value) <> 0 Then
        MsgBox "Erro: " & Val(OraParameters("vErro").Value) & " - " & _
                          OraParameters("vMsgErro").Value & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
        Exit Sub
    End If

    If db_CONS.EOF Then
        'MsgBox "Problemas ao consultar os dados para preencher combo de Filiais. " & Chr(13) & _
               "Por Favor, avise Depto.Suporte ", vbCritical, "CAD060 - Cadastro de Cliente"
        MsgBox "N�o h� fichas para serem analisadas!", vbInformation, "CAD060 - Cadastro de Cliente"
        Exit Sub
    End If
    
    CampoTela.Clear
    While Not db_CONS.EOF
        'Se vier espa�o em branco, n�o considerar como uma das op��es a serem exibidas no combo
        If Trim(db_CONS!Cod_Filial) <> "" Then
            CampoTela.AddItem Format(db_CONS!Cod_Filial.Value, "0000") & " - " & db_CONS!nome_filial.Value
        End If
        db_CONS.MoveNext
    Wend
    
End Sub

Sub Monta_Grid_Analise()

    With grdflexAnalise
    
        .Rows = 1
        .Cols = 10
        
        'Quando n�o for consulta, n�o vou mostrar a coluna de motivo de recusa
        If vFl_Consulta = False Then
            .ColWidth(0) = 1500: .TextMatrix(0, 0) = "CGC"
            .ColWidth(9) = 0: .TextMatrix(0, 9) = ""
        Else
            .ColWidth(0) = 1500: .ColAlignment(0) = 0: .TextMatrix(0, 0) = "C�d./CGC"
            .ColWidth(9) = 4000: .ColAlignment(9) = 0: .TextMatrix(0, 9) = "Motivo Recusa"
        End If
        .ColWidth(1) = 2500: .ColAlignment(1) = 0: .TextMatrix(0, 1) = "Nome Cliente"
       '.ColWidth(2) = 3150: .ColAlignment(2) = 0: .TextMatrix(0, 2) = "Representante"
        .ColWidth(2) = 1700: .ColAlignment(2) = 0: .TextMatrix(0, 2) = "Remetente"
        .ColWidth(3) = 1700: .ColAlignment(3) = 0: .TextMatrix(0, 3) = "Dt/Hr.Solic."
        .ColWidth(4) = 1000: .ColAlignment(4) = 0: .TextMatrix(0, 4) = "Situa��o"
        .ColWidth(5) = 1700: .ColAlignment(5) = 0: .TextMatrix(0, 5) = "Dt/Hr.An�lise"
        .ColWidth(6) = 1500: .ColAlignment(6) = 0: .TextMatrix(0, 6) = "Resp.Cr�dito"
        .ColWidth(7) = 300:  .ColAlignment(7) = 0: .TextMatrix(0, 7) = ""
        .ColWidth(8) = 800: .ColAlignment(8) = 0: .TextMatrix(0, 8) = "Tp.Solic."
        
        'Esta coluna ir� servir
       '.ColWidth(8) = 1: .ColAlignment(8) = 0: .TextMatrix(0, 8) = "CGC"
    
    
    End With

End Sub

Private Sub cboFilial_Click()
     Dim vMsg As String, I As Integer, objRepr As Object
    Dim j As Long, vQtdFichas As Long

    'Prepara barra de t�tulo do form
    If vFl_Tipo_Solicitacao = 0 And vFl_Consulta = False Then
        
        frmAnaliseWeb.Caption = "CAD060 -- An�lise WEB -- PR�-CADASTRO"
        vMsg = "Pr�-Cadastros"
        
        'Verificar se existe algum caso de cliente analisado, j� aceito e que ainda continua
        'com situa��o = 1 (Em Analise). Se houver, alterar para situa��o=2 (Aceito)
        Set OraParameters = db_INT.Parameters
        OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
        OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
        db_INT.Executesql "BEGIN INTRANET.PCK_CLIENTES_TESTE.PR_CLIE_ACEITO_ENROSC(:vErro,:vMsgErro); END;"
        If Val(IIf(IsNull(OraParameters("vErro")), 0, OraParameters("vErro"))) <> 0 Then
           MsgBox "Erro: " & Val(OraParameters("vErro").Value) & " - " & _
                   OraParameters("vMsgErro").Value & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
            Exit Sub
        End If
        
    ElseIf vFl_Tipo_Solicitacao = 1 Then
        frmAnaliseWeb.Caption = "CAD060 -- An�lise WEB -- PR�-ALTERACAO"
        vMsg = "Pr�-Altera��es"
    ElseIf vFl_Tipo_Solicitacao = 2 Then
        frmAnaliseWeb.Caption = "CAD060 -- An�lise WEB -- PR�-RECADASTRO"
        vMsg = "Pr�-Recadastros"
    'Se for consulta, mas n�o for consulta de recusados
    ElseIf vFl_Consulta = True And chkConsRecusados.Value = 0 Then
        frmAnaliseWeb.Caption = "CAD060 -- An�lise WEB -- CONSULTA DE PR�-CADASTROS E RECADASTROS"
        vMsg = "Consulta de Pr�-Cadastros e Recadastros"
    'Se for consulta de recusados
    ElseIf vFl_Consulta = True And chkConsRecusados.Value = 1 Then
        frmAnaliseWeb.Caption = "CAD060 -- An�lise WEB -- CONSULTA DE PR�-CADASTROS E RECADASTROS RECUSADOS"
        vMsg = "Consulta de Pr�-Cadastros e Recadastros Recusados"
    End If

    'Fazer 2 selects:   Para pegar as solicita��es pendentes e em an�lise
                       'Para pegar as solicita��es (pr�-cadastro e recadastro) aceitos
    
    'Vou trazer informa��es do futuro Cliente, que ainda est� pendente para an�lise
    Set OraParameters = db_INT.Parameters
    Criar_Cursor OraParameters, "vCursor"
    If vFl_Consulta = False Then
        OraParameters.Remove "TP_CONS": OraParameters.Add "TP_CONS", 0, 1
        OraParameters.Remove "TP_SOL": OraParameters.Add "TP_SOL", vFl_Tipo_Solicitacao, 1
    'Se for uma consulta de solicita��es (pr�-cadastro e recadastro aceitas)
    ElseIf vFl_Consulta = True And chkConsRecusados.Value = 0 Then
        OraParameters.Remove "TP_CONS": OraParameters.Add "TP_CONS", 1, 1
        OraParameters.Remove "TP_SOL": OraParameters.Add "TP_SOL", 0, 1 'Aqui o tipo de solic.n�o faz diferen�a pois n�o ser� usado
    'Se for uma consulta de solicita��es (pr�-cadastro e recadastro recusadas)
    ElseIf vFl_Consulta = True And chkConsRecusados.Value = 1 Then
        OraParameters.Remove "TP_CONS": OraParameters.Add "TP_CONS", 2, 1
        OraParameters.Remove "TP_SOL": OraParameters.Add "TP_SOL", 0, 1 'Aqui o tipo de solic.n�o faz diferen�a pois n�o ser� usado
    End If
    
    OraParameters.Remove "COD_FIL": OraParameters.Add "COD_FIL", Val(cboFilial), 1
    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
      
    vErro = vVB_Generica_001.ExecutaPl(db_INT, "INTRANET.PCK_CLIENTES_TESTE.Pr_Form_Pend_Web(:vCursor,:TP_CONS,:TP_SOL,:COD_FIL,:vErro,:vMsgErro)")
      
    If vErro = "" Then
        Set db_CONS = OraParameters("vCursor").Value
    Else
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    'Deixa somente a linha de cabe�alho do grid
    grdflexAnalise.Rows = 1
    
    If db_CONS.EOF Then
        MsgBox "N�o h� " & vMsg & " para serem analisados", vbInformation, "CAD060"
        lblQtdFichas = ""
        vFl_Encontrou = False
        Screen.MousePointer = vbNormal
        Exit Sub
    Else
        vFl_Encontrou = True
    End If
    
    Monta_Grid_Analise

    vQtdFichas = db_CONS.RecordCount

    'Monta o grid com as informa��es
    With grdflexAnalise
    .Rows = 1
    
        For j = 1 To db_CONS.RecordCount
        
            'Na tela de an�lise somente vai mostrar, se for solicita��o n�o analisada ou em an�lise
            If vFl_Consulta = False And (db_CONS!Sit = 0 Or db_CONS!Sit = 1) Then
        
                .Rows = .Rows + 1
                I = .Rows - 1
                
                .TextMatrix(I, 0) = db_CONS!CGC.Value
                .TextMatrix(I, 1) = db_CONS!Nome.Value
                .TextMatrix(I, 2) = db_CONS!USER_REMET.Value
                .TextMatrix(I, 3) = Format(db_CONS!DTSOL.Value, "DD/MM/YY HH:MM:SS")
                .TextMatrix(I, 5) = IIf(IsNull(db_CONS!DTANA.Value), "", (Format(db_CONS!DTANA.Value, "DD/MM/YY HH:MM:SS")))
                .TextMatrix(I, 6) = IIf(IsNull(db_CONS!USER_CRED.Value), "", db_CONS!USER_CRED.Value)
                
                .Row = I
                If db_CONS!Sit = 0 Then
                    .TextMatrix(I, 4) = "N�o Analisado"
                    .Col = 7
                    Set .CellPicture = imlFiguras2.ListImages(1).Picture
                ElseIf db_CONS!Sit = 1 Then
                    .TextMatrix(I, 4) = "Em An�lise"
                    .Col = 7
                    Set .CellPicture = imlFiguras2.ListImages(3).Picture
                End If
                
                .TextMatrix(I, 8) = IIf(vFl_Tipo_Solicitacao = 0, "CAD", IIf(vFl_Tipo_Solicitacao = 1, "ALT", "REC"))
                
                'Para an�lise n�o traz solicita��es recusadas
                .ColWidth(9) = 0: .TextMatrix(0, 9) = ""
                .TextMatrix(I, 9) = ""
    
            'Na tela de consulta de pr�-cadastro/recadastro vai mostrar dados da solicita��o que poder� estar aprovada ou recusada (conf.checkbox)
            'ElseIf vFl_Consulta = True And db_CONS!Sit = 2 Then
            ElseIf vFl_Consulta = True Then
            
                .Rows = .Rows + 1
                I = .Rows - 1
                
                If db_CONS!Sit = 2 Then
                    
                    .TextMatrix(I, 0) = Format(db_CONS!COD.Value, "000000") & " - " & db_CONS!CGC.Value
                    
                    .Row = I
                    .TextMatrix(I, 4) = "Aprovado"
                    .Col = 7
                    Set .CellPicture = imlFiguras2.ListImages(2).Picture
                
                    'Para consulta de solic.aceitas, nem mostra coluna de motivo de recusa
                    .ColWidth(9) = 0: .TextMatrix(0, 9) = ""
                    .TextMatrix(I, 9) = ""
                
                ElseIf db_CONS!Sit = 9 Then
                    
                    .TextMatrix(I, 0) = db_CONS!CGC.Value
                
                    .Row = I
                    .TextMatrix(I, 4) = "Recusado"
                    .Col = 7
                    Set .CellPicture = imlFiguras2.ListImages(1).Picture
                
                    .TextMatrix(I, 9) = db_CONS!Desc_Recusa
                
                End If
                
                .TextMatrix(I, 1) = db_CONS!Nome.Value
                .TextMatrix(I, 2) = db_CONS!USER_REMET.Value
                .TextMatrix(I, 3) = Format(db_CONS!DTSOL.Value, "DD/MM/YY HH:MM:SS")
                .TextMatrix(I, 5) = IIf(IsNull(db_CONS!DTANA.Value), "", (Format(db_CONS!DTANA.Value, "DD/MM/YY HH:MM:SS")))
                .TextMatrix(I, 6) = IIf(IsNull(db_CONS!USER_CRED.Value), "", db_CONS!USER_CRED.Value)
                
                .Row = I
                
                .TextMatrix(I, 8) = IIf(Val(db_CONS!TPSOL.Value) = 0, "0 - CAD", IIf(Val(db_CONS!TPSOL.Value) = 1, "1- ALT", "2 - REC"))
            
            End If
    
            If I < db_CONS.RecordCount Then
                db_CONS.MoveNext
            End If
        Next j
    End With

    lblQtdFichas = vQtdFichas

End Sub


Private Sub chkConsRecusados_Click()
    If Trim(cboFilial.Text) <> "" Then
        Call cboFilial_Click
    End If
End Sub


Private Sub cmdAtualizarCombo_Click()
    Call Carrega_Combo_Filial(Me.cboFilial, vTP_CONSULTA)
    Call cboFilial_Click
End Sub


Private Sub cmdVoltar_Click()
    Unload Me
End Sub


Private Sub cmdVoltar2_Click()
    vFl_Analise = False
    Unload Me
End Sub


Private Sub Form_Activate()
    If vFl_Encontrou = False Then
        Unload Me
    Else
        vFl_Encontrou = True
    End If
End Sub

Private Sub Form_Load_Ant()
    Dim vMsg As String, I As Integer, objRepr As Object
    Dim j As Long

    'vFl_Volta_Analise = False
    'vFl_Analise = True
    
    Screen.MousePointer = vbHourglass
    
    Me.Top = mdiCAD060.Menu.Height * 2
    Me.Left = 50
    
    'Ocultar a barra de botoes do MDI
    mdiCAD060.SSPanel1.Visible = False

    'Vou trazer informa��es do futuro Cliente, que ainda est� pendente para an�lise
    Set OraParameters = db_INT.Parameters
    Criar_Cursor OraParameters, "vCursor"
    OraParameters.Remove "TP_CONS": OraParameters.Add "TP_CONS", 0, 1
    OraParameters.Remove "TP_SOL": OraParameters.Add "TP_SOL", vFl_Tipo_Solicitacao, 1
    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
      
    'ALTERAR
    'Fazer 2 selects:   Para pegar as solicita��es pendentes e em an�lise
                       'Para pegar as solicita��es (pr�-cadastro e recadastro) aceitos
      
    'Vai fazer select distinct na tabela cliente, para o campo que eu levar como par�metro, para mostrar na combo
    vErro = vVB_Generica_001.ExecutaPl(db_INT, "INTRANET.PCK_CLIENTES_TESTE.Pr_Form_Pend_Web(:vCursor,:TP_CONS,:TP_SOL,:vErro,:vMsgErro)")
      
    If vErro = "" Then
        Set db_CONS = OraParameters("vCursor").Value
    Else
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    If vFl_Tipo_Solicitacao = 0 And vFl_Consulta = False Then
        frmAnaliseWeb.Caption = "CAD060 -- An�lise WEB -- PR�-CADASTRO"
        vMsg = "Pr�-Cadastros"
    ElseIf vFl_Tipo_Solicitacao = 1 Then
        frmAnaliseWeb.Caption = "CAD060 -- An�lise WEB -- PR�-ALTERACAO"
        vMsg = "Pr�-Altera��es"
    ElseIf vFl_Tipo_Solicitacao = 2 Then
        frmAnaliseWeb.Caption = "CAD060 -- An�lise WEB -- PR�-RECADASTRO"
        vMsg = "Pr�-Recadastros"
    ElseIf vFl_Tipo_Solicitacao = 0 And vFl_Consulta = True Then
        frmAnaliseWeb.Caption = "CAD060 -- An�lise WEB -- CONSULTA PR�-CADASTRO ACEITO"
        vMsg = "Consulta de Pr�-Cadastros"
    End If
    
    If db_CONS.EOF Then
        MsgBox "N�o h� " & vMsg & " para serem analisados", vbInformation, "CAD060"
        'Call Limpa_FlexGrid(grdflexAnalise)
        vFl_Encontrou = False
        Screen.MousePointer = vbNormal
        Exit Sub
    Else
        vFl_Encontrou = True
    End If
    
    'Call Limpa_FlexGrid(grdflexAnalise)
    Monta_Grid_Analise

    'Se n�o for consulta, Se existir somente um registro e for uma solicita��o aceita, n�o vai mostrar
    If db_CONS.RecordCount = 1 And db_CONS!Sit <> 0 And db_CONS!Sit <> 1 And vFl_Consulta = False Then
        MsgBox "N�o h� " & vMsg & " para serem analisados", vbInformation, "CAD060"
        vFl_Encontrou = False
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    
    With grdflexAnalise
    .Rows = 1
    
        For j = 1 To db_CONS.RecordCount
        
            'Na tela de an�lise somente vai mostrar, se for solicita��o n�o analisada ou em an�lise
            If vFl_Consulta = False And (db_CONS!Sit = 0 Or db_CONS!Sit = 1) Then
        
                .Rows = .Rows + 1
                I = .Rows - 1
                
                .TextMatrix(I, 0) = db_CONS!CGC.Value
                .TextMatrix(I, 1) = db_CONS!Nome.Value
                .TextMatrix(I, 2) = db_CONS!USER_REMET.Value
                .TextMatrix(I, 3) = db_CONS!DTSOL.Value
                .TextMatrix(I, 5) = IIf(IsNull(db_CONS!DTANA.Value), "", (db_CONS!DTANA.Value))
                
                .Row = I
                If db_CONS!Sit = 0 Then
                    .TextMatrix(I, 4) = "N�o Analisado"
                    .Col = 6
                    Set .CellPicture = imlFiguras2.ListImages(1).Picture
                ElseIf db_CONS!Sit = 1 Then
                    .TextMatrix(I, 4) = "Em An�lise"
                    .Col = 6
                    Set .CellPicture = imlFiguras2.ListImages(3).Picture
                End If
    
            'Na tela de consulta de pr�-cadastro somente vai mostrar se for solicita��o aprovada
            ElseIf vFl_Consulta = True And db_CONS!Sit = 2 Then
            
                .Rows = .Rows + 1
                I = .Rows - 1
                
                .TextMatrix(I, 0) = db_CONS!CGC.Value
                .TextMatrix(I, 1) = db_CONS!Nome.Value
                .TextMatrix(I, 2) = db_CONS!USER_REMET.Value
                .TextMatrix(I, 3) = db_CONS!DTSOL.Value
                .TextMatrix(I, 5) = IIf(IsNull(db_CONS!DTANA.Value), "", (db_CONS!DTANA.Value))
                
                .Row = I
                If db_CONS!Sit = 2 Then
                        .TextMatrix(I, 4) = "Aprovado"
                        .Col = 6
                        Set .CellPicture = imlFiguras2.ListImages(2).Picture
                End If
            End If
    
            If I < db_CONS.RecordCount Then
                db_CONS.MoveNext
            End If
        Next j
    End With

    
    'For i = 1 To db_CONS.recordcount
    '    With grdflexAnalise
    '        'Na tela de an�lise somente vai mostrar, se for solicita��o n�o analisada ou em an�lise
    '        If vFl_Consulta = False And (db_CONS!Sit = 0 Or db_CONS!Sit = 1) Then
    '            .TextMatrix(i, 0) = db_CONS!CGC.Value
    '            .TextMatrix(i, 1) = db_CONS!NOME.Value
    '            '.TextMatrix(i, 2) = db_CONS!REPR.Value
    '            .TextMatrix(i, 2) = db_CONS!USER_REMET.Value
    '            'Selecionar o nome do representante
    '            'Set OraParameters = db.Parameters
    '            'Criar_Cursor OraParameters, "vCursor"
    '            'OraParameters.Remove "CODCLI": OraParameters.Add "CODCLI", 0, 1
    '            'OraParameters.Remove "CODREPR": OraParameters.Add "CODREPR", Val(db_CONS!REPR.Value), 1
    '            'OraParameters.Remove "TP_CONS": OraParameters.Add "TP_CONS", 3, 1
    '            'OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    '            'OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    '            'vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD060.Pr_Verifica_Repres(:vCursor, :CODCLI, :CODREPR, :TP_CONS, :vErro, :vMsgErro)")
    '            'If vErro = "" Then
    '            '    Set objRepr = OraParameters("vCursor").Value
    '            'Else
    '            '    MsgBox "C�d.Representante: " & Format(db_CONS!REPR, "0000") & " n�o encontrado!", vbCritical, "CAD060 - Aten��o"
    '            '    Screen.MousePointer = vbNormal
    '            '    Exit Sub
    '            'End If
    '            .TextMatrix(i, 3) = db_CONS!DTSOL.Value
    '            .TextMatrix(i, 5) = IIf(IsNull(db_CONS!DTANA.Value), "", (db_CONS!DTANA.Value))
    '            .Row = i
    '            If db_CONS!Sit = 0 Then
    '                .TextMatrix(i, 4) = "N�o Analisado"
    '                .Col = 6
    '                'Set .CellPicture = LoadPicture("F:\USR\ALETHEA\ICONES\gifs\sinal_vermelho.gif")
    '                Set .CellPicture = imlFiguras2.ListImages(1).Picture
    '            ElseIf db_CONS!Sit = 1 Then
    '                .TextMatrix(i, 4) = "Em An�lise"
    '                .Col = 6
    '                Set .CellPicture = imlFiguras2.ListImages(3).Picture
    '            'ElseIf db_CONS!SIT = 2 Then
    '            '        .TextMatrix(i, 4) = "Aprovado"
    '            '        .Col = 6
    '            '        Set .CellPicture = imlFiguras2.ListImages(2).Picture
    '            'ElseIf db_CONS!SIT = 9 Then
    '            '    .TextMatrix(I, 4) = "Recusado"
    '            '    .Col = 6
    '            '    Set .CellPicture = LoadPicture("F:\USR\ALETHEA\ICONES\cancelado.ICO")
    '            End If
    '            .Rows = .Rows + 1
    '        'Na tela de consulta de pr�-cadastro somente vai mostrar se for solicita��o aprovada
    '        ElseIf vFl_Consulta = True And db_CONS!Sit = 2 Then
    '            .TextMatrix(i, 0) = db_CONS!CGC.Value
    '            .TextMatrix(i, 1) = db_CONS!NOME.Value
    '            .TextMatrix(i, 2) = db_CONS!USER_REMET.Value
    '            .TextMatrix(i, 3) = db_CONS!DTSOL.Value
    '            .TextMatrix(i, 5) = IIf(IsNull(db_CONS!DTANA.Value), "", (db_CONS!DTANA.Value))
    '            .Row = i
    '            If db_CONS!Sit = 2 Then
    '                    .TextMatrix(i, 4) = "Aprovado"
    '                    .Col = 6
    '                   Set .CellPicture = imlFiguras2.ListImages(2).Picture
    '            End If
    '            .Rows = .Rows + 1
    '        Else
    '            .Rows = .Rows - 1
    '        End If
    '    End With
    '    If i < db_CONS.recordcount Then
    '        db_CONS.movenext
    '    End If
    'Next i
    
    Screen.MousePointer = vbNormal
    
End Sub


Private Sub Form_Load()

    Screen.MousePointer = vbHourglass
    Me.Top = mdiCAD060.Menu.Height * 2
    Me.Left = 50
    'Ocultar a barra de botoes do MDI
    mdiCAD060.SSPanel1.Visible = False

    Monta_Grid_Analise
    lblQtdFichas = ""
    
    'Seleciona as filiais, para preencher o combo - ALTERADO CONCEITO EM 03/08/06
    If vFl_Consulta = False And vFl_Tipo_Solicitacao = 0 Then       'ANALISE DE PRE-CADASTROS
        vTP_CONSULTA = 0
    ElseIf vFl_Consulta = False And vFl_Tipo_Solicitacao = 1 Then   'ANALISE DE ALTERACOES
        vTP_CONSULTA = 1
    ElseIf vFl_Consulta = False And vFl_Tipo_Solicitacao = 2 Then   'ANALISE DE RECADASTROS
        vTP_CONSULTA = 2
    ElseIf vFl_Consulta = True And vFl_Tipo_Solicitacao = 0 Then    'CONSULTA DE PRE-CADASTROS E RECADASTROS
        vTP_CONSULTA = 3
    End If
    Call Carrega_Combo_Filial(Me.cboFilial, vTP_CONSULTA)
    
    vFl_Encontrou = True
    
    'Se for consulta, deixar vis�vel o check para possibilitar consulta de recusados
    If vFl_Consulta = True Then
        chkConsRecusados.Visible = True
    Else
        chkConsRecusados.Visible = False
    End If
    
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    'Exibir a barra de botoes do MDI
    mdiCAD060.SSPanel1.Visible = True
End Sub

Private Sub grdflexAnalise_DblClick()

    'Se for consulta de recusados, n�o habilita duplo clique, pois o Cr�dito n�o tem informa��es a verificar, j� que recusou
    If vFl_Consulta = True And chkConsRecusados.Value = 1 Then
        Exit Sub
    End If

    'Guarda linha e coluna de onde foi dado o duplo clique
    vGuardaLin = grdflexAnalise.Row
    vGuardaCol = grdflexAnalise.Col
    
    vFl_Volta_Analise = False
    vFl_Analise = True
    
    If vFl_Consulta = True Then
        vCGC_Analisado = Mid(grdflexAnalise.TextMatrix(vGuardaLin, 0), 10)
    Else
        vCGC_Analisado = grdflexAnalise.TextMatrix(vGuardaLin, 0)
    End If
    vNome_Analisado = grdflexAnalise.TextMatrix(vGuardaLin, 1)
    vUserRem_Analisado = grdflexAnalise.TextMatrix(vGuardaLin, 2)
    'Se tiver fazendo consulta, ainda n�o sei se � cadastro ou recadastro, ent�o vou pegar o tipo da solicita��o que est� no grid
    If vFl_Consulta = True Then
        vFl_Tipo_Solicitacao = Val(Mid(grdflexAnalise.TextMatrix(vGuardaLin, 8), 1, 1))
    End If
    
    'S� vai alterar a situa��o da solicita��o, se n�o for uma consulta
    If vFl_Consulta = False Then
    
        'Pega a data e hora da altera��o
        Sql = "SELECT TO_CHAR(sysdate,'dd/mm/yy hh24:mi:ss') FROM dual "
        Set db_Hora = db.dbcreatedynaset(Sql, 0&)
        vDtHoraHoje = db_Hora.Fields(0).Value
    
        '**********
        'Alterar situa��o do formul�rio na controle.cliente_web para situacao=1 (Em analise)
        Set OraParameters = db_INT.Parameters
        OraParameters.Remove "CGC": OraParameters.Add "CGC", Val(vCGC_Analisado), 1
        OraParameters.Remove "TPSOL": OraParameters.Add "TPSOL", Val(vFl_Tipo_Solicitacao), 1
        OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
        OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
        'Alterar Dt.Analise = Dt.Hoje, Situacao = 1 (Em an�lise), Cod_Usuario_Credito = vUser_Intranet
        'OraParameters.Remove "DTANAL": OraParameters.Add "DTANAL", vDtHoje, 1
        OraParameters.Remove "DTANAL": OraParameters.Add "DTANAL", vDtHoraHoje, 1
        OraParameters.Remove "SITUACAO": OraParameters.Add "SITUACAO", 1, 1
        OraParameters.Remove "CODUSER": OraParameters.Add "CODUSER", Val(vCodUser_Intranet), 1
        
        db_INT.Executesql "BEGIN INTRANET.PCK_CLIENTES_TESTE.PR_ATU_PRECAD_CTRCLIWEB" & _
                                 "(:CGC,:TPSOL,TO_DATE(:DTANAL,'DD/MM/RR HH24:MI:SS'),:SITUACAO," & _
                                 " :CODUSER,:vErro,:vMsgErro); END;"
        
        If Val(IIf(IsNull(OraParameters("vErro")), 0, OraParameters("vErro"))) <> 0 Then
           MsgBox "Erro: " & Val(OraParameters("vErro").Value) & " - " & _
                           OraParameters("vMsgErro").Value & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
            Exit Sub
        End If
        '**********
    
    End If
    
    If vFl_Tipo_Solicitacao = 0 And vFl_Consulta = False Then
        frmCadCliente.Show vbModal
    ElseIf vFl_Consulta = True And (vFl_Tipo_Solicitacao = 0 Or vFl_Tipo_Solicitacao = 2) Then
        frmCadCliente.Show vbModal
    ElseIf vFl_Tipo_Solicitacao = 1 Then    'Altera��o
        frmConfirmarAlteracoes.Show vbModal
    ElseIf vFl_Tipo_Solicitacao = 2 Then    'Recadastro
        frmAltRecadastro.Show vbModal
    End If

    'Se estiver voltando da tela de cadastro (an�lise de web), vai fazer refresh das informa��es
    If vFl_Volta_Analise = True Then
        Call Form_Load
    End If


End Sub


