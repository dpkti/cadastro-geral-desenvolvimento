VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Form1 
   Caption         =   "frmVendor"
   ClientHeight    =   6285
   ClientLeft      =   60
   ClientTop       =   1500
   ClientWidth     =   11835
   LinkTopic       =   "Form1"
   ScaleHeight     =   6285
   ScaleWidth      =   11835
   Begin VB.Frame Frame2 
      BackColor       =   &H00FFFFFF&
      Height          =   705
      Left            =   135
      TabIndex        =   0
      Top             =   60
      Width           =   10725
      Begin VB.ComboBox cboSitVendor 
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   4875
         TabIndex        =   4
         Text            =   "02 - PESSOA JURIDICA"
         Top             =   315
         Width           =   2055
      End
      Begin VB.TextBox txtDtContratoVendor 
         Alignment       =   2  'Center
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2070
         MaxLength       =   10
         TabIndex        =   3
         Top             =   315
         Width           =   1215
      End
      Begin VB.TextBox txtContratoVendor 
         BackColor       =   &H000040C0&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Left            =   60
         MaxLength       =   20
         TabIndex        =   2
         Text            =   "00000000000000000000"
         Top             =   315
         Width           =   1965
      End
      Begin VB.TextBox txtVlLimiteVendor 
         Alignment       =   2  'Center
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3330
         MaxLength       =   15
         TabIndex        =   1
         Text            =   "000000000000.00"
         Top             =   315
         Width           =   1515
      End
      Begin VB.Label Label74 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Dt Contrato"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   2100
         TabIndex        =   8
         Top             =   120
         Width           =   930
      End
      Begin VB.Label Label73 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "N� Contrato"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   120
         TabIndex        =   7
         Top             =   120
         Width           =   930
      End
      Begin VB.Label Label72 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Vl.Limite"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   3345
         TabIndex        =   6
         Top             =   120
         Width           =   735
      End
      Begin VB.Label Label71 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Situa��o"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   4935
         TabIndex        =   5
         Top             =   120
         Width           =   690
      End
   End
   Begin MSComctlLib.ListView lsvVendor 
      Height          =   4815
      Left            =   105
      TabIndex        =   9
      Top             =   765
      Width           =   10740
      _ExtentX        =   18944
      _ExtentY        =   8493
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   4
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Contrato"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Dt Contrato"
         Object.Width           =   1939
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Vl Limite Vendor"
         Object.Width           =   2646
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Situa��o"
         Object.Width           =   1411
      EndProperty
   End
   Begin Bot�o.cmd cmdAdVendor 
      Height          =   705
      Left            =   10935
      TabIndex        =   10
      Top             =   2700
      Width           =   765
      _ExtentX        =   1349
      _ExtentY        =   1244
      BTYPE           =   3
      TX              =   "Adicionar"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "FrmVendor.frx":0000
      PICN            =   "FrmVendor.frx":001C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdExVendor 
      Height          =   705
      Left            =   10935
      TabIndex        =   11
      Top             =   4140
      Width           =   765
      _ExtentX        =   1349
      _ExtentY        =   1244
      BTYPE           =   3
      TX              =   "Excluir"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "FrmVendor.frx":0176
      PICN            =   "FrmVendor.frx":0192
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdGrVendor 
      Height          =   705
      Left            =   10935
      TabIndex        =   12
      Top             =   3420
      Width           =   765
      _ExtentX        =   1349
      _ExtentY        =   1244
      BTYPE           =   3
      TX              =   "Gravar"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "FrmVendor.frx":02EC
      PICN            =   "FrmVendor.frx":0308
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdLiVendor 
      Height          =   705
      Left            =   10935
      TabIndex        =   13
      Top             =   4860
      Width           =   765
      _ExtentX        =   1349
      _ExtentY        =   1244
      BTYPE           =   3
      TX              =   "Limpar"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "FrmVendor.frx":0462
      PICN            =   "FrmVendor.frx":047E
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Shape shLinha6 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   1  'Opaque
      Height          =   5610
      Left            =   45
      Top             =   45
      Width           =   11730
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Sub Limpar_Campos_Vendor()
    txtContratoVendor.Text = ""
    txtDtContratoVendor.Text = ""
    txtVlLimiteVendor.Text = ""
    cboSitVendor.Text = ""
    lsvVendor.ListItems.Clear
End Sub

Private Sub cboSitVendor_Change()

End Sub

Private Sub cboSitVendor_Click()
End Sub


Private Sub cboSitVendor_GotFocus()
    cboSitVendor.Clear
    cboSitVendor.AddItem "0 - Ativo"
    cboSitVendor.AddItem "9 - Desativado"
End Sub


Private Sub cmdAdVendor_Click()
    Limpar_Campos_Vendor
    
    If Trim(txtCodCliente.Text) = "" Then
        MsgBox "Por favor, escolha um cliente ou inclua-o, antes de gravar dados de Vendor", vbCritical, "CAD060 - Aten��o"
        Exit Sub
    End If
    
    txtContratoVendor.Text = lsvVendor.ListItems(lsvVendor.ListItems.Count) + 1
    txtDtContratoVendor.SetFocus

End Sub


Private Sub cmdGrVendor_Click()
    Dim litem As ListItem
    
    If Trim(txtCodCliente.Text) = "" Then
        MsgBox "Por favor, escolha um cliente ou inclua-o, antes de gravar dados de Vendor", vbCritical, "CAD060 - Aten��o"
        Exit Sub
    End If
    
    Set litem = lsvVendor.ListItems.Add
    litem = txtContratoVendor.Text
    litem.SubItems(1) = txtDtContratoVendor.Text
    litem.SubItems(2) = txtVlLimiteVendor.Text
    litem.SubItems(3) = cboSitVendor.Text

    'Gravar no banco

End Sub


Private Sub cmdLiVendor_Click()
    txtContratoVendor.Text = ""
    txtDtContratoVendor.Text = ""
    txtVlLimiteVendor.Text = ""
    cboSitVendor.Text = ""

End Sub


Private Sub Form_Load()

End Sub


Private Sub lsvVendor_BeforeLabelEdit(Cancel As Integer)

End Sub

Private Sub lsvVendor_ItemClick(ByVal Item As MSComctlLib.ListItem)
    txtContratoVendor.Text = lsvVendor.SelectedItem
    txtDtContratoVendor.Text = lsvVendor.SelectedItem.SubItems(1)
    txtVlLimiteVendor.Text = lsvVendor.SelectedItem.SubItems(2)
    cboSitVendor.Text = lsvVendor.SelectedItem.SubItems(3)

End Sub


Private Sub txtDtContratoVendor_Change()

End Sub


Private Sub txtDtContratoVendor_KeyPress(KeyAscii As Integer)
    ValidaCampo.Data KeyAscii, txtDtContratoVendor

End Sub


Private Sub txtVlLimiteVendor_Change()

End Sub


Private Sub txtVlLimiteVendor_KeyPress(KeyAscii As Integer)
    KeyAscii = ValidaCampo.Valor(KeyAscii, txtVlLimiteVendor)

End Sub


