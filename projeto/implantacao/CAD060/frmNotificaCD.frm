VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Begin VB.Form frmNotificaCD 
   Caption         =   "..: Notifica��o de CD's Atualizados :.."
   ClientHeight    =   6735
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   5475
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6735
   ScaleWidth      =   5475
   StartUpPosition =   1  'CenterOwner
   Begin Bot�o.cmd cmdOK 
      Default         =   -1  'True
      Height          =   495
      Left            =   2280
      TabIndex        =   87
      Top             =   6150
      Width           =   945
      _ExtentX        =   1667
      _ExtentY        =   873
      BTYPE           =   3
      TX              =   "OK"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   14215660
      BCOLO           =   14215660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmNotificaCD.frx":0000
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Frame Frame1 
      Caption         =   "CDs Atualizados:"
      Enabled         =   0   'False
      ForeColor       =   &H8000000D&
      Height          =   5655
      Left            =   300
      TabIndex        =   0
      Top             =   150
      Width           =   4785
      Begin VB.CheckBox chk20 
         Caption         =   "20 - Presid. Prud."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   88
         Top             =   5070
         Width           =   1815
      End
      Begin VB.CheckBox chk09 
         Caption         =   "09 - Brasilia"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   17
         Top             =   1770
         Width           =   1515
      End
      Begin VB.CheckBox chk01 
         Caption         =   "01 - Campinas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   16
         Top             =   270
         Width           =   1545
      End
      Begin VB.CheckBox chk04 
         Caption         =   "04 - Goiania"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   15
         Top             =   570
         Width           =   1515
      End
      Begin VB.CheckBox chk05 
         Caption         =   "05 - Fortaleza"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   14
         Top             =   870
         Width           =   1515
      End
      Begin VB.CheckBox chk07 
         Caption         =   "07 - B.H."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   13
         Top             =   1170
         Width           =   1515
      End
      Begin VB.CheckBox chk08 
         Caption         =   "08 - P.A."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   12
         Top             =   1470
         Width           =   1515
      End
      Begin VB.CheckBox chk15 
         Caption         =   "15 - Ribeir�o P."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   11
         Top             =   3570
         Width           =   1665
      End
      Begin VB.CheckBox chk10 
         Caption         =   "10 - R.J."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   10
         Top             =   2070
         Width           =   1515
      End
      Begin VB.CheckBox chk11 
         Caption         =   "11 - Salvador"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   9
         Top             =   2370
         Width           =   1515
      End
      Begin VB.CheckBox chk12 
         Caption         =   "12 - Recife"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   8
         Top             =   2670
         Width           =   1515
      End
      Begin VB.CheckBox chk13 
         Caption         =   "13 - Mato Grosso"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   7
         Top             =   2970
         Width           =   1785
      End
      Begin VB.CheckBox chk14 
         Caption         =   "14 - Curitiba"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   6
         Top             =   3270
         Width           =   1515
      End
      Begin VB.CheckBox chk16 
         Caption         =   "16 - S.P."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   5
         Top             =   3870
         Width           =   1515
      End
      Begin VB.CheckBox chk17 
         Caption         =   "17 - Cuiaba"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   4
         Top             =   4170
         Width           =   1515
      End
      Begin VB.CheckBox chk18 
         Caption         =   "18 - Florian�polis"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   3
         Top             =   4470
         Width           =   1785
      End
      Begin VB.CheckBox chk19 
         Caption         =   "19 - Belem"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   2
         Top             =   4770
         Width           =   1515
      End
      Begin VB.CheckBox chk21 
         Caption         =   "21 - Vitoria"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   60
         TabIndex        =   1
         Top             =   5370
         Width           =   1515
      End
      Begin VB.Label lblFinalCD20 
         AutoSize        =   -1  'True
         Caption         =   "XX:XX:XX"
         Height          =   195
         Left            =   3840
         TabIndex        =   92
         Top             =   5070
         Width           =   720
      End
      Begin VB.Label lblInicioCD20 
         AutoSize        =   -1  'True
         Caption         =   "XX:XX:XX"
         Height          =   195
         Left            =   2430
         TabIndex        =   91
         Top             =   5070
         Width           =   720
      End
      Begin VB.Label Label36 
         AutoSize        =   -1  'True
         Caption         =   "Final:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   3330
         TabIndex        =   90
         Top             =   5370
         Width           =   480
      End
      Begin VB.Label Label31 
         AutoSize        =   -1  'True
         Caption         =   "Inicio:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   165
         Left            =   1860
         TabIndex        =   89
         Top             =   5370
         Width           =   540
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Inicio:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1860
         TabIndex        =   85
         Top             =   270
         Width           =   540
      End
      Begin VB.Label lblInicioCD01 
         AutoSize        =   -1  'True
         Caption         =   "XX:XX:XX"
         Height          =   195
         Left            =   2430
         TabIndex        =   84
         Top             =   270
         Width           =   720
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Final:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   3330
         TabIndex        =   83
         Top             =   270
         Width           =   480
      End
      Begin VB.Label lblFinalCD01 
         AutoSize        =   -1  'True
         Caption         =   "XX:XX:XX"
         Height          =   195
         Left            =   3840
         TabIndex        =   82
         Top             =   270
         Width           =   720
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         Caption         =   "Inicio:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1860
         TabIndex        =   81
         Top             =   1470
         Width           =   540
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "Inicio:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1860
         TabIndex        =   80
         Top             =   1170
         Width           =   540
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         Caption         =   "Inicio:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1860
         TabIndex        =   79
         Top             =   870
         Width           =   540
      End
      Begin VB.Label Label9 
         AutoSize        =   -1  'True
         Caption         =   "Inicio:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1860
         TabIndex        =   78
         Top             =   570
         Width           =   540
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         Caption         =   "Inicio:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1860
         TabIndex        =   77
         Top             =   1770
         Width           =   540
      End
      Begin VB.Label lblInicioCD09 
         AutoSize        =   -1  'True
         Caption         =   "XX:XX:XX"
         Height          =   195
         Left            =   2430
         TabIndex        =   76
         Top             =   1770
         Width           =   720
      End
      Begin VB.Label lblInicioCD08 
         AutoSize        =   -1  'True
         Caption         =   "XX:XX:XX"
         Height          =   195
         Left            =   2430
         TabIndex        =   75
         Top             =   1470
         Width           =   720
      End
      Begin VB.Label lblInicioCD07 
         AutoSize        =   -1  'True
         Caption         =   "XX:XX:XX"
         Height          =   195
         Left            =   2430
         TabIndex        =   74
         Top             =   1170
         Width           =   720
      End
      Begin VB.Label lblInicioCD05 
         AutoSize        =   -1  'True
         Caption         =   "XX:XX:XX"
         Height          =   195
         Left            =   2430
         TabIndex        =   73
         Top             =   870
         Width           =   720
      End
      Begin VB.Label lblInicioCD04 
         AutoSize        =   -1  'True
         Caption         =   "XX:XX:XX"
         Height          =   195
         Left            =   2430
         TabIndex        =   72
         Top             =   570
         Width           =   720
      End
      Begin VB.Label Label11 
         AutoSize        =   -1  'True
         Caption         =   "Final:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   3330
         TabIndex        =   71
         Top             =   870
         Width           =   480
      End
      Begin VB.Label Label12 
         AutoSize        =   -1  'True
         Caption         =   "Final:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   3330
         TabIndex        =   70
         Top             =   1170
         Width           =   480
      End
      Begin VB.Label Label13 
         AutoSize        =   -1  'True
         Caption         =   "Final:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   3330
         TabIndex        =   69
         Top             =   1470
         Width           =   480
      End
      Begin VB.Label Label14 
         AutoSize        =   -1  'True
         Caption         =   "Final:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   3330
         TabIndex        =   68
         Top             =   1770
         Width           =   480
      End
      Begin VB.Label Label15 
         AutoSize        =   -1  'True
         Caption         =   "Final:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   3330
         TabIndex        =   67
         Top             =   570
         Width           =   480
      End
      Begin VB.Label lblFinalCD07 
         AutoSize        =   -1  'True
         Caption         =   "XX:XX:XX"
         Height          =   195
         Left            =   3840
         TabIndex        =   66
         Top             =   1170
         Width           =   720
      End
      Begin VB.Label lblFinalCD08 
         AutoSize        =   -1  'True
         Caption         =   "XX:XX:XX"
         Height          =   195
         Left            =   3840
         TabIndex        =   65
         Top             =   1470
         Width           =   720
      End
      Begin VB.Label lblFinalCD09 
         AutoSize        =   -1  'True
         Caption         =   "XX:XX:XX"
         Height          =   195
         Left            =   3840
         TabIndex        =   64
         Top             =   1770
         Width           =   720
      End
      Begin VB.Label lblFinalCD05 
         AutoSize        =   -1  'True
         Caption         =   "XX:XX:XX"
         Height          =   195
         Left            =   3840
         TabIndex        =   63
         Top             =   870
         Width           =   720
      End
      Begin VB.Label lblFinalCD04 
         AutoSize        =   -1  'True
         Caption         =   "XX:XX:XX"
         Height          =   195
         Left            =   3840
         TabIndex        =   62
         Top             =   570
         Width           =   720
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Inicio:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1860
         TabIndex        =   61
         Top             =   3570
         Width           =   540
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Inicio:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1860
         TabIndex        =   60
         Top             =   2370
         Width           =   540
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Inicio:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1860
         TabIndex        =   59
         Top             =   2670
         Width           =   540
      End
      Begin VB.Label Label16 
         AutoSize        =   -1  'True
         Caption         =   "Inicio:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1860
         TabIndex        =   58
         Top             =   2970
         Width           =   540
      End
      Begin VB.Label Label17 
         AutoSize        =   -1  'True
         Caption         =   "Inicio:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1860
         TabIndex        =   57
         Top             =   3270
         Width           =   540
      End
      Begin VB.Label Label18 
         AutoSize        =   -1  'True
         Caption         =   "Inicio:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1860
         TabIndex        =   56
         Top             =   2070
         Width           =   540
      End
      Begin VB.Label Label20 
         AutoSize        =   -1  'True
         Caption         =   "Inicio:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1860
         TabIndex        =   55
         Top             =   4170
         Width           =   540
      End
      Begin VB.Label Label21 
         AutoSize        =   -1  'True
         Caption         =   "Inicio:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1860
         TabIndex        =   54
         Top             =   4470
         Width           =   540
      End
      Begin VB.Label Label22 
         AutoSize        =   -1  'True
         Caption         =   "Inicio:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1860
         TabIndex        =   53
         Top             =   4770
         Width           =   540
      End
      Begin VB.Label Label23 
         AutoSize        =   -1  'True
         Caption         =   "Inicio:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   165
         Left            =   1860
         TabIndex        =   52
         Top             =   5070
         Width           =   540
      End
      Begin VB.Label Label24 
         AutoSize        =   -1  'True
         Caption         =   "Inicio:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1860
         TabIndex        =   51
         Top             =   3870
         Width           =   540
      End
      Begin VB.Label lblInicioCD11 
         AutoSize        =   -1  'True
         Caption         =   "XX:XX:XX"
         Height          =   195
         Left            =   2430
         TabIndex        =   50
         Top             =   2370
         Width           =   720
      End
      Begin VB.Label lblInicioCD12 
         AutoSize        =   -1  'True
         Caption         =   "XX:XX:XX"
         Height          =   195
         Left            =   2430
         TabIndex        =   49
         Top             =   2670
         Width           =   720
      End
      Begin VB.Label lblInicioCD13 
         AutoSize        =   -1  'True
         Caption         =   "XX:XX:XX"
         Height          =   195
         Left            =   2430
         TabIndex        =   48
         Top             =   2970
         Width           =   720
      End
      Begin VB.Label lblInicioCD14 
         AutoSize        =   -1  'True
         Caption         =   "XX:XX:XX"
         Height          =   195
         Left            =   2430
         TabIndex        =   47
         Top             =   3270
         Width           =   720
      End
      Begin VB.Label lblInicioCD15 
         AutoSize        =   -1  'True
         Caption         =   "XX:XX:XX"
         Height          =   195
         Left            =   2430
         TabIndex        =   46
         Top             =   3570
         Width           =   720
      End
      Begin VB.Label lblInicioCD10 
         AutoSize        =   -1  'True
         Caption         =   "XX:XX:XX"
         Height          =   195
         Left            =   2430
         TabIndex        =   45
         Top             =   2070
         Width           =   720
      End
      Begin VB.Label lblInicioCD17 
         AutoSize        =   -1  'True
         Caption         =   "XX:XX:XX"
         Height          =   195
         Left            =   2430
         TabIndex        =   44
         Top             =   4170
         Width           =   720
      End
      Begin VB.Label lblInicioCD18 
         AutoSize        =   -1  'True
         Caption         =   "XX:XX:XX"
         Height          =   195
         Left            =   2430
         TabIndex        =   43
         Top             =   4470
         Width           =   720
      End
      Begin VB.Label lblInicioCD19 
         AutoSize        =   -1  'True
         Caption         =   "XX:XX:XX"
         Height          =   195
         Left            =   2430
         TabIndex        =   42
         Top             =   4770
         Width           =   720
      End
      Begin VB.Label lblInicioCD21 
         AutoSize        =   -1  'True
         Caption         =   "XX:XX:XX"
         Height          =   195
         Left            =   2430
         TabIndex        =   41
         Top             =   5370
         Width           =   720
      End
      Begin VB.Label lblInicioCD16 
         AutoSize        =   -1  'True
         Caption         =   "XX:XX:XX"
         Height          =   195
         Left            =   2430
         TabIndex        =   40
         Top             =   3870
         Width           =   720
      End
      Begin VB.Label Label19 
         AutoSize        =   -1  'True
         Caption         =   "Final:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   3330
         TabIndex        =   39
         Top             =   2370
         Width           =   480
      End
      Begin VB.Label Label25 
         AutoSize        =   -1  'True
         Caption         =   "Final:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   3330
         TabIndex        =   38
         Top             =   3570
         Width           =   480
      End
      Begin VB.Label Label26 
         AutoSize        =   -1  'True
         Caption         =   "Final:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   3330
         TabIndex        =   37
         Top             =   3270
         Width           =   480
      End
      Begin VB.Label Label27 
         AutoSize        =   -1  'True
         Caption         =   "Final:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   3330
         TabIndex        =   36
         Top             =   2970
         Width           =   480
      End
      Begin VB.Label Label28 
         AutoSize        =   -1  'True
         Caption         =   "Final:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   3330
         TabIndex        =   35
         Top             =   2670
         Width           =   480
      End
      Begin VB.Label Label29 
         AutoSize        =   -1  'True
         Caption         =   "Final:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   3330
         TabIndex        =   34
         Top             =   2070
         Width           =   480
      End
      Begin VB.Label Label30 
         AutoSize        =   -1  'True
         Caption         =   "Final:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   3330
         TabIndex        =   33
         Top             =   4170
         Width           =   480
      End
      Begin VB.Label Label32 
         AutoSize        =   -1  'True
         Caption         =   "Final:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   3330
         TabIndex        =   32
         Top             =   5070
         Width           =   480
      End
      Begin VB.Label Label33 
         AutoSize        =   -1  'True
         Caption         =   "Final:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   3330
         TabIndex        =   31
         Top             =   4770
         Width           =   480
      End
      Begin VB.Label Label34 
         AutoSize        =   -1  'True
         Caption         =   "Final:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   3330
         TabIndex        =   30
         Top             =   4470
         Width           =   480
      End
      Begin VB.Label Label35 
         AutoSize        =   -1  'True
         Caption         =   "Final:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   3330
         TabIndex        =   29
         Top             =   3870
         Width           =   480
      End
      Begin VB.Label lblFinalCD11 
         AutoSize        =   -1  'True
         Caption         =   "XX:XX:XX"
         Height          =   195
         Left            =   3840
         TabIndex        =   28
         Top             =   2370
         Width           =   720
      End
      Begin VB.Label lblFinalCD12 
         AutoSize        =   -1  'True
         Caption         =   "XX:XX:XX"
         Height          =   195
         Left            =   3840
         TabIndex        =   27
         Top             =   2670
         Width           =   720
      End
      Begin VB.Label lblFinalCD15 
         AutoSize        =   -1  'True
         Caption         =   "XX:XX:XX"
         Height          =   195
         Left            =   3840
         TabIndex        =   26
         Top             =   3570
         Width           =   720
      End
      Begin VB.Label lblFinalCD14 
         AutoSize        =   -1  'True
         Caption         =   "XX:XX:XX"
         Height          =   195
         Left            =   3840
         TabIndex        =   25
         Top             =   3270
         Width           =   720
      End
      Begin VB.Label lblFinalCD13 
         AutoSize        =   -1  'True
         Caption         =   "XX:XX:XX"
         Height          =   195
         Left            =   3840
         TabIndex        =   24
         Top             =   2970
         Width           =   720
      End
      Begin VB.Label lblFinalCD10 
         AutoSize        =   -1  'True
         Caption         =   "XX:XX:XX"
         Height          =   195
         Left            =   3840
         TabIndex        =   23
         Top             =   2070
         Width           =   720
      End
      Begin VB.Label lblFinalCD17 
         AutoSize        =   -1  'True
         Caption         =   "XX:XX:XX"
         Height          =   195
         Left            =   3840
         TabIndex        =   22
         Top             =   4170
         Width           =   720
      End
      Begin VB.Label lblFinalCD18 
         AutoSize        =   -1  'True
         Caption         =   "XX:XX:XX"
         Height          =   195
         Left            =   3840
         TabIndex        =   21
         Top             =   4470
         Width           =   720
      End
      Begin VB.Label lblFinalCD21 
         AutoSize        =   -1  'True
         Caption         =   "XX:XX:XX"
         Height          =   195
         Left            =   3840
         TabIndex        =   20
         Top             =   5370
         Width           =   720
      End
      Begin VB.Label lblFinalCD19 
         AutoSize        =   -1  'True
         Caption         =   "XX:XX:XX"
         Height          =   195
         Left            =   3840
         TabIndex        =   19
         Top             =   4770
         Width           =   720
      End
      Begin VB.Label lblFinalCD16 
         AutoSize        =   -1  'True
         Caption         =   "XX:XX:XX"
         Height          =   195
         Left            =   3840
         TabIndex        =   18
         Top             =   3870
         Width           =   720
      End
   End
   Begin VB.Label lblMensagem 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      Caption         =   "Registros Gravados com Sucesso."
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   0
      TabIndex        =   86
      Top             =   5880
      Width           =   5490
   End
End
Attribute VB_Name = "frmNotificaCD"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdOK_Click()
    Unload Me
End Sub
