VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmCadEmail 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Cadastro de E-mail"
   ClientHeight    =   5265
   ClientLeft      =   3660
   ClientTop       =   3330
   ClientWidth     =   8805
   ClipControls    =   0   'False
   Icon            =   "frmCadEmail.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5265
   ScaleWidth      =   8805
   ShowInTaskbar   =   0   'False
   Begin MSFlexGridLib.MSFlexGrid msfEmail 
      Height          =   1935
      Left            =   30
      TabIndex        =   24
      TabStop         =   0   'False
      Top             =   3270
      Width           =   8745
      _ExtentX        =   15425
      _ExtentY        =   3413
      _Version        =   393216
      Cols            =   5
      FixedCols       =   0
      WordWrap        =   -1  'True
      HighLight       =   2
      SelectionMode   =   1
      Appearance      =   0
   End
   Begin VB.TextBox txtDtAtual 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   7470
      Locked          =   -1  'True
      MaxLength       =   8
      TabIndex        =   22
      TabStop         =   0   'False
      Top             =   1050
      Width           =   1260
   End
   Begin VB.Frame Frame1 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   1335
      Left            =   30
      TabIndex        =   5
      Top             =   1440
      Width           =   8715
      Begin VB.TextBox txtEmail 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   990
         MaxLength       =   100
         TabIndex        =   15
         Top             =   900
         Width           =   7680
      End
      Begin VB.TextBox txtDtCadastro 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   7410
         Locked          =   -1  'True
         MaxLength       =   8
         TabIndex        =   14
         TabStop         =   0   'False
         Top             =   540
         Width           =   1260
      End
      Begin VB.TextBox txtContato 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   990
         MaxLength       =   30
         TabIndex        =   13
         Top             =   540
         Width           =   5220
      End
      Begin VB.TextBox txtSeq 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   990
         Locked          =   -1  'True
         MaxLength       =   100
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   180
         Width           =   540
      End
      Begin VB.ComboBox cboDepto 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   4380
         Style           =   2  'Dropdown List
         TabIndex        =   11
         Top             =   180
         Width           =   4275
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "E-mail:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   420
         TabIndex        =   20
         Top             =   990
         Width           =   525
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Dt.Cadastro:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   6360
         TabIndex        =   19
         Top             =   630
         Width           =   1020
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Contato:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   240
         TabIndex        =   18
         Top             =   630
         Width           =   720
      End
      Begin VB.Label Label4 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Sequ�ncia:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   60
         TabIndex        =   17
         Top             =   270
         Width           =   900
      End
      Begin VB.Label Label5 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Departamento:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   3120
         TabIndex        =   16
         Top             =   270
         Width           =   1245
      End
   End
   Begin VB.TextBox txtCodCliente 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H8000000F&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1080
      Locked          =   -1  'True
      MaxLength       =   100
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   1050
      Width           =   960
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      Top             =   960
      Width           =   8715
      _ExtentX        =   15372
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   855
      Left            =   7920
      TabIndex        =   0
      TabStop         =   0   'False
      ToolTipText     =   "Sair do sistema"
      Top             =   60
      Width           =   855
      _ExtentX        =   1508
      _ExtentY        =   1508
      BTYPE           =   3
      TX              =   "Sair"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadEmail.frx":0CCA
      PICN            =   "frmCadEmail.frx":0CE6
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdAdd 
      Height          =   405
      Left            =   3000
      TabIndex        =   6
      ToolTipText     =   "Salvar E-mail"
      Top             =   2820
      Width           =   1035
      _ExtentX        =   1826
      _ExtentY        =   714
      BTYPE           =   3
      TX              =   "Salvar"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadEmail.frx":19C0
      PICN            =   "frmCadEmail.frx":19DC
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdExcluir 
      Height          =   405
      Left            =   7650
      TabIndex        =   10
      ToolTipText     =   "Excluir E-mail selecionado"
      Top             =   2820
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   714
      BTYPE           =   3
      TX              =   "Excluir"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadEmail.frx":1F76
      PICN            =   "frmCadEmail.frx":1F92
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdNovo 
      Height          =   405
      Left            =   4050
      TabIndex        =   7
      ToolTipText     =   "Adicionar E-mail"
      Top             =   2820
      Width           =   1035
      _ExtentX        =   1826
      _ExtentY        =   714
      BTYPE           =   3
      TX              =   "Novo"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadEmail.frx":252C
      PICN            =   "frmCadEmail.frx":2548
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdLimpar 
      Height          =   405
      Left            =   6450
      TabIndex        =   9
      ToolTipText     =   "Limpar"
      Top             =   2820
      Width           =   1185
      _ExtentX        =   2090
      _ExtentY        =   714
      BTYPE           =   3
      TX              =   "Limpar"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadEmail.frx":2A04
      PICN            =   "frmCadEmail.frx":2A20
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdSemEmail 
      Height          =   405
      Left            =   5100
      TabIndex        =   8
      ToolTipText     =   "Cliente n�o possui email"
      Top             =   2820
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   714
      BTYPE           =   3
      TX              =   "Sem Email"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadEmail.frx":2D3A
      PICN            =   "frmCadEmail.frx":2D56
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label6 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Dt.Atual:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   6720
      TabIndex        =   23
      Top             =   1140
      Width           =   735
   End
   Begin VB.Label lblNomeCliente 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   330
      Left            =   2100
      TabIndex        =   21
      Top             =   1050
      Width           =   4515
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "E-mails Cadastrados:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   60
      TabIndex        =   4
      Top             =   3030
      Width           =   1635
   End
   Begin VB.Label lblCodCliente 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "C�d.Cliente:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   60
      TabIndex        =   3
      Top             =   1140
      Width           =   990
   End
End
Attribute VB_Name = "frmCadEmail"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : frmCadEmail
' Author    : c.samuel.oliveira
' Date      : 07/05/2018
' Purpose   : TI-6488
'---------------------------------------------------------------------------------------

Option Explicit

Public vCgc As String
Dim vLoad As Boolean
Dim vRst As Object
Dim vHoje As Date

'#1198 - Atualizar email no SAP
Dim blnAtualizarEmailSAP As String

'Emerson - 22/12/17 - TI-6287 - Cadastro Email de cliente - Sistema de Televendas DPK
Dim vstrEmail_Selecionado As String

Private Sub cmdAdd_Click()

    'Muniz 10/02/10 valida espa�os em branco no campo contato
    If Not ValidaBranco(txtContato) Then
        MsgBox "Existe mais de um espa�o em branco no campo Contato.", vbCritical, "CAD060 - Aten��o"
        txtContato.SetFocus
        Exit Sub
    End If
    'fim Muniz
    
    If cboDepto.Text = "" Then
        MsgBox "O Campo Departamento n�o pode estar em branco.", vbInformation, "Aten��o"
        cboDepto.SetFocus
        Exit Sub
    End If
    
    If txtCodCliente = "" Then
        MsgBox "O Campo C�digo do Cliente n�o pode estar em branco." & vbCrLf & "Entre em contato com o Helpdesk.", vbInformation, "Aten��o"
        Exit Sub
    End If
    
        '#1198 - Prever a exclus�o de todos os e-mails do cliente
    'If Trim(txtEmail) = "" Then
        If (Trim(txtEmail) = "") And (msfEmail.Rows > 1) Then
        MsgBox "Informe o E-mail.", vbInformation, "Aten��o"
        txtEmail.SetFocus
        Exit Sub
    End If
        
    'Validar Email
        '#1198 - Prever a exclus�o de todos os e-mails do cliente
        'If isEmail(txtEmail) = False Then
    If (isEmail(txtEmail) = False) And (msfEmail.Rows > 1) Then
       MsgBox "E-mail inv�lido.", vbInformation, "Aten��o"
       txtEmail.SetFocus
       Exit Sub
    End If

    'TI-6488
    For I = 1 To msfEmail.Rows - 1
        If txtSeq <> msfEmail.TextMatrix(I, 0) And (cboDepto.ItemData(cboDepto.ListIndex) = "19" And cboDepto = msfEmail.TextMatrix(I, 3)) Then
            MsgBox "J� existe E-Mail Comercial cadastrado!", vbInformation, "Aten��o"
            Exit Sub
        End If
    Next
    'FIM TI-6488
    
'Verificar se existe um igual, se existir sair da rotina
    For I = 1 To msfEmail.Rows - 1
        If txtEmail = msfEmail.TextMatrix(I, 1) And txtContato = msfEmail.TextMatrix(I, 2) And cboDepto = msfEmail.TextMatrix(I, 3) And txtDtCadastro = msfEmail.TextMatrix(I, 4) Then
            MsgBox "Este email j� existe.", vbInformation, "Aten��o"
            Exit Sub
        End If
    Next
    
    'Verificar se existe uma sequencia igual, se existir dar update
    If UCase(Frame1.Caption) = "ALTERAR" Then
    
        '#1198 - Atualizar os emails no SAP
        If blnAtualizarEmailSAP <> 0 Then
            If msfEmail.Rows > 1 Then
                For I = 1 To msfEmail.Rows
                       If txtSeq = msfEmail.TextMatrix(I, 0) Then
                          Exit For
                       End If
                   Next
    
                   msfEmail.TextMatrix(I, 0) = txtSeq
                   msfEmail.TextMatrix(I, 1) = txtEmail
                   msfEmail.TextMatrix(I, 2) = txtContato
                   msfEmail.TextMatrix(I, 3) = cboDepto
                   msfEmail.TextMatrix(I, 4) = txtDtCadastro
                End If

        Else
    
            If MsgBox("Confirma a altera��o deste e-mail ?", vbYesNo, "Aten��o") = vbYes Then
               For I = 1 To msfEmail.Rows
                   If txtSeq = msfEmail.TextMatrix(I, 0) Then
                      Exit For
                   End If
               Next
               
               msfEmail.TextMatrix(I, 0) = txtSeq
               msfEmail.TextMatrix(I, 1) = txtEmail
               msfEmail.TextMatrix(I, 2) = txtContato
               msfEmail.TextMatrix(I, 3) = cboDepto
               msfEmail.TextMatrix(I, 4) = txtDtCadastro
            Else
                Exit Sub
            End If

        End If

    ElseIf UCase(Frame1.Caption) = "NOVO" Then
       msfEmail.Rows = msfEmail.Rows + 1
       msfEmail.TextMatrix(I, 0) = txtSeq
       msfEmail.TextMatrix(I, 1) = txtEmail
       msfEmail.TextMatrix(I, 2) = txtContato
       msfEmail.TextMatrix(I, 3) = cboDepto
       msfEmail.TextMatrix(I, 4) = txtDtCadastro
    End If
    
    
      'carlosj - Ci&T - 28/08/12 - [ISA-330] - Salvar dados (Comunicacao) do cliente no SAP
'      Dim cli As ClienteSAP TI-6488
'      Dim msgErro As String
'
'      Set cli = BuscarClienteSAP(Val(frmCadCliente.cboTpDocto), vCgc, msgErro)
'
'      If msgErro <> "" Then
'            MsgBox msgErro
'            Exit Sub
'      ElseIf Val(cli.NoClienteSAP) = 0 Then
'            MsgBox "Cliente n�o encontrado no SAP, opera��o ser� abortada.", vbCritical, "CAD060 - Aten��o"
'            Exit Sub
'      End If
'
'      Call PopularObjetoClienteComunicacao(cli)
'
'      If EnviarClienteSAP(cli) Then TI-6488
        
        'Inserir no banco
        db.Parameters.Remove "Cod_Cliente"
        db.Parameters.Remove "Sequencia"
        db.Parameters.Remove "Email"
        db.Parameters.Remove "CodDepto"
        db.Parameters.Remove "Contato"
        
        db.Parameters.Add "Cod_Cliente", txtCodCliente, 1
        db.Parameters.Add "Sequencia", txtSeq, 1
        db.Parameters.Add "Email", txtEmail, 1
        db.Parameters.Add "CodDepto", cboDepto.ItemData(cboDepto.ListIndex), 1
        db.Parameters.Add "Contato", txtContato, 1
        
        
        If UCase(Frame1.Caption) = "NOVO" Then
           db.Executesql "BEGIN " & strTabela_Banco & "PCK_CAD_EMAIL.PR_INCLUIR_EMAIL(:Cod_Cliente,:Sequencia, :Email, :CodDepto, :Contato); END;"
           
            'Emerson - 22/12/17 - TI-6287 - Cadastro Email de cliente - Sistema de Televendas DPK
            If Trim(vstrEmail_Selecionado) <> "" Then
               db.Parameters.Remove "Cod_Repres"
               db.Parameters.Remove "Email_DE"
               db.Parameters.Remove "Email_PARA"
             
               db.Parameters.Add "Cod_Repres", Val(vCodUser_Intranet), 1
               db.Parameters.Add "Email_DE", vstrEmail_Selecionado, 1
               db.Parameters.Add "Email_PARA", txtEmail, 1
             
               db.Executesql "BEGIN " & strTabela_Banco & "PCK_CAD_EMAIL.PR_LOG_CLIENTE_EMAIL(:Cod_Repres, :Cod_Cliente, :Email_DE, :Email_PARA); END;"
            End If
            'Emerson - 22/12/17 - TI-6287 - Cadastro Email de cliente - Sistema de Televendas DPK
           
        ElseIf UCase(Frame1.Caption) = "ALTERAR" Then
            '#1198 - Verificar se todos os emails foram removidos
            If msfEmail.Rows > 1 Then
                db.Executesql "BEGIN " & strTabela_Banco & "PCK_CAD_EMAIL.PR_ALTERAR_EMAIL(:Cod_Cliente,:Sequencia, :Email, :CodDepto, :Contato); END;"
                
                'Emerson - 22/12/17 - TI-6287 - Cadastro Email de cliente - Sistema de Televendas DPK
                If Trim(vstrEmail_Selecionado) <> Trim(txtEmail) Then
                     db.Parameters.Remove "Cod_Repres"
                     db.Parameters.Remove "Email_DE"
                     db.Parameters.Remove "Email_PARA"
                
                     db.Parameters.Add "Cod_Repres", Val(vCodUser_Intranet), 1
                     db.Parameters.Add "Email_DE", vstrEmail_Selecionado, 1
                     db.Parameters.Add "Email_PARA", txtEmail, 1
                
                     db.Executesql "BEGIN " & strTabela_Banco & "PCK_CAD_EMAIL.PR_LOG_CLIENTE_EMAIL(:Cod_Repres, :Cod_Cliente, :Email_DE, :Email_PARA); END;"
                 End If
                'Emerson - 22/12/17 - TI-6287 - Cadastro Email de cliente - Sistema de Televendas DPK
                
                        End If
        End If
        
'      Else TI-6488
'          MsgBox "Erro ao gravar o cliente no SAP. Cadastro Abortado.", vbCritical, "CAD060 - Aten��o"
'      End If
    
    
'    If UCase(Frame1.Caption) = "NOVO" Then
'       db.ExecuteSQL "BEGIN " & strTabela_Banco & "PCK_CAD_EMAIL.PR_INCLUIR_EMAIL(:Cod_Cliente,:Sequencia, :Email, :CodDepto, :Contato); END;"
'    ElseIf UCase(Frame1.Caption) = "ALTERAR" Then
'       db.ExecuteSQL "BEGIN " & strTabela_Banco & "PCK_CAD_EMAIL.PR_ALTERAR_EMAIL(:Cod_Cliente,:Sequencia, :Email, :CodDepto, :Contato); END;"
'    End If
    
    txtContato = ""
    txtEmail = ""
    txtSeq = ""
    cboDepto.ListIndex = -1
    
        blnAtualizarEmailSAP = 0
     
    'Emerson - 22/12/17 - TI-6187 - Cadastro Email de cliente - Sistema de Televendas DPK.
    vstrEmail_Selecionado = ""
     
    Frame1.Enabled = False
    
    Preencher_Email
    
    Botoes True, False, False, False, False, True
    
    
End Sub


Private Sub cmdAlterar_Click()
    Frame1.Enabled = True
    Frame1.Caption = "Alterar"
    txtDtCadastro = txtDtAtual
    Botoes True, True, True, True, True, True
End Sub

Private Sub cmdExcluir_Click()
    If Val(txtSeq) = 0 Then
       MsgBox "Selecione um item da lista.", vbInformation, "Aten��o"
       Exit Sub
    End If
    
    If MsgBox("Confirma a exclus�o do e-mail selecionado ?", vbYesNo, "Aten��o") = vbYes Then
        db.Parameters.Remove "COD_CLIENTE"
        db.Parameters.Remove "Sequencia"
        db.Parameters.Add "Cod_Cliente", txtCodCliente, 1
        db.Parameters.Add "Sequencia", txtSeq, 1
        
        db.Executesql "BEGIN " & strTabela_Banco & "PCK_CAD_EMAIL.PR_EXCLUIR_EMAIL(:Cod_Cliente,:Sequencia); END;"
        
        db.Executesql "BEGIN " & strTabela_Banco & "PCK_CAD_EMAIL.PR_RESEQUENCIAR(:Cod_Cliente); END;"
        
        Preencher_Email
        
        'Lu�s Mota: 03/09/2014
        'Descri��o: #1198 - Atualizar o SAP
        blnAtualizarEmailSAP = 1
        msfEmail_Click
        cmdAdd_Click
        

    End If
    
    txtSeq = ""
    txtContato = ""
    txtEmail = ""
    cboDepto.ListIndex = -1
        
    'Emerson - 22/12/17 - TI-6187 - Cadastro Email de cliente - Sistema de Televendas DPK.
    vstrEmail_Selecionado = ""
        
    Frame1.Enabled = False
    
    Botoes True, False, False, False, False, True
    
    cboDepto = "COMERCIAL"
    
End Sub

Private Sub cmdLimpar_Click()
    txtContato = ""
    txtEmail = ""
    cboDepto.ListIndex = -1
    Me.Frame1.Caption = ""
    
    'Emerson - 22/12/17 - TI-6287 - Cadastro Email de cliente - Sistema de Televendas DPK
    vstrEmail_Selecionado = ""
    
    Frame1.Enabled = False
    
    Botoes True, False, True, False, False, False
End Sub

Private Sub cmdNovo_Click()
    
    On Error GoTo Trata_Erro
    
    Frame1.Enabled = True
    
    If msfEmail.Rows = 1 Then
        txtSeq = 1
    Else
        txtSeq = msfEmail.TextMatrix(msfEmail.Rows - 1, 0) + 1
    End If
    
    'Emerson - 22/12/17 - TI-6287 - Cadastro Email de cliente - Sistema de Televendas DPK
    vstrEmail_Selecionado = "NOVO"
    
    txtContato = ""
    txtEmail = ""
    cboDepto.ListIndex = -1
    
    txtDtCadastro = txtDtAtual
    
    cboDepto = "COMERCIAL"
    
    txtContato.SetFocus
    
    Frame1.Caption = "Novo"
    
    Botoes False, False, True, True, False, True
    
Trata_Erro:
    If Err.Number = 5 Then
        Resume Next
    ElseIf Err.Number <> 0 Then
        MsgBox "Sub CmdNovo_CLick" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description, vbCritical, "Aten��o"
    End If
    
End Sub

Private Sub cmdSair_Click()
    Unload Me
    'frmVenda.Verificar_Email
End Sub

Private Sub cmdSemEmail_Click()

    'Non'StayOnTop frmCadEmail

    If MsgBox("Confirma que este cliente n�o possui email ?", vbYesNo + vbQuestion, "Aten��o") = vbNo Then Exit Sub
    
    If msfEmail.Rows > 1 Then
        MsgBox "Este cliente j� possui email cadastrado." & vbCrLf & "Caso queria informar que o cliente n�o possui email" & vbCrLf & "exclua os emails da lista.", vbInformation, "Aten��o"
        ''StayOnTop frmCadEmail
        Exit Sub
    End If
    
    If cboDepto.Text = "" Then
        MsgBox "O Campo Departamento n�o pode estar em branco.", vbInformation, "Aten��o"
        ''StayOnTop frmCadEmail
        Exit Sub
    End If
    
    If txtCodCliente = "" Then
        MsgBox "O Campo C�digo do Cliente n�o pode estar em branco." & vbCrLf & "Entre em contato com o Helpdesk.", vbInformation, "Aten��o"
        ''StayOnTop frmCadEmail
        Exit Sub
    End If
    
    Frame1.Caption = "Novo"
    
    If msfEmail.Rows = 1 Then
        txtSeq = 1
    Else
        txtSeq = msfEmail.TextMatrix(msfEmail.Rows - 1, 0) + 1
    End If
    
    txtEmail = ""
    
'Verificar se existe um igual, se existir sair da rotina
    For I = 1 To msfEmail.Rows - 1
        If txtEmail = msfEmail.TextMatrix(I, 1) And txtContato = msfEmail.TextMatrix(I, 2) And cboDepto = msfEmail.TextMatrix(I, 3) And txtDtCadastro = msfEmail.TextMatrix(I, 4) Then
            MsgBox "Este email j� existe.", vbInformation, "Aten��o"
            'StayOnTop frmCadEmail
            Exit Sub
        End If
    Next
    
    'Verificar se existe uma sequencia igual, se existir dar update
    If UCase(Frame1.Caption) = "ALTERAR" Then
        If MsgBox("Confirma a altera��o deste e-mail ?", vbYesNo, "Aten��o") = vbYes Then
           For I = 1 To msfEmail.Rows
               If txtSeq = msfEmail.TextMatrix(I, 0) Then
                  Exit For
               End If
           Next
           
           msfEmail.TextMatrix(I, 0) = txtSeq
           msfEmail.TextMatrix(I, 1) = txtEmail
           msfEmail.TextMatrix(I, 2) = txtContato
           msfEmail.TextMatrix(I, 3) = cboDepto
           msfEmail.TextMatrix(I, 4) = txtDtCadastro
        Else
            'StayOnTop frmCadEmail
            Exit Sub
        End If
    ElseIf UCase(Frame1.Caption) = "NOVO" Then
       msfEmail.Rows = msfEmail.Rows + 1
       msfEmail.TextMatrix(I, 0) = txtSeq
       msfEmail.TextMatrix(I, 1) = txtEmail
       msfEmail.TextMatrix(I, 2) = txtContato
       msfEmail.TextMatrix(I, 3) = cboDepto
       msfEmail.TextMatrix(I, 4) = txtDtCadastro
    End If
    
    'Inserir no banco
    db.Parameters.Remove "Cod_Cliente"
    db.Parameters.Remove "Sequencia"
    db.Parameters.Remove "Email"
    db.Parameters.Remove "CodDepto"
    db.Parameters.Remove "Contato"
    
    db.Parameters.Add "Cod_Cliente", txtCodCliente, 1
    db.Parameters.Add "Sequencia", txtSeq, 1
    db.Parameters.Add "Email", Null, 1
    db.Parameters.Add "CodDepto", cboDepto.ItemData(cboDepto.ListIndex), 1
    db.Parameters.Add "Contato", txtContato, 1
    
    If UCase(Frame1.Caption) = "NOVO" Then
       db.Executesql "BEGIN " & strTabela_Banco & "PCK_CAD_EMAIL.PR_INCLUIR_EMAIL(:Cod_Cliente,:Sequencia, :Email, :CodDepto, :Contato); END;"
    ElseIf UCase(Frame1.Caption) = "ALTERAR" Then
       db.Executesql "BEGIN " & strTabela_Banco & "PCK_CAD_EMAIL.PR_ALTERAR_EMAIL(:Cod_Cliente,:Sequencia, :Email, :CodDepto, :Contato); END;"
    End If
    
    txtContato = ""
    txtEmail = ""
    txtSeq = ""
    cboDepto.ListIndex = -1
     
    Frame1.Enabled = False
    
    Preencher_Email
    
    Botoes True, False, False, False, False, False
    

End Sub

Private Sub Form_Activate()
    If msfEmail.Rows = 1 Then cmdNovo_Click
End Sub

Private Sub Form_Load()
    
    vLoad = True
    
    blnAtualizarEmailSAP = 0
        
    'Me.txtCodCliente = frmVenda.txtCOD_CLIENTE
    Me.txtCodCliente = frmCadCliente.txtCodCliente
        
    Botoes True, False, False, False, False, False

    Preencher_Data
        
    Preencher_Depto
    
    Preencher_Email
    
    Pegar_nome_Cliente
    
    vLoad = False
    
    'Emerson - 22/12/17 - TI-6187 - Cadastro Email de cliente - Sistema de Televendas DPK.
    vstrEmail_Selecionado = ""
    
End Sub

Private Sub msfEmail_ItemClick(ByVal Item As MSComctlLib.ListItem)
    txtSeq = Item
    txtEmail = Item.SubItems(1)
    txtContato = Item.SubItems(2)
    cboDepto = Item.SubItems(3)
    txtDtCadastro = Item.SubItems(4)
        
    If Trim(txtDtCadastro) = "" Then txtDtCadastro = txtDtAtual
        
    Frame1.Enabled = False
    Frame1.Caption = ""
    Botoes False, True, True, False, True, True
        
End Sub

Sub Preencher_Data()
    
    Criar_Cursor OraParameters, "cCursor"

    db.Executesql "BEGIN " & strTabela_Banco & "PCK_CAD_EMAIL.PR_SELECT_DATA_CADASTRO(:vCursor); END;"

    Set vRst = db.Parameters("vCursor").Value
            
    txtDtAtual = vDtHoje
    vHoje = vDtHoje

End Sub

Sub Preencher_Depto()
    
    db.Executesql "BEGIN " & strTabela_Banco & "PCK_CAD_EMAIL.PR_SELECT_DEPTO(:vCursor); end;"

    Set vRst = db.Parameters("vCursor").Value
            
    For I = 1 To vRst.RecordCount
        cboDepto.AddItem vRst!Descricao
        cboDepto.ItemData(cboDepto.NewIndex) = vRst!Cod_Depto
        vRst.MoveNext
    Next

End Sub

Sub Preencher_Email()
    Dim litem As ListItem
    Dim II As Integer
    
    Criar_Cursor OraParameters, "cCursor"
    
    db.Parameters.Remove "Cod_Cliente"
    db.Parameters.Add "Cod_cliente", txtCodCliente, 1
    
    db.Executesql "BEGIN " & strTabela_Banco & "PCK_CAD_EMAIL.PR_SELECT_EMAIL(:vCursor, :Cod_Cliente); END;"
    
    Set vRst = db.Parameters("vCursor").Value
    
    msfEmail.Clear
    
    Montar_Grid vRst
    
    For I = 1 To vRst.RecordCount
        With msfEmail
            .TextMatrix(I, 0) = vRst!sequencia
            .TextMatrix(I, 1) = IIf(IsNull(vRst!Email), "", vRst!Email)
            .TextMatrix(I, 2) = IIf(IsNull(vRst!NOME_CONTATO), "", vRst!NOME_CONTATO)
            .TextMatrix(I, 3) = vRst!Descricao
            .TextMatrix(I, 4) = IIf(IsNull(vRst!Dt_Email), "", vRst!Dt_Email)
        
            If .TextMatrix(I, 4) = "" Then
               For II = 0 To 4
                    .Row = I
                    .Col = II
                    .CellFontBold = True
                    .CellForeColor = vbRed
               Next
            Else
                If DateAdd("m", -6, vHoje) > CDate(.TextMatrix(I, 4)) Then
                    For II = 0 To 4
                         .Row = I
                         .Col = II
                         .CellFontBold = True
                         .CellForeColor = vbRed
                    Next
                End If
            End If
        End With
        vRst.MoveNext
    Next
    Set vRst = Nothing
End Sub

Sub Pegar_nome_Cliente()

    db.Parameters.Remove "Cod_Cliente"
    db.Parameters.Add "Cod_Cliente", txtCodCliente, 1
    
    db.Executesql "BEGIN " & strTabela_Banco & "PCK_CAD_EMAIL.PR_SELECT_NOME_CLIENTE(:vCursor, :Cod_Cliente); END;"

    Set vRst = db.Parameters("vCursor").Value
    
    If vRst.EOF = False Then Me.lblNomeCliente = vRst!nome_cliente
    
    Set vRst = Nothing

End Sub

Sub Botoes(pNovo As Boolean, pAlterar As Boolean, pDesfazer As Boolean, pGravar As Boolean, pExcluir As Boolean, pSemEmail As Boolean)

    cmdNovo.Enabled = pNovo
    cmdLimpar.Enabled = pDesfazer
    cmdAdd.Enabled = pGravar
    cmdExcluir.Enabled = pExcluir
    cmdSemEmail.Enabled = pSemEmail
End Sub

Function isEmail(ByVal pEmail As String) As Boolean
        
    Dim Conta As Integer, Flag As Integer, cValido As String
    
    isEmail = False
    
    If Len(pEmail) < 3 Then
        Exit Function
    End If

    
    'Verifica se existe caracter inv�lido
    
    For Conta = 1 To Len(pEmail)
        cValido = Mid(pEmail, Conta, 1)
        If Not (LCase(cValido) Like "[a-z]" Or cValido = _
            "@" Or cValido = "." Or cValido = "-" Or _
            cValido = "_" Or cValido Like "[0-9]") Then
            Exit Function
        End If
    Next
  

    'Verifica a exist�ncia de (@)
    
    If InStr(pEmail, "@") = 0 Then
        Exit Function
    Else
        Flag = 0
        
        For Conta = 1 To Len(pEmail)
            If Mid(pEmail, Conta, 1) = "@" Then
                Flag = Flag + 1
            End If
        Next
        
        If Flag > 1 Then Exit Function
    End If
  
    If Left(pEmail, 1) = "@" Then
        Exit Function
    ElseIf Right(pEmail, 1) = "@" Then
        Exit Function
    ElseIf InStr(pEmail, ".@") > 0 Then
        Exit Function
    ElseIf InStr(pEmail, "@.") > 0 Then
        Exit Function
    End If
  
  
    'Verifica a exist�ncia de (.)
    
    If InStr(pEmail, ".") = 0 Then
        Exit Function
    ElseIf Left(pEmail, 1) = "." Then
        Exit Function
    ElseIf Right(pEmail, 1) = "." Then
        Exit Function
    ElseIf InStr(pEmail, "..") > 0 Then
        Exit Function
    End If
    
    'Emerson - 22/12/17 - TI-6287 - Cadastro Email de cliente - Sistema de Televendas DPK
    'Se o cliente for Funcion�rio permite email DPK...
    If Not Verifica_Funcionario And _
        InStr(UCase$(pEmail), "DPK") > 0 Then
        Exit Function
    End If
  
    isEmail = True

End Function

Sub Montar_Grid(rst As Object)
    With msfEmail
        .Rows = rst.RecordCount + 1
        .TextMatrix(0, 0) = "Seq"
        .TextMatrix(0, 1) = "E-mail"
        .TextMatrix(0, 2) = "Contato"
        .TextMatrix(0, 3) = "Departamento"
        .TextMatrix(0, 4) = "Dt Cadastro"
        .ColAlignment(0) = vbCenter
        .ColWidth(0) = 500
        .ColWidth(1) = 3000
        .ColWidth(2) = 2000
        .ColWidth(3) = 2000
        .ColWidth(4) = 1200

    End With
End Sub

Private Sub msfEmail_Click()
    If msfEmail.TextMatrix(0, 0) = "" Or vLoad = True Then Exit Sub
    msfEmail_EnterCell
End Sub

Private Sub msfEmail_EnterCell()
    If msfEmail.Text = "" Or vLoad = True Then Exit Sub
    
    txtSeq = msfEmail.TextMatrix(msfEmail.Row, 0)
    txtEmail = msfEmail.TextMatrix(msfEmail.Row, 1)
    txtContato = msfEmail.TextMatrix(msfEmail.Row, 2)
    
    '#1198 - Caso sejam excluidos todos os emails do cliente
    'cboDepto = msfEmail.TextMatrix(msfEmail.Row, 3)
    If msfEmail.Rows > 1 Then cboDepto = msfEmail.TextMatrix(msfEmail.Row, 3)

    txtDtCadastro = msfEmail.TextMatrix(msfEmail.Row, 4)
        
    If Trim(txtDtCadastro) = "" Then txtDtCadastro = txtDtAtual
        
    'Emerson - 22/12/17 - TI-6187 - Cadastro Email de cliente - Sistema de Televendas DPK.
    vstrEmail_Selecionado = txtEmail
        
    Frame1.Enabled = False
    Frame1.Caption = ""
    Botoes True, True, True, True, True, True
    
    cmdAlterar_Click
    
    txtContato.SetFocus
    
End Sub


Private Sub txtContato_KeyPress(KeyAscii As Integer)
    'Muniz 10/02/10 valida txtContato letra mai�scula e alfa num�rico
    KeyAscii = ValidaMaiuscula(KeyAscii)
    KeyAscii = ValidaAlfaNumerico(KeyAscii)
End Sub

Private Sub txtEmail_KeyPress(KeyAscii As Integer)
    'Muniz 11/02/10 valida txtContato letra mai�scula e alfa num�rico
    KeyAscii = ValidaMaiuscula(KeyAscii)
    KeyAscii = ValidaAlfaNumerico(KeyAscii)
End Sub

Private Sub txtEmail_LostFocus()
    Me.cmdAdd.SetFocus
End Sub

'carlosj CiT [28/08/2012]
 'Popula comunicacao (emails) do array do objeto cliente
 Sub PopularObjetoClienteComunicacao(ByRef oCliente As ClienteSAP)
    
    'verifica se a lista esta vazia
    If (msfEmail.Rows < 1) Then
        Exit Sub
    End If
    
    Dim posicaoItemMaisRecente As Integer
     posicaoItemMaisRecente = ObterPosicaoItemMaisAtual
    
    Dim arrayComunicacao() As New Comunicacao
    Dim oComunicacao As Comunicacao
    'redimensiona o array
    
   'Luis Mota: 03/09/2014
   'Descri��o: #1198 - Ler todos os registros da grid
   
   '*** Inicio da altera��o ***
   'ReDim arrayComunicacao(msfEmail.Rows - 2)
    ReDim arrayComunicacao(msfEmail.Rows - 1)
        '*** Fim altera��o ***
    
    For I = 1 To msfEmail.Rows - 1
    
    Set oComunicacao = New Comunicacao
    
    If (I = posicaoItemMaisRecente) Then
        oComunicacao.Preferencial = "S"
    Else
        oComunicacao.Preferencial = "N"
    End If
    
    oComunicacao.TpMail = ""
    oComunicacao.Email = msfEmail.TextMatrix(I, 1)
    Set arrayComunicacao(I - 1) = oComunicacao
    
    Next I
    
     Call oCliente.ArrayComunicacoes(arrayComunicacao)
     
 End Sub

 'carlosj CiT [28/08/2012]
 'verifica a data mais atual existente na lista de email e retorna a sua posi��o na lista (grid)
 Private Function ObterPosicaoItemMaisAtual() As Integer
 
    Dim dtMaisAtual As Date
    Dim posicaoItem As Integer
    
    posicaoItem = 0
    
     For I = 1 To msfEmail.Rows - 1
       If (I = 1) Then
        dtMaisAtual = msfEmail.TextMatrix(I, 4)
        ElseIf (DateDiff("d", dtMaisAtual, msfEmail.TextMatrix(I, 4)) >= 0) Then
           dtMaisAtual = msfEmail.TextMatrix(I, 4)
           posicaoItem = I
       End If
     Next I
    ObterPosicaoItemMaisAtual = posicaoItem
 End Function

'Emerson - 22/12/17 - TI-6287 - Cadastro Email de cliente - Sistema de Televendas DPK
Function Verifica_Funcionario() As Boolean

    Verifica_Funcionario = False

    db.Parameters.Remove "Cod_Cliente"
    db.Parameters.Add "Cod_Cliente", txtCodCliente, 1
    
    db.Executesql "BEGIN " & strTabela_Banco & "PCK_CAD_EMAIL.PR_VERIFICA_FUNCIONARIO(:vCursor, :Cod_Cliente); END;"

    Set vRst = db.Parameters("vCursor").Value
    
    If vRst!Funcionario = "SIM" Then Verifica_Funcionario = True
    
    Set vRst = Nothing

End Function
'Emerson - 22/12/17 - TI-6287 - Cadastro Email de cliente - Sistema de Televendas DPK

