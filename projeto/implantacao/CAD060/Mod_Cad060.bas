Attribute VB_Name = "Mod_Cad060"
'---------------------------------------------------------------------------------------
' Module    : Mod_Cad060
' Author    : c.samuel.oliveira
' Date      : 07/05/2018
' Purpose   : TI-6488
'---------------------------------------------------------------------------------------

'---------------------------------------------------------------------------------------
' Module    : Mod_Cad060
' Author    : c.samuel.oliveira
' Date      : 23/10/15
' Purpose   : TI-3221
'---------------------------------------------------------------------------------------

Option Explicit

Public dbOra  As Object                 'Sess�o do banco
Public dbOraInt  As Object                 'Sess�o do banco
Public db As Object                     'Conex�o com banco oracle Producao
Public db_INT As Object                 'Conex�o com banco oracle Internet
Public db_CONS As Object                'Objeto para recordset em geral
Public vUser_Intranet As String         'Pega o login do usu�rio da intranet
Public vCodUser_Intranet As Double      'Pega o c�d do usu�rio da intranet
Public OraParameters As Object
Public Sql As String                    'String para selects em geral
Public vFl_Analise As Boolean           'Indica se a tela que ser� aberta, ser� de cadastro de solicita��o internet ou cadastro normal
Public vFl_Volta_Analise As Boolean     'Indica se o usu�rio est� voltando da tela de cadastro (an�lise web), para fazer refresh do grid de solicita��es a serem analisadas
Public vFl_Tipo_Solicitacao As Integer  'Indica o tipo da solicita��o da WEB (0=Pr�-Cadastro/1=Pr�-Altera��o/2=Pr�-Recadastro)
Public vFl_Consulta As Boolean          'Indica se � consulta de pre-cadastro
Public vFl_IncCad As Boolean            'Indica se ser� inclus�o de cliente ou altera��o, no formul�rio de cadastro
Public vTP_CONSULTA As Byte             'Indica quais as condi��es para formar a combo de filial na an�lise das fichas
Public vDtHoje As String
Public vDtHoraHoje As String
Public db_Hora As Object
Public vCGC_Analisado As String         'Armazena o CGC do cliente que est� sendo analisado
Public vNome_Analisado As String        'Armazena o nome do cliente que est� sendo analisado
Public vCodCli_Analisado As String      'Armazena o c�digo do cliente que est� sendo analisado (somente no caso de pr�-altera��o e recadastro)
Public vUserRem_Analisado As String     'Armazena o usu�rio q/ remeteu a solicita�ao
Public vFl_Campos_OK As Boolean         'Flag que indica que todos os campos est�o ok para grava��o das refer. no caso de vfl_analise=true
Public Caminho As String
Public CodigoSistema As Double          'Armazena o Codigo do Sistema
Public Rst_Geral As Object                  'Retorno de cursor - funcoes.dll
Public rst As Object
'Public ValidaCampo As New Funcoes
Public strTabela_Banco As String
Global Const ORATYPE_CURSOR = 102
Global Const ORADYN_NO_BLANKSTRIP = &H2&
Global Const ORAPARM_INPUT = 1              'CONSTANTE DE BIND INPUT
Global Const ORAPARM_OUTPUT = 2             'CONSTANTE DE BIND OUTPUT
Global Const ORAPARM_BOTH = 3               'CONSTANTE DE BIND INPUT/OUTPUT
Global Const ORATYPE_NUMBER = 2

Public Fl_Cliente_Novo
Public Fl_Ja_Relacionou
Public Fl_Tare
Public Cliente_SCI
Public Linha_Produto
Public Fl_Invalida
Public vFilialRepres    As Long

'Janela de Pastas - Inicio
    Public Type BROWSEINFO
      hOwner          As Long
      pidlRoot        As Long
      pszDisplayName  As String
      lpszTitle       As String
      ulFlags         As Long
      lpfn            As Long
      lParam          As Long
      iImage          As Long
    End Type
        
    Public Declare Function SHGetPathFromIDList Lib _
       "shell32.dll" Alias "SHGetPathFromIDListA" _
       (ByVal pidl As Long, _
        ByVal pszPath As String) As Long
    
    Public Declare Function SHBrowseForFolder Lib _
       "shell32.dll" Alias "SHBrowseForFolderA" _
       (lpBrowseInfo As BROWSEINFO) As Long
'Janela de pastas - Fim

Sub pGravaControleWeb()

    'Se j� tiver aceito qualquer tipo de solicita��o, vai alterar a situa��o para "2"
    If vFl_Analise = True And vFl_Consulta = False Then
    
    
        'Pega a data e hora da altera��o
        Sql = "SELECT TO_CHAR(sysdate,'dd/mm/yy hh24:mi:ss') FROM dual "
        Set db_Hora = db.dbcreatedynaset(Sql, 0&)
        vDtHoraHoje = db_Hora.Fields(0).Value
    
    
        Set OraParameters = db_INT.Parameters
        OraParameters.Remove "CGC": OraParameters.Add "CGC", Val(vCGC_Analisado), 1
        OraParameters.Remove "TPSOL": OraParameters.Add "TPSOL", Val(vFl_Tipo_Solicitacao), 1
        OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
        OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
        'Alterar Dt.Analise = Dt.Hoje, Situacao = 2 (Aceito), Cod_Usuario_Credito = vUser_Intranet
        'OraParameters.Remove "DTANAL": OraParameters.Add "DTANAL", vDtHoje, 1
        OraParameters.Remove "DTANAL": OraParameters.Add "DTANAL", vDtHoraHoje, 1
        OraParameters.Remove "SITUACAO": OraParameters.Add "SITUACAO", 2, 1
        OraParameters.Remove "CODUSER": OraParameters.Add "CODUSER", Val(vCodUser_Intranet), 1
        db_INT.Executesql "BEGIN INTRANET.PCK_CLIENTES_TESTE.PR_ATU_PRECAD_CTRCLIWEB" & _
                                 "(:CGC,:TPSOL,TO_DATE(:DTANAL,'DD/MM/RR HH24:MI:SS'),:SITUACAO," & _
                                 " :CODUSER,:vErro,:vMsgErro); END;"
        
        If Val(IIf(IsNull(db.Parameters("vErro")), 0, db.Parameters("vErro"))) <> 0 Then
           MsgBox "Erro: " & Val(OraParameters("vErro").Value) & " - " & _
                           OraParameters("vMsgErro").Value & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
            Exit Sub
        End If
    
    End If
    
    'Se tiver descartando a consulta de um pr�-cadastro ou recadastro aceito da web ou se for altera��o, vai deletar da cliente_web
    If vFl_Consulta = True Or Val(vFl_Tipo_Solicitacao) = 1 Then
    
        'Vai Deletar registro da Cliente_Web somente ap�s o cr�dito preencher limite do cliente
        Set OraParameters = db_INT.Parameters
        OraParameters.Remove "CGC": OraParameters.Add "CGC", Val(vCGC_Analisado), 1
        OraParameters.Remove "TPSOL": OraParameters.Add "TPSOL", Val(vFl_Tipo_Solicitacao), 1
        OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
        OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
        db_INT.Executesql "BEGIN INTRANET.PCK_CLIENTES_TESTE.PR_DEL_CLIWEB" & _
                                 "(:CGC,:TPSOL,:vErro,:vMsgErro); END;"
        If Val(IIf(IsNull(db.Parameters("vErro")), 0, db.Parameters("vErro"))) <> 0 Then
            MsgBox "Erro " & db.Parameters("vErro") & "-" & db.Parameters("vMsgErro") & ". Avise o Depto.Suporte!", vbCritical, "CAD060 - Aten��o"
            Exit Sub
        End If

    End If


End Sub

Sub Alinha_Formulario(formulario As Object)
    
    'Vai alinhar subformul�rio, de acordo com o tamanho do mdi
    formulario.Height = 8520
    formulario.Left = -15
    formulario.Top = 1635
    formulario.Width = 12000
    

End Sub

Sub Carrega_Combos_Genericas(CampoTabela As String, CampoTela As Object)
    Dim vPassou1Vez As Byte

    Set OraParameters = db.Parameters
    OraParameters.Remove "CPO_CONS": OraParameters.Add "CPO_CONS", CampoTabela, 1
    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    OraParameters("vErro").ServerType = ORATYPE_NUMBER
      
    Criar_Cursor OraParameters, "vCursor"
      
    'Vai fazer select distinct na tabela cliente, para o campo que eu levar como par�metro, para mostrar na combo
    'Sql = "Begin Producao.PCK_CAD060.Pr_Combos_Genericas(:vCursor,:CPO_CONS,:vErro,:vMsgErro); End; "
    'db.Executesql Sql
      
    vErro = vVB_Generica_001.ExecutaPl(db, "Producao.PCK_CAD060.Pr_Combos_Genericas(:vCursor,:CPO_CONS,:vErro,:vMsgErro)")
      
    If vErro = "" Then
        Set db_CONS = OraParameters("vCursor").Value
    Else
        Exit Sub
    End If
    
    If db_CONS.EOF Then
        MsgBox "Problemas ao consultar os dados para preencher as combos. " & Chr(13) & _
               "Por Favor, avise Depto.Suporte ", vbCritical, "CAD060 - Cadastro de Cliente"
        Exit Sub
    End If


    vPassou1Vez = 0
    CampoTela.Clear
    
    While Not db_CONS.EOF
        
        If Not IsNull(db_CONS!Campo.Value) Then
        
            If CampoTabela = "TPDOC" Then
                
                CampoTela.AddItem db_CONS!Campo.Value & " - " & _
                                  IIf(Val(db_CONS!Campo.Value) = 1, "PESSOA JURIDICA", "PESSOA FISICA")
            
            ElseIf CampoTabela = "STATUS" Then
            
                CampoTela.AddItem db_CONS!Campo.Value & " - " & _
                                  IIf(Val(db_CONS!Campo.Value) = 0, "NORMAL", "OUTRAS SAIDAS")
                
            ElseIf CampoTabela = "TPOP" Then
            
                CampoTela.AddItem db_CONS!Campo.Value & " - " & _
                                  IIf(db_CONS!Campo.Value = "A", "NORMAL", _
                                  IIf(db_CONS!Campo.Value = "B", "BOLETO", "ANTES"))
                
            ElseIf CampoTabela = "CHEQUE" Then
            
                CampoTela.AddItem db_CONS!Campo.Value & " - " & _
                                  IIf(Val(db_CONS!Campo.Value) = 0, "SEM CHEQUE S/FUNDO", _
                                  IIf(Val(db_CONS!Campo.Value) = 1, "TEVE 1", "TEVE 2 OU MAIS"))
            
            ElseIf CampoTabela = "PROTESTO" Then
            
                If vPassou1Vez = 0 Then
                    CampoTela.AddItem ""
                    vPassou1Vez = 1
                End If
                'Se vier espa�o em branco, n�o considerar como uma das op��es a serem exibidas no combo
                If Trim(db_CONS!Campo.Value) <> "" Then
                    CampoTela.AddItem db_CONS!Campo.Value & " - " & _
                                      IIf(db_CONS!Campo.Value = "NP", "N�O PROTESTAR", "")
                End If
            
            ElseIf CampoTabela = "CLASSIFICACAO" Then
            
                CampoTela.AddItem db_CONS!Campo.Value
            
            End If
        End If
        db_CONS.MoveNext
    
    Wend
    
End Sub

'fpiardi Ci&T - 26/11/2012
'Nova rotina passando o Segmento como parametro
Sub Carrega_Combo_Tipo_ClienteDPK(CampoTela As Object, Segmento As String)

    Set OraParameters = db.Parameters
    OraParameters.Remove "PM_SEGMENTO": OraParameters.Add "PM_SEGMENTO", Segmento, 1
    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
      
    'Vai fazer select na tabela tipo_cliente, considerando o campo que eu levar como par�metro, para mostrar na combo tipo dpk ou blau

    Criar_Cursor OraParameters, "vCursor"
    
    vErro = vVB_Generica_001.ExecutaPl(db, "Producao.PCK_CAD060.Pr_Combo_TpClienteDPK(:vCursor,:PM_SEGMENTO,:vErro,:vMsgErro)")
    If vErro <> "" Then
        Exit Sub
    End If
    vVB_Generica_001.PreencheComboList OraParameters("vCursor").Value, CampoTela, "Desc_Tipo_Cli", "cod_tipo_cli"

End Sub
      
'fpiardi Ci&T - 26/11/2012
'rotina abaixo n�o � mais utilizada
'Sub Carrega_Combo_Tipo_ClienteDPK(CampoTela As Object)
'
'    Set OraParameters = db.Parameters
'    OraParameters.Remove "TP_CONS": OraParameters.Add "TP_CONS", 0, 1
'    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
'    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
'
'    'Vai fazer select na tabela tipo_cliente, considerando o campo que eu levar como par�metro, para mostrar na combo tipo dpk ou blau
'
'    Criar_Cursor OraParameters, "vCursor"
'
'    vErro = vVB_Generica_001.ExecutaPl(db, "Producao.PCK_CAD060.Pr_Combo_TpClienteDPK(:vCursor,:TP_CONS,:vErro,:vMsgErro)")
'    If vErro <> "" Then
'        Exit Sub
'    End If
'    vVB_Generica_001.PreencheComboList OraParameters("vCursor").Value, CampoTela, "Desc_Tipo_Cli", "cod_tipo_cli"
'
'
''    Sql = "Begin Producao.PCK_CAD060.Pr_Combo_TpClienteDPK(:vCursor,:TP_CONS,:vErro,:vMsgErro); End; "
''    db.Executesql Sql
'
''    Set db_CONS = OraParameters("vCursor").Value
''    If Val(OraParameters("vErro").Value) <> 0 Then
''            MsgBox "Erro: " & Val(OraParameters("vErro").Value) & " - " & _
''                              OraParameters("vMsgErro").Value & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
''        Exit Sub
''    End If
'
''    If db_CONS.EOF Then
''        MsgBox "Problemas ao consultar os dados para preencher a combo de tipo de cliente DPK. " & Chr(13) & _
''               "Por Favor, avise Depto.Suporte ", vbCritical, "CAD060 - Cadastro de Cliente"
''        Exit Sub
''    End If
'
''    CampoTela.Clear
''    While Not db_CONS.EOF
''        CampoTela.AddItem db_CONS!Desc_Tipo_Cli.Value & " - " & Format(db_CONS!cod_tipo_cli.Value, "00")
''        db_CONS.movenext
''    Wend
'
'End Sub


Sub Carrega_Combo_ClieEndereco(CampoTela As Object, TpCons As Integer)

    Set OraParameters = db.Parameters
    OraParameters.Remove "TP_CONS": OraParameters.Add "TP_CONS", TpCons, 1
    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
      
    'Vai fazer select na tabela tipo_cliente, considerando o campo que eu levar como par�metro, para mostrar na combo tipo dpk ou blau
'    Sql = "Begin Producao.PCK_CAD060.Pr_Combo_ClieEndereco(:vCursor,:TP_CONS,:vErro,:vMsgErro); End; "
'    db.Executesql Sql
      
    Criar_Cursor OraParameters, "vCursor"
    
    vErro = vVB_Generica_001.ExecutaPl(db, "Producao.PCK_CAD060.Pr_Combo_ClieEndereco(:vCursor,:TP_CONS,:vErro,:vMsgErro)")
    If vErro <> "" Then
        Exit Sub
    End If
    
    If TpCons = 0 Then  'Tipo de Endere�o
        vVB_Generica_001.PreencheComboList OraParameters("vCursor").Value, CampoTela, "Tp_Endereco_Cod", "Tp_Endereco"
    ElseIf TpCons = 1 Then  'Tipo de Documento para Outros Endere�os
        vVB_Generica_001.PreencheComboList OraParameters("vCursor").Value, CampoTela, "Tp_Docto_Cod", "Tp_Docto"
    End If
    
'    Set db_CONS = OraParameters("vCursor").Value
'    If Val(OraParameters("vErro").Value) <> 0 Then
'            MsgBox "Erro: " & Val(OraParameters("vErro").Value) & " - " & _
'                              OraParameters("vMsgErro").Value & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
'        Exit Sub
'    End If
'
'    If db_CONS.EOF Then
'        MsgBox "Problemas ao consultar os dados para preencher a combos de outros endere�o. " & Chr(13) & _
'               "Por Favor, avise Depto.Suporte ", vbCritical, "CAD060 - Cadastro de Cliente"
'        Exit Sub
'    End If
'
'    CampoTela.Clear
'    While Not db_CONS.EOF
'        If TpCons = 0 Then  'Tipo de Endere�o
'            CampoTela.AddItem db_CONS!Tp_Endereco.Value & " - " & IIf(Val(db_CONS!Tp_Endereco.Value) = 1, "ENTREGA", "COBRAN�A")
'        ElseIf TpCons = 1 Then  'Tipo de Documento para Outros Endere�os
'            If Not IsNull(db_CONS!Tp_Docto.Value) Then
'                CampoTela.AddItem db_CONS!Tp_Docto.Value & " - " & IIf(Val(db_CONS!Tp_Docto.Value) = 1, "PESSOA JUR�DICA", "PESSOA F�SICA")
'            End If
'        End If
'        db_CONS.movenext
'    Wend
    
End Sub

'Sub Carrega_Combo_Tipo_ClienteBlau(CampoTela As Object)
    'Set OraParameters = db.Parameters
    'OraParameters.Remove "TP_CONS": OraParameters.Add "TP_CONS", 0, 1
    'OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    'OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    'OraParameters("vErro").ServerType = ORATYPE_NUMBER
    'Criar_Cursor OraParameters, "vCursor"
    'vVB_Generica_001.ExecutaPl db, "Producao.PCK_CAD060.Pr_Combo_TpClienteBlau(:vCursor,:TP_CONS,:vErro,:vMsgErro)"
    'vVB_Generica_001.PreencheComboList OraParameters("vCursor").Value, CampoTela, "Desc_Tipo_Cli", "cod_tipo_cli"
'End Sub



Sub Carrega_Combo_Repres(CampoTabela As String, CampoTela As Object)

    Set OraParameters = db.Parameters
    OraParameters.Remove "CPO_CONS": OraParameters.Add "CPO_CONS", CampoTabela, 1
    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    OraParameters("vErro").ServerType = ORATYPE_NUMBER
      
    'Vai fazer select na tabela representante, considerando o campo que eu levar como par�metro, para mostrar na combo repres dpk ou blau
    'Sql = "Begin Producao.PCK_CAD060.Pr_Combo_Repres(:vCursor,:CPO_CONS,:vErro,:vMsgErro); End; "
    'db.Executesql Sql
      
    Criar_Cursor OraParameters, "vCursor"
    vErro = vVB_Generica_001.ExecutaPl(db, "Producao.PCK_CAD060.Pr_Combo_Repres(:vCursor,:CPO_CONS,:vErro,:vMsgErro)")
    If vErro <> "" Then
        Exit Sub
    End If
    vVB_Generica_001.PreencheComboList OraParameters("vCursor").Value, CampoTela, "Nome_Repres", "Cod_Repres"
      
    'Set db_CONS = OraParameters("vCursor").Value
    'If Val(OraParameters("vErro").Value) <> 0 Then
    '        MsgBox "Erro: " & Val(OraParameters("vErro").Value) & " - " & _
    '                          OraParameters("vMsgErro").Value & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
    '    Exit Sub
    'End If

    'If db_CONS.EOF Then
    '    MsgBox "Problemas ao consultar os dados para preencher a combo de representante. " & Chr(13) & _
    '           "Por Favor, avise Depto.Suporte ", vbCritical, "CAD060 - Cadastro de Cliente"
    '    Exit Sub
    'End If

    'CampoTela.Clear
    'While Not db_CONS.EOF

        'Se vier espa�o em branco, n�o considerar como uma das op��es a serem exibidas no combo
'        If Trim(db_CONS!nome_Repres.Value) <> "" Then
'            CampoTela.AddItem db_CONS!nome_Repres.Value & " - " & Format(db_CONS!cod_Repres.Value, "0000")
'        End If
'        db_CONS.movenext

'    Wend

End Sub

Sub Carrega_Combo_Mensagem(CampoTabela As String, CampoTela As Object)

    Set OraParameters = db.Parameters
    OraParameters.Remove "CPO_CONS": OraParameters.Add "CPO_CONS", CampoTabela, 1
    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    'OraParameters("vErro").ServerType = ORATYPE_NUMBER
      
    'Vai fazer select na tabela mensagem, considerando o campo que eu levar como par�metro, para mostrar na combo repres dpk ou blau
'    Sql = "Begin Producao.PCK_CAD060.Pr_Combo_Mensagem(:vCursor,:CPO_CONS,:vErro,:vMsgErro); End; "
'    db.Executesql Sql
      
    Criar_Cursor OraParameters, "vCursor"
      
    vErro = vVB_Generica_001.ExecutaPl(db, "Producao.PCK_CAD060.Pr_Combo_Mensagem(:vCursor,:CPO_CONS,:vErro,:vMsgErro)")
    If vErro <> "" Then
        Exit Sub
    End If
      
    vVB_Generica_001.PreencheComboList OraParameters("vCursor").Value, CampoTela, "Desc_Mens", "cod_mensagem"
    CampoTela.AddItem ""
    
    'Set db_CONS = OraParameters("vCursor").Value
'    If Val(OraParameters("vErro").Value) <> 0 Then
'        MsgBox "Erro: " & Val(OraParameters("vErro").Value) & " - " & _
'                          OraParameters("vMsgErro").Value & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
'        Exit Sub
'    End If
'
'    If db_CONS.EOF Then
'        MsgBox "Problemas ao consultar os dados para preencher as combos de mensagem. " & Chr(13) & _
'               "Por Favor, avise Depto.Suporte ", vbCritical, "CAD060 - Cadastro de Cliente"
'        Exit Sub
'    End If
'
'    CampoTela.Clear
'    While Not db_CONS.EOF
'
'        'Se vier espa�o em branco, n�o considerar como uma das op��es a serem exibidas no combo
'        If Trim(db_CONS!Desc_Mens.Value) <> "" Then
'            CampoTela.AddItem db_CONS!Desc_Mens.Value & " - " & Format(db_CONS!cod_mensagem.Value, "00")
'        End If
'        db_CONS.movenext
'
'    Wend
    
End Sub


Sub Carrega_Combo_Cidade(CodUF As String, CampoTela As Object)

    Set OraParameters = db.Parameters
    OraParameters.Remove "CPO_CONS": OraParameters.Add "CPO_CONS", CodUF, 1
    OraParameters.Remove "CODCID": OraParameters.Add "CODCID", 0, 1
    OraParameters.Remove "TP_CONS": OraParameters.Add "TP_CONS", 0, 1
    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    OraParameters("vErro").ServerType = ORATYPE_NUMBER
      
    Criar_Cursor OraParameters, "vCursor"
      
    'Vai fazer select na tabela cidade, considerando o campo que eu levar como par�metro, para mostrar na combo repres dpk ou blau
    'Sql = "Begin Producao.PCK_CAD060.Pr_Combo_Cidade(:vCursor,:CPO_CONS,:CODCID,:TP_CONS,:vErro,:vMsgErro); End; "
    'db.Executesql Sql
      
    vErro = vVB_Generica_001.ExecutaPl(db, "Producao.PCK_CAD060.Pr_Combo_Cidade(:vCursor,:CPO_CONS,:CODCID,:TP_CONS,:vErro,:vMsgErro)")
    If vErro <> "" Then
        Exit Sub
    End If
      
    vVB_Generica_001.PreencheComboList OraParameters("vCursor").Value, CampoTela, "nome_cidade", "cod_cidade"
      
    'Set db_CONS = OraParameters("vCursor").Value
    
'    If Val(OraParameters("vErro").Value) <> 0 Then
'        MsgBox "Erro: " & Val(OraParameters("vErro").Value) & " - " & _
'                          OraParameters("vMsgErro").Value & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
'        Exit Sub
'    End If
'
'    If db_CONS.EOF Then
'        MsgBox "Problemas ao consultar os dados para preencher a combo de cidade. " & Chr(13) & _
'               "Por Favor, avise Depto.Suporte ", vbCritical, "CAD060 - Cadastro de Cliente"
'        Exit Sub
'    End If
'
'    CampoTela.Clear
'    While Not db_CONS.EOF
'
'        CampoTela.AddItem db_CONS!nome_cidade.Value & " - " & Format(db_CONS!cod_cidade.Value, "0000")
'        db_CONS.movenext
'
'    Wend
    
End Sub
Sub Carrega_Combo_Cnae(CodCnae As String, CampoTela As Object)
'SDS 2557 - Marco Aurelio Andrade Jr - Cria��o do combo CNAE
    Set OraParameters = db.Parameters
    OraParameters.Remove "CODCNAE": OraParameters.Add "CODCNAE", CodCnae, 1
    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    OraParameters("vErro").ServerType = ORATYPE_NUMBER
      
    Criar_Cursor OraParameters, "vCursor"
      
    'Vai fazer select na tabela cidade, considerando o campo que eu levar como par�metro, para mostrar na combo repres dpk ou blau
    'Sql = "Begin Producao.PCK_CAD060.Pr_Combo_Cidade(:vCursor,:CPO_CONS,:CODCID,:TP_CONS,:vErro,:vMsgErro); End; "
    'db.Executesql Sql
      
    vErro = vVB_Generica_001.ExecutaPl(db, "Producao.PCK_CAD060.Pr_Combo_Cnae(:vCursor,:CODCNAE,:vErro,:vMsgErro)")
    If vErro <> "" Then
        Exit Sub
    End If
      
    vVB_Generica_001.PreencheComboList OraParameters("vCursor").Value, CampoTela, "cod_cnae", "descricao"
      
    'Set db_CONS = OraParameters("vCursor").Value
    
'    If Val(OraParameters("vErro").Value) <> 0 Then
'        MsgBox "Erro: " & Val(OraParameters("vErro").Value) & " - " & _
'                          OraParameters("vMsgErro").Value & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
'        Exit Sub
'    End If
'
'    If db_CONS.EOF Then
'        MsgBox "Problemas ao consultar os dados para preencher a combo de cidade. " & Chr(13) & _
'               "Por Favor, avise Depto.Suporte ", vbCritical, "CAD060 - Cadastro de Cliente"
'        Exit Sub
'    End If
'
'    CampoTela.Clear
'    While Not db_CONS.EOF
'
'        CampoTela.AddItem db_CONS!nome_cidade.Value & " - " & Format(db_CONS!cod_cidade.Value, "0000")
'        db_CONS.movenext
'
'    Wend
    
End Sub
Sub Carrega_Combo_Responsaveis(TipoCombo As String, CampoTela As Object)

    Set OraParameters = db_INT.Parameters
    OraParameters.Remove "CPO_CONS": OraParameters.Add "CPO_CONS", "", 1
    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    OraParameters("vErro").ServerType = ORATYPE_NUMBER
    Criar_Cursor OraParameters, "vCursor"
    
    'Vai alimentar combo com todos os usu�rios que hoje j� t�m clientes sob sua responsabilidade
    If TipoCombo = "CLIENTE" Then
        OraParameters.Remove "TP_CONS": OraParameters.Add "TP_CONS", 0, 1
    
    'Vai alimentar combo com todos os usu�rios cadastrados na Intranet
    ElseIf TipoCombo = "TODOS" Then
        OraParameters.Remove "TP_CONS": OraParameters.Add "TP_CONS", 1, 1
    
    End If

    vErro = vVB_Generica_001.ExecutaPl(db_INT, "Intranet.PCK_CLIENTES_TESTE.Pr_Responsaveis(:vCursor,:CPO_CONS,:TP_CONS,:vErro,:vMsgErro)")
    If vErro <> "" Then
        MsgBox "Problemas na consulta. Contate o suporte.", vbInformation, "CAD060 - Cadastro de Clientes"
        Exit Sub
    End If
      
    vVB_Generica_001.PreencheComboList OraParameters("vCursor").Value, CampoTela, "Nome_Usuario", "Cod_Usuario"


End Sub

Sub Carrega_Combo_Caracteristica(CampoTela As Object)

    Set OraParameters = db.Parameters
        
    OraParameters.Remove "TP_CONS": OraParameters.Add "TP_CONS", 0, 1
    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    OraParameters("vErro").ServerType = ORATYPE_NUMBER
      
    Criar_Cursor OraParameters, "vCursor"
      
    vErro = vVB_Generica_001.ExecutaPl(db, "Producao.PCK_CAD060.Pr_Combo_Caracteristica(:vCursor,:TP_CONS,:vErro,:vMsgErro)")
    If vErro <> "" Then
        Exit Sub
    End If
      
    Set db_CONS = OraParameters("vCursor").Value
    
    vVB_Generica_001.PreencheComboList OraParameters("vCursor").Value, CampoTela, "Caracteristica", "Desc_Caracteristica"
   
End Sub

Sub Carrega_Combo_UF(CampoTela As Object)
    Set OraParameters = db.Parameters
        
    Criar_Cursor OraParameters, "vCursor"
    OraParameters.Remove "TP_CONS": OraParameters.Add "TP_CONS", 0, 1
    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
      
    'Vai fazer select na tabela uf, para o campo que eu levar como par�metro, para mostrar na combo
    vErro = vVB_Generica_001.ExecutaPl(db, "Producao.PCK_CAD060.Pr_Combo_UF(:vCursor,:TP_CONS,:vErro,:vMsgErro)")
    If vErro <> "" Then
        Exit Sub
    End If
    Set db_CONS = OraParameters("vCursor").Value
    
    vVB_Generica_001.PreencheComboList OraParameters("vCursor").Value, CampoTela, "COD_UF"
    CampoTela.Text = "SP"
    
'
'    If db_CONS.EOF Then
'        MsgBox "Problemas ao consultar os dados para preencher combo de UF. " & Chr(13) & _
'               "Por Favor, avise Depto.Suporte ", vbCritical, "CAD060 - Cadastro de Cliente"
'        Exit Sub
'    End If
'
'    CampoTela.Clear
'    While Not db_CONS.EOF
'        CampoTela.AddItem db_CONS!cod_uf.Value
'        db_CONS.movenext
'    Wend
    
End Sub

Sub Carrega_Combo_ClieVendor(CampoTela As Object, TpCons As Integer)

    Set OraParameters = db.Parameters
    OraParameters.Remove "vCursor": OraParameters.Add "vCursor", 0, 2
    OraParameters("vCursor").ServerType = ORATYPE_CURSOR
    OraParameters("vCursor").DynasetOption = ORADYN_NO_BLANKSTRIP
    OraParameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
    OraParameters.Remove "TP_CONS": OraParameters.Add "TP_CONS", 0, 1
    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    OraParameters("vErro").ServerType = ORATYPE_NUMBER
      
    'Vai fazer select na tabela uf, para o campo que eu levar como par�metro, para mostrar na combo
    Sql = "Begin Producao.PCK_CAD060.Pr_Combo_ClieVendor(:vCursor,:TP_CONS,:vErro,:vMsgErro); End; "
    db.Executesql Sql
      
    Set db_CONS = OraParameters("vCursor").Value
    If Val(OraParameters("vErro").Value) <> 0 Then
        MsgBox "Erro: " & Val(OraParameters("vErro").Value) & " - " & _
                          OraParameters("vMsgErro").Value & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
        Exit Sub
    End If

    If db_CONS.EOF Then
        MsgBox "Problemas ao consultar os dados para preencher combo de Situa��o Vendor. " & Chr(13) & _
               "Por Favor, avise Depto.Suporte ", vbCritical, "CAD060 - Cadastro de Cliente"
        Exit Sub
    End If

    CampoTela.Clear
    While Not db_CONS.EOF
        If TpCons = 0 Then  'Situa��o Vendor
            CampoTela.AddItem db_CONS!Situacao.Value & " - " & IIf(Val(db_CONS!Situacao.Value) = 0, "ATIVO", "DESATIVADO")
        End If
        db_CONS.MoveNext
    Wend
    
End Sub


Sub Carrega_Combo_Banco(CampoTela As Object)

    Set OraParameters = db.Parameters
    OraParameters.Remove "vCursor": OraParameters.Add "vCursor", 0, 2
    OraParameters("vCursor").ServerType = ORATYPE_CURSOR
    OraParameters("vCursor").DynasetOption = ORADYN_NO_BLANKSTRIP
    OraParameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
    OraParameters.Remove "TP_CONS": OraParameters.Add "TP_CONS", 0, 1
    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    OraParameters("vErro").ServerType = ORATYPE_NUMBER
      
    'Vai fazer select na tabela banco, para o campo que eu levar como par�metro, para mostrar na combo
    Sql = "Begin Producao.PCK_CAD060.Pr_Combo_Banco(:vCursor,:TP_CONS,:vErro,:vMsgErro); End; "
    db.Executesql Sql
      
    Set db_CONS = OraParameters("vCursor").Value
    If Val(OraParameters("vErro").Value) <> 0 Then
        MsgBox "Erro: " & Val(OraParameters("vErro").Value) & " - " & _
                          OraParameters("vMsgErro").Value & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
        Exit Sub
    End If

    If db_CONS.EOF Then
        MsgBox "Problemas ao consultar os dados para preencher combo de Banco. " & Chr(13) & _
               "Por Favor, avise Depto.Suporte ", vbCritical, "CAD060 - Cadastro de Cliente"
        Exit Sub
    End If

    CampoTela.Clear
    While Not db_CONS.EOF
        'Se vier espa�o em branco, n�o considerar como uma das op��es a serem exibidas no combo
        If Trim(db_CONS!sigla.Value) <> "" Then
            CampoTela.AddItem db_CONS!sigla.Value & " - " & Format(db_CONS!cod_banco.Value, "000")
        End If
        db_CONS.MoveNext
    Wend
    
End Sub

Sub Carrega_Combo_CategBosch(CampoTela As Object)

    Set OraParameters = db.Parameters
    OraParameters.Remove "vCursor": OraParameters.Add "vCursor", 0, 2
    OraParameters("vCursor").ServerType = ORATYPE_CURSOR
    OraParameters("vCursor").DynasetOption = ORADYN_NO_BLANKSTRIP
    OraParameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
    OraParameters.Remove "TP_CONS": OraParameters.Add "TP_CONS", 0, 1
    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    OraParameters("vErro").ServerType = ORATYPE_NUMBER
      
    'Vai fazer select na tabela vdr.desconto_categ, para o campo que eu levar como par�metro, para mostrar na combo
    Sql = "Begin Producao.PCK_CAD060.Pr_Combo_CategBosch(:vCursor,:TP_CONS,:vErro,:vMsgErro); End; "
    db.Executesql Sql
      
    Set db_CONS = OraParameters("vCursor").Value
    If Val(OraParameters("vErro").Value) <> 0 Then
        MsgBox "Erro: " & Val(OraParameters("vErro").Value) & " - " & _
                          OraParameters("vMsgErro").Value & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
        Exit Sub
    End If

    If db_CONS.EOF Then
        MsgBox "Problemas ao consultar os dados para preencher combo de Categoria. " & Chr(13) & _
               "Por Favor, avise Depto.Suporte ", vbCritical, "CAD060 - Cadastro de Cliente" 'TI-3221
        Exit Sub
    End If

    CampoTela.Clear
    While Not db_CONS.EOF
        CampoTela.AddItem db_CONS!Categoria.Value
        db_CONS.MoveNext
    Wend
    
End Sub

Sub Carrega_Combo_Transportadora(CampoTela As Object)

    Set OraParameters = db.Parameters
    OraParameters.Remove "vCursor": OraParameters.Add "vCursor", 0, 2
    OraParameters("vCursor").ServerType = ORATYPE_CURSOR
    OraParameters("vCursor").DynasetOption = ORADYN_NO_BLANKSTRIP
    OraParameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
    OraParameters.Remove "TP_CONS": OraParameters.Add "TP_CONS", 0, 1
    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    OraParameters("vErro").ServerType = ORATYPE_NUMBER
      
    'Vai fazer select na tabela transportadora, para o campo que eu levar como par�metro, para mostrar na combo
    Sql = "Begin Producao.PCK_CAD060.Pr_Combo_Transp(:vCursor,:TP_CONS,:vErro,:vMsgErro); End; "
    db.Executesql Sql
      
    Set db_CONS = OraParameters("vCursor").Value
    If Val(OraParameters("vErro").Value) <> 0 Then
        MsgBox "Erro: " & Val(OraParameters("vErro").Value) & " - " & _
                          OraParameters("vMsgErro").Value & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
        Exit Sub
    End If

    If db_CONS.EOF Then
        MsgBox "Problemas ao consultar os dados para preencher combo de Transportadora. " & Chr(13) & _
               "Por Favor, avise Depto.Suporte ", vbCritical, "CAD060 - Cadastro de Cliente"
        Exit Sub
    End If

    CampoTela.Clear
    While Not db_CONS.EOF
        'Se vier espa�o em branco, n�o considerar como uma das op��es a serem exibidas no combo
        If Trim(db_CONS!nome_transp.Value) <> "" Then
            CampoTela.AddItem db_CONS!nome_transp.Value & " - " & Format(db_CONS!Cod_transp.Value, "0000")
        End If
        db_CONS.MoveNext
    Wend
    
End Sub

Sub Limpa_FlexGrid(pNomeGrd As Object)
Dim L As Long
Dim C As Integer
Dim vTotLin As Long

With pNomeGrd

    vTotLin = .Rows - 1
    
    'Varre todas as linhas do grid
    For L = 1 To vTotLin
        'Se for a �ltima linha a ser removida, deleta os campos de cada coluna
        If L = vTotLin Then
            For C = 0 To .Cols - 1
                .TextMatrix(L, C) = ""
            Next C
        'Sen�o remove o item
        Else
            .RemoveItem 1
        End If
    Next L

End With

End Sub

Sub Process_Line_Errors(ByRef Sql)
    Dim iFnum As Integer
        
    If Err.Number = 3186 Or Err.Number = 3188 Or Err.Number = 3260 Then
        Resume
    Else
        MsgBox "Ocorreu o erro: " & Err.Number & " -" & Err.Description & ". Ligue para o departamento de sistemas"
        Screen.MousePointer = vbDefault
        End
    End If
    Exit Sub

Handler_Process_Line_Errors:
    DoEvents
    Resume Next

End Sub

Public Function Pegar_VL_Parametro(NOME_PARAMETRO As String)
    Criar_Cursor db.Parameters, "PM_CURSOR1"
    
    db.Parameters.Remove "Cod_Sistema"
    db.Parameters.Add "Cod_Sistema", CodigoSistema, 1
    
    db.Parameters.Remove "Nome_Parametro"
    db.Parameters.Add "Nome_Parametro", NOME_PARAMETRO, 1
    
    vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD060.PR_SELECT_VL_PARAMETRO(:PM_CURSOR1, :Cod_Sistema, :Nome_Parametro)")
                            
    If vErro = "" Then
       Set rst = db.Parameters("PM_CURSOR1").Value
       Pegar_VL_Parametro = rst("VL_PARAMETRO")
    End If
End Function

' carlosj 22/08/2012 CiT - [ISA 312]
'Concatena rua, endereco e complemento em uma string (separados por v�rgula)
Function ConcatenarEnderecoSAP(rua As String, numero As String, complemento As String) As String

    Dim endereco As String
    endereco = rua & ", " & numero & ", " & complemento
    
    ConcatenarEnderecoSAP = endereco

End Function

' carlosj 22/08/2012 CiT - [ISA 312]
'Desconcatena um endereco separando-o por v�rgula em rua, numero e complemento
Sub DesconcatenarEnderecoSAP(endereco As String, ByRef rua As String, ByRef numero As String, ByRef complemento As String)
 
    Dim arrayEndereco() As String
    Dim I As Integer
    arrayEndereco = Split(endereco, ",", 3)
    
    rua = ""
    numero = ""
    complemento = ""
        
    For I = 0 To UBound(arrayEndereco)
        Select Case I
            Case 0
                rua = Trim(arrayEndereco(I)) ' seta rua
            Case 1
                numero = Trim(arrayEndereco(I)) ' seta numero
            Case 2
                complemento = Trim(arrayEndereco(I)) ' seta complemento
        End Select
    Next I

End Sub

' Kevlin Toshinari Ossada - CiT - 28/08/2012
' Fun��o que busca cliente no SAP e retorna o codigo SAP
Public Function ObterCodSAP(ClienteCGC As String) As String
    Dim oCliente As ClienteSAP
    Dim msgErro As String
    
    If vVB_Generica_001.ValidaCIC(ClienteCGC) Then
        Set oCliente = BuscarClienteSAP(2, ClienteCGC, msgErro)
    ElseIf vVB_Generica_001.ValidaCGC(ClienteCGC) Then
        Set oCliente = BuscarClienteSAP(1, ClienteCGC, msgErro)
    End If
    
    If msgErro <> "" Then
        MsgBox msgErro
    End If
    
    ObterCodSAP = oCliente.NoClienteSAP
End Function

' fpallini - Ci&T - 22/08/2012 - [ISA-330]
' Fun��o B�sica para buscar cliente no SAP
Function BuscarClienteSAP(TipoPessoa As Integer, CnpjOrCpf As String, ByRef msgErro As String) As ClienteSAP
    
    Dim oServicoCliente As New ServicoClienteSAP
    Dim oRetornoPI As New RetornoPI
    msgErro = ""
        
    If TipoPessoa = 1 Then
        'Chamada do Metodo que busca o cliente no SAP por CNPJ
        Set oRetornoPI = oServicoCliente.ConsultaCliente(vUser_Intranet, "", "", "", CnpjOrCpf, "")
    ElseIf TipoPessoa = 2 Then
        'Chamada do Metodo que busca o cliente no SAP por CPF
        Set oRetornoPI = oServicoCliente.ConsultaCliente(vUser_Intranet, "", "", CnpjOrCpf, "", "")
    End If
    
    ' Se teve sucesso utiliza o cliente retornado
    If Trim(UCase(oRetornoPI.CodErro)) = "S" Then
        Set BuscarClienteSAP = oRetornoPI.retorno
        BuscarClienteSAP.DtAlteracaoSAP = Date
        BuscarClienteSAP.DataReCadastro = Date
    ElseIf Trim(UCase(oRetornoPI.CodErro)) = "W" Then
        Set BuscarClienteSAP = New ClienteSAP
        BuscarClienteSAP.DtAlteracaoSAP = Date
        BuscarClienteSAP.DataReCadastro = Date
        BuscarClienteSAP.DtCriacaoSAP = Date
        BuscarClienteSAP.DataCadastro = Date
        BuscarClienteSAP.AnoCadastro = Year(Date)
        BuscarClienteSAP.MesCadastro = Month(Date)
    Else
        Set BuscarClienteSAP = New ClienteSAP
        msgErro = "Erro ao buscar cliente no SAP" & vbNewLine & oRetornoPI.CodErro & " : " & oRetornoPI.mensagem
    End If

End Function


' fpallini - Ci&T - 22/08/2012 - [ISA-330]
' Fun��o B�sica para Salvar Cliente no SAP
Function EnviarClienteSAP(oCliente As ClienteSAP) As Boolean
    Dim oServicoCliente As New ServicoClienteSAP
    Dim retornoCadastro As RetornoPI
    Dim sMsg As String
    Dim descricaoMsgBox As String
    
    ' Faz o cadastro do novo cliente
    Set retornoCadastro = oServicoCliente.CadastrarCliente(vUser_Intranet, oCliente)
    
    ' Obter erros do SAP
    descricaoMsgBox = retornoCadastro.GetErrosSAP()
    
    EnviarClienteSAP = retornoCadastro.Sucesso
    
    'fpiardi - exibir �cone de erro, somente se ocorreu erro
    If (retornoCadastro.Sucesso = True) Then
        MsgBox descricaoMsgBox, vbInformation, "CAD060 - Aten��o"
    Else
        MsgBox descricaoMsgBox, vbCritical, "CAD060 - Aten��o"
    End If

End Function

' vxavier - Ci&T - 06/11/12 - [ISA - 964]
' Essa fun��o ir� buscar o Setor Industrial do cliente para posteriormente enviar para o SAP
Function RetornarSetorIndustrial(Tp_Docto As String, Uf As String, Cnae As String, Cgc As String, Tare As String, IE As String, Tipo As String) As String
    
    Tp_Docto = Val(Tp_Docto)
    Cnae = Trim(Val(Replace(Replace(Left(Cnae, 9), "-", ""), "/", "")))
    IE = Replace(IE, ".", "")
    'Tipo = Right(Tipo, 2)
    
    db.Parameters.Remove "P_TP_DOCTO"
    db.Parameters.Add "P_TP_DOCTO", Tp_Docto, 1
    
    db.Parameters.Remove "P_UF"
    db.Parameters.Add "P_UF", Uf, 1
    
    db.Parameters.Remove "P_CNAE"
    db.Parameters.Add "P_CNAE", Cnae, 1
    
    db.Parameters.Remove "P_CGC"
    db.Parameters.Add "P_CGC", Cgc, 1
    
    db.Parameters.Remove "P_TARE"
    db.Parameters.Add "P_TARE", Tare, 1
    
    db.Parameters.Remove "P_INSCRICAO_ESTADUAL"
    db.Parameters.Add "P_INSCRICAO_ESTADUAL", IE, 1
    
    db.Parameters.Remove "P_TIPO_CLIENTE"
    db.Parameters.Add "P_TIPO_CLIENTE", Tipo, 1
    
    db.Parameters.Remove "P_OUT_SETOR_INDUSTRIAL"
    db.Parameters.Add "P_OUT_SETOR_INDUSTRIAL", "", 2
    
    vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD060.PR_SEL_SETOR_INDUSTRIAL(:P_TP_DOCTO, :P_UF, :P_CNAE, :P_CGC, :P_TARE, :P_INSCRICAO_ESTADUAL, :P_TIPO_CLIENTE, :P_OUT_SETOR_INDUSTRIAL)")
    
    If vErro <> "" Then
        MsgBox "Erro: " & vErro & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
        Exit Function
    End If
    
    RetornarSetorIndustrial = db.Parameters("P_OUT_SETOR_INDUSTRIAL").Value

End Function

'ecampos - Ci&T - 18/12/2012
Function BuscaCidadePorCodigoCidade(CodCidade As String) As String
        
    db.Parameters.Remove "P_COD_CIDADE"
    db.Parameters.Add "P_COD_CIDADE", CodCidade, 1
    
    db.Parameters.Remove "O_NOME_CIDADE"
    db.Parameters.Add "O_NOME_CIDADE", "", 2
    
    vErro = vVB_Generica_001.ExecutaPl(db, "SAP.PCK_INT_SAP.PR_SEL_NOME_POR_CODCIDADE(:P_COD_CIDADE, :O_NOME_CIDADE)")
    
    If vErro <> "" Then
        MsgBox "Erro: " & vErro & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
        Exit Function
    End If
    
    BuscaCidadePorCodigoCidade = db.Parameters("O_NOME_CIDADE").Value

End Function
'ecampos - Ci&T - 18/12/2012
Function BuscaUfPorCodigoCidade(CodCidade As String) As String
        
    db.Parameters.Remove "P_COD_CIDADE"
    db.Parameters.Add "P_COD_CIDADE", CodCidade, 1
    
    db.Parameters.Remove "O_UF_CIDADE"
    db.Parameters.Add "O_UF_CIDADE", "", 2
    
    vErro = vVB_Generica_001.ExecutaPl(db, "SAP.PCK_INT_SAP.PR_SEL_UF_POR_CODCIDADE(:P_COD_CIDADE, :O_UF_CIDADE)")
    
    If vErro <> "" Then
        MsgBox "Erro: " & vErro & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
        Exit Function
    End If
    
    BuscaUfPorCodigoCidade = db.Parameters("O_UF_CIDADE").Value

End Function

' fpiardi - Ci&T - 06/11/2012 - [ISA-965]
' Essa fun��o ir� executar valida��es e buscar a Categoria CFO que ser� enviada para o SAP
Function RetornarCategoriaCFO(Tp_Docto As String, Cnae As String, IE As String) As String
    
    Tp_Docto = Val(Tp_Docto)
    Cnae = Trim(Val(Replace(Replace(Left(Cnae, 9), "-", ""), "/", "")))
    IE = Replace(IE, ".", "")
    
    db.Parameters.Remove "P_TP_DOCTO"
    db.Parameters.Add "P_TP_DOCTO", Tp_Docto, 1
    
    db.Parameters.Remove "P_CNAE"
    db.Parameters.Add "P_CNAE", Cnae, 1
    
    db.Parameters.Remove "P_INSCRICAO_ESTADUAL"
    db.Parameters.Add "P_INSCRICAO_ESTADUAL", IE, 1
    
    db.Parameters.Remove "P_OUT_CAT_CFO"
    db.Parameters.Add "P_OUT_CAT_CFO", "", 2
        
    vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD060.PR_SEL_CATEGORIA_CFO(:P_TP_DOCTO, :P_CNAE, :P_INSCRICAO_ESTADUAL, :P_OUT_CAT_CFO)")
    If vErro <> "" Then
        MsgBox "Erro: " & vErro & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
        Exit Function
    End If

    RetornarCategoriaCFO = Format(db.Parameters("P_OUT_CAT_CFO").Value, "00")
    
End Function


'carlosj CiT [22/11/2012] - ISA 1019
'carrega combo Segmento
Sub Carrega_Combo_Segmento_ClienteDPK(CampoTela As Object, tipoDocumento As Integer)

 Set OraParameters = db.Parameters
    'OraParameters.Remove "TP_CONS": OraParameters.Add "TP_CONS", 0, 1
    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    OraParameters.Remove "TPDOCTO": OraParameters.Add "TPDOCTO", tipoDocumento, 1
   
    Criar_Cursor OraParameters, "vCursor"
    
    vErro = vVB_Generica_001.ExecutaPl(db, "Producao.PCK_CAD060.PR_COMBO_SEGMENTOCLIENTEDPK(:vCursor,:TPDOCTO,:vErro,:vMsgErro)")
    If vErro <> "" Then
        Exit Sub
    End If
    vVB_Generica_001.PreencheComboList OraParameters("vCursor").Value, CampoTela, "Desc_Segmento", "cod_Segmento"

End Sub

'carlosj CiT [22/11/2012] - ISA 1019
'Seta o combo Segmento pelo tipo de cliente
Sub SetarComboTipoSegmento(comboTipoSegmento As ComboBox, tpCliente As String)

  If (Trim(tpCliente) = "") Then Exit Sub ' Se o tpCliente vier nulo sai do metodo
   
  Set OraParameters = db.Parameters
    OraParameters.Remove "P_OUT_SEGMENTO": OraParameters.Add "P_OUT_SEGMENTO", 0, 2
    OraParameters.Remove "PM_ERRO": OraParameters.Add "PM_ERRO", 0, 2
    OraParameters.Remove "PM_MSGERRO": OraParameters.Add "PM_MSGERRO", "", 2
    OraParameters.Remove "P_TIPO_CLIENTE": OraParameters.Add "P_TIPO_CLIENTE", tpCliente, 1
   
    Criar_Cursor OraParameters, "vCursor"
    
    vErro = vVB_Generica_001.ExecutaPl(db, "Producao.PCK_CAD060.PR_SEL_TP_SEGMENTO(:P_OUT_SEGMENTO,:P_TIPO_CLIENTE,:PM_ERRO,:PM_MSGERRO)")
    If vErro = "" Then
   
        Dim nCnt As Long
    
        For nCnt = 0 To comboTipoSegmento.ListCount - 1
            If comboTipoSegmento.List(nCnt) = db.Parameters("P_OUT_SEGMENTO").Value Then
                comboTipoSegmento = comboTipoSegmento.List(nCnt)
                Exit For
            End If
        Next
    End If
End Sub
'TI-3221
Sub Carrega_Combo_Forn(CampoTela As Object)

    Set OraParameters = db.Parameters
    OraParameters.Remove "vCursor": OraParameters.Add "vCursor", 0, 2
    OraParameters("vCursor").ServerType = ORATYPE_CURSOR
    OraParameters("vCursor").DynasetOption = ORADYN_NO_BLANKSTRIP
    OraParameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
    OraParameters.Remove "TP_CONS": OraParameters.Add "TP_CONS", 1, 1
    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    OraParameters("vErro").ServerType = ORATYPE_NUMBER
      
    'Vai fazer select na tabela vdr.desconto_categ, para o campo que eu levar como par�metro, para mostrar na combo
    Sql = "Begin Producao.PCK_CAD060.Pr_Combo_CategBosch(:vCursor,:TP_CONS,:vErro,:vMsgErro); End; "
    db.Executesql Sql
      
    Set db_CONS = OraParameters("vCursor").Value
    If Val(OraParameters("vErro").Value) <> 0 Then
        MsgBox "Erro: " & Val(OraParameters("vErro").Value) & " - " & _
                          OraParameters("vMsgErro").Value & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
        Exit Sub
    End If

    If db_CONS.EOF Then
        MsgBox "Problemas ao consultar os dados para preencher combo de Fornecedor VDR. " & Chr(13) & _
               "Por Favor, avise Depto.Suporte ", vbCritical, "CAD060 - Cadastro de Cliente"
        Exit Sub
    End If

    CampoTela.Clear
    While Not db_CONS.EOF
        CampoTela.AddItem db_CONS!Categoria.Value
        db_CONS.MoveNext
    Wend
    
End Sub
'FIM TI-3221
'TI-6264 /
  Sub PopularObjetoClienteEmails(ByRef oCliente As ClienteSAP, pEmail As String, pCodCliente As Long)
    Dim I As Integer
    Dim arrayComunicacao() As New Comunicacao
    Dim oComunicacao As Comunicacao
    Dim rs  As Object
    Dim sequencia As Integer
    Dim vEmail As String
    
    If Len(Trim(pEmail)) = 0 Then Exit Sub
    
    GravarEmailMarketing pCodCliente, Trim(UCase(pEmail))

    Criar_Cursor db.Parameters, "vCursor"
    
    db.Parameters.Remove "Cod_Cliente"
    db.Parameters.Add "Cod_cliente", pCodCliente, 1
    
    db.Executesql "BEGIN " & strTabela_Banco & "PCK_CAD_EMAIL.PR_SEL_CLIENTE_EMAIL(:vCursor, :Cod_Cliente); END;"
    
    Set rs = db.Parameters("vCursor").Value
    
    'verifica se a lista esta vazia
    If rs.RecordCount = 0 Then
        Exit Sub
    End If
    
    ReDim arrayComunicacao(rs.RecordCount)
    
    For I = 1 To rs.RecordCount
        
        Set oComunicacao = New Comunicacao
        
        vEmail = Trim(IIf(IsNull(rs!Email), "", rs!Email))
        
        If I = 1 Then
            oComunicacao.Preferencial = "S"
        Else
            oComunicacao.Preferencial = "N"
        End If
        
        oComunicacao.TpMail = ""
        oComunicacao.Email = vEmail
        Set arrayComunicacao(I - 1) = oComunicacao
    
        rs.MoveNext
    Next
    
    Set rs = Nothing

    Call oCliente.ArrayComunicacoes(arrayComunicacao)

 End Sub
 
Private Sub GravarEmailMarketing(pCodCliente As Long, pEmail As String)

On Error GoTo Trataerro
    
    If Len(Trim(pEmail)) = 0 Then Exit Sub
    
    db.Parameters.Remove "Cod_Cliente"
    db.Parameters.Remove "Email"
    
    db.Parameters.Add "Cod_Cliente", pCodCliente, 1
    db.Parameters.Add "Email", pEmail, 1
    
    db.Executesql "BEGIN " & strTabela_Banco & "PCK_CAD_EMAIL.PR_INS_CLIENTE_EMAIL(:Cod_Cliente,:Email); END;"
    
Trataerro:

End Sub

Function ValidarEmail(ByVal pEmail As String) As Boolean
        
    Dim Conta As Integer, Flag As Integer, cValido As String
    
    ValidarEmail = False
    
    If Len(pEmail) < 3 Then
        Exit Function
    End If

    
    'Verifica se existe caracter inv�lido
    
    For Conta = 1 To Len(pEmail)
        cValido = Mid(pEmail, Conta, 1)
        If Not (LCase(cValido) Like "[a-z]" Or cValido = _
            "@" Or cValido = "." Or cValido = "-" Or _
            cValido = "_" Or cValido Like "[0-9]") Then
            Exit Function
        End If
    Next
  

    'Verifica a exist�ncia de (@)
    
    If InStr(pEmail, "@") = 0 Then
        Exit Function
    Else
        Flag = 0
        
        For Conta = 1 To Len(pEmail)
            If Mid(pEmail, Conta, 1) = "@" Then
                Flag = Flag + 1
            End If
        Next
        
        If Flag > 1 Then Exit Function
    End If
  
    If Left(pEmail, 1) = "@" Then
        Exit Function
    ElseIf Right(pEmail, 1) = "@" Then
        Exit Function
    ElseIf InStr(pEmail, ".@") > 0 Then
        Exit Function
    ElseIf InStr(pEmail, "@.") > 0 Then
        Exit Function
    End If
  
  
    'Verifica a exist�ncia de (.)
    
    If InStr(pEmail, ".") = 0 Then
        Exit Function
    ElseIf Left(pEmail, 1) = "." Then
        Exit Function
    ElseIf Right(pEmail, 1) = "." Then
        Exit Function
    ElseIf InStr(pEmail, "..") > 0 Then
        Exit Function
    End If
  
    ValidarEmail = True

End Function

'FIM TI-6264 / TI-6488
