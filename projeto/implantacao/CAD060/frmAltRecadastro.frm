VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmAltRecadastro 
   BackColor       =   &H00FFFFFF&
   Caption         =   "CAD060 - An�lise de Cliente - Recadastro"
   ClientHeight    =   6900
   ClientLeft      =   90
   ClientTop       =   1545
   ClientWidth     =   12180
   LinkTopic       =   "Form2"
   ScaleHeight     =   6900
   ScaleWidth      =   12180
   Begin VB.TextBox txtSAPRecadastro 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0E0FF&
      Height          =   285
      Left            =   10920
      Locked          =   -1  'True
      TabIndex        =   22
      Top             =   45
      Width           =   1200
   End
   Begin VB.TextBox txtDtRecadastro 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0E0FF&
      Height          =   285
      Left            =   6270
      Locked          =   -1  'True
      TabIndex        =   19
      Top             =   360
      Width           =   1050
   End
   Begin VB.TextBox txtCep 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0E0FF&
      Height          =   285
      Left            =   5355
      Locked          =   -1  'True
      TabIndex        =   18
      Top             =   360
      Width           =   840
   End
   Begin VB.TextBox txtUF 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0E0FF&
      Height          =   285
      Left            =   4800
      Locked          =   -1  'True
      TabIndex        =   17
      Top             =   360
      Width           =   480
   End
   Begin VB.TextBox txtCidade 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0E0FF&
      Height          =   285
      Left            =   2265
      Locked          =   -1  'True
      TabIndex        =   16
      Top             =   360
      Width           =   2520
   End
   Begin VB.TextBox txtNomeCli 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0E0FF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   2265
      Locked          =   -1  'True
      TabIndex        =   15
      Top             =   45
      Width           =   5055
   End
   Begin VB.Frame Frame4 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Dados da Cobran�a"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3660
      Left            =   0
      TabIndex        =   13
      Top             =   3240
      Width           =   6255
      Begin MSComctlLib.ListView lsvCobranca 
         Height          =   3375
         Left            =   60
         TabIndex        =   14
         Top             =   210
         Width           =   6105
         _ExtentX        =   10769
         _ExtentY        =   5953
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Campo"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Conte�do"
            Object.Width           =   5292
         EndProperty
      End
   End
   Begin VB.Frame Frame3 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Refer�ncias Banc�rias"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1680
      Left            =   6330
      TabIndex        =   11
      Top             =   3240
      Width           =   5580
      Begin MSComctlLib.ListView lsvRefBancarias 
         Height          =   1395
         Left            =   60
         TabIndex        =   12
         Top             =   210
         Width           =   5430
         _ExtentX        =   9578
         _ExtentY        =   2461
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Campo"
            Object.Width           =   3175
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Conte�do"
            Object.Width           =   3528
         EndProperty
      End
   End
   Begin VB.TextBox txtCGC 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0E0FF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   9165
      Locked          =   -1  'True
      TabIndex        =   6
      Top             =   360
      Width           =   1905
   End
   Begin VB.TextBox txtCodCliente 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0E0FF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   9165
      Locked          =   -1  'True
      TabIndex        =   5
      Top             =   45
      Width           =   840
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Campos Alterados"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2565
      Left            =   0
      TabIndex        =   2
      Top             =   660
      Width           =   11925
      Begin MSComctlLib.ListView lsvAlterados 
         Height          =   2295
         Left            =   60
         TabIndex        =   3
         Top             =   195
         Width           =   11790
         _ExtentX        =   20796
         _ExtentY        =   4048
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   3
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Campo"
            Object.Width           =   4410
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "De"
            Object.Width           =   7056
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Para"
            Object.Width           =   7056
         EndProperty
      End
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Refer�ncias Comerciais"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1980
      Left            =   7035
      TabIndex        =   0
      Top             =   4920
      Width           =   4860
      Begin MSComctlLib.ListView lsvRefCom 
         Height          =   1695
         Left            =   60
         TabIndex        =   1
         Top             =   210
         Width           =   4710
         _ExtentX        =   8308
         _ExtentY        =   2990
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Seq"
            Object.Width           =   882
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Empresa"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "DDD"
            Object.Width           =   1235
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Fone"
            Object.Width           =   2117
         EndProperty
      End
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   540
      Left            =   1290
      TabIndex        =   8
      Top             =   15
      Width           =   585
      _ExtentX        =   1032
      _ExtentY        =   953
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmAltRecadastro.frx":0000
      PICN            =   "frmAltRecadastro.frx":001C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdRecusar 
      Height          =   540
      Left            =   675
      TabIndex        =   9
      ToolTipText     =   "Recusar Pr�-Altera��o"
      Top             =   15
      Width           =   585
      _ExtentX        =   1032
      _ExtentY        =   953
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmAltRecadastro.frx":0CF6
      PICN            =   "frmAltRecadastro.frx":0D12
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdGravar 
      Height          =   540
      Left            =   45
      TabIndex        =   10
      ToolTipText     =   "Aceitar Pr�-Altera��o"
      Top             =   15
      Width           =   585
      _ExtentX        =   1032
      _ExtentY        =   953
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmAltRecadastro.frx":15EC
      PICN            =   "frmAltRecadastro.frx":1608
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label lbSAP 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "C�d. SAP:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   10080
      TabIndex        =   21
      Top             =   120
      Width           =   795
   End
   Begin VB.Image imgGrMaster 
      Height          =   195
      Left            =   6795
      Picture         =   "frmAltRecadastro.frx":1922
      Top             =   5160
      Visible         =   0   'False
      Width           =   210
   End
   Begin VB.Label lblGrMaster 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Mesmo Grupo "
      Height          =   450
      Left            =   6270
      TabIndex        =   20
      Top             =   5040
      Visible         =   0   'False
      Width           =   570
   End
   Begin VB.Label lblCGC 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "CGC do Cliente:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   7815
      TabIndex        =   7
      Top             =   435
      Width           =   1290
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "C�digo do Cliente:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   7605
      TabIndex        =   4
      Top             =   120
      Width           =   1515
   End
End
Attribute VB_Name = "frmAltRecadastro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public vFechar As Boolean
Public vFl_Alt_Dados As Byte
Public vFl_Alt_End As Byte
Public vFl_Alt_RefCom As Byte
Public vFl_Alt_RefBco As Byte
Private vEndCobranca As endereco

Private Sub cmdGravar_Click()
    Dim II As Integer
    Dim vUpdate(14, 2) As String 'TI-6488
    Dim objUF As Object
    Dim dadosCliente As Object
    Dim oCliente As ClienteSAP
    Dim msgErro As String
    Dim end_end As String
    Dim end_nro As String
    Dim end_cpl As String
    
    If vVB_Generica_001.ValidaCGC(txtCGC.Text) Then
        Set oCliente = BuscarClienteSAP(1, txtCGC.Text, msgErro)
    ElseIf vVB_Generica_001.ValidaCIC(txtCGC.Text) Then
        Set oCliente = BuscarClienteSAP(2, txtCGC.Text, msgErro)
    End If
    
    If msgErro <> "" Then
        MsgBox msgErro
        Exit Sub
    ElseIf Val(oCliente.NoClienteSAP) = 0 Then
        MsgBox "Esse cliente n�o est� cadastrado no SAP, processo ser� abortado", vbCritical, "CAD060 - Aten��o"
        Exit Sub
    Else
        txtSAPRecadastro.Text = oCliente.NoClienteSAP
    End If
    
    'Se houver altera��es a serem feitas
    If vFl_Alt_Dados = 1 Then
              
        vUpdate(0, 0) = "NOME CLIENTE":                 vUpdate(0, 1) = "BRANCO"
        'vUpdate(1, 0) = "ENDERE�O":                     vUpdate(1, 1) = "BRANCO"
        vUpdate(1, 0) = "ENDERECO":                     vUpdate(1, 1) = "BRANCO"
        vUpdate(2, 0) = "BAIRRO":                       vUpdate(2, 1) = "BRANCO"
        vUpdate(3, 0) = "CEP":                          vUpdate(3, 1) = "BRANCO"
        'vUpdate(4, 0) = "C�DIGO CIDADE":                vUpdate(4, 1) = "BRANCO"
        vUpdate(4, 0) = "CODIGO CIDADE":                vUpdate(4, 1) = "BRANCO"
        vUpdate(5, 0) = "DDD 1":                        vUpdate(5, 1) = "BRANCO"
        vUpdate(6, 0) = "FONE 1":                       vUpdate(6, 1) = "BRANCO"
        vUpdate(7, 0) = "NOME CONTATO":                 vUpdate(7, 1) = "BRANCO"
        'vUpdate(8, 0) = "NOME PROPRIET�RIO":            vUpdate(8, 1) = "BRANCO"
        vUpdate(8, 0) = "NOME PROPRIETARIO":            vUpdate(8, 1) = "BRANCO"
        'vUpdate(9, 0) = "INSCRI��O ESTADUAL":           vUpdate(9, 1) = "BRANCO"
        vUpdate(9, 0) = "INSCRICAO ESTADUAL":           vUpdate(9, 1) = "BRANCO"
        'vUpdate(10, 0) = "C�DIGO DO REPRESENTANTE":     vUpdate(10, 1) = "BRANCO"
        vUpdate(10, 0) = "CODIGO REPRESENTANTE":     vUpdate(10, 1) = "BRANCO"
        'vUpdate(11, 0) = "C�DIGO MENSAGEM FISCAL":      vUpdate(11, 1) = "BRANCO"
        vUpdate(11, 0) = "CODIGO MENSAGEM FISCAL":      vUpdate(11, 1) = "BRANCO"
        vUpdate(12, 0) = "CONSUMIDOR FINAL":            vUpdate(12, 1) = "BRANCO"
        vUpdate(13, 0) = "TP_EMPRESA":                  vUpdate(13, 1) = "BRANCO"
        vUpdate(14, 0) = "EMAIL":                      vUpdate(14, 1) = "BRANCO" 'TI-6488
    
        For I = 1 To lsvAlterados.ListItems.Count
            If lsvAlterados.ListItems(I).Checked = True Then
            
                If UCase(lsvAlterados.ListItems(I)) = "CONTATO" Then
                    oCliente.ContatoPrincipal.Nome = lsvAlterados.ListItems(I).SubItems(2)
                End If
            
                For II = 0 To UBound(vUpdate)
                    Select Case UCase(lsvAlterados.ListItems(I))
                        Case vUpdate(II, 0)
                             vUpdate(II, 1) = lsvAlterados.ListItems(I).SubItems(2)
                             'Kevlin Toshinari Ossada - CIT - 22/8/2012 - Instancia objeto cliente para gravar no SAP
                             Select Case II
                                Case 0
                                    oCliente.Name1 = IIf(Trim(lsvAlterados.ListItems(I).SubItems(2)) = "", "", lsvAlterados.ListItems(I).SubItems(2)) 'NOME CLIENTE
                                Case 1
                                    If Trim(lsvAlterados.ListItems(I).SubItems(2)) <> "" Then
                                        Call DesconcatenarEnderecoSAP(CStr(lsvAlterados.ListItems(I).SubItems(2)), end_end, end_nro, end_cpl)
                                        oCliente.EnderecoPrincipal.rua = end_end
                                        oCliente.EnderecoPrincipal.NoResidencia = end_nro
                                        oCliente.EnderecoPrincipal.complemento = end_cpl
                                    End If
                                Case 2
                                    oCliente.EnderecoPrincipal.Bairro = IIf(Trim(lsvAlterados.ListItems(I).SubItems(2)) = "", "", lsvAlterados.ListItems(I).SubItems(2)) 'BAIRRO
                                Case 3
                                    oCliente.EnderecoPrincipal.CEP = IIf(Trim(lsvAlterados.ListItems(I).SubItems(2)) = "", "", lsvAlterados.ListItems(I).SubItems(2)) 'CEP
                                Case 4
                                    oCliente.EnderecoPrincipal.Cidade = BuscaCidadePorCodigoCidade(lsvAlterados.ListItems(I).SubItems(2)) 'CODIGO CIDADE COBRANCA
                                    oCliente.EnderecoPrincipal.Regiao = BuscaUfPorCodigoCidade(lsvAlterados.ListItems(I).SubItems(2)) 'CODIGO CIDADE
                                Case 5
                                    oCliente.EnderecoPrincipal.DDDFax = IIf(Trim(lsvAlterados.ListItems(I).SubItems(2)) = "", "", lsvAlterados.ListItems(I).SubItems(2))  'DDD 1
                                Case 6
                                    oCliente.EnderecoPrincipal.Telefone = IIf(Trim(lsvAlterados.ListItems(I).SubItems(2)) = "", "", lsvAlterados.ListItems(I).SubItems(2))  'FONE 1
                                Case 7
                                    oCliente.ContatoPrincipal.Nome = IIf(Trim(lsvAlterados.ListItems(I).SubItems(2)) = "", "", lsvAlterados.ListItems(I).SubItems(2)) 'NOME CONTATO
                                Case 8
                                    'NOME PROPRIETARIO
                                Case 9
                                    oCliente.NoIDFiscal3 = IIf(Trim(lsvAlterados.ListItems(I).SubItems(2)) = "", "", lsvAlterados.ListItems(I).SubItems(2)) 'INSCRICAO ESTADUAL
                                Case 10
                                    'CODIGO REPRESENTANTE
                                Case 11
                                    oCliente.FISCAL.MsgFiscal1 = IIf(Trim(lsvAlterados.ListItems(I).SubItems(2)) = "", "", lsvAlterados.ListItems(I).SubItems(2)) 'CODIGO MENSAGEM FISCAL
                                Case 12
                                    'CONSUMIDOR FINAL
                                Case 13
                                    oCliente.FISCAL.TpEmpresa = IIf(Trim(lsvAlterados.ListItems(I).SubItems(2)) = "", "", lsvAlterados.ListItems(I).SubItems(2)) 'TP_EMPRESA
                                End Select
                             Exit For
                    End Select
                Next II
            End If
        Next I
        
        If oCliente.ContatoPrincipal.Nome = "" Then
          oCliente.ContatoPrincipal.Nome = Mid(oCliente.Name1, 1, 15)
        End If
        
        ' Endereco de Cobranca
        If vFl_Alt_End = 1 Then
        
            If (oCliente.EnderecoCobranca Is Nothing) Then
                oCliente.EnderecoCobranca = vEndCobranca
            End If
            
            If Trim(lsvCobranca.ListItems(6).SubItems(1)) <> "" And lsvCobranca.ListItems(6).Checked Then
                 Call DesconcatenarEnderecoSAP(CStr(lsvCobranca.ListItems(6).SubItems(1)), end_end, end_nro, end_cpl)
                 oCliente.EnderecoCobranca.rua = end_end
                 oCliente.EnderecoCobranca.NoResidencia = end_nro
                 oCliente.EnderecoCobranca.complemento = end_cpl
            End If
            
            If Trim(lsvCobranca.ListItems(7).SubItems(1)) <> "" And lsvCobranca.ListItems(7).Checked Then
                oCliente.EnderecoCobranca.Bairro = Trim(lsvCobranca.ListItems(7).SubItems(1))
            End If
            
            If Trim(lsvCobranca.ListItems(8).SubItems(1)) <> "" And lsvCobranca.ListItems(8).Checked Then
               oCliente.EnderecoCobranca.CEP = Trim(lsvCobranca.ListItems(8).SubItems(1))
            End If
            
            If Trim(lsvCobranca.ListItems(5).SubItems(1)) <> "" And lsvCobranca.ListItems(5).Checked Then
                oCliente.EnderecoCobranca.Cidade = BuscaCidadePorCodigoCidade(lsvCobranca.ListItems(5).SubItems(1))
                oCliente.EnderecoCobranca.Regiao = BuscaUfPorCodigoCidade(lsvCobranca.ListItems(5).SubItems(1))
            End If

        End If
        
        'ecampos 08/11/2012 [ISA-966] - Consulta no BD do legado para preencher os campos Setor e CatgCFOP
        db.Parameters.Remove "P_CGC"
        db.Parameters.Add "P_CGC", txtCGC.Text, 1
        Criar_Cursor db.Parameters, "P_CURSOR"
        
        vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD060.PR_SEL_INFOCLIENTE(:P_CGC, :P_CURSOR)")
        If vErro <> "" Then
            Exit Sub
        End If
        Set dadosCliente = db.Parameters("P_CURSOR").Value
        
        oCliente.Setor = RetornarSetorIndustrial(IIf(IsNull(dadosCliente!Tp_Docto), "", dadosCliente!Tp_Docto), txtUF.Text, IIf(IsNull(dadosCliente!cod_cnae), "", dadosCliente!cod_cnae), txtCGC.Text, IIf(IsNull(dadosCliente!cod_tare), "", dadosCliente!cod_tare), IIf(IsNull(oCliente.NoIDFiscal3), "", oCliente.NoIDFiscal3), IIf(IsNull(oCliente.Tipo), "", oCliente.Tipo))
        oCliente.FISCAL.CatgCFOP = RetornarCategoriaCFO(IIf(IsNull(dadosCliente!Tp_Docto), "", dadosCliente!Tp_Docto), IIf(IsNull(dadosCliente!cod_cnae), "", dadosCliente!cod_cnae), IIf(IsNull(oCliente.NoIDFiscal3), "", oCliente.NoIDFiscal3))
        
        'TI-6488
        If oCliente.EnderecoPrincipal.Telefone = "" Then
          oCliente.EnderecoPrincipal.Telefone = "0"
        End If
        If vUpdate(14, 1) <> "BRANCO" And (ValidarEmail(vUpdate(14, 1)) = True) Then
            Call PopularObjetoClienteEmails(oCliente, vUpdate(14, 1), Val(txtCodCliente))
        End If
        'FIM TI-6488
        
        'Kevlin Toshinari Ossada - CIT - 22/8/2012 - Tenta enviar cliente para o SAP
        If Not EnviarClienteSAP(oCliente) Then
            MsgBox "Erro ao gravar o cliente no SAP. Cadastro Abortado.", vbCritical, "CAD060 - Aten��o"
            Exit Sub
        End If
        
        'Os parametros devem vir na ordem que est�o.
        
        'Adicionar o Parametro com o Codigo do Cliente
        db.Parameters.Remove "CodCli"
        db.Parameters.Add "CodCli", txtCodCliente, 1
        
        'Criar parametros para efetuar UPDATE
        For I = 0 To UBound(vUpdate())
            vUpdate(I, 0) = Replace(vUpdate(I, 0), "-", "")
            db.Parameters.Remove Replace(vUpdate(I, 0), " ", "_")
            db.Parameters.Add Replace(vUpdate(I, 0), " ", "_"), vUpdate(I, 1), 1
        Next I
        
        'Data de Hoje
        db.Parameters.Remove "Data"
        db.Parameters.Add "Data", vDtHoje, 1
        
        'Adicionar os Parametros de Erro
        db.Parameters.Remove "CodErro"
        db.Parameters.Add "CodErro", 0, 2
        
        db.Parameters.Remove "TxtErro"
        db.Parameters.Add "TxtErro", "", 2
        
        
        '*******************************************************************************
        'Conf.solic.Fiscal, qdo. cliente for de SC, msg.fiscal deve ser 53 pois n�o pode ter Antec.Tribut�ria - Alethea - 30/01/07
        'If db.Parameters("C�DIGO_CIDADE") <> "BRANCO" Then
        If db.Parameters("CODIGO_CIDADE") <> "BRANCO" Then
            'db.Parameters.Remove "CODCID": db.Parameters.Add "CODCID", db.Parameters("C�DIGO_CIDADE"), 1
            db.Parameters.Remove "CODCID": db.Parameters.Add "CODCID", db.Parameters("CODIGO_CIDADE"), 1
            Sql = "Select cod_uf " & _
                    "FROM Producao.Cidade " & _
                   "WHERE cod_cidade = :CODCID"
            Set objUF = db.dbcreatedynaset(Sql, 0&)
            If objUF!COD_UF = "SC" Then
                'db.Parameters.Remove "C�DIGO_MENSAGEM_FISCAL"
                'db.Parameters.Add "C�DIGO_MENSAGEM_FISCAL", 53, 1
                db.Parameters.Remove "CODIGO_MENSAGEM_FISCAL"
                db.Parameters.Add "CODIGO_MENSAGEM_FISCAL", 53, 1
            End If
        End If
        
        'Executar a Procedure para Update
        'vSql = "BEGIN PRODUCAO.PCK_CLIENTE.Pr_Update_Recadastro (" & _
                      ":CodCli, :NOME_CLIENTE, :ENDERE�O, :BAIRRO, :CEP, " & _
                      ":C�DIGO_CIDADE, :DDD_1, :FONE_1, :NOME_CONTATO, " & _
                      ":NOME_PROPRIET�RIO, :INSCRI��O_ESTADUAL, :C�DIGO_DO_REPRESENTANTE, " & _
                      ":C�DIGO_MENSAGEM_FISCAL, :CONSUMIDOR_FINAL, :TP_EMPRESA, :DATA, " & _
                      ":CodErro, :TxtErro); END;"
        'db.Executesql vSql
        
        'If Val(IIf(IsNull(db.Parameters("CodErro")), 0, db.Parameters("CodErro"))) <> 0 Then
        '    MsgBox "C�digo do Erro: " & db.Parameters("CodErro") & _
        '           "Descri��o: " & db.Parameters("TxtErro")
        '    Exit Sub
        'End If
        'vSql = "PRODUCAO.PCK_CLIENTE.Pr_Update_Recadastro (" & _
               ":CodCli, :NOME_CLIENTE, :ENDERE�O, :BAIRRO, :CEP, " & _
               ":C�DIGO_CIDADE, :DDD_1, :FONE_1, :NOME_CONTATO, " & _
               ":NOME_PROPRIET�RIO, :INSCRI��O_ESTADUAL, :C�DIGO_DO_REPRESENTANTE, " & _
               ":C�DIGO_MENSAGEM_FISCAL, :CONSUMIDOR_FINAL, :TP_EMPRESA, :DATA, " & _
               ":CodErro, :TxtErro)"
        vSql = "PRODUCAO.PCK_CLIENTE.Pr_Update_Recadastro (" & _
               ":CodCli, :NOME_CLIENTE, :ENDERECO, :BAIRRO, :CEP, " & _
               ":CODIGO_CIDADE, :DDD_1, :FONE_1, :NOME_CONTATO, " & _
               ":NOME_PROPRIETARIO, :INSCRICAO_ESTADUAL, :CODIGO_REPRESENTANTE, " & _
               ":CODIGO_MENSAGEM_FISCAL, :CONSUMIDOR_FINAL, :TP_EMPRESA, :DATA, " & _
               ":CodErro, :TxtErro)"
        If vVB_Generica_001.ExecutaPl(db, vSql) <> "" Then
            Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
            Screen.MousePointer = 0
            Exit Sub
        End If
        
        
    End If
    
    
        'Adicionar o Parametro com o Codigo do Cliente
        db.Parameters.Remove "CodCli": db.Parameters.Add "CodCli", txtCodCliente, 1
    
    'Executar Procedure para Inserir as Referencias Banc�rias
    If vFl_Alt_RefBco = 1 Then
        db.Parameters.Remove "Banco":   db.Parameters.Add "Banco", lsvRefBancarias.ListItems(1).SubItems(1), 1
        db.Parameters.Remove "Agencia": db.Parameters.Add "Agencia", lsvRefBancarias.ListItems(2).SubItems(1), 1
        db.Parameters.Remove "DDD":     db.Parameters.Add "DDD", lsvRefBancarias.ListItems(3).SubItems(1), 1
        db.Parameters.Remove "FONE":    db.Parameters.Add "FONE", lsvRefBancarias.ListItems(4).SubItems(1), 1
        db.Parameters.Remove "DTCONS": db.Parameters.Add "DTCONS", IIf(Trim(vDtHoje) = "", Null, vDtHoje), 1
        db.Parameters.Remove "CodErro": db.Parameters.Add "CodErro", 0, 2
        db.Parameters.Remove "TxtErro": db.Parameters.Add "TxtErro", "", 2
        
        'vSql = "BEGIN PRODUCAO.PCK_CLIENTE2.Pr_Update_Recadastro_Ref_Bco (" & _
                ":CodCli, " & _
                ":BANCO, " & _
                ":AGENCIA, " & _
                ":DDD, " & _
                ":FONE, " & _
                "to_date(:DTCONS,'dd/mm/rr'), " & _
                ":CodErro, " & _
                ":TxtErro); END;"
            
        vSql = "BEGIN PRODUCAO.PCK_CLIENTE.Pr_Update_Recadastro_Ref_Bco (" & _
                ":CodCli, " & _
                ":BANCO, " & _
                ":AGENCIA, " & _
                ":DDD, " & _
                ":FONE, " & _
                "to_date(:DTCONS,'dd/mm/rr'), " & _
                ":CodErro, " & _
                ":TxtErro); END;"
            
            
        db.Executesql vSql
        
        If Val(IIf(IsNull(db.Parameters("CodErro")), 0, db.Parameters("CodErro"))) <> 0 Then
           MsgBox "C�digo do Erro: " & db.Parameters("CodErro") & _
                  "Descri��o: " & db.Parameters("TxtErro")
           Exit Sub
        End If
    End If
    
    
    'Executar Procedure para Inserir os Dados de Cobranca
    If vFl_Alt_End = 1 Then
    
        'Inserida consist�ncia para n�o permitir que seja inclu�do um endere�o em branco - Alethea - 18/05/05
        If Trim(lsvCobranca.ListItems(6).SubItems(1)) = "" Then
            MsgBox "Endere�o de cobran�a n�o pode ser branco. " & Chr(15) & _
                   "Este registro de outros endere�os, n�o ser� gravado!", vbCritical, "CAD060 - Aten��o"
        Else
                    
            db.Parameters.Remove "TipoDoctoCob": db.Parameters.Add "TipoDoctoCob", lsvCobranca.ListItems(1).SubItems(1), 1
            db.Parameters.Remove "CGCCob": db.Parameters.Add "CGCCob", lsvCobranca.ListItems(2).SubItems(1), 1
            db.Parameters.Remove "InscrEstCob": db.Parameters.Add "InscrEstCob", lsvCobranca.ListItems(3).SubItems(1), 1
            db.Parameters.Remove "NomeCliCob": db.Parameters.Add "NomeCliCob", lsvCobranca.ListItems(4).SubItems(1), 1
            db.Parameters.Remove "CodCidadeCob": db.Parameters.Add "CodCidadeCob", lsvCobranca.ListItems(5).SubItems(1), 1
            db.Parameters.Remove "EnderecoCob": db.Parameters.Add "EnderecoCob", lsvCobranca.ListItems(6).SubItems(1), 1
            db.Parameters.Remove "BairroCob": db.Parameters.Add "BairroCob", lsvCobranca.ListItems(7).SubItems(1), 1
            db.Parameters.Remove "CEPCob": db.Parameters.Add "CEPCob", lsvCobranca.ListItems(8).SubItems(1), 1
            db.Parameters.Remove "CodErro": db.Parameters.Add "CodErro", 0, 2
            db.Parameters.Remove "TxtErro": db.Parameters.Add "TxtErro", "", 2
            
            vSql = "BEGIN PRODUCAO.PCK_CLIENTE.Pr_Update_Recadastro_Cobranca (" & _
                    ":CodCli, " & _
                    ":TipoDoctoCob, " & _
                    ":CGCCob, " & _
                    ":InscrEstCob, " & _
                    ":NomeCliCob, " & _
                    ":CodCidadeCob, " & _
                    ":EnderecoCob, " & _
                    ":BairroCob, " & _
                    ":CEPCob, " & _
                    ":CodErro, " & _
                    ":TxtErro); END;"
                
            db.Executesql vSql
            
            If Val(IIf(IsNull(db.Parameters("CodErro")), 0, db.Parameters("CodErro"))) <> 0 Then
               MsgBox "C�digo do Erro: " & db.Parameters("CodErro") & _
                      "Descri��o: " & db.Parameters("TxtErro")
               Exit Sub
            End If
        End If
    
    End If
    
    'Executar Procedure para Inserir as Referencias Comerciais
    If vFl_Alt_RefCom = 1 Then
    
        For I = 1 To lsvRefCom.ListItems.Count
            'Vai tratar diferente a 1a. ref.coml., caso o cliente seja de mesmo grupo
            If I = 1 And lblGrMaster.Visible = True And imgGrMaster.Visible = True Then
                db.Parameters.Remove "Empresa": db.Parameters.Add "Empresa", "OBS", 1
                db.Parameters.Remove "OBS": db.Parameters.Add "OBS", "MESMO GRUPO DE " & lsvRefCom.ListItems(I).SubItems(1), 1
            Else
                db.Parameters.Remove "Empresa": db.Parameters.Add "Empresa", lsvRefCom.ListItems(I).SubItems(1), 1
                db.Parameters.Remove "OBS": db.Parameters.Add "OBS", Null, 1
            End If
            db.Parameters.Remove "DDD": db.Parameters.Add "DDD", lsvRefCom.ListItems(I).SubItems(2), 1
            db.Parameters.Remove "FONE": db.Parameters.Add "FONE", lsvRefCom.ListItems(I).SubItems(3), 1
            db.Parameters.Remove "DTCONS": db.Parameters.Add "DTCONS", IIf(Trim(vDtHoje) = "", Null, vDtHoje), 1
            db.Parameters.Remove "CodErro": db.Parameters.Add "CodErro", 0, 2
            db.Parameters.Remove "TxtErro": db.Parameters.Add "TxtErro", "", 2
            
            'Mudar de pck_cliente2 para pck_cliente
            'vSql = "BEGIN PRODUCAO.PCK_CLIENTE2.Pr_Insert_Clie_Refer (" & _
                   ":CodCli, " & _
                   ":EMPRESA, " & _
                   ":DDD, " & _
                   ":FONE, " & _
                   ":OBS, " & _
                   "To_Date(:DTCONS,'dd/mm/rr'), " & _
                   ":CodErro, " & _
                   ":TxtErro); END;"
            
            vSql = "BEGIN PRODUCAO.PCK_CLIENTE.Pr_Insert_Clie_Refer (" & _
                   ":CodCli, " & _
                   ":EMPRESA, " & _
                   ":DDD, " & _
                   ":FONE, " & _
                   ":OBS, " & _
                   "To_Date(:DTCONS,'dd/mm/rr'), " & _
                   ":CodErro, " & _
                   ":TxtErro); END;"
            
            
            db.Executesql vSql
        
            If Val(IIf(IsNull(db.Parameters("CodErro")), 0, db.Parameters("CodErro"))) <> 0 Then
                MsgBox "C�digo do Erro: " & db.Parameters("CodErro") & _
                   "Descri��o: " & db.Parameters("TxtErro")
                Exit Sub
            End If
        Next
        
        'Remover os parametros
        For I = 0 To UBound(vUpdate())
            db.Parameters.Remove Replace(vUpdate(I, 0), " ", "_")
        Next I
    End If
    
    
    '**************************************************************
    'Se o Codigo do Representante da Producao.Cliente for Diferente
    'do Codigo do Representante da Intranet.Cliente_web
    'Enviar Email para o SPC
    '**************************************************************
EnviarEmail:
    For I = 1 To lsvAlterados.ListItems.Count
        'If UCase(lsvAlterados.ListItems(I)) = UCase("C�digo do Representante") Then
        If UCase(lsvAlterados.ListItems(I)) = UCase("Codigo Representante") Then
            If lsvAlterados.ListItems(I).SubItems(1) <> lsvAlterados.ListItems(I).SubItems(2) Then
               Dim Texto As String
               Texto = "Solicita��o de Alteracao de Representante feita por " & vUserRem_Analisado & vbCrLf & vbCrLf & _
                       "Data da Solicita��o : " & Day(Date) & "/" & Month(Date) & "/" & Year(Date) & " as " & FormatDateTime(Time, 4) & vbCrLf & vbCrLf & "Nao responder esta mensagem." & vbCrLf & vbCrLf & _
                       "Codigo Atual        : " & lsvAlterados.ListItems(I).SubItems(1) & vbCrLf & _
                       "Codigo Novo         : " & lsvAlterados.ListItems(I).SubItems(2) & vbCrLf & _
                       "Codigo do Cliente   : " & txtCodCliente & vbCrLf & _
                       "CGC do Cliente      : " & vCGC_Analisado
            End If
            'If vVB_Generica_001.EnviarEmail(db, "Consultor7@maxxipel.com.br", "Alteracao do Representante", Texto) = False Then
            'vVB_Generica_001.EnviarEmail db, "alethea@maxxipel.com.br", "Alteracao do Representante", Texto
            vVB_Generica_001.EnviarEmail db_INT, "spc@dpk.com.br", "Alteracao do Representante", Texto
            Exit For
        End If
     Next

    Call pGravaControleWeb            'Gravar controle de cliente na web, quando pr�-recadastro aceito


'Enviar e-mail ao usu�rio solicitante, retornando a situa��o de sua solicita��o
EnviarEmailConf:

    Dim vAuxTexto As String
    'Seleciona C�d.Usu�rio da Controle_Cliente_Web e seu endereco de e-mail
    Set OraParameters = db_INT.Parameters
    OraParameters.Remove "CGC": OraParameters.Add "CGC", Val(vCGC_Analisado), 1
    OraParameters.Remove "TP_SOL": OraParameters.Add "TP_SOL", vFl_Tipo_Solicitacao, 1
    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    Criar_Cursor OraParameters, "PM_CURSOR1"
    vErro = vVB_Generica_001.ExecutaPl(db_INT, "Intranet.PCK_CLIENTES_TESTE.Pr_Sel_UserIntranet(:PM_CURSOR1, :CGC, :TP_SOL, :vErro, :vMsgErro)")
    If vErro = "" Then
        Set db_CONS = OraParameters("PM_CURSOR1").Value
    Else
        Exit Sub
    End If

    If vFl_Tipo_Solicitacao = 0 Then
        vAuxTexto = "Pr�-Cadastro"
    ElseIf vFl_Tipo_Solicitacao = 1 Then
        vAuxTexto = "Pr�-Altera��o"
    ElseIf vFl_Tipo_Solicitacao = 2 Then
        vAuxTexto = "Pr�-Recadastro"
    End If
    
    Texto = "Solicitacao de " & vAuxTexto & " para o CGC " & vCGC_Analisado & " foi aceita." & vbCrLf & vbCrLf & _
            "Data da Solicitacao   : " & Format(db_CONS!Dt_Solicitacao, "DD/MM/YY HH:MM:SS") & vbCrLf & vbCrLf & _
            "Data da Analise       : " & Format(db_CONS!Dt_Analise, "DD/MM/YY HH:MM:SS") & vbCrLf & vbCrLf & _
            "Favor nao responder esta mensagem." & vbCrLf & vbCrLf
    
    'PARA ALTERACAO DE FICHA ENVIADA PELO DPKNET, N�O PRECISA ENVIAR E-MAIL DE CONFIRMA��O DE ACEITE - ALETHEA - 06/09/07
    'If db_CONS!COD_USUARIO <> 1743 Then        'DESENV
    If db_CONS!cod_usuario <> 1905 Then
        'vVB_Generica_001.EnviarEmail db_INT, db_CONS!E_Mail, "Retorno de sua Solicita��o", Texto
        vVB_Generica_001.EnviarEmail db_INT, db_CONS!E_Mail, vAuxTexto & " ACEITO!!! - CGC " & vCGC_Analisado, Texto
    End If


    MsgBox "Recadastro OK!", vbInformation, "CAD060 - Cadastro de Cliente"
    Call cmdSair_Click

End Sub

Private Sub cmdRecusar_Click()
    'Alterar situa��o, data de an�lise e cod_usuario_credito na Controle_Cliente_Web
    'Deletar da tabela Cliente_Web
    frmMotivoRecusa.Show vbModal
    If vFechar = True Then
          Call cmdSair_Click
    End If
End Sub

Private Sub cmdSair_Click()
    vFl_Volta_Analise = True
    Unload Me
End Sub

Private Sub Form_Load()
    Dim litem As ListItem
    Dim rst2 As Object
    Dim rst3 As Object
    Dim II As Integer
    Dim Vlr_Campo_Prod As String
    Dim Vlr_Campo_Web As String
    Dim end_end As String
    Dim end_nro As String
    Dim end_cpl As String
    
    
    'Ocultar a barra de botoes do MDI
    mdiCAD060.SSPanel1.Visible = False
    
    Me.Top = mdiCAD060.Menu.Height * 2
    Me.Left = 50
    
    'Inicializa vari�veis, para saber se existir�o informa��es a serem alteradas/gravadas
    vFl_Alt_Dados = 0
    vFl_Alt_End = 0
    vFl_Alt_RefCom = 0
    vFl_Alt_RefBco = 0

    lblGrMaster.Visible = False
    imgGrMaster.Visible = False
    
    'Pegar todos os campos da tabela da Producao
    db_INT.Parameters.Remove "CGC": db_INT.Parameters.Add "CGC", Val(vCGC_Analisado), 1
    db.Parameters.Remove "CGC": db.Parameters.Add "CGC", Val(vCGC_Analisado), 1
    Criar_Cursor db.Parameters, "cCursor1"
    'Mudar de pck_cad0602 para pck_cad060
    'vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD0602.PR_SELECT_CLIENTE_RECAD_VB(:cCursor1, :CGC)")
    vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD060.PR_SELECT_CLIENTE_RECAD_VB(:cCursor1, :CGC)")
    If vErro <> "" Then
        Exit Sub
    End If
    Set rst = db.Parameters("cCURSOR1").Value

    'Pegar os Campos Alterados na Cliente_Web
    Criar_Cursor db_INT.Parameters, "cALTERADOS"
    Criar_Cursor db_INT.Parameters, "cALTREFBCO"
    Criar_Cursor db_INT.Parameters, "cALTCOBRAN"
    
    'vVB_Generica_001.Retorna_Cursor db_INT, "cALTERADOS", "INTRANET.PCK_CLIENTES_TESTE.PR_SELECT_CLIENTE_RECAD_VB_ALT", "CGC"
    'Voltar pck_cliente_teste2 para pck_cliente_teste
    vErro = vVB_Generica_001.ExecutaPl(db_INT, "INTRANET.PCK_CLIENTES_TESTE.PR_SELECT_CLIENTE_RECAD_VB_ALT(:cALTERADOS, :cALTREFBCO, :cALTCOBRAN, :CGC)")
    If vErro <> "" Then
        Exit Sub
    End If
    Set rst2 = db_INT.Parameters("cALTERADOS").Value
    
    If rst2("GrMaster").Value = "S" Then
        lblGrMaster.Visible = True
        imgGrMaster.Visible = True
    End If
    
    
    'Pegar os Campos Alterados na Cliente_Web das Referencias Comerciais
    Criar_Cursor db_INT.Parameters, "cREFCOM"
    'vVB_Generica_001.Retorna_Cursor db_INT, "cREFCOM", "INTRANET.PCK_CLIENTES_TESTE.PR_SELECT_CLIENTE_RECAD_REFCOM", "CGC"
    vErro = vVB_Generica_001.ExecutaPl(db_INT, "INTRANET.PCK_CLIENTES_TESTE.PR_SELECT_CLIENTE_RECAD_REFCOM(:cREFCOM, :CGC)")
    If vErro <> "" Then
        Exit Sub
    End If
    Set rst3 = db_INT.Parameters("cREFCOM").Value
    
    'Pegar o codigo do cliente
    txtCodCliente = rst("Codigo Cliente")
    txtCGC = vCGC_Analisado
    vCodCli_Analisado = txtCodCliente
    txtNomeCli = vNome_Analisado
    txtDtRecadastro = IIf(IsNull(rst("Dt_Recadastro")), "", rst("Dt_Recadastro"))
    txtCidade = IIf(IsNull(rst("Nome Cidade")), "", rst("Nome Cidade"))
    txtUF = IIf(IsNull(rst("Codigo UF")), "", rst("Codigo UF"))
    txtCep = IIf(IsNull(rst("Cep")), "", rst("Cep"))
    
    'Preencher o List View com os campos que sofreram altera��es
    For I = 0 To rst2.Fields.Count - 1
        If UCase(rst2.Fields(I).Name) <> "CGC" Then
            
            'If rst2.fields(I).Name = "C�digo Mensagem Fiscal" Then
            '    MsgBox ""
            'End If
            
            
            For II = 0 To rst.Fields.Count - 1
                If UCase(rst2.Fields(I).Name) = UCase(rst.Fields(II).Name) Then
                    Vlr_Campo_Prod = UCase(IIf(IsNull(rst.Fields(II).Value), "", rst.Fields(II).Value))
                    Vlr_Campo_Web = UCase(IIf(IsNull(rst2.Fields(I).Value), "", rst2.Fields(I).Value))
                    If Vlr_Campo_Web <> Vlr_Campo_Prod Then
                       If Vlr_Campo_Web <> "" And (Vlr_Campo_Prod <> "0" Or Trim(Vlr_Campo_Prod) <> "") Then
                          Set litem = lsvAlterados.ListItems.Add
                          litem = rst.Fields(II).Name
                          litem.SubItems(1) = IIf(IsNull(rst.Fields(II).Value), "", rst.Fields(II).Value)
                          litem.SubItems(2) = IIf(IsNull(rst2.Fields(I).Value), "", rst2.Fields(I).Value)
                          litem.Checked = True
                          vFl_Alt_Dados = 1
                       End If
                    Else
                        Exit For
                    End If
                End If
            Next
        End If
    Next
    
    'Preencher o List View com as Referencias Bancarias
    Set rst = db_INT.Parameters("cALTREFBCO").Value
    If Not IsNull(rst.Fields(0)) Then
        vFl_Alt_RefBco = 1
                
        Set litem = lsvRefBancarias.ListItems.Add
        litem = "Nome do Banco"
        litem.SubItems(1) = IIf(IsNull(rst.Fields(0)), "", rst.Fields(0))
        litem.Checked = True
        
        Set litem = lsvRefBancarias.ListItems.Add
        litem = "Ag�ncia"
        litem.SubItems(1) = IIf(IsNull(rst.Fields(1)), "", rst.Fields(1))
        litem.Checked = True
        
        Set litem = lsvRefBancarias.ListItems.Add
        litem = "DDD"
        litem.SubItems(1) = IIf(IsNull(rst.Fields(2)), "", rst.Fields(2))
        litem.Checked = True
        
        Set litem = lsvRefBancarias.ListItems.Add
        litem = "Fone"
        litem.SubItems(1) = IIf(IsNull(rst.Fields(3)), "", rst.Fields(3))
        litem.Checked = True
    End If
    
    'Preencher o List View com os Dados de Cobranca
    Set rst = db_INT.Parameters("cALTCOBRAN").Value
    
    If Not IsNull(rst("TP_DOCTO_COB")) Then
        vFl_Alt_End = 1
        
        ' fpallini - CiT - 02/04/13 - Carregar endereco de cobranca na memoria
        Set vEndCobranca = New endereco
      
        Set litem = lsvCobranca.ListItems.Add
        litem = "Tipo Documento"
        litem.SubItems(1) = IIf(IsNull(rst("TP_DOCTO_COB")), "", rst("TP_DOCTO_COB"))
        litem.Checked = True
                                 
        Set litem = lsvCobranca.ListItems.Add
        litem = "CGC"
        litem.SubItems(1) = IIf(IsNull(rst("CGC_COB")), "", rst("CGC_COB"))
        litem.Checked = True
                                 
        Set litem = lsvCobranca.ListItems.Add
        litem = "Inscri��o Estadual"
        litem.SubItems(1) = IIf(IsNull(rst("INSCR_ESTADUAL_COB")), "", rst("INSCR_ESTADUAL_COB"))
        litem.Checked = True
                                 
        Set litem = lsvCobranca.ListItems.Add
        litem = "Nome Cliente"
        litem.SubItems(1) = IIf(IsNull(rst("NOME_CLIENTE_COB")), "", rst("NOME_CLIENTE_COB"))
        litem.Checked = True
                                 
        Set litem = lsvCobranca.ListItems.Add
        litem = "C�digo Cidade"
        litem.SubItems(1) = IIf(IsNull(rst("COD_CIDADE_COB")), "", rst("COD_CIDADE_COB"))
        litem.Checked = True
        vEndCobranca.Cidade = BuscaCidadePorCodigoCidade(litem.SubItems(1))
        vEndCobranca.Regiao = BuscaUfPorCodigoCidade(litem.SubItems(1))
                                 
        Set litem = lsvCobranca.ListItems.Add
        litem = "Endere�o"
        litem.SubItems(1) = IIf(IsNull(rst("ENDERECO_COB")), "", rst("ENDERECO_COB"))
        litem.Checked = True
        Call DesconcatenarEnderecoSAP(CStr(litem.SubItems(1)), end_end, end_nro, end_cpl)
        vEndCobranca.rua = end_end
        vEndCobranca.NoResidencia = end_nro
        vEndCobranca.complemento = end_cpl
                             
        Set litem = lsvCobranca.ListItems.Add
        litem = "Bairro"
        litem.SubItems(1) = IIf(IsNull(rst("BAIRRO_COB")), "", rst("BAIRRO_COB"))
        litem.Checked = True
        vEndCobranca.Bairro = litem.SubItems(1)
                               
        Set litem = lsvCobranca.ListItems.Add
        litem = "CEP"
        litem.SubItems(1) = IIf(IsNull(rst("CEP_COB")), "", rst("CEP_COB"))
        litem.Checked = True
        vEndCobranca.CEP = litem.SubItems(1)
        
    End If
                           

    'Preencher o List View com as Referencias Comerciais
    If Not IsNull(rst3("Empresa1_Ref_COM")) Then
        vFl_Alt_RefCom = 1
        II = 0
        For I = 0 To 4
            Select Case II
                Case 0, 3, 6, 9, 12
                    If IsNull(rst3.Fields(II)) Then Exit For
            End Select
            Set litem = lsvRefCom.ListItems.Add
            litem = I + 1
            litem.Checked = True
            litem.SubItems(1) = IIf(IsNull(rst3.Fields(II).Value), "", rst3.Fields(II).Value)
            II = II + 1
            litem.SubItems(2) = IIf(IsNull(rst3.Fields(II).Value), "", rst3.Fields(II).Value)
            II = II + 1
            litem.SubItems(3) = IIf(IsNull(rst3.Fields(II).Value), "", rst3.Fields(II).Value)
            II = II + 1
        Next
    End If
        
    txtSAPRecadastro.Text = ObterCodSAP(txtCGC.Text)
End Sub

