VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmParametros 
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Parametros"
   ClientHeight    =   2700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7125
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2700
   ScaleWidth      =   7125
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtSiteSERASA 
      Height          =   315
      Left            =   30
      MaxLength       =   150
      TabIndex        =   9
      Top             =   2295
      Width           =   6765
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   45
      Left            =   30
      TabIndex        =   8
      Top             =   780
      Width           =   7065
      _ExtentX        =   12462
      _ExtentY        =   79
      Color1          =   0
      Color2          =   12632256
      BackColor       =   -2147483633
   End
   Begin VB.TextBox txtSCI_Antigo 
      Enabled         =   0   'False
      Height          =   315
      Left            =   30
      MaxLength       =   150
      TabIndex        =   6
      Top             =   1710
      Width           =   6765
   End
   Begin VB.TextBox txtCaminhoSerasa 
      Enabled         =   0   'False
      Height          =   315
      Left            =   30
      MaxLength       =   150
      TabIndex        =   1
      Top             =   1140
      Width           =   6765
   End
   Begin Bot�o.cmd cmdSalvar 
      Height          =   705
      Left            =   60
      TabIndex        =   2
      ToolTipText     =   "Gravar Altera��es"
      Top             =   30
      Width           =   765
      _ExtentX        =   1349
      _ExtentY        =   1244
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmParametros.frx":0000
      PICN            =   "frmParametros.frx":001C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   705
      Left            =   900
      TabIndex        =   3
      ToolTipText     =   "Fechar"
      Top             =   30
      Width           =   765
      _ExtentX        =   1349
      _ExtentY        =   1244
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmParametros.frx":0CF6
      PICN            =   "frmParametros.frx":0D12
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdSerasa 
      Height          =   255
      Left            =   6840
      TabIndex        =   4
      Top             =   1200
      Width           =   255
      _ExtentX        =   450
      _ExtentY        =   450
      BTYPE           =   3
      TX              =   "..."
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmParametros.frx":19EC
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdSCIAntigo 
      Height          =   255
      Left            =   6870
      TabIndex        =   7
      Top             =   1770
      Width           =   255
      _ExtentX        =   450
      _ExtentY        =   450
      BTYPE           =   3
      TX              =   "..."
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmParametros.frx":1A08
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      Caption         =   "Site SERASA:"
      Height          =   195
      Left            =   30
      TabIndex        =   10
      Top             =   2040
      Width           =   1005
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      Caption         =   "SCI Antigo em:"
      Height          =   195
      Left            =   30
      TabIndex        =   5
      Top             =   1500
      Width           =   1050
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      Caption         =   "Salvar Serasa/Sci em:"
      Height          =   195
      Left            =   30
      TabIndex        =   0
      Top             =   930
      Width           =   1590
   End
End
Attribute VB_Name = "frmParametros"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : frmParametros
' Author    : C.SAMUEL.OLIVEIRA
' Date      : 22/02/16
' Purpose   : TI-4188
'---------------------------------------------------------------------------------------

Option Explicit
Dim pidl, R, pos
Dim fName As String
Dim fPath As String
Dim bi As BROWSEINFO
Dim tmpPath As String
Dim vSiteSERASA As String 'TI-4188


Private Sub cmdSalvar_Click()
    If txtCaminhoSerasa = "" Then
       MsgBox "Campo Obrigat�rio.", vbInformation, "Aten��o"
       txtCaminhoSerasa.SetFocus
       Exit Sub
    End If
    
    If txtSCI_Antigo = "" Then
       MsgBox "Campo Obrigat�rio.", vbInformation, "Aten��o"
       txtSCI_Antigo.SetFocus
       Exit Sub
    End If

    'TI-4188
    If Trim(LCase(txtSiteSERASA.Text)) <> vSiteSERASA Then
        'Atualizar o Banco - SITE SERASA
        db.Parameters.Remove "Cod_Soft": db.Parameters.Add "Cod_Soft", CodigoSistema, 1
        db.Parameters.Remove "Nome_Param": db.Parameters.Add "Nome_Param", "SITESERASA", 1
        db.Parameters.Remove "VL_PARAM": db.Parameters.Add "VL_PARAM", Trim(LCase(txtSiteSERASA.Text)), 1
        db.Parameters.Remove "Cod_erro": db.Parameters.Add "Cod_erro", 0, 2
        db.Parameters.Remove "Txt_Erro": db.Parameters.Add "Txt_Erro", "", 2
        
        vVB_Generica_001.ExecutaPl db, "PRODUCAO.PCK_Cliente.PR_INSERT_UPDATE_PARAMETROS(:Cod_Soft, :Nome_Param, :VL_PARAM, :Cod_erro, :Txt_Erro)"
        
        If vErro = "" Then
            MsgBox "Site SERASA Atualizado com Sucesso!!", vbInformation, "Aten��o"
        Else
            MsgBox "Problemas ao atualizar site serasa. Favor avisar Depto.Suporte", vbCritical, "CAD060 - Aten��o"
        End If
        Exit Sub
    End If
    'FIM TI-4188
    
    'Atualizar o Banco - CAMINHO SERASA
    db.Parameters.Remove "Cod_Soft": db.Parameters.Add "Cod_Soft", CodigoSistema, 1
    db.Parameters.Remove "Nome_Param": db.Parameters.Add "Nome_Param", "CAMINHOSERASA", 1
    db.Parameters.Remove "VL_PARAM": db.Parameters.Add "VL_PARAM", txtCaminhoSerasa, 1
    db.Parameters.Remove "Cod_erro": db.Parameters.Add "Cod_erro", 0, 2
    db.Parameters.Remove "Txt_Erro": db.Parameters.Add "Txt_Erro", "", 2
    
    vVB_Generica_001.ExecutaPl db, "PRODUCAO.PCK_Cliente.PR_INSERT_UPDATE_PARAMETROS(:Cod_Soft, :Nome_Param, :VL_PARAM, :Cod_erro, :Txt_Erro)"
    
    If vErro = "" Then
        
        'Atualizar o Banco - CAMINHO SCI ANTIGO
        db.Parameters.Remove "Cod_Soft": db.Parameters.Add "Cod_Soft", CodigoSistema, 1
        db.Parameters.Remove "Nome_Param": db.Parameters.Add "Nome_Param", "CAMINHOSCIANTIGO", 1
        db.Parameters.Remove "VL_PARAM": db.Parameters.Add "VL_PARAM", txtSCI_Antigo, 1
        db.Parameters.Remove "Cod_erro": db.Parameters.Add "Cod_erro", 0, 2
        db.Parameters.Remove "Txt_Erro": db.Parameters.Add "Txt_Erro", "", 2
        
        vVB_Generica_001.ExecutaPl db, "PRODUCAO.PCK_Cliente.PR_INSERT_UPDATE_PARAMETROS(:Cod_Soft, :Nome_Param, :VL_PARAM, :Cod_erro, :Txt_Erro)"
        
        If vErro = "" Then
            MsgBox "Dados Atualizados com Sucesso!!", vbInformation, "Aten��o"
        End If
    
    Else
        MsgBox "Problemas ao atualizar caminhos. Favor avisar Depto.Suporte", vbCritical, "CAD060 - Aten��o"
    End If

End Sub

Private Sub cmdSCIAntigo_Click()
    txtSCI_Antigo = vbGetBrowseDirectory
End Sub

Private Sub cmdSerasa_Click()
    txtCaminhoSerasa = vbGetBrowseDirectory
End Sub

Public Function vbGetBrowseDirectory() As String

    pidl = SHBrowseForFolder(bi)
    
    tmpPath = Space$(512)
    R = SHGetPathFromIDList(ByVal pidl, ByVal tmpPath)
      
    If R Then
          pos = InStr(tmpPath, Chr$(0))
          tmpPath = Left(tmpPath, pos - 1)
          vbGetBrowseDirectory = ValidateDir(tmpPath)
    Else: vbGetBrowseDirectory = ""
    End If

End Function

Private Function ValidateDir(tmpPath As String) As String

    If Right$(tmpPath, 1) = "\" Then
          ValidateDir = tmpPath
    Else: ValidateDir = tmpPath & "\"
    End If
End Function

Private Sub cmdVoltar_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    txtCaminhoSerasa = Pegar_VL_Parametro("CAMINHOSERASA")
    txtSCI_Antigo = Pegar_VL_Parametro("CAMINHOSCIANTIGO")
    vSiteSERASA = "" & Pegar_VL_Parametro("SITESERASA") 'TI-4188
    txtSiteSERASA.Text = vSiteSERASA 'TI-4188
End Sub
