VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmCadCliente 
   BackColor       =   &H80000009&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "CAD060 - Cadastro de Cliente"
   ClientHeight    =   7590
   ClientLeft      =   60
   ClientTop       =   1485
   ClientWidth     =   11850
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7590
   ScaleWidth      =   11850
   Begin TabDlg.SSTab sstCadastro 
      Height          =   6930
      Left            =   0
      TabIndex        =   116
      Top             =   0
      Width           =   12225
      _ExtentX        =   21564
      _ExtentY        =   12224
      _Version        =   393216
      Tabs            =   5
      TabsPerRow      =   5
      TabHeight       =   520
      BackColor       =   16777215
      ForeColor       =   8388608
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Cadastro Geral"
      TabPicture(0)   =   "frmCadCliente.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Shape2"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Label9"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Label10"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Label11"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "Label12"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "Label13"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "Label18"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "Label17"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "Label16"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "Label15"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "Label14"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "Label6"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "Label5"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).Control(13)=   "Label4"
      Tab(0).Control(13).Enabled=   0   'False
      Tab(0).Control(14)=   "Label2"
      Tab(0).Control(14).Enabled=   0   'False
      Tab(0).Control(15)=   "lblDocto"
      Tab(0).Control(15).Enabled=   0   'False
      Tab(0).Control(16)=   "Shape1"
      Tab(0).Control(16).Enabled=   0   'False
      Tab(0).Control(17)=   "Label8"
      Tab(0).Control(17).Enabled=   0   'False
      Tab(0).Control(18)=   "Label7"
      Tab(0).Control(18).Enabled=   0   'False
      Tab(0).Control(19)=   "Label27"
      Tab(0).Control(19).Enabled=   0   'False
      Tab(0).Control(20)=   "Label28"
      Tab(0).Control(20).Enabled=   0   'False
      Tab(0).Control(21)=   "Label29"
      Tab(0).Control(21).Enabled=   0   'False
      Tab(0).Control(22)=   "Label30"
      Tab(0).Control(22).Enabled=   0   'False
      Tab(0).Control(23)=   "Label31"
      Tab(0).Control(23).Enabled=   0   'False
      Tab(0).Control(24)=   "lblTransp"
      Tab(0).Control(24).Enabled=   0   'False
      Tab(0).Control(25)=   "Label35"
      Tab(0).Control(25).Enabled=   0   'False
      Tab(0).Control(26)=   "Label33"
      Tab(0).Control(26).Enabled=   0   'False
      Tab(0).Control(27)=   "Label34"
      Tab(0).Control(27).Enabled=   0   'False
      Tab(0).Control(28)=   "Label36(0)"
      Tab(0).Control(28).Enabled=   0   'False
      Tab(0).Control(29)=   "Label21"
      Tab(0).Control(29).Enabled=   0   'False
      Tab(0).Control(30)=   "Label22"
      Tab(0).Control(30).Enabled=   0   'False
      Tab(0).Control(31)=   "Label39(0)"
      Tab(0).Control(31).Enabled=   0   'False
      Tab(0).Control(32)=   "Shape3"
      Tab(0).Control(32).Enabled=   0   'False
      Tab(0).Control(33)=   "Shape6"
      Tab(0).Control(33).Enabled=   0   'False
      Tab(0).Control(34)=   "Label38"
      Tab(0).Control(34).Enabled=   0   'False
      Tab(0).Control(35)=   "Shape7"
      Tab(0).Control(35).Enabled=   0   'False
      Tab(0).Control(36)=   "Label20"
      Tab(0).Control(36).Enabled=   0   'False
      Tab(0).Control(37)=   "Label19"
      Tab(0).Control(37).Enabled=   0   'False
      Tab(0).Control(38)=   "Shape8"
      Tab(0).Control(38).Enabled=   0   'False
      Tab(0).Control(39)=   "Label70"
      Tab(0).Control(39).Enabled=   0   'False
      Tab(0).Control(40)=   "Shape4"
      Tab(0).Control(40).Enabled=   0   'False
      Tab(0).Control(41)=   "Label73"
      Tab(0).Control(41).Enabled=   0   'False
      Tab(0).Control(42)=   "Label74"
      Tab(0).Control(42).Enabled=   0   'False
      Tab(0).Control(43)=   "Shape5"
      Tab(0).Control(43).Enabled=   0   'False
      Tab(0).Control(44)=   "Label71"
      Tab(0).Control(44).Enabled=   0   'False
      Tab(0).Control(45)=   "lblVlPrevCpra"
      Tab(0).Control(45).Enabled=   0   'False
      Tab(0).Control(46)=   "Label72"
      Tab(0).Control(46).Enabled=   0   'False
      Tab(0).Control(47)=   "lblDiferenciado"
      Tab(0).Control(47).Enabled=   0   'False
      Tab(0).Control(48)=   "Label75"
      Tab(0).Control(48).Enabled=   0   'False
      Tab(0).Control(49)=   "Label76"
      Tab(0).Control(49).Enabled=   0   'False
      Tab(0).Control(50)=   "cmd1"
      Tab(0).Control(50).Enabled=   0   'False
      Tab(0).Control(51)=   "cmdCEP_CORREIOS"
      Tab(0).Control(51).Enabled=   0   'False
      Tab(0).Control(52)=   "txtEndereco"
      Tab(0).Control(52).Enabled=   0   'False
      Tab(0).Control(53)=   "txtBairro"
      Tab(0).Control(53).Enabled=   0   'False
      Tab(0).Control(54)=   "txtCep"
      Tab(0).Control(54).Enabled=   0   'False
      Tab(0).Control(55)=   "cboUF"
      Tab(0).Control(55).Enabled=   0   'False
      Tab(0).Control(56)=   "cboCidade"
      Tab(0).Control(56).Enabled=   0   'False
      Tab(0).Control(57)=   "txtContato"
      Tab(0).Control(57).Enabled=   0   'False
      Tab(0).Control(58)=   "txtDDD2"
      Tab(0).Control(58).Enabled=   0   'False
      Tab(0).Control(59)=   "txtFone2"
      Tab(0).Control(59).Enabled=   0   'False
      Tab(0).Control(60)=   "txtDDD1"
      Tab(0).Control(60).Enabled=   0   'False
      Tab(0).Control(61)=   "txtFone1"
      Tab(0).Control(61).Enabled=   0   'False
      Tab(0).Control(62)=   "txtFax"
      Tab(0).Control(62).Enabled=   0   'False
      Tab(0).Control(63)=   "txtTelex"
      Tab(0).Control(63).Enabled=   0   'False
      Tab(0).Control(64)=   "txtCxPostal"
      Tab(0).Control(64).Enabled=   0   'False
      Tab(0).Control(65)=   "Frame6"
      Tab(0).Control(65).Enabled=   0   'False
      Tab(0).Control(66)=   "cboStatus"
      Tab(0).Control(66).Enabled=   0   'False
      Tab(0).Control(67)=   "cboCaracteristica"
      Tab(0).Control(67).Enabled=   0   'False
      Tab(0).Control(68)=   "txtInscrEstadual"
      Tab(0).Control(68).Enabled=   0   'False
      Tab(0).Control(69)=   "cboTpDocto"
      Tab(0).Control(69).Enabled=   0   'False
      Tab(0).Control(70)=   "txtDocto"
      Tab(0).Control(70).Enabled=   0   'False
      Tab(0).Control(71)=   "txtVlMaiorSlDev"
      Tab(0).Control(71).Enabled=   0   'False
      Tab(0).Control(72)=   "txtDtMaiorSlDev"
      Tab(0).Control(72).Enabled=   0   'False
      Tab(0).Control(73)=   "cboProtesto"
      Tab(0).Control(73).Enabled=   0   'False
      Tab(0).Control(74)=   "cboTpOP"
      Tab(0).Control(74).Enabled=   0   'False
      Tab(0).Control(75)=   "cboClassificacao"
      Tab(0).Control(75).Enabled=   0   'False
      Tab(0).Control(76)=   "cboBanco"
      Tab(0).Control(76).Enabled=   0   'False
      Tab(0).Control(77)=   "cboCheque"
      Tab(0).Control(77).Enabled=   0   'False
      Tab(0).Control(78)=   "cboTransportadora"
      Tab(0).Control(78).Enabled=   0   'False
      Tab(0).Control(79)=   "cboMsgCliente"
      Tab(0).Control(79).Enabled=   0   'False
      Tab(0).Control(80)=   "txtDtMsg"
      Tab(0).Control(80).Enabled=   0   'False
      Tab(0).Control(81)=   "txtResponsavel"
      Tab(0).Control(81).Enabled=   0   'False
      Tab(0).Control(82)=   "cboMsgFiscal"
      Tab(0).Control(82).Enabled=   0   'False
      Tab(0).Control(83)=   "txtHomePage"
      Tab(0).Control(83).Enabled=   0   'False
      Tab(0).Control(84)=   "txtEmail"
      Tab(0).Control(84).Enabled=   0   'False
      Tab(0).Control(85)=   "Frame8(0)"
      Tab(0).Control(85).Enabled=   0   'False
      Tab(0).Control(86)=   "txtProprietario"
      Tab(0).Control(86).Enabled=   0   'False
      Tab(0).Control(87)=   "txtInscrSuframa"
      Tab(0).Control(87).Enabled=   0   'False
      Tab(0).Control(88)=   "txtDtTare"
      Tab(0).Control(88).Enabled=   0   'False
      Tab(0).Control(89)=   "txtCodTare"
      Tab(0).Control(89).Enabled=   0   'False
      Tab(0).Control(90)=   "chkPredioProprio"
      Tab(0).Control(90).Enabled=   0   'False
      Tab(0).Control(91)=   "chkConsumidorFinal"
      Tab(0).Control(91).Enabled=   0   'False
      Tab(0).Control(92)=   "chkUtility"
      Tab(0).Control(92).Enabled=   0   'False
      Tab(0).Control(93)=   "chkExportacao"
      Tab(0).Control(93).Enabled=   0   'False
      Tab(0).Control(94)=   "cmdCep"
      Tab(0).Control(94).Enabled=   0   'False
      Tab(0).Control(95)=   "fraGrupoMaster"
      Tab(0).Control(95).Enabled=   0   'False
      Tab(0).Control(96)=   "cboTpEmpresa"
      Tab(0).Control(96).Enabled=   0   'False
      Tab(0).Control(97)=   "txtDtLimite"
      Tab(0).Control(97).Enabled=   0   'False
      Tab(0).Control(98)=   "txtLimite"
      Tab(0).Control(98).Enabled=   0   'False
      Tab(0).Control(99)=   "cboCNAE"
      Tab(0).Control(99).Enabled=   0   'False
      Tab(0).Control(100)=   "cboDiferenciado"
      Tab(0).Control(100).Enabled=   0   'False
      Tab(0).Control(101)=   "txtNumero"
      Tab(0).Control(101).Enabled=   0   'False
      Tab(0).Control(102)=   "txtComplemento"
      Tab(0).Control(102).Enabled=   0   'False
      Tab(0).ControlCount=   103
      TabCaption(1)   =   "Representantes"
      TabPicture(1)   =   "frmCadCliente.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "shLinha2"
      Tab(1).Control(1)=   "lblRepresDPK"
      Tab(1).Control(2)=   "Label24"
      Tab(1).Control(3)=   "Label77"
      Tab(1).Control(4)=   "Label78"
      Tab(1).Control(5)=   "lblCategBosch"
      Tab(1).Control(6)=   "grdflexRepres"
      Tab(1).Control(7)=   "cmdRepres"
      Tab(1).Control(8)=   "cboRepresDPK"
      Tab(1).Control(9)=   "cboTpCliente"
      Tab(1).Control(10)=   "fraCategBosch"
      Tab(1).Control(11)=   "cboTpSegmento"
      Tab(1).Control(12)=   "cboFornVDR"
      Tab(1).Control(13)=   "cboCategBosch"
      Tab(1).ControlCount=   14
      TabCaption(2)   =   "Refer�ncias Comerciais"
      TabPicture(2)   =   "frmCadCliente.frx":0038
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "shLinha3"
      Tab(2).Control(1)=   "lblMsgCom"
      Tab(2).Control(2)=   "cmdDesfRefCom"
      Tab(2).Control(3)=   "cmdGrlsvRefComl"
      Tab(2).Control(4)=   "cmdEdRefComl"
      Tab(2).Control(5)=   "cmdGrRefComl"
      Tab(2).Control(6)=   "cmdAdRefComl"
      Tab(2).Control(7)=   "lsvRefComl"
      Tab(2).Control(8)=   "fraRef01"
      Tab(2).ControlCount=   9
      TabCaption(3)   =   "Refer�ncias Banc�rias"
      TabPicture(3)   =   "frmCadCliente.frx":0054
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "Frame1"
      Tab(3).Control(1)=   "cmdAdicionarRefBco"
      Tab(3).Control(2)=   "lsvRefBanco"
      Tab(3).Control(3)=   "cmdGrRefBco"
      Tab(3).Control(4)=   "cmdEditarRefBco"
      Tab(3).Control(5)=   "cmdGrlsvRefBanco"
      Tab(3).Control(6)=   "cmdDesfRefBco"
      Tab(3).Control(7)=   "lblMsgBco"
      Tab(3).Control(8)=   "shLinha4"
      Tab(3).ControlCount=   9
      TabCaption(4)   =   "Outros Endere�os"
      TabPicture(4)   =   "frmCadCliente.frx":0070
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "fraOutEnd"
      Tab(4).Control(1)=   "lsvOutEnd"
      Tab(4).Control(2)=   "cmdAdOutEnd"
      Tab(4).Control(3)=   "cmdGrOutEnd"
      Tab(4).Control(4)=   "cmdEdOutEnd"
      Tab(4).Control(5)=   "cmdGrlsvOutEnd"
      Tab(4).Control(6)=   "cmdDesfOutEnd"
      Tab(4).Control(7)=   "lblMsgEnd"
      Tab(4).Control(8)=   "shLinha5"
      Tab(4).ControlCount=   9
      Begin VB.ComboBox cboCategBosch 
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   -74835
         Style           =   2  'Dropdown List
         TabIndex        =   236
         Top             =   1950
         Width           =   960
      End
      Begin VB.ComboBox cboFornVDR 
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   -74835
         Style           =   2  'Dropdown List
         TabIndex        =   235
         Top             =   1325
         Width           =   1680
      End
      Begin VB.ComboBox cboTpSegmento 
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   -74835
         Style           =   2  'Dropdown List
         TabIndex        =   63
         Top             =   570
         Width           =   2670
      End
      Begin VB.TextBox txtComplemento 
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2640
         MaxLength       =   33
         TabIndex        =   14
         Text            =   "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
         Top             =   2250
         Width           =   1425
      End
      Begin VB.TextBox txtNumero 
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2040
         MaxLength       =   33
         TabIndex        =   13
         Text            =   "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
         Top             =   2250
         Width           =   555
      End
      Begin VB.ComboBox cboDiferenciado 
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmCadCliente.frx":008C
         Left            =   10560
         List            =   "frmCadCliente.frx":008E
         Style           =   2  'Dropdown List
         TabIndex        =   227
         Top             =   4680
         Width           =   1095
      End
      Begin VB.ComboBox cboCNAE 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   120
         TabIndex        =   223
         Top             =   6480
         Width           =   11460
      End
      Begin VB.TextBox txtLimite 
         Alignment       =   2  'Center
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   9300
         MaxLength       =   14
         TabIndex        =   53
         Text            =   "00000000000000"
         Top             =   3420
         Width           =   1395
      End
      Begin VB.TextBox txtDtLimite 
         Alignment       =   2  'Center
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   10755
         MaxLength       =   10
         TabIndex        =   54
         Text            =   "00/00/0000"
         Top             =   3435
         Width           =   930
      End
      Begin VB.ComboBox cboTpEmpresa 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmCadCliente.frx":0090
         Left            =   8460
         List            =   "frmCadCliente.frx":00A0
         Style           =   2  'Dropdown List
         TabIndex        =   40
         Top             =   5010
         Width           =   2055
      End
      Begin VB.Frame fraGrupoMaster 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   780
         Left            =   6600
         TabIndex        =   207
         Top             =   5430
         Width           =   3930
         Begin VB.ComboBox cboCodCliGrMaster 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0E0FF&
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   330
            ItemData        =   "frmCadCliente.frx":00DC
            Left            =   1650
            List            =   "frmCadCliente.frx":00DE
            TabIndex        =   48
            Top             =   390
            Width           =   2145
         End
         Begin VB.CheckBox chkGrupoMaster 
            BackColor       =   &H80000009&
            Caption         =   "Grupo Master"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   270
            Left            =   75
            TabIndex        =   46
            Top             =   135
            Width           =   1485
         End
         Begin VB.TextBox txtGrupoMaster 
            Alignment       =   2  'Center
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   60
            MaxLength       =   10
            TabIndex        =   47
            Top             =   405
            Width           =   1485
         End
         Begin VB.Label Label37 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "Cliente Master"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   2055
            TabIndex        =   211
            Top             =   150
            Width           =   1215
         End
      End
      Begin Bot�o.cmd cmdCep 
         Height          =   300
         Left            =   6825
         TabIndex        =   206
         ToolTipText     =   "Sites de CEP"
         Top             =   2250
         Width           =   315
         _ExtentX        =   556
         _ExtentY        =   529
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadCliente.frx":00E0
         PICN            =   "frmCadCliente.frx":00FC
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Frame fraCategBosch 
         BackColor       =   &H00FFFFFF&
         Height          =   2235
         Left            =   -73065
         TabIndex        =   198
         Top             =   1125
         Width           =   6600
         Begin MSFlexGridLib.MSFlexGrid grdflexLinVdr 
            Height          =   1995
            Left            =   780
            TabIndex        =   199
            ToolTipText     =   "Escolha ao menos 1 linha VDR"
            Top             =   150
            Width           =   4980
            _ExtentX        =   8784
            _ExtentY        =   3519
            _Version        =   393216
            Cols            =   3
            BackColorBkg    =   14737632
            TextStyle       =   1
            TextStyleFixed  =   1
            GridLines       =   2
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin Bot�o.cmd cmdGrLinVdr 
            Height          =   615
            Left            =   5850
            TabIndex        =   68
            ToolTipText     =   "Gravar Linhas VDR"
            Top             =   1545
            Visible         =   0   'False
            Width           =   675
            _ExtentX        =   1191
            _ExtentY        =   1085
            BTYPE           =   3
            TX              =   "Gravar"
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   -1  'True
            BCOL            =   13160660
            BCOLO           =   13160660
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "frmCadCliente.frx":020E
            PICN            =   "frmCadCliente.frx":022A
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   2
            NGREY           =   0   'False
            FX              =   0
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin Bot�o.cmd cmdLinVdr 
            Height          =   615
            Left            =   60
            TabIndex        =   67
            ToolTipText     =   "Linhas VDR"
            Top             =   150
            Width           =   675
            _ExtentX        =   1191
            _ExtentY        =   1085
            BTYPE           =   3
            TX              =   ""
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   -1  'True
            BCOL            =   13160660
            BCOLO           =   13160660
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "frmCadCliente.frx":0384
            PICN            =   "frmCadCliente.frx":03A0
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   2
            NGREY           =   0   'False
            FX              =   0
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin Bot�o.cmd cmdLimparVdr 
            Height          =   615
            Left            =   60
            TabIndex        =   239
            ToolTipText     =   "Limpar op��o VDR"
            Top             =   840
            Width           =   675
            _ExtentX        =   1191
            _ExtentY        =   1085
            BTYPE           =   3
            TX              =   ""
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   -1  'True
            BCOL            =   16777215
            BCOLO           =   16777215
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "frmCadCliente.frx":06BA
            PICN            =   "frmCadCliente.frx":06D6
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   2
            NGREY           =   0   'False
            FX              =   0
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
      End
      Begin VB.Frame fraOutEnd 
         BackColor       =   &H00FFFFFF&
         Height          =   1815
         Left            =   -74880
         TabIndex        =   182
         Top             =   1125
         Width           =   11580
         Begin VB.TextBox txtComplEnd01 
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   7560
            MaxLength       =   33
            TabIndex        =   103
            Text            =   "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
            Top             =   880
            Width           =   1185
         End
         Begin VB.TextBox txtNumEnd01 
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   6840
            MaxLength       =   33
            TabIndex        =   102
            Text            =   "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
            Top             =   880
            Width           =   705
         End
         Begin VB.TextBox txtSeqEnd01 
            Alignment       =   2  'Center
            BackColor       =   &H000040C0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   90
            MaxLength       =   6
            TabIndex        =   95
            Text            =   "000000"
            Top             =   360
            Width           =   705
         End
         Begin VB.TextBox txtCGCEnd01 
            Alignment       =   2  'Center
            BackColor       =   &H00C0E0FF&
            BeginProperty DataFormat 
               Type            =   0
               Format          =   "00.000.000/0000-00"
               HaveTrueFalseNull=   0
               FirstDayOfWeek  =   0
               FirstWeekOfYear =   0
               LCID            =   1046
               SubFormatType   =   0
            EndProperty
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   5100
            MaxLength       =   18
            TabIndex        =   98
            Text            =   "00.000.000/0001-00"
            Top             =   375
            Width           =   1650
         End
         Begin VB.ComboBox cboTpEnd01 
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            ItemData        =   "frmCadCliente.frx":0B28
            Left            =   900
            List            =   "frmCadCliente.frx":0B32
            Style           =   2  'Dropdown List
            TabIndex        =   96
            Top             =   375
            Width           =   2055
         End
         Begin VB.ComboBox cboTpDoctoEnd01 
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            ItemData        =   "frmCadCliente.frx":0B51
            Left            =   3000
            List            =   "frmCadCliente.frx":0B5E
            Style           =   2  'Dropdown List
            TabIndex        =   97
            Top             =   375
            Width           =   2055
         End
         Begin VB.TextBox txtIEEnd01 
            Alignment       =   2  'Center
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   6810
            MaxLength       =   14
            TabIndex        =   99
            Text            =   "00000000000000"
            Top             =   375
            Width           =   1380
         End
         Begin VB.ComboBox cboCidadeEnd01 
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1560
            Style           =   2  'Dropdown List
            TabIndex        =   107
            Top             =   1410
            Width           =   3060
         End
         Begin VB.ComboBox cboUFEnd01 
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   885
            Style           =   2  'Dropdown List
            TabIndex        =   106
            Top             =   1410
            Width           =   660
         End
         Begin VB.TextBox txtCepEnd01 
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   10365
            MaxLength       =   8
            TabIndex        =   105
            Text            =   "00000000"
            Top             =   885
            Width           =   825
         End
         Begin VB.TextBox txtBairroEnd01 
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   8730
            MaxLength       =   20
            TabIndex        =   104
            Text            =   "XXXXXXXXXXXXXXXXXXXX"
            Top             =   885
            Width           =   1605
         End
         Begin VB.TextBox txtEnd01 
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   4515
            MaxLength       =   33
            TabIndex        =   101
            Text            =   "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
            Top             =   885
            Width           =   2265
         End
         Begin VB.TextBox txtNomeEnd01 
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   885
            MaxLength       =   30
            TabIndex        =   100
            Text            =   "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
            Top             =   885
            Width           =   3585
         End
         Begin VB.TextBox txtCxPosEnd01 
            Alignment       =   2  'Center
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   7485
            MaxLength       =   5
            TabIndex        =   111
            Text            =   "00000"
            Top             =   1410
            Width           =   615
         End
         Begin VB.TextBox txtFaxEnd01 
            Alignment       =   2  'Center
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   6075
            MaxLength       =   13
            TabIndex        =   110
            Text            =   "0000000000000"
            Top             =   1410
            Width           =   1350
         End
         Begin VB.TextBox txtFoneEnd01 
            Alignment       =   2  'Center
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   5085
            MaxLength       =   9
            TabIndex        =   109
            Top             =   1410
            Width           =   930
         End
         Begin VB.TextBox txtDDDEnd01 
            Alignment       =   2  'Center
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   4650
            MaxLength       =   4
            TabIndex        =   108
            Top             =   1410
            Width           =   435
         End
         Begin Bot�o.cmd cmdCEP_CORREIOS2 
            Height          =   450
            Left            =   10815
            TabIndex        =   221
            ToolTipText     =   "Consulta CEP - CORREIOS"
            Top             =   225
            Width           =   645
            _ExtentX        =   1138
            _ExtentY        =   794
            BTYPE           =   3
            TX              =   ""
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   -1  'True
            BCOL            =   13160660
            BCOLO           =   13160660
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "frmCadCliente.frx":0B8C
            PICN            =   "frmCadCliente.frx":0BA8
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   0
            NGREY           =   0   'False
            FX              =   0
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin VB.Label lblComplEnd01 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "Compl."
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   7560
            TabIndex        =   233
            Top             =   690
            Width           =   585
         End
         Begin VB.Label lblNumEnd01 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "Num."
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   6840
            TabIndex        =   232
            Top             =   690
            Width           =   420
         End
         Begin VB.Label Label52 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "Seq."
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   120
            TabIndex        =   196
            Top             =   165
            Width           =   360
         End
         Begin VB.Label Label53 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "CGC"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   5115
            TabIndex        =   195
            Top             =   165
            Width           =   360
         End
         Begin VB.Label Label54 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "Tipo Endere�o"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   900
            TabIndex        =   194
            Top             =   180
            Width           =   1185
         End
         Begin VB.Label Label55 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "Tipo Docto"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   3015
            TabIndex        =   193
            Top             =   180
            Width           =   870
         End
         Begin VB.Label Label60 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "Inscr.Estadual"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   6855
            TabIndex        =   192
            Top             =   165
            Width           =   1155
         End
         Begin VB.Label Label61 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "Cidade"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   1590
            TabIndex        =   191
            Top             =   1215
            Width           =   570
         End
         Begin VB.Label Label62 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "UF"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   885
            TabIndex        =   190
            Top             =   1215
            Width           =   195
         End
         Begin VB.Label Label63 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "CEP"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   10365
            TabIndex        =   189
            Top             =   690
            Width           =   315
         End
         Begin VB.Label Label64 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "Bairro"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   8745
            TabIndex        =   188
            Top             =   690
            Width           =   495
         End
         Begin VB.Label Label65 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "Endere�o"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   4515
            TabIndex        =   187
            Top             =   690
            Width           =   780
         End
         Begin VB.Label Label66 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "Nome Cliente"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   885
            TabIndex        =   186
            Top             =   690
            Width           =   1110
         End
         Begin VB.Label Label67 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "C.Postal"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   7485
            TabIndex        =   185
            Top             =   1215
            Width           =   675
         End
         Begin VB.Label Label68 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "Fax"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   6120
            TabIndex        =   184
            Top             =   1215
            Width           =   270
         End
         Begin VB.Label Label69 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "DDD/Fones"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   4695
            TabIndex        =   183
            Top             =   1215
            Width           =   870
         End
      End
      Begin VB.Frame Frame1 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         ForeColor       =   &H80000008&
         Height          =   1290
         Left            =   -74880
         TabIndex        =   172
         Top             =   1110
         Width           =   11565
         Begin VB.TextBox txtDDD 
            Alignment       =   2  'Center
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   4485
            MaxLength       =   4
            TabIndex        =   88
            Top             =   315
            Width           =   435
         End
         Begin VB.TextBox txtFone 
            Alignment       =   2  'Center
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   4920
            MaxLength       =   9
            TabIndex        =   89
            Top             =   315
            Width           =   930
         End
         Begin VB.TextBox txtDt 
            Alignment       =   2  'Center
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   2445
            MaxLength       =   10
            TabIndex        =   86
            Top             =   315
            Width           =   1215
         End
         Begin VB.TextBox txtSeq 
            Alignment       =   2  'Center
            BackColor       =   &H000040C0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   90
            Locked          =   -1  'True
            MaxLength       =   2
            TabIndex        =   84
            TabStop         =   0   'False
            Text            =   "00"
            Top             =   315
            Width           =   495
         End
         Begin VB.TextBox txtBanco 
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   645
            MaxLength       =   15
            TabIndex        =   85
            Text            =   "XXXXXXXXXXXXXXX"
            Top             =   315
            Width           =   1740
         End
         Begin VB.TextBox txtAgencia 
            Alignment       =   2  'Center
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   3720
            MaxLength       =   5
            TabIndex        =   87
            Text            =   "00000"
            Top             =   315
            Width           =   675
         End
         Begin VB.TextBox txtConceito 
            Alignment       =   2  'Center
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   5910
            MaxLength       =   5
            TabIndex        =   90
            Text            =   "XXXXX"
            Top             =   315
            Width           =   765
         End
         Begin VB.TextBox txtObs 
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   645
            MaxLength       =   80
            TabIndex        =   91
            Text            =   "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
            Top             =   825
            Width           =   8565
         End
         Begin VB.Label Label50 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "DDD/Fone"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   4500
            TabIndex        =   179
            Top             =   120
            Width           =   765
         End
         Begin VB.Label Label56 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "Dt Consulta"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   2475
            TabIndex        =   178
            Top             =   120
            Width           =   945
         End
         Begin VB.Label Label59 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "Seq."
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   90
            TabIndex        =   177
            Top             =   120
            Width           =   360
         End
         Begin VB.Label Label58 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "Banco"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   675
            TabIndex        =   176
            Top             =   120
            Width           =   495
         End
         Begin VB.Label Label57 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "Ag�ncia"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   3750
            TabIndex        =   175
            Top             =   105
            Width           =   660
         End
         Begin VB.Label Label51 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "Conceito"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   5910
            TabIndex        =   174
            Top             =   120
            Width           =   735
         End
         Begin VB.Label Label49 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "Observa��o"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   630
            TabIndex        =   173
            Top             =   630
            Width           =   975
         End
      End
      Begin VB.Frame fraRef01 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         ForeColor       =   &H80000008&
         Height          =   1095
         Left            =   -74880
         TabIndex        =   159
         Top             =   1110
         Width           =   11580
         Begin VB.TextBox txtEmp01 
            Alignment       =   2  'Center
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   630
            MaxLength       =   10
            TabIndex        =   70
            Text            =   "XXXXXXXXXX"
            Top             =   315
            Width           =   1455
         End
         Begin VB.TextBox txtObs01 
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1485
            MaxLength       =   105
            TabIndex        =   80
            Text            =   "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
            Top             =   705
            Width           =   10020
         End
         Begin VB.TextBox txtConceito01 
            Alignment       =   2  'Center
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   10485
            MaxLength       =   10
            TabIndex        =   79
            Text            =   "XXXXX"
            Top             =   315
            Width           =   1005
         End
         Begin VB.TextBox txtAtraso01 
            Alignment       =   2  'Center
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   9950
            MaxLength       =   4
            TabIndex        =   78
            Text            =   "0000"
            Top             =   315
            Width           =   525
         End
         Begin VB.TextBox txtDtCad01 
            Alignment       =   2  'Center
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   8700
            MaxLength       =   10
            TabIndex        =   77
            Top             =   315
            Width           =   1215
         End
         Begin VB.TextBox txtMaiSlDev01 
            Alignment       =   2  'Center
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   7250
            MaxLength       =   14
            TabIndex        =   76
            Text            =   "00000000"
            Top             =   315
            Width           =   1380
         End
         Begin VB.TextBox txtDtUltFat01 
            Alignment       =   2  'Center
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   6000
            MaxLength       =   10
            TabIndex        =   75
            Top             =   315
            Width           =   1215
         End
         Begin VB.TextBox txtVlUltFat01 
            Alignment       =   2  'Center
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   4905
            MaxLength       =   14
            TabIndex        =   74
            Text            =   "00000000"
            Top             =   315
            Width           =   1065
         End
         Begin VB.TextBox txtDDD01 
            Alignment       =   2  'Center
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   3480
            MaxLength       =   4
            TabIndex        =   72
            Top             =   315
            Width           =   435
         End
         Begin VB.TextBox txtFone01 
            Alignment       =   2  'Center
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   3915
            MaxLength       =   9
            TabIndex        =   73
            Top             =   315
            Width           =   930
         End
         Begin VB.TextBox txtDtCons01 
            Alignment       =   2  'Center
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   2160
            MaxLength       =   10
            TabIndex        =   71
            Top             =   315
            Width           =   1215
         End
         Begin VB.TextBox txtSeq01 
            Alignment       =   2  'Center
            BackColor       =   &H000040C0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   90
            MaxLength       =   2
            TabIndex        =   69
            Text            =   "00"
            Top             =   315
            Width           =   495
         End
         Begin VB.Label Label44 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "Observa��o"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   450
            TabIndex        =   170
            Top             =   765
            Width           =   975
         End
         Begin VB.Label Label48 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "Maior Sl.Devedor"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   7250
            TabIndex        =   169
            Top             =   120
            Width           =   1395
         End
         Begin VB.Label Label47 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "Conceito"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   10620
            TabIndex        =   168
            Top             =   120
            Width           =   735
         End
         Begin VB.Label Label46 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "Atraso"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   9950
            TabIndex        =   167
            Top             =   120
            Width           =   555
         End
         Begin VB.Label Label45 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "Dt.Cadastram."
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   8730
            TabIndex        =   166
            Top             =   120
            Width           =   1155
         End
         Begin VB.Label Label43 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "Dt �lt Fatura"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   6120
            TabIndex        =   165
            Top             =   120
            Width           =   975
         End
         Begin VB.Label Label42 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "Vl �lt Fatura"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   4935
            TabIndex        =   164
            Top             =   120
            Width           =   975
         End
         Begin VB.Label Label41 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "DDD/Fone"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   3510
            TabIndex        =   163
            Top             =   120
            Width           =   765
         End
         Begin VB.Label Label40 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "Dt Consulta"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   2235
            TabIndex        =   162
            Top             =   120
            Width           =   945
         End
         Begin VB.Label Label32 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "Empresa"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   675
            TabIndex        =   161
            Top             =   120
            Width           =   735
         End
         Begin VB.Label Label26 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "Seq."
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   90
            TabIndex        =   160
            Top             =   120
            Width           =   360
         End
      End
      Begin VB.CheckBox chkExportacao 
         BackColor       =   &H00C0FFFF&
         Caption         =   "Exporta��o"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   5265
         TabIndex        =   45
         Top             =   5955
         Width           =   1200
      End
      Begin VB.CheckBox chkUtility 
         BackColor       =   &H00C0FFFF&
         Caption         =   "UTILITY"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   4080
         TabIndex        =   44
         Top             =   5955
         Width           =   975
      End
      Begin VB.CheckBox chkConsumidorFinal 
         BackColor       =   &H00C0FFFF&
         Caption         =   "Consumidor Final"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   1995
         TabIndex        =   43
         Top             =   5955
         Width           =   1815
      End
      Begin VB.CheckBox chkPredioProprio 
         BackColor       =   &H00C0FFFF&
         Caption         =   "Pr�dio Pr�prio"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   210
         TabIndex        =   42
         Top             =   5955
         Width           =   1575
      End
      Begin VB.TextBox txtCodTare 
         Alignment       =   2  'Center
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   10680
         MaxLength       =   6
         TabIndex        =   49
         Text            =   "000000"
         Top             =   5280
         Width           =   705
      End
      Begin VB.TextBox txtDtTare 
         Alignment       =   2  'Center
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   10680
         MaxLength       =   10
         TabIndex        =   50
         Text            =   "00/00/0000"
         Top             =   5835
         Width           =   945
      End
      Begin VB.TextBox txtInscrSuframa 
         Alignment       =   2  'Center
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   9975
         MaxLength       =   14
         TabIndex        =   11
         Top             =   1800
         Width           =   1695
      End
      Begin VB.TextBox txtProprietario 
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   135
         MaxLength       =   60
         TabIndex        =   41
         Text            =   "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
         Top             =   5550
         Width           =   6405
      End
      Begin VB.Frame Frame8 
         BackColor       =   &H00FFFFFF&
         Caption         =   "Data Funda��o"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   585
         Index           =   0
         Left            =   7635
         TabIndex        =   154
         Top             =   2565
         Width           =   1320
         Begin VB.TextBox txtDtFundacao 
            Alignment       =   2  'Center
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   45
            MaxLength       =   10
            TabIndex        =   27
            Top             =   210
            Width           =   1215
         End
      End
      Begin VB.TextBox txtEmail 
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000003&
         Height          =   330
         Left            =   120
         Locked          =   -1  'True
         MaxLength       =   40
         TabIndex        =   28
         Text            =   "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
         Top             =   3345
         Width           =   3735
      End
      Begin VB.TextBox txtHomePage 
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   4905
         MaxLength       =   40
         TabIndex        =   29
         Text            =   "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
         Top             =   3345
         Width           =   4320
      End
      Begin VB.ComboBox cboMsgFiscal 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   120
         Sorted          =   -1  'True
         TabIndex        =   33
         Text            =   "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX - 00"
         Top             =   4425
         Width           =   5985
      End
      Begin VB.TextBox txtResponsavel 
         Alignment       =   2  'Center
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   7365
         MaxLength       =   12
         TabIndex        =   32
         Top             =   3885
         Width           =   1455
      End
      Begin VB.TextBox txtDtMsg 
         Alignment       =   2  'Center
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   6120
         MaxLength       =   10
         TabIndex        =   31
         Top             =   3885
         Width           =   1215
      End
      Begin VB.ComboBox cboMsgCliente 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   120
         Sorted          =   -1  'True
         TabIndex        =   30
         Text            =   "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX - 00"
         Top             =   3885
         Width           =   5985
      End
      Begin VB.ComboBox cboTransportadora 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   6120
         TabIndex        =   34
         Text            =   "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX - 0000"
         Top             =   4425
         Width           =   4380
      End
      Begin VB.ComboBox cboCheque 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmCadCliente.frx":1066
         Left            =   3330
         List            =   "frmCadCliente.frx":1073
         Style           =   2  'Dropdown List
         TabIndex        =   37
         Top             =   4995
         Width           =   2340
      End
      Begin VB.ComboBox cboBanco 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   180
         Style           =   2  'Dropdown List
         TabIndex        =   35
         Top             =   4995
         Width           =   1860
      End
      Begin VB.ComboBox cboClassificacao 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmCadCliente.frx":10AF
         Left            =   7770
         List            =   "frmCadCliente.frx":10C2
         Style           =   2  'Dropdown List
         TabIndex        =   39
         Top             =   4995
         Width           =   555
      End
      Begin VB.ComboBox cboTpOP 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmCadCliente.frx":10D5
         Left            =   2055
         List            =   "frmCadCliente.frx":10E2
         Style           =   2  'Dropdown List
         TabIndex        =   36
         Top             =   4995
         Width           =   1260
      End
      Begin VB.ComboBox cboProtesto 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmCadCliente.frx":1109
         Left            =   5700
         List            =   "frmCadCliente.frx":1113
         Style           =   2  'Dropdown List
         TabIndex        =   38
         Top             =   4995
         Width           =   2055
      End
      Begin VB.TextBox txtDtMaiorSlDev 
         Alignment       =   2  'Center
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   10755
         MaxLength       =   10
         TabIndex        =   52
         Text            =   "00/00/0000"
         Top             =   2835
         Width           =   930
      End
      Begin VB.TextBox txtVlMaiorSlDev 
         Alignment       =   2  'Center
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   9300
         MaxLength       =   14
         TabIndex        =   51
         Text            =   "00000000000000"
         Top             =   2835
         Width           =   1395
      End
      Begin VB.TextBox txtDocto 
         Alignment       =   2  'Center
         BackColor       =   &H00C0E0FF&
         BeginProperty DataFormat 
            Type            =   0
            Format          =   "00.000.000/0000-00"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1046
            SubFormatType   =   0
         EndProperty
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2190
         MaxLength       =   18
         TabIndex        =   7
         Text            =   "00.000.000/0001-00"
         Top             =   1680
         Width           =   1650
      End
      Begin VB.ComboBox cboTpDocto 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   330
         ItemData        =   "frmCadCliente.frx":112D
         Left            =   120
         List            =   "frmCadCliente.frx":1137
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   1710
         Width           =   2055
      End
      Begin VB.TextBox txtInscrEstadual 
         Alignment       =   2  'Center
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3855
         MaxLength       =   14
         TabIndex        =   8
         Text            =   "00000000000000"
         Top             =   1710
         Width           =   1380
      End
      Begin VB.ComboBox cboCaracteristica 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmCadCliente.frx":1163
         Left            =   7470
         List            =   "frmCadCliente.frx":1165
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   10
         Top             =   1710
         Width           =   2295
      End
      Begin VB.ComboBox cboStatus 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmCadCliente.frx":1167
         Left            =   5280
         List            =   "frmCadCliente.frx":1171
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   1710
         Width           =   2175
      End
      Begin VB.Frame Frame6 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1230
         Left            =   105
         TabIndex        =   119
         Top             =   315
         Width           =   11655
         Begin VB.TextBox txtSAP 
            Alignment       =   2  'Center
            BackColor       =   &H000040C0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   9405
            Locked          =   -1  'True
            MaxLength       =   18
            TabIndex        =   229
            Top             =   720
            Width           =   1695
         End
         Begin VB.TextBox txtCodCliente 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H000040C0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   60
            Locked          =   -1  'True
            MaxLength       =   6
            TabIndex        =   1
            Top             =   300
            Width           =   975
         End
         Begin VB.ComboBox cboCliente 
            Appearance      =   0  'Flat
            BackColor       =   &H000040C0&
            CausesValidation=   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   330
            ItemData        =   "frmCadCliente.frx":1194
            Left            =   1060
            List            =   "frmCadCliente.frx":1196
            TabIndex        =   0
            Top             =   300
            Width           =   4305
         End
         Begin VB.PictureBox pictSituacao 
            Appearance      =   0  'Flat
            AutoRedraw      =   -1  'True
            BackColor       =   &H80000005&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   495
            Left            =   11070
            ScaleHeight     =   495
            ScaleWidth      =   495
            TabIndex        =   120
            Top             =   150
            Width           =   495
         End
         Begin VB.TextBox txtSituacao 
            Alignment       =   2  'Center
            BackColor       =   &H000040C0&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   9405
            Locked          =   -1  'True
            MaxLength       =   14
            TabIndex        =   5
            Top             =   315
            Width           =   1695
         End
         Begin VB.TextBox txtDtRecadastro 
            Alignment       =   2  'Center
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00404040&
            Height          =   285
            Left            =   6645
            MaxLength       =   10
            TabIndex        =   3
            Top             =   315
            Width           =   1215
         End
         Begin VB.TextBox txtDtSolicitacao 
            Alignment       =   2  'Center
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00404040&
            Height          =   285
            Left            =   7905
            Locked          =   -1  'True
            MaxLength       =   17
            TabIndex        =   4
            Top             =   315
            Width           =   1215
         End
         Begin VB.TextBox txtDtCadastro 
            Alignment       =   2  'Center
            BackColor       =   &H00C0E0FF&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00404040&
            Height          =   285
            Left            =   5385
            MaxLength       =   10
            TabIndex        =   2
            Top             =   315
            Width           =   1215
         End
         Begin VB.Label LabelSAP 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "SAP"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   9420
            TabIndex        =   228
            Top             =   550
            Width           =   330
         End
         Begin VB.Label lblRecusa2 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Cliente anteriormente recusado por motivo de:"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   75
            TabIndex        =   215
            Top             =   675
            Width           =   3900
         End
         Begin VB.Label lblRecusado2 
            AutoSize        =   -1  'True
            BorderStyle     =   1  'Fixed Single
            Caption         =   "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   270
            Left            =   75
            TabIndex        =   214
            Top             =   870
            Width           =   8985
         End
         Begin VB.Label Label23 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            BackStyle       =   0  'Transparent
            Caption         =   "Cliente"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   90
            TabIndex        =   213
            Top             =   120
            Width           =   585
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "Dt Cadastro"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   5400
            TabIndex        =   124
            Top             =   120
            Width           =   960
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "Situa��o"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   9420
            TabIndex        =   123
            Top             =   120
            Width           =   690
         End
         Begin VB.Label Label25 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "Dt Recadastro"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   6660
            TabIndex        =   122
            Top             =   120
            Width           =   1140
         End
         Begin VB.Label lblDtSolicitacao 
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFFFF&
            Caption         =   "Dt Solicitacao"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   7920
            TabIndex        =   121
            Top             =   120
            Width           =   1080
         End
      End
      Begin VB.TextBox txtCxPostal 
         Alignment       =   2  'Center
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   6060
         MaxLength       =   5
         TabIndex        =   25
         Text            =   "00000"
         Top             =   2790
         Width           =   615
      End
      Begin VB.TextBox txtTelex 
         Alignment       =   2  'Center
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   6720
         MaxLength       =   8
         TabIndex        =   26
         Text            =   "00000000"
         Top             =   2790
         Width           =   855
      End
      Begin VB.TextBox txtFax 
         Alignment       =   2  'Center
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2925
         MaxLength       =   13
         TabIndex        =   23
         Text            =   "0000000000000"
         Top             =   2790
         Width           =   1350
      End
      Begin VB.TextBox txtFone1 
         Alignment       =   2  'Center
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   585
         MaxLength       =   9
         TabIndex        =   20
         Top             =   2790
         Width           =   900
      End
      Begin VB.TextBox txtDDD1 
         Alignment       =   2  'Center
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   120
         MaxLength       =   4
         TabIndex        =   19
         Top             =   2790
         Width           =   435
      End
      Begin VB.TextBox txtFone2 
         Alignment       =   2  'Center
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1965
         MaxLength       =   9
         TabIndex        =   22
         Text            =   "000000000"
         Top             =   2790
         Width           =   930
      End
      Begin VB.TextBox txtDDD2 
         Alignment       =   2  'Center
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1500
         MaxLength       =   4
         TabIndex        =   21
         Text            =   "0000"
         Top             =   2790
         Width           =   435
      End
      Begin VB.TextBox txtContato 
         Alignment       =   2  'Center
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   4305
         MaxLength       =   15
         TabIndex        =   24
         Text            =   "XXXXXXXXXXXXXXX"
         Top             =   2790
         Width           =   1725
      End
      Begin VB.ComboBox cboCidade 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   7875
         TabIndex        =   18
         Text            =   "cboCidade"
         Top             =   2250
         Width           =   3060
      End
      Begin VB.ComboBox cboUF 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   7200
         TabIndex        =   17
         Text            =   "XX"
         Top             =   2250
         Width           =   660
      End
      Begin VB.TextBox txtCep 
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   5970
         MaxLength       =   8
         TabIndex        =   16
         Text            =   "00000000"
         Top             =   2250
         Width           =   825
      End
      Begin VB.TextBox txtBairro 
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   4095
         MaxLength       =   20
         TabIndex        =   15
         Text            =   "XXXXXXXXXXXXXXXXXXXX"
         Top             =   2250
         Width           =   1845
      End
      Begin VB.TextBox txtEndereco 
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   120
         MaxLength       =   33
         TabIndex        =   12
         Text            =   "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
         Top             =   2250
         Width           =   1905
      End
      Begin VB.ComboBox cboTpCliente 
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   -72120
         Style           =   2  'Dropdown List
         TabIndex        =   64
         Top             =   585
         Width           =   2775
      End
      Begin VB.ComboBox cboRepresDPK 
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   -69330
         TabIndex        =   65
         Text            =   "cboRepresDPK"
         Top             =   585
         Width           =   4620
      End
      Begin Bot�o.cmd cmdRepres 
         Height          =   630
         Left            =   -64560
         TabIndex        =   66
         ToolTipText     =   "Representantes do Cliente"
         Top             =   585
         Width           =   660
         _ExtentX        =   1164
         _ExtentY        =   1111
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadCliente.frx":1198
         PICN            =   "frmCadCliente.frx":11B4
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MSFlexGridLib.MSFlexGrid grdflexRepres 
         Height          =   2130
         Left            =   -74625
         TabIndex        =   171
         Top             =   3540
         Visible         =   0   'False
         Width           =   9570
         _ExtentX        =   16880
         _ExtentY        =   3757
         _Version        =   393216
         Cols            =   6
         BackColorBkg    =   14737632
         TextStyle       =   1
         TextStyleFixed  =   1
         GridLines       =   2
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin Bot�o.cmd cmdAdicionarRefBco 
         Height          =   705
         Left            =   -74880
         TabIndex        =   92
         Top             =   405
         Width           =   765
         _ExtentX        =   1349
         _ExtentY        =   1244
         BTYPE           =   3
         TX              =   "Adicionar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadCliente.frx":14CE
         PICN            =   "frmCadCliente.frx":14EA
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MSComctlLib.ListView lsvRefBanco 
         Height          =   4260
         Left            =   -74895
         TabIndex        =   180
         Top             =   2460
         Width           =   11595
         _ExtentX        =   20452
         _ExtentY        =   7514
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   8
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Seq"
            Object.Width           =   882
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Banco"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Dt Consulta"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Ag�ncia"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "DDD"
            Object.Width           =   1058
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Text            =   "Fone"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Text            =   "Conceito"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Text            =   "Observa��es"
            Object.Width           =   5292
         EndProperty
      End
      Begin Bot�o.cmd cmdGrRefBco 
         Height          =   705
         Left            =   -73290
         TabIndex        =   94
         Top             =   405
         Width           =   765
         _ExtentX        =   1349
         _ExtentY        =   1244
         BTYPE           =   3
         TX              =   "Gravar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadCliente.frx":1644
         PICN            =   "frmCadCliente.frx":1660
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdEditarRefBco 
         Height          =   705
         Left            =   -74085
         TabIndex        =   93
         Top             =   405
         Width           =   765
         _ExtentX        =   1349
         _ExtentY        =   1244
         BTYPE           =   3
         TX              =   "Editar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadCliente.frx":17BA
         PICN            =   "frmCadCliente.frx":17D6
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MSComctlLib.ListView lsvRefComl 
         Height          =   4470
         Left            =   -74880
         TabIndex        =   181
         Top             =   2250
         Width           =   11595
         _ExtentX        =   20452
         _ExtentY        =   7885
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   12
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Seq"
            Object.Width           =   882
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Empresa"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Dt Consulta"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "DDD"
            Object.Width           =   1058
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Fone"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Text            =   "Vl Ult Fatura"
            Object.Width           =   2469
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Text            =   "Dt Ult Fatura"
            Object.Width           =   1939
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Text            =   "Maior Sl Devedor"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Text            =   "Dt Cadastro"
            Object.Width           =   1939
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Text            =   "Dias de Atraso"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Text            =   "Conceito"
            Object.Width           =   1940
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   11
            Text            =   "Observacao"
            Object.Width           =   19402
         EndProperty
      End
      Begin Bot�o.cmd cmdAdRefComl 
         Height          =   705
         Left            =   -74865
         TabIndex        =   81
         Top             =   405
         Width           =   765
         _ExtentX        =   1349
         _ExtentY        =   1244
         BTYPE           =   3
         TX              =   "Adicionar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadCliente.frx":1930
         PICN            =   "frmCadCliente.frx":194C
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdGrRefComl 
         Height          =   705
         Left            =   -73290
         TabIndex        =   83
         Top             =   405
         Width           =   765
         _ExtentX        =   1349
         _ExtentY        =   1244
         BTYPE           =   3
         TX              =   "Gravar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadCliente.frx":1AA6
         PICN            =   "frmCadCliente.frx":1AC2
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdEdRefComl 
         Height          =   705
         Left            =   -74085
         TabIndex        =   82
         Top             =   405
         Width           =   765
         _ExtentX        =   1349
         _ExtentY        =   1244
         BTYPE           =   3
         TX              =   "Editar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadCliente.frx":1C1C
         PICN            =   "frmCadCliente.frx":1C38
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MSComctlLib.ListView lsvOutEnd 
         Height          =   3780
         Left            =   -74880
         TabIndex        =   197
         Top             =   2955
         Width           =   11595
         _ExtentX        =   20452
         _ExtentY        =   6668
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   17
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Seq"
            Object.Width           =   882
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Tp Endereco"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Tp Docto"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "CGC"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Inscr.Estadual"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Text            =   "Nome"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Text            =   "Endere�o"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Text            =   "N�mero"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Text            =   "Complemento"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Text            =   "Bairro"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Text            =   "Cep"
            Object.Width           =   1058
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   11
            Text            =   "UF"
            Object.Width           =   706
         EndProperty
         BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   12
            Text            =   "Cidade"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   13
            Text            =   "DDD"
            Object.Width           =   1058
         EndProperty
         BeginProperty ColumnHeader(15) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   14
            Text            =   "Fone"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(16) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   15
            Text            =   "Fax"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(17) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   16
            Text            =   "Cx.Postal"
            Object.Width           =   1764
         EndProperty
      End
      Begin Bot�o.cmd cmdAdOutEnd 
         Height          =   705
         Left            =   -74880
         TabIndex        =   112
         Top             =   420
         Width           =   765
         _ExtentX        =   1349
         _ExtentY        =   1244
         BTYPE           =   3
         TX              =   "Adicionar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadCliente.frx":1D92
         PICN            =   "frmCadCliente.frx":1DAE
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdGrOutEnd 
         Height          =   705
         Left            =   -73290
         TabIndex        =   114
         Top             =   420
         Width           =   765
         _ExtentX        =   1349
         _ExtentY        =   1244
         BTYPE           =   3
         TX              =   "Gravar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadCliente.frx":1F08
         PICN            =   "frmCadCliente.frx":1F24
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdEdOutEnd 
         Height          =   705
         Left            =   -74085
         TabIndex        =   113
         Top             =   420
         Width           =   765
         _ExtentX        =   1349
         _ExtentY        =   1244
         BTYPE           =   3
         TX              =   "Editar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadCliente.frx":207E
         PICN            =   "frmCadCliente.frx":209A
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdGrlsvRefComl 
         Height          =   705
         Left            =   -64065
         TabIndex        =   203
         Top             =   405
         Visible         =   0   'False
         Width           =   765
         _ExtentX        =   1349
         _ExtentY        =   1244
         BTYPE           =   3
         TX              =   "Gravar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadCliente.frx":21F4
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdGrlsvRefBanco 
         Height          =   705
         Left            =   -64095
         TabIndex        =   204
         Top             =   405
         Visible         =   0   'False
         Width           =   765
         _ExtentX        =   1349
         _ExtentY        =   1244
         BTYPE           =   3
         TX              =   "Gravar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadCliente.frx":2210
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdGrlsvOutEnd 
         Height          =   705
         Left            =   -64080
         TabIndex        =   205
         Top             =   420
         Visible         =   0   'False
         Width           =   765
         _ExtentX        =   1349
         _ExtentY        =   1244
         BTYPE           =   3
         TX              =   "Gravar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadCliente.frx":222C
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdDesfRefCom 
         Height          =   705
         Left            =   -72495
         TabIndex        =   208
         TabStop         =   0   'False
         ToolTipText     =   "Desfazer"
         Top             =   405
         Width           =   765
         _ExtentX        =   1349
         _ExtentY        =   1244
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadCliente.frx":2248
         PICN            =   "frmCadCliente.frx":2264
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdDesfRefBco 
         Height          =   705
         Left            =   -72495
         TabIndex        =   209
         TabStop         =   0   'False
         ToolTipText     =   "Desfazer"
         Top             =   405
         Width           =   765
         _ExtentX        =   1349
         _ExtentY        =   1244
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadCliente.frx":2F3E
         PICN            =   "frmCadCliente.frx":2F5A
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdDesfOutEnd 
         Height          =   705
         Left            =   -72495
         TabIndex        =   210
         TabStop         =   0   'False
         ToolTipText     =   "Desfazer"
         Top             =   420
         Width           =   765
         _ExtentX        =   1349
         _ExtentY        =   1244
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadCliente.frx":3C34
         PICN            =   "frmCadCliente.frx":3C50
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdCEP_CORREIOS 
         Height          =   450
         Left            =   10995
         TabIndex        =   220
         ToolTipText     =   "Consulta CEP - CORREIOS"
         Top             =   2160
         Width           =   645
         _ExtentX        =   1138
         _ExtentY        =   794
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadCliente.frx":492A
         PICN            =   "frmCadCliente.frx":4946
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmd1 
         Height          =   435
         Left            =   3930
         TabIndex        =   222
         ToolTipText     =   "Novo Cadastro de E-mails"
         Top             =   3255
         Width           =   525
         _ExtentX        =   926
         _ExtentY        =   767
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadCliente.frx":4E04
         PICN            =   "frmCadCliente.frx":4E20
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label lblCategBosch 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Categoria"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   -74835
         TabIndex        =   238
         Top             =   1755
         Width           =   795
      End
      Begin VB.Label Label78 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Fornecedor VDR"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   -74835
         TabIndex        =   237
         Top             =   1125
         Width           =   1335
      End
      Begin VB.Label Label77 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Tipo Segmento"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   -74835
         TabIndex        =   234
         Top             =   375
         Width           =   1260
      End
      Begin VB.Label Label76 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Complemento"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   2640
         TabIndex        =   231
         Top             =   2055
         Width           =   1185
      End
      Begin VB.Label Label75 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Num"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   2040
         TabIndex        =   230
         Top             =   2055
         Width           =   375
      End
      Begin VB.Label lblDiferenciado 
         BackColor       =   &H00FFFFFF&
         Caption         =   "Diferenciado"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   10605
         TabIndex        =   226
         Top             =   4440
         Width           =   1095
      End
      Begin VB.Label Label72 
         BackColor       =   &H00FFFFFF&
         Caption         =   "CNAE"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   135
         TabIndex        =   225
         Top             =   6240
         Width           =   495
      End
      Begin VB.Label lblVlPrevCpra 
         Alignment       =   2  'Center
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   1  'Fixed Single
         Height          =   300
         Left            =   9300
         TabIndex        =   55
         Top             =   4020
         Width           =   1395
      End
      Begin VB.Label Label71 
         AutoSize        =   -1  'True
         BackColor       =   &H00E0E0E0&
         Caption         =   "Vl.Previs�o Compra"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   9300
         TabIndex        =   219
         Top             =   3810
         Width           =   1635
      End
      Begin VB.Shape Shape5 
         BackColor       =   &H00E0E0E0&
         BackStyle       =   1  'Opaque
         Height          =   570
         Left            =   9255
         Top             =   3795
         Width           =   2475
      End
      Begin VB.Label Label74 
         AutoSize        =   -1  'True
         BackColor       =   &H00C0FFFF&
         Caption         =   "Data Limite"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   10770
         TabIndex        =   218
         Top             =   3225
         Width           =   915
      End
      Begin VB.Label Label73 
         AutoSize        =   -1  'True
         BackColor       =   &H00C0FFFF&
         Caption         =   "Limite"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   9300
         TabIndex        =   217
         Top             =   3225
         Width           =   525
      End
      Begin VB.Shape Shape4 
         BackColor       =   &H00C0FFFF&
         BackStyle       =   1  'Opaque
         Height          =   570
         Left            =   9255
         Top             =   3210
         Width           =   2475
      End
      Begin VB.Label Label70 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Tp.Empresa"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   8475
         TabIndex        =   216
         Top             =   4815
         Width           =   990
      End
      Begin VB.Label lblMsgEnd 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Registro gravado com sucesso !"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000040C0&
         Height          =   315
         Left            =   -71580
         TabIndex        =   202
         Top             =   810
         Visible         =   0   'False
         Width           =   2865
      End
      Begin VB.Label lblMsgBco 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Registro gravado com sucesso !"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000040C0&
         Height          =   315
         Left            =   -71595
         TabIndex        =   201
         Top             =   795
         Visible         =   0   'False
         Width           =   2865
      End
      Begin VB.Label lblMsgCom 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Registro gravado com sucesso !"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000040C0&
         Height          =   315
         Left            =   -71550
         TabIndex        =   200
         Top             =   795
         Visible         =   0   'False
         Width           =   2865
      End
      Begin VB.Shape shLinha5 
         BackColor       =   &H00FFFFFF&
         BackStyle       =   1  'Opaque
         Height          =   6525
         Left            =   -74985
         Top             =   345
         Width           =   11730
      End
      Begin VB.Shape shLinha3 
         BackColor       =   &H00FFFFFF&
         BackStyle       =   1  'Opaque
         Height          =   6525
         Left            =   -74940
         Top             =   345
         Width           =   11730
      End
      Begin VB.Shape Shape8 
         BackColor       =   &H00C0FFFF&
         BackStyle       =   1  'Opaque
         Height          =   300
         Left            =   150
         Top             =   5895
         Width           =   6375
      End
      Begin VB.Label Label19 
         AutoSize        =   -1  'True
         BackColor       =   &H00C0FFFF&
         Caption         =   "C�d.TARE"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   10665
         TabIndex        =   158
         Top             =   5070
         Width           =   795
      End
      Begin VB.Label Label20 
         AutoSize        =   -1  'True
         BackColor       =   &H00C0FFFF&
         Caption         =   "Data TARE"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   10680
         TabIndex        =   157
         Top             =   5640
         Width           =   810
      End
      Begin VB.Shape Shape7 
         BackColor       =   &H00C0FFFF&
         BackStyle       =   1  'Opaque
         Height          =   1140
         Left            =   10605
         Top             =   5055
         Width           =   1080
      End
      Begin VB.Label Label38 
         AutoSize        =   -1  'True
         BackColor       =   &H00C0FFFF&
         Caption         =   "Inscri��o Suframa"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   9990
         TabIndex        =   156
         Top             =   1590
         Width           =   1485
      End
      Begin VB.Shape Shape6 
         BackColor       =   &H00C0FFFF&
         BackStyle       =   1  'Opaque
         Height          =   570
         Left            =   9945
         Top             =   1575
         Width           =   1785
      End
      Begin VB.Shape Shape3 
         Height          =   555
         Left            =   120
         Top             =   4800
         Width           =   8295
      End
      Begin VB.Label Label39 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Propriet�rio"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   0
         Left            =   150
         TabIndex        =   155
         Top             =   5355
         Width           =   990
      End
      Begin VB.Label Label22 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "E-mail"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   135
         TabIndex        =   153
         Top             =   3135
         Width           =   495
      End
      Begin VB.Label Label21 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Home Page"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   4935
         TabIndex        =   152
         Top             =   3135
         Width           =   930
      End
      Begin VB.Label Label36 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Mensagem Fiscal"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Index           =   0
         Left            =   120
         TabIndex        =   151
         Top             =   4230
         Width           =   1440
      End
      Begin VB.Label Label34 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Respons�vel"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   7545
         TabIndex        =   150
         Top             =   3690
         Width           =   1065
      End
      Begin VB.Label Label33 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Dt Mensagem"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   6135
         TabIndex        =   149
         Top             =   3690
         Width           =   1140
      End
      Begin VB.Label Label35 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Mensagem Cliente"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   120
         TabIndex        =   148
         Top             =   3690
         Width           =   1560
      End
      Begin VB.Label lblTransp 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Transportadora"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   6150
         TabIndex        =   147
         Top             =   4230
         Width           =   1350
      End
      Begin VB.Label Label31 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Cheque sem fundo"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   3345
         TabIndex        =   146
         Top             =   4800
         Width           =   1590
      End
      Begin VB.Label Label30 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Banco"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   180
         TabIndex        =   145
         Top             =   4800
         Width           =   495
      End
      Begin VB.Label Label29 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Classif."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   7770
         TabIndex        =   144
         Top             =   4800
         Width           =   615
      End
      Begin VB.Label Label28 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Tipo OP"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   2070
         TabIndex        =   143
         Top             =   4800
         Width           =   630
      End
      Begin VB.Label Label27 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Protesto"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   5715
         TabIndex        =   142
         Top             =   4800
         Width           =   720
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         BackColor       =   &H00C0FFFF&
         Caption         =   "Data"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   10800
         TabIndex        =   141
         Top             =   2640
         Width           =   345
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         BackColor       =   &H00C0FFFF&
         Caption         =   "Maior Sl.Devedor"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   9300
         TabIndex        =   140
         Top             =   2640
         Width           =   1395
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H00C0FFFF&
         BackStyle       =   1  'Opaque
         Height          =   570
         Left            =   9255
         Top             =   2625
         Width           =   2475
      End
      Begin VB.Label lblDocto 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "CGC"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   2205
         TabIndex        =   139
         Top             =   1515
         Width           =   360
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Tipo Docto"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   120
         TabIndex        =   138
         Top             =   1515
         Width           =   870
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Inscr.Estadual"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   3900
         TabIndex        =   137
         Top             =   1515
         Width           =   1155
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Status"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   5310
         TabIndex        =   136
         Top             =   1515
         Width           =   525
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Caracter�stica"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   7485
         TabIndex        =   135
         Top             =   1515
         Width           =   1140
      End
      Begin VB.Label Label14 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "C.Postal"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   6030
         TabIndex        =   134
         Top             =   2595
         Width           =   675
      End
      Begin VB.Label Label15 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Telex"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   6840
         TabIndex        =   133
         Top             =   2595
         Width           =   450
      End
      Begin VB.Label Label16 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Fax"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   2940
         TabIndex        =   132
         Top             =   2595
         Width           =   270
      End
      Begin VB.Label Label17 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "DDD/Fones"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   120
         TabIndex        =   131
         Top             =   2595
         Width           =   870
      End
      Begin VB.Label Label18 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Contato"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   4320
         TabIndex        =   130
         Top             =   2595
         Width           =   645
      End
      Begin VB.Label Label13 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Cidade"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   7875
         TabIndex        =   129
         Top             =   2055
         Width           =   570
      End
      Begin VB.Label Label12 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "UF"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   7200
         TabIndex        =   128
         Top             =   2055
         Width           =   195
      End
      Begin VB.Label Label11 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "CEP"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   5970
         TabIndex        =   127
         Top             =   2055
         Width           =   315
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Bairro"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   4095
         TabIndex        =   126
         Top             =   2055
         Width           =   495
      End
      Begin VB.Label Label9 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Endere�o"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   120
         TabIndex        =   125
         Top             =   2055
         Width           =   780
      End
      Begin VB.Label Label24 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Tipo Cliente"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   -72120
         TabIndex        =   118
         Top             =   375
         Width           =   990
      End
      Begin VB.Label lblRepresDPK 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         Caption         =   "Representante DPK"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Left            =   -69285
         TabIndex        =   117
         Top             =   375
         Width           =   1590
      End
      Begin VB.Shape shLinha2 
         BackColor       =   &H00FFFFFF&
         BackStyle       =   1  'Opaque
         Height          =   6525
         Left            =   -74940
         Top             =   360
         Width           =   11730
      End
      Begin VB.Shape shLinha4 
         BackColor       =   &H00FFFFFF&
         BackStyle       =   1  'Opaque
         Height          =   6525
         Left            =   -74955
         Top             =   330
         Width           =   11730
      End
      Begin VB.Shape Shape2 
         BackColor       =   &H00FFFFFF&
         BackStyle       =   1  'Opaque
         Height          =   6525
         Left            =   120
         Top             =   360
         Width           =   11700
      End
   End
   Begin Threed.SSPanel sspCadCli 
      Align           =   2  'Align Bottom
      Height          =   615
      Left            =   0
      TabIndex        =   115
      Top             =   6975
      Width           =   11850
      _Version        =   65536
      _ExtentX        =   20902
      _ExtentY        =   1085
      _StockProps     =   15
      ForeColor       =   8388608
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BevelOuter      =   1
      Alignment       =   0
      Begin Bot�o.cmd cmdVoltar 
         Height          =   540
         Left            =   11190
         TabIndex        =   62
         ToolTipText     =   "Voltar"
         Top             =   30
         Width           =   585
         _ExtentX        =   1032
         _ExtentY        =   953
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadCliente.frx":53BA
         PICN            =   "frmCadCliente.frx":53D6
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdCancelar 
         Height          =   540
         Left            =   10440
         TabIndex        =   61
         ToolTipText     =   "Cancelar"
         Top             =   30
         Width           =   585
         _ExtentX        =   1032
         _ExtentY        =   953
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadCliente.frx":5B50
         PICN            =   "frmCadCliente.frx":5B6C
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MSComctlLib.ImageList imlFiguras 
         Left            =   30
         Top             =   0
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   32
         ImageHeight     =   32
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   5
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmCadCliente.frx":5FBE
               Key             =   ""
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmCadCliente.frx":60B2
               Key             =   ""
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmCadCliente.frx":61A7
               Key             =   ""
            EndProperty
            BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmCadCliente.frx":629C
               Key             =   ""
            EndProperty
            BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmCadCliente.frx":65B6
               Key             =   ""
            EndProperty
         EndProperty
      End
      Begin Bot�o.cmd cmdGravar 
         Height          =   540
         Left            =   9690
         TabIndex        =   60
         ToolTipText     =   "Gravar"
         Top             =   30
         Width           =   585
         _ExtentX        =   1032
         _ExtentY        =   953
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadCliente.frx":68D0
         PICN            =   "frmCadCliente.frx":68EC
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdRecusar 
         Height          =   540
         Left            =   8070
         TabIndex        =   58
         ToolTipText     =   "Recusar"
         Top             =   30
         Width           =   585
         _ExtentX        =   1032
         _ExtentY        =   953
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadCliente.frx":71C6
         PICN            =   "frmCadCliente.frx":71E2
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdAceitar 
         Height          =   525
         Left            =   7335
         TabIndex        =   57
         ToolTipText     =   "Aceitar"
         Top             =   45
         Width           =   585
         _ExtentX        =   1032
         _ExtentY        =   926
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadCliente.frx":7ABC
         PICN            =   "frmCadCliente.frx":7AD8
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdSCI 
         Height          =   540
         Left            =   5475
         TabIndex        =   56
         ToolTipText     =   "Adicionar SCI/SERASA para o Cliente"
         Top             =   30
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   953
         BTYPE           =   3
         TX              =   "Adicionar   SCI / SERASA"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   4210752
         FCOLO           =   4210752
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadCliente.frx":7DF2
         PICN            =   "frmCadCliente.frx":7E0E
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   1
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdConsSCISerasa 
         Height          =   540
         Left            =   3630
         TabIndex        =   212
         ToolTipText     =   "Ver listagem SCI/SERASA do Cliente"
         Top             =   30
         Width           =   1725
         _ExtentX        =   3043
         _ExtentY        =   953
         BTYPE           =   3
         TX              =   "Consultar   SCI / SERASA"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   4210752
         FCOLO           =   4210752
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadCliente.frx":86E8
         PICN            =   "frmCadCliente.frx":8704
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   1
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdDescartar 
         Height          =   540
         Left            =   8970
         TabIndex        =   59
         ToolTipText     =   "Descartar Consulta de Pr�-Cadastro"
         Top             =   30
         Width           =   585
         _ExtentX        =   1032
         _ExtentY        =   953
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadCliente.frx":8FDE
         PICN            =   "frmCadCliente.frx":8FFA
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
   End
   Begin VB.Label Label36 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      Caption         =   "Mensagem Fiscal"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Index           =   1
      Left            =   240
      TabIndex        =   224
      Top             =   7080
      Width           =   1440
   End
End
Attribute VB_Name = "frmCadCliente"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : frmCadCliente
' Author    : c.samuel.oliveira
' Date      : 05/02/2019
' Purpose   : TI-6870
'---------------------------------------------------------------------------------------

'---------------------------------------------------------------------------------------
' Module    : frmCadCliente
' Author    : c.samuel.oliveira
' Date      : 21/11/2017
' Purpose   : TI-6264
'---------------------------------------------------------------------------------------

'---------------------------------------------------------------------------------------
' Module    : frmCadCliente
' Author    : c.samuel.oliveira
' Date      : 23/10/15
' Purpose   : TI-3221
'---------------------------------------------------------------------------------------
'---------------------------------------------------------------------------------------
' Module    : frmCadCliente
' Author    : c.alexandre.ferreira
' Date      : 18/10/16
' Purpose   : TI-5408 - Retirada da trava que n�o permite o cadastro de cliente isento
'             ou pessoa f�sica p/endere�o de cobran�a,conforme orienta��o do setor fiscal
'---------------------------------------------------------------------------------------


Option Explicit

Private vAcao As String
Private vNomeCliente As String
Private vSaldoPedidos As Double
Private vVlMaiorSlDevGravado As String
Private vCod_GrupoMaster As String  'Guarda o c�digo do cliente que se refere ao cliente master do que est� sendo cadastrado

Dim vReprDPKGravar       As String     ' strings usadas para testar
'Dim vReprBLAUGravar      As String    ' se o representante foi alterado
Dim vReprDPKGravado      As String
'Dim vReprBLAUGravado     As String
Dim vMaior_SalDevGravado As String

'Dim vCod_tipo_clBLAU  As Integer   'Vai indicar se zerou tipo Blau ou n�o
Dim vCod_tipo_Cliente As Integer

'Dim vComboRepresBlau     As String
Dim vComboRepresDPK      As String
Dim vComboTransportadora As String

Dim vFl_Adic_OutEnd As Boolean  'Indica se foi pedido para adicionar outros end.

Dim vMaiorSlDev_Ant As Double
Dim vDtMaiorSlDev_Ant As String
Dim vMaiorSlDev_Atu As Double
Dim vDtMaiorSlDev_Atu As String
Dim vTipoDocto As String

Dim vDtTresMeses As String
' Vari�vel para gravar novo campo FL_AGRUP_DIFERENCIADA - SDS1889 - Jairo Almeida - 22-02-2012
Public strFLDiferenciado As String

Dim oServicoCliente As New ServicoClienteSAP

Sub pCarregaComboCliente(lcboCliente As ComboBox, lstrParametro As String)
    
    Dim lstrSQL      As String
    Dim lstrWHERE    As String
    Dim objCliente    As Object
    Dim lstrCliente   As String
    
    lstrWHERE = ""
    lcboCliente.Clear
    lstrParametro = UCase(lstrParametro)
    
    'NOME OU CODIGO
    
    If Trim(lstrParametro) <> "" Then
        
        If IsNumeric(Trim(lstrParametro)) Then
            
            If Len(Trim(lstrParametro)) > 6 Then ' cgc=14
                
                db.Parameters.Remove "CGC": db.Parameters.Add "CGC", lstrParametro, 1
                
                lstrWHERE = lstrWHERE & " cgc = :CGC"
                
                '---------- testa CGC / CIC -----------
                On Error Resume Next
                'If Len(lstrParametro) <> 11 And Len(lstrParametro) <> 14 Then
                '    MsgBox "CGC/CIC Inv�lido!!!", vbCritical, "Aten��o"
                '    Exit Sub
                'End If
                
                'cboTpDocto.Text = IIf(Len(lstrParametro) = 14, "1 - Pessoa Jur�dica", "2 - Pessoa F�sica")
                'txtDocto = Format(lstrParametro, IIf(Len(lstrParametro) = 14, "@@.@@@.@@@/@@@@-@@", "@@@.@@@.@@@-@@"))
                
                If Trim(cboTpDocto) = "" And Len(Trim(lstrParametro)) > 6 Then
                    If Len(lstrParametro) <= 11 And (vFl_Analise = False And vFl_Consulta = False) Then  '� cliente novo e � pessoa f�sica
                        cboTpDocto = "2 - PESSOA FISICA"
                  
                    'Inserido dia 31/08/05 - Conforme solicitacao da Alethea
                    ElseIf Len(lstrParametro) <= 11 And (vFl_Analise = True Or vFl_Consulta = True) Then '� cliente novo e � pessoa f�sica
                        cboTpDocto = "2 - PESSOA FISICA"
                    
                    ElseIf Len(lstrParametro) > 11 Or (vFl_Analise = True Or vFl_Consulta = True) Then '� cliente novo e � pessoa jur�dica
                        cboTpDocto = "1 - PESSOA JURIDICA"
                    End If
                End If
                
                'Valida CGC ou CPF - acertando o tipo do documento de acordo com o que foi v�lido
                If Val(cboTpDocto.Text) = 1 Then
                    
                    'Se o CGC n�o for v�lido, ent�o verificar se passa como CPF
                    If Not vVB_Generica_001.ValidaCGC(lstrParametro) Then
                        If Not vVB_Generica_001.ValidaCIC(lstrParametro) Then
                            MsgBox "N�mero de Documento Inv�lido (CGC e CPF)!!!", vbCritical, "Aten��o"
                            Exit Sub
                        Else
                            'Se trata-se de CPF, acerta o tipo do documento
                            cboTpDocto = "2 - PESSOA FISICA"
                        End If
                    '� um CGC v�lido
                    Else
                        cboTpDocto = "1 - PESSOA JURIDICA"
                    End If
                
                Else
                    
                    'Se o CPF n�o for v�lido, ent�o verificar se passa como CGC
                    If Not vVB_Generica_001.ValidaCIC(lstrParametro) Then
                        If Not vVB_Generica_001.ValidaCGC(lstrParametro) Then
                            MsgBox "N�mero de Documento Inv�lido (CGC e CPF)!!!", vbCritical, "Aten��o"
                            Exit Sub
                        Else
                            'Se trata-se de CGC, acerta o tipo do documento
                            cboTpDocto = "1 - PESSOA JURIDICA"
                        End If
                    '� um CGC v�lido
                    Else
                        cboTpDocto = "2 - PESSOA FISICA"
                    End If
                
                End If
                
                'cboCliente = ""
            
            Else ' codigo=6
                
                db.Parameters.Remove "CODCLI": db.Parameters.Add "CODCLI", lstrParametro, 1
                lstrWHERE = lstrWHERE & " cod_cliente = :CODCLI "
            
            End If
        
        Else
    
            'SE NAO FOR NUMERICO E NAO TIVER '%'
            If Right(lstrParametro, 1) <> "%" And Left(lstrParametro, 1) <> "%" And Trim(txtCodCliente) = "" Then
                lcboCliente.Text = lstrParametro
                txtDtCadastro = vDtHoje
                Exit Sub
            End If
            
            db.Parameters.Remove "NOME": db.Parameters.Add "NOME", lstrParametro, 1
            
            lstrWHERE = lstrWHERE & " nome_cliente " & _
                        IIf(Left(lstrParametro, 1) = "%" Or _
                           Right(lstrParametro, 1) = "%", " LIKE ", "=") & " :NOME "
        
        End If
    
    End If
    
   
    lstrSQL = "SELECT Cod_cliente, Nome_cliente " & _
                "FROM Producao.Cliente"
    
    lstrSQL = lstrSQL & IIf(lstrWHERE = "", "", " WHERE " & lstrWHERE)
    
    Set objCliente = db.dbcreatedynaset(lstrSQL, 0&)
    
    lcboCliente.Clear
    Screen.MousePointer = vbHourglass
    While Not objCliente.EOF
        'MDIForm1.sspMsg.Caption = "PESQUISANDO CLIENTES . . . " & lcboCliente.ListCount + 1
        lcboCliente.AddItem objCliente.Fields("nome_cliente") & " - " & Format(objCliente.Fields("cod_cliente"), "000000")
        lstrCliente = objCliente.Fields("nome_cliente") & " - " & Format(objCliente.Fields("cod_cliente"), "000000")
        objCliente.MoveNext
    Wend
    
    Screen.MousePointer = vbDefault
    If objCliente.RecordCount <= 1 Then
        If objCliente.RecordCount = 0 Then
                            
            Dim retorno As RetornoPI
            ' Se n�o encontrou cliente no legado busca no SAP atrav�s do n�mero do documento.
            Set retorno = ConsultarSAP(Val(cboTpDocto.Text), Trim(lstrParametro), "", False)
            
            Fl_Cliente_Novo = "S"
            Fl_Ja_Relacionou = "N"
            
            ' C�digo W � retornado quando SAP n�o encontra o cliente, ou seja, cliente novo.
            If retorno.CodErro = "W" Then
                cboTpDocto.Enabled = True
                txtDocto.Enabled = True
            ElseIf Not retorno.Sucesso Then
                cboTpDocto.Enabled = False
                txtDocto.Enabled = False
                MsgBox "Erro ao buscar cliente no SAP: " & retorno.mensagem, vbInformation, "Aten��o"
            End If
            
            'Se o que foi digitado foi um cgc ou cpf, guarda no campo de no.de documento
            If IsNumeric(Trim(lstrParametro)) And Len(lstrParametro) > 6 Then
                txtDocto.Text = lstrParametro
            ElseIf Not IsNumeric(lstrParametro) Then
                lcboCliente.Text = Left(lstrParametro, 30)
            'Else
            '    lcboCliente.SetFocus
            End If
            pictSituacao.Picture = imlFiguras.ListImages(5).Picture
            txtSituacao.Text = "0 - Ativo"
            lcboCliente.SetFocus
           
        Else
            Fl_Cliente_Novo = "N"
            Fl_Ja_Relacionou = "S"
            'cboRepresentanteDPK.Enabled = False
            objCliente.MoveFirst
            lcboCliente.Text = objCliente.Fields("nome_cliente")
            txtCodCliente = objCliente.Fields("cod_cliente")
            Call pCarregaDados
        End If
    Else
        MsgBox "Encontrei mais de um Cliente." & Chr(10) & "Selecione um !", vbInformation, "Aten��o"
    End If

End Sub


Sub pCarregaCliMaster(lcboCliente As ComboBox, lstrParametro As String)
    
    
    Dim lstrSQL      As String
    Dim lstrWHERE    As String
    Dim objCliente    As Object
    Dim lstrCliente   As String
    
    lstrWHERE = ""
    lcboCliente.Clear
    lstrParametro = UCase(lstrParametro)
    
    'NOME OU CODIGO
    If Trim(lstrParametro) <> "" Then
        
        If IsNumeric(Trim(lstrParametro)) Then
            
            If Len(Trim(lstrParametro)) > 6 Then ' cgc=14
                
                db.Parameters.Remove "CGC": db.Parameters.Add "CGC", lstrParametro, 1
                
                lstrWHERE = lstrWHERE & " cgc = :CGC"
                
                '---------- testa CGC / CIC -----------
                On Error Resume Next
                If Len(lstrParametro) <> 11 And Len(lstrParametro) <> 14 Then
                    MsgBox "CGC/CIC Inv�lido!!!", vbCritical, "Aten��o"
                    Exit Sub
                End If
                
                'Valida CGC ou CPF
                If Val(cboTpDocto.Text) = 1 Then
                    If Not vVB_Generica_001.ValidaCGC(lstrParametro) Then
                        MsgBox "CGC Inv�lido!!!", vbCritical, "Aten��o"
                        Exit Sub
                    End If
                Else
                    If Not vVB_Generica_001.ValidaCIC(lstrParametro) Then
                        MsgBox "CPF Inv�lido!!!", vbCritical, "Aten��o"
                        Exit Sub
                    End If
                End If
                
                'cboCliente = ""
            
            Else ' codigo=6
                
                db.Parameters.Remove "CODCLI": db.Parameters.Add "CODCLI", lstrParametro, 1
                lstrWHERE = lstrWHERE & " cod_cliente = :CODCLI "
            
            End If
        
        Else
            
            'SE NAO FOR NUMERICO E NAO TIVER '%'
            If Right(lstrParametro, 1) <> "%" And Left(lstrParametro, 1) <> "%" And Trim(txtCodCliente) = "" Then
                lcboCliente.Text = lstrParametro
                Exit Sub
            End If
            
            db.Parameters.Remove "NOME": db.Parameters.Add "NOME", lstrParametro, 1
            
            lstrWHERE = lstrWHERE & " nome_cliente LIKE :NOME "
        
        End If
    
    End If
    
   
    lstrSQL = "SELECT Cod_cliente, Nome_Cliente " & _
                "FROM Producao.Cliente"
    
    lstrSQL = lstrSQL & IIf(lstrWHERE = "", "", " WHERE " & lstrWHERE)
    
    Set objCliente = db.dbcreatedynaset(lstrSQL, 0&)
    
    lcboCliente.Clear
    Screen.MousePointer = vbHourglass
    
    While Not objCliente.EOF
        lcboCliente.AddItem Mid(objCliente.Fields("nome_cliente"), 1, 10) & " - " & Format(objCliente.Fields("cod_cliente"), "000000")
        lstrCliente = Mid(objCliente.Fields("nome_cliente"), 1, 10) & " - " & Format(objCliente.Fields("cod_cliente"), "000000")
        objCliente.MoveNext
    Wend
    
    Screen.MousePointer = vbDefault
    If objCliente.RecordCount <= 1 Then
        If objCliente.RecordCount = 0 Then
            MsgBox "Cliente n�o Encontrado !", vbInformation, "Aten��o"
            If Not IsNumeric(lstrParametro) Then
                lcboCliente.Text = Left(lstrParametro, 10)
            Else
                lcboCliente.SetFocus
            End If
        Else
            objCliente.MoveFirst
            lcboCliente.Text = Mid(objCliente.Fields("nome_cliente"), 1, 10) & " - " & Format(objCliente.Fields("cod_cliente").Value, "000000")
        End If
    Else
        MsgBox "Encontrei mais de um Cliente." & Chr(10) & "Selecione um !", vbInformation, "Aten��o"
    End If

End Sub

'Sempre que for da tabela da Intranet.CLiente_Web
'Ser� apenas 1 registro
Sub Carregar_Referencias_Intranet(rst As Object)
    
    Dim litem As ListItem
    
    If Not IsNull(rst("Banco_Ref_Bco")) Then
        Set litem = lsvRefBanco.ListItems.Add
        litem = 1
        litem.SubItems(1) = IIf(IsNull(rst("Banco_Ref_Bco")), "", rst("Banco_Ref_Bco"))
        litem.SubItems(2) = vDtHoje
        litem.SubItems(3) = IIf(IsNull(rst("Agencia_ref_bco")) Or Trim("Agencia_ref_bco") = 0, "", rst("Agencia_ref_bco"))
        litem.SubItems(4) = IIf(IsNull(rst("DDD_ref_bco")) Or Trim("DDD_ref_bco") = 0, "", rst("DDD_ref_bco"))
        litem.SubItems(5) = IIf(IsNull(rst("FONE_REF_BCO")) Or Trim("FONE_REF_BCO") = 0, "", rst("FONE_REF_BCO"))
        litem.SubItems(6) = ""
        litem.SubItems(7) = ""
    End If

End Sub

'Sempre que for da tabela da Intranet.CLiente_Web
'Ser� apenas 1 registro
Sub Carregar_Ref_Coml_Intranet(rst As Object)
    
    Dim litem As ListItem
    
    For I = 5 To 1 Step -1
        If IsNull(rst("Empresa" & I & "_Ref_Com")) Then
            I = I - 1
        Else
            Set litem = lsvRefComl.ListItems.Add
            litem = I
            litem.SubItems(1) = IIf(IsNull(rst("Empresa" & I & "_Ref_Com")), "", rst("Empresa" & I & "_Ref_Com"))
            litem.SubItems(2) = vDtHoje
            litem.SubItems(3) = IIf(IsNull(rst("DDD" & I & "_Ref_Com")) Or Trim("DDD" & I & "_Ref_Com") = 0, "", rst("DDD" & I & "_Ref_Com"))
            litem.SubItems(4) = IIf(IsNull(rst("Fone" & I & "_Ref_Com")) Or Trim("Fone" & I & "_Ref_Com") = 0, "", rst("Fone" & I & "_Ref_Com"))
            litem.SubItems(5) = ""
            litem.SubItems(6) = ""
            litem.SubItems(7) = ""
            litem.SubItems(8) = ""
            litem.SubItems(9) = ""
            litem.SubItems(10) = ""
            litem.SubItems(11) = IIf((Val(vCod_GrupoMaster) <> 0 And I = 1), "MESMO GRUPO CLIENTE: " & cboCodCliGrMaster.Text, "")
        End If
    Next I

End Sub

'Sempre que for da tabela da Intranet.CLiente_Web
'Ser� apenas 1 registro
Sub Carregar_OutEnd_Intranet(rst As Object)
    
    Dim litem As ListItem
    Dim endereco As String
    Dim Num As String
    Dim compl As String
        
    If Not IsNull(rst("TP_DOCTO_COB")) Then
        Set litem = lsvOutEnd.ListItems.Add
        litem = 1
        litem.SubItems(1) = "2 - COBRAN�A"
        If IsNull(rst("TP_DOCTO_COB")) Then
            litem.SubItems(2) = ""
        Else
            litem.SubItems(2) = IIf(Val(rst("TP_DOCTO_COB")) = 1, "1 - PESSOA JURIDICA", "2 - PESSOA FISICA")
        End If
        'litem.SubItems(2) = IIf(IsNull(rst("TP_DOCTO_COB")), "", rst("TP_DOCTO_COB"))
        litem.SubItems(3) = IIf(IsNull(rst("CGC_Cob")), "", rst("CGC_Cob"))
        litem.SubItems(4) = IIf(IsNull(rst("Inscr_Estadual_Cob")), "", rst("Inscr_Estadual_Cob"))
        litem.SubItems(5) = IIf(IsNull(rst("Nome_Cliente_Cob")), "", rst("Nome_Cliente_Cob"))
        
        'Kevlin Toshinari Ossada - CIT - 22/8/2012 - Desconcatena valor endere�o do SGDB e seta campos da grid
        DesconcatenarEnderecoSAP rst("Endereco_Cob"), endereco, Num, compl
        'litem.SubItems(6) = IIf(IsNull(rst("Endereco_Cob")), "", rst("Endereco_Cob"))
        litem.SubItems(6) = IIf(IsNull(endereco), "", endereco)
        'vxavier - CIT - 29/08/2012 - Caso o n�mero esteja vazio colocar zero
        If Num = "" Or IsNull(Num) Then
            litem.SubItems(7) = 0
        Else
            litem.SubItems(7) = Num
        End If
        litem.SubItems(8) = IIf(IsNull(compl), "", compl)
        
        litem.SubItems(9) = IIf(IsNull(rst("Bairro_Cob")), "", rst("Bairro_Cob"))
        'vxavier CIT 11/12/2012 - Defect #360 - CAD060 - Ceps com zeros a esquerda s�o salvos sem o zero
        litem.SubItems(10) = IIf(IsNull(rst("Cep_Cob")), "", Format(rst("Cep_Cob"), "00000000"))
        litem.SubItems(11) = IIf(IsNull(rst("Cod_Cidade_Cob")), "", rst("Cod_Uf"))
        litem.SubItems(12) = IIf(IsNull(rst("Cod_Cidade_Cob")), "", rst("Nome_Cidade") & " - " & Format(rst("Cod_Cidade_Cob"), "0000"))
        litem.SubItems(13) = ""
        litem.SubItems(14) = ""
        litem.SubItems(15) = ""
        litem.SubItems(16) = ""
    End If

End Sub


Sub Carregar_Referencias_Producao(Qtd As Long, rst As Object)
    
    Dim litem As ListItem
    
    For I = 1 To Qtd
        Set litem = lsvRefBanco.ListItems.Add
        litem = rst("Sequencia")
        litem.SubItems(1) = IIf(IsNull(rst("Banco")), "", rst("Banco"))
        litem.SubItems(2) = IIf(IsNull(rst("Dt_Consulta")), "", rst("Dt_Consulta"))
        litem.SubItems(3) = IIf(IsNull(rst("Agencia")) Or Trim(rst("Agencia")) = 0, "", rst("Agencia"))
        litem.SubItems(4) = IIf(IsNull(rst("DDD")) Or Trim(rst("DDD")) = 0, "", rst("DDD"))
        litem.SubItems(5) = IIf(IsNull(rst("FONE")) Or Trim(rst("FONE")) = 0, "", rst("FONE"))
        litem.SubItems(6) = IIf(IsNull(rst("CONCEITO")) Or Trim(rst("CONCEITO")) = 0, "", rst("CONCEITO"))
        litem.SubItems(7) = IIf(IsNull(rst("OBSERVACAO")), "", rst("OBSERVACAO"))
        rst.MoveNext
    Next

End Sub

Sub Carregar_Ref_Coml_Producao(Qtd As Long, rst As Object)
    
    Dim litem As ListItem
    
    For I = 1 To Qtd
        Set litem = lsvRefComl.ListItems.Add
        litem = rst("Sequencia")
        litem.SubItems(1) = IIf(IsNull(rst("Empresa")), "", rst("Empresa"))
        litem.SubItems(2) = IIf(IsNull(rst("Dt_Consulta")), "", rst("Dt_Consulta"))
        litem.SubItems(3) = IIf(IsNull(rst("DDD")) Or Trim(rst("DDD")) = 0, "", rst("DDD"))
        litem.SubItems(4) = IIf(IsNull(rst("Fone")) Or Trim(rst("Fone")) = 0, "", rst("Fone"))
        litem.SubItems(5) = IIf(IsNull(rst("Ult_Fatura")) Or Trim(rst("Ult_Fatura")) = 0, "", rst("Ult_Fatura"))
        litem.SubItems(6) = IIf(IsNull(rst("Dt_Ult_Fatura")), "", rst("Dt_Ult_Fatura"))
        litem.SubItems(7) = IIf(IsNull(rst("Maior_SalDev")) Or Trim(rst("Maior_SalDev")) = 0, "", rst("Maior_SalDev"))
        litem.SubItems(8) = IIf(IsNull(rst("Dt_Cadastr")), "", rst("Dt_Cadastr"))
        litem.SubItems(9) = IIf(IsNull(rst("Atraso_Medio")) Or Trim(rst("Atraso_Medio")) = 0, "", rst("Atraso_Medio"))
        litem.SubItems(10) = IIf(IsNull(rst("Conceito_Empresa")) Or Trim(rst("Conceito_Empresa")) = 0, "", rst("Conceito_Empresa"))
        litem.SubItems(11) = IIf(IsNull(rst("Observacao")), "", rst("Observacao"))
        rst.MoveNext
    Next I

End Sub

Sub Carregar_OutEnd_Producao(Qtd As Long, rst As Object)
    
    Dim litem As ListItem
    
    Dim endereco As String
    Dim Num As String
    Dim compl As String
    
    
    For I = 1 To Qtd
        Set litem = lsvOutEnd.ListItems.Add
        litem = rst("Sequencia")
        litem.SubItems(1) = IIf(IsNull(rst("Tp_Endereco")), "", _
                                IIf(Val(rst("Tp_Endereco")) = 1, "1 - ENTREGA", "2 - COBRAN�A"))
        If IsNull(rst("Tp_Docto")) Then
            litem.SubItems(2) = ""
        Else
            litem.SubItems(2) = IIf(Val(rst("Tp_Docto")) = 1, "1 - PESSOA JURIDICA", "2 - PESSOA FISICA")
        End If
        
        litem.SubItems(3) = IIf(IsNull(rst("CGC")), "", rst("CGC"))
        litem.SubItems(4) = IIf(IsNull(rst("Inscr_Estadual")), "", rst("Inscr_Estadual"))
        litem.SubItems(5) = IIf(IsNull(rst("Nome_Cliente")), "", rst("Nome_Cliente"))
        
        Call DesconcatenarEnderecoSAP(rst("Endereco"), endereco, Num, compl)
        
        'Kevlin Toshinari Ossda - CIT - 22/8/2012 - Desconcatena valor endere�o do SGDB e seta campos da grid
        'litem.SubItems(6) = IIf(IsNull(rst("Endereco")), "", rst("Endereco"))
        litem.SubItems(6) = IIf(IsNull(endereco), "", endereco)
        'vxavier - CIT - 29/08/2012 - Caso o n�mero esteja vazio colocar zero
        If Num = "" Or IsNull(Num) Then
            litem.SubItems(7) = 0
        Else
            litem.SubItems(7) = Num
        End If
        litem.SubItems(8) = IIf(IsNull(compl), "", compl)
        
        litem.SubItems(9) = IIf(IsNull(rst("Bairro")), "", rst("Bairro"))
        'vxavier CIT 11/12/2012 - Defect #360 - CAD060 - Ceps com zeros a esquerda s�o salvos sem o zero
        litem.SubItems(10) = IIf(IsNull(rst("Cep")), "", Format(rst("Cep"), "00000000"))
        litem.SubItems(11) = IIf(IsNull(rst("Cod_Cidade")), "", rst("Cod_Uf"))
        litem.SubItems(12) = IIf(IsNull(rst("Cod_Cidade")), "", rst("Nome_Cidade") & " - " & rst("Cod_Cidade"))
        litem.SubItems(13) = IIf(IsNull(rst("DDD")), "", rst("DDD"))
        litem.SubItems(14) = IIf(IsNull(rst("Fone")), "", rst("Fone"))
        litem.SubItems(15) = IIf(IsNull(rst("Fax")), "", rst("Fax"))
        litem.SubItems(16) = IIf(IsNull(rst("CxPostal")), "", rst("CxPostal"))
        rst.MoveNext
    Next I

End Sub

'Sub Carregar_Vendor_Producao(Qtd As Long, rst As Object)
'    Dim litem As ListItem
'    Dim i As Integer
'
'    For i = 1 To Qtd
'        Set litem = Me.lsvVendor.ListItems.Add
'        litem = rst("Num_Contrato")
'        litem.SubItems(1) = IIf(IsNull(rst("Dt_Contrato")), "", rst("Dt_Contrato"))
'        litem.SubItems(2) = IIf(IsNull(rst("Vl_Limite")), "", rst("Vl_Limite"))
'        litem.SubItems(3) = IIf(IsNull(rst("Situacao")), "", rst("Situacao"))
'        rst.movenext
'    Next i
'End Sub

'Sub Alinha_Shapes()
'
'    With shLinha1
'        .Top = 345
'        .Height = 5610
'        .Left = 45
'        .Width = 11730
'    End With
'    With shLinha2
'        .Top = 345
'        .Height = 5610
'        .Left = 45
'        .Width = 11730
'    End With
'    With shLinha3
'        .Top = 345
'        .Height = 5610
'        .Left = 45
'        .Width = 11730
'    End With
'    With shLinha4
'        .Top = 345
'        .Height = 5610
'        .Left = 45
'        .Width = 11730
'    End With
'    With shLinha5
'        .Top = 345
'        .Height = 5610
'        .Left = 45
'        .Width = 11730
'    End With
'    'With shLinha6
'    '    .Top = 345
'    '    .Height = 5610
'    '    .Left = 45
'    '    .Width = 11730
'    'End With
'
'End Sub


Sub Carregar_Dados_Pre_Cadastro(pCGC As String, pTPSOLIC As Integer)
    
    Dim objCliente As Object, objRepres As Object, I As Integer
    Dim vReprAux As Long
    Dim oRetornoPI As RetornoPI
    Dim rua As String
    Dim numero As String
    Dim complemento As String
    
    db_INT.Parameters.Remove "CGC": db_INT.Parameters.Add "CGC", Val(pCGC), 1
    db_INT.Parameters.Remove "TPSOL": db_INT.Parameters.Add "TPSOL", pTPSOLIC, 1

    'J� existe uma PACKAGE para este Select no PCK_CAD060.Pr_Select_Cliente_Pre_Cadastro
    Sql = "Select To_Char(C.CGC,'00000000000000') CGC, " & _
                " C.Nome_Cliente, " & _
                " C.Nome_Contato, " & _
                " C.Endereco, " & _
                " C.Cod_Cidade, CI.Nome_Cidade, CI.Cod_Uf, " & _
                " C.Bairro, " & _
                " C.DDD1 ddd1c, C.Fone1 fone1c, Nvl(C.DDD2,0) DDD2c, Nvl(C.Fone2,0) FONE2c, " & _
                " C.Cep, " & _
                " C.Inscr_Estadual, " & _
                " Nvl(C.Tp_Empresa,'') TP_EMPRESA, " & _
                " Nvl(C.Inscr_Suframa,'') INSCR_SUFRAMA, " & _
                " C.Cod_Tipo_Cliente, TC.Desc_Tipo_Cli, " & _
                " Nvl(C.Cod_Repr_Vend,0) COD_REPR_VEND, R.Nome_Repres, " & _
                " To_Char(C.Dt_Fundacao,'dd/mm/yy') Dt_Fundacao, C.Propriedade, " & _
                " C.Nome_Proprietario, " & _
                " Nvl(C.Fl_Cons_Final,'') FL_CONS_FINAL, " & _
                " Nvl(C.Fl_Categ_Bosch_Diesel,'') FL_CATEG_BOSCH_DIESEL, " & _
                " Nvl(C.Fl_Grupo_Master,'') FL_GRUPO_MASTER, " & _
                " Nvl(C.Cod_Tare,0) COD_TARE, " & _
                " Nvl(To_Char(C.Dt_Tare,'dd/mm/yy'),'') DT_TARE, " & _
                " Nvl(C.Cod_Mensagem_Fiscal,0) CODFIS, M.Desc_Mens MSGFIS, " & _
                " Nvl(C.E_Mail,'') E_MAIL, Nvl(C.HomePage,'') HOMEPAGE, " & _
                " To_Char(C.Dt_Solicitacao,'dd/mm/yy HH24:MI:SS') Dt_Solicitacao, " & _
                " CT.Cod_Usuario, U.Nome_Usuario, " & _
                " Nvl(C.Cod_Motivo_Recusa_Ant,0) COD_MOTIVO_RECUSA_ANT, MR.Descricao, "
    'Refer�ncias Comerciais
    Sql = Sql & " Nvl(C.Empresa1_Ref_Com,'') EMP1, " & _
                " Nvl(C.DDD1_Ref_Com,0) DDD1, Nvl(C.Fone1_Ref_Com,0) FONE1, " & _
                " Nvl(C.Empresa2_Ref_Com,'') EMP2, " & _
                " Nvl(C.DDD2_Ref_Com,0) DDD2, Nvl(C.Fone2_Ref_Com,0) FONE2, " & _
                " Nvl(C.Empresa3_Ref_Com,'') EMP3, " & _
                " Nvl(C.DDD3_Ref_Com,0) DDD3, Nvl(C.Fone3_Ref_Com,0) FONE3, " & _
                " Nvl(C.Empresa4_Ref_Com,'') EMP3, " & _
                " Nvl(C.DDD4_Ref_Com,0) DDD4, Nvl(C.Fone4_Ref_Com,0) FONE3, " & _
                " Nvl(C.Empresa5_Ref_Com,'') EMP3, " & _
                " Nvl(C.DDD5_Ref_Com,0) DDD5, Nvl(C.Fone5_Ref_Com,0) FONE5, "
    'Refer�ncias Banc�rias
    Sql = Sql & " Nvl(C.Banco_Ref_Bco,'') BCO_BCO, " & _
                " Nvl(C.Agencia_Ref_Bco,0) AG_BCO, Nvl(C.DDD_Ref_Bco,0) DDD_BCO, "
    'Endere�o de Cobran�a
    Sql = Sql & " Nvl(C.Tp_Docto_Cob,0) TPDOC_COB, " & _
                " Nvl(C.CGC_Cob,0) CGC_COB, Nvl(C.Nome_Cliente_Cob,'') NOME_COB, " & _
                " Nvl(C.Endereco_Cob,'') END_COB, Nvl(C.Cod_Cidade_Cob,0) CID_COB, " & _
                " CI2.Nome_Cidade, CI2.Cod_Uf, " & _
                " Nvl(C.Bairro_Cob,'') BAI_COB, Nvl(C.Cep_Cob,0) CEP_COB, " & _
                " Nvl(C.Inscr_Estadual_Cob,'') INSR_EST_COB "
    Sql = Sql & "From Intranet.Cliente_Web C, " & _
                    " Producao.Cidade CI, " & _
                    " Producao.Cidade CI2, " & _
                    " Producao.Representante R, " & _
                    " Intranet.Controle_Cliente_Web CT, " & _
                    " Producao.V_Usuario_Intranet U, " & _
                    " Producao.Clie_Mensagem M, " & _
                    " Producao.Tipo_Cliente TC, " & _
                    " Credito.Motivo_Recusa MR " & _
               "Where C.CGC = :CGC " & _
                 "And C.Tp_Solicitacao = :TPSOL " & _
                 "And C.CGC = CT.CGC " & _
                 "And C.Tp_Solicitacao = CT.Tp_Solicitacao " & _
                 "And C.Cod_Tipo_Cliente = TC.Cod_Tipo_Cli " & _
                 "And C.Cod_Cidade = CI.Cod_Cidade " & _
                 "And CT.Cod_Usuario = U.Cod_Usuario " & _
                 "And C.Cod_Cidade = CI2.Cod_Cidade(+) " & _
                 "And C.Cod_Repr_Vend = R.Cod_Repres(+) " & _
                 "And C.Cod_Mensagem_Fiscal = M.Cod_Mensagem(+) " & _
                 "And C.Cod_Motivo_Recusa_Ant = MR.Cod_Motivo(+) " & _
                 "And (CT.Situacao = 0 Or CT.Situacao = 1)"

    Set objCliente = db_INT.createDynaset(Sql, 0&)

    If objCliente.EOF Then
        MsgBox "Problemas para trazer dados do cliente a ser analisado." & Chr(13) & _
               "Avise Depto.Suporte!", vbCritical, "CAD060 - Aten��o"
        Exit Sub
    End If

    Fl_Cliente_Novo = "S"
    'CARREGA DADOS DO PR�-CADASTRO NO FORMUL�RIO
    txtCodCliente.Text = ""
    cboCliente.Text = IIf(IsNull(objCliente!nome_cliente.Value), "", objCliente!nome_cliente.Value)
    txtDtCadastro.Text = vDtHoje
    txtDtCadastro.Locked = True
    txtDtRecadastro.Text = ""
    txtDtSolicitacao.Text = Format(objCliente!Dt_Solicitacao.Value, "DD/MM/YY HH:MM:SS")
    txtSituacao.Text = "0 - ATIVO"
    pictSituacao.Picture = imlFiguras.ListImages(5).Picture
    
    'Se houver c�digo de recusa anterior, mostrar mensagem para o cr�dito
    If Val(objCliente!Cod_Motivo_Recusa_Ant.Value) <> 0 Then
       'fraRecusado.Visible = True
       lblRecusa2.Visible = True
       lblRecusado2.Visible = True

       lblRecusado2.Caption = IIf(IsNull(objCliente!Descricao.Value), "", objCliente!Descricao.Value)
    Else
       'fraRecusado.Visible = False
        lblRecusa2.Visible = False
        lblRecusado2.Visible = False
    End If
    
    
    '--------------------------------
    'TIPO / No. DOCUMENTO (CGC OU CPF)
    '--------------------------------
    cboTpDocto.Text = "1 - PESSOA JURIDICA"
    Call cboTpDocto_Click
    txtDocto.Text = IIf(IsNull(objCliente!Cgc.Value), "", objCliente!Cgc.Value)
    
    ' vxavier CIT 11/12/2012 - Preencher combo segmento
    If (objCliente!Cod_Tipo_Cliente.Value <> "") Then
    
        'seta o combo tipo segmento
        Call SetarComboTipoSegmento(cboTpSegmento, Format(objCliente!Cod_Tipo_Cliente.Value, "00"))
        If (cboTpCliente.ListCount <> 0) Then
            cboTpCliente.ListIndex = 0
        End If
     End If
    
    '--------------------------------
    'INSCRI��O ESTADUAL
    '--------------------------------
    txtInscrEstadual.Text = IIf(IsNull(objCliente!INSCR_ESTADUAL.Value), "", objCliente!INSCR_ESTADUAL.Value)
    '--------------------------------
    'INFORMA��ES GERAIS
    '--------------------------------
    'Campos com valores pr�-definidos pelo Depto.Cr�dito
    cboStatus.Text = "0 - NORMAL"
    cboCaracteristica.Text = "N - NORMAL"
    '---------------------------------
    
    Call DesconcatenarEnderecoSAP(objCliente!endereco.Value, rua, numero, complemento)
    
    txtEndereco.Text = rua
    If numero = "" Then
        txtNumero.Text = 0
    Else
        txtNumero.Text = numero
    End If
    txtComplemento.Text = complemento
    
    'txtEndereco.Text = IIf(IsNull(objCliente!endereco.Value), "", objCliente!endereco.Value)
    txtBairro.Text = IIf(IsNull(objCliente!Bairro.Value), "", objCliente!Bairro.Value)
    txtCep.Text = IIf(IsNull(objCliente!CEP.Value), "", Format(objCliente!CEP.Value, "00000000"))
    cboUF.Text = IIf(IsNull(objCliente!COD_UF.Value), "", objCliente!COD_UF.Value)
    Call cboUF_Click
    cboCidade.Text = IIf(IsNull(objCliente!nome_cidade.Value), "", objCliente!nome_cidade.Value & " - " & Format(objCliente!COD_CIDADE.Value, "0000"))
    txtDDD1.Text = IIf(IsNull(objCliente!ddd1c.Value), "", objCliente!ddd1c.Value)
    txtFone1.Text = IIf(IsNull(objCliente!fone1c.Value), "", objCliente!fone1c.Value)
    txtDDD2.Text = IIf(IsNull(objCliente!ddd2c.Value), "", objCliente!ddd2c.Value)
    txtFone2.Text = IIf(IsNull(objCliente!fone2c.Value), "", objCliente!fone2c.Value)
    txtFax.Text = ""
    txtContato.Text = IIf(IsNull(objCliente!NOME_CONTATO.Value), "", objCliente!NOME_CONTATO.Value)
    txtCxPostal.Text = ""
    txtTelex.Text = ""
    
    If IsNull(objCliente!tp_empresa.Value) Then
        'cboTpEmpresa.Text = ""
        cboTpEmpresa.ListIndex = -1
    Else
        If objCliente!tp_empresa.Value = "M" Then
            cboTpEmpresa.Text = "M - MICROEMPRESA"
        ElseIf objCliente!tp_empresa.Value = "P" Then
            cboTpEmpresa.Text = "P - EMPRESA PEQ.PORTE"
        Else
            cboTpEmpresa.Text = "E - EMPRESA"
        End If
    End If
   
    '--------------------------------
    'CLIENTE COM DADOS INTERNET - E-MAIL/HOMEPAGE
    '--------------------------------
    txtEmail.Text = IIf(IsNull(objCliente!E_Mail.Value), "", objCliente!E_Mail.Value)
    txtHomePage.Text = IIf(IsNull(objCliente!HomePage.Value), "", objCliente!HomePage.Value)
    '--------------------------------
    'TIPO DE CLIENTE DPK E BLAU
    '--------------------------------
    'cboTpCliente.Text = IIf(IsNull(objCliente!Desc_Tipo_Cli.Value), "", objCliente!Desc_Tipo_Cli.Value & " - " & Format(objCliente!Cod_Tipo_Cliente.Value, "00"))
    'cboTpClienteBlau.Text = ""
    'vCod_tipo_clBLAU = 0
    vCod_tipo_Cliente = Val(Right(cboTpCliente.Text, 2))
    '--------------------------------
    'REPRESENTANTE DPK
    '--------------------------------
    vReprDPKGravado = IIf(IsNull(objCliente!Cod_Repr_Vend.Value), 0, objCliente!Cod_Repr_Vend.Value)
    vReprDPKGravar = vReprDPKGravado
    
    If IsNull(objCliente!Cod_Repr_Vend.Value) Then
        vReprAux = 0
    Else
        vReprAux = objCliente!Cod_Repr_Vend.Value
    End If
    
    If Val(vReprAux) <> 0 Then
        '-----------------------------------------------------------------------
        'SELECT
        'Se houver representante cadastrado para o cliente, seleciona os dados do mesmo
        Set OraParameters = db.Parameters
        OraParameters.Remove "CODCLI": OraParameters.Add "CODCLI", 0, 1
        OraParameters.Remove "CODREPR": OraParameters.Add "CODREPR", Val(objCliente!Cod_Repr_Vend.Value), 1
        OraParameters.Remove "TP_CONS": OraParameters.Add "TP_CONS", 1, 1
        OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
        OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
        'DLL - Fun��o que declara e retorna o cursor com dados do meu select
        'vVB_Generica_001.Retorna_Cursor db, "pm_CURSOR1", "PRODUCAO.PCK_CAD060.Pr_Verifica_Repres", "CODCLI", "CODREPR", "TP_CONS", "vErro", "vMsgErro"
        
        Criar_Cursor OraParameters, "PM_CURSOR1"
        
        vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD060.Pr_Verifica_Repres(:PM_CURSOR1, :CODCLI, :CODREPR, :TP_CONS, :vErro, :vMsgErro)")
        If vErro = "" Then
            Set objRepres = OraParameters("PM_CURSOR1").Value
        Else
            Exit Sub
        End If
        
        '---------------------------------------------------------------------------
        
        If objRepres.EOF Then
            lblRepresDPK.Caption = "Representante DPK"
        Else
            '14/12/04, aqui vai mostrar pois � o que est� vindo da intranet
            lblRepresDPK.Caption = "Representante DPK: " & Format(objRepres!cod_Repres.Value, "0000")
            lblRepresDPK.Caption = lblRepresDPK.Caption & "." & objRepres!seq_franquia.Value
        End If
        
        cboRepresDPK.Text = IIf(IsNull(objRepres!nome_Repres.Value), "", objRepres!nome_Repres.Value)
        vFilialRepres = IIf(IsNull(objRepres!Cod_Filial.Value), 0, objRepres!Cod_Filial.Value)
        vReprDPKGravar = IIf(IsNull(objCliente!Cod_Repr_Vend.Value), "", objCliente!Cod_Repr_Vend.Value)
        cboRepresDPK.Enabled = False
    End If
    '--------------------------------
    'REPRESENTANTE BLAU
    '--------------------------------
    'vReprBLAUGravado = ""
    'vReprBLAUGravar = vReprBLAUGravado
    'vReprAux = 0
    'lblRepresBlau.Caption = "Representante Blau"
    'cboRepresBlau.Text = ""
    'vReprBLAUGravar = ""
    'cboRepresBlau.Enabled = True
    
    Fl_Ja_Relacionou = "N"
            
    If Fl_Cliente_Novo = "S" And Val(vReprDPKGravado) = 0 Then
       cboRepresDPK.Enabled = True
    ElseIf Fl_Cliente_Novo = "N" And Val(vReprDPKGravado) = 0 And Fl_Ja_Relacionou = "S" Then
       cboRepresDPK.Enabled = False
    ElseIf Fl_Cliente_Novo = "N" And Val(vReprDPKGravado) = 0 And Fl_Ja_Relacionou = "N" Then
       cboRepresDPK.Enabled = True
    End If
    '--------------------------------
    'DADOS DE COBRAN�A - BANCO/CHEQUE
    '--------------------------------
    'Campos com valores pr�-definidos pelo Depto.Cr�dito
    cboBanco.Text = "BRASIL - 001"
    cboTpOP.Text = "  - ANTES"
    cboCheque.Text = "0 - SEM CHEQUE S/FUNDO"
    cboProtesto.ListIndex = -1
    'cboTpEmpresa.ListIndex = -1
    cboClassificacao.Text = "X"
    '--------------------------------
    'TRANSPORTADORA
    '--------------------------------
    lblTransp.Caption = "Transportadora: 0999"
    cboTransportadora = ". - 0999"
    '--------------------------------
    'MENSAGEM DO CLIENTE
    '--------------------------------
    cboMsgCliente.Text = ""
    txtDtMsg.Text = ""
    '--------------------------------
    'MENSAGEM FISCAL
    '--------------------------------
    cboMsgFiscal.Text = IIf(IsNull(objCliente!MsgFis.Value), "", objCliente!MsgFis.Value & " - " & Format(objCliente!CodFis.Value, "00"))
    '--------------------------------
    'DADOS DA PROPRIEDADE E PARTICULARIDADES DO CLIENTE
    '--------------------------------
    txtResponsavel.Text = ""
    txtProprietario.Text = IIf(IsNull(objCliente!nome_proprietario.Value), "", objCliente!nome_proprietario.Value)
    chkPredioProprio.Value = IIf(IsNull(objCliente!Propriedade.Value), Unchecked, IIf(objCliente!Propriedade.Value = "S", Checked, Unchecked))
    chkConsumidorFinal.Value = IIf(IsNull(objCliente!fl_cons_final.Value), Unchecked, IIf(objCliente!fl_cons_final.Value = "S", Checked, Unchecked))
    chkUtility.Value = Unchecked
    chkExportacao.Value = Unchecked
    txtDtFundacao.Text = IIf(IsNull(objCliente!dt_fundacao.Value), "", objCliente!dt_fundacao.Value)
    '---------------------------------
    'CLIENTE BOSCH DIESEL
    '---------------------------------
    If IsNull(objCliente!FL_CATEG_BOSCH_DIESEL.Value) Then
        cboCategBosch.ListIndex = -1
    Else
        cboCategBosch = objCliente!FL_CATEG_BOSCH_DIESEL.Value
    End If
    'cboCategBosch = IIf(IsNull(objCliente!FL_CATEG_BOSCH_DIESEL.Value), cboCategBosch.ListIndex = -1, objCliente!FL_CATEG_BOSCH_DIESEL.Value)
    If Trim(cboCategBosch.Text) <> "" Then
        fraCategBosch.Visible = True
        Call cmdLinVdr_Click
    End If
    '---------------------------------
    'CLIENTE TARE
    '---------------------------------
    txtCodTare.Text = IIf(IsNull(objCliente!cod_tare.Value) = True, 0, objCliente!cod_tare.Value)
    txtDtTare.Text = IIf(IsNull(objCliente!dt_tare.Value) = True, "", objCliente!dt_tare.Value)
    '--------------------------------
    'DADOS DE CR�DITO DO CLIENTE
    '--------------------------------
    vVlMaiorSlDevGravado = "0"
    txtVlMaiorSlDev = "0.00"
    txtDtMaiorSlDev.Enabled = True
    txtDtMaiorSlDev.Text = ""
    txtDtMaiorSlDev.Enabled = False
    vSaldoPedidos = 0
    'Novos campos inseridos em Julho/2005 - Alethea
    txtLimite = "0.00"
    txtDtLimite = ""
    lblVlPrevCpra = "0.00"
    '--------------------------------
    'INSCRI��O SUFRAMA
    '--------------------------------
    txtInscrSuframa.Text = IIf(IsNull(objCliente!inscr_suframa.Value), "", objCliente!inscr_suframa.Value)
    '--------------------------------
    'GRUPO MASTER
    '--------------------------------
    chkGrupoMaster.Value = IIf(IsNull(objCliente!Fl_Grupo_Master.Value), Unchecked, IIf(objCliente!Fl_Grupo_Master.Value = "S", Checked, Unchecked))
    If chkGrupoMaster.Value = Checked Then
        
        txtGrupoMaster.Text = IIf(IsNull(objCliente!EMP1.Value), "", objCliente!EMP1.Value)
        
        'Se constar um n�mero no campo de empresa grupo master, ent�o verificar se trata-se de c�digo de cliente
        If Not IsNumeric(txtGrupoMaster.Text) Then
            vCod_GrupoMaster = 0
            cboCodCliGrMaster.Text = ""
        Else
            vCod_GrupoMaster = Left(txtGrupoMaster.Text, 6)
        
            'Seleciona nome do cliente para colocar nas observa��es da Ref.Coml. ref. ao cliente master
            Set OraParameters = db.Parameters
            OraParameters.Remove "CODCLIMASTER": OraParameters.Add "CODCLIMASTER", Val(vCod_GrupoMaster), 1
            OraParameters.Remove "CGCMASTER": OraParameters.Add "CGCMASTER", 0, 1
            OraParameters.Remove "TP_CONS": OraParameters.Add "TP_CONS", 2, 1
            OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
            OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", 0, 2
            OraParameters("vErro").ServerType = ORATYPE_NUMBER
            
            'vVB_Generica_001.Retorna_Cursor db, "pm_CURSOR1", "PRODUCAO.PCK_CAD060.Pr_Verifica_Cliente", "CODCLIMASTER", "CGCMASTER", "TP_CONS", "vErro", "vMsgErro"
            Criar_Cursor OraParameters, "PM_CURSOR1"
            vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD060.Pr_Verifica_Cliente(:PM_CURSOR1, :CODCLIMASTER, :CGCMASTER, :TP_CONS, :vErro, :vMsgErro)")
            If vErro <> "" Then
                Exit Sub
            End If
            Set rst = OraParameters("PM_CURSOR1").Value
            
            'Se n�o achou o c�d.cliente, deixa campos em branco para avisar na hora de gravar
            If rst.EOF Then
                vCod_GrupoMaster = 0
                cboCodCliGrMaster.Text = ""
            Else
                vCod_GrupoMaster = Left(txtGrupoMaster.Text, 6) & " - " & IIf(IsNull(rst!nome_cliente.Value), "", Mid(rst!nome_cliente.Value, 1, 30))
            End If
        End If
    
    Else
        txtGrupoMaster.Text = ""
        vCod_GrupoMaster = 0
    End If
    
    ' fpallini - Ci&T - 21/08/12 - Buscar Cadastro do SAP.
    Set oRetornoPI = ConsultarSAP(Val(cboTpDocto.Text), Trim(txtDocto.Text), "", True)
    If Not oRetornoPI.Sucesso And Not oRetornoPI.CodErro = "W" Then
       MsgBox "Erro ao buscar cliente no SAP: " & oRetornoPI.mensagem, vbInformation, "Aten��o"
    End If
    
    Screen.MousePointer = 0

End Sub


Sub Habilitar_Objs_Analise()
    lblDtSolicitacao.Visible = True
    txtDtSolicitacao.Visible = True
    txtDtRecadastro.Enabled = False
    fraGrupoMaster.Visible = True
    cmdGravar.Visible = False
    cmdAceitar.Visible = True
    cmdRecusar.Visible = True
    'fraRecusado.Visible = False
    lblRecusa2.Visible = False
    lblRecusado2.Visible = False
    cmdDescartar.Visible = False
End Sub

Sub Habilitar_Objs_Cadastro()
    lblDtSolicitacao.Visible = False
    txtDtSolicitacao.Visible = False
    txtDtRecadastro.Enabled = True
    fraGrupoMaster.Visible = False
    cmdGravar.Visible = True
    cmdAceitar.Visible = False
    cmdRecusar.Visible = False
    'fraRecusado.Visible = False
    lblRecusa2.Visible = False
    lblRecusado2.Visible = False
    If vFl_Consulta = True Then
        cmdDescartar.Visible = True
    Else
        cmdDescartar.Visible = False
    End If
End Sub

Sub Limpar_Campos_OutEnd(pLimpaTudo As Boolean)
    txtSeqEnd01.Text = ""
    If vFl_Adic_OutEnd = False Then
        cboTpEnd01.ListIndex = -1
    End If
    cboTpDoctoEnd01.ListIndex = -1
    txtCGCEnd01.Text = ""
    txtIEEnd01.Text = ""
    txtNomeEnd01.Text = ""
    txtEnd01.Text = ""
    
    'Kevlin Toshinari Ossada - CIT - Limpa campo Num. e Compl
    txtNumEnd01 = ""
    txtComplEnd01 = ""
    
    txtBairroEnd01.Text = ""
    txtCepEnd01.Text = ""
    cboUFEnd01.ListIndex = -1
    cboCidadeEnd01.ListIndex = -1
    txtDDDEnd01.Text = ""
    txtFoneEnd01.Text = ""
    txtFaxEnd01.Text = ""
    txtCxPosEnd01.Text = ""
    If pLimpaTudo = True Then
        lsvOutEnd.ListItems.Clear
    End If
End Sub


Sub Limpar_Campos_RefBco(pLimpaTudo As Boolean)
    txtSeq.Text = ""
    txtBanco.Text = ""
    txtDt.Text = ""
    txtAgencia.Text = ""
    txtDDD.Text = ""
    txtFone.Text = ""
    txtConceito.Text = ""
    txtObs.Text = ""
    If pLimpaTudo = True Then
        lsvRefBanco.ListItems.Clear
    End If
End Sub

Sub Limpar_Campos_RefComl(pLimpaTudo As Boolean)
    txtSeq01.Text = ""
    txtEmp01.Text = ""
    txtDtCons01.Text = ""
    txtDDD01.Text = ""
    txtFone01.Text = ""
    txtVlUltFat01.Text = ""
    txtDtUltFat01.Text = ""
    txtMaiSlDev01.Text = ""
    txtDtCad01.Text = ""
    txtAtraso01.Text = ""
    txtConceito01.Text = ""
    txtObs01.Text = ""
    If pLimpaTudo = True Then
        lsvRefComl.ListItems.Clear
    End If
End Sub

Sub Limpar_Campos_Vendor()
'    txtContratoVendor.Text = ""
'    txtDtContratoVendor.Text = ""
'    txtVlLimiteVendor.Text = ""
'    cboSitVendor.Text = ""
'    lsvVendor.ListItems.Clear
End Sub

Sub pCarregaComboRepresentante(lcboRepres As ComboBox, lstrParametro As String, lstrDivisao As String)
    
    Dim lstrSQL          As String
    Dim lstrWHERE        As String
    Dim objRepres        As Object
    Dim lstrRepres       As String
    Dim lstrSequencia    As String
    Dim lstrSQLSequencia As String
    
    On Error GoTo erros
    
    lstrSequencia = "0"
    lstrWHERE = ""
    lstrSQLSequencia = ""
    lcboRepres.Clear
    lstrParametro = UCase(lstrParametro)
    
    'NOME OU CODIGO
    If IsNumeric(lstrParametro) Then
        If lstrDivisao <> "B" Then
            vReprDPKGravar = Fix(lstrParametro)
        'Else
        '    vReprBLAUGravar = Fix(lstrParametro)
        End If
    Else
        If lstrDivisao <> "B" Then
            vReprDPKGravar = ""
        'Else
        '    vReprBLAUGravar = ""
        End If
    End If
    
    If Trim(lstrParametro) <> "" Then
        
        If IsNumeric(Trim(lstrParametro)) Then
            
            db.Parameters.Remove "CODREPR": db.Parameters.Add "CODREPR", Fix(lstrParametro), 1
            
            lstrWHERE = lstrWHERE & " cod_Repres =:CODREPR "
            
            If InStr(1, lstrParametro, ".") <> 0 Then ' se tem sequencia
                
                lstrSequencia = Mid(lstrParametro, InStr(1, lstrParametro, ".") + 1, Len(lstrParametro))
                
                'Set OraParameters = db.Parameters
                'OraParameters.Remove "CODREPR": OraParameters.Add "CODREPR", Fix(lstrParametro), 1
                'OraParameters.Remove "DIVISAO": OraParameters.Add "DIVISAO", 0, 1
                'OraParameters.Remove "CODFRAN": OraParameters.Add "CODFRAN", 0, 1
                'OraParameters.Remove "SEQFRAN": OraParameters.Add "SEQFRAN", 0, 1
                'OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
                'OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
                'OraParameters("vErro").ServerType = ORATYPE_NUMBER
                
                
                'carlosj CiT [22/11/2012] - ISA 1022
                If Right(Trim(cboTpSegmento.Text), 2) = "AG" Then
                    
                    If Val(lstrSequencia) = 0 Then
                        
                        'OraParameters.Remove "TP_CONS": OraParameters.Add "TP_CONS", 0, 1
                        'DLL - Fun��o que declara e retorna o cursor com dados do meu select
                        'vVB_Generica_001.Retorna_Cursor db, "pm_CURSOR1", "PRODUCAO.PCK_CAD060.Pr_Dados_Cliente", "TP_CONS", "vErro", "vMsgErro"
                        'Set db_CONS = vVB_Generica_001.Rst_Geral
                        'vVB_Generica_001.Rst_Geral.Close: Set vVB_Generica_001.Rst_Geral = Nothing
                        'If Val(OraParameters("vErro").Value) <> 0 Then
                        '    MsgBox "Erro: " & Val(OraParameters("vErro").Value) & " - " & _
                        '                      OraParameters("vMsgErro").Value & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
                        '    Exit Sub
                        'End If
                        
                        
                        'Em 14/12/04 foi solicitado que somente representantes do tipo "R" possam
                        'ser ligados ao cliente e n�o mais televendas e gerente, para isso foi comentado o trecho:
                        'AND (a.tipo in ('R','V','M','P'))

                        'Altera��o da consistencia conf.solic.Monica - Alethea - 28/11/06
                        '"AND (a.tipo = 'R' or a.cod_repres = 599 or "
                        
                        lstrSQLSequencia = "SELECT a.cod_repres, a.nome_repres, " & _
                                                 " b.cod_regional, b.cod_filial " & _
                                             "FROM representante a, filial b " & _
                                            "WHERE a.cod_repres = :CODREPR " & _
                                              "AND a.seq_franquia = 0 " & _
                                              "AND b.cod_filial = a.cod_filial " & _
                                              "AND a.divisao = :DIVISAO " & _
                                              "AND a.situacao = 0 " & _
                                              "AND (a.tipo in ('R','P') or a.cod_repres = 599 or " & _
                                                  "(a.tipo in ('M','A','V') and " & _
                                                  " b.tp_filial in ('P','T'))) "
                    Else
                        lstrSQLSequencia = "SELECT a.cod_repres, a.nome_repres, " & _
                                                 " b.cod_regional, d.divisao, b.cod_filial " & _
                                             "FROM representante a , filial b , representante c, divisao d " & _
                                            "WHERE b.cod_franqueador = :COD_FRANQUEADOR " & _
                                              "AND a.cod_filial = b.cod_filial " & _
                                              "AND a.seq_franquia = :SEQ_FRANQUIA " & _
                                              "AND a.divisao = d.divisao " & _
                                              "AND a.situacao = 0 " & _
                                              "AND c.cod_repres = b.cod_franqueador " & _
                                              "AND c.seq_franquia = 0 " & _
                                              "AND (a.tipo in ('R','P') or a.cod_repres = 599 or " & _
                                                  "(a.tipo in ('M','A','V') and " & _
                                                  " b.tp_filial in ('P','T'))) " & _
                                              "AND a.seq_franquia = :seq_franquia "
                    End If
                
                ElseIf Val(lstrSequencia) <> 0 Then
                    
                    'Altera��o da consistencia conf.solic.Monica - Alethea - 28/11/06
                    '"AND (a.tipo = 'R' or a.cod_repres = 599 or "
                    lstrSQLSequencia = "SELECT a.cod_repres, a.nome_repres, b.cod_regional, " & _
                                             " d.divisao, b.cod_filial " & _
                                         "FROM representante a, filial b, " & _
                                             " representante c, divisao d " & _
                                        "WHERE b.cod_franqueador = :COD_FRANQUEADOR " & _
                                          "AND a.cod_filial = b.cod_filial " & _
                                          "AND a.seq_franquia = :SEQ_FRANQUIA " & _
                                          "AND a.divisao = d.divisao " & _
                                          "AND a.situacao = 0 " & _
                                          "AND c.cod_repres = b.cod_franqueador " & _
                                          "AND c.seq_franquia = 0 " & _
                                              "AND (a.tipo in ('R','P') or a.cod_repres = 599 or " & _
                                                  "(a.tipo in ('M','A','V') and " & _
                                                  " b.tp_filial in ('P','T'))) "
                Else
                    lstrSQLSequencia = "SELECT a.cod_repres, a.nome_repres, b.cod_regional, " & _
                                             " b.cod_filial " & _
                                         "FROM representante a, filial b " & _
                                        "WHERE a.cod_repres = :CODREPR " & _
                                          "AND a.seq_franquia = 0 " & _
                                          "AND b.cod_filial = a.cod_filial " & _
                                          "AND a.divisao = :DIVISAO " & _
                                          "AND a.situacao = 0 " & _
                                              "AND (a.tipo in ('R','P') or a.cod_repres = 599 or " & _
                                                  "(a.tipo in ('M','A','V') and " & _
                                                  " b.tp_filial in ('P','T'))) "
                End If
                
                db.Parameters.Remove "cod_franqueador": db.Parameters.Add "cod_franqueador", Fix(lstrParametro), 1
                db.Parameters.Remove "seq_franquia": db.Parameters.Add "seq_franquia", lstrSequencia, 1
            End If
        Else
            db.Parameters.Remove "nome_Repres": db.Parameters.Add "nome_Repres", lstrParametro, 1
            
            lstrWHERE = lstrWHERE & " nome_Repres " & IIf(Left(lstrParametro, 1) = "%" Or Right(lstrParametro, 1) = "%", " LIKE ", "=") & " :nome_Repres "
        
        End If
    End If
    db.Parameters.Remove "divisao": db.Parameters.Add "divisao", lstrDivisao, 1
    
    If lstrSQLSequencia <> "" Then
        
        Screen.MousePointer = vbHourglass
        Set objRepres = db.dbcreatedynaset(lstrSQLSequencia, 0&)
        lcboRepres.Clear
        
        If Not objRepres.EOF Then     ' nome_repres                           cod_repres
            If lstrDivisao <> "B" Then
                vReprDPKGravar = objRepres.Fields(0).Value
            'Else
            '    vReprBLAUGravar = objRepres.Fields(0).Value
            End If
            
            'Altera��o da consistencia conf.solic.Monica - Alethea - 28/11/06
            '"AND (a.tipo = 'R' or a.cod_repres = 599 or "
            lstrSQL = "SELECT cod_Repres, nome_Repres, b.cod_regional, b.cod_filial " & _
                       " FROM Representante a, Filial b " & _
                       "WHERE cod_repres = :CODREPR " & _
                         "AND a.divisao = :DIVISAO " & _
                         "AND a.cod_filial = b.cod_filial " & _
                         "AND a.situacao = 0 " & _
                        "AND (a.tipo in ('R','P') or a.cod_repres = 599 or " & _
                            "(a.tipo in ('M','A','V') and " & _
                            " b.tp_filial in ('P','T'))) "
            Screen.MousePointer = vbHourglass
            db.Parameters.Remove "CODREPR": db.Parameters.Add "CODREPR", Fix(lstrParametro), 1
            Set objRepres = db.dbcreatedynaset(lstrSQL, 0&)
            Screen.MousePointer = vbDefault
            lcboRepres.Clear
            If Not objRepres.EOF Then
               lcboRepres.AddItem objRepres!nome_Repres.Value & " - " & Format(objRepres!cod_Repres.Value, "0000")
            End If
        
        End If
        
    Else
    
        'Altera��o da consistencia conf.solic.Monica - Alethea - 28/11/06
        '"AND (a.tipo = 'R' or a.cod_repres = 599 or "
        lstrSQL = "SELECT cod_Repres, nome_Repres, b.cod_regional, b.cod_filial " & _
                    "FROM Representante a, Filial b "
        lstrSQL = lstrSQL & _
                    IIf(lstrWHERE = "", "", " WHERE " & lstrWHERE & _
                    " AND a.divisao = :DIVISAO " & _
                    " AND situacao = 0 " & _
                    " AND a.cod_filial = b.cod_filial " & _
                    " AND (a.tipo in ('R','P') or a.cod_repres = 599 or " & _
                        "(a.tipo in ('M','A','V') and " & _
                        " b.tp_filial in ('P','T'))) " & _
                    " AND a.seq_franquia=0")
        
        Set objRepres = db.dbcreatedynaset(lstrSQL, 0&)
        
        lcboRepres.Clear
        Screen.MousePointer = vbHourglass
        
        While Not objRepres.EOF
            mdiCAD060.stbBarra.Panels(1) = "PESQUISANDO REPRESENTANTE . . . " & lcboRepres.ListCount
            lcboRepres.AddItem objRepres!nome_Repres.Value & " - " & Format(objRepres!cod_Repres.Value, "0000") & ".0"
            objRepres.MoveNext
        Wend
        mdiCAD060.stbBarra.Panels(1) = ""
    
    End If
    
    Screen.MousePointer = vbDefault
    
    If objRepres.RecordCount <= 1 Then
        If objRepres.RecordCount = 0 Then
            MsgBox "Representante n�o Encontrado !", vbInformation, "Aten��o"
            'If lstrDivisao = "B" Then
            '    lblRepresBlau = "Representante Blau"
            '   lblRepresBlau.Refresh
            '    vReprBLAUGravar = ""
            'Else
            If lstrDivisao <> "B" Then
                lblRepresDPK = "Representante DPK"
                lblRepresDPK.Refresh
                vReprDPKGravar = ""
            End If
        Else
            
            lcboRepres.RemoveItem 0
            objRepres.MoveFirst
            'lcboRepres.Text = objRepres.Fields("nome_Repres").Value
            lcboRepres.Text = objRepres.Fields("nome_Repres").Value & " - " & Format(objRepres!cod_Repres.Value, "0000") & ".0"
            
            'If lstrDivisao = "B" Then
            '    lblRepresBlau = "Representante Blau: " & Format(objRepres.Fields("cod_repres").Value, "0000") & "." & lstrSequencia
            '    lblRepresBlau.Refresh
            '    vComboRepresBlau = lcboRepres
            '    If Trim(lstrSQLSequencia) = "" Then
            '        vReprBLAUGravar = objRepres.Fields("cod_repres").Value
            '    End If
            'Else
            If lstrDivisao <> "B" Then
                lblRepresDPK = "Representante DPK: " & Format(objRepres.Fields("cod_repres").Value, "0000") & "." & lstrSequencia
                lblRepresDPK.Refresh
                'vComboRepresDPK = lcboRepres
                If Trim(lstrSQLSequencia) = "" Then
                    vReprDPKGravar = objRepres.Fields("cod_repres").Value
                End If
            End If
        End If
    Else
        MsgBox "Encontrei mais de um Representante" & Chr(10) & "Selecione um !", vbInformation, "Aten��o"
    End If
    
    'comentada rotina abaixo de validacao clientes DF - cristiano - 07/10/2008
    ''*******************************************************************************
    ''Para clientes do DF, verifica se Representante pertence a Regional 21 ou a uma
    ''Filial que perten�a a Regional 21
    ''Travado a partir de 21/05/01
    ''Set OraParameters = db.Parameters
    ''OraParameters.Remove "CODCID": OraParameters.Add "CODCID", Right(cboCidade.Text, 4), 1
    ''OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    ''OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    ''OraParameters("vErro").ServerType = ORATYPE_NUMBER
    ''OraParameters.Remove "TP_CONS": OraParameters.Add "TP_CONS", 0, 1
    ''DLL - Fun��o que declara e retorna o cursor com dados do meu select
    ''vVB_Generica_001.Retorna_Cursor db, "pm_CURSOR1", "PRODUCAO.PCK_CAD060.Pr_Dados_Cliente", "TP_CONS", "vErro", "vMsgErro"
    ''Set db_CONS = vVB_Generica_001.Rst_Geral
    ''vVB_Generica_001.Rst_Geral.Close: Set vVB_Generica_001.Rst_Geral = Nothing
    ''If Val(OraParameters("vErro").Value) <> 0 Then
    ''    MsgBox "Erro: " & Val(OraParameters("vErro").Value) & " - " & _
    ''                      OraParameters("vMsgErro").Value & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
    ''    Exit Sub
    ''End If
    '
    '
    'db.Parameters.Remove "CODCID": db.Parameters.Add "CODCID", Val(Mid(cboCidade.Text, (InStr(1, cboCidade.Text, "-") + 2))), 1
    '
    'Sql = "Select cod_uf " & _
    '        "FROM Producao.Cidade " & _
    '       "WHERE cod_cidade = :CODCID"
    'Set objRepres = db.dbcreatedynaset(Sql, 0&)
    '
    ''Consist�ncia de status colocada em 21/01/05 - Alethea
    ''Permitir que cliente de DF, que for OUTRAS SA�DAS, possa ter representante = 599
    'If objRepres!COD_UF = "DF" And cboStatus.Text = "0 - NORMAL" Then
    '
    '    db.Parameters.Remove "CODUF": db.Parameters.Add "CODUF", objRepres!COD_UF.Value, 1
    '    db.Parameters.Remove "CODREPR": db.Parameters.Add "CODREPR", Fix(lstrParametro), 1
    '
    '    'Sql = " Select f.cod_filial, f.cod_regional " & _
    '            " from filial f, representante r " & _
    '           " where r.cod_repres =  :CODREPR " & _
    '             " and f.cod_filial = r.cod_filial  " & _
    '             " and ( f.cod_Filial = 21 or f.cod_regional = 21 ) "
    '
    '    Sql = " Select distinct f.cod_filial, f.cod_regional " & _
    '            " from filial f, representante r " & _
    '           " where r.cod_repres =  :CODREPR " & _
    '             " and f.cod_filial = r.cod_filial  " & _
    '             " and (f.cod_Filial in (21,143,186) or f.cod_regional in (21,143,186)) "
    '
    '    Set objRepres = db.dbcreatedynaset(Sql, 0&)
    '
    '    If objRepres.EOF Then
    '       'MsgBox " Por causa do TARE (CD DF), este Cliente deve ser " & Chr(10) & _
    '              " cadastrado para um  Representante  que  perten�a" & Chr(10) & _
    '              " � Regional 21, 143 ou 186 ", , "ATEN��O!!!!"
    '       MsgBox " Cliente status normal, do estado de 'DF' (venda limitada), deve ser " & Chr(10) & _
    '              " cadastrado para um  Representante  que  perten�a" & Chr(10) & _
    '              " � Regional 21, 143 ou 186 ", , "ATEN��O!!!!"
    '
    '       cboRepresDPK.SetFocus
    '       Exit Sub
    '    End If
    'End If
    ''*******************************************************************************
    
    Exit Sub
erros:
    If Err = 7 Then
        MsgBox "Consulta muito grande!", vbCritical, "Aten��o"
    End If
              
End Sub

Function fVerificaRepresentantes() 'true = OK ,      false = NAO ALTERAR
    
    '-------------------------------------------------------------------------------
    ' NAO PODE ZERAR REPRESENTANTE SE:                                             '
    ' -Tiver duplicata em aberto para o repr. antigo OU                            '
    ' -Tiver pedidos em aberto para o repr. antigo (pednota_venda.fl_ger_nfis).    '
    ' NAO PODE TROCAR REPRESENTANTE SE:                                            '
    ' -Tiver ped. em aberto(SIT<>9) p/o repr.antigo(pednota_venda.fl_ger_nfis='N').'
    ' ( situacao nao cancelada e nota fiscal nao emitida )                         '
    '-------------------------------------------------------------------------------
    fVerificaRepresentantes = True
    
    db.Parameters.Remove "CODCLI": db.Parameters.Add "CODCLI", Val(txtCodCliente.Text), 1
    db.Parameters.Remove "CODREPR": db.Parameters.Add "CODREPR", Val(vReprDPKGravado), 1
    
    '--------------- ZERANDO REPRESENTANTE DPK -------------------------------
    If Val(vReprDPKGravado) <> 0 And Val(vReprDPKGravar) = 0 Then
        
        '----------------- VERIFICA PEDIDOS EM ABERTO -----------------
        Sql = " SELECT cod_cliente " & _
                 "FROM pednota_venda " & _
                "WHERE situacao <> 9 " & _
                  "AND fl_ger_nfis = 'N' " & _
                  "AND cod_cliente = :CODCLI " & _
                  "AND cod_repres = :CODREPR"
        Set db_CONS = db.dbcreatedynaset(Sql, 0&)
        
        If Not db_CONS.EOF Then
            MsgBox "Existem pedidos em aberto para este Cliente/Representante. " & Chr(10) & Chr(10) & " N�o posso alterar o Representante do Cliente!", vbCritical, "CAD060 - Aten��o"
            fVerificaRepresentantes = False
            Exit Function
        End If
        
        '----------------- VERIFICA DUPLICATAS EM ABERTO -----------------
        Sql = " SELECT cod_cliente " & _
                 "FROM cobranca.duplicatas " & _
                "WHERE situacao_pagto = 0 " & _
                  "AND cod_cliente = :CODCLI " & _
                  "AND cod_responsavel = :CODREPR"
        Set db_CONS = db.dbcreatedynaset(Sql, 0&)
        
        If Not db_CONS.EOF Then
            MsgBox "Existem duplicatas em aberto para este Cliente/Representante. " & Chr(10) & Chr(10) & " N�o posso alterar o Representante do Cliente!", vbCritical, "CAD060 - Aten��o"
            fVerificaRepresentantes = False
            Exit Function
        End If
    End If
    
    '--------------- ZERANDO REPRESENTANTE BLAU -------------------------------
    'If Val(vReprBLAUGravado) <> 0 And Val(vReprBLAUGravar) = 0 Then
    '    '----------------- VERIFICA PEDIDOS EM ABERTO -----------------
    '    Sql = " SELECT cod_cliente " & _
    '             "FROM pednota_venda " & _
    '            "WHERE situacao <> 9 " & _
    '              "AND fl_ger_nfis = 'N' " & _
    '              "AND cod_cliente = :CODCLI " & _
    '              "AND cod_repres = :CODREPR "
    '    Set db_CONS = db.dbcreatedynaset(Sql, 0&)
    '    If Not db_CONS.EOF Then
    '        MsgBox "Existem pedidos em aberto para este Cliente/Representante. " & Chr(10) & Chr(10) & " N�o posso alterar o Representante do Cliente!", vbCritical, "CAD060 - Aten��o"
    '        fVerificaRepresentantes = False
    '        Exit Function
    '    End If
    '    '----------------- VERIFICA DUPLICATAS EM ABERTO -----------------
    '    Sql = " SELECT cod_cliente " & _
    '             "FROM cobranca.duplicatas " & _
    '            "WHERE situacao_pagto = 0 " & _
    '              "AND cod_cliente = :CODCLI " & _
    '              "AND cod_responsavel = :CODREPR"
    '    Set db_CONS = db.dbcreatedynaset(Sql, 0&)
    '    If Not db_CONS.EOF Then
    '        MsgBox "Existem duplicatas em aberto para este Cliente/Representante. " & Chr(10) & Chr(10) & " N�o posso alterar o Representante do Cliente!", vbCritical, "CAD060 - Aten��o"
    '        fVerificaRepresentantes = False
    '        Exit Function
    '    End If
    'End If
    
    '----------------- SE TROCOU O REPRESENTANTE DPK ---------------------------
    If Val(vReprDPKGravado) <> 0 And Val(vReprDPKGravado) <> 0 Then
        
        If Val(vReprDPKGravado) <> Val(vReprDPKGravar) Then
            
            Sql = " SELECT cod_cliente " & _
                     "FROM pednota_venda " & _
                    "WHERE situacao <> 9 " & _
                      "AND fl_ger_nfis = 'N' " & _
                      "AND cod_cliente = :CODCLI " & _
                      "AND cod_repres = :CODREPR"
            Set db_CONS = db.dbcreatedynaset(Sql, 0&)
            
            If Not db_CONS.EOF Then
                MsgBox "Existem pedidos em aberto para este Cliente/Representante. " & Chr(10) & Chr(10) & " N�o posso alterar o Representante do Cliente!", vbCritical, "CAD060 - Aten��o"
                fVerificaRepresentantes = False
                Exit Function
            End If
        End If
    End If
    
    '----------------- SE TROCOU O REPRESENTANTE BLAU ---------------------------
    'If Val(vReprBLAUGravado) <> 0 And Val(vReprBLAUGravar) <> 0 Then
    '    If Val(vReprBLAUGravado) <> Val(vReprBLAUGravar) Then
    '        Sql = " SELECT cod_cliente " & _
    '                 "FROM pednota_venda " & _
    '                "WHERE situacao <> 9 " & _
    '                  "AND fl_ger_nfis = 'N' " & _
    '                  "AND cod_cliente = :CODCLI " & _
    '                  "AND cod_repres = :CODREPR"
    '        Set db_CONS = db.dbcreatedynaset(Sql, 0&)
    '        If Not db_CONS.EOF Then
    '            MsgBox "Existem pedidos em aberto para este Cliente/Representante. " & Chr(10) & " N�o posso alterar o Representante do Cliente!", vbCritical, "Aten��o"
    '            fVerificaRepresentantes = False
    '            Exit Function
    '        End If
    '    End If
    'End If
End Function

Sub pCarregaDados()
    
    Dim objCliente As Object, objRepres As Object, I As Integer
    Dim vReprAux As Long
    Dim rua As String
    Dim numero As String
    Dim complemento As String
    
    On Error Resume Next
    
    If txtCodCliente.Text = "" Then
        Exit Sub
    End If
    
    
    Screen.MousePointer = 11
    sspCadCli.Caption = "Aguarde... carregando informa��es do cliente"
    
    '-----------------------------------------------------------------------
    'SELECT
    'Vou trazer informa��es do Cliente, de acordo com o documento digitado
    Set OraParameters = db.Parameters
    OraParameters.Remove "CODCLI": OraParameters.Add "CODCLI", CDbl(txtCodCliente.Text), 1
    OraParameters.Remove "CGC": OraParameters.Add "CGC", 0, 1
    OraParameters.Remove "TP_CONS": OraParameters.Add "TP_CONS", 1, 1   'Tp_Cons=1 - Traz dados gerais do cadastro
    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    Criar_Cursor OraParameters, "PM_CURSOR1"
    vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD060.Pr_Verifica_Cliente(:PM_CURSOR1, :CODCLI, :CGC, :TP_CONS, :vErro, :vMsgErro)")
    If vErro <> "" Then
        Exit Sub
    End If
    Set objCliente = OraParameters("PM_CURSOR1").Value
    '----------------------------------------------------------------------
    
    Limpar_Campos_Cadastro (False)
    Limpar_Campos_RefBco (True)
    Limpar_Campos_RefComl (True)
    Limpar_Campos_OutEnd (True)
    'Limpar_Campos_Vendor
    
    
    If objCliente.EOF Then
        MsgBox "Cliente n�o encontrado! ", vbInformation, "CAD060 - Cadastro de Cliente"
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
        
    txtCodCliente.Text = objCliente!COD_CLIENTE.Value
    
    ' fpallini - Ci&T - 21/08/12 - ISA-260 - Trazer c�digo SAP do legado
    txtSAP.Text = IIf(IsNull(objCliente!COD_SAP.Value), "", objCliente!COD_SAP.Value)
    
    cboCliente.Text = IIf(IsNull(objCliente!nome_cliente.Value), "", objCliente!nome_cliente.Value)
    txtDtCadastro.Text = IIf(IsNull(objCliente!Dt_Cadastro.Value), "", objCliente!Dt_Cadastro.Value)
    txtDtCadastro.Enabled = False
    txtDtRecadastro.Text = IIf(IsNull(objCliente!Dt_Recadastro.Value), "", objCliente!Dt_Recadastro.Value)
    txtSituacao.Text = IIf(objCliente!Situacao.Value = 0, "0 - ATIVO", "9 - DESATIVADO")
    pictSituacao.Picture = IIf(txtSituacao.Text = "0 - Ativo", imlFiguras.ListImages(5).Picture, imlFiguras.ListImages(4).Picture)
    
    'Preenchendo comboDiferenciado - SDS1889 - Jairo Almeida 22-02-2012 - Consulta de Dados
    If objCliente!fl_agrup_diferenciada.Value = "S" Then
      cboDiferenciado.ListIndex = 0
    ElseIf objCliente!fl_agrup_diferenciada.Value = "" Then
      cboDiferenciado.ListIndex = 1
    ElseIf objCliente!fl_agrup_diferenciada.Value = "N" Then
      cboDiferenciado.ListIndex = 1
    End If
    
    txtSituacao.Text = objCliente!fl_agrup_diferenciada.Value
    '--------------Fim implementa��es SDS1889-----------------------------------------------
    
    '--------------------------------
    'TIPO / No. DOCUMENTO (CGC OU CPF)
    '--------------------------------
    cboTpDocto.Text = IIf(objCliente!Tp_Docto.Value = 1, "1 - PESSOA JURIDICA", "2 - PESSOA FISICA")
    Call cboTpDocto_Click
    txtDocto.Text = IIf(IsNull(objCliente!Cgc.Value), "", objCliente!Cgc.Value)
    cboTpDocto.Enabled = False
    txtDocto.Enabled = False
    '--------------------------------
    'INSCRI��O ESTADUAL
    '--------------------------------
    txtInscrEstadual.Text = IIf(IsNull(objCliente!INSCR_ESTADUAL.Value), "", objCliente!INSCR_ESTADUAL.Value)
    '--------------------------------
    'INFORMA��ES GERAIS
    '--------------------------------
    cboStatus.Text = IIf(IsNull(objCliente!status.Value), 0, IIf(objCliente!status.Value = 0, "0 - NORMAL", "1 - OUTRAS SAIDAS"))
    cboCaracteristica.Text = IIf(IsNull(objCliente!Caracteristica.Value), "", objCliente!Caracteristica.Value & " - " & objCliente!Desc_Caracteristica.Value)
  
    'Eduardo Faria de Oliveira  - CIT - 22/08/12 -Testa se h� retorno do banco, para ent�o separa os campos rua,numero e complemento
    'txtEndereco.Text = IIf(IsNull(objCliente!endereco.Value), "", objCliente!endereco.Value)
    If IsNull(objCliente!endereco.Value) Then
        rua = ""
        numero = ""
        complemento = ""
    Else
        Call DesconcatenarEnderecoSAP(objCliente!endereco.Value, rua, numero, complemento)
    End If
    
    txtEndereco.Text = rua
    If numero = "" Then
        txtNumero.Text = 0
    Else
        txtNumero.Text = numero
    End If
    txtComplemento.Text = complemento
    
    
    txtBairro.Text = IIf(IsNull(objCliente!Bairro.Value), "", objCliente!Bairro.Value)
    txtCep.Text = IIf(IsNull(objCliente!CEP.Value), "", Format(objCliente!CEP.Value, "00000000"))
    cboUF.Text = IIf(IsNull(objCliente!COD_UF.Value), "", objCliente!COD_UF.Value)
    Call cboUF_Click
    cboCidade.Text = IIf(IsNull(objCliente!COD_CIDADE.Value), "", Trim(objCliente!nome_cidade.Value) & " - " & Format(objCliente!COD_CIDADE.Value, "0000"))
    txtDDD1.Text = IIf(IsNull(objCliente!ddd1.Value), "", objCliente!ddd1.Value)
    txtFone1.Text = IIf(IsNull(objCliente!fone1.Value), "", objCliente!fone1.Value)
    txtDDD2.Text = IIf(IsNull(objCliente!ddd2.Value), "", objCliente!ddd2.Value)
    txtFone2.Text = IIf(IsNull(objCliente!fone2.Value), "", objCliente!fone2.Value)
    txtFax.Text = IIf(IsNull(objCliente!fax.Value), "", objCliente!fax.Value)
    txtContato.Text = IIf(IsNull(objCliente!NOME_CONTATO.Value), "", objCliente!NOME_CONTATO.Value)
    txtCxPostal.Text = IIf(IsNull(objCliente!CxPostal.Value), "", objCliente!CxPostal.Value)
    txtTelex.Text = IIf(IsNull(objCliente!Telex.Value), "", objCliente!Telex.Value)
    
    If IsNull(objCliente!tp_empresa.Value) Then
        'cboTpEmpresa.Text = ""
        cboTpEmpresa.ListIndex = -1
    Else
        If objCliente!tp_empresa.Value = "M" Then
            cboTpEmpresa.Text = "M - MICROEMPRESA"
        ElseIf objCliente!tp_empresa.Value = "P" Then
            cboTpEmpresa.Text = "P - EMPRESA PEQ.PORTE"
        Else
            cboTpEmpresa.Text = "E - EMPRESA"
        End If
    End If
   
    '--------------------------------
    'CLIENTE COM DADOS INTERNET - E-MAIL/HOMEPAGE
    '--------------------------------
    txtEmail.Text = IIf(IsNull(objCliente!E_Mail.Value) = True, "", objCliente!E_Mail.Value)
    txtHomePage.Text = IIf(IsNull(objCliente!HomePage.Value) = True, "", objCliente!HomePage.Value)
    '--------------------------------
    'TIPO DE CLIENTE DPK E BLAU
    '--------------------------------
    
    
    'carlosj Cit (23/11/2012) - ISA 1032
    'INCIO ALTERACAO
     If (objCliente!Cod_Tipo_Cliente.Value <> "") Then
    
        Call CarregarComboTipoSegmento
        
        'seta o combo tipo segmento
        Call SetarComboTipoSegmento(cboTpSegmento, Format(objCliente!Cod_Tipo_Cliente.Value, "00"))
    
     End If
    'carlosj Cit (23/11/2012) - ISA 1032
    'FIM ALTERACAO
    
    
    cboTpCliente.Text = IIf(IsNull(objCliente!Cod_Tipo_Cliente.Value), "", objCliente!TipoCliDPK.Value & " - " & Format(objCliente!Cod_Tipo_Cliente.Value, "00"))
    'cboTpClienteBlau.Text = IIf(IsNull(objCliente!Cod_Tipo_ClBlau.Value) Or Val(objCliente!Cod_Tipo_ClBlau.Value) = 0, "", objCliente!TipoCliBlau.Value & " - " & Format(objCliente!Cod_Tipo_ClBlau.Value, "00"))
    'vCod_tipo_clBLAU = Val(Right(cboTpClienteBlau.Text, 2))
    vCod_tipo_Cliente = Val(Right(cboTpCliente.Text, 2))
    '--------------------------------
    'REPRESENTANTE DPK
    '--------------------------------
    vReprDPKGravado = objCliente!Cod_Repr_Vend.Value
    vReprDPKGravar = vReprDPKGravado
        
    If Fl_Cliente_Novo = "N" Then
        '-----------------------------------------------------------------------
        'SELECT
        'Verifica��o Tare
        Set OraParameters = db.Parameters
        OraParameters.Remove "CODCLI": OraParameters.Add "CODCLI", Val(txtCodCliente.Text), 1
        OraParameters.Remove "CODREPR": OraParameters.Add "CODREPR", 0, 1
        OraParameters.Remove "TP_CONS": OraParameters.Add "TP_CONS", 0, 1
        OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
        OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
        
        Criar_Cursor OraParameters, "PM_CURSOR1"
        
        vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD060.Pr_Verifica_Repres(:pm_CURSOR1,:CODCLI, :CODREPR, :TP_CONS, :vErro, :vMsgErro)")
        
        If vErro <> "" Then
            Exit Sub
        End If
        
        Set db_CONS = OraParameters("PM_CURSOR1").Value
        '---------------------------------------------------------------------------
        If Not db_CONS.EOF Then 'Achou repres para o cliente que n�o pertencem a Filial 26 ou 135
           Fl_Tare = "S"
        Else
           Fl_Tare = "N"
        End If
    End If
    
    If IsNull(objCliente!Cod_Repr_Vend.Value) Then
        vReprAux = 0
    Else
        vReprAux = objCliente!Cod_Repr_Vend.Value
    End If
    
    
    If Val(vReprAux) <> 0 Then
        '-----------------------------------------------------------------------
        'SELECT
        'Se houver representante cadastrado para o cliente, seleciona os dados do mesmo
        Set OraParameters = db.Parameters
        OraParameters.Remove "CODCLI": OraParameters.Add "CODCLI", 0, 1
        OraParameters.Remove "CODREPR": OraParameters.Add "CODREPR", Val(objCliente!Cod_Repr_Vend.Value), 1
        OraParameters.Remove "TP_CONS": OraParameters.Add "TP_CONS", 1, 1
        OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
        OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
        
        Criar_Cursor OraParameters, "PM_CURSOR1"
        vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD060.Pr_Verifica_Repres(:PM_CURSOR1,:CODCLI, :CODREPR, :TP_CONS, :vErro, :vMsgErro)")
        
        If vErro <> "" Then
            Exit Sub
        End If
        
        Set objRepres = OraParameters("PM_CURSOR1").Value
        '---------------------------------------------------------------------------
        
        If objRepres.EOF Then
            lblRepresDPK.Caption = "Representante DPK"
        Else
            
            '14/12/04 Vou mostrar sempre em branco qdo. a inf.vier da tab. CLIENTE
            'pois vale somente o da r_clie_repres
            lblRepresDPK.Caption = "Representante DPK"
            
            'lblRepresDPK.Caption = "Representante DPK: " & Format(objRepres!cod_Repres.Value, "0000")
            'lblRepresDPK.Caption = lblRepresDPK.Caption & "." & objRepres!seq_franquia.Value
        End If
        
        'Mostrar sempre este representante em branco pois deve prevalecer o que esta na r_clie_repres (14/12/04)
        'cboRepresDPK.Text = IIf(IsNull(objRepres!nome_Repres.Value), "", objRepres!nome_Repres.Value)
        cboRepresDPK.Text = ""
        
        vFilialRepres = IIf(IsNull(objRepres!Cod_Filial.Value), "", objRepres!Cod_Filial.Value)
        vReprDPKGravar = IIf(IsNull(objCliente!Cod_Repr_Vend.Value), "", objCliente!Cod_Repr_Vend.Value)
        cboRepresDPK.Enabled = False
    
    'Se n�o houver representante cadastrado para o cliente, verifica se algum j� est� cadastrado
    Else
        lblRepresDPK.Caption = "Representante DPK"
        cboRepresDPK.Text = ""
        
        '-----------------------------------------------------------------------
        'SELECT
        ' Select que traz as informa��es da carteira de cliente do representante
        Set OraParameters = db.Parameters
        OraParameters.Remove "CODCLI": OraParameters.Add "CODCLI", CDbl(txtCodCliente.Text), 1
        OraParameters.Remove "CODREPR": OraParameters.Add "CODREPR", 0, 1
        OraParameters.Remove "TP_CONS": OraParameters.Add "TP_CONS", 2, 1
        OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
        OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
        Criar_Cursor OraParameters, "PM_CURSOR1"
        vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD060.Pr_Verifica_Repres(:PM_CURSOR1, :CODCLI, :CODREPR, :TP_CONS, :vErro, :vMsgErro)")
        If vErro <> "" Then
            Exit Sub
        End If
        Set objRepres = OraParameters("PM_CURSOR1").Value
        
        If objRepres.EOF Then
           cboRepresDPK.Enabled = True
           Fl_Ja_Relacionou = "N"
        Else
           cboRepresDPK.Enabled = False
           Fl_Ja_Relacionou = "S"
        End If
        
        'Inserido para resolver problema de estar ficando o campo de repres. desabilitado
        If Fl_Cliente_Novo = "S" Then
            cboRepresDPK.Enabled = True
            Fl_Ja_Relacionou = "N"
        End If
        
        vReprDPKGravar = ""
    End If
    '--------------------------------
    'REPRESENTANTE BLAU
    '--------------------------------
    'vReprBLAUGravado = IIf(IsNull(objCliente!Cod_Repres_Blau.Value), 0, objCliente!Cod_Repres_Blau.Value)
    'vReprBLAUGravar = vReprBLAUGravado
    'If IsNull(objCliente!Cod_Repres_Blau.Value) Then
    '    vReprAux = 0
    'Else
    '    vReprAux = objCliente!Cod_Repres_Blau.Value
    'End If
    'If Val(vReprAux) <> 0 Then
    '    '-----------------------------------------------------------------------
    '    'SELECT
    '    'Se houver representante cadastrado para o cliente, carrega dados do mesmo (sequencia, franquia, filial)
    '    Set OraParameters = db.Parameters
    '    OraParameters.Remove "CODCLI": OraParameters.Add "CODCLI", 0, 1
    '    OraParameters.Remove "CODREPR": OraParameters.Add "CODREPR", Val(objCliente!Cod_Repres_Blau.Value), 1
    '    OraParameters.Remove "TP_CONS": OraParameters.Add "TP_CONS", 1, 1
    '    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    '    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    '    OraParameters("vErro").ServerType = ORATYPE_NUMBER
    '    'DLL - Fun��o que declara e retorna o cursor com dados do meu select
    '    'vVB_Generica_001.Retorna_Cursor db, "pm_CURSOR1", "PRODUCAO.PCK_CAD060.Pr_Verifica_Repres", "CODCLI", "CODREPR", "TP_CONS", "vErro", "vMsgErro"
    '    vVB_Generica_001.ExecutaPl db, "PRODUCAO.PCK_CAD060.Pr_Verifica_Repres( :CODCLI, :CODREPR, :TP_CONS, :vErro, :vMsgErro)"
    '    Set objRepres = OraParameters("PM_CURSOR1").Value
    '    If Val(OraParameters("vErro").Value) <> 0 Then
    '        MsgBox "Erro: " & Val(OraParameters("vErro").Value) & " - " & _
    '                          OraParameters("vMsgErro").Value & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
    '        Exit Sub
    '    End If
    '    '---------------------------------------------------------------------------
    '     If objRepres.EOF Then
    '        '-----------------------------------------------------------------------
    '        'SELECT
    '        'Se n�o achou, seleciona somente os dados gerais do representante (sem filial, franquia, etc)
    '        Set OraParameters = db.Parameters
    '        OraParameters.Remove "CODCLI": OraParameters.Add "CODCLI", 0, 1
    '        OraParameters.Remove "CODREPR": OraParameters.Add "CODREPR", Val(objCliente!Cod_Repres_Blau.Value), 1
    '        OraParameters.Remove "TP_CONS": OraParameters.Add "TP_CONS", 3, 1
    '        OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    '        OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    '        OraParameters("vErro").ServerType = ORATYPE_NUMBER
    '        'DLL - Fun��o que declara e retorna o cursor com dados do meu select
    '        'vVB_Generica_001.Retorna_Cursor db, "pm_CURSOR1", "PRODUCAO.PCK_CAD060.Pr_Verifica_Repres", "CODCLI", "CODREPR", "TP_CONS", "vErro", "vMsgErro"
    '        vVB_Generica_001.ExecutaPl db, "PRODUCAO.PCK_CAD060.Pr_Verifica_Repres( :CODCLI, :CODREPR, :TP_CONS, :vErro, :vMsgErro)"
    '        Set objRepres = OraParameters("PM_CURSOR1").Value
    '        If Val(OraParameters("vErro").Value) <> 0 Then
    '            MsgBox "Erro: " & Val(OraParameters("vErro").Value) & " - " & _
    '                              OraParameters("vMsgErro").Value & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
    '            Exit Sub
    '        End If
    '        '---------------------------------------------------------------------------
    '    End If
    '    lblRepresBlau.Caption = "Representante Blau: " & Format(objRepres!cod_Repres.Value, "0000") & ".0"
    '    cboRepresBlau.Text = objRepres!nome_Repres.Value
    '    vReprBLAUGravar = objCliente!Cod_Repres_Blau.Value
    ''Se n�o houver representante cadastrado para o cliente, deixa inabilitado
    'Else
    '    lblRepresBlau.Caption = "Representante Blau"
    '    cboRepresBlau.Text = ""
    '    vReprBLAUGravar = ""
    'End If
    'cboRepresBlau.Enabled = IIf(Val(vReprBLAUGravado) = 0, True, False)
        
    '-----------------------------------------------------------------------
    'SELECT
    ' Select que traz as informa��es da carteira de cliente do representante - Verifica se j� existe clie_repres
    'Set OraParameters = db.Parameters
    'OraParameters.Remove "CODCLI": OraParameters.Add "CODCLI", CDbl(txtCodCliente.Text), 1
    'OraParameters.Remove "CODREPR": OraParameters.Add "CODREPR", 0, 1
    'OraParameters.Remove "TP_CONS": OraParameters.Add "TP_CONS", 2, 1
    'OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    'OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    'Criar_Cursor OraParameters, "PM_CURSOR1"
    'vVB_Generica_001.ExecutaPl db, "PRODUCAO.PCK_CAD060.Pr_Verifica_Repres(:PM_CURSOR1, :CODCLI, :CODREPR, :TP_CONS, :vErro, :vMsgErro)"
    'Set objRepres = OraParameters("PM_CURSOR1").Value
    'If Val(OraParameters("vErro").Value) <> 0 Then
    '    MsgBox "Erro: " & Val(OraParameters("vErro").Value) & " - " & _
    '                      OraParameters("vMsgErro").Value & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
    '    Exit Sub
    'End If
    '---------------------------------------------------------------------------
    'If objRepres.EOF Then
    '   Fl_Ja_Relacionou = "N"
    'Else
    '   Fl_Ja_Relacionou = "S"
    'End If
            
    If Fl_Cliente_Novo = "S" And Val(vReprDPKGravado) = 0 Then
       cboRepresDPK.Enabled = True
    ElseIf Fl_Cliente_Novo = "N" And Val(vReprDPKGravado) = 0 And Fl_Ja_Relacionou = "S" Then
       cboRepresDPK.Enabled = False
    ElseIf Fl_Cliente_Novo = "N" And Val(vReprDPKGravado) = 0 And Fl_Ja_Relacionou = "N" Then
       cboRepresDPK.Enabled = True
    End If
    '--------------------------------
    'DADOS DE COBRAN�A - BANCO/CHEQUE
    '--------------------------------
    'cboBanco.Text = IIf(IsNull(objCliente!cod_banco.Value), "", objCliente!sigla.Value & " - " & Format(objCliente!cod_banco.Value, "000"))
    cboBanco.Text = IIf(IsNull(objCliente!cod_banco.Value), "", IIf(Val(objCliente!cod_banco.Value) = 0, "", objCliente!sigla.Value & " - " & Format(objCliente!cod_banco.Value, "000")))
    
    
    Select Case objCliente!Tp_ordem_pgto.Value
        Case "A": cboTpOP.Text = "A - NORMAL" '"A - APOS" DF#356 n�o estava carregando quando o valor salvo = A
        Case "B": cboTpOP.Text = "B - BOLETO"
        Case Else: cboTpOP.Text = "  - ANTES"
    End Select
    Select Case objCliente!cheque_sf.Value
        Case " ": cboCheque.Text = " "
        Case "0": cboCheque.Text = "0 - SEM CHEQUE S/FUNDO"
        Case "1": cboCheque.Text = "1 - TEVE 1"
        Case "2": cboCheque.Text = "2 - TEVE 2 OU MAIS"
    End Select
    'Eduardo 22/07/05
    If Not IsNull(objCliente!cod_ctr_protesto.Value) Then
       cboProtesto.Text = Format(objCliente!cod_ctr_protesto.Value, "@@") & IIf(objCliente!cod_ctr_protesto.Value = "NP", " - N�O PROTESTAR", "")
    End If
    cboClassificacao.Text = objCliente!classificacao.Value
    '--------------------------------
    'TRANSPORTADORA
    '--------------------------------
    If objCliente!Cod_transp.Value = 0 Then
        lblTransp.Caption = "Transportadora"
        cboTransportadora = ""
    Else
        lblTransp.Caption = "Transportadora: " & Format(objCliente!Cod_transp.Value, "0000")
        cboTransportadora = objCliente!nome_transp.Value & " - " & Format(objCliente!Cod_transp.Value, "0000")
    End If
    '--------------------------------
    'MENSAGEM DO CLIENTE
    '--------------------------------
    cboMsgCliente.Text = IIf(IsNull(objCliente!Cod_Mensagem.Value), "", objCliente!Desc_Mens.Value & " - " & Format(objCliente!Cod_Mensagem.Value, "00"))
    txtDtMsg.Text = IIf(IsNull(objCliente!dataMSG.Value), "", objCliente!dataMSG.Value)
    '--------------------------------
    'MENSAGEM FISCAL
    '--------------------------------
    cboMsgFiscal.Text = IIf(IsNull(objCliente!CodMsgFis.Value), "", objCliente!MsgFis.Value & " - " & Format(objCliente!CodMsgFis.Value, "00"))
    '--------------------------------
    'DADOS DA PROPRIEDADE E PARTICULARIDADES DO CLIENTE
    '--------------------------------
    txtResponsavel.Text = IIf(IsNull(objCliente!nome_respons.Value), "", objCliente!nome_respons.Value)
    txtProprietario.Text = IIf(IsNull(objCliente!nome_proprietario.Value), "", objCliente!nome_proprietario.Value)
    chkPredioProprio.Value = IIf(IsNull(objCliente!Propriedade.Value), Unchecked, IIf(objCliente!Propriedade.Value = "S", Checked, Unchecked))
    chkConsumidorFinal.Value = IIf(IsNull(objCliente!fl_cons_final.Value), Unchecked, IIf(objCliente!fl_cons_final.Value = "S", Checked, Unchecked))
    chkUtility.Value = IIf(IsNull(objCliente!Fl_Creden.Value), Unchecked, IIf(objCliente!Fl_Creden.Value = "S", Checked, Unchecked))
    chkExportacao.Value = IIf(Not IsNull(objCliente!CliExp.Value), Checked, Unchecked)
    txtDtFundacao.Text = objCliente!dt_fundacao.Value
    '--------------------------------
    'CATEGORIA BOSCH DIESEL
    '--------------------------------
    If Not IsNull(objCliente!Categoria.Value) Then
       cboCategBosch = IIf(IsNull(objCliente!Categoria.Value), "", FnCateg(objCliente!Categoria.Value, 1)) 'TI-3221
       cboFornVDR.ListIndex = FnFornVDR(IIf(IsNull(objCliente!Categoria.Value), "", objCliente!Categoria.Value)) 'TI-3221
    End If
    If Trim(cboCategBosch.Text) <> "" Then
        fraCategBosch.Visible = True
        Call cmdLinVdr_Click
    End If
    '--------------------------------
    'CLIENTE TARE
    '--------------------------------
    txtCodTare.Text = IIf(IsNull(objCliente!cod_tare.Value), "", Val(objCliente!cod_tare.Value))
    txtDtTare.Text = IIf(IsNull(objCliente!dt_tare.Value), "", objCliente!dt_tare.Value)
    '--------------------------------
    'DADOS DE CR�DITO DO CLIENTE
    '--------------------------------
    vVlMaiorSlDevGravado = "0"
    txtVlMaiorSlDev = IIf(IsNull(objCliente!MAIOR_SALDEV.Value), 0, Format(objCliente!MAIOR_SALDEV.Value, "0.00"))
    vVlMaiorSlDevGravado = txtVlMaiorSlDev
    txtDtMaiorSlDev.Enabled = True
    txtDtMaiorSlDev.Text = IIf(IsNull(objCliente!Dt_Maior_Saldev), "", objCliente!Dt_Maior_Saldev)
    txtDtMaiorSlDev.Enabled = False
    vSaldoPedidos = IIf(IsNull(objCliente!SALDO_PEDIDOS.Value), 0, objCliente!SALDO_PEDIDOS.Value)
    'Novos campos inseridos em Julho/2005 - Alethea
    txtLimite = IIf(IsNull(objCliente!LIMITE.Value), 0, Format(objCliente!LIMITE.Value, "0.00"))
    txtDtLimite.Text = IIf(IsNull(objCliente!DT_LIMITE), "", objCliente!DT_LIMITE)
    lblVlPrevCpra = IIf(IsNull(objCliente!VL_PREV_CPRA.Value), 0, Format(objCliente!VL_PREV_CPRA.Value, "0.00"))
    '--------------------------------
    'INSCRI��O SUFRAMA
    '--------------------------------
    txtInscrSuframa.Text = IIf(IsNull(objCliente!inscr_suframa.Value), "", objCliente!inscr_suframa.Value)
    If chkConsumidorFinal.Value = 0 And Trim(txtInscrEstadual.Text) <> "ISENTO" And _
       InStr(1, "AM_RO_RR_AP_AC", cboUF.Text) <> 0 And vSaldoPedidos = 0 Then
        txtInscrSuframa.Enabled = True
    Else
        txtInscrSuframa.Enabled = False
    End If
    'SDS 2557 - Marco Aurelio Andrade Jr - Leitura do C�digo CNAE que esta cadastrado
    '--------------------------------
    'C�DIGO CNAE
    '--------------------------------
    If (objCliente!cod_cnae.Value <> "") Then
    cboCNAE.Text = IIf(IsNull(objCliente!cod_cnae.Value), "", Trim(objCliente!cod_cnae.Value) & " - " & objCliente!Descricao.Value)
    End If
    sspCadCli.Caption = ""
    Screen.MousePointer = 0
    
End Sub

Sub Limpar_Campos_Cadastro(pLimpaCod As Boolean)

    If pLimpaCod = True Then
        txtCodCliente.Text = ""
        cboCliente.Text = ""
    End If
    txtDtCadastro.Text = ""
    txtDtRecadastro.Text = ""
    txtDtSolicitacao.Text = ""
    txtSituacao.Text = ""
    'pictSituacao.Picture = ""
    txtVlMaiorSlDev.Enabled = True
    txtDtMaiorSlDev.Enabled = True
    txtVlMaiorSlDev.Text = ""
    txtDtMaiorSlDev.Text = ""
    txtDtMaiorSlDev.Enabled = False
    'Novos campos inseridos em Julho/05 - Alethea
    txtLimite.Text = ""
    txtDtLimite.Text = ""
    lblVlPrevCpra.Caption = ""
    '--
    txtCodTare.Text = ""
    txtDtTare.Text = ""
    txtInscrSuframa.Text = ""
    cboTpDocto.ListIndex = -1
    txtDocto.Text = ""
    txtInscrEstadual.Text = ""
    cboStatus.ListIndex = -1
    If cboStatus.ListCount > 0 Then cboStatus = "0 - NORMAL"
    cboCaracteristica.ListIndex = -1
    If cboCaracteristica.ListCount > 0 Then cboCaracteristica = "N - NORMAL"
    txtEndereco.Text = ""
    txtNumero.Text = ""
    txtComplemento.Text = ""
    txtBairro.Text = ""
    txtCep.Text = ""
    cboUF.Text = "SP"
    Call cboUF_Click
    cboCidade.ListIndex = -1
    txtDDD1.Text = ""
    txtFone1.Text = ""
    txtDDD2.Text = ""
    txtFone2.Text = ""
    txtFax.Text = ""
    txtContato.Text = ""
    txtCxPostal.Text = ""
    txtTelex.Text = ""
    txtEmail.Text = ""
    txtHomePage.Text = ""
    cboTpSegmento.ListIndex = -1
    cboTpCliente.ListIndex = -1
    cboRepresDPK.Enabled = True
    cboRepresDPK = ""
    grdflexRepres.Visible = False
    'cboTpClienteBlau.Text = ""
    'cboRepresBlau.Text = ""
    cboBanco.ListIndex = -1
    If cboBanco.ListCount > 0 Then cboBanco = "BRASIL - 001"
    cboTpOP.ListIndex = -1
    If cboTpOP.ListCount > 0 Then cboTpOP = "  - ANTES"
    If cboCheque.ListCount > 0 Then cboCheque = "0 - SEM CHEQUE S/FUNDO"
    cboProtesto.ListIndex = -1
    cboClassificacao.ListIndex = -1
    If cboClassificacao.ListCount > 0 Then cboClassificacao = "X"
    cboTransportadora.ListIndex = -1
    If cboTransportadora.ListCount > 0 Then cboTransportadora = ". - 0999"
    cboTpEmpresa.ListIndex = -1
    cboDiferenciado.ListIndex = 1
    'If cboTpEmpresa.ListCount > 0 Then cboTpEmpresa = "E - EMPRESA"
    cboMsgCliente.Text = ""
    txtDtMsg.Text = ""
    txtResponsavel.Text = ""
    cboMsgFiscal.Text = ""
    txtProprietario.Text = ""
    txtDtFundacao.Text = ""
    txtGrupoMaster.Text = ""
    cboCategBosch.ListIndex = -1
    cboFornVDR.ListIndex = -1 'TI-3221
    chkPredioProprio.Value = 0
    chkConsumidorFinal.Value = 0
    chkUtility.Value = 0
    chkExportacao.Value = 0
    chkGrupoMaster.Value = 0
    fraCategBosch.Visible = False
    cboCodCliGrMaster.Text = ""
    lblRecusado2.Caption = ""
    'SDS 2557 - Marco Aurelio Andrade Jr - Limpando campo CNAE
    cboCNAE.Text = ""
    cboCNAE.ListIndex = -1
    
    ' fpallini - Ci&T - 21/08/12 - Limpar N�mero SAP
    txtSAP.Text = ""

End Sub


Sub pGrava_Ref_Bancarias()
    
    Set OraParameters = db.Parameters
    OraParameters.Remove "CODCLI": OraParameters.Add "CODCLI", Val(txtCodCliente.Text), 1
    OraParameters.Remove "SEQ": OraParameters.Add "SEQ", Val(txtSeq.Text), 1
    OraParameters.Remove "BANCO": OraParameters.Add "BANCO", txtBanco.Text, 1
    OraParameters.Remove "DTCONS": OraParameters.Add "DTCONS", txtDt.Text, 1
    OraParameters.Remove "AGENCIA": OraParameters.Add "AGENCIA", Val(txtAgencia.Text), 1
    OraParameters.Remove "DDD": OraParameters.Add "DDD", Val(txtDDD.Text), 1
    OraParameters.Remove "FONE": OraParameters.Add "FONE", Val(txtFone.Text), 1
    OraParameters.Remove "CONCEITO": OraParameters.Add "CONCEITO", txtConceito.Text, 1
    OraParameters.Remove "OBSERVACAO": OraParameters.Add "OBSERVACAO", txtObs.Text, 1
    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    
    db.Executesql "BEGIN PRODUCAO.PCK_CLIENTE.PR_INSERT_REF_BANCO" & _
                             "(:CODCLI,:SEQ,:BANCO,TO_DATE(:DTCONS,'DD/MM/RR')," & _
                             " :AGENCIA,:DDD,:FONE,:CONCEITO,:OBSERVACAO," & _
                             " :vErro,:vMsgErro); END;"
                             
    If Val(IIf(IsNull(db.Parameters("vErro")), 0, db.Parameters("vErro"))) <> 0 Then
        MsgBox "Erro: " & Val(OraParameters("vErro").Value) & " - " & _
                          OraParameters("vMsgErro").Value & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
        Exit Sub
    End If
    
    lblMsgBco.Visible = True

End Sub

Sub pGrava_Ref_Comerciais()
    
    Set OraParameters = db.Parameters
    OraParameters.Remove "CODCLI": OraParameters.Add "CODCLI", Val(txtCodCliente.Text), 1
    OraParameters.Remove "SEQ": OraParameters.Add "SEQ", Val(txtSeq01.Text), 1
    OraParameters.Remove "DTCONS": OraParameters.Add "DTCONS", txtDtCons01.Text, 1
    OraParameters.Remove "EMPRESA": OraParameters.Add "EMPRESA", txtEmp01.Text, 1
    OraParameters.Remove "DTCAD": OraParameters.Add "DTCAD", txtDtCad01.Text, 1
    OraParameters.Remove "ULTFAT": OraParameters.Add "ULTFAT", Val(txtVlUltFat01.Text), 1
    OraParameters.Remove "DTULTFAT": OraParameters.Add "DTULTFAT", txtDtUltFat01.Text, 1
    OraParameters.Remove "SLDEV": OraParameters.Add "SLDEV", Val(txtMaiSlDev01.Text), 1
    OraParameters.Remove "ATRASO": OraParameters.Add "ATRASO", Val(txtAtraso01.Text), 1
    OraParameters.Remove "CONCEITO": OraParameters.Add "CONCEITO", txtConceito01.Text, 1
    OraParameters.Remove "OBSERVACAO": OraParameters.Add "OBSERVACAO", txtObs01.Text, 1
    OraParameters.Remove "DDD": OraParameters.Add "DDD", Val(txtDDD01.Text), 1
    OraParameters.Remove "FONE": OraParameters.Add "FONE", Val(txtFone01.Text), 1
    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    
    db.Executesql "BEGIN PRODUCAO.PCK_CLIENTE.PR_INSERT_REF_COML" & _
                             "(:CODCLI,:SEQ,TO_DATE(:DTCONS,'DD/MM/RR')," & _
                             " :EMPRESA,TO_DATE(:DTCAD,'DD/MM/RR'),:ULTFAT," & _
                             " TO_DATE(:DTULTFAT,'DD/MM/RR'),:SLDEV,:ATRASO,:CONCEITO,:OBSERVACAO," & _
                             " :DDD,:FONE,:vErro,:vMsgErro); END;"
                             
    If Val(IIf(IsNull(db.Parameters("vErro")), 0, db.Parameters("vErro"))) <> 0 Then
        MsgBox "Erro: " & Val(OraParameters("vErro").Value) & " - " & _
                          OraParameters("vMsgErro").Value & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
        Exit Sub
    End If
    
    lblMsgCom.Visible = True
    
End Sub


Sub pGrava_Out_Enderecos()

    Set OraParameters = db.Parameters
    OraParameters.Remove "CODCLI": OraParameters.Add "CODCLI", Val(txtCodCliente.Text), 1
    OraParameters.Remove "TPEND": OraParameters.Add "TPEND", Val(Left(cboTpEnd01.Text, 1)), 1
    OraParameters.Remove "SEQ": OraParameters.Add "SEQ", Val(txtSeqEnd01.Text), 1
    
    'Kevlin Toshinari Ossada - CIT - 22/8/2012 - Concatenado campos referente a enderecos para passar como parametro
    OraParameters.Remove "END": OraParameters.Add "END", ConcatenarEnderecoSAP(txtEnd01.Text, txtNumEnd01.Text, txtComplEnd01.Text), 1
    
    OraParameters.Remove "CODCID": OraParameters.Add "CODCID", Val(Right(cboCidadeEnd01.Text, 4)), 1
    OraParameters.Remove "BAIRRO": OraParameters.Add "BAIRRO", txtBairroEnd01.Text, 1
    OraParameters.Remove "CEP": OraParameters.Add "CEP", Val(txtCepEnd01.Text), 1
    OraParameters.Remove "DDD": OraParameters.Add "DDD", Val(txtDDDEnd01.Text), 1
    OraParameters.Remove "FONE": OraParameters.Add "FONE", Val(txtFoneEnd01.Text), 1
    OraParameters.Remove "FAX": OraParameters.Add "FAX", Val(txtFaxEnd01.Text), 1
    OraParameters.Remove "CXPOST": OraParameters.Add "CXPOST", Val(txtCxPosEnd01.Text), 1
    OraParameters.Remove "TPDOC": OraParameters.Add "TPDOC", Val(Left(cboTpDoctoEnd01.Text, 1)), 1
    OraParameters.Remove "CGC": OraParameters.Add "CGC", Val(txtCGCEnd01.Text), 1
    OraParameters.Remove "IE": OraParameters.Add "IE", txtIEEnd01.Text, 1
    OraParameters.Remove "NOME": OraParameters.Add "NOME", txtNomeEnd01.Text, 1
    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    
   
    db.Executesql "BEGIN PRODUCAO.PCK_CLIENTE.PR_INSERT_OUT_END" & _
                             "(:CODCLI,:TPEND,:SEQ,:END,:CODCID,:BAIRRO,:CEP,:DDD," & _
                             " :FONE,:FAX,:CXPOST,:TPDOC,:CGC,:IE,:NOME,:vErro,:vMsgErro); " & _
                        "END;"
                             
    If Val(IIf(IsNull(db.Parameters("vErro")), 0, db.Parameters("vErro"))) <> 0 Then
        MsgBox "Erro: " & Val(OraParameters("vErro").Value) & " - " & _
                          OraParameters("vMsgErro").Value & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
        Exit Sub
    End If
    
    lblMsgEnd.Visible = True
    
End Sub


Sub pGravaDadosdoCliente(oCliente As ProxySAP.ClienteSAP)

Dim vCodCliente As Long, vFl_Cliente_Diesel As String
Dim vMaior_SalDev As Double, vDt_Maior_SalDev As String
Dim SALDO_PEDIDOS As Double, SALDO_BORDERO As Double, MAIOR_FATURA As Double
Dim NOTA_CREDITO As Double, ATRASO_MEDIO As Double, INTERVALO_ATR_MEDIO As Double
Dim MEDIA_COMPRA As Double, NOTA_FREQUENCIA As Double
Dim vReprCliente As String
Dim lstrSQL As String, mSql As String
Dim Sql_conexao As String, objConexao As Object
Dim rst As Object
Dim flCliExp As Boolean
Dim vLimite As Double, vDt_Limite As String, vVlPrevCpra As Double

Dim vCd As String

'//-----------------------------
'Cliente Novo � Incluido On-Line
'Mas n�o � Alterado On-Line
'-----------------------------//

'Faz as valida��es dos campos
If Valida_Campos_Cadastro = False Then
    vFl_Campos_OK = False
    Exit Sub
Else
    vFl_Campos_OK = True
End If

'Se estiver tudo ok, grava
If MsgBox("Confirma " & IIf(Trim(txtCodCliente.Text) = "", "inclus�o", "altera��o") & " de Dados do Cliente ?", vbQuestion + vbYesNo, "Aten��o") = vbYes Then
 
    'Enviando dados para o SAP
    If EnviarClienteSAP(oCliente) Then
        txtSAP.Text = oCliente.NoClienteSAP
    Else
        MsgBox "Erro ao gravar o cliente no SAP. Cadastro Abortado.", vbCritical, "CAD060 - Aten��o"
        vFl_Campos_OK = False
        Exit Sub
    End If

    'INCLUS�O DE CLIENTE
    If txtCodCliente.Text = "" Then
        
        Fl_Cliente_Novo = "S"
        Fl_Ja_Relacionou = "S"
        
        'Vou selecionar o c�digo para este novo cliente
        Set OraParameters = db.Parameters
        OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
        OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
        
        Criar_Cursor OraParameters, "PM_CURSOR1"
        
        vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD060.Pr_Novo_CodCliente(:PM_CURSOR1, :vErro, :vMsgErro)")
        If vErro <> "" Then
            Exit Sub
        End If
        Set rst = OraParameters("PM_CURSOR1").Value
        
        vCodCliente = Val(rst!CodCli)
    
    'ALTERA��O DE CLIENTE
    Else
        vCodCliente = Val(txtCodCliente.Text)
        Fl_Cliente_Novo = "N"
        'Altera��o de cliente, n�o pode alterar representante - quem faz isso � o SPC
    End If

    If Trim(cboCategBosch.Text) <> "" Then
       vFl_Cliente_Diesel = "S"
    Else
       vFl_Cliente_Diesel = "N"
    End If
  
  
    frmNotificaCD.lblInicioCD01.Caption = Time
    frmNotificaCD.chk01.Value = 1
  
    
    'Incluido por Aleth�a, devido � check constraint de Nota Frequencia
    'Se for cliente novo, a procedure vai fazer insert de todos os campos abaixo, mas se for altera��o, vai somente alterar o maior sl.devedor e a dt.maior sl.dev
    SALDO_PEDIDOS = 0
    SALDO_BORDERO = 0
    vMaior_SalDev = IIf(txtVlMaiorSlDev.Text = "", 0, txtVlMaiorSlDev.Text)
    'vDt_Maior_SalDev = txtDtCadastro.Text
    vDt_Maior_SalDev = IIf(txtDtMaiorSlDev = "", txtDtCadastro, txtDtMaiorSlDev)
    MAIOR_FATURA = 0
    NOTA_CREDITO = 0
    ATRASO_MEDIO = 0
    INTERVALO_ATR_MEDIO = 0
    MEDIA_COMPRA = 0
    NOTA_FREQUENCIA = 1
    'Novos campos inseridos em Julho/05 - Alethea
    vLimite = IIf(txtLimite.Text = "", 0, txtLimite.Text)
    vDt_Limite = IIf(txtDtLimite.Text = "", "", txtDtLimite.Text)
    If lblVlPrevCpra.Caption = "" Then
        vVlPrevCpra = 0
    Else
        vVlPrevCpra = CDbl(lblVlPrevCpra.Caption)
    End If
    
    
    'Declara��o de binds
    With db.Parameters
        .Remove "CODCLI":    .Add "CODCLI", vCodCliente, 1
        .Remove "CGC":       .Add "CGC", Val(txtDocto.Text), 1
        .Remove "TPDOC":     .Add "TPDOC", Val(Left(cboTpDocto.Text, 1)), 1
        .Remove "NOME":      .Add "NOME", Left(Trim(cboCliente.Text), 30), 1
        .Remove "CONTATO":   .Add "CONTATO", IIf(txtContato.Text = "", Null, txtContato.Text), 1
        .Remove "CODTARE":   .Add "CODTARE", IIf(Trim(txtCodTare.Text) = "", 0, Val(txtCodTare.Text)), 1
        .Remove "DTTARE":    .Add "DTTARE", IIf(Trim(txtDtTare.Text) = "", Null, txtDtTare.Text), 1
        .Remove "CLASS":     .Add "CLASS", cboClassificacao.Text, 1
        .Remove "STATUS":    .Add "STATUS", Val(Left(cboStatus.Text, 1)), 1
        .Remove "CARAC":     .Add "CARAC", Left(cboCaracteristica.Text, 1), 1
        'Eduardo Faria de OLiveira - CIT - 22/08/2012 - Endere�o seraparado por v�rgula
        '.Remove "END":       .Add "END", IIf(txtEndereco.Text = "", Null, txtEndereco.Text), 1
        '.Remove "ENDERECO":  .Add "ENDERECO", IIf(txtEndereco.Text = "", Null, txtEndereco.Text), 1
        .Remove "END":       .Add "END", IIf(txtEndereco.Text = "", Null, ConcatenarEnderecoSAP(txtEndereco.Text, txtNumero.Text, txtComplemento.Text)), 1
        .Remove "ENDERECO":  .Add "ENDERECO", IIf(txtEndereco.Text = "", Null, ConcatenarEnderecoSAP(txtEndereco.Text, txtNumero.Text, txtComplemento.Text)), 1
        .Remove "CODCID":    .Add "CODCID", Val(Right(cboCidade.Text, 4)), 1
        .Remove "BAI":       .Add "BAI", IIf(Trim(txtBairro.Text) = "", " ", txtBairro.Text), 1
        .Remove "DDD1":      .Add "DDD1", IIf(Trim(txtDDD1.Text) = "", 0, Val(txtDDD1.Text)), 1
        .Remove "DDD2":      .Add "DDD2", IIf(Trim(txtDDD2.Text) = "", 0, Val(txtDDD2.Text)), 1
        .Remove "Fone2":     .Add "Fone2", IIf(Trim(txtFone2.Text) = "", 0, Val(txtFone2.Text)), 1
        .Remove "Fone1":     .Add "Fone1", IIf(Trim(txtFone1.Text) = "", 0, Val(txtFone1.Text)), 1
        .Remove "Cep":       .Add "Cep", IIf(Trim(txtCep.Text) = "", 0, Val(txtCep.Text)), 1
        .Remove "CXPOS":     .Add "CXPOS", IIf(Trim(txtCxPostal.Text) = "", 0, Val(txtCxPostal.Text)), 1
        .Remove "TELEX":     .Add "TELEX", IIf(Trim(txtTelex.Text) = "", 0, Val(txtTelex.Text)), 1
        .Remove "FAX":       .Add "FAX", IIf(Trim(txtFax.Text) = "", 0, Val(txtFax.Text)), 1
        .Remove "IEST":      .Add "IEST", Trim(txtInscrEstadual.Text), 1
        'Inscri��es suframa bloqueadas, conf.solicita��o discal de dez/04
        '.Remove "ISUF":      .Add "ISUF", IIf(Trim(txtInscrSuframa.Text) = "", Null, txtInscrSuframa.Text), 1
        .Remove "ISUF":      .Add "ISUF", Null, 1
        .Remove "DTCAD":     .Add "DTCAD", IIf(Trim(txtDtCadastro.Text) = "", Null, txtDtCadastro.Text), 1
        .Remove "DTRECAD":   .Add "DTRECAD", IIf(Trim(txtDtRecadastro.Text) = "", Null, txtDtRecadastro.Text), 1
        .Remove "TPCLI":     .Add "TPCLI", IIf(Trim(cboTpCliente.Text) = "", "", Right(cboTpCliente.Text, 2)), 1
        .Remove "CODTRAN":   .Add "CODTRAN", IIf(IsNumeric(Right(lblTransp.Caption, 4)), Val(Right(lblTransp.Caption, 4)), 999), 1
        .Remove "CODREPRD":  .Add "CODREPRD", 0, 1
        '.Remove "CODREPRB":  .Add "CODREPRB", IIf(IsNumeric(Right(lblRepresBlau.Caption, 6)), Val(vReprBLAUGravar), 0), 1
        .Remove "CODREPRB":  .Add "CODREPRB", 0, 1
        .Remove "CODBCO":    .Add "CODBCO", Val(Right(cboBanco.Text, 3)), 1
        .Remove "TPOP":      .Add "TPOP", IIf(Trim(Left(cboTpOP.Text, 1)) = "", Null, Left(cboTpOP.Text, 1)), 1
        .Remove "SIT":       .Add "SIT", Val(Left(txtSituacao, 1)), 1
        .Remove "CODMSG":    .Add "CODMSG", IIf(IsNumeric(Right(cboMsgCliente, 2)), Val(Right(cboMsgCliente, 2)), 0), 1
        
        'Este campo (Msg. fISCAL Cliente) deve atualizar on-line nos CDs, conforme solicita��o de jan/07 - Alethea
        'Conforme solicita��o do Fiscal em 30/01/07, todo cliente da UF = SC, n�o poder� ter antecipa��o tribut�ria - Aleth�a
        If cboUF = "SC" Then
            .Remove "CODMSGFI":  .Add "CODMSGFI", 53, 1
        Else
            .Remove "CODMSGFI":  .Add "CODMSGFI", IIf(IsNumeric(Right(cboMsgFiscal, 2)), Val(Right(cboMsgFiscal, 2)), 0), 1
        End If
        
        .Remove "DTMSG":     .Add "DTMSG", IIf(txtDtMsg.Text = "", Null, txtDtMsg.Text), 1
        .Remove "RESP":      .Add "RESP", IIf(txtResponsavel.Text = "", Null, txtResponsavel.Text), 1
        .Remove "PROTES":    .Add "PROTES", IIf(Trim(Left(cboProtesto.Text, 2)) = "", Null, Trim(Left(cboProtesto.Text, 2))), 1
        .Remove "DTFUND":    .Add "DTFUND", IIf(Trim(txtDtFundacao.Text) = "", Null, Trim(txtDtFundacao.Text)), 1
        .Remove "PREDIO":    .Add "PREDIO", IIf(chkPredioProprio.Value, "S", "N"), 1
        .Remove "CHEQUE":    .Add "CHEQUE", Val(Left(cboCheque.Text, 1)), 1
        '.Remove "TPCLIB":    .Add "TPCLIB", IIf(IsNumeric(Right(cboTpClienteBlau.Text, 2)), Val(Right(cboTpClienteBlau.Text, 2)), 0), 1
        .Remove "TPCLIB":    .Add "TPCLIB", 0, 1
        .Remove "PROPR":     .Add "PROPR", IIf(Trim(txtProprietario.Text) = "", Null, txtProprietario.Text), 1
        .Remove "CONSFI":    .Add "CONSFI", IIf(chkConsumidorFinal = False, "N", "S"), 1
        .Remove "TPEMPRESA": .Add "TPEMPRESA", IIf(Trim(Left(cboTpEmpresa.Text, 1)) = "", Null, Left(cboTpEmpresa.Text, 1)), 1
        ' Incluindo novo parametro para gravar no banco solicitado na SDS1889- Jairo Almeida - 22-02-2012
         .Remove "FL_DIF":    .Add "FL_DIF", Trim(strFLDiferenciado), 1
        '----------------Fim da solicita��o SDS1889---------------------------------
'        .Remove "novoDPK":   .Add "novoDPK", Val(vReprDPKGravar), 1
'        .Remove "velhoDPK":  .Add "velhoDPK", Val(vReprDPKGravado), 1
'        .Remove "novoBLAU":  .Add "novoBLAU", Val(vReprBLAUGravar), 1
'        .Remove "velhoBLAU": .Add "velhoBLAU", Val(vReprBLAUGravado), 1
        .Remove "dataHoje":  .Add "dataHoje", vDtHoje, 1
        
        'Clie_Credito
        .Remove "Saldo_Pedidos":       .Add "Saldo_Pedidos", SALDO_PEDIDOS, 1
        .Remove "Saldo_Bordero":       .Add "Saldo_Bordero", SALDO_BORDERO, 1
        .Remove "Maior_Saldev":        .Add "Maior_Saldev", vMaior_SalDev, 1
        .Remove "Dt_Maior_Saldev":     .Add "Dt_Maior_Saldev", IIf(vDt_Maior_SalDev = "", Null, vDt_Maior_SalDev), 1
        'Novos campos incluidos em Julho/05 - Alethea
        .Remove "Limite":              .Add "Limite", vLimite, 1
        .Remove "Dt_Limite":           .Add "Dt_Limite", IIf(vDt_Limite = "", Null, vDt_Limite), 1
        .Remove "Vl_Prev_Cpra":        .Add "Vl_Prev_Cpra", vVlPrevCpra, 1
        '--
        .Remove "Maior_Fatura":        .Add "Maior_Fatura", MAIOR_FATURA, 1
        .Remove "Nota_Credito":        .Add "Nota_Credito", NOTA_CREDITO, 1
        .Remove "Atraso_Medio":        .Add "Atraso_Medio", ATRASO_MEDIO, 1
        .Remove "Intervalo_Atr_Medio": .Add "Intervalo_Atr_Medio", INTERVALO_ATR_MEDIO, 1
        .Remove "Media_Compra":        .Add "Media_Compra", MEDIA_COMPRA, 1
        .Remove "Nota_Frequencia":     .Add "Nota_Frequencia", NOTA_FREQUENCIA, 1
        
        'Flags
        .Remove "CLIE_DIESEL": .Add "CLIE_DIESEL", vFl_Cliente_Diesel, 1
        .Remove "CLI_NOVO":    .Add "CLI_NOVO", Fl_Cliente_Novo, 1
        'Erros
        .Remove "vErro":       .Add "vErro", 0, 2
        .Remove "vMsgErro":    .Add "vMsgErro", "", 2
        'R_Clie_Repres
        .Remove "CODREPR":     .Add "CODREPR", Val(vReprDPKGravar), 1
        'Esta variavel foi criada para garantir que seja gravado o representante correto nos cds (on-line)
        vReprCliente = vReprDPKGravar
        'Clie_Creden
        .Remove "FL_CREDEN":   .Add "FL_CREDEN", IIf(chkUtility.Value, "S", "N"), 1
        'Vdr.Cliente_Categ_Vdr
        .Remove "CATEG":       .Add "CATEG", IIf(cboCategBosch.Text <> "", IIf(cboFornVDR.ListIndex = 1, FnCateg(cboCategBosch.Text, 0), cboCategBosch.Text), ""), 1 'TI-3221
        'Vdr.R_CLie_Linha_Produto
        .Remove "LINHA":       .Add "LINHA", "", 1
        'Marketing.Cliente_Internet
        .Remove "EMAIL":       .Add "EMAIL", txtEmail.Text, 1
        .Remove "HOMEPG":      .Add "HOMEPG", txtHomePage.Text, 1
        'SDS 2557 - Marco Aurelio Andrade Jr - Passado parametro para cadastro do Cliente
        'CNAE
        .Remove "COD_CNAE":    .Add "COD_CNAE", Trim(Val(Replace(Replace(Left(cboCNAE.Text, 9), "-", ""), "/", ""))), 1
        ' carlosj 16/08/2012 CiT - [ISA 260] Atualizar Legado com dados de cadastro de Clientes do SAP
        .Remove "COD_SAP":       .Add "COD_SAP", txtSAP.Text, 1
    End With
    
    db.Executesql "BEGIN PRODUCAO.PCK_CLIENTE.Pr_GRAVADADOS" & _
                  "(:CODCLI,:CGC,:TPDOC,:NOME,:CONTATO,:CLASS,:STATUS,:END,:CODCID," & _
                  " :BAI,:DDD1,:FONE1,:DDD2,:FONE2,:CEP,:CXPOS,:TELEX,:FAX,:IEST,:ISUF," & _
                  " :TPCLI,to_date(:DTCAD,'DD/MM/RR'),:CODTRAN,:CODREPRD,:CODREPRB," & _
                  " :CODBCO,:TPOP,:SIT,:CODMSG,:RESP,:PROTES,to_date(:DTFUND,'DD/MM/RR')," & _
                  " :PREDIO,:CHEQUE,:TPCLIB,:PROPR,:CONSFI,:TPEMPRESA,:FL_DIF,to_date(:DTMSG,'DD/MM/RR')," & _
                  " :CARAC,:CODTARE,to_date(:DTTARE,'DD/MM/RR'),:CLI_NOVO,:FL_CREDEN," & _
                  " :CLIE_DIESEL,:CATEG,:LINHA,:Maior_Saldev,to_date(:Dt_Maior_Saldev,'DD/MM/RR')," & _
                  " :Limite, to_date(:Dt_Limite,'DD/MM/RR'),:Vl_Prev_Cpra," & _
                  " :Saldo_Pedidos, :Saldo_Bordero, :Maior_Fatura, :Nota_Credito, " & _
                  " :Atraso_Medio, :Intervalo_Atr_Medio, :Media_Compra, :Nota_Frequencia, " & _
                  " :CODREPR,:EMAIL,:HOMEPG,:CODMSGFI,to_date(:DTRECAD,'DD/MM/RR'),:COD_CNAE, :COD_SAP," & _
                  " :vErro,:vMsgErro); " & _
                "END;"
    
    
    If Val(IIf(IsNull(db.Parameters("vErro")), 0, db.Parameters("vErro"))) <> 0 Then
        MsgBox "Erro " & db.Parameters("vErro") & "-" & db.Parameters("vMsgErro") & ". Avise o Depto.Suporte!", vbCritical, "CAD060 - Aten��o"
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    '------------------------------------------
    'Gravar R_Clie_Repres Para Cliente Antigo
    '------------------------------------------
    If Fl_Cliente_Novo = "N" And Fl_Ja_Relacionou = "N" Then
        db.Executesql "BEGIN PRODUCAO.PCK_CLIENTE.PR_GRAVAR_CLIE_REPRES" & _
                      "(:CODCLI,:CODREPR,:vErro,:vMsgErro); END;"
                      
        If Val(IIf(IsNull(db.Parameters("vErro")), 0, db.Parameters("vErro"))) <> 0 Then
            MsgBox "Erro " & db.Parameters("vErro") & "-" & db.Parameters("vMsgErro") & ". Avise o Depto.Suporte!", vbCritical, "CAD060 - Aten��o"
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
    End If

    frmNotificaCD.lblFinalCD01.Caption = Time
    
    'FIM DO PROCESSO PRINCIPAL DE INCLUS�O EM CAMPINAS
    'If Fl_Cliente_Novo = "S" Then
        'Retirada a mensagem conf. solic. M�nica - Alethea - 23/08/07
        'MsgBox "Registro Incluido em Campinas!", vbInformation, "CAD060 - Cadastro de Cliente"
    'End If
    
    'vReprBLAUGravado = vReprBLAUGravar
    vReprDPKGravado = vReprDPKGravar
        
    If Trim(txtCodCliente.Text) = "" Then 'se for inclusao
        
        txtCodCliente.Text = db.Parameters("CODCLI")
        'frmCadCliente.Caption = frmCadCliente.Caption & txtCodCliente.Text & "-" & cboCliente.Text
        
    '/* comentado enquanto n�o arrumo
        'Seleciona lojas da Deposito_Visao
        Set OraParameters = db.Parameters
        OraParameters.Remove "NOMEPRG": OraParameters.Add "NOMEPRG", "CAD060", 1
        OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
        OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
        OraParameters("vErro").ServerType = ORATYPE_NUMBER
        'vVB_Generica_001.Retorna_Cursor db, "pm_CURSOR1", "PRODUCAO.PCK_CAD060.Pr_Deposito_Visao", "NOMEPRG", "vErro", "vMsgErro"
        vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD060.Pr_Deposito_Visao(:PM_CURSOR1,:NOMEPRG, :vErro, :vMsgErro)")
        If vErro <> "" Then
            Exit Sub
        End If
        Set db_CONS = OraParameters("PM_CURSOR1").Value
    
        If db_CONS.EOF Then
            MsgBox "N�o existem lojas cadastradas na tabela DEPOSITO_VISAO. " & Chr(13) & _
                   "O cliente somente ser� cadastrado ON-LINE em Campinas. " & Chr(13) & _
                   "Por favor, avise o suporte!", vbInformation, "CAD060 - Cadastro de Cliente"
        Else
    
            While Not db_CONS.EOF
            
                'Luciano Carlos - 23/04/2008
                NotificaCD Format(db_CONS!Cod_Loja.Value, "00"), 0, 1
               
    
                'Verifica timeout na Loja_Conexao
                Set OraParameters = db.Parameters
                OraParameters.Remove "LOJA": OraParameters.Add "LOJA", Val(db_CONS!Cod_Loja.Value), 1
                OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
                OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
                Criar_Cursor OraParameters, "PM_CURSOR1"
                vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD060.Pr_Loja_Conexao(:PM_CURSOR1, :LOJA, :vErro, :vMsgErro)")
                If vErro <> "" Then
                    Exit Sub
                End If
                Set objConexao = OraParameters("PM_CURSOR1").Value
    
                SUCCESS = Ping(objConexao!ip.Value, objConexao!Cod_Loja.Value, ECHO)
    
                'Sem Comunica��o
                If InStr(GetStatusCode(SUCCESS), ("ip success")) = 0 Or ECHO.RoundTripTime > objConexao!time_out.Value Then
                    
                    NotificaCD Format(db_CONS!Cod_Loja.Value, "00"), 1, 0
                    
                    db_CONS.MoveNext
                    
                'Comunica��o OK
                Else
                
                    '-------------------------------------------------------
                    'Inclui na tabela CLIENTE de todos os CDS (On_Line) - Um a Um
                    '--- Via procedure devido a troca de vers�o do Oracle - Campinas
                    '--- em 09/Junho/2001
                    '-------------------------------------------------------
                    OraParameters.Remove "LOJA": OraParameters.Add "LOJA", Format(db_CONS!Cod_Loja.Value, "00"), 1
                    
                    'Defect #250 Sincronizar cod_sap de clientes criados no CAD060
                    'Fernando Piardi - 30/11/2012, adicionado parametro COD_SAP
                    OraParameters.Remove "COD_SAP": OraParameters.Add "COD_SAP", txtSAP.Text, 1
                    
                    db.Executesql "BEGIN PRODUCAO.PCK_CLIENTE.PR_INC_CLIENTE" & _
                                  "(:LOJA,:CODCLI,:CGC,:TPDOC,:NOME,:CONTATO,:CLASS," & _
                                  ":STATUS,:ENDERECO,:CODCID,:BAI,:DDD1,:FONE1,:DDD2,:FONE2,:CEP," & _
                                  ":CXPOS,:TELEX,:FAX,:IEST,:ISUF,:TPCLI," & _
                                  ":DTCAD," & _
                                  ":CODTRAN,:CODREPRD,:CODREPRB," & _
                                  ":CODBCO,:TPOP,:SIT,:CODMSG,:RESP,:PROTES," & _
                                  ":DTFUND," & _
                                  ":PREDIO,:CHEQUE,:TPCLIB," & _
                                  ":PROPR,:CONSFI,:DTMSG," & _
                                  ":CARAC,:CODTARE," & _
                                  ":DTTARE," & _
                                  ":CODMSGFI," & _
                                  ":DTRECAD," & _
                                  ":TPEMPRESA," & _
                                  ":FL_DIF," & _
                                  ":COD_SAP," & _
                                  ":vErro,:vMsgErro); END;"
    
                    If Val(IIf(IsNull(db.Parameters("vErro")), 0, db.Parameters("vErro"))) <> 0 Then
                        MsgBox "Erro " & db.Parameters("vErro") & "-" & db.Parameters("vMsgErro") & ". Avise o Depto.Suporte!", vbCritical, "CAD060 - Aten��o"
                        Exit Sub
                    End If
    
   
                    '-------------------------------------------------------
                    'Insere r_clie_repres para cliente novo nos CD�s remotos
                    '-------------------------------------------------------
                    Set OraParameters = db.Parameters
                    OraParameters.Remove "LOJA": OraParameters.Add "LOJA", Val(db_CONS!Cod_Loja.Value), 1
                    OraParameters.Remove "CODREPR": OraParameters.Add "CODREPR", Val(vReprCliente), 1
                    db.Executesql "Begin PRODUCAO.PCK_CLIENTE.PR_INC_CLREPRES" & _
                                  "(:LOJA,:CODCLI,:CODREPR,:vErro,:vMsgErro); END;"
                    If Val(IIf(IsNull(db.Parameters("vErro")), 0, db.Parameters("vErro"))) <> 0 Then
                        MsgBox "Erro: " & Val(OraParameters("vErro").Value) & " - " & _
                                          OraParameters("vMsgErro").Value & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
                        Exit Sub
                    End If
    
                    '------------------------------------------------------------------------
                    '     Insere clie_cr�dito on-line para cliente novo nos CD�s remotos
                    '------------------------------------------------------------------------
                    'Foi necess�rio colocar este if pois existe dblink para o CD16 no CDDPK e se deixar fazer, d� unique constraint - Alethea - 24/09/07
                    If Val(db_CONS!Cod_Loja.Value) <> 16 Then
                        Set OraParameters = db.Parameters
                        OraParameters.Remove "LOJA": OraParameters.Add "LOJA", Val(db_CONS!Cod_Loja.Value), 1
                        db.Executesql "Begin PRODUCAO.PCK_CLIENTE.PR_INC_CLCREDITO" & _
                                      "(:LOJA,:CODCLI," & _
                                      ":Saldo_Pedidos, :Saldo_Bordero,:Maior_Saldev,to_date(:Dt_Maior_Saldev,'DD/MM/RR')," & _
                                      ":Maior_Fatura, :Nota_Credito, :Atraso_Medio, :Intervalo_Atr_Medio, " & _
                                      ":Media_Compra, :Nota_Frequencia,:Limite, to_date(:Dt_Limite,'DD/MM/RR'),:Vl_Prev_Cpra," & _
                                      ":vErro,:vMsgErro); END;"
                        If Val(IIf(IsNull(db.Parameters("vErro")), 0, db.Parameters("vErro"))) <> 0 Then
                            MsgBox "Erro: " & Val(OraParameters("vErro").Value) & " - " & _
                                              OraParameters("vMsgErro").Value & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
                            Exit Sub
                        End If
                    End If
                    
                    
                    NotificaCD Format(db_CONS!Cod_Loja.Value, "00"), 1, 1
                    
                    'FIM DO PROCESSO PRINCIPAL DE INCLUS�O EM CADA CD ACESSADO
                    'Retirada a mensagem conf. solic. M�nica - Alethea - 23/08/07
                    'MsgBox "Registro Incluido em " & db_CONS!nome_fantasia.Value & "!", vbInformation, "CAD060 - Cadastro de Cliente"
                    
                    
                    db_CONS.MoveNext
    
                End If
            Wend
    
    
    
        End If
    '*/
    
    Else
        
        '----------------------------------------------------------------------------
        'Efetua Altera��o de Mensagem on-line nos CDs - Solicit. em Jan/07 - Alethea
        '----------------------------------------------------------------------------
        Set OraParameters = db.Parameters
        OraParameters.Remove "CODCLI":      OraParameters.Add "CODCLI", vCodCliente, 1
        'Este campo (Msg. fISCAL Cliente) deve atualizar on-line nos CDs, conforme solicita��o de jan/07 - Alethea
        'Conforme solicita��o do Fiscal em 30/01/07, todo cliente da UF = SC, n�o poder� ter antecipa��o tribut�ria - Aleth�a
        If cboUF = "SC" Then
            OraParameters.Remove "CODMSGFI":    OraParameters.Add "CODMSGFI", 53, 1
        Else
            OraParameters.Remove "CODMSGFI":    OraParameters.Add "CODMSGFI", IIf(Trim(cboMsgFiscal) = "" Or IsNull(cboMsgFiscal), 0, IIf(IsNumeric(Right(cboMsgFiscal, 2)), Val(Right(cboMsgFiscal, 2)), 0)), 1
        End If
        OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
        OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
            
        db.Executesql "BEGIN PRODUCAO.PCK_Cliente.PR_GRAVA_MSG_FISCAL(:CODCLI,:CODMSGFI,:vErro,:vMsgErro); END;"
        
        If Val(IIf(IsNull(OraParameters("vErro")), 0, OraParameters("vErro"))) <> 0 Then
            MsgBox "Erro: " & Val(OraParameters("vErro").Value) & " - " & _
                              OraParameters("vMsgErro").Value & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
            MsgBox "Problemas para gravar on-line, a mensagem fiscal do cliente!     ", vbInformation, "CAD060 - Aten��o"
        End If
 
        
        If Trim(txtCodCliente.Text) = "" Then
            txtCodCliente.Text = db.Parameters("CODCLI")
        End If
        
       
    End If

'*****
    'Grava Dados do Cliente Exporta��o
    'Verifica se h� registro exporta��o para este cliente
    Set OraParameters = db.Parameters
    OraParameters.Remove "CODCLI": OraParameters.Add "CODCLI", Val(txtCodCliente.Text), 1
    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    'vVB_Generica_001.Retorna_Cursor db, "pm_CURSOR1", "PRODUCAO.PCK_CAD060.Pr_Select_ClieExport", "CODCLI", "vErro", "vMsgErro"
    Criar_Cursor OraParameters, "PM_CURSOR1"
    vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD060.Pr_Select_ClieExport(:PM_CURSOR1, :CODCLI, :vErro, :vMsgErro)")
    If vErro <> "" Then
        Exit Sub
    End If
    Set db_CONS = OraParameters("PM_CURSOR1").Value
    
    'Seta flag para true, se o cliente j� estiver marcado como cliente exporta��o
    If db_CONS!Qtd <> 0 Then
        flCliExp = True
    Else
        flCliExp = False
    End If
    
    If chkExportacao.Value = 1 Then     'Cliente exporta��o
        
        'Se o cliente ainda n�o estiver marcado como exporta��o, inclui o registro
        If flCliExp = False Then
        
            Set OraParameters = db.Parameters
            OraParameters.Remove "CODCLI": OraParameters.Add "CODCLI", Val(txtCodCliente.Text), 1
            OraParameters.Remove "TPTRANS": OraParameters.Add "TPTRANS", "I", 1
            OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
            OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
            
            db.Executesql "Begin PRODUCAO.PCK_CLIENTE.PR_CLI_EXPORTACAO" & _
                          "(:CODCLI,:TPTRANS,:vErro,:vMsgErro); END;"
            If Val(IIf(IsNull(db.Parameters("vErro")), 0, db.Parameters("vErro"))) <> 0 Then
                MsgBox "Erro: " & Val(OraParameters("vErro").Value) & " - " & _
                                  OraParameters("vMsgErro").Value & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
                Exit Sub
            End If
        
        End If
    
    'Deleta registro de cliente exporta��o, se o mesmo estiver gravado
    ElseIf chkExportacao.Value = 0 Then     'Cliente n�o � exporta��o
    
        'Se o cliente estiver marcado como exporta��o, deleta registro
        If flCliExp = True Then
            
            Set OraParameters = db.Parameters
            OraParameters.Remove "CODCLI": OraParameters.Add "CODCLI", Val(txtCodCliente.Text), 1
            OraParameters.Remove "TPTRANS": OraParameters.Add "TPTRANS", "D", 1
            OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
            OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
            
            db.Executesql "Begin PRODUCAO.PCK_CLIENTE.PR_CLI_EXPORTACAO" & _
                          "(:CODCLI,:TPTRANS,:vErro,:vMsgErro); END;"
            If Val(IIf(IsNull(db.Parameters("vErro")), 0, db.Parameters("vErro"))) <> 0 Then
                MsgBox "Erro: " & Val(OraParameters("vErro").Value) & " - " & _
                                  OraParameters("vMsgErro").Value & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
                Exit Sub
            End If
        End If
    
    End If
    
    '// --------------------------------------------------------------------------
    'Grava Linha do Produto para Cliente Novo
    'Abre a Tela para Inclus�o das Linhas de Produtos que o Cliente poder� comprar
    'Esta tela est� no final para Cliente Novo, porque no in�cio ainda n�o tenho o
    'c�digo do cliente para poder gravar na tabela.
    'If Fl_Cliente_Novo = "S" And Len(Trim(cboCategBosch.Text)) <> 0 Then
    '    MsgBox "Por favor, inclua as linhas VDR para este cliente!", vbInformation, "CAD060 - Aten��o"
        'Call cmdGrLinVdr_Click
        'Frm_Linha_VDR.Show 1
    
    'Grava Linhas VDR marcadas para o cliente (vai deletar tudo e gravar de novo)
    If Len(Trim(cboCategBosch.Text)) <> 0 Then
        Call cmdGrLinVdr_Click
    End If
    
    'Se for altera��o de cliente, e campo de categ.Bosch estiver em branco, vai deletar registros da VDR.R_CLIE_LINHA_PRODUTO
    If Fl_Cliente_Novo = "N" And (Len(Trim(cboCategBosch.Text)) = 0 Or Trim(cboCategBosch) = "") Then
    
        Set OraParameters = db.Parameters
        OraParameters.Remove "CODCLI": OraParameters.Add "CODCLI", Val(txtCodCliente.Text), 1
        OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
        OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
        
        db.Executesql "Begin PRODUCAO.PCK_CLIENTE.PR_DEL_LINHA_PRODUTO" & _
                      "(:CODCLI,:vErro,:vMsgErro); END;"
        If Val(IIf(IsNull(db.Parameters("vErro")), 0, db.Parameters("vErro"))) <> 0 Then
            MsgBox "Erro: " & Val(OraParameters("vErro").Value) & " - " & _
                              OraParameters("vMsgErro").Value & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
            Exit Sub
        End If
    
    
    End If
    
    frmNotificaCD.Show 1
    'MsgBox "Registro Gravado!", vbInformation, "CAD060 - Cadastro de Cliente"
End If

End Sub

Sub Status_Controles_Ref_Bancarias(status As Boolean)
    txtSeq.Enabled = status
    txtBanco.Enabled = status
    txtDt.Enabled = status
    txtAgencia.Enabled = status
    txtDDD.Enabled = status
    txtFone.Enabled = status
    txtConceito.Enabled = status
    txtObs.Enabled = status
End Sub

Sub Status_Controles_Ref_Comerciais(status As Boolean)
    txtSeq01.Enabled = status
    txtEmp01.Enabled = status
    txtDtCons01.Enabled = status
    txtDDD01.Enabled = status
    txtFone01.Enabled = status
    txtVlUltFat01.Enabled = status
    txtDtUltFat01.Enabled = status
    txtMaiSlDev01.Enabled = status
    txtDtCad01.Enabled = status
    txtAtraso01.Enabled = status
    txtConceito01.Enabled = status
    txtObs01.Enabled = status
End Sub


Sub Status_Controles_Out_Enderecos(status As Boolean)
    txtSeqEnd01.Enabled = status
    'cboTpEnd01.Enabled = status
    cboTpDoctoEnd01.Enabled = status
    
    If vAcao = "Editar" And Val(cboTpEnd01) = 1 Then
        cboTpEnd01.Enabled = False
        txtCGCEnd01.Enabled = False
    Else
        cboTpEnd01.Enabled = True
        txtCGCEnd01.Enabled = status
    End If
    
    txtIEEnd01.Enabled = status
    txtNomeEnd01.Enabled = status
    txtEnd01.Enabled = status
    txtNumEnd01.Enabled = status
    txtComplEnd01.Enabled = status
    txtBairroEnd01.Enabled = status
    txtCepEnd01.Enabled = status
    cboUFEnd01.Enabled = status
    cboCidadeEnd01.Enabled = status
    txtDDDEnd01.Enabled = status
    txtFoneEnd01.Enabled = status
    txtFaxEnd01.Enabled = status
    txtCxPosEnd01.Enabled = status
End Sub


Sub NotificaCD(pCD As String, pOpcao As Byte, pCheck As Byte)
    'Parametro pCD   : Traz o numero do CD
    'Parametro pOpcao: 0 Para inicio / 1 Para Final
    'Parametro pCheck: 0 para Unchecked e cor vermelho / 1 para Checked


    
    Dim vControle As Control
   
    For Each vControle In frmNotificaCD.Controls
        If TypeOf vControle Is Label Then
            If pOpcao = 0 Then
                'Debug.Print UCase(vControle.Name)
                If UCase(vControle.Name) = UCase("LBLINICIOCD" & pCD) Then
                    vControle = Time
                    If UCase("LBLINICIOCD" & pCD) = UCase("LBLINICIOCD11") Then
                        frmNotificaCD.lblInicioCD12 = Time
                        frmNotificaCD.lblInicioCD13 = Time
                        frmNotificaCD.lblInicioCD15 = Time
                        frmNotificaCD.lblInicioCD17 = Time
                        frmNotificaCD.lblInicioCD18 = Time
                        frmNotificaCD.lblInicioCD19 = Time
                        frmNotificaCD.lblInicioCD20 = Time
                        frmNotificaCD.lblInicioCD21 = Time
                    End If
                End If
            ElseIf pOpcao = 1 Then
                If UCase(vControle.Name) = UCase("LBLFINALCD" & pCD) Then
                    vControle = Time
                    If UCase("LBLFINALCD" & pCD) = UCase("LBLFINALCD11") Then
                        frmNotificaCD.lblFinalCD12 = Time
                        frmNotificaCD.lblFinalCD13 = Time
                        frmNotificaCD.lblFinalCD15 = Time
                        frmNotificaCD.lblFinalCD17 = Time
                        frmNotificaCD.lblFinalCD18 = Time
                        frmNotificaCD.lblFinalCD19 = Time
                        frmNotificaCD.lblFinalCD20 = Time
                        frmNotificaCD.lblFinalCD21 = Time
                    End If
                End If
            End If
        End If
    Next
    
    'Abilita o CheckBox
    For Each vControle In frmNotificaCD.Controls
        If TypeOf vControle Is CheckBox Then
            'Debug.Print UCase(vControle.Name)
            If pCheck = 1 Then
                If UCase(vControle.Name) = UCase("chk" & pCD) Then
                    vControle.Value = pCheck
                    If UCase("chk" & pCD) = UCase("chk11") Then
                        frmNotificaCD.chk12.Value = pCheck
                        frmNotificaCD.chk13.Value = pCheck
                        frmNotificaCD.chk15.Value = pCheck
                        frmNotificaCD.chk17.Value = pCheck
                        frmNotificaCD.chk18.Value = pCheck
                        frmNotificaCD.chk19.Value = pCheck
                        frmNotificaCD.chk20.Value = pCheck
                        frmNotificaCD.chk21.Value = pCheck
                    End If
                End If
            ElseIf pCheck = 0 Then
                If UCase(vControle.Name) = UCase("chk" & pCD) Then
                    vControle.Value = pCheck
                    vControle.ForeColor = &HFF&
                    If UCase("chk" & pCD) = UCase("chk11") Then
                        frmNotificaCD.chk12.Value = pCheck
                        frmNotificaCD.chk12.ForeColor = &HFF&
                        frmNotificaCD.chk13.Value = pCheck
                        frmNotificaCD.chk13.ForeColor = &HFF&
                        frmNotificaCD.chk15.Value = pCheck
                        frmNotificaCD.chk15.ForeColor = &HFF&
                        frmNotificaCD.chk17.Value = pCheck
                        frmNotificaCD.chk17.ForeColor = &HFF&
                        frmNotificaCD.chk18.Value = pCheck
                        frmNotificaCD.chk18.ForeColor = &HFF&
                        frmNotificaCD.chk19.Value = pCheck
                        frmNotificaCD.chk19.ForeColor = &HFF&
                        frmNotificaCD.chk20.Value = pCheck
                        frmNotificaCD.chk20.ForeColor = &HFF&
                        frmNotificaCD.chk21.Value = pCheck
                        frmNotificaCD.chk21.ForeColor = &HFF&
                    End If
                End If
            End If
        End If
    Next
    'frmNotificaCD.Show 1
    
End Sub


Function VerificaUFTare(ByVal strUF As String) As Boolean
    VerificaUFTare = False
    
    Dim strUFTare As String
    strUFTare = Pegar_VL_Parametro("UFTARE")
    
    Dim arrUF As Variant
    arrUF = Split(strUFTare, ",")

    Dim intX As Integer

    For intX = 0 To UBound(arrUF)
        If arrUF(intX) = strUF Then
            VerificaUFTare = True
            Exit For
        End If
    Next

    Exit Function
End Function

Function Valida_Campos_Cadastro() As Boolean

Dim vAux As Byte
Dim UF_Valida As Boolean

Valida_Campos_Cadastro = True

If Trim(cboCliente.Text) = "" Then
    MsgBox "Digite o nome do cliente!", vbCritical, "CAD060 - Aten��o"
    Valida_Campos_Cadastro = False
    If cboCliente.Enabled = True Then cboCliente.SetFocus
    Exit Function
End If

If Len(Trim(cboCliente.Text)) < 5 Then
    MsgBox "O nome do cliente deve conter no m�nimo 5 caracteres!", vbCritical, "CAD060 - Aten��o"
    Valida_Campos_Cadastro = False
    Exit Function
End If

If Len(Trim(cboCliente.Text)) < 5 Then
    MsgBox "O nome do cliente deve conter no m�nimo 5 caracteres!", vbCritical, "CAD060 - Aten��o"
    Valida_Campos_Cadastro = False
    Exit Function
End If

If Not Valida_Caracteres_invalidos(Trim(cboCliente.Text)) Then
    MsgBox "O nome do cliente cont�m caracteres inv�lidos.", vbCritical, "CAD060 - Aten��o"
    Valida_Campos_Cadastro = False
    Exit Function
End If

If Val(Mid(txtSituacao.Text, 1)) = 9 Then
    MsgBox "Cliente desativado!", vbCritical, "CAD060 - Aten��o"
    Valida_Campos_Cadastro = False
    Exit Function
End If
If cboTpDocto.Text = "" Then
    MsgBox "Escolha um tipo de documento!", vbCritical, "CAD060 - Aten��o"
    Valida_Campos_Cadastro = False
    Exit Function
End If
If Trim(txtDocto.Text) = "" Then
    MsgBox "Digite o documento!", vbCritical, "CAD060 - Aten��o"
    Valida_Campos_Cadastro = False
    If txtDocto.Enabled = True Then txtDocto.SetFocus
    Exit Function
End If
If Trim(txtInscrEstadual.Text) = "" Then
    MsgBox "Digite a Inscri��o Estadual!", vbCritical, "CAD060 - Aten��o"
    Valida_Campos_Cadastro = False
    If txtInscrEstadual.Enabled = True Then txtInscrEstadual.SetFocus
    Exit Function
End If
If Trim(txtInscrEstadual.Text) <> "" Then
    Call Valida_IE(Me.txtInscrEstadual, cboUF)
    If Fl_Invalida = "S" Then
        Valida_Campos_Cadastro = False
        Exit Function
    End If
End If
If Trim(cboStatus.Text) = "" Then
    MsgBox "Escolha o status do cliente!", vbCritical, "CAD060 - Aten��o"
    Valida_Campos_Cadastro = False
    Exit Function
End If
If Trim(cboCaracteristica.Text) = "" Then
    MsgBox "Escolha a caracter�stica do cliente!", vbCritical, "CAD060 - Aten��o"
    Valida_Campos_Cadastro = False
    Exit Function
End If
If cboTpEmpresa.Text = "" And txtInscrEstadual <> "ISENTO" And chkConsumidorFinal.Value = 0 Then
    MsgBox "Para cliente REVENDEDOR, o tipo da empresa do cliente � obrigat�rio!", vbCritical, "CAD060 - Aten��o"
    Valida_Campos_Cadastro = False
    Exit Function
End If
If Trim(txtEndereco.Text) = "" Then
    MsgBox "Digite o endere�o!", vbCritical, "CAD060 - Aten��o"
    Valida_Campos_Cadastro = False
    Exit Function
End If
'Eduardo Faria de Oliveira - CIT - 22/08/2012
If ValidaCamposEndereco(txtEndereco.Text, ",") Then
    MsgBox "N�o � permitida a digita��o de v�rgula (,) nos campos de endere�o.", vbCritical, "CAD060 - Aten��o"
    Valida_Campos_Cadastro = False
    Exit Function
End If
'Eduardo Faria de Oliveira - CIT - 22/08/2012
If ValidaCamposEndereco(txtNumero.Text, ",") Then
    MsgBox "N�o � permitida a digita��o de v�rgula (,) nos campos de endere�o.", vbCritical, "CAD060 - Aten��o"
    Valida_Campos_Cadastro = False
    Exit Function
End If
'Eduardo Faria de Oliveira - CIT - 22/08/2012
If ValidaCamposEndereco(txtNumero.Text, ".") Then
    MsgBox "N�o � permitida a digita��o de v�rgula (,) nos campos de endere�o.", vbCritical, "CAD060 - Aten��o"
    Valida_Campos_Cadastro = False
    Exit Function
End If
'Eduardo Faria de Oliveira - CIT - 22/08/2012
If ValidaCamposEndereco(txtComplemento.Text, ",") Then
    MsgBox "N�o � permitida a digita��o de v�rgula (,) nos campos de endere�o.", vbCritical, "CAD060 - Aten��o"
    Valida_Campos_Cadastro = False
    Exit Function
End If
If Not Valida_Caracteres_invalidos(Trim(txtEndereco.Text)) Then
    MsgBox "O endere�o cont�m caracteres inv�lidos.", vbCritical, "CAD060 - Aten��o"
    Valida_Campos_Cadastro = False
    If txtEndereco.Enabled = True Then txtEndereco.SetFocus
    Exit Function
End If
'Eduardo Faria de Oliveira - CIT - 22/08/2012
If Not IsNumeric(txtNumero.Text) Then
    MsgBox "N�mero inv�lido!", vbCritical, "CAD060 - Aten��o"
    Valida_Campos_Cadastro = False
    Exit Function
End If
If Trim(txtBairro.Text) = "" Then
    MsgBox "Digite o bairro!", vbCritical, "CAD060 - Aten��o"
    Valida_Campos_Cadastro = False
    If txtBairro.Enabled = True Then txtBairro.SetFocus
    Exit Function
End If
If Not Valida_Caracteres_invalidos(Trim(txtBairro.Text)) Then
    MsgBox "O bairro cont�m caracteres inv�lidos.", vbCritical, "CAD060 - Aten��o"
    Valida_Campos_Cadastro = False
    Exit Function
End If
UF_Valida = False
For I = 0 To cboUF.ListCount - 1
    If UCase(cboUF.Text) = UCase(cboUF.List(I)) Then
        UF_Valida = True
        Exit For
    End If
Next I
If Not UF_Valida Then
    MsgBox "UF inv�lida!", vbCritical, "CAD060 - Aten��o"
    Valida_Campos_Cadastro = False
    Exit Function
End If
If Trim(cboCidade.Text) = "" Then
    MsgBox "Digite a cidade!", vbCritical, "CAD060 - Aten��o"
    Valida_Campos_Cadastro = False
    Exit Function
End If
If Len(Trim(cboCidade.Text)) < 3 Then
    MsgBox "O campo cidade deve conter no m�nimo 3 caracteres!", vbCritical, "CAD060 - Aten��o"
    Valida_Campos_Cadastro = False
    Exit Function
End If
If Not Valida_Caracteres_invalidos(Trim(cboCidade.Text)) Then
    MsgBox "A cidade cont�m caracteres inv�lidos.", vbCritical, "CAD060 - Aten��o"
    Valida_Campos_Cadastro = False
    Exit Function
End If

'13/04/2009
'Eduardo Relvas
'Verificar se a cidade digitada esta na lista
If fTaNaLista(cboCidade) = False Then
    MsgBox "A cidade deve pertencer a lista de cidades.", vbCritical, "CAD060 - Aten��o"
    Valida_Campos_Cadastro = False
    cboCidade.SetFocus
    Exit Function
End If

If InStr(txtEmail.Text, "@") = 0 And Len(Trim(txtEmail.Text)) > 0 Then
    MsgBox "Endere�o de E-mail Inv�lido!", vbCritical, "CAD060 - Aten��o"
    Valida_Campos_Cadastro = False
    Exit Function
End If
If Trim(txtVlMaiorSlDev.Text) = "" Then
    txtVlMaiorSlDev.Text = 0
End If
If Trim(vVlMaiorSlDevGravado) = "" Then
     vVlMaiorSlDevGravado = 0
End If
If CDbl(vVlMaiorSlDevGravado) > CDbl(txtVlMaiorSlDev.Text) Then
    If MsgBox("Maior saldo devedor (" & txtVlMaiorSlDev.Text & ") menor que o gravado(" & vMaior_SalDevGravado & ")" & Chr(10) & " Confirma Altera��o?", vbQuestion + vbYesNo, "Aten��o") = vbNo Then
        Valida_Campos_Cadastro = False
        Exit Function
    End If
End If
'Novo campo inserido em Julho/05 - Alethea
'Conf. solic. da M�nica, para o campo data n�o precisa existir checagem com o dia do faturamento pois o limite pode
'estar vencido e ent�o vai ficar valendo o limite autom�tico - Alethea - 27/07/05
If Trim(txtLimite.Text) = "" Then
    txtLimite.Text = 0
Else

    'If CDbl(Trim(txtLimite.Text)) >= 100000 Then
    '    MsgBox "O valor m�ximo para o limite � de 99.999"
    '    txtLimite.SetFocus
    '    Valida_Campos_Cadastro = False
    'End If
    
    '17/02/2011
    'SDS2182 - alterar valor de limite para 7 d�gitos----------------------------
    If CDbl(Trim(txtLimite.Text)) > 1000000 Then
        MsgBox "O valor m�ximo para o limite � de 1000000.00"
        txtLimite.SetFocus
        Valida_Campos_Cadastro = False
    End If
    '----------------------------------------------------------------------------
    
    
    If Trim(txtDtLimite) <> "" Then
        'Sql = "SELECT TO_CHAR(add_months(dt_faturamento,3),'dd/mm/yy') FROM datas "
        
        '17/02/2011
        'SDS2182 - alterar data limite de 3 meses para 6 meses-----------------------
        Sql = "SELECT TO_CHAR(add_months(dt_faturamento,6),'dd/mm/yy') FROM datas "
        '----------------------------------------------------------------------------
        
        Set db_CONS = db.dbcreatedynaset(Sql, 0&)
        vDtTresMeses = db_CONS.Fields(0).Value
    
        If CDate(txtDtLimite) > CDate(vDtTresMeses) Then
            'vVB_Generica_001.Informar ("Data limite n�o pode ser maior que tr�s meses!")
            
            '17/02/2011
            'SDS2182 - alterar data limite de 3 meses para 6 meses-----------------------
            vVB_Generica_001.Informar ("Data limite n�o pode ser maior que seis meses!")
            '----------------------------------------------------------------------------
            
            Valida_Campos_Cadastro = False
            Exit Function
        End If
    End If
'Else
'    If Val(txtDtLimite) <> 0 And txtDtLimite < vDtHoje Then
'        MsgBox "Data Final do Limite deve ser maior que a data de hoje!", vbCritical, "CAD060 - Cadastro de Clientes"
'        Valida_Campos_Cadastro = False
'        Exit Function
'    End If
End If


If Trim(cboBanco.Text) = "" Then
    MsgBox "Banco inv�lido!", vbCritical, "CAD060 - Aten��o"
    Valida_Campos_Cadastro = False
    Exit Function
End If

'Valida tipo de cliente e categoria Bosch
If Trim(cboTpCliente.Text) = "" Then
    If Left(cboStatus.Text, 1) = "0" Then
        MsgBox "� obrigat�rio informar o Tipo do Cliente!", vbCritical, "CAD060 - Aten��o"
        If cboTpCliente.Enabled = True Then
            Me.sstCadastro.Tab = 2
            cboTpCliente.SetFocus
        End If
        
        Valida_Campos_Cadastro = False
        Exit Function
    End If
Else
    If Len(Trim(cboCategBosch.Text)) <> 0 Or Len(Trim(cboFornVDR.Text)) <> 0 Then 'TI-3221
        
        'TI-3221
        If Len(Trim(cboFornVDR.Text)) = 0 Then
            MsgBox "Fornecedor VDR inv�lido!", vbCritical, "CAD060 - Aten��o"
            fraCategBosch.Visible = False
            Valida_Campos_Cadastro = False
            cboCategBosch.ListIndex = -1
            Exit Function
        End If
        'FIM TI-3221
           
        Set OraParameters = db.Parameters
        '#Hemerson Milano - SMS 1389 - 02/12/2014
        'OraParameters.Remove "TPCLI": OraParameters.Add "TPCLI", Val(Mid(cboTpCliente, (InStr(1, cboTpCliente, " - ") + 3))), 1
        OraParameters.Remove "TPCLI": OraParameters.Add "TPCLI", Mid(cboTpCliente, (InStr(1, cboTpCliente, " - ") + 3)), 1
        OraParameters.Remove "TP_CONS": OraParameters.Add "TP_CONS", 0, 1
        OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
        OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
        Criar_Cursor OraParameters, "PM_CURSOR1"
        vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD060.Pr_Select_ContVDR(:PM_CURSOR1, :TPCLI, :TP_CONS, :vErro, :vMsgErro)")
        If vErro = "" Then
            Set rst = OraParameters("PM_CURSOR1").Value
        Else
            Valida_Campos_Cadastro = False
            Exit Function
        End If
        If Not rst.EOF Then
            If Trim(cboCategBosch.Text) = "" Then
                MsgBox "Categoria inv�lida para este Tipo de Cliente!", vbCritical, "CAD060 - Aten��o" 'TI-3221
                fraCategBosch.Visible = False
                Valida_Campos_Cadastro = False
                Exit Function
            Else
                'Verifica se existem linhas VDR marcadas para serem gravadas
                vAux = 0
                With grdflexLinVdr
                    .Col = 2
                    For I = 1 To grdflexLinVdr.Rows - 1
                        If .TextMatrix(.Row, .Col) = "X" And vAux = 0 Then
                            vAux = 1
                        End If
                    Next I
                End With
                If vAux = 0 Then
                    MsgBox "Por favor, assinale as linhas VDR para serem gravadas para este cliente!", vbInformation, "CAD060 - Cadastro de Cliente"
                    Valida_Campos_Cadastro = False
                    Exit Function
                End If
            End If
        Else
           cboCategBosch.ListIndex = -1
           MsgBox "Tipo de Cliente n�o pode ser VDR!", vbCritical, "CAD060 - Aten��o" 'TI-3221
           Valida_Campos_Cadastro = False
           Exit Function
        End If
    End If
End If

'Verifica e zera cod Suframa
If Trim(cboClassificacao.Text) = "" Then
    cboClassificacao.Text = "X"
End If
If Len(Trim(txtDtCadastro.Text)) <> 8 Then
    MsgBox "Data de Cadastro Inv�lida !!!", vbCritical, "CAD060 - Aten��o"
    Valida_Campos_Cadastro = False
    Exit Function
End If
If Len(Trim(txtDtFundacao.Text)) <> 0 And Len(Trim(txtDtFundacao.Text)) <> 8 Then
    MsgBox "Data de Funda��o Inv�lida !!!", vbCritical, "CAD060 - Aten��o"
    Valida_Campos_Cadastro = False
    Exit Function
End If
If Len(Trim(txtDtMsg.Text)) <> 0 And Len(Trim(txtDtMsg.Text)) <> 8 Then
    MsgBox "Data da Mensagem Inv�lida !!!", vbCritical, "CAD060 - Aten��o"
    Valida_Campos_Cadastro = False
    Exit Function
End If
If chkConsumidorFinal.Value Or Trim(txtInscrEstadual.Text) = "ISENTO" Or _
       InStr(1, "AM_RO_RR_AP_AC", cboUF.Text) = 0 Then
    txtInscrSuframa = ""
    'MsgBox "Este cliente n�o deve possuir Inscri��o Suframa !!!", vbCritical, "CAD060 - Aten��o"
    'Valida_Campos_Cadastro = False
    'Exit Function
End If

'Bloquear inscri��es suframa - solicita��o Fiscal de 16/12/04, para entrar em 02/Jan/05
If Trim(txtInscrSuframa.Text) <> "" And Trim(txtInscrSuframa.Text) <> 0 Then
    MsgBox "Inscri��es Suframa est�o bloqueadas desde Dez/2005!", vbInformation, "CAD060 - Aten��o"
    txtInscrSuframa = ""
End If

'Consist�ncia do CEP, conforme solicita��o da M�nica e Nat�lia em 25/05/05 - Alethea 06/06/05
If Trim(txtCep) = "" Or Val(txtCep) = 0 Then
    vVB_Generica_001.Informar "CEP Inv�lido"
    Valida_Campos_Cadastro = False
    Exit Function
ElseIf Len(txtCep) <> 8 Then
    vVB_Generica_001.Informar "CEP Inv�lido! CEP precisa conter 8 d�gitos."
    Valida_Campos_Cadastro = False
    Exit Function
Else
    Dim vAuxCep As String
    vAuxCep = Mid(txtCep, 1, 1)     'Pega o 1o. d�gito do CEP e valida de acordo com a UF
    If cboUF = "SP" And (vAuxCep <> 0 And vAuxCep <> 1) Then
        vVB_Generica_001.Informar "CEP Inv�lido! Os CEPs do estado de SP devem iniciar com 0 ou 1."
        Valida_Campos_Cadastro = False
        txtCep.SetFocus
        Exit Function
    ElseIf (cboUF = "RJ" Or cboUF = "ES") And (vAuxCep <> 2) Then
        vVB_Generica_001.Informar "CEP Inv�lido! Os CEPs dos estados do RJ e ES devem iniciar com 2."
        Valida_Campos_Cadastro = False
        Exit Function
    ElseIf cboUF = "MG" And (vAuxCep <> 3) Then
        vVB_Generica_001.Informar "CEP Inv�lido! Os CEPs do estado de MG devem iniciar com 3."
        Valida_Campos_Cadastro = False
        Exit Function
    ElseIf (cboUF = "BA" Or cboUF = "SE") And (vAuxCep <> 4) Then
        vVB_Generica_001.Informar "CEP Inv�lido! Os CEPs dos estados da BA e SE devem iniciar com 4."
        Valida_Campos_Cadastro = False
        Exit Function
    ElseIf (cboUF = "PE" Or cboUF = "AL" Or cboUF = "PB" Or cboUF = "RN") And (vAuxCep <> 5) Then
        vVB_Generica_001.Informar "CEP Inv�lido! Os CEPs dos estados de PE, AL, PB e RN devem iniciar com 5."
        Valida_Campos_Cadastro = False
        Exit Function
    ElseIf (cboUF = "CE" Or cboUF = "PI" Or cboUF = "MA" Or cboUF = "PA" Or cboUF = "AM" Or cboUF = "AC" Or _
            cboUF = "AP" Or cboUF = "RR") And (vAuxCep <> 6) Then
        vVB_Generica_001.Informar "CEP Inv�lido! Os CEPs dos estados do CE, PI, MA, PA, AM, AC, AP e RR devem iniciar com 6."
        Valida_Campos_Cadastro = False
        Exit Function
    ElseIf (cboUF = "DF" Or cboUF = "GO" Or cboUF = "TO" Or cboUF = "MS" Or cboUF = "RO") And (vAuxCep <> 7) Then
        vVB_Generica_001.Informar "CEP Inv�lido! Os CEPs dos estados do DF, GO, TO, MS e RO devem iniciar com 7."
        Valida_Campos_Cadastro = False
        Exit Function
    ElseIf (cboUF = "PR" Or cboUF = "SC") And (vAuxCep <> 8) Then
        vVB_Generica_001.Informar "CEP Inv�lido! Os CEPs dos estados do PR e SC devem iniciar com 8."
        Valida_Campos_Cadastro = False
        Exit Function
    ElseIf (cboUF = "RS") And (vAuxCep <> 9) Then
        vVB_Generica_001.Informar "CEP Inv�lido! Os CEPs dos estados do RS devem iniciar com 9."
        Valida_Campos_Cadastro = False
        Exit Function
    End If
End If

If Trim(cboTpDocto.Text) <> "" Then
    'Valida CGC ou CPF
    If Val(cboTpDocto.Text) = 1 Then
        If Not vVB_Generica_001.ValidaCGC(txtDocto.Text) Then
            MsgBox "CGC Inv�lido!!!", vbCritical, "CAD060 - Aten��o"
            Valida_Campos_Cadastro = False
            Exit Function
        End If
    Else
        If Not vVB_Generica_001.ValidaCIC(txtDocto.Text) Then
            MsgBox "CPF Inv�lido!!!", vbCritical, "CAD060 - Aten��o"
            Valida_Campos_Cadastro = False
            Exit Function
        End If
    End If
End If

If UCase(lblTransp.Caption) = "TRANSPORTADORA" Then
    MsgBox "Informe a Transportadora.", vbInformation, "Aten��o"
    Me.cboTransportadora.SetFocus
    Valida_Campos_Cadastro = False
    Exit Function
End If

'If Trim(cboTpClienteBlau.Text) <> "" And _
'        Not IsNumeric(Right(lblRepresBlau.Caption, 6)) Then
'    MsgBox "Representante BLAU Inv�lido!!!", vbCritical, "Aten��o"
'    Valida_Campos_Cadastro = False
'    Exit Function
'End If
'se nao selecionou o tipo BLAU e selecionou o repres
'If Trim(cboTpClienteBlau.Text) = "" And IsNumeric(Right(lblRepresBlau.Caption, 6)) Then
'    MsgBox "Tipo Representante BLAU n�o Selecionado!!!", vbCritical, "CAD060 - Aten��o"
'    Valida_Campos_Cadastro = False
'    Exit Function
'End If
If Trim(cboCheque.Text) = "" Then
    MsgBox "Campo Cheques s/Fundo Inv�lido!!!", vbCritical, "CAD060 - Aten��o"
    Valida_Campos_Cadastro = False
    Exit Function
End If
If Trim(txtFone1.Text) <> "" And Trim(txtDDD1.Text) = "" Then
    MsgBox "DDD Inv�lido!!!", vbCritical, "CAD060 - Aten��o"
    Valida_Campos_Cadastro = False
    Exit Function
End If
If Trim(txtFone2.Text) <> "" And Trim(txtDDD2.Text) = "" Then
    MsgBox "DDD Inv�lido!!!", vbCritical, "CAD060 - Aten��o"
    Valida_Campos_Cadastro = False
    Exit Function
End If
'Verifica se j� existe algum repres cadastrado para o cliente
If Fl_Ja_Relacionou = "N" And chkUtility.Value = True Then
    MsgBox "Para ser Cliente UTILITY deve ter Representante DPK Cadastrado!!!", vbCritical, "CAD060 - Aten��o"
    Valida_Campos_Cadastro = False
    Exit Function
End If
If Left(cboStatus.Text, 1) = "1" And chkUtility.Value = True Then
    MsgBox "Cliente UTILITY n�o pode ser 'OUTRAS SAIDAS'!", vbCritical, "CAD060 - Aten��o"
    Valida_Campos_Cadastro = False
    Exit Function
End If
If Trim(txtCodCliente.Text) = "" Then
     '// ---------------- VERIFICA A EXISTENCIA DO CGC ---------------------- //
    Sql = "SELECT cgc " & _
            "FROM cliente " & _
           "WHERE cgc = :cgc"
    db.Parameters.Remove "cgc": db.Parameters.Add "cgc", Val(txtDocto.Text), 1
    Set db_CONS = db.dbcreatedynaset(Sql, 0&)
    
    If Not db_CONS.EOF Then
        MsgBox IIf(Left(cboTpDocto, 1) = 1, "CGC ", "CIC ") & " j� cadastrado !!!", vbCritical, "CAD060 - Aten��o"
        txtDocto.SetFocus
        Valida_Campos_Cadastro = False
        Exit Function
    End If
End If
If Trim(txtCodCliente.Text) <> "" Then
     If Not fVerificaRepresentantes() Then
        Valida_Campos_Cadastro = False
        Exit Function
     End If
End If
'// ------------- Verifica se cliente pode ter inscricao TARE -------------- //
If Val(txtCodTare.Text) <> 0 Or Trim(txtDtTare.Text) <> "" Then
    If txtCodTare.Text = "" Or txtDtTare.Text = "" Then
       MsgBox "Campos de codigo e data TARE devem ser preenchidos", vbCritical, "CAD060 - Aten��o"
        Valida_Campos_Cadastro = False
        Exit Function
    End If
    
    
    'A partir de Set./2005, solicitado pelo Fiscal que fosse permitido Tare tb. para DF (DF e GO)
    'If txtInscrEstadual.Text = "ISENTO" Or (cboUF.Text <> "GO" And cboUF.Text <> "DF") Or _

    If txtInscrEstadual.Text = "ISENTO" Or (VerificaUFTare(Trim(cboUF.Text)) = False) Or _
       chkConsumidorFinal.Value = True Or Fl_Tare = "S" Then
        'MsgBox "Para cliente ser TARE, deve: " & Chr(10) & _
              Chr(10) & _
              "- Ser do Estado de GOI�S " & Chr(10) & _
              "- Ter Inscr.Estadual diferente de ISENTO " & Chr(10) & _
              "- N�O PODE SER Consumidor Final " & Chr(10) & _
              "- O seu Representante DPK deve pertencer � Filial 26 - GOIANIA", vbCritical, "CAD060 - Aten��o"
        MsgBox "Para cliente ser TARE, deve: " & Chr(10) & _
              Chr(10) & _
              "- Ser do Estado de " & Pegar_VL_Parametro("UFTARE") & " " & Chr(10) & _
              "- Ter Inscr.Estadual diferente de ISENTO " & Chr(10) & _
              "- N�O PODE SER Consumidor Final " & Chr(10) & _
              "- O seu Representante DPK deve pertencer �s Filiais 26,135,140 - GO ou 21,143,186 - DF", vbCritical, "CAD060 - Aten��o"
        Valida_Campos_Cadastro = False
        Exit Function
    End If
End If
 
'Se cliente tiver assinalado grupo master e n�o tiver dado um c�digo, avisar Depto.Cr�dito
If chkGrupoMaster.Value = Checked Then
    If vCod_GrupoMaster = 0 And Trim(cboCodCliGrMaster.Text) = "" Then
        MsgBox "Cliente pertence a grupo master j� cadastrado na DPK. " & Chr(13) & _
               "Favor indicar o cliente master!", vbCritical, "CAD060 - Aten��o!"
        Valida_Campos_Cadastro = False
    End If
End If
'SDS 2557 - Marco Aurelio Andrade Jr - Valida��o se esta preenchido apenas para PJ
If Trim(cboCNAE.Text) = "" And Left(cboTpDocto.Text, 1) = 1 Then
    MsgBox "Escolha o CNAE!", vbCritical, "CAD060 - Aten��o"
    Valida_Campos_Cadastro = False
    Exit Function
End If
'SDS 2557 - Marco Aurelio Andrade Jr - Validando pois PF n�o possui CNAE
If Trim(cboCNAE.Text) <> "" And Left(cboTpDocto.Text, 1) = 2 Then
    MsgBox "Pessoa F�sica n�o possui CNAE!", vbCritical, "CAD060 - Aten��o"
    cboCNAE.ListIndex = -1
    Valida_Campos_Cadastro = False
    Exit Function
End If

'Eduardo Faria de Oliveira - CIT - 22/08/12
If Len(ConcatenarEnderecoSAP(txtEndereco.Text, txtNumero.Text, txtComplemento.Text)) > 33 Then
    MsgBox "O endere�o completo n�o deve ultrapassar 33 caract�res.", vbCritical, "CAD060 - Aten��o"
    Valida_Campos_Cadastro = False
    Exit Function
End If

End Function

Function Valida_Caracteres_invalidos(strTexto As String) As Boolean
    Valida_Caracteres_invalidos = False
    If InStr(strTexto, "!") > 0 Or _
       InStr(strTexto, "@") > 0 Or _
       InStr(strTexto, "#") > 0 Or _
       InStr(strTexto, "$") > 0 Or _
       InStr(strTexto, "%") > 0 Or _
       InStr(strTexto, "*") > 0 Or _
       InStr(strTexto, "+") > 0 Or _
       InStr(strTexto, "=") > 0 Then
        Exit Function
    End If
    Valida_Caracteres_invalidos = True
End Function

Private Sub cboBanco_GotFocus()
    'Call Carrega_Combo_Banco(cboBanco)
    'cboBanco = "BRASIL - 001"
End Sub


Private Sub cboCaracteristica_GotFocus()
'    Call Carrega_Combo_Caracteristica(cboCaracteristica)
'    cboCaracteristica = "N - NORMAL"
End Sub


Private Sub cboCategBosch_Click()
    
    'Verifica se este tipo de cliente pode ser Bosch Diesel
    If Trim(cboCategBosch.Text) <> "" And Trim(txtDocto.Text) <> "" And Trim(cboTpCliente) <> "" Then 'TI-3221
    'And Fl_Cliente_Novo = "N" Then
       
        Set OraParameters = db.Parameters
        '#Hemerson Milano - SMS 1789 - 02/12/2014
        'OraParameters.Remove "TPCLI": OraParameters.Add "TPCLI", Val(Mid(cboTpCliente, (InStr(1, cboTpCliente, " - ") + 3))), 1
        OraParameters.Remove "TPCLI": OraParameters.Add "TPCLI", Mid(cboTpCliente, (InStr(1, cboTpCliente, " - ") + 3)), 1
        OraParameters.Remove "TP_CONS": OraParameters.Add "TP_CONS", 0, 1
        OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
        OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
        Criar_Cursor OraParameters, "PM_CURSOR1"
        vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD060.Pr_Select_ContVDR(:PM_CURSOR1, :TPCLI, :TP_CONS, :vErro, :vMsgErro)")
        If vErro = "" Then
            Set rst = OraParameters("PM_CURSOR1").Value
        Else
            Exit Sub
        End If
        If Not rst.EOF Then
           If Trim(cboCategBosch.Text) = "" Then
              MsgBox "Categoria  inv�lida para este Tipo de Cliente!", vbCritical, "CAD060 - Aten��o" 'TI-3221
              fraCategBosch.Visible = False
              Exit Sub
           End If
        Else
           cboCategBosch.ListIndex = -1
           MsgBox "Tipo de Cliente n�o pode ser VDR!", vbCritical, "CAD060 - Aten��o" 'TI-3221
            fraCategBosch.Visible = False
           Exit Sub
        End If
    
        'Se estiver tudo ok com a categoria escolhida e com o tipo de cliente, vai mostrar as linhas vdr
        fraCategBosch.Visible = True
        Call cmdLinVdr_Click
    
    End If

End Sub


Private Sub cboCategBosch_GotFocus()
    'Call Carrega_Combo_CategBosch(cboCategBosch)
End Sub

Private Sub cboCliente_Change()

    Me.Caption = "Cadastro: " & cboCliente

End Sub

Private Sub cboCliente_Click()
    
    Dim vNomeCli As String, vVezes As Byte
        
'    txtCodCliente = Right(cboCliente, 6)
'    cboCliente.Text = Left(cboCliente, Len(cboCliente) - 9)
'    vNomeCli = cboCliente
'    Call pCarregaDados
        
    If Not IsNumeric(Right(cboCliente, 6)) Then Exit Sub
    
    txtCodCliente.Text = Right(cboCliente, 6)
    vNomeCli = Left(cboCliente, Len(cboCliente) - 9)
        
    fraCategBosch.Visible = False
    
    Call pCarregaDados
    grdflexRepres.Visible = False
    
    cboCliente.Clear
    cboCliente.AddItem vNomeCli
    cboCliente.ListIndex = 0
    
End Sub


Private Sub cboCliente_KeyPress(KeyAscii As Integer)
    
    If Chr$(KeyAscii) = "'" Or Chr$(KeyAscii) = "�" Or Chr$(KeyAscii) = "`" Then
        KeyAscii = 0
        Beep
    Else
        KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
        KeyAscii = ValidaAlfaNumerico(KeyAscii)
    End If
    

End Sub


Private Sub cboCliente_LostFocus()
    
    cboCliente.Text = UCase(Left(cboCliente.Text, 30))
    
    If cboCliente <> "" And Trim(txtSAP) = "" Then
        If Trim(txtCodCliente) = "" Then
            Call pCarregaComboCliente(frmCadCliente.cboCliente, cboCliente.Text)
        End If
    End If

End Sub

Private Sub cboCodCliGrMaster_Click()
    
    Dim vNomeCli As String
    Dim vAuxNome As String, vAuxCod As String
        
    If cboCodCliGrMaster.Tag = "1" Then Exit Sub
    
    If Not IsNumeric(Right(cboCodCliGrMaster, 6)) Then Exit Sub
    
    vAuxCod = Right(cboCodCliGrMaster, 6)
    vAuxNome = Mid(Left(cboCodCliGrMaster, Len(cboCodCliGrMaster) - 9), 1, 10)
    vNomeCli = vAuxNome & " - " & vAuxCod
        
    cboCodCliGrMaster.Tag = "1"
    cboCodCliGrMaster.Clear
    cboCodCliGrMaster.AddItem vNomeCli
    cboCodCliGrMaster.ListIndex = 0

End Sub


Private Sub cboCodCliGrMaster_KeyPress(KeyAscii As Integer)
    If Chr$(KeyAscii) = "'" Or Chr$(KeyAscii) = "�" Or Chr$(KeyAscii) = "`" Then
        KeyAscii = 0
        Beep
    Else
        KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
    End If

End Sub


Private Sub cboCodCliGrMaster_LostFocus()

    cboCodCliGrMaster.Tag = ""

    cboCodCliGrMaster.Text = UCase(Left(cboCodCliGrMaster.Text, 10))
    
    If cboCodCliGrMaster <> "" Then
        Call pCarregaCliMaster(frmCadCliente.cboCodCliGrMaster, cboCodCliGrMaster.Text)
    End If

End Sub
Private Sub cboDiferenciado_Click()  ' Combo adicionando Valores S ou N conforme DUM 1889  - Jairo Almeida  - 22-02-12
strFLDiferenciado = cboDiferenciado.List(cboDiferenciado.ListIndex)
If strFLDiferenciado = "SIM" Then
strFLDiferenciado = "S"
End If
If strFLDiferenciado = "N�O" Then
strFLDiferenciado = "N"
End If
End Sub

Private Sub cboMsgCliente_GotFocus()
    'Call Carrega_Combo_Mensagem("C", cboMsgCliente)          'Par�metros: C=Cliente / F=Fiscal
End Sub

Private Sub cboMsgCliente_LostFocus()
    If cboMsgCliente.Text = " " Then
        txtDtMsg.Text = ""
        txtResponsavel.Text = ""
    End If
End Sub


Private Sub cboMsgFiscal_GotFocus()
    'Call Carrega_Combo_Mensagem("F", cboMsgFiscal)           'Par�metros: C=Cliente / F=Fiscal
End Sub


'Private Sub cboRepresBlau_Change()
    'If cboRepresBlau.Text = "" Then
    '    lblRepresBlau = "Representante Blau"
    '    vReprBLAUGravar = ""
    'End If
'End Sub

'Private Sub cboRepresBlau_LostFocus()
    'If cboRepresBlau.Text <> "" Then
    '    If Trim(cboTpClienteBlau.Text) = "" Then ' se nao selecionou o
    '        cboRepresBlau = ""  ' tipo entao zera o repre
    '    End If
    '    If vComboRepresBlau <> cboRepresBlau.Text Then
    '        Call pCarregaComboRepresentante(cboRepresBlau, cboRepresBlau.Text, "B")
    '        vComboRepresBlau = cboRepresBlau.Text
    '    End If
    'Else
    '    On Error Resume Next
    '    lblRepresBlau = "Representante Blau"
    '    lblRepresBlau.Refresh
    '    Call Carrega_Combo_Tipo_ClienteBlau(Me.cboTpClienteBlau)'
    'End If
'End Sub


Private Sub cboRepresDPK_Click()
    
    If Not IsNumeric(Mid(cboRepresDPK.Text, (InStr(1, cboRepresDPK.Text, " - ") + 3), Len(cboRepresDPK.Text))) Then
        Exit Sub
    End If
    
    lblRepresDPK.Caption = "Representante DPK: " & Mid(cboRepresDPK.Text, (InStr(1, cboRepresDPK.Text, " - ") + 3), Len(cboRepresDPK.Text))
    

End Sub


Private Sub cboRepresDPK_GotFocus()
    'Call Carrega_Combo_Repres("D", cboRepresDPK)             'Par�metros: D=DPK / B=BLAU
End Sub


Private Sub cboRepresDPK_LostFocus()

Dim vAux1 As String, vAux2 As String

'lblRepresDPK = "Representante DPK: "
If IsNumeric(cboRepresDPK) Then
    vAux1 = cboRepresDPK
Else
    vAux1 = Mid(cboRepresDPK, InStr(1, cboRepresDPK, " - ") + 3, Len(cboRepresDPK))
End If
vAux2 = Mid(lblRepresDPK, InStr(1, lblRepresDPK, ":") + 2, Len(lblRepresDPK))


   If cboRepresDPK.Text <> "" Then
       If vAux1 <> vAux2 Then
          Call pCarregaComboRepresentante(cboRepresDPK, cboRepresDPK.Text, "D")
          lblRepresDPK.Caption = "Representante DPK: " & Mid(cboRepresDPK, InStr(1, cboRepresDPK, " - ") + 3, Len(cboRepresDPK))
       Else
          Call pCarregaComboRepresentante(cboRepresDPK, vAux1, "D")
          lblRepresDPK.Caption = "Representante DPK: " & Mid(cboRepresDPK, InStr(1, cboRepresDPK, " - ") + 3, Len(cboRepresDPK))
       End If
    Else
       lblRepresDPK.Caption = "Representante DPK"
    End If

End Sub


Private Sub cboStatus_LostFocus()
    If Trim(cboStatus.Text) = "" Then
        cboStatus.Text = "0 - Normal"
    End If
End Sub


Private Sub cboTpCliente_Click()
    
    'Verifica se este tipo de cliente pode ser Bosch Diesel
    If Trim(cboCategBosch.Text) <> "" And Trim(txtDocto.Text) <> "" And Trim(cboTpCliente) <> "" Then 'TI-3221
    'And Fl_Cliente_Novo = "N" Then
       
        Set OraParameters = db.Parameters
        OraParameters.Remove "TPCLI": OraParameters.Add "TPCLI", Val(Mid(cboTpCliente, (InStr(1, cboTpCliente, " - ") + 3))), 1
        OraParameters.Remove "TP_CONS": OraParameters.Add "TP_CONS", 0, 1
        OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
        OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
        Criar_Cursor OraParameters, "PM_CURSOR1"
        vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD060.Pr_Select_ContVDR(:PM_CURSOR1, :TPCLI, :TP_CONS, :vErro, :vMsgErro)")
        If vErro = "" Then
            Set rst = OraParameters("PM_CURSOR1").Value
        Else
            Exit Sub
        End If
        If Not rst.EOF Then
           If Trim(cboCategBosch.Text) = "" Then
              MsgBox "Categoria inv�lida para este Tipo de Cliente!", vbCritical, "CAD060 - Aten��o" 'TI-3221
              fraCategBosch.Visible = False
              Exit Sub
           End If
        Else
           cboCategBosch.ListIndex = -1
           MsgBox "Tipo de Cliente n�o pode ser VDR!", vbCritical, "CAD060 - Aten��o" 'TI-3221
            fraCategBosch.Visible = False
           Exit Sub
        End If
    
        'Se estiver tudo ok com a categoria escolhida e com o tipo de cliente, vai mostrar as linhas vdr
        fraCategBosch.Visible = True
        Call cmdLinVdr_Click
    
    End If

End Sub

Private Sub cboTpCliente_GotFocus()
    'Call Carrega_Combo_Tipo_ClienteDPK(cboTpCliente)
End Sub


Private Sub cboTpCliente_LostFocus()
    
    If vCod_tipo_Cliente <> 0 And Trim(cboTpCliente.Text) = "" Then
        cboRepresDPK.Text = ""
        lblRepresDPK.Caption = "Representante DPK"
        vReprDPKGravar = "0"
    End If
    
    'Em 23/06/06 foi solicitado pela M�nica (Cr�dito) para que sempre que escolher tipo de
    'cliente 99 (Forn/Func), automaticamente colocar banco 99 - CARTEIRA) - Alethea
    
    'oliveira@ - cit - 22/11/12 - Altera��o para verificar segmento do cliente tipo AG para setar a carteira 999
    If Mid(Trim(cboTpSegmento.Text), InStr(Trim(cboTpSegmento.Text), " - ") + 3, 2) = "AG" Then
        cboBanco.Text = "CARTEIRA - 999"
    End If
    
End Sub


'Private Sub cboTpClienteBlau_Click()
'    If Trim(cboTpClienteBlau) = "" Then
'        cboRepresBlau.Text = ""
'        Call cboRepresBlau_LostFocus
'    End If
'End Sub


'Private Sub cboTpClienteBlau_LostFocus()
'----------- se ZEROU O TIPO ----------------------
'If vCod_tipo_clBLAU <> 0 And Trim(cboTpClienteBlau.Text) = "" Then
'    cboRepresBlau.Text = ""
'    lblRepresBlau.Caption = "Representante Blau"
'    vReprBLAUGravar = "0"
'End If
'End Sub


Private Sub cboTpDocto_Click()
    
    If Left(cboTpDocto, 1) = "1" Then 'Pessoa Jur�dica
        lblDocto = "CGC"
    Else 'Pessoa F�sica
        lblDocto = "CIC"
    End If

    'carlosj CiT (26/11/2012) - ISA 1033
    'chama o metodo para carregar o combo segmento
    Call CarregarComboTipoSegmento

End Sub

Private Sub cboTpDocto_LostFocus()
    If Val(Mid(cboTpDocto.Text, 1, 2)) = 1 Then
        lblDocto.Caption = "CGC"
    ElseIf Val(Mid(cboTpDocto.Text, 1, 2)) = 2 Then
        lblDocto.Caption = "CPF"
    ElseIf Val(Mid(cboTpDocto.Text, 1, 2)) = "" Then
        Exit Sub
    End If
    
    'carlosj CiT (26/11/2012) - ISA 1033
    'chama o metodo para carregar o combo segmento
    Call CarregarComboTipoSegmento
    
End Sub


Private Sub cboTpEmpresa_LostFocus()
    If Trim(cboTpEmpresa.Text) = "" Then
        'cbotpempresa.Text = "E - EMPRESA"
        cboTpEmpresa.ListIndex = -1
    End If

End Sub


Private Sub cboTpEnd01_Click()

Dim vMaxSeq As Long
Dim vTpEnd As Long

    If vFl_Adic_OutEnd = False Then
        Exit Sub
    End If
    
    'Eduardo - 30/08/05
    'Se o cliente tiver inscricao estadual igual a ISENTO ent�o dar mensagem.
    'Retirada esta consistencia em 03/10/05, conf. solicita��o do Fiscal - Alethea
    'If Val(txtInscrEstadual) = 0 And Val(cboTpEnd01) = 1 Then
    '    MsgBox "Voc� n�o pode adicionar um Endere�o de Entrega para um cliente sem Inscri��o Estadual.", vbInformation, "Aten��o"
    '    Exit Sub
    'End If
    
    '18/10/06 - Alethea - Altera��o com solicita��o de servi�os
    'Conforme solicita��o do Fiscal / O&M, foi colocada novamente a verifica��o para cliente com IE Isento
    'Somente cliente do tipo "63 - Seguradora" - que n�o tiver IE - poder� receber End.Entrega
    'Incluido cliente do tipo "78 - Locadora de Ve�culos" - que n�o tiver IE - poder� receber End.Entrega - Conf.solic.Nat�lia O&M/M�nica Fiscal - Alethea - 26/03/07
    'Incluido cliente do tipo "77 - Leve" - que n�o tiver IE - poder� receber End.Entrega - Conf.solic.Nat�lia O&M/M�nica Fiscal - Alethea - 03/08/07
    
    'oliveira@ - cit - 22/11/12 -  validar se o segmento � diferente de "BA" e "BC".
    Dim codTpSegmento As String
    If cboTpSegmento <> "" Then
        codTpSegmento = Split(cboTpSegmento, " - ")(1)
    End If
    
    '--------------------------------------------
    'TI-5408 - c.alexandre.ferreira 18/10/16
    '          Retirada da trava que n�o permite o cadastro de cliente isento ou pessoa f�sica para endere�o de cobran�a,
    '          conforme orienta��o do setor fiscal
    '----------
    'If (Val(txtInscrEstadual) = 0 Or Trim(txtInscrEstadual) = "ISENTO") And _
    '    Val(cboTpEnd01) = 1 And _
    '    codTpSegmento <> "BA" And _
    '    codTpSegmento <> "BC" Then
    '    MsgBox "Voc� n�o pode adicionar um Endere�o de Entrega para um cliente sem Inscri��o Estadual.", vbInformation, "Aten��o"
    '    Exit Sub
    'End If
    '--------------------------------------------
    
    
    Limpar_Campos_OutEnd (False)
    Status_Botoes_Out_Enderecos "ADICIONAR"
    Status_Controles_Out_Enderecos True
    lblMsgEnd.Visible = False
    
    vTpEnd = Val(cboTpEnd01)
    
    'Pegar Sequencia
    If lsvOutEnd.ListItems.Count = 0 Then
        txtSeqEnd01 = 1
    Else
       
        'Fazer select max na tabela de out.ender., para pegar a maior sequencia+1
        Set OraParameters = db.Parameters
        OraParameters.Remove "CODCLICGC": OraParameters.Add "CODCLICGC", Val(txtCodCliente.Text), 1
        OraParameters.Remove "TPEND": OraParameters.Add "TPEND", vTpEnd, 1
        OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
        OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
        Criar_Cursor OraParameters, "cCursor"
        'vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD0602.PR_MAXSEQ_OUTEND(:cCursor, :CODCLICGC, :TPEND, :vErro, :vMsgErro)")
        vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD060.PR_MAXSEQ_OUTEND(:cCursor, :CODCLICGC, :TPEND, :vErro, :vMsgErro)")
        If vErro = "" Then
            Set rst = OraParameters("cCursor").Value
        Else
            Exit Sub
        End If
    
        vMaxSeq = IIf(IsNull(rst!SeqMax), 0, rst!SeqMax)
    
       txtSeqEnd01 = lsvOutEnd.ListItems(lsvOutEnd.ListItems.Count) + 1
       'txtSeqEnd01 = lsvOutEnd.ListItems.Count + 1
       txtSeqEnd01 = vMaxSeq + 1
       
    End If
    
    cboTpEnd01.SetFocus

End Sub


'vxavier Ci&T - 22/11/2012
'[ISA - 1021] - Criar um m�todo cboTpSegmento_LostFocus() para carregar o combo cboTpCliente com o segmento selecionado, e desativar o m�todo antigo Carrega_Combo_Tipo_ClienteDPK.
'fpiardi - 26/11/2012 alterado para o evento Click
Private Sub cboTpSegmento_Click()

    If Len(Trim(cboTpSegmento.Text)) > 2 Then
        Call Carrega_Combo_Tipo_ClienteDPK(cboTpCliente, Right(Trim(cboTpSegmento.Text), 2))
    End If
    
End Sub

Private Sub cboTransportadora_Change()
    If cboTransportadora.Text = "" Then
        lblTransp.Caption = "Transportadora"
    End If

End Sub

Private Sub cboTransportadora_Click()
      Dim lstrAux As String
          
         'eduardo relvas
         '14/04/2009
         'Estava dando erro que digitava um cliente e ap�s gravado os dados do cliente eu clicava no limpar
         'Coloquei o if abaixo para nao deixar passar se a transportadora estiver com a combo em branco.
         If Trim(cboTransportadora) = "" Then Exit Sub
         
          
         lblTransp.Caption = "Transportadora:" & Right(cboTransportadora.Text, 4)
         lstrAux = Left(cboTransportadora.Text, Len(cboTransportadora.Text) - 7)
         vComboTransportadora = lstrAux
          'cboTransportadora.Clear
          'cboTransportadora.Text = lstrAux
          'If Not IsNumeric(Right(cboCliente, 6)) Then Exit Sub
          'txtCodCliente.Text = Right(cboCliente, 6)
          'vNomeCli = Left(cboCliente, Len(cboCliente) - 9)
          'cboCliente.Clear
          'cboCliente.AddItem vNomeCli
          'cboCliente.ListIndex = 0

End Sub


Private Sub cboTransportadora_GotFocus()
    'Call Carrega_Combo_Transportadora(cboTransportadora)

End Sub

Private Sub cboTransportadora_LostFocus()
    If cboTransportadora.Text <> "" Then
    
        'Left(cboTransportadora.Text, Len(cboTransportadora.Text) - 7)
        If vComboTransportadora & " - " & Right(cboTransportadora.Text, 4) <> cboTransportadora.Text Then
            Call Carrega_Combo_Transportadora(Me.cboTransportadora)
            vComboTransportadora = cboTransportadora.Text
        End If
    Else
        lblTransp.Caption = "Transportadora"
        lblTransp.Refresh
    End If

End Sub
Private Sub cboUF_Click()
    Call Carrega_Combo_Cidade(Me.cboUF.Text, Me.cboCidade)
End Sub

Private Sub cboUF_GotFocus()
    'Call Carrega_Combo_UF(Me.cboUF)
    'Call Carrega_Combo_Cidade(cboUF.Text, cboCidade)
End Sub


Private Sub cboUFEnd01_Click()
    Call Carrega_Combo_Cidade(Me.cboUFEnd01.Text, Me.cboCidadeEnd01)
End Sub


Private Sub cboUFEnd01_GotFocus()
    'Call Carrega_Combo_UF(cboUFEnd01)
End Sub


Private Sub chkConsumidorFinal_Click()
    If txtInscrEstadual.Text = "ISENTO" And Val(cboTpDocto.Text) = 1 Then 'TI-6870
        chkConsumidorFinal.Value = False
    End If
    If chkConsumidorFinal.Value Then
        txtInscrSuframa.Text = ""
    End If

End Sub



Private Sub cmd1_Click()
    
    frmCadEmail.vCgc = frmCadCliente.txtDocto
    frmCadEmail.Show 1
End Sub

Private Sub cmdAceitar_Click()

    Call cmdGravar_Click            'Gravar dados cadastro geral
    If vFl_Campos_OK = True Then
        Call cmdGrlsvRefComl_Click      'Gravar refer�ncias comerciais
        Call cmdGrlsvRefBanco_Click     'Gravar refer�ncias banc�rias
        Call cmdGrlsvOutEnd_Click       'Gravar outros endere�os
        Call pGravaControleWeb          'Gravar controle de cliente na web, quando pr�-cadastro aceito
        sstCadastro.Tab = 0
    
        GoTo EnviarEmail
        MsgBox "Cliente " & Format(txtCodCliente, "000000") & " - WEB cadastrado com sucesso!", vbInformation, "CAD060 - Cadastro de Cliente"
    
        Limpar_Campos_Cadastro (True)
        Limpar_Campos_RefBco (True)
        Limpar_Campos_RefComl (True)
        Limpar_Campos_OutEnd (True)
        'Limpar_Campos_Vendor
    
    End If

Exit Sub

'Enviar e-mail ao usu�rio solicitante, retornando a situa��o de sua solicita��o
EnviarEmail:

    Dim Texto As String, vAuxTexto As String
    'Seleciona C�d.Usu�rio da Controle_Cliente_Web e seu endereco de e-mail
    Set OraParameters = db_INT.Parameters
    OraParameters.Remove "CGC": OraParameters.Add "CGC", Val(vCGC_Analisado), 1
    OraParameters.Remove "TP_SOL": OraParameters.Add "TP_SOL", vFl_Tipo_Solicitacao, 1
    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    Criar_Cursor OraParameters, "PM_CURSOR1"
    vErro = vVB_Generica_001.ExecutaPl(db_INT, "Intranet.PCK_CLIENTES_TESTE.Pr_Sel_UserIntranet(:PM_CURSOR1, :CGC, :TP_SOL, :vErro, :vMsgErro)")
    If vErro = "" Then
        Set db_CONS = OraParameters("PM_CURSOR1").Value
    Else
        Exit Sub
    End If

    If vFl_Tipo_Solicitacao = 0 Then
        vAuxTexto = "Pr�-Cadastro"
    ElseIf vFl_Tipo_Solicitacao = 1 Then
        vAuxTexto = "Pr�-Altera��o"
    ElseIf vFl_Tipo_Solicitacao = 2 Then
        vAuxTexto = "Pr�-Recadastro"
    End If
    
    Texto = "Solicitacao de " & vAuxTexto & " para o CGC " & txtDocto & " foi aceita." & vbCrLf & vbCrLf & _
            "C�digo do Novo Cliente: " & Format(txtCodCliente.Text, "000000") & vbCrLf & vbCrLf & _
            "Favor nao responder esta mensagem." & vbCrLf & vbCrLf
    
    'PARA CADASTRO DE FICHA ENVIADA PELO DPKNET, N�O PRECISA ENVIAR E-MAIL DE CONFIRMA��O DE ACEITE - ALETHEA - 06/09/07
    'If db_CONS!COD_USUARIO <> 1743 Then
    If db_CONS!cod_usuario <> 1905 Then
        'vVB_Generica_001.EnviarEmail db, db_CONS!E_Mail, vAuxTexto & " ACEITO!!! - CGC " & txtDocto, Texto
        vVB_Generica_001.EnviarEmail db_INT, db_CONS!E_Mail, vAuxTexto & " ACEITO!!! - CGC " & txtDocto, Texto
    End If

'        If MsgBox("Erro ao tentar enviar email para o usu�rio solicitante." & vbCrLf & "Deseja tentar novamente ?", vbInformation + vbYesNo, "Aten��o") = vbYes Then
'           GoTo EnviarEmail
'        End If
'    End If

End Sub

Private Sub cmdAdicionar_Click()

    Limpar_Campos_RefBco (False)
    
    If Trim(txtCodCliente.Text) = "" Then
        MsgBox "Por favor, escolha um cliente ou inclua-o, antes de gravar as Refer�ncias", vbCritical, "CAD060 - Aten��o"
        Exit Sub
    End If
    
    txtSeq = lsvRefBanco.ListItems(lsvRefBanco.ListItems.Count) + 1
    txtBanco.SetFocus

End Sub

Private Sub cmdAdicionarRefBco_Click()

Dim vMaxSeq As Long

    vAcao = "Adicionar"
    
    Limpar_Campos_RefBco (False)
    
    Status_Botoes_Ref_Bancarias "ADICIONAR"
    Status_Controles_Ref_Bancarias True
    lblMsgBco.Visible = False
    
    'Pegar Sequencia
    If lsvRefBanco.ListItems.Count = 0 Then
        txtSeq = 1
    Else
       
        'Fazer select max na tabela de referencia banc., para pegar a maior sequencia+1
        Set OraParameters = db.Parameters
        OraParameters.Remove "CODCLICGC": OraParameters.Add "CODCLICGC", Val(txtCodCliente.Text), 1
        OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
        OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
        Criar_Cursor OraParameters, "cCursor"
        'vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD0602.PR_MAXSEQ_REFBCO(:cCursor, :CODCLICGC, :vErro, :vMsgErro)")
        vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD060.PR_MAXSEQ_REFBCO(:cCursor, :CODCLICGC, :vErro, :vMsgErro)")
        If vErro = "" Then
            Set rst = OraParameters("cCursor").Value
        Else
            Exit Sub
        End If
    
        vMaxSeq = IIf(IsNull(rst!SeqMax), 0, rst!SeqMax)
    
       'txtSeq = lsvRefBanco.ListItems(lsvRefBanco.ListItems.Count) + 1
       'txtSeq = lsvRefBanco.ListItems.Count + 1
       txtSeq = vMaxSeq + 1
    
    End If
    
    txtBanco.SetFocus

End Sub

Private Sub cmdAdOutEnd_Click()
   
    vAcao = "Adicionar"
    vFl_Adic_OutEnd = True
    
    If Trim(cboTpEnd01) = "" Then
        MsgBox "� preciso escolher o tipo de endere�o a ser adicionado!", vbCritical, "CAD060 - Aten��o"
        cboTpEnd01.SetFocus
        Exit Sub
    End If
    
End Sub

Private Sub cmdAdRefComl_Click()

Dim vMaxSeq As Long

    vAcao = "Adicionar"
    
    Limpar_Campos_RefComl (False)
    
    Status_Botoes_Ref_Comerciais "ADICIONAR"
    Status_Controles_Ref_Comerciais True
    lblMsgCom.Visible = False
    
    'Pegar Sequencia
    If lsvRefComl.ListItems.Count = 0 Then
        txtSeq01 = 1
    Else
    
        'Fazer select max na tabela de referencia coml, para pegar a maior sequencia+1
        Set OraParameters = db.Parameters
        OraParameters.Remove "CODCLICGC": OraParameters.Add "CODCLICGC", Val(txtCodCliente.Text), 1
        OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
        OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
        Criar_Cursor OraParameters, "cCursor"
        'vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD0602.PR_MAXSEQ_REFCOML(:cCursor, :CODCLICGC, :vErro, :vMsgErro)")
        vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD060.PR_MAXSEQ_REFCOML(:cCursor, :CODCLICGC, :vErro, :vMsgErro)")
        If vErro = "" Then
            Set rst = OraParameters("cCursor").Value
        Else
            Exit Sub
        End If
    
        vMaxSeq = IIf(IsNull(rst!SeqMax), 0, rst!SeqMax)
    
       'txtSeq01 = lsvRefComl.ListItems(lsvRefComl.ListItems.Count) + 1
       'txtSeq01 = lsvRefComl.ListItems.Count + 1
       txtSeq01 = vMaxSeq + 1
    
    End If
    
    txtEmp01.SetFocus

End Sub


'Private Sub cmdAdVendor_Click()
'    Limpar_Campos_Vendor
'
'    If Trim(txtCodCliente.Text) = "" Then
'        MsgBox "Por favor, escolha um cliente ou inclua-o, antes de gravar dados de Vendor", vbCritical, "CAD060 - Aten��o"
'        Exit Sub
'    End If
'
'    txtContratoVendor.Text = lsvVendor.ListItems(lsvVendor.ListItems.Count) + 1
'    txtDtContratoVendor.SetFocus
'End Sub

Private Sub cmdCancelar_Click()

    'carlosj CiT (26/11/2012) -ISA 1033
    'limpa variavel global
    vTipoDocto = ""
    
    Limpar_Campos_Cadastro (True)
    Limpar_Campos_RefBco (True)
    Limpar_Campos_RefComl (True)
    Limpar_Campos_OutEnd (True)
    'Limpar_Campos_Vendor

End Sub


Private Sub cmdCep_Click()
    frmCEPS.Show 1
End Sub

Private Sub cmdCEP_CORREIOS_Click()

    If Err.Number <> 0 Then
        Call MsgBox(CStr(Err.Number) & " - " & Err.Description)
    End If

    Dim psqEndereco As New clsPesquisaEndereco
    Dim vCidade As String

    'DESENV
    'psqEndereco.BancoOracle = "SDPKT"
    'psqEndereco.UsuarioOracle = "PRODUCAO"
    'psqEndereco.SenhaOracle = "DPK"
    
    'PRODUCAO
    psqEndereco.BancoOracle = "SDPK"
    psqEndereco.UsuarioOracle = "CAD060"
    psqEndereco.SenhaOracle = "PROD"

'   ***
'   *** Para atribuir dados a serem preenchidos antes de utilizar a pesquisa
    psqEndereco.CEP = txtCep
    If Trim(cboCidade.Text) <> "" And InStr(1, cboCidade.Text, " - ") > 0 Then
        psqEndereco.Cidade = Mid(cboCidade.Text, 1, InStr(1, cboCidade.Text, " - ") - 1)
    Else
        psqEndereco.Cidade = ""
    End If
    psqEndereco.Uf = cboUF.Text
    psqEndereco.Bairro = txtBairro
    psqEndereco.Logradouro = txtEndereco
    psqEndereco.CPC = txtCxPostal

    If psqEndereco.Pesquisa() Then
        txtEndereco = psqEndereco.Logradouro
        txtBairro = psqEndereco.Bairro
        txtCep = psqEndereco.CEP
        
        cboUF.Text = psqEndereco.Uf
        Call Carrega_Combo_Cidade(Me.cboUF.Text, Me.cboCidade)
        vCidade = psqEndereco.Cidade
        'Pesquisar na tabela de cidade, para devolver corretamente o nome da cidade e o c�digo
        Set OraParameters = db.Parameters
        OraParameters.Remove "NOME":        OraParameters.Add "NOME", Trim(vCidade), 1
        OraParameters.Remove "CODCID":      OraParameters.Add "CODCID", Null, 1
        OraParameters.Remove "CODUF":      OraParameters.Add "CODUF", cboUF.Text, 1
        OraParameters.Remove "TP_CONS":     OraParameters.Add "TP_CONS", 0, 1
        OraParameters.Remove "vErro":       OraParameters.Add "vErro", 0, 2
        OraParameters.Remove "vMsgErro":    OraParameters.Add "vMsgErro", "", 2
        OraParameters("vErro").ServerType = ORATYPE_NUMBER
        Criar_Cursor OraParameters, "vCursor"
        vErro = vVB_Generica_001.ExecutaPl(db, "Producao.PCK_CAD060.Pr_Sel_Cidade(:vCursor,:NOME,:CODCID,:CODUF," & _
                                                                                 ":TP_CONS,:vErro,:vMsgErro)")
        If vErro <> "" Then
            Exit Sub
        End If
        vVB_Generica_001.PreencheComboList OraParameters("vCursor").Value, cboCidade, "nome_cidade", "cod_cidade"
        cboCidade.ListIndex = 0
    End If


End Sub


Private Sub cmdCEP_CORREIOS2_Click()

    Dim psqEndereco As New clsPesquisaEndereco
    Dim vCidade As String

    psqEndereco.BancoOracle = "SDPK"
    psqEndereco.UsuarioOracle = "PRODUCAO"
    psqEndereco.SenhaOracle = "DPK"
    'psqEndereco.BancoOracle = "PRODUCAO"
    'psqEndereco.UsuarioOracle = "CAD060"
    'psqEndereco.SenhaOracle = "PROD"

'   ***
'   *** Para atribuir dados a serem preenchidos antes de utilizar a pesquisa
    psqEndereco.CEP = txtCepEnd01
    If Trim(cboCidadeEnd01.Text) <> "" Then
        psqEndereco.Cidade = Mid(cboCidadeEnd01.Text, 1, InStr(1, cboCidadeEnd01.Text, " - ") - 1)
    Else
        psqEndereco.Cidade = ""
    End If
    psqEndereco.Uf = cboUFEnd01.Text
    psqEndereco.Bairro = txtBairroEnd01
    psqEndereco.Logradouro = txtEnd01
    psqEndereco.CPC = txtCxPosEnd01

    If psqEndereco.Pesquisa() Then
        txtEnd01 = psqEndereco.Logradouro
        txtBairroEnd01 = psqEndereco.Bairro
        txtCepEnd01 = psqEndereco.CEP
        
        cboUFEnd01 = psqEndereco.Uf
        Call Carrega_Combo_Cidade(Me.cboUFEnd01.Text, Me.cboCidadeEnd01)
        vCidade = psqEndereco.Cidade
        'Pesquisar na tabela de cidade, para devolver corretamente o nome da cidade e o c�digo
        Set OraParameters = db.Parameters
        OraParameters.Remove "NOME":        OraParameters.Add "NOME", Trim(vCidade), 1
        OraParameters.Remove "CODCID":      OraParameters.Add "CODCID", Null, 1
        OraParameters.Remove "CODUF":      OraParameters.Add "CODUF", cboUFEnd01.Text, 1
        OraParameters.Remove "TP_CONS":     OraParameters.Add "TP_CONS", 0, 1
        OraParameters.Remove "vErro":       OraParameters.Add "vErro", 0, 2
        OraParameters.Remove "vMsgErro":    OraParameters.Add "vMsgErro", "", 2
        OraParameters("vErro").ServerType = ORATYPE_NUMBER
        Criar_Cursor OraParameters, "vCursor"
        vErro = vVB_Generica_001.ExecutaPl(db, "Producao.PCK_CAD060.Pr_Sel_Cidade(:vCursor,:NOME,:CODCID,:CODUF," & _
                                                                                 ":TP_CONS,:vErro,:vMsgErro)")
        If vErro <> "" Then
            Exit Sub
        End If
        vVB_Generica_001.PreencheComboList OraParameters("vCursor").Value, cboCidadeEnd01, "nome_cidade", "cod_cidade"
        cboCidadeEnd01.ListIndex = 0
    End If

End Sub


Private Sub cmdConsSCISerasa_Click()
    If txtCodCliente = "" Then
        MsgBox "Selecione um cliente.", vbInformation, "Aten��o"
        Exit Sub
    End If
    frmConsulta.Show 1
End Sub

Private Sub cmdDescartar_Click()

    If Trim(vCGC_Analisado) <> Trim(txtDocto) Then
        MsgBox "Este cliente n�o pode ser descartado! " & Chr(13) & _
               "N�o se trata do cliente escolhido na tela de an�lise." & Chr(13) & _
               "CGC do Cliente Analisado: " & vCGC_Analisado, vbInformation, "CAD060 - Aten��o"
        Exit Sub
    End If
    
    Call pGravaControleWeb          'Gravar controle de cliente na web, quando consulta de pr�-cadastro aceito

    Limpar_Campos_Cadastro (True)
    Limpar_Campos_RefBco (True)
    Limpar_Campos_RefComl (True)
    Limpar_Campos_OutEnd (True)

End Sub

Private Sub cmdDesfOutEnd_Click()

    vFl_Adic_OutEnd = False
    Limpar_Campos_OutEnd False
    Status_Botoes_Out_Enderecos "ABRIR"
    Status_Controles_Out_Enderecos False

End Sub

Private Sub cmdDesfRefBco_Click()
    
    Limpar_Campos_RefBco False
    Status_Botoes_Ref_Bancarias "ABRIR"
    Status_Controles_Ref_Bancarias False

End Sub

Private Sub cmdDesfRefCom_Click()

    Limpar_Campos_RefComl False
    Status_Botoes_Ref_Comerciais "ABRIR"
    Status_Controles_Ref_Comerciais False

End Sub

Private Sub cmdEditarRefBco_Click()
    vAcao = "Editar"
    lblMsgBco.Visible = False
    If txtSeq = "" Then
        MsgBox "Selecione um item da lista."
        Exit Sub
    End If
    Status_Botoes_Ref_Bancarias "ADICIONAR"
    Status_Controles_Ref_Bancarias True
End Sub

Private Sub cmdEdRefComl_Click()
    vAcao = "Editar"
    lblMsgCom.Visible = False
    If txtSeq01 = "" Then
        MsgBox "Selecione um item da lista.", vbInformation, "CAD060 - Aten��o"
        Exit Sub
    End If
    Status_Botoes_Ref_Comerciais "ADICIONAR"
    Status_Controles_Ref_Comerciais True
End Sub

Private Sub cmdGravar_Click()

       'Muniz 10/02/10 Valida espa�o em branco dos campos abaixo
    If Not ValidaBranco(cboCliente) Then
        MsgBox "Existe mais de um espa�o em branco no campo Nome do Cliente.", vbCritical, "CAD060 - Aten��o"
        cboCliente.SetFocus
        Exit Sub
    End If
    If Not ValidaBranco(txtNomeEnd01) Then
        MsgBox "Existe mais de um espa�o em branco no campo Nome Cliente.", vbCritical, "CAD060 - Aten��o"
        txtNomeEnd01.SetFocus
        Exit Sub
    End If
    If Not ValidaBranco(txtEnd01) Then
        MsgBox "Existe mais de um espa�o em branco no campo Endere�o.", vbCritical, "CAD060 - Aten��o"
        txtEnd01.SetFocus
        Exit Sub
    End If
    If Not ValidaBranco(txtBairroEnd01) Then
        MsgBox "Existe mais de um espa�o em branco no campo Bairro.", vbCritical, "CAD060 - Aten��o"
        txtBairroEnd01.SetFocus
        Exit Sub
    End If
    If Not ValidaBranco(txtBairroEnd01) Then
        MsgBox "Existe mais de um espa�o em branco no campo Bairro.", vbCritical, "CAD060 - Aten��o"
        txtBairroEnd01.SetFocus
        Exit Sub
    End If
    '-------
    If Not ValidaBranco(txtEndereco) Then
        MsgBox "Existe mais de um espa�o em branco no campo Endere�o.", vbCritical, "CAD060 - Aten��o"
        txtEndereco.SetFocus
        Exit Sub
    End If
    'Eduardo Faria de Oliveira - CIT - 22/08/12
    If Not ValidaBranco(txtComplemento) Then
        MsgBox "Existe mais de um espa�o em branco no campo Endere�o.", vbCritical, "CAD060 - Aten��o"
        txtEndereco.SetFocus
        Exit Sub
    End If
    If Not ValidaBranco(txtBairro) Then
        MsgBox "Existe mais de um espa�o em branco no campo Bairro.", vbCritical, "CAD060 - Aten��o"
        txtBairro.SetFocus
        Exit Sub
    End If
    If Not ValidaBranco(txtContato) Then
        MsgBox "Existe mais de um espa�o em branco no campo Contato.", vbCritical, "CAD060 - Aten��o"
        txtContato.SetFocus
        Exit Sub
    End If
'    If Not ValidaBranco(txtEmail) Then
'        MsgBox "Existe mais de um espa�o em branco no campo Email.", vbCritical, "CAD060 - Aten��o"
'        txtEmail.SetFocus
'        Exit Sub
'    End If
    If Not ValidaBranco(txtHomePage) Then
        MsgBox "Existe mais de um espa�o em branco no campo HomePage.", vbCritical, "CAD060 - Aten��o"
        txtHomePage.SetFocus
        Exit Sub
    End If
    If Not ValidaBranco(txtResponsavel) Then
        MsgBox "Existe mais de um espa�o em branco no campo Responsavel.", vbCritical, "CAD060 - Aten��o"
        txtResponsavel.SetFocus
        Exit Sub
    End If
    If Not ValidaBranco(txtProprietario) Then
        MsgBox "Existe mais de um espa�o em branco no campo Propriet�rio.", vbCritical, "CAD060 - Aten��o"
        txtProprietario.SetFocus
        Exit Sub
    End If
    
    ' Tratamento de erro para o campo Diferenciado - N�o deixar informar valores diferentes de SIM ou N�O e n�o pode ficar em branco - SDS1889 - Jairo Almeida  - 22-02-2012
    If cboDiferenciado = "" Then
        MsgBox "Nenhum valor foi selecionado no Combo Diferenciado, selecione uma das op��es para continuar", vbCritical, "CAD060 - Aten��o"
        cboDiferenciado.SetFocus
        Exit Sub
    End If
    
    'fim Muniz
    
    'vxavier CIT 14/12/2012 - Defect #439 - [CAD060] Erro ao gravar o cliente sem preencher o campo Cidade
    If cboCidade.Text = "" Then
        MsgBox "� necess�rio escolher uma cidade", vbCritical, "CAD060 - Aten��o"
        cboCidade.SetFocus
        Exit Sub
    End If
    
    'vxavier - CIT - 22/08/2012 - [ISA349] - [frmCadCliente] Alterar eventos click
    If txtDDD1.Text = "" Then
        MsgBox "� necess�rio preencher o DDD", vbCritical, "CAD060 - Aten��o"
        txtDDD1.SetFocus
        Exit Sub
    End If
    
    If txtFone1.Text = "" Then
        MsgBox "� necess�rio preencher o telefone", vbCritical, "CAD060 - Aten��o"
        txtFone1.SetFocus
        Exit Sub
    End If
    
    If txtContato.Text = "" Then
        MsgBox "� necess�rio preencher o contato", vbCritical, "CAD060 - Aten��o"
        txtContato.SetFocus
        Exit Sub
    End If
    
    If txtNumero.Text = "" Then
        txtNumero.Text = 0
    End If
    
    If Not IsNumeric(txtNumero.Text) Then
        MsgBox "O n�mero est� em formato incorreto", vbCritical, "CAD060 - Aten��o"
        txtNumero.SetFocus
        Exit Sub
    End If
    
    If ValidaCamposEndereco(txtEndereco.Text, ",") Then
        MsgBox "N�o pode conter virgula no endere�o", vbCritical, "CAD060 - Aten��o"
        txtEndereco.SetFocus
        Exit Sub
    End If
    
    If ValidaCamposEndereco(txtNumero.Text, ",") Or ValidaCamposEndereco(txtNumero.Text, ".") Then
        MsgBox "N�o pode conter virgula ou ponto no n�mero", vbCritical, "CAD060 - Aten��o"
        txtNumero.SetFocus
        Exit Sub
    End If
    
    'Eveline Dupas - CIT - 04/01/2013
    If ValidaRepresentante(cboRepresDPK.Text) = False Then
        MsgBox "� necess�rio que o cliente tenha um representante", vbCritical, "CAD060 - Aten��o"
        Exit Sub
    End If

'------- NAO se pode alterar dados de um DESATIVADO --------
If Left(txtSituacao.Text, 1) = "9" Then
    MsgBox "Cliente Desativado. N�o posso Alterar Dados !!!", vbCritical, "CAD060 - Aten��o"
    Exit Sub
End If
If Trim(cboStatus.Text) = "" Then
    MsgBox "STATUS do Cliente n�o Selecionado!", vbCritical, "CAD060 - Aten��o"
    Exit Sub
End If

If vFl_Analise = True And vFl_Tipo_Solicitacao = 0 Then
    Fl_Cliente_Novo = "S"
End If

Select Case sstCadastro.Tab
    Case 0, 1
    
        ' fpallini - Ci&T - 23/08/12 - [ISA-330] - Salvar dados do cliente no SAP
        Dim cli As ClienteSAP
        Dim msgErro As String
        Set cli = BuscarClienteSAP(Val(cboTpDocto), txtDocto, msgErro)
        
        If msgErro <> "" Then
            MsgBox msgErro
            Exit Sub
        End If
        
        txtSAP.Text = cli.NoClienteSAP
        
        Call PopularObjetoCliente(cli)
        
        ' fpallini - Ci&T - 23/11/12 - CR#420 - isqa - Garantir preenchimento de endere�os vindos do pr�-cadastro
        Call PopularObjetoClienteEnderecos(cli)
        
        Call PopularObjetoClienteEmails(cli, txtEmail, Val(txtCodCliente.Text)) 'TI-6264
        
        'If EnviarClienteSAP(cli) Then
        '    txtSAP.Text = cli.NoClienteSAP
            ' Se funcionou o envio do cliente pro SAP ent�o persistimos no legado
            Call pGravaDadosdoCliente(cli)
        'Else
        '    MsgBox "Erro ao gravar o cliente no SAP. Cadastro Abortado.", vbCritical, "CAD060 - Aten��o"
        'End If
                
End Select

If vFl_Campos_OK = True Then
    Fl_Cliente_Novo = "N"
End If


End Sub

Private Sub cmdGrLinVdr_Click()
Dim vAux As Integer

If Trim(txtCodCliente.Text) = "" Then
    MsgBox "Por favor, escolha um cliente ou inclua-o, antes de gravar Linhas VDR", vbCritical, "CAD060 - Aten��o"
    Exit Sub
End If

'Verifica se existem linhas VDR marcadas para serem gravadas
vAux = 0
With grdflexLinVdr
    .Col = 2
    If .TextMatrix(.Row, .Col) = "X" And vAux = 0 Then
        vAux = 1
    End If
End With
If vAux = 0 Then
    MsgBox "Por favor, assinale as linhas VDR para serem gravadas para este cliente!", vbInformation, "CAD060 - Cadastro de Cliente"
    Exit Sub
End If


'If MsgBox("Confirma inclus�o/altera��o das linhas vdr?", vbYesNo, "CAD060 - Cadastro de Clientes") = vbNo Then
'    Exit Sub
'End If

Screen.MousePointer = 11

'Vai deletar todas as linhas cadastradas para o cliente e gravar tudo, conforme alterado/inserido/retirado pelo Depto.Cr�dito
Set OraParameters = db.Parameters
OraParameters.Remove "CODCLI": OraParameters.Add "CODCLI", Val(txtCodCliente.Text), 1
OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2

db.Executesql "Begin PRODUCAO.PCK_CLIENTE.PR_DEL_LINHA_PRODUTO" & _
              "(:CODCLI,:vErro,:vMsgErro); END;"
If Val(IIf(IsNull(db.Parameters("vErro")), 0, db.Parameters("vErro"))) <> 0 Then
    MsgBox "Erro: " & Val(OraParameters("vErro").Value) & " - " & _
                      OraParameters("vMsgErro").Value & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
    Exit Sub
End If

'Vai gravar tudo o que estiver marcado com "X"
I = 1
With grdflexLinVdr
    
    Do While .Rows <> I
        
        If .TextMatrix(I, 2) = "X" Then
           
            Set OraParameters = db.Parameters
            OraParameters.Remove "CODCLI": OraParameters.Add "CODCLI", Val(txtCodCliente.Text), 1
            OraParameters.Remove "LINHA": OraParameters.Add "LINHA", .TextMatrix(I, 0), 1
            OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
            OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
            db.Executesql "BEGIN PRODUCAO.PCK_CLIENTE.Pr_Grava_Linha_Produto(:CODCLI, :LINHA, :vErro, :vMsgErro); END;"
            
            If Val(IIf(IsNull(OraParameters("vErro")), 0, OraParameters("vErro"))) <> 0 Then
                MsgBox "Erro: " & Val(OraParameters("vErro").Value) & " - " & _
                                  OraParameters("vMsgErro").Value & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
                Exit Sub
            End If
           .Refresh
         End If
        
        I = I + 1
        
    Loop
End With
    
Screen.MousePointer = 0
'MsgBox "Linhas Salvas para o Cliente"
End Sub

Private Sub cmdGrlsvOutEnd_Click()
    
    
    Set OraParameters = db.Parameters
    OraParameters.Remove "CODCLI": OraParameters.Add "CODCLI", Val(txtCodCliente.Text), 1
    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    
    
    For I = 1 To lsvOutEnd.ListItems.Count
    
        'Inserida consist�ncia para n�o permitir que seja inclu�do um endere�o em branco - Alethea - 18/05/05
        If Trim(lsvOutEnd.ListItems(I).SubItems(6)) = "" Then
            MsgBox "Endere�o n�o pode ser branco. " & Chr(15) & _
                   "Sequ�ncia " & Val(lsvOutEnd.ListItems(I)) & " de outros endere�os, n�o ser� gravada!", vbCritical, "CAD060 - Aten��o"
        Else
            
            OraParameters.Remove "SEQ": OraParameters.Add "SEQ", Val(lsvOutEnd.ListItems(I)), 1
            OraParameters.Remove "TPEND": OraParameters.Add "TPEND", Val(Left(lsvOutEnd.ListItems(I).SubItems(1), 1)), 1
            OraParameters.Remove "TPDOC": OraParameters.Add "TPDOC", Val(Left(lsvOutEnd.ListItems(I).SubItems(2), 1)), 1
            OraParameters.Remove "CGC": OraParameters.Add "CGC", Val(lsvOutEnd.ListItems(I).SubItems(3)), 1
            OraParameters.Remove "IE": OraParameters.Add "IE", lsvOutEnd.ListItems(I).SubItems(4), 1
            OraParameters.Remove "NOME": OraParameters.Add "NOME", lsvOutEnd.ListItems(I).SubItems(5), 1
            
            'Kevlin Toshinari Ossada  - CIT - 22/8/2012 Concatena campos de endere�o e passa como parametro para a PROC
            OraParameters.Remove "END": OraParameters.Add "END", ConcatenarEnderecoSAP(lsvOutEnd.ListItems(I).SubItems(6), lsvOutEnd.ListItems(I).SubItems(7), lsvOutEnd.ListItems(I).SubItems(8)), 1
            
            OraParameters.Remove "CODCID": OraParameters.Add "CODCID", Val(Right(lsvOutEnd.ListItems(I).SubItems(12), 4)), 1
            OraParameters.Remove "BAIRRO": OraParameters.Add "BAIRRO", lsvOutEnd.ListItems(I).SubItems(9), 1
            OraParameters.Remove "CEP": OraParameters.Add "CEP", Val(lsvOutEnd.ListItems(I).SubItems(10)), 1
            OraParameters.Remove "DDD": OraParameters.Add "DDD", Val(lsvOutEnd.ListItems(I).SubItems(13)), 1
            OraParameters.Remove "FONE": OraParameters.Add "FONE", Val(lsvOutEnd.ListItems(I).SubItems(14)), 1
            OraParameters.Remove "FAX": OraParameters.Add "FAX", Val(lsvOutEnd.ListItems(I).SubItems(15)), 1
            OraParameters.Remove "CXPOST": OraParameters.Add "CXPOST", Val(lsvOutEnd.ListItems(I).SubItems(16)), 1
            
            db.Executesql "BEGIN PRODUCAO.PCK_CLIENTE.PR_INSERT_OUT_END" & _
                                 "(:CODCLI,:TPEND,:SEQ,:END,:CODCID,:BAIRRO,:CEP,:DDD," & _
                                 " :FONE,:FAX,:CXPOST,:TPDOC,:CGC,:IE,:NOME,:vErro,:vMsgErro); " & _
                            "END;"
            If Val(IIf(IsNull(db.Parameters("vErro")), 0, db.Parameters("vErro"))) <> 0 Then
                MsgBox "Erro: " & Val(OraParameters("vErro").Value) & " - " & _
                                  OraParameters("vMsgErro").Value & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
                Exit Sub
            End If
        
        End If
    
    Next I

End Sub

Private Sub cmdGrlsvRefBanco_Click()


    'Varre o listview e grava todas as linhas de refer�ncias
    Set OraParameters = db.Parameters
    OraParameters.Remove "CODCLI": OraParameters.Add "CODCLI", Val(txtCodCliente.Text), 1
    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    
    For I = 1 To lsvRefBanco.ListItems.Count
    
        OraParameters.Remove "SEQ": OraParameters.Add "SEQ", Val(lsvRefBanco.ListItems(I)), 1
        OraParameters.Remove "BANCO": OraParameters.Add "BANCO", lsvRefBanco.ListItems(I).SubItems(1), 1
        'OraParameters.Remove "DTCONS": OraParameters.Add "DTCONS", lsvRefBanco.ListItems(i).SubItems(2), 1
        OraParameters.Remove "DTCONS": OraParameters.Add "DTCONS", IIf(Trim(txtDtCadastro.Text) = "", Null, txtDtCadastro.Text), 1
        OraParameters.Remove "AGENCIA": OraParameters.Add "AGENCIA", Val(lsvRefBanco.ListItems(I).SubItems(3)), 1
        OraParameters.Remove "DDD": OraParameters.Add "DDD", Val(lsvRefBanco.ListItems(I).SubItems(4)), 1
        OraParameters.Remove "FONE": OraParameters.Add "FONE", Val(lsvRefBanco.ListItems(I).SubItems(5)), 1
        OraParameters.Remove "CONCEITO": OraParameters.Add "CONCEITO", lsvRefBanco.ListItems(I).SubItems(6), 1
        OraParameters.Remove "OBSERVACAO": OraParameters.Add "OBSERVACAO", lsvRefBanco.ListItems(I).SubItems(7), 1
        
        db.Executesql "BEGIN PRODUCAO.PCK_CLIENTE.PR_INSERT_REF_BANCO" & _
                                 "(:CODCLI,:SEQ,:BANCO,TO_DATE(:DTCONS,'DD/MM/RR')," & _
                                 " :AGENCIA,:DDD,:FONE,:CONCEITO,:OBSERVACAO," & _
                                 " :vErro,:vMsgErro); END;"
                                 
        If Val(IIf(IsNull(db.Parameters("vErro")), 0, db.Parameters("vErro"))) <> 0 Then
            MsgBox "Erro: " & Val(OraParameters("vErro").Value) & " - " & _
                              OraParameters("vMsgErro").Value & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
            Exit Sub
        End If

    Next I

End Sub

Private Sub cmdGrlsvRefComl_Click()
    
    'Varre o listview e grava todas as linhas de refer�ncias
    Set OraParameters = db.Parameters
    OraParameters.Remove "CODCLI": OraParameters.Add "CODCLI", Val(txtCodCliente.Text), 1
    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    
    For I = 1 To lsvRefComl.ListItems.Count
    
        If Trim(cboCodCliGrMaster) <> "" And I = 1 Then
            'litem.SubItems(11) = IIf((Val(vCod_GrupoMaster) <> 0 And i = 1), "MESMO GRUPO CLIENTE: " & cboCodCliGrMaster.Text, "")
            lsvRefComl.ListItems(I).SubItems(11) = "MESMO GRUPO CLIENTE: " & cboCodCliGrMaster.Text
        End If
    
        OraParameters.Remove "SEQ": OraParameters.Add "SEQ", Val(lsvRefComl.ListItems(I)), 1
        OraParameters.Remove "EMPRESA": OraParameters.Add "EMPRESA", lsvRefComl.ListItems(I).SubItems(1), 1
        'OraParameters.Remove "DTCONS": OraParameters.Add "DTCONS", lsvRefComl.ListItems(i).SubItems(2), 1
        OraParameters.Remove "DTCONS": OraParameters.Add "DTCONS", IIf(Trim(txtDtCadastro.Text) = "", Null, txtDtCadastro.Text), 1
        OraParameters.Remove "DDD": OraParameters.Add "DDD", Val(lsvRefComl.ListItems(I).SubItems(3)), 1
        OraParameters.Remove "FONE": OraParameters.Add "FONE", Val(lsvRefComl.ListItems(I).SubItems(4)), 1
        OraParameters.Remove "ULTFAT": OraParameters.Add "ULTFAT", Val(lsvRefComl.ListItems(I).SubItems(5)), 1
        OraParameters.Remove "DTULTFAT": OraParameters.Add "DTULTFAT", lsvRefComl.ListItems(I).SubItems(6), 1
        OraParameters.Remove "SLDEV": OraParameters.Add "SLDEV", Val(lsvRefComl.ListItems(I).SubItems(7)), 1
        OraParameters.Remove "DTCAD": OraParameters.Add "DTCAD", lsvRefComl.ListItems(I).SubItems(8), 1
        OraParameters.Remove "ATRASO": OraParameters.Add "ATRASO", Val(lsvRefComl.ListItems(I).SubItems(9)), 1
        OraParameters.Remove "CONCEITO": OraParameters.Add "CONCEITO", lsvRefComl.ListItems(I).SubItems(10), 1
        OraParameters.Remove "OBSERVACAO": OraParameters.Add "OBSERVACAO", lsvRefComl.ListItems(I).SubItems(11), 1
        
        db.Executesql "BEGIN PRODUCAO.PCK_CLIENTE.PR_INSERT_REF_COML" & _
                                 "(:CODCLI,:SEQ,TO_DATE(:DTCONS,'DD/MM/RR')," & _
                                 " :EMPRESA,TO_DATE(:DTCAD,'DD/MM/RR'),:ULTFAT," & _
                                 " :DTULTFAT,:SLDEV,:ATRASO,:CONCEITO,:OBSERVACAO," & _
                                 " :DDD,:FONE,:vErro,:vMsgErro); END;"
                                 
        If Val(IIf(IsNull(db.Parameters("vErro")), 0, db.Parameters("vErro"))) <> 0 Then
            MsgBox "Erro: " & Val(OraParameters("vErro").Value) & " - " & _
                              OraParameters("vMsgErro").Value & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
            Exit Sub
        End If
    
    Next I

End Sub

Private Sub cmdGrOutEnd_Click()
    Dim litem As ListItem
    
    lblMsgEnd.Visible = False
    
    If Left(txtSituacao.Text, 1) = "9" Then
        MsgBox "Cliente Desativado. N�o posso Alterar Dados !!!", vbCritical, "CAD060 - Aten��o"
        Exit Sub
    End If
    
    'Kevlin Toshinari Ossda - CIT - 22/08/2012 - Valida campos de num, endere�o e compl com virgula
    If ValidaCamposEndereco(txtEnd01.Text, ",") Or ValidaCamposEndereco(txtNumEnd01.Text, ",") Or ValidaCamposEndereco(txtComplEnd01.Text, ",") Then
        MsgBox ("N�o � permitida a digita��o de v�rgula (,) nos campos de endere�o")
        Exit Sub
    End If
    
    'vxavier - CIT - 29/08/2012 - n�o deve inserir no sap numero vazio
    If txtNumEnd01.Text = "" Then
        txtNumEnd01.Text = 0
    End If
    
    'Kevlin Toshinari Ossda - CIT - 22/08/2012 - Valida se campo Num. � v�lido
    If Not IsNumeric(txtNumEnd01.Text) Then
        MsgBox ("N�mero Inv�lido")
        Exit Sub
    End If
    
    'Kevlin Toshinari Ossda - CIT - 22/08/2012 - Valida valida o tamanho de endere�o
    If Len(ConcatenarEnderecoSAP(txtEnd01.Text, txtNumEnd01.Text, txtComplEnd01.Text)) > 33 Then
        MsgBox ("O endere�o completo n�o deve ultrapassar 33 caract�res.")
        Exit Sub
    End If
    
    'vxavier - CIT - 22/08/2012 - [ISA-349] - [frmCadCliente] Alterar eventos click
    If txtDDDEnd01 = "" Then
        MsgBox "� necess�rio preencher o DDD", vbCritical, "CAD060 - Aten��o"
        Exit Sub
    End If
    
    'vxavier - CIT - 22/08/2012 - [ISA-349] - [frmCadCliente] Alterar eventos click
    If txtFoneEnd01 = "" Then
        MsgBox "� necess�rio preencher o telefone", vbCritical, "CAD060 - Aten��o"
        Exit Sub
    End If
    
    'Relvas Durante teste das alteracoes do Muniz
    If Not ValidaBranco(Me.txtNomeEnd01) Then
        MsgBox "Existe mais de um espa�o em branco no campo Nome Cliente.", vbCritical, "CAD060 - Aten��o"
        txtNomeEnd01.SetFocus
        Exit Sub
    End If

    If Not ValidaBranco(txtEnd01) Then
        MsgBox "Existe mais de um espa�o em branco no campo Endere�o.", vbCritical, "CAD060 - Aten��o"
        txtEnd01.SetFocus
        Exit Sub
    End If
    
    If Not ValidaBranco(txtBairroEnd01) Then
        MsgBox "Existe mais de um espa�o em branco no campo Bairro.", vbCritical, "CAD060 - Aten��o"
        txtBairroEnd01.SetFocus
        Exit Sub
    End If
    
    '-------------------------
    'Valida��es de Campos
    '-------------------------
    'Consist�ncia do CEP, conforme solicita��o da M�nica e Nat�lia em 25/05/05 - Alethea 06/06/05
    If Trim(txtCepEnd01) = "" Or Val(txtCepEnd01) = 0 Then
        vVB_Generica_001.Informar "CEP Inv�lido"
        txtCepEnd01.SetFocus
        Exit Sub
    ElseIf Len(txtCepEnd01) <> 8 Then
        vVB_Generica_001.Informar "CEP Inv�lido! CEP precisa conter 8 d�gitos."
        txtCepEnd01.SetFocus
        Exit Sub
    Else
        Dim vAuxCep As String
        vAuxCep = Mid(txtCepEnd01, 1, 1)     'Pega o 1o. d�gito do CEP e valida de acordo com a UF
        If cboUFEnd01 = "SP" And (vAuxCep <> 0 And vAuxCep <> 1) Then
            vVB_Generica_001.Informar "CEP Inv�lido! Os CEPs do estado de SP devem iniciar com 0 ou 1."
            txtCepEnd01.SetFocus
            Exit Sub
        ElseIf (cboUFEnd01 = "RJ" Or cboUFEnd01 = "ES") And (vAuxCep <> 2) Then
            vVB_Generica_001.Informar "CEP Inv�lido! Os CEPs dos estados do RJ e ES devem iniciar com 2."
            txtCepEnd01.SetFocus
            Exit Sub
        ElseIf cboUFEnd01 = "MG" And (vAuxCep <> 3) Then
            vVB_Generica_001.Informar "CEP Inv�lido! Os CEPs do estado de MG devem iniciar com 3."
            txtCepEnd01.SetFocus
            Exit Sub
        ElseIf (cboUFEnd01 = "BA" Or cboUFEnd01 = "SE") And (vAuxCep <> 4) Then
            vVB_Generica_001.Informar "CEP Inv�lido! Os CEPs dos estados da BA e SE devem iniciar com 4."
            txtCepEnd01.SetFocus
            Exit Sub
        ElseIf (cboUFEnd01 = "PE" Or cboUFEnd01 = "AL" Or cboUFEnd01 = "PB" Or cboUFEnd01 = "RN") And _
               (vAuxCep <> 5) Then
            vVB_Generica_001.Informar "CEP Inv�lido! Os CEPs dos estados de PE, AL, PB e RN devem iniciar com 5."
            txtCepEnd01.SetFocus
            Exit Sub
        ElseIf (cboUFEnd01 = "CE" Or cboUFEnd01 = "PI" Or cboUFEnd01 = "MA" Or cboUFEnd01 = "PA" Or _
                cboUFEnd01 = "AM" Or cboUFEnd01 = "AC" Or cboUFEnd01 = "AP" Or cboUFEnd01 = "RR") And (vAuxCep <> 6) Then
            vVB_Generica_001.Informar "CEP Inv�lido! Os CEPs dos estados do CE, PI, MA, PA, AM, AC, AP e RR devem iniciar com 6."
            txtCepEnd01.SetFocus
            Exit Sub
        ElseIf (cboUFEnd01 = "DF" Or cboUFEnd01 = "GO" Or cboUFEnd01 = "TO" Or cboUFEnd01 = "MS" Or _
                cboUFEnd01 = "RO") And (vAuxCep <> 7) Then
            vVB_Generica_001.Informar "CEP Inv�lido! Os CEPs dos estados do DF, GO, TO, MS e RO devem iniciar com 7."
            txtCepEnd01.SetFocus
            Exit Sub
        ElseIf (cboUFEnd01 = "PR" Or cboUFEnd01 = "SC") And (vAuxCep <> 8) Then
            vVB_Generica_001.Informar "CEP Inv�lido! Os CEPs dos estados do PR e SC devem iniciar com 8."
            txtCepEnd01.SetFocus
            Exit Sub
        ElseIf (cboUFEnd01 = "RS") And (vAuxCep <> 9) Then
            vVB_Generica_001.Informar "CEP Inv�lido! Os CEPs dos estados do RS devem iniciar com 9."
            txtCepEnd01.SetFocus
            Exit Sub
        End If
    End If
    
    If Trim(cboTpDoctoEnd01) = "" Then
        MsgBox "Tipo de Documento Inv�lido!!!", vbCritical, "CAD060 - Aten��o"
        cboTpDoctoEnd01.SetFocus
        Exit Sub
    End If
    If Trim(txtDocto) = "" Then
        MsgBox "Documento Inv�lido - Guia Cadastro Geral!!!", vbCritical, "CAD060 - Aten��o"
        If txtDocto.Enabled = True Then txtDocto.SetFocus
        Exit Sub
    End If
    'Valida CGC ou CPF
    If Val(cboTpDoctoEnd01.Text) = 1 Then
        If Not vVB_Generica_001.ValidaCGC(txtCGCEnd01.Text) Then
            MsgBox "CGC Inv�lido!!!", vbCritical, "CAD060 - Aten��o"
            txtCGCEnd01.SetFocus
            Exit Sub
        End If
    Else
        If Not vVB_Generica_001.ValidaCIC(txtCGCEnd01.Text) Then
            MsgBox "CPF Inv�lido!!!", vbCritical, "CAD060 - Aten��o"
            If txtCGCEnd01.Enabled = True Then txtCGCEnd01.SetFocus
            Exit Sub
        End If
    End If
    If Trim(cboTpEnd01) = "" Then
        MsgBox "Tipo de Endere�o Inv�lido!!!", vbCritical, "CAD060 - Aten��o"
        cboTpEnd01.SetFocus
        Exit Sub
    End If
    If Trim(txtEnd01) = "" Then
        MsgBox "Endere�o Inv�lido!!!", vbCritical, "CAD060 - Aten��o"
        txtEnd01.SetFocus
        Exit Sub
    End If
    'Quando for Tipo 1 n�o exigir o Bairro - Eduardo - 01/09/05
    If Val(Mid(cboTpEnd01.Text, 1, 1)) <> 1 Then
        If Trim(txtBairroEnd01) = "" Then
            MsgBox "Bairro Inv�lido!!!", vbCritical, "CAD060 - Aten��o"
            txtBairroEnd01.SetFocus
            Exit Sub
        End If
    End If
    If Trim(cboCidadeEnd01.Text) = "" Then
        MsgBox "Cidade Inv�lida!!!", vbCritical, "CAD060 - Aten��o"
        cboCidadeEnd01.SetFocus
        Exit Sub
    End If
    If Trim(cboUFEnd01.Text) = "" Then
        MsgBox "UF Inv�lido!!!", vbCritical, "CAD060 - Aten��o"
        cboUFEnd01.SetFocus
        Exit Sub
    End If
    If Trim(txtIEEnd01.Text) <> "" Then
        Call Valida_IE(frmCadCliente.txtIEEnd01, frmCadCliente.cboUFEnd01)
        If Fl_Invalida = "S" Then
            MsgBox "Inscri��o Estadual Inv�lida!", vbCritical, "CAD060 - Aten��o"
            txtIEEnd01.SetFocus
            Exit Sub
        End If
    End If
    
    '-----------------------------------------------------------------------------------------------------------
    'NOVAS VALIDA��ES QUANDO FOR ENDERE�O TIPO 1 - ENTREGA
    'A PARTIR DO NOVO CONCEITO DE VENDA COM ENTREGA EM LOCAL DIVERSO - EM 01/09/05 - ALETHEA/MARICI/NATALIA
    '-----------------------------------------------------------------------------------------------------------
    If Val(Mid(cboTpEnd01.Text, 1, 1)) = 1 Then
    
        'O Cliente para o qual esta sendo inserido o registro deve possuir IE.
        'Retirada esta consistencia em 04/10/05 conf. solic. Fiscal - Alethea
        'If Trim(txtInscrEstadual) = "" Then
        '    MsgBox "Este cliente deve possuir Inscri��o Estadual.", vbInformation, "Aten��o"
        '    Exit Sub
        'End If
    
        '18/10/06 - Alethea - Altera��o com solicita��o de servi�os
        'Conforme solicita��o do Fiscal / O&M, foi colocada novamente a verifica��o para cliente com IE Isento
        'Somente cliente do tipo "63 - Seguradora" - que n�o tiver IE - poder� receber End.Entrega
        'Incluido cliente do tipo "78 - Locadora de Ve�culos" - que n�o tiver IE - poder� receber End.Entrega - Conf.solic.Nat�lia O&M/M�nica Fiscal - Alethea - 26/03/07
        'Incluido cliente do tipo "77 - Leve" - que n�o tiver IE - poder� receber End.Entrega - Conf.solic.Nat�lia O&M/M�nica Fiscal - Alethea - 03/08/07
        
        'oliveira@ - cit - 22/11/12 - validar se o segmento � diferente de "BA" e "BC".
        Dim codTpSegmento As String
        If cboTpSegmento <> "" Then
            codTpSegmento = Split(cboTpSegmento, " - ")(1)
        End If
        
        'TI-5408
        'If (Val(txtInscrEstadual) = 0 Or Trim(txtInscrEstadual) = "ISENTO") And _
        '    codTpSegmento <> "BA" And _
        '    codTpSegmento <> "BC" Then
        '    MsgBox "Este cliente deve possuir Inscri��o Estadual.", vbInformation, "Aten��o"
        '    Exit Sub
        'End If
    
    
        'UF DO ENDERE�O DE ENTREGA DEVE SER IGUAL A UF DO CLIENTE ORIGINAL
        If (Trim(cboUFEnd01.Text) <> Trim(cboUF.Text)) Then
            MsgBox "UF do Endere�o de Entrega deve ser a mesma UF do Cliente!!!", vbCritical, "CAD060 - Aten��o"
            cboUFEnd01.SetFocus
            Exit Sub
        End If
        'CGC do Endereco de Entrega tem que ser diferente do CGC do Cliente
        If Trim(txtCGCEnd01) = Trim(txtDocto) Then
            MsgBox "CGC do Endere�o de Entrega n�o pode ser igual ao CGC do Cliente.", vbInformation, "Aten��o"
            txtCGCEnd01.SetFocus
            Exit Sub
        End If
    
        'TODOS OS CAMPOS DO ENDERE�O DE ENTREGA DEVEM SER OBRIGATORIOS, COM EXCE�AO DA CAIXA POSTAL
        'CGC/CIC
        If Trim(txtCGCEnd01.Text) = "" Then
            MsgBox "CGC/CIC Inv�lido!!!", vbCritical, "CAD060 - Aten��o"
            txtCGCEnd01.SetFocus
            Exit Sub
        End If
        'NOME CLIENTE
        If Trim(txtNomeEnd01.Text) = "" Then
            MsgBox "Nome do Cliente Inv�lido!!!", vbCritical, "CAD060 - Aten��o"
            txtNomeEnd01.SetFocus
            Exit Sub
        End If
        'DDD
        'If Trim(txtDDDEnd01.Text) = "" Then
        '    MsgBox "DDD Inv�lido!!!", vbCritical, "CAD060 - Aten��o"
        '    txtDDDEnd01.SetFocus
        '    Exit Sub
        'End If
        'FONE
        'If Trim(txtFoneEnd01.Text) = "" Then
        '    MsgBox "Fone Inv�lido!!!", vbCritical, "CAD060 - Aten��o"
        '    txtFoneEnd01.SetFocus
        '    Exit Sub
        'End If
    
    End If
    '-----------------------------------------------------------------------------------------------------------
    
    'Se passou em todas as valida��es, acerta bot�es
    Status_Botoes_Out_Enderecos "GRAVAR"
    
    'Inclui o item no listview
    If vAcao = "Adicionar" Then
        Set litem = lsvOutEnd.ListItems.Add(1)
    Else
        For I = 1 To lsvOutEnd.ListItems.Count
            If lsvOutEnd.ListItems(I) = txtSeqEnd01 Then
                Set litem = lsvOutEnd.ListItems(I)
                Exit For
            End If
        Next I
    End If
    
    
    litem = txtSeqEnd01
    litem.SubItems(1) = cboTpEnd01.Text
    litem.SubItems(2) = cboTpDoctoEnd01.Text
    litem.SubItems(3) = txtCGCEnd01.Text
    litem.SubItems(4) = txtIEEnd01.Text
    litem.SubItems(5) = txtNomeEnd01.Text
    
    'Kevlin Toshinari Ossada - CIT - 22/8/2012 - Adicionado 2 novas colunas no grid (Num. e Complemento)
    litem.SubItems(6) = txtEnd01.Text
    litem.SubItems(7) = txtNumEnd01.Text
    litem.SubItems(8) = txtComplEnd01.Text
    
    litem.SubItems(9) = txtBairroEnd01.Text
    'vxavier CIT 11/12/2012 - Defect #360 - CAD060 - Ceps com zeros a esquerda s�o salvos sem o zero
    litem.SubItems(10) = IIf(txtCepEnd01.Text = "", txtCepEnd01.Text, Format(txtCepEnd01.Text, "00000000"))
    litem.SubItems(11) = cboUFEnd01.Text
    litem.SubItems(12) = cboCidadeEnd01.Text
    litem.SubItems(13) = txtDDDEnd01.Text
    litem.SubItems(14) = txtFoneEnd01.Text
    litem.SubItems(15) = txtFaxEnd01.Text
    litem.SubItems(16) = txtCxPosEnd01.Text
    
    
    
     ' carlosj - Ci&T - 27/08/12 - [ISA-330] - Salvar dados (bancarios) do cliente no SAP
      Dim cli As ClienteSAP
      Dim msgErro As String
      Set cli = BuscarClienteSAP(Val(cboTpDocto), txtDocto, msgErro)
      
      If msgErro <> "" Then
        MsgBox msgErro
        Exit Sub
      End If
      
      'dbaldo - Cit
      txtSAP.Text = cli.NoClienteSAP
      If Val(txtSAP.Text) = 0 Then
        MsgBox "Esse cliente n�o foi enviado ao SAP", vbCritical, "CAD060 - Aten��o"
        Exit Sub
      End If
      
      'popula o vetor de endere�os no bjeto cliente
      Call PopularObjetoClienteEnderecos(cli)
      
      If EnviarClienteSAP(cli) Then
        ' Se funcionou o envio do cliente pro SAP ent�o persistimos no legado
        'Eduardo - 30/08/05
         If Val(cboTpEnd01) = 1 Then
             Atualizar_OnLine
         Else
             'Gravar no banco
             pGrava_Out_Enderecos
         End If
      Else
          MsgBox "Erro ao gravar o cliente no SAP. Cadastro Abortado.", vbCritical, "CAD060 - Aten��o"
      End If
  
    Status_Controles_Out_Enderecos False
    vFl_Adic_OutEnd = False
    Limpar_Campos_OutEnd False

    sstCadastro_Click (4)

End Sub

Private Sub cmdGrRefBco_Click()
    Dim litem As ListItem
    
    lblMsgBco.Visible = False
    
    'Relvas Durante teste das alteracoes do Muniz
    If Not ValidaBranco(Me.txtBanco) Then
        MsgBox "Existe mais de um espa�o em branco no campo Banco.", vbCritical, "CAD060 - Aten��o"
        txtBanco.SetFocus
        Exit Sub
    End If
    
    'Muniz 10/02/10 valida espa�os em branco no campo contato
    If Not ValidaBranco(txtObs) Then
        MsgBox "Existe mais de um espa�o em branco no campo Observa��o.", vbCritical, "CAD060 - Aten��o"
        txtObs.SetFocus
        Exit Sub
    End If
    'fim Muniz
    
    
    If Left(txtSituacao.Text, 1) = "9" Then
        MsgBox "Cliente Desativado. N�o posso Alterar Dados !!!", vbCritical, "CAD060 - Aten��o"
        Exit Sub
    End If
    
    'N�o validava nenhum campo pois v� que existem registros no banco de dados, cujos campos est�o todos em branco, exceto a sequ�ncia
    'Ent�o vou validar para que n�o fique registros sem nenhuma informa��o
    If Trim(txtBanco) = "" Then
        MsgBox "Digite o banco.", vbInformation, "CAD060 - Aten��o"
        txtBanco.SetFocus
        Exit Sub
    End If
    
    Status_Botoes_Ref_Bancarias "GRAVAR"
    
    'Inclui item no listview
    If vAcao = "Adicionar" Then
        Set litem = lsvRefBanco.ListItems.Add(1)
    Else
        For I = 1 To lsvRefBanco.ListItems.Count
            If lsvRefBanco.ListItems(I) = txtSeq Then
                Set litem = lsvRefBanco.ListItems(I)
                Exit For
            End If
        Next I
    End If
    
    litem = txtSeq
    litem.SubItems(1) = txtBanco
    litem.SubItems(2) = txtDt
    litem.SubItems(3) = txtAgencia
    litem.SubItems(4) = txtDDD
    litem.SubItems(5) = txtFone
    litem.SubItems(6) = txtConceito
    litem.SubItems(7) = txtObs

    'Gravar no banco
    pGrava_Ref_Bancarias
    
    Status_Controles_Ref_Bancarias False
    Limpar_Campos_RefBco False
    
End Sub

Private Sub cmdGrRefComl_Click()
    Dim litem As ListItem
    
    lblMsgCom.Visible = False
    
    'Relvas - Durante teste das alteracoes do Muniz
    If Not ValidaBranco(Me.txtEmp01) Then
        MsgBox "Existe mais de um espa�o em branco no campo Empresa.", vbCritical, "CAD060 - Aten��o"
        txtEmp01.SetFocus
        Exit Sub
    End If
    
    'Muniz 10/02/10 valida espa�os em branco no campo contato
    If Not ValidaBranco(txtObs01) Then
        MsgBox "Existe mais de um espa�o em branco no campo Observa��o.", vbCritical, "CAD060 - Aten��o"
        txtObs01.SetFocus
        Exit Sub
    End If
    'fim Muniz
    
    
    If Left(txtSituacao.Text, 1) = "9" Then
        MsgBox "Cliente Desativado. N�o posso Alterar Dados !!!", vbCritical, "CAD060 - Aten��o"
        Exit Sub
    End If
    
    'Valida Empresa
    If Trim(txtEmp01) = "" Then
        MsgBox "Digite a Empresa.", vbInformation, "CAD060 - Aten��o"
        txtEmp01.SetFocus
        Exit Sub
    End If
    
    Status_Botoes_Ref_Comerciais "GRAVAR"
    
    'Inclui o item no listview
    If vAcao = "Adicionar" Then
        Set litem = lsvRefComl.ListItems.Add(1)
    Else
        For I = 1 To lsvRefComl.ListItems.Count
            If lsvRefComl.ListItems(I) = txtSeq01 Then
                Set litem = lsvRefComl.ListItems(I)
                Exit For
            End If
        Next I
    End If
    
    
    litem = txtSeq01
    litem.SubItems(1) = txtEmp01
    litem.SubItems(2) = txtDtCons01
    litem.SubItems(3) = txtDDD01
    litem.SubItems(4) = txtFone01
    litem.SubItems(5) = txtVlUltFat01
    litem.SubItems(6) = txtDtUltFat01
    litem.SubItems(7) = txtMaiSlDev01
    litem.SubItems(8) = txtDtCad01
    litem.SubItems(9) = txtAtraso01
    litem.SubItems(10) = txtConceito01
    litem.SubItems(11) = txtObs01

    'Gravar no banco
    pGrava_Ref_Comerciais
    
    Status_Controles_Ref_Comerciais False
    
    Limpar_Campos_RefComl False

End Sub


'Private Sub cmdGrVendor_Click()
'    Dim litem As ListItem
'
'    If Trim(txtCodCliente.Text) = "" Then
'        MsgBox "Por favor, escolha um cliente ou inclua-o, antes de gravar dados de Vendor", vbCritical, "CAD060 - Aten��o"
'        Exit Sub
'    End If
'
'    Set litem = lsvVendor.ListItems.Add
'    litem = txtContratoVendor.Text
'    litem.SubItems(1) = txtDtContratoVendor.Text
'    litem.SubItems(2) = txtVlLimiteVendor.Text
'    litem.SubItems(3) = cboSitVendor.Text
'
'    'Gravar no banco
'
'End Sub

Private Sub cmdLimpar_Click()
    txtSeq.Text = ""
    txtBanco.Text = ""
    txtDt.Text = ""
    txtAgencia.Text = ""
    txtDDD.Text = ""
    txtFone.Text = ""
    txtConceito.Text = ""
    txtObs.Text = ""
End Sub




Private Sub cmdLinVdr_Click()
Dim Y As Integer, I As Integer
    
    grdflexLinVdr.Visible = True
    
    'Ao inv�s de trazer todas as linhas vdr cadastradas na linha_produto, vou trazer as linhas para este tipo de cliente cadastradas na controle_vdr
    Set OraParameters = db.Parameters
    '#Hemerson Milano - SMS 1789 - 02/12/2014
    'OraParameters.Remove "TPCLI":    OraParameters.Add "TPCLI", Val(Mid(cboTpCliente, (InStr(1, cboTpCliente, " - ") + 3))), 1
    OraParameters.Remove "TPCLI":    OraParameters.Add "TPCLI", Mid(cboTpCliente, (InStr(1, cboTpCliente, " - ") + 3)), 1
    OraParameters.Remove "TP_CONS":  OraParameters.Add "TP_CONS", 1, 1
    OraParameters.Remove "vErro":    OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    Criar_Cursor OraParameters, "PM_CURSOR1"
    vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD060.Pr_Select_ContVDR(:PM_CURSOR1, :TPCLI, :TP_CONS, :vErro, :vMsgErro)")
    If vErro = "" Then
        Set db_CONS = OraParameters("PM_CURSOR1").Value
    Else
        Exit Sub
    End If
    
    If db_CONS.EOF Then
        MsgBox "Nenhuma Linha Encontrada para este tipo de cliente na Controle_Vdr - Entre em contato com Depto Compras !", vbInformation, "CAD060 - Aten��o"
        Exit Sub
    Else
        
        With grdflexLinVdr
            .Row = 0
            .Col = 0
            .ColWidth(0) = 1000: .ColAlignment(0) = 4: .TextMatrix(0, 0) = "C�d."
            .ColWidth(1) = 2000: .ColAlignment(1) = 1: .TextMatrix(0, 1) = "Linha"
            .ColWidth(2) = 1300: .ColAlignment(2) = 4: .TextMatrix(0, 2) = "Gravar Linha"
            'Limpa Grid
            .Row = 1
            For I = 0 To .Cols - 1
                .Col = I:   .Text = ""
            Next I
        End With
    
       I = 0
       On Error Resume Next
       While Not db_CONS.EOF    'Carrega Grid
            I = I + 1
            With grdflexLinVdr
                .Rows = I + 1
                .Row = I
                .TextMatrix(I, 0) = db_CONS("linha_produto")
                .TextMatrix(I, 1) = db_CONS("desc_linha")
            End With
            db_CONS.MoveNext
       Wend
    End If
    
    If Fl_Cliente_Novo = "N" Then    'N�o � Cliente Novo
      
        Set OraParameters = db.Parameters
        OraParameters.Remove "CODCLI": OraParameters.Add "CODCLI", Val(txtCodCliente.Text), 1
        OraParameters.Remove "TP_CONS": OraParameters.Add "TP_CONS", 1, 1
        OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
        OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
        Criar_Cursor OraParameters, "PM_CURSOR1"
        vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD060.Pr_Linha_Produto(:PM_CURSOR1, :CODCLI, :TP_CONS, :vErro, :vMsgErro)")
        If vErro = "" Then
            Set rst = OraParameters("PM_CURSOR1").Value
        Else
            Exit Sub
        End If
        
        'Pego cada linha de produto do cliente e vou procurar se existe no grid que j� foi montado com todas as possiveis linhas para este tipo de cliente
        While Not rst.EOF
            With grdflexLinVdr
                For Y = 1 To .Rows - 1
                    .Row = Y
                    If .TextMatrix(Y, 0) = rst!linha.Value Then
                        .TextMatrix(Y, 2) = "X"
                        Y = .Rows
                    End If
                Next Y
            End With
            rst.MoveNext
        Wend
    End If

End Sub

Private Sub cmdEdOutEnd_Click()
    vAcao = "Editar"
    
    lblMsgEnd.Visible = False
    If txtSeqEnd01 = "" Then
        MsgBox "Selecione um item da lista.", vbInformation, "CAD060 - Aten��o"
        Exit Sub
    End If
    Status_Botoes_Out_Enderecos "ADICIONAR"
    Status_Controles_Out_Enderecos True
    
End Sub


'Private Sub cmdLiVendor_Click()
'    txtContratoVendor.Text = ""
'    txtDtContratoVendor.Text = ""
'    txtVlLimiteVendor.Text = ""
'    cboSitVendor.Text = ""
'End Sub

Private Sub cmdRecusar_Click()

    frmMotivoRecusa.Show vbModal

    Call cmdCancelar_Click


End Sub

Private Sub cmdRepres_Click()

Call Limpa_FlexGrid(grdflexRepres)

'// ----------------------------------
'Carrega dados da Tabela R_CLIE_REPRES para mostrar todos os representantes para os quais o cliente est� gravado
Sql = " Select r.cod_repres, pseudonimo, e.cod_filial, nome_filial " & _
        " from r_clie_repres r, representante e , filial f " & _
       " where r.cod_Cliente = :CODCLI " & _
         " and r.cod_repres = e.cod_repres " & _
         " and e.cod_Filial = f.cod_filial " & _
    " order by r.cod_repres "
           
db.Parameters.Remove "CODCLI": db.Parameters.Add "CODCLI", txtCodCliente.Text, 1
Set db_CONS = db.dbcreatedynaset(Sql, 0&)
    
I = 1
If db_CONS.EOF Then
   MsgBox "N�o existem representantes cadastrados para este cliente!", vbInformation, "CAD060 - Aten��o"
    Exit Sub
End If
    
With grdflexRepres

    'Monta cabe�alho do grid
    .Rows = 2
    .Cols = 4
    .ColWidth(0) = 1200: .ColAlignment(0) = 0: .TextMatrix(0, 0) = "Cod.Repres"
    .ColWidth(1) = 3000: .ColAlignment(1) = 0: .TextMatrix(0, 1) = "Pseudonimo"
    .ColWidth(2) = 1200: .ColAlignment(2) = 0: .TextMatrix(0, 2) = "C�d.Filial"
    .ColWidth(3) = 3000: .ColAlignment(3) = 0: .TextMatrix(0, 3) = "Filial"

    Do While Not db_CONS.EOF
        .Rows = db_CONS.RecordCount + 1
        .Row = I
        .TextMatrix(I, 0) = db_CONS.Fields("cod_repres").Value
        .TextMatrix(I, 1) = db_CONS.Fields("pseudonimo").Value
        .TextMatrix(I, 2) = db_CONS.Fields("cod_filial").Value
        .TextMatrix(I, 3) = db_CONS.Fields("nome_filial").Value
        I = I + 1
        db_CONS.MoveNext
    Loop
End With

grdflexRepres.Visible = True

End Sub

'Private Sub cmdRetTela_Click()
'    SSPanel1.Visible = False
'End Sub

Private Sub cmdSCI_Click()
    If Me.txtCodCliente = "" Then
        MsgBox "Selecione um cliente.", vbInformation, "Aten��o"
        Exit Sub
    End If
    frmSerasa.Show vbModal
End Sub

Private Sub cmdVoltar_Click()
    
    'carlosj CiT (26/11/2012) -ISA 1033
    'limpa variavel global
    vTipoDocto = ""
    
    If vFl_Analise = True Then
        vFl_Volta_Analise = True
        
    'Para consulta um cadastro j� aceito da web, precisei colocar false na vfl_analise
    '(para conseguir habilitar os campos de refs.) e agora, para voltar na tela de consulta,
    'preciso voltar a flag na situa��o correta
    ElseIf vFl_Analise = False And vFl_Consulta = True Then
        vFl_Analise = True
        vFl_Volta_Analise = True
    
    Else
        vFl_Volta_Analise = False
    End If
    Unload Me
End Sub


Private Sub Form_Load()
    Dim I As Long
    Dim vAuxRC As Byte, vAuxRB As Byte
    
    vTipoDocto = ""
    
    Screen.MousePointer = vbHourglass
    
    'Preenchendo Combo Diferenciado - Jairo Almeida - SDS1889 - 22-02-2012
    cboDiferenciado.AddItem "SIM"
    cboDiferenciado.AddItem "N�O"
        
    'Ocultar a barra de botoes do MDI
    mdiCAD060.SSPanel1.Visible = False
    
    Me.Top = mdiCAD060.Menu.Height * 2
    Me.Left = 50
    
    'Obs.: Vendor e Cliente Blau passam a ser inutilizados a partir de Maio/2004 - Aleth�a
    'Limpar_Campos_Vendor
    'Call Carrega_Combo_Tipo_ClienteBlau(cboTpClienteBlau)
    'Call Carrega_Combo_Repres("B", cboRepresBlau)            'Par�metros: D=DPK / B=BLAU
    'Call Carrega_Combo_ClieVendor(Me.cboSitVendor, 0)
    
    Limpar_Campos_Cadastro (True)
    Limpar_Campos_RefBco (True)
    Limpar_Campos_RefComl (True)
    Limpar_Campos_OutEnd (True)

    'OBS.:
    'Tirei toda a parte que fazia select distinct na tabela cliente, para pegar as op��es
    'das combo, para que ficasse mais r�pida a entrada dos dados no sistema. 29/06/04
    
    
    'Carrega todas as combos da tela
    'Call Carrega_Combos_Genericas("TPDOC", cboTpDocto)
    'Call Carrega_Combos_Genericas("STATUS", cboStatus)
    'Call Carrega_Combos_Genericas("TPOP", cboTpOP)
    'Call Carrega_Combos_Genericas("CHEQUE", cboCheque)
    'Call Carrega_Combos_Genericas("PROTESTO", cboProtesto)
    'Call Carrega_Combos_Genericas("CLASSIFICACAO", cboClassificacao)
    Call Carrega_Combo_Caracteristica(cboCaracteristica)
    Call Carrega_Combo_UF(Me.cboUF)
    Call Carrega_Combo_Cidade(cboUF.Text, cboCidade)
    'SDS 2557 - Marco Aurelio Andrade Jr - Montando combo do CNAE
    Call Carrega_Combo_Cnae(cboCNAE.Text, cboCNAE)
    'Call Carrega_Combo_Segmento_ClienteDPK(cboTpSegmento, Left(cboTpDocto.Text, 1))
    'Call Carrega_Combo_Tipo_ClienteDPK(cboTpCliente)
    Call Carrega_Combo_Repres("D", cboRepresDPK)             'Par�metros: D=DPK / B=BLAU
    Call Carrega_Combo_Banco(cboBanco)
    Call Carrega_Combo_Transportadora(cboTransportadora)
    Call Carrega_Combo_Mensagem("C", cboMsgCliente)          'Par�metros: C=Cliente / F=Fiscal
    Call Carrega_Combo_Mensagem("F", cboMsgFiscal)           'Par�metros: C=Cliente / F=Fiscal
    Call Carrega_Combo_Forn(cboFornVDR) 'TI-3221
    Call Carrega_Combo_CategBosch(cboCategBosch)
    'Call Carrega_Combo_ClieEndereco(cboTpEnd01, 0)
    'Call Carrega_Combo_ClieEndereco(cboTpDoctoEnd01, 1)
    Call Carrega_Combo_UF(cboUFEnd01)

    cboBanco = "BRASIL - 001"
    cboTpOP = "  - ANTES"
    cboCheque = "0 - SEM CHEQUE S/FUNDO"
    cboStatus = "0 - NORMAL"
    cboClassificacao = "X"
    cboCaracteristica = "N - NORMAL"
    cboTransportadora = ". - 0999"
    'cboTpEmpresa = "E - EMPRESA"

    fraCategBosch.Visible = False
   
    sstCadastro.Tab = 0
   
    'Verifica se a chamada do formul�rio de cadastro, veio atrav�s do duplo clique da tela de an�lise ou n�o
    'A chamada tem que vir da tela de an�lise mas n�o pode ser consulta de pr�-cadastro
    If vFl_Analise = True And vFl_Consulta = False Then
        
        Habilitar_Objs_Analise
        
        If vFl_Tipo_Solicitacao = 0 Then
            
            frmCadCliente.Caption = "CAD060 - An�lise de Cliente                                       PR�-CADASTRO"
            Call Carregar_Dados_Pre_Cadastro(vCGC_Analisado, vFl_Tipo_Solicitacao)
            sstCadastro.Tab = 2 'Carregar referencias comerciais
            Call sstCadastro_Click(2)
            sstCadastro.Tab = 3 'Carregar referencias banc�rias
            Call sstCadastro_Click(3)
            sstCadastro.Tab = 4 'Carregar outros endere�os
            Call sstCadastro_Click(4)
            sstCadastro.Tab = 0 'Voltar para a tela de cadastro geral
            Call sstCadastro_Click(0)
            
            'Verifica se existem Refer�ncias Banc./Coml., se n�o existir, alterar mensagem do cliente para 43 - Compra a Vista
            'Ref.Coml.
            vAuxRC = 0
            For I = 1 To lsvRefComl.ListItems.Count
                If vAuxRC = 0 And Trim(lsvRefComl.ListItems(I)) <> "" Then
                    vAuxRC = 1
                End If
            Next I
            'Ref.Banc�rias.
            vAuxRB = 0
            For I = 1 To lsvRefBanco.ListItems.Count
                If vAuxRB = 0 And Trim(lsvRefBanco.ListItems(I)) <> "" Then
                    vAuxRB = 1
                End If
            Next I
            If vAuxRC = 0 And vAuxRB = 0 Then
                'Trazer na combo de mensagem de cliente, o c�digo de mensagem = 43
                Set OraParameters = db.Parameters
                OraParameters.Remove "CODMSG": OraParameters.Add "CODMSG", Val(43), 1
                OraParameters.Remove "TP_CONS": OraParameters.Add "TP_CONS", 0, 1
                OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
                OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
                Criar_Cursor OraParameters, "PM_CURSOR1"
                vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD060.Pr_Select_Mensagem(:PM_CURSOR1, :CODMSG, :TP_CONS, :vErro, :vMsgErro)")
                If vErro = "" Then
                    Set db_CONS = OraParameters("PM_CURSOR1").Value
                Else
                    Exit Sub
                End If
                If db_CONS.EOF Then
                    MsgBox "Problemas ao consultar C�digo de Mensagem 43 - Compra a Vista." & Chr(13) & _
                           "Favor indicar a mensagem para este novo cliente e avisar Depto.Suporte!", vbInformation, "CAD060 - Aten��o"
                Else
                    cboMsgCliente.Clear
                    cboMsgCliente.AddItem db_CONS!Desc_Mens.Value & " - " & Format(db_CONS!Cod_Mensagem.Value, "00")
                    cboMsgCliente.ListIndex = 0
                End If
            End If
            
        ElseIf vFl_Tipo_Solicitacao = 1 Then
            frmCadCliente.Caption = "CAD060 - An�lise de Cliente                                       PR�-ALTERA��O"
        ElseIf vFl_Tipo_Solicitacao = 2 Then
            frmCadCliente.Caption = "CAD060 - An�lise de Cliente                                       PR�-RECADASTRO"
        End If
    
    'A chamada tem que vir da tela de an�lise e tem que ser consulta de pr�-cadastro
    ElseIf vFl_Analise = True And vFl_Consulta = True Then
        vFl_Analise = False
        Habilitar_Objs_Cadastro
        frmCadCliente.Caption = "CAD060 - Cadastro de Cliente"
        cboCliente = vCGC_Analisado
        Call cboCliente_LostFocus
    Else
        vFl_Analise = False
        Habilitar_Objs_Cadastro
        frmCadCliente.Caption = "CAD060 - Cadastro de Cliente"
    End If
    
    txtDtCadastro.Locked = False
    grdflexRepres.Visible = False
    vCod_GrupoMaster = 0

    Screen.MousePointer = 0
    
    mdiCAD060.stbBarra.Panels(1) = ""
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    'Ocultar a barra de botoes do MDI
    mdiCAD060.SSPanel1.Visible = True
End Sub

Private Sub grdflexLinVdr_DblClick()
    With grdflexLinVdr
        .Col = 2
        If .TextMatrix(.Row, .Col) = "" Then
            .Text = "X"
        Else
            .Text = ""
        End If
    End With
End Sub


Private Sub Image1_Click()

End Sub

Private Sub imgLOG_CORREIOS_Click()
    cmdCEP_CORREIOS_Click
End Sub



Private Sub lsvOutEnd_ItemClick(ByVal Item As MSComctlLib.ListItem)
    txtSeqEnd01 = lsvOutEnd.SelectedItem
    cboTpEnd01.Text = lsvOutEnd.SelectedItem.SubItems(1)
    
    'Eduardo - 31/08/2005
    'Se o tipo de endereco for igual a 1 - Travar o campo cboTpEnd01
    If Val(cboTpEnd01) = 1 Then
        cboTpEnd01.Enabled = False
    Else
        cboTpEnd01.Enabled = True
    End If
    
    cboTpDoctoEnd01.Text = IIf(Trim(lsvOutEnd.SelectedItem.SubItems(2)) = "", "1 - PESSOA JURIDICA", lsvOutEnd.SelectedItem.SubItems(2))
    'cboTpDoctoEnd01.Text = lsvOutEnd.SelectedItem.SubItems(2)
    txtCGCEnd01.Text = lsvOutEnd.SelectedItem.SubItems(3)
    txtIEEnd01.Text = lsvOutEnd.SelectedItem.SubItems(4)
    txtNomeEnd01.Text = lsvOutEnd.SelectedItem.SubItems(5)
    txtEnd01.Text = lsvOutEnd.SelectedItem.SubItems(6)
    
    'Kevlin Toshinari Ossada - CIT - 22/8/2012 - Inicializa valores do campo Num. e Endere�o
    txtNumEnd01.Text = lsvOutEnd.SelectedItem.SubItems(7)
    txtComplEnd01.Text = lsvOutEnd.SelectedItem.SubItems(8)
    
    txtBairroEnd01.Text = lsvOutEnd.SelectedItem.SubItems(9)
    'vxavier CIT 11/12/2012 - Defect #360 - CAD060 - Ceps com zeros a esquerda s�o salvos sem o zero
    txtCepEnd01.Text = IIf(lsvOutEnd.SelectedItem.SubItems(10) = "", lsvOutEnd.SelectedItem.SubItems(10), Format(lsvOutEnd.SelectedItem.SubItems(10), "00000000"))
    cboUFEnd01.Text = lsvOutEnd.SelectedItem.SubItems(11)
    Call cboUFEnd01_Click
    
  '  cboCidadeEnd01.Text = Mid(lsvOutEnd.SelectedItem.SubItems(10), 1, InStr(1, lsvOutEnd.SelectedItem.SubItems(10), " -") - 1) & _
                     " - " & Format(Mid(lsvOutEnd.SelectedItem.SubItems(10), InStr(lsvOutEnd.SelectedItem.SubItems(10), " - ") + 3), "0000")
    
    'Kevlin Toshinari Ossada - CIT - 22/8/2012 - Foi alterado o numero da coluna referente a cod da cidade
    cboCidadeEnd01.Text = Trim(Mid(lsvOutEnd.SelectedItem.SubItems(12), 1, InStr(1, lsvOutEnd.SelectedItem.SubItems(12), " -") - 1)) & _
                     " - " & Format(Mid(lsvOutEnd.SelectedItem.SubItems(12), InStr(lsvOutEnd.SelectedItem.SubItems(12), " - ") + 3), "0000")

    
    
    'cboCidadeEnd01 = lsvOutEnd.SelectedItem.SubItems(12)
    txtDDDEnd01.Text = lsvOutEnd.SelectedItem.SubItems(13)
    txtFoneEnd01.Text = lsvOutEnd.SelectedItem.SubItems(14)
    txtFaxEnd01.Text = lsvOutEnd.SelectedItem.SubItems(15)
    txtCxPosEnd01.Text = lsvOutEnd.SelectedItem.SubItems(16)

    Status_Botoes_Out_Enderecos "SELECIONAR"

End Sub


Private Sub lsvRefBanco_ItemClick(ByVal Item As MSComctlLib.ListItem)
    txtSeq = lsvRefBanco.SelectedItem
    txtBanco = lsvRefBanco.SelectedItem.SubItems(1)
    txtDt = lsvRefBanco.SelectedItem.SubItems(2)
    txtAgencia = lsvRefBanco.SelectedItem.SubItems(3)
    txtDDD = lsvRefBanco.SelectedItem.SubItems(4)
    txtFone = lsvRefBanco.SelectedItem.SubItems(5)
    txtConceito = lsvRefBanco.SelectedItem.SubItems(6)
    txtObs = lsvRefBanco.SelectedItem.SubItems(7)

    Status_Botoes_Ref_Bancarias "SELECIONAR"

End Sub


Private Sub lsvRefComl_ItemClick(ByVal Item As MSComctlLib.ListItem)
    txtSeq01.Text = lsvRefComl.SelectedItem
    txtEmp01.Text = lsvRefComl.SelectedItem.SubItems(1)
    txtDtCons01.Text = lsvRefComl.SelectedItem.SubItems(2)
    txtDDD01.Text = lsvRefComl.SelectedItem.SubItems(3)
    txtFone01.Text = lsvRefComl.SelectedItem.SubItems(4)
    txtVlUltFat01.Text = lsvRefComl.SelectedItem.SubItems(5)
    txtDtUltFat01.Text = lsvRefComl.SelectedItem.SubItems(6)
    txtMaiSlDev01.Text = lsvRefComl.SelectedItem.SubItems(7)
    txtDtCad01.Text = lsvRefComl.SelectedItem.SubItems(8)
    txtAtraso01.Text = lsvRefComl.SelectedItem.SubItems(9)
    txtConceito01.Text = lsvRefComl.SelectedItem.SubItems(10)
    txtObs01.Text = lsvRefComl.SelectedItem.SubItems(11)
    
    Status_Botoes_Ref_Comerciais "SELECIONAR"
    
End Sub


'Private Sub lsvVendor_ItemClick(ByVal Item As MSComctlLib.ListItem)
'    txtContratoVendor.Text = lsvVendor.SelectedItem
'    txtDtContratoVendor.Text = lsvVendor.SelectedItem.SubItems(1)
'    txtVlLimiteVendor.Text = lsvVendor.SelectedItem.SubItems(2)
'    cboSitVendor.Text = lsvVendor.SelectedItem.SubItems(3)
'End Sub


Private Sub sstCadastro_Click(PreviousTab As Integer)
    If sstCadastro.Tab <> 0 And sstCadastro.Tab <> 1 Then
        'Se estiver analisando, nome do cliente n�o pode estar em branco
        'Se estiver cadastrando normalmente, c�d.cliente n�o pode estar em branco
        If IIf(vFl_Analise = True, Trim(cboCliente.Text), Trim(txtCodCliente.Text)) = "" Then
            MsgBox "Por favor, escolha um cliente ou inclua-o, antes de gravar as Refer�ncias", vbCritical, "CAD060 - Aten��o"
            sstCadastro.Tab = 0
            Exit Sub
        End If
    End If
    
    lblMsgCom.Visible = False
    lblMsgBco.Visible = False
    lblMsgEnd.Visible = False
    
    If vFl_Analise = True Then
        Status_Botoes_Ref_Bancarias ""
        Status_Botoes_Ref_Comerciais ""
        Status_Botoes_Out_Enderecos ""
        Status_Controles_Ref_Bancarias False
        Status_Controles_Ref_Comerciais False
        Status_Controles_Out_Enderecos False
    Else
        Status_Botoes_Ref_Bancarias "ABRIR"
        Status_Botoes_Ref_Comerciais "ABRIR"
        Status_Botoes_Out_Enderecos "ABRIR"
        Status_Controles_Ref_Bancarias False
        Status_Controles_Ref_Comerciais False
        Status_Controles_Out_Enderecos False
    End If
    
    
    If sstCadastro.Tab = 2 Or sstCadastro.Tab = 3 Or sstCadastro.Tab = 4 Then
        sspCadCli.Caption = "Carregando informa��es adicionais. Por favor, aguarde..."
    Else
        sspCadCli.Caption = ""
    End If
    
    'Pasta Representantes = Verifica as linhas VDR gravadas para o cliente
    If sstCadastro.Tab = 1 Then
        If Fl_Cliente_Novo = "N" And Trim(cboCategBosch.Text) <> "" Then
            fraCategBosch.Visible = True
            Call cmdLinVdr_Click
        End If
    
    End If
    
    'Referencias Comerciais
    If sstCadastro.Tab = 2 Then
        
        '--------------------------------
        'REFER�NCIAS COMERCIAIS
        '--------------------------------
        'Se for an�lise, deve-se selecionar dados da tabela CLIENTE_WEB, sen�o da CLIENTE e CLIE_REFER
        If vFl_Analise = False Then  'Tabela CLIENTE
            Set OraParameters = db.Parameters
        Else    'Tabela Cliente_Web
            Set OraParameters = db_INT.Parameters
        End If
        OraParameters.Remove "CODCLICGC": OraParameters.Add "CODCLICGC", Val(txtDocto.Text), 1
        OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
        OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
        Criar_Cursor OraParameters, "cRefComl"
        If vFl_Analise = False Then  'Tabela CLIENTE
            'vVB_Generica_001.Retorna_Cursor db, "cRefComl", "PRODUCAO.PCK_CAD060.PR_SELECT_REF_COML_P", "CODCLICGC", "vErro", "vMsgErro"
            'vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD0602.PR_SELECT_REF_COML_P(:cRefComl, :CODCLICGC, :vErro, :vMsgErro)")
            vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD060.PR_SELECT_REF_COML_P(:cRefComl, :CODCLICGC, :vErro, :vMsgErro)")
        Else    'Tabela Cliente_Web
            'vVB_Generica_001.Retorna_Cursor db_INT, "cRefComl", "INTRANET.PCK_CLIENTES_TESTE.PR_SELECT_REF_COML", "CODCLICGC", "vErro", "vMsgErro"
            vErro = vVB_Generica_001.ExecutaPl(db_INT, "INTRANET.PCK_CLIENTES_TESTE.PR_SELECT_REF_COML(:cRefComl, :CODCLICGC, :vErro, :vMsgErro)")
        End If
        If vErro = "" Then
            Set rst = OraParameters("cRefComl").Value
        Else
            Exit Sub
        End If
            
        
        Limpar_Campos_RefComl (True)
                
        If vFl_Analise = False Then
            Call Carregar_Ref_Coml_Producao(rst.RecordCount, rst)
        Else
            Call Carregar_Ref_Coml_Intranet(rst)
        End If
    
    'Referencias Bancarias
    ElseIf sstCadastro.Tab = 3 Then
        
        '--------------------------------
        'REFER�NCIAS BANC�RIAS
        '--------------------------------
        'Se for an�lise, deve-se selecionar dados da tabela CLIENTE_WEB, sen�o da CLIENTE e CLIE_BANCO
        If vFl_Analise = False Then  'Tabela CLIENTE
            Set OraParameters = db.Parameters
        Else    'Tabela Cliente_Web
            Set OraParameters = db_INT.Parameters
        End If
        OraParameters.Remove "CODCLICGC": OraParameters.Add "CODCLICGC", Val(txtDocto.Text), 1
        OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
        OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
        
        Criar_Cursor OraParameters, "cRefBco"
        
        If vFl_Analise = False Then  'Tabela CLIENTE
            'vVB_Generica_001.Retorna_Cursor db, "cRefBco", "PRODUCAO.PCK_CAD060.PR_SELECT_REF_BANCO_P", "CODCLICGC", "vErro", "vMsgErro"
            'vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD0602.PR_SELECT_REF_BANCO_P(:cRefBco, :CODCLICGC, :vErro, :vMsgErro)")
            vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD060.PR_SELECT_REF_BANCO_P(:cRefBco, :CODCLICGC, :vErro, :vMsgErro)")
        Else    'Tabela Cliente_Web
            'vVB_Generica_001.Retorna_Cursor db_INT, "cRefBco", "INTRANET.PCK_CLIENTES_TESTE.PR_SELECT_REF_BANCO", "CODCLICGC", "vErro", "vMsgErro"
            vErro = vVB_Generica_001.ExecutaPl(db_INT, "INTRANET.PCK_CLIENTES_TESTE.PR_SELECT_REF_BANCO(:cRefBco, :CODCLICGC, :vErro, :vMsgErro)")
        End If
        If vErro = "" Then
            Set rst = OraParameters("cRefBco").Value
        Else
            Exit Sub
        End If
        
        Limpar_Campos_RefBco (True)
                
        'If db_INT.Parameters("Tabela") = 1 Then
        If vFl_Analise = False Then
            Call Carregar_Referencias_Producao(rst.RecordCount, rst)
        Else
            Call Carregar_Referencias_Intranet(rst)
        End If
    
    'Outros Endere�os
    ElseIf sstCadastro.Tab = 4 Then
    
        '--------------------------------
        'OUTROS ENDERE�OS
        '--------------------------------
        'Se for an�lise, deve-se selecionar dados da tabela CLIENTE_WEB, sen�o da CLIENTE e CLIE_ENDERECO
        If vFl_Analise = False Then  'Tabela CLIENTE
            Set OraParameters = db.Parameters
        Else    'Tabela Cliente_Web
            Set OraParameters = db_INT.Parameters
        End If
        OraParameters.Remove "CODCLICGC": OraParameters.Add "CODCLICGC", Val(txtDocto.Text), 1
        OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
        OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
        
        Criar_Cursor OraParameters, "cOutEnd"
        
        If vFl_Analise = False Then  'Tabela CLIENTE
            'vVB_Generica_001.Retorna_Cursor db, "cOutEnd", "PRODUCAO.PCK_CAD060.PR_SELECT_OUT_END_P", "CODCLICGC", "vErro", "vMsgErro"
            'vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD0602.PR_SELECT_OUT_END_P(:cOutEnd, :CODCLICGC, :vErro, :vMsgErro)")
            vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD060.PR_SELECT_OUT_END_P(:cOutEnd, :CODCLICGC, :vErro, :vMsgErro)")
        Else    'Tabela Cliente_Web
            'vVB_Generica_001.Retorna_Cursor db_INT, "cOutEnd", "INTRANET.PCK_CLIENTES_TESTE.PR_SELECT_OUT_END", "CODCLICGC", "vErro", "vMsgErro"
            vErro = vVB_Generica_001.ExecutaPl(db_INT, "INTRANET.PCK_CLIENTES_TESTE.PR_SELECT_OUT_END(:cOutEnd,:CODCLICGC, :vErro, :vMsgErro)")
        End If
        If vErro = "" Then
            Set rst = OraParameters("cOutEnd").Value
        Else
            Exit Sub
        End If
        
        vFl_Adic_OutEnd = False
        
        Limpar_Campos_OutEnd (True)
                
        'If db_INT.Parameters("Tabela") = 1 Then
        If vFl_Analise = False Then
            Call Carregar_OutEnd_Producao(rst.RecordCount, rst)
        Else
            Call Carregar_OutEnd_Intranet(rst)
        End If
    
    'Vendor
    'Vendor foi retirado a partir de Mar�o/2004, devido � negocia��o agora com B.Brasil - Cobran�a - Aleth�a - 29/04/04
    'ElseIf sstCadastro.Tab = 5 Then
        '--------------------------------
        'CLIENTE VENDOR
        '--------------------------------
        'Se for producao deve-se usar o Codigo do Cliente e Tipo da Tabela igual a 1 -54157
        'Caso contr�rio vai mostrar tudo em branco pois a cliente_web n�o tem informa��es de cliente vendor
        'Limpar_Campos_Vendor
        'If vFl_Analise = False Then  'Tabela CLIENTE
        '    db.Parameters.Remove "CODCLICGC": db.Parameters.Add "CODCLICGC", Val(txtDocto.Text), 1
        '    db.Parameters.Remove "Tabela": db.Parameters.Add "Tabela", 1, 1
        '    vVB_Generica_001.Retorna_Cursor db, "cVendor", "PRODUCAO.PCK_CAD060.PR_SELECT_VENDOR", "CODCLICGC", "TABELA"
        '    Set rst = vVB_Generica_001.Rst_Geral
        '    vVB_Generica_001.Rst_Geral.Close: Set vVB_Generica_001.Rst_Geral = Nothing
        '    Call Carregar_Vendor_Producao(rst.RecordCount, rst)
        '    cmdAdVendor.Enabled = True
        '    cmdExVendor.Enabled = True
        '    cmdGrVendor.Enabled = True
        '    cmdLiVendor.Enabled = True
        'Else    'Traz em branco
        '    cmdAdVendor.Enabled = False
        '    cmdExVendor.Enabled = False
        '    cmdGrVendor.Enabled = False
        '    cmdLiVendor.Enabled = False
        'End If
        
    End If

    sspCadCli.Caption = ""

End Sub

Sub Status_Botoes_Ref_Bancarias(Acao As String)
    If vFl_Analise = False Then
        If Acao = "ABRIR" Then
            cmdAdicionarRefBco.Enabled = True
            cmdEditarRefBco.Enabled = False
            cmdGrRefBco.Enabled = False
            cmdDesfRefBco.Enabled = False
        ElseIf Acao = "ADICIONAR" Then
            cmdAdicionarRefBco.Enabled = False
            cmdEditarRefBco.Enabled = False
            cmdGrRefBco.Enabled = True
            cmdDesfRefBco.Enabled = True
        ElseIf Acao = "GRAVAR" Then
            cmdAdicionarRefBco.Enabled = True
            cmdEditarRefBco.Enabled = False
            cmdGrRefBco.Enabled = False
            cmdDesfRefBco.Enabled = True
        ElseIf Acao = "SELECIONAR" Then
            cmdAdicionarRefBco.Enabled = True
            cmdEditarRefBco.Enabled = True
            cmdGrRefBco.Enabled = False
            cmdDesfRefBco.Enabled = False
        End If
    Else
        cmdAdicionarRefBco.Enabled = False
        cmdEditarRefBco.Enabled = False
        cmdGrRefBco.Enabled = False
        cmdDesfRefBco.Enabled = False
    End If
End Sub

Sub Status_Botoes_Out_Enderecos(Acao As String)
    If vFl_Analise = False Then
        If Acao = "ABRIR" Then
            cmdAdOutEnd.Enabled = True
            cmdEdOutEnd.Enabled = False
            cmdGrOutEnd.Enabled = False
            cmdDesfOutEnd.Enabled = False
        ElseIf Acao = "ADICIONAR" Then
            cmdAdOutEnd.Enabled = False
            cmdEdOutEnd.Enabled = False
            cmdGrOutEnd.Enabled = True
            cmdDesfOutEnd.Enabled = True
        ElseIf Acao = "GRAVAR" Then
            cmdAdOutEnd.Enabled = True
            cmdEdOutEnd.Enabled = False
            cmdGrOutEnd.Enabled = False
            cmdDesfOutEnd.Enabled = False
        ElseIf Acao = "SELECIONAR" Then
            cmdAdOutEnd.Enabled = True
            cmdEdOutEnd.Enabled = True
            cmdGrOutEnd.Enabled = False
            cmdDesfOutEnd.Enabled = False
        End If
    Else
        cmdAdOutEnd.Enabled = False
        cmdEdOutEnd.Enabled = False
        cmdGrOutEnd.Enabled = False
        cmdDesfOutEnd.Enabled = False
    End If
End Sub

Sub Status_Botoes_Ref_Comerciais(Acao As String)
    If vFl_Analise = False Then
        If Acao = "ABRIR" Then
            cmdAdRefComl.Enabled = True
            cmdEdRefComl.Enabled = False
            cmdGrRefComl.Enabled = False
            cmdDesfRefCom.Enabled = False
        ElseIf Acao = "ADICIONAR" Then
            cmdAdRefComl.Enabled = False
            cmdEdRefComl.Enabled = False
            cmdGrRefComl.Enabled = True
            cmdDesfRefCom.Enabled = True
        ElseIf Acao = "GRAVAR" Then
            cmdAdRefComl.Enabled = True
            cmdEdRefComl.Enabled = False
            cmdGrRefComl.Enabled = False
            cmdDesfRefCom.Enabled = False
        ElseIf Acao = "SELECIONAR" Then
            cmdAdRefComl.Enabled = True
            cmdEdRefComl.Enabled = True
            cmdGrRefComl.Enabled = False
            cmdDesfRefCom.Enabled = False
        End If
    Else
        cmdAdRefComl.Enabled = False
        cmdEdRefComl.Enabled = False
        cmdGrRefComl.Enabled = False
        cmdDesfRefCom.Enabled = False
    End If
End Sub

Private Sub sstCadastro_DblClick()
    
    'Alinha_Shapes

End Sub






Private Sub txtAgencia_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.numero(KeyAscii)

End Sub


Private Sub txtAtraso01_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.numero(KeyAscii)

End Sub


Private Sub txtBairro_KeyPress(KeyAscii As Integer)
'    If Chr$(Keyascii) = "'" Or Chr$(Keyascii) = "�" Or Chr$(Keyascii) = "`" Then
'        Keyascii = 0
'        Beep
'    Else
'        Keyascii = vVB_Generica_001.Maiuscula(Keyascii)
'    End If
    'Muniz 10/02/10 valida txtBairro letra mai�scula e alfa num�rico
    KeyAscii = ValidaMaiuscula(KeyAscii)
    KeyAscii = ValidaAlfaNumerico(KeyAscii)
    'fim Muniz
End Sub


Private Sub txtBairroEnd01_KeyPress(KeyAscii As Integer)
'    If Chr$(Keyascii) = "'" Or Chr$(Keyascii) = "�" Or Chr$(Keyascii) = "`" Then
'        Keyascii = 0
'        Beep
'    Else
'        Keyascii = vVB_Generica_001.Maiuscula(Keyascii)
'    End If
    'Muniz 10/02/10 Valida campo txtBairro, letra mai�sculo e alfanum�rico
    KeyAscii = ValidaMaiuscula(KeyAscii)
    KeyAscii = ValidaAlfaNumerico(KeyAscii)
    'fim Muniz
End Sub


Private Sub txtBanco_KeyPress(KeyAscii As Integer)
    If Chr$(KeyAscii) = "'" Or Chr$(KeyAscii) = "�" Or Chr$(KeyAscii) = "`" Then
        KeyAscii = 0
        Beep
    Else
        KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
        KeyAscii = ValidaAlfaNumerico(KeyAscii)
    End If

End Sub


Private Sub txtCep_KeyPress(KeyAscii As Integer)
    'Keyascii = vVB_Generica_001.Numero(Keyascii)
    
    'Muniz 10/02/10 valida txtCep letra mai�scula e alfa num�rico
    KeyAscii = ValidaNumero(KeyAscii)
    'fim Muniz
    
End Sub




Private Sub txtCepEnd01_KeyPress(KeyAscii As Integer)
'    Keyascii = vVB_Generica_001.Numero(Keyascii)
    'Muniz 10/02/10 valida campo cep
    KeyAscii = ValidaAlfaNumerico(KeyAscii)
    'fim Muniz
End Sub


Private Sub txtCGCEnd01_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.numero(KeyAscii)

End Sub


Private Sub txtCGCEnd01_LostFocus()
    
    'Valida CGC ou CPF
    If Val(cboTpDoctoEnd01.Text) = 1 Then
        If Not vVB_Generica_001.ValidaCGC(txtCGCEnd01.Text) Then
            MsgBox "CGC Inv�lido!!!", vbCritical, "CAD060 - Aten��o"
            txtCGCEnd01.SetFocus
            Exit Sub
        End If
    Else
        If Not vVB_Generica_001.ValidaCIC(txtCGCEnd01.Text) Then
            MsgBox "CPF Inv�lido!!!", vbCritical, "CAD060 - Aten��o"
            txtCGCEnd01.SetFocus
            Exit Sub
        End If
    End If

End Sub

Private Sub txtCodTare_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.numero(KeyAscii)

End Sub

'Eduardo Faria de Oliveira - CIT - 22/08/12
Private Sub txtComplemento_KeyPress(KeyAscii As Integer)
    KeyAscii = ValidaMaiuscula(KeyAscii)
    KeyAscii = ValidaAlfaNumerico(KeyAscii)
End Sub

Private Sub txtComplEnd01_KeyPress(KeyAscii As Integer)
    KeyAscii = ValidaMaiuscula(KeyAscii)
    KeyAscii = ValidaAlfaNumerico(KeyAscii)
End Sub

Private Sub txtConceito_KeyPress(KeyAscii As Integer)
    If Chr$(KeyAscii) = "'" Or Chr$(KeyAscii) = "�" Or Chr$(KeyAscii) = "`" Then
        KeyAscii = 0
        Beep
    Else
        KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
    End If

End Sub


Private Sub txtConceito01_KeyPress(KeyAscii As Integer)
    If Chr$(KeyAscii) = "'" Or Chr$(KeyAscii) = "�" Or Chr$(KeyAscii) = "`" Then
        KeyAscii = 0
        Beep
    Else
        KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
    End If

End Sub




Private Sub txtContato_KeyPress(KeyAscii As Integer)
'    If Chr$(Keyascii) = "'" Or Chr$(Keyascii) = "�" Or Chr$(Keyascii) = "`" Then
'        Keyascii = 0
'        Beep
'    Else
'        Keyascii = vVB_Generica_001.Maiuscula(Keyascii)
'    End If
    'Muniz 10/02/10 Valida campo txtContato, letra mai�sculo e alfanum�rico
    KeyAscii = ValidaMaiuscula(KeyAscii)
    KeyAscii = ValidaAlfaNumerico(KeyAscii)
    'fim Muniz


End Sub


Private Sub txtContratoVendor_Change()

End Sub

Private Sub txtCxPosEnd01_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.numero(KeyAscii)

End Sub


Private Sub txtCxPostal_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.numero(KeyAscii)

End Sub


Private Sub txtDDD_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.numero(KeyAscii)

End Sub


Private Sub txtDDD01_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.numero(KeyAscii)

End Sub


Private Sub txtDDD1_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.numero(KeyAscii)

End Sub


Private Sub txtDDD2_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.numero(KeyAscii)

End Sub


Private Sub txtDDDEnd01_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.numero(KeyAscii)

End Sub




Private Sub txtDocto_GotFocus()
    If txtCodCliente.Text <> "" Then
          Cliente_SCI = txtCodCliente.Text
    End If
End Sub

Private Sub txtDocto_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.numero(KeyAscii)

End Sub


Private Sub txtDocto_LostFocus()
    If Trim(cboTpDocto.Text) = "" Or Trim(txtDocto.Text) = "" Then
        MsgBox "Preenchimento do documento � obrigat�rio!", vbCritical, "CAD060 - Aten��o"
        Exit Sub
    End If
    
    'Valida CGC ou CPF
    If Val(cboTpDocto.Text) = 1 Then
        If Not vVB_Generica_001.ValidaCGC(txtDocto.Text) Then
            MsgBox "CGC Inv�lido!!!", vbCritical, "Aten��o"
            txtDocto.SetFocus
            Exit Sub
        End If
    Else
        If Not vVB_Generica_001.ValidaCIC(txtDocto.Text) Then
            MsgBox "CPF Inv�lido!!!", vbCritical, "Aten��o"
            txtDocto.SetFocus
            Exit Sub
        End If
    End If
    
    
    'Vou trazer informa��es do Cliente, de acordo com o documento digitado
    Set OraParameters = db.Parameters
    If Trim(txtCodCliente.Text) <> "" Then
        OraParameters.Remove "CODCLI": OraParameters.Add "CODCLI", CDbl(txtCodCliente.Text), 1
    Else
        OraParameters.Remove "CODCLI": OraParameters.Add "CODCLI", 0, 1
    End If
    OraParameters.Remove "CGC": OraParameters.Add "CGC", CDbl(txtDocto.Text), 1
    OraParameters.Remove "TP_CONS": OraParameters.Add "TP_CONS", 0, 1
    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
   
    Criar_Cursor OraParameters, "PM_CURSOR1"
    
    'DLL - Fun��o que declara e retorna o cursor com dados do meu select
'    vVB_Generica_001.Retorna_Cursor db, "pm_CURSOR1", "PRODUCAO.PCK_CAD060.Pr_Verifica_Cliente", "CODCLI", "CGC", "TP_CONS", "vErro", "vMsgErro"
    vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD060.Pr_Verifica_Cliente(:PM_CURSOR1,:CODCLI, :CGC, :TP_CONS, :vErro, :vMsgErro)")
    If vErro = "" Then
        Set db_CONS = OraParameters("PM_CURSOR1").Value
    Else
        Exit Sub
    End If
    
    'INCLUS�O DE CLIENTE
    If db_CONS.EOF Then
        vFl_IncCad = True
        ' fpallini - Ci&T - 21/08/12 - Buscar Cadastro do SAP.
        Dim oRetornoPI As New RetornoPI
        If Val(cboTpDocto.Text) = 1 Then
            'Chamada do Metodo que busca o cliente no SAP por CNPJ
            Set oRetornoPI = oServicoCliente.ConsultaCliente(vUser_Intranet, "", "", "", Trim(txtDocto.Text), "")
        ElseIf Val(cboTpDocto.Text) = 2 Then
            'Chamada do Metodo que busca o cliente no SAP por CPF
            Set oRetornoPI = oServicoCliente.ConsultaCliente(vUser_Intranet, "", "", Trim(txtDocto.Text), "", "")
        End If
        'vxavier - CIT - 24/08/2012 - Seguir mesmo padrao do legado
        If oRetornoPI.Sucesso() Then
            'vFl_IncCad = False
            'MsgBox IIf(Left(cboTpDocto, 1) = 1, "CGC ", "CIC ") & "j� cadastrado !!!", vbCritical, "CAD060 - Aten��o"
            'txtDocto.SetFocus
            Exit Sub
        ElseIf Not oRetornoPI.CodErro = "W" Then ' W � retornado quando o SAP n�o encontra o cliente, ou seja, cliente novo.
             MsgBox "Erro ao buscar cliente no SAP: " & oRetornoPI.mensagem, vbInformation, "Aten��o"
        End If
    'ALTERA��O DE CLIENTE
    Else
        vFl_IncCad = False
        MsgBox IIf(Left(cboTpDocto, 1) = 1, "CGC ", "CIC ") & "j� cadastrado !!!", vbCritical, "CAD060 - Aten��o"
        txtDocto.SetFocus
        Exit Sub
    End If
    
End Sub

Private Sub txtDt_KeyPress(KeyAscii As Integer)
    vVB_Generica_001.Data KeyAscii, txtDt
End Sub

Private Sub txtDtCad01_KeyPress(KeyAscii As Integer)
    vVB_Generica_001.Data KeyAscii, txtDtCad01
End Sub

Private Sub txtDtCadastro_KeyPress(KeyAscii As Integer)
    vVB_Generica_001.Data KeyAscii, txtDtCadastro
End Sub


Private Sub txtDtCons01_KeyPress(KeyAscii As Integer)
    vVB_Generica_001.Data KeyAscii, txtDtCons01
End Sub

Private Sub txtDtFundacao_KeyPress(KeyAscii As Integer)
    vVB_Generica_001.Data KeyAscii, txtDtFundacao
End Sub




Private Sub txtDtLimite_KeyPress(KeyAscii As Integer)
    vVB_Generica_001.Data KeyAscii, txtDtLimite

End Sub






Private Sub txtDtMaiorSlDev_KeyPress(KeyAscii As Integer)
    vVB_Generica_001.Data KeyAscii, txtDtMaiorSlDev
End Sub


Private Sub txtDtMsg_GotFocus()
    If cboMsgCliente.Text = " " Then
        txtDtMsg.Text = ""
        txtResponsavel.SetFocus
    End If
End Sub

Private Sub txtDtMsg_KeyPress(KeyAscii As Integer)
    vVB_Generica_001.Data KeyAscii, txtDtMsg
End Sub


Private Sub txtDtRecadastro_KeyPress(KeyAscii As Integer)
    vVB_Generica_001.Data KeyAscii, txtDtRecadastro
End Sub

Private Sub txtDtTare_KeyPress(KeyAscii As Integer)
    vVB_Generica_001.Data KeyAscii, txtDtTare
End Sub

Private Sub txtDtUltFat01_KeyPress(KeyAscii As Integer)
    vVB_Generica_001.Data KeyAscii, txtDtUltFat01
End Sub

Private Sub txtEmail_KeyPress(KeyAscii As Integer)
    'Keyascii = vVB_Generica_001.Maiuscula(Keyascii)
    
    'Muniz 10/02/10 Valida campo txtEmail, letra mai�sculo e alfanum�rico
    KeyAscii = ValidaMaiuscula(KeyAscii)
    KeyAscii = ValidaAlfaNumerico(KeyAscii)
    'fim Muniz
    
End Sub


Private Sub txtEmp01_KeyPress(KeyAscii As Integer)
    If Chr$(KeyAscii) = "'" Or Chr$(KeyAscii) = "�" Or Chr$(KeyAscii) = "`" Then
        KeyAscii = 0
        Beep
    Else
        KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
        KeyAscii = ValidaAlfaNumerico(KeyAscii)
    End If
End Sub


Private Sub txtEnd01_KeyPress(KeyAscii As Integer)
'    If Chr$(Keyascii) = "'" Or Chr$(Keyascii) = "�" Or Chr$(Keyascii) = "`" Then
'        Keyascii = 0
'        Beep
'    Else
'        Keyascii = vVB_Generica_001.Maiuscula(Keyascii)
'    End If
    'Muniz 10/02/10 Valida campo txtEndereco, letra mai�sculo e alfanum�rico
    KeyAscii = ValidaMaiuscula(KeyAscii)
    KeyAscii = ValidaAlfaNumerico(KeyAscii)
    'fim Muniz
End Sub


Private Sub txtEndereco_KeyPress(KeyAscii As Integer)
'    If Chr$(Keyascii) = "'" Or Chr$(Keyascii) = "�" Or Chr$(Keyascii) = "`" Then
'        Keyascii = 0
'        Beep
'    Else
    
    'Muniz 10/02/10 valida txtEndere�o letra mai�scula e alfa num�rico
    KeyAscii = ValidaMaiuscula(KeyAscii)
    KeyAscii = ValidaAlfaNumerico(KeyAscii)
    'fim Muniz

'    End If
End Sub


Private Sub txtFax_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.numero(KeyAscii)
End Sub


Private Sub txtFaxEnd01_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.numero(KeyAscii)
End Sub


Private Sub txtFone_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.numero(KeyAscii)
End Sub


Private Sub txtFone01_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.numero(KeyAscii)
End Sub


Private Sub txtFone1_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.numero(KeyAscii)
End Sub

Private Sub txtFone2_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.numero(KeyAscii)
End Sub


Private Sub txtFoneEnd01_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.numero(KeyAscii)
End Sub

Private Sub txtGrupoMaster_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
End Sub


Private Sub txtHomePage_KeyPress(KeyAscii As Integer)
    'Keyascii = vVB_Generica_001.Maiuscula(Keyascii)
    'Muniz 10/02/10 Valida campo txtHomepage, letra mai�sculo e alfanum�rico
    KeyAscii = ValidaMaiuscula(KeyAscii)
    KeyAscii = ValidaAlfaNumerico(KeyAscii)
    'fim Muniz
End Sub

Private Sub txtIEEnd01_KeyPress(KeyAscii As Integer)
    If Chr$(KeyAscii) = "'" Or Chr$(KeyAscii) = "�" Or Chr$(KeyAscii) = "`" Then
        KeyAscii = 0
        Beep
    Else
        KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
    End If
End Sub


Private Sub txtIEEnd01_LostFocus()
    
    'No campo Inscri��o Estadual n�o s�o permitidos os caracteres
    '. ponto   - hifen    ()parenteses
    If InStr(txtIEEnd01, ".") <> 0 Then
       MsgBox " O caracter '.' � invalido. Retire-o da Inscri��o Estadual "
       txtIEEnd01.SetFocus
    ElseIf InStr(txtIEEnd01, "-") <> 0 Then
       MsgBox " O caracter '-' � invalido. Retire-o da Inscri��o Estadual "
       txtIEEnd01.SetFocus
    ElseIf InStr(txtIEEnd01, "(") <> 0 Then
       MsgBox " O caracter '(' � invalido. Retire-o da Inscri��o Estadual "
       txtIEEnd01.SetFocus
    ElseIf InStr(txtIEEnd01, ")") <> 0 Then
       MsgBox " O caracter ')' � invalido. Retire-o da Inscri��o Estadual "
       txtIEEnd01.SetFocus
    ElseIf Trim(txtIEEnd01.Text) <> "" Then
        Call Valida_IE(frmCadCliente.txtIEEnd01, frmCadCliente.cboUFEnd01)
        If Fl_Invalida = "S" Then
            MsgBox "Inscri��o Estadual Inv�lida!", vbCritical, "CAD060 - Aten��o"
        End If
    End If

End Sub


Private Sub txtInscrEstadual_KeyPress(KeyAscii As Integer)
    If Chr$(KeyAscii) = "'" Or Chr$(KeyAscii) = "�" Or Chr$(KeyAscii) = "`" Then
        KeyAscii = 0
        Beep
    Else
        KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
    End If
End Sub


Private Sub txtInscrEstadual_LostFocus()
    
    If txtInscrEstadual = "ISENTO" And Val(cboTpDocto.Text) = 1 Then 'TI-6870
        chkConsumidorFinal.Value = False
    End If
        
    'No campo Inscri��o Estadual n�o s�o permitidos os caracteres
    '. ponto   - hifen    ()parenteses
    If InStr(txtInscrEstadual, ".") <> 0 Then
       MsgBox " O caracter '.' � invalido. Retire-o da Inscri��o Estadual "
       txtInscrEstadual.SetFocus
    ElseIf InStr(txtInscrEstadual, "-") <> 0 Then
       MsgBox " O caracter '-' � invalido. Retire-o da Inscri��o Estadual "
       txtInscrEstadual.SetFocus
    ElseIf InStr(txtInscrEstadual, "(") <> 0 Then
       MsgBox " O caracter '(' � invalido. Retire-o da Inscri��o Estadual "
       txtInscrEstadual.SetFocus
    ElseIf InStr(txtInscrEstadual, ")") <> 0 Then
       MsgBox " O caracter ')' � invalido. Retire-o da Inscri��o Estadual "
       txtInscrEstadual.SetFocus
    ElseIf Trim(txtInscrEstadual.Text) <> "" Then
        Call Valida_IE(frmCadCliente.txtInscrEstadual, frmCadCliente.cboUF)
        If Fl_Invalida = "S" Then
            MsgBox "Inscri��o Estadual Inv�lida!", vbCritical, "CAD060 - Aten��o"
        End If
    End If

End Sub


Private Sub txtInscrSuframa_KeyPress(KeyAscii As Integer)
    If Chr$(KeyAscii) = "'" Or Chr$(KeyAscii) = "�" Or Chr$(KeyAscii) = "`" Then
        KeyAscii = 0
        Beep
    Else
        KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
    End If

End Sub


Private Sub txtLimite_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.numero(KeyAscii)

    Dim vText As Double
'    If txtLimite = "" Then
'        vText = 0
'    Else
'        vText = CDbl(txtLimite.Text)
'    End If
'    If KeyAscii > 47 And KeyAscii < 58 Then
'        If vText = 0 Then
'            vText = ".0" & Chr(KeyAscii)
'        ElseIf vText < 0.09 Then
'            vText = "0." & Right(CStr(vText), 1) & Chr(KeyAscii)
'        ElseIf vText < 0.99 Then
'            vText = CStr(Int(vText * 10)) & "." & Right(CStr(vText), 1) & Chr(KeyAscii)
'        ElseIf vText < 9.99 Then
'            vText = CStr(Int(vText * 10)) & "." & Right(CStr(vText), 1) & Chr(KeyAscii)
'        ElseIf vText < 99.99 Then
'            vText = CStr(Int(vText * 10)) & "." & Right(CStr(vText), 1) & Chr(KeyAscii)
'        ElseIf vText < 999.99 Then
'            vText = Left(CStr(Int(vText * 10)), 1) & "," & Right(CStr(Int(vText * 10)), 3) & "." & Right(CStr(vText), 1) & Chr(KeyAscii)
'        ElseIf vText < 9999.99 Then
'            vText = Left(CStr(Int(vText * 10)), 2) & "," & Right(CStr(Int(vText * 10)), 3) & "." & Right(CStr(vText), 1) & Chr(KeyAscii)
'        ElseIf vText < 99999.99 Then
'            vText = Left(CStr(CDbl(vText * 10)), 3) & "," & Right(CStr(CDbl(vText * 10)), 3) & "." & Right(CStr(vText), 1) & Chr(KeyAscii)
'        ElseIf vText < 999999.99 Then
'            vText = Left(CStr(CDbl(vText * 10)), 1) & "," & Mid(CStr(CLng(vText * 10)), 2, 3) & "," & Mid(CStr(CLng(vText * 100)), 5, 3) & "." & Right(CStr(vText), 1) & Chr(KeyAscii)
'        ElseIf vText < 9999999.99 Then
'            vText = Left(CStr(CDbl(vText * 10)), 2) & "," & Mid(CStr(CLng(vText * 10)), 3, 3) & "," & Mid(CStr(CLng(vText * 100)), 6, 3) & "." & Right(CStr(vText), 1) & Chr(KeyAscii)
'        End If
'    End If
'    KeyAscii = 0
'    txtLimite = vText
'
'    txtLimite = FormatNumber(txtLimite, 2, vbTrue, vbTrue, vbTrue)
'
'    txtLimite.SetFocus
'    SendKeys ("{END}")
End Sub


Private Sub txtMaiSlDev01_GotFocus()
    txtMaiSlDev01 = ""
End Sub

Private Sub txtMaiSlDev01_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtMaiSlDev01)

End Sub


Private Sub txtNomeEnd01_KeyPress(KeyAscii As Integer)
'    If Chr$(Keyascii) = "'" Or Chr$(Keyascii) = "�" Or Chr$(Keyascii) = "`" Then
'        Keyascii = 0
'        Beep
'    Else
'        Keyascii = vVB_Generica_001.Maiuscula(Keyascii)
'    End If
    'Muniz 10/02/10 Valida campo txtNome, letra mai�sculo e alfanum�rico
    KeyAscii = ValidaMaiuscula(KeyAscii)
    KeyAscii = ValidaAlfaNumerico(KeyAscii)
    'fim Muniz
End Sub

'Kevlin Toshinari Ossda - CIT - 22/08/2012 - Valida o digito de campos n�mericos no campo numero
Private Sub txtNumEnd01_KeyPress(KeyAscii As Integer)
    KeyAscii = ValidaNumero(KeyAscii)
End Sub

'Eduardo Faria de Oliveira - CIT - 22/08/2012 - Somente numeros
Private Sub txtNumero_KeyPress(KeyAscii As Integer)
    KeyAscii = ValidaNumero(KeyAscii)
End Sub

Private Sub txtNumero_LostFocus()
    If Trim(txtNumero.Text) = "" Then
        txtNumero.Text = "0"
    End If
End Sub

Private Sub txtObs_KeyPress(KeyAscii As Integer)
'    If Chr$(Keyascii) = "'" Or Chr$(Keyascii) = "�" Or Chr$(Keyascii) = "`" Then
'        Keyascii = 0
'        Beep
'    Else
'        Keyascii = vVB_Generica_001.Maiuscula(Keyascii)
'    End If
    'Muniz 10/02/10 valida txtObs letra mai�scula e alfa num�rico
    KeyAscii = ValidaMaiuscula(KeyAscii)
    KeyAscii = ValidaAlfaNumerico(KeyAscii)
    'fim Muniz
End Sub


Private Sub txtObs01_KeyPress(KeyAscii As Integer)
'    If Chr$(Keyascii) = "'" Or Chr$(Keyascii) = "�" Or Chr$(Keyascii) = "`" Then
'        Keyascii = 0
'        Beep
'    Else
'        Keyascii = vVB_Generica_001.Maiuscula(Keyascii)
'    End If
    'Muniz 10/02/10 valida txtObs letra mai�scula e alfa num�rico
    KeyAscii = ValidaMaiuscula(KeyAscii)
    KeyAscii = ValidaAlfaNumerico(KeyAscii)
    'fim Muniz

End Sub



Private Sub txtProprietario_KeyPress(KeyAscii As Integer)
'    If Chr$(Keyascii) = "'" Or Chr$(Keyascii) = "�" Or Chr$(Keyascii) = "`" Then
'        Keyascii = 0
'        Beep
'    Else
'        Keyascii = vVB_Generica_001.Maiuscula(Keyascii)
'    End If
    'Muniz 10/02/10 Valida campo txtResponsavel, letra mai�sculo e alfanum�rico
    KeyAscii = ValidaMaiuscula(KeyAscii)
    KeyAscii = ValidaAlfaNumerico(KeyAscii)
    'fim Muniz

End Sub


Private Sub txtResponsavel_GotFocus()
    If cboMsgCliente.Text = " " Then
        txtResponsavel.Text = ""
        txtProprietario.SetFocus
    End If

End Sub

Private Sub txtResponsavel_KeyPress(KeyAscii As Integer)
'    If Chr$(Keyascii) = "'" Or Chr$(Keyascii) = "�" Or Chr$(Keyascii) = "`" Then
'        Keyascii = 0
'        Beep
'    Else
'        Keyascii = vVB_Generica_001.Maiuscula(Keyascii)
'    End If
    'Muniz 10/02/10 Valida campo txtResponsavel, letra mai�sculo e alfanum�rico
    KeyAscii = ValidaMaiuscula(KeyAscii)
    KeyAscii = ValidaAlfaNumerico(KeyAscii)
    'fim Muniz
End Sub


Private Sub txtSituacao_LostFocus()

    If Fl_Cliente_Novo = "S" Then
        pictSituacao.Picture = imlFiguras.ListImages(5).Picture
        txtSituacao.Text = "0 - Ativo"
    Else
        pictSituacao.Picture = IIf(txtSituacao.Text = "0 - Ativo", imlFiguras.ListImages(5).Picture, imlFiguras.ListImages(4).Picture)
    End If
End Sub



Private Sub txtTelex_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.numero(KeyAscii)

End Sub




Private Sub txtVlMaiorSlDev_GotFocus()
Dim I As Integer
Dim vSenhaOK As Boolean

For I = 1 To 3
    If InputBox("Senha", "Altera��o de Campo") = "XXXXXX" Then
        txtVlMaiorSlDev.Enabled = True
        'txtDtMaiorSlDev.Enabled = True
        'Vari�veis para controlar a altera��o dos campos de maior saldo devedor para LOG
        vMaiorSlDev_Ant = CDbl(txtVlMaiorSlDev)
        vMaiorSlDev_Atu = CDbl(txtVlMaiorSlDev)
        vDtMaiorSlDev_Ant = txtDtMaiorSlDev
        vDtMaiorSlDev_Atu = txtDtMaiorSlDev
        vSenhaOK = True
        I = 3
    Else
        vSenhaOK = False
    End If
Next I

If vSenhaOK = False Then
    txtCodTare.SetFocus
    txtVlMaiorSlDev.Enabled = False
    txtDtMaiorSlDev.Enabled = False
End If

End Sub

'Private Sub txtVlLimiteVendor_KeyPress(KeyAscii As Integer)
'    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtVlLimiteVendor)
'End Sub


Private Sub txtVlMaiorSlDev_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtVlMaiorSlDev)

End Sub


Private Sub txtVlMaiorSlDev_LostFocus()

    If Not IsNumeric(txtVlMaiorSlDev) Then Exit Sub

    vMaiorSlDev_Atu = CDbl(txtVlMaiorSlDev)
    
    'Grava o log da altera��o
    If Trim(vMaiorSlDev_Ant) <> Trim(vMaiorSlDev_Atu) Then
        
        'Atribui a data de hoje para o campo de data de maior saldo devedor
        txtDtMaiorSlDev.Enabled = True
        txtDtMaiorSlDev = vDtHoje
        txtDtMaiorSlDev.Enabled = False
        vDtMaiorSlDev_Atu = txtDtMaiorSlDev
        
        'Pega a data e hora da altera��o
        Sql = "SELECT TO_CHAR(sysdate,'dd/mm/yy hh24:mi:ss') FROM dual "
        Set db_CONS = db.dbcreatedynaset(Sql, 0&)
        vDtHoraHoje = db_CONS.Fields(0).Value
        
        Set OraParameters = db.Parameters
        OraParameters.Remove "CODUSER":  OraParameters.Add "CODUSER", vCodUser_Intranet, 1
        OraParameters.Remove "DTALT":    OraParameters.Add "DTALT", vDtHoraHoje, 1
        OraParameters.Remove "VL_ANT":   OraParameters.Add "VL_ANT", IIf(Trim(vMaiorSlDev_Ant) = "", Null, vMaiorSlDev_Ant), 1
        OraParameters.Remove "VL_ATU":   OraParameters.Add "VL_ATU", IIf(Trim(vMaiorSlDev_Atu) = "", Null, vMaiorSlDev_Atu), 1
        OraParameters.Remove "DT_ANT":   OraParameters.Add "DT_ANT", IIf(Trim(vDtMaiorSlDev_Ant) = "", Null, vDtMaiorSlDev_Ant), 1
        OraParameters.Remove "vERRO":    OraParameters.Add "vERRO", 0, 2
        OraParameters.Remove "vMSGERRO": OraParameters.Add "vMSGERRO", "", 2
    
        'db.Executesql "BEGIN PRODUCAO.PCK_CAD060.PR_LOG_MAIORSLDEV" & _
                            "(:CODUSER,TO_DATE(:DTALT,'DD/MM/RR HH24:MI:SS')," & _
                            " :VL_ANT,:VL_ATU,TO_DATE(:DT_ANT,'DD/MM/RR')," & _
                            " :vErro,:vMsgErro); END;"
        
        db.Executesql "BEGIN PRODUCAO.PCK_CAD060.PR_LOG_MAIORSLDEV" & _
                            "(:CODUSER,:DTALT," & _
                            " :VL_ANT,:VL_ATU,TO_DATE(:DT_ANT,'DD/MM/RR')," & _
                            " :vErro,:vMsgErro); END;"
        
        If Val(IIf(IsNull(db.Parameters("vErro")), 0, db.Parameters("vErro"))) <> 0 Then
           MsgBox "Erro: " & Val(OraParameters("vErro").Value) & " - " & _
                           OraParameters("vMsgErro").Value & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
            Exit Sub
        End If
    
    End If
    
End Sub

Private Sub txtVlUltFat01_GotFocus()
    txtVlUltFat01 = ""
End Sub

Private Sub txtVlUltFat01_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtVlUltFat01)

End Sub



Sub Atualizar_OnLine()
        
    Dim objConexao As Object
        
    Screen.MousePointer = vbHourglass
        
    'Seleciona lojas da Deposito_Visao
    Set OraParameters = db.Parameters
    OraParameters.Remove "NOMEPRG": OraParameters.Add "NOMEPRG", "CAD060", 1
    OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
    OraParameters("vErro").ServerType = ORATYPE_NUMBER
    vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD060.Pr_Deposito_Visao(:PM_CURSOR1,:NOMEPRG, :vErro, :vMsgErro)")
    
    If vErro <> "" Then Exit Sub
    
    Set db_CONS = OraParameters("PM_CURSOR1").Value
    
    If db_CONS.EOF Then
        MsgBox "N�o existem lojas cadastradas na tabela DEPOSITO_VISAO. " & Chr(13) & _
               "O cliente somente ser� cadastrado ON-LINE em Campinas. " & Chr(13) & _
               "Por favor, avise o suporte!", vbInformation, "CAD060 - Cadastro de Cliente"
    End If
        
        Set OraParameters = db.Parameters
        'Usados na Insercao do Cliente e Clie_Endereco
        OraParameters.Remove "CGC":             OraParameters.Add "CGC", txtCGCEnd01, 1
        OraParameters.Remove "TPDOCTO":         OraParameters.Add "TPDOCTO", Val(cboTpDoctoEnd01), 1
        OraParameters.Remove "NOMECLIENTE":     OraParameters.Add "NOMECLIENTE", txtNomeEnd01, 1
        OraParameters.Remove "ENDERECO":        OraParameters.Add "ENDERECO", txtEnd01, 1
        OraParameters.Remove "CODCIDADE":       OraParameters.Add "CODCIDADE", Val(Right(cboCidadeEnd01.Text, 4)), 1
        OraParameters.Remove "BAIRRO":          OraParameters.Add "BAIRRO", txtBairroEnd01, 1
        OraParameters.Remove "DDD1":            OraParameters.Add "DDD1", Val(txtDDDEnd01), 1
        OraParameters.Remove "FONE1":           OraParameters.Add "FONE1", Val(txtFoneEnd01), 1
        OraParameters.Remove "CEP":             OraParameters.Add "CEP", txtCepEnd01, 1
        OraParameters.Remove "IE":              OraParameters.Add "IE", txtIEEnd01, 1
        OraParameters.Remove "NOMEPROPRIET":    OraParameters.Add "NOMEPROPRIET", txtNomeEnd01, 1
        OraParameters.Remove "vERRO":           OraParameters.Add "vERRO", 0, 2
        OraParameters.Remove "vMSGERRO":        OraParameters.Add "vMSGERRO", "", 2
        
        'Usado exclusivamente na Insercao do Clie_Endereco
        OraParameters.Remove "CODCLI": OraParameters.Add "CODCLI", Val(txtCodCliente.Text), 1
        OraParameters.Remove "SEQ": OraParameters.Add "SEQ", Val(txtSeqEnd01.Text), 1
        OraParameters.Remove "FAX": OraParameters.Add "FAX", Val(txtFaxEnd01.Text), 1
        OraParameters.Remove "CXPOST": OraParameters.Add "CXPOST", Val(txtCxPosEnd01.Text), 1
    
        '-------------------------------------------------------
        'Inclui na tabela CLIENTE de todos os CDS (On_Line)
        '--- em 30/08/2005
        '-------------------------------------------------------
        db.Executesql "BEGIN PRODUCAO.PCK_CAD060.PR_INSERT_CLIENTE_END_ENTREGA" & _
                      "(:CGC,:TPDOCTO,:NOMECLIENTE,:ENDERECO,:CODCIDADE, :BAIRRO, :DDD1, :FONE1, :CEP, :IE, :NOMEPROPRIET, :vErro,:vMsgErro); END;"
                
        If Val(IIf(IsNull(db.Parameters("vErro")), 0, db.Parameters("vErro"))) <> 0 Then
           MsgBox "Erro: " & Val(OraParameters("vErro").Value) & " - " & _
                           OraParameters("vMsgErro").Value & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
            Exit Sub
        End If

        '*******************************************************************************
        'Incluir registro na tabela CLIE_ENDERECO para todos os CDs - Eduardo - 30/08/05
        '*******************************************************************************
        db.Executesql "BEGIN PRODUCAO.PCK_CAD060.PR_GRAVA_END_ENTREGA" & _
                            "(:CODCLI, :SEQ, :ENDERECO, :CODCIDADE, :BAIRRO, :CEP, :DDD1, :FONE1, :FAX, :CXPOST, :TPDOCTO, :CGC, :IE,:NOMECLIENTE, :vERRO, :vMSGERRO); END;"
        
        If Val(IIf(IsNull(db.Parameters("vErro")), 0, db.Parameters("vErro"))) <> 0 Then
           MsgBox "Erro: " & Val(OraParameters("vErro").Value) & " - " & _
                           OraParameters("vMsgErro").Value & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
            Exit Sub
        End If

        MsgBox "Registro Incluido com Sucesso !", vbInformation, "CAD060 - Cadastro de Cliente"
    
    'End If
    
    Screen.MousePointer = vbNormal
End Sub

Function fTaNaLista(pCbo As ComboBox)
    fTaNaLista = False
    
    For I = 0 To pCbo.ListCount - 1
    
        If UCase(pCbo.Text) = pCbo.List(I) Then
            fTaNaLista = True
            Exit For
        End If
        
    Next
End Function

'vxavier - CIT - 15/08/2012 - ISA - 265 - Alterar CAD060 para consultar e carregar os dados do SAP
Function ConsultarSAP(TipoPessoa As Integer, Optional CnpjOrCpf As String, Optional Nome As String, Optional PreencherSomenteNroSAP As Boolean = False) As RetornoPI
    Dim oCliente As ClienteSAP
    Dim oRetornoPI As New RetornoPI
    
    On Error GoTo Trataerro
        
    Set ConsultarSAP = oRetornoPI
        
    If TipoPessoa = 1 Then
        'Chamada do Metodo que busca o cliente no SAP por CNPJ
        Set oRetornoPI = oServicoCliente.ConsultaCliente(vUser_Intranet, "", "", "", CnpjOrCpf, "")
    ElseIf TipoPessoa = 2 Then
        'Chamada do Metodo que busca o cliente no SAP por CPF
        Set oRetornoPI = oServicoCliente.ConsultaCliente(vUser_Intranet, "", "", CnpjOrCpf, "", "")
    ElseIf Trim(Nome) <> "" Then
        'Chamada do Metodo que busca o cliente no SAP por Nome
        Set oRetornoPI = oServicoCliente.ConsultaCliente(vUser_Intranet, "", "", "", "", Nome)
    Else
        ' Se n�o houver busca por CPF/CNPJ ou Nome aborta a fun��o
        Exit Function
    End If
    
    Set ConsultarSAP = oRetornoPI
    
    ' Em caso de erro na consulta do SAP avisa o usu�rio e aborta a fun��o.
    If Not oRetornoPI.CodErro = "S" Then
        Exit Function
    End If

    Set oCliente = oRetornoPI.retorno
    
    ' Preencher somente o n�mero SAP ou o form completo.
    If PreencherSomenteNroSAP Then
        If oCliente.NoClienteSAP > 0 Then
            txtSAP = oCliente.NoClienteSAP
        End If
    Else
        Call PreencherForm(oCliente)
    End If
        
    Exit Function
    
Trataerro:
        
    Set ConsultarSAP = New RetornoPI
    ConsultarSAP.CodErro = "Erro VB"
    ConsultarSAP.mensagem = "Erro na consulta do cliente, contacte o administrador."
        
End Function

'Kevlin toshinari Ossada - CIT - 22/08/2012 - Fun��o para verificar se n�o foi digitado caracteres invalidos nos campos de endereco
Function ValidaCamposEndereco(campoEndereco As String, caracterProibido As String) As Boolean
    ValidaCamposEndereco = InStr(campoEndereco, caracterProibido) <> 0
End Function
'Eveline Dupas - CIT - 04/01/2012 - Verifica se h� representante vinculado ao cadastro do cliente
Function ValidaRepresentante(representante As String)
    ValidaRepresentante = True
    If representante = "" Then
        Sql = " Select r.cod_repres " & _
                " from r_clie_repres r, representante e , filial f " & _
               " where r.cod_Cliente = :CODCLI " & _
                 " and r.cod_repres = e.cod_repres " & _
                 " and e.cod_Filial = f.cod_filial " & _
            " order by r.cod_repres "
                   
        db.Parameters.Remove "CODCLI": db.Parameters.Add "CODCLI", txtCodCliente.Text, 1
        Set db_CONS = db.dbcreatedynaset(Sql, 0&)
            
        I = 1
        If db_CONS.EOF Then
           ValidaRepresentante = False
        End If
    End If

End Function
'vxavier - CIT - 17/08/2012 - ISA - 265 - Alterar CAD060 para consultar e carregar os dados do SAP
Sub PreencherForm(oCliente As ClienteSAP)
    
    Dim CodCidade As Integer
    Dim mensagemFiscal As String
    Dim indiceTpSegmento As Integer
    Dim indiceTpCliente As Integer
    Dim codcidadeaux As String
    
    'Preencher o FormCadCliente
    If Not IsNull(oCliente) Then
    
        If oCliente.NoClientePOS > 0 Then
            txtCodCliente.Text = oCliente.NoClientePOS
        End If
        
        If oCliente.NoClienteSAP > 0 Then
            txtSAP.Text = oCliente.NoClienteSAP
        End If
    
        cboCliente.Text = oCliente.Name1
        
        If Trim(UCase(oCliente.GrupoConta)) = "ZCPJ" Then
            txtDocto = oCliente.CNPJ
            lblDocto = "CNPJ"
            txtDocto.Enabled = False
            cboTpDocto.Text = "1 - PESSOA JURIDICA"
            cboTpDocto.Enabled = False
            txtDtFundacao.Text = Format(oCliente.DataFundacao, "dd/MM/yy")
        ElseIf Trim(UCase(oCliente.GrupoConta)) = "ZCPF" Then
            txtDocto = oCliente.CPF
            lblDocto = "CPF"
            txtDocto.Enabled = False
            cboTpDocto.Text = "2 - PESSOA FISICA"
            cboTpDocto.Enabled = False
            txtDtFundacao.Text = Format(oCliente.DtNascimento, "dd/MM/yy")
        End If
        
        txtDtCadastro.Text = IIf(IsNull(oCliente.DataCadastro), "", Format(oCliente.DataCadastro, "dd/MM/yy"))
        txtDtCadastro.Enabled = False
        txtDtRecadastro.Text = IIf(IsNull(oCliente.DataReCadastro), "", Format(oCliente.DataReCadastro, "dd/MM/yy"))
        txtInscrEstadual.Text = IIf(IsNull(oCliente.NoIDFiscal3), "", oCliente.NoIDFiscal3)
        cboMsgCliente.Text = IIf(IsNull(oCliente.Mesagem), "", oCliente.Mesagem)
        
        If Not oCliente.EnderecoPrincipal Is Nothing Then
            txtEndereco.Text = IIf(IsNull(oCliente.EnderecoPrincipal.rua), "", oCliente.EnderecoPrincipal.rua)
            If IsNull(oCliente.EnderecoPrincipal.NoResidencia) Or oCliente.EnderecoPrincipal.NoResidencia = "" Then
                txtNumero.Text = 0
            Else
                txtNumero.Text = oCliente.EnderecoPrincipal.NoResidencia
            End If
            txtComplemento.Text = IIf(IsNull(oCliente.EnderecoPrincipal.complemento), "", oCliente.EnderecoPrincipal.complemento)
            txtBairro.Text = IIf(IsNull(oCliente.EnderecoPrincipal.Bairro), "", oCliente.EnderecoPrincipal.Bairro)
            txtCep.Text = IIf(IsNull(oCliente.EnderecoPrincipal.CEP), "", Replace(Format(oCliente.EnderecoPrincipal.CEP, "00000000"), "-", "")) ' Retirar o tra�o do campo de CEP
            txtCxPostal.Text = IIf(IsNull(oCliente.EnderecoPrincipal.CxPostal), "", oCliente.EnderecoPrincipal.CxPostal)
            cboUF.Text = IIf(IsNull(oCliente.EnderecoPrincipal.Regiao), "", oCliente.EnderecoPrincipal.Regiao)
            Call cboUF_Click
            If Not IsNull(oCliente.EnderecoPrincipal.Cidade) And Not IsNull(oCliente.EnderecoPrincipal.Regiao) Then
                codcidadeaux = oServicoCliente.ObterCodCidadePorDomFiscal(oCliente.FISCAL.domFiscal)
                If codcidadeaux <> "" Then
                    CodCidade = CInt(codcidadeaux)
                End If
                If CodCidade > 0 Then
                    cboCidade.Text = IIf(IsNull(oCliente.EnderecoPrincipal.Cidade), "", Trim(oCliente.EnderecoPrincipal.Cidade) & " - " & Format(CodCidade, "0000"))
                End If
            End If
            txtDDD1.Text = IIf(IsNull(oCliente.EnderecoPrincipal.DDDFone), "", oCliente.EnderecoPrincipal.DDDFone)
            txtFone1.Text = IIf(IsNull(oCliente.EnderecoPrincipal.Telefone), "", oCliente.EnderecoPrincipal.Telefone)
            txtFone2.Text = IIf(IsNull(oCliente.EnderecoPrincipal.Celular), "", oCliente.EnderecoPrincipal.Celular)
            txtDDD2.Text = IIf(IsNull(oCliente.EnderecoPrincipal.DDDFax), "", oCliente.EnderecoPrincipal.DDDFax)
            txtFax.Text = IIf(IsNull(oCliente.EnderecoPrincipal.TelFax), "", oCliente.EnderecoPrincipal.TelFax)
            txtTelex.Text = IIf(IsNull(oCliente.EnderecoPrincipal.Telex), "", oCliente.EnderecoPrincipal.Telex)
        End If
        
        If Not oCliente.ContatoPrincipal Is Nothing Then
            txtContato.Text = IIf(IsNull(oCliente.ContatoPrincipal.Nome), "", oCliente.ContatoPrincipal.Nome)
        End If
        
        If Not oCliente.ObterComunicacaoCliente(0) Is Nothing Then
            txtEmail.Text = IIf(IsNull(oCliente.ObterComunicacaoCliente(0).Email), "", oCliente.ObterComunicacaoCliente(0).Email)
        End If
        
        If Not oCliente.FISCAL Is Nothing Then
            
            If (Trim(oCliente.FISCAL.MsgFiscal1) = "S" Or Trim(UCase(oCliente.EnderecoPrincipal.Regiao)) = "SC") Then
                mensagemFiscal = oServicoCliente.ObterMensagemFiscalPorCodigo("53")
                cboMsgFiscal.Text = mensagemFiscal & " - 53"
            ElseIf Trim(oCliente.FISCAL.MsgFiscal2) = "S" Then
                mensagemFiscal = oServicoCliente.ObterMensagemFiscalPorCodigo("56")
                cboMsgFiscal.Text = mensagemFiscal & " - 56"
            Else
                cboMsgFiscal.Text = ""
            End If
            
            If Trim(oCliente.FISCAL.TpEmpresa) = "" Then
                cboTpEmpresa.ListIndex = -1
            Else
                If oCliente.FISCAL.TpEmpresa = "M" Then
                    cboTpEmpresa.Text = "M - MICROEMPRESA"
                ElseIf oCliente.FISCAL.TpEmpresa = "P" Then
                    cboTpEmpresa.Text = "P - EMPRESA PEQ.PORTE"
                Else
                    cboTpEmpresa.Text = "E - EMPRESA"
                End If
            End If
            
            txtCodTare.Text = IIf(IsNull(oCliente.FISCAL.CodTARE), "", oCliente.FISCAL.CodTARE)
            
        End If
        
        'Kevlin Toshinari Ossda - CIT - 21/08/2012 - For que passa por todos os items do combobox e seleciona o tipo que o SAP tras
        For indiceTpSegmento = 0 To cboTpSegmento.ListCount - 1
            If (Trim(oCliente.Tipo) = "") Then
                cboTpSegmento.ListIndex = -1
                cboTpCliente.ListIndex = -1
                Exit For
            ElseIf Split(cboTpSegmento.List(indiceTpSegmento), " - ")(1) = oCliente.Tipo Then
                cboTpSegmento.ListIndex = indiceTpSegmento
                
                For indiceTpCliente = 0 To cboTpCliente.ListCount - 1
                    If (Trim(oCliente.SubTipo) = "") Then
                        cboTpCliente.ListIndex = -1
                        Exit For
                    ElseIf Split(cboTpCliente.List(indiceTpCliente), " - ")(1) = oCliente.SubTipo Then
                        cboTpCliente.ListIndex = indiceTpCliente
                        Exit For
                    End If
                
                Next
                
                Exit For
            End If
        Next
        
        'Kevlin Toshinari Ossda - CIT - 21/08/2012 - For que passa por todos os items do combobox e seleciona o banco que o SAP tras
        'For indiceCBO = 0 To cboBanco.ListCount - 1
        '    If (Trim(oCliente.BancoPrincipal.CodBanco) = "") Then
        '        cboBanco.ListIndex = 1
        '        Exit For
        '    ElseIf CDbl(Right(cboBanco.List(indiceCBO), 3)) = CDbl(oCliente.BancoPrincipal.CodBanco) Then
        '        cboBanco.ListIndex = indiceCBO
        '        Exit For
        '    End If
        'Next

        txtInscrSuframa.Text = IIf(IsNull(oCliente.NoIDFiscal4), "", oCliente.NoIDFiscal4)
        
     End If
End Sub

'carlosj CiT [22/08/2012]
Sub PopularObjetoCliente(ByRef oCliente As ClienteSAP)
        
        If Val(txtCodCliente.Text) > 0 Then
            oCliente.NoClientePOS = txtCodCliente.Text
        End If
        
        oCliente.Name1 = cboCliente.Text
        
        If Val(cboTpDocto) = 1 Then ' PJ
            oCliente.CNPJ = Format(txtDocto.Text, "00000000000000")
            oCliente.GrupoConta = "ZCPJ"
            If IsDate(txtDtFundacao.Text) Then
                oCliente.DataFundacao = CDate(txtDtFundacao.Text)
            End If
        ElseIf Val(cboTpDocto) = 2 Then ' PF
            oCliente.CPF = Format(txtDocto.Text, "00000000000")
            oCliente.GrupoConta = "ZCPF"
            If IsDate(txtDtFundacao.Text) Then
                oCliente.DtNascimento = CDate(txtDtFundacao.Text)
            End If
        End If
                
        If IsDate(txtDtCadastro.Text) Then
            oCliente.DataCadastro = CDate(txtDtCadastro.Text)
            oCliente.AnoCadastro = Year(oCliente.DataCadastro)
            oCliente.MesCadastro = Month(oCliente.DataCadastro)
        End If
                
        If IsDate(txtDtRecadastro.Text) Then
            oCliente.DataReCadastro = CDate(txtDtRecadastro.Text)
        End If
        
        oCliente.NoIDFiscal3 = txtInscrEstadual.Text
        oCliente.Mesagem = cboMsgCliente.Text
        
        ' Se cliente n�o tinha endere�o 0, seta o primeiro
        If oCliente.EnderecoPrincipal Is Nothing Then
            oCliente.EnderecoPrincipal = New endereco
        End If
        
        oCliente.EnderecoPrincipal.rua = txtEndereco.Text
        oCliente.EnderecoPrincipal.NoResidencia = txtNumero.Text
        oCliente.EnderecoPrincipal.complemento = txtComplemento.Text
        oCliente.EnderecoPrincipal.Bairro = txtBairro.Text
        oCliente.EnderecoPrincipal.CEP = txtCep.Text
        oCliente.EnderecoPrincipal.CxPostal = txtCxPostal.Text
        oCliente.EnderecoPrincipal.Regiao = cboUF.Text
        oCliente.EnderecoPrincipal.Cidade = Split(cboCidade.Text, " - ")(0)
        oCliente.EnderecoPrincipal.DDDFone = txtDDD1.Text
        oCliente.EnderecoPrincipal.Telefone = txtFone1.Text
        oCliente.EnderecoPrincipal.Celular = txtFone2.Text
        oCliente.EnderecoPrincipal.DDDFax = txtDDD2.Text
        oCliente.EnderecoPrincipal.TelFax = txtFax.Text
        oCliente.EnderecoPrincipal.Telex = txtTelex.Text
        
        If oCliente.ContatoPrincipal Is Nothing Then
            oCliente.ContatoPrincipal = New Contato
        End If
        
        oCliente.ContatoPrincipal.Nome = txtContato.Text
        
        ' Atribui na primeira posicao das comunicaoes o email
        Dim comunicacoes() As Comunicacao
        comunicacoes = oCliente.RetArrayComunicoes
        If UBound(comunicacoes) > 0 Then
            comunicacoes(0).Email = txtEmail.Text
            Call oCliente.ArrayComunicacoes(comunicacoes)
        End If
        
        If oCliente.FISCAL Is Nothing Then
            oCliente.FISCAL = New FISCAL
        End If
        
        ' Mensagem Fiscal
        If Trim(UCase(cboUF.Text)) = "SC" Then ' Santa Cataria ou 53 recebe S/N
            oCliente.FISCAL.MsgFiscal1 = "S"
            oCliente.FISCAL.MsgFiscal2 = "N"
        ElseIf InStr(cboMsgFiscal.Text, " - ") > 0 Then
            If Val(Split(cboMsgFiscal.Text, " - ")(1)) = 53 Then
                oCliente.FISCAL.MsgFiscal1 = "S"
                oCliente.FISCAL.MsgFiscal2 = "N"
            ElseIf Val(Split(cboMsgFiscal.Text, " - ")(1)) = 56 Then
                oCliente.FISCAL.MsgFiscal1 = "N"
                oCliente.FISCAL.MsgFiscal2 = "S"
            Else
                oCliente.FISCAL.MsgFiscal1 = "N"
                oCliente.FISCAL.MsgFiscal2 = "N"
            End If
        Else
            oCliente.FISCAL.MsgFiscal1 = "N"
            oCliente.FISCAL.MsgFiscal2 = "N"
        End If
        
        If cboTpEmpresa.Text = "" Then
            oCliente.FISCAL.TpEmpresa = ""
        ElseIf InStr(cboTpEmpresa.Text, " - ") > 0 Then
            oCliente.FISCAL.TpEmpresa = Split(cboTpEmpresa.Text, " - ")(0)
        End If
        
        oCliente.FISCAL.CodTARE = txtCodTare.Text
        
        oCliente.FISCAL.domFiscal = oServicoCliente.ObterDomFiscalPorCodigoCidade((Split(cboCidade.Text, " - ")(1)))
        
        If InStr(cboTpCliente.Text, " - ") > 0 And InStr(cboTpSegmento.Text, " - ") > 0 Then
            
            'carlosj CiT (26/11/2012) - ISA 1033
            ' fpallini - Cit -03/12/2012 - isqa - DF#260 - Melhoria DE/PARA tipologia SAP
            oCliente.Tipo = Split(cboTpSegmento.Text, " - ")(1)
            oCliente.SubTipo = oServicoCliente.ObterTipoClientePorCodigo((Split(cboTpCliente.Text, " - ")(1)))
            
        End If
        
        ' vxavier - Ci&T - 06/11/12 - [ISA - 967]
        oCliente.Setor = RetornarSetorIndustrial(cboTpDocto.Text, cboUF.Text, cboCNAE.Text, txtDocto.Text, txtCodTare.Text, txtInscrEstadual.Text, oCliente.Tipo)
        oCliente.FISCAL.CatgCFOP = RetornarCategoriaCFO(cboTpDocto.Text, cboCNAE.Text, txtInscrEstadual.Text)
                
        
        If oCliente.BancoPrincipal Is Nothing Then
            oCliente.BancoPrincipal = New Banco
        End If
        
        'oCliente.BancoPrincipal.Banco = Split(cboBanco.Text, " - ")(0)
        'oCliente.BancoPrincipal.CodBanco = Split(cboBanco.Text, " - ")(1)
        
        oCliente.NoIDFiscal4 = txtInscrSuframa.Text
    
End Sub

 'carlosj CiT [24/08/2012]
  'Popula contados (Referencias comerciais)
  Sub PopularObjetoClienteContatos(ByRef oCliente As ClienteSAP)

      'verifica se a lista esta vazia
    If (lsvRefComl.ListItems.Count < 1) Then
        Exit Sub
    End If
    
    Dim arrayContato() As New Contato
    Dim oContato As Contato
    
    'oCliente.Contatos = New Contato
    
    ReDim arrayContato(lsvRefComl.ListItems.Count - 1)
          
    For I = 1 To lsvRefComl.ListItems.Count
    
    Set oContato = New Contato
    
     oContato.EmpresaContato = lsvRefComl.ListItems(I).SubItems(1)
     oContato.DDD = lsvRefComl.ListItems(I).SubItems(3)
     oContato.Fone = lsvRefComl.ListItems(I).SubItems(4)
     
    Set arrayContato(I - 1) = oContato
  
    Next I
    
    Call oCliente.ArrayContatos(arrayContato)
    
    ' Dim arrayContatos() As Contato
     '  arrayContatos = oCliente.RetArrayContatos
 
End Sub

'carlosj CiT [24/08/2012]
  'Popula enderecos do array do objeto cliente
  Sub PopularObjetoClienteEnderecos(ByRef oCliente As ClienteSAP)
    
     'verifica se a lista esta vazia
    If (lsvOutEnd.ListItems.Count < 1) Then
        Exit Sub
    End If

    'declarar variaveis
    Dim strTipoEndereco As String
    Dim intTipoEndereco As Integer
    Dim encontrouTipoEntrega As Boolean
    Dim encontrouTipoCobranca As Boolean
    
    ' seta valor inicial de variaveis
    encontrouTipoEntrega = False
    encontrouTipoCobranca = False
    intTipoEndereco = 0
    
    'o for ira pegar apenas um endere�o do tipo Cobran�a e um do tipo Entrega
    For I = 1 To lsvOutEnd.ListItems.Count
     
        'obtem o primeiro caractere da do subitem
        strTipoEndereco = Left(lsvOutEnd.ListItems(I).SubItems(1), 1)
        
        ' seta a variavel do tipo inteiro com o valor da string strTipoEndereco
        If IsNumeric(strTipoEndereco) Then
            intTipoEndereco = CInt(strTipoEndereco)
        Else
            intTipoEndereco = 0
        End If
        
        'verifica se ja encontrou endereco tipo Entrega e se este ja foi adicionado no array
        If (intTipoEndereco = 1 And encontrouTipoEntrega = False) Then
             encontrouTipoEntrega = True
            
                        ' fpallini - CIT - isqa - correcoes
            If oCliente.EnderecoEntrega Is Nothing Then
                oCliente.EnderecoEntrega = New endereco
            End If
             
            oCliente.EnderecoEntrega.TpEndereco = "ZR"
            oCliente.EnderecoEntrega.GrpContaClient2 = "ZPAR"
            oCliente.EnderecoEntrega.rua = lsvOutEnd.ListItems(I).SubItems(6)
            oCliente.EnderecoEntrega.NoResidencia = lsvOutEnd.ListItems(I).SubItems(7)
            oCliente.EnderecoEntrega.complemento = lsvOutEnd.ListItems(I).SubItems(8)
            oCliente.EnderecoEntrega.Bairro = lsvOutEnd.ListItems(I).SubItems(9)
            oCliente.EnderecoEntrega.Cidade = Split(lsvOutEnd.ListItems(I).SubItems(12), " - ")(0)
            'cep
            oCliente.EnderecoEntrega.CEP = lsvOutEnd.ListItems(I).SubItems(10)
            'caixa postal
            oCliente.EnderecoEntrega.CxPostal = lsvOutEnd.ListItems(I).SubItems(16)
            oCliente.EnderecoEntrega.Pais = "BRL"
            oCliente.EnderecoEntrega.Regiao = lsvOutEnd.ListItems(I).SubItems(11)
            oCliente.EnderecoEntrega.DDDFone = lsvOutEnd.ListItems(I).SubItems(13)
            oCliente.EnderecoEntrega.Telefone = lsvOutEnd.ListItems(I).SubItems(14)
            oCliente.EnderecoEntrega.Celular = ""
            oCliente.EnderecoEntrega.DDDFax = lsvOutEnd.ListItems(I).SubItems(13)
            oCliente.EnderecoEntrega.TelFax = lsvOutEnd.ListItems(I).SubItems(15)
            oCliente.EnderecoEntrega.Telex = ""
             
        'verifica se ja encontrou endereco tipo Cobranca
        ElseIf (intTipoEndereco = 2 And encontrouTipoCobranca = False) Then
            encontrouTipoCobranca = True
            
            If oCliente.EnderecoCobranca Is Nothing Then
                oCliente.EnderecoCobranca = New endereco
            End If
            
            oCliente.EnderecoCobranca.TpEndereco = "ZP"
            oCliente.EnderecoCobranca.GrpContaClient2 = "ZPAR"
            oCliente.EnderecoCobranca.rua = lsvOutEnd.ListItems(I).SubItems(6)
            oCliente.EnderecoCobranca.NoResidencia = lsvOutEnd.ListItems(I).SubItems(7)
            oCliente.EnderecoCobranca.complemento = lsvOutEnd.ListItems(I).SubItems(8)
            oCliente.EnderecoCobranca.Bairro = lsvOutEnd.ListItems(I).SubItems(9)
            oCliente.EnderecoCobranca.Cidade = Split(lsvOutEnd.ListItems(I).SubItems(12), " - ")(0)
            'cep
            oCliente.EnderecoCobranca.CEP = lsvOutEnd.ListItems(I).SubItems(10)
            'caixa postal
            oCliente.EnderecoCobranca.CxPostal = lsvOutEnd.ListItems(I).SubItems(16)
            oCliente.EnderecoCobranca.Pais = "BRL"
            oCliente.EnderecoCobranca.Regiao = lsvOutEnd.ListItems(I).SubItems(11)
            oCliente.EnderecoCobranca.DDDFone = lsvOutEnd.ListItems(I).SubItems(13)
            oCliente.EnderecoCobranca.Telefone = lsvOutEnd.ListItems(I).SubItems(14)
            oCliente.EnderecoCobranca.Celular = ""
            oCliente.EnderecoCobranca.DDDFax = lsvOutEnd.ListItems(I).SubItems(13)
            oCliente.EnderecoCobranca.TelFax = lsvOutEnd.ListItems(I).SubItems(15)
            oCliente.EnderecoCobranca.Telex = ""

        End If
        
        'Se ja encontrou os dois tipos de endere�o sai do looping
        If (encontrouTipoCobranca = True) And (encontrouTipoEntrega = True) Then
            Exit For
        End If
        
    Next I

 End Sub

'carlosj 23/11/2012 - ISA 1032
'Carrega os itens no combo Tipo Segmento
Private Sub CarregarComboTipoSegmento()
    
    'se a variavel global vTipoDocto ainda nao tem valor, carrega o combo segmento e seta o valor desta
    If vTipoDocto = "" Then
        Call Carrega_Combo_Segmento_ClienteDPK(cboTpSegmento, Val(Mid(cboTpDocto.Text, 1, 2)))
         'seta variavel global vTipoDocto
         vTipoDocto = Val(Mid(cboTpDocto.Text, 1, 2))
    'verifica se o tipo de documento foi alterado
    ElseIf vTipoDocto <> Val(Mid(cboTpDocto.Text, 1, 2)) Then
        'seta variavel global vTipoDocto
        vTipoDocto = Val(Mid(cboTpDocto.Text, 1, 2))
        cboTpSegmento.Clear
        Call Carrega_Combo_Segmento_ClienteDPK(cboTpSegmento, Val(Mid(cboTpDocto.Text, 1, 2)))
        cboTpCliente.Clear
    End If

End Sub
'TI-3221
Private Function FnCateg(pCateg As String, pTipo As Byte) As String

    FnCateg = ""
    
    If pTipo = 0 Then
        'Faz de/para da categoria
        Select Case pCateg
            Case "A"
                FnCateg = "E"
            Case "B"
                FnCateg = "F"
            Case "C"
                FnCateg = "G"
            Case "D"
                FnCateg = "H"
        End Select
    Else
        'Faz de/para da categoria
        Select Case pCateg
            Case "E"
                FnCateg = "A"
            Case "F"
                FnCateg = "B"
            Case "G"
                FnCateg = "C"
            Case "H"
                FnCateg = "D"
            Case Else
                FnCateg = pCateg
        End Select
    End If
            
End Function
Private Function FnFornVDR(pCateg As String) As Integer

    FnFornVDR = 0
    
    'Faz de/para da categoria
    Select Case pCateg
        Case "A", "B", "C", "D"
            FnFornVDR = 0
        Case "E", "F", "G", "H"
            FnFornVDR = 1
    End Select
            
End Function
Private Sub cmdLimparVdr_Click()
    cboFornVDR.ListIndex = -1
    cboCategBosch.ListIndex = -1
    fraCategBosch.Visible = False
End Sub
'FIM TI-3221
