CREATE OR REPLACE PACKAGE PCK_CAD060 IS

	TYPE TP_CURSOR IS REF CURSOR;

	PROCEDURE PR_COMBOS_GENERICAS
	(
		PM_CURSOR1  IN OUT TP_CURSOR,
		PM_CPO_CONS IN VARCHAR2,
		PM_ERRO     OUT NUMBER,
		PM_MSGERRO  OUT CHAR
	);

	PROCEDURE PR_COMBO_CARACTERISTICA
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_TP_CONS IN NUMBER,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	);

	PROCEDURE PR_COMBO_UF
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_TP_CONS IN NUMBER,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	);

	PROCEDURE PR_COMBO_TPCLIENTEDPK
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_TP_CONS IN NUMBER,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	);

	PROCEDURE PR_COMBO_TPCLIENTEBLAU
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_TP_CONS IN NUMBER,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	);

	PROCEDURE PR_COMBO_REPRES
	(
		PM_CURSOR1  IN OUT TP_CURSOR,
		PM_CPO_CONS IN VARCHAR2,
		PM_ERRO     OUT NUMBER,
		PM_MSGERRO  OUT CHAR
	);

	PROCEDURE PR_COMBO_BANCO
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_TP_CONS IN NUMBER,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	);

	PROCEDURE PR_COMBO_TRANSP
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_TP_CONS IN NUMBER,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	);

	PROCEDURE PR_COMBO_MENSAGEM
	(
		PM_CURSOR1  IN OUT TP_CURSOR,
		PM_CPO_CONS IN VARCHAR2,
		PM_ERRO     OUT NUMBER,
		PM_MSGERRO  OUT CHAR
	);

	PROCEDURE PR_COMBO_CATEGBOSCH
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_TP_CONS IN NUMBER,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	);

	PROCEDURE PR_COMBO_CIDADE
	(
		PM_CURSOR1  IN OUT TP_CURSOR,
		PM_CPO_CONS IN VARCHAR2,
		PM_CODCID   IN PRODUCAO.CIDADE.COD_CIDADE%TYPE,
		PM_TP_CONS  IN VARCHAR2,
		PM_ERRO     OUT NUMBER,
		PM_MSGERRO  OUT CHAR
	);

	PROCEDURE PR_VERIFICA_CLIENTE
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_CODCLI  IN PRODUCAO.CLIENTE.COD_CLIENTE%TYPE,
		PM_CGC     IN PRODUCAO.CLIENTE.CGC%TYPE,
		PM_TP_CONS IN NUMBER,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT VARCHAR2
	);

	PROCEDURE PR_VERIFICA_REPRES
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_CODCLI  IN PRODUCAO.CLIENTE.COD_CLIENTE%TYPE,
		PM_CODREPR IN PRODUCAO.REPRESENTANTE.COD_REPRES%TYPE,
		PM_TP_CONS IN NUMBER,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	);

	PROCEDURE PR_COMBO_CLIEENDERECO
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_TP_CONS IN NUMBER,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	);

	PROCEDURE PR_COMBO_CLIEVENDOR
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_TP_CONS IN NUMBER,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	);

	PROCEDURE PR_CLIEREFBCO
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_CODCLI  IN PRODUCAO.CLIENTE.COD_CLIENTE%TYPE,
		PM_TP_CONS IN NUMBER,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	);

	PROCEDURE PR_CLIEREFER
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_CODCLI  IN PRODUCAO.CLIENTE.COD_CLIENTE%TYPE,
		PM_TP_CONS IN NUMBER,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	);

	PROCEDURE PR_CLIEVENDOR
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_CODCLI  IN PRODUCAO.CLIENTE.COD_CLIENTE%TYPE,
		PM_TP_CONS IN NUMBER,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	);

	PROCEDURE PR_SELECT_VENDOR
	(
		CVENDOR      IN OUT TP_CURSOR,
		PCOD_CLI_CGC IN PRODUCAO.CLIENTE.CGC%TYPE,
		PTABELA      IN NUMBER,
		PM_ERRO      OUT NUMBER,
		PM_MSGERRO   OUT CHAR
	);

	PROCEDURE PR_NOVO_CODCLIENTE
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	);

	PROCEDURE PR_LINHA_PRODUTO
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_CODCLI  IN PRODUCAO.CLIENTE.COD_CLIENTE%TYPE,
		PM_TP_CONS IN NUMBER,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	);

	PROCEDURE PR_DEPOSITO_VISAO
	(
		PM_CURSOR1  IN OUT TP_CURSOR,
		PM_NOME_PRG IN PRODUCAO.DEPOSITO_VISAO.NOME_PROGRAMA%TYPE,
		PM_ERRO     OUT NUMBER,
		PM_MSGERRO  OUT CHAR
	);

	PROCEDURE PR_LOJA_CONEXAO
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_CODLOJA IN PRODUCAO.LOJA.COD_LOJA%TYPE,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	);

	PROCEDURE PR_MAIORSEQ_REFBCO
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_CODCLI  IN PRODUCAO.CLIENTE.COD_CLIENTE%TYPE,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	);

	PROCEDURE PR_SELECT_MOTIVOSRECUSA(PM_CURSOR1 IN OUT TP_CURSOR);

	PROCEDURE PR_INS_UPD_DEL_MOTIVOSRECUSA
	(
		PM_ACAO      IN NUMBER,
		PM_CODIGO    IN NUMBER,
		PM_DESCRICAO IN VARCHAR2,
		PM_CODERRO   OUT NUMBER,
		PM_TXTERRO   OUT VARCHAR2
	);

	PROCEDURE PR_SELECT_CLIEEXPORT
	(
		PM_CURSOR1     IN OUT TP_CURSOR,
		PM_COD_CLIENTE IN PRODUCAO.CLIENTE.COD_CLIENTE%TYPE,
		PM_ERRO        OUT NUMBER,
		PM_MSGERRO     OUT CHAR
	);

	PROCEDURE PR_SELECT_REPRESENTANTE
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_CGC     IN PRODUCAO.CLIENTE.CGC%TYPE
	);

	PROCEDURE PR_SELECT_CLIENTE_VB
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_CGC     IN PRODUCAO.CLIENTE.CGC%TYPE
	);

	PROCEDURE PR_LOGIN_USER
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_LOGIN   IN HELPDESK.USUARIO.LOGIN%TYPE,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	);

	PROCEDURE PR_SELECT_REF_COML_P
	(
		CREFCOML     IN OUT TP_CURSOR,
		PCOD_CLI_CGC IN PRODUCAO.CLIENTE.CGC%TYPE,
		PM_ERRO      OUT NUMBER,
		PM_MSGERRO   OUT CHAR
	);

	PROCEDURE PR_SELECT_REF_BANCO_P
	(
		CREFBCO      IN OUT TP_CURSOR,
		PCOD_CLI_CGC IN PRODUCAO.CLIENTE.CGC%TYPE,
		PM_ERRO      OUT NUMBER,
		PM_MSGERRO   OUT CHAR
	);

	PROCEDURE PR_SELECT_OUT_END_P
	(
		COUTEND      IN OUT TP_CURSOR,
		PCOD_CLI_CGC IN PRODUCAO.CLIENTE.CGC%TYPE,
		PM_ERRO      OUT NUMBER,
		PM_MSGERRO   OUT CHAR
	);

	PROCEDURE PR_SELECT_CLIENTE_RECAD_VB
	(
		CCURSOR1 IN OUT TP_CURSOR,
		PCGC     IN PRODUCAO.CLIENTE.CGC%TYPE
	);

	PROCEDURE PR_VER_PEDIDO_ABERTO
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_CODCLI  IN PRODUCAO.CLIENTE.COD_CLIENTE%TYPE,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	);

	PROCEDURE PR_SELECT_MENSAGEM
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_CODMSG  IN PRODUCAO.CLIE_MENSAGEM.COD_MENSAGEM%TYPE,
		PM_TP_CONS IN NUMBER,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	);

	PROCEDURE PR_SELECT_CONTVDR
	(
		PM_CURSOR1  IN OUT TP_CURSOR,
		PM_CODTPCLI IN VDR.CONTROLE_VDR.COD_TIPO_CLIENTE%TYPE,
		PM_TP_CONS  IN NUMBER,
		PM_ERRO     OUT NUMBER,
		PM_MSGERRO  OUT CHAR
	);

	PROCEDURE PR_SEL_MKTTPCONTATO
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	);

	PROCEDURE PR_SEL_MKTHOBBY
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	);

	PROCEDURE PR_SEL_MKTTIME
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	);

	PROCEDURE PR_SEL_DADOSMKTCONTATO
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_CODCLI  IN PRODUCAO.CLIENTE.COD_CLIENTE%TYPE,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	);

	PROCEDURE PR_SEL_USERINTRANET
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_CODUSER IN HELPDESK.USUARIO.COD_USUARIO%TYPE,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	);

	PROCEDURE PR_SELECT_COD_SOFTWARE
	(
		PM_NOME_SISTEMA IN HELPDESK.SOFTWARE.NOME_SOFTWARE%TYPE,
		PM_COD_SISTEMA  OUT NUMBER
	);

	PROCEDURE PR_SELECT_VL_PARAMETRO
	(
		PM_CURSOR1     IN OUT TP_CURSOR,
		PM_COD_SISTEMA IN NUMBER,
		PM_NOME_PARAM  IN VARCHAR2
	);

	PROCEDURE PR_SELECT_FILIAL
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	);

	PROCEDURE PR_MAXSEQ_REFCOML
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_CODCLI  IN PRODUCAO.CLIENTE.COD_CLIENTE%TYPE,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	);

	PROCEDURE PR_MAXSEQ_REFBCO
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_CODCLI  IN PRODUCAO.CLIENTE.COD_CLIENTE%TYPE,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	);

	PROCEDURE PR_MAXSEQ_OUTEND
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_CODCLI  IN PRODUCAO.CLIENTE.COD_CLIENTE%TYPE,
		PM_TPEND   IN NUMBER,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	);

	PROCEDURE PR_LOG_MAIORSLDEV
	(
		PM_CODUSER     IN NUMBER,
		PM_DTALTER     IN VARCHAR2,
		PM_SLDEV_ANT   IN NUMBER,
		PM_SLDEV_ATU   IN NUMBER,
		PM_DTSLDEV_ANT IN VARCHAR2,
		PM_CODERRO     OUT NUMBER,
		PM_TXTERRO     OUT VARCHAR2
	);

	PROCEDURE PR_SEL_CIDADE
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_NOME    IN VARCHAR2,
		PM_CODCID  IN PRODUCAO.CIDADE.COD_CIDADE%TYPE,
		PM_UF      IN PRODUCAO.CIDADE.COD_UF%TYPE,
		PM_TP_CONS IN VARCHAR2,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	);

	PROCEDURE PR_INSERT_CLIENTE_END_ENTREGA
	(
		PM_CGC          IN CLIENTE.CGC%TYPE,
		PM_TPDOCTO      IN CLIENTE.TP_DOCTO%TYPE,
		PM_NOMECLIENTE  IN CLIENTE.NOME_CLIENTE%TYPE,
		PM_ENDERECO     IN CLIENTE.ENDERECO%TYPE,
		PM_CODCIDADE    IN CLIENTE.COD_CIDADE%TYPE,
		PM_BAIRRO       IN CLIENTE.BAIRRO%TYPE,
		PM_DDD1         IN CLIENTE.DDD1%TYPE,
		PM_FONE1        IN CLIENTE.FONE1%TYPE,
		PM_CEP          IN CLIENTE.CEP%TYPE,
		PM_IE           IN CLIENTE.INSCR_ESTADUAL%TYPE,
		PM_NOMEPROPRIET IN CLIENTE.NOME_PROPRIETARIO%TYPE,
		PM_VERRO        OUT NUMBER,
		PM_VMSGERRO     OUT VARCHAR2
	);

	PROCEDURE PR_GRAVA_END_ENTREGA
	(
		PM_CODCLIENTE     IN PRODUCAO.CLIE_ENDERECO.COD_CLIENTE%TYPE,
		PM_SEQUENCIA      IN PRODUCAO.CLIE_ENDERECO.SEQUENCIA%TYPE,
		PM_ENDERECO       IN PRODUCAO.CLIE_ENDERECO.ENDERECO%TYPE,
		PM_COD_CIDADE     IN PRODUCAO.CLIE_ENDERECO.COD_CIDADE%TYPE,
		PM_BAIRRO         IN PRODUCAO.CLIE_ENDERECO.BAIRRO%TYPE,
		PM_CEP            IN PRODUCAO.CLIE_ENDERECO.CEP%TYPE,
		PM_DDD            IN PRODUCAO.CLIE_ENDERECO.DDD%TYPE,
		PM_FONE           IN PRODUCAO.CLIE_ENDERECO.FONE%TYPE,
		PM_FAX            IN PRODUCAO.CLIE_ENDERECO.FAX%TYPE,
		PM_CXPOSTAL       IN PRODUCAO.CLIE_ENDERECO.CXPOSTAL%TYPE,
		PM_TP_DOCTO       IN PRODUCAO.CLIE_ENDERECO.TP_DOCTO%TYPE,
		PM_CGC            IN PRODUCAO.CLIE_ENDERECO.CGC%TYPE,
		PM_INSCR_ESTADUAL IN PRODUCAO.CLIE_ENDERECO.INSCR_ESTADUAL%TYPE,
		PM_NOME_CLIENTE   IN PRODUCAO.CLIE_ENDERECO.NOME_CLIENTE%TYPE,
		PM_VERRO          OUT NUMBER,
		PM_MSGERRO        OUT VARCHAR2
	);
	PROCEDURE PR_SELECT_TIPO_USUARIO
	(
		PM_CURSOR IN OUT TP_CURSOR,
		PM_CGC    IN NUMBER
	);

	PROCEDURE PR_SELECT_FLVISIT_COMENT
	(
		PM_CURSOR IN OUT TP_CURSOR,
		PM_CGC    IN NUMBER,
		PM_TIPO   IN NUMBER
	);
	PROCEDURE PR_SELECT_FL_VISITOU
	(
		PM_CURSOR IN OUT TP_CURSOR,
		PM_CGC    IN NUMBER
	);
END PCK_CAD060;
/
CREATE OR REPLACE PACKAGE BODY PCK_CAD060 IS

	-- Declarac?o de cariaveis publicas da package
	VAR_SELECT  VARCHAR2(4000) := '';
	VAR_FROM    VARCHAR2(4000) := '';
	VAR_WHERE   VARCHAR2(4000) := '';
	VAR_ORDERBY VARCHAR2(4000) := '';

	--------------------------------------------------------------------------------------------------
	-- PROCEDURE PARA TRAZER OPC?ES PARA COMBOS QUE S?O PREENCHIDOS COM SELECT DISTINCT DE CAMPOS DA TABELA CLIENTE
	--------------------------------------------------------------------------------------------------
	PROCEDURE PR_COMBOS_GENERICAS
	(
		PM_CURSOR1  IN OUT TP_CURSOR,
		PM_CPO_CONS IN VARCHAR2,
		PM_ERRO     OUT NUMBER,
		PM_MSGERRO  OUT CHAR
	) AS
	BEGIN
	
		PM_ERRO    := 0;
		PM_MSGERRO := '';
		VAR_SELECT := 'Select Distinct ';
	
		IF PM_CPO_CONS = 'TPDOC' THEN
		
			VAR_SELECT  := VAR_SELECT || '(Tp_Docto) CAMPO ';
			VAR_ORDERBY := 'Order by Tp_Docto';
		
		ELSIF PM_CPO_CONS = 'STATUS' THEN
		
			VAR_SELECT  := VAR_SELECT || '(Status) CAMPO ';
			VAR_ORDERBY := 'Order by Status';
		
		ELSIF PM_CPO_CONS = 'TPOP' THEN
		
			VAR_SELECT  := VAR_SELECT || '(Tp_Ordem_Pgto) CAMPO ';
			VAR_ORDERBY := 'Order by Tp_Ordem_Pgto';
		
		ELSIF PM_CPO_CONS = 'CHEQUE' THEN
		
			VAR_SELECT  := VAR_SELECT || '(Cheque_SF) CAMPO ';
			VAR_ORDERBY := 'Order by Cheque_SF';
		
		ELSIF PM_CPO_CONS = 'PROTESTO' THEN
		
			VAR_SELECT  := VAR_SELECT || '(Cod_Ctr_Protesto) CAMPO ';
			VAR_ORDERBY := 'Order by Cod_Ctr_Protesto';
		
		ELSIF PM_CPO_CONS = 'CLASSIFICACAO' THEN
		
			VAR_SELECT  := VAR_SELECT || '(Classificacao) CAMPO ';
			VAR_ORDERBY := 'Order by Classificacao';
		
		END IF;
	
		VAR_SELECT := VAR_SELECT || 'From Producao.Cliente ' || VAR_ORDERBY;
	
		OPEN PM_CURSOR1 FOR VAR_SELECT;
	
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			OPEN PM_CURSOR1 FOR
				SELECT DUMMY FROM DUAL;
		WHEN OTHERS THEN
			PM_ERRO    := SQLCODE;
			PM_MSGERRO := SQLERRM;
		
	END PR_COMBOS_GENERICAS;

	--------------------------------------------------------------------------------------------------
	-- PROCEDURE PARA TRAZER OPC?ES PARA COMBO DE CARACTERISTICA DO CLIENTE
	--------------------------------------------------------------------------------------------------
	PROCEDURE PR_COMBO_CARACTERISTICA
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_TP_CONS IN NUMBER,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	) AS
	BEGIN
	
		PM_ERRO    := 0;
		PM_MSGERRO := '';
	
		IF PM_TP_CONS = 0 THEN
			VAR_SELECT := 'Select Caracteristica, Desc_Caracteristica ';
			VAR_SELECT := VAR_SELECT || 'From Producao.Cliente_Caracteristica ';
			VAR_SELECT := VAR_SELECT || 'Order By Caracteristica';
		END IF;
	
		OPEN PM_CURSOR1 FOR VAR_SELECT;
	
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			OPEN PM_CURSOR1 FOR
				SELECT DUMMY FROM DUAL;
		WHEN OTHERS THEN
			PM_ERRO    := SQLCODE;
			PM_MSGERRO := SQLERRM;
		
	END PR_COMBO_CARACTERISTICA;

	--------------------------------------------------------------------------------------------------
	-- PROCEDURE PARA TRAZER OPC?ES PARA COMBO DE UF
	--------------------------------------------------------------------------------------------------
	PROCEDURE PR_COMBO_UF
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_TP_CONS IN NUMBER,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	) AS
	BEGIN
	
		PM_ERRO    := 0;
		PM_MSGERRO := '';
	
		IF PM_TP_CONS = 0 THEN
			VAR_SELECT := 'Select Cod_UF ';
			VAR_SELECT := VAR_SELECT || 'From Producao.UF ';
			VAR_SELECT := VAR_SELECT || 'Order By Cod_UF ';
		END IF;
	
		OPEN PM_CURSOR1 FOR VAR_SELECT;
	
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			OPEN PM_CURSOR1 FOR
				SELECT DUMMY FROM DUAL;
		WHEN OTHERS THEN
			PM_ERRO    := SQLCODE;
			PM_MSGERRO := SQLERRM;
		
	END PR_COMBO_UF;

	--------------------------------------------------------------------------------------------------
	-- PROCEDURE PARA TRAZER OPC?ES PARA COMBOS QUE S?O PREENCHIDOS COM TIPO DE CLIENTE DPK
	--------------------------------------------------------------------------------------------------
	PROCEDURE PR_COMBO_TPCLIENTEDPK
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_TP_CONS IN NUMBER,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	) AS
	BEGIN
	
		PM_ERRO    := 0;
		PM_MSGERRO := '';
	
		IF PM_TP_CONS = 0 THEN
			VAR_SELECT := 'Select lpad(Cod_Tipo_Cli,2,0) Cod_Tipo_Cli , Desc_Tipo_Cli ';
			VAR_SELECT := VAR_SELECT || 'From Producao.Tipo_Cliente ';
			VAR_SELECT := VAR_SELECT || 'Order By Desc_Tipo_Cli';
		END IF;
	
		OPEN PM_CURSOR1 FOR VAR_SELECT;
	
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			OPEN PM_CURSOR1 FOR
				SELECT DUMMY FROM DUAL;
		WHEN OTHERS THEN
			PM_ERRO    := SQLCODE;
			PM_MSGERRO := SQLERRM;
		
	END PR_COMBO_TPCLIENTEDPK;

	--------------------------------------------------------------------------------------------------
	-- PROCEDURE PARA TRAZER OPC?ES PARA COMBOS QUE S?O PREENCHIDOS COM TIPO DE CLIENTE BLAU
	--------------------------------------------------------------------------------------------------
	PROCEDURE PR_COMBO_TPCLIENTEBLAU
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_TP_CONS IN NUMBER,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	) AS
	BEGIN
	
		PM_ERRO    := 0;
		PM_MSGERRO := '';
	
		IF PM_TP_CONS = 0 THEN
			VAR_SELECT := 'Select LPAD(Cod_Tipo_Cli,2,0) Cod_Tipo_Cli, Desc_Tipo_Cli ';
			VAR_SELECT := VAR_SELECT || 'From Producao.Tipo_Cliente_Blau ';
			VAR_SELECT := VAR_SELECT || 'Order By Desc_Tipo_Cli';
		END IF;
	
		OPEN PM_CURSOR1 FOR VAR_SELECT;
	
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			OPEN PM_CURSOR1 FOR
				SELECT DUMMY FROM DUAL;
		WHEN OTHERS THEN
			PM_ERRO    := SQLCODE;
			PM_MSGERRO := SQLERRM;
		
	END PR_COMBO_TPCLIENTEBLAU;

	--------------------------------------------------------------------------------------------------
	-- PROCEDURE PARA TRAZER OPC?ES PARA COMBOS QUE S?O PREENCHIDOS COM REPRESENTANTE DPK E BLAU
	--------------------------------------------------------------------------------------------------
	PROCEDURE PR_COMBO_REPRES
	(
		PM_CURSOR1  IN OUT TP_CURSOR,
		PM_CPO_CONS IN VARCHAR2,
		PM_ERRO     OUT NUMBER,
		PM_MSGERRO  OUT CHAR
	) AS
	BEGIN
	
		PM_ERRO    := 0;
		PM_MSGERRO := '';
	
		--EM 23/12/04, SOLICITAC?O PARA SOMENTE PERMITIR REPRESENTANTE TIPO R OU COD 599
		--OU REPRESENTANTE TIPO M, A OU V CUJA FILIAL SEJA TIPO P OU T - ALETHEA
		--"AND (a.tipo = 'R' or a.cod_repres = 599 or " & _
		--  "(a.tipo in ('M','A','V') and " & _
		--  " b.tp_filial in ('P','T'))) "
	
		/*
        var_SELECT := 'Select R.Nome_Repres, lpad(R.Cod_Repres,4,0) Cod_Repres ';
        var_SELECT := var_SELECT || 'From Producao.Representante R, Producao.Filial F ';
        var_SELECT := var_SELECT || 'Where R.Cod_FILIAL = F.COD_FILIAL ';
        var_SELECT := var_SELECT || '  AND R.Situacao = 0 ';
        var_SELECT := var_SELECT || '  And (R.Tipo = ' || chr(39) || 'R' || chr(39) || ' OR ';
        var_SELECT := var_SELECT || '       R.Cod_Repres = 599 OR ';
        var_SELECT := var_SELECT || '       (R.TIPO IN (' || CHR(39) || 'M' || CHR(30) || ',';
        var_SELECT := var_SELECT || '                   ' || CHR(39) || 'A' || CHR(30) || ',';
        var_SELECT := var_SELECT || '                   ' || CHR(39) || 'V' || CHR(30) || ')';
        var_SELECT := var_SELECT || '        AND F.TP_FILIAL ';
        var_SELECT := var_SELECT || '               IN (' || CHR(39) || 'P' || CHR(30) || ',';
        var_SELECT := var_SELECT || '                   ' || CHR(39) || 'T' || CHR(30) || '))) ';
        var_SELECT := var_SELECT || 'And R.Divisao = ' || chr(39) || pm_CPO_CONS || chr(39) || ' ';
        var_SELECT := var_SELECT || 'Order By R.Nome_Repres';
        */
	
		-- Alterada a consistencia em 24/11/06 - Alethea
		--And (R.Tipo = ''R'' OR R.Cod_Repres = 599 OR
		VAR_SELECT := 'Select R.Nome_Repres,
							  lpad(R.Cod_Repres,4,0) Cod_Repres
						 From Producao.Representante R,
						 	  Producao.Filial F
						Where R.Cod_FILIAL = F.COD_FILIAL
						  AND R.Situacao = 0
						  And (R.Tipo in (''R'',''P'') OR R.Cod_Repres = 599 OR
							   (R.TIPO IN (''M'',''A'',''V'')
								AND F.TP_FILIAL IN (''P'',''T'')))
						  And R.Divisao = ' || CHR(39) || PM_CPO_CONS ||
					  CHR(39) || '
					 Order By R.Nome_Repres';
	
		OPEN PM_CURSOR1 FOR VAR_SELECT;
	
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			OPEN PM_CURSOR1 FOR
				SELECT DUMMY FROM DUAL;
		WHEN OTHERS THEN
			PM_ERRO    := SQLCODE;
			PM_MSGERRO := SQLERRM;
		
	END PR_COMBO_REPRES;

	--------------------------------------------------------------------------------------------------
	-- PROCEDURE PARA TRAZER OPC?ES PARA COMBO QUE E PREENCHIDA COM BANCOS
	--------------------------------------------------------------------------------------------------
	PROCEDURE PR_COMBO_BANCO
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_TP_CONS IN NUMBER,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	) AS
	BEGIN
	
		PM_ERRO    := 0;
		PM_MSGERRO := '';
	
		IF PM_TP_CONS = 0 THEN
			VAR_SELECT := 'Select lpad(Cod_Banco,3,0) Cod_Banco, Sigla ';
			VAR_SELECT := VAR_SELECT || 'From Producao.Banco ';
			VAR_SELECT := VAR_SELECT || 'Where Fl_Venda = ' || CHR(39) || 'V' ||
						  CHR(39) || ' ';
			VAR_SELECT := VAR_SELECT || 'Order By Sigla';
		END IF;
	
		OPEN PM_CURSOR1 FOR VAR_SELECT;
	
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			OPEN PM_CURSOR1 FOR
				SELECT DUMMY FROM DUAL;
		WHEN OTHERS THEN
			PM_ERRO    := SQLCODE;
			PM_MSGERRO := SQLERRM;
		
	END PR_COMBO_BANCO;

	--------------------------------------------------------------------------------------------------
	-- PROCEDURE PARA TRAZER OPC?ES PARA COMBO QUE E PREENCHIDA COM TRANSPORTADORA
	--------------------------------------------------------------------------------------------------
	PROCEDURE PR_COMBO_TRANSP
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_TP_CONS IN NUMBER,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	) AS
	BEGIN
	
		PM_ERRO    := 0;
		PM_MSGERRO := '';
	
		IF PM_TP_CONS = 0 THEN
			VAR_SELECT := 'Select lpad(Cod_Transp,4,0) Cod_Transp, Nome_Transp ';
			VAR_SELECT := VAR_SELECT || 'From Producao.Transportadora ';
			VAR_SELECT := VAR_SELECT || 'Where Situacao = 0 ';
			VAR_SELECT := VAR_SELECT || 'Order By Nome_Transp';
		END IF;
	
		OPEN PM_CURSOR1 FOR VAR_SELECT;
	
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			OPEN PM_CURSOR1 FOR
				SELECT DUMMY FROM DUAL;
		WHEN OTHERS THEN
			PM_ERRO    := SQLCODE;
			PM_MSGERRO := SQLERRM;
		
	END PR_COMBO_TRANSP;

	--------------------------------------------------------------------------------------------------
	-- PROCEDURE PARA TRAZER OPC?ES PARA COMBOS QUE SER?O PREENCHIDOS COM MENSAGEM DE CLIENTES E FISCAL
	--------------------------------------------------------------------------------------------------
	PROCEDURE PR_COMBO_MENSAGEM
	(
		PM_CURSOR1  IN OUT TP_CURSOR,
		PM_CPO_CONS IN VARCHAR2,
		PM_ERRO     OUT NUMBER,
		PM_MSGERRO  OUT CHAR
	) AS
	BEGIN
	
		PM_ERRO    := 0;
		PM_MSGERRO := '';
	
		IF PM_CPO_CONS = 'C' THEN
			VAR_SELECT := 'Select LPAD(Cod_Mensagem,2,0) Cod_Mensagem, Desc_Mens ';
			VAR_SELECT := VAR_SELECT || 'From Producao.Clie_Mensagem ';
			VAR_SELECT := VAR_SELECT || 'Where Cod_Mensagem NOT IN (53,56) ';
		ELSIF PM_CPO_CONS = 'F' THEN
			VAR_SELECT := 'Select Cod_Mensagem, Desc_Mens ';
			VAR_SELECT := VAR_SELECT || 'From Producao.Clie_Mensagem ';
			VAR_SELECT := VAR_SELECT || 'Where Cod_Mensagem IN (53,56) ';
		END IF;
	
		VAR_SELECT := VAR_SELECT || 'Order By Desc_Mens';
	
		OPEN PM_CURSOR1 FOR VAR_SELECT;
	
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			OPEN PM_CURSOR1 FOR
				SELECT DUMMY FROM DUAL;
		WHEN OTHERS THEN
			PM_ERRO    := SQLCODE;
			PM_MSGERRO := SQLERRM;
		
	END PR_COMBO_MENSAGEM;

	--------------------------------------------------------------------------------------------------
	-- PROCEDURE PARA TRAZER OPC?ES PARA COMBO QUE E PREENCHIDA COM CATEGORIA BOSCH
	--------------------------------------------------------------------------------------------------
	PROCEDURE PR_COMBO_CATEGBOSCH
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_TP_CONS IN NUMBER,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	) AS
	BEGIN
	
		PM_ERRO    := 0;
		PM_MSGERRO := '';
	
		IF PM_TP_CONS = 0 THEN
			VAR_SELECT := 'Select Categoria ';
			VAR_SELECT := VAR_SELECT || 'From VDR.Desconto_Categ ';
			VAR_SELECT := VAR_SELECT || 'Order By Categoria';
		END IF;
	
		OPEN PM_CURSOR1 FOR VAR_SELECT;
	
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			OPEN PM_CURSOR1 FOR
				SELECT DUMMY FROM DUAL;
		WHEN OTHERS THEN
			PM_ERRO    := SQLCODE;
			PM_MSGERRO := SQLERRM;
		
	END PR_COMBO_CATEGBOSCH;

	--------------------------------------------------------------------------------------------------
	-- PROCEDURE PARA TRAZER OPC?ES PARA COMBO QUE E PREENCHIDA COM CIDADES
	--------------------------------------------------------------------------------------------------
	PROCEDURE PR_COMBO_CIDADE
	(
		PM_CURSOR1  IN OUT TP_CURSOR,
		PM_CPO_CONS IN VARCHAR2,
		PM_CODCID   IN PRODUCAO.CIDADE.COD_CIDADE%TYPE,
		PM_TP_CONS  IN VARCHAR2,
		PM_ERRO     OUT NUMBER,
		PM_MSGERRO  OUT CHAR
	) AS
	BEGIN
	
		PM_ERRO    := 0;
		PM_MSGERRO := '';
	
		IF PM_TP_CONS = 0 THEN
		
			VAR_SELECT := 'Select LPAD(Cod_Cidade,4,0) Cod_Cidade, Nome_Cidade ';
			VAR_SELECT := VAR_SELECT || 'From Producao.Cidade ';
			VAR_SELECT := VAR_SELECT || 'Where Cod_Uf = ' || CHR(39) ||
						  PM_CPO_CONS || CHR(39) || ' ';
			VAR_SELECT := VAR_SELECT || 'Order By Nome_Cidade';
		
		END IF;
	
		OPEN PM_CURSOR1 FOR VAR_SELECT;
	
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			OPEN PM_CURSOR1 FOR
				SELECT DUMMY FROM DUAL;
		WHEN OTHERS THEN
			PM_ERRO    := SQLCODE;
			PM_MSGERRO := SQLERRM;
		
	END PR_COMBO_CIDADE;

	--------------------------------------------------------------------------------------------------
	-- PROCEDURE PARA TRAZER DADOS DO CLIENTE
	--------------------------------------------------------------------------------------------------
	PROCEDURE PR_VERIFICA_CLIENTE
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_CODCLI  IN PRODUCAO.CLIENTE.COD_CLIENTE%TYPE,
		PM_CGC     IN PRODUCAO.CLIENTE.CGC%TYPE,
		PM_TP_CONS IN NUMBER,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT VARCHAR2
	) AS
	BEGIN
	
		PM_ERRO    := 0;
		PM_MSGERRO := '';
		VAR_SELECT := '';
		VAR_FROM   := '';
		VAR_WHERE  := '';
	
		IF PM_TP_CONS = 0 THEN
			VAR_SELECT := 'Select Cod_Cliente, Nome_Cliente ';
			VAR_SELECT := VAR_SELECT || 'From Producao.Cliente ';
			VAR_SELECT := VAR_SELECT || 'Where CGC = ' || PM_CGC || ' ';
			IF PM_CODCLI <> 0 THEN
				VAR_SELECT := VAR_SELECT || 'And Cod_Cliente <> ' || PM_CODCLI || ' ';
			END IF;
		
			-- Traz a maioria das informac?es do cadastro do cliente
		ELSIF PM_TP_CONS = 1 THEN
		
			VAR_SELECT := 'SELECT C.Cod_Cliente, C.Nome_Cliente, ';
			VAR_SELECT := VAR_SELECT || 'TO_CHAR(C.Dt_Cadastr,' || CHR(39) ||
						  'DD/MM/YY' || CHR(39) || ') Dt_Cadastro, ';
			VAR_SELECT := VAR_SELECT || 'TO_CHAR(C.Dt_Recadastro,' || CHR(39) ||
						  'DD/MM/YY' || CHR(39) || ') Dt_Recadastro, ';
			VAR_SELECT := VAR_SELECT || 'C.Situacao, C.TP_EMPRESA, ';
			VAR_SELECT := VAR_SELECT ||
						  'C.Tp_docto, C.cgc, C.Inscr_Estadual, C.Status, C.Caracteristica, ';
			VAR_SELECT := VAR_SELECT ||
						  'CCA.Desc_Caracteristica, C.Endereco, C.Bairro, C.Cep, CID.Cod_Uf, ';
			VAR_SELECT := VAR_SELECT ||
						  'CID.Nome_Cidade, C.Cod_Cidade, C.DDD1, C.Fone1, C.Ddd2, C.Fone2, ';
			VAR_SELECT := VAR_SELECT ||
						  'C.Fax, C.Nome_Contato, C.Cxpostal, C.Telex, M.E_Mail, M.HomePage, ';
			VAR_SELECT := VAR_SELECT || 'NVL(C.Cod_Tipo_Cliente,' || CHR(39) || '' ||
						  CHR(39) || ') Cod_Tipo_Cliente, ';
			VAR_SELECT := VAR_SELECT ||
						  'TC.Desc_Tipo_Cli TipoCliDPK, C.Cod_Repr_Vend, ';
			VAR_SELECT := VAR_SELECT ||
						  'C.Cod_Tipo_Clblau, TCB.Desc_Tipo_Cli TipoCliBlau, C.Cod_Repres_Blau, ';
			VAR_SELECT := VAR_SELECT ||
						  'C.Cod_Banco, B.Sigla, C.Tp_Ordem_Pgto, C.Cheque_Sf, ';
			VAR_SELECT := VAR_SELECT ||
						  'C.Cod_Ctr_Protesto, C.Classificacao, C.Cod_Transp, T.Nome_Transp, ';
			VAR_SELECT := VAR_SELECT || 'C.Cod_Mensagem, CM.Desc_Mens, ';
			VAR_SELECT := VAR_SELECT || 'TO_CHAR(C.Dt_Mensagem,' || CHR(39) ||
						  'DD/MM/YY' || CHR(39) || ') AS DataMSG, ';
			VAR_SELECT := VAR_SELECT ||
						  'C.Nome_Respons, C.Cod_Mensagem_Fiscal CodMsgFis, CMF.Desc_Mens MsgFis, ';
			VAR_SELECT := VAR_SELECT ||
						  'C.Nome_Proprietario, C.Propriedade, C.Fl_Cons_final, EXP.Cod_Cliente CliExp, ';
			VAR_SELECT := VAR_SELECT || 'TO_CHAR(C.Dt_Fundacao,' || CHR(39) ||
						  'DD/MM/YY' || CHR(39) || ') Dt_Fundacao, ';
			VAR_SELECT := VAR_SELECT || 'CC.Maior_Saldev, ';
			VAR_SELECT := VAR_SELECT || 'TO_CHAR(CC.Dt_Maior_Saldev,' ||
						  CHR(39) || 'DD/MM/YY' || CHR(39) ||
						  ') Dt_Maior_Saldev, ';
			-- Novos campos da CLIE_CREDITO - Alethea - 11/07/05
			VAR_SELECT := VAR_SELECT || 'Nvl(CC.Limite,0) Limite, ';
			VAR_SELECT := VAR_SELECT || 'TO_CHAR(CC.Dt_Final_Limite,' ||
						  CHR(39) || 'DD/MM/YY' || CHR(39) || ') Dt_Limite, ';
			VAR_SELECT := VAR_SELECT ||
						  'Nvl(CC.Vl_Previsao_Compra,0) Vl_Prev_Cpra, ';
			--
			VAR_SELECT := VAR_SELECT || 'C.Inscr_Suframa, C.Cod_Tare, ';
			VAR_SELECT := VAR_SELECT || 'TO_CHAR(C.Dt_Tare,' || CHR(39) ||
						  'DD/MM/YY' || CHR(39) || ') Dt_Tare, ';
			VAR_SELECT := VAR_SELECT ||
						  'CREDEN.Fl_Creden, VDR.categoria, CC.Saldo_Pedidos ';
		
			VAR_FROM := VAR_FROM || 'FROM Producao.Cliente C, ';
			VAR_FROM := VAR_FROM || '     Producao.Cidade CID, ';
			VAR_FROM := VAR_FROM || '     Producao.Tipo_Cliente TC, ';
			VAR_FROM := VAR_FROM || '     Producao.Tipo_Cliente_Blau TCB, ';
			VAR_FROM := VAR_FROM || '     Producao.Clie_Creden CREDEN, ';
			VAR_FROM := VAR_FROM || '     Producao.Clie_Credito CC, ';
			VAR_FROM := VAR_FROM || '     Producao.Clie_Mensagem CM, ';
			VAR_FROM := VAR_FROM || '     Producao.Clie_Mensagem CMF, ';
			VAR_FROM := VAR_FROM || '     Producao.Transportadora T, ';
			VAR_FROM := VAR_FROM || '     Producao.Banco B, ';
			VAR_FROM := VAR_FROM || '     Vdr.Cliente_Categ_Vdr VDR, ';
			VAR_FROM := VAR_FROM || '     Marketing.Cliente_Internet M, ';
			VAR_FROM := VAR_FROM ||
						'     Producao.Cliente_Caracteristica CCA, ';
			VAR_FROM := VAR_FROM || '     Producao.Cliente_Exportacao EXP ';
		
			VAR_WHERE := VAR_WHERE || 'WHERE C.Cod_Cliente = ' || PM_CODCLI || ' ';
			VAR_WHERE := VAR_WHERE ||
						 'AND CM.Cod_Mensagem(+) = C.Cod_Mensagem ';
			VAR_WHERE := VAR_WHERE ||
						 'AND CMF.Cod_Mensagem(+) = C.Cod_Mensagem_Fiscal ';
			VAR_WHERE := VAR_WHERE ||
						 'AND TC.Cod_Tipo_Cli(+) = C.Cod_Tipo_Cliente ';
			VAR_WHERE := VAR_WHERE ||
						 'AND TCB.Cod_Tipo_Cli(+) = C.Cod_Tipo_Clblau ';
			VAR_WHERE := VAR_WHERE || 'AND T.Cod_Transp(+) = C.Cod_Transp ';
			VAR_WHERE := VAR_WHERE || 'AND B.Cod_Banco(+) = C.Cod_Banco ';
			VAR_WHERE := VAR_WHERE || 'AND CID.Cod_Cidade = C.Cod_Cidade ';
			VAR_WHERE := VAR_WHERE || 'AND CC.Cod_Cliente(+) = C.Cod_Cliente ';
			VAR_WHERE := VAR_WHERE ||
						 'AND CREDEN.Cod_Cliente(+) = C.Cod_Cliente ';
			VAR_WHERE := VAR_WHERE || 'AND VDR.Cod_Cliente(+) = C.Cod_Cliente ';
			VAR_WHERE := VAR_WHERE || 'AND M.Cod_Cliente(+) = C.Cod_Cliente ';
			VAR_WHERE := VAR_WHERE ||
						 'AND CCA.Caracteristica = C.Caracteristica ';
			VAR_WHERE := VAR_WHERE || 'AND EXP.Cod_Cliente(+) = C.Cod_Cliente ';
			VAR_WHERE := VAR_WHERE || 'AND C.Cod_Cliente = CC.Cod_Cliente(+)';
		
		ELSIF PM_TP_CONS = 2 THEN
		
			VAR_SELECT := 'Select * ';
			VAR_SELECT := VAR_SELECT || 'From Producao.Cliente ';
			VAR_SELECT := VAR_SELECT || 'Where Cod_Cliente = ' || PM_CODCLI;
		
		END IF;
	
		OPEN PM_CURSOR1 FOR VAR_SELECT || VAR_FROM || VAR_WHERE;
	
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			OPEN PM_CURSOR1 FOR
				SELECT DUMMY FROM DUAL;
		WHEN OTHERS THEN
			PM_ERRO    := SQLCODE;
			PM_MSGERRO := SQLERRM;
		
	END PR_VERIFICA_CLIENTE;

	--------------------------------------------------------------------------------------------------
	-- PROCEDURE PARA SELECTS QUE TRAZEM INFORMAC?ES DE REPRESENTANTE
	--------------------------------------------------------------------------------------------------
	PROCEDURE PR_VERIFICA_REPRES
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_CODCLI  IN PRODUCAO.CLIENTE.COD_CLIENTE%TYPE,
		PM_CODREPR IN PRODUCAO.REPRESENTANTE.COD_REPRES%TYPE,
		PM_TP_CONS IN NUMBER,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	) AS
	BEGIN
	
		PM_ERRO    := 0;
		PM_MSGERRO := '';
		VAR_SELECT := '';
	
		-- Select que traz informac?es do Representante para saber se cliente pode ser Tare
		IF PM_TP_CONS = 0 THEN
		
			/*var_SELECT := 'Select Cod_Filial ';
            var_SELECT := var_SELECT || 'From Producao.Representante R, Producao.R_Clie_Repres C ';
            var_SELECT := var_SELECT || 'Where Cod_Cliente = ' || pm_CODCLI || ' ';
            var_SELECT := var_SELECT || 'And Cod_Filial <> 26 AND Cod_Filial <> 135 ';
            var_SELECT := var_SELECT || 'And C.Cod_Repres = R.Cod_Repres ';
            */
		
			-- A partir de Set/05 foi solicitado que permitisse Tare tambem para DF (DF e GO) - Solic. Fiscal - Alethea
			VAR_SELECT := 'Select Cod_Filial ';
			VAR_SELECT := VAR_SELECT ||
						  'From Producao.Representante R, Producao.R_Clie_Repres C ';
			VAR_SELECT := VAR_SELECT || 'Where Cod_Cliente = ' || PM_CODCLI || ' ';
			VAR_SELECT := VAR_SELECT ||
						  'And Cod_Filial <> 26 AND Cod_Filial <> 135 AND Cod_Filial <> 140 ';
			VAR_SELECT := VAR_SELECT ||
						  'And Cod_Filial <> 21 AND Cod_Filial <> 143 AND Cod_Filial <> 186 ';
			VAR_SELECT := VAR_SELECT || 'And C.Cod_Repres = R.Cod_Repres ';
		
			OPEN PM_CURSOR1 FOR VAR_SELECT;
		
			-- Select que traz dados do representante e de sua filial
		ELSIF PM_TP_CONS = 1 THEN
		
			OPEN PM_CURSOR1 FOR
				SELECT C.NOME_REPRES,
					   LPAD(C.COD_REPRES,
							4,
							0) COD_REPRES,
					   B.COD_FRANQUEADOR,
					   A.SEQ_FRANQUIA,
					   A.COD_FILIAL
				  FROM PRODUCAO.REPRESENTANTE A,
					   PRODUCAO.FILIAL        B,
					   PRODUCAO.REPRESENTANTE C
				 WHERE A.COD_REPRES = PM_CODREPR
				   AND A.COD_FILIAL = B.COD_FILIAL
				   AND C.COD_REPRES = DECODE(A.SEQ_FRANQUIA,
											 0,
											 A.COD_REPRES,
											 B.COD_FRANQUEADOR);
		
			-- Select que traz as informac?es da carteira de cliente do representante
		ELSIF PM_TP_CONS = 2 THEN
		
			VAR_SELECT := 'Select * ';
			VAR_SELECT := VAR_SELECT || 'FROM Producao.R_Clie_Repres ';
			VAR_SELECT := VAR_SELECT || 'WHERE Cod_Repres <> 0 ';
			VAR_SELECT := VAR_SELECT || 'AND Cod_Cliente = ' || PM_CODCLI || ' ';
		
			OPEN PM_CURSOR1 FOR VAR_SELECT;
		
			-- Select que traz todas as informac?es do cliente
		ELSIF PM_TP_CONS = 3 THEN
		
			VAR_SELECT := 'SELECT * ';
			VAR_SELECT := VAR_SELECT || 'FROM Producao.Representante ';
			VAR_SELECT := VAR_SELECT || 'WHERE Cod_Repres = ' || PM_CODREPR || ' ';
		
			OPEN PM_CURSOR1 FOR VAR_SELECT;
		
		END IF;
	
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			OPEN PM_CURSOR1 FOR
				SELECT DUMMY FROM DUAL;
		WHEN OTHERS THEN
			PM_ERRO    := SQLCODE;
			PM_MSGERRO := SQLERRM;
		
	END PR_VERIFICA_REPRES;

	--------------------------------------------------------------------------------------------------
	-- PROCEDURE PARA TRAZER OPC?ES PARA COMBOS DE CLIE_ENDERECO
	--------------------------------------------------------------------------------------------------
	PROCEDURE PR_COMBO_CLIEENDERECO
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_TP_CONS IN NUMBER,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	) AS
	BEGIN
	
		PM_ERRO    := 0;
		PM_MSGERRO := '';
	
		IF PM_TP_CONS = 0 THEN
			VAR_SELECT := 'Select Distinct Tp_Endereco Tp_Endereco_Cod, Decode(Tp_Endereco,1,' ||
						  CHR(39) || 'ENTREGA' || CHR(39) || ',' || CHR(39) ||
						  'COBRANCA' || CHR(39) || ') Tp_Endereco ';
			VAR_SELECT := VAR_SELECT || 'From Producao.Clie_Endereco ';
			VAR_SELECT := VAR_SELECT || 'Order By Tp_Endereco_Cod';
		ELSIF PM_TP_CONS = 1 THEN
			VAR_SELECT := 'Select Distinct Tp_Docto Tp_Docto_Cod, Decode(Tp_Docto,1,' ||
						  CHR(39) || 'Pessoa Juridica' || CHR(39) || ',' ||
						  CHR(39) || 'Pessoa Fisica' || CHR(39) ||
						  ') Tp_Docto ';
			VAR_SELECT := VAR_SELECT || 'From Producao.Clie_Endereco ';
			VAR_SELECT := VAR_SELECT || 'Order By Tp_Docto_Cod';
		END IF;
	
		OPEN PM_CURSOR1 FOR VAR_SELECT;
	
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			OPEN PM_CURSOR1 FOR
				SELECT DUMMY FROM DUAL;
		WHEN OTHERS THEN
			PM_ERRO    := SQLCODE;
			PM_MSGERRO := SQLERRM;
		
	END PR_COMBO_CLIEENDERECO;

	--------------------------------------------------------------------------------------------------
	-- PROCEDURE PARA TRAZER OPC?ES PARA COMBOS DE CLIENTE_VENDOR
	--------------------------------------------------------------------------------------------------
	PROCEDURE PR_COMBO_CLIEVENDOR
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_TP_CONS IN NUMBER,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	) AS
	BEGIN
	
		PM_ERRO    := 0;
		PM_MSGERRO := '';
	
		IF PM_TP_CONS = 0 THEN
			VAR_SELECT := 'Select Distinct Situacao ';
			VAR_SELECT := VAR_SELECT || 'From Producao.Cliente_Vendor ';
			VAR_SELECT := VAR_SELECT || 'Order By Situacao';
		END IF;
	
		OPEN PM_CURSOR1 FOR VAR_SELECT;
	
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			OPEN PM_CURSOR1 FOR
				SELECT DUMMY FROM DUAL;
		WHEN OTHERS THEN
			PM_ERRO    := SQLCODE;
			PM_MSGERRO := SQLERRM;
		
	END PR_COMBO_CLIEVENDOR;

	--------------------------------------------------------------------------------------------------
	-- PROCEDURE PARA TRAZER DADOS DA CLIE_REFBCO
	--------------------------------------------------------------------------------------------------
	PROCEDURE PR_CLIEREFBCO
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_CODCLI  IN PRODUCAO.CLIENTE.COD_CLIENTE%TYPE,
		PM_TP_CONS IN NUMBER,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	) AS
	BEGIN
	
		PM_ERRO    := 0;
		PM_MSGERRO := '';
	
		IF PM_TP_CONS = 0 THEN
			VAR_SELECT := 'Select * ';
			VAR_SELECT := VAR_SELECT || 'From Producao.Clie_RefBco ';
			VAR_SELECT := VAR_SELECT || 'Where Cod_Cliente = ' || PM_CODCLI || ' ';
			VAR_SELECT := VAR_SELECT || 'Order by Sequencia ';
		END IF;
	
		OPEN PM_CURSOR1 FOR VAR_SELECT;
	
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			OPEN PM_CURSOR1 FOR
				SELECT DUMMY FROM DUAL;
		WHEN OTHERS THEN
			PM_ERRO    := SQLCODE;
			PM_MSGERRO := SQLERRM;
		
	END PR_CLIEREFBCO;

	--------------------------------------------------------------------------------------------------
	-- PROCEDURE PARA TRAZER DADOS DA CLIE_REFER
	--------------------------------------------------------------------------------------------------
	PROCEDURE PR_CLIEREFER
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_CODCLI  IN PRODUCAO.CLIENTE.COD_CLIENTE%TYPE,
		PM_TP_CONS IN NUMBER,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	) AS
	BEGIN
	
		PM_ERRO    := 0;
		PM_MSGERRO := '';
	
		IF PM_TP_CONS = 0 THEN
			VAR_SELECT := 'Select * ';
			VAR_SELECT := VAR_SELECT || 'From Producao.Clie_Refer ';
			VAR_SELECT := VAR_SELECT || 'Where Cod_Cliente = ' || PM_CODCLI || ' ';
			VAR_SELECT := VAR_SELECT || 'Order by Sequencia ';
		END IF;
	
		OPEN PM_CURSOR1 FOR VAR_SELECT;
	
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			OPEN PM_CURSOR1 FOR
				SELECT DUMMY FROM DUAL;
		WHEN OTHERS THEN
			PM_ERRO    := SQLCODE;
			PM_MSGERRO := SQLERRM;
		
	END PR_CLIEREFER;

	--------------------------------------------------------------------------------------------------
	-- PROCEDURE PARA TRAZER DADOS DA CLIENTE_VENDOR
	--------------------------------------------------------------------------------------------------
	PROCEDURE PR_CLIEVENDOR
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_CODCLI  IN PRODUCAO.CLIENTE.COD_CLIENTE%TYPE,
		PM_TP_CONS IN NUMBER,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	) AS
	BEGIN
	
		PM_ERRO    := 0;
		PM_MSGERRO := '';
	
		IF PM_TP_CONS = 0 THEN
			VAR_SELECT := 'Select * ';
			VAR_SELECT := VAR_SELECT || 'From Producao.Cliente_Vendor ';
			VAR_SELECT := VAR_SELECT || 'Where Cod_Cliente = ' || PM_CODCLI || ' ';
			VAR_SELECT := VAR_SELECT || 'Order by Num_Contrato ';
		END IF;
	
		OPEN PM_CURSOR1 FOR VAR_SELECT;
	
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			OPEN PM_CURSOR1 FOR
				SELECT DUMMY FROM DUAL;
		WHEN OTHERS THEN
			PM_ERRO    := SQLCODE;
			PM_MSGERRO := SQLERRM;
		
	END PR_CLIEVENDOR;

	--------------------------------------------------------------------------------------------------
	-- PROCEDURE PARA TRAZER INFOR. CLIENTE VENDOR
	--------------------------------------------------------------------------------------------------
	--Se a Tabela for igual a 1 ent?o Producao.cliente_vendor
	PROCEDURE PR_SELECT_VENDOR
	(
		CVENDOR      IN OUT TP_CURSOR,
		PCOD_CLI_CGC IN PRODUCAO.CLIENTE.CGC%TYPE,
		PTABELA      IN NUMBER,
		PM_ERRO      OUT NUMBER,
		PM_MSGERRO   OUT CHAR
	) AS
	BEGIN
	
		PM_ERRO    := 0;
		PM_MSGERRO := '';
	
		IF PTABELA = 1 THEN
			OPEN CVENDOR FOR
				SELECT B.*
				  FROM PRODUCAO.CLIENTE        C,
					   PRODUCAO.CLIENTE_VENDOR B
				 WHERE C.CGC = PCOD_CLI_CGC
				   AND C.COD_CLIENTE = B.COD_CLIENTE
				 ORDER BY B.NUM_CONTRATO;
		END IF;
	
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			OPEN CVENDOR FOR
				SELECT DUMMY FROM DUAL;
		WHEN OTHERS THEN
			PM_ERRO    := SQLCODE;
			PM_MSGERRO := SQLERRM;
		
	END PR_SELECT_VENDOR;

	--------------------------------------------------------------------------------------------------
	-- PROCEDURE PARA PEGAR UM NOVO CODIGO DE CLIENTE
	--------------------------------------------------------------------------------------------------
	PROCEDURE PR_NOVO_CODCLIENTE
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	) AS
	BEGIN
	
		PM_ERRO    := 0;
		PM_MSGERRO := '';
	
		OPEN PM_CURSOR1 FOR
			SELECT SEQ_CLIENTE.NEXTVAL CODCLI FROM DUAL;
	
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			OPEN PM_CURSOR1 FOR
				SELECT DUMMY FROM DUAL;
		WHEN OTHERS THEN
			PM_ERRO    := SQLCODE;
			PM_MSGERRO := SQLERRM;
		
	END PR_NOVO_CODCLIENTE;

	--------------------------------------------------------------------------------------------------
	-- PROCEDURE PARA PEGAR LINHAS PRODUTO VDR
	--------------------------------------------------------------------------------------------------
	PROCEDURE PR_LINHA_PRODUTO
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_CODCLI  IN PRODUCAO.CLIENTE.COD_CLIENTE%TYPE,
		PM_TP_CONS IN NUMBER,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	) AS
	BEGIN
	
		PM_ERRO    := 0;
		PM_MSGERRO := '';
	
		IF PM_TP_CONS = 0 THEN
			VAR_SELECT := 'Select * From Producao.Linha_Produto ';
			VAR_SELECT := VAR_SELECT || 'Order By Linha_Produto';
		
		ELSIF PM_TP_CONS = 1 THEN
		
			VAR_SELECT := 'Select l.linha_produto as linha, desc_linha ';
			VAR_SELECT := VAR_SELECT || 'FROM VDR.R_Clie_Linha_Produto V, ';
			VAR_SELECT := VAR_SELECT || 'Producao.Linha_Produto L ';
			VAR_SELECT := VAR_SELECT || 'WHERE V.Cod_Cliente = ' || PM_CODCLI || ' ';
			VAR_SELECT := VAR_SELECT ||
						  'AND V.Linha_Produto = L.Linha_Produto ';
			VAR_SELECT := VAR_SELECT || 'ORDER BY L.Linha_Produto ';
		END IF;
	
		OPEN PM_CURSOR1 FOR VAR_SELECT;
	
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			OPEN PM_CURSOR1 FOR
				SELECT DUMMY FROM DUAL;
		WHEN OTHERS THEN
			PM_ERRO    := SQLCODE;
			PM_MSGERRO := SQLERRM;
		
	END PR_LINHA_PRODUTO;

	--------------------------------------------------------------------------------------------------
	-- PROCEDURE PARA SELECIONAR LOJAS DA DEPOSITO_VISAO
	--------------------------------------------------------------------------------------------------
	PROCEDURE PR_DEPOSITO_VISAO
	(
		PM_CURSOR1  IN OUT TP_CURSOR,
		PM_NOME_PRG IN PRODUCAO.DEPOSITO_VISAO.NOME_PROGRAMA%TYPE,
		PM_ERRO     OUT NUMBER,
		PM_MSGERRO  OUT CHAR
	) AS
	BEGIN
	
		PM_ERRO    := 0;
		PM_MSGERRO := '';
	
		OPEN PM_CURSOR1 FOR
			SELECT D.COD_LOJA,
				   L.NOME_FANTASIA
			  FROM PRODUCAO.LOJA           L,
				   PRODUCAO.DEPOSITO_VISAO D
			 WHERE L.COD_LOJA = D.COD_LOJA
			   AND D.NOME_PROGRAMA = PM_NOME_PRG;
	
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			OPEN PM_CURSOR1 FOR
				SELECT DUMMY FROM DUAL;
		WHEN OTHERS THEN
			PM_ERRO    := SQLCODE;
			PM_MSGERRO := SQLERRM;
		
	END PR_DEPOSITO_VISAO;

	--------------------------------------------------------------------------------------------------
	-- PROCEDURE PARA VERIFICAR TIMEOUT DA LOJA_CONEXAO
	--------------------------------------------------------------------------------------------------
	PROCEDURE PR_LOJA_CONEXAO
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_CODLOJA IN PRODUCAO.LOJA.COD_LOJA%TYPE,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	) AS
	BEGIN
	
		PM_ERRO    := 0;
		PM_MSGERRO := '';
	
		OPEN PM_CURSOR1 FOR
			SELECT * FROM PRODUCAO.LOJA_CONEXAO WHERE COD_LOJA = PM_CODLOJA;
	
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			OPEN PM_CURSOR1 FOR
				SELECT DUMMY FROM DUAL;
		WHEN OTHERS THEN
			PM_ERRO    := SQLCODE;
			PM_MSGERRO := SQLERRM;
		
	END PR_LOJA_CONEXAO;

	--------------------------------------------------------------------------------------------------
	-- PROCEDURE PARA SELECIONAR A MAIOR SEQUENCIA DAS REFERENCIAS BANCARIAS JA CAD. P/ CLIENTE
	--------------------------------------------------------------------------------------------------
	PROCEDURE PR_MAIORSEQ_REFBCO
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_CODCLI  IN PRODUCAO.CLIENTE.COD_CLIENTE%TYPE,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	) AS
	BEGIN
		PM_ERRO    := 0;
		PM_MSGERRO := '';
	
		OPEN PM_CURSOR1 FOR
			SELECT MAX(SEQUENCIA)
			  FROM PRODUCAO.CLIE_REFBCO
			 WHERE COD_CLIENTE = PM_CODCLI;
	
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			OPEN PM_CURSOR1 FOR
				SELECT DUMMY FROM DUAL;
		WHEN OTHERS THEN
			PM_ERRO    := SQLCODE;
			PM_MSGERRO := SQLERRM;
		
	END PR_MAIORSEQ_REFBCO;

	--------------------------------------------------------------------------------------------------
	-- PROCEDURE PARA SELECIONAR MOTIVOS DE RECUSA DO CLIENTE
	--------------------------------------------------------------------------------------------------
	PROCEDURE PR_SELECT_MOTIVOSRECUSA(PM_CURSOR1 IN OUT TP_CURSOR) AS
	BEGIN
		OPEN PM_CURSOR1 FOR
			SELECT COD_MOTIVO,
				   DESCRICAO
			  FROM CREDITO.MOTIVO_RECUSA
			 ORDER BY COD_MOTIVO;
	END PR_SELECT_MOTIVOSRECUSA;

	PROCEDURE PR_INS_UPD_DEL_MOTIVOSRECUSA
	(
		PM_ACAO      IN NUMBER,
		PM_CODIGO    IN NUMBER,
		PM_DESCRICAO IN VARCHAR2,
		PM_CODERRO   OUT NUMBER,
		PM_TXTERRO   OUT VARCHAR2
	) AS
	BEGIN
		--Insert
		IF PM_ACAO = 1 THEN
			INSERT INTO CREDITO.MOTIVO_RECUSA
				(COD_MOTIVO,
				 DESCRICAO)
			VALUES
				(PM_CODIGO,
				 PM_DESCRICAO);
			--Update
		ELSIF PM_ACAO = 2 THEN
			UPDATE CREDITO.MOTIVO_RECUSA
			   SET DESCRICAO = PM_DESCRICAO
			 WHERE COD_MOTIVO = PM_CODIGO;
			--Delete
		ELSIF PM_ACAO = 3 THEN
			DELETE FROM CREDITO.MOTIVO_RECUSA WHERE COD_MOTIVO = PM_CODIGO;
		END IF;
	
		COMMIT;
	
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			PM_CODERRO := SQLCODE;
			PM_TXTERRO := SQLERRM;
	END PR_INS_UPD_DEL_MOTIVOSRECUSA;

	--------------------------------------------------------------------------------------------------
	-- PROCEDURE PARA SELECIONAR SE CLIENTE EXPORTAC?O
	--------------------------------------------------------------------------------------------------
	PROCEDURE PR_SELECT_CLIEEXPORT
	(
		PM_CURSOR1     IN OUT TP_CURSOR,
		PM_COD_CLIENTE IN PRODUCAO.CLIENTE.COD_CLIENTE%TYPE,
		PM_ERRO        OUT NUMBER,
		PM_MSGERRO     OUT CHAR
	) AS
	BEGIN
		PM_ERRO    := 0;
		PM_MSGERRO := '';
	
		OPEN PM_CURSOR1 FOR
			SELECT COUNT(*) AS QTD
			  FROM PRODUCAO.CLIENTE_EXPORTACAO
			 WHERE COD_CLIENTE = PM_COD_CLIENTE;
	
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			OPEN PM_CURSOR1 FOR
				SELECT DUMMY FROM DUAL;
		WHEN OTHERS THEN
			PM_ERRO    := SQLCODE;
			PM_MSGERRO := SQLERRM;
		
	END PR_SELECT_CLIEEXPORT;

	--------------------------------------------------------------------------------------------------
	-- PROCEDURE PARA SELECIONAR CODIGO DO REPRESENTANTE
	--------------------------------------------------------------------------------------------------
	PROCEDURE PR_SELECT_REPRESENTANTE
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_CGC     IN PRODUCAO.CLIENTE.CGC%TYPE
	) IS
	BEGIN
		OPEN PM_CURSOR1 FOR
			SELECT COD_REPR_VEND FROM PRODUCAO.CLIENTE WHERE CGC = PM_CGC;
	
	END PR_SELECT_REPRESENTANTE;

	--------------------------------------------------------------------------------------------------
	--Selecionar todos os campos da Producao referente ao CGC informado
	--Ser?o exibidos na tela de Alterac?o
	--------------------------------------------------------------------------------------------------
	PROCEDURE PR_SELECT_CLIENTE_VB
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_CGC     IN PRODUCAO.CLIENTE.CGC%TYPE
	) IS
	BEGIN
		OPEN PM_CURSOR1 FOR
			SELECT C.COD_CLIENTE "Codigo do Cliente",
				   TO_CHAR(C.CGC,
						   '00000000000000') CGC,
				   C.NOME_CLIENTE "Nome Cliente",
				   C.NOME_CONTATO "Nome Contato",
				   C.ENDERECO ENDERECO,
				   C.COD_CIDADE "Codigo Cidade",
				   CI.NOME_CIDADE "Nome Cidade",
				   CI.COD_UF "Codigo UF",
				   C.BAIRRO BAIRRO,
				   C.DDD1 "DDD 1",
				   C.FONE1 "Fone 1",
				   NVL(C.DDD2,
					   0) "DDD 2",
				   NVL(C.FONE2,
					   0) "Fone 2",
				   C.CEP,
				   C.INSCR_ESTADUAL "Inscricao Estadual",
				   NVL(C.INSCR_SUFRAMA,
					   '') "Inscricao Suframa",
				   C.COD_TIPO_CLIENTE "Codigo Tipo Cliente",
				   TC.DESC_TIPO_CLI "Descricao Tipo Cliente",
				   NVL(CR.COD_REPRES,
					   0) "Codigo Representante",
				   R.NOME_REPRES "Nome Representante",
				   C.DT_FUNDACAO "Data Fundacao",
				   C.PROPRIEDADE PROPRIEDADE,
				   C.NOME_PROPRIETARIO "Nome Proprietario",
				   NVL(C.FL_CONS_FINAL,
					   '') "Consumidor Final",
				   NVL(C.COD_TARE,
					   0) "Codigo Tare",
				   NVL(C.DT_TARE,
					   '') "Data Tare",
				   NVL(C.COD_MENSAGEM_FISCAL,
					   0) "Codigo Mensagem Fiscal",
				   NVL(M.DESC_MENS,
					   '') "Mensagem Fiscal",
				   NVL(C.TP_EMPRESA,
					   '') "TP_EMPRESA",
				   NVL(MCI.E_MAIL,
					   '') "E-Mail",
				   NVL(MCI.HOMEPAGE,
					   '') "Home Page",
				   NVL(CE.ENDERECO,
					   '') "Endereco Cobranca",
				   NVL(CE.COD_CIDADE,
					   0) "Codigo Cidade Cobranca",
				   CI2.NOME_CIDADE "Nome Cidade Cobranca",
				   CI2.COD_UF "Codigo UF Cobranca",
				   NVL(CE.BAIRRO,
					   '') "Bairro Cobranca",
				   NVL(CE.CEP,
					   0) "Cep Cobranca",
				   NVL(CE.INSCR_ESTADUAL,
					   '') "Inscricao Estadual Cobranca",
				   C.DT_RECADASTRO
			  FROM PRODUCAO.CLIENTE           C,
				   PRODUCAO.CIDADE            CI,
				   PRODUCAO.CIDADE            CI2,
				   PRODUCAO.REPRESENTANTE     R,
				   PRODUCAO.CLIE_MENSAGEM     M,
				   PRODUCAO.TIPO_CLIENTE      TC,
				   PRODUCAO.CLIE_ENDERECO     CE,
				   PRODUCAO.R_CLIE_REPRES     CR,
				   MARKETING.CLIENTE_INTERNET MCI
			 WHERE C.CGC = PM_CGC
			   AND C.COD_TIPO_CLIENTE = TC.COD_TIPO_CLI(+)
			   AND C.COD_CIDADE = CI.COD_CIDADE(+)
			   AND CE.COD_CIDADE = CI2.COD_CIDADE(+)
			   AND C.COD_CLIENTE = CR.COD_CLIENTE(+)
			   AND CR.COD_REPRES = R.COD_REPRES(+)
			   AND C.COD_MENSAGEM_FISCAL = M.COD_MENSAGEM(+)
			   AND C.COD_CLIENTE = CE.COD_CLIENTE(+)
			   AND C.COD_CLIENTE = MCI.COD_CLIENTE(+);
	
	END PR_SELECT_CLIENTE_VB;

	--------------------------------------------------------------------------------------------------
	-- PROCEDURE PARA VERIFICAR LOGIN INTERNET DO USUARIO
	--------------------------------------------------------------------------------------------------
	PROCEDURE PR_LOGIN_USER
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_LOGIN   IN HELPDESK.USUARIO.LOGIN%TYPE,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	) AS
	BEGIN
		PM_ERRO    := 0;
		PM_MSGERRO := '';
	
		OPEN PM_CURSOR1 FOR
			SELECT COD_USUARIO,
				   LOGIN,
				   DESCRIPTOGRAFA_SENHA(SENHA) SENHA
			  FROM HELPDESK.USUARIO
			 WHERE LOGIN = PM_LOGIN;
	
	EXCEPTION
		WHEN OTHERS THEN
			PM_ERRO    := SQLCODE;
			PM_MSGERRO := SQLERRM;
		
	END PR_LOGIN_USER;

	--------------------------------------------------------------------------------------------------
	-- PROCEDURE PARA TRAZER INFOR. REFERENCIAS COMERCIAS DA TABELA PRODUCAO (CLIENTE)
	--------------------------------------------------------------------------------------------------
	PROCEDURE PR_SELECT_REF_COML_P
	(
		CREFCOML     IN OUT TP_CURSOR,
		PCOD_CLI_CGC IN PRODUCAO.CLIENTE.CGC%TYPE,
		PM_ERRO      OUT NUMBER,
		PM_MSGERRO   OUT CHAR
	) IS
	BEGIN
	
		PM_ERRO    := 0;
		PM_MSGERRO := '';
	
		OPEN CREFCOML FOR
			SELECT B.*
			  FROM PRODUCAO.CLIENTE    C,
				   PRODUCAO.CLIE_REFER B
			 WHERE C.CGC = PCOD_CLI_CGC
			   AND C.COD_CLIENTE = B.COD_CLIENTE
			 ORDER BY B.SEQUENCIA DESC;
	
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			OPEN CREFCOML FOR
				SELECT DUMMY FROM DUAL;
		WHEN OTHERS THEN
			PM_ERRO    := SQLCODE;
			PM_MSGERRO := SQLERRM;
		
	END PR_SELECT_REF_COML_P;

	--------------------------------------------------------------------------------------------------
	-- PROCEDURE PARA TRAZER INFOR. REFERENCIAS BANCARIAS DA TABELA INTRANET (CLIENTE_WEB)
	--------------------------------------------------------------------------------------------------
	PROCEDURE PR_SELECT_REF_BANCO_P
	(
		CREFBCO      IN OUT TP_CURSOR,
		PCOD_CLI_CGC IN PRODUCAO.CLIENTE.CGC%TYPE,
		PM_ERRO      OUT NUMBER,
		PM_MSGERRO   OUT CHAR
	) IS
	BEGIN
	
		PM_ERRO    := 0;
		PM_MSGERRO := '';
	
		OPEN CREFBCO FOR
			SELECT B.*
			  FROM PRODUCAO.CLIENTE     C,
				   PRODUCAO.CLIE_REFBCO B
			 WHERE C.CGC = PCOD_CLI_CGC
			   AND C.COD_CLIENTE = B.COD_CLIENTE
			 ORDER BY B.SEQUENCIA DESC;
	
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			OPEN CREFBCO FOR
				SELECT DUMMY FROM DUAL;
		WHEN OTHERS THEN
			PM_ERRO    := SQLCODE;
			PM_MSGERRO := SQLERRM;
		
	END PR_SELECT_REF_BANCO_P;

	--------------------------------------------------------------------------------------------------
	-- PROCEDURE PARA TRAZER INFOR. OUTROS ENDERECOS DA TABELA INTRANET (CLIENTE_WEB)
	--------------------------------------------------------------------------------------------------
	PROCEDURE PR_SELECT_OUT_END_P
	(
		COUTEND      IN OUT TP_CURSOR,
		PCOD_CLI_CGC IN PRODUCAO.CLIENTE.CGC%TYPE,
		PM_ERRO      OUT NUMBER,
		PM_MSGERRO   OUT CHAR
	) IS
	BEGIN
	
		PM_ERRO    := 0;
		PM_MSGERRO := '';
	
		OPEN COUTEND FOR
			SELECT B.COD_CLIENTE,
				   B.TP_ENDERECO,
				   B.SEQUENCIA,
				   B.ENDERECO,
				   B.COD_CIDADE,
				   CI.NOME_CIDADE,
				   CI.COD_UF,
				   B.BAIRRO,
				   B.CEP,
				   B.DDD,
				   B.FONE,
				   B.FAX,
				   B.CXPOSTAL,
				   B.TP_DOCTO,
				   B.CGC,
				   B.INSCR_ESTADUAL,
				   B.NOME_CLIENTE
			  FROM PRODUCAO.CLIENTE       C,
				   PRODUCAO.CLIE_ENDERECO B,
				   CIDADE                 CI
			 WHERE C.CGC = PCOD_CLI_CGC
			   AND C.COD_CLIENTE = B.COD_CLIENTE
			   AND B.COD_CIDADE = CI.COD_CIDADE
			 ORDER BY B.TP_ENDERECO,
					  B.SEQUENCIA DESC;
	
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			OPEN COUTEND FOR
				SELECT DUMMY FROM DUAL;
		WHEN OTHERS THEN
			PM_ERRO    := SQLCODE;
			PM_MSGERRO := SQLERRM;
		
	END PR_SELECT_OUT_END_P;

	-- Selecionar dados da Producao para serem exibidas
	-- na coluna DE da tela de RECADASTRO, onde o usuario do
	-- Depto de Credito ira aceitar ou n?o as alterac?es efetuadas
	PROCEDURE PR_SELECT_CLIENTE_RECAD_VB
	(
		CCURSOR1 IN OUT TP_CURSOR,
		PCGC     IN PRODUCAO.CLIENTE.CGC%TYPE
	) IS
	BEGIN
	
		--C.Cod_Repr_Vend "Codigo do Representante",
		--And C.Cod_Repr_Vend = R.Cod_Repres(+)
	
		OPEN CCURSOR1 FOR
			SELECT C.COD_CLIENTE "Codigo Cliente",
				   C.NOME_CLIENTE "Nome Cliente",
				   TO_CHAR(C.CGC,
						   'FM99999999999999') CGC,
				   C.ENDERECO ENDERECO,
				   C.BAIRRO BAIRRO,
				   C.CEP,
				   CI.COD_UF "Codigo UF",
				   C.COD_CIDADE "Codigo Cidade",
				   CI.NOME_CIDADE "Nome Cidade",
				   C.DDD1 "DDD 1",
				   C.FONE1 "Fone 1",
				   C.NOME_CONTATO "Nome Contato",
				   C.NOME_PROPRIETARIO "Nome Proprietario",
				   C.INSCR_ESTADUAL "Inscricao Estadual",
				   NVL(CR.COD_REPRES,
					   0) "Codigo Representante",
				   CRB.BANCO "Banco Ref Bancaria",
				   CRB.AGENCIA "Agencia Ref Bancaria",
				   CRB.DDD "DDD Ref Bancaria",
				   CRB.FONE "Fone Ref Bancaria",
				   CE.TP_DOCTO "Tipo do Docto de Cobranca",
				   CE.CGC "CGC Cobranca",
				   NVL(CE.INSCR_ESTADUAL,
					   '') "Inscricao Estadual Cobranca",
				   CE.NOME_CLIENTE "Nome Cliente Cobranca",
				   NVL(CE.COD_CIDADE,
					   0) "Codigo Cidade Cobranca",
				   NVL(CE.ENDERECO,
					   '') "Endereco Cobranca",
				   NVL(CE.BAIRRO,
					   '') "Bairro Cobranca",
				   NVL(CE.CEP,
					   0) "Cep Cobranca",
				   NVL(C.COD_MENSAGEM_FISCAL,
					   0) "Codigo Mensagem Fiscal",
				   NVL(M.DESC_MENS,
					   '') "Mensagem Fiscal",
				   NVL(C.TP_EMPRESA,
					   '') "TP_EMPRESA",
				   CI2.COD_UF "Codigo UF Cobranca",
				   C.DT_RECADASTRO,
				   EMA.EMAIL "Email",
				   EMA.NOME_CONTATO "Contato"
			
			  FROM PRODUCAO.CLIENTE           C,
				   PRODUCAO.CIDADE            CI,
				   PRODUCAO.CIDADE            CI2,
				   PRODUCAO.REPRESENTANTE     R,
				   PRODUCAO.CLIE_MENSAGEM     M,
				   PRODUCAO.TIPO_CLIENTE      TC,
				   PRODUCAO.CLIE_ENDERECO     CE,
				   PRODUCAO.R_CLIE_REPRES     CR,
				   MARKETING.CLIENTE_INTERNET MCI,
				   PRODUCAO.CLIE_REFBCO       CRB,
				   PRODUCAO.R_CLIENTE_EMAIL   EMA
			 WHERE C.CGC = PCGC
			   AND C.COD_TIPO_CLIENTE = TC.COD_TIPO_CLI(+)
			   AND C.COD_CIDADE = CI.COD_CIDADE(+)
			   AND C.COD_CIDADE = CI2.COD_CIDADE(+)
			   AND C.COD_CLIENTE = CR.COD_CLIENTE(+)
			   AND CR.COD_REPRES = R.COD_REPRES(+)
			   AND C.COD_MENSAGEM_FISCAL = M.COD_MENSAGEM(+)
			   AND C.COD_CLIENTE = CE.COD_CLIENTE(+)
			   AND C.COD_CLIENTE = MCI.COD_CLIENTE(+)
			   AND C.COD_CLIENTE = CRB.COD_CLIENTE(+)
			   AND C.COD_CLIENTE = EMA.COD_CLIENTE(+);
	
	END PR_SELECT_CLIENTE_RECAD_VB;

	--------------------------------------------------------------------------------------------------
	-- PROCEDURE PARA SELECIONAR DUPLICATAS EM ABERTO DO CLIENTE
	--------------------------------------------------------------------------------------------------
	PROCEDURE PR_VER_PEDIDO_ABERTO
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_CODCLI  IN PRODUCAO.CLIENTE.COD_CLIENTE%TYPE,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	) AS
	BEGIN
		PM_ERRO    := 0;
		PM_MSGERRO := '';
	
		OPEN PM_CURSOR1 FOR
			SELECT COUNT(*)
			  FROM COBRANCA.DUPLICATAS
			 WHERE COD_CLIENTE = PM_CODCLI
			   AND SITUACAO_PAGTO = 0;
	
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			OPEN PM_CURSOR1 FOR
				SELECT DUMMY FROM DUAL;
		WHEN OTHERS THEN
			PM_ERRO    := SQLCODE;
			PM_MSGERRO := SQLERRM;
		
	END PR_VER_PEDIDO_ABERTO;

	--------------------------------------------------------------------------------------------------
	-- PROCEDURE PARA VERIFICAR COD. MENSAGEM DE CLIENTES
	--------------------------------------------------------------------------------------------------
	PROCEDURE PR_SELECT_MENSAGEM
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_CODMSG  IN PRODUCAO.CLIE_MENSAGEM.COD_MENSAGEM%TYPE,
		PM_TP_CONS IN NUMBER,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	) AS
	BEGIN
	
		PM_ERRO    := 0;
		PM_MSGERRO := '';
	
		IF PM_TP_CONS = 0 THEN
		
			OPEN PM_CURSOR1 FOR
				SELECT *
				  FROM PRODUCAO.CLIE_MENSAGEM
				 WHERE COD_MENSAGEM = PM_CODMSG;
		
		END IF;
	
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			OPEN PM_CURSOR1 FOR
				SELECT DUMMY FROM DUAL;
		WHEN OTHERS THEN
			PM_ERRO    := SQLCODE;
			PM_MSGERRO := SQLERRM;
		
	END PR_SELECT_MENSAGEM;

	--------------------------------------------------------------------------------------------------
	-- PROCEDURE PARA SELECIONAR TIPOS DE CLIENTE VDR / LINHA PRODUTO
	--------------------------------------------------------------------------------------------------
	PROCEDURE PR_SELECT_CONTVDR
	(
		PM_CURSOR1  IN OUT TP_CURSOR,
		PM_CODTPCLI IN VDR.CONTROLE_VDR.COD_TIPO_CLIENTE%TYPE,
		PM_TP_CONS  IN NUMBER,
		PM_ERRO     OUT NUMBER,
		PM_MSGERRO  OUT CHAR
	) AS
	BEGIN
	
		PM_ERRO    := 0;
		PM_MSGERRO := '';
	
		-- VERIFICAR SE TIPO DE CLIENTE ESCOLHIDO PODE SER BOSCH DIESEL
		IF PM_TP_CONS = 0 THEN
			OPEN PM_CURSOR1 FOR
				SELECT *
				  FROM VDR.CONTROLE_VDR
				 WHERE COD_TIPO_CLIENTE = PM_CODTPCLI;
			-- SELECIONA AS LINHAS PRODUTO CADASTRADAS NA CONTROLE_VDR, PARA O TIPO DE CLIENTE ESCOLHIDO
		ELSIF PM_TP_CONS = 1 THEN
			OPEN PM_CURSOR1 FOR
				SELECT DISTINCT C.LINHA_PRODUTO LINHA_PRODUTO,
								L.DESC_LINHA
				  FROM VDR.CONTROLE_VDR C,
					   LINHA_PRODUTO    L
				 WHERE C.COD_TIPO_CLIENTE = PM_CODTPCLI
				   AND C.LINHA_PRODUTO = L.LINHA_PRODUTO
				 ORDER BY C.LINHA_PRODUTO;
		END IF;
	
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			OPEN PM_CURSOR1 FOR
				SELECT DUMMY FROM DUAL;
		WHEN OTHERS THEN
			PM_ERRO    := SQLCODE;
			PM_MSGERRO := SQLERRM;
		
	END PR_SELECT_CONTVDR;

	--------------------------------------------------------------------------------------------------
	-- PROCEDURE TRAZER INFORMAC?ES DE CARGO - MARKETING CLIENTE
	--------------------------------------------------------------------------------------------------
	PROCEDURE PR_SEL_MKTTPCONTATO
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	) AS
	BEGIN
		PM_ERRO    := 0;
		PM_MSGERRO := '';
	
		OPEN PM_CURSOR1 FOR
			SELECT * FROM MARKETING.TIPO_CONTATO ORDER BY COD_TIPO_CONTATO;
	
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			OPEN PM_CURSOR1 FOR
				SELECT DUMMY FROM DUAL;
		WHEN OTHERS THEN
			PM_ERRO    := SQLCODE;
			PM_MSGERRO := SQLERRM;
		
	END PR_SEL_MKTTPCONTATO;

	--------------------------------------------------------------------------------------------------
	-- PROCEDURE TRAZER INFORMAC?ES DE HOBBY - MARKETING CLIENTE
	--------------------------------------------------------------------------------------------------
	PROCEDURE PR_SEL_MKTHOBBY
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	) AS
	BEGIN
		PM_ERRO    := 0;
		PM_MSGERRO := '';
	
		OPEN PM_CURSOR1 FOR
			SELECT * FROM MARKETING.HOBBY_CLIENTE ORDER BY COD_HOBBY;
	
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			OPEN PM_CURSOR1 FOR
				SELECT DUMMY FROM DUAL;
		WHEN OTHERS THEN
			PM_ERRO    := SQLCODE;
			PM_MSGERRO := SQLERRM;
		
	END PR_SEL_MKTHOBBY;

	--------------------------------------------------------------------------------------------------
	-- PROCEDURE TRAZER INFORMAC?ES DE TIME DE FUTEBOL - MARKETING CLIENTE
	--------------------------------------------------------------------------------------------------
	PROCEDURE PR_SEL_MKTTIME
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	) AS
	BEGIN
		PM_ERRO    := 0;
		PM_MSGERRO := '';
	
		OPEN PM_CURSOR1 FOR
			SELECT * FROM MARKETING.TIME_FUTEBOL ORDER BY COD_TIME;
	
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			OPEN PM_CURSOR1 FOR
				SELECT DUMMY FROM DUAL;
		WHEN OTHERS THEN
			PM_ERRO    := SQLCODE;
			PM_MSGERRO := SQLERRM;
		
	END PR_SEL_MKTTIME;

	--------------------------------------------------------------------------------------------------
	-- PROCEDURE TRAZER INFORMAC?ES DOS DADOS DE CONTATO - MARKETING CLIENTE
	--------------------------------------------------------------------------------------------------
	PROCEDURE PR_SEL_DADOSMKTCONTATO
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_CODCLI  IN PRODUCAO.CLIENTE.COD_CLIENTE%TYPE,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	) AS
	BEGIN
		PM_ERRO    := 0;
		PM_MSGERRO := '';
	
		OPEN PM_CURSOR1 FOR
			SELECT C.NOME_CONTATO1,
				   C.NOME_CONTATO2,
				   C.COD_TIPO_CONTATO1,
				   C.COD_TIPO_CONTATO2,
				   C.COD_LINHA,
				   C.DT_NASC_CONTATO1,
				   C.DT_NASC_CONTATO2,
				   C.COD_HOBBY_CONTATO1,
				   C.COD_HOBBY_CONTATO2,
				   C.COD_TIME_CONTATO1,
				   C.COD_TIME_CONTATO2,
				   T.DESC_CONTATO DESC1,
				   TT.DESC_CONTATO DESC2,
				   F.DESC_TIME TIME1,
				   FF.DESC_TIME TIME2,
				   H.DESC_HOBBY HOBBY1,
				   HH.DESC_HOBBY HOBBY2
			  FROM MARKETING.CLIENTE_CONTATO C,
				   MARKETING.TIPO_CONTATO    T,
				   MARKETING.TIPO_CONTATO    TT,
				   MARKETING.TIME_FUTEBOL    F,
				   MARKETING.TIME_FUTEBOL    FF,
				   MARKETING.HOBBY_CLIENTE   H,
				   MARKETING.HOBBY_CLIENTE   HH
			 WHERE C.COD_CLIENTE = PM_CODCLI
			   AND C.COD_TIPO_CONTATO1 = T.COD_TIPO_CONTATO
			   AND C.COD_TIPO_CONTATO2 = TT.COD_TIPO_CONTATO
			   AND C.COD_TIME_CONTATO1 = F.COD_TIME
			   AND C.COD_TIME_CONTATO2 = FF.COD_TIME
			   AND C.COD_HOBBY_CONTATO1 = H.COD_HOBBY
			   AND C.COD_HOBBY_CONTATO2 = HH.COD_HOBBY;
	
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			OPEN PM_CURSOR1 FOR
				SELECT DUMMY FROM DUAL;
		WHEN OTHERS THEN
			PM_ERRO    := SQLCODE;
			PM_MSGERRO := SQLERRM;
		
	END PR_SEL_DADOSMKTCONTATO;

	--------------------------------------------------------------------------------------------------
	-- PROCEDURE TRAZER INFORMAC?ES DO CODIGO DE USUARIO
	--------------------------------------------------------------------------------------------------
	PROCEDURE PR_SEL_USERINTRANET
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_CODUSER IN HELPDESK.USUARIO.COD_USUARIO%TYPE,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	) AS
	BEGIN
		PM_ERRO    := 0;
		PM_MSGERRO := '';
	
		OPEN PM_CURSOR1 FOR
			SELECT * FROM HELPDESK.USUARIO WHERE COD_USUARIO = PM_CODUSER;
	
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			OPEN PM_CURSOR1 FOR
				SELECT DUMMY FROM DUAL;
		WHEN OTHERS THEN
			PM_ERRO    := SQLCODE;
			PM_MSGERRO := SQLERRM;
		
	END PR_SEL_USERINTRANET;

	---------------------------------------------------------------------
	-- Pegar o codigo do sistema
	PROCEDURE PR_SELECT_COD_SOFTWARE
	(
		PM_NOME_SISTEMA IN HELPDESK.SOFTWARE.NOME_SOFTWARE%TYPE,
		PM_COD_SISTEMA  OUT NUMBER
	) IS
	BEGIN
		SELECT COD_SOFTWARE
		  INTO PM_COD_SISTEMA
		  FROM HELPDESK.SOFTWARE
		 WHERE NOME_SOFTWARE = PM_NOME_SISTEMA;
	
	END PR_SELECT_COD_SOFTWARE;

	---------------------------------------------------------------------
	-- Pegar o VALOR DO PARAMETRO
	PROCEDURE PR_SELECT_VL_PARAMETRO
	(
		PM_CURSOR1     IN OUT TP_CURSOR,
		PM_COD_SISTEMA IN NUMBER,
		PM_NOME_PARAM  IN VARCHAR2
	) IS
	BEGIN
		OPEN PM_CURSOR1 FOR
			SELECT *
			  FROM HELPDESK.PARAMETROS
			 WHERE HELPDESK.PARAMETROS.COD_SOFTWARE = PM_COD_SISTEMA
			   AND HELPDESK.PARAMETROS.NOME_PARAMETRO = PM_NOME_PARAM;
	
	END PR_SELECT_VL_PARAMETRO;

	---------------------------------------------------------------------
	-- Seleciona todas as filiais
	PROCEDURE PR_SELECT_FILIAL
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	) AS
	BEGIN
		PM_ERRO    := 0;
		PM_MSGERRO := '';
	
		OPEN PM_CURSOR1 FOR
		
			SELECT NOME_FILIAL,
				   COD_FILIAL,
				   COD_REGIONAL
			  FROM PRODUCAO.FILIAL
			 WHERE DIVISAO = 'D'
			 ORDER BY NOME_FILIAL;
	
	END PR_SELECT_FILIAL;

	---------------------------------------------------------------------
	-- Seleciona a sequencia maxima das ref.coml de um determinado cliente
	PROCEDURE PR_MAXSEQ_REFCOML
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_CODCLI  IN PRODUCAO.CLIENTE.COD_CLIENTE%TYPE,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	) AS
	BEGIN
		PM_ERRO    := 0;
		PM_MSGERRO := '';
	
		OPEN PM_CURSOR1 FOR
		
			SELECT MAX(SEQUENCIA) SEQMAX
			  FROM PRODUCAO.CLIE_REFER
			 WHERE COD_CLIENTE = PM_CODCLI;
	
	END PR_MAXSEQ_REFCOML;

	---------------------------------------------------------------------
	-- Seleciona a sequencia maxima das ref.banc. de um determinado cliente
	PROCEDURE PR_MAXSEQ_REFBCO
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_CODCLI  IN PRODUCAO.CLIENTE.COD_CLIENTE%TYPE,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	) AS
	BEGIN
		PM_ERRO    := 0;
		PM_MSGERRO := '';
	
		OPEN PM_CURSOR1 FOR
		
			SELECT MAX(SEQUENCIA) SEQMAX
			  FROM PRODUCAO.CLIE_REFBCO
			 WHERE COD_CLIENTE = PM_CODCLI;
	
	END PR_MAXSEQ_REFBCO;

	---------------------------------------------------------------------
	-- Seleciona a sequencia maxima das ref.banc. de um determinado cliente
	PROCEDURE PR_MAXSEQ_OUTEND
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_CODCLI  IN PRODUCAO.CLIENTE.COD_CLIENTE%TYPE,
		PM_TPEND   IN NUMBER,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	) AS
	BEGIN
		PM_ERRO    := 0;
		PM_MSGERRO := '';
	
		IF PM_TPEND = 1 THEN
			OPEN PM_CURSOR1 FOR
				SELECT MAX(SEQUENCIA) SEQMAX
				  FROM PRODUCAO.CLIE_ENDERECO
				 WHERE TP_ENDERECO = PM_TPEND;
		ELSE
			OPEN PM_CURSOR1 FOR
				SELECT MAX(SEQUENCIA) SEQMAX
				  FROM PRODUCAO.CLIE_ENDERECO
				 WHERE COD_CLIENTE = PM_CODCLI
				   AND TP_ENDERECO = PM_TPEND;
		END IF;
	
	END PR_MAXSEQ_OUTEND;

	---------------------------------------------------------------------------------
	-- Log de acompanhamento de alterac?o dos campos de vl. e dt. maior saldo devedor
	PROCEDURE PR_LOG_MAIORSLDEV
	(
		PM_CODUSER     IN NUMBER,
		PM_DTALTER     IN VARCHAR2,
		PM_SLDEV_ANT   IN NUMBER,
		PM_SLDEV_ATU   IN NUMBER,
		PM_DTSLDEV_ANT IN VARCHAR2,
		PM_CODERRO     OUT NUMBER,
		PM_TXTERRO     OUT VARCHAR2
	) AS
	BEGIN
	
		INSERT INTO PRODUCAO.LOG_MAIORSLDEV
			(COD_USUARIO,
			 DT_ALTERACAO,
			 MAIOR_SALDEV_ANT,
			 MAIOR_SALDEV_ATU,
			 DT_MAIOR_SALDEV_ANT)
		VALUES
			(PM_CODUSER,
			 TO_DATE(PM_DTALTER,
					 'dd/mm/rr hh24:mi:ss'),
			 PM_SLDEV_ANT,
			 PM_SLDEV_ATU,
			 PM_DTSLDEV_ANT);
	
		COMMIT;
	
	EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			PM_CODERRO := SQLCODE;
			PM_TXTERRO := SQLERRM;
		
	END PR_LOG_MAIORSLDEV;

	--------------------------------------------------------------------------------------------------
	-- PROCEDURE PARA TRAZER CIDADES DE ACORDO COM PARAMETROS FORNECIDOS
	--------------------------------------------------------------------------------------------------
	PROCEDURE PR_SEL_CIDADE
	(
		PM_CURSOR1 IN OUT TP_CURSOR,
		PM_NOME    IN VARCHAR2,
		PM_CODCID  IN PRODUCAO.CIDADE.COD_CIDADE%TYPE,
		PM_UF      IN PRODUCAO.CIDADE.COD_UF%TYPE,
		PM_TP_CONS IN VARCHAR2,
		PM_ERRO    OUT NUMBER,
		PM_MSGERRO OUT CHAR
	) AS
	BEGIN
	
		PM_ERRO    := 0;
		PM_MSGERRO := '';
	
		-- Esta procedure faz consulta de cidade, de acordo com o que o sistema de consulta de cep
		-- dos correios trouxer - Alethea - Julho/05
		IF PM_TP_CONS = 0 THEN
		
			VAR_SELECT := 'Select LPAD(Cod_Cidade,4,0) Cod_Cidade, Nome_Cidade, Cod_Uf ';
			VAR_SELECT := VAR_SELECT || 'From Producao.Cidade ';
			VAR_SELECT := VAR_SELECT || 'Where 1=1 ';
		
			IF PM_NOME IS NOT NULL THEN
				VAR_SELECT := VAR_SELECT || ' And Nome_Cidade = ' || CHR(39) ||
							  PM_NOME || CHR(39) || ' ';
			END IF;
			IF PM_CODCID IS NOT NULL THEN
				VAR_SELECT := VAR_SELECT || ' And Cod_Cidade = ' || PM_CODCID || ' ';
			END IF;
			IF PM_UF IS NOT NULL THEN
				VAR_SELECT := VAR_SELECT || ' And Cod_Uf = ' || CHR(39) ||
							  PM_UF || CHR(39) || ' ';
			END IF;
		
			VAR_SELECT := VAR_SELECT || 'Order By Nome_Cidade';
		
		END IF;
	
		OPEN PM_CURSOR1 FOR VAR_SELECT;
	
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			OPEN PM_CURSOR1 FOR
				SELECT DUMMY FROM DUAL;
		WHEN OTHERS THEN
			PM_ERRO    := SQLCODE;
			PM_MSGERRO := SQLERRM;
		
	END PR_SEL_CIDADE;

	---------------------------------------------------------------------------------

	PROCEDURE PR_INSERT_CLIENTE_END_ENTREGA
	(
		PM_CGC          IN CLIENTE.CGC%TYPE,
		PM_TPDOCTO      IN CLIENTE.TP_DOCTO%TYPE,
		PM_NOMECLIENTE  IN CLIENTE.NOME_CLIENTE%TYPE,
		PM_ENDERECO     IN CLIENTE.ENDERECO%TYPE,
		PM_CODCIDADE    IN CLIENTE.COD_CIDADE%TYPE,
		PM_BAIRRO       IN CLIENTE.BAIRRO%TYPE,
		PM_DDD1         IN CLIENTE.DDD1%TYPE,
		PM_FONE1        IN CLIENTE.FONE1%TYPE,
		PM_CEP          IN CLIENTE.CEP%TYPE,
		PM_IE           IN CLIENTE.INSCR_ESTADUAL%TYPE,
		PM_NOMEPROPRIET IN CLIENTE.NOME_PROPRIETARIO%TYPE,
		PM_VERRO        OUT NUMBER,
		PM_VMSGERRO     OUT VARCHAR2
	) IS
		VDATA       DATE;
		VSQL        VARCHAR2(4000) := '';
		VCODCLIENTE NUMBER(6) := 0;
		VDEPOSITO   VARCHAR(9) := '';
	
		VCODCLI   NUMBER(7) := 0;
		VSITUACAO NUMBER(2) := 0;
		VSTATUS   NUMBER(2) := 0;
	
		-- VARIAVEIS PARA TRANSLATE
		VAR_CAR_INVALIDO VARCHAR2(50);
		VAR_CAR_VALIDO   VARCHAR2(50);
	
		CURSOR CUR_LOJAS IS
			SELECT LOJA
			  FROM (SELECT '01' LOJA
					  FROM DUAL
					UNION
					SELECT TRIM(TO_CHAR(D.COD_LOJA,
										'00')) LOJA
					  FROM PRODUCAO.LOJA           L,
						   PRODUCAO.DEPOSITO_VISAO D
					 WHERE L.COD_LOJA = D.COD_LOJA
					   AND D.NOME_PROGRAMA = 'CAD060')
			 ORDER BY LOJA;
	
	BEGIN
		VAR_CAR_INVALIDO := '�����������������������/%$#�-*=@�:|\�+_!?<>;�' || '' ||
							CHR(39) || '';
		VAR_CAR_VALIDO   := 'AAAAAEEEEIIIIOOOOOUUUUC......................';
		PM_VERRO         := 0;
		PM_VMSGERRO      := '';
		VDATA            := SYSDATE;
	
		FOR CONT IN CUR_LOJAS LOOP
		
			IF CONT.LOJA = '01' THEN
				VDEPOSITO := '';
			ELSE
				VDEPOSITO := 'DEP' || TRIM(CONT.LOJA) || '.';
			END IF;
		
			VSQL := 'UPDATE ' || VDEPOSITO || 'CLIENTE';
			VSQL := VSQL || ' SET CLIENTE.NOME_CLIENTE = ' || CHR(39) ||
					TRANSLATE(PM_NOMECLIENTE,
							  VAR_CAR_INVALIDO,
							  VAR_CAR_VALIDO) || CHR(39) || ',';
			VSQL := VSQL || ' CLIENTE.ENDERECO     = ' || CHR(39) ||
					TRANSLATE(PM_ENDERECO,
							  VAR_CAR_INVALIDO,
							  VAR_CAR_VALIDO) || CHR(39) || ',';
			VSQL := VSQL || ' CLIENTE.COD_CIDADE   = ' || PM_CODCIDADE || ',';
		
			IF PM_BAIRRO IS NULL THEN
				VSQL := VSQL || ' CLIENTE.BAIRRO       = NULL,';
			ELSE
				VSQL := VSQL || ' CLIENTE.BAIRRO       = ' || CHR(39) ||
						TRANSLATE(PM_BAIRRO,
								  VAR_CAR_INVALIDO,
								  VAR_CAR_VALIDO) || CHR(39) || ',';
			END IF;
		
			VSQL := VSQL || ' CLIENTE.DDD1         = ' || PM_DDD1 || ',';
			VSQL := VSQL || ' CLIENTE.FONE1        = ' || PM_FONE1 || ',';
			VSQL := VSQL || ' CLIENTE.CEP          = ' || PM_CEP || ',';
		
			IF PM_IE IS NULL THEN
				VSQL := VSQL || ' CLIENTE.INSCR_ESTADUAL = NULL' || ',';
			ELSE
				VSQL := VSQL || ' CLIENTE.INSCR_ESTADUAL = ' || CHR(39) ||
						PM_IE || CHR(39) || ',';
			END IF;
		
			VSQL := VSQL || ' CLIENTE.NOME_PROPRIETARIO = ' || CHR(39) ||
					PM_NOMEPROPRIET || CHR(39);
			VSQL := VSQL || ' WHERE  CLIENTE.CGC = ' || PM_CGC;
		
			EXECUTE IMMEDIATE VSQL;
		
			IF SQL%NOTFOUND THEN
			
				IF TRIM(CONT.LOJA) = '01' THEN
					SELECT SEQ_CLIENTE.NEXTVAL INTO VCODCLIENTE FROM DUAL;
				END IF;
			
				-- TESTE DE VARIAVEL DIFERENTE DE ZERO PARA N?O DAR PROBLEMA DE GRAVAR COD.CLIENTE = 0 NA ATUALIZAC?O - ALETHEA - 22/02/06
				IF VCODCLIENTE <> 0 THEN
				
					VSQL := 'INSERT INTO ' || VDEPOSITO || 'CLIENTE (';
					VSQL := VSQL || 'COD_CLIENTE, CGC, TP_DOCTO, NOME_CLIENTE, NOME_CONTATO,
						CLASSIFICACAO, STATUS,CARACTERISTICA,ENDERECO, COD_CIDADE, BAIRRO,DDD1,
						FONE1,DDD2,FONE2, CEP, CXPOSTAL, TELEX, FAX,INSCR_ESTADUAL,INSCR_SUFRAMA,
						DT_CADASTR, COD_TIPO_CLIENTE,COD_TRANSP,COD_REPR_VEND,COD_REPRES_BLAU,
						COD_BANCO,TP_ORDEM_PGTO, SITUACAO, COD_MENSAGEM,NOME_RESPONS,
						COD_CTR_PROTESTO,DT_FUNDACAO,PROPRIEDADE,CHEQUE_SF,COD_TIPO_CLBLAU,
						NOME_PROPRIETARIO, FL_CONS_FINAL, DT_MENSAGEM,TP_EMPRESA)
						VALUES (';
					VSQL := VSQL || VCODCLIENTE || ',';
					VSQL := VSQL || PM_CGC || ',';
					VSQL := VSQL || PM_TPDOCTO || ',';
					VSQL := VSQL || CHR(39) ||
							TRANSLATE(PM_NOMECLIENTE,
									  VAR_CAR_INVALIDO,
									  VAR_CAR_VALIDO) || CHR(39) || ',';
					VSQL := VSQL || 'NULL,';
					VSQL := VSQL || CHR(39) || 'X' || CHR(39) || ',';
					VSQL := VSQL || 1 || ',';
					VSQL := VSQL || CHR(39) || 'N' || CHR(39) || ',';
					VSQL := VSQL || CHR(39) ||
							TRANSLATE(PM_ENDERECO,
									  VAR_CAR_INVALIDO,
									  VAR_CAR_VALIDO) || CHR(39) || ',';
					VSQL := VSQL || PM_CODCIDADE || ',';
					VSQL := VSQL || CHR(39) ||
							TRANSLATE(PM_BAIRRO,
									  VAR_CAR_INVALIDO,
									  VAR_CAR_VALIDO) || CHR(39) || ',';
					VSQL := VSQL || PM_DDD1 || ',';
					VSQL := VSQL || PM_FONE1 || ',';
					VSQL := VSQL || 0 || ',';
					VSQL := VSQL || 0 || ',';
					VSQL := VSQL || PM_CEP || ',';
					VSQL := VSQL || 0 || ',';
					VSQL := VSQL || 0 || ',';
					VSQL := VSQL || 0 || ',';
				
					IF PM_IE IS NULL THEN
						VSQL := VSQL || 'NULL,';
					ELSE
						VSQL := VSQL || CHR(39) || PM_IE || CHR(39) || ',';
					END IF;
				
					VSQL := VSQL || 'NULL,';
					VSQL := VSQL || CHR(39) || VDATA || CHR(39) || ',';
					VSQL := VSQL || CHR(39) || '15' || CHR(39) || ',';
					VSQL := VSQL || 999 || ',';
					VSQL := VSQL || 'NULL,';
					VSQL := VSQL || 0 || ',';
					VSQL := VSQL || 999 || ',';
					VSQL := VSQL || 'NULL,';
					VSQL := VSQL || 0 || ',';
					VSQL := VSQL || 0 || ',';
					VSQL := VSQL || 'NULL,';
					VSQL := VSQL || 'NULL,';
					VSQL := VSQL || CHR(39) || VDATA || CHR(39) || ',';
					VSQL := VSQL || CHR(39) || 'S' || CHR(39) || ',';
					VSQL := VSQL || 0 || ',';
					VSQL := VSQL || CHR(39) || '0' || CHR(39) || ' ,';
					VSQL := VSQL || CHR(39) ||
							TRANSLATE(PM_NOMEPROPRIET,
									  VAR_CAR_INVALIDO,
									  VAR_CAR_VALIDO) || CHR(39) || ',';
					VSQL := VSQL || CHR(39) || 'N' || CHR(39) || ',';
					VSQL := VSQL || 'NULL,';
					VSQL := VSQL || CHR(39) || 'M' || CHR(39) || ')';
				
					EXECUTE IMMEDIATE VSQL;
				
				END IF;
			
			ELSE
			
				-- No caso de ja existir o cliente, verifica se situac?o = 9 e se e outras saidas, se n?o for, fazer estas alterac?es
				-- Isso para os casos de clientes que ja eram clientes normais da DPK mas que foram desativados,
				-- ent?o dever?o ser ativados como outras saidas - Conf. Solicitac?o da Natalia em 05/09/05 - Alethea
				SELECT COD_CLIENTE,
					   SITUACAO,
					   STATUS
				  INTO VCODCLI,
					   VSITUACAO,
					   VSTATUS
				  FROM PRODUCAO.CLIENTE
				 WHERE CGC = PM_CGC;
			
				IF VSITUACAO = 9 AND VSTATUS = 0 THEN
				
					-- Processo de reativac?o retirado da PCK_CLIENTE.PR_GRAVA_ATIVA_DESATIVA - Cliente outras saidas n?o deve ter representante
					-- Incluido alterac?o de stats do Cliente de Normal para Outras Saidas
					VSQL := ' UPDATE ' || VDEPOSITO || 'CLIENTE';
					VSQL := VSQL || ' SET CLIENTE.SITUACAO = 0, ';
					VSQL := VSQL || ' CLIENTE.STATUS = 1, ';
					VSQL := VSQL || ' CLIENTE.COD_REPR_VEND = NULL, ';
					VSQL := VSQL || ' CLIENTE.COD_MENSAGEM  = 0 ';
					VSQL := VSQL || ' WHERE CLIENTE.CGC = ' || PM_CGC;
				
					EXECUTE IMMEDIATE VSQL;
				
					VSQL := ' DELETE FROM ' || VDEPOSITO || 'R_CLIE_REPRES ';
					VSQL := VSQL || ' WHERE R_CLIE_REPRES.COD_CLIENTE = ' ||
							VCODCLI;
				
					EXECUTE IMMEDIATE VSQL;
				
				END IF;
			
			END IF;
			COMMIT;
		
		END LOOP;
	
		--Endereco de Entrega
	
	EXCEPTION
		WHEN OTHERS THEN
			PM_VERRO    := SQLCODE;
			PM_VMSGERRO := SQLERRM;
	END;

	PROCEDURE PR_GRAVA_END_ENTREGA
	(
		PM_CODCLIENTE     IN PRODUCAO.CLIE_ENDERECO.COD_CLIENTE%TYPE,
		PM_SEQUENCIA      IN PRODUCAO.CLIE_ENDERECO.SEQUENCIA%TYPE,
		PM_ENDERECO       IN PRODUCAO.CLIE_ENDERECO.ENDERECO%TYPE,
		PM_COD_CIDADE     IN PRODUCAO.CLIE_ENDERECO.COD_CIDADE%TYPE,
		PM_BAIRRO         IN PRODUCAO.CLIE_ENDERECO.BAIRRO%TYPE,
		PM_CEP            IN PRODUCAO.CLIE_ENDERECO.CEP%TYPE,
		PM_DDD            IN PRODUCAO.CLIE_ENDERECO.DDD%TYPE,
		PM_FONE           IN PRODUCAO.CLIE_ENDERECO.FONE%TYPE,
		PM_FAX            IN PRODUCAO.CLIE_ENDERECO.FAX%TYPE,
		PM_CXPOSTAL       IN PRODUCAO.CLIE_ENDERECO.CXPOSTAL%TYPE,
		PM_TP_DOCTO       IN PRODUCAO.CLIE_ENDERECO.TP_DOCTO%TYPE,
		PM_CGC            IN PRODUCAO.CLIE_ENDERECO.CGC%TYPE,
		PM_INSCR_ESTADUAL IN PRODUCAO.CLIE_ENDERECO.INSCR_ESTADUAL%TYPE,
		PM_NOME_CLIENTE   IN PRODUCAO.CLIE_ENDERECO.NOME_CLIENTE%TYPE,
		PM_VERRO          OUT NUMBER,
		PM_MSGERRO        OUT VARCHAR2
	) IS
	
		VSQL      VARCHAR(4000) := '';
		VDEPOSITO VARCHAR(9) := '';
	
		-- VARIAVEIS PARA TRANSLATE
		VAR_CAR_INVALIDO VARCHAR2(50);
		VAR_CAR_VALIDO   VARCHAR2(50);
	
		CURSOR CUR_LOJAS IS
			SELECT '01' LOJA
			  FROM DUAL
			UNION
			SELECT TRIM(TO_CHAR(D.COD_LOJA,
								'00')) LOJA
			  FROM PRODUCAO.LOJA           L,
				   PRODUCAO.DEPOSITO_VISAO D
			 WHERE L.COD_LOJA = D.COD_LOJA
			   AND D.NOME_PROGRAMA = 'CAD060';
	
	BEGIN
	
		VAR_CAR_INVALIDO := '�����������������������/%$#�-*=@�:|\�+_!?<>;�' || '' ||
							CHR(39) || '';
		VAR_CAR_VALIDO   := 'AAAAAEEEEIIIIOOOOOUUUUC......................';
	
		PM_VERRO   := 0;
		PM_MSGERRO := '';
	
		FOR CONT IN CUR_LOJAS LOOP
			IF CONT.LOJA = '01' THEN
				VDEPOSITO := '';
			ELSE
				VDEPOSITO := 'DEP' || TRIM(CONT.LOJA) || '.';
			END IF;
		
			VSQL := 'UPDATE ' || VDEPOSITO || 'CLIE_ENDERECO ';
			VSQL := VSQL || 'SET ENDERECO =' || CHR(39) ||
					TRANSLATE(PM_ENDERECO,
							  VAR_CAR_INVALIDO,
							  VAR_CAR_VALIDO) || CHR(39) || ',';
			VSQL := VSQL || 'COD_CIDADE 	=' || PM_COD_CIDADE || ',';
			IF PM_BAIRRO IS NULL THEN
				VSQL := VSQL || 'BAIRRO		    = NULL,';
			ELSE
				VSQL := VSQL || 'BAIRRO		    =' || CHR(39) ||
						TRANSLATE(PM_BAIRRO,
								  VAR_CAR_INVALIDO,
								  VAR_CAR_VALIDO) || CHR(39) || ',';
			END IF;
			VSQL := VSQL || 'CEP		      =' || PM_CEP || ',';
			VSQL := VSQL || 'DDD		      =' || PM_DDD || ',';
			VSQL := VSQL || 'FONE		      =' || PM_FONE || ',';
			VSQL := VSQL || 'FAX		      =' || PM_FAX || ',';
			VSQL := VSQL || 'CXPOSTAL	    =' || PM_CXPOSTAL || ',';
			VSQL := VSQL || 'TP_DOCTO	    =' || PM_TP_DOCTO || ',';
			VSQL := VSQL || 'CGC		      =' || PM_CGC || ',';
			VSQL := VSQL || 'INSCR_ESTADUAL	=' || CHR(39) || PM_INSCR_ESTADUAL ||
					CHR(39) || ',';
			VSQL := VSQL || 'NOME_CLIENTE 	=' || CHR(39) ||
					TRANSLATE(PM_NOME_CLIENTE,
							  VAR_CAR_INVALIDO,
							  VAR_CAR_VALIDO) || CHR(39);
			VSQL := VSQL || ' WHERE COD_CLIENTE = ' || PM_CODCLIENTE;
			VSQL := VSQL || ' AND SEQUENCIA   = ' || PM_SEQUENCIA;
			VSQL := VSQL || ' AND TP_ENDERECO = 1';
		
			EXECUTE IMMEDIATE VSQL;
		
			IF SQL%NOTFOUND THEN
				VSQL := 'INSERT INTO ' || VDEPOSITO || 'CLIE_ENDERECO
					            (COD_CLIENTE,
				              TP_ENDERECO,
				              SEQUENCIA,
				              ENDERECO,
				              COD_CIDADE,
					            BAIRRO,
				              CEP,
				              DDD,
				              FONE,
				              FAX,
				              CXPOSTAL,
				              TP_DOCTO,
				              CGC,
				              INSCR_ESTADUAL,
						NOME_CLIENTE)
				         VALUES (';
				VSQL := VSQL || PM_CODCLIENTE || ',';
				VSQL := VSQL || '1,';
				VSQL := VSQL || PM_SEQUENCIA || ',';
				VSQL := VSQL || CHR(39) ||
						TRANSLATE(PM_ENDERECO,
								  VAR_CAR_INVALIDO,
								  VAR_CAR_VALIDO) || CHR(39) || ',';
				VSQL := VSQL || PM_COD_CIDADE || ',';
				VSQL := VSQL || CHR(39) ||
						TRANSLATE(PM_BAIRRO,
								  VAR_CAR_INVALIDO,
								  VAR_CAR_VALIDO) || CHR(39) || ',';
				VSQL := VSQL || PM_CEP || ',';
				VSQL := VSQL || PM_DDD || ',';
				VSQL := VSQL || PM_FONE || ',';
				VSQL := VSQL || PM_FAX || ',';
				VSQL := VSQL || PM_CXPOSTAL || ',';
				VSQL := VSQL || PM_TP_DOCTO || ',';
				VSQL := VSQL || PM_CGC || ',';
				VSQL := VSQL || CHR(39) || PM_INSCR_ESTADUAL || CHR(39) || ',';
				VSQL := VSQL || CHR(39) ||
						TRANSLATE(PM_NOME_CLIENTE,
								  VAR_CAR_INVALIDO,
								  VAR_CAR_VALIDO) || CHR(39) || ')';
			
				EXECUTE IMMEDIATE VSQL;
			
			END IF;
		
			COMMIT;
		END LOOP;
	
	END PR_GRAVA_END_ENTREGA;

	PROCEDURE PR_SELECT_TIPO_USUARIO
	(
		PM_CURSOR IN OUT TP_CURSOR,
		PM_CGC    IN NUMBER
	) IS
	BEGIN
		OPEN PM_CURSOR FOR
			SELECT CASE TP_USUARIO
					   WHEN 'C' THEN
						'COORDENADOR'
					   WHEN 'D' THEN
						'GERENTE DE DIVISAO'
					   WHEN 'F' THEN
						'FUNCIONARIO'
					   WHEN 'G' THEN
						'GERENTE'
					   WHEN 'P' THEN
						'PROMOTOR'
					   WHEN 'R' THEN
						'REPRESENTANTE'
					   WHEN 'S' THEN
						'SUPERVISOR'
				   END TIPO,
				   COD_USUARIO,
				   NOME_USUARIO
			  FROM HELPDESK.USUARIO
			 WHERE COD_USUARIO = (SELECT DISTINCT COD_USUARIO
									FROM INTRANET.CLIENTE_WEB@LNK_IDPK
								   WHERE CGC = PM_CGC);
	END;

	PROCEDURE PR_SELECT_FLVISIT_COMENT
	(
		PM_CURSOR IN OUT TP_CURSOR,
		PM_CGC    IN NUMBER,
		PM_TIPO   IN NUMBER
	) IS
	BEGIN
		OPEN PM_CURSOR FOR
			SELECT FL_REPRES_VISITOU,
				   OBSERVACOES
			  FROM INTRANET.CLIENTE_WEB@LNK_IDPK
			 WHERE CGC = PM_CGC
			   AND TP_SOLICITACAO = PM_TIPO;
	END;

	PROCEDURE PR_SELECT_FL_VISITOU
	(
		PM_CURSOR IN OUT TP_CURSOR,
		PM_CGC    IN NUMBER
	) IS
		VTP_SOLIC NUMBER := 0;
	BEGIN
		SELECT TP_SOLICITACAO
		  INTO VTP_SOLIC
		  FROM (SELECT *
				  FROM INTRANET.CONTROLE_CLIENTE_WEB@LNK_IDPK
				 WHERE CGC = PM_CGC
				 ORDER BY DT_SOLICITACAO DESC)
		 WHERE ROWNUM = 1;
	
		OPEN PM_CURSOR FOR
			SELECT FL_REPRES_VISITOU,
				   OBSERVACOES
			  FROM INTRANET.CLIENTE_WEB@LNK_IDPK
			 WHERE CGC = PM_CGC
			   AND TP_SOLICITACAO = VTP_SOLIC;
	
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			OPEN PM_CURSOR FOR
				SELECT 'N' FL_REPRES_VISITOU,
					   '' OBSERVACOES
				  FROM DUAL;
		
	END;
END PCK_CAD060;
/
