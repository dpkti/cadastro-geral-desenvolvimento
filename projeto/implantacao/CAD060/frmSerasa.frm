VERSION 5.00
Object = "{EAB22AC0-30C1-11CF-A7EB-0000C05BAE0B}#1.1#0"; "ieframe.dll"
Object = "{38911DA0-E448-11D0-84A3-00DD01104159}#1.1#0"; "comct332.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Begin VB.Form frmSerasa 
   Caption         =   "Consultar"
   ClientHeight    =   5400
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10305
   LinkTopic       =   "Form1"
   ScaleHeight     =   5400
   ScaleWidth      =   10305
   WindowState     =   2  'Maximized
   Begin SHDocVwCtl.WebBrowser Browse 
      Height          =   4065
      Left            =   30
      TabIndex        =   4
      Top             =   1260
      Width           =   7215
      ExtentX         =   12726
      ExtentY         =   7170
      ViewMode        =   0
      Offline         =   0
      Silent          =   0
      RegisterAsBrowser=   0
      RegisterAsDropTarget=   1
      AutoArrange     =   0   'False
      NoClientEdge    =   0   'False
      AlignLeft       =   0   'False
      NoWebView       =   0   'False
      HideFileNames   =   0   'False
      SingleClick     =   0   'False
      SingleSelection =   0   'False
      NoFolders       =   0   'False
      Transparent     =   0   'False
      ViewID          =   "{0057D0E0-3573-11CF-AE69-08002B2E1262}"
      Location        =   "http:///"
   End
   Begin ComCtl3.CoolBar CoolBar1 
      Align           =   1  'Align Top
      Height          =   1230
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   10305
      _ExtentX        =   18177
      _ExtentY        =   2170
      BandCount       =   2
      BackColor       =   14737632
      FixedOrder      =   -1  'True
      _CBWidth        =   10305
      _CBHeight       =   1230
      _Version        =   "6.7.9816"
      Child1          =   "Toolbar1"
      MinHeight1      =   780
      Width1          =   4950
      NewRow1         =   0   'False
      Child2          =   "cboSite"
      MinHeight2      =   360
      Width2          =   7755
      FixedBackground2=   0   'False
      NewRow2         =   -1  'True
      Begin MSComctlLib.Toolbar Toolbar1 
         Height          =   780
         Left            =   30
         TabIndex        =   2
         Top             =   30
         Width           =   10185
         _ExtentX        =   17965
         _ExtentY        =   1376
         ButtonWidth     =   1296
         ButtonHeight    =   1376
         Style           =   1
         ImageList       =   "ImageList1"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   15
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Serasa"
               Key             =   "Serasa"
               ImageIndex      =   1
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "EquiFax"
               Key             =   "EquiFax"
               ImageIndex      =   5
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   4
            EndProperty
            BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Salvar"
               Key             =   "Salvar"
               ImageIndex      =   6
            EndProperty
            BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Listagem"
               Key             =   "Listagem"
               ImageIndex      =   7
            EndProperty
            BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Parar"
               Key             =   "Parar"
               ImageIndex      =   2
            EndProperty
            BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Refresh"
               Key             =   "Refresh"
               ImageIndex      =   3
            EndProperty
            BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   4
            EndProperty
            BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Voltar"
               Key             =   "Voltar"
               ImageIndex      =   8
            EndProperty
            BeginProperty Button12 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Pr�ximo"
               Key             =   "Pr�ximo"
               ImageIndex      =   9
            EndProperty
            BeginProperty Button13 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   4
            EndProperty
            BeginProperty Button14 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Sair"
               Key             =   "Sair"
               ImageIndex      =   4
            EndProperty
            BeginProperty Button15 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Ir Para"
               Key             =   "IR"
               Object.ToolTipText     =   "Ir para o Site informado"
               ImageIndex      =   10
            EndProperty
         EndProperty
         Begin VB.CommandButton Command1 
            BackColor       =   &H00E0E0E0&
            Caption         =   "Navigate"
            Default         =   -1  'True
            Height          =   315
            Left            =   8760
            TabIndex        =   3
            Top             =   180
            Visible         =   0   'False
            Width           =   1395
         End
         Begin MSComctlLib.ImageList ImageList1 
            Left            =   8700
            Top             =   120
            _ExtentX        =   1005
            _ExtentY        =   1005
            BackColor       =   -2147483643
            ImageWidth      =   32
            ImageHeight     =   32
            MaskColor       =   12632256
            _Version        =   393216
            BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
               NumListImages   =   10
               BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmSerasa.frx":0000
                  Key             =   ""
               EndProperty
               BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmSerasa.frx":08DA
                  Key             =   ""
               EndProperty
               BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmSerasa.frx":663C
                  Key             =   ""
               EndProperty
               BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmSerasa.frx":6796
                  Key             =   ""
               EndProperty
               BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmSerasa.frx":CA30
                  Key             =   ""
               EndProperty
               BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmSerasa.frx":D30A
                  Key             =   ""
               EndProperty
               BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmSerasa.frx":DBE4
                  Key             =   ""
               EndProperty
               BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmSerasa.frx":E4BE
                  Key             =   ""
               EndProperty
               BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmSerasa.frx":E910
                  Key             =   ""
               EndProperty
               BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                  Picture         =   "frmSerasa.frx":ED62
                  Key             =   ""
               EndProperty
            EndProperty
         End
      End
      Begin VB.ComboBox cboSite 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   360
         ItemData        =   "frmSerasa.frx":F1E4
         Left            =   165
         List            =   "frmSerasa.frx":F1EE
         TabIndex        =   1
         Top             =   840
         Width           =   10050
      End
   End
End
Attribute VB_Name = "frmSerasa"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : frmSerasa
' Author    : C.SAMUEL.OLIVEIRA
' Date      : 22/02/16
' Purpose   : TI-4188
'---------------------------------------------------------------------------------------

Option Explicit
Public Documento As String
Public Pagina As String
Dim vNewWindow As Boolean

Private Sub cboSite_Click()
    Command1_Click
End Sub


Private Sub Command1_Click()
    Browse.Navigate cboSite
End Sub

Private Sub Form_Load()
    If vNewWindow = False Then
        Caminho = Pegar_VL_Parametro("CAMINHOSERASA")
        Browse.Navigate "ABOUT:BLANK"
    End If
End Sub

Private Sub Form_Resize()
    If Me.Height > 360 Then
        Browse.Width = Me.Width - 200
        'Browse.Height = Me.Height - (Toolbar1.Height)
        Dim altura As Integer
        altura = Me.Height - (Toolbar1.Height) - 1000
        If altura <= 0 Then altura = 1
        Browse.Height = altura
    End If
    
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case Button.Key
        Case "Serasa"
            Pagina = "Serasa"
            'cboSite = "https://sitenet11.serasa.com.br"
            cboSite = "" & Pegar_VL_Parametro("SITESERASA") 'TI-4188
            Browse.Navigate cboSite.Text
        
        Case "EquiFax"
            Pagina = "Equifax"
            cboSite = "www.equifax.com.br"
            Browse.Navigate cboSite.Text
        
        Case "Salvar"
            frmDocto.Show 1
            
        Case "Listagem"
            frmConsulta.Show 1
            
        Case "Parar"
            Browse.Stop
        
        Case "Refresh"
            Browse.Refresh
        
        Case "Voltar"
            Browse.GoBack
            
        Case "Pr�ximo"
            Browse.GoForward
        
        Case "Sair"
            Unload Me
        
        Case Else
            Pagina = "Equifax"
            Browse.Navigate cboSite
            
    End Select
End Sub
