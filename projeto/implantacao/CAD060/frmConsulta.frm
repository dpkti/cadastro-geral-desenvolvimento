VERSION 5.00
Object = "{EAB22AC0-30C1-11CF-A7EB-0000C05BAE0B}#1.1#0"; "shdocvw.dll"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmConsulta 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Listagem das Consultas"
   ClientHeight    =   7185
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11310
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7185
   ScaleWidth      =   11310
   ShowInTaskbar   =   0   'False
   Begin VB.FileListBox FileEquiFax 
      Height          =   480
      Left            =   5190
      TabIndex        =   3
      Top             =   1170
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.FileListBox FileSerasa 
      Height          =   480
      Left            =   210
      TabIndex        =   2
      Top             =   1170
      Visible         =   0   'False
      Width           =   1245
   End
   Begin Bot�o.cmd cmdImprimir 
      Height          =   645
      Left            =   9780
      TabIndex        =   1
      Top             =   210
      Width           =   705
      _ExtentX        =   1244
      _ExtentY        =   1138
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsulta.frx":0000
      PICN            =   "frmConsulta.frx":001C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSComctlLib.ListView lsvEquifax 
      Height          =   1575
      Left            =   4920
      TabIndex        =   0
      Top             =   240
      Width           =   4785
      _ExtentX        =   8440
      _ExtentY        =   2778
      View            =   3
      LabelEdit       =   1
      Sorted          =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   3
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Seq"
         Object.Width           =   882
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Documento"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Data"
         Object.Width           =   2646
      EndProperty
   End
   Begin SHDocVwCtl.WebBrowser Browser 
      CausesValidation=   0   'False
      Height          =   5295
      Left            =   45
      TabIndex        =   4
      Top             =   1800
      Width           =   11205
      ExtentX         =   19764
      ExtentY         =   9340
      ViewMode        =   0
      Offline         =   0
      Silent          =   0
      RegisterAsBrowser=   0
      RegisterAsDropTarget=   0
      AutoArrange     =   0   'False
      NoClientEdge    =   0   'False
      AlignLeft       =   0   'False
      NoWebView       =   0   'False
      HideFileNames   =   0   'False
      SingleClick     =   0   'False
      SingleSelection =   0   'False
      NoFolders       =   0   'False
      Transparent     =   0   'False
      ViewID          =   "{0057D0E0-3573-11CF-AE69-08002B2E1262}"
      Location        =   "http:///"
   End
   Begin MSComctlLib.ListView lsvSerasa 
      Height          =   1575
      Left            =   60
      TabIndex        =   5
      Top             =   240
      Width           =   4785
      _ExtentX        =   8440
      _ExtentY        =   2778
      View            =   3
      LabelEdit       =   1
      Sorted          =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   3
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Seq"
         Object.Width           =   882
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Documento"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Data"
         Object.Width           =   2646
      EndProperty
   End
   Begin Bot�o.cmd cmd1 
      Height          =   645
      Left            =   10530
      TabIndex        =   8
      Top             =   210
      Width           =   705
      _ExtentX        =   1244
      _ExtentY        =   1138
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsulta.frx":0CF6
      PICN            =   "frmConsulta.frx":0D12
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "EquiFax"
      Height          =   195
      Left            =   4920
      TabIndex        =   7
      Top             =   30
      Width           =   570
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Serasa"
      Height          =   195
      Left            =   90
      TabIndex        =   6
      Top             =   30
      Width           =   495
   End
End
Attribute VB_Name = "frmConsulta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim LsvClicado As String
Dim I As Integer
Public vCodCli As String

Private Sub Browser_NavigateComplete2(ByVal pDisp As Object, URL As Variant)

    lsvSerasa.Enabled = True

End Sub

Private Sub lsvSerasa_ItemClick(ByVal Item As MSComctlLib.ListItem)

    On Error GoTo ERRO
    
    lsvSerasa.Enabled = False

    LsvClicado = "S"
    'Browser.Navigate Caminho & "Serasa\S_" & Format(vCodCli, "000000") & "_" & lsvSerasa.SelectedItem.SubItems(1) & "_" & Format(CDate(lsvSerasa.SelectedItem.SubItems(2)), "YYYYMMDD") & ".htm", 1
    
    Open Caminho & "Serasa\S_" & Format(vCodCli, "000000") & "_" & lsvSerasa.SelectedItem.SubItems(1) & "_" & Format(CDate(lsvSerasa.SelectedItem.SubItems(2)), "YYYYMMDD") & ".htm" For Input As #1
    Open Caminho & "Serasa\S_" & Format(vCodCli, "000000") & "_" & lsvSerasa.SelectedItem.SubItems(1) & "_" & Format(CDate(lsvSerasa.SelectedItem.SubItems(2)), "YYYYMMDD") & ".htm_temp" For Output As #2
    
    Dim vLinha, vLinhaBranco
    
    While Not EOF(1)
    
        Line Input #1, vLinha
        
        If InStr(1, vLinha, "exibeBannerConfie") > 0 Then
        
            Print #2, vLinhaBranco
            
        Else
        
            Print #2, vLinha
        
        End If
    
    Wend
    
    Close #1
    Close #2
    
    Dim fs As New FileSystemObject
    fs.DeleteFile Caminho & "Serasa\S_" & Format(vCodCli, "000000") & "_" & lsvSerasa.SelectedItem.SubItems(1) & "_" & Format(CDate(lsvSerasa.SelectedItem.SubItems(2)), "YYYYMMDD") & ".htm", True
    fs.MoveFile Caminho & "Serasa\S_" & Format(vCodCli, "000000") & "_" & lsvSerasa.SelectedItem.SubItems(1) & "_" & Format(CDate(lsvSerasa.SelectedItem.SubItems(2)), "YYYYMMDD") & ".htm_temp", Caminho & "Serasa\S_" & Format(vCodCli, "000000") & "_" & lsvSerasa.SelectedItem.SubItems(1) & "_" & Format(CDate(lsvSerasa.SelectedItem.SubItems(2)), "YYYYMMDD") & ".htm"
    
    'Browser.Navigate Caminho & "Serasa\S_" & Format(vCodCli, "000000") & "_" & lsvSerasa.SelectedItem.SubItems(1) & "_" & Format(CDate(lsvSerasa.SelectedItem.SubItems(2)), "YYYYMMDD") & ".htm"
    Browser.Navigate Caminho & "Serasa\S_" & Format(vCodCli, "000000") & "_" & lsvSerasa.SelectedItem.SubItems(1) & "_" & Format(CDate(lsvSerasa.SelectedItem.SubItems(2)), "YYYYMMDD") & ".htm"

ERRO:

    If Err.Number = 70 Then
    
        If Dir(Caminho & "Serasa\S_" & Format(vCodCli, "000000") & "_" & lsvSerasa.SelectedItem.SubItems(1) & "_" & Format(CDate(lsvSerasa.SelectedItem.SubItems(2)), "YYYYMMDD") & ".htm_temp") <> "" Then
        
            Dim fs2 As New FileSystemObject
            fs2.DeleteFile Caminho & "Serasa\S_" & Format(vCodCli, "000000") & "_" & lsvSerasa.SelectedItem.SubItems(1) & "_" & Format(CDate(lsvSerasa.SelectedItem.SubItems(2)), "YYYYMMDD") & ".htm_temp", True
        
        End If
        
        MsgBox "Este cliente est� sendo consultado por outro usu�rio. Favor consultar mais tarde."
        Exit Sub
    
    End If

End Sub
Private Sub Browser_BeforeNavigate2(ByVal pDisp As Object, URL As Variant, Flags As Variant, TargetFrameName As Variant, PostData As Variant, Headers As Variant, Cancel As Boolean)
    If InStr(1, UCase(URL), "WWW") > 0 Or InStr(1, UCase(URL), "JAVASCRIPT") Or InStr(1, UCase(URL), "ASP") Then
        If LsvClicado = "E" Then
            lsvEquifax_ItemClick lsvEquifax.SelectedItem
        ElseIf LsvClicado = "S" Then
            lsvSerasa_ItemClick lsvSerasa.SelectedItem
        End If
    End If
End Sub

Private Sub cmd1_Click()
    Unload Me
End Sub

Private Sub cmdImprimir_Click()
On Error GoTo ErrHandler
    
    Browser.ExecWB OLECMDID_PRINT, OLECMDEXECOPT_DODEFAULT: Exit Sub

ErrHandler:
    MsgBox Err.Description, vbInformation, "Cadastro de Clientes"

End Sub

Private Sub Form_Load()
'On Error Resume Next
    
    Dim fs As FileSystemObject
    Dim litem As ListItem
    
    Set fs = New FileSystemObject
    
'    Caminho = "F:\SISTEMAS\ORACLE\CAD060\VB\vb60"
    Caminho = Pegar_VL_Parametro("CAMINHOSERASA")
    
    If Not fs.FolderExists(Caminho & "Serasa") Then
        fs.CreateFolder Caminho & "Serasa"
    End If
    If Not fs.FolderExists(Caminho & "EquiFax") Then
        fs.CreateFolder Caminho & "EquiFax"
    End If
    
    Preenche_listas
        
    Browser.Navigate "ABOUT:BLANK"
        
End Sub

Private Sub lsvEquifax_ItemClick(ByVal Item As MSComctlLib.ListItem)
    LsvClicado = "E"
    Browser.Navigate Caminho & "Equifax\E_" & Format(vCodCli, "000000") & "_" & lsvEquifax.SelectedItem.SubItems(1) & "_" & Format(CDate(lsvEquifax.SelectedItem.SubItems(2)), "YYYYMMDD") & ".htm"
End Sub

Private Sub lsvEquifax_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim Data As String
    If Button = 2 Then
        If MsgBox("Confirma Exclus�o do Item selecionado ?", vbYesNo + vbQuestion, "Excluir Consulta") = vbYes Then
           Data = lsvEquifax.SelectedItem.SubItems(2)
           Data = Right(Data, 4) & Mid(Data, 4, 2) & Left(Data, 2)
           'Kill Caminho & "EquiFax\E_" & lsvEquifax.SelectedItem.SubItems(1) & "_" & Data & ".htm"
           Kill Caminho & "EquiFax\E_" & Format(vCodCli, "000000") & "_" & lsvEquifax.SelectedItem.SubItems(1) & "_" & Data & ".htm"
           Preenche_listas
        End If
    End If
End Sub

Private Sub lsvSerasa_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim Data As String
    If Button = 2 Then
        If MsgBox("Confirma Exclus�o do Item selecionado ?", vbYesNo + vbQuestion, "Excluir Consulta") = vbYes Then
           Data = Me.lsvSerasa.SelectedItem.SubItems(2)
           Data = Right(Data, 4) & Mid(Data, 4, 2) & Left(Data, 2)
           Kill Caminho & "Serasa\S_" & Format(vCodCli, "000000") & "_" & lsvSerasa.SelectedItem.SubItems(1) & "_" & Data & ".htm"
           Preenche_listas
        End If
    End If
End Sub
 
Sub Preenche_listas()
    Dim Data As String
    Dim Doc As String
    Dim CodCli As String
    Dim litem As ListItem
    
    FileSerasa.Refresh
    FileEquiFax.Refresh
    
    If Trim(frmCadCliente.txtCodCliente) <> "" Then
        vCodCli = frmCadCliente.txtCodCliente
    End If
    
    FileSerasa.Path = Caminho & "Serasa"
    FileEquiFax.Path = Caminho & "EquiFax"
        
    lsvSerasa.ListItems.Clear
    lsvEquifax.ListItems.Clear
    For I = 0 To FileSerasa.ListCount - 1
        If InStr(1, FileSerasa.List(I), "_") > 0 Then
            CodCli = Mid(FileSerasa.List(I), 3, 6)
            Doc = Mid(FileSerasa.List(I), 10, InStr(10, FileSerasa.List(I), "_") - 10)
            Data = Left(Right(FileSerasa.List(I), 12), 8)
            
            If CDbl(CodCli) = Val(vCodCli) Then
                Set litem = lsvSerasa.ListItems.Add
                litem = I + 1
                litem.SubItems(1) = Doc
                litem.SubItems(2) = Right(Data, 2) & "/" & Mid(Data, 5, 2) & "/" & Left(Data, 4)
            End If
        End If
    Next
        
    For I = 0 To FileEquiFax.ListCount - 1
        CodCli = Mid(FileEquiFax.List(I), 3, 6)
        Doc = Mid(FileEquiFax.List(I), 10, InStr(10, FileEquiFax.List(I), "_") - 10)
        Data = Left(Right(FileEquiFax.List(I), 12), 8)
        If IsNumeric(CodCli) Then
            If CDbl(CodCli) = Val(vCodCli) Then
                Set litem = lsvEquifax.ListItems.Add
                litem = I + 1
                litem.SubItems(1) = Doc
                litem.SubItems(2) = Right(Data, 2) & "/" & Mid(Data, 5, 2) & "/" & Left(Data, 4)
            End If
        End If
    Next
End Sub


