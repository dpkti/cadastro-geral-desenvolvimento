VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmResponsavel 
   BackColor       =   &H00FFFFFF&
   Caption         =   "CAD060 - Manuten��o de Usu�rio Respons�vel"
   ClientHeight    =   5655
   ClientLeft      =   225
   ClientTop       =   1530
   ClientWidth     =   10020
   LinkTopic       =   "Form1"
   ScaleHeight     =   5655
   ScaleWidth      =   10020
   Begin VB.Frame fraTrocaRespCli 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Novo Usu�rio Repons�vel"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   825
      Left            =   345
      TabIndex        =   14
      Top             =   1680
      Visible         =   0   'False
      Width           =   5805
      Begin VB.ComboBox cboRespCli 
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   105
         TabIndex        =   4
         Top             =   285
         Width           =   4815
      End
      Begin Bot�o.cmd cmdConfTrocaRespCli 
         Height          =   510
         Left            =   5115
         TabIndex        =   5
         ToolTipText     =   "Confirmar Troca"
         Top             =   180
         Width           =   525
         _ExtentX        =   926
         _ExtentY        =   900
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "FrmResponsavel.frx":0000
         PICN            =   "FrmResponsavel.frx":001C
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Troca de Respons�vel para casos de Altera��o de Carteira"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2730
      Left            =   195
      TabIndex        =   13
      Top             =   2775
      Width           =   9585
      Begin VB.ComboBox cboNovoResp 
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   240
         TabIndex        =   7
         Top             =   1335
         Width           =   4815
      End
      Begin VB.ComboBox cboRespAtu 
         BackColor       =   &H00C0E0FF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   240
         TabIndex        =   6
         Top             =   600
         Width           =   4815
      End
      Begin Bot�o.cmd cmdConfTrocaResp 
         Height          =   510
         Left            =   225
         TabIndex        =   8
         ToolTipText     =   "Confirmar Troca de Resp. para Todos os Clientes da Carteira"
         Top             =   1800
         Width           =   525
         _ExtentX        =   926
         _ExtentY        =   900
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "FrmResponsavel.frx":0CF6
         PICN            =   "FrmResponsavel.frx":0D12
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MSFlexGridLib.MSFlexGrid grdflexCliente 
         Height          =   1995
         Left            =   5475
         TabIndex        =   17
         Top             =   585
         Width           =   3630
         _ExtentX        =   6403
         _ExtentY        =   3519
         _Version        =   393216
         BackColorBkg    =   14737632
         TextStyle       =   1
         TextStyleFixed  =   1
         GridLines       =   2
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label5 
         BackColor       =   &H00FFFFFF&
         Caption         =   "Usu�rio Atual Respons�vel pelos Clientes:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   5490
         TabIndex        =   18
         Top             =   330
         Width           =   3735
      End
      Begin VB.Label Label4 
         BackColor       =   &H00FFFFFF&
         Caption         =   "Novo Usu�rio Respons�vel"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   240
         TabIndex        =   16
         Top             =   1065
         Width           =   2385
      End
      Begin VB.Label Label3 
         BackColor       =   &H00FFFFFF&
         Caption         =   "Usu�rio Respons�vel Atual"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   240
         TabIndex        =   15
         Top             =   330
         Width           =   2385
      End
   End
   Begin VB.Frame Frame8 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Consulta/Altera��o de Respons�vel"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2535
      Left            =   195
      TabIndex        =   10
      Top             =   90
      Width           =   6120
      Begin VB.TextBox txtCGC 
         BackColor       =   &H000040C0&
         BeginProperty DataFormat 
            Type            =   0
            Format          =   "00.000.000/0000-00"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1046
            SubFormatType   =   0
         EndProperty
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Left            =   240
         MaxLength       =   18
         TabIndex        =   0
         Top             =   585
         Width           =   1650
      End
      Begin VB.TextBox txtCliente 
         Alignment       =   2  'Center
         BackColor       =   &H000040C0&
         BeginProperty DataFormat 
            Type            =   0
            Format          =   "00.000.000/0000-00"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1046
            SubFormatType   =   0
         EndProperty
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Left            =   1890
         Locked          =   -1  'True
         MaxLength       =   18
         TabIndex        =   1
         Top             =   585
         Width           =   3255
      End
      Begin Bot�o.cmd cmdTrocaRespCli 
         Height          =   510
         Left            =   5265
         TabIndex        =   3
         ToolTipText     =   "Trocar respons�vel deste cliente"
         Top             =   990
         Width           =   525
         _ExtentX        =   926
         _ExtentY        =   900
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "FrmResponsavel.frx":19EC
         PICN            =   "FrmResponsavel.frx":1A08
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFFFFF&
         Caption         =   "CGC do Cliente"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   240
         TabIndex        =   12
         Top             =   330
         Width           =   1320
      End
      Begin VB.Label lblResponsavel 
         BackColor       =   &H00C0E0FF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   315
         Left            =   240
         TabIndex        =   2
         Top             =   1185
         Width           =   4875
      End
      Begin VB.Label Label2 
         BackColor       =   &H00FFFFFF&
         Caption         =   "Usu�rio Respons�vel Atual"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   240
         TabIndex        =   11
         Top             =   930
         Width           =   2385
      End
   End
   Begin Bot�o.cmd cmdVoltar2 
      Height          =   585
      Left            =   9105
      TabIndex        =   9
      ToolTipText     =   "Voltar"
      Top             =   165
      Width           =   585
      _ExtentX        =   1032
      _ExtentY        =   1032
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "FrmResponsavel.frx":26E2
      PICN            =   "FrmResponsavel.frx":26FE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   1
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmResponsavel"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private vRespCli_Atual As Long     'Guarda o c�digo do respons�vel atual do cliente
Private vRespCli_Novo As Long      'Guarda o c�digo do novo respons�vel pelo cliente
Private vRespCart_Atual As Long     'Guarda o c�digo do respons�vel atual pela carteira de clientes
Private vRespCart_Novo As Long      'Guarda o c�digo do novo respons�vel pela carteira de clientes



Private Sub cmd1_Click()

End Sub

Private Sub cboRespAtu_Click()
    Call cboRespAtu_LostFocus
End Sub

Private Sub cboRespAtu_LostFocus()

    If Trim(cboRespAtu.Text) <> "" Then
        
        vRespCart_Atual = Val(Mid(cboRespAtu, InStr(cboRespAtu, "-") + 2, 5))
        
        '-------------------------------
        'Traz todos os clientes que est�o sob a responsabilidade do usu�rio escolhido
        '-------------------------------
        Set OraParameters = db_INT.Parameters
        OraParameters.Remove "USERATU":     OraParameters.Add "USERATU", vRespCart_Atual, 1
        OraParameters.Remove "TP_CONS":     OraParameters.Add "TP_CONS", 0, 1
        OraParameters.Remove "vErro":       OraParameters.Add "vErro", 0, 2
        OraParameters.Remove "vMsgErro":    OraParameters.Add "vMsgErro", "", 2
        Criar_Cursor OraParameters, "pm_Cursor1"
        vErro = vVB_Generica_001.ExecutaPl(db_INT, "INTRANET.PCK_CLIENTES_TESTE.Pr_Cli_Responsavel(:pm_CURSOR1, :USERATU, :TP_CONS, :vErro, :vMsgErro)")
        
        If vErro = "" Then
            Set db_CONS = OraParameters("PM_CURSOR1").Value
        Else
            MsgBox "Problemas na consulta. Contate o suporte.", vbInformation, "CAD060 - Cadastro de Clientes"
            Exit Sub
        End If
        
        If db_CONS.EOF Then
            Call vVB_Generica_001.LimpaFlexGridComTitulo(grdflexCliente)
            MsgBox "N�o existem clientes sob a responsabilidade deste usu�rio!", vbInformation, "CAD060 - Cadastro de Clientes"
            Exit Sub
        End If
    
        Call vVB_Generica_001.CarregaGridTabela(grdflexCliente, db_CONS, 2)
    
    End If

End Sub


Private Sub cmdConfTrocaResp_Click()

If MsgBox("Confirma a troca de respons�vel da carteira de clientes ?", vbQuestion + vbYesNo, "CAD060 - Cadastro de Clientes") = vbYes Then
    
    If cboRespAtu.Text = "" Then
        MsgBox "Por favor, indique o respons�vel atual a ser substitu�do.", vbInformation, "CAD060 - Aten��o"
        cboRespAtu.SetFocus
        Exit Sub
    End If
    If cboNovoResp.Text = "" Then
        MsgBox "Por favor, indique o novo respons�vel para a carteira de clientes.", vbInformation, "CAD060 - Aten��o"
        cboNovoResp.SetFocus
        Exit Sub
    End If

    Screen.MousePointer = 11
    
    vRespCart_Novo = Val(Mid(cboNovoResp, InStr(cboNovoResp, "-") + 2, 5))

    'Troca o respons�vel de uma carteira de clientes
    Set OraParameters = db_INT.Parameters
    OraParameters.Remove "CGC":         OraParameters.Add "CGC", 0, 1
    OraParameters.Remove "USERATU":     OraParameters.Add "USERATU", vRespCart_Atual, 1
    OraParameters.Remove "USERNOVO":    OraParameters.Add "USERNOVO", vRespCart_Novo, 1
    OraParameters.Remove "TP_CONS":     OraParameters.Add "TP_CONS", 1, 1
    OraParameters.Remove "vErro":       OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro":    OraParameters.Add "vMsgErro", "", 2
        
    db_INT.Executesql "BEGIN INTRANET.PCK_CLIENTES_TESTE.PR_TROCA_RESP_CLI(:CGC,:USERATU,:USERNOVO,:TP_CONS,:vErro,:vMsgErro); END;"
   
    If Val(IIf(IsNull(OraParameters("vErro")), 0, OraParameters("vErro"))) <> 0 Then
        Screen.MousePointer = 0
        MsgBox "Erro: " & Val(OraParameters("vErro").Value) & " - " & _
                          OraParameters("vMsgErro").Value & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
        Exit Sub
    Else
        MsgBox "Troca de respons�veis efetuada com sucesso para esta carteira!", vbInformation, "CAD060 - Aten��o"
        Call vVB_Generica_001.LimpaFlexGridComTitulo(grdflexCliente)
        cboRespAtu.Text = ""
        cboNovoResp.Text = ""
        'Vai carregar combo com todos os usu�rios que j� t�m clientes sob sua responsabilidade
        'Isso para atualizar caso tenha atribu�do clientes a algum usu�rio que antes n�o possu�a nenhum sob sua responsabilidade
        Call Carrega_Combo_Responsaveis("CLIENTE", cboRespAtu)
    End If
End If

Screen.MousePointer = 0


End Sub


Private Sub cmdConfTrocaRespCli_Click()

If MsgBox("Confirma a troca de respons�vel do cliente " & txtCGC & " ?", vbQuestion + vbYesNo, "CAD060 - Cadastro de Clientes") = vbYes Then
    
    If lblResponsavel.Caption = "" Then
        MsgBox "A troca n�o poder� ser feita pois n�o existe um respons�vel atual para este cliente.", vbCritical, "CAD060 - Aten��o"
        txtCGC.SetFocus
        Exit Sub
    End If
    If cboRespCli.Text = "" Then
        MsgBox "Por favor, indique o novo respons�vel para o cliente.", vbInformation, "CAD060 - Aten��o"
        cboRespCli.SetFocus
        Exit Sub
    End If
        
    Screen.MousePointer = 11
        
    vRespCli_Novo = Val(Mid(cboRespCli, InStr(cboRespCli, "-") + 2, 5))
        
    'Troca o respons�vel de um cliente espec�fico
    Set OraParameters = db_INT.Parameters
    OraParameters.Remove "CGC":         OraParameters.Add "CGC", Val(txtCGC.Text), 1
    OraParameters.Remove "USERATU":     OraParameters.Add "USERATU", vRespCli_Atual, 1
    OraParameters.Remove "USERNOVO":    OraParameters.Add "USERNOVO", vRespCli_Novo, 1
    OraParameters.Remove "TP_CONS":     OraParameters.Add "TP_CONS", 0, 1
    OraParameters.Remove "vErro":       OraParameters.Add "vErro", 0, 2
    OraParameters.Remove "vMsgErro":    OraParameters.Add "vMsgErro", "", 2
        
    db_INT.Executesql "BEGIN INTRANET.PCK_CLIENTES_TESTE.PR_TROCA_RESP_CLI(:CGC,:USERATU,:USERNOVO,:TP_CONS,:vErro,:vMsgErro); END;"
   
    If Val(IIf(IsNull(OraParameters("vErro")), 0, OraParameters("vErro"))) <> 0 Then
        Screen.MousePointer = 0
        MsgBox "Erro: " & Val(OraParameters("vErro").Value) & " - " & _
                          OraParameters("vMsgErro").Value & ". Contate o Suporte!", vbInformation, "CAD060 - Aten��o"
        Exit Sub
    Else
        MsgBox "Troca de respons�veis efetuada com sucesso para este cliente!", vbInformation, "CAD060 - Aten��o"
        txtCGC = ""
        txtCliente = ""
        lblResponsavel = ""
        cboRespCli.Text = ""
        'Vai carregar combo com todos os usu�rios que j� t�m clientes sob sua responsabilidade
        'Isso para atualizar caso tenha atribu�do clientes a algum usu�rio que antes n�o possu�a nenhum sob sua responsabilidade
        Call Carrega_Combo_Responsaveis("CLIENTE", cboRespAtu)
    End If
    Screen.MousePointer = 0

    
End If
            
End Sub


Private Sub cmdTrocaRespCli_Click()

    fraTrocaRespCli.Visible = True
    
    'Vai carregar combo com todos os usu�rios cadastrados na Intrante para poder substituir
    vRespCli_Atual = Val(Mid(lblResponsavel, 1, InStr(lblResponsavel, "-") - 2))
    
    Call Carrega_Combo_Responsaveis("TODOS", cboRespCli)
    

End Sub


Private Sub cmdVoltar2_Click()
    mdiCAD060.SSPanel1.Visible = True
    Unload Me
End Sub

Private Sub Form_Load()
    
    Me.Top = mdiCAD060.Menu.Height * 4
    Me.Left = (mdiCAD060.Width - Me.Width) / 2
    
    fraTrocaRespCli.Visible = False
    
    'Vai carregar combo com todos os usu�rios que j� t�m clientes sob sua responsabilidade
    Call Carrega_Combo_Responsaveis("CLIENTE", cboRespAtu)
    'Vai carregar combo com todos os usu�rios cadastrados na Intranet
    Call Carrega_Combo_Responsaveis("TODOS", cboNovoResp)
    
    
End Sub



Private Sub txtCGC_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Numero(KeyAscii)

End Sub

Private Sub txtCGC_LostFocus()
Dim db_Cons2 As Object
    
    If Trim(txtCGC.Text) <> "" Then
        '-------------------------------
        'Verifica CGC do cliente
        '-------------------------------
        Set OraParameters = db_INT.Parameters
        OraParameters.Remove "CGC": OraParameters.Add "CGC", Val(txtCGC), 1
        OraParameters.Remove "TP_CONS": OraParameters.Add "TP_CONS", 0, 1
        OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
        OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
        Criar_Cursor OraParameters, "pm_Cursor1"
        vErro = vVB_Generica_001.ExecutaPl(db_INT, "INTRANET.PCK_CLIENTES_TESTE.Pr_RespCli(:pm_CURSOR1, :CGC, :TP_CONS, :vErro, :vMsgErro)")
        
        If vErro = "" Then
            Set db_CONS = OraParameters("PM_CURSOR1").Value
        Else
            MsgBox "Problemas na consulta. Contate o suporte.", vbInformation, "CAD060 - Cadastro de Clientes"
            Exit Sub
        End If
        
        If db_CONS.EOF Then
            MsgBox "Cliente ainda n�o foi digitado na Intranet!", vbInformation, "CAD060 - Cadastro de Clientes"
             Exit Sub
          End If
    
        'Se a solicita��o deste cliente foi recusada, n�o ser� poss�vel encontrar o nome dele na cliente
        'ent�o acessar a tabela cliente_web
        If Trim(db_CONS!nome_cliente) = "NOME INDISPONIVEL" Then
        
            'Verifica nome do cliente que j� foi aceito pelo Cr�dito
            Set OraParameters = db_INT.Parameters
            OraParameters.Remove "CGC": OraParameters.Add "CGC", Val(txtCGC), 1
            OraParameters.Remove "TP_CONS": OraParameters.Add "TP_CONS", 1, 1
            OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
            OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
            Criar_Cursor OraParameters, "pm_Cursor1"
            vErro = vVB_Generica_001.ExecutaPl(db_INT, "INTRANET.PCK_CLIENTES_TESTE.Pr_RespCli(:pm_CURSOR1, :CGC, :TP_CONS, :vErro, :vMsgErro)")
            If vErro = "" Then
                Set db_Cons2 = OraParameters("PM_CURSOR1").Value
            Else
                MsgBox "Problemas na consulta. Contate o suporte.", vbInformation, "CAD060 - Cadastro de Clientes"
                Exit Sub
            End If
            If db_Cons2.EOF Then
                MsgBox "N�o foi encontrado o nome deste cliente! Contate o Suporte.", vbInformation, "CAD060 - Cadastro de Clientes"
                Exit Sub
            End If
        
            txtCliente = db_Cons2!nome_cliente
        
        Else
            txtCliente = db_CONS!nome_cliente
        End If
        lblResponsavel = db_CONS!cod_usuario & " - " & db_CONS!nome_usuario
        fraTrocaRespCli.Visible = False
    End If

End Sub


