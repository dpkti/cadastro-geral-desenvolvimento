VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmConfirmarAlteracoes 
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "CAD060 - An�lise de Cliente - Altera��o"
   ClientHeight    =   6810
   ClientLeft      =   75
   ClientTop       =   1590
   ClientWidth     =   11190
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6810
   ScaleWidth      =   11190
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox txtSAPAlteracao 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0E0FF&
      Height          =   285
      Left            =   10320
      Locked          =   -1  'True
      TabIndex        =   17
      Top             =   45
      Width           =   855
   End
   Begin VB.TextBox txtCidade 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0E0FF&
      Height          =   285
      Left            =   2355
      Locked          =   -1  'True
      TabIndex        =   15
      Top             =   360
      Width           =   2520
   End
   Begin VB.TextBox txtUF 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0E0FF&
      Height          =   285
      Left            =   4890
      Locked          =   -1  'True
      TabIndex        =   14
      Top             =   360
      Width           =   480
   End
   Begin VB.TextBox txtCep 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0E0FF&
      Height          =   285
      Left            =   5445
      Locked          =   -1  'True
      TabIndex        =   13
      Top             =   360
      Width           =   840
   End
   Begin VB.TextBox txtDtRecadastro 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0E0FF&
      Height          =   285
      Left            =   6360
      Locked          =   -1  'True
      TabIndex        =   12
      Top             =   360
      Width           =   1050
   End
   Begin VB.TextBox txtNomeCli 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0E0FF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   2355
      Locked          =   -1  'True
      TabIndex        =   11
      Top             =   45
      Width           =   5055
   End
   Begin VB.TextBox txtCodCliente 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0E0FF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   9000
      Locked          =   -1  'True
      TabIndex        =   8
      Top             =   45
      Width           =   855
   End
   Begin VB.TextBox txtCGC 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0E0FF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   9000
      Locked          =   -1  'True
      TabIndex        =   7
      Top             =   360
      Width           =   1905
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Todos os Campos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3165
      Left            =   30
      TabIndex        =   2
      Top             =   3615
      Width           =   8685
      Begin MSComctlLib.ListView lsvGeral 
         Height          =   2865
         Left            =   60
         TabIndex        =   3
         Top             =   210
         Width           =   8565
         _ExtentX        =   15108
         _ExtentY        =   5054
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Campo"
            Object.Width           =   4410
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Valor Atual"
            Object.Width           =   8819
         EndProperty
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00FFFFFF&
      Caption         =   "Campos Alterados"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2925
      Left            =   30
      TabIndex        =   0
      Top             =   675
      Width           =   11085
      Begin MSComctlLib.ListView lsvAlterados 
         Height          =   2655
         Left            =   60
         TabIndex        =   1
         Top             =   210
         Width           =   10965
         _ExtentX        =   19341
         _ExtentY        =   4683
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   3
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Campo"
            Object.Width           =   4410
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "De"
            Object.Width           =   7056
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Para"
            Object.Width           =   7056
         EndProperty
      End
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   540
      Left            =   1305
      TabIndex        =   4
      Top             =   45
      Width           =   585
      _ExtentX        =   1032
      _ExtentY        =   953
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConfirmarAlteracoes.frx":0000
      PICN            =   "frmConfirmarAlteracoes.frx":001C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdRecusar 
      Height          =   540
      Left            =   690
      TabIndex        =   5
      ToolTipText     =   "Recusar Pr�-Altera��o"
      Top             =   45
      Width           =   585
      _ExtentX        =   1032
      _ExtentY        =   953
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConfirmarAlteracoes.frx":0CF6
      PICN            =   "frmConfirmarAlteracoes.frx":0D12
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdGravar 
      Height          =   540
      Left            =   75
      TabIndex        =   6
      ToolTipText     =   "Aceitar Pr�-Altera��o"
      Top             =   45
      Width           =   585
      _ExtentX        =   1032
      _ExtentY        =   953
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConfirmarAlteracoes.frx":15EC
      PICN            =   "frmConfirmarAlteracoes.frx":1608
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label lbSAPAlteracao 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "SAP:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   9960
      TabIndex        =   16
      Top             =   120
      Width           =   375
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "C�digo do Cliente:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   7440
      TabIndex        =   10
      Top             =   120
      Width           =   1515
   End
   Begin VB.Label lblCGC 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "CGC do Cliente:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   7680
      TabIndex        =   9
      Top             =   435
      Width           =   1290
   End
End
Attribute VB_Name = "frmConfirmarAlteracoes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public vFl_Alt_Dados As Byte
Public vFechar As Boolean
Private vEnderecoCobranca As endereco

Private Sub cmdGravar_Click()
    Dim II As Integer
    Dim vUpdate(33, 2) As String
    Dim vCodTareAlt As String
    Dim vDtTareAlt As String
    'Eduardo Faria de Oliveira - CIT - ISA-353 - 23/08/12 - Declarando e Setando Objeto Cliente
    Dim oCliente As ClienteSAP
    Set oCliente = New ClienteSAP
    Dim dadosCliente As Object
    Dim msgErro As String
    Dim end_end As String
    Dim end_nro As String
    Dim end_cpl As String
    
    'Se n�o houver nenhum dado na tela � porque tudo que o cliente mandou como altera��o est� igual ao que est� na produ��o - Alethea - 14/05/07
    If vFl_Alt_Dados <> 0 Then
    
        vUpdate(0, 0) = "NOME CLIENTE":                  vUpdate(0, 1) = "BRANCO"
        vUpdate(1, 0) = "CGC":                           vUpdate(1, 1) = "BRANCO"
        'vUpdate(2, 0) = "ENDERE�O":                      vUpdate(2, 1) = "BRANCO"
        vUpdate(2, 0) = "ENDERECO":                      vUpdate(2, 1) = "BRANCO"
        vUpdate(3, 0) = "BAIRRO":                        vUpdate(3, 1) = "BRANCO"
        vUpdate(4, 0) = "CEP":                           vUpdate(4, 1) = "0"
        'vUpdate(5, 0) = "C�DIGO CIDADE":                 vUpdate(5, 1) = "0"
        vUpdate(5, 0) = "CODIGO CIDADE":                 vUpdate(5, 1) = "0"
        vUpdate(6, 0) = "DDD 1":                         vUpdate(6, 1) = "0"
        vUpdate(7, 0) = "FONE 1":                        vUpdate(7, 1) = "0"
        vUpdate(8, 0) = "DDD 2":                         vUpdate(8, 1) = "0"
        vUpdate(9, 0) = "FONE 2":                        vUpdate(9, 1) = "0"
        vUpdate(10, 0) = "NOME CONTATO":                 vUpdate(10, 1) = "BRANCO"
        vUpdate(11, 0) = "HOME PAGE":                    vUpdate(11, 1) = "BRANCO"
        vUpdate(12, 0) = "E-MAIL":                       vUpdate(12, 1) = "BRANCO"
        'vUpdate(13, 0) = "C�DIGO TARE":                  vUpdate(13, 1) = "BRANCO"
        vUpdate(13, 0) = "CODIGO TARE":                  vUpdate(13, 1) = "BRANCO"
        vUpdate(14, 0) = "DATA TARE":                    vUpdate(14, 1) = "BRANCO"
        'vUpdate(15, 0) = "C�DIGO TIPO CLIENTE":          vUpdate(15, 1) = "BRANCO"
        vUpdate(15, 0) = "CODIGO TIPO CLIENTE":          vUpdate(15, 1) = "BRANCO"
        vUpdate(16, 0) = "CATEGORIA BOSCH DIESEL":       vUpdate(16, 1) = "BRANCO"
        'vUpdate(17, 0) = "C�DIGO REPRESENTANTE":         vUpdate(17, 1) = "0"
        vUpdate(17, 0) = "CODIGO REPRESENTANTE":         vUpdate(17, 1) = "0"
        'vUpdate(18, 0) = "NOME PROPRIET�RIO":            vUpdate(18, 1) = "BRANCO"
        vUpdate(18, 0) = "NOME PROPRIETARIO":            vUpdate(18, 1) = "BRANCO"
        vUpdate(19, 0) = "PROPRIEDADE":                  vUpdate(19, 1) = "BRANCO"
        'vUpdate(20, 0) = "DATA FUNDA��O":                vUpdate(20, 1) = "BRANCO"
        vUpdate(20, 0) = "DATA FUNDACAO":                vUpdate(20, 1) = "BRANCO"
        'vUpdate(21, 0) = "INSCRI��O SUFRAMA":            vUpdate(21, 1) = "BRANCO"
        vUpdate(21, 0) = "INSCRICAO SUFRAMA":            vUpdate(21, 1) = "BRANCO"
        'vUpdate(22, 0) = "INSCRI��O ESTADUAL":           vUpdate(22, 1) = "BRANCO"
        vUpdate(22, 0) = "INSCRICAO ESTADUAL":           vUpdate(22, 1) = "BRANCO"
        vUpdate(23, 0) = "TIPO DO DOCTO DE COBRAN�A":    vUpdate(23, 1) = "BRANCO"
        vUpdate(24, 0) = "CGC COBRAN�A":                 vUpdate(24, 1) = "BRANCO"
        'vUpdate(25, 0) = "INSCRI��O ESTADUAL COBRAN�A":  vUpdate(25, 1) = "BRANCO"
        vUpdate(25, 0) = "INSCRICAO ESTADUAL COBRANCA":  vUpdate(25, 1) = "BRANCO"
        vUpdate(26, 0) = "NOME CLIENTE COBRAN�A":        vUpdate(26, 1) = "BRANCO"
        'vUpdate(27, 0) = "C�DIGO CIDADE COBRAN�A":       vUpdate(27, 1) = "0"
        vUpdate(27, 0) = "CODIGO CIDADE COBRANCA":       vUpdate(27, 1) = "0"
        'vUpdate(28, 0) = "ENDERE�O COBRAN�A":            vUpdate(28, 1) = "BRANCO"
        vUpdate(28, 0) = "ENDERECO COBRANCA":            vUpdate(28, 1) = "BRANCO"
        'vUpdate(29, 0) = "BAIRRO COBRAN�A":              vUpdate(29, 1) = "BRANCO"
        vUpdate(29, 0) = "BAIRRO COBRANCA":              vUpdate(29, 1) = "BRANCO"
        'vUpdate(30, 0) = "CEP COBRAN�A":                 vUpdate(30, 1) = "BRANCO"
        vUpdate(30, 0) = "CEP COBRANCA":                 vUpdate(30, 1) = "BRANCO"
        'vUpdate(31, 0) = "C�DIGO MENSAGEM FISCAL":       vUpdate(31, 1) = "BRANCO"
        vUpdate(31, 0) = "CODIGO MENSAGEM FISCAL":       vUpdate(31, 1) = "BRANCO"
        vUpdate(32, 0) = "TP_EMPRESA":                   vUpdate(32, 1) = "BRANCO"
        vUpdate(33, 0) = "CONSUMIDOR FINAL":             vUpdate(33, 1) = "BRANCO"
        
        If vVB_Generica_001.ValidaCGC(txtCGC.Text) Then
            Set oCliente = BuscarClienteSAP(1, txtCGC.Text, msgErro)
        ElseIf vVB_Generica_001.ValidaCIC(txtCGC.Text) Then
            Set oCliente = BuscarClienteSAP(2, txtCGC.Text, msgErro)
        End If
        
        If msgErro <> "" Then
            MsgBox msgErro
            Exit Sub
        ElseIf Val(oCliente.NoClienteSAP) = 0 Then
            MsgBox "Esse cliente n�o est� cadastrado no SAP, o processo ser� abortado", vbCritical, "CAD060 - Aten��o"
            Exit Sub
        Else
            txtSAPAlteracao.Text = oCliente.NoClienteSAP
        End If
        
        For I = 1 To lsvAlterados.ListItems.Count
            If lsvAlterados.ListItems(I).Checked = True Then
            
                If UCase(lsvAlterados.ListItems(I)) = "CONTATO" Then
                    oCliente.ContatoPrincipal.Nome = lsvAlterados.ListItems(I).SubItems(2)
                End If
            
                For II = 0 To UBound(vUpdate)
                    Select Case UCase(lsvAlterados.ListItems(I))
                        Case vUpdate(II, 0)
                             vUpdate(II, 1) = lsvAlterados.ListItems(I).SubItems(2)
 
                             Select Case II
                                                                
                                Case 0
                                    oCliente.Name1 = lsvAlterados.ListItems(I).SubItems(2) 'NOME CLIENTE
                                Case 1
                                    oCliente.CNPJ = lsvAlterados.ListItems(I).SubItems(2) 'CGC
                                Case 2
                                    Call DesconcatenarEnderecoSAP(CStr(lsvAlterados.ListItems(I).SubItems(2)), end_end, end_nro, end_cpl) 'ENDERECO
                                    oCliente.EnderecoPrincipal.rua = end_end
                                    oCliente.EnderecoPrincipal.NoResidencia = end_nro
                                    oCliente.EnderecoPrincipal.complemento = end_cpl
                                Case 3
                                    oCliente.EnderecoPrincipal.Bairro = lsvAlterados.ListItems(I).SubItems(2) 'BAIRRO
                                Case 4
                                    oCliente.EnderecoPrincipal.CEP = lsvAlterados.ListItems(I).SubItems(2) 'CEP
                                Case 5
                                    oCliente.EnderecoPrincipal.Cidade = BuscaCidadePorCodigoCidade(lsvAlterados.ListItems(I).SubItems(2)) 'CODIGO CIDADE
                                    oCliente.EnderecoPrincipal.Regiao = BuscaUfPorCodigoCidade(lsvAlterados.ListItems(I).SubItems(2))
                                Case 6
                                    oCliente.EnderecoPrincipal.DDDFone = lsvAlterados.ListItems(I).SubItems(2) 'DDD 1
                                Case 7
                                    oCliente.EnderecoPrincipal.Telefone = lsvAlterados.ListItems(I).SubItems(2) 'FONE 1
                                Case 8
                                    oCliente.EnderecoPrincipal.DDDFax = lsvAlterados.ListItems(I).SubItems(2) 'DDD 2
                                Case 9
                                    oCliente.EnderecoPrincipal.TelFax = lsvAlterados.ListItems(I).SubItems(2) 'FONE 2
                                Case 10
                                    oCliente.ContatoPrincipal.Nome = lsvAlterados.ListItems(I).SubItems(2) 'NOME CONTATO
                                Case 11
                                    'HOME PAGE
                                Case 12
                                    oCliente.ContatoPrincipal.Email3 = lsvAlterados.ListItems(I).SubItems(2) 'E-MAIL
                                Case 13
                                    oCliente.FISCAL.CodTARE = lsvAlterados.ListItems(I).SubItems(2) 'CODIGO TARE
                                Case 14
                                    'DATA TARE
                                Case 15
                                    'CODIGO TIPO CLIENTE
                                Case 16
                                    'CATEGORIA BOSCH DIESEL
                                Case 17
                                    'CODIGO REPRESENTANTE
                                Case 18
                                    'NOME PROPRIETARIO
                                Case 19
                                    'PROPRIEDADE
                                Case 20
                                    oCliente.DtNascimento = lsvAlterados.ListItems(I).SubItems(2) 'DATA FUNDACAO
                                Case 21
                                    oCliente.NoIDFiscal4 = lsvAlterados.ListItems(I).SubItems(2) 'INSCRICAO SUFRAMA
                                Case 22
                                    oCliente.NoIDFiscal3 = lsvAlterados.ListItems(I).SubItems(2) 'INSCRICAO ESTADUAL
                                Case 23
                                    'TIPO DO DOCTO DE COBRAN�A
                                Case 24
                                    'CGC COBRAN�A
                                Case 25
                                    'INSCRICAO ESTADUAL COBRANCA
                                Case 26
                                    'NOME CLIENTE COBRAN�A
                                Case 27
                                    'Eduardo Faria de Oliveira - CIT - ISA-353 - 23/08/12 - altera o objeto cliente para gravar no SAP
                                    If (oCliente.EnderecoCobranca Is Nothing) Then
                                       oCliente.EnderecoCobranca = vEnderecoCobranca
                                    End If
                                    oCliente.EnderecoCobranca.Cidade = BuscaCidadePorCodigoCidade(lsvAlterados.ListItems(I).SubItems(2)) 'CODIGO CIDADE COBRANCA
                                    oCliente.EnderecoCobranca.Regiao = BuscaUfPorCodigoCidade(lsvAlterados.ListItems(I).SubItems(2))
                                Case 28
                                    If (oCliente.EnderecoCobranca Is Nothing) Then
                                       oCliente.EnderecoCobranca = vEnderecoCobranca
                                    End If
                                    Call DesconcatenarEnderecoSAP(CStr(lsvAlterados.ListItems(I).SubItems(2)), end_end, end_nro, end_cpl) 'ENDERECO COBRANCA
                                    oCliente.EnderecoCobranca.rua = end_end
                                    oCliente.EnderecoCobranca.NoResidencia = end_nro
                                    oCliente.EnderecoCobranca.complemento = end_cpl
                                Case 29
                                    If (oCliente.EnderecoCobranca Is Nothing) Then
                                       oCliente.EnderecoCobranca = vEnderecoCobranca
                                    End If
                                    oCliente.EnderecoCobranca.Bairro = lsvAlterados.ListItems(I).SubItems(2) 'BAIRRO COBRANCA
                                Case 30
                                    If (oCliente.EnderecoCobranca Is Nothing) Then
                                       oCliente.EnderecoCobranca = vEnderecoCobranca
                                    End If
                                    oCliente.EnderecoCobranca.CEP = lsvAlterados.ListItems(I).SubItems(2) 'CEP COBRANCA
                                Case 31
                                    oCliente.FISCAL.MsgFiscal1 = lsvAlterados.ListItems(I).SubItems(2) 'CODIGO MENSAGEM FISCAL
                                Case 32
                                    oCliente.FISCAL.TpEmpresa = lsvAlterados.ListItems(I).SubItems(2) 'TP_EMPRESA
                                Case 33
                                    'CONSUMIDOR FINAL
                                    
                            End Select
 Exit For
                    End Select
                Next II
            End If
        Next I
        
        If oCliente.ContatoPrincipal.Nome = "" Then
          oCliente.ContatoPrincipal.Nome = Mid(oCliente.Name1, 1, 15)
        End If
        
        'ecampos 08/11/2012 [ISA-966] - Consulta no BD do legado para preencher os campos Setor e CatgCFOP
        db.Parameters.Remove "P_CGC"
        db.Parameters.Add "P_CGC", txtCGC.Text, 1
        Criar_Cursor db.Parameters, "P_CURSOR"
        
        vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD060.PR_SEL_INFOCLIENTE(:P_CGC, :P_CURSOR)")
        If vErro <> "" Then
            Exit Sub
        End If
        Set dadosCliente = db.Parameters("P_CURSOR").Value
        
        oCliente.Setor = RetornarSetorIndustrial(IIf(IsNull(dadosCliente!Tp_Docto), "", dadosCliente!Tp_Docto), txtUF.Text, IIf(IsNull(dadosCliente!cod_cnae), "", dadosCliente!cod_cnae), oCliente.CNPJ, IIf(IsNull(oCliente.FISCAL.CodTARE), "", oCliente.FISCAL.CodTARE), IIf(IsNull(oCliente.NoIDFiscal3), "", oCliente.NoIDFiscal3), IIf(IsNull(dadosCliente!COD_SEGMENTO), "", dadosCliente!COD_SEGMENTO))
        oCliente.FISCAL.CatgCFOP = RetornarCategoriaCFO(IIf(IsNull(dadosCliente!Tp_Docto), "", dadosCliente!Tp_Docto), IIf(IsNull(dadosCliente!cod_cnae), "", dadosCliente!cod_cnae), oCliente.CNPJ)
        
        'TI-6488
        If oCliente.EnderecoPrincipal.Telefone = "" Then
          oCliente.EnderecoPrincipal.Telefone = "0"
        End If
        If vUpdate(12, 1) <> "BRANCO" And (ValidarEmail(vUpdate(12, 1)) = True) Then
            Call PopularObjetoClienteEmails(oCliente, vUpdate(12, 1), Val(lsvGeral.ListItems(1).SubItems(1)))
        End If
        'FIM TI-6488
        
        'Eduardo Faria de Oliveira - CIT - ISA-353 - 23/08/12 - Tenta enviar cliente para o SAP. Se POSITIVO, faz as funcionalidades do legado
        If Not EnviarClienteSAP(oCliente) Then
            MsgBox "Erro ao gravar o cliente no SAP. Cadastro Abortado.", vbCritical, "CAD060 - Aten��o"
            Exit Sub
        End If
        
        '********************************************************************
        ' Se funcionou o envio do cliente pro SAP ent�o persistimos no legado
        '********************************************************************
        
            'Criar parametros para efetuar UPDATE
            For I = 0 To UBound(vUpdate())
                vUpdate(I, 0) = Replace(vUpdate(I, 0), "-", "")
                db.Parameters.Remove Replace(vUpdate(I, 0), " ", "_")
                db.Parameters.Add Replace(vUpdate(I, 0), " ", "_"), vUpdate(I, 1), 1
            Next I
            
            'Adicionar o Parametro com o Codigo do Cliente
            db.Parameters.Remove "CodCli"
            db.Parameters.Add "CodCli", lsvGeral.ListItems(1).SubItems(1), 1
            
            'Adicionar os Parametros de Erro
            db.Parameters.Remove "CodErro"
            db.Parameters.Add "CodErro", 0, 2
            
            db.Parameters.Remove "TxtErro"
            db.Parameters.Add "TxtErro", "", 2
            
            
            '-------------------------------------------------------------------------------------------------------------
            'Valida��o do Tare - Alethea - 01/02/07
            'vCodTareAlt = db.Parameters("C�DIGO_TARE")
            vCodTareAlt = db.Parameters("CODIGO_TARE")
            vDtTareAlt = db.Parameters("DATA_TARE")
            If (vCodTareAlt <> "0" And vCodTareAlt <> "BRANCO") Or (Trim(vDtTareAlt) <> "" And vDtTareAlt <> "BRANCO") Then
                If Trim(vCodTareAlt) = "BRANCO" Or Trim(vDtTareAlt) = "BRANCO" Then
                    MsgBox "Altera��o n�o poder� ser aceita. Campos de codigo e data TARE devem ser preenchidos!", vbCritical, "CAD060 - Aten��o"
                    Exit Sub
                End If
            End If
            '-------------------------------------------------------------------------------------------------------------
            
            'Executar a Procedure para Update
            vSql = "BEGIN PRODUCAO.PCK_CLIENTE.Pr_Update_Alteracoes (" & _
                          ":CodCli, " & _
                          ":NOME_CLIENTE, " & _
                          ":CGC, " & _
                          ":ENDERECO, " & _
                          ":BAIRRO, " & _
                          ":CEP, " & _
                          ":CODIGO_CIDADE, " & _
                          ":DDD_1, " & _
                          ":FONE_1, " & _
                          ":DDD_2, " & _
                          ":FONE_2, " & _
                          ":NOME_CONTATO, " & _
                          ":HOME_PAGE, " & _
                          ":EMAIL, " & _
                          ":CODIGO_TARE, " & _
                          ":DATA_TARE, " & _
                          ":CODIGO_TIPO_CLIENTE, " & _
                          ":CATEGORIA_BOSCH_DIESEL, " & _
                          ":CODIGO_REPRESENTANTE, "
            
            'vSql = "BEGIN PRODUCAO.PCK_CLIENTE.Pr_Update_Alteracoes (" & _
                          ":CodCli, " & _
                          ":NOME_CLIENTE, " & _
                          ":CGC, " & _
                          ":ENDERE�O, " & _
                          ":BAIRRO, " & _
                          ":CEP, " & _
                          ":C�DIGO_CIDADE, " & _
                          ":DDD_1, " & _
                          ":FONE_1, " & _
                          ":DDD_2, " & _
                          ":FONE_2, " & _
                          ":NOME_CONTATO, " & _
                          ":HOME_PAGE, " & _
                          ":EMAIL, " & _
                          ":C�DIGO_TARE, " & _
                          ":DATA_TARE, " & _
                          ":C�DIGO_TIPO_CLIENTE, " & _
                          ":CATEGORIA_BOSCH_DIESEL, " & _
                          ":C�DIGO_REPRESENTANTE, "
            
            'vSql = vSql & _
                          ":NOME_PROPRIET�RIO, " & _
                          ":PROPRIEDADE, " & _
                          ":DATA_FUNDA��O, " & _
                          ":INSCRI��O_SUFRAMA, " & _
                          ":INSCRI��O_ESTADUAL, " & _
                          ":TIPO_DO_DOCTO_DE_COBRAN�A, " & _
                          ":CGC_COBRAN�A, " & _
                          ":INSCRI��O_ESTADUAL_COBRAN�A, " & _
                          ":NOME_CLIENTE_COBRAN�A, " & _
                          ":C�DIGO_CIDADE_COBRAN�A, " & _
                          ":ENDERE�O_COBRAN�A, " & _
                          ":BAIRRO_COBRAN�A, " & _
                          ":CEP_COBRAN�A, " & _
                          ":C�DIGO_MENSAGEM_FISCAL, " & _
                          ":TP_EMPRESA, " & _
                          ":CONSUMIDOR_FINAL, " & _
                          ":CodErro, " & _
                          ":TxtErro); END;"
            
            
            vSql = vSql & _
                          ":NOME_PROPRIETARIO, " & _
                          ":PROPRIEDADE, " & _
                          ":DATA_FUNDACAO, " & _
                          ":INSCRICAO_SUFRAMA, " & _
                          ":INSCRICAO_ESTADUAL, " & _
                          ":TIPO_DO_DOCTO_DE_COBRAN�A, " & _
                          ":CGC_COBRAN�A, " & _
                          ":INSCRICAO_ESTADUAL_COBRANCA, " & _
                          ":NOME_CLIENTE_COBRAN�A, " & _
                          ":CODIGO_CIDADE_COBRANCA, " & _
                          ":ENDERECO_COBRANCA, " & _
                          ":BAIRRO_COBRANCA, " & _
                          ":CEP_COBRANCA, " & _
                          ":CODIGO_MENSAGEM_FISCAL, " & _
                          ":TP_EMPRESA, " & _
                          ":CONSUMIDOR_FINAL, " & _
                          ":CodErro, " & _
                          ":TxtErro); END;"
            
            db.Executesql vSql
            
            'Remover os parametros
            For I = 0 To UBound(vUpdate())
                db.Parameters.Remove Replace(vUpdate(I, 0), " ", "_")
            Next I
            
            If Val(IIf(IsNull(db.Parameters("CodErro")), 0, db.Parameters("CodErro"))) <> 0 Then
               MsgBox "C�digo do Erro: " & db.Parameters("CodErro") & vbCrLf & "Descri��o: " & db.Parameters("TxtErro")
               Exit Sub
            End If
            
            '**************************************************************
            'Se o Codigo do Representante da Producao.Cliente for Diferente
            'do Codigo do Representante da Intranet.Cliente_web
            'Enviar Email para o SPC
            '**************************************************************
EnviarEmail:
        
            For I = 1 To lsvAlterados.ListItems.Count
                'If UCase(lsvAlterados.ListItems(I)) = UCase("C�digo Representante") Then
                If UCase(lsvAlterados.ListItems(I)) = UCase("Codigo Representante") Then
                    If lsvAlterados.ListItems(I).SubItems(1) <> lsvAlterados.ListItems(I).SubItems(2) Then
                       Dim Texto As String
                       'Texto = "Ocorreu a alteracao do Representante." & vbCrLf & vbCrLf & _
                               "Data da Alteracao   : " & Day(Date) & "/" & Month(Date) & "/" & Year(Date) & " as " & FormatDateTime(Time, 4) & vbCrLf & vbCrLf & "Nao responder esta mensagem." & vbCrLf & vbCrLf & _
                               "Codigo Atual        : " & lsvAlterados.ListItems(i).SubItems(1) & vbCrLf & _
                               "Codigo Novo         : " & lsvAlterados.ListItems(i).SubItems(2) & vbCrLf & _
                               "Codigo do Cliente   : " & lsvGeral.ListItems(1).SubItems(1) & vbCrLf & _
                               "CGC do Cliente      : " & lsvGeral.ListItems(2).SubItems(1)
                       Texto = "Solicita��o de Alteracao de Representante feita por " & vUserRem_Analisado & vbCrLf & vbCrLf & _
                               "Data da Solicita��o : " & Day(Date) & "/" & Month(Date) & "/" & Year(Date) & " as " & FormatDateTime(Time, 4) & vbCrLf & vbCrLf & "Nao responder esta mensagem." & vbCrLf & vbCrLf & _
                               "Codigo Atual        : " & lsvAlterados.ListItems(I).SubItems(1) & vbCrLf & _
                               "Codigo Novo         : " & lsvAlterados.ListItems(I).SubItems(2) & vbCrLf & _
                               "Codigo do Cliente   : " & txtCodCliente & vbCrLf & _
                               "CGC do Cliente      : " & vCGC_Analisado
                    End If
                    
                    vVB_Generica_001.EnviarEmail db_INT, "spc@dpk.com.br", "Alteracao do Representante", Texto
                    'vVB_Generica_001.EnviarEmail db_INT, "alethea@maxxipel.com.br", "Alteracao do Representante", Texto
                    
                    Exit For
                
                End If
             Next

            Call pGravaControleWeb            'Gravar controle de cliente na web, quando pr�-altera��o aceita
        
        
            'Enviar e-mail ao usu�rio solicitante, retornando situa��o de sua solicita��o
EnviarEmailConf:
    
            Dim vAuxTexto As String
            'Seleciona C�d.Usu�rio da Controle_Cliente_Web e seu endereco de e-mail
            Set OraParameters = db_INT.Parameters
            OraParameters.Remove "CGC": OraParameters.Add "CGC", Val(vCGC_Analisado), 1
            OraParameters.Remove "TP_SOL": OraParameters.Add "TP_SOL", vFl_Tipo_Solicitacao, 1
            OraParameters.Remove "vErro": OraParameters.Add "vErro", 0, 2
            OraParameters.Remove "vMsgErro": OraParameters.Add "vMsgErro", "", 2
            
            Criar_Cursor OraParameters, "PM_CURSOR1"
            
            vErro = vVB_Generica_001.ExecutaPl(db_INT, "Intranet.PCK_CLIENTES_TESTE.Pr_Sel_UserIntranet(:PM_CURSOR1, :CGC, :TP_SOL, :vErro, :vMsgErro)")
            
            If vErro = "" Then
                Set db_CONS = OraParameters("PM_CURSOR1").Value
            Else
                Exit Sub
            End If
            
            If vFl_Tipo_Solicitacao = 0 Then
                vAuxTexto = "Pr�-Cadastro"
            ElseIf vFl_Tipo_Solicitacao = 1 Then
                vAuxTexto = "Pr�-Altera��o"
            ElseIf vFl_Tipo_Solicitacao = 2 Then
                vAuxTexto = "Pr�-Recadastro"
            End If
            
            Texto = "Solicitacao de " & vAuxTexto & " para o CGC " & vCGC_Analisado & " foi aceita." & vbCrLf & vbCrLf & _
                    "Data da Solicitacao   : " & Format(db_CONS!Dt_Solicitacao, "DD/MM/YY HH:MM:SS") & vbCrLf & vbCrLf & _
                    "Data da Analise       : " & Format(db_CONS!Dt_Analise, "DD/MM/YY HH:MM:SS") & vbCrLf & vbCrLf & _
                    "Favor nao responder esta mensagem." & vbCrLf & vbCrLf
            
            'PARA ALTERACAO DE FICHA ENVIADA PELO DPKNET, N�O PRECISA ENVIAR E-MAIL DE CONFIRMA��O DE ACEITE - ALETHEA - 06/09/07
            'If db_CONS!COD_USUARIO <> 1743 Then        'DESENV
            If db_CONS!cod_usuario <> 1905 Then
                vVB_Generica_001.EnviarEmail db_INT, db_CONS!E_Mail, vAuxTexto & " ACEITO!!! - CGC " & vCGC_Analisado, Texto
            End If
            MsgBox "Altera��o OK!", vbInformation, "CAD060 - Cadastro de Cliente"
    End If
    
    Texto = "Solicitacao de " & vAuxTexto & " para o CGC " & vCGC_Analisado & " foi aceita." & vbCrLf & vbCrLf & _
            "Data da Solicitacao   : " & Format(db_CONS!Dt_Solicitacao, "DD/MM/YY HH:MM:SS") & vbCrLf & vbCrLf & _
            "Data da Analise       : " & Format(db_CONS!Dt_Analise, "DD/MM/YY HH:MM:SS") & vbCrLf & vbCrLf & _
            "Favor nao responder esta mensagem." & vbCrLf & vbCrLf
    
    'PARA ALTERACAO DE FICHA ENVIADA PELO DPKNET, N�O PRECISA ENVIAR E-MAIL DE CONFIRMA��O DE ACEITE - ALETHEA - 06/09/07
    'If db_CONS!COD_USUARIO <> 1743 Then        'DESENV
    If db_CONS!cod_usuario <> 1905 Then
        vVB_Generica_001.EnviarEmail db_INT, db_CONS!E_Mail, vAuxTexto & " ACEITO!!! - CGC " & vCGC_Analisado, Texto
    End If

    MsgBox "Altera��o OK!", vbInformation, "CAD060 - Cadastro de Cliente"
    Call cmdSair_Click

End Sub

Private Sub cmdRecusar_Click()
    'Alterar situa��o, data de an�lise e cod_usuario_credito na Controle_Cliente_Web
    'Deletar da tabela Cliente_Web
    frmMotivoRecusa.Show vbModal
    If vFechar = True Then
        Call cmdSair_Click
    End If
End Sub

Private Sub cmdSair_Click()
    vFl_Volta_Analise = True
    Unload Me
End Sub

Private Sub Form_Load()
    
    Dim litem As ListItem
    Dim rst2 As Object
    Dim II As Integer
    Dim Vlr_Campo_Prod As String
    Dim Vlr_Campo_Web As String
    Dim end_end As String
    Dim end_nro As String
    Dim end_cpl As String
    
    'Inicializa vari�veis, para saber se existir�o informa��es a serem alteradas/gravadas
    vFl_Alt_Dados = 0
    
    Me.Top = mdiCAD060.Menu.Height * 2
    Me.Left = (mdiCAD060.Width - Me.Width) / 2
    
    db_INT.Parameters.Remove "CGC": db_INT.Parameters.Add "CGC", Val(vCGC_Analisado), 1
    db_INT.Parameters.Remove "TPSOL": db_INT.Parameters.Add "TPSOL", Val(1), 1
    db.Parameters.Remove "CGC": db.Parameters.Add "CGC", Val(vCGC_Analisado), 1

    Criar_Cursor db.Parameters, "PM_CURSOR1"
    'Mudar de pck_cad0602 para pck_cad060
    'vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD0602.PR_SELECT_CLIENTE_VB(:PM_CURSOR1, :CGC)")
    vErro = vVB_Generica_001.ExecutaPl(db, "PRODUCAO.PCK_CAD060.PR_SELECT_CLIENTE_VB(:PM_CURSOR1, :CGC)")
    If vErro <> "" Then
        Exit Sub
    End If
    Set rst = db.Parameters("PM_CURSOR1").Value

    Criar_Cursor db_INT.Parameters, "cAlterados"
    vErro = vVB_Generica_001.ExecutaPl(db_INT, "INTRANET.PCK_CLIENTES_TESTE.PR_SELECT_CLIENTE_VB_ALTERADOS(:cAlterados, :CGC, :TPSOL)")
    If vErro <> "" Then
        Exit Sub
    End If
    Set rst2 = db_INT.Parameters("cAlterados").Value
    
    'Pegar o codigo do cliente
    txtCodCliente = rst("Codigo do Cliente")
    txtCGC = vCGC_Analisado
    vCodCli_Analisado = txtCodCliente
    txtNomeCli = vNome_Analisado
    txtDtRecadastro = IIf(IsNull(rst("Dt_Recadastro")), "", rst("Dt_Recadastro"))
    txtCidade = IIf(IsNull(rst("Nome Cidade")), "", rst("Nome Cidade"))
    txtUF = IIf(IsNull(rst("Codigo UF")), "", rst("Codigo UF"))
    txtCep = IIf(IsNull(rst("Cep")), "", rst("Cep"))
    
    ' fpallini - CiT - 02/04/13 - Carregar endereco de cobranca na memoria
    Set vEnderecoCobranca = New endereco
    vEnderecoCobranca.Cidade = IIf(IsNull(rst("Codigo Cidade Cobranca")), "", BuscaCidadePorCodigoCidade(rst("Codigo Cidade Cobranca")))
    vEnderecoCobranca.Regiao = IIf(IsNull(rst("Codigo Cidade Cobranca")), "", BuscaUfPorCodigoCidade(rst("Codigo Cidade Cobranca")))
    If Not IsNull(rst("Endereco Cobranca")) Then
        Call DesconcatenarEnderecoSAP(CStr(rst("Endereco Cobranca")), end_end, end_nro, end_cpl)
        vEnderecoCobranca.rua = end_end
        vEnderecoCobranca.NoResidencia = end_nro
        vEnderecoCobranca.complemento = end_cpl
    End If
    vEnderecoCobranca.Bairro = IIf(IsNull(rst("Bairro Cobranca")), "", rst("Bairro Cobranca"))
    vEnderecoCobranca.CEP = IIf(IsNull(rst("Cep Cobranca")), "", rst("Cep Cobranca"))


    'Alterado por Eduardo Relvas - 25/05/2007

    'Preencher o List View com os campos que sofreram altera��es
    For I = 0 To rst2.Fields.Count - 1
        If UCase(rst2.Fields(I).Name) <> "CGC" Then
            For II = 0 To rst.Fields.Count - 1
                'Debug.Print UCase(rst2.Fields(I).Name) & " - " & UCase(rst.Fields(ii).Name)
                If UCase(rst2.Fields(I).Name) = UCase(fReplace(rst.Fields(II).Name)) Then

                    'If rst2.fields(I).Name = "C�digo Mensagem Fiscal" Then
                    '    MsgBox ""
                    'End If

                    'Debug.Print rst2.Fields(I).Name & "." & rst2.Fields(I) & " / " & rst.Fields(ii).Name & "." & rst.Fields(ii).Value
                    Vlr_Campo_Prod = UCase(IIf(IsNull(rst.Fields(II).Value), "", rst.Fields(II).Value))
                    Vlr_Campo_Web = UCase(IIf(IsNull(rst2.Fields(I).Value), "", rst2.Fields(I).Value))
                    If Vlr_Campo_Web <> Vlr_Campo_Prod Or rst.Fields(II).Name = "E-Mail" Then
                       If Vlr_Campo_Web <> "" And (Vlr_Campo_Prod <> "0" Or Trim(Vlr_Campo_Prod) <> "") Then
                           Set litem = lsvAlterados.ListItems.Add
                           litem = rst.Fields(II).Name
                           litem.SubItems(1) = IIf(IsNull(rst.Fields(II).Value), "", rst.Fields(II).Value)
                           litem.SubItems(2) = IIf(IsNull(rst2.Fields(I).Value), "", rst2.Fields(I).Value)
                           litem.Checked = True
                            vFl_Alt_Dados = 1
                        End If
                        Exit For
                    Else
                        Exit For
                    End If
                End If
            Next
        End If
    Next
    
    'Preencher o List View com todos os campos da Producao com os valores atuais
    For I = 0 To rst.Fields.Count - 1
        Set litem = lsvGeral.ListItems.Add
        litem = UCase(rst.Fields(I).Name)
        litem.SubItems(1) = IIf(IsNull(rst.Fields(I).Value), "", rst.Fields(I).Value)
    Next
    
    'Eduardo Faria de Oliveira - CIT - ISA-353 - 23/08/12 - Atribuindo o codigo SAP no campo correspondente
    txtSAPAlteracao.Text = ObterCodSAP(txtCGC.Text)
End Sub

Function fReplace(pCampo As String) As String
    Dim vCampo As String
    vCampo = UCase(pCampo)
    vCampo = Replace(vCampo, "�", "A")
    vCampo = Replace(vCampo, "�", "O")
    vCampo = Replace(vCampo, "�", "C")
    vCampo = Replace(vCampo, "�", "E")
    fReplace = vCampo
End Function
