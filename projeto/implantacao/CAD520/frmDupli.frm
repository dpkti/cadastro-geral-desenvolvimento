VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form frmDuplica 
   Caption         =   "Duplica��o de Dados"
   ClientHeight    =   3975
   ClientLeft      =   1515
   ClientTop       =   1980
   ClientWidth     =   6690
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   3975
   ScaleWidth      =   6690
   Begin VB.TextBox txtCodForn 
      ForeColor       =   &H00C00000&
      Height          =   300
      Left            =   2325
      TabIndex        =   7
      Top             =   1740
      Width           =   765
   End
   Begin VB.ComboBox cmbDestino 
      ForeColor       =   &H00C00000&
      Height          =   315
      Left            =   2790
      TabIndex        =   5
      Top             =   1140
      Width           =   2565
   End
   Begin VB.ComboBox cmbOrigem 
      ForeColor       =   &H00C00000&
      Height          =   315
      Left            =   2790
      TabIndex        =   4
      Top             =   525
      Width           =   2565
   End
   Begin Threed.SSCommand SSConfirmaDupli 
      Height          =   495
      Left            =   4125
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   2820
      Width           =   495
      _Version        =   65536
      _ExtentX        =   873
      _ExtentY        =   873
      _StockProps     =   78
      Picture         =   "frmDupli.frx":0000
   End
   Begin Threed.SSCommand cmdSair 
      Height          =   510
      Left            =   4875
      TabIndex        =   0
      Top             =   2805
      Width           =   510
      _Version        =   65536
      _ExtentX        =   900
      _ExtentY        =   900
      _StockProps     =   78
      ForeColor       =   -2147483640
      Picture         =   "frmDupli.frx":031A
   End
   Begin VB.Label lblFornec 
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00C00000&
      Height          =   315
      Left            =   3165
      TabIndex        =   8
      Top             =   1740
      Width           =   2190
   End
   Begin VB.Label Label1 
      Caption         =   "Fornecedor"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1140
      TabIndex        =   6
      Top             =   1740
      Width           =   1065
   End
   Begin VB.Label lblDestino 
      Caption         =   "Dep�sito Destino"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1140
      TabIndex        =   3
      Top             =   1140
      Width           =   1545
   End
   Begin VB.Label lblOrigem 
      Caption         =   "Dep�sito Origem"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1140
      TabIndex        =   2
      Top             =   525
      Width           =   1530
   End
End
Attribute VB_Name = "frmDuplica"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit



Private Sub cmdSair_Click()

    Unload Me
    
End Sub


Private Sub Form_Load()

    CmdSql = "select a.COD_LOJA, a.NOME_FANTASIA " & _
             "from LOJA a, DEPOSITO_VISAO b " & _
             "where b.NOME_PROGRAMA = 'CAD520' and " & _
             "a.COD_LOJA = b.COD_LOJA " & _
             "order by COD_LOJA"

    Set db_CONS = db.createdynaset(CmdSql, 0&)

    If db_CONS.EOF = True Then
        MsgBox "N�o Existe Dep�sito Cadastrado Avisar Analista Respons�vel", vbCritical, Msg_Padrao
        End
    End If

    While db_CONS.EOF = False ' carrega todos os depositos existentes
        cmbOrigem.AddItem Format(db_CONS!COD_LOJA, "00") & " - " & db_CONS!NOME_FANTASIA
        cmbDestino.AddItem Format(db_CONS!COD_LOJA, "00") & " - " & db_CONS!NOME_FANTASIA
        db_CONS.MoveNext
    Wend
    cmbOrigem.Text = "01 - CAMPINAS"
    cmbDestino.Text = "01 - CAMPINAS"

End Sub

Private Sub SSConfirmaDupli_Click()

    Dim cod_errora As Long
    Dim txt_errora As String
    Dim strSQL As String
    
    If cmbOrigem.Text = cmbDestino Then
        MsgBox "Dep�sito Destino deve ser diferente do Origem", vbCritical, Msg_Padrao
        cmbDestino.SetFocus
        Exit Sub
    End If
    
    If Trim(txtCodForn) = "" Or Trim(lblFornec) = "" Then
        MsgBox "Digite o C�digo do Fornecedor", vbCritical, Msg_Padrao
        Exit Sub
    End If
        
    If MsgBox("Confirma Duplica��o?", vbQuestion + vbYesNo + vbDefaultButton2, Msg_Padrao) = vbNo Then
        Exit Sub
    End If

    db.Parameters.Remove "ORIGEM": db.Parameters.Add "ORIGEM", Trim(Mid(cmbOrigem.Text, 1, 2)), 1
    db.Parameters.Remove "DESTINO": db.Parameters.Add "DESTINO", Trim(Mid(cmbDestino.Text, 1, 2)), 1
    db.Parameters.Remove "CODFORN": db.Parameters.Add "CODFORN", Val(txtCodForn), 1
    db.Parameters.Remove "cod_errora": db.Parameters.Add "cod_errora", 0, 2
    db.Parameters.Remove "txt_errora": db.Parameters.Add "txt_errora", 0, 2
    

'Fiz nova procedure (utilizando a mesma id�ia de PR_DUPLICA_COMPRAS_FATOR mas agora consistindo
'o c�digo do fornecedor) e dei a ela o nome de PR_DUPL_FATOR_FORN - Aleth�a 28/03/2002

'    CmdSql = "BEGIN PRODUCAO.PR_DUPLICA_COMPRAS_FATOR(:ORIGEM, :DESTINO);"
    
    CmdSql = "BEGIN PRODUCAO.PR_DUPL_FATOR_FORN(:ORIGEM, :DESTINO, :CODFORN); "
    CmdSql = CmdSql & " COMMIT;"
    CmdSql = CmdSql & " EXCEPTION WHEN OTHERS THEN ROLLBACK;"
    CmdSql = CmdSql & " :cod_errora := SQLCODE;"
    CmdSql = CmdSql & " :txt_errora := SQLERRM;"
    CmdSql = CmdSql & " END; "

    db.ExecuteSQL (CmdSql)

    If IIf(IsNull(db.Parameters("Cod_Errora")), 0, Val(db.Parameters("Cod_Errora"))) <> 0 Then
        cod_errora = db.Parameters("cod_errora").Value
        txt_errora = db.Parameters("txt_errora").Value
        strSQL = "C�digo: " & cod_errora & " - " & txt_errora
        MsgBox strSQL, vbOK, "ERRO NA DUPLICA��O!"
        Exit Sub
    
    End If
    
    
    MsgBox "Duplica��o OK!", vbInformation, Msg_Padrao

    db.Parameters.Remove "ORIGEM"
    db.Parameters.Remove "DESTINO"
    db.Parameters.Remove "CODFORN"
    db.Parameters.Remove "cod_errora"
    db.Parameters.Remove "txt_errora"

End Sub


Private Sub txtCodForn_KeyPress(KeyAscii As Integer)
    KeyAscii = Numerico(KeyAscii)
End Sub


Private Sub txtCodForn_LostFocus()

    lblFornec = ""
    
    db.Parameters.Remove "CODFORN": db.Parameters.Add "CODFORN", Val(txtCodForn), 1
    
    CmdSql = "Select sigla, situacao " & _
             "From FORNECEDOR " & _
             "Where cod_fornecedor = :CODFORN "
    
    Set db_CONS = db.createdynaset(CmdSql, 0&)

    If db_CONS.EOF = True Then
        MsgBox "Fornecedor N�o Cadastrado!", vbCritical, Msg_Padrao
        lblFornec = ""
        Exit Sub
    Else
        If db_CONS!situacao = 9 Or db_CONS!situacao = 9 Then
            MsgBox "Fornecedor Desativado ou Substituido", vbInformation, Msg_Padrao
            lblFornec = ""
            Exit Sub
        End If
    End If

    lblFornec = db_CONS!sigla

End Sub


