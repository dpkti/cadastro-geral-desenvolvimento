VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.MDIForm mdicad520 
   BackColor       =   &H8000000C&
   Caption         =   "Cadastro de Fator e Tipo de Fator"
   ClientHeight    =   4140
   ClientLeft      =   3780
   ClientTop       =   2535
   ClientWidth     =   6690
   LinkTopic       =   "MDIForm1"
   Begin Threed.SSPanel SSPanel1 
      Align           =   1  'Align Top
      Height          =   630
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6690
      _Version        =   65536
      _ExtentX        =   11800
      _ExtentY        =   1111
      _StockProps     =   15
      Caption         =   " "
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin Threed.SSCommand cmdDuplica 
         Height          =   495
         Left            =   900
         TabIndex        =   5
         Top             =   60
         Width           =   495
         _Version        =   65536
         _ExtentX        =   873
         _ExtentY        =   873
         _StockProps     =   78
         Picture         =   "mdiforn.frx":0000
      End
      Begin Threed.SSCommand cmdFatura 
         Height          =   510
         Left            =   100
         TabIndex        =   1
         Top             =   60
         Width           =   510
         _Version        =   65536
         _ExtentX        =   900
         _ExtentY        =   900
         _StockProps     =   78
         Picture         =   "mdiforn.frx":031A
      End
      Begin Threed.SSCommand cmdSair 
         Height          =   510
         Left            =   2500
         TabIndex        =   3
         Top             =   60
         Width           =   510
         _Version        =   65536
         _ExtentX        =   900
         _ExtentY        =   900
         _StockProps     =   78
         Picture         =   "mdiforn.frx":0634
      End
      Begin Threed.SSCommand cmdSobre 
         Height          =   495
         Left            =   1700
         TabIndex        =   2
         Top             =   60
         Width           =   495
         _Version        =   65536
         _ExtentX        =   873
         _ExtentY        =   873
         _StockProps     =   78
         Picture         =   "mdiforn.frx":094E
      End
   End
   Begin Threed.SSPanel SSRodape 
      Align           =   2  'Align Bottom
      Height          =   450
      Left            =   0
      TabIndex        =   4
      Top             =   3690
      Width           =   6690
      _Version        =   65536
      _ExtentX        =   11800
      _ExtentY        =   794
      _StockProps     =   15
      ForeColor       =   255
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Menu munCadastro 
      Caption         =   "&Cadastro "
   End
   Begin VB.Menu mnuDuplica 
      Caption         =   "&Duplica��o"
      Index           =   2
   End
   Begin VB.Menu mnuSobre 
      Caption         =   "&Sobre"
   End
   Begin VB.Menu mnuSair 
      Caption         =   "&Sair"
   End
End
Attribute VB_Name = "mdicad520"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdDuplica_Click()

    frmDuplica.Show
    
End Sub


Private Sub cmdFatura_Click()

    Screen.MousePointer = 11
    frmCadastro.Show 1
    
End Sub


Private Sub cmdSair_Click()

    End

End Sub

Private Sub cmdSobre_Click()

    Screen.MousePointer = 11
    FrmSobre.Show 1
    
End Sub


Private Sub MDIForm_Load()
   
    dlgLogin.Show 1
   
    Msg_Padrao = "Cadastro de Fator e Tipo de Fator"
    WindowState = 2
 
    If App.PrevInstance Then
        MsgBox "J� EXISTE UMA INST�NCIA ABERTA", vbCritical, Msg_Padrao
        End
    End If

    'ABERTURA DO ORACLE
    Set dbOra = CreateObject("oracleinprocserver.xorasession")
    'Set db = dbOra.OpenDatabase("SDPKT", "PRODUCAO/DPK", 0&)
    Set db = dbOra.OpenDatabase("PRODUCAO", "CAD520/PROD", 0&)

    Set db_CONS = db.dbCreateDynaset("SELECT TO_CHAR(DT_REAL,'DD/MM/YY') DATA_REAL FROM DATAS", 8&)
    Dt_Entrada = db_CONS!datA_real
    
End Sub


Private Sub mnuDuplica_Click(Index As Integer)

    frmDuplica.Show

End Sub

Private Sub mnuSair_Click()

    End

End Sub


Private Sub mnuSobre_Click()
    
    Screen.MousePointer = 11
    FrmSobre.Show 1

End Sub


Private Sub munCadastro_Click()

    Screen.MousePointer = 11
    frmCadastro.Show 1

End Sub


Private Sub SSCommand1_Click()

End Sub


