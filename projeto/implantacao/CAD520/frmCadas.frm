VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form frmCadastro 
   Caption         =   "Cadastro de Fator e Tipo de Fator"
   ClientHeight    =   7830
   ClientLeft      =   1545
   ClientTop       =   1710
   ClientWidth     =   9540
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7830
   ScaleWidth      =   9540
   Begin Threed.SSFrame fraTipoFator 
      Height          =   1020
      Left            =   90
      TabIndex        =   0
      Top             =   150
      Width           =   9405
      _Version        =   65536
      _ExtentX        =   16589
      _ExtentY        =   1799
      _StockProps     =   14
      Caption         =   "TIPO DE FATOR"
      ForeColor       =   192
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin VB.TextBox txtdescrFator 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   285
         Left            =   2985
         MaxLength       =   15
         TabIndex        =   4
         Text            =   "AAAAAAAAAAAAAAA"
         Top             =   450
         Width           =   2730
      End
      Begin VB.TextBox txtTipoFator 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   285
         Left            =   1020
         MaxLength       =   2
         TabIndex        =   2
         Text            =   "12"
         Top             =   450
         Width           =   435
      End
      Begin Threed.SSPanel SSPanel2 
         Height          =   825
         Left            =   7710
         TabIndex        =   5
         Top             =   150
         Width           =   1650
         _Version        =   65536
         _ExtentX        =   2910
         _ExtentY        =   1455
         _StockProps     =   15
         BackColor       =   12632256
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Begin Threed.SSCommand SSConfirmaDupli 
            Height          =   495
            Left            =   120
            TabIndex        =   6
            TabStop         =   0   'False
            Top             =   60
            Width           =   495
            _Version        =   65536
            _ExtentX        =   873
            _ExtentY        =   873
            _StockProps     =   78
            Picture         =   "frmCadas.frx":0000
         End
         Begin Threed.SSCommand SSApagaDupli 
            Height          =   495
            Left            =   780
            TabIndex        =   7
            TabStop         =   0   'False
            Top             =   60
            Width           =   495
            _Version        =   65536
            _ExtentX        =   873
            _ExtentY        =   873
            _StockProps     =   78
            Picture         =   "frmCadas.frx":031A
         End
         Begin VB.Label lblAux1 
            AutoSize        =   -1  'True
            BackColor       =   &H0000FFFF&
            Caption         =   "lblaux1"
            Height          =   195
            Left            =   810
            TabIndex        =   40
            Top             =   570
            Visible         =   0   'False
            Width           =   495
         End
      End
      Begin VB.Label Label3 
         Caption         =   "Descri��o"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   315
         Left            =   2025
         TabIndex        =   3
         Top             =   450
         Width           =   930
      End
      Begin VB.Label Label2 
         Caption         =   "Tipo Fator"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   75
         TabIndex        =   1
         Top             =   450
         Width           =   915
      End
   End
   Begin Threed.SSFrame fraFator 
      Height          =   6585
      Left            =   120
      TabIndex        =   8
      Top             =   1200
      Width           =   9435
      _Version        =   65536
      _ExtentX        =   16642
      _ExtentY        =   11615
      _StockProps     =   14
      Caption         =   "FATOR"
      ForeColor       =   192
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin VB.TextBox txtDescAlt 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   525
         Left            =   60
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   53
         Top             =   5100
         Visible         =   0   'False
         Width           =   4755
      End
      Begin VB.TextBox txtDtFimAlt 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   315
         Left            =   8130
         TabIndex        =   50
         Top             =   5100
         Visible         =   0   'False
         Width           =   1245
      End
      Begin VB.TextBox txtDtIniAlt 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   315
         Left            =   6840
         TabIndex        =   49
         Top             =   5100
         Visible         =   0   'False
         Width           =   1245
      End
      Begin VB.TextBox txtDescricao 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   795
         Left            =   2940
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   30
         Top             =   2160
         Width           =   6405
      End
      Begin VB.Frame Frame1 
         Caption         =   "Vig�ncia"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   885
         Left            =   120
         TabIndex        =   45
         Top             =   2070
         Width           =   2745
         Begin VB.TextBox txtDtIniVig 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   315
            Left            =   90
            TabIndex        =   28
            Top             =   480
            Width           =   1245
         End
         Begin VB.TextBox txtDtFimVig 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   315
            Left            =   1380
            TabIndex        =   29
            Top             =   480
            Width           =   1245
         End
         Begin VB.Label Label11 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "In�cio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   90
            TabIndex        =   47
            Top             =   270
            Width           =   510
         End
         Begin VB.Label Label12 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   "Fim"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   1380
            TabIndex        =   46
            Top             =   270
            Width           =   300
         End
      End
      Begin VB.TextBox txtSit 
         ForeColor       =   &H00404040&
         Height          =   315
         Left            =   5655
         TabIndex        =   44
         Text            =   "0"
         Top             =   5100
         Visible         =   0   'False
         Width           =   1125
      End
      Begin VB.TextBox txtFat 
         Alignment       =   1  'Right Justify
         ForeColor       =   &H00404040&
         Height          =   315
         Left            =   4860
         TabIndex        =   43
         Text            =   "99,9999"
         Top             =   5100
         Visible         =   0   'False
         Width           =   780
      End
      Begin VB.TextBox txtCodFabrica 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   285
         Left            =   5700
         MaxLength       =   19
         TabIndex        =   21
         Text            =   "AAAAAAAAAAAAAAAAAAA"
         Top             =   1230
         Width           =   3405
      End
      Begin VB.ComboBox cboSituacao 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   315
         ItemData        =   "frmCadas.frx":0634
         Left            =   7260
         List            =   "frmCadas.frx":0641
         Style           =   2  'Dropdown List
         TabIndex        =   27
         Top             =   1620
         Width           =   1845
      End
      Begin VB.TextBox txtFator 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   285
         Left            =   4800
         MaxLength       =   7
         TabIndex        =   25
         Text            =   "12.2346"
         Top             =   1620
         Width           =   915
      End
      Begin VB.TextBox txtCodDpk 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   285
         Left            =   1000
         MaxLength       =   9
         TabIndex        =   19
         Text            =   "123456"
         Top             =   1230
         Width           =   1215
      End
      Begin VB.ComboBox cboSubGrupo 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   315
         ItemData        =   "frmCadas.frx":0663
         Left            =   5700
         List            =   "frmCadas.frx":066A
         Style           =   2  'Dropdown List
         TabIndex        =   17
         Top             =   810
         Width           =   3405
      End
      Begin VB.ComboBox cboGrupo 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   315
         ItemData        =   "frmCadas.frx":0671
         Left            =   1000
         List            =   "frmCadas.frx":0678
         Style           =   2  'Dropdown List
         TabIndex        =   15
         Top             =   810
         Width           =   3405
      End
      Begin VB.TextBox txtCgc 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   285
         Left            =   4275
         MaxLength       =   14
         TabIndex        =   12
         Text            =   "12345678901234"
         Top             =   375
         Width           =   1635
      End
      Begin VB.ComboBox cboTipoFator 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   315
         ItemData        =   "frmCadas.frx":067F
         Left            =   1000
         List            =   "frmCadas.frx":0686
         TabIndex        =   23
         Text            =   "15- ASDFGHJKL�POIUY"
         Top             =   1620
         Width           =   2415
      End
      Begin VB.ComboBox cboDeposito 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   315
         ItemData        =   "frmCadas.frx":068D
         Left            =   1000
         List            =   "frmCadas.frx":0694
         Style           =   2  'Dropdown List
         TabIndex        =   10
         Top             =   375
         Width           =   1815
      End
      Begin Threed.SSPanel SSPanel1 
         Height          =   825
         Left            =   4215
         TabIndex        =   31
         Top             =   5670
         Width           =   5175
         _Version        =   65536
         _ExtentX        =   9128
         _ExtentY        =   1455
         _StockProps     =   15
         BackColor       =   12632256
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Begin Threed.SSCommand cmdImprimir 
            Height          =   495
            Left            =   3315
            TabIndex        =   42
            TabStop         =   0   'False
            Top             =   60
            Width           =   495
            _Version        =   65536
            _ExtentX        =   873
            _ExtentY        =   873
            _StockProps     =   78
            Picture         =   "frmCadas.frx":069B
         End
         Begin Threed.SSCommand cmdCadastro 
            Height          =   495
            Left            =   1350
            TabIndex        =   34
            TabStop         =   0   'False
            Top             =   60
            Width           =   495
            _Version        =   65536
            _ExtentX        =   873
            _ExtentY        =   873
            _StockProps     =   78
            Picture         =   "frmCadas.frx":09B5
         End
         Begin Threed.SSCommand SSCommand1 
            Height          =   495
            Left            =   735
            TabIndex        =   33
            Top             =   60
            Width           =   495
            _Version        =   65536
            _ExtentX        =   873
            _ExtentY        =   873
            _StockProps     =   78
            Caption         =   "SSCommand3"
            ForeColor       =   -2147483640
            Picture         =   "frmCadas.frx":0CCF
         End
         Begin Threed.SSCommand SSCommand3 
            Height          =   495
            Left            =   1995
            TabIndex        =   35
            TabStop         =   0   'False
            Top             =   60
            Width           =   495
            _Version        =   65536
            _ExtentX        =   873
            _ExtentY        =   873
            _StockProps     =   78
            Picture         =   "frmCadas.frx":0FE9
         End
         Begin Threed.SSCommand SSConsDupli 
            Height          =   495
            Left            =   120
            TabIndex        =   32
            Top             =   60
            Width           =   495
            _Version        =   65536
            _ExtentX        =   868
            _ExtentY        =   868
            _StockProps     =   78
            Picture         =   "frmCadas.frx":1303
         End
         Begin Threed.SSCommand SSCommand2 
            Height          =   495
            Left            =   2640
            TabIndex        =   36
            TabStop         =   0   'False
            Top             =   60
            Width           =   495
            _Version        =   65536
            _ExtentX        =   873
            _ExtentY        =   873
            _StockProps     =   78
            Picture         =   "frmCadas.frx":161D
         End
         Begin Threed.SSCommand SSSaiDupli 
            Height          =   495
            Left            =   4575
            TabIndex        =   37
            Top             =   60
            Width           =   495
            _Version        =   65536
            _ExtentX        =   873
            _ExtentY        =   873
            _StockProps     =   78
            Picture         =   "frmCadas.frx":1937
         End
         Begin VB.Label lblAux2 
            AutoSize        =   -1  'True
            BackColor       =   &H0000FFFF&
            Caption         =   "lblaux1"
            Height          =   195
            Left            =   3330
            TabIndex        =   41
            Top             =   550
            Visible         =   0   'False
            Width           =   495
         End
      End
      Begin MSGrid.Grid Grid1 
         Height          =   465
         Left            =   105
         TabIndex        =   39
         Top             =   3030
         Visible         =   0   'False
         Width           =   1140
         _Version        =   65536
         _ExtentX        =   2011
         _ExtentY        =   820
         _StockProps     =   77
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Cols            =   5
         FixedCols       =   4
         MouseIcon       =   "frmCadas.frx":1C51
      End
      Begin VB.Label lblDescricaoAlt 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Descri��o:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   60
         TabIndex        =   56
         Top             =   4890
         Visible         =   0   'False
         Width           =   930
      End
      Begin VB.Label lblSituacaoAlt 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Situa��o"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   5670
         TabIndex        =   55
         Top             =   4890
         Visible         =   0   'False
         Width           =   765
      End
      Begin VB.Label lblFatorAlt 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Fator"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   4860
         TabIndex        =   54
         Top             =   4890
         Visible         =   0   'False
         Width           =   450
      End
      Begin VB.Label lblDtFimAlt 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Dt. Fim"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   8130
         TabIndex        =   52
         Top             =   4890
         Visible         =   0   'False
         Width           =   615
      End
      Begin VB.Label lblDtInicioAlt 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Dt. In�cio"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   6840
         TabIndex        =   51
         Top             =   4890
         Visible         =   0   'False
         Width           =   825
      End
      Begin VB.Label Label13 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Descri��o"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   2940
         TabIndex        =   48
         Top             =   1950
         Width           =   870
      End
      Begin VB.Label Label4 
         Caption         =   "F�brica"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   4980
         TabIndex        =   20
         Top             =   1230
         Width           =   675
      End
      Begin VB.Label Label9 
         Caption         =   "Situa��o"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   225
         Left            =   6405
         TabIndex        =   26
         Top             =   1620
         Width           =   825
      End
      Begin VB.Label lblMensagem 
         Caption         =   "MENSAGEM"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H008080FF&
         Height          =   615
         Left            =   90
         TabIndex        =   38
         Top             =   5655
         Visible         =   0   'False
         Width           =   3615
         WordWrap        =   -1  'True
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         Caption         =   "Fator"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   4275
         TabIndex        =   24
         Top             =   1620
         Width           =   480
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "DPK"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   550
         TabIndex        =   18
         Top             =   1230
         Width           =   390
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         Caption         =   "Subgrupo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   4830
         TabIndex        =   16
         Top             =   810
         Width           =   825
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Grupo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   420
         TabIndex        =   14
         Top             =   795
         Width           =   525
      End
      Begin VB.Label lblNomeFornec 
         Caption         =   "Nome Fornecedor"
         ForeColor       =   &H00FF0000&
         Height          =   405
         Left            =   6060
         TabIndex        =   13
         Top             =   375
         Width           =   1785
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblCgc 
         AutoSize        =   -1  'True
         Caption         =   "C�d./CGC"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   3360
         TabIndex        =   11
         Top             =   375
         Width           =   870
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Tipo Fator"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   75
         TabIndex        =   22
         Top             =   1620
         Width           =   885
      End
      Begin VB.Label Label10 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Dep�sito"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   180
         TabIndex        =   9
         Top             =   375
         Width           =   765
      End
   End
End
Attribute VB_Name = "frmCadastro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Eduardo - 29/06/2007
Dim vFatorAnt As String
Dim vSituacaoAnt As String
Dim vDescricaoAnt As String
Dim vDtIniAnt As String
Dim vDtFimAnt As String


Dim Fl_Fornec As Boolean
Dim Fl_Consul As Boolean
Dim Fl_Itens As Boolean
Dim Fl_Erro_Tipo_Fator As Boolean
Dim Fl_Erro_Fator As Boolean
Dim Fl_Achou_Tipo_Fator As Boolean
Dim Fl_Achou_Fator As Boolean

Dim vGuardaLin As Integer
Dim vGuardaCol As Integer
Sub Carrega_Depositos()

    CmdSql = "select a.COD_LOJA, a.NOME_FANTASIA " & _
             "from LOJA a, DEPOSITO_VISAO b " & _
             "where b.NOME_PROGRAMA = 'CAD520' and " & _
             "a.COD_LOJA = b.COD_LOJA " & _
             "order by COD_LOJA"

    Set db_CONS = db.createdynaset(CmdSql, 0&)

    If db_CONS.EOF = True Then
        MsgBox "N�o Existe Dep�sito Cadastrado Avisar Analista Respons�vel", vbCritical, Msg_Padrao
        End
    End If

    While db_CONS.EOF = False ' carrega todos os depositos existentes
        cboDeposito.AddItem db_CONS!COD_LOJA & " - " & db_CONS!NOME_FANTASIA
        db_CONS.MoveNext
    Wend
    cboDeposito.Text = "1 - CAMPINAS"

End Sub

Sub Carrega_Fatores()

    CmdSql = "select * " & _
             "from COMPRAS.TIPO_FATOR "
         
    Set db_CONS = db.createdynaset(CmdSql, 0&)

    If db_CONS.EOF = True Then
        Screen.MousePointer = 0
        MsgBox "N�o Existe Fatores Cadastrados, Avisar Analista Respons�vel", vbCritical, Msg_Padrao
        End
    End If

    While db_CONS.EOF = False ' carrega todos os fatores existentes
        cboTipoFator.AddItem db_CONS!TIPO_FATOR & " - " & db_CONS!DESCR_FATOR
        db_CONS.MoveNext
    Wend

End Sub

Sub Carrega_Grupo()
    
    If Trim(txtCgc.Text) = "" Then
        Exit Sub
    End If

    Screen.MousePointer = 11
    lblMensagem.Caption = "Pesquisando Grupos"
    lblMensagem.Visible = True
    Me.Refresh

    db.Parameters.Remove "CGC"
    db.Parameters.Add "CGC", txtCgc.Text, 1

    CmdSql = "select distinct a.COD_GRUPO, a.DESC_GRUPO " & _
             "from GRUPO a, ITEM_CADASTRO b, FORNECEDOR c " & _
             "where c.CGC = :CGC and " & _
             "b.COD_FORNECEDOR = c.COD_FORNECEDOR " & _
             "order by a.cod_grupo"
    
    Set db_CONS = db.createdynaset(CmdSql, 0&)
         
    If db_CONS.EOF = True Then
        lblMensagem.Visible = False
        Screen.MousePointer = 0
        MsgBox "N�o Existe Cadastro de Grupo Para o Fornecedor Escolhido", vbCritical, Msg_Padrao
        Exit Sub
    End If

    cboGrupo.Clear
    cboGrupo.AddItem "0"

    While db_CONS.EOF = False
        Me.Refresh
        lblMensagem.Caption = "Carregando C�digo de Grupo" & Chr(13) & _
                               db_CONS!COD_GRUPO & " - " & db_CONS!DESC_GRUPO
        cboGrupo.AddItem db_CONS!COD_GRUPO & " - " & db_CONS!DESC_GRUPO
        db_CONS.MoveNext
    Wend
    
    cboGrupo.Text = "0"
    lblMensagem.Visible = False
    Screen.MousePointer = 0
    
End Sub

Sub Carrega_SubGrupo()

    Screen.MousePointer = 11
    lblMensagem.Caption = "Pesquisando SubGrupos"
    lblMensagem.Visible = True
    Me.Refresh
    
    db.Parameters.Remove "CGC"
    db.Parameters.Remove "COD_GRUPO"
    db.Parameters.Add "CGC", Trim(txtCgc.Text), 1
    db.Parameters.Add "COD_GRUPO", Trim(Mid(cboGrupo.Text, 1, 2)), 1

    'Estava assim at� 23/07/02 - Aleth�a
    'CmdSql = "select distinct a.COD_SUBGRUPO, a.DESC_SUBGRUPO " & _
             "from SUBGRUPO a, ITEM_CADASTRO b, FORNECEDOR c " & _
             "where c.CGC = :CGC and " & _
             "c.COD_FORNECEDOR = b.COD_FORNECEDOR and " & _
             "b.COD_GRUPO = :COD_GRUPO and " & _
             "b.COD_GRUPO = a.COD_GRUPO and " & _
             "b.COD_SUBGRUPO = a.COD_GRUPO"
CmdSql = "Select distinct s.COD_SUBGRUPO, s.DESC_SUBGRUPO " & _
          "From SUBGRUPO s, ITEM_CADASTRO c, FORNECEDOR f " & _
         "Where f.cgc = :CGC " & _
           "And c.COD_GRUPO = :COD_GRUPO " & _
           "And f.COD_FORNECEDOR = c.COD_FORNECEDOR " & _
           "And c.COD_GRUPO = s.COD_GRUPO " & _
           "Order by s.cod_subgrupo"

    Set db_CONS = db.createdynaset(CmdSql, 0&)

    If db_CONS.EOF = True Then
        lblMensagem.Visible = False
        Screen.MousePointer = 0
        MsgBox " N�o Existe SubGrupo Cadastrado Para o Grupo Selecionado", vbCritical, Msg_Padrao
        Exit Sub
    End If

    cboSubGrupo.Clear
    cboSubGrupo.AddItem "0"
    
    While db_CONS.EOF = False
        Me.Refresh
        lblMensagem.Caption = "Carregando C�digo de SubGrupo" & Chr(13) & _
                               db_CONS!COD_SUBGRUPO & " - " & db_CONS!DESC_SUBGRUPO
        cboSubGrupo.AddItem db_CONS!COD_SUBGRUPO & " - " & db_CONS!DESC_SUBGRUPO
        db_CONS.MoveNext
    Wend
    
    cboSubGrupo.Text = "0"
    lblMensagem.Visible = False
    Screen.MousePointer = 0

End Sub

Sub Checa_Campos_Fator()

    Fl_Erro_Fator = False

    If Trim(cboDeposito.Text) = "" Then
        MsgBox "Escolha um Dep�sito para o Cadastro de Fator", vbcritcal, Msg_Padrao
        cboDeposito.SetFocus
        Fl_Erro_Fator = True
        Exit Sub

    ElseIf Trim(txtCgc.Text) = "" Then
        MsgBox "Selecione um Fornecedor Para o Fator", vbCritical, Msg_Padrao
        txtCgc.SetFocus
        Fl_Erro_Fator = True
        Exit Sub
        
    ElseIf Trim(cboGrupo) = "0" And Trim(cboSubGrupo) <> "0" Then
        MsgBox "Quando Grupo = 0, Subgrupo tamb�m tem que ser 0!", vbCritical, msf_padrao
        cboSubGrupo.SetFocus
        Fl_Erro_Fator = True
        Exit Sub

    ElseIf Trim(cboTipoFator.Text) = "" Then
        MsgBox "Escolha um Tipo de Fator Para o Cadastro de Fator", vbCritical, msf_padrao
        cboTipoFator.SetFocus
        Fl_Erro_Fator = True
        Exit Sub

    ElseIf Trim(txtFator.Text) = "" Then
        MsgBox "Informe o Fator Multiplicativo Para os Dados", vbCritical, Msg_Padrao
        txtFator.SetFocus
        Fl_Erro_Fator = True
        Exit Sub

    ElseIf Trim(cboSituacao.Text) = "" Then
        MsgBox "Selecione uma Situa��o Para o Cadastro do Fator", vbCritical, Msg_Padrao
        cboSituacao.SetFocus
        Fl_Erro_Fator = True
        Exit Sub
        
    End If

    If Trim(txtDtFimVig) = "" Or Trim(txtDtIniVig) = "" Then
        MsgBox "Informe a data de in�cio de vig�ncia e a data de fim de vig�ncia.", vbInformation, "Aten��o"
        Fl_Erro_Fator = True
        txtDtIniVig.SetFocus
        Exit Sub
    Else
        If CDate(txtDtFimVig) < CDate(txtDtIniVig) Then
            MsgBox "Data de in�cio deve ser maior que data de fim da vig�ncia.", vbInformation, "Aten��o"
            Fl_Erro_Fator = True
            txtDtIniVig.SetFocus
            Exit Sub
        End If
    End If
    
    If Trim(txtDescricao) = "" Or fDescricaoValida(Trim(txtDescricao)) = False Then
        MsgBox "Informe uma descri��o v�lida.", vbInformation, "Aten��o"
        Fl_Erro_Fator = True
        txtDescricao.SetFocus
        Exit Sub
    End If

End Sub

Function fDescricaoValida(pDesc As String) As Boolean

If Mid(pDesc, 1, 1) = "!" Or Mid(pDesc, 1, 1) = "@" Or Mid(pDesc, 1, 1) = "#" Or Mid(pDesc, 1, 1) = "$" _
Or Mid(pDesc, 1, 1) = "%" Or Mid(pDesc, 1, 1) = "�" Or Mid(pDesc, 1, 1) = "&" Or Mid(pDesc, 1, 1) = "*" _
Or Mid(pDesc, 1, 1) = "(" Or Mid(pDesc, 1, 1) = ")" Or Mid(pDesc, 1, 1) = "_" Or Mid(pDesc, 1, 1) = "-" _
Or Mid(pDesc, 1, 1) = "+" Or Mid(pDesc, 1, 1) = "=" Or Mid(pDesc, 1, 1) = "[" Or Mid(pDesc, 1, 1) = "{" _
Or Mid(pDesc, 1, 1) = "]" Or Mid(pDesc, 1, 1) = "}" Or Mid(pDesc, 1, 1) = "/" Or Mid(pDesc, 1, 1) = "?" _
Or Mid(pDesc, 1, 1) = ";" Or Mid(pDesc, 1, 1) = ":" Or Mid(pDesc, 1, 1) = "." Or Mid(pDesc, 1, 1) = ">" _
Or Mid(pDesc, 1, 1) = "," Or Mid(pDesc, 1, 1) = "<" Or Mid(pDesc, 1, 1) = "\" Or Mid(pDesc, 1, 1) = "|" _
Or Mid(pDesc, 1, 1) = "�" Or Mid(pDesc, 1, 1) = "`" Or Mid(pDesc, 1, 1) = "'" _
Or Mid(pDesc, 1, 1) = "0" Or Mid(pDesc, 1, 1) = "1" Or Mid(pDesc, 1, 1) = "2" Or Mid(pDesc, 1, 1) = "3" _
Or Mid(pDesc, 1, 1) = "4" Or Mid(pDesc, 1, 1) = "5" Or Mid(pDesc, 1, 1) = "6" Or Mid(pDesc, 1, 1) = "7" _
Or Mid(pDesc, 1, 1) = "8" Or Mid(pDesc, 1, 1) = "9" Then
    fDescricaoValida = False
Else
    fDescricaoValida = True
End If

End Function

Sub Checa_Campos_Tipo_Fator()

    Fl_Erro_Tipo_Fator = False

    If Trim(txtTipoFator.Text) = "" Then
        MsgBox "Tipo de Fator em Branco, Favor Corrigir!", vbCritical, Msg_Padrao
        Fl_Erro_Tipo_Fator = True
        Exit Sub
    End If

    If Trim(txtdescrFator.Text) = "" Then
        MsgBox "Descri��o de Fator em Branco, Favor Corrigir!", vbCritical, Msg_Padrao
        Fl_Erro_Tipo_Fator = True
        txtdescrFator.SetFocus
        Exit Sub
    End If
    
End Sub

Sub Limpa_Campos_Fator()

    cboDeposito.Text = " "
    txtCgc.Text = ""
    lblNomeFornec.Caption = ""
    cboGrupo.Text = "0"
    cboSubGrupo.Text = "0"
    txtCodFabrica.Text = ""
    txtDescItem = ""
    txtCodDpk.Text = "0"
    cboTipoFator.Text = " "
    txtFator.Text = ""
    cboSituacao.Text = " "
    Grid1.Visible = False
    txtDtFimVig = ""
    txtDtIniVig = ""
    txtDescricao = ""

End Sub

Sub Limpa_Campos_Tipo_Fator()
    
    txtTipoFator.Text = ""
    txtdescrFator.Text = ""
    
End Sub

Sub Procura_Fator()
'Fun��o que procura o fator cadastrado para os dados informados

db.Parameters.Remove "COD_LOJA": db.Parameters.Add "COD_LOJA", Trim(Mid(cboDeposito.Text, 1, 2)), 1
db.Parameters.Remove "COD_FORNECEDOR": db.Parameters.Add "COD_FORNECEDOR", Val(Trim(Mid(lblNomeFornec.Caption, 1, 3))), 1
db.Parameters.Remove "COD_GRUPO": db.Parameters.Add "COD_GRUPO", IIf(Trim(cboGrupo.Text) = "", "0", Trim(Mid(cboGrupo.Text, 1, 2))), 1
db.Parameters.Remove "COD_SUBGRUPO": db.Parameters.Add "COD_SUBGRUPO", IIf(Trim(cboSubGrupo.Text) = "", "0", Trim(Mid(cboSubGrupo.Text, 1, 2))), 1
db.Parameters.Remove "COD_DPK": db.Parameters.Add "COD_DPK", IIf(Trim(txtCodDpk.Text) = "", "0", Trim(txtCodDpk.Text)), 1
db.Parameters.Remove "TIPO_FATOR": db.Parameters.Add "TIPO_FATOR", Trim(Mid(cboTipoFator.Text, 1, 2)), 1
db.Parameters.Remove "FATOR": db.Parameters.Add "FATOR", Trim(txtFator.Text), 1

'Procura com todos os campos
CmdSql = "Select FATOR, SITUACAO " & _
         "From COMPRAS.FATOR " & _
         "Where COD_LOJA = :COD_LOJA " & _
         "And COD_FORNECEDOR = :COD_FORNECEDOR " & _
         "And COD_GRUPO = :COD_GRUPO " & _
         "And COD_SUBGRUPO = :COD_SUBGRUPO " & _
         "And COD_DPK = :COD_DPK " & _
         "And TIPO_FATOR = :TIPO_FATOR"
Set db_CONS = db.createdynaset(CmdSql, 8&)

If db_CONS.EOF = True Then 'N�o existe cadastro, ent�o pode incluir
   'txtFator.Text = ""
   Fl_Achou_Fator = False
   Exit Sub
End If

txtFator.Text = Format(db_CONS!FATOR, "0.0000")
cboSituacao.Text = IIf(db_CONS!situacao = 0, "0 - Ativo", "9 - Desativado")
Fl_Achou_Fator = True
    
End Sub

Private Sub cboDeposito_GotFocus()

    If Trim(cboDeposito.Text) = "" Then
        Exit Sub
    End If
    
    Grid1.Visible = False

    'cboDeposito.SelStart = 0
    'cboDeposito.SelLength = Len(cboDeposito.Text)

End Sub


Private Sub cboGrupo_GotFocus()

    If Trim(txtCgc.Text) = "" Then
        Exit Sub
    End If

    Call Carrega_Grupo  'Fun��o que carrega o grupo de fornecedores
    
End Sub



Private Sub cboGrupo_LostFocus()

    If Trim(cboGrupo.Text) = "" Then
        cboSubGrupo.Text = "0"
    End If
    
End Sub


Private Sub cboSituacao_GotFocus()

    If Trim(cboSituacao.Text) = "" Then
        Exit Sub
    End If
    
End Sub


Private Sub cboSituacao_KeyPress(KeyAscii As Integer)

    KeyAscii = Numerico(KeyAscii)

End Sub


Private Sub cboSubGrupo_GotFocus()

    If Trim(cboGrupo.Text) = "" Or Trim(cboGrupo.Text) = "0" Then
        Exit Sub
    End If
    
    Call Carrega_SubGrupo
    
End Sub


Private Sub cboSubGrupo_LostFocus()

    If Trim(cboSubGrupo.Text) = "" Then
        cboSubGrupo.Text = "0"
    End If
    
End Sub


Private Sub cboTipoFator_LostFocus()

 '   Call Procura_Fator

End Sub


Private Sub cmdCadastro_Click()

    Fl_Fornec = False
    Fl_Itens = True
    Fl_Consul = False
    Screen.MousePointer = 11

    If Trim(txtCgc.Text) = "" Then
        Screen.MousePointer = 0
        MsgBox "Para Pesquisar �tens Pelo C�digo de F�brica e Necess�rio que se Escolha um Fornecedor", vbCritical, Msg_Padrao
        txtCgc.SetFocus
        Exit Sub
    End If

    RET = UCase(InputBox("Digite o Cod. F�brica ou Parte do Mesmo:", Msg_Padrao, ""))

    If RET = "" Then
        Screen.MousePointer = 0
        Exit Sub
    End If

    lblMensagem.Caption = "Pesquisando Itens "
    lblMensagem.Visible = True

    db.Parameters.Remove "COD_FABRICA"
    db.Parameters.Remove "COD_FORNECEDOR"
    db.Parameters.Add "COD_FABRICA", "%" & RET & "%", 1
    db.Parameters.Add "COD_FORNECEDOR", Val(Trim(Mid(lblNomeFornec.Caption, 1, 3))), 1

    CmdSql = "select COD_DPK, COD_FABRICA, DESC_ITEM " & _
             "from ITEM_CADASTRO " & _
             "where COD_FORNECEDOR = :COD_FORNECEDOR and " & _
             "COD_FABRICA like :COD_FABRICA " & _
             "order by COD_FABRICA"
         
    Set db_CONS = db.createdynaset(CmdSql, 0&)

    If db_CONS.EOF = True Then
        lblMensagem.Visible = False
        Screen.MousePointer = 0
        MsgBox "C�digo de F�brica n�o Encontrado !", vbCritical, Msg_Padrao
    End If
    
    'Chama o grid que est� escondido em um frame no formul�rio de manuten��o de natureza

    Grid1.Visible = True
    Grid1.Cols = 3
    Grid1.Top = 2300
    Grid1.Left = 1000
    Grid1.Height = 1700
    Grid1.Width = 7500
    Grid1.Rows = db_CONS.RecordCount + 1
    Grid1.Row = 0
    Grid1.Col = 0
    Grid1.Text = "COD.DPK"
    Grid1.Col = 1
    Grid1.Text = "COD.FABRICA"
    Grid1.Col = 2
    Grid1.Text = "DESCRI��O ITEM"
    Grid1.FixedRows = 1
    Grid1.ColWidth(0) = 5 * 184
    Grid1.ColWidth(1) = 12 * 184
    Grid1.ColWidth(2) = 22 * 184

    For i = 0 To 2
        Grid1.Col = i
        Grid1.FixedAlignment(i) = 2
    Next i

    frmCadastro.Refresh

    'Carrega o grid com as informa��es referentes a cidade
    i = 1
    
    While Not db_CONS.EOF
        lblMensagem.Caption = "Carregando Itens ! " & Chr(13) & db_CONS!DESC_ITEM
        Me.Refresh
        Grid1.Row = i
        Grid1.Col = 0
        Grid1.Text = db_CONS!COD_DPK
        Grid1.Col = 1
        Grid1.Text = db_CONS!COD_FABRICA
        Grid1.Col = 2
        Grid1.Text = db_CONS!DESC_ITEM
        db_CONS.MoveNext
        i = i + 1
    Wend

    Grid1.SetFocus
    lblMensagem.Visible = False
    Screen.MousePointer = 0

End Sub

Private Sub cmdCadastro_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

    lblAux2.Left = 1380
    lblAux2.Caption = "Cons.Itens"
    lblAux2.Visible = True

End Sub


Private Sub cmdImprimir_Click()

Dim liLinhaGrid, liColunaGrid As Integer

liLINHAImpressao = 0
    
    
If MsgBox("Confirma Impress�o?", vbQuestion + vbYesNo, "Aten��o") = vbYes Then
        
    Grid1.Visible = True
        
'    If Grid1.Visible = False Then
'        MsgBox "N�o existem informa��es a serem impressas !", vbCritical, "Aten��o!"
'        Exit Sub
'    End If
        
    Printer.Font = "COURIER NEW"
    Printer.Orientation = 2
    
    With Grid1
            
        'Verifica se existem informa��es a serem impressas
        If .Rows = 2 Then
            .Row = 1
            .Col = 0
            If .Text = "" Then
                MsgBox "N�o existem informa��es a serem impressas !", vbCritical, "Aten��o!"
                Grid1.Visible = False
                Exit Sub
            End If
        End If
            
        .Row = 0
        liPagina = 0
            
        'Imprime cabe�alho do relat�rio de fator
        GoSub pImprimirCabecalho
            
        Do
            .Row = .Row + 1
            .Col = 0    'CD
                
            'Imprime linha de detalhe do relat�rio, sempre que existir conte�do na coluna zero
            If Trim(.Text) <> "" Then
                    
                'Vai imprimir CGC, Nome, Categoria, Linhas e Observa��o
                .Col = 0:  LSTRLINHA = Format(Mid(Trim(.Text), 1, InStr(Trim(.Text), " - ") - 1), "00")
                .Col = 1:  LSTRLINHA = LSTRLINHA & " " & _
                           Format(Mid(Trim(.Text), 1, InStr(Trim(.Text), " - ") - 1), "000")
                .Col = 2:
                If Trim(.Text) = "-" Then
                    LSTRLINHA = LSTRLINHA & " 0               "
                Else
                    LSTRLINHA = LSTRLINHA & " " & _
                           Format(Mid(Trim(.Text), 1, InStr(Trim(.Text), " - ") - 1), "00") & _
                           "-" & _
                           Left(Mid(Trim(.Text), InStr(Trim(.Text), " - ") + 3), 13) & _
                           Space(13 - Len(Left(Mid(Trim(.Text), InStr(Trim(.Text), " - ") + 3), 13)))
                End If
                .Col = 3:
                If Trim(.Text) = "-" Then
                    LSTRLINHA = LSTRLINHA & " 0               "
                Else
                    LSTRLINHA = LSTRLINHA & " " & _
                           Format(Mid(Trim(.Text), 1, InStr(Trim(.Text), " - ") - 1), "00") & _
                           "-" & _
                           Left(Mid(Trim(.Text), InStr(Trim(.Text), " - ") + 3), 13) & _
                           Space(13 - Len(Left(Mid(Trim(.Text), InStr(Trim(.Text), " - ") + 3), 13)))
                End If
                'eoliveira - CiT - 08/08/2012
                ' ISA-175 - Alterar tamanho do c�digo DPK
                .Col = 4:  LSTRLINHA = LSTRLINHA & " " & String(18 - Len(Trim(.Text)), " ") & Trim(.Text)
                .Col = 6:
                If Trim(.Text) = "-" Then
                    LSTRLINHA = LSTRLINHA & "                 "
                Else
                    LSTRLINHA = LSTRLINHA & " " & _
                           Format(Mid(Trim(.Text), 1, InStr(Trim(.Text), " - ") - 1), "00") & _
                           "-" & _
                           Left(Mid(Trim(.Text), InStr(Trim(.Text), " - ") + 3), 13) & _
                           Space(13 - Len(Left(Mid(Trim(.Text), InStr(Trim(.Text), " - ") + 3), 13)))
                End If
                .Col = 7: LSTRLINHA = LSTRLINHA & " " & _
                                       String(7 - Len(Trim(.Text)), "0") & Trim(.Text)
                .Col = 8: LSTRLINHA = LSTRLINHA & " " & _
                                       Left(Mid(Trim(.Text), InStr(Trim(.Text), " - ") + 3), 3)
                    
                'Eduardo - 08/06/2007
                .Col = 9: LSTRLINHA = LSTRLINHA & "  " & Trim(.Text)
                .Col = 10: LSTRLINHA = LSTRLINHA & "  " & Trim(.Text)
                .Col = 11: LSTRLINHA = LSTRLINHA & "  " & Trim(.Text)
                    
                    
                Printer.Print LSTRLINHA
                liLINHAImpressao = liLINHAImpressao + 1
                    
                    
                'VERIFICA QUEBRA DE PAGINA
                If liLINHAImpressao > 55 And .Row <> .Rows - 1 Then
                    Printer.EndDoc
                    GoSub pImprimirCabecalho
                End If
                    
            End If
            
        Loop Until .Row = .Rows - 1
        'eoliveira - CiT - 09/08/2012
        'ISA-175 Altera��o do tamanho do c�digo DPK
        Printer.Print String(162, "-")
        Printer.EndDoc
            
        MsgBox "Fim de Impress�o!", vbInformation, "Aten��o"
        
    End With
    
End If
Exit Sub


pImprimirCabecalho:
    Printer.Print " "
    Printer.Print "                                                            CAD520 - RELATORIO DE FATOR (VENDOR)                     "
    Printer.Print "                                                            ====================================                     "
    Printer.Print " "
    'eoliveira - CiT - 08/08/2012
    ' ISA-175 - Alterar tamanho do c�digo DPK
    Printer.Print "==+===+================+================+==================+================+=======+====+=========+=========+===================================================+"
    Printer.Print "CD|FOR|GRUPO           |SUBGRUPO        |DPK               |TP.FAT          |FATOR  |SIT |INI VIG  |FIM VIG  |DESCRICAO                                          |"
    Printer.Print "==+===+================+================+==================+================+=======+====+=========+=========+===================================================+"
    liLINHAImpressao = 7
    Return


End Sub

Private Sub cmdImprimir_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblAux2.Left = 3330
    lblAux2.Caption = "Relat�rio"
    lblAux2.Visible = True
End Sub


Private Sub Form_Load()

    WindowState = 2
    Call Carrega_Depositos          'Carrega combo com os depositos
    Call Carrega_Fatores            'Carrega combo com os fatores
    Call Limpa_Campos_Tipo_Fator    'Limpa campos do tipo de fator
    Call Limpa_Campos_Fator         'Limpa campos do tipo de fator
    Screen.MousePointer = 0
    
    txtFat.Visible = False
    txtSit.Visible = False
    
    
End Sub


Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

    lblAux1.Visible = False
    lblAux2.Visible = False
    
End Sub


Private Sub fraFator_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

    lblAux2.Visible = False

End Sub


Private Sub fraTipoFator_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

    lblAux1.Visible = False

End Sub


Private Sub Grid1_DblClick()

'***
'Se em alguma linha j� foi dado duplo clique para alterar uma das duas colunas (7 ou 8)
'e a opera��o ainda n�o foi finalizada, n�o faz nada
If txtFat.Visible = True Or txtSit.Visible = True Or txtDtIniAlt.Visible = True Or Me.txtDtFimAlt.Visible = True Or Me.txtDescAlt.Visible = True Then
    Exit Sub
End If

Screen.MousePointer = 11
vGuardaLin = Grid1.Row
vGuardaCol = Grid1.Col

'Eduardo - 29/06/2007
'neste ponto pego todos os valores e jogo nas variaveis para uso no Insert da Compras.Log_Fator
Grid1.Col = 7
vFatorAnt = Grid1
Grid1.Col = 8
vSituacaoAnt = Grid1
Grid1.Col = 9
vDtIniAnt = Grid1
Grid1.Col = 10
vDtFimAnt = Grid1
Grid1.Col = 11
vDescricaoAnt = Grid1

Grid1.Col = vGuardaCol

'Se o duplo clique for nas colunas de Fator ou Situa��o, prepara as vari�veis de bind para fazer update
If vGuardaCol >= 7 Then     'Fator ou Situa��o ou Dt Inicio Vigencia ou Dt Fim Vigencia ou Descricao
  
    Grid1.Row = vGuardaLin
    
    If vGuardaCol = 7 Then
        lblFatorAlt.Visible = True
        txtFat.Visible = True
        txtFat = Grid1
        txtFat.SetFocus
        
    ElseIf vGuardaCol = 8 Then
        lblSituacaoAlt.Visible = True
        txtSit.Visible = True
        txtSit = Mid(Grid1, 1, 1)
        txtSit.SetFocus
        
    ElseIf vGuardaCol = 9 Or vGuardaCol = 10 Then
        
        lblDtInicioAlt.Visible = True
        txtDtIniAlt.Visible = True
        
        lblDtFimAlt.Visible = True
        txtDtFimAlt.Visible = True
        
        Grid1.Col = 9
        txtDtIniAlt = Grid1
        txtDtIniAlt.Tag = Grid1
        
        Grid1.Col = 10
        txtDtFimAlt = Grid1
        txtDtFimAlt.Tag = Grid1
        
        txtDtIniAlt.SetFocus
        
    ElseIf vGuardaCol = 11 Then
        
        Grid1.Col = 11
        lblDescricaoAlt.Visible = True
        txtDescAlt.Visible = True
        txtDescAlt = Grid1
        txtDescAlt.SetFocus
        
    End If
    
End If

'***
    
'Anteriormente, qdo. dado duplo clique, o sistema passava as informa��es da linha do grid
'para o cadastro
    
'    If Fl_Fornec = True Then
'        Grid1.Col = 1
'        lblNomeFornec.Caption = Trim(Grid1.Text)
'        Grid1.Col = 2
'        txtCgc.Text = Trim(Grid1.Text)
'        Grid1.Rows = 1
'        Grid1.Rows = 1
'        txtCgc.SetFocus
'    ElseIf Fl_Consul = True Then
'        Grid1.Col = 0
'        cboDeposito.Text = Trim(Grid1.Text)
'        Grid1.Col = 1
'        txtCgc.Text = Val(Trim(Mid(Grid1.Text, 1, 3)))
'        txtCgc_LostFocus
'        Screen.MousePointer = 11
'        Call Carrega_Grupo
'        Screen.MousePointer = 11
'        Grid1.Col = 2
'        If Trim(Mid(Grid1.Text, 1, 1)) <> "" Then 'caso tenha gurupo
'            cboGrupo.Text = Trim(Grid1.Text)
'            Call Carrega_SubGrupo
'            Screen.MousePointer = 11
'            Grid1.Col = 3
'            If Trim(Mid(Grid1.Text, 1, 1)) <> "" Then 'caso tenha subgurupo
'                cboSubGrupo.Text = Trim(Grid1.Text)
'            Else
'                cboSubGrupo.Text = "0"
'            End If
'        Else ' caso nao tenha grupo
'            cboGrupo.Text = "0"
'            cboSubGrupo.Text = "0"
'        End If
    
'        Grid1.Col = 4
'        txtcodDpk.Text = Grid1.Text
'        Grid1.Col = 5
'        txtCodFabrica.Text = Grid1.Text
'        Grid1.Col = 6
'        cboTipoFator.Text = Grid1.Text
'        Grid1.Col = 7
'        txtFator.Text = Grid1.Text
'        Grid1.Col = 8
'        cboSituacao.Text = Grid1.Text
'        txtFator.SetFocus
'        Fl_Achou_Fator = True
'    ElseIf Fl_Itens = True Then
'        Grid1.Col = 0
'        txtcodDpk.Text = Trim(Grid1.Text)
'        txtcodDpk_LostFocus
'        txtFator.SetFocus
'    End If
    
    Screen.MousePointer = 0

End Sub

Private Sub Grid1_LostFocus()

    Screen.MousePointer = 0
'    Grid1.Visible = False
    
End Sub


Private Sub SSApagaDupli_Click()

    'Fun��o que limpa os campos do cadastro de tipo de fator
    Call Limpa_Campos_Tipo_Fator
    
End Sub

Private Sub SSApagaDupli_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

    lblAux1.Left = 810
    lblAux1.Caption = "Limpar"
    lblAux1.Visible = True

End Sub


Private Sub SSCommand1_Click()

    DoEvents
    Fl_Fornec = True
    Fl_Consul = False
    Fl_Itens = False

    Dim RET As String   'Vari�vel que recebe parte do nome da natureza de opera��o para pesquisa

    Screen.MousePointer = 0
    RET = UCase(InputBox("Digite o nome do fornecedor ou Parte do Mesmo:", Msg_Padrao, ""))

    If RET = "" Then
        Exit Sub
    End If

    lblMensagem.Caption = "Pesquisando Fornecedores "
    Me.Refresh
    lblMensagem.Visible = True
    DoEvents
    Grid1.Visible = False
    Screen.MousePointer = 11
    frmCadastro.Refresh

    db.Parameters.Remove "NOME_FORNEC"
    db.Parameters.Add "NOME_FORNEC", "%" & RET & "%", 1

    CmdSql = "SELECT COD_FORNECEDOR, SIGLA, CGC " & _
             "From FORNECEDOR " & _
             "Where NOME_FORNEC like :NOME_FORNEC " & _
             "Order By NOME_FORNEC "
       
    Set db_CONS = db.createdynaset(CmdSql, 0&)
     
    If db_CONS.EOF = True Then
        Screen.MousePointer = 0
        lblMensagem.Visible = False
        MsgBox "Fornecedor N�o Existe!", vbExclamation, Msg_Padrao
        Exit Sub
    End If

    'Chama o grid que est� escondido em um frame no formul�rio de manuten��o de natureza
    Grid1.Visible = True
    Grid1.Cols = 3
    Grid1.Top = 2300
    Grid1.Left = 2500
    Grid1.Height = 1700
    Grid1.Width = 4580
    Grid1.Rows = db_CONS.RecordCount + 1
    Grid1.Row = 0
    Grid1.Col = 0
    Grid1.Text = "COD"
    Grid1.Col = 1
    Grid1.Text = "NOME"
    Grid1.Col = 2
    Grid1.Text = "CGC"
    Grid1.FixedRows = 1
    Grid1.ColWidth(0) = 3 * 184
    Grid1.ColWidth(1) = 12 * 184
    Grid1.ColWidth(2) = 8 * 184

    For i = 0 To 2
        Grid1.Col = i
        Grid1.FixedAlignment(i) = 2
    Next i

    frmCadastro.Refresh

    'Carrega o grid com as informacoes referentes a cidade
    i = 1

    While Not db_CONS.EOF
        lblMensagem.Caption = "Carregando Fornecedor " & db_CONS!sigla
        Me.Refresh
        Grid1.Row = i
        Grid1.Col = 0
        Grid1.Text = db_CONS!COD_FORNECEDOR
        Grid1.Col = 1
        Grid1.Text = db_CONS!sigla
        Grid1.Col = 2
        Grid1.Text = db_CONS!cgc
        db_CONS.MoveNext
        i = i + 1
    Wend
    
    lblMensagem.Visible = False
    Screen.MousePointer = 0
    Grid1.SetFocus

End Sub

Private Sub SSCommand1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

    lblAux2.Left = 720
    lblAux2.Caption = "Cons.Fornecedor"
    lblAux2.Visible = True

End Sub


Private Sub SSCommand2_Click()

    'Limpa campos do tipo de fator
    Call Limpa_Campos_Fator

End Sub

Private Sub SSCommand2_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    lblAux2.Left = 2700
    lblAux2.Caption = "Limpar"
    lblAux2.Visible = True

End Sub


Private Sub SSCommand3_Click()

'Verifica se ainda n�o existe este cadastro, para poder ent�o incluir
Call Procura_Fator
'Se j� existe, n�o pode incluir
If Fl_Achou_Fator = True Then
    MsgBox "Fator j� cadastrado!" & Chr(13) & _
           "Clique em 'Consultar' e altere diretamente no grid.", vbInformation, "Aten��o!"
    Exit Sub
End If
'Verifica preenchimento dos campos
Call Checa_Campos_Fator
'Se algum campo n�o estiver preenchido corretamente, sai da rotina
If Fl_Erro_Fator Then
    Exit Sub
End If

db.Parameters.Remove "Cod_ErrorPl": db.Parameters.Add "Cod_ErrorPl", 0, 2
db.Parameters.Remove "Txt_ErrorPl": db.Parameters.Add "Txt_ErrorPl", "", 2
db.Parameters.Remove "COD_LOJA": db.Parameters.Add "COD_LOJA", Trim(Mid(cboDeposito.Text, 1, 2)), 1
db.Parameters.Remove "COD_FORNECEDOR": db.Parameters.Add "COD_FORNECEDOR", Val(Trim(Mid(lblNomeFornec.Caption, 1, 3))), 1
db.Parameters.Remove "COD_GRUPO": db.Parameters.Add "COD_GRUPO", IIf(Trim(cboGrupo.Text) = "", "0", Trim(Mid(cboGrupo.Text, 1, 2))), 1
db.Parameters.Remove "COD_SUBGRUPO": db.Parameters.Add "COD_SUBGRUPO", IIf(Trim(cboSubGrupo.Text) = "", "0", Trim(Mid(cboSubGrupo.Text, 1, 2))), 1
db.Parameters.Remove "COD_DPK": db.Parameters.Add "COD_DPK", IIf(Trim(txtCodDpk.Text) = "", "0", Trim(txtCodDpk.Text)), 1
db.Parameters.Remove "TIPO_FATOR": db.Parameters.Add "TIPO_FATOR", Trim(Mid(cboTipoFator.Text, 1, 2)), 1
db.Parameters.Remove "FATOR": db.Parameters.Add "FATOR", Trim(txtFator.Text), 1
db.Parameters.Remove "SITUACAO": db.Parameters.Add "SITUACAO", Mid(cboSituacao.Text, 1, 1), 1

'Eduardo - 08/06/2007
db.Parameters.Remove "DT_INI": db.Parameters.Add "DT_INI", CDate(txtDtIniVig), 1
db.Parameters.Remove "DT_FIM": db.Parameters.Add "DT_FIM", CDate(txtDtFimVig), 1
db.Parameters.Remove "DESCRICAO": db.Parameters.Add "DESCRICAO", UCase(txtDescricao), 1

CmdSql = "BEGIN "


'Com a altera��o feita em 25/03/2002, a altera��o ser� feita com o duplo clique no grid
'e somente a inclus�o ser� feita pelo bot�o de OK
'Altera��o
'If Fl_Achou_Fator = True Then
    
'    If MsgBox("Confirma Altera��o ?", vbQuestion + vbYesNo + vbDefaultButton2, Msg_Padrao) = vbNo Then
'        Exit Sub
'    End If

 '   CmdSql = CmdSql & "Update COMPRAS.FATOR " & _
                      "Set FATOR = :FATOR, SITUACAO = :SITUACAO " & _
                      "Where COD_LOJA = :COD_LOJA " & _
                      "And COD_FORNECEDOR = :COD_FORNECEDOR " & _
                      "And COD_GRUPO = :COD_GRUPO " & _
                      "And COD_SUBGRUPO = :COD_SUBGRUPO " & _
                      "And COD_DPK = :COD_DPK " & _
                      "And TIPO_FATOR = :TIPO_FATOR;"

'Else  'Inclus�o
If Fl_Achou_Fator = False Then
    
    If MsgBox("Confirma Inclus�o ?", vbQuestion + vbYesNo + vbDefaultButton2, Msg_Padrao) = vbNo Then
        Exit Sub
    End If
    
    CmdSql = CmdSql & "Insert Into COMPRAS.FATOR " & _
                      "(COD_LOJA, COD_FORNECEDOR, COD_GRUPO, COD_SUBGRUPO, " & _
                       "COD_DPK, TIPO_FATOR, FATOR, SITUACAO, DT_INI_VIGENCIA, DT_FIM_VIGENCIA, DESCRICAO) " & _
                      "Values ( :COD_LOJA, :COD_FORNECEDOR, :COD_GRUPO, :COD_SUBGRUPO, " & _
                      ":COD_DPK, :TIPO_FATOR, :FATOR, :SITUACAO,TO_DATE(:DT_INI,'DD/MM/RR'), TO_DATE(:DT_FIM,'DD/MM/RR'), :DESCRICAO)"
End If

CmdSql = CmdSql & ";Commit; " & _
                  "Exception " & _
                  "When Others then " & _
                  "RollBack; " & _
                  ":Cod_ErrorPl := SQLCODE; " & _
                  ":Txt_ErrorPl := SQLERRM; " & _
                  "End; "

db.ExecuteSQL (CmdSql)

If IIf(IsNull(db.Parameters("Cod_ErrorPl")), 0, Val(db.Parameters("Cod_ErrorPl"))) <> 0 Then
    Screen.MousePointer = 0
    MsgBox "Erro - " & db.Parameters("Txt_ErrorPl") & ". Favor avisar Depto. de Sistemas!", vbExclamation, MSG_TIT
    Exit Sub
Else
    MsgBox "Inclus�o OK", vbInformation, Msg_Padrao
    Call Limpa_Campos_Fator
End If

End Sub

Private Sub SSCommand3_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

    lblAux2.Left = 2040
    lblAux2.Caption = "Confirmar"
    lblAux2.Visible = True

End Sub


Private Sub SSConfirmaDupli_Click()

    Call Checa_Campos_Tipo_Fator

    If Fl_Erro_Tipo_Fator Then
        Exit Sub
    End If

    db.Parameters.Remove "Cod_ErrorPl"
    db.Parameters.Remove "Txt_ErrorPl"
    db.Parameters.Remove "TIPO_FATOR"
    db.Parameters.Remove "DESCR_FATOR"

    db.Parameters.Add "Cod_ErrorPl", 0, 2
    db.Parameters.Add "Txt_ErrorPl", "", 2
    db.Parameters.Add "TIPO_FATOR", Val(Trim(txtTipoFator.Text)), 1
    db.Parameters.Add "DESCR_FATOR", Trim(txtdescrFator.Text), 1

    CmdSql = "begin "

    If Fl_Achou_Tipo_Fator = True Then 'Altera��o
    
        If MsgBox("Confirma Altera��o ?", vbQuestion + vbYesNo + vbDefaultButton2, Msg_Padrao) = vbNo Then
            Exit Sub
        End If
    
        CmdSql = CmdSql & "update COMPRAS.TIPO_FATOR " & _
                 "set DESCR_FATOR = :DESCR_FATOR " & _
                 "where TIPO_FATOR = :TIPO_FATOR;"
    
    Else 'Inclus�o
    
        If MsgBox("Confirma Inclus�o ?", vbQuestion + vbYesNo + vbDefaultButton2, Msg_Padrao) = vbNo Then
            Exit Sub
        End If
    
        CmdSql = CmdSql & "insert into COMPRAS.TIPO_FATOR (" & _
                 "TIPO_FATOR, DESCR_FATOR) values ( " & _
                 ":TIPO_FATOR, :DESCR_FATOR);"
    End If

    CmdSql = CmdSql & "Commit; " & _
             "Exception " & _
             "When Others then " & _
             "RollBack; " & _
             ":Cod_ErrorPl := SQLCODE; " & _
             ":Txt_ErrorPl := SQLERRM; " & _
             "End; "

    db.ExecuteSQL (CmdSql)

    If IIf(IsNull(db.Parameters("Cod_ErrorPl")), 0, Val(db.Parameters("Cod_ErrorPl"))) <> 0 Then
        Screen.MousePointer = 0
        MsgBox "Erro - " & db.Parameters("Txt_ErrorPl") & ". Favor avisar Depto. de Sistemas!", vbExclamation, MSG_TIT
        Exit Sub
    Else
        MsgBox "Altera��o / Inclus�o OK", vbInformation, Msg_Padrao
        Call Limpa_Campos_Tipo_Fator
    End If

End Sub

Private Sub SSConfirmaDupli_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

    lblAux1.Left = 150
    lblAux1.Caption = "Confirmar"
    lblAux1.Visible = True
    
End Sub


Private Sub SSConsDupli_Click()

    Screen.MousePointer = 11
    Fl_Fornec = False
    Fl_Itens = False
    Fl_Consul = True

    db.Parameters.Remove "COD_LOJA"
    db.Parameters.Remove "COD_FORNECEDOR"
    db.Parameters.Remove "COD_GRUPO"
    db.Parameters.Remove "COD_SUBGRUPO"
    db.Parameters.Remove "COD_DPK"
    db.Parameters.Remove "TIPO_FATOR"
    db.Parameters.Remove "FATOR"
    db.Parameters.Remove "SITUACAO"

    db.Parameters.Add "COD_LOJA", Val(Trim(Mid(cboDeposito.Text, 1, 2))), 1
    db.Parameters.Add "COD_FORNECEDOR", Val(Trim(Mid(lblNomeFornec.Caption, 1, 3))), 1
    db.Parameters.Add "COD_GRUPO", IIf(Trim(cboGrupo.Text) = "", "0", Trim(Mid(cboGrupo.Text, 1, 2))), 1
    db.Parameters.Add "COD_SUBGRUPO", IIf(Trim(cboSubGrupo.Text) = "", "0", Trim(Mid(cboSubGrupo.Text, 1, 2))), 1
    db.Parameters.Add "COD_DPK", IIf(Trim(txtCodDpk.Text) = "", "0", Trim(txtCodDpk.Text)), 1
    db.Parameters.Add "TIPO_FATOR", Trim(Mid(cboTipoFator.Text, 1, 2)), 1
    db.Parameters.Add "FATOR", Trim(txtFator.Text), 1
    db.Parameters.Add "SITUACAO", Mid(cboSituacao.Text, 1, 1), 1

    CmdSql = "select a.COD_FORNECEDOR ||' - ' || a.SIGLA FORNECEDOR, " & _
             "b.COD_LOJA ||' - '|| b.NOME_FANTASIA DEPOSITO, " & _
             "e.COD_GRUPO ||' - '|| e.DESC_GRUPO GRUPO, " & _
             "f.COD_SUBGRUPO ||' - '|| f.DESC_SUBGRUPO SUBGRUPO, " & _
             "g.TIPO_FATOR ||' - '|| g.DESCR_FATOR TIPO_FATOR, " & _
             "d.COD_LOJA, d.COD_FORNECEDOR, d.COD_DPK, d.FATOR, " & _
             "d.SITUACAO, c.COD_FABRICA, DT_INI_VIGENCIA, DT_FIM_VIGENCIA, DESCRICAO "
    CmdSql = CmdSql & "FROM " & _
             "fornecedor a, " & _
             "loja b, " & _
             "item_cadastro c, " & _
             "compras.fator d, " & _
             "grupo e, " & _
             "subgrupo f, " & _
             "compras.tipo_fator g "
    CmdSql = CmdSql & "where " & _
             "a.cod_fornecedor  = d.cod_fornecedor and " & _
             "b.cod_loja        = d.cod_loja       and " & _
             "c.cod_dpk(+)      = d.cod_dpk        and " & _
             "e.cod_grupo(+)    = d.cod_grupo      and " & _
             "f.cod_grupo(+)    = d.cod_grupo      and " & _
             "f.cod_subgrupo(+) = d.cod_subgrupo   and " & _
             "g.tipo_fator      = d.tipo_fator "
         
    If Trim(cboDeposito.Text) <> "" Then
        CmdSql = CmdSql & "and d.COD_LOJA = :COD_LOJA "
    End If

    If Trim(lblNomeFornec.Caption) <> "" Then
        CmdSql = CmdSql & "and d.COD_FORNECEDOR = :COD_FORNECEDOR "
    End If

    If Mid(cboGrupo.Text, 1, 2) <> 0 Then
        CmdSql = CmdSql & "and d.COD_GRUPO = :COD_GRUPO "
    End If

    If Mid(cboSubGrupo.Text, 1, 2) <> 0 Then
        CmdSql = CmdSql & "and d.COD_SUBGRUPO = :COD_SUBGRUPO "
    End If

    If Trim(txtCodDpk.Text) <> "" And Trim(txtCodDpk.Text) <> "0" Then
        CmdSql = CmdSql & "and d.COD_DPK = :COD_DPK "
    End If

    If Trim(cboTipoFator.Text) <> "" Then
        CmdSql = CmdSql & "and d.TIPO_FATOR = :TIPO_FATOR "
    End If

    If Trim(cboSituacao.Text) <> "" Then
        CmdSql = CmdSql & "and d.SITUACAO= :SITUACAO "
    End If

    CmdSql = CmdSql & "order by d.COD_LOJA, d.COD_FORNECEDOR"

    lblMensagem.Caption = "Carregando Fatores"
    lblMensagem.Visible = True
    
    Set db_CONS = db.createdynaset(CmdSql, 0&)

    If db_CONS.EOF = True Then
        Screen.MousePointer = 0
        lblMensagem.Visible = False
        MsgBox "N�o Existe Cadastro de Fator Para os Dados Informados!", vbCritical, Msg_Padrao
        Exit Sub
    End If
          
    'Chama o grid que est� escondido em um frame no formul�rio de manuten��o de natureza
    Grid1.Visible = True
    Grid1.Cols = 12
    Grid1.Top = 3030
    Grid1.Left = 70
    Grid1.Height = 1700
    Grid1.Width = 9300
    Grid1.Rows = db_CONS.RecordCount + 1
    Grid1.Row = 0
    Grid1.Col = 0
'    Grid1.Text = "LOJA"
    Grid1.Text = "CD"
    Grid1.Col = 1
    Grid1.Text = "FORN"
    Grid1.Col = 2
    Grid1.Text = "GRUPO"
    Grid1.Col = 3
    Grid1.Text = "SUBGRUPO"
    Grid1.Col = 4
    Grid1.Text = "DPK"
    Grid1.Col = 5
    Grid1.Text = "F�BRICA"
    Grid1.Col = 6
    Grid1.Text = "TIPO FATOR"
    Grid1.Col = 7
    Grid1.Text = "FATOR"
    Grid1.Col = 8
    Grid1.Text = "SITUA��O"
    
    Grid1.Col = 9
    Grid1.Text = "IN�CIO"
    Grid1.Col = 10
    Grid1.Text = "FIM"
    Grid1.Col = 11
    Grid1.Text = "DESCRI��O"
    
    
    Grid1.FixedRows = 1
'    Grid1.ColWidth(0) = 7 * 184
    Grid1.ColWidth(0) = 250
'    Grid1.ColWidth(1) = 9 * 184
    Grid1.ColWidth(1) = 500
'    Grid1.ColWidth(2) = 15 * 184
    Grid1.ColWidth(2) = 2000
'    Grid1.ColWidth(3) = 15 * 184
    Grid1.ColWidth(3) = 2000
    'eoliveira - CiT - 09/08/2012
    'ISA-175 - alterar tamanho do c�digo DPK
    Grid1.ColWidth(4) = 10 * 184
    Grid1.ColWidth(5) = 10 * 184
    Grid1.ColWidth(6) = 7 * 184
    Grid1.ColWidth(7) = 4 * 184
    Grid1.ColWidth(8) = 7 * 184

    Grid1.ColWidth(9) = 1200
    Grid1.ColWidth(10) = 1200
    Grid1.ColWidth(11) = 5000

    For i = 0 To 10
        Grid1.Col = i
        Grid1.FixedAlignment(i) = 0
    Next i

    frmCadastro.Refresh

    'Carrega o grid com as informa��es referentes � cidade
    i = 1

    While Not db_CONS.EOF
        'Me.Refresh
        Grid1.Row = i
        Grid1.Col = 0
'        Grid1.Text = Mid(db_CONS!DEPOSITO, 1, (InStr(db_CONS!DEPOSITO, " -") - 1))
        Grid1.Text = db_CONS!DEPOSITO
        Grid1.Col = 1
'        Grid1.Text = Mid(db_CONS!FORNECEDOR, 1, (InStr(db_CONS!FORNECEDOR, " -") - 1))
        Grid1.Text = db_CONS!FORNECEDOR
        Grid1.Col = 2
        Grid1.Text = db_CONS!GRUPO
        Grid1.Col = 3
        Grid1.Text = db_CONS!SUBGRUPO
        Grid1.Col = 4
        Grid1.Text = db_CONS!COD_DPK
        Grid1.Col = 5
        Grid1.Text = IIf(IsNull(db_CONS!COD_FABRICA) = True, "", db_CONS!COD_FABRICA)
        Grid1.Col = 6
        Grid1.Text = db_CONS!TIPO_FATOR
        Grid1.Col = 7
        Grid1.Text = Format(db_CONS!FATOR, "0.0000")
        Grid1.Col = 8
        Grid1.Text = IIf(db_CONS!situacao = 0, "0 - Ativo", "9 - Desativado")
        
'Eduardo - 08/06/2007
        Grid1.Col = 9
        Grid1.Text = "" & db_CONS!Dt_ini_Vigencia
        Grid1.Col = 10
        Grid1.Text = "" & db_CONS!dt_fim_vigencia
        Grid1.Col = 11
        Grid1.Text = "" & db_CONS!Descricao
        
        db_CONS.MoveNext
        i = i + 1

    Wend

    lblMensagem.Visible = False
    Screen.MousePointer = 0
    Grid1.SetFocus

End Sub

Private Sub SSConsDupli_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

    lblAux2.Left = 120
    lblAux2.Caption = "Consultar"
    lblAux2.Visible = True

End Sub


Private Sub SSPanel1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

    lblAux2.Visible = False

End Sub


Private Sub SSPanel2_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    lblAux1.Visible = False

End Sub


Private Sub SSSaiDupli_Click()
    
    Unload Me

End Sub








Private Sub SSSaiDupli_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

    lblAux2.Left = 4605
    lblAux2.Caption = "Voltar"
    lblAux2.Visible = True

End Sub

Private Sub Text1_Change()

End Sub

Private Sub txtCgc_GotFocus()
    
    Grid1.Visible = False
    txtCgc.SelStart = 0
    txtCgc.SelLength = Len(txtCgc.Text)
    
End Sub

Private Sub txtCgc_LostFocus()

    If txtCgc.Text = "" Then
        lblNomeFornec.Caption = ""
        Exit Sub
    End If
    
    Fl_Consul = False

    If Len(txtCgc.Text) >= 4 Then  ' para cgc
        
        db.Parameters.Remove "CGC"
        db.Parameters.Add "CGC", txtCgc.Text, 1
    
        CmdSql = "select COD_FORNECEDOR,COD_FORNECEDOR,SIGLA, CGC " & _
                 "from FORNECEDOR " & _
                 "where CGC =:CGC"
             
        Set db_CONS = db.createdynaset(CmdSql, 8&)
        
        If db_CONS.EOF = True Then
            MsgBox "CGC Fornecedor Inv�lido", vbCritical, Msg_Padrao
            txtCgc.SetFocus
            txtCgc.Text = ""
            Exit Sub
        End If
        
        txtCodDpk.Text = ""
        txtCodFabrica.Text = ""
        lblNomeFornec.Caption = db_CONS!COD_FORNECEDOR & " - " & db_CONS!sigla

    ElseIf Len(txtCgc.Text) <= 3 Then  ' para codigo
    
        db.Parameters.Remove "COD_FORNEC"
        db.Parameters.Add "COD_FORNEC", txtCgc.Text, 1
    
        CmdSql = "select COD_FORNECEDOR, SIGLA, CGC " & _
                 "from FORNECEDOR " & _
                 "where COD_FORNECEDOR =:COD_FORNEC"

        Set db_CONS = db.createdynaset(CmdSql, 8&)
        
        If db_CONS.EOF = True Then
            MsgBox "C�digo do Fornecedor Inv�lido", vbCritical, Msg_Padrao
            txtCgc.SetFocus
            txtCgc.Text = ""
            Exit Sub
        End If
        
        txtCgc.Text = db_CONS!cgc
        txtCodDpk.Text = ""
        txtCodFabrica.Text = ""
        lblNomeFornec.Caption = db_CONS!COD_FORNECEDOR & " - " & db_CONS!sigla
    Else
        MsgBox "O N�mero Digitado n�o Corresponde a um Cgc Nem a um C�digo de Fornecedor", vbCritical, Msg_Padrao
        txtCgc.SetFocus
        txtCgc.Text = ""
        lblNomeFornec.Caption = ""
    End If
    
End Sub
Private Sub txtcodDpk_GotFocus()

    txtCodDpk.SelStart = 0
    txtCodDpk.SelLength = Len(txtCodDpk.Text)
    
End Sub


Private Sub txtcodDpk_LostFocus()

    If Trim(txtCodDpk.Text) = "" Or Trim(txtCodDpk.Text) = "0" Then
        txtCodDpk.Text = 0
        Exit Sub
    End If

    Screen.MousePointer = 11
    db.Parameters.Remove "COD_DPK"

    db.Parameters.Add "COD_DPK", Trim(txtCodDpk.Text), 1

    CmdSql = "select COD_FORNECEDOR,COD_FABRICA, COD_GRUPO, COD_SUBGRUPO " & _
             "from ITEM_CADASTRO " & _
             "where COD_DPK = :COD_DPK"
         
    Set db_CONS = db.createdynaset(CmdSql, 8&)

    If db_CONS.EOF = True Then
        Screen.MousePointer = 0
        MsgBox "C�digo do DPK Digitado n�o Localizado", vbCritical, Msg_Padrao
        txtCodDpk.Text = ""
        txtCodDpk.SetFocus
        Exit Sub
    End If

    If Trim(cboGrupo.Text) <> "0" Then
    
        If Trim(Mid(cboGrupo.Text, 1, 2)) <> db_CONS!COD_GRUPO Then
            Screen.MousePointer = 0
            MsgBox "Grupo Escolhido Diferente do Grupo do DPK Selecionado", vbCritical, Msg_Padrao
            txtCodDpk.Text = ""
            txtCodDpk.SetFocus
            Exit Sub
        End If

    ElseIf Trim(cboSubGrupo.Text) <> "0" Then
        
        If Trim(Mid(cboSubGrupo.Text, 1, 2)) <> db_CONS!COD_SUBGRUPO Then
            Screen.MousePointer = 0
            MsgBox "Subgrupo Escolhido Diferente do Subrupo do DPK Selecionado ! ", vbCritical, Msg_Padrao
            txtCodDpk.Text = ""
            txtCodDpk.SetFocus
            Exit Sub
        End If

    End If

    If Val(Trim(Mid(lblNomeFornec, 1, 3))) <> db_CONS!COD_FORNECEDOR Then
        Screen.MousePointer = 0
        MsgBox "Fornecedor do DPK Escolhido Diferente do Informado", vbCritical, Msg_Padrao
        txtCodDpk.Text = ""
        txtCodDpk.SetFocus
    End If

    txtCodFabrica.Text = db_CONS!COD_FABRICA
    Screen.MousePointer = 0

End Sub


Private Sub txtDescAlt_LostFocus()
    If Trim(txtDescAlt) = "" Then
        MsgBox "Informe a Descri��o.", vbInformation, "Aten��o"
        txtDescAlt.SetFocus
        Exit Sub
    End If
    Salvar_Descricao
End Sub

Private Sub txtdescrfator_KeyPress(KeyAscii As Integer)

    KeyAscii = Maiusculo(KeyAscii)

End Sub



Private Sub txtDtFimAlt_KeyPress(KeyAscii As Integer)
    DATA KeyAscii, txtDtFimAlt
End Sub

Private Sub txtDtFimAlt_LostFocus()
    If CDate(txtDtFimAlt) > CDate(txtDtFimAlt) Then
        MsgBox "A data de t�rmino n�o pode ser menor que a data de in�cio.", vbInformation, "Aten��o"
        txtDtFimAlt.SetFocus
        Exit Sub
    End If
    
    If Trim(txtDtFimAlt) = "" Or Trim(txtDtFimAlt) = "" Then
        MsgBox "Como a data de in�cio ou a data de fim est� em branco, os dados n�o ser�o alterados.", vbInformation, "Aten��o"
    End If
    
    If (Trim(txtDtIniAlt.Tag) = "" Or Trim(txtDtFimAlt.Tag) = "") Then
        Salvar_Periodo
    ElseIf (Trim(txtDtIniAlt.Tag) <> "" And CDate(txtDtIniAlt.Tag) <> txtDtIniAlt) Or _
       (Trim(txtDtFimAlt.Tag) <> "" And CDate(txtDtFimAlt.Tag) <> CDate(txtDtFimAlt)) Then
        Salvar_Periodo
    Else
        lblDtInicioAlt.Visible = False
        lblDtFimAlt.Visible = False
        txtDtIniAlt.Visible = False
        txtDtFimAlt.Visible = False
    End If
End Sub

Private Sub txtDtFimVig_KeyPress(KeyAscii As Integer)
    DATA KeyAscii, txtDtFimVig
End Sub


Private Sub txtDtIniAlt_KeyPress(KeyAscii As Integer)
    DATA KeyAscii, txtDtIniAlt
End Sub

Private Sub txtDtIniAlt_LostFocus()

    If Trim(txtDtFimAlt.Text) <> "" Then
        If CDate(txtDtIniAlt) > CDate(txtDtFimAlt) Then
            MsgBox "A data de in�cio n�o pode ser maior que a data de t�rmino.", vbInformation, "Aten��o"
            txtDtIniAlt = txtDtIniAlt.Tag
            Exit Sub
        End If
    End If
    
    'If Trim(txtDtIniAlt) = "" Or Trim(txtDtFimAlt) = "" Then
    '    MsgBox "Como a data de in�cio ou a data de fim est� em branco, os dados n�o ser�o alterados.", vbInformation, "Aten��o"
    '    Exit Sub
    'End If
    
    'txtDtFimAlt.SetFocus
        
End Sub

Private Sub txtDtIniVig_KeyPress(KeyAscii As Integer)
    DATA KeyAscii, txtDtIniVig
End Sub

Private Sub txtFat_Click()

txtFat = ""

End Sub

Private Sub txtFat_KeyPress(KeyAscii As Integer)

KeyAscii = Valor(KeyAscii, txtFat)

End Sub


Private Sub txtFat_LostFocus()
Dim vAux As String

If Not IsNumeric(txtFat) Then
    MsgBox "Fator inv�lido", vbInformation, "Aten��o"
    txtFat.SetFocus
    Exit Sub
End If
'Passa para o grid, a informa��o digitada no campo texto
Grid1.Row = vGuardaLin
Grid1.Col = 7
Grid1.Text = Format(txtFat, "0.0000")

'Passa as iforma��es da linha do grid e armazena nas vari�veis de bind
Grid1.Col = 0
vAux = Trim(Mid(Grid1, 1, (InStr(1, Grid1.Text, " -") - 1)))
db.Parameters.Remove "COD_LOJA": db.Parameters.Add "COD_LOJA", Val(vAux), 1
Grid1.Col = 1
vAux = Trim(Mid(Grid1, 1, (InStr(1, Grid1.Text, " -") - 1)))
db.Parameters.Remove "COD_FORNECEDOR": db.Parameters.Add "COD_FORNECEDOR", Val(vAux), 1
Grid1.Col = 2
vAux = Trim(Mid(Grid1, 1, (InStr(1, Grid1.Text, " -") - 1)))
db.Parameters.Remove "COD_GRUPO": db.Parameters.Add "COD_GRUPO", Val(IIf(vAux = "", "0", vAux)), 1
Grid1.Col = 3
vAux = Trim(Mid(Grid1, 1, (InStr(1, Grid1.Text, " -") - 1)))
db.Parameters.Remove "COD_SUBGRUPO": db.Parameters.Add "COD_SUBGRUPO", Val(IIf(vAux = "", "0", vAux)), 1
Grid1.Col = 4
db.Parameters.Remove "COD_DPK": db.Parameters.Add "COD_DPK", Val(IIf(Trim(Grid1) = "", "0", Trim(Grid1))), 1
Grid1.Col = 6
vAux = Trim(Mid(Grid1, 1, (InStr(1, Grid1.Text, " -") - 1)))
db.Parameters.Remove "TIPO_FATOR": db.Parameters.Add "TIPO_FATOR", Val(vAux), 1
Grid1.Col = 7
db.Parameters.Remove "FATOR": db.Parameters.Add "FATOR", CDbl(IIf(Trim(Grid1) = "", 0, Trim(Grid1))), 1
Grid1.Col = 8
vAux = Trim(Mid(Grid1, 1, (InStr(1, Grid1.Text, " -") - 1)))
db.Parameters.Remove "SITUACAO": db.Parameters.Add "SITUACAO", Val(vAux), 1

'Eduardo - 29/06/2007
Grid1.Col = 9
vAux = Grid1
db.Parameters.Remove "DT_INI": db.Parameters.Add "DT_INI", vAux, 1

Grid1.Col = 10
vAux = Grid1
db.Parameters.Remove "DT_FIM": db.Parameters.Add "DT_FIM", vAux, 1

db.Parameters.Remove "Cod_ErrorPl": db.Parameters.Add "Cod_ErrorPl", 0, 2
db.Parameters.Remove "Txt_ErrorPl": db.Parameters.Add "Txt_ErrorPl", "", 2
    
CmdSql = "BEGIN "
CmdSql = CmdSql & "Update COMPRAS.FATOR " & _
                  "Set FATOR = :FATOR, SITUACAO = :SITUACAO " & _
                  "Where COD_LOJA = :COD_LOJA " & _
                  "And COD_FORNECEDOR = :COD_FORNECEDOR " & _
                  "And COD_GRUPO = :COD_GRUPO " & _
                  "And COD_SUBGRUPO = :COD_SUBGRUPO " & _
                  "And COD_DPK = :COD_DPK " & _
                  "And TIPO_FATOR = :TIPO_FATOR " & _
                  "And DT_INI_VIGENCIA = TO_DATE(:DT_INI,'DD/MM/RR') " & _
                  "And DT_FIM_VIGENCIA = TO_DATE(:DT_FIM,'DD/MM/RR'); "
CmdSql = CmdSql & "Commit; " & _
                  "Exception " & _
                  "When Others then " & _
                  "RollBack; " & _
                  ":Cod_ErrorPl := SQLCODE; " & _
                  ":Txt_ErrorPl := SQLERRM; " & _
                  "End; "

db.ExecuteSQL (CmdSql)

Insert_Log_Fator db.Parameters("Fator").Value, db.Parameters("Situacao").Value, db.Parameters("DT_INI").Value, db.Parameters("DT_FIM").Value

If IIf(IsNull(db.Parameters("Cod_ErrorPl")), 0, Val(db.Parameters("Cod_ErrorPl"))) <> 0 Then
    Screen.MousePointer = 0
    MsgBox "Erro - " & db.Parameters("Txt_ErrorPl") & ". Favor avisar Depto. de Sistemas!", vbExclamation, MSG_TIT
    Exit Sub
'Else
'    MsgBox "Altera��o OK", vbInformation, Msg_Padrao
'    Call Limpa_Campos_Fator
End If

lblFatorAlt.Visible = False
txtFat.Visible = False
Grid1.SetFocus

End Sub


Private Sub txtFator_GotFocus()

    txtFator.SelStart = 0
    txtFator.SelLength = Len(txtFator.Text)
    
End Sub


Private Sub txtFator_KeyPress(KeyAscii As Integer)

    KeyAscii = Valor(KeyAscii, txtFator)

End Sub


Private Sub txtFator_LostFocus()

    If Trim(txtFator.Text) = "" Then
        Exit Sub
    End If

    txtFator.Text = Format(txtFator.Text, "0.0000")
    
    If Len(txtFator.Text) > 7 Or CDbl(txtFator.Text) >= 100 Then
        MsgBox "Fator Incorreto, Favor Verificar!", vbCritical, Msg_Padrao
        txtFator.Text = ""
        txtFator.SetFocus
    End If
    
End Sub

Private Sub txtSit_Click()

txtSit = ""

End Sub

Private Sub txtSit_KeyPress(KeyAscii As Integer)
'    KeyAscii = Numerico(KeyAscii)

    If KeyAscii <> 48 And KeyAscii <> "57" Then
        KeyAscii = 0
    End If
    
'    Numerico = KeyAscii



End Sub


Private Sub txtSit_LostFocus()
Dim vAux As String

'Passa para o grid, a informa��o digitada no campo texto
Grid1.Row = vGuardaLin
Grid1.Col = 8

If txtSit = "0" Then
    txtSit = "0 - Ativo"
ElseIf txtSit = "9" Then
    txtSit = "9 - Desativado"
Else
    lblSituacaoAlt.Visible = False
    txtSit.Visible = False
    Exit Sub
End If

Grid1.Text = txtSit

'Passa as iforma��es da linha do grid e armazena nas vari�veis de bind
Grid1.Col = 0
vAux = Trim(Mid(Grid1, 1, (InStr(1, Grid1.Text, " -") - 1)))
db.Parameters.Remove "COD_LOJA": db.Parameters.Add "COD_LOJA", Val(vAux), 1
Grid1.Col = 1
vAux = Trim(Mid(Grid1, 1, (InStr(1, Grid1.Text, " -") - 1)))
db.Parameters.Remove "COD_FORNECEDOR": db.Parameters.Add "COD_FORNECEDOR", Val(vAux), 1
Grid1.Col = 2
vAux = Trim(Mid(Grid1, 1, (InStr(1, Grid1.Text, " -") - 1)))
db.Parameters.Remove "COD_GRUPO": db.Parameters.Add "COD_GRUPO", Val(IIf(vAux = "", "0", vAux)), 1
Grid1.Col = 3
vAux = Trim(Mid(Grid1, 1, (InStr(1, Grid1.Text, " -") - 1)))
db.Parameters.Remove "COD_SUBGRUPO": db.Parameters.Add "COD_SUBGRUPO", Val(IIf(vAux = "", "0", vAux)), 1
Grid1.Col = 4
db.Parameters.Remove "COD_DPK": db.Parameters.Add "COD_DPK", Val(IIf(Trim(Grid1) = "", "0", Trim(Grid1))), 1
Grid1.Col = 6
vAux = Trim(Mid(Grid1, 1, (InStr(1, Grid1.Text, " -") - 1)))
db.Parameters.Remove "TIPO_FATOR": db.Parameters.Add "TIPO_FATOR", Val(vAux), 1
Grid1.Col = 7
db.Parameters.Remove "FATOR": db.Parameters.Add "FATOR", CDbl(Trim(Grid1)), 1
Grid1.Col = 8
vAux = Trim(Mid(Grid1, 1, (InStr(1, Grid1.Text, " -") - 1)))
db.Parameters.Remove "SITUACAO": db.Parameters.Add "SITUACAO", Val(vAux), 1

'Eduardo - 29/06/2007
Grid1.Col = 9
vAux = Grid1
db.Parameters.Remove "DT_INI": db.Parameters.Add "DT_INI", IIf(vAux = "", "NULL", vAux), 1

Grid1.Col = 10
vAux = Grid1
db.Parameters.Remove "DT_FIM": db.Parameters.Add "DT_FIM", IIf(vAux = "", "NULL", vAux), 1

db.Parameters.Remove "Cod_ErrorPl": db.Parameters.Add "Cod_ErrorPl", 0, 2
db.Parameters.Remove "Txt_ErrorPl": db.Parameters.Add "Txt_ErrorPl", "", 2
    
CmdSql = "BEGIN "
CmdSql = CmdSql & "Update COMPRAS.FATOR " & _
                  "Set FATOR = :FATOR, SITUACAO = :SITUACAO " & _
                  "Where COD_LOJA = :COD_LOJA " & _
                  "And COD_FORNECEDOR = :COD_FORNECEDOR " & _
                  "And COD_GRUPO = :COD_GRUPO " & _
                  "And COD_SUBGRUPO = :COD_SUBGRUPO " & _
                  "And COD_DPK = :COD_DPK " & _
                  "And TIPO_FATOR = :TIPO_FATOR "
                
                If vAux <> "" Then
                    CmdSql = CmdSql & "And DT_INI_VIGENCIA = TO_DATE(:DT_INI,'DD/MM/RR') " & _
                                     "And DT_FIM_VIGENCIA = TO_DATE(:DT_FIM,'DD/MM/RR') "
                End If
                
CmdSql = CmdSql & "; Commit; " & _
                  "Exception " & _
                  "When Others then " & _
                  "RollBack; " & _
                  ":Cod_ErrorPl := SQLCODE; " & _
                  ":Txt_ErrorPl := SQLERRM; " & _
                  "End; "

db.ExecuteSQL (CmdSql)

If Val(db.Parameters("COD_ERRORPL").Value) <> 0 Then
    MsgBox "Codigo:" & db.Parameters("cod_errorpl").Value & vbCrLf & "Descri��o:" & db.Parameters("Txt_Errorpl").Value
End If
    

Insert_Log_Fator db.Parameters("Fator").Value, db.Parameters("Situacao").Value, db.Parameters("DT_INI").Value, db.Parameters("DT_FIM").Value

If IIf(IsNull(db.Parameters("Cod_ErrorPl")), 0, Val(db.Parameters("Cod_ErrorPl"))) <> 0 Then
    Screen.MousePointer = 0
    MsgBox "Erro - " & db.Parameters("Txt_ErrorPl") & ". Favor avisar Depto. de Sistemas!", vbExclamation, MSG_TIT
    Exit Sub
'Else
'    MsgBox "Altera��o OK", vbInformation, Msg_Padrao
'    Call Limpa_Campos_Fator
End If

lblSituacaoAlt.Visible = False
txtSit.Visible = False
Grid1.SetFocus

End Sub


Private Sub txtTipoFator_GotFocus()

    txtTipoFator.SelStart = 0
    txtTipoFator.SelLength = Len(txtTipoFator.Text)

End Sub

Private Sub txtTipoFator_KeyPress(KeyAscii As Integer)

    KeyAscii = Numerico(KeyAscii)

End Sub


Private Sub txtTipoFator_LostFocus()

    If Trim(txtTipoFator.Text) = "" Then
        Exit Sub
    End If

    db.Parameters.Remove "TIPO_FATOR"
    db.Parameters.Add "TIPO_FATOR", Trim(txtTipoFator.Text), 1

    CmdSql = "select * " & _
             "from COMPRAS.TIPO_FATOR " & _
             "where TIPO_FATOR = :TIPO_FATOR"
         
    Set db_CONS = db.createdynaset(CmdSql, 8&)

    If db_CONS.EOF = True Then
        Fl_Achou_Tipo_Fator = False
        txtdescrFator.Text = ""
        Exit Sub
    End If

    Fl_Achou_Tipo_Fator = True
    txtdescrFator.Text = db_CONS!DESCR_FATOR

End Sub


Sub Salvar_Periodo()

    Dim vAux As String
    
    'Passa para o grid, a informa��o digitada no campo texto
    Grid1.Row = vGuardaLin
    
    Grid1.Col = 9
    Grid1.Text = txtDtIniAlt
    
    Grid1.Col = 10
    Grid1.Text = txtDtFimAlt
    
    'Passa as iforma��es da linha do grid e armazena nas vari�veis de bind
    Grid1.Col = 0
    vAux = Trim(Mid(Grid1, 1, (InStr(1, Grid1.Text, " -") - 1)))
    db.Parameters.Remove "COD_LOJA": db.Parameters.Add "COD_LOJA", Val(vAux), 1
    Grid1.Col = 1
    vAux = Trim(Mid(Grid1, 1, (InStr(1, Grid1.Text, " -") - 1)))
    db.Parameters.Remove "COD_FORNECEDOR": db.Parameters.Add "COD_FORNECEDOR", Val(vAux), 1
    Grid1.Col = 2
    vAux = Trim(Mid(Grid1, 1, (InStr(1, Grid1.Text, " -") - 1)))
    db.Parameters.Remove "COD_GRUPO": db.Parameters.Add "COD_GRUPO", Val(IIf(vAux = "", "0", vAux)), 1
    Grid1.Col = 3
    vAux = Trim(Mid(Grid1, 1, (InStr(1, Grid1.Text, " -") - 1)))
    db.Parameters.Remove "COD_SUBGRUPO": db.Parameters.Add "COD_SUBGRUPO", Val(IIf(vAux = "", "0", vAux)), 1
    Grid1.Col = 4
    db.Parameters.Remove "COD_DPK": db.Parameters.Add "COD_DPK", Val(IIf(Trim(Grid1) = "", "0", Trim(Grid1))), 1
    Grid1.Col = 6
    vAux = Trim(Mid(Grid1, 1, (InStr(1, Grid1.Text, " -") - 1)))
    db.Parameters.Remove "TIPO_FATOR": db.Parameters.Add "TIPO_FATOR", Val(vAux), 1
    
    Grid1.Col = 9
    db.Parameters.Remove "DT_INI": db.Parameters.Add "DT_INI", Grid1, 1
    db.Parameters.Remove "DT_INI_ANT": db.Parameters.Add "DT_INI_ANT", vDtIniAnt, 1
    
    Grid1.Col = 10
    db.Parameters.Remove "DT_FIM": db.Parameters.Add "DT_FIM", Grid1, 1
    db.Parameters.Remove "DT_FIM_ANT": db.Parameters.Add "DT_FIM_ANT", vDtFimAnt, 1
    
    db.Parameters.Remove "Cod_ErrorPl": db.Parameters.Add "Cod_ErrorPl", 0, 2
    db.Parameters.Remove "Txt_ErrorPl": db.Parameters.Add "Txt_ErrorPl", "", 2
        
    CmdSql = "BEGIN "
    CmdSql = CmdSql & "Update COMPRAS.FATOR " & _
                      "Set DT_INI_VIGENCIA = to_date(:DT_INI,'dd/mm/rr'), DT_FIM_VIGENCIA = to_date(:DT_FIM,'dd/mm/rr') " & _
                      "Where COD_LOJA = :COD_LOJA " & _
                      "And COD_FORNECEDOR = :COD_FORNECEDOR " & _
                      "And COD_GRUPO = :COD_GRUPO " & _
                      "And COD_SUBGRUPO = :COD_SUBGRUPO " & _
                      "And COD_DPK = :COD_DPK " & _
                      "And TIPO_FATOR = :TIPO_FATOR "
    If Trim(vDtIniAnt) <> "" Then
        CmdSql = CmdSql & "And DT_INI_VIGENCIA = TO_DATE(:DT_INI_ANT,'DD/MM/RR') "
    End If
    If Trim(vDtFimAnt) <> "" Then
        CmdSql = CmdSql & "And DT_FIM_VIGENCIA = TO_DATE(:DT_FIM_ANT,'DD/MM/RR') "
    End If
    CmdSql = CmdSql & "; Commit; " & _
                      "Exception " & _
                      "When Others then " & _
                      "RollBack; " & _
                      ":Cod_ErrorPl := SQLCODE; " & _
                      ":Txt_ErrorPl := SQLERRM; " & _
                      "End; "
    
    db.ExecuteSQL (CmdSql)
    
    If IIf(IsNull(db.Parameters("Cod_ErrorPl")), 0, Val(db.Parameters("Cod_ErrorPl"))) <> 0 Then
        Screen.MousePointer = 0
        MsgBox "Erro - " & db.Parameters("Txt_ErrorPl") & ". Favor avisar Depto. de Sistemas!", vbExclamation, MSG_TIT
        Exit Sub
    End If
    
    
    lblDtInicioAlt.Visible = False
    lblDtFimAlt.Visible = False
    txtDtIniAlt.Visible = False
    txtDtFimAlt.Visible = False
    Grid1.SetFocus
    
End Sub

Sub Salvar_Descricao()

    Dim vAux As String
    
    'Passa para o grid, a informa��o digitada no campo texto
    Grid1.Row = vGuardaLin
    
    Grid1.Col = 11
    Grid1.Text = txtDescAlt
    
    'Passa as iforma��es da linha do grid e armazena nas vari�veis de bind
    Grid1.Col = 0
    vAux = Trim(Mid(Grid1, 1, (InStr(1, Grid1.Text, " -") - 1)))
    db.Parameters.Remove "COD_LOJA": db.Parameters.Add "COD_LOJA", Val(vAux), 1
    Grid1.Col = 1
    vAux = Trim(Mid(Grid1, 1, (InStr(1, Grid1.Text, " -") - 1)))
    db.Parameters.Remove "COD_FORNECEDOR": db.Parameters.Add "COD_FORNECEDOR", Val(vAux), 1
    Grid1.Col = 2
    vAux = Trim(Mid(Grid1, 1, (InStr(1, Grid1.Text, " -") - 1)))
    db.Parameters.Remove "COD_GRUPO": db.Parameters.Add "COD_GRUPO", Val(IIf(vAux = "", "0", vAux)), 1
    Grid1.Col = 3
    vAux = Trim(Mid(Grid1, 1, (InStr(1, Grid1.Text, " -") - 1)))
    db.Parameters.Remove "COD_SUBGRUPO": db.Parameters.Add "COD_SUBGRUPO", Val(IIf(vAux = "", "0", vAux)), 1
    Grid1.Col = 4
    db.Parameters.Remove "COD_DPK": db.Parameters.Add "COD_DPK", Val(IIf(Trim(Grid1) = "", "0", Trim(Grid1))), 1
    Grid1.Col = 6
    vAux = Trim(Mid(Grid1, 1, (InStr(1, Grid1.Text, " -") - 1)))
    db.Parameters.Remove "TIPO_FATOR": db.Parameters.Add "TIPO_FATOR", Val(vAux), 1
    
    Grid1.Col = 9
    db.Parameters.Remove "DT_INI": db.Parameters.Add "DT_INI", Grid1, 1
    db.Parameters.Remove "DT_INI_ANT": db.Parameters.Add "DT_INI_ANT", vDtIniAnt, 1
    
    Grid1.Col = 10
    db.Parameters.Remove "DT_FIM": db.Parameters.Add "DT_FIM", Grid1, 1
    db.Parameters.Remove "DT_FIM_ANT": db.Parameters.Add "DT_FIM_ANT", vDtFimAnt, 1
    
    Grid1.Col = 11
    db.Parameters.Remove "DESCRICAO": db.Parameters.Add "DESCRICAO", Grid1, 1
    
    db.Parameters.Remove "Cod_ErrorPl": db.Parameters.Add "Cod_ErrorPl", 0, 2
    db.Parameters.Remove "Txt_ErrorPl": db.Parameters.Add "Txt_ErrorPl", "", 2
        
    CmdSql = "BEGIN "
    CmdSql = CmdSql & "Update COMPRAS.FATOR " & _
                      "Set DESCRICAO = :DESCRICAO " & _
                      "Where COD_LOJA = :COD_LOJA " & _
                      "And COD_FORNECEDOR = :COD_FORNECEDOR " & _
                      "And COD_GRUPO = :COD_GRUPO " & _
                      "And COD_SUBGRUPO = :COD_SUBGRUPO " & _
                      "And COD_DPK = :COD_DPK " & _
                      "And TIPO_FATOR = :TIPO_FATOR " & _
                      "And DT_INI_VIGENCIA = TO_DATE(:DT_INI_ANT,'DD/MM/RR') " & _
                      "And DT_FIM_VIGENCIA = TO_DATE(:DT_FIM_ANT,'DD/MM/RR');"
                      CmdSql = CmdSql & "Commit; " & _
                      "Exception " & _
                      "When Others then " & _
                      "RollBack; " & _
                      ":Cod_ErrorPl := SQLCODE; " & _
                      ":Txt_ErrorPl := SQLERRM; " & _
                      "End; "
    
    db.ExecuteSQL (CmdSql)
    
    If IIf(IsNull(db.Parameters("Cod_ErrorPl")), 0, Val(db.Parameters("Cod_ErrorPl"))) <> 0 Then
        Screen.MousePointer = 0
        MsgBox "Erro - " & db.Parameters("Txt_ErrorPl") & ". Favor avisar Depto. de Sistemas!", vbExclamation, MSG_TIT
        Exit Sub
    End If
    
    lblDescricaoAlt.Visible = False
    txtDescAlt.Visible = False
    Grid1.SetFocus
    
End Sub

'************************************************************************
'Eduardo - 29/06/2007
'Uso os parametros que foram inseridos na chamada do LostFocus dos campos
'Por isso nao preciso declarar todos os parametros nesta sub
'************************************************************************
Sub Insert_Log_Fator(pFatorNovo As String, pSituacaoNovo As String, pDtIniVigNovo As String, pDtFimVigNovo As String)
    
    Dim vSql As String
    
    db.Parameters.Remove "FatorAnt": db.Parameters.Add "FatorAnt", vFatorAnt, 1
    db.Parameters.Remove "FatorNovo": db.Parameters.Add "FatorNovo", pFatorNovo, 1
    
    db.Parameters.Remove "SituacaoAnt": db.Parameters.Add "SituacaoAnt", Val(vSituacaoAnt), 1
    db.Parameters.Remove "SituacaoNovo": db.Parameters.Add "SituacaoNovo", pSituacaoNovo, 1
        
    db.Parameters.Remove "DtIniVigAnt": db.Parameters.Add "DtIniVigAnt", IIf(vDtIniAnt = "", "NULL", vDtIniAnt), 1
    db.Parameters.Remove "DtIniVigNovo": db.Parameters.Add "DtIniVigNovo", IIf(pDtIniVigNovo = "", "NULL", pDtIniVigNovo), 1
        
    db.Parameters.Remove "DtFimVigAnt": db.Parameters.Add "DtFimVigAnt", IIf(vDtFimAnt = "", "NULL", vDtFimAnt), 1
    db.Parameters.Remove "DtFimVigNovo": db.Parameters.Add "DtFimVigNovo", IIf(pDtFimVigNovo = "", "NULL", pDtFimVigNovo), 1
    
    db.Parameters.Remove "CodUsuario": db.Parameters.Add "CodUsuario", vCodUser_Intranet, 1
        
    db.Parameters.Remove "Cod_ErrorPl": db.Parameters.Add "Cod_ErrorPl", 0, 2
    db.Parameters.Remove "Txt_ErrorPl": db.Parameters.Add "Txt_ErrorPl", "", 2
        
    vSql = "BEGIN "
    
    If vDtFimAnt = "" Then
        vSql = vSql & "Insert into COMPRAS.LOG_FATOR " & _
                      " values (:COD_LOJA, :COD_FORNECEDOR, :COD_GRUPO, :COD_SUBGRUPO, :COD_DPK, :TIPO_FATOR, TO_DATE(TO_CHAR(SYSDATE,'DD/MM/RRRR HH24:MI:SS'),'DD/MM/RRRR HH24:MI:SS'), :FatorAnt, :FatorNovo, :SituacaoAnt, :SituacaoNovo, NULL, NULL, NULL, NULL, :CodUsuario) ;"
    Else
        vSql = vSql & "Insert into COMPRAS.LOG_FATOR " & _
                      " values (:COD_LOJA, :COD_FORNECEDOR, :COD_GRUPO, :COD_SUBGRUPO, :COD_DPK, :TIPO_FATOR, TO_DATE(TO_CHAR(SYSDATE,'DD/MM/RRRR HH24:MI:SS'),'DD/MM/RRRR HH24:MI:SS'), :FatorAnt, :FatorNovo, :SituacaoAnt, :SituacaoNovo, TO_DATE(:DtIniVigAnt,'DD/MM/RR'), TO_DATE(:DtIniVigNovo,'DD/MM/RR'), TO_DATE(:DtFimVigAnt,'DD/MM/RR'), TO_DATE(:DtFimVigNovo,'DD/MM/RR'), :CodUsuario) ;"
    End If
    vSql = vSql & "Commit; " & _
                      "Exception " & _
                      "When Others then " & _
                      "RollBack; " & _
                      ":Cod_ErrorPl := SQLCODE; " & _
                      ":Txt_ErrorPl := SQLERRM; " & _
                      "End; "
    
    db.ExecuteSQL (vSql)
    
    If Val(db.Parameters("Cod_ErrorPl").Value) <> 0 Then
        MsgBox db.Parameters("Txt_ErrorPl").Value
    End If
    
End Sub
