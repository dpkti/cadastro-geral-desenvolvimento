Attribute VB_Name = "MOD_PAD"
Public vAuxSenha As String
Public vCodUser_Intranet As Long
Public vUser_Intranet As String
    
Global Const ORATYPE_CURSOR = 102
Global Const ORADYN_NO_BLANKSTRIP = &H2&
Global Const ORAPARM_INPUT = 1              'CONSTANTE DE BIND INPUT
Global Const ORAPARM_OUTPUT = 2             'CONSTANTE DE BIND OUTPUT
Global Const ORAPARM_BOTH = 3               'CONSTANTE DE BIND INPUT/OUTPUT
Global Const ORATYPE_NUMBER = 2
    
Global NomeArq As String    ' nome do arquivo
Global dbOra  As Object         'Para Abrir o Bco.
Global db As Object             'Database
Global db_CONS As Object        'Variavel Bco. de Consulta
Global cod_errora As Integer    'Cod. Erro PL-SQL
Global txt_errora As String     'Texto Erro PL-SQL
Global CmdSql As String         'String para o SQL
Global Dt_Entrada As Date
Global Msg_Padrao As String   ' variavel para mensagem padrao de msgbox
Global Tipo_Trabalho As String

Function Letras(ByVal KeyAscii As Integer) As Integer
    
    If KeyAscii = 8 Then    'BACKSPACE
        Letras = KeyAscii
        Exit Function
    ElseIf Chr(KeyAscii) >= "0" And Chr(KeyAscii) <= "9" Then
        KeyAscii = 0
        Beep
    ElseIf Chr$(KeyAscii) = "'" Or Chr$(KeyAscii) = "," Or Chr$(KeyAscii) = ";" Then
        KeyAscii = 0
        Beep
    Else
        KeyAscii = Asc(UCase(Chr$(KeyAscii)))
    End If
    
    Letras = KeyAscii
    
End Function

Sub DATA_YYYY(ByRef KeyAscii, ByRef txtCampo)
    
    On Error GoTo TrataErro

    Dim bTam As Byte    'tamanho do campo JA digitado
    Dim strData As String
    Dim bKey As Byte
    
    bTam = Len(txtCampo.Text)
    
    If KeyAscii = 8 And bTam > 0 Then 'backspace
        
        If Mid$(txtCampo.Text, bTam) = "/" Then
            txtCampo.Text = Mid$(txtCampo.Text, 1, bTam - 2)
        Else
            txtCampo.Text = Mid$(txtCampo.Text, 1, bTam - 1)
        End If
            
    ElseIf Chr$(KeyAscii) >= "0" And Chr$(KeyAscii) <= "9" Then
        
        If bTam = 1 Then
            strData = txtCampo.Text & Chr$(KeyAscii)
            If CInt(strData) < 1 Or CInt(strData > 31) Then
                Beep
            Else
                txtCampo.Text = txtCampo.Text & Chr$(KeyAscii) & "/"
            End If
            
        ElseIf bTam = 4 Then
            strData = Mid$(txtCampo.Text, 4) & Chr$(KeyAscii)
            If CInt(strData) < 1 Or CInt(strData > 12) Then
                Beep
            Else
                txtCampo.Text = txtCampo.Text & Chr$(KeyAscii) & "/"
            End If
        
        ElseIf bTam = 9 Then
            
            strData = Mid$(txtCampo.Text, 1, 2)     'dia
            
            If CInt(strData) < 1 Or CInt(strData > 31) Then
                Beep
            Else
                strData = Mid$(txtCampo.Text, 4, 2)     'mes
                
                If CInt(strData) < 1 Or CInt(strData > 12) Then
                    Beep
                Else
                    strData = txtCampo.Text & Chr$(KeyAscii)
                    
                    If Not IsDate(CDate(strData)) Then
                        Beep
                    Else
                        txtCampo.Text = strData
                    End If
                
                End If
            
            End If
            
        ElseIf bTam < 10 Then
            
            bKey = KeyAscii
        Else
            bKey = 0
        
        End If
    
    Else
        
        Beep
    
    End If
    
    SendKeys "{END}"
    KeyAscii = bKey
    Exit Sub
    
TrataErro:

    If Err.Number = 13 Then
        MsgBox strData, vbInformation, "Data Inv�lida"
        KeyAscii = 0
        Beep
        Err.Clear
    End If

End Sub

Sub MMAA(ByRef KeyAscii, ByRef txtCampo)
    
    On Error GoTo TrataErro

    Dim bTam As Byte    'tamanho do campo JA digitado
    Dim strData As String
    Dim bKey As Byte
    
    bTam = Len(txtCampo.Text)
    
    If KeyAscii = 8 And bTam > 0 Then 'backspace
        
        If Mid$(txtCampo.Text, bTam) = "/" Then
            txtCampo.Text = Mid$(txtCampo.Text, 1, bTam - 2)
        Else
            txtCampo.Text = Mid$(txtCampo.Text, 1, bTam - 1)
        End If
            
    ElseIf Chr$(KeyAscii) >= "0" And Chr$(KeyAscii) <= "9" Then
        
        If bTam = 1 Then
            strData = txtCampo.Text & Chr$(KeyAscii)
            
            If CInt(strData) < 1 Or CInt(strData > 12) Then
                Beep
            Else
                txtCampo.Text = txtCampo.Text & Chr$(KeyAscii) & "/"
            End If
        
        ElseIf bTam = 5 Then
            
            strData = Mid$(txtCampo.Text, 1, 2)     'mes
            
            If CInt(strData) < 1 Or CInt(strData > 12) Then
               Beep
            Else
               strData = txtCampo.Text & Chr$(KeyAscii)
               
               If Not IsDate(CDate(strData)) Then
                  Beep
               Else
                  txtCampo.Text = strData
               End If
            
            End If
            
        ElseIf bTam < 5 Then
            bKey = KeyAscii
        Else
            bKey = 0
        End If
    
    Else
        
        Beep
    
    End If
    
    SendKeys "{END}"
    KeyAscii = bKey
    Exit Sub
    
TrataErro:

    If Err.Number = 13 Then
        MsgBox strData, vbInformation, "Data Inv�lida"
        KeyAscii = 0
        Beep
        Err.Clear
    End If

End Sub



Sub DATA(ByRef KeyAscii, ByRef txtCampo)
    
    On Error GoTo TrataErro

    Dim bTam As Byte    'tamanho do campo JA digitado
    Dim strData As String
    Dim bKey As Byte
    
    bTam = Len(txtCampo.Text)
    
    If KeyAscii = 8 And bTam > 0 Then 'backspace
        
        If Mid$(txtCampo.Text, bTam) = "/" Then
            txtCampo.Text = Mid$(txtCampo.Text, 1, bTam - 2)
        Else
            txtCampo.Text = Mid$(txtCampo.Text, 1, bTam - 1)
        End If
            
    ElseIf Chr$(KeyAscii) >= "0" And Chr$(KeyAscii) <= "9" Then
        
        If bTam = 1 Then
            strData = txtCampo.Text & Chr$(KeyAscii)
            
            If CInt(strData) < 1 Or CInt(strData > 31) Then
                Beep
            Else
                txtCampo.Text = txtCampo.Text & Chr$(KeyAscii) & "/"
            End If
            
        ElseIf bTam = 4 Then
            
            strData = Mid$(txtCampo.Text, 4) & Chr$(KeyAscii)
            
            If CInt(strData) < 1 Or CInt(strData > 12) Then
                Beep
            Else
                txtCampo.Text = txtCampo.Text & Chr$(KeyAscii) & "/"
            End If
        
        ElseIf bTam = 7 Then
            
            strData = Mid$(txtCampo.Text, 1, 2)     'dia
            
            If CInt(strData) < 1 Or CInt(strData > 31) Then
                Beep
            Else
                strData = Mid$(txtCampo.Text, 4, 2)     'mes
                
                If CInt(strData) < 1 Or CInt(strData > 12) Then
                    Beep
                Else
                    strData = txtCampo.Text & Chr$(KeyAscii)
                    
                    If Not IsDate(CDate(strData)) Then
                        Beep
                    Else
                        txtCampo.Text = strData
                    End If
                
                End If
            
            End If
            
        ElseIf bTam < 8 Then
            bKey = KeyAscii
        Else
            bKey = 0
        End If
    
    Else
        
        Beep
    
    End If
    
    SendKeys "{END}"
    KeyAscii = bKey
    Exit Sub
    
TrataErro:

    If Err.Number = 13 Then
        MsgBox strData, vbInformation, "Data Inv�lida"
        KeyAscii = 0
        Beep
        Err.Clear
    End If

End Sub


Function CONV_VALOR(strValor As String) As String
    
    Dim i As Byte
    
    i = InStr(strValor, ",")
    
    Do Until i = 0
        Mid(strValor, i, 1) = "."
        i = InStr(strValor, ",")
    Loop
    
    CONV_VALOR = strValor

End Function



Sub Process_Line_Errors()
    
    Dim iFnum As Integer
    'On Error GoTo Handler_Process_Line_Errors
                
    If Err.Number = 3186 Or Err.Number = 3188 Or Err.Number = 3260 Or Err.Number = 3197 Then
        Resume
    Else
        MsgBox "Ocorreu o erro: " & Err.Number & " -" & Err.Description & ". Ligue para o departamento de sistemas"
        Screen.MousePointer = vbDefault
        End
    End If

    Exit Sub

Handler_Process_Line_Errors:
    
    DoEvents
    Resume Next

End Sub

Function FmtBR(ByVal Valor) As String

    Dim Temp As String
    Dim i As Integer
    
    Temp = Trim(Valor)
        
    For i = 1 To Len(Temp)
        If Mid$(Temp, i, 1) = "," Then
            Mid$(Temp, i, 1) = "."
        End If
    Next i

    FmtBR = Temp

End Function


Function iRetornaDigito(ByVal iOrigem As Long) As Integer

    Dim iSoma As Integer
    Dim gResto As Single
    Dim iTam As Integer
    Dim iFator As Integer
    Dim iContador As Integer
    Dim sOrigem As String

    iTam = Len(Trim$(Str$(iOrigem)))
    iFator = 2
    
    sOrigem = Trim$(Str$(iOrigem))
    
    For iContador = iTam To 1 Step -1
        iSoma = iSoma + Val(Mid$(sOrigem, iContador, 1)) * iFator
        iFator = iFator + 1
    Next iContador
    
    gResto = iSoma Mod 11
    
    If gResto = 0 Then
        iRetornaDigito = 1
    ElseIf gResto = 1 Then
        iRetornaDigito = 0
    Else
        iRetornaDigito = 11 - gResto
    End If

End Function


Function Maiusculo(KeyAscii As Integer) As Integer
    
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    Maiusculo = KeyAscii
    
End Function

Function Numerico(ByVal KeyAscii As Integer) As Integer
    
    If KeyAscii = 8 Then    'BACKSPACE
        Numerico = KeyAscii
        Exit Function
    End If
    
    If Chr(KeyAscii) < "0" Or Chr(KeyAscii) > "9" Then
        KeyAscii = 0
    End If
    
    Numerico = KeyAscii

End Function

Function Texto(ByVal KeyAscii As Integer) As Integer
    
    If KeyAscii = 8 Then    'BACKSPACE
        Texto = KeyAscii
        Exit Function
    End If
    
    If Chr$(KeyAscii) = "'" Or Chr$(KeyAscii) = "," Or Chr$(KeyAscii) = ";" Then
        KeyAscii = 0
        Beep
    Else
        KeyAscii = Asc(UCase(Chr$(KeyAscii)))
    End If
    
    Texto = KeyAscii

End Function

Function Valor(ByVal KeyAscii As Integer, strCampo As String) As Integer
    
    If KeyAscii = 8 Then    'BACKSPACE
        Valor = KeyAscii
        Exit Function
    End If
    
    If Chr$(KeyAscii) = "," Then
        KeyAscii = Asc(".")
    End If
    
    If Chr$(KeyAscii) = "," Or Chr$(KeyAscii) = "." Then
       If InStr(strCampo, ",") > 0 Or InStr(strCampo, ".") > 0 Then
            KeyAscii = 0
            Beep
        End If
    Else
        If Chr$(KeyAscii) < "0" Or Chr$(KeyAscii) > "9" Then
            KeyAscii = 0
            Beep
        End If
    End If
    
   Valor = KeyAscii

End Function

Sub Hora(ByRef KeyAscii, ByRef txtCampo)

    On Error GoTo TrataErro

    Dim bTam As Byte    'tamanho do campo JA digitado
    Dim strData As String
    Dim bKey As Byte
    
    bTam = Len(txtCampo.Text)
    
    If Chr$(KeyAscii) >= "0" And Chr$(KeyAscii) <= "9" Then
    
        If bTam = 0 Then
            
            If Chr$(KeyAscii) = "0" Or Chr$(KeyAscii) = "1" Or Chr$(KeyAscii) = "2" Then
                bKey = KeyAscii
            Else
                Beep
                bKey = 0
            End If
                
        ElseIf bTam = 1 Then
            
            strData = txtCampo
            
            If Mid(strData, 1, 1) = "1" Or Mid(strData, 1, 1) = "0" Then
                txtCampo.Text = strData & Chr$(KeyAscii) & ":"
                
            ElseIf Mid(strData, 1, 1) = "2" And Chr$(KeyAscii) <= "3" Then
                txtCampo.Text = strData & Chr$(KeyAscii) & ":"
                
            Else
                Beep
                bKey = 0
            End If
            
        ElseIf bTam = 3 Then
            
            strData = txtCampo
            
            If Chr$(KeyAscii) < 6 Then
                bKey = KeyAscii
            Else
                Beep
                bKey = 0
            End If
            
        ElseIf bTam = 4 Then
            
            bKey = KeyAscii
           
        Else
            
            bKey = 0
            
        End If
        
    End If
      
    SendKeys "{END}"
    KeyAscii = bKey
    Exit Sub
    
TrataErro:

    If Err.Number = 13 Then
        MsgBox strData, vbInformation, "Hora Inv�lida"
        KeyAscii = 0
        Beep
        Err.Clear
    End If

End Sub

