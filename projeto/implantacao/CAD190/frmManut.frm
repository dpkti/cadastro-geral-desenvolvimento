VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmManutConta 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Manuten��o de Contas"
   ClientHeight    =   3915
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5535
   Icon            =   "frmManut.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   261
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   369
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fra 
      Caption         =   "Dados"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   2535
      Left            =   60
      TabIndex        =   12
      Top             =   930
      Width           =   5445
      Begin VB.TextBox txtAgenciaConta 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1710
         MaxLength       =   20
         TabIndex        =   5
         Top             =   2100
         Width           =   2595
      End
      Begin VB.TextBox txtCodCarteira 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1710
         MaxLength       =   2
         TabIndex        =   4
         Top             =   1740
         Width           =   360
      End
      Begin VB.TextBox txtCodBco 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1710
         MaxLength       =   3
         TabIndex        =   2
         Top             =   968
         Width           =   450
      End
      Begin VB.ComboBox cboDeposito 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         ItemData        =   "frmManut.frx":23D2
         Left            =   1710
         List            =   "frmManut.frx":23D9
         Style           =   2  'Dropdown List
         TabIndex        =   0
         Top             =   180
         Width           =   2445
      End
      Begin VB.ComboBox cboDivisao 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         ItemData        =   "frmManut.frx":23E0
         Left            =   1710
         List            =   "frmManut.frx":23E7
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   570
         Width           =   1995
      End
      Begin VB.ComboBox cboTpDuplicata 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         ItemData        =   "frmManut.frx":23EE
         Left            =   1710
         List            =   "frmManut.frx":240A
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   1350
         Width           =   2445
      End
      Begin VB.Label lblNomeBco 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   285
         Left            =   2220
         TabIndex        =   19
         Top             =   1020
         Width           =   3135
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         Caption         =   "Ag�ncia/Conta"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   450
         TabIndex        =   18
         Top             =   2130
         Width           =   1500
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         Caption         =   "C�digo da Carteira"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   150
         TabIndex        =   17
         Top             =   1770
         Width           =   2040
      End
      Begin VB.Label Label6 
         Appearance      =   0  'Flat
         Caption         =   "Tipo Duplicata"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   480
         TabIndex        =   16
         Top             =   1410
         Width           =   1380
      End
      Begin VB.Label Label5 
         Appearance      =   0  'Flat
         Caption         =   "C�digo do Banco"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   270
         TabIndex        =   15
         Top             =   1020
         Width           =   1650
      End
      Begin VB.Label Label4 
         Appearance      =   0  'Flat
         Caption         =   "Divis�o"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   1080
         TabIndex        =   14
         Top             =   630
         Width           =   690
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         Caption         =   "Dep�sito"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   900
         TabIndex        =   13
         Top             =   240
         Width           =   750
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   6
      Top             =   3585
      Width           =   5535
      _ExtentX        =   9763
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   9710
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   810
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   8
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmManut.frx":2484
      PICN            =   "frmManut.frx":24A0
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdOk 
      Height          =   690
      Left            =   1740
      TabIndex        =   9
      TabStop         =   0   'False
      ToolTipText     =   "OK"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmManut.frx":317A
      PICN            =   "frmManut.frx":3196
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdConsCta 
      Height          =   690
      Left            =   870
      TabIndex        =   10
      TabStop         =   0   'False
      ToolTipText     =   "Consultar Contas"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmManut.frx":3E70
      PICN            =   "frmManut.frx":3E8C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdCancel 
      Height          =   690
      Left            =   2610
      TabIndex        =   11
      TabStop         =   0   'False
      ToolTipText     =   "Limpar Campos"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmManut.frx":4B66
      PICN            =   "frmManut.frx":4B82
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmManutConta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Sub cboTpDuplicata_LostFocus()

    Call vVB_Generica_001.ExcluiBind(vBanco)
    Call Criar_Cursor(vBanco, "vCursor")
    vBanco.Parameters.Add "vCOD_LOJA", Trim(Mid(cboDeposito.Text, 1, 2)), 1
    vBanco.Parameters.Add "vCOD_BANCO", Trim(txtCodBco.Text), 1
    vBanco.Parameters.Add "vDIVISAO", Mid(cboDivisao.Text, 1, 1), 1
    vBanco.Parameters.Add "vTP_DUPLICATA", Mid(cboTpDuplicata.Text, 1, 1), 1
    vSql = "PRODUCAO.PCK_CAD190.PR_GET_BANCO_CONTA(:vCursor,:vCOD_LOJA,:vCOD_BANCO,:vDIVISAO,:vTP_DUPLICATA)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro)
        Exit Sub
    Else
        Set vObjOracle = vParameters("vCursor").Value
    End If
    If vObjOracle.EOF = True Then
        fl_Achou = 0
        Exit Sub
    End If
    
    fl_Achou = 1
    txtCodCarteira.Text = vObjOracle!COD_CARTEIRA
    txtAgenciaConta.Text = vObjOracle!AGENCIA
    
End Sub

Private Sub cmdCancel_Click()

    cboDeposito.ListIndex = -1
    cboDivisao.ListIndex = -1
    txtCodBco = ""
    lblNomeBco.Caption = ""
    cboTpDuplicata.ListIndex = -1
    txtCodCarteira = ""
    txtAgenciaConta = ""
    cboDeposito.SetFocus
    
End Sub

Private Sub cmdConsCta_Click()

    frmConsCta.Enabled = True
    frmConsCta.Show 1
    
End Sub

Private Sub cmdOk_Click()

    If Trim(cboDeposito.Text) = "" Then
        Call vVB_Generica_001.Informar("� Necess�rio que se Escolha um Dep�sito!")
        cboDeposito.SetFocus
        Exit Sub

    ElseIf Trim(cboDivisao.Text) = "" Then
        Call vVB_Generica_001.Informar("� Necess�rio que se Escolha uma Divis�o!")
        cboDivisao.SetFocus
        Exit Sub
    
    ElseIf Trim(txtCodBco.Text) = "" Then
        Call vVB_Generica_001.Informar("� Necess�rio que se Escolha um Banco!")
        txtCodBco.SetFocus
        Exit Sub

    ElseIf Trim(cboTpDuplicata.Text) = "" Then
        Call vVB_Generica_001.Informar("� Necess�rio que se Escolha um Tipo de Duplicata!")
        cboTpDuplicata.SetFocus
        Exit Sub

    ElseIf Trim(txtCodCarteira.Text) = "" Then
        Call vVB_Generica_001.Informar("� Necess�rio que se Escolha um C�digo de Carteira!")
        txtCodCarteira.SetFocus
        Exit Sub

    ElseIf Trim(txtAgenciaConta.Text) = "" Then
        Call vVB_Generica_001.Informar("� Necess�rio que se Escolha uma Ag�ncia/Conta!")
        txtAgenciaConta.SetFocus
        Exit Sub
    
    End If
    
    Call vVB_Generica_001.ExcluiBind(vBanco)
    vBanco.Parameters.Add "FL_ACHOU", fl_Achou, 1
    vBanco.Parameters.Add "vCOD_BANCO", Trim(txtCodBco.Text), 1
    vBanco.Parameters.Add "vDIVISAO", Mid(cboDivisao.Text, 1, 1), 1
    vBanco.Parameters.Add "vTP_DUPLICATA", Mid(cboTpDuplicata.Text, 1, 1), 1
    vBanco.Parameters.Add "vCOD_CARTEIRA", Trim(txtCodCarteira.Text), 1
    vBanco.Parameters.Add "vAGENCIA", Trim(txtAgenciaConta.Text), 1
    vBanco.Parameters.Add "vCOD_LOJA", Trim(Mid(cboDeposito.Text, 1, 2)), 1
    If fl_Achou = 1 Then 'alteracao
        If vVB_Generica_001.Perguntar("Confirma Altera��o?") = 7 Then
            Exit Sub
        End If
    ElseIf vVB_Generica_001.Perguntar("Confirma Inclus�o?") = 7 Then
        Exit Sub
    End If
    vSql = "PRODUCAO.PCK_CAD190.PR_INSERT_UPDATE_BANCO_CONTA(:FL_ACHOU, :vCOD_BANCO, :vDIVISAO, :vTP_DUPLICATA, :vCOD_CARTEIRA, :vAGENCIA, :vCOD_LOJA)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro)
        Exit Sub
    End If
    Call vVB_Generica_001.Informar("Inclus�o/Altera��o Efetuada com Sucesso!")
    Call cmdCancel_Click
    
End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    
    Me.Top = 0
    Me.Left = 0
    '-----------Carrega Depositos-------------------
    Call vVB_Generica_001.ExcluiBind(vBanco)
    Set vObjOracle = vVB_Generica_001.TabelaLoja(vBanco)
    Call vVB_Generica_001.PreencheComboList(vObjOracle, cboDeposito, "DEP�SITO")
    cboDeposito.RemoveItem (4)
    
    '----------Carrega Divis�es---------------------
    Call vVB_Generica_001.ExcluiBind(vBanco)
    Call Criar_Cursor(vBanco, "vCursor")
    vSql = "PRODUCAO.PCK_CAD190.PR_DIVISAO(:vCursor)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro)
        Exit Sub
    Else
        Set vObjOracle = vParameters("vCursor").Value
    End If
    If Not vObjOracle.EOF Then
        Call vVB_Generica_001.PreencheComboList(vObjOracle, cboDivisao, "DIVISAO", "DESCRICAO")
        cboDivisao.AddItem "T - TODOS"
    End If
    
End Sub

Private Sub txtAgenciaConta_GotFocus()

    cmdOk.Default = True
    Call vVB_Generica_001.SelecionaCampo(txtAgenciaConta)

End Sub

Private Sub txtAgenciaConta_KeyPress(KeyAscii As Integer)

    'KeyAscii = vVB_Generica_001.Numero(KeyAscii)
    
    If InStr(1, "!@#$%�&*()_+=|\:;?/{}[]-<>,.'", Chr(KeyAscii)) <> 0 Then
        KeyAscii = 0
    End If
    
End Sub

Private Sub txtAgenciaConta_LostFocus()

    cmdOk.Default = False
    
End Sub

Private Sub txtCodBco_Change()
    
    lblNomeBco = ""
    If Len(txtCodBco) = 3 And frmConsCta.Enabled = False Then
        cboTpDuplicata.SetFocus
    End If
    
End Sub

Private Sub txtCodBco_GotFocus()

    Call vVB_Generica_001.SelecionaCampo(txtCodBco)
    
End Sub

Private Sub txtCodBco_KeyPress(KeyAscii As Integer)

    If KeyAscii = 13 Then
        cboTpDuplicata.SetFocus
    End If
    KeyAscii = vVB_Generica_001.Numero(KeyAscii)
    
End Sub

Public Sub txtCodBco_LostFocus()

    If Trim(txtCodBco.Text) = "" Then
        Exit Sub
    End If
    Call vVB_Generica_001.ExcluiBind(vBanco)
    vBanco.Parameters.Add "vCovbancoco", Trim(txtCodBco.Text), 1
    vBanco.Parameters.Add "vNomeBco", "", 2
    vSql = "PRODUCAO.PCK_CAD190.PR_GET_NOME_BANCO(:vCovbancoco,:vNomeBco)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro)
        Exit Sub
    End If
    If vBanco.Parameters("vNomeBco").Value = 0 Then
        Call vVB_Generica_001.Informar("C�digo do Banco n�o Cadastrado!")
        txtCodBco = ""
        txtCodBco.SetFocus
        Exit Sub
    Else
        lblNomeBco.Caption = vBanco.Parameters("vNomeBco").Value
    End If

End Sub


Private Sub txtCodCarteira_Change()

    If Len(txtCodCarteira) = 2 And txtAgenciaConta.Enabled = True Then
        txtAgenciaConta.SetFocus
    End If
    
End Sub

Private Sub txtCodCarteira_GotFocus()

    Call vVB_Generica_001.SelecionaCampo(txtCodCarteira)
    
End Sub

Private Sub txtCodCarteira_KeyPress(KeyAscii As Integer)

    If KeyAscii = 13 Then
        txtAgenciaConta.SetFocus
    End If
    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)

End Sub

