VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmBanco 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Manuten��o de Bancos"
   ClientHeight    =   5190
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6405
   Icon            =   "frmBanco.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   346
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   427
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraBanco 
      Caption         =   "Inclus�o"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   3795
      Left            =   60
      TabIndex        =   6
      Top             =   930
      Width           =   6285
      Begin VB.TextBox txtDespCart 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2100
         MaxLength       =   18
         TabIndex        =   21
         Top             =   3270
         Width           =   1956
      End
      Begin VB.TextBox txtData 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2100
         MaxLength       =   8
         TabIndex        =   17
         Top             =   1560
         Width           =   960
      End
      Begin VB.TextBox txtSigla 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2100
         MaxLength       =   10
         TabIndex        =   16
         Top             =   1140
         Width           =   1245
      End
      Begin VB.TextBox txtNomeBco 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2100
         MaxLength       =   35
         TabIndex        =   15
         Top             =   720
         Width           =   3915
      End
      Begin VB.TextBox txtCodBco 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2100
         MaxLength       =   3
         TabIndex        =   14
         Top             =   300
         Width           =   504
      End
      Begin VB.ComboBox cboVenda 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         ItemData        =   "frmBanco.frx":23D2
         Left            =   2100
         List            =   "frmBanco.frx":23DC
         Style           =   2  'Dropdown List
         TabIndex        =   19
         Top             =   2415
         Width           =   2190
      End
      Begin VB.ComboBox cboAceita 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         ItemData        =   "frmBanco.frx":23FE
         Left            =   2100
         List            =   "frmBanco.frx":2408
         Style           =   2  'Dropdown List
         TabIndex        =   20
         Top             =   2835
         Width           =   1152
      End
      Begin VB.ComboBox cboDivisao 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         ItemData        =   "frmBanco.frx":2416
         Left            =   2100
         List            =   "frmBanco.frx":241D
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   18
         Top             =   1995
         Width           =   1845
      End
      Begin VB.Label Label7 
         Appearance      =   0  'Flat
         Caption         =   "Despesas de Cart�rio"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   300
         TabIndex        =   22
         Top             =   3330
         Width           =   2040
      End
      Begin VB.Label Label6 
         Appearance      =   0  'Flat
         Caption         =   "Aceita Dep�sito?"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   630
         TabIndex        =   13
         Top             =   2880
         Width           =   1650
      End
      Begin VB.Label Label5 
         Appearance      =   0  'Flat
         Caption         =   "Venda"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   1470
         TabIndex        =   12
         Top             =   2460
         Width           =   690
      End
      Begin VB.Label Label4 
         Appearance      =   0  'Flat
         Caption         =   "Divis�o"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   1470
         TabIndex        =   11
         Top             =   2040
         Width           =   690
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         Caption         =   "Data de In�cio "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   900
         TabIndex        =   10
         Top             =   1590
         Width           =   1920
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         Caption         =   "Sigla"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   1650
         TabIndex        =   9
         Top             =   1170
         Width           =   690
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         Caption         =   "Nome"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   1530
         TabIndex        =   8
         Top             =   750
         Width           =   690
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         Caption         =   "C�digo do Banco"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   630
         TabIndex        =   7
         Top             =   360
         Width           =   1650
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   4860
      Width           =   6405
      _ExtentX        =   11298
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   11245
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   810
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmBanco.frx":2424
      PICN            =   "frmBanco.frx":2440
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdOk 
      Height          =   690
      Left            =   1770
      TabIndex        =   3
      TabStop         =   0   'False
      ToolTipText     =   "OK"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmBanco.frx":311A
      PICN            =   "frmBanco.frx":3136
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdConsCta 
      Height          =   690
      Left            =   900
      TabIndex        =   4
      TabStop         =   0   'False
      ToolTipText     =   "Consultar Bancos"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmBanco.frx":3E10
      PICN            =   "frmBanco.frx":3E2C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdCancel 
      Height          =   690
      Left            =   2640
      TabIndex        =   5
      TabStop         =   0   'False
      ToolTipText     =   "Limpar Campos"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmBanco.frx":4B06
      PICN            =   "frmBanco.frx":4B22
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmBanco"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cboAceita_GotFocus()

    cmdOk.Default = True
    
End Sub

Private Sub cboAceita_LostFocus()

    cmdOk.Default = False

End Sub

Private Sub cmdCancel_Click()

    fraBanco.Caption = "Inclus�o"
    txtCodBco = ""
    txtDespCart = ""
    txtCodBco.SetFocus
    txtData = ""
    txtNomeBco = ""
    txtSigla = ""
    cboAceita.ListIndex = -1
    cboDivisao.ListIndex = -1
    cboVenda.ListIndex = -1
    
End Sub

Private Sub cmdConsCta_Click()

    frmConsBco.Show
    
End Sub

Private Sub cmdOk_Click()

        
    Dim vDIVISAO As String   'VARI�VEL QUE RECEBE "D" OU "B" P/ GRAVAR NO ORACLE
    Dim vDTINI As String     'VARI�VEL QUE RECEBE A DATA P/ GRAVAR NO ORACLE
    Dim vVENDA As String     'VARI�VEL QUE RECEBE "V" OU "O" P/ GRAVAR NO ORACLE
    Dim vACEITA As String    'VARI�VEL QUE RECEBE "S" OU "N" P/ GRAVAR NO ORACLE

    If vModo = 0 Then
        If VERIFICA_CAMPOS(frmBanco) = False Then
            txtCodBco.SetFocus
            Exit Sub
        End If
        If Len(txtData) <> 8 Then
            Call vVB_Generica_001.Informar("Data Inv�lida!")
            txtData = ""
            txtData.SetFocus
            Exit Sub
        End If
    
    ElseIf vModo = 1 Then
        If Trim(txtCodBco) = "" Then
            Call vVB_Generica_001.Informar("C�digo do Banco n�o Pode Estar em Branco!")
            txtCodBco.SetFocus
            Exit Sub
        End If
        If fl_Alt = 0 Then
            Call vVB_Generica_001.Informar("N�o � Poss�vel Cadastrar Banco Nessa Tela!")
            txtCodBco.Text = ""
            txtDespCart.Text = ""
            Exit Sub
        End If
    End If
    
    vDIVISAO = Mid(cboDivisao.Text, 1, 1)
    vDTINI = txtData.Text
    vVENDA = Mid(cboVenda.Text, 1, 1)
    vACEITA = Mid(cboAceita.Text, 1, 1)
    
    Call vVB_Generica_001.ExcluiBind(vBanco)
    vBanco.Parameters.Add "vCovbancoco", txtCodBco, 1
    vBanco.Parameters.Add "flAlt", fl_Alt, 1
    vBanco.Parameters.Add "vSigla", txtSigla, 1
    vBanco.Parameters.Add "vNOME", Trim(txtNomeBco), 1
    vBanco.Parameters.Add "vDIVISAO", vDIVISAO, 1
    vBanco.Parameters.Add "vVENDA", vVENDA, 1
    vBanco.Parameters.Add "vDTINI", vDTINI, 1
    vBanco.Parameters.Add "vDESPCART", IIf(txtDespCart.Text = 0, 0, Format(txtDespCart.Text, "############.00")), 1
    vBanco.Parameters.Add "vACEITA", Mid(cboAceita.Text, 1, 1), 1
    
    If fl_Alt = 0 Then ' INCLUS�O
        If txtDespCart.Text <> 0 Then
            Call vVB_Generica_001.Informar("Valor das Despesas na Inclus�o Deve Ser '0.00'")
            txtDespCart.Text = "0"
        End If
        If vVB_Generica_001.Perguntar("Confirma Inclus�o?") = 7 Then
            Call cmdCancel_Click
            Exit Sub
        End If
        vSql = "PRODUCAO.PCK_CAD190.PR_INSERT_UPDATE_BANCO(:flAlt, :vCovbancoco, :vSigla, :vNOME, :vDIVISAO, :vVENDA, :vDTINI, :vDESPCART, :vACEITA)"
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        If vErro <> "" Then
            vVB_Generica_001.ProcessaErro (vErro)
            Exit Sub
        Else
            Call vVB_Generica_001.Informar("Inclus�o Efetuada Com Sucesso!")
            Call cmdCancel_Click
        End If
    
    Else 'ALTERA��O
        If vVB_Generica_001.Perguntar("Confirma Altera��o?") = 7 Then
            Call cmdCancel_Click
            Exit Sub
        End If
        If txtDespCart.Text <> 0 And fl_Alt = 0 Then
            Call vVB_Generica_001.Informar("Valor das Despesas na Altera��o � Diferente de 0")
            txtDespCart.Text = "0"
        End If
        vSql = "PRODUCAO.PCK_CAD190.PR_INSERT_UPDATE_BANCO(:flAlt, :vCovbancoco, :vSigla, :vNOME, :vDIVISAO, :vVENDA, :vDTINI, :vDESPCART, :vACEITA)"
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        If vErro <> "" Then
            vVB_Generica_001.ProcessaErro (vErro)
            Exit Sub
        Else
            Call vVB_Generica_001.Informar("Altera��o Efetuada Com Sucesso!")
            Call cmdCancel_Click
        End If
    
    End If
    
End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    
    Me.Top = 0
    Me.Left = 0
    
    If vModo = 0 Then
    '-------------------manutencao de bancos-------------------
        Label7.Visible = False
        txtDespCart.Visible = False
        fraBanco.Height = 221
        Me.Height = 5130
        
    Else
    '-----------------------cart�rio---------------------------
    
        fl_Alt = 0
        Me.Height = 3000
        Me.Width = 4575
        fraBanco.Width = 291
        Me.Caption = "Cadastro de Despesas de Cart�rio"
        Label1.Visible = False
        Label2.Visible = False
        Label3.Visible = False
        Label4.Visible = False
        Label5.Visible = False
        Label6.Visible = False
        fraBanco.Height = 80
        Label7.Top = Label1.Top
        txtDespCart.Top = txtNomeBco.Top
        txtData.Visible = False
        txtNomeBco.Visible = False
        txtSigla.Visible = False
        cboAceita.Visible = False
        cboDivisao.Visible = False
        cboVenda.Visible = False
    End If
    
    Call vVB_Generica_001.ExcluiBind(vBanco)
    Call Criar_Cursor(vBanco, "vCursor")
    vSql = "PRODUCAO.PCK_CAD190.PR_DIVISAO(:vCursor)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro)
        Exit Sub
    Else
        Set vObjOracle = vParameters("vCursor").Value
    End If
    If Not vObjOracle.EOF Then
        Call vVB_Generica_001.PreencheComboList(vObjOracle, cboDivisao, "DIVISAO", "DESCRICAO")
        cboDivisao.AddItem "T - TODOS"
    End If
    
End Sub


Private Sub txtCodBco_Change()

    If Len(txtCodBco) = 3 Then
        If vModo = 1 Then
            txtDespCart.SetFocus
            Exit Sub
        Else
            txtNomeBco.SetFocus
            Exit Sub
        End If
    End If
    txtDespCart = ""
    txtData = ""
    txtNomeBco = ""
    txtSigla = ""
    cboAceita.ListIndex = -1
    cboDivisao.ListIndex = -1
    cboVenda.ListIndex = -1
    
End Sub

Private Sub txtCodBco_GotFocus()

    Call vVB_Generica_001.SelecionaCampo(txtCodBco)
    
End Sub

Private Sub txtCodBco_KeyPress(KeyAscii As Integer)

    If KeyAscii = 13 Then 'enter
        If vModo = 1 Then
            txtDespCart.SetFocus
        Else
            txtNomeBco.SetFocus
        End If
    End If
    KeyAscii = vVB_Generica_001.Numero(KeyAscii)
    
End Sub

Public Sub txtCodBco_LostFocus()
    
    If Trim(txtCodBco) = "" Then
        Exit Sub
    End If
    
    txtDespCart = ""
    txtData = ""
    txtNomeBco = ""
    txtSigla = ""
    cboAceita.ListIndex = -1
    cboDivisao.ListIndex = -1
    cboVenda.ListIndex = -1
    
    ' Procura na tabela BANCO se o c�digo existe
    
    Call vVB_Generica_001.ExcluiBind(vBanco)
    Call Criar_Cursor(vBanco, "vCursor")
    vBanco.Parameters.Add "vCovbancoanco", txtCodBco, 1
    vSql = "PRODUCAO.PCK_CAD190.PR_GET_BANCO(:vCursor,:vCovbancoanco)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro)
        Exit Sub
    Else
        Set vObjOracle = vParameters("vCursor").Value
    End If
    If Not vObjOracle.EOF Then
        fl_Alt = 1
        fraBanco.Caption = "Altera��o/Consulta"
        txtDespCart.Text = Format(vObjOracle!DESP_CARTORIO, "###,###,###,##0.00")
        txtNomeBco.Text = vObjOracle!NOME_BANCO
        txtSigla.Text = vObjOracle!Sigla
        txtData.Text = vObjOracle!Dt_Inicio
        If vObjOracle!DIVISAO = "D" Then
            cboDivisao.Text = "D - DPK"
        ElseIf vObjOracle!DIVISAO = "B" Then
            cboDivisao.Text = "B - BLAU PECAS"
        
        ElseIf vObjOracle!DIVISAO = "P" Then
            cboDivisao.Text = "P - DPASCHOAL"
    
        ElseIf vObjOracle!DIVISAO = "Z" Then 'ver se funciona
            cboDivisao.Text = "Z - AUTOZ"
    
        ElseIf vObjOracle!DIVISAO = "T" Then
            cboDivisao.Text = "T - Todos"
        End If

        If vObjOracle!Fl_Venda = "V" Then
            cboVenda.ListIndex = 1
        Else
            cboVenda.ListIndex = 0
        End If
        
        If vObjOracle!Fl_Deposito = "S" Then
            cboAceita.Text = "Sim"
        Else
            cboAceita.Text = "N�o"
        End If
        
        If vModo = 0 Then
            cboDivisao.SetFocus
        Else
            txtDespCart.SetFocus
        End If
        
    Else
        fl_Alt = 0
        fraBanco.Caption = "Inclus�o"
    End If
    
End Sub


Private Sub txtData_Change()

    If Len(txtData) = 8 And vModo = 0 Then
        cboDivisao.SetFocus
    End If
    
End Sub

Private Sub txtData_GotFocus()

    Call vVB_Generica_001.SelecionaCampo(txtData)
    
End Sub

Private Sub txtData_KeyPress(KeyAscii As Integer)

    If KeyAscii = 13 Then
        cboDivisao.SetFocus
    End If
    Call vVB_Generica_001.Data(KeyAscii, txtData)

End Sub

Private Sub txtDespCart_GotFocus()

    cmdOk.Default = True
    Call vVB_Generica_001.SelecionaCampo(txtDespCart)

End Sub

Private Sub txtDespCart_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtDespCart)
    If Chr$(KeyAscii) = "," Then
        KeyAscii = Asc(".")
    End If
    
End Sub

Private Sub txtDespCart_LostFocus()

    cmdOk.Default = False
    If txtDespCart.Text = "" Then
        txtDespCart.Text = "0.00"
    Else
        txtDespCart.Text = Format(txtDespCart.Text, "###,###,###,##0.00")
    End If
    
End Sub

Private Sub txtNomeBco_GotFocus()

    Call vVB_Generica_001.SelecionaCampo(txtNomeBco)
    
End Sub

Private Sub txtNomeBco_KeyPress(KeyAscii As Integer)

    If KeyAscii = 13 Then
        txtSigla.SetFocus
    End If
    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)

End Sub

Private Sub txtSigla_GotFocus()

    Call vVB_Generica_001.SelecionaCampo(txtSigla)
    
End Sub

Private Sub txtSigla_KeyPress(KeyAscii As Integer)

    If KeyAscii = 13 Then
        txtData.SetFocus
    End If
    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)

End Sub
