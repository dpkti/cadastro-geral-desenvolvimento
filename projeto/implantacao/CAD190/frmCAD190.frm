VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmCAD190 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "CAD190 - Cadastro de Banco/Conta Corrente"
   ClientHeight    =   8205
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11910
   Icon            =   "frmCAD190.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   8205
   ScaleWidth      =   11910
   WindowState     =   2  'Maximized
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   4
      Top             =   1125
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   7875
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   8308
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   2646
            MinWidth        =   2646
            Object.ToolTipText     =   "Usu�rio da rede"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   2381
            MinWidth        =   2381
            Object.ToolTipText     =   "Usu�rio do banco de dados"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   3528
            MinWidth        =   3528
            Object.ToolTipText     =   "Banco de dados conectado"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            Alignment       =   1
            Object.Width           =   2381
            MinWidth        =   2381
            TextSave        =   "13/04/06"
            Object.ToolTipText     =   "Data"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   1
            Object.Width           =   1587
            MinWidth        =   1587
            TextSave        =   "17:10"
            Object.ToolTipText     =   "Hora"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin Bot�o.cmd cmdSobre 
      Height          =   1005
      Left            =   1350
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Sobre"
      Top             =   45
      Width           =   1005
      _ExtentX        =   1773
      _ExtentY        =   1773
      BTYPE           =   3
      TX              =   "Sobre"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCAD190.frx":23D2
      PICN            =   "frmCAD190.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   1005
      Left            =   6525
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Sair do sistema"
      Top             =   45
      Width           =   1005
      _ExtentX        =   1773
      _ExtentY        =   1773
      BTYPE           =   3
      TX              =   "Sair"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCAD190.frx":30C8
      PICN            =   "frmCAD190.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdBancos 
      Height          =   1005
      Left            =   2385
      TabIndex        =   3
      TabStop         =   0   'False
      ToolTipText     =   "Manuten��o de Bancos"
      Top             =   45
      Width           =   1005
      _ExtentX        =   1773
      _ExtentY        =   1773
      BTYPE           =   3
      TX              =   "Bancos"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCAD190.frx":3DBE
      PICN            =   "frmCAD190.frx":3DDA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdDeposito 
      Height          =   1005
      Left            =   3420
      TabIndex        =   5
      TabStop         =   0   'False
      ToolTipText     =   "Cadastro de Prioridade Banco/Dep�sito"
      Top             =   45
      Width           =   1005
      _ExtentX        =   1773
      _ExtentY        =   1773
      BTYPE           =   3
      TX              =   "Dep�sito"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCAD190.frx":46B4
      PICN            =   "frmCAD190.frx":46D0
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdContaCorrente 
      Height          =   1005
      Left            =   4455
      TabIndex        =   6
      TabStop         =   0   'False
      ToolTipText     =   "Manuten��o de Contas"
      Top             =   45
      Width           =   1005
      _ExtentX        =   1773
      _ExtentY        =   1773
      BTYPE           =   3
      TX              =   "Conta Corrente"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCAD190.frx":53AA
      PICN            =   "frmCAD190.frx":53C6
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdCartorio 
      Height          =   1005
      Left            =   5490
      TabIndex        =   7
      TabStop         =   0   'False
      ToolTipText     =   "Cadastro de Despesas de Cart�rio"
      Top             =   45
      Width           =   1005
      _ExtentX        =   1773
      _ExtentY        =   1773
      BTYPE           =   3
      TX              =   "Cartorio"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCAD190.frx":60A0
      PICN            =   "frmCAD190.frx":60BC
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Image Image1 
      Height          =   1185
      Left            =   -90
      MouseIcon       =   "frmCAD190.frx":6D96
      MousePointer    =   99  'Custom
      Picture         =   "frmCAD190.frx":70A0
      Stretch         =   -1  'True
      ToolTipText     =   "Acessar a Intranet"
      Top             =   -45
      Width           =   1455
   End
End
Attribute VB_Name = "frmCAD190"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub cmdBancos_Click()

    vModo = 0
    frmBanco.Show

End Sub

Private Sub cmdCartorio_Click()

    vModo = 1
    frmBanco.Show
    
End Sub

Private Sub cmdContaCorrente_Click()

    frmManutConta.Show
    
End Sub

Private Sub cmdDeposito_Click()

    frmPrior.Show
    
End Sub

Private Sub cmdSair_Click()

    If vVB_Generica_001.Sair = 6 Then
    
        Set vObjOracle = Nothing
        Set vBanco = Nothing
        Set vSessao = Nothing
        
        End
    
    End If

End Sub
Private Sub cmdSobre_Click()

    frmSobre.Show 1

End Sub
Private Sub Form_Load()
    
    'INCLUIR OS ARQUIVOS: (PROJECT -> ADD FILE OU CTRL+D)
    'H:\Usr\Padroes\VB6\Forms\Aguardar.frm
    'H:\Usr\Padroes\VB6\Forms\Logo.frm
    'H:\Usr\Padroes\VB6\M�dulos\M�dulo padr�o.bas
    frmLogo.Show 1
    
    Me.Top = 0
    Me.Left = 0

    '<BANCO> = BANCO DE DADOS A SER CONECTADO
    '<USU�RIO> = USU�RIO DE CONEX�O COM O ORACLE
    'vErro = vVB_Generica_001.ConectaOracle("DESENV", "CAD190", True, Me)
    vErro = vVB_Generica_001.ConectaOracle("producao", "CAD190", True, Me)

    If vErro <> "" Then

        Call vVB_Generica_001.ProcessaErro(vErro)
        End

    End If

    vCd = vVB_Generica_001.vCd
    vTipoCD = vVB_Generica_001.vTipoCD
    Set vSessao = vVB_Generica_001.vSessao
    Set vBanco = vVB_Generica_001.vBanco
    Set vParameters = vBanco.Parameters
    
    'PARA SISTEMAS QUE FOREM CONECTAR NOS CD'S, DESCOMENTAR A LINHA
    'ABAIXO E INCLUIR O FORM ABAIXO (PROJECT -> ADD FORM)
    'Conex�o CD's
    'dlgConexao.Show 1

    Set vObjOracle = vVB_Generica_001.InformacoesSistema(vBanco, UCase(App.Title))

    If vObjOracle.EOF Then

        Call vVB_Generica_001.Informar("Sistema n�o cadastrado. � necess�rio fazer o cadastro para executar o programa.")
        End

    End If

    Call DefinirTelaSobre
    
End Sub
Private Sub Image1_Click()

    Shell "C:\Arquivos de programas\Internet Explorer\IEXPLORE.EXE intranet.dpk.com.br", vbNormalFocus

End Sub
