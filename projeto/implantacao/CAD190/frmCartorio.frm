VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmCartorio 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cadastro de Despesas de Cart�rio"
   ClientHeight    =   2745
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4350
   Icon            =   "frmCartorio.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   183
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   290
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraBanco 
      Caption         =   "Inclus�o"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1305
      Left            =   60
      TabIndex        =   8
      Top             =   960
      Width           =   4245
      Begin VB.TextBox txtCodBco 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2040
         MaxLength       =   3
         TabIndex        =   0
         Top             =   300
         Width           =   504
      End
      Begin VB.TextBox txtDespCart 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2040
         MaxLength       =   18
         TabIndex        =   1
         Top             =   780
         Width           =   1956
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         Caption         =   "C�digo do Banco"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   570
         TabIndex        =   10
         Top             =   360
         Width           =   1650
      End
      Begin VB.Label Label7 
         Appearance      =   0  'Flat
         Caption         =   "Despesas de Cart�rio"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   240
         TabIndex        =   9
         Top             =   840
         Width           =   2040
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   2
      Top             =   2415
      Width           =   4350
      _ExtentX        =   7673
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   7620
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   810
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   4
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCartorio.frx":23D2
      PICN            =   "frmCartorio.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdOk 
      Default         =   -1  'True
      Height          =   690
      Left            =   1740
      TabIndex        =   5
      TabStop         =   0   'False
      ToolTipText     =   "OK"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCartorio.frx":30C8
      PICN            =   "frmCartorio.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdConsCta 
      Height          =   690
      Left            =   870
      TabIndex        =   6
      TabStop         =   0   'False
      ToolTipText     =   "Consultar Bancos"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCartorio.frx":3DBE
      PICN            =   "frmCartorio.frx":3DDA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdCancel 
      Height          =   690
      Left            =   2610
      TabIndex        =   7
      TabStop         =   0   'False
      ToolTipText     =   "Limpar Campos"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCartorio.frx":4AB4
      PICN            =   "frmCartorio.frx":4AD0
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmCartorio"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdCancel_Click()

    fraBanco.Caption = "Inclus�o"
    txtCodBco = ""
    txtDespCart = ""
    txtCodBco.SetFocus

    
End Sub

Private Sub cmdConsCta_Click()

    frmConsBco.Show 1
    
End Sub

Private Sub cmdOk_Click()

    If Trim(txtCodBco) = "" Then
        Call vVB_Generica_001.Informar("C�digo do Banco n�o Pode Estar em Branco!")
        txtCodBco.SetFocus
        Exit Sub
    End If
    If fl_Alt = 0 Then
        Call vVB_Generica_001.Informar("N�o � Poss�vel Cadastrar Banco Nessa Tela!")
        txtCodBco.Text = ""
        txtDespCart.Text = ""
        Exit Sub
    End If
    
    'SE TODOS OS CAMPOS ESTIVEREM DEVIDAMENTE PREENCHIDOS, INICIALIZA
    'PROCESSO P/ INCLUS�O OU ALTERA��O NAS TABELAS
    
    Call vVB_Generica_001.ExcluiBind(vBanco)
    vBanco.Parameters.Add "vCOvbancoCO", txtCodBco, 1
    vBanco.Parameters.Add "vDESPCART", IIf(txtDespCart.Text = 0, 0, Format(txtDespCart.Text, "############.00")), 1
    If fl_Alt = 0 Then ' INCLUS�O
        If txtDespCart.Text <> 0 Then
            Call vVB_Generica_001.Informar("Valor das Despesas na Inclus�o Deve Ser '0,00'")
            txtDespCart.Text = "0"
        End If
        If vVB_Generica_001.Perguntar("Confirma Inclus�o?") = 7 Then
            Call cmdCancel_Click
            Exit Sub
        End If
        vSql = "PRODUCAO.PCK_CAD190.PR_INSERT_UPDATE_BANCO()"
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        If vErro <> "" Then
            vVB_Generica_001.ProcessaErro (vErro)
            Exit Sub
        Else
            Set vObjOracle = vParameters("vCursor").Value
        End If
    End If
    
    
End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    
    Me.Top = 0
    Me.Left = 0
    fl_Alt = 0
    
End Sub


Private Sub txtCodBco_Change()

    If Len(txtCodBco) = 3 And vModo = 1 Then
        txtDespCart.SetFocus
    End If
    txtDespCart = ""
    
End Sub

Private Sub txtCodBco_GotFocus()

    cmdOk.Default = False
    
End Sub

Private Sub txtCodBco_KeyPress(KeyAscii As Integer)

    If KeyAscii = 13 And vModo = 1 Then 'enter
        txtDespCart.SetFocus
    End If
    KeyAscii = vVB_Generica_001.Numero(KeyAscii)

End Sub


Private Sub txtCodBco_LostFocus()

    cmdOk.Default = True
    
    ' Procura na tabela BANCO se o c�digo existe
    
    Call vVB_Generica_001.ExcluiBind(vBanco)
    Call Criar_Cursor(vBanco, "vCursor")
    vBanco.Parameters.Add "vCovbancoanco", txtCodBco, 1
    vSql = "PRODUCAO.PCK_CAD190.PR_GET_BANCO(:vCursor,:vCovbancoanco)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro)
        Exit Sub
    Else
        Set vObjOracle = vParameters("vCursor").Value
    End If
    If Not vObjOracle.EOF Then
        fl_Alt = 1
        fraBanco.Caption = "Altera��o/Consulta"
        txtDespCart.Text = Format(vObjOracle!DESP_CARTORIO, "###,###,###,##0.00")
    Else
        fl_Alt = 0
        fraBanco.Caption = "Inclus�o"
    End If
    
End Sub

Private Sub txtDespCart_GotFocus()

    Call vVB_Generica_001.SelecionaCampo(txtDespCart)
    
End Sub

Private Sub txtDespCart_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtDespCart)
    If Chr$(KeyAscii) = "," Then
        KeyAscii = Asc(".")
    End If

End Sub
Private Sub txtDespCart_LostFocus()

    If txtDespCart.Text = "" Then
        txtDespCart.Text = "0.00"
    Else
        txtDespCart.Text = Format(txtDespCart.Text, "###,###,###,##0.00")
    End If
    
End Sub
