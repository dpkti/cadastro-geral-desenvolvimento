VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmConsBco 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Consulta de Bancos"
   ClientHeight    =   6735
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6705
   Icon            =   "frmConsBco.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   449
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   447
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fra 
      Caption         =   "Dados"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   705
      Left            =   60
      TabIndex        =   4
      Top             =   960
      Width           =   6555
      Begin VB.TextBox txtNome 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3450
         TabIndex        =   0
         Top             =   240
         Width           =   2940
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         Caption         =   "Digite o Nome do Banco ou Parte Dele:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   150
         TabIndex        =   5
         Top             =   300
         Width           =   3780
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   1
      Top             =   6405
      Width           =   6705
      _ExtentX        =   11827
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   11774
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   810
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   3
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsBco.frx":23D2
      PICN            =   "frmConsBco.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSFlexGridLib.MSFlexGrid mfgBancos 
      Height          =   4350
      Left            =   60
      TabIndex        =   6
      TabStop         =   0   'False
      ToolTipText     =   "Duplo Clique Para Alterar"
      Top             =   1800
      Width           =   6570
      _ExtentX        =   11589
      _ExtentY        =   7673
      _Version        =   393216
      BackColorBkg    =   -2147483633
      AllowUserResizing=   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin Bot�o.cmd cmdPesquisar 
      Height          =   690
      Left            =   900
      TabIndex        =   7
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsBco.frx":30C8
      PICN            =   "frmConsBco.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmConsBco"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdPesquisar_Click()

    If Trim(txtNome) = "" Then
        Call vVB_Generica_001.Informar("Digite o Nome Para Pesquisa!")
        cmdPesquisar.SetFocus
        Exit Sub
    End If
    
    Call Aguardar
    Call vVB_Generica_001.ExcluiBind(vBanco)
    Call Criar_Cursor(vBanco, "vCursor")
    vBanco.Parameters.Add "vNOME_BANCO", txtNome.Text, 1
    vSql = "PRODUCAO.PCK_CAD190.PR_SEL_BANCO(:vCursor,:vNOME_BANCO)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    Unload frmAguardar
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro)
        Exit Sub
    Else
        Set vObjOracle = vParameters("vCursor").Value
    End If
    If vObjOracle.EOF = True Then    'n�o achou
        Call vVB_Generica_001.Informar("Banco Inexistente!")
        txtNome = ""
        txtNome.SetFocus
        Exit Sub
    Else
        Call vVB_Generica_001.CarregaGridTabela(mfgBancos, vObjOracle, 3)
        txtNome = ""
    End If
        
    
End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    
    Me.Top = 0
    Me.Left = 0
    
End Sub
Private Sub mfgBancos_DblClick()

    Dim vCod As Integer 'VARI�VEL QUE RECEBE O COD DO BANCO P/ PESQUISA
    
    With mfgBancos
        .Col = 1
        If Trim(.Text) = "" Then
            Exit Sub
        End If
        vCod = .Text
    End With
    Unload Me
    frmBanco.txtCodBco.Text = vCod
    Call frmBanco.txtCodBco_LostFocus

End Sub

Private Sub txtNome_KeyPress(KeyAscii As Integer)

    If KeyAscii = 13 Then 'enter
        Call cmdPesquisar_Click
    End If
    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)

End Sub
