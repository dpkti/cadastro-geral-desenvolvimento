VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmPrior 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cadastro de Prioridade Banco/Dep�sito"
   ClientHeight    =   2865
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5385
   Icon            =   "frmPrior.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   191
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   359
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fra 
      Caption         =   "Dados"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1515
      Left            =   90
      TabIndex        =   8
      Top             =   930
      Width           =   5205
      Begin VB.CheckBox chkPrior 
         Appearance      =   0  'Flat
         Caption         =   "Prioridade"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   1890
         TabIndex        =   2
         Top             =   1140
         Width           =   1170
      End
      Begin VB.ComboBox cboBanco 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   750
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   690
         Width           =   4260
      End
      Begin VB.ComboBox cboLoja 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   750
         Style           =   2  'Dropdown List
         TabIndex        =   0
         Top             =   270
         Width           =   1956
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         Caption         =   "Banco"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   150
         TabIndex        =   10
         Top             =   750
         Width           =   690
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         Caption         =   "Loja"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   300
         TabIndex        =   9
         Top             =   360
         Width           =   690
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   3
      Top             =   2535
      Width           =   5385
      _ExtentX        =   9499
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   9446
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   810
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   5
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmPrior.frx":23D2
      PICN            =   "frmPrior.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd CmdGravar 
      Default         =   -1  'True
      Height          =   690
      Left            =   1770
      TabIndex        =   6
      TabStop         =   0   'False
      ToolTipText     =   "Gravar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmPrior.frx":30C8
      PICN            =   "frmPrior.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdCancel 
      Height          =   690
      Left            =   2640
      TabIndex        =   7
      TabStop         =   0   'False
      ToolTipText     =   "Limpar Campos"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmPrior.frx":3DBE
      PICN            =   "frmPrior.frx":3DDA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdConsultar 
      Height          =   690
      Left            =   900
      TabIndex        =   11
      TabStop         =   0   'False
      ToolTipText     =   "Consultar Prioridades Atuais"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmPrior.frx":4AB4
      PICN            =   "frmPrior.frx":4AD0
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmPrior"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdCancel_Click()

    cboLoja.ListIndex = -1
    cboBanco.ListIndex = -1
    chkPrior.Value = 0
    cboLoja.SetFocus
    
End Sub

Private Sub cmdConsultar_Click()

    If vVB_Generica_001.Perguntar("Confirma Pesquisa?") = 7 Then
        Exit Sub
    End If
    Call Aguardar
    Call vVB_Generica_001.ExcluiBind(vBanco)
    Call Criar_Cursor(vBanco, "vCursor")
    vBanco.Parameters.Add "vCOD_BANCO", 1, 1
    vBanco.Parameters.Add "vCOD_LOJA", 1, 1
    vBanco.Parameters.Add "vSelect", 3, 1
    vSql = "PRODUCAO.PCK_CAD190.PR_COD_LOJA(:vCursor,:vCOD_LOJA,:vCOD_BANCO,:vSelect)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    Unload frmAguardar
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro)
        Exit Sub
    Else
        Set vObjOracle = vParameters("vCursor").Value
    End If
    If vObjOracle.EOF Then
        Call vVB_Generica_001.Informar("N�o h� dados cadastrados!!")
        Exit Sub
    End If
    Call vVB_Generica_001.CarregaGridTabela(frmConsPrior.mfgPriorBancos, vObjOracle, 4)
    frmConsPrior.Show 1
    
End Sub

Private Sub CmdGravar_Click()

    If cboLoja.ListIndex = -1 Or cboBanco.ListIndex = -1 Then
        Call vVB_Generica_001.Informar("Todos os Campos Devem Estar Selecionados!")
        cboLoja.SetFocus
        Exit Sub
    End If
    If vVB_Generica_001.Perguntar("Confirma os Dados?") = 7 Then
        Exit Sub
    End If
    
    Call vVB_Generica_001.ExcluiBind(vBanco)
    Call Criar_Cursor(vBanco, "vCursor")
    If Mid(cboBanco, 2, 2) = " -" Then
        vBanco.Parameters.Add "vCOD_BANCO", Val(Left(cboBanco, 1)), 1
    Else
        vBanco.Parameters.Add "vCOD_BANCO", Val(Left(cboBanco, 3)), 1
    End If
    vBanco.Parameters.Add "vCOD_LOJA", Val(Left(cboLoja, 3)), 1
    vBanco.Parameters.Add "vSelect", 1, 1
    vSql = "PRODUCAO.PCK_CAD190.PR_COD_LOJA(:vCursor,:vCOD_LOJA,:vCOD_BANCO,:vSelect)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro)
        Exit Sub
    Else
        Set vObjOracle = vParameters("vCursor").Value
    End If
    If vObjOracle.RecordCount = 0 Then
        vSql = "PRODUCAO.PCK_CAD190.PR_INSERT_LOJA_BANCO(:vCOD_LOJA,:vCOD_BANCO)"
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        If vErro <> "" Then
            vVB_Generica_001.ProcessaErro (vErro)
            Exit Sub
        End If
    Else
        vBanco.Parameters.Remove "vCursor"
        Call Criar_Cursor(vBanco, "vCursor")
        vBanco.Parameters.Remove "vSelect"
        vBanco.Parameters.Add "vSelect", 2, 1
        vSql = "PRODUCAO.PCK_CAD190.PR_COD_LOJA(:vCursor,:vCOD_LOJA,:vCOD_BANCO,:vSelect)"
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        If vErro <> "" Then
            vVB_Generica_001.ProcessaErro (vErro)
            Exit Sub
        Else
            Set vObjOracle = vParameters("vCursor").Value
        End If
        
        If vObjOracle.RecordCount = 0 Then
            If chkPrior = 1 Then
            
                vBanco.Parameters.Remove "vSQL"
                vBanco.Parameters.Add "vSQL", "SQL2", 1
                vSql = "PRODUCAO.PCK_CAD190.PR_UPDATE_LOJA_BANCO(:vCOD_LOJA,:vCOD_BANCO,:vSQL)"
                vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
                If vErro <> "" Then
                    vVB_Generica_001.ProcessaErro (vErro)
                    Exit Sub
                End If
                
                vBanco.Parameters.Remove "vSQL"
                vBanco.Parameters.Add "vSQL", "SQL3", 1
                vSql = "PRODUCAO.PCK_CAD190.PR_UPDATE_LOJA_BANCO(:vCOD_LOJA,:vCOD_BANCO,:vSQL)"
                vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
                If vErro <> "" Then
                    vVB_Generica_001.ProcessaErro (vErro)
                    Exit Sub
                End If
                
            Else
            
                vBanco.Parameters.Remove "vSQL"
                vBanco.Parameters.Add "vSQL", "SQL4", 1
                vSql = "PRODUCAO.PCK_CAD190.PR_UPDATE_LOJA_BANCO(:vCOD_LOJA,:vCOD_BANCO,:vSQL)"
                vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
                If vErro <> "" Then
                    vVB_Generica_001.ProcessaErro (vErro)
                    Exit Sub
                End If
            End If
        
        Else
            
            vBanco.Parameters.Remove "vSQL"
            vBanco.Parameters.Add "vSQL", "SQL5", 1
            vSql = "PRODUCAO.PCK_CAD190.PR_UPDATE_LOJA_BANCO(:vCOD_LOJA,:vCOD_BANCO,:vSQL)"
            vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
            If vErro <> "" Then
                vVB_Generica_001.ProcessaErro (vErro)
                Exit Sub
            End If
            
            vBanco.Parameters.Remove "vSQL"
            vBanco.Parameters.Add "vSQL", "SQL6", 1
            vSql = "PRODUCAO.PCK_CAD190.PR_UPDATE_LOJA_BANCO(:vCOD_LOJA,:vCOD_BANCO,:vSQL)"
            vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
            If vErro <> "" Then
                vVB_Generica_001.ProcessaErro (vErro)
                Exit Sub
            End If
            
        End If
          
    End If
    
    Call vVB_Generica_001.Informar("Cadastro Registrado!")
    Call cmdCancel_Click
    
End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    
    Me.Top = 0
    Me.Left = 0
    
        '-----------Carrega Bancos----------------------
    Call vVB_Generica_001.ExcluiBind(vBanco)
    Call Criar_Cursor(vBanco, "vCursor")
    vBanco.Parameters.Add "vCodBanco", Null, 1
    vBanco.Parameters.Add "vSigla", Null, 1
    vBanco.Parameters.Add "vNome", Null, 1
    vBanco.Parameters.Add "vDivisao", Null, 1
    vBanco.Parameters.Add "vFlVenda", Null, 1
    vBanco.Parameters.Add "vFlDeposito", Null, 1
    vBanco.Parameters.Add "vCodErro", Null, 1
    vBanco.Parameters.Add "vTxtErro", Null, 1
    vSql = "producao.pck_generica.pr_banco(:vCursor,:vCodBanco,:vSigla,:vNome,:vDivisao,:vFlVenda,:vFlDeposito,:vCodErro,:vTxtErro)"
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        End
    Else
        Set vObjOracle = vParameters("vCursor").Value
    End If
    Call vVB_Generica_001.PreencheComboList(vObjOracle, cboBanco, "Banco", "Nome_Banco")
    '-----------Carrega Lojas-------------------
    Call vVB_Generica_001.ExcluiBind(vBanco)
    Set vObjOracle = vVB_Generica_001.TabelaLoja(vBanco)
    Call vVB_Generica_001.PreencheComboList(vObjOracle, cboLoja, "DEP�SITO")
    cboLoja.RemoveItem (4)
    
End Sub
