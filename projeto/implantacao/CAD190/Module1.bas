Attribute VB_Name = "Module1"
Option Explicit
Public vParameters As Object
Public fl_Alt As Byte          'Flag de Inclus�o=0/Altera��o=1
Public vModo As Byte           'variavel de escolha de formulario
Public CurControl As Control
Public fl_Achou As Byte

Sub Criar_Cursor(Banco As Object, pNomeCursor As String)

    vParameters.Remove pNomeCursor
    vParameters.Add pNomeCursor, 0, 3
    vParameters(pNomeCursor).ServerType = 102
    vParameters(pNomeCursor).DynasetOption = &H2&
    vParameters(pNomeCursor).DynasetCacheParams 256, 16, 20, 2000, 0
    
End Sub

Function VERIFICA_CAMPOS(frm As Form) As Boolean

    frm.txtDespCart.Text = "0"
    
    For Each CurControl In frm.Controls
        If TypeOf CurControl Is TextBox Or _
        TypeOf CurControl Is ComboBox Then
            If CurControl.Text = "" Then
                VERIFICA_CAMPOS = False
                Call vVB_Generica_001.Informar("Campo(s) N�o Pode(m) Estar em Branco!")
                Exit Function
            End If
        End If
    Next
    
    VERIFICA_CAMPOS = True
    
End Function


