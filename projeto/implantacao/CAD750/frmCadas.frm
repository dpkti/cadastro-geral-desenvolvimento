VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "grid32.ocx"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form frmCadastro 
   Caption         =   "CAD750 - Cadastro Autom�tico de Itens DPA por Leitura de Arquivo"
   ClientHeight    =   7095
   ClientLeft      =   570
   ClientTop       =   1350
   ClientWidth     =   12300
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7095
   ScaleWidth      =   12300
   StartUpPosition =   1  'CenterOwner
   Begin Threed.SSFrame SSFrame2 
      Height          =   6915
      Left            =   60
      TabIndex        =   0
      Top             =   90
      Width           =   12150
      _Version        =   65536
      _ExtentX        =   21431
      _ExtentY        =   12197
      _StockProps     =   14
      ForeColor       =   12582912
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin Threed.SSFrame SSFrame3 
         Height          =   1095
         Left            =   180
         TabIndex        =   1
         Top             =   285
         Width           =   8940
         _Version        =   65536
         _ExtentX        =   15769
         _ExtentY        =   1931
         _StockProps     =   14
         Caption         =   "Arquivo"
         ForeColor       =   12582912
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Begin Threed.SSCommand cmdArquivo 
            Height          =   540
            Left            =   165
            TabIndex        =   3
            ToolTipText     =   "Abrir arquivo."
            Top             =   420
            Width           =   630
            _Version        =   65536
            _ExtentX        =   1111
            _ExtentY        =   952
            _StockProps     =   78
            Picture         =   "frmCadas.frx":0000
         End
         Begin MSComDlg.CommonDialog controle 
            Left            =   8295
            Top             =   150
            _ExtentX        =   847
            _ExtentY        =   847
            _Version        =   393216
            DefaultExt      =   "*.PRN"
            InitDir         =   "c:\cad750"
         End
         Begin VB.Label lblHelp1 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H0000FFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Abrir Arquivo"
            ForeColor       =   &H80000008&
            Height          =   225
            Left            =   855
            TabIndex        =   11
            Top             =   420
            Visible         =   0   'False
            Width           =   930
         End
         Begin VB.Label lblArquivo 
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   840
            TabIndex        =   2
            Top             =   690
            Width           =   7950
         End
      End
      Begin Threed.SSCommand scmdGravaEmergencia 
         Height          =   435
         Left            =   10080
         TabIndex        =   10
         Top             =   5925
         Width           =   1875
         _Version        =   65536
         _ExtentX        =   3307
         _ExtentY        =   767
         _StockProps     =   78
         Caption         =   "Grava Emerg�ncia"
         Picture         =   "frmCadas.frx":031A
      End
      Begin Threed.SSCommand cmdVoltar 
         Height          =   975
         Left            =   11115
         TabIndex        =   7
         Top             =   390
         Width           =   840
         _Version        =   65536
         _ExtentX        =   1482
         _ExtentY        =   1720
         _StockProps     =   78
         Picture         =   "frmCadas.frx":0336
      End
      Begin Threed.SSCommand cmdConfirma 
         Height          =   975
         Left            =   10155
         TabIndex        =   6
         Top             =   390
         Width           =   840
         _Version        =   65536
         _ExtentX        =   1482
         _ExtentY        =   1720
         _StockProps     =   78
         Picture         =   "frmCadas.frx":0650
      End
      Begin Threed.SSCommand cmdLog 
         Height          =   975
         Left            =   9195
         TabIndex        =   4
         Top             =   390
         Width           =   840
         _Version        =   65536
         _ExtentX        =   1482
         _ExtentY        =   1720
         _StockProps     =   78
         Picture         =   "frmCadas.frx":096A
      End
      Begin MSGrid.Grid grdArquivo 
         Height          =   4200
         Left            =   195
         TabIndex        =   8
         Top             =   1650
         Width           =   11775
         _Version        =   65536
         _ExtentX        =   20770
         _ExtentY        =   7408
         _StockProps     =   77
         ForeColor       =   8388608
         BackColor       =   16777215
         Cols            =   28
      End
      Begin VB.Label lblMsg 
         AutoSize        =   -1  'True
         Caption         =   "Mensagem"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   240
         Left            =   195
         TabIndex        =   9
         Top             =   5955
         Width           =   1155
      End
      Begin VB.Label lblHelp2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H0000FFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Gerar Arq.de Log"
         ForeColor       =   &H80000008&
         Height          =   225
         Left            =   9930
         TabIndex        =   5
         Top             =   1380
         Visible         =   0   'False
         Width           =   1245
      End
   End
End
Attribute VB_Name = "frmCadastro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim vNomeArq As String
Dim varLinhaArquivo As String   'Recebe a linha lida do arquivo texto
Dim fl_Log As Boolean
Dim vGuardaCol As Integer
Dim vGuardaLin As Long


Sub LIMPA_GRID()

        'Limpa dados do grid
        For I = 1 To grdArquivo.Rows - 1
                grdArquivo.RemoveItem 1
        Next I
        
End Sub





Private Sub cmdArquivo_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp1.Visible = True
End Sub

Private Sub cmdConfirma_Click_ANT()

Dim I As Integer              'Ponteiro de linha do grid
Dim vTamObs As Long           'Guarda a qtd. de caracteres da coluna Observa��o
Dim vObs As String            'Guarda o conte�do da coluna de Observa��es
Dim vFl_SeqExiste As Boolean  'Indica se a sequ�ncia de tabela j� existe ou n�o
Dim vSequencia As Double      'Guarda a sequ�ncia durante a pesquisa do MAX
Dim vSeqArq As String         'Guarda a sequ�ncia que veio no arquivo
Dim vReg_Gravado As Long      'Guarda a qtd. de itens gravados
Dim vCommit As Long           'Guarda a qtd. de itens para commitar de 20 em 20 nos CD's
Dim vLinSemObs As Long        'Guarda a qtd.de linhas sem observa��o
Dim vFl_Edi_2 As String       'Vai dizer se o fornecedor trabalha com EDI ou n�o
Dim vClassif_Forn_2 As String 'Vai dizer a classifica��o do fornecedor
Dim vCod_Fabr_Edi As String   'Vai conter o c�digo de f�brica edi
Dim vCD As String             'Vai conter o c�digo de loja para grava��o do item no cd
Dim vDep As String            'Vai conter o link do cd p/ grava��o no cd
Dim vNomeFanta As String      'Vai conter o nome fantasia do cd
Dim objconexao As Object      'Guarda resultado do select na LOJA_CONEXAO
Dim cmdPlSql As String        'Comando SQL da LOJA_CONEXAO
Dim fl_Conexao As Boolean     'Seta se pingou bem ou n�o no CD

If MsgBox("Confirma inclus�o dos itens?", vbYesNo, "CAD750") = vbYes Then

    'Primeiro vai percorrer todo o grid p/ ver se tem itens a serem gravados
    CmdPl = ""
    vLinSemObs = 0
    For I = 1 To grdArquivo.Rows - 1
        grdArquivo.Row = I
        grdArquivo.Col = 22
        vObs = grdArquivo.Text
        'Se a coluna de Observa��o n�o contiver nenhuma mensagem de inconsist�ncia vai gravar
        If vObs = "" Then
            vLinSemObs = vLinSemObs + 1
        End If
    Next I
    If vLinSemObs = 0 Then
        MsgBox "Nenhum item poder� ser cadastrado!", vbCritical, "Aten��o!"
        Exit Sub
    End If
    
    'Se houverem itens a serem gravados, vai iniciar todo o processo
    'Zera contador de linhas gravadas
    vReg_Gravado = 0

    db.Parameters.Remove "DTCA": db.Parameters.Add "DTCA", vDt_Cad, 1
    db.Parameters.Remove "AAMM": db.Parameters.Add "AAMM", vAnoMes, 1
    db.Parameters.Remove "cod_errora": db.Parameters.Add "cod_errora", 0, 2
    db.Parameters.Remove "txt_errora": db.Parameters.Add "txt_errora", "", 2
    
    'Pega linha a linha do grid para gravar as que cont�m campo de Observa��o <> ""
    For I = 1 To grdArquivo.Rows - 1
        
       
        grdArquivo.Row = I
        grdArquivo.Col = 22
        vObs = Trim(grdArquivo.Text)
        
        'Se a coluna de Observa��o n�o contiver nenhuma mensagem de inconsist�ncia vai gravar
        If vObs = "" Then
            
            '//////////////////////////////////////////////
            'Vai alimentar as vari�veis normais e de bind
            '/////////////////////////////////////////////
            'Limpa vari�veis auxiliares de grava��o
            CmdPl = ""
            vFl_Edi_2 = ""
            vClassif_Forn_2 = ""
            vCod_Fabr_Edi = ""
            
            'Pega informa��es das 2 �ltimas colunas, para serem usadas como auxiliares na grava��o
            grdArquivo.Col = 20
            vCod_Fabr_Edi = Trim(grdArquivo.Text)
            grdArquivo.Col = 24
            vFl_Edi_2 = Trim(grdArquivo.Text)
            grdArquivo.Col = 25
            vClassif_Forn_2 = Trim(grdArquivo.Text)
            
            'Guarda informa��es da linha em refer�ncia do grid, nas vari�veis de bind
            grdArquivo.Col = 1
            db.Parameters.Remove "FORN" & I: db.Parameters.Add "FORN" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 2
            db.Parameters.Remove "FABR" & I: db.Parameters.Add "FABR" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 3
            db.Parameters.Remove "DESC" & I: db.Parameters.Add "DESC" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 4
            db.Parameters.Remove "LINH" & I: db.Parameters.Add "LINH" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 5
            db.Parameters.Remove "GRUP" & I: db.Parameters.Add "GRUP" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 6
            db.Parameters.Remove "SUBG" & I: db.Parameters.Add "SUBG" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 7
            db.Parameters.Remove "MASC" & I: db.Parameters.Add "MASC" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 9
            db.Parameters.Remove "CLAS" & I: db.Parameters.Add "CLAS" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 10
            db.Parameters.Remove "PESO" & I: db.Parameters.Add "PESO" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 11
            db.Parameters.Remove "VOLU" & I: db.Parameters.Add "VOLU" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 12
            db.Parameters.Remove "UNID" & I: db.Parameters.Add "UNID" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 13
            db.Parameters.Remove "TICM" & I: db.Parameters.Add "TICM" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 14
            db.Parameters.Remove "TIPI" & I: db.Parameters.Add "TIPI" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 15
            db.Parameters.Remove "PIPI" & I: db.Parameters.Add "PIPI" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 16
            db.Parameters.Remove "PROC" & I: db.Parameters.Add "PROC" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 17
            db.Parameters.Remove "MINF" & I: db.Parameters.Add "MINF" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 18
            db.Parameters.Remove "MINV" & I: db.Parameters.Add "MINV" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 19
            db.Parameters.Remove "MAXV" & I: db.Parameters.Add "MAXV" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 20
            db.Parameters.Remove "CEDI" & I: db.Parameters.Add "CEDI" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 21
            db.Parameters.Remove "CATE" & I: db.Parameters.Add "CATE" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 26
            db.Parameters.Remove "MINEST" & I: db.Parameters.Add "MINEST" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 27
            db.Parameters.Remove "FABRICANTE" & I: db.Parameters.Add "FABRICANTE" & I, Trim(grdArquivo.Text), 1
            
            '//////////////////////////////////////
            'PROCESSO PARA O PR�XIMO C�DIGO DPK
            '//////////////////////////////////////
                        
            '//////////////////////////////////////////////////////////////////////////////////////////////////////////
            'Altera��o para utiliza��o da sequence de Cod.DPK - Alethea - 03/08/06
            
            'Todo este processo foi substituido pelo comando de Insert na ITEM_CADASTRO, pegando diretamente o NextVal - Alethea - 03/08/06
            
            'CmdSql = "Select PRODUCAO.Seq_Cddpk.Nextval DPK From DUAL "
            'Set db_CONS = db.dbCreateDynaset(CmdSql, 0&)
            'If db_CONS.EOF Then
            '    MsgBox "Problemas para pegar a pr�xima sequ�ncia do C�d.DPK! " & Chr(13) & _
            '           "Ligue para o analista respons�vel.", vbCritical, "Aten��o!"
            '    Exit Sub
            'Else
            '    db.Parameters.Remove "CODDPK" & I: db.Parameters.Add "CODDPK" & I, Val(db_CONS!DPK), 1
            '    'db.Parameters.Remove "CODDPK" & I: db.Parameters.Add "CODDPK" & I, 56322, 1
            'End If
            
            '//////////////////////////////////////////////////////////////////////////////////////////////////////////
            'Este processo abaixo d� certo e n�o pula sequencia da numera��o mas usar o insert direto � mais garantido - Alethea - 03/08/06
            
            ''Substitui��o para excluir bloco an�nimos
            'Set OraParameters = db.Parameters
            'OraParameters.Remove "vCursor": OraParameters.Add "vCursor", 0, 2
            'OraParameters("vCursor").serverType = ORATYPE_CURSOR
            'OraParameters("vCursor").DynasetOption = ORADYN_NO_BLANKSTRIP
            'OraParameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
            'OraParameters.Remove "vErro":   OraParameters.Add "vErro", 0, 2
            'db.Parameters.Remove "txt_errora": db.Parameters.Add "txt_errora", 0, 2
            'CmdSql = "Begin PRODUCAO.Pck_CAD750.Pr_Seq_Dpk(:vCursor,:vErro,:txt_errora); End; "
            'db.ExecuteSQL CmdSql
            'Set db_CONS = OraParameters("vCursor").Value
            'vERRO = IIf(db.Parameters("verro").Value = Null, 0, db.Parameters("verro").Value)
            'If Val(vERRO) <> 0 Then
            '    Call Process_Line_Errors
            '    MsgBox "Problemas para pegar a pr�xima sequ�ncia do C�d.DPK! " & Chr(13) & _
            '           "Ligue para o analista respons�vel.", vbCritical, "Aten��o!"
            '    Exit Sub
            'Else
            '    db.Parameters.Remove "CODDPK" & I: db.Parameters.Add "CODDPK" & I, Val(db_CONS!DPK), 1
            'End If
            
            'Coloca na 24.coluna do grid, o c�digo DPK que foi atribuido ao item que ser� cadastrado
            'Isso vai facilitar no momento de gravar os itens nos CD's
            'grdArquivo.Col = 23
            'grdArquivo.Text = Val(Trim(db_CONS!DPK))
            'grdArquivo.Text = Trim(56322)
            
            '//////////////////////////////////////////////////////////////////////////////////////////////////////////
            
            
            
            
            '//////////////////////////////////////
            'PROCESSO PARA ENCONTRAR MAX DA SEQ.TAB
            '//////////////////////////////////////
            
            'Guarda numa vari�vel, a sequ�ncia que veio no arquivo
            grdArquivo.Col = 8
            vSeqArq = Trim(grdArquivo.Text)
            
            'Se n�o veio nenhuma sequ�ncia no arquivo, vai verificar qual poder� ser usada
            If vSeqArq = "" Then
                
                'Verifica na tabela item_global o maior sequencia cadastrada para este fornecedor e
                'soma mais 200 para aplicar a este item
                CmdSql = "Select Max(Nvl(g.sequencia_tab,0)) SEQ " & _
                         "From ITEM_ESTOQUE e, ITEM_GLOBAL g, ITEM_CADASTRO c " & _
                         "Where e.cod_loja = 1 " & _
                         "And c.cod_fornecedor = :FORN" & I & " " & _
                         "And e.cod_dpk = g.cod_dpk " & _
                         "And g.cod_dpk = c.cod_dpk "
                Set db_CONS = db.DbCreateDynaset(CmdSql, 0&)
                
                'Se select n�o trouxer nada � porque houve algum problema com o fornecedor
                If db_CONS.EOF Then
                    MsgBox "Problemas para encontrar pr�xima sequ�ncia de tabela!" & Chr(13) & _
                           "Verifique o fornecedor!", vbCritical, "Aten��o!"
                    Exit Sub
                'Se a seq. trazida for um no. que n�o d� para somar 200, faz outro select
                ElseIf db_CONS!SEQ > 999799 Then
                
                    vFl_SeqExiste = True        'Indica que a sequ�ncia encontrada n�o pode ser usada
                    vSequencia = db_CONS!SEQ    'Guarda o valor da sequ�ncia encontrada
                    'Atribui � vari�vel de bind a primeira sequ�ncia encntrada (> 999799)
                    db.Parameters.Remove "SEQ" & I: db.Parameters.Add "SEQ" & I, vSequencia, 1
                    
                    'Enquanto a sequ�ncia encontrada n�o puder ser usada, vai procurar por outra
                    While vFl_SeqExiste = True
                    
                        'Vai procurar a maior sequ�ncia depois da 1a. encontrada (que n�o podia ser usada)
                        CmdSql2 = "Select Max(Nvl(g.sequencia_tab,0)) SEQ " & _
                                  "From ITEM_ESTOQUE e, ITEM_GLOBAL g, ITEM_CADASTRO c " & _
                                  "Where e.cod_loja = 1 " & _
                                  "And c.cod_fornecedor = :FORN" & I & " " & _
                                  "And g.sequencia_tab < :SEQ" & I & " " & _
                                  "And e.cod_dpk = g.cod_dpk " & _
                                  "And g.cod_dpk = c.cod_dpk "
                        Set db_CONS2 = db.DbCreateDynaset(CmdSql2, 0&)
                    
                        vSequencia = db_CONS2!SEQ + 200
                        
                        'Atribui � vari�vel de bind a pr�xima maior sequ�ncia
                        db.Parameters.Remove "SEQ" & I: db.Parameters.Add "SEQ" & I, vSequencia, 1
                    
                        'Verifica se a pr�xima sequ�ncia pode ser usada
                        CmdSql3 = "Select count(*) QtdSeq " & _
                                  "From ITEM_ESTOQUE e, ITEM_CADASTRO c, ITEM_GLOBAL g " & _
                                  "Where e.cod_loja = 1 " & _
                                  "And c.cod_fornecedor = :FORN" & I & " " & _
                                  "And g.sequencia_tab = :SEQ" & I & " " & _
                                  "And e.cod_dpk = g.cod_dpk " & _
                                  "And g.cod_dpk = c.cod_dpk "
                        Set db_CONS3 = db.DbCreateDynaset(CmdSql3, 0&)
                    
                        'Se ainda n�o existir, seta flag que encontrou
                        If db_CONS3!QtdSeq = 0 Then
                            vFl_SeqExiste = False
                        Else    'Se existir, continua procurando o m�x. anterior
                            vFl_SeqExiste = True
                        End If
                    
                    Wend
    
                'Verifica se a seq.trazida + 200 pode ser usada
                Else
                    
                    vSequencia = db_CONS!SEQ + 200  'Guarda o valor da sequ�ncia encontrada
                        
                    'Atribui � vari�vel de bind a primeira sequ�ncia encntrada
                    db.Parameters.Remove "SEQ" & I: db.Parameters.Add "SEQ" & I, vSequencia, 1
                    
                    'Verifica se a pr�xima sequ�ncia poide ser usada
                    CmdSql3 = "Select count(*) QtdSeq " & _
                              "From ITEM_ESTOQUE e, ITEM_CADASTRO c, ITEM_GLOBAL g " & _
                              "Where e.cod_loja = 1 " & _
                              "And c.cod_fornecedor = :FORN" & I & " " & _
                              "And g.sequencia_tab = :SEQ" & I & " " & _
                              "And e.cod_dpk = g.cod_dpk " & _
                              "And g.cod_dpk = c.cod_dpk "
                        
                    Set db_CONS3 = db.DbCreateDynaset(CmdSql3, 0&)
                    
                    'Se seq. puder ser usada, seta flag que encontrou
                    If db_CONS3!QtdSeq = 0 Then
                        vFl_SeqExiste = False
                        
                    'Se j� existir, vai procurando o Max abaixo do �ltimo encontrado, at� achar um que possa ser usado
                    Else
                        
                        vFl_SeqExiste = True        'Indica que a sequ�ncia encontrada n�o pode ser usada
                        vSequencia = db_CONS!SEQ    'Guarda o valor da sequ�ncia encontrada
                        
                        'Atribui � vari�vel de bind a primeira sequ�ncia encontrada (que j� existe)
                        db.Parameters.Remove "SEQ" & I: db.Parameters.Add "SEQ" & I, vSequencia, 1
                    
                        'Enquanto a sequ�ncia encontrada n�o puder ser usada, vai procurar por outra
                        While vFl_SeqExiste = True
                            
                            'Vai procurar a maior sequ�ncia depois da 1a. encontrada (que n�o podia ser usada)
                            CmdSql2 = "Select Max(Nvl(g.sequencia_tab)) SEQ " & _
                                      "From ITEM_ESTOQUE e, ITEM_GLOBAL g, ITEM_CADASTRO c " & _
                                      "Where e.cod_loja = 1 " & _
                                      "And c.cod_fornecedor = :FORN " & _
                                      "And g.sequencia_tab < :SEQ " & _
                                      "And e.cod_dpk = g.cod_dpk " & _
                                      "And g.cod_dpk = c.cod_dpk "
                            Set db_CONS2 = db.DbCreateDynaset(CmdSql2, 0&)
                        
                            vSequencia = db_CONS2!SEQ + 200
                            
                            'Atribui � vari�vel de bind a pr�xima maior sequ�ncia
                            db.Parameters.Remove "SEQ" & I: db.Parameters.Add "SEQ" & I, vSequencia, 1
                        
                            'Verifica se a pr�xima sequ�ncia pode ser usada
                            CmdSql3 = "Select count(*) QtdSeq " & _
                                      "From ITEM_ESTOQUE e, ITEM_CADASTRO c, ITEM_GLOBAL g " & _
                                      "Where e.cod_loja = 1 " & _
                                      "And c.cod_fornecedor = :FORN" & I & " " & _
                                      "And g.sequencia_tab = :SEQ" & I & " " & _
                                      "And e.cod_dpk = g.cod_dpk " & _
                                      "And g.cod_dpk = c.cod_dpk "
                            Set db_CONS3 = db.DbCreateDynaset(CmdSql3, 0&)
                        
                            'Se n�o existir, seta flag que encontrou
                            If db_CONS3!QtdSeq = 0 Then
                                vFl_SeqExiste = False
                            Else
                                vFl_SeqExiste = True
                            End If
                        
                        Wend
    
                    End If
                    
                End If
            Else
                'vSequencia = Val(vSeqArq)
                db.Parameters.Remove "SEQ" & I: db.Parameters.Add "SEQ" & I, Val(vSeqArq), 1
            End If
            
            'Atribui � vari�vel de bind, a pr�xima sequ�ncia de tabela encontrada
            'db.Parameters.Remove "SEQT" & I: db.Parameters.Add "SEQT" & I, Trim(vSequencia), 1
            
            'Coloca na 9a. coluna do grid, a sequ�ncia que foi encontrada p/o item que ser� cadastrado
            'Isso vai facilitar no momento de gravar os itens nos CD's
            If Trim(vSequencia) <> 0 Then
                grdArquivo.Col = 8
                grdArquivo.Text = Trim(vSequencia)
            End If
    
            '//////////////////////////////////////
            'PROCESSO PARA GRAVAR ITEM EM CAMPINAS
            '//////////////////////////////////////
            
            '//////////////////////////////////////////////////////////////////////////////////////////////////////////
            'Altera��o para utiliza��o da sequence de Cod.DPK - Alethea - 03/08/06
            
            'CmdPl = CmdPl & _
                    "Insert into ITEM_CADASTRO " & _
                    "(cod_Dpk, cod_Fornecedor, cod_fabrica, desc_item, cod_linha," & _
                    " cod_grupo, cod_subgrupo, dt_cadastramento, mascarado, class_fiscal," & _
                    " peso, volume, cod_unidade, cod_tributacao, cod_tributacao_ipi," & _
                    " pc_ipi, cod_procedencia, cod_dpk_ant, qtd_minforn, qtd_minvda) "
            'CmdPl = CmdPl & _
                    "Values " & _
                    "(:CODDPK" & I & ", :FORN" & I & ", :FABR" & I & ", " & _
                    " TRANSLATE(:DESC" & I & ",'" & VAR_CAR_INVALIDO & "','" & VAR_CAR_VALIDO & "'), " & _
                    " :LINH" & I & ", " & _
                    " :GRUP" & I & ", :SUBG" & I & ", To_Date(:DTCA,'DD/MM/RR'), :MASC" & I & ", " & _
                    " :CLAS" & I & ", :PESO" & I & ", :VOLU" & I & ", :UNID" & I & ", " & _
                    " :TICM" & I & ", :TIPI" & I & ", :PIPI" & I & ", :PROC" & I & ", 0, " & _
                    " :MINF" & I & ", :MINV" & I & "); "
    
            'Em 03/08/06 foi feito insert na ITEM_CADASTRO usando diretamente NEXTVAL da sequence pois estava tendo problemas de pular sequencia. - Alethea
            CmdPl = CmdPl & _
                    "Insert into ITEM_CADASTRO " & _
                    "(cod_Dpk, cod_Fornecedor, cod_fabrica, desc_item, cod_linha," & _
                    " cod_grupo, cod_subgrupo, dt_cadastramento, mascarado, class_fiscal," & _
                    " peso, volume, cod_unidade, cod_tributacao, cod_tributacao_ipi," & _
                    " pc_ipi, cod_procedencia, cod_dpk_ant, qtd_minforn, qtd_minvda) "
            CmdPl = CmdPl & _
                    "Values " & _
                    "(PRODUCAO.Seq_Cddpk.Nextval, :FORN" & I & ", :FABR" & I & ", " & _
                    " TRANSLATE(:DESC" & I & ",'" & VAR_CAR_INVALIDO & "','" & VAR_CAR_VALIDO & "'), " & _
                    " :LINH" & I & ", " & _
                    " :GRUP" & I & ", :SUBG" & I & ", To_Date(:DTCA,'DD/MM/RR'), :MASC" & I & ", " & _
                    " :CLAS" & I & ", :PESO" & I & ", :VOLU" & I & ", :UNID" & I & ", " & _
                    " :TICM" & I & ", :TIPI" & I & ", :PIPI" & I & ", :PROC" & I & ", 0, " & _
                    " :MINF" & I & ", :MINV" & I & "); "
    
            CmdPl = "BEGIN " & CmdPl & " Commit; " & _
                        "Exception " & _
                            "When Others then " & _
                                "RollBack; " & _
                        ":Cod_Errora := SQLCODE; " & _
                        ":Txt_Errora := SQLERRM; " & _
                    "End;"
            
            'Executa transa��es
            db.ExecuteSQL CmdPl
            If IIf(IsNull(db.Parameters("cod_errora")), 0, Val(db.Parameters("cod_errora"))) <> 0 Then
                If Val(db.Parameters("cod_errora")) = -1 Then
                    If MsgBox("Item j� cadastrado!" & Chr(13) & "Forn./F�br. referente � " & I & "� linha do arquivo." & Chr(13) & "Deseja continuar a processar demais linhas do arquivo?", vbYesNo, "ATEN��O!") = vbYes Then
                        GoTo PROXIMO_ITEM
                    Else
                        Exit Sub
                    End If
                Else
                    MsgBox db.Parameters("txt_errora") & ". Ligue p/ Depto.Sistemas"
                End If
                Exit Sub
            End If
            CmdPl = ""
    
            'Agora fa�o select para pegar o c�digo DPK que foi atribuido
            CmdSql3 = "Select Cod_Dpk " & _
                      "From ITEM_CADASTRO " & _
                      "Where cod_fornecedor = :FORN" & I & " " & _
                      "And cod_fabrica = :FABR" & I & " "
            Set db_CONS3 = db.DbCreateDynaset(CmdSql3, 0&)
        
            If db_CONS3.EOF Then
                MsgBox "Problemas para selecionar o c�digo DPK atribuido ao " & I & "� item do arquivo! " & Chr(13) & _
                       "Favor abrir chamado no HelpDesk.", vbCritical, "Aten��o!"
                Exit Sub
            Else
                db.Parameters.Remove "CODDPK" & I: db.Parameters.Add "CODDPK" & I, Val(db_CONS3!cod_dpk), 1
            End If
            
            'Coloca na 24.coluna do grid, o c�digo DPK que foi atribuido ao item cadastrado
            'Isso vai facilitar no momento de gravar os itens nos CD's
            grdArquivo.Col = 23
            grdArquivo.Text = Val(Trim(db_CONS3!cod_dpk))
            
            '//////////////////////////////////////////////////////////////////////////////////////////////////////////
            
            'Continua os cadastros nas demais tabelas
    
            'EM 01/11/07 (IMPLEMENTADO EM 04/03/08) ALTERADO PARA SEMPRE QUE FOR ITEM NOVO, ALTERAR PARA CATEG. "E"
    
            'CmdPl = CmdPl & _
                    "Insert into ITEM_ANALITICO " & _
                    "(cod_loja, cod_dpk, categoria, qtd_mes_ant, custo_medio, custo_medio_ant, " & _
                    " class_abc, class_abcf, class_venda) " & _
                    "Values " & _
                    "(1, :CODDPK" & I & ", :CATE" & I & ", 0, 0, 0, 'D', 'D', 99999); "
    
            CmdPl = CmdPl & _
                    "Insert into ITEM_ANALITICO " & _
                    "(cod_loja, cod_dpk, categoria, qtd_mes_ant, custo_medio, custo_medio_ant, " & _
                    " class_abc, class_abcf, class_venda) " & _
                    "Values " & _
                    "(1, :CODDPK" & I & ", :CATE" & I & ", 0, 0, 0, 'E', 'E', 99999); "
        
            'A partir de 01/11/07 (IMPLEMENTADO EM 04/03/08), incluir tb. na tabela Compras.Item_Curva_ABC, sendo que itens novos receber�o classifica��o "E"
            CmdPl = CmdPl & _
                " INSERT INTO compras.item_curva_abc " & _
                "(Cod_dpk" & I & ", Class_ABC, Class_Venda, Class_ABCF, Pc_Acum_ABCF ) " & _
                "VALUES (:CODDPK, 'E', 99999, 'E', 0); "

    
            CmdPl = CmdPl & _
                    "Insert into Producao.R_ITEM_FABRICANTE " & _
                    "(Cod_Dpk, Cod_Fabricante) Values " & _
                    "(:CODDPK" & I & ", :FABRICANTE" & I & "); "
    
            CmdPl = CmdPl & _
                    "Insert into ITEM_GLOBAL " & _
                    "(cod_dpk, preco_compra, preco_compra_ant, sequencia_tab) " & _
                    "Values " & _
                    "(:CODDPK" & I & ", 0, 0, :SEQ" & I & "); "
    
            CmdPl = CmdPl & _
                    "Insert into ITEM_PRECO " & _
                    "(cod_loja, cod_dpk, preco_venda, preco_venda_ant, preco_of," & _
                    " preco_of_ant, preco_sp, preco_sp_ant) " & _
                    "Values " & _
                    "(1, :CODDPK" & I & ", 0, 0, 0, 0, 0, 0); "
        
            CmdPl = CmdPl & _
                    "Insert into ITEM_ESTOQUE " & _
                    "(cod_loja, cod_dpk, qtd_atual, qtd_reserv, qtd_pendente, qtd_maxvda," & _
                    " cuema, dt_cuema, vl_ult_compra, dt_ult_compra, situacao) " & _
                    "Values " & _
                    "(1, :CODDPK" & I & ", 0, 0, 0, :MAXV" & I & ", 0, NULL, 0, NULL, 0); "
                    
            CmdPl = CmdPl & _
                    "Insert into ITEM_CUSTO " & _
                    "(cod_loja, cod_dpk, custo_reposicao) " & _
                    "Values " & _
                    "(1, :CODDPK" & I & ", 0); "
                    
            CmdPl = CmdPl & _
                    "Insert into TRANSFERENCIA.ESTOQUE_MINIMO " & _
                    "(cod_dpk, min_estoque) " & _
                    "Values " & _
                    "(:CODDPK" & I & ", :MINEST" & I & " ); "
                    
            'Se a classifica��o do fornecedor for "A" - REVENDA, insere em outras tabelas
            If vClassif_Forn_2 = "A" Then
            
                CmdPl = CmdPl & _
                        "Insert into ESTATISTICA_ITEM " & _
                        "(cod_loja, cod_dpk, ano_mes, qtd_compra, qtd_venda," & _
                        " qtd_venda_sp, qtd_venda_cp) " & _
                        "Values " & _
                        "(1, :CODDPK" & I & ", :AAMM, 0, 0, 0, 0); "
            
                CmdPl = CmdPl & _
                        "Insert into COMPRAS.TBITEM " & _
                        "(cod_dpk, preco_venda, preco_of, preco_sp, preco_compra," & _
                        " pc_desc1, pc_desc2, pc_icm, pc_margem_vda, prazo_fabrica," & _
                        " fator_compra, fator_venda) " & _
                        "Values " & _
                        "(:CODDPK" & I & ", 0.00, 0.00, 0.00, 0.000, 0.00, 0.00, 0.00, 0.00," & _
                        " 0, 0.0000, 0.0000); "
            
                CmdPl = CmdPl & _
                        "Insert into COMPRAS.TBITEM_PRECO " & _
                        "(cod_loja, cod_dpk, preco_of, preco_sp) " & _
                        "Values " & _
                        "(1, :CODDPK" & I & ", 0.00, 0.00); "
            
                'Se o fornecedor trabalhar com EDI e o c�d.fabr.edi for diferente de branco
                'vai gravar na tabela compra.r_item_edi
                If vFl_Edi_2 = "S" And vCod_Fabr_Edi <> "" Then
                        
                    CmdPl = CmdPl & _
                            "Insert into COMPRAS.R_ITEM_EDI " & _
                            "(cod_dpk, cod_fornecedor, cod_fabrica_edi) " & _
                            "Values " & _
                            "(:CODDPK" & I & ", :FORN" & I & ", :CEDI" & I & "); "
                
                End If
            
            End If
            
            'Executa transa��o gravando o item em Campinas
            'Em CPS � necess�rio gravar de 1 em 1 para n�o se perder a sequ�ncia do DPK e da tabela
            
            CmdPl = "BEGIN " & CmdPl & " Commit; " & _
                        "Exception " & _
                            "When Others then " & _
                                "RollBack; " & _
                        ":Cod_Errora := SQLCODE; " & _
                        ":Txt_Errora := SQLERRM; " & _
                    "End;"
            'Executa transa��es
            'On Error Resume Next
            db.ExecuteSQL CmdPl
        
            If IIf(IsNull(db.Parameters("cod_errora")), 0, Val(db.Parameters("cod_errora"))) <> 0 Then
                MsgBox db.Parameters("txt_errora") & ". Ligue p/ Depto.Sistemas"
                Exit Sub
            End If
        
            vReg_Gravado = vReg_Gravado + 1
            
        'N�o faz nada se a linha do grid contiver msg.de inconsist�ncia no cpo.Observa��o
        End If
    
PROXIMO_ITEM:
    
    
    Next I
    MsgBox vReg_Gravado & " ITENS INCLUIDOS OK EM CAMPINAS!", vbInformation, "Aten��o!"

    'Comentei este trecho pois a inclus�o n�o on-line estava dando muitos problemas na
    'libera��o de NF nos CDs (Aleth�a - Maio/2002)
    'If MsgBox("Confirma inclus�o on-line dos itens nos CD�s?", vbYesNo, "CAD750") = vbYes Then

        '////////////////////////////////////
        'PROCESSO PARA INCLUIR ITENS NOS CD'S - Vai sempre incluir p/Cds que o Cad070 incluir
        '////////////////////////////////////
        
        db.Parameters.Remove "DTCA": db.Parameters.Add "DTCA", vDt_Cad, 1
    '    db.Parameters.Remove "AAMM": db.Parameters.Add "AAMM", vAnoMes, 1
        db.Parameters.Remove "cod_errora": db.Parameters.Add "cod_errora", 0, 2
        db.Parameters.Remove "txt_errora": db.Parameters.Add "txt_errora", 0, 2
        
        CmdSql = "Select l.cod_loja CD, l.nome_fantasia NOME " & _
                 "From LOJA l, DEPOSITO_VISAO d " & _
                 "Where d.cod_loja = l.cod_loja " & _
                 "And d.cod_loja <> 1 " & _
                 "And d.nome_programa = 'CAD070'" & _
                 "Order by l.cod_loja"
        Set db_CONS = db.DbCreateDynaset(CmdSql, 0&)
        
        'Enquanto ainda houverem CD's a incluir os itens, faz o processo
        While Not db_CONS.EOF
        
            vCD = Format(Trim(db_CONS!CD), "00")
            vDep = "DEP" & Format(Trim(db_CONS!CD), "00") & "."
            vNomeFanta = Trim(db_CONS!NOME)
            
            '////////////////////////////////////////////////////////////////////
            'TESTA TIME-OUT NO CD P/ VERIFICAR SE VALE A PENA GRAVAR ITEM ON-LINE
            '////////////////////////////////////////////////////////////////////
            db.Parameters.Remove "LJ": db.Parameters.Add "LJ", Val(vCD), 1
            
            cmdPlSql = "Select cod_loja, ip, time_out " & _
                       "From PRODUCAO.LOJA_CONEXAO " & _
                       "Where cod_loja = :LJ "
            Set objconexao = db.DbCreateDynaset(cmdPlSql, 0&)
                        
            If objconexao.EOF Then
                MsgBox "CD " & vCD & " n�o cadastrada na tabela LOJA_CONEXAO! " & Chr(13) & _
                        "Chame o analista respons�vel.", vbCritical, "Aten��o"
                Exit Sub
            End If
                    
            IP = objconexao!IP
            TIMEOUT = Val(objconexao!TIME_OUT)
            CDLJ = objconexao!cod_loja
            success = Ping(IP, CDLJ, ECHO)
                    
            'Vai verificar se o tempo do ping, compensar a conex�o com o CD
            If InStr(GetStatusCode(success), ("ip success")) = 0 Or _
                ECHO.RoundTripTime > TIMEOUT Then
                MsgBox "CD " & CDLJ & " sem comunica��o!", vbInformation, "Aten��o"
                fl_Conexao = False
            Else
                fl_Conexao = True
            End If
                
            'Caso o timeout com o CD compense, prepara itens para serem gravado no CD
            If fl_Conexao = True Then
                    
                vReg_Gravado = 0
                vCommit = 0
                
                'Pega linha a linha do grid para gravar as que cont�m campo de Observa��o <> ""
                For I = 1 To grdArquivo.Rows - 1
                    
                    grdArquivo.Row = I
                    grdArquivo.Col = 22
                    vObs = grdArquivo.Text
                    
                    'Se a coluna de Observa��o n�o contiver nenhuma mensagem de inconsist�ncia vai gravar
                    If vObs = "" Then
                        
                        vReg_Gravado = vReg_Gravado + 1
                    
                        'Vai alimentar as vari�veis normais e de bind
                        'Limpa vari�veis auxiliares de grava��o
                        vFl_Edi_2 = ""
                        vClassif_Forn_2 = ""
                        vCod_Fabr_Edi = ""
                                    
                        'Pega informa��es das 2 �ltimas colunas, para serem usadas como auxiliares na grava��o
                        grdArquivo.Col = 20
                        vCod_Fabr_Edi = Trim(grdArquivo.Text)
                        grdArquivo.Col = 24
                        vFl_Edi_2 = Trim(grdArquivo.Text)
                        grdArquivo.Col = 25
                        vClassif_Forn_2 = Trim(grdArquivo.Text)
                                    
                        'Guarda informa��es da linha em refer�ncia do grid, nas vari�veis de bind
                        grdArquivo.Col = 1
                        db.Parameters.Remove "FORN" & I: db.Parameters.Add "FORN" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 2
                        db.Parameters.Remove "FABR" & I: db.Parameters.Add "FABR" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 3
                        db.Parameters.Remove "DESC" & I: db.Parameters.Add "DESC" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 4
                        db.Parameters.Remove "LINH" & I: db.Parameters.Add "LINH" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 5
                        db.Parameters.Remove "GRUP" & I: db.Parameters.Add "GRUP" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 6
                        db.Parameters.Remove "SUBG" & I: db.Parameters.Add "SUBG" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 7
                        db.Parameters.Remove "MASC" & I: db.Parameters.Add "MASC" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 9
                        db.Parameters.Remove "CLAS" & I: db.Parameters.Add "CLAS" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 10
                        db.Parameters.Remove "PESO" & I: db.Parameters.Add "PESO" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 11
                        db.Parameters.Remove "VOLU" & I: db.Parameters.Add "VOLU" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 12
                        db.Parameters.Remove "UNID" & I: db.Parameters.Add "UNID" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 13
                        db.Parameters.Remove "TICM" & I: db.Parameters.Add "TICM" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 14
                        db.Parameters.Remove "TIPI" & I: db.Parameters.Add "TIPI" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 15
                        db.Parameters.Remove "PIPI" & I: db.Parameters.Add "PIPI" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 16
                        db.Parameters.Remove "PROC" & I: db.Parameters.Add "PROC" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 17
                        db.Parameters.Remove "MINF" & I: db.Parameters.Add "MINF" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 18
                        db.Parameters.Remove "MINV" & I: db.Parameters.Add "MINV" & I, Trim(grdArquivo.Text), 1
                        'grdArquivo.Col = 19
                        'db.Parameters.Remove "MAXV" & I: db.Parameters.Add "MAXV" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 20
                        db.Parameters.Remove "CEDI" & I: db.Parameters.Add "CEDI" & I, Trim(grdArquivo.Text), 1
                        'GrdArquivo.Col = 21
                        'db.Parameters.Remove "CATE" & I: db.Parameters.Add "CATE" & I, Trim(grdArquivo.Text), 1
                        
                        'Pega cod.DPK e Seq.Tab. atribu�dos para o item no momento da grava��o em CPS
                        grdArquivo.Col = 23
                        db.Parameters.Remove "CODDPK" & I: db.Parameters.Add "CODDPK" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 8
                        db.Parameters.Remove "SEQ" & I: db.Parameters.Add "SEQ" & I, Trim(grdArquivo.Text), 1
                        
                        '//////////////////////////////////////
                        'PROCESSO PARA GRAVAR ITEM NOS CDS
                        '//////////////////////////////////////
                CmdPl = "Insert into " & vDep & "ITEM_CADASTRO " & _
                        "(cod_Dpk, cod_Fornecedor, cod_fabrica, desc_item, cod_linha," & _
                        " cod_grupo, cod_subgrupo, dt_cadastramento, mascarado, class_fiscal," & _
                        " peso, volume, cod_unidade, cod_tributacao, cod_tributacao_ipi," & _
                        " pc_ipi, cod_procedencia, cod_dpk_ant, qtd_minforn, qtd_minvda) "
                CmdPl = CmdPl & _
                        "Values " & _
                        "(:CODDPK" & I & ", :FORN" & I & ", :FABR" & I & ", " & _
                        " TRANSLATE(:DESC" & I & ",'" & VAR_CAR_INVALIDO & "','" & VAR_CAR_VALIDO & "'), " & _
                        " :LINH" & I & ", " & _
                        " :GRUP" & I & ", :SUBG" & I & ", To_Date(:DTCA,'DD/MM/RR'), :MASC" & I & ", " & _
                        " :CLAS" & I & ", :PESO" & I & ", :VOLU" & I & ", :UNID" & I & ", " & _
                        " :TICM" & I & ", :TIPI" & I & ", :PIPI" & I & ", :PROC" & I & ", 0, " & _
                        " :MINF" & I & ", :MINV" & I & "); "
                        vCommit = vCommit + 1
                        
                CmdPl = CmdPl & _
                        "Insert into " & vDep & "ITEM_GLOBAL " & _
                        "(cod_dpk, preco_compra, preco_compra_ant, sequencia_tab) " & _
                        "Values " & _
                        "(:CODDPK" & I & ", 0, 0, :SEQ" & I & "); "
                        vCommit = vCommit + 1
                                
                        'Se a classifica��o do fornecedor for "A" - REVENDA, insere em outras tabelas
                        If vClassif_Forn_2 = "A" Then
                        
                            CmdPl = CmdPl & _
                                    "Insert into " & vDep & "TBITEM " & _
                                    "(cod_dpk, preco_venda, preco_of, preco_sp, preco_compra," & _
                                    " pc_desc1, pc_desc2, pc_icm, pc_margem_vda, prazo_fabrica," & _
                                    " fator_compra, fator_venda) " & _
                                    "Values " & _
                                    "(:CODDPK" & I & ", 0.00, 0.00, 0.00, 0.000, 0.00, 0.00, " & _
                                    " 0.00, 0.00, 0, 0.0000, 0.0000); "
                            vCommit = vCommit + 1
                        
                            'Se o fornecedor trabalhar com EDI e o c�d.fabr.edi for diferente de branco
                            'vai gravar na tabela compra.r_item_edi
                            If vFl_Edi_2 = "S" And vCod_Fabr_Edi <> "" Then
                                CmdPl = CmdPl & _
                                        "Insert into " & vDep & "R_ITEM_EDI " & _
                                        "(cod_dpk, cod_fornecedor, cod_fabrica_edi) " & _
                                        "Values " & _
                                        "(:CODDPK" & I & ", :FORN" & I & ", :CEDI" & I & "); "
                                vCommit = vCommit + 1
                            End If
                        
                        End If
                        
                    'N�o faz nada se a linha do grid contiver msg.de inconsist�ncia no cpo.Observa��o
                    End If
                
                    If vCommit >= 20 Then
                        
                        'Executa transa��o gravando o item em Campinas
                        'Em CPS � necess�rio gravar de 1 em 1 para n�o se perder a sequ�ncia do DPK e da tabela
                        
                        CmdPl = "BEGIN " & CmdPl & "Commit; "
                        CmdPl = CmdPl & "Exception "
                        CmdPl = CmdPl & "When Others then "
                        CmdPl = CmdPl & "RollBack; "
                        CmdPl = CmdPl & ":Cod_Errora := SQLCODE; "
                        CmdPl = CmdPl & ":Txt_Errora := SQLERRM; "
                        CmdPl = CmdPl & "End;"
                        'Executa transa��es
                        db.ExecuteSQL CmdPl
                        If IIf(IsNull(db.Parameters("cod_errora")), 0, Val(db.Parameters("cod_errora"))) <> 0 Then
                            
                            If Val(db.Parameters("cod_errora")) = -1400 Or Val(db.Parameters("cod_errora")) = -1 Then
                                If MsgBox("Item j� cadastrado!" & Chr(13) & "Forn./F�br. referente � " & I & "� linha do arquivo." & Chr(13) & "Deseja continuar a processar demais linhas do arquivo?", vbYesNo, "ATEN��O!") = vbYes Then
                                    GoTo PROXIMO_ITEM_CDS
                                Else
                                    Exit Sub
                                End If
                            Else
                                MsgBox db.Parameters("txt_errora") & ". Ligue p/ Depto.Sistemas"
                            End If
                            
                            Exit Sub
                        End If
                
                        vCommit = 0
                    
                    End If
                
PROXIMO_ITEM_CDS:
                Next I
                
                'Executa transa��o
                'Executa transa��o gravando o item em Campinas
                'Em CPS � necess�rio gravar de 1 em 1 para n�o se perder a sequ�ncia do DPK e da tabela
                
                CmdPl = "BEGIN " & CmdPl & "Commit; "
                CmdPl = CmdPl & "Exception "
                CmdPl = CmdPl & "When Others then "
                CmdPl = CmdPl & "RollBack; "
                CmdPl = CmdPl & ":Cod_Errora := SQLCODE; "
                CmdPl = CmdPl & ":Txt_Errora := SQLERRM; "
                CmdPl = CmdPl & "End;"
                'Executa transa��es
                db.ExecuteSQL CmdPl
                If IIf(IsNull(db.Parameters("cod_errora")), 0, Val(db.Parameters("cod_errora"))) <> 0 Then
                    MsgBox db.Parameters("txt_errora") & ". Ligue p/ Depto.Sistemas"
                    Exit Sub
                End If
                
                MsgBox vReg_Gravado & " Itens Inclu�dos em " & vNomeFanta & "!", vbInformation, "Aten��o!"
    
            End If

            
            
            'Caso o timeout n�o tenha compensado ou j� tenha feito transa��o para CD anterior, l� pr�ximo CD
            db_CONS.MoveNext

            
        Wend

    'End If

End If

End Sub




Private Sub cmdConfirma_Click()


Dim I As Integer              'Ponteiro de linha do grid
Dim vTamObs As Long           'Guarda a qtd. de caracteres da coluna Observa��o
Dim vObs As String            'Guarda o conte�do da coluna de Observa��es
Dim vFl_SeqExiste As Boolean  'Indica se a sequ�ncia de tabela j� existe ou n�o
Dim vSequencia As Double      'Guarda a sequ�ncia durante a pesquisa do MAX
Dim vSeqArq As String         'Guarda a sequ�ncia que veio no arquivo
Dim vReg_Gravado As Long      'Guarda a qtd. de itens gravados
Dim vCommit As Long           'Guarda a qtd. de itens para commitar de 20 em 20 nos CD's
Dim vLinSemObs As Long        'Guarda a qtd.de linhas sem observa��o
Dim vFl_Edi_2 As String       'Vai dizer se o fornecedor trabalha com EDI ou n�o
Dim vClassif_Forn_2 As String 'Vai dizer a classifica��o do fornecedor
Dim vCod_Fabr_Edi As String   'Vai conter o c�digo de f�brica edi
Dim vCD As String             'Vai conter o c�digo de loja para grava��o do item no cd
Dim vDep As String            'Vai conter o link do cd p/ grava��o no cd
Dim vNomeFanta As String      'Vai conter o nome fantasia do cd
Dim objconexao As Object      'Guarda resultado do select na LOJA_CONEXAO
Dim cmdPlSql As String        'Comando SQL da LOJA_CONEXAO
Dim fl_Conexao As Boolean     'Seta se pingou bem ou n�o no CD

If Len(lblArquivo.Caption) = 0 Then
   MsgBox ("Caminho ou arquivo inv�lido! A opera��o ser� abortada."), vbCritical + vbExclamation, "CAD750 - Opera��o incorreta!"
   Exit Sub
End If

If MsgBox("Confirma inclus�o dos itens?", vbYesNo, "CAD750") = vbYes Then

    'Primeiro vai percorrer todo o grid p/ ver se tem itens a serem gravados
    CmdPl = ""
    vLinSemObs = 0
    For I = 1 To grdArquivo.Rows - 1
        grdArquivo.Row = I
        grdArquivo.Col = 22
        vObs = grdArquivo.Text
        'Se a coluna de Observa��o n�o contiver nenhuma mensagem de inconsist�ncia vai gravar
        If vObs = "" Then
            vLinSemObs = vLinSemObs + 1
        End If
    Next I
    If vLinSemObs = 0 Then
        MsgBox "Nenhum item poder� ser cadastrado!", vbCritical, "Aten��o!"
        Exit Sub
    End If
    
    'Se houverem itens a serem gravados, vai iniciar todo o processo
    'Zera contador de linhas gravadas
    vReg_Gravado = 0

    db.Parameters.Remove "DTCA": db.Parameters.Add "DTCA", vDt_Cad, 1
    db.Parameters.Remove "AAMM": db.Parameters.Add "AAMM", vAnoMes, 1
    db.Parameters.Remove "cod_errora": db.Parameters.Add "cod_errora", 0, 2
    db.Parameters.Remove "txt_errora": db.Parameters.Add "txt_errora", "", 2
    
    'Pega linha a linha do grid para gravar as que cont�m campo de Observa��o <> ""
    For I = 1 To grdArquivo.Rows - 1
        
       
        grdArquivo.Row = I
        grdArquivo.Col = 22
        vObs = Trim(grdArquivo.Text)
        
        'Se a coluna de Observa��o n�o contiver nenhuma mensagem de inconsist�ncia vai gravar
        If vObs = "" Then
            
            '//////////////////////////////////////////////
            'Vai alimentar as vari�veis normais e de bind
            '/////////////////////////////////////////////
            'Limpa vari�veis auxiliares de grava��o
            CmdPl = ""
            vFl_Edi_2 = ""
            vClassif_Forn_2 = ""
            vCod_Fabr_Edi = ""
            
            'Pega informa��es das 2 �ltimas colunas, para serem usadas como auxiliares na grava��o
            grdArquivo.Col = 20
            vCod_Fabr_Edi = Trim(grdArquivo.Text)
            grdArquivo.Col = 24
            vFl_Edi_2 = Trim(grdArquivo.Text)
            grdArquivo.Col = 25
            vClassif_Forn_2 = Trim(grdArquivo.Text)
            
            'Guarda informa��es da linha em refer�ncia do grid, nas vari�veis de bind
            grdArquivo.Col = 1
            db.Parameters.Remove "FORN" & I: db.Parameters.Add "FORN" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 2
            db.Parameters.Remove "FABR" & I: db.Parameters.Add "FABR" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 3
            db.Parameters.Remove "DESC" & I: db.Parameters.Add "DESC" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 4
            db.Parameters.Remove "LINH" & I: db.Parameters.Add "LINH" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 5
            db.Parameters.Remove "GRUP" & I: db.Parameters.Add "GRUP" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 6
            db.Parameters.Remove "SUBG" & I: db.Parameters.Add "SUBG" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 7
            db.Parameters.Remove "MASC" & I: db.Parameters.Add "MASC" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 9
            db.Parameters.Remove "CLAS" & I: db.Parameters.Add "CLAS" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 10
            db.Parameters.Remove "PESO" & I: db.Parameters.Add "PESO" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 11
            db.Parameters.Remove "VOLU" & I: db.Parameters.Add "VOLU" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 12
            db.Parameters.Remove "UNID" & I: db.Parameters.Add "UNID" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 13
            db.Parameters.Remove "TICM" & I: db.Parameters.Add "TICM" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 14
            db.Parameters.Remove "TIPI" & I: db.Parameters.Add "TIPI" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 15
            db.Parameters.Remove "PIPI" & I: db.Parameters.Add "PIPI" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 16
            db.Parameters.Remove "PROC" & I: db.Parameters.Add "PROC" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 17
            db.Parameters.Remove "MINF" & I: db.Parameters.Add "MINF" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 18
            db.Parameters.Remove "MINV" & I: db.Parameters.Add "MINV" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 19
            db.Parameters.Remove "MAXV" & I: db.Parameters.Add "MAXV" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 20
            db.Parameters.Remove "CEDI" & I: db.Parameters.Add "CEDI" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 21
            db.Parameters.Remove "CATE" & I: db.Parameters.Add "CATE" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 26
            db.Parameters.Remove "MINEST" & I: db.Parameters.Add "MINEST" & I, Trim(grdArquivo.Text), 1
            grdArquivo.Col = 27
            db.Parameters.Remove "FABRICANTE" & I: db.Parameters.Add "FABRICANTE" & I, Trim(grdArquivo.Text), 1
            
            '//////////////////////////////////////
            'PROCESSO PARA O PR�XIMO C�DIGO DPK
            '//////////////////////////////////////
                        
            '//////////////////////////////////////////////////////////////////////////////////////////////////////////
            'Altera��o para utiliza��o da sequence de Cod.DPK - Alethea - 03/08/06
            
            'Todo este processo foi substituido pelo comando de Insert na ITEM_CADASTRO, pegando diretamente o NextVal - Alethea - 03/08/06
            
            'CmdSql = "Select PRODUCAO.Seq_Cddpk.Nextval DPK From DUAL "
            'Set db_CONS = db.dbCreateDynaset(CmdSql, 0&)
            'If db_CONS.EOF Then
            '    MsgBox "Problemas para pegar a pr�xima sequ�ncia do C�d.DPK! " & Chr(13) & _
            '           "Ligue para o analista respons�vel.", vbCritical, "Aten��o!"
            '    Exit Sub
            'Else
            '    db.Parameters.Remove "CODDPK" & I: db.Parameters.Add "CODDPK" & I, Val(db_CONS!DPK), 1
            '    'db.Parameters.Remove "CODDPK" & I: db.Parameters.Add "CODDPK" & I, 56322, 1
            'End If
            
            '//////////////////////////////////////////////////////////////////////////////////////////////////////////
            'Este processo abaixo d� certo e n�o pula sequencia da numera��o mas usar o insert direto � mais garantido - Alethea - 03/08/06
            
            ''Substitui��o para excluir bloco an�nimos
            'Set OraParameters = db.Parameters
            'OraParameters.Remove "vCursor": OraParameters.Add "vCursor", 0, 2
            'OraParameters("vCursor").serverType = ORATYPE_CURSOR
            'OraParameters("vCursor").DynasetOption = ORADYN_NO_BLANKSTRIP
            'OraParameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
            'OraParameters.Remove "vErro":   OraParameters.Add "vErro", 0, 2
            'db.Parameters.Remove "txt_errora": db.Parameters.Add "txt_errora", 0, 2
            'CmdSql = "Begin PRODUCAO.Pck_CAD750.Pr_Seq_Dpk(:vCursor,:vErro,:txt_errora); End; "
            'db.ExecuteSQL CmdSql
            'Set db_CONS = OraParameters("vCursor").Value
            'vERRO = IIf(db.Parameters("verro").Value = Null, 0, db.Parameters("verro").Value)
            'If Val(vERRO) <> 0 Then
            '    Call Process_Line_Errors
            '    MsgBox "Problemas para pegar a pr�xima sequ�ncia do C�d.DPK! " & Chr(13) & _
            '           "Ligue para o analista respons�vel.", vbCritical, "Aten��o!"
            '    Exit Sub
            'Else
            '    db.Parameters.Remove "CODDPK" & I: db.Parameters.Add "CODDPK" & I, Val(db_CONS!DPK), 1
            'End If
            
            'Coloca na 24.coluna do grid, o c�digo DPK que foi atribuido ao item que ser� cadastrado
            'Isso vai facilitar no momento de gravar os itens nos CD's
            'grdArquivo.Col = 23
            'grdArquivo.Text = Val(Trim(db_CONS!DPK))
            'grdArquivo.Text = Trim(56322)
            
            '//////////////////////////////////////////////////////////////////////////////////////////////////////////
            
            
            
            
            '//////////////////////////////////////
            'PROCESSO PARA ENCONTRAR MAX DA SEQ.TAB
            '//////////////////////////////////////
            
            'Guarda numa vari�vel, a sequ�ncia que veio no arquivo
            grdArquivo.Col = 8
            vSeqArq = Trim(grdArquivo.Text)
            
            'Se n�o veio nenhuma sequ�ncia no arquivo, vai verificar qual poder� ser usada
            If vSeqArq = "" Then
                
                'Verifica na tabela item_global o maior sequencia cadastrada para este fornecedor e
                'soma mais 200 para aplicar a este item
                CmdSql = "Select Max(Nvl(g.sequencia_tab,0)) SEQ " & _
                         "From ITEM_ESTOQUE e, ITEM_GLOBAL g, ITEM_CADASTRO c " & _
                         "Where e.cod_loja = 1 " & _
                         "And c.cod_fornecedor = :FORN" & I & " " & _
                         "And e.cod_dpk = g.cod_dpk " & _
                         "And g.cod_dpk = c.cod_dpk "
                Set db_CONS = db.DbCreateDynaset(CmdSql, 0&)
                
                'Se select n�o trouxer nada � porque houve algum problema com o fornecedor
                If db_CONS.EOF Then
                    MsgBox "Problemas para encontrar pr�xima sequ�ncia de tabela!" & Chr(13) & _
                           "Verifique o fornecedor!", vbCritical, "Aten��o!"
                    Exit Sub
                'Se a seq. trazida for um no. que n�o d� para somar 200, faz outro select
                ElseIf db_CONS!SEQ > 999799 Then
                
                    vFl_SeqExiste = True        'Indica que a sequ�ncia encontrada n�o pode ser usada
                    vSequencia = db_CONS!SEQ    'Guarda o valor da sequ�ncia encontrada
                    'Atribui � vari�vel de bind a primeira sequ�ncia encntrada (> 999799)
                    db.Parameters.Remove "SEQ" & I: db.Parameters.Add "SEQ" & I, vSequencia, 1
                    
                    'Enquanto a sequ�ncia encontrada n�o puder ser usada, vai procurar por outra
                    While vFl_SeqExiste = True
                    
                        'Vai procurar a maior sequ�ncia depois da 1a. encontrada (que n�o podia ser usada)
                        CmdSql2 = "Select Max(Nvl(g.sequencia_tab,0)) SEQ " & _
                                  "From ITEM_ESTOQUE e, ITEM_GLOBAL g, ITEM_CADASTRO c " & _
                                  "Where e.cod_loja = 1 " & _
                                  "And c.cod_fornecedor = :FORN" & I & " " & _
                                  "And g.sequencia_tab < :SEQ" & I & " " & _
                                  "And e.cod_dpk = g.cod_dpk " & _
                                  "And g.cod_dpk = c.cod_dpk "
                        Set db_CONS2 = db.DbCreateDynaset(CmdSql2, 0&)
                    
                        vSequencia = db_CONS2!SEQ + 200
                        
                        'Atribui � vari�vel de bind a pr�xima maior sequ�ncia
                        db.Parameters.Remove "SEQ" & I: db.Parameters.Add "SEQ" & I, vSequencia, 1
                    
                        'Verifica se a pr�xima sequ�ncia pode ser usada
                        CmdSql3 = "Select count(*) QtdSeq " & _
                                  "From ITEM_ESTOQUE e, ITEM_CADASTRO c, ITEM_GLOBAL g " & _
                                  "Where e.cod_loja = 1 " & _
                                  "And c.cod_fornecedor = :FORN" & I & " " & _
                                  "And g.sequencia_tab = :SEQ" & I & " " & _
                                  "And e.cod_dpk = g.cod_dpk " & _
                                  "And g.cod_dpk = c.cod_dpk "
                        Set db_CONS3 = db.DbCreateDynaset(CmdSql3, 0&)
                    
                        'Se ainda n�o existir, seta flag que encontrou
                        If db_CONS3!QtdSeq = 0 Then
                            vFl_SeqExiste = False
                        Else    'Se existir, continua procurando o m�x. anterior
                            vFl_SeqExiste = True
                        End If
                    
                    Wend
    
                'Verifica se a seq.trazida + 200 pode ser usada
                Else
                    
                    vSequencia = db_CONS!SEQ + 200  'Guarda o valor da sequ�ncia encontrada
                        
                    'Atribui � vari�vel de bind a primeira sequ�ncia encntrada
                    db.Parameters.Remove "SEQ" & I: db.Parameters.Add "SEQ" & I, vSequencia, 1
                    
                    'Verifica se a pr�xima sequ�ncia poide ser usada
                    CmdSql3 = "Select count(*) QtdSeq " & _
                              "From ITEM_ESTOQUE e, ITEM_CADASTRO c, ITEM_GLOBAL g " & _
                              "Where e.cod_loja = 1 " & _
                              "And c.cod_fornecedor = :FORN" & I & " " & _
                              "And g.sequencia_tab = :SEQ" & I & " " & _
                              "And e.cod_dpk = g.cod_dpk " & _
                              "And g.cod_dpk = c.cod_dpk "
                        
                    Set db_CONS3 = db.DbCreateDynaset(CmdSql3, 0&)
                    
                    'Se seq. puder ser usada, seta flag que encontrou
                    If db_CONS3!QtdSeq = 0 Then
                        vFl_SeqExiste = False
                        
                    'Se j� existir, vai procurando o Max abaixo do �ltimo encontrado, at� achar um que possa ser usado
                    Else
                        
                        vFl_SeqExiste = True        'Indica que a sequ�ncia encontrada n�o pode ser usada
                        vSequencia = db_CONS!SEQ    'Guarda o valor da sequ�ncia encontrada
                        
                        'Atribui � vari�vel de bind a primeira sequ�ncia encontrada (que j� existe)
                        db.Parameters.Remove "SEQ" & I: db.Parameters.Add "SEQ" & I, vSequencia, 1
                    
                        'Enquanto a sequ�ncia encontrada n�o puder ser usada, vai procurar por outra
                        While vFl_SeqExiste = True
                            
                            'Vai procurar a maior sequ�ncia depois da 1a. encontrada (que n�o podia ser usada)
                            CmdSql2 = "Select Max(Nvl(g.sequencia_tab)) SEQ " & _
                                      "From ITEM_ESTOQUE e, ITEM_GLOBAL g, ITEM_CADASTRO c " & _
                                      "Where e.cod_loja = 1 " & _
                                      "And c.cod_fornecedor = :FORN " & _
                                      "And g.sequencia_tab < :SEQ " & _
                                      "And e.cod_dpk = g.cod_dpk " & _
                                      "And g.cod_dpk = c.cod_dpk "
                            Set db_CONS2 = db.DbCreateDynaset(CmdSql2, 0&)
                        
                            vSequencia = db_CONS2!SEQ + 200
                            
                            'Atribui � vari�vel de bind a pr�xima maior sequ�ncia
                            db.Parameters.Remove "SEQ" & I: db.Parameters.Add "SEQ" & I, vSequencia, 1
                        
                            'Verifica se a pr�xima sequ�ncia pode ser usada
                            CmdSql3 = "Select count(*) QtdSeq " & _
                                      "From ITEM_ESTOQUE e, ITEM_CADASTRO c, ITEM_GLOBAL g " & _
                                      "Where e.cod_loja = 1 " & _
                                      "And c.cod_fornecedor = :FORN" & I & " " & _
                                      "And g.sequencia_tab = :SEQ" & I & " " & _
                                      "And e.cod_dpk = g.cod_dpk " & _
                                      "And g.cod_dpk = c.cod_dpk "
                            Set db_CONS3 = db.DbCreateDynaset(CmdSql3, 0&)
                        
                            'Se n�o existir, seta flag que encontrou
                            If db_CONS3!QtdSeq = 0 Then
                                vFl_SeqExiste = False
                            Else
                                vFl_SeqExiste = True
                            End If
                        
                        Wend
    
                    End If
                    
                End If
            Else
                'vSequencia = Val(vSeqArq)
                db.Parameters.Remove "SEQ" & I: db.Parameters.Add "SEQ" & I, Val(vSeqArq), 1
            End If
            
            'Atribui � vari�vel de bind, a pr�xima sequ�ncia de tabela encontrada
            'db.Parameters.Remove "SEQT" & I: db.Parameters.Add "SEQT" & I, Trim(vSequencia), 1
            
            'Coloca na 9a. coluna do grid, a sequ�ncia que foi encontrada p/o item que ser� cadastrado
            'Isso vai facilitar no momento de gravar os itens nos CD's
            If Trim(vSequencia) <> 0 Then
                grdArquivo.Col = 8
                grdArquivo.Text = Trim(vSequencia)
            End If
    
            '//////////////////////////////////////
            'PROCESSO PARA GRAVAR ITEM EM CAMPINAS
            '//////////////////////////////////////
            
            '//////////////////////////////////////////////////////////////////////////////////////////////////////////
            'Altera��o para utiliza��o da sequence de Cod.DPK - Alethea - 03/08/06
            
            'CmdPl = CmdPl & _
                    "Insert into ITEM_CADASTRO " & _
                    "(cod_Dpk, cod_Fornecedor, cod_fabrica, desc_item, cod_linha," & _
                    " cod_grupo, cod_subgrupo, dt_cadastramento, mascarado, class_fiscal," & _
                    " peso, volume, cod_unidade, cod_tributacao, cod_tributacao_ipi," & _
                    " pc_ipi, cod_procedencia, cod_dpk_ant, qtd_minforn, qtd_minvda) "
            'CmdPl = CmdPl & _
                    "Values " & _
                    "(:CODDPK" & I & ", :FORN" & I & ", :FABR" & I & ", " & _
                    " TRANSLATE(:DESC" & I & ",'" & VAR_CAR_INVALIDO & "','" & VAR_CAR_VALIDO & "'), " & _
                    " :LINH" & I & ", " & _
                    " :GRUP" & I & ", :SUBG" & I & ", To_Date(:DTCA,'DD/MM/RR'), :MASC" & I & ", " & _
                    " :CLAS" & I & ", :PESO" & I & ", :VOLU" & I & ", :UNID" & I & ", " & _
                    " :TICM" & I & ", :TIPI" & I & ", :PIPI" & I & ", :PROC" & I & ", 0, " & _
                    " :MINF" & I & ", :MINV" & I & "); "
    
            'Em 03/08/06 foi feito insert na ITEM_CADASTRO usando diretamente NEXTVAL da sequence pois estava tendo problemas de pular sequencia. - Alethea
            
            
            '********************************************************************************************************
            'Comentado para que somente inclua em todas as tabelas ou em nenhuma delas - Alethea - 13/03/08
            'CmdPl = CmdPl & _
                    "Insert into ITEM_CADASTRO " & _
                    "(cod_Dpk, cod_Fornecedor, cod_fabrica, desc_item, cod_linha," & _
                    " cod_grupo, cod_subgrupo, dt_cadastramento, mascarado, class_fiscal," & _
                    " peso, volume, cod_unidade, cod_tributacao, cod_tributacao_ipi," & _
                    " pc_ipi, cod_procedencia, cod_dpk_ant, qtd_minforn, qtd_minvda) "
            'CmdPl = CmdPl & _
                    "Values " & _
                    "(PRODUCAO.Seq_Cddpk.Nextval, :FORN" & I & ", :FABR" & I & ", " & _
                    " TRANSLATE(:DESC" & I & ",'" & VAR_CAR_INVALIDO & "','" & VAR_CAR_VALIDO & "'), " & _
                    " :LINH" & I & ", " & _
                    " :GRUP" & I & ", :SUBG" & I & ", To_Date(:DTCA,'DD/MM/RR'), :MASC" & I & ", " & _
                    " :CLAS" & I & ", :PESO" & I & ", :VOLU" & I & ", :UNID" & I & ", " & _
                    " :TICM" & I & ", :TIPI" & I & ", :PIPI" & I & ", :PROC" & I & ", 0, " & _
                    " :MINF" & I & ", :MINV" & I & "); "
            'CmdPl = "BEGIN " & CmdPl & " Commit; " & _
                        "Exception " & _
                            "When Others then " & _
                                "RollBack; " & _
                        ":Cod_Errora := SQLCODE; " & _
                        ":Txt_Errora := SQLERRM; " & _
                    "End;"
            ''Executa transa��es
            'db.ExecuteSQL CmdPl
            'If IIf(IsNull(db.Parameters("cod_errora")), 0, Val(db.Parameters("cod_errora"))) <> 0 Then
            '    If Val(db.Parameters("cod_errora")) = -1 Then
            '        If MsgBox("Item j� cadastrado!" & Chr(13) & "Forn./F�br. referente � " & I & "� linha do arquivo." & Chr(13) & "Deseja continuar a processar demais linhas do arquivo?", vbYesNo, "ATEN��O!") = vbYes Then
            '            GoTo PROXIMO_ITEM
            '        Else
            '            Exit Sub
            '        End If
            '    Else
            '        MsgBox db.Parameters("txt_errora") & ". Ligue p/ Depto.Sistemas"
            '    End If
            '    Exit Sub
            'End If
            'CmdPl = ""
            '********************************************************************************************************
                        
            db.Parameters.Remove "CODDPK" & I: db.Parameters.Add "CODDPK" & I, 0, 1
            
            CmdPl = ""
            'WILLIAM LEITE
            'CmdPl = CmdPl & " SELECT PRODUCAO.Seq_Cddpk.Nextval INTO :CODDPK" & I & " FROM DUAL; "
            CmdPl = CmdPl & " Select Min(a.gap) CODDPK " & _
                            "  Into :CODDPK" & I & " " & _
                            "  From (Select min_a - 1 + Level gap " & _
                            "          From (Select Min(cod_dpk) min_a, Max(cod_dpk) max_a " & _
                            "                  From producao.item_cadastro) " & _
                            "                       Connect By Level <= max_a - min_a + 1 " & _
                            "         Minus Select cod_dpk from producao.item_cadastro) a; "
                            
            CmdPl = CmdPl & _
                    "Insert into ITEM_CADASTRO " & _
                    "(cod_Dpk, cod_Fornecedor, cod_fabrica, desc_item, cod_linha," & _
                    " cod_grupo, cod_subgrupo, dt_cadastramento, mascarado, class_fiscal," & _
                    " peso, volume, cod_unidade, cod_tributacao, cod_tributacao_ipi," & _
                    " pc_ipi, cod_procedencia, cod_dpk_ant, qtd_minforn, qtd_minvda) "
            CmdPl = CmdPl & _
                    "Values " & _
                    "(:CODDPK" & I & ", :FORN" & I & ", :FABR" & I & ", " & _
                    " TRANSLATE(:DESC" & I & ",'" & VAR_CAR_INVALIDO & "','" & VAR_CAR_VALIDO & "'), " & _
                    " :LINH" & I & ", " & _
                    " :GRUP" & I & ", :SUBG" & I & ", To_Date(:DTCA,'DD/MM/RR'), :MASC" & I & ", " & _
                    " :CLAS" & I & ", :PESO" & I & ", :VOLU" & I & ", :UNID" & I & ", " & _
                    " :TICM" & I & ", :TIPI" & I & ", :PIPI" & I & ", :PROC" & I & ", 0, " & _
                    " :MINF" & I & ", :MINV" & I & "); "
            
            
    
            'Agora fa�o select para pegar o c�digo DPK que foi atribuido
            'CmdSql3 = "Select Cod_Dpk " & _
                      "From ITEM_CADASTRO " & _
                      "Where cod_fornecedor = :FORN" & I & " " & _
                      "And cod_fabrica = :FABR" & I & " "
            'Set db_CONS3 = db.dbCreateDynaset(CmdSql3, 0&)
            'If db_CONS3.EOF Then
            '    MsgBox "Problemas para selecionar o c�digo DPK atribuido ao " & I & "� item do arquivo! " & Chr(13) & _
            '           "Favor abrir chamado no HelpDesk.", vbCritical, "Aten��o!"
            '    Exit Sub
            'Else
            '    db.Parameters.Remove "CODDPK" & I: db.Parameters.Add "CODDPK" & I, Val(db_CONS3!Cod_DPK), 1
            'End If
            
            'Coloca na 24.coluna do grid, o c�digo DPK que foi atribuido ao item cadastrado
            'Isso vai facilitar no momento de gravar os itens nos CD's
            'grdArquivo.Col = 23
            'grdArquivo.Text = Val(Trim(db_CONS3!Cod_DPK))
            
            
            '********************************************************************************************************
            
            
            
            '//////////////////////////////////////////////////////////////////////////////////////////////////////////
            
            'Continua os cadastros nas demais tabelas
    
            'EM 01/11/07 (IMPLEMENTADO EM 04/03/08) ALTERADO PARA SEMPRE QUE FOR ITEM NOVO, ALTERAR PARA CATEG. "E"
    
            'CmdPl = CmdPl & _
                    "Insert into ITEM_ANALITICO " & _
                    "(cod_loja, cod_dpk, categoria, qtd_mes_ant, custo_medio, custo_medio_ant, " & _
                    " class_abc, class_abcf, class_venda) " & _
                    "Values " & _
                    "(1, :CODDPK" & I & ", :CATE" & I & ", 0, 0, 0, 'D', 'D', 99999); "
    
            CmdPl = CmdPl & _
                    "Insert into ITEM_ANALITICO " & _
                    "(cod_loja, cod_dpk, categoria, qtd_mes_ant, custo_medio, custo_medio_ant, " & _
                    " class_abc, class_abcf, class_venda) " & _
                    "Values " & _
                    "(1, :CODDPK" & I & ", :CATE" & I & ", 0, 0, 0, 'E', 'E', 99999); "
        
            'A partir de 01/11/07 (IMPLEMENTADO EM 04/03/08), incluir tb. na tabela Compras.Item_Curva_ABC, sendo que itens novos receber�o classifica��o "E"
            CmdPl = CmdPl & _
                " INSERT INTO compras.item_curva_abc " & _
                "(Cod_dpk, Class_ABC, Class_Venda, Class_ABCF, Pc_Acum_ABCF ) " & _
                "VALUES (:CODDPK" & I & ", 'E', 99999, 'E', 0); "

    
            CmdPl = CmdPl & _
                    "Insert into Producao.R_ITEM_FABRICANTE " & _
                    "(Cod_Dpk, Cod_Fabricante) Values " & _
                    "(:CODDPK" & I & ", :FABRICANTE" & I & "); "
    
            CmdPl = CmdPl & _
                    "Insert into ITEM_GLOBAL " & _
                    "(cod_dpk, preco_compra, preco_compra_ant, sequencia_tab) " & _
                    "Values " & _
                    "(:CODDPK" & I & ", 0, 0, :SEQ" & I & "); "
    
            CmdPl = CmdPl & _
                    "Insert into ITEM_PRECO " & _
                    "(cod_loja, cod_dpk, preco_venda, preco_venda_ant, preco_of," & _
                    " preco_of_ant, preco_sp, preco_sp_ant) " & _
                    "Values " & _
                    "(1, :CODDPK" & I & ", 0, 0, 0, 0, 0, 0); "
        
            CmdPl = CmdPl & _
                    "Insert into ITEM_ESTOQUE " & _
                    "(cod_loja, cod_dpk, qtd_atual, qtd_reserv, qtd_pendente, qtd_maxvda," & _
                    " cuema, dt_cuema, vl_ult_compra, dt_ult_compra, situacao) " & _
                    "Values " & _
                    "(1, :CODDPK" & I & ", 0, 0, 0, :MAXV" & I & ", 0, NULL, 0, NULL, 0); "
                    
            CmdPl = CmdPl & _
                    "Insert into ITEM_CUSTO " & _
                    "(cod_loja, cod_dpk, custo_reposicao) " & _
                    "Values " & _
                    "(1, :CODDPK" & I & ", 0); "
                    
            CmdPl = CmdPl & _
                    "Insert into TRANSFERENCIA.ESTOQUE_MINIMO " & _
                    "(cod_dpk, min_estoque) " & _
                    "Values " & _
                    "(:CODDPK" & I & ", :MINEST" & I & " ); "
                    
            'Se a classifica��o do fornecedor for "A" - REVENDA, insere em outras tabelas
            If vClassif_Forn_2 = "A" Then
            
                CmdPl = CmdPl & _
                        "Insert into ESTATISTICA_ITEM " & _
                        "(cod_loja, cod_dpk, ano_mes, qtd_compra, qtd_venda," & _
                        " qtd_venda_sp, qtd_venda_cp) " & _
                        "Values " & _
                        "(1, :CODDPK" & I & ", :AAMM, 0, 0, 0, 0); "
            
                CmdPl = CmdPl & _
                        "Insert into COMPRAS.TBITEM " & _
                        "(cod_dpk, preco_venda, preco_of, preco_sp, preco_compra," & _
                        " pc_desc1, pc_desc2, pc_icm, pc_margem_vda, prazo_fabrica," & _
                        " fator_compra, fator_venda) " & _
                        "Values " & _
                        "(:CODDPK" & I & ", 0.00, 0.00, 0.00, 0.000, 0.00, 0.00, 0.00, 0.00," & _
                        " 0, 0.0000, 0.0000); "
            
                CmdPl = CmdPl & _
                        "Insert into COMPRAS.TBITEM_PRECO " & _
                        "(cod_loja, cod_dpk, preco_of, preco_sp) " & _
                        "Values " & _
                        "(1, :CODDPK" & I & ", 0.00, 0.00); "
            
                'Se o fornecedor trabalhar com EDI e o c�d.fabr.edi for diferente de branco
                'vai gravar na tabela compra.r_item_edi
                If vFl_Edi_2 = "S" And vCod_Fabr_Edi <> "" Then
                        
                    CmdPl = CmdPl & _
                            "Insert into COMPRAS.R_ITEM_EDI " & _
                            "(cod_dpk, cod_fornecedor, cod_fabrica_edi) " & _
                            "Values " & _
                            "(:CODDPK" & I & ", :FORN" & I & ", :CEDI" & I & "); "
                
                End If
            
            End If
            
            'Executa transa��o gravando o item em Campinas
            'Em CPS � necess�rio gravar de 1 em 1 para n�o se perder a sequ�ncia do DPK e da tabela
            
            CmdPl = "BEGIN " & CmdPl & " Commit; " & _
                        "Exception " & _
                            "When Others then " & _
                                "RollBack; " & _
                        ":Cod_Errora := SQLCODE; " & _
                        ":Txt_Errora := SQLERRM; " & _
                    "End;"
            'Executa transa��es
            'On Error Resume Next
            db.ExecuteSQL CmdPl
        
            If IIf(IsNull(db.Parameters("cod_errora")), 0, Val(db.Parameters("cod_errora"))) <> 0 Then
                MsgBox db.Parameters("txt_errora") & ". Ligue p/ Depto.Sistemas"
                Exit Sub
            End If
        
            'Coloca na 24.coluna do grid, o c�digo DPK que foi atribuido ao item cadastrado
            'Isso vai facilitar no momento de gravar os itens nos CD's
            CmdSql3 = "Select cod_dpk from item_cadastro "
            CmdSql3 = CmdSql3 & " Where Cod_Fornecedor = :FORN" & I
            CmdSql3 = CmdSql3 & " And Cod_Fabrica = :FABR" & I
            Set db_CONS3 = db.DbCreateDynaset(CmdSql3, 0&)
            If db_CONS3.EOF Then
                MsgBox "Problemas para selecionar o c�digo DPK atribuido ao " & I & "� item do arquivo! " & Chr(13) & "CDs n�o ser�o atualizados on-line! Favor abrir chamado no HelpDesk.", vbCritical, "Aten��o!"
                GoTo PROXIMO_ITEM
            Else
                grdArquivo.Col = 23
                grdArquivo.Text = Val(Trim(db_CONS3!cod_dpk))
            End If
            
            'grdArquivo.Col = 23
            Dim vAuxCodDpk As Long
            Dim sAuxCodDPk As String
            'If IsNull(db.Parameters("CODDPK").Value) Then
            '    vAuxCodDpk = 0
            'Else
            '    sAuxCodDPk = "CODDPK" & I
            '    vAuxCodDpk = Val(db.Parameters("CODDPK" & I & "").Value)
            'End If
            'grdArquivo.Text = db.Parameters("CODDPK" & I & "").Value
            
            vReg_Gravado = vReg_Gravado + 1
            
        'N�o faz nada se a linha do grid contiver msg.de inconsist�ncia no cpo.Observa��o
        End If
    
PROXIMO_ITEM:
    
    
    Next I
    MsgBox vReg_Gravado & " ITENS INCLUIDOS OK EM CAMPINAS!", vbInformation, "Aten��o!"

    'Comentei este trecho pois a inclus�o n�o on-line estava dando muitos problemas na
    'libera��o de NF nos CDs (Aleth�a - Maio/2002)
    'If MsgBox("Confirma inclus�o on-line dos itens nos CD�s?", vbYesNo, "CAD750") = vbYes Then

        '////////////////////////////////////
        'PROCESSO PARA INCLUIR ITENS NOS CD'S - Vai sempre incluir p/Cds que o Cad070 incluir
        '////////////////////////////////////
        
        db.Parameters.Remove "DTCA": db.Parameters.Add "DTCA", vDt_Cad, 1
    '    db.Parameters.Remove "AAMM": db.Parameters.Add "AAMM", vAnoMes, 1
        db.Parameters.Remove "cod_errora": db.Parameters.Add "cod_errora", 0, 2
        db.Parameters.Remove "txt_errora": db.Parameters.Add "txt_errora", 0, 2
        
        CmdSql = "Select l.cod_loja CD, l.nome_fantasia NOME " & _
                 "From LOJA l, DEPOSITO_VISAO d " & _
                 "Where d.cod_loja = l.cod_loja " & _
                 "And d.cod_loja <> 1 " & _
                 "And d.nome_programa = 'CAD070' " & _
                 "And l.cod_loja not in (12,13,15,17,18,19,20,21) " & _
                 "Order by l.cod_loja"
        Set db_CONS = db.DbCreateDynaset(CmdSql, 0&)
        
        'Enquanto ainda houverem CD's a incluir os itens, faz o processo
        While Not db_CONS.EOF
        
            vCD = Format(Trim(db_CONS!CD), "00")
            vDep = "DEP" & Format(Trim(db_CONS!CD), "00") & "."
            vNomeFanta = Trim(db_CONS!NOME)
            
            '////////////////////////////////////////////////////////////////////
            'TESTA TIME-OUT NO CD P/ VERIFICAR SE VALE A PENA GRAVAR ITEM ON-LINE
            '////////////////////////////////////////////////////////////////////
            db.Parameters.Remove "LJ": db.Parameters.Add "LJ", Val(vCD), 1
            
            cmdPlSql = "Select cod_loja, ip, time_out " & _
                       "From PRODUCAO.LOJA_CONEXAO " & _
                       "Where cod_loja = :LJ "
            Set objconexao = db.DbCreateDynaset(cmdPlSql, 0&)
                        
            If objconexao.EOF Then
                MsgBox "CD " & vCD & " n�o cadastrada na tabela LOJA_CONEXAO! " & Chr(13) & _
                        "Chame o analista respons�vel.", vbCritical, "Aten��o"
                Exit Sub
            End If
                    
            IP = objconexao!IP
            TIMEOUT = Val(objconexao!TIME_OUT)
            CDLJ = objconexao!cod_loja
            success = Ping(IP, CDLJ, ECHO)
                    
            'Vai verificar se o tempo do ping, compensar a conex�o com o CD
            If InStr(GetStatusCode(success), ("ip success")) = 0 Or _
                ECHO.RoundTripTime > TIMEOUT Then
                MsgBox "CD " & CDLJ & " sem comunica��o!", vbInformation, "Aten��o"
                fl_Conexao = False
            Else
                fl_Conexao = True
            End If
                
            'Caso o timeout com o CD compense, prepara itens para serem gravado no CD
            If fl_Conexao = True Then
                    
                vReg_Gravado = 0
                vCommit = 0
                
                'Pega linha a linha do grid para gravar as que cont�m campo de Observa��o <> ""
                For I = 1 To grdArquivo.Rows - 1
                    
                    grdArquivo.Row = I
                    grdArquivo.Col = 22
                    vObs = grdArquivo.Text
                    
                    'Se a coluna de Observa��o n�o contiver nenhuma mensagem de inconsist�ncia vai gravar
                    If vObs = "" Then
                        
                        vReg_Gravado = vReg_Gravado + 1
                    
                        'Vai alimentar as vari�veis normais e de bind
                        'Limpa vari�veis auxiliares de grava��o
                        vFl_Edi_2 = ""
                        vClassif_Forn_2 = ""
                        vCod_Fabr_Edi = ""
                                    
                        'Pega informa��es das 2 �ltimas colunas, para serem usadas como auxiliares na grava��o
                        grdArquivo.Col = 20
                        vCod_Fabr_Edi = Trim(grdArquivo.Text)
                        grdArquivo.Col = 24
                        vFl_Edi_2 = Trim(grdArquivo.Text)
                        grdArquivo.Col = 25
                        vClassif_Forn_2 = Trim(grdArquivo.Text)
                                    
                        'Guarda informa��es da linha em refer�ncia do grid, nas vari�veis de bind
                        grdArquivo.Col = 1
                        db.Parameters.Remove "FORN" & I: db.Parameters.Add "FORN" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 2
                        db.Parameters.Remove "FABR" & I: db.Parameters.Add "FABR" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 3
                        db.Parameters.Remove "DESC" & I: db.Parameters.Add "DESC" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 4
                        db.Parameters.Remove "LINH" & I: db.Parameters.Add "LINH" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 5
                        db.Parameters.Remove "GRUP" & I: db.Parameters.Add "GRUP" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 6
                        db.Parameters.Remove "SUBG" & I: db.Parameters.Add "SUBG" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 7
                        db.Parameters.Remove "MASC" & I: db.Parameters.Add "MASC" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 9
                        db.Parameters.Remove "CLAS" & I: db.Parameters.Add "CLAS" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 10
                        db.Parameters.Remove "PESO" & I: db.Parameters.Add "PESO" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 11
                        db.Parameters.Remove "VOLU" & I: db.Parameters.Add "VOLU" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 12
                        db.Parameters.Remove "UNID" & I: db.Parameters.Add "UNID" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 13
                        db.Parameters.Remove "TICM" & I: db.Parameters.Add "TICM" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 14
                        db.Parameters.Remove "TIPI" & I: db.Parameters.Add "TIPI" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 15
                        db.Parameters.Remove "PIPI" & I: db.Parameters.Add "PIPI" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 16
                        db.Parameters.Remove "PROC" & I: db.Parameters.Add "PROC" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 17
                        db.Parameters.Remove "MINF" & I: db.Parameters.Add "MINF" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 18
                        db.Parameters.Remove "MINV" & I: db.Parameters.Add "MINV" & I, Trim(grdArquivo.Text), 1
                        'grdArquivo.Col = 19
                        'db.Parameters.Remove "MAXV" & I: db.Parameters.Add "MAXV" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 20
                        db.Parameters.Remove "CEDI" & I: db.Parameters.Add "CEDI" & I, Trim(grdArquivo.Text), 1
                        'GrdArquivo.Col = 21
                        'db.Parameters.Remove "CATE" & I: db.Parameters.Add "CATE" & I, Trim(grdArquivo.Text), 1
                        
                        'Pega cod.DPK e Seq.Tab. atribu�dos para o item no momento da grava��o em CPS
                        grdArquivo.Col = 23
                        db.Parameters.Remove "CODDPK" & I: db.Parameters.Add "CODDPK" & I, Trim(grdArquivo.Text), 1
                        grdArquivo.Col = 8
                        db.Parameters.Remove "SEQ" & I: db.Parameters.Add "SEQ" & I, Trim(grdArquivo.Text), 1
                        
                        '//////////////////////////////////////
                        'PROCESSO PARA GRAVAR ITEM NOS CDS
                        '//////////////////////////////////////
                CmdPl = "Insert into " & vDep & "ITEM_CADASTRO " & _
                        "(cod_Dpk, cod_Fornecedor, cod_fabrica, desc_item, cod_linha," & _
                        " cod_grupo, cod_subgrupo, dt_cadastramento, mascarado, class_fiscal," & _
                        " peso, volume, cod_unidade, cod_tributacao, cod_tributacao_ipi," & _
                        " pc_ipi, cod_procedencia, cod_dpk_ant, qtd_minforn, qtd_minvda) "
                CmdPl = CmdPl & _
                        "Values " & _
                        "(:CODDPK" & I & ", :FORN" & I & ", :FABR" & I & ", " & _
                        " TRANSLATE(:DESC" & I & ",'" & VAR_CAR_INVALIDO & "','" & VAR_CAR_VALIDO & "'), " & _
                        " :LINH" & I & ", " & _
                        " :GRUP" & I & ", :SUBG" & I & ", To_Date(:DTCA,'DD/MM/RR'), :MASC" & I & ", " & _
                        " :CLAS" & I & ", :PESO" & I & ", :VOLU" & I & ", :UNID" & I & ", " & _
                        " :TICM" & I & ", :TIPI" & I & ", :PIPI" & I & ", :PROC" & I & ", 0, " & _
                        " :MINF" & I & ", :MINV" & I & "); "
                        vCommit = vCommit + 1
                        
                CmdPl = CmdPl & _
                        "Insert into " & vDep & "ITEM_GLOBAL " & _
                        "(cod_dpk, preco_compra, preco_compra_ant, sequencia_tab) " & _
                        "Values " & _
                        "(:CODDPK" & I & ", 0, 0, :SEQ" & I & "); "
                        vCommit = vCommit + 1
                                
                        'Se a classifica��o do fornecedor for "A" - REVENDA, insere em outras tabelas
                        If vClassif_Forn_2 = "A" Then
                        
                            CmdPl = CmdPl & _
                                    "Insert into " & vDep & "TBITEM " & _
                                    "(cod_dpk, preco_venda, preco_of, preco_sp, preco_compra," & _
                                    " pc_desc1, pc_desc2, pc_icm, pc_margem_vda, prazo_fabrica," & _
                                    " fator_compra, fator_venda) " & _
                                    "Values " & _
                                    "(:CODDPK" & I & ", 0.00, 0.00, 0.00, 0.000, 0.00, 0.00, " & _
                                    " 0.00, 0.00, 0, 0.0000, 0.0000); "
                            vCommit = vCommit + 1
                        
                            'Se o fornecedor trabalhar com EDI e o c�d.fabr.edi for diferente de branco
                            'vai gravar na tabela compra.r_item_edi
                            If vFl_Edi_2 = "S" And vCod_Fabr_Edi <> "" Then
                                CmdPl = CmdPl & _
                                        "Insert into " & vDep & "R_ITEM_EDI " & _
                                        "(cod_dpk, cod_fornecedor, cod_fabrica_edi) " & _
                                        "Values " & _
                                        "(:CODDPK" & I & ", :FORN" & I & ", :CEDI" & I & "); "
                                vCommit = vCommit + 1
                            End If
                        
                        End If
                        
                    'N�o faz nada se a linha do grid contiver msg.de inconsist�ncia no cpo.Observa��o
                    End If
                
                    If vCommit >= 20 Then
                        
                        'Executa transa��o gravando o item em Campinas
                        'Em CPS � necess�rio gravar de 1 em 1 para n�o se perder a sequ�ncia do DPK e da tabela
                        
                        CmdPl = "BEGIN " & CmdPl & "Commit; "
                        CmdPl = CmdPl & "Exception "
                        CmdPl = CmdPl & "When Others then "
                        CmdPl = CmdPl & "RollBack; "
                        CmdPl = CmdPl & ":Cod_Errora := SQLCODE; "
                        CmdPl = CmdPl & ":Txt_Errora := SQLERRM; "
                        CmdPl = CmdPl & "End;"
                        'Executa transa��es
                        db.ExecuteSQL CmdPl
                        If IIf(IsNull(db.Parameters("cod_errora")), 0, Val(db.Parameters("cod_errora"))) <> 0 Then
                            
                            If Val(db.Parameters("cod_errora")) = -1400 Or Val(db.Parameters("cod_errora")) = -1 Then
                                If MsgBox("Item j� cadastrado!" & Chr(13) & "Forn./F�br. referente � " & I & "� linha do arquivo." & Chr(13) & "Deseja continuar a processar demais linhas do arquivo?", vbYesNo, "ATEN��O!") = vbYes Then
                                    GoTo PROXIMO_ITEM_CDS
                                Else
                                    Exit Sub
                                End If
                            Else
                                MsgBox db.Parameters("txt_errora") & ". Ligue p/ Depto.Sistemas"
                            End If
                            
                            Exit Sub
                        End If
                
                        vCommit = 0
                    
                    End If
                
PROXIMO_ITEM_CDS:
                Next I
                
                'Executa transa��o
                'Executa transa��o gravando o item em Campinas
                'Em CPS � necess�rio gravar de 1 em 1 para n�o se perder a sequ�ncia do DPK e da tabela
                
                CmdPl = "BEGIN " & CmdPl & "Commit; "
                CmdPl = CmdPl & "Exception "
                CmdPl = CmdPl & "When Others then "
                CmdPl = CmdPl & "RollBack; "
                CmdPl = CmdPl & ":Cod_Errora := SQLCODE; "
                CmdPl = CmdPl & ":Txt_Errora := SQLERRM; "
                CmdPl = CmdPl & "End;"
                'Executa transa��es
                db.ExecuteSQL CmdPl
                If IIf(IsNull(db.Parameters("cod_errora")), 0, Val(db.Parameters("cod_errora"))) <> 0 Then
                    MsgBox db.Parameters("txt_errora") & ". Ligue p/ Depto.Sistemas"
                    Exit Sub
                End If
                
                MsgBox vReg_Gravado & " Itens Inclu�dos em " & vNomeFanta & "!", vbInformation, "Aten��o!"
    
            End If

            
            
            'Caso o timeout n�o tenha compensado ou j� tenha feito transa��o para CD anterior, l� pr�ximo CD
            db_CONS.MoveNext

            
        Wend

    'End If

End If

End Sub
Private Sub cmdConfirma_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp2.Left = 10000
    lblHelp2.Top = 1410
    lblHelp2.Visible = True
    lblHelp2.Caption = "Gravar Dados"
End Sub


Private Sub cmdLog_Click()

Dim I As Integer        'Ponteiro de linha do grid
Dim J As Long           'Ponteiro de caracter no campo observa��o
Dim vTamObs As Long     'Guarda a qtd. de caracteres da coluna Observa��o
Dim vObs As String      'Guarda o conte�do da coluna de Observa��es
Dim vForn As String     'Guarda o c�digo do fornecedor para impress�o
Dim vFabr As String     'Guarda o c�digo de f�brica para impress�o
    
If Len(lblArquivo.Caption) = 0 Then
   MsgBox ("Caminho ou arquivo inv�lido! A opera��o ser� abortada."), vbCritical + vbExclamation, "CAD750 - Opera��o incorreta!"
   Exit Sub
End If

If MsgBox("Confirma gera��o do arquivo de log?", vbYesNo, "CAD750") = vbYes Then

    'Gera arquivo de log com o nome LODDMMAA.TXT
    Open "C:\CAD750\LOG\Lo" & Mid(vDt_Cad, 1, 2) & _
                        Mid(vDt_Cad, 4, 2) & Mid(vDt_Cad, 7, 2) & ".txt" For Output As #1
'    Open "F:\Sistemas\Oracle\CAD750\VB32\Producao\LOG\Lo" & Mid(vDt_Cad, 1, 2) & _
                        Mid(vDt_Cad, 4, 2) & Mid(vDt_Cad, 7, 2) & ".txt" For Output As #1
    
    Print #1, "LOG DO ARQUIVO: " & vNomeArq
    Print #1, ""
    Print #1, "Forn  Fabrica             Observa��es"
    Print #1, "----  ------------------- ------------------------------------------------------"
    
    'Pega linha a linha do grid para gravar as que cont�m campo de Observa��o <> ""
    For I = 1 To grdArquivo.Rows - 1
        
        grdArquivo.Row = I
        
        grdArquivo.Col = 1
        vForn = Trim(grdArquivo.Text)       'Pega o c�digo do fornecedor
        grdArquivo.Col = 2
        vFabr = Trim(grdArquivo.Text)       'Pega o c�digo de f�brica
        grdArquivo.Col = 22
        vObs = Trim(grdArquivo.Text)        'Pega as Observa��es
        
        'Se a coluna de Observa��o n�o estiver em branco
        If vObs <> "" Then
            
            'Zero ponteiro que vai me indicar que caracter no campo obs. estou lendo
            J = 0
            'Pego o tamanho da observa��o para esta linha
            vTamObs = CLng(Len(vObs))
            
            'Se o campo de observa��o contiver mais de 55 caracteres, o arquivo de log ter�
            'que conter mais de 1 linha de mensagens para aquele item
            'Conto 54 pois antes da Obs., a 1a. linha ir� conter o C�d.Forn - C�d.F�br. (26 caracteres)
            'E para a impress�o correta do arquivo s� pode ter 80 colunas
            
            'Se for menor ou igual a 54, gravo a linha no arquivo normalmente
            If vTamObs <= 54 Then
                Print #1, String(3 - Len(vForn), " ") & vForn & " - " & vFabr & Space(19 - Len(vFabr)) & " " & vObs
            'Sen�o
            Else
                'Imprimo os primeiros 54 caracteres
                Print #1, String(3 - Len(vForn), " ") & vForn & " - " & vFabr & Space(19 - Len(vFabr)) & " " & Mid(vObs, 1, 54)
                'Tamanho agora vai ser igual ao tamanho original - 54
                vTamObs = vTamObs - 54
                'Indico de que caracter vou come�ar a ler no cpo. Observa��o
                J = J + 55
                
                'Enquanto o tamanho for maior que 0
                Do While vTamObs > 0
                    'Se tamanho maior que 54, imprime os pr�ximos 54 caracteres e atualiza tam.e ponteiro
                    If vTamObs > 54 Then
                        Print #1, Space(26) & Mid(vObs, J, 54)
                        vTamObs = vTamObs - 54
                        J = J + 55
                    'Se tamanho menor que 54, imprime os caracteres que faltam
                    Else
                        Print #1, Space(26) & Mid(vObs, J, vTamObs)
                        vTamObs = 0
                    End If
                Loop
            End If
            
        End If
            
    Next I

    Print #1, ""
    Print #1, "***************** FIM DO ARQUIVO DE LOG ***********************"
    
    Close #1

    MsgBox "Arquivo gerado com sucesso!", vbInformation, "Aten��o!"
    Shell ("Explorer C:\CAD750\LOG"), vbNormalFocus

End If

End Sub

Private Sub cmdLog_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp2.Left = 8805
    lblHelp2.Top = 1410
    lblHelp2.Visible = True
    lblHelp2.Caption = "Gerar Arq.de Log"
End Sub


Private Sub cmdVoltar_Click()
    Unload Me
End Sub

Private Sub cmdVoltar_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp2.Left = 11295
    lblHelp2.Top = 1410
    lblHelp2.Visible = True
    lblHelp2.Caption = "Voltar"
End Sub


Private Sub Form_Load()
    grdArquivo.Visible = False
    lblHelp1.Visible = False
    lblHelp2.Visible = False
    lblMsg.Visible = False
    lblArquivo.Caption = ""
End Sub

Private Sub lblHelp1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp1.Visible = False
End Sub


Private Sub cmdArquivo_Click()
    
    'Vari�veis auxiliares para consulta ao banco
    Dim db_CONS2 As Object        'Variavel Bco. de Consulta
    Dim CmdSql2 As String         'String para o SQL
    Dim db_CONS3 As Object        'Variavel Bco. de Consulta
    Dim CmdSql3 As String         'String para o SQL
    'Vari�veis locais
    Dim I As Integer              'Ponteiro / Contador
    Dim vStrLinhaArq As String    'Guarda o peda�o (Coluna) do arquivo que est� sendo lido
    Dim vDivisao As String        'Guarda a divis�o a que o fornecedor pertence
    Dim vDPK As Double            'Guarda o c�digo DPK se o item j� estiver cadastrado
    Dim vFl_Forn As Boolean       'Indica se fornecedor atendeu �s condi��es
    Dim vFl_Fabr As Boolean       'Indica se f�brica atendeu �s condi��es
    Dim vCod_Trib_ICM As String   'Guarda o c�d. trib. Icm para depois validar class. fiscal
    Dim vCod_Trib_IPI As String   'Guarda o c�d. trib. Ipi para depois validar proced�ncia
    Dim vFl_Edi As String         'Guarda flag que diz se fornecedor trabalha com EDI ou n�o
    Dim vClassif_Forn As String   'Guarda classifica��o do fornecedor
    Dim objSubTrib As Object
    Dim vTexto As String
    
    
    'On Error GoTo Trata_Erro
    
    'Indica que o controle ser� usado para abrir o arquivo
    controle.ShowOpen
    lblArquivo.Caption = controle.FileName
    vNomeArq = lblArquivo

    'Verifica se foi indicado o diret�rio e nome do arquivo a ser lido
    If lblArquivo.Caption = "" Then
        MsgBox "Indique o arquivo a ser aberto", , MSG_TIT
        Exit Sub
    End If


    '/////////////////////////
    'MONTA O GRID
    '/////////////////////////
    grdArquivo.Visible = True

    grdArquivo.Row = 0
    
    grdArquivo.Col = 0
    grdArquivo.ColWidth(0) = 450
    grdArquivo.Text = "Reg"
    
    grdArquivo.Col = 1
    grdArquivo.ColWidth(1) = 400
    grdArquivo.Text = "Forn"
    
    grdArquivo.Col = 2
    grdArquivo.ColWidth(2) = 1000
    grdArquivo.Text = "C�d. F�brica"
    
    grdArquivo.Col = 3
    grdArquivo.ColWidth(3) = 3500
    grdArquivo.Text = "Descri��o"
    
    grdArquivo.Col = 4
    grdArquivo.ColWidth(4) = 350
    grdArquivo.Text = "Lin"
    
    grdArquivo.Col = 5
    grdArquivo.ColWidth(5) = 350
    grdArquivo.Text = "Gru"
    
    grdArquivo.Col = 6
    grdArquivo.ColWidth(6) = 350
    grdArquivo.Text = "Sub"
    
    grdArquivo.Col = 7
    grdArquivo.ColWidth(7) = 900
    grdArquivo.Text = "C�d. Mascarado"
    
    grdArquivo.Col = 8
    grdArquivo.ColWidth(8) = 700
    grdArquivo.Text = "Seq.Tab"
    
    grdArquivo.Col = 9
    grdArquivo.ColWidth(9) = 1000
    grdArquivo.Text = "Clas.Fisc."
    
    grdArquivo.Col = 10
    grdArquivo.ColWidth(10) = 900
    grdArquivo.Text = "Peso"
    
    grdArquivo.Col = 11
    grdArquivo.ColWidth(11) = 700
    grdArquivo.Text = "Vol"
    
    grdArquivo.Col = 12
    grdArquivo.ColWidth(12) = 350
    grdArquivo.Text = "Uni"
    
    grdArquivo.Col = 13
    grdArquivo.ColWidth(13) = 600
    grdArquivo.Text = "Tr.ICM"
    
    grdArquivo.Col = 14
    grdArquivo.ColWidth(14) = 600
    grdArquivo.Text = "Tr.IPI"
    
    grdArquivo.Col = 15
    grdArquivo.ColWidth(15) = 600
    grdArquivo.Text = "Pc.IPI"
    
    grdArquivo.Col = 16
    grdArquivo.ColWidth(16) = 400
    grdArquivo.Text = "Proc"
    
    grdArquivo.Col = 17
    grdArquivo.ColWidth(17) = 1100
    grdArquivo.Text = "Qt.Min.Forn"
    
    grdArquivo.Col = 18
    grdArquivo.ColWidth(18) = 1100
    grdArquivo.Text = "Qt.Min.Vda"
    
    grdArquivo.Col = 19
    grdArquivo.ColWidth(19) = 1100
    grdArquivo.Text = "Qt.Max.Vda"
    
    grdArquivo.Col = 20
    grdArquivo.ColWidth(20) = 3200
    grdArquivo.Text = "Cod.Fabr.EDI "
    
    grdArquivo.Col = 21
    grdArquivo.ColWidth(21) = 350
    grdArquivo.Text = "Cat"
    
    grdArquivo.Col = 22
    grdArquivo.ColWidth(22) = 5000
    grdArquivo.Text = "Observa��o"
    
    grdArquivo.Col = 23
    grdArquivo.ColWidth(23) = 1
    grdArquivo.Text = "Dpk"
    
    'Estas duas colunas ser�o utilizadas para identificar se o fornecedor trabalha com EDI ou n�o
    'E para identificar sua classifica��o que ser� utilizada no momento das consist�ncias e do cadastro
    grdArquivo.Col = 24
    grdArquivo.ColWidth(24) = 1
    grdArquivo.Text = "EDI"
    
    grdArquivo.Col = 25
    grdArquivo.ColWidth(25) = 1
    grdArquivo.Text = "Clas_Forn"
    
    
    'Esta coluna foi inserida posteriormente, devido �s altera��es para transfer�ncia autom�tica
    'cujo campo MIN_ESTOQUE da tabela TRANSFERENCIA.ESTOQUE_MINIMO, ser� alimentado e alterado
    'pelo cadastro de itens (autom�tico ou manual)
    grdArquivo.Col = 26
    grdArquivo.ColWidth(26) = 1110
    grdArquivo.Text = "Qt.Min.Est."
    
    'Coluna inserida em Maio/04 devido ao novo cadastro de fabricante - Conta 900 - Aleth�a
    grdArquivo.Col = 27
    grdArquivo.ColWidth(27) = 1100
    grdArquivo.Text = "Fabricante"
    
    
    
    '//////////////////////////
    'LEITURA DO ARQUIVO
    '//////////////////////////
    Open vNomeArq For Input As #1
    'Inicializa contadores de linhas do grid
    I = 0
    lblMsg.Visible = True
    
    'L� enquanto n�o for final de arquivo
    Do While Not EOF(1)
    
        Screen.MousePointer = 11
        I = I + 1
        
        lblMsg = "CARREGANDO ...." & I
        lblMsg.Refresh
        
        'Inicializa flags e vari�veis
        vFl_Forn = False
        vFl_Fabr = False
        vDPK = 0
        vClassif_Forn = ""
        vFl_Edi = ""
        vDivisao = ""
        vCod_Trib_ICM = ""
        vCod_Trib_IPI = ""
        vStrLinhaArq = ""
        
        
        'L� registro - linha - do arquivo, joga na vari�vel  e seta ponteiro para o pr�ximo
        Line Input #1, varLinhaArquivo
        'Se linha lida for em branco, sai do loop
        If Mid(varLinhaArquivo, 1, 203) = "" Then
            Exit Do
        End If
        
        
        'Qtd. Linhas do grid ser� correspondente � qtd. de linhas lidas do arquivo texto
        grdArquivo.Rows = I + 1
        
        grdArquivo.Row = I
                
        '1a Coluna vai receber a numera��o de registros lidos do arquivo
        grdArquivo.Col = 0
        grdArquivo.Text = Format(I, "0000")
        
        
        '////////////////////////////
        'CONSISTE FORNECEDOR
        '////////////////////////////
        vStrLinhaArq = Trim(Mid(varLinhaArquivo, 1, 3))
        If vStrLinhaArq = "" Then
            grdArquivo.Col = 22
            grdArquivo.Text = "Forn.n�o atende! "
        Else
            db.Parameters.Remove "FORN"
            db.Parameters.Add "FORN", Val(vStrLinhaArq), 1
            
            CmdSql = "Select Cod_Fornecedor, Classificacao, Fl_Edi, Divisao " & _
                     "From FORNECEDOR " & _
                     "Where Cod_Fornecedor = :FORN " & _
                     "And Situacao = 0 " & _
                     "And Classificacao in ('A', 'C')"
            
            Set db_CONS = db.DbCreateDynaset(CmdSql, 0&)
            
            'Se select n�o atender �s condi��es, coloca msg na col. de Obs. do grid
            If db_CONS.EOF Then
                grdArquivo.Col = 22
                grdArquivo.Text = "Forn.n�o atende! "
            Else
                vClassif_Forn = db_CONS!Classificacao
                vFl_Edi = db_CONS!Fl_Edi
                vDivisao = db_CONS!Divisao
                vFl_Forn = True
                '25a Coluna vai receber a informa��o do fl_edi p/ser usado posteriormente na grav��o do item
                grdArquivo.Col = 24
                grdArquivo.Text = Trim(vFl_Edi)
                '26a Coluna vai receber a informa��o da clas.do forn. p/ser usado posteriormente na grav��o do item
                grdArquivo.Col = 25
                grdArquivo.Text = Trim(vClassif_Forn)
            End If
        End If
        '2a Coluna vai receber c�d.do fornecedor lido do arq. independente de fornecedor atender ou n�o
        grdArquivo.Col = 1
        grdArquivo.Text = Format(vStrLinhaArq, "###")
    
        '////////////////////////////
        'CONSISTE C�D. F�BRICA
        '////////////////////////////
        vStrLinhaArq = Trim(Mid(varLinhaArquivo, 4, 19))
        If vStrLinhaArq = "" Then
            grdArquivo.Col = 22
            grdArquivo.Text = grdArquivo.Text & "Item Inv.! "
        Else
        
            db.Parameters.Remove "FABR"
            db.Parameters.Add "FABR", vStrLinhaArq, 1
            
            CmdSql = "Select c.Cod_Fornecedor, c.Cod_Fabrica, c.Cod_Dpk DPK, nvl(e.Situacao,0) Situacao " & _
                     "From ITEM_ESTOQUE e, ITEM_CADASTRO c " & _
                     "Where c.Cod_Fornecedor = :FORN " & _
                     "And c.Cod_Fabrica = :FABR " & _
                     "And e.Cod_Dpk(+) = c.Cod_Dpk "
            
            Set db_CONS = db.DbCreateDynaset(CmdSql, 0&)
            
            'Se encontrar algum item com mesmo cod.forn e fabr., verifica demais condi��es e coloca
            'mensagem no campo de observa��o
            If Not db_CONS.EOF Then
                If db_CONS!Situacao = 9 Then
                    grdArquivo.Col = 22
                    grdArquivo.Text = grdArquivo.Text & "Item desativado! "
                ElseIf db_CONS!Situacao = 8 Then
                    grdArquivo.Col = 22
                    grdArquivo.Text = grdArquivo.Text & "Item substitu�do! "
                ElseIf db_CONS!Situacao = 0 Then
                    grdArquivo.Col = 22
                    grdArquivo.Text = grdArquivo.Text & "Forn./F�br. existem - DPK " & db_CONS!DPK & " ! "
                End If
                vFl_Forn = True
            End If
            
        End If
        '3a Coluna vai receber c�d.de f�brica lido do arq. independente de atender ou n�o
        grdArquivo.Col = 2
        grdArquivo.Text = vStrLinhaArq
    
        'Se o fornecedor e f�brica j� estiverem cadastrados, copia o c�digo DPK para vari�vel
        If vFl_Forn = True And vFl_Fabr = True Then
            vDPK = CDbl(db_CONS!DPK)
        End If
        
        
        
        
        '////////////////////////////
        'CONSISTE DESCRI��O DO ITEM
        '////////////////////////////
        '4a Coluna vai receber c�d.de f�brica lido do arq.
        'Se o item j� estiver cadastrado, o cpo. Observa��o j� estar� com a msg. que a consist�ncia
        'do c�digo f�brica fez, sen�o vai cadastrar com a descri��o que veio no arquivo
        grdArquivo.Col = 3
        grdArquivo.Text = Trim(Mid(varLinhaArquivo, 23, 40))
        
        
        
        
        '////////////////////////////
        'CONSISTE LINHA
        '////////////////////////////
        vStrLinhaArq = Trim(Mid(varLinhaArquivo, 63, 2))
        If vStrLinhaArq = "" Then
            grdArquivo.Col = 22
            grdArquivo.Text = grdArquivo.Text & "Linha Inv.! "
        Else
            db.Parameters.Remove "LIN"
            db.Parameters.Add "LIN", Val(vStrLinhaArq), 1
            
            CmdSql = "Select Count(*) QtdLin " & _
                     "From LINHA " & _
                     "Where Cod_Linha = :LIN "
            
            Set db_CONS = db.DbCreateDynaset(CmdSql, 0&)
            
            'Se linha n�o cadastrada, coloca msg na col. de Obs. do grid
            If db_CONS!QtdLin = 0 Then
                grdArquivo.Col = 22
                grdArquivo.Text = grdArquivo.Text & "Linha n/cad.! "
            End If
        End If
    
        '5a Coluna vai receber a linha lida do arq. independente de atender ou n�o
        grdArquivo.Col = 4
        grdArquivo.Text = Format(vStrLinhaArq, "##")
        
        
        
        
        '////////////////////////////
        'CONSISTE GRUPO
        '////////////////////////////
        vStrLinhaArq = Trim(Mid(varLinhaArquivo, 65, 2))
        If vStrLinhaArq = "" Then
            grdArquivo.Col = 22
            grdArquivo.Text = grdArquivo.Text & "Grupo Inv.! "
        Else
            db.Parameters.Remove "GRU"
            db.Parameters.Add "GRU", Val(vStrLinhaArq), 1
            
            CmdSql = "Select Count(*) QtdGru " & _
                     "From GRUPO " & _
                     "Where Cod_Grupo = :GRU "
            
            Set db_CONS = db.DbCreateDynaset(CmdSql, 0&)
            
            'Se grupo n�o cadastrado, coloca msg na col. de Obs. do grid
            If db_CONS!QtdGru = 0 Then
                grdArquivo.Col = 22
                grdArquivo.Text = grdArquivo.Text & "Grupo n/cad.! "
            End If
        End If
        '6a Coluna vai receber c�d.do grupo lido do arq. independente de atender ou n�o
        grdArquivo.Col = 5
        grdArquivo.Text = Format(vStrLinhaArq, "##")
                
                
                
                
        '////////////////////////////
        'CONSISTE SUBGRUPO
        '////////////////////////////
        vStrLinhaArq = Trim(Mid(varLinhaArquivo, 67, 2))
        If vStrLinhaArq = "" Then
            grdArquivo.Col = 22
            grdArquivo.Text = grdArquivo.Text & "Subgr. Inv.! "
        Else
            db.Parameters.Remove "SUB"
            db.Parameters.Add "SUB", Val(vStrLinhaArq), 1
            
            CmdSql = "Select Count(*) QtdSub " & _
                     "From SUBGRUPO " & _
                     "Where Cod_Grupo = :GRU " & _
                     "And Cod_Subgrupo = :SUB"
            
            Set db_CONS = db.DbCreateDynaset(CmdSql, 0&)
            
            'Se grupo/subgrupo n�o cadastrados, coloca msg na col. de Obs. do grid
            If db_CONS!QtdSub = 0 Then
                grdArquivo.Col = 22
                grdArquivo.Text = grdArquivo.Text & "Subgrupo n/cad. p/o Grupo! "
            End If
        End If
    
        '7a Coluna vai receber c�d.do grupo lido do arq. independente de atender ou n�o
        grdArquivo.Col = 6
        grdArquivo.Text = Format(vStrLinhaArq, "##")
        
        
        
        
        '////////////////////////////
        'CONSISTE MASCARADO
        '////////////////////////////
        '8a Coluna vai receber c�d.MASCARADO lido do arq.
        'Se o item j� estiver cadastrado, o cpo. Observa��o j� estar� com a msg. que a consist�ncia
        'do c�digo f�brica fez, sen�o vai cadastrar com O MASCARADO que veio no arquivo
        grdArquivo.Col = 7
        grdArquivo.Text = Trim(Mid(varLinhaArquivo, 69, 19))
                
        
        
        
        '////////////////////////////
        'CONSISTE SEQUENCIA DE TABELA
        '////////////////////////////
        
        'Atribui a sequ�ncia na 9a. coluna do grid
        'Qualquer sequ�ncia (diferente de branco ""), que vier no arquivo, eu vou cadastrar,
        'independente de j� existir algum outro item com esta mesma sequ�ncia de tabela
        'O �nico problema de existirem dois itens com a mesma seq. tabela � que na hora
        'de imprimir a Lista de Pre�o, o sistema vai colocar estes dois itens na order
        'em que ele achar que � a correta (segundo Vanessa - Compras)
        'Por�m, se n�o vier nenhuma sequ�ncia no arquivo, somente na hora de gravar realmente
        'as informa��es � que vou procurar uma sequ�ncia que possa ser utilizada.
        grdArquivo.Col = 8
        grdArquivo.Text = Format(Trim(Mid(varLinhaArquivo, 88, 6)), "######")
        
                
                
                
        '////////////////////////////
        'CONSISTE COD. TRIBUTA��O ICM
        '////////////////////////////
        'Ser� necess�rio fazer esta consist�ncia antes, pois o resultado ser� usado
        'na consist�ncia da Classifica��o Fiscal
        
        'Se cod. tributa��o ICM do arquivo, vier em branco, coloca mensagem no cpo. Observa��o
        vStrLinhaArq = Trim(Mid(varLinhaArquivo, 122, 1))
        If vStrLinhaArq = "" Then
            grdArquivo.Col = 22
            grdArquivo.Text = grdArquivo.Text & "Cod.Trib.ICM Inv.! "
        'Se divis�o do fornecedor for DPK, o c�d.trib.icm deve ser (0,1,4 ou 8)
        'Conf.solic.controladoria, somente poder�o ser aceitos CT 1 ou 8  (CT 4 somente no CAD070) - 18/01/08
        'ElseIf vDivisao = "D" And vStrLinhaArq <> "0" And _
                                  vStrLinhaArq <> "1" And _
                                  vStrLinhaArq <> "4" And _
                                  vStrLinhaArq <> "8" Then
        ElseIf vDivisao = "D" And vStrLinhaArq <> "1" And vStrLinhaArq <> "8" Then
            grdArquivo.Col = 22
            grdArquivo.Text = grdArquivo.Text & "Cod.Trib.Icm Inv.! "
        'Se divis�o do fornecedor for Blau, o c�d.trib.icm deve ser (0 ou 4)
        'Conf.solic.controladoria, somente poder�o ser aceitos CT 1 ou 8  (CT 4 somente no CAD070) - 18/01/08
        'ElseIf vDivisao = "B" And vStrLinhaArq <> "0" And _
                                  vStrLinhaArq <> "4" Then
        ElseIf vDivisao = "B" And vStrLinhaArq <> "1" Then
            grdArquivo.Col = 22
            grdArquivo.Text = grdArquivo.Text & "Cod.Trib.Icm Inv.! "
        End If
        'Atribui o c�d.trib.ICM na 14a. coluna do grid, independente do resultado
        grdArquivo.Col = 13
        grdArquivo.Text = IIf(vStrLinhaArq = "", "", Format(vStrLinhaArq, "0"))
        vCod_Trib_ICM = vStrLinhaArq
        
        
        
                
        '////////////////////////////
        'CONSISTE CLASSIF. FISCAL
        '////////////////////////////
        vStrLinhaArq = Trim(Mid(varLinhaArquivo, 94, 10))
        If vStrLinhaArq = "" Then
            grdArquivo.Col = 22
            grdArquivo.Text = grdArquivo.Text & "Clas.Fiscal Inv.! "
        Else
            db.Parameters.Remove "FIS": db.Parameters.Add "FIS", Val(vStrLinhaArq), 1
            
            CmdSql = "Select * " & _
                     "From COMPRAS.SUBST_TRIBUTARIA " & _
                     "Where class_fiscal = :FIS "
            
            Set db_CONS = db.DbCreateDynaset(CmdSql, 0&)
            
            'Se n�o encontrar Class.Fiscal, verifica Cod.Trib.ICM e coloca msg na col. de Obs. do grid
            'Observa��o existente no Cda.Item - CAD070
            'Devido �s altera��es p/ carga de 06/08/2001, foi necess�rio consistir tamb�m o cod.tributa��o = 8
            'Aliq.Reduzida - Altera��es de Desc. Per�odo / Lista de Pre�o com 4% / etc. - Aleth�a (31/07/01)
            If db_CONS.EOF And (vCod_Trib_ICM = "1" Or vCod_Trib_ICM = "8") Then
                grdArquivo.Col = 22
                grdArquivo.Text = grdArquivo.Text & "Clas.Fiscal Inv.! "
            
            
            
            'Verifica��o implementada em 26/04/05 para que n�o d� problema na hora da venda, no caso de faltar algum
            'cadastro de uf origem destino - Alethea / Marici
            ElseIf (vCod_Trib_ICM = "1" Or vCod_Trib_ICM = "8") Then
            
                db.Parameters.Remove "FIS": db.Parameters.Add "FIS", Val(vStrLinhaArq), 1
                
                CmdSql = "Select :FIS class_fiscal, b.cod_uf UF1, a.cod_uf UF2 " & _
                            "From (Select b.cod_uf from loja a, cidade b where a.cod_cidade=b.cod_cidade) b, " & _
                            "     Uf a " & _
                           "Where (:FIS, b.cod_uf, a.cod_uf) NOT IN " & _
                                " (Select class_fiscal, cod_uf_origem, cod_uf_destino " & _
                                    "From compras.subst_tributaria " & _
                                   "Where class_fiscal = :FIS)"
                
                Set objSubTrib = db.DbCreateDynaset(CmdSql, 0&)
            
                'Se ainda houver algum cadastro a ser feito, monta um e-mail e manda para Fernanda do Fiscal
                vTexto = ""
                While Not objSubTrib.EOF
                
                    vTexto = vTexto & Trim(objSubTrib!Class_Fiscal) & String(12 - Len(Trim(objSubTrib!Class_Fiscal)), " ") & "   " & _
                                      Trim(objSubTrib!UF1) & " - " & Trim(objSubTrib!UF2) & vbCrLf
                    objSubTrib.MoveNext
                
                Wend
            
                If vTexto <> "" Then
                
                    vTexto = "Favor cadastrar na COMPRAS.SUBST_TRIBUTARIA, conforme discriminado abaixo: " & vbCrLf & vbCrLf & _
                             "Clas. Fiscal - UF Origem - UF Destino" & vbCrLf & vbCrLf & _
                             vTexto
                
                    db.Parameters.Remove "TEXTO": db.Parameters.Add "TEXTO", vTexto, 1
                    db.Parameters.Remove "vErro": db.Parameters.Add "vErro", 0, 2
                    'CmdSql = "Begin Intranet.Envia_Email('alethea@maxxipel.com.br','CLASS_SUBST_TRIBUTARIA', :TEXTO); End; "
                    CmdSql = "Begin Intranet.Envia_Email('fiscal@dpaschoal.com.br','CLASS_SUBST_TRIBUTARIA', :TEXTO); End; "
                    db.ExecuteSQL CmdSql
                End If
            
            
            Else
                'Alterado em 23/01/2003
                'Verifica na tabela ANTECIPACAO_TRIBUTARIA se existe class.fiscal cadastrada,
                'Se existir, o Cod.Tributacao (ICM) tem que ser = 1
                db.Parameters.Remove "UF_ORI": db.Parameters.Add "UF_ORI", db_CONS!Cod_Uf_Origem, 1
                db.Parameters.Remove "UF_DES": db.Parameters.Add "UF_DES", db_CONS!Cod_Uf_Destino, 1
                'Alterada estrutura de tabela a partir da entrada do CD Belem - Alethea - 08/06/05
                'Tabela ANTECIPACAO_TRIBUTARIA ser� inutilizada
                'CmdSql = "Select Count(*) QtdAnt " & _
                           "From Producao.Antecipacao_Tributaria " & _
                          "Where Cod_Uf_Origem = :UF_ORI " & _
                            "And Cod_Uf_Destino = :UF_DES "
                db.Parameters.Remove "FIS": db.Parameters.Add "FIS", Val(vStrLinhaArq), 1
                CmdSql = "Select Count(*) QtdAnt " & _
                           "From Compras.Class_Antec_Entrada " & _
                          "Where Cod_Uf_Origem = :UF_ORI " & _
                            "And Cod_Uf_Destino = :UF_DES " & _
                            "And Class_Fiscal = :FIS "
                
                Set db_CONS = db.DbCreateDynaset(CmdSql, 0&)
                
                If (db_CONS!QtdAnt > 0) And (vCod_Trib_ICM <> "1") Then
                    grdArquivo.Col = 22
                    grdArquivo.Text = grdArquivo.Text & "C�d.Trib.ICM tem que ser 1! "
                End If
            End If
        End If
        
        '10a Coluna vai receber clas.fiscal lida do arq. independente de atender ou n�o
        grdArquivo.Col = 9
        grdArquivo.Text = Format(vStrLinhaArq, "##########")
                
                
                
                
        '////////////////////////////
        'CONSISTE PESO
        '////////////////////////////
        vStrLinhaArq = Trim(Mid(varLinhaArquivo, 104, 9))
        'Se peso for maior do que 99999.999 coloca msg. no cpo. Observa��o
        If CDbl(vStrLinhaArq) > 99999.999 Then
            grdArquivo.Col = 22
            grdArquivo.Text = grdArquivo.Text & "Peso Inv.! "
        End If
        'Atribui peso que veio no arquivo na 11a. coluna do grid
        'Se peso do arquivo, vier em branco, atribui 0
        grdArquivo.Col = 10
        grdArquivo.Text = Format(IIf(vStrLinhaArq = "", 0, vStrLinhaArq), "####0.000")
                
                
                
                
        '////////////////////////////
        'CONSISTE VOLUME
        '////////////////////////////
        vStrLinhaArq = Trim(Mid(varLinhaArquivo, 113, 7))
        'Se peso for maior do que 999.999 coloca msg. no cpo. Observa��o
        If CDbl(vStrLinhaArq) > 999.999 Then
            grdArquivo.Col = 22
            grdArquivo.Text = grdArquivo.Text & "Vol. Inv.! "
        End If
        'Atribui vol. que veio no arquivo na 12a. coluna do grid
        'Se volume do arquivo, vier em branco, atribui 0
        grdArquivo.Col = 11
        grdArquivo.Text = Format(IIf(vStrLinhaArq = "", 0, vStrLinhaArq), "##0.000")
                
                
                
                
        '////////////////////////////
        'CONSISTE UNIDADE
        '////////////////////////////
        vStrLinhaArq = Trim(Mid(varLinhaArquivo, 120, 2))
        If vStrLinhaArq = "" Then
            grdArquivo.Col = 22
            grdArquivo.Text = grdArquivo.Text & "Unid. Inv.! "
        Else
            db.Parameters.Remove "UNI"
            db.Parameters.Add "UNI", vStrLinhaArq, 1
            
            CmdSql = "Select Count(*) QtdUni " & _
                     "From UNIDADE " & _
                     "Where Cod_Unidade = :UNI "
            
            Set db_CONS = db.DbCreateDynaset(CmdSql, 0&)
            
            If db_CONS!QtdUni = 0 Then
                grdArquivo.Col = 22
                grdArquivo.Text = grdArquivo.Text & "Unid. n/cad.! "
            End If
        End If
        
        '13a Coluna vai receber unidade lida do arq. independente de atender ou n�o
        grdArquivo.Col = 12
        grdArquivo.Text = vStrLinhaArq
                
                
                
                
        '////////////////////////////
        'CONSISTE COD. TRIBUTA��O IPI
        '////////////////////////////
        'Se cod. tributa��o IPI do arquivo vier em branco, coloca mensagem no cpo. Observa��o
        vStrLinhaArq = Trim(Mid(varLinhaArquivo, 123, 1))
        If vStrLinhaArq = "" Then
            grdArquivo.Col = 22
            grdArquivo.Text = grdArquivo.Text & "Cod.Trib.IPI Inv.! "
        'C�d.trib.ipi deve ser (0 ou 1)
        ElseIf vStrLinhaArq <> "0" And vStrLinhaArq <> "1" Then
            grdArquivo.Col = 22
            grdArquivo.Text = grdArquivo.Text & "Cod.Trib.IPI Inv.! "
        End If
        'Atribui o c�d.trib.IPI na 15a. coluna do grid, independente de atender ou n�o
        grdArquivo.Col = 14
        grdArquivo.Text = IIf(vStrLinhaArq = "", "", Format(vStrLinhaArq, "0"))
        vCod_Trib_IPI = vStrLinhaArq
                



        '////////////////////////////
        'CONSISTE PERCENTUAL IPI
        '////////////////////////////
        vStrLinhaArq = Trim(Mid(varLinhaArquivo, 124, 5))
        'Se peso for maior do que 99999.999 coloca msg. no cpo. Observa��o
        If CDbl(vStrLinhaArq) > 99.99 Then
            grdArquivo.Col = 22
            grdArquivo.Text = grdArquivo.Text & "%IPI Inv.! "
        End If
        'Atribui %IPI que veio no arquivo na 16a. coluna do grid, independente de atender ou n�o
        'Se %IPI do arquivo, vier em branco, atribui 0
        grdArquivo.Col = 15
        grdArquivo.Text = Format(IIf(vStrLinhaArq = "", 0, vStrLinhaArq), "#0.00")

        
        
        
        '////////////////////////////
        'CONSISTE COD_PROCEDENCIA
        '////////////////////////////
        'Para ter c�d.de proced�ncia, precisa ter cod.trib.IPI
        vStrLinhaArq = Trim(Mid(varLinhaArquivo, 129, 1))
        
        'Se c�d.proced�ncia vier em branco no arquivo, coloca msg.no cpo. Observa��o
        If vStrLinhaArq = "" Then
            grdArquivo.Col = 22
            grdArquivo.Text = grdArquivo.Text & "Proced.Inv.! "
        'Se cod. tributa��o IPI do arquivo vier em branco, coloca mensagem no cpo. Observa��o
        ElseIf vCod_Trib_IPI = "" Then
            grdArquivo.Col = 22
            grdArquivo.Text = grdArquivo.Text & "Proced.Inv.! "
        'Se cod.tributa��o IPI diferente de branco
        'Se item ainda n�o estiver cadastrado e a divis�o do fornecedor for DPK,
        'o c�digo de proced�ncia tem que ser = 0, sen�o coloca msg. no cpo. observa��o
        ElseIf vDPK = 0 And vDivisao = "D" And vStrLinhaArq <> "0" Then
            grdArquivo.Col = 22
            grdArquivo.Text = grdArquivo.Text & "Proced.Inv.! "
        'Sen�o verifica o cod.trib.ipi para ent�o validar proced�ncia
        Else
            'Se cod.trib.ipi = 1, procedencia pode ser 0, 1 ou 2
            'Em 20/11/06, foi alterado para que produtos de ct IPI = 1 (importados) somente possam ter procedencia estrangeira - Alethea
            'If vCod_Trib_IPI = "1" And vStrLinhaArq <> "0" And _
                                     vStrLinhaArq <> "1" And _
                                     vStrLinhaArq <> "2" Then
            If vCod_Trib_IPI = "1" And vStrLinhaArq <> "1" And _
                                     vStrLinhaArq <> "2" Then
                grdArquivo.Col = 22
                grdArquivo.Text = grdArquivo.Text & "Proced.Inv.! "
            'Sen�o procedencia pode ser 0, 1 ou 2
            ElseIf vStrLinhaArq <> "0" And StrLinhaArq <> "2" Then
                grdArquivo.Col = 22
                grdArquivo.Text = grdArquivo.Text & "Proced.Inv.! "
            End If
        End If
        
        'Atribui o c�d.proced�ncia na 17a. coluna do grid, independente de atender ou n�o
        grdArquivo.Col = 16
        grdArquivo.Text = IIf(vStrLinhaArq = "", "", Format(vStrLinhaArq, "0"))
        
        
        
        
        '////////////////////////////
        'CONSISTE QTD.MIN.DO FORNEC.
        '////////////////////////////
        vStrLinhaArq = Trim(Mid(varLinhaArquivo, 130, 10))
        'Se qtd.min.forn > 99999999.9, ou zero ou branco, coloca msg. no cpo. Observa��o
        If CDbl(vStrLinhaArq) > 99999999.9 Or CDbl(vStrLinhaArq) = 0 Or vStrLinhaArq = "" Then
            grdArquivo.Col = 22
            grdArquivo.Text = grdArquivo.Text & "Qt.Min.Forn. Inv.! "
        End If
        'Atribui qtd.min.forn que veio no arquivo na 18a. coluna do grid
        grdArquivo.Col = 17
        grdArquivo.Text = Format(IIf(CDbl(vStrLinhaArq) = 0, "", vStrLinhaArq), "#######0.0")
        
        
        
        
        '////////////////////////////
        'CONSISTE QTD.MIN. VENDA
        '////////////////////////////
        vStrLinhaArq = Trim(Mid(varLinhaArquivo, 140, 10))
        'Se qtd.min.vda > 99999999.9, ou zero ou branco, coloca msg. no cpo. Observa��o
        If CDbl(vStrLinhaArq) > 99999999.9 Or CDbl(vStrLinhaArq) = 0 Or vStrLinhaArq = "" Then
            grdArquivo.Col = 22
            grdArquivo.Text = grdArquivo.Text & "Qt.Min.Vda. Inv.! "
        End If
        'Atribui qtd.min.venda que veio no arquivo na 19a. coluna do grid
        grdArquivo.Col = 18
        grdArquivo.Text = Format(IIf(CDbl(vStrLinhaArq) = 0, "", vStrLinhaArq), "#######0.0")
        
        
        
        
        '////////////////////////////
        'CONSISTE QTD.M�X. VENDA
        '////////////////////////////
        vStrLinhaArq = Trim(Mid(varLinhaArquivo, 150, 10))
        'Se qtd.min.vda > 99999999.9, coloca msg. no cpo. Observa��o
        If CDbl(vStrLinhaArq) > 99999999.9 Then
            grdArquivo.Col = 22
            grdArquivo.Text = grdArquivo.Text & "Qt.Min.Vda. Inv.! "
        End If
        'Atribui qtd.min.venda que veio no arquivo na 20a. coluna do grid
        'Se vier em branco, atribui zero
        grdArquivo.Col = 19
        grdArquivo.Text = Format(IIf(CDbl(vStrLinhaArq) = 0, 0, vStrLinhaArq), "#######0.0")
        
        
        
        
        '////////////////////////////
        'CONSISTE C�D. F�BR. EDI
        '////////////////////////////
        vStrLinhaArq = Trim(Mid(varLinhaArquivo, 160, 30))
        'Se o fornecedor trabalhar com EDI, o c�digo de f�br. EDI � obrigat�rio
        If vFl_Edi = "S" And vStrLinhaArq = "" Then
            grdArquivo.Col = 22
            grdArquivo.Text = grdArquivo.Text & "C�d.F�br.EDI Inv.! "
        Else
            'Verifica se j� existe o c�d.f�br.edi cadastrado
            db.Parameters.Remove "EDI"
            db.Parameters.Add "EDI", vStrLinhaArq, 1
            
            CmdSql = "Select Cod_Dpk " & _
                     "From COMPRAS.R_ITEM_EDI " & _
                     "Where Cod_Fornecedor = :FORN " & _
                     "And Cod_Fabrica_Edi = :EDI"
            
            Set db_CONS = db.DbCreateDynaset(CmdSql, 0&)
            
            If Not db_CONS.EOF Then
                grdArquivo.Col = 22
                grdArquivo.Text = grdArquivo.Text & "C�d.F�br.EDI j� cad.p/DPK " & _
                                  db_CONS!cod_dpk & "! "
            End If
        
        End If
        '21a Coluna vai receber c�d.f�br.edi lida do arq. independente de atender ou n�o
        grdArquivo.Col = 20
        grdArquivo.Text = vStrLinhaArq
        
        
        
             
        '////////////////////////////
        'CONSISTE CATEGORIA
        '////////////////////////////
        vStrLinhaArq = Trim(Mid(varLinhaArquivo, 190, 1))
        'Se vier branco no arquivo, coloca msg.no cpo. Observa��o
        If vStrLinhaArq = "" Then
            grdArquivo.Col = 22
            grdArquivo.Text = grdArquivo.Text & "Categ. Inv.! "
        'Sen�o, verifica se a categoria existe
        Else
            db.Parameters.Remove "CATEG"
            db.Parameters.Add "CATEG", vStrLinhaArq, 1
        
            CmdSql = "Select Count(*) QtdCateg " & _
                     "From CATEG_SINAL " & _
                     "Where Categoria = :CATEG"
        
            Set db_CONS = db.DbCreateDynaset(CmdSql, 0&)
            
            If db_CONS!QtdCateg = 0 Then
                grdArquivo.Col = 22
                grdArquivo.Text = grdArquivo.Text & "Categ. Inv.! "
            End If
        End If
        '22a Coluna vai receber categoria lida do arq. independente de atender ou n�o
        grdArquivo.Col = 21
        grdArquivo.Text = Format(vStrLinhaArq, "#")
        
        
        
        '////////////////////////////
        'CONSISTE MIN.DE ESTOQUE DO ITEM
        '////////////////////////////
        vStrLinhaArq = Trim(Mid(varLinhaArquivo, 191, 10))
        'Se qtd.min.estoque > 99999999.9, coloca msg. no cpo. Observa��o
        If CDbl(vStrLinhaArq) > 99999999.9 Or CDbl(vStrLinhaArq) = 0 Or vStrLinhaArq = "" Then
            grdArquivo.Col = 22
            grdArquivo.Text = grdArquivo.Text & "Min.Est. Inv.! "
        End If
        'Atribui qtd.min.estoque que veio no arquivo na 27a. coluna do grid
        grdArquivo.Col = 26
        grdArquivo.Text = Format(IIf(CDbl(vStrLinhaArq) = 0, "", vStrLinhaArq), "#######0.0")
        
        
        '////////////////////////////
        'CONSISTE C�DIGO DO FABRICANTE
        '////////////////////////////
        vStrLinhaArq = Trim(Mid(varLinhaArquivo, 201, 3))
        'Se vier branco no arquivo, coloca msg.no cpo. Observa��o
        If vStrLinhaArq = "" Then
            grdArquivo.Col = 22
            grdArquivo.Text = grdArquivo.Text & "Fabricante Inv.! "
        'Sen�o, verifica se o fabricante existe
        Else
            db.Parameters.Remove "FABRICANTE"
            db.Parameters.Add "FABRICANTE", vStrLinhaArq, 1
        
            CmdSql = "Select Count(*) QtdFabr " & _
                     "From Producao.Fabricante " & _
                     "Where Cod_Fabricante = :FABRICANTE"
        
            Set db_CONS = db.DbCreateDynaset(CmdSql, 0&)
            
            If db_CONS!QtdFabr = 0 Then
                grdArquivo.Col = 22
                grdArquivo.Text = grdArquivo.Text & "Fabricante Inv.! "
            End If
        End If
        '26a Coluna vai receber fabricante lida do arq. independente de atender ou n�o
        grdArquivo.Col = 27
        grdArquivo.Text = Format(vStrLinhaArq, "###")
        
    Loop
    
    MsgBox "Arquivo Lido!", vbInformation, "Aten��o!"
    lblMsg = I & " Linhas Lidas do Arquivo!"
    lblMsg.Refresh
    
    Close #1    ' Close file.
    Screen.MousePointer = 0
        
    Exit Sub
        
        
Trata_Erro:
    
    If Err.Number = 53 Then
        MsgBox "Arquivo de Controle n�o Encontrado. ", , MSG_TIT
    Else
        Call Process_Line_Errors
    End If

End Sub




Private Sub lblHelp2_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp2.Visible = False
End Sub





Private Sub SSCommand1_Click()

End Sub

Private Sub scmdGravaEmergencia_Click()

'Op��o criada para gravar item nas demais tabelas, quando o mesmo foi criado
'somente na Item_Cadastro - Alethea - 13/03/08

'PARAMETROS:
'        P_CATEGORIA     IN  PRODUCAO.ITEM_ANALITICO.CATEGORIA%TYPE,         -- POSI��O 190 DO ARQUIVO .TXT A SER IMPORTADO (01 POSI��O)
'        P_FABRICANTE    IN  PRODUCAO.R_ITEM_FABRICANTE.COD_FABRICANTE%TYPE, -- POSI��O 201 DO ARQUIVO .TXT A SER IMPORTADO (03 POSI��ES)
'        P_QTD_MAXVDA    IN  PRODUCAO.ITEM_ESTOQUE.QTD_MAXVDA%TYPE,          -- POSI��O 150 DO ARQUIVO .TXT A SER IMPORTADO (10 POSI��ES)
'        P_MIN_ESTOQUE   IN  TRANSFERENCIA.ESTOQUE_MINIMO.MIN_ESTOQUE%TYPE,  -- POSI��O 191 DO ARQUIVO .TXT A SER IMPORTADO (10 POSI��ES)
'        P_COD_FABR_EDI  IN  COMPRAS.R_ITEM_EDI.COD_FABRICA_EDI%TYPE,        -- POSI��O 160 DO ARQUIVO .TXT A SER IMPORTADO (30 POSI��ES)
'        CodErro OUT Number, MsgErro OUT Varchar2


db.Parameters.Remove "cod_errora": db.Parameters.Add "cod_errora", 0, 2
db.Parameters.Remove "txt_errora": db.Parameters.Add "txt_errora", "", 2
  
Screen.MousePointer = 11
  
CmdSql = "Begin PRODUCAO.PR_CAD_ITENS_AUX(:cod_errora,:txt_errora); End; "
db.ExecuteSQL CmdSql


vERRO = IIf(IsNull(db.Parameters("cod_errora").Value), 0, db.Parameters("cod_errora").Value)
If Val(vERRO) <> 0 Then
    MsgBox db.Parameters("txt_errora") & ".Ligue para o Depto de Sistemas", 0, "Aten��o"
    Screen.MousePointer = 11
    Exit Sub
End If

Screen.MousePointer = 0

MsgBox "Processamento OK!"


End Sub


Private Sub SSFrame2_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp1.Visible = False
    lblHelp2.Visible = False
End Sub


Private Sub SSFrame3_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp1.Visible = False
End Sub



































