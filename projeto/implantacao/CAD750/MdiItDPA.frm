VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "comctl32.ocx"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form mdiITDPA 
   BackColor       =   &H00C0C0C0&
   Caption         =   "CAD750 - Inclus�o Autom�tica de Itens DPA"
   ClientHeight    =   7815
   ClientLeft      =   60
   ClientTop       =   630
   ClientWidth     =   13200
   LinkTopic       =   "MDIForm1"
   ScaleHeight     =   7815
   ScaleWidth      =   13200
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.Frame Frame1 
      Caption         =   "Atalhos"
      Height          =   1290
      Left            =   105
      TabIndex        =   1
      Top             =   105
      Width           =   2115
      Begin Threed.SSCommand cmdSair 
         Height          =   705
         Left            =   1215
         TabIndex        =   2
         Top             =   405
         Width           =   705
         _Version        =   65536
         _ExtentX        =   1244
         _ExtentY        =   1244
         _StockProps     =   78
         Picture         =   "MdiItDPA.frx":0000
      End
      Begin VB.Image cmdCadastrar 
         Height          =   870
         Left            =   105
         Picture         =   "MdiItDPA.frx":031A
         Stretch         =   -1  'True
         Top             =   300
         Width           =   975
      End
   End
   Begin ComctlLib.StatusBar stsBar 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   7530
      Width           =   13200
      _ExtentX        =   23283
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
         NumPanels       =   1
         BeginProperty Panel1 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
            Object.Tag             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuCadastrar 
      Caption         =   "&Cadastrar"
      Begin VB.Menu mnuItens 
         Caption         =   "&Itens"
      End
   End
   Begin VB.Menu mnuSobre 
      Caption         =   "S&obre"
   End
   Begin VB.Menu mnuSair 
      Caption         =   "&Sair"
   End
End
Attribute VB_Name = "mdiITDPA"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdCadastrar_Click()
    frmCadastro.Show vbModal
End Sub

Private Sub cmdCadastrar_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    'William Leite
    'SSFrame2.Visible = True
    'lblMsg.Visible = True
    'lblMsg.Caption = "Faz o cadastro autom�tico de itens DPA por leitura de arquivo."
    stsBar.Panels.Item(1).Text = "Faz o cadastro autom�tico de itens DPA por leitura de arquivo!"
    stsBar.Panels.Item(1).AutoSize = sbrContents
End Sub

Private Sub cmdSair_Click()
    End
End Sub

Private Sub cmdSair_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    'William Leite
    'lblMsg.Visible = True
    'lblMsg.Caption = "Sai do Programa"
    stsBar.Panels.Item(1).Text = "Encerra programa!"
    stsBar.Panels.Item(1).AutoSize = sbrContents
End Sub

Private Sub Form_Load()
  On Error GoTo TrataErro
    
    If App.PrevInstance Then
        MsgBox "J� EXISTE UMA INST�NCIA DO PROGRAMA NO AR"
        End
    End If
    
    'posicionar tela
    mdiITDPA.Top = 0
    mdiITDPA.Left = 0
    mdiITDPA.Width = Screen.Width
    mdiITDPA.Height = Screen.Height
    'mdiITDPA.Width = 13520
    'mdiITDPA.Height = 8500
        
    'Conexao oracle
    Set dbOra = CreateObject("oracleinproCServer.xorasession")
    
    'CAMPINAS
    Set db = dbOra.OpenDatabase("SDPK_DEV", "CAD750/PROD", 0&)
    'Set db = dbOra.OpenDatabase("sdpk_teste", "CAD750/PROD", 0&)
           
    'Pega a data de faturamento do sistema para usar na montagem do nome do arq. de log e p/
    'servir de refer�ncia na data do cadastramento
    CmdSql = " Select To_Char(dt_faturamento,'dd/mm/yyyy') Dt_Cad From DATAS "
    Set db_CONS = db.DbCreateDynaset(CmdSql, 0&)
    
    vDt_Cad = Format(db_CONS!Dt_Cad, "dd/mm/yy")
    vAnoMes = Format(db_CONS!Dt_Cad, "yyyymm")
           
    'VAR_CAR_INVALIDO = "�����������������������/%$#-*=@:|\&+_!?<>;"
    'VAR_CAR_VALIDO = "AAAAAEEEEIIII00000UUUUC..................."
    VAR_CAR_INVALIDO = "�����������������������"
    VAR_CAR_VALIDO = "AAAAAEEEEIIIIOOOOOUUUUC"
           
    Exit Sub

TrataErro:
    MsgBox "Ocorreu o erro: " & Err & "-" & Err.Description, vbExclamation, "Aten��o"
    Exit Sub
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    stsBar.Panels.Item(1).Text = ""
    stsBar.Panels.Item(1).AutoSize = sbrContents
    stsBar.Panels.Item(1).Text = Date & " - " & Time
End Sub

Private Sub mnuItens_Click()
    Call cmdCadastrar_Click
End Sub

Private Sub mnuSair_Click()
    Call cmdSair_Click
End Sub

Private Sub mnuSobre_Click()
   frmSobre.Show vbModal
End Sub


