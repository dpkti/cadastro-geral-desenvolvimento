create or replace package PCK_CAD550  is

  -- Author  : CARLOS.MOURA
  -- Created : 19/7/2004 15:01:16
  -- Purpose : 

  type tp_cursor is ref cursor;

  
 PROCEDURE pr_deletar;

 PROCEDURE pr_inserir
 (
  pm_data_uss in varchar2,
  pm_valor_uss in dolar_diario_aux.valor_uss%type
  );

 PROCEDURE pr_inserir_AUX
 (
  pm_data_uss in varchar2,
  pm_valor_uss in dolar_diario_aux.valor_uss%type
  );

 PROCEDURE PR_GRAVA_DOLAR;

 PROCEDURE pr_comparar
 ( 
   p_cursor	in out tp_cursor,
   pm_data_uss in varchar2
  );

 PROCEDURE pr_faturamento
 ( 
   p_cursor	in out tp_cursor
  );

  PROCEDURE pr_consistir
  (  
    pm_data_uss in varchar2,
    pm_valor_uss in dolar_diario_aux.valor_uss%type,
    pm_compara out number
  ); 
   

end ;
/
create or replace package body PCK_CAD550  is

PROCEDURE pr_deletar

 is 
 begin

    DELETE PRODUCAO.DOLAR_DIARIO_AUX;
    COMMIT;

 END pr_deletar;

PROCEDURE PR_GRAVA_DOLAR

IS

BEGIN

    INSERT INTO PRODUCAO.DOLAR_DIARIO_AUX
    SELECT DISTINCT A.DATA_USS, A.VALOR_USS
      FROM PRODUCAO.TMP_DOLAR_DIARIO_AUX A
     WHERE (A.DATA_USS) NOT IN
           (SELECT B.DATA_USS
             FROM PRODUCAO.DOLAR_DIARIO_AUX B
            WHERE A.DATA_USS=B.DATA_USS);

    COMMIT;

END PR_GRAVA_DOLAR;

 PROCEDURE pr_inserir
 (
  pm_data_uss in varchar2,
  pm_valor_uss in dolar_diario_aux.valor_uss%type
  )

 is 
 begin
   INSERT INTO DOLAR_DIARIO_AUX
   VALUES (to_date(pm_data_uss,'DD/MM/RRRR'),pm_valor_uss);
   
END pr_inserir;

 PROCEDURE pr_inserir_AUX
 (
  pm_data_uss in varchar2,
  pm_valor_uss in dolar_diario_aux.valor_uss%type
  )

 is 
 begin
   INSERT INTO TMP_DOLAR_DIARIO_AUX
   VALUES (to_date(pm_data_uss,'DD/MM/RRRR'),pm_valor_uss);
   
END pr_inserir_AUX;


 PROCEDURE pr_comparar
 ( 
   p_cursor	in out tp_cursor,
   pm_data_uss in varchar2
  )
 is 
 begin
    open p_cursor for
    SELECT VALOR_USS 
    FROM DOLAR_DIARIO
    WHERE data_uss= to_date(pm_data_uss,'DD-MM-RR');

 END pr_comparar;

 PROCEDURE pr_faturamento
 ( 
   p_cursor	in out tp_cursor
  )
  is 
  begin
    open p_cursor for
    SELECT to_char(DT_FATURAMENTO, 'MM/RR') MES_ANO
    FROM DATAS;

 END pr_faturamento;

  PROCEDURE pr_consistir
  (  
    pm_data_uss in varchar2,
    pm_valor_uss in dolar_diario_aux.valor_uss%type,
    pm_compara out number
  )
  is 

  vValor Number :=0;
  vData VarChar2(15) :='';

  Cursor C_dolar Is
      SELECT * FROM DOLAR_DIARIO_AUX WHERE to_date(DATA_USS,'DD-MM-RR')=to_date(pm_data_uss,'DD-MM-RR');
  L_DOLAR C_DOLAR%RowType;

  begin

		Open C_DOLAR;
			Fetch C_DOLAR Into L_DOLAR;
			If C_DOLAR%NotFound Then
				vValor := 0;
                vData := '';
			Else
				vValor := L_DOLAR.VALOR_USS;
				vData := L_DOLAR.DATA_USS;
			End If;
		Close C_DOLAR;

    if vValor <> pm_valor_uss then
        pm_compara:=1;
    else
        pm_compara:=0;
    end if;
    if to_date(vData,'DD-MM-RR') <> to_date(pm_data_uss,'DD-MM-RR') then
        pm_compara:=1;
    else
        pm_compara:=0;
    end if;

  END pr_consistir;

end ;
/
