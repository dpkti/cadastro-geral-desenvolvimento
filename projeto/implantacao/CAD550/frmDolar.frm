VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmDolar 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Atualizar D�lar "
   ClientHeight    =   3390
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   2715
   Icon            =   "frmDolar.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   226
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   181
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame1 
      Caption         =   "�ndice:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   780
      Left            =   45
      TabIndex        =   10
      Top             =   930
      Width           =   2595
      Begin VB.OptionButton optIndice 
         Caption         =   "IDPc"
         Height          =   360
         Index           =   1
         Left            =   1560
         TabIndex        =   12
         Top             =   315
         Width           =   750
      End
      Begin VB.OptionButton optIndice 
         Caption         =   "IDPe"
         Height          =   360
         Index           =   0
         Left            =   345
         TabIndex        =   11
         Top             =   315
         Width           =   750
      End
   End
   Begin VB.Frame fra 
      Caption         =   "Data:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1275
      Left            =   45
      TabIndex        =   8
      Top             =   1785
      Width           =   2595
      Begin VB.TextBox txtAno 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1110
         MaxLength       =   2
         TabIndex        =   1
         Top             =   750
         Width           =   615
      End
      Begin VB.TextBox txtMes 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1110
         MaxLength       =   2
         TabIndex        =   0
         Top             =   300
         Width           =   615
      End
      Begin VB.Label lblAno 
         Appearance      =   0  'Flat
         Caption         =   "Ano:"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   570
         TabIndex        =   2
         Top             =   780
         Width           =   435
      End
      Begin VB.Label lblMes 
         Appearance      =   0  'Flat
         Caption         =   "M�s:"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   225
         Left            =   570
         TabIndex        =   9
         Top             =   360
         Width           =   540
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   3
      Top             =   3060
      Width           =   2715
      _ExtentX        =   4789
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   4736
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   810
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   5
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmDolar.frx":23D2
      PICN            =   "frmDolar.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdGravar 
      Default         =   -1  'True
      Height          =   690
      Left            =   1935
      TabIndex        =   6
      TabStop         =   0   'False
      ToolTipText     =   "Atualizar D�lar Auxiliar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmDolar.frx":30C8
      PICN            =   "frmDolar.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdLimpar 
      Height          =   690
      Left            =   990
      TabIndex        =   7
      TabStop         =   0   'False
      ToolTipText     =   "Limpar Campos"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmDolar.frx":3DBE
      PICN            =   "frmDolar.frx":3DDA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmDolar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdGravar_Click()

    Dim dolar_aux As String
    
    On Error GoTo TrataErro
    
    flErro = False
    
    If Trim(txtMes) = "" Then
        Call vVB_Generica_001.Informar("M�s Em Branco!")
        txtMes.SetFocus
        Exit Sub
    End If
    If Val(txtMes) > 12 Or Val(txtMes) = 0 Then
        Call vVB_Generica_001.Informar("M�s Inv�lido!")
        txtMes = ""
        txtMes.SetFocus
        Exit Sub
    End If
    If Trim(txtAno) = "" Then
        Call vVB_Generica_001.Informar("Ano Em Branco!")
        txtAno.SetFocus
        Exit Sub
    End If
    
    'andrevp - SDS2028 - 25/03/10 - Inclus�o do �ndice IDPc - In�cio
    If optIndice(0).Value = 0 And optIndice(1).Value = 0 Then
        Call vVB_Generica_001.Informar("Escolha o tipo de �ndice a ser atualizado!")
        cmdGravar.SetFocus
        Exit Sub
    End If
    
    If vVB_Generica_001.Perguntar("Confirma a importa��o do indice " & IIf(optIndice(0).Value, "IDP", "IDPc") & " ?") = 7 Then
        cmdGravar.SetFocus
        Exit Sub
    End If
    'andrevp - SDS2028 - 25/03/10 - Inclus�o do �ndice IDPc - fim
    
    
    Call Aguardar
    
    'mapeamento DPK: \\office_dpk\usuarios$\contab
    'vNomeArquivo = "g:\usr\contab\indcdp\usdp"
    'vNomeArquivo = "H:\USR\CARLOS\CAD550\dolar_auxiliar"
    
    'andrevp - SDS2028 - 25/03/10 - Inclus�o do �ndice IDPc - In�cio
    If optIndice(0).Value Then
       vNomeArquivo = "\\office_dpk\usuarios$\CONTAB\INDCDP\USDP" & txtMes.Text & txtAno.Text & ".txt"
    Else
       
       'andrevp - SDS2028 - 25/03/10 - Inclus�o do �ndice IDPc - In�cio
       '*************** Eliminar essa linha ************************
       'vNomeArquivo = "C:\Andre\Novo IDP\arquivos TXT\USDPC" & txtMes.Text & txtAno.Text & ".txt"
       
       'andrevp - SDS2028 - 25/03/10 - Inclus�o do �ndice IDPc - In�cio
       '*************** Voltar essa linha ************************
       vNomeArquivo = "\\office_dpk\usuarios$\CONTAB\INDCDP\USDPC" & txtMes.Text & txtAno.Text & ".txt"
       
    End If
    'andrevp - SDS2028 - 25/03/10 - Inclus�o do �ndice IDPc - fim
    
    If Dir(vNomeArquivo, vbArchive) = "" Then
        Unload frmAguardar
        Call vVB_Generica_001.Informar("Arquivo " & vNomeArquivo & " n�o Encontrado!")
        Call cmdLimpar_Click
        Exit Sub
    End If
    
    Call Limpa_Dolar_Diario_Aux
    
    vLinha = 0
    
    Open vNomeArquivo For Input As #1
    
    vAux_Dolar_Ant = 0
    vAux_Data_Ant = "01-Jan-99"
    
    While Not EOF(1)
    
        Line Input #1, dolar_aux
        
        vLinha = vLinha + 1

        If Trim(Mid(dolar_aux, 1, 10)) = "" Or Trim(Mid(dolar_aux, 12, 24)) = "" Then
            Unload frmAguardar
            Call vVB_Generica_001.Informar("Erro - o Arquivo Est� em Branco na Linha " & vLinha & "!")
            Close (1)
            vBanco.ExecuteSQL "Rollback"
            Call cmdLimpar_Click
            Exit Sub
        End If
        vAux_Data = Format(Mid(dolar_aux, 1, 10), "dd-mm-yy")
        If Not IsNumeric(Mid(dolar_aux, 12, 24)) Then
            Unload frmAguardar
            Call vVB_Generica_001.Informar("Erro - Este Arquivo Cont�m Valor de D�lar Incorreto na Linha " & vLinha & ":" & vbCrLf & dolar_aux & vbCr & "Favor Verificar!")
            Close (1)
            vBanco.ExecuteSQL "Rollback"
            Call cmdLimpar_Click
            Exit Sub
        End If
        vAux_Dolar = Mid(dolar_aux, 12, 24)
        Call Procura_Letra(CStr(vAux_Dolar), dolar_aux)
        If flErro = True Then
            Close (1)
            vBanco.ExecuteSQL "Rollback"
            Call cmdLimpar_Click
            Exit Sub
        End If
        Call Procura_Letra(vAux_Data, dolar_aux)
        If flErro = True Then
            Close (1)
            vBanco.ExecuteSQL "Rollback"
            Call cmdLimpar_Click
            Exit Sub
        End If
        Call Compara_Dolar(dolar_aux)
        If flErro = True Then
            Close (1)
            vBanco.ExecuteSQL "Rollback"
            Call cmdLimpar_Click
            Exit Sub
        End If
        Call Compara_Data(dolar_aux)
        If flErro = True Then
            Close (1)
            vBanco.ExecuteSQL "Rollback"
            Call cmdLimpar_Click
            Exit Sub
        End If
        
        Call vVB_Generica_001.ExcluiBind(vBanco)
        vBanco.Parameters.Add "vData", vAux_Data, 1
        vBanco.Parameters.Add "vDolar", vAux_Dolar, 1
        
        'andrevp - SDS2028 - 25/03/10 - Inclus�o do �ndice IDPc - In�cio
        If frmDolar.optIndice(0) Then
           vSql = "PRODUCAO.pck_cad550.pr_inserir_AUX(:vData,:vDolar)"
        Else
           vSql = "PRODUCAO.pck_cad550c.pr_inserir_AUX(:vData,:vDolar)"
        End If
        'andrevp - SDS2028 - 25/03/10 - Inclus�o do �ndice IDPc - Fim
        
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        
        If vErro <> "" Then
            Close (1)
            Unload frmAguardar
            vVB_Generica_001.ProcessaErro (vErro)
            vBanco.ExecuteSQL "Rollback"
            Call cmdLimpar_Click
            Exit Sub
        End If
    Wend
    
    'andrevp - SDS2028 - 25/03/10 - Inclus�o do �ndice IDPc - In�cio
    If frmDolar.optIndice(0) Then
       vBanco.ExecuteSQL "BEGIN producao.pck_CAD550.PR_GRAVA_DOLAR; END;"
    Else
       vBanco.ExecuteSQL "BEGIN producao.pck_CAD550c.PR_GRAVA_DOLAR; END;"
    End If
    'andrevp - SDS2028 - 25/03/10 - Inclus�o do �ndice IDPc - Fim
    
    Call cmdLimpar_Click
    vLinha = 0
    Seek #1, 1
    While Not EOF(1)
        Line Input #1, dolar_aux
        vLinha = vLinha + 1
        vAux_Data = Format(Mid(dolar_aux, 1, 10), "dd-mm-yy")
        vAux_Dolar = Mid(dolar_aux, 12, 24)
        Call vVB_Generica_001.ExcluiBind(vBanco)
        vBanco.Parameters.Add "vData", vAux_Data, 1
        vBanco.Parameters.Add "vDolar", vAux_Dolar, 1
        vBanco.Parameters.Add "vCompara", 0, 2
        
        'andrevp - SDS2028 - 25/03/10 - Inclus�o do �ndice IDPc - In�cio
        If frmDolar.optIndice(0) Then
           vSql = "PRODUCAO.pck_cad550.pr_consistir(:vData,:vDolar,:vCompara)"
        Else
           vSql = "PRODUCAO.pck_cad550c.pr_consistir(:vData,:vDolar,:vCompara)"
        End If
        'andrevp - SDS2028 - 25/03/10 - Inclus�o do �ndice IDPc - Fim
        
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        If vErro <> "" Then
            GoTo TrataErro
            Exit Sub
        Else
            If Val(vParameters("vCompara").Value) <> 0 Then
                Call vVB_Generica_001.Informar("H� Diverg�ncia nos Valores Enviados ao Banco na Linha " & vLinha & "." _
                & Chr(10) & "Favor Ligar no Departamento de Sistemas.")
                Close (1)
                Exit Sub
            End If
        End If
    Wend
    
    Unload frmAguardar
    
    Call vVB_Generica_001.Informar("Cadastro de D�lar Gravado com Sucesso!")
    
    Close (1)
    
    Exit Sub
    
TrataErro:

    Unload frmAguardar
    Call vVB_Generica_001.Informar("Erro - Este Arquivo Cont�m Caracteres Incorretos na Linha " & vLinha & ":" & vbCrLf & dolar_aux & vbCr & "Favor Verificar!")
    Close (1)
    
End Sub


Private Sub cmdLimpar_Click()

    txtMes = ""
    txtAno = ""
    txtMes.SetFocus
    
End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    
    Me.Top = 0
    Me.Left = 0
    
End Sub


Private Sub Option1_Click(Index As Integer)

End Sub

Private Sub txtAno_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Numero(KeyAscii)
    
End Sub


Private Sub txtAno_LostFocus()

    Dim Data_Fatur As String
    Dim Data_Digit As String
    
    If Trim(txtAno) = "" Then
        Exit Sub
    End If
    If Len(txtAno.Text) = 1 Then
        txtAno.Text = "0" & txtAno.Text
    End If
    If Trim(txtMes) = "" Then
        Exit Sub
    End If
    If Val(txtMes) > 12 Or Val(txtMes) = 0 Then
        Exit Sub
    End If
    Call vVB_Generica_001.ExcluiBind(vBanco)
    Call Criar_Cursor("vCursor")
    vSql = "producao.pck_cad550.pr_faturamento(:vCursor)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro)
        Exit Sub
    Else
        Set vObjOracle = vParameters("vCursor").Value
    End If
    Data_Fatur = vObjOracle!MES_ANO
    Data_Digit = txtMes.Text & "/" & txtAno.Text
    If CDate(Data_Digit) < CDate(Data_Fatur) Then
        Call vVB_Generica_001.Informar("O M�s Para Processamento N�o Pode Ser Anterior ao Atual!")
        Call cmdLimpar_Click
    End If
    
End Sub


Private Sub txtMes_Change()

    If Len(txtMes) = 2 Then
        txtAno.SetFocus
    End If

End Sub

Private Sub txtMes_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Numero(KeyAscii)
    
End Sub

Private Sub txtMes_LostFocus()

    txtMes = Format(txtMes.Text, "00")
        
End Sub

