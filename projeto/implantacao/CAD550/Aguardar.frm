VERSION 5.00
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form frmAguardar 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   0  'None
   ClientHeight    =   1875
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   4695
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   1875
   ScaleWidth      =   4695
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   255
      Left            =   60
      TabIndex        =   2
      Top             =   1560
      Width           =   4575
      _ExtentX        =   8070
      _ExtentY        =   450
      Color1          =   16711680
      Color2          =   0
      BackColor       =   -2147483633
      Begin VB.Label Label4 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BackStyle       =   0  'Transparent
         Caption         =   "Este processo pode levar alguns minutos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   60
         MousePointer    =   1  'Arrow
         TabIndex        =   3
         Top             =   0
         Width           =   4455
      End
   End
   Begin VB.Image Image1 
      Height          =   1515
      Left            =   -45
      Picture         =   "AGUARDAR.frx":0000
      Stretch         =   -1  'True
      Top             =   0
      Width           =   1830
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      Caption         =   "O sistema est� procurando as informa��es solicitadas."
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   1680
      TabIndex        =   1
      Top             =   660
      Width           =   2895
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      Caption         =   "Aguarde..."
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1680
      TabIndex        =   0
      Top             =   180
      Width           =   2895
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00000000&
      Height          =   1875
      Left            =   0
      Top             =   0
      Width           =   4695
   End
End
Attribute VB_Name = "frmAguardar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
    Screen.MousePointer = vbHourglass
End Sub
Private Sub Form_Unload(Cancel As Integer)
    Screen.MousePointer = vbDefault
End Sub

Private Sub Timer1_Timer()
    If Label2.Top > 0 Then
        Label2.Top = Label2.Top - 30
    Else
        If Label2.Top > -1080 Then
            Label2.Top = Label2.Top - 30
        ElseIf Label2.Top <= -1080 Then
            Label2.Top = 1140
        End If
    End If
    Me.Refresh
End Sub
