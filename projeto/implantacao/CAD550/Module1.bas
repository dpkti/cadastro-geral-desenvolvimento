Attribute VB_Name = "Module1"
Public vParameters As Object
Public vNomeArquivo As String
Public vAux_Data As String
Public vAux_Data_Ant As String
Public vAux_Dolar As Double
Public vAux_Dolar_Ant As Double
Public vVal_Dolar As Double
Public flErro As Boolean
Public vLinha As Double

Sub Criar_Cursor(pNomeCursor As String)

    vParameters.Remove pNomeCursor
    vParameters.Add pNomeCursor, 0, 3
    vParameters(pNomeCursor).ServerType = 102
    vParameters(pNomeCursor).DynasetOption = &H2&
    vParameters(pNomeCursor).DynasetCacheParams 256, 16, 20, 2000, 0

End Sub

Sub Compara_Data(vLinhaProcessada As String)

    If CDate(vAux_Data) <= CDate(vAux_Data_Ant) Then
        Unload frmAguardar
        Call vVB_Generica_001.Informar("Erro - Este Arquivo Cont�m Data Incorreta na Linha " & vLinha & ":" & vbCrLf & vLinhaProcessada & vbCr & "Favor Verificar!")
        frmDolar.txtMes = ""
        frmDolar.txtAno = ""
        frmDolar.txtMes.SetFocus
        flErro = True
        Exit Sub
    End If

    vAux_Data_Ant = vAux_Data

End Sub

Sub Compara_Dolar(vLinhaProcessada As String)

    On Error GoTo TrataErro
        
    Call vVB_Generica_001.ExcluiBind(vBanco)
    Call Criar_Cursor("vCursor")
    vBanco.Parameters.Add "vData", vAux_Data, 1
    
    'andrevp - SDS2028 - 25/03/10 - Inclus�o do �ndice IDPc - In�cio
    If frmDolar.optIndice(0) Then
       vSql = "PRODUCAO.pck_cad550.pr_comparar(:vCursor,:vData)"
    Else
       vSql = "PRODUCAO.pck_cad550c.pr_comparar(:vCursor,:vData)"
    End If
    'andrevp - SDS2028 - 25/03/10 - Inclus�o do �ndice IDPc - Fim
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    If vErro <> "" Then
        GoTo TrataErro
        Exit Sub
    Else
        Set vObjOracle = vParameters("vCursor").Value
    End If
    Val_Dolar = IIf(IsNull(vObjOracle!VALOR_USS) = True, 0, vObjOracle!VALOR_USS)
    'A linha acima resolve o problema da vers�o anterior

'    If (Aux_Dolar < Val_Dolar) Or (Aux_Dolar < Aux_Dolar_Ant) Or (Aux_Dolar <= 0) Then
'
'    O Conceito passado de que o dolar_diario_aux n�o poderia ser menor que o
'    o dolar_di�rio da data correspondente, foi mudado por Sonia em 30/12/99,
'    motivo alegado - os valores podem diminuir

    If (vAux_Dolar < vAux_Dolar_Ant) Or (vAux_Dolar <= 0) Then
        Unload frmAguardar
        Call vVB_Generica_001.Informar("Erro - Este Arquivo Cont�m Valor de D�lar Incorreto na Linha " & vLinha & ":" & vbCrLf & vLinhaProcessada & vbCr & "Favor Verificar!")
        frmDolar.txtMes = ""
        frmDolar.txtAno = ""
        flErro = True
        frmDolar.txtMes.SetFocus
        Exit Sub
    End If

    vAux_Dolar_Ant = vAux_Dolar
    
    Exit Sub
    
TrataErro:

    flErro = True
    Unload frmAguardar
    Call vVB_Generica_001.Informar("Erro - Este Arquivo Cont�m Caracteres Incorretos na Linha " & vLinha & ":" & vbCrLf & vLinhaProcessada & vbCr & "Favor Verificar!")

End Sub

Sub Limpa_Dolar_Diario_Aux()

    Call vVB_Generica_001.ExcluiBind(vBanco)
    
    'andrevp - SDS2028 - 25/03/10 - Inclus�o do �ndice IDPc - In�cio
    If frmDolar.optIndice(0) Then
       vSql = "PRODUCAO.pck_cad550.pr_deletar"
    Else
       vSql = "PRODUCAO.pck_cad550c.pr_deletar"
    End If
    'andrevp - SDS2028 - 25/03/10 - Inclus�o do �ndice IDPc - Fim
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro)
    End If
    
End Sub

Sub Procura_Letra(vTipo As String, vLinhaProcessada As String)

    Dim vDigito As String
    
    For i = 1 To Len(vTipo)
        vDigito = UCase(Mid(vTipo, i, 1))
        If InStr(1, "ABCDEFGHIJKLMNOPQRSTUVW ;,+$%#@!*\|XZY", vDigito, 1) <> 0 Then
            Unload frmAguardar
            Call vVB_Generica_001.Informar("Erro - Este Arquivo Cont�m Caracteres Incorretos na Linha " & vLinha & ":" & vbCrLf & vLinhaProcessada & vbCr & "Favor Verificar!")
            flErro = True
            Exit Sub
        End If
    Next i

End Sub

