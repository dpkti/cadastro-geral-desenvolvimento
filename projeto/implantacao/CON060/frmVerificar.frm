VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Begin VB.Form frmVerificar 
   BackColor       =   &H00FFFFFF&
   Caption         =   "Verificar Arquivos"
   ClientHeight    =   2340
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   1725
   Icon            =   "frmVerificar.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   2340
   ScaleWidth      =   1725
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer Timer1 
      Interval        =   60000
      Left            =   -30
      Top             =   60
   End
   Begin MSComctlLib.ListView lsv 
      Height          =   2025
      Left            =   30
      TabIndex        =   0
      Top             =   30
      Width           =   1665
      _ExtentX        =   2937
      _ExtentY        =   3572
      View            =   3
      LabelWrap       =   0   'False
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   1
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Sistema"
         Object.Width           =   2540
      EndProperty
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Height          =   165
      Left            =   0
      TabIndex        =   1
      Top             =   2100
      Width           =   1695
   End
End
Attribute VB_Name = "frmVerificar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'*********
'ICON TRAY
'*********

    
Const NIM_ADD = &H0    ' Add an icon
Const NIM_MODIFY = &H1 ' Modify an icon
Const NIM_DELETE = &H2 ' Delete an icon

Const NIF_MESSAGE = &H1       ' To change uCallBackMessage member
Const NIF_ICON = &H2          ' To change the icon
Const NIF_TIP = &H4           ' To change the tooltip text

Const WM_MOUSEMOVE = &H200
Const WM_LBUTTONDOWN = &H201   ' Left click
Const WM_LBUTTONDBLCLK = &H203 ' Left double click
Const WM_RBUTTONDOWN = &H204   ' Right click
Const WM_RBUTTONDBLCLK = &H206 ' Right double click

'Dim nid As NOTIFYICONDATA
'*************
'FIM ICON TRAY
'*************

Const ORATYPE_CURSOR = 102
Const ORADYN_NO_BLANKSTRIP = &H2&
Const ORAPARM_INPUT = 1              'CONSTANTE DE BIND INPUT
Const ORAPARM_OUTPUT = 2             'CONSTANTE DE BIND OUTPUT
Const ORAPARM_BOTH = 3               'CONSTANTE DE BIND INPUT/OUTPUT
Const ORATYPE_NUMBER = 2

Dim vFso As FileSystemObject

Dim vTempo As Byte

Private Sub Form_Load()
          
    Dim Item As ListItem
          
1         If App.PrevInstance = True Then
2           MsgBox "Este programa j� est� aberto."
3           End
4         End If

15        Set Item = lsv.ListItems.Add
16        Item = "EMVIA"

18        Set Item = lsv.ListItems.Add
19        Item = "EDIALOCA"

21        Set Item = lsv.ListItems.Add
22        Item = "FIL200"

24        Set Item = lsv.ListItems.Add
25        Item = "FIL470"

27        Set Item = lsv.ListItems.Add
28        Item = "VDA730"

         Me.Label1.Caption = "Rodar em 5 minutos"
          
End Sub

Private Sub Timer1_Timer()
          
1         If Format(Now, "HH:MM") > Format("19:00", "HH:MM") Then Exit Sub
          
2         On Error GoTo Trata_Erro
          
          Dim vFolder As Folder
          Dim vFile As File
          Dim vStatus As Byte

3         vTempo = vTempo + 1

          Me.Label1.Caption = "Rodar em " & 5 - vTempo & " minutos."

4         If vTempo >= 5 Then

5             Set vFso = New FileSystemObject
6             Set vFolder = vFso.GetFolder("P:\IN\")
              
7             If Dir("H:\software\emvia\log\Log" & Format(Date, "YYMMDD") & ".TXT") <> "" Then
8                If DateDiff("n", FileDateTime("H:\software\emvia\log\Log" & Format(Date, "YYMMDD") & ".TXT"), Now) > 5 Then
9                   Enviar_Email 1277, "EMVIA", "EMVIA ou EDIALOCA Parado", "Um dos sistemas EMVIA ou EDIALOCA est�o desativados. " & vbCrLf & "Estes sistemas devem ficar ativos na maquina COMPAQ2." & vbCrLf & "Por favor verifique. "
10               End If
11            End If
              
12            If Dir("H:\software\emvia\logEDI\Log" & Format(Date, "YYMMDD") & ".TXT") <> "" Then
13               If DateDiff("n", FileDateTime("H:\software\emvia\logEDI\Log" & Format(Date, "YYMMDD") & ".TXT"), Now) > 5 Then
14                  Enviar_Email 1277, "EMVIA", "EMVIA ou EDIALOCA Parado", "Um dos sistemas EMVIA ou EDIALOCA est�o desativados. " & vbCrLf & "Estes sistemas devem ficar ativos na maquina COMPAQ2." & vbCrLf & "Por favor verifique. "
15               End If
16            End If
              
17            For Each vFile In vFolder.Files
18                If UCase(vFile.Type) = UCase("Documento de texto") Then
19                   If Right(vFile, 3) = "TXT" Then
20                      If DateDiff("n", vFile.DateLastModified, Now) > 5 And Hour(Now) < 19 And Hour(Now) > 7 Then
21                         Enviar_Email 491, "EMAIL", "Pedidos ou Consultas Parados no P:\IN", "Existem Pedidos ou Consultas paradas a mais de 5 minutos no diretorio P:\IN." & vbCrLf & "Por favor Verifique!!"
22                         Exit For
23                      End If
24                   End If
25                End If
26            Next
          
              'AutoZ
              
27            If GetKeyVal("C:\VDA730.INI", "CDS", "01") = 0 Then
28               Enviar_Email 1116, "EMAIL", "Pedidos AutoZ", "Processa Pedidos AutoZ de Campinas foi Reiniciado"
29               Shell "H:\ORACLE\SISTEMAS\VB\32BITS\VDA730_NOVO.EXE C", vbNormalFocus
30            End If
31            If GetKeyVal("C:\VDA730.INI", "CDS", "04") = 0 Then
32               Enviar_Email 1116, "EMAIL", "Pedidos AutoZ", "Processa Pedidos AutoZ de Goiania foi Reiniciado"
33               Shell "H:\ORACLE\SISTEMAS\VB\32BITS\VDA730_NOVO.EXE G", vbNormalFocus
34            End If
35            If GetKeyVal("C:\VDA730.INI", "CDS", "05") = 0 Then
36               Enviar_Email 1116, "EMAIL", "Pedidos AutoZ", "Processa Pedidos AutoZ do Fortaleza foi Reiniciado"
37               Shell "H:\ORACLE\SISTEMAS\VB\32BITS\VDA730_NOVO.EXE 5", vbNormalFocus
38            End If
39            If GetKeyVal("C:\VDA730.INI", "CDS", "07") = 0 Then
40               Enviar_Email 1116, "EMAIL", "Pedidos AutoZ", "Processa Pedidos AutoZ do BH foi Reiniciado"
41               Shell "H:\ORACLE\SISTEMAS\VB\32BITS\VDA730_NOVO.EXE 7", vbNormalFocus
42            End If
43            If GetKeyVal("C:\VDA730.INI", "CDS", "08") = 0 Then
44               Enviar_Email 1116, "EMAIL", "Pedidos AutoZ", "Processa Pedidos AutoZ do POA foi Reiniciado"
45               Shell "H:\ORACLE\SISTEMAS\VB\32BITS\VDA730_NOVO.EXE 8", vbNormalFocus
46            End If
47            If GetKeyVal("C:\VDA730.INI", "CDS", "10") = 0 Then
48               Enviar_Email 1116, "EMAIL", "Pedidos AutoZ", "Processa Pedidos AutoZ do RJ foi Reiniciado"
49               Shell "H:\ORACLE\SISTEMAS\VB\32BITS\VDA730_NOVO.EXE 10", vbNormalFocus
50            End If
51            If GetKeyVal("C:\VDA730.INI", "CDS", "12") = 0 Then
52               Enviar_Email 1116, "EMAIL", "Pedidos AutoZ", "Processa Pedidos AutoZ do Recife foi Reiniciado"
53               Shell "H:\ORACLE\SISTEMAS\VB\32BITS\VDA730_NOVO.EXE 12", vbNormalFocus
54            End If
55            If GetKeyVal("C:\VDA730.INI", "CDS", "14") = 0 Then
56               Enviar_Email 1116, "EMAIL", "Pedidos AutoZ", "Processa Pedidos AutoZ do Curitiba foi Reiniciado"
57               Shell "H:\ORACLE\SISTEMAS\VB\32BITS\VDA730_NOVO.EXE 14", vbNormalFocus
58            End If
59            If GetKeyVal("C:\VDA730.INI", "CDS", "16") = 0 Then
60               Enviar_Email 1116, "EMAIL", "Pedidos AutoZ", "Processa Pedidos AutoZ do Casa Verde foi Reiniciado"
61               Shell "H:\ORACLE\SISTEMAS\VB\32BITS\VDA730_NOVO.EXE 16", vbNormalFocus
62            End If
          
63            vTempo = 0
              Me.Label1.Caption = "Rodar em 5 minutos"
      
64        End If
          
Trata_Erro:
65        If Err.Number <> 0 Then
66      Enviar_Email 1264, "CON060", "Erro no CON060", "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
67        End If

End Sub

Public Sub Criar_Cursor(pBanco As Object)
    pBanco.Parameters.Remove "vCursor"
    pBanco.Parameters.Add "vCursor", 0, 2
    pBanco.Parameters("vCursor").ServerType = ORATYPE_CURSOR
    pBanco.Parameters("vCursor").DynasetOption = ORADYN_NO_BLANKSTRIP
    pBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
End Sub



Sub Enviar_Email(pCodSoftware As Integer, pParametro As String, pTitulo As String, pMensagem As String)
        
    Dim vrstEmail As Object
    Dim vNomeusuario As String
    
    Dim vSql As String
    Dim vSessaoIDPK As Object
    Dim vBancoIDPK As Object
    Dim i As Integer
          
1     Set vSessaoIDPK = CreateObject("oracleinprocserver.xorasession")
2     Set vBancoIDPK = vSessaoIDPK.OpenDatabase("IDPK", "INTRANET/PROD", 0&)
    
3     Set vrstEmail = vBancoIDPK.CreateDynaset("Select vl_parametro from Helpdesk.parametros@lnk_producao where cod_software=" & pCodSoftware & " and nome_parametro='" & pParametro & "'", 0&)
        
4     For i = 1 To vrstEmail.RecordCount
5       vBancoIDPK.Parameters.Remove "Para"
6       vBancoIDPK.Parameters.Add "Para", vrstEmail!vl_parametro, 1
7       vBancoIDPK.Parameters.Remove "Titulo"
8       vBancoIDPK.Parameters.Add "Titulo", pTitulo, 1
9       vBancoIDPK.Parameters.Remove "Conteudo"
10      vBancoIDPK.Parameters.Add "Conteudo", pMensagem, 1
11      vBancoIDPK.ExecuteSQL "BEGIN INTRANET.ENVIA_EMAIL (:para, :titulo, :conteudo); END;"
12      vrstEmail.MoveNext
13    Next
    
14    vrstEmail.Close
15    Set vrstEmail = Nothing

16        Set vSessaoIDPK = Nothing
17        Set vBancoIDPK = Nothing
    
End Sub

