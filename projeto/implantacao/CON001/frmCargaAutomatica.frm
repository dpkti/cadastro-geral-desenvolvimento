VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Object = "{E57FB72C-1106-44AF-9706-0BA099A707C7}#4.2#0"; "XPFrame.ocx"
Object = "{69DB0C7E-0FDF-467F-8B05-D41D843CC417}#1.0#0"; "SMOOTHBAR.OCX"
Object = "{E910F8E1-8996-4EE9-90F1-3E7C64FA9829}#1.1#0"; "vbaListView6.ocx"
Begin VB.Form frmCargaAutomatica 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "::: CARGA AUTOM�TICA :::"
   ClientHeight    =   8745
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   15180
   ControlBox      =   0   'False
   Icon            =   "frmCargaAutomatica.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   583
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   1012
   Begin VB.FileListBox lstArquivos 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   2205
      TabIndex        =   12
      Top             =   45
      Visible         =   0   'False
      Width           =   555
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   810
      Width           =   15090
      _ExtentX        =   26617
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   14445
      TabIndex        =   0
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCargaAutomatica.frx":23D2
      PICN            =   "frmCargaAutomatica.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdExecutar 
      Height          =   690
      Left            =   45
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Executar a carga autom�tica"
      Top             =   45
      Width           =   1410
      _ExtentX        =   2487
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   "Enviar"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCargaAutomatica.frx":30C8
      PICN            =   "frmCargaAutomatica.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdAtualizar 
      Height          =   690
      Left            =   1485
      TabIndex        =   3
      TabStop         =   0   'False
      ToolTipText     =   "Atualizar informa��es"
      Top             =   45
      Visible         =   0   'False
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCargaAutomatica.frx":3DBE
      PICN            =   "frmCargaAutomatica.frx":3DDA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin CoolXPFrame.xpFrame fraCargas 
      Height          =   6135
      Left            =   45
      Top             =   810
      Visible         =   0   'False
      Width           =   15090
      _ExtentX        =   26617
      _ExtentY        =   10821
      Caption         =   "Cargas agendadas para hoje"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   13977088
      BorderColor     =   8421504
      ColorStyle      =   99
      UseCoolToolTip  =   0   'False
      Begin VB.TextBox txtTamanhoArquivo 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   13230
         Locked          =   -1  'True
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   5715
         Width           =   1500
      End
      Begin vbalListViewLib6.vbalListViewCtl lstCargasAgendadas 
         Height          =   5370
         Left            =   90
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   270
         Width           =   14910
         _ExtentX        =   26300
         _ExtentY        =   9472
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         View            =   1
         LabelEdit       =   0   'False
         FullRowSelect   =   -1  'True
         AutoArrange     =   0   'False
         Appearance      =   0
         HeaderButtons   =   0   'False
         HeaderTrackSelect=   0   'False
         HideSelection   =   0   'False
         InfoTips        =   0   'False
      End
      Begin VB.Label Label5 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "KB"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   14805
         TabIndex        =   7
         Top             =   5760
         Width           =   210
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Tamanho dos arquivos das cargas agendadas"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   9495
         TabIndex        =   6
         Top             =   5760
         Width           =   3660
      End
   End
   Begin CoolXPFrame.xpFrame fraInformacoes 
      Height          =   1770
      Left            =   45
      Top             =   6930
      Visible         =   0   'False
      Width           =   15090
      _ExtentX        =   26617
      _ExtentY        =   3122
      Caption         =   "Log"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   13977088
      BorderColor     =   8421504
      ColorStyle      =   99
      UseCoolToolTip  =   0   'False
      Begin VB.ListBox lstLog 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1395
         ItemData        =   "frmCargaAutomatica.frx":4AB4
         Left            =   90
         List            =   "frmCargaAutomatica.frx":4AB6
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   270
         Width           =   14055
      End
      Begin Bot�o.cmd cmdLimpar 
         Height          =   780
         Left            =   14220
         TabIndex        =   8
         TabStop         =   0   'False
         ToolTipText     =   "Limpar log"
         Top             =   900
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   1376
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCargaAutomatica.frx":4AB8
         PICN            =   "frmCargaAutomatica.frx":4AD4
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
   End
   Begin CoolXPFrame.xpFrame fraEnvio 
      Height          =   735
      Left            =   4725
      Top             =   0
      Visible         =   0   'False
      Width           =   5460
      _ExtentX        =   9631
      _ExtentY        =   1296
      Caption         =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   13977088
      BorderColor     =   8421504
      ColorStyle      =   99
      UseCoolToolTip  =   0   'False
      Begin GradProgBar.SmoothBaar smtCargaTotal 
         Height          =   420
         Left            =   1665
         TabIndex        =   10
         Top             =   225
         Width           =   3705
         _ExtentX        =   6535
         _ExtentY        =   741
         Raised          =   0
         ProgressStartColor=   16711680
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Appearance      =   3
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Envio dos arquivos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   90
         TabIndex        =   11
         Top             =   315
         Width           =   1500
      End
   End
   Begin VB.Label lblNenhumaCarga 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "Nenhuma carga pendente para hoje"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   435
      Left            =   135
      TabIndex        =   13
      Top             =   900
      Visible         =   0   'False
      Width           =   6405
   End
End
Attribute VB_Name = "frmCargaAutomatica"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdSair_Click()

    Unload Me

End Sub

Public Sub cmdAtualizar_Click()

    fraEnvio.Visible = False
    lblNenhumaCarga.Visible = False
    fraInformacoes.Visible = False
    Call CarregarAgendamentos_HOJE

End Sub
Public Sub cmdExecutar_Click()

    On Error GoTo Erro

    If fraCargas.Visible = True Then

        Dim vArquivoBackup As String
        Dim vTotalArquivos As Double
        Dim vPercentualTotal As Integer
        Dim vPercentualArquivo As Integer
        Dim vMudouLoja As Boolean
        Dim vDepositoAnterior As Integer
        Dim vStatus As Integer
        Dim vSequencia As Integer
        Dim vCodAnalista As Integer
        Dim vEmail As String
        Dim vTeste
        Dim vNomeArquivoLog As String

        vTeste = Dir("C:\LOG_CON001", vbDirectory)

        If vTeste = "" Then

            MkDir "C:\LOG_CON001"

        End If

        vMudouLoja = True

        fraEnvio.Visible = True
        smtCargaTotal.Value 0
        Me.Refresh

        vTotalArquivos = lstCargasAgendadas.ListItems.Count
        vDiretorioOrigem = ""
        vArquivoOrigem = ""
        vDiretorioDestino = ""
        vArquivoDestino = ""

        Call GeraLog(Me, "In�cio do envio dos arquivos", False)

        vNomeArquivoLog = "C:\LOG_CON001\LOG_CON001" & Format(Now, "DDMMYYYYHHMM") & ".TXT"
        Open vNomeArquivoLog For Output As #2
        Print #2, CStr(Format(Now, "dd/mm/yyyy hh:mm:ss")) & " -> " & "IN�CIO DO ENVIO DOS ARQUIVOS"

        For i = 1 To lstCargasAgendadas.ListItems.Count

            Print #2, CStr(Format(Now, "dd/mm/yyyy hh:mm:ss")) & " -> " & UCase(CStr(lstCargasAgendadas.ListItems(i).SubItems(1).Caption)) & "     " & UCase(lstCargasAgendadas.ListItems(i).SubItems(2).Caption) & "     " & UCase(CStr(lstCargasAgendadas.ListItems(i).SubItems(6).Caption))

        Next

        Close #2

        For i = 1 To lstCargasAgendadas.ListItems.Count

            vSequencia = Val(lstCargasAgendadas.ListItems(i))

            vCodAnalista = Val(lstCargasAgendadas.ListItems(i).SubItems(6).Caption)

            Call vVB_Generica_001.ExcluiBind(vBanco)

            vBanco.Parameters.Add "pAcao", 3, 1
            vBanco.Parameters.Add "pSequencia", vSequencia, 1
            vBanco.Parameters.Add "pStatus", 0, 1
            vBanco.Parameters.Add "pArquivo", "", 1
            vBanco.Parameters.Add "pCodAnalista", vCodAnalista, 1

            vBanco.Parameters.Add "pRetorno1", 0, 2
            vBanco.Parameters("pRetorno1").ServerType = 102
            vBanco.Parameters("pRetorno1").DynasetOption = &H2&
            vBanco.Parameters("pRetorno1").DynasetCacheParams 256, 16, 20, 2000, 0

            vBanco.Parameters.Add "pCodErro", 0, 2
            vBanco.Parameters.Add "pMsgErro", "", 2

            vSql = _
                    "PRODUCAO.PCK_CON001.PRCARGAAUTOMATICA(" & _
                    ":pAcao, " & _
                    ":pSequencia, " & _
                    ":pStatus, " & _
                    ":pArquivo, " & _
                    ":pCodAnalista, " & _
                    ":pRetorno1, " & _
                    ":pCodErro, " & _
                    ":pMsgErro)"

            vBanco.ExecuteSQL "Begin " & vSql & "; End;"

            If vBanco.Parameters("pCodErro") <> 0 Then

                vErro = "Ocorreu um erro" & Chr(13) & _
                        "C�d. do erro: " & CStr(vBanco.Parameters("pCodErro")) & Chr(13) & _
                        "Msg. de erro: " & CStr(vBanco.Parameters("pMsgErro"))

                Call vVB_Generica_001.ProcessaErro(vErro)
                Exit Sub

            End If

            Set vObjOracle = vBanco.Parameters("pRetorno1").Value

            vEmail = vObjOracle.Fields(0)

            If Val(lstCargasAgendadas.ListItems(i).SubItems(1).Caption) <> vDepositoAnterior Then

                vDepositoAnterior = Val(lstCargasAgendadas.ListItems(i).SubItems(1).Caption)
                vMudouLoja = True

            Else

                vMudouLoja = False

            End If

            If vMudouLoja = False Then

                GoTo Enviar

            End If

            Call Desconectar
            Call GeraLog(Me, "Conectando no dep�sito " & lstCargasAgendadas.ListItems(i).SubItems(1).Caption, False)
            If fConectar(Val(lstCargasAgendadas.ListItems(i).SubItems(1).Caption), False) = True Then

Enviar:
                vPercentualTotal = (i * 100) / vTotalArquivos

                vDiretorioOrigem = UCase(lstCargasAgendadas.ListItems(i).SubItems(4).Caption)
                vDiretorioDestino = UCase(lstCargasAgendadas.ListItems(i).SubItems(5).Caption)
                vArquivoOrigem = UCase(lstCargasAgendadas.ListItems(i).SubItems(2).Caption)
                vArquivoDestino = vArquivoOrigem
                
                If fVerificarSeArquivoExiste(vDiretorioOrigem & vArquivoOrigem) = False Then

                    vStatus = 1
                    Call vVB_Generica_001.EnviarEmail(vBanco, vEmail, "Carga automatica - ID: " & CStr(vSequencia), _
                            "Arquivo origem " & vDiretorioOrigem & vArquivoOrigem & " nao encontrado")
                    Call GeraLog(Me, "Arquivo origem " & vDiretorioOrigem & vArquivoOrigem & " n�o encontrado", False)

                Else

                    If fVerificarSeArquivoEstaAberto(vDiretorioOrigem & vArquivoOrigem) = True Then

                        vStatus = 2
                        Call vVB_Generica_001.EnviarEmail(vBanco, vEmail, "Carga automatica - ID: " & CStr(vSequencia), _
                                "Arquivo origem " & vDiretorioOrigem & vArquivoOrigem & " esta em uso")
                        Call GeraLog(Me, "Arquivo origem " & vDiretorioOrigem & vArquivoOrigem & " est� em uso", False)

                    Else

                        vTeste = Dir(vDiretorioDestino, vbDirectory)

                        If vTeste <> "" Then

                            If fVerificarSeArquivoExiste(vDiretorioDestino & vArquivoDestino) = True Then

                                Dim vDiretorioFechar As String
                                Dim vIP As String
                                Dim vCompartilhamento As String
                                Dim vUsuario As String
                                Dim vSenha As String

                                Call vVB_Generica_001.ExcluiBind(vBanco)

                                vBanco.Parameters.Add "pAcao", 1, 1
                                vBanco.Parameters.Add "pCodLoja", Val(lstCargasAgendadas.ListItems(i).SubItems(1).Caption), 1
                                vBanco.Parameters.Add "pIP", "", 1
                                vBanco.Parameters.Add "pCompartilhamento", "", 1
                                vBanco.Parameters.Add "pUsuario", "", 1
                                vBanco.Parameters.Add "pSenha", "", 1

                                vBanco.Parameters.Add "pRetorno1", 0, 2
                                vBanco.Parameters("pRetorno1").ServerType = 102
                                vBanco.Parameters("pRetorno1").DynasetOption = &H2&
                                vBanco.Parameters("pRetorno1").DynasetCacheParams 256, 16, 20, 2000, 0

                                vBanco.Parameters.Add "pCodErro", 0, 2
                                vBanco.Parameters.Add "pMsgErro", "", 2

                                vSql = _
                                        "PRODUCAO.PCK_CON001.PRCADASTRO(" & _
                                        ":pAcao, " & _
                                        ":pCodLoja, " & _
                                        ":pIP, " & _
                                        ":pCompartilhamento, " & _
                                        ":pUsuario, " & _
                                        ":pSenha, " & _
                                        ":pRetorno1, " & _
                                        ":pCodErro, " & _
                                        ":pMsgErro)"

                                vBanco.ExecuteSQL "Begin " & vSql & "; End;"

                                If vBanco.Parameters("pCodErro") <> 0 Then

                                    vErro = "Ocorreu um erro" & Chr(13) & _
                                            "C�d. do erro: " & CStr(vBanco.Parameters("pCodErro")) & Chr(13) & _
                                            "Msg. de erro: " & CStr(vBanco.Parameters("pMsgErro"))

                                    Call vVB_Generica_001.ProcessaErro(vErro)
                                    Exit Sub

                                End If

                                Set vObjOracle = vBanco.Parameters("pRetorno1").Value

                                If Not vObjOracle.EOF Then

                                    vIP = CStr(vObjOracle.Fields(0))
                                    vCompartilhamento = "D:" & CStr(vObjOracle.Fields(1))
                                    vUsuario = CStr(vObjOracle.Fields(2))
                                    vSenha = CStr(vObjOracle.Fields(3))

                                End If

                                vDiretorioFechar = Replace(vDiretorioDestino, "Z:\", Replace(vCompartilhamento, "$", "") & "\")

                                Call FecharArquivo(vIP, vUsuario, vSenha, vDiretorioFechar, vArquivoDestino)
                                
                                vStatus = 5
                                Call GeraLog(Me, "Enviando arquivo " & vDiretorioOrigem & vArquivoOrigem & " para " & vDiretorioDestino & vArquivoDestino & " - Tam.: " & CStr(fTamanhoArquivo(vDiretorioOrigem & vArquivoOrigem)) & " KB", False)

                                Call CopiarArquivo(vDiretorioOrigem & vArquivoOrigem, vDiretorioDestino & vArquivoDestino, True)

                                smtCargaTotal.Value CLng(vPercentualTotal)

                                Call vVB_Generica_001.EnviarEmail(vBanco, vEmail, "Carga automatica - ID: " & CStr(vSequencia), _
                                        "Arquivo " & vDiretorioOrigem & vArquivoOrigem & " enviado com sucesso para o deposito " & CStr(lstCargasAgendadas.ListItems(i).SubItems(1).Caption))
                                Call GeraLog(Me, "Arquivo enviado com sucesso", False)

                            Else

                                vStatus = 5
                                Call GeraLog(Me, "Enviando arquivo " & vDiretorioOrigem & vArquivoOrigem & " para " & vDiretorioDestino & vArquivoDestino & " - Tam.: " & CStr(fTamanhoArquivo(vDiretorioOrigem & vArquivoOrigem)) & " KB", False)

                                Call CopiarArquivo(vDiretorioOrigem & vArquivoOrigem, vDiretorioDestino & vArquivoDestino, True)

                                smtCargaTotal.Value CLng(vPercentualTotal)

                                Call vVB_Generica_001.EnviarEmail(vBanco, vEmail, "Carga automatica - ID: " & CStr(vSequencia), _
                                        "Arquivo " & vDiretorioOrigem & vArquivoOrigem & " enviado com sucesso para o deposito " & CStr(lstCargasAgendadas.ListItems(i).SubItems(1).Caption))
                                Call GeraLog(Me, "Arquivo enviado com sucesso", False)

                            End If

                        Else

                            vStatus = 7
                            Call vVB_Generica_001.EnviarEmail(vBanco, vEmail, "Carga automatica - ID: " & CStr(vSequencia), _
                                    "Diret�rio destino " & vDiretorioDestino & " nao encontrado")
                            Call GeraLog(Me, "Diret�rio destino " & vDiretorioDestino & " n�o encontrado", False)

                        End If

                    End If

                End If

            Else

                vStatus = 4
                Call vVB_Generica_001.EnviarEmail(vBanco, vEmail, "Carga automatica - ID: " & CStr(vSequencia), _
                        "Nao foi possivel conectar no deposito " & CStr(lstCargasAgendadas.ListItems(i).SubItems(1).Caption))
                Call GeraLog(Me, "N�o foi poss�vel conectar no dep�sito " & CStr(lstCargasAgendadas.ListItems(i).SubItems(1).Caption), False)

            End If

            Call vVB_Generica_001.ExcluiBind(vBanco)

            vBanco.Parameters.Add "pAcao", 1, 1
            vBanco.Parameters.Add "pSequencia", vSequencia, 1
            vBanco.Parameters.Add "pStatus", vStatus, 1
            vBanco.Parameters.Add "pArquivo", "", 1
            vBanco.Parameters.Add "pCodAnalista", 0, 1

            vBanco.Parameters.Add "pRetorno1", 0, 2
            vBanco.Parameters("pRetorno1").ServerType = 102
            vBanco.Parameters("pRetorno1").DynasetOption = &H2&
            vBanco.Parameters("pRetorno1").DynasetCacheParams 256, 16, 20, 2000, 0

            vBanco.Parameters.Add "pCodErro", 0, 2
            vBanco.Parameters.Add "pMsgErro", "", 2

            vSql = _
                    "PRODUCAO.PCK_CON001.PRCARGAAUTOMATICA(" & _
                    ":pAcao, " & _
                    ":pSequencia, " & _
                    ":pStatus, " & _
                    ":pArquivo, " & _
                    ":pCodAnalista, " & _
                    ":pRetorno1, " & _
                    ":pCodErro, " & _
                    ":pMsgErro)"

            vBanco.ExecuteSQL "Begin " & vSql & "; End;"

            If vBanco.Parameters("pCodErro") <> 0 Then

                vErro = "Ocorreu um erro" & Chr(13) & _
                        "C�d. do erro: " & CStr(vBanco.Parameters("pCodErro")) & Chr(13) & _
                        "Msg. de erro: " & CStr(vBanco.Parameters("pMsgErro"))

                Call vVB_Generica_001.ProcessaErro(vErro)
                Exit Sub

            End If

        Next

        If vStatus <> 2 And _
                vStatus <> 3 Then

            Call ExcluirArquivos

        End If
        
        Call Desconectar
        Call GeraLog(Me, "Fim do envio dos arquivos", False)

        Open vNomeArquivoLog For Append As #2
        Print #2, CStr(Format(Now, "dd/mm/yyyy hh:mm:ss")) & " -> " & "FIM DO ENVIO DOS ARQUIVOS"
        Close #2

        Call vVB_Generica_001.Informar("Opera��o realizada com sucesso.")
        Unload Me

    End If

    Exit Sub

Erro:

    vStatus = 6

    Call vVB_Generica_001.ExcluiBind(vBanco)

    vBanco.Parameters.Add "pAcao", 1, 1
    vBanco.Parameters.Add "pSequencia", vSequencia, 1
    vBanco.Parameters.Add "pStatus", vStatus, 1
    vBanco.Parameters.Add "pArquivo", "", 1
    vBanco.Parameters.Add "pCodAnalista", 0, 1

    vBanco.Parameters.Add "pRetorno1", 0, 2
    vBanco.Parameters("pRetorno1").ServerType = 102
    vBanco.Parameters("pRetorno1").DynasetOption = &H2&
    vBanco.Parameters("pRetorno1").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Add "pCodErro", 0, 2
    vBanco.Parameters.Add "pMsgErro", "", 2

    vSql = _
            "PRODUCAO.PCK_CON001.PRCARGAAUTOMATICA(" & _
            ":pAcao, " & _
            ":pSequencia, " & _
            ":pStatus, " & _
            ":pArquivo, " & _
            ":pCodAnalista, " & _
            ":pRetorno1, " & _
            ":pCodErro, " & _
            ":pMsgErro)"

    vBanco.ExecuteSQL "Begin " & vSql & "; End;"

    If vBanco.Parameters("pCodErro") <> 0 Then

        vErro = "Ocorreu um erro" & Chr(13) & _
                "C�d. do erro: " & CStr(vBanco.Parameters("pCodErro")) & Chr(13) & _
                "Msg. de erro: " & CStr(vBanco.Parameters("pMsgErro"))

        Call vVB_Generica_001.ProcessaErro(vErro)
        Exit Sub

    End If

    Call vVB_Generica_001.EnviarEmail(vBanco, vEmail, "Carga automatica - ID: " & CStr(vSequencia), _
            "Ocorreu um erro: " & CStr(Err.Number) & " - " & CStr(Err.Description))
    Call GeraLog(Me, "Ocorreu um erro. C�d. do erro: " & CStr(Err.Number) & "   Msg. de erro: " & CStr(Err.Description), False)

    vErro = "Ocorreu um erro" & Chr(13) & _
            "C�d. do erro: " & CStr(Err.Number) & Chr(13) & _
            "Msg. de erro: " & CStr(Err.Description)

    Call vVB_Generica_001.ProcessaErro(vErro)

End Sub
Private Sub cmdLimpar_Click()

    lstLog.Clear

End Sub
Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()

    On Error GoTo Erro

    Me.Top = 0
    Me.Left = 0

    lstArquivos.Path = "H:\ORACLE\SISTEMAS\CARGAS\"
    lstArquivos.Refresh

    lstCargasAgendadas.Columns.Add , , "ID", , 800
    lstCargasAgendadas.Columns.Add , , "DEP�SITO", , 2000
    lstCargasAgendadas.Columns.Add , , "ARQUIVO", , 2000
    lstCargasAgendadas.Columns.Add , , "DT. AGENDAMENTO", , 2000
    lstCargasAgendadas.Columns.Add , , "ORIGEM", , 5200
    lstCargasAgendadas.Columns.Add , , "DESTINO", , 5200
    lstCargasAgendadas.Columns.Add , , "ANALISTA", , 3000
    lstCargasAgendadas.Columns(1).Alignment = eLVColumnAlignCenter

    Call CarregarAgendamentos_HOJE

    Exit Sub

Erro:

    vErro = "Ocorreu um erro" & Chr(13) & _
            "C�d. do erro: " & Err.Number & Chr(13) & _
            "Msg. de erro: " & Err.Description

    Call vVB_Generica_001.ProcessaErro(vErro)

End Sub
