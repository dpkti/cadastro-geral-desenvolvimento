VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "ComDlg32.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Object = "{E57FB72C-1106-44AF-9706-0BA099A707C7}#4.2#0"; "XPFrame.ocx"
Object = "{BD5DC045-AFFA-40B8-A887-95734A65FE55}#1.3#0"; "XPCheck.ocx"
Object = "{69DB0C7E-0FDF-467F-8B05-D41D843CC417}#1.0#0"; "SMOOTHBAR.OCX"
Object = "{E910F8E1-8996-4EE9-90F1-3E7C64FA9829}#1.1#0"; "vbaListView6.ocx"
Begin VB.Form frmCargaManual 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "::: CARGA MANUAL :::"
   ClientHeight    =   8745
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   15180
   ControlBox      =   0   'False
   Icon            =   "frmCargaManual.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   583
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   1012
   Begin MSComDlg.CommonDialog dlgDiretorios 
      Left            =   3510
      Top             =   45
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   12
      TabStop         =   0   'False
      Top             =   810
      Width           =   15090
      _ExtentX        =   26617
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   14445
      TabIndex        =   3
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCargaManual.frx":23D2
      PICN            =   "frmCargaManual.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin CoolXPFrame.xpFrame fraOrigem 
      Height          =   4740
      Left            =   45
      Top             =   810
      Width           =   8250
      _ExtentX        =   14552
      _ExtentY        =   8361
      Caption         =   "Origem"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   13977088
      BorderColor     =   8421504
      ColorStyle      =   99
      UseCoolToolTip  =   0   'False
      Begin Bot�o.cmd cmdAtualizar 
         Height          =   375
         Left            =   1305
         TabIndex        =   7
         TabStop         =   0   'False
         ToolTipText     =   "Atualizar arquivos de origem"
         Top             =   270
         Width           =   375
         _ExtentX        =   661
         _ExtentY        =   661
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCargaManual.frx":30C8
         PICN            =   "frmCargaManual.frx":30E4
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdExcluir 
         Height          =   375
         Left            =   900
         TabIndex        =   6
         TabStop         =   0   'False
         ToolTipText     =   "Excluir o arquivo selecionado para outro diret�rio"
         Top             =   270
         Width           =   375
         _ExtentX        =   661
         _ExtentY        =   661
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCargaManual.frx":347E
         PICN            =   "frmCargaManual.frx":349A
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdCopiar 
         Height          =   375
         Left            =   495
         TabIndex        =   5
         TabStop         =   0   'False
         ToolTipText     =   "Copiar o arquivo selecionado para outro diret�rio"
         Top             =   270
         Width           =   375
         _ExtentX        =   661
         _ExtentY        =   661
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCargaManual.frx":3834
         PICN            =   "frmCargaManual.frx":3850
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin CoolXPCheck.xpCheck chkBackup 
         Height          =   285
         Left            =   5490
         TabIndex        =   8
         TabStop         =   0   'False
         ToolTipText     =   "Cria um backup do arquivo que est� sendo utilizado, durante o processo de envio"
         Top             =   405
         Width           =   2715
         _ExtentX        =   4789
         _ExtentY        =   503
         Caption         =   " &Criar backup do arquivo atual"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Value           =   1
      End
      Begin Bot�o.cmd cmdDiretorioOrigemPadrao 
         Height          =   375
         Left            =   90
         TabIndex        =   4
         TabStop         =   0   'False
         ToolTipText     =   "Ir para diret�rio padr�o de cargas"
         Top             =   270
         Width           =   375
         _ExtentX        =   661
         _ExtentY        =   661
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCargaManual.frx":3BEA
         PICN            =   "frmCargaManual.frx":3C06
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.FileListBox lstArquivosOrigem 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3930
         Left            =   3735
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   720
         Width           =   4425
      End
      Begin VB.DriveListBox lstDrivesOrigem 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   90
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   4320
         Width           =   3570
      End
      Begin VB.DirListBox lstDiretoriosOrigem 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3465
         Left            =   90
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   720
         Width           =   3570
      End
   End
   Begin CoolXPFrame.xpFrame fraDeposito 
      Height          =   690
      Left            =   45
      Top             =   0
      Width           =   3390
      _ExtentX        =   5980
      _ExtentY        =   1217
      Caption         =   "Dep�sito"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   13977088
      BorderColor     =   8421504
      ColorStyle      =   99
      UseCoolToolTip  =   0   'False
      Begin VB.ComboBox cmbLoja 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   90
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   270
         Width           =   3210
      End
   End
   Begin CoolXPFrame.xpFrame fraArquivosEnviar 
      Height          =   1995
      Left            =   45
      Top             =   5535
      Width           =   10230
      _ExtentX        =   18045
      _ExtentY        =   3519
      Caption         =   "Arquivos a serem enviados"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   13977088
      BorderColor     =   8421504
      ColorStyle      =   99
      UseCoolToolTip  =   0   'False
      Begin vbalListViewLib6.vbalListViewCtl lstArquivosEnviar 
         Height          =   1635
         Left            =   90
         TabIndex        =   30
         TabStop         =   0   'False
         ToolTipText     =   "Duplo clique para retirar o arquivo da carga"
         Top             =   270
         Width           =   10050
         _ExtentX        =   17727
         _ExtentY        =   2884
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         View            =   1
         LabelEdit       =   0   'False
         FullRowSelect   =   -1  'True
         AutoArrange     =   0   'False
         Appearance      =   0
         HeaderButtons   =   0   'False
         HeaderTrackSelect=   0   'False
         HideSelection   =   0   'False
         InfoTips        =   0   'False
      End
   End
   Begin Bot�o.cmd cmdEnviar 
      Height          =   1365
      Left            =   8370
      TabIndex        =   14
      TabStop         =   0   'False
      ToolTipText     =   "Enviar arquivos selecionados"
      Top             =   2340
      Width           =   1905
      _ExtentX        =   3360
      _ExtentY        =   2408
      BTYPE           =   3
      TX              =   "Enviar arquivos"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   14737632
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCargaManual.frx":3FA0
      PICN            =   "frmCargaManual.frx":3FBC
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdVerificar 
      Height          =   1320
      Left            =   8370
      TabIndex        =   13
      TabStop         =   0   'False
      ToolTipText     =   "Verificar se os arquivos est�o dispon�veis para realizar a carga"
      Top             =   945
      Width           =   1905
      _ExtentX        =   3360
      _ExtentY        =   2328
      BTYPE           =   3
      TX              =   "Verificar arquivos"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   14737632
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   16777215
      MPTR            =   1
      MICON           =   "frmCargaManual.frx":4F00
      PICN            =   "frmCargaManual.frx":4F1C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin CoolXPFrame.xpFrame fraDestino 
      Height          =   6720
      Left            =   10350
      Top             =   810
      Visible         =   0   'False
      Width           =   4785
      _ExtentX        =   8440
      _ExtentY        =   11853
      Caption         =   "Destino"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   13977088
      BorderColor     =   8421504
      ColorStyle      =   99
      UseCoolToolTip  =   0   'False
      Begin Bot�o.cmd cmd2 
         Height          =   375
         Left            =   3105
         TabIndex        =   20
         TabStop         =   0   'False
         ToolTipText     =   "Copiar o arquivo selecionado para outro diret�rio"
         Top             =   225
         Width           =   375
         _ExtentX        =   661
         _ExtentY        =   661
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCargaManual.frx":566F
         PICN            =   "frmCargaManual.frx":568B
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdInformacoesDestino 
         Height          =   375
         Left            =   2700
         TabIndex        =   19
         TabStop         =   0   'False
         ToolTipText     =   "Informa��es sobre o arquivo selecionado"
         Top             =   225
         Width           =   375
         _ExtentX        =   661
         _ExtentY        =   661
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCargaManual.frx":5A25
         PICN            =   "frmCargaManual.frx":5A41
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmd1 
         Height          =   375
         Left            =   3510
         TabIndex        =   21
         TabStop         =   0   'False
         ToolTipText     =   "Excluir o arquivo selecionado para outro diret�rio"
         Top             =   225
         Width           =   375
         _ExtentX        =   661
         _ExtentY        =   661
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCargaManual.frx":5DDB
         PICN            =   "frmCargaManual.frx":5DF7
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdDiretorioDestinoPadrao 
         Height          =   375
         Left            =   4320
         TabIndex        =   23
         TabStop         =   0   'False
         ToolTipText     =   "Ir para diret�rio padr�o de sistemas"
         Top             =   225
         Width           =   375
         _ExtentX        =   661
         _ExtentY        =   661
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCargaManual.frx":6191
         PICN            =   "frmCargaManual.frx":61AD
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdNovaPastaDestino 
         Height          =   375
         Left            =   3915
         TabIndex        =   22
         TabStop         =   0   'False
         ToolTipText     =   "Criar nova pasta"
         Top             =   225
         Width           =   375
         _ExtentX        =   661
         _ExtentY        =   661
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCargaManual.frx":6547
         PICN            =   "frmCargaManual.frx":6563
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.DirListBox lstDiretoriosDestino 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3015
         Left            =   90
         TabIndex        =   28
         TabStop         =   0   'False
         Top             =   675
         Width           =   4605
      End
      Begin VB.FileListBox lstArquivosDestino 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2760
         Left            =   90
         TabIndex        =   29
         TabStop         =   0   'False
         ToolTipText     =   "Duplo clique para visualizar quem est� usando o  arquivo, caso o mesmo esteja aberto"
         Top             =   3870
         Width           =   4605
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "IP do servidor:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   90
         TabIndex        =   17
         Top             =   270
         Width           =   1065
      End
      Begin VB.Label lblIPDestino 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "#"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   1215
         TabIndex        =   18
         Top             =   270
         Width           =   135
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Compartilhamento:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   90
         TabIndex        =   26
         Top             =   495
         Visible         =   0   'False
         Width           =   1365
      End
      Begin VB.Label lblCompartilhamentoDestino 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "#"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   1530
         TabIndex        =   27
         Top             =   495
         Visible         =   0   'False
         Width           =   135
      End
      Begin VB.Label Label4 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Senha:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   2655
         TabIndex        =   24
         Top             =   405
         Visible         =   0   'False
         Width           =   510
      End
      Begin VB.Label lblSenhaDestino 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "#"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   3240
         TabIndex        =   25
         Top             =   405
         Visible         =   0   'False
         Width           =   135
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Usu�rio:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   2655
         TabIndex        =   15
         Top             =   180
         Visible         =   0   'False
         Width           =   600
      End
      Begin VB.Label lblUsuarioDestino 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "#"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   3330
         TabIndex        =   16
         Top             =   180
         Visible         =   0   'False
         Width           =   135
      End
   End
   Begin CoolXPFrame.xpFrame fraInformacoes 
      Height          =   1185
      Left            =   45
      Top             =   7515
      Width           =   15045
      _ExtentX        =   26538
      _ExtentY        =   2090
      Caption         =   "Log"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   13977088
      BorderColor     =   8421504
      ColorStyle      =   99
      UseCoolToolTip  =   0   'False
      Begin Bot�o.cmd cmdLimpar 
         Height          =   780
         Left            =   14175
         TabIndex        =   32
         TabStop         =   0   'False
         ToolTipText     =   "Limpar log"
         Top             =   270
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   1376
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCargaManual.frx":68FD
         PICN            =   "frmCargaManual.frx":6919
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.ListBox lstLog 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   810
         ItemData        =   "frmCargaManual.frx":71F3
         Left            =   90
         List            =   "frmCargaManual.frx":71F5
         TabIndex        =   31
         TabStop         =   0   'False
         Top             =   270
         Width           =   14010
      End
   End
   Begin CoolXPFrame.xpFrame fraEnvio 
      Height          =   735
      Left            =   6345
      Top             =   -45
      Visible         =   0   'False
      Width           =   5460
      _ExtentX        =   9631
      _ExtentY        =   1296
      Caption         =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   13977088
      BorderColor     =   8421504
      ColorStyle      =   99
      UseCoolToolTip  =   0   'False
      Begin GradProgBar.SmoothBaar smtCargaTotal 
         Height          =   420
         Left            =   1665
         TabIndex        =   1
         Top             =   225
         Width           =   3705
         _ExtentX        =   6535
         _ExtentY        =   741
         Raised          =   0
         ProgressStartColor=   16711680
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Appearance      =   3
      End
      Begin VB.Label Label5 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Envio dos arquivos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   90
         TabIndex        =   0
         Top             =   315
         Width           =   1500
      End
   End
End
Attribute VB_Name = "frmCargaManual"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmbLoja_Click()

    On Error GoTo Erro

    If cmbLoja.ListIndex <> -1 Then

        Call Aguardar

        Call Desconectar

        fraDestino.Visible = False

        Call vVB_Generica_001.ExcluiBind(vBanco)

        vBanco.Parameters.Add "pAcao", 1, 1
        vBanco.Parameters.Add "pCodLoja", Val(cmbLoja), 1
        vBanco.Parameters.Add "pIP", "", 1
        vBanco.Parameters.Add "pCompartilhamento", "", 1
        vBanco.Parameters.Add "pUsuario", "", 1
        vBanco.Parameters.Add "pSenha", "", 1

        vBanco.Parameters.Add "pRetorno1", 0, 2
        vBanco.Parameters("pRetorno1").ServerType = 102
        vBanco.Parameters("pRetorno1").DynasetOption = &H2&
        vBanco.Parameters("pRetorno1").DynasetCacheParams 256, 16, 20, 2000, 0

        vBanco.Parameters.Add "pCodErro", 0, 2
        vBanco.Parameters.Add "pMsgErro", "", 2

        vSql = _
                "PRODUCAO.PCK_CON001.PRCADASTRO(" & _
                ":pAcao, " & _
                ":pCodLoja, " & _
                ":pIP, " & _
                ":pCompartilhamento, " & _
                ":pUsuario, " & _
                ":pSenha, " & _
                ":pRetorno1, " & _
                ":pCodErro, " & _
                ":pMsgErro)"

        vBanco.ExecuteSQL "Begin " & vSql & "; End;"

        If vBanco.Parameters("pCodErro") <> 0 Then

            Unload frmAguardar

            vErro = "Ocorreu um erro" & Chr(13) & _
                    "C�d. do erro: " & CStr(vBanco.Parameters("pCodErro")) & Chr(13) & _
                    "Msg. de erro: " & CStr(vBanco.Parameters("pMsgErro"))

            Call vVB_Generica_001.ProcessaErro(vErro)
            Exit Sub

        End If

        Set vObjOracle = vBanco.Parameters("pRetorno1").Value

        If Not vObjOracle.EOF Then

            lblIPDestino = vObjOracle.Fields(0)
            lblCompartilhamentoDestino = vObjOracle.Fields(1)
            lblUsuarioDestino = vObjOracle.Fields(2)
            lblSenhaDestino = vObjOracle.Fields(3)

            If fConectar(Val(cmbLoja), True) = False Then

                Unload frmAguardar
                Exit Sub

            End If

            Call cmdDiretorioDestinoPadrao_Click

            fraDestino.Visible = True

        Else

            Unload frmAguardar

            Call vVB_Generica_001.Informar("As informa��es de cadastro do dep�sito n�o foram encontradas.")
            Exit Sub

        End If

        Unload frmAguardar

    End If

    Exit Sub

Erro:

    Unload frmAguardar

    vErro = "Ocorreu um erro" & Chr(13) & _
            "C�d. do erro: " & Err.Number & Chr(13) & _
            "Msg. de erro: " & Err.Description

    Call vVB_Generica_001.ProcessaErro(vErro)

End Sub

Private Sub cmd1_Click()

    On Error GoTo Erro

    Dim vSelecionouArquivo As Boolean
    Dim vArquivo As String
    Dim fso As FileSystemObject

    Set fso = New FileSystemObject
    vSelecionouArquivo = False

    For i = 0 To lstArquivosDestino.ListCount - 1

        If lstArquivosDestino.Selected(i) = True Then

            vSelecionouArquivo = True
            vArquivo = lstArquivosDestino.List(i)

        End If

    Next

    If vSelecionouArquivo = False Then

        Call vVB_Generica_001.Informar("Escolha o arquivo desejado.")
        Exit Sub

    End If

    If Right(lstDiretoriosDestino, 1) <> "\" Then

        vDiretorioDestino = lstDiretoriosDestino & "\"

    Else

        vDiretorioDestino = lstDiretoriosDestino

    End If

    If vVB_Generica_001.Perguntar("Confirma exclus�o do arquivo " & Chr(13) & _
            vDiretorioDestino & vArquivo & "?") = 6 Then

        fso.DeleteFile vDiretorioDestino & vArquivo, True

        lstArquivosDestino.Refresh
        Call vVB_Generica_001.Informar("Arquivo exclu�do.")

    End If

    Exit Sub

Erro:

    Unload frmAguardar

    vErro = "Ocorreu um erro" & Chr(13) & _
            "C�d. do erro: " & Err.Number & Chr(13) & _
            "Msg. de erro: " & Err.Description

    Call vVB_Generica_001.ProcessaErro(vErro)

End Sub

Private Sub cmd2_Click()

    On Error GoTo Erro

    Dim vSelecionouArquivo As Boolean
    Dim vArquivo As String
    Dim fso As FileSystemObject

    Set fso = New FileSystemObject
    vSelecionouArquivo = False

    For i = 0 To lstArquivosDestino.ListCount - 1

        If lstArquivosDestino.Selected(i) = True Then

            vSelecionouArquivo = True
            vArquivo = lstArquivosDestino.List(i)

        End If

    Next

    If vSelecionouArquivo = False Then

        Call vVB_Generica_001.Informar("Escolha o arquivo desejado.")
        Exit Sub

    End If

    If Right(lstDiretoriosDestino, 1) <> "\" Then

        vDiretorioDestino = lstDiretoriosDestino & "\"

    Else

        vDiretorioDestino = lstDiretoriosDestino

    End If

    dlgDiretorios.FileName = vArquivo
    dlgDiretorios.ShowSave

    If dlgDiretorios.FileName <> "" Then

        If fso.FileExists(dlgDiretorios.FileName) = True Then

            If vVB_Generica_001.Perguntar("Arquivo j� existe, deseja substituir?") = 6 Then

                fso.CopyFile vDiretorioDestino & vArquivo, dlgDiretorios.FileName, True

                lstArquivosDestino.Refresh
                Call vVB_Generica_001.Informar("Arquivo copiado.")

            End If

        Else

            If fso.FolderExists(Replace(dlgDiretorios.FileName, vArquivo, "")) = True Then

                fso.CopyFile vDiretorioDestino & vArquivo, dlgDiretorios.FileName, True

                lstArquivosDestino.Refresh
                Call vVB_Generica_001.Informar("Arquivo copiado.")

            End If

        End If

    End If

    Exit Sub

Erro:

    Unload frmAguardar

    vErro = "Ocorreu um erro" & Chr(13) & _
            "C�d. do erro: " & Err.Number & Chr(13) & _
            "Msg. de erro: " & Err.Description

    Call vVB_Generica_001.ProcessaErro(vErro)

End Sub
Private Sub cmdAtualizar_Click()

    lstArquivosOrigem.Refresh

End Sub
Private Sub cmdCopiar_Click()

    On Error GoTo Erro

    Dim vSelecionouArquivo As Boolean
    Dim vArquivo As String
    Dim fso As FileSystemObject

    Set fso = New FileSystemObject
    vSelecionouArquivo = False

    For i = 0 To lstArquivosOrigem.ListCount - 1

        If lstArquivosOrigem.Selected(i) = True Then

            vSelecionouArquivo = True
            vArquivo = lstArquivosOrigem.List(i)

        End If

    Next

    If vSelecionouArquivo = False Then

        Call vVB_Generica_001.Informar("Escolha o arquivo desejado.")
        Exit Sub

    End If

    If Right(lstDiretoriosOrigem, 1) <> "\" Then

        vDiretorioOrigem = lstDiretoriosOrigem & "\"

    Else

        vDiretorioOrigem = lstDiretoriosOrigem

    End If

    dlgDiretorios.FileName = vArquivo
    dlgDiretorios.ShowSave

    If dlgDiretorios.FileName <> "" Then

        If fso.FileExists(dlgDiretorios.FileName) = True Then

            If vVB_Generica_001.Perguntar("Arquivo j� existe, deseja substituir?") = 6 Then

                fso.CopyFile vDiretorioOrigem & vArquivo, dlgDiretorios.FileName, True

                lstArquivosOrigem.Refresh
                Call vVB_Generica_001.Informar("Arquivo copiado.")

            End If

        Else

            If fso.FolderExists(Replace(dlgDiretorios.FileName, vArquivo, "")) = True Then

                fso.CopyFile vDiretorioOrigem & vArquivo, dlgDiretorios.FileName, True

                lstArquivosOrigem.Refresh
                Call vVB_Generica_001.Informar("Arquivo copiado.")

            End If

        End If

    End If

    Exit Sub

Erro:

    Unload frmAguardar

    vErro = "Ocorreu um erro" & Chr(13) & _
            "C�d. do erro: " & Err.Number & Chr(13) & _
            "Msg. de erro: " & Err.Description

    Call vVB_Generica_001.ProcessaErro(vErro)

End Sub
Private Sub cmdDiretorioDestinoPadrao_Click()

    On Error GoTo Erro

    Call Aguardar

    lstDiretoriosDestino.Path = "Z:\ORACLE\SISTEMAS\VB\32BITS"
    lstDiretoriosDestino.Refresh
    lstArquivosDestino.Refresh

    Unload frmAguardar

    Exit Sub

Erro:

    Unload frmAguardar

    vErro = "Ocorreu um erro" & Chr(13) & _
            "C�d. do erro: " & Err.Number & Chr(13) & _
            "Msg. de erro: " & Err.Description

    Call vVB_Generica_001.ProcessaErro(vErro)

End Sub
Private Sub cmdDiretorioOrigemPadrao_Click()

    lstDrivesOrigem.Drive = "P:"
    lstDiretoriosOrigem.Path = "P:\ORACLE\SISTEMAS\CARGAS\"

End Sub

Private Sub cmdEnviar_Click()

    On Error GoTo Erro

    If cmbLoja.ListIndex = -1 Then

        Call vVB_Generica_001.Informar("Escolha o dep�sito.")
        cmbLoja.SetFocus
        Exit Sub

    End If

    If lstArquivosEnviar.ListItems.Count = 0 Then

        Call vVB_Generica_001.Informar("Escolha os arquivos a serem enviados.")
        Exit Sub

    End If

    If fraDestino.Visible = False Then

        Call vVB_Generica_001.Informar("N�o foi poss�vel localizar as informa��es do dep�sito selecionado.")
        Exit Sub

    End If

    If vVB_Generica_001.Perguntar("Confirma opera��o?") = 6 Then

        If chkBackup.Value = cNichtAktiviert Then

            If vVB_Generica_001.Perguntar("Para maior seguran�a, deseja gerar backup do arquivo a ser atualizado antes da c�pia?") = 6 Then

                chkBackup.Value = cAktiviert

            End If

        End If

        Call Aguardar

        Dim vArquivoBackup As String
        Dim vTotalArquivos As Double
        Dim vPercentualTotal As Integer
        Dim vPercentualArquivo As Integer

        fraEnvio.Visible = True
        smtCargaTotal.Value 0
        Me.Refresh

        vTotalArquivos = lstArquivosEnviar.ListItems.Count
        vDiretorioOrigem = ""
        vArquivoOrigem = ""
        vDiretorioDestino = ""
        vArquivoDestino = ""

        Call GeraLog(Me, "In�cio do envio dos arquivos", False)

        For i = 1 To lstArquivosEnviar.ListItems.Count

            vPercentualTotal = (i * 100) / vTotalArquivos

            vDiretorioOrigem = UCase(lstArquivosEnviar.ListItems(i).SubItems(1).Caption)

            If Right(lstDiretoriosDestino.Path, 1) <> "\" Then

                vDiretorioDestino = lstDiretoriosDestino.Path & "\"

            Else

                vDiretorioDestino = lstDiretoriosDestino.Path

            End If

            vArquivoOrigem = UCase(lstArquivosEnviar.ListItems(i).Text)
            vArquivoDestino = vArquivoOrigem

            If fVerificarSeArquivoExiste(vDiretorioOrigem & vArquivoOrigem) = False Then

                Call GeraLog(Me, "Arquivo origem " & vDiretorioOrigem & vArquivoOrigem & " n�o encontrado", False)

            Else

                If fVerificarSeArquivoEstaAberto(vDiretorioOrigem & vArquivoOrigem) = True Then

                    Call GeraLog(Me, "Arquivo origem " & vDiretorioOrigem & vArquivoOrigem & " est� em uso", False)

                Else

                    If fVerificarSeArquivoExiste(vDiretorioDestino & vArquivoDestino) = True Then

                        If fVerificarSeArquivoEstaAberto(vDiretorioDestino & vArquivoDestino) = True Then

                            If vVB_Generica_001.Perguntar("Arquivo destino " & vDiretorioDestino & vArquivoDestino & " est� em uso." & Chr(13) & _
                                    "Deseja fechar o arquivo para efetuar o envio?" & Chr(13) & _
                                    "Caso seja confirmado, aparecer� um erro para todos os usu�rios que estiverem utilizando o arquivo") = 6 Then

                                Dim vDiretorioFechar As String
                                vDiretorioFechar = Replace(vDiretorioDestino, "Z:\", "D:" & Replace(lblCompartilhamentoDestino, "$", "") & "\")

                                Call FecharArquivo(lblIPDestino, lblUsuarioDestino, lblSenhaDestino, vDiretorioFechar, vArquivoOrigem)

                                Call GeraLog(Me, "Enviando arquivo " & vDiretorioOrigem & vArquivoOrigem & " para " & vDiretorioDestino & vArquivoDestino & " - Tam.: " & fTamanhoArquivo(vDiretorioOrigem & vArquivoOrigem) & " KB", False)

                                Call CopiarArquivo(vDiretorioOrigem & vArquivoOrigem, vDiretorioDestino & vArquivoDestino, chkBackup.Value)

                                smtCargaTotal.Value CLng(vPercentualTotal)

                                Call GeraLog(Me, "Arquivo enviado com sucesso", False)

                            Else

                                Call GeraLog(Me, "Arquivo destino " & vDiretorioDestino & vArquivoDestino & " est� em uso", False)

                            End If

                        Else

                            Call GeraLog(Me, "Enviando arquivo " & vDiretorioOrigem & vArquivoOrigem & " para " & vDiretorioDestino & vArquivoDestino & " - Tam.: " & fTamanhoArquivo(vDiretorioOrigem & vArquivoOrigem) & " KB", False)

                            Call CopiarArquivo(vDiretorioOrigem & vArquivoOrigem, vDiretorioDestino & vArquivoDestino, chkBackup.Value)

                            smtCargaTotal.Value CLng(vPercentualTotal)

                            Call GeraLog(Me, "Arquivo enviado com sucesso", False)

                        End If

                    Else

                        Call GeraLog(Me, "Enviando arquivo " & vDiretorioOrigem & vArquivoOrigem & " para " & vDiretorioDestino & vArquivoDestino & " - Tam.: " & fTamanhoArquivo(vDiretorioOrigem & vArquivoOrigem) & " KB", False)

                        Call CopiarArquivo(vDiretorioOrigem & vArquivoOrigem, vDiretorioDestino & vArquivoDestino, chkBackup.Value)

                        smtCargaTotal.Value CLng(vPercentualTotal)

                        Call GeraLog(Me, "Arquivo enviado com sucesso", False)

                    End If

                End If

            End If

        Next

        Unload frmAguardar
        lstArquivosDestino.Refresh
        Call GeraLog(Me, "Fim do envio dos arquivos", False)
        Call vVB_Generica_001.Informar("Opera��o realizada com sucesso.")

    End If

    Exit Sub

Erro:

    Unload frmAguardar

    Call GeraLog(Me, "Ocorreu um erro. C�d. do erro: " & Err.Number & "   Msg. de erro: " & Err.Description, False)
    vErro = "Ocorreu um erro" & Chr(13) & _
            "C�d. do erro: " & Err.Number & Chr(13) & _
            "Msg. de erro: " & Err.Description

    Call vVB_Generica_001.ProcessaErro(vErro)

End Sub
Private Sub cmdExcluir_Click()

    On Error GoTo Erro

    Dim vSelecionouArquivo As Boolean
    Dim vArquivo As String
    Dim fso As FileSystemObject

    Set fso = New FileSystemObject
    vSelecionouArquivo = False

    For i = 0 To lstArquivosOrigem.ListCount - 1

        If lstArquivosOrigem.Selected(i) = True Then

            vSelecionouArquivo = True
            vArquivo = lstArquivosOrigem.List(i)

        End If

    Next

    If vSelecionouArquivo = False Then

        Call vVB_Generica_001.Informar("Escolha o arquivo desejado.")
        Exit Sub

    End If

    If Right(lstDiretoriosOrigem, 1) <> "\" Then

        vDiretorioOrigem = lstDiretoriosOrigem & "\"

    Else

        vDiretorioOrigem = lstDiretoriosOrigem

    End If

    If vVB_Generica_001.Perguntar("Confirma exclus�o do arquivo " & Chr(13) & _
            vDiretorioOrigem & vArquivo & "?") = 6 Then

        fso.DeleteFile vDiretorioOrigem & vArquivo, True

        lstArquivosOrigem.Refresh
        Call vVB_Generica_001.Informar("Arquivo exclu�do.")

    End If

    Exit Sub

Erro:

    Unload frmAguardar

    vErro = "Ocorreu um erro" & Chr(13) & _
            "C�d. do erro: " & Err.Number & Chr(13) & _
            "Msg. de erro: " & Err.Description

    Call vVB_Generica_001.ProcessaErro(vErro)

End Sub

Private Sub cmdInformacoesDestino_Click()

    If lstArquivosDestino.ListIndex <> -1 Then
    
        If Right(lstDiretoriosDestino, 1) <> "\" Then
    
            vDiretorioDestino = lstDiretoriosDestino & "\"
    
        Else
    
            vDiretorioDestino = lstDiretoriosDestino
    
        End If
        
        Call vVB_Generica_001.Informar("Arquivo: " & lstArquivosDestino & vbCrLf & _
            "Tamanho: " & fTamanhoArquivo(vDiretorioDestino & lstArquivosDestino) & " KB" & vbCrLf & _
            "�ltima atualiza��o: " & FileDateTime(vDiretorioDestino & lstArquivosDestino))
    
    End If

End Sub
Private Sub cmdLimpar_Click()

    lstLog.Clear

End Sub
Private Sub cmdNovaPastaDestino_Click()

    Dim vPasta As String
    Dim a
    Dim vDiretorioDestino

    vPasta = InputBox("Digite o nome da pasta a ser criada", "Nova pasta")

    If vPasta <> "" Then

        Call Aguardar

        vDiretorioDestino = lstDiretoriosDestino.Path

        If Right(vDiretorioDestino, 1) <> "\" Then

            vDiretorioDestino = vDiretorioDestino & "\"

        End If

        a = Dir(vDiretorioDestino & vPasta, vbDirectory)

        If a = "" Then

            MkDir vDiretorioDestino & vPasta
            lstDiretoriosDestino.Refresh

        Else

            Unload frmAguardar

            Call vVB_Generica_001.Informar("Pasta j� existe.")
            Exit Sub

        End If

    End If

    Unload frmAguardar

End Sub
Private Sub cmdVerificar_Click()

    On Error GoTo Erro

    If cmbLoja.ListIndex = -1 Then

        Call vVB_Generica_001.Informar("Escolha o dep�sito.")
        cmbLoja.SetFocus
        Exit Sub

    End If

    If lstArquivosEnviar.ListItems.Count = 0 Then

        Call vVB_Generica_001.Informar("Escolha os arquivos a serem verificados.")
        Exit Sub

    End If

    If fraDestino.Visible = False Then

        Call vVB_Generica_001.Informar("N�o foi poss�vel localizar as informa��es do dep�sito selecionado.")
        Exit Sub

    End If

    If vVB_Generica_001.Perguntar("Confirma opera��o?") = 6 Then

        Call Aguardar

        Dim vTeste
        vTeste = Dir("Z:", vbDirectory)

        If vTeste = "" Then

            If fConectar(Val(cmbLoja), True) = False Then

                Unload frmAguardar
                Exit Sub

            End If

        End If

        Dim vDiretorioOrigem As String
        Dim vDiretorioDestino As String
        Dim vArquivoOrigem As String
        Dim vArquivoDestino As String

        Call GeraLog(Me, "In�cio da verifica��o", False)

        For i = 1 To lstArquivosEnviar.ListItems.Count

            vDiretorioOrigem = UCase(lstArquivosEnviar.ListItems(i).SubItems(1).Caption)

            If Right(lstDiretoriosDestino.Path, 1) <> "\" Then

                vDiretorioDestino = lstDiretoriosDestino.Path & "\"

            Else

                vDiretorioDestino = lstDiretoriosDestino.Path

            End If

            vArquivoOrigem = UCase(lstArquivosEnviar.ListItems(i).Text)
            vArquivoDestino = vArquivoOrigem

            If fVerificarSeArquivoExiste(vDiretorioOrigem & vArquivoOrigem) = False Then

                Call GeraLog(Me, "Arquivo origem " & vDiretorioOrigem & vArquivoOrigem & " n�o encontrado", False)

            Else

                If fVerificarSeArquivoEstaAberto(vDiretorioOrigem & vArquivoOrigem) = True Then

                    Call GeraLog(Me, "Arquivo origem " & vDiretorioOrigem & vArquivoOrigem & " est� em uso", False)

                Else

                    If fVerificarSeArquivoExiste(vDiretorioDestino & vArquivoDestino) = True Then

                        If fVerificarSeArquivoEstaAberto(vDiretorioDestino & vArquivoDestino) = True Then

                            Call GeraLog(Me, "Arquivo destino " & vDiretorioDestino & vArquivoDestino & " est� em uso", False)

                        Else

                            Call GeraLog(Me, "A carga do arquivo " & vDiretorioDestino & vArquivoDestino & " est� liberada", False)

                        End If

                    Else

                        Call GeraLog(Me, "A carga do arquivo " & vDiretorioDestino & vArquivoDestino & " est� liberada", False)

                    End If

                End If

            End If

        Next

        Unload frmAguardar
        Call GeraLog(Me, "Fim da verifica��o", False)

    End If

    Exit Sub

Erro:

    Unload frmAguardar

    Call GeraLog(Me, "Ocorreu um erro. C�d. do erro: " & Err.Number & "Msg. de erro: " & Err.Description, False)
    vErro = "Ocorreu um erro" & Chr(13) & _
            "C�d. do erro: " & Err.Number & Chr(13) & _
            "Msg. de erro: " & Err.Description

    Call vVB_Generica_001.ProcessaErro(vErro)

End Sub
Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()

    On Error GoTo Erro

    dlgDiretorios.InitDir = "P:\"

    Me.Top = 0
    Me.Left = 0

    fraEnvio.Visible = False

    smtCargaTotal.Value 0

    Set vObjOracle = vVB_Generica_001.TabelaLoja(vBanco)
    Call vVB_Generica_001.PreencheComboList(vObjOracle, cmbLoja, vObjOracle.Fields(0).Name)

    lstDrivesOrigem.Drive = "P:"
    lstDiretoriosOrigem.Path = "P:\ORACLE\SISTEMAS\CARGAS\"
    lstArquivosOrigem.Path = lstDiretoriosOrigem.Path

    lstArquivosEnviar.Columns.Add , , "ARQUIVO", , 2000
    lstArquivosEnviar.Columns.Add , , "DIRET�RIO", , 5200
    lstArquivosEnviar.Columns.Add , , "TAMANHO (KB)", , 1400
    lstArquivosEnviar.Columns(1).Alignment = eLVColumnAlignLeft
    lstArquivosEnviar.Columns(2).Alignment = eLVColumnAlignLeft
    lstArquivosEnviar.Columns(3).Alignment = eLVColumnAlignRight

    Exit Sub

Erro:

    Unload frmAguardar

    vErro = "Ocorreu um erro" & Chr(13) & _
            "C�d. do erro: " & Err.Number & Chr(13) & _
            "Msg. de erro: " & Err.Description

    Call vVB_Generica_001.ProcessaErro(vErro)

End Sub
Private Sub lstArquivosDestino_DblClick()

    Call Aguardar

    Dim vDiretorio As String
    Dim vArquivo As String

    If Right(lstArquivosDestino.Path, 1) <> "\" Then

        vDiretorio = lstArquivosDestino.Path & "\"

    Else

        vDiretorio = lstArquivosDestino.Path

    End If

    vArquivo = lstArquivosDestino

    If fVerificarSeArquivoEstaAberto(vDiretorio & vArquivo) = True Then

        dlgUsuarios.lstUsuarios.Clear

        Open "C:\LOG_USO.BAT" For Output As #1
        Print #1, "PSFILE \\" & lblIPDestino & " -U " & lblUsuarioDestino & " -P " & lblSenhaDestino & " " & "D:" & Replace(lblCompartilhamentoDestino, "$", "") & "\" & Replace(vDiretorio, "Z:\", "") & vArquivo & " > C:\LOG_USO.LOG"
        Close #1

        Dim a
        a = Shell("C:\LOG_USO.BAT", vbHide)
        Call ExecutarEsperar("C:\LOG_USO.BAT")
        Call Sleep(2000)
        Kill "C:\LOG_USO.BAT"

        Open "C:\LOG_USO.LOG" For Input As #1

        While Not EOF(1)

            Line Input #1, vLinhaArquivo

            If InStr(1, UCase(vLinhaArquivo), "USER:") > 0 Then

                vUsuario = UCase(Mid(vLinhaArquivo, 13))
                dlgUsuarios.lstUsuarios.AddItem vUsuario

            End If

        Wend

        Close #1

        Unload frmAguardar
        dlgUsuarios.Show 1

        If fVerificarSeArquivoExiste("C:\LOG_USO.LOG") = True Then

            Kill "C:\LOG_USO.LOG"

        End If

    Else

        Unload frmAguardar
        Call vclsdpk001.Informar("Arquivo n�o est� em uso.")
        Exit Sub

    End If

    Unload frmAguardar

End Sub
Private Sub lstArquivosEnviar_ItemDblClick(Item As vbalListViewLib6.cListItem)

    lstArquivosEnviar.ListItems.Remove (lstArquivosEnviar.SelectedItem.Index)

End Sub

Private Sub lstArquivosOrigem_DblClick()

    Dim vDiretorioOrigem As String
    Dim vArquivoOrigem As String
    Dim vItem As vbalListViewLib6.cListItem

    vArquivoOrigem = UCase(lstArquivosOrigem)

    If Right(lstDiretoriosOrigem, 1) <> "\" Then

        vDiretorioOrigem = lstDiretoriosOrigem & "\"

    Else

        vDiretorioOrigem = lstDiretoriosOrigem

    End If

    Set vItem = lstArquivosEnviar.ListItems.Add(, , vArquivoOrigem)
    vItem.SubItems(1).Caption = UCase(vDiretorioOrigem)
    vItem.SubItems(2).Caption = fTamanhoArquivo(vDiretorioOrigem & vArquivoOrigem)

    lstArquivosEnviar.Columns(1).SortType = eLVSortString
    lstArquivosEnviar.Columns(1).SortOrder = eSortOrderAscending
    lstArquivosEnviar.ListItems.SortItems

End Sub
Private Sub lstDiretoriosDestino_Change()

    lstArquivosDestino.Path = lstDiretoriosDestino.Path

End Sub
Private Sub lstDiretoriosOrigem_Change()

    lstArquivosOrigem.Path = lstDiretoriosOrigem.Path

End Sub

Private Sub lstDrivesOrigem_Change()

    lstDiretoriosOrigem.Path = lstDrivesOrigem.Drive

End Sub
