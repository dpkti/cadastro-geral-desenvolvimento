VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Object = "{E57FB72C-1106-44AF-9706-0BA099A707C7}#4.2#0"; "XPFrame.ocx"
Object = "{E910F8E1-8996-4EE9-90F1-3E7C64FA9829}#1.1#0"; "vbaListView6.ocx"
Begin VB.Form frmAcompanhamento 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "::: ACOMPANHAMENTO DAS CARGAS AUTOM�TICAS E AGENDAMENTOS :::"
   ClientHeight    =   7710
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   15180
   ControlBox      =   0   'False
   Icon            =   "frmAcompanhamento.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   514
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   1012
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   810
      Width           =   15090
      _ExtentX        =   26617
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   14445
      TabIndex        =   0
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmAcompanhamento.frx":23D2
      PICN            =   "frmAcompanhamento.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin CoolXPFrame.xpFrame fraParametros 
      Height          =   645
      Left            =   45
      Top             =   0
      Width           =   4875
      _ExtentX        =   8599
      _ExtentY        =   1138
      Caption         =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   13977088
      BorderColor     =   8421504
      ColorStyle      =   99
      UseCoolToolTip  =   0   'False
      Begin VB.ComboBox cmbAnalista 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   765
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   225
         Width           =   4020
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Analista"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   90
         TabIndex        =   3
         Top             =   270
         Width           =   615
      End
   End
   Begin CoolXPFrame.xpFrame fraAgenda 
      Height          =   3435
      Left            =   45
      Top             =   810
      Visible         =   0   'False
      Width           =   15090
      _ExtentX        =   26617
      _ExtentY        =   6059
      Caption         =   "Cargas agendadas"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   13977088
      BorderColor     =   8421504
      ColorStyle      =   99
      UseCoolToolTip  =   0   'False
      Begin vbalListViewLib6.vbalListViewCtl lstAgenda 
         Height          =   3075
         Left            =   90
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   270
         Width           =   14910
         _ExtentX        =   26300
         _ExtentY        =   5424
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         View            =   1
         LabelEdit       =   0   'False
         FullRowSelect   =   -1  'True
         AutoArrange     =   0   'False
         Appearance      =   0
         HeaderButtons   =   0   'False
         HeaderTrackSelect=   0   'False
         HideSelection   =   0   'False
         InfoTips        =   0   'False
      End
   End
   Begin CoolXPFrame.xpFrame fraEfetuadas 
      Height          =   3435
      Left            =   45
      Top             =   4230
      Visible         =   0   'False
      Width           =   15090
      _ExtentX        =   26617
      _ExtentY        =   6059
      Caption         =   "Cargas efetuadas"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   13977088
      BorderColor     =   8421504
      ColorStyle      =   99
      UseCoolToolTip  =   0   'False
      Begin vbalListViewLib6.vbalListViewCtl lstCargas 
         Height          =   3075
         Left            =   90
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   270
         Width           =   14910
         _ExtentX        =   26300
         _ExtentY        =   5424
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         View            =   1
         LabelEdit       =   0   'False
         FullRowSelect   =   -1  'True
         AutoArrange     =   0   'False
         Appearance      =   0
         HeaderButtons   =   0   'False
         HeaderTrackSelect=   0   'False
         HideSelection   =   0   'False
         InfoTips        =   0   'False
      End
   End
End
Attribute VB_Name = "frmAcompanhamento"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdSair_Click()

    Unload Me

End Sub

Private Sub cmbAnalista_Click()

    Dim vItem As vbalListViewLib6.cListItem

    fraAgenda.Visible = False
    fraEfetuadas.Visible = False
    lstAgenda.ListItems.Clear
    lstCargas.ListItems.Clear

    Call vVB_Generica_001.ExcluiBind(vBanco)

    vBanco.Parameters.Add "pCodAnalista", Val(cmbAnalista), 1

    vBanco.Parameters.Add "pRetorno1", 0, 2
    vBanco.Parameters("pRetorno1").ServerType = 102
    vBanco.Parameters("pRetorno1").DynasetOption = &H2&
    vBanco.Parameters("pRetorno1").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Add "pRetorno2", 0, 2
    vBanco.Parameters("pRetorno2").ServerType = 102
    vBanco.Parameters("pRetorno2").DynasetOption = &H2&
    vBanco.Parameters("pRetorno2").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Add "pCodErro", 0, 2
    vBanco.Parameters.Add "pMsgErro", "", 2

    vSql = _
            "PRODUCAO.PCK_CON001.PRACOMPANHAMENTO(" & _
            ":pCodAnalista, " & _
            ":pRetorno1, " & _
            ":pRetorno2, " & _
            ":pCodErro, " & _
            ":pMsgErro)"

    Call Aguardar

    vBanco.ExecuteSQL "Begin " & vSql & "; End;"

    Unload frmAguardar

    If vBanco.Parameters("pCodErro") <> 0 Then

        vErro = "Ocorreu um erro" & Chr(13) & _
                "C�d. do erro: " & CStr(vBanco.Parameters("pCodErro")) & Chr(13) & _
                "Msg. de erro: " & CStr(vBanco.Parameters("pMsgErro"))

        Call vVB_Generica_001.ProcessaErro(vErro)
        Exit Sub

    End If

    Set vObjOracle = vBanco.Parameters("pRetorno1").Value

    If Not vObjOracle.EOF Then

        While Not vObjOracle.EOF
        
            Set vItem = lstAgenda.ListItems.Add(, , CStr(vObjOracle.Fields(0)))
            vItem.SubItems(1).Caption = CStr(vObjOracle.Fields(1))
            vItem.SubItems(2).Caption = CStr(vObjOracle.Fields(2))
            vItem.SubItems(3).Caption = CStr(vObjOracle.Fields(3))
            vItem.SubItems(4).Caption = CStr(vObjOracle.Fields(4))
            vItem.SubItems(5).Caption = CStr(vObjOracle.Fields(5))
            vItem.SubItems(6).Caption = CStr(vObjOracle.Fields(6))
            
            vObjOracle.MoveNext
        
        Wend
        
        lstAgenda.ListItems(1).Selected = True
        
        fraAgenda.Visible = True

    End If

    Set vObjOracle = vBanco.Parameters("pRetorno2").Value

    If Not vObjOracle.EOF Then

        While Not vObjOracle.EOF
        
            Set vItem = lstCargas.ListItems.Add(, , CStr(vObjOracle.Fields(0)))
            vItem.SubItems(1).Caption = CStr(vObjOracle.Fields(1))
            vItem.SubItems(2).Caption = CStr(vObjOracle.Fields(2))
            vItem.SubItems(3).Caption = CStr(vObjOracle.Fields(3))
            vItem.SubItems(4).Caption = CStr(vObjOracle.Fields(4))
            vItem.SubItems(5).Caption = CStr(vObjOracle.Fields(5))
            vItem.SubItems(6).Caption = CStr(vObjOracle.Fields(6))
            vItem.SubItems(7).Caption = CStr(vObjOracle.Fields(7))
            
            vObjOracle.MoveNext
        
        Wend
        
        lstCargas.ListItems(1).Selected = True
        
        fraEfetuadas.Visible = True

    End If

End Sub
Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()

    On Error GoTo Erro

    lstAgenda.Columns.Add , , "ID", , 800
    lstAgenda.Columns.Add , , "DEP�SITO", , 2000
    lstAgenda.Columns.Add , , "ARQUIVO", , 2000
    lstAgenda.Columns.Add , , "ORIGEM", , 4000
    lstAgenda.Columns.Add , , "DESTINO", , 4000
    lstAgenda.Columns.Add , , "DT. AGENDAMENTO", , 1700
    lstAgenda.Columns.Add , , "DT. PROGRAMADA", , 1700
    
    lstCargas.Columns.Add , , "ID", , 800
    lstCargas.Columns.Add , , "DEP�SITO", , 2000
    lstCargas.Columns.Add , , "ARQUIVO", , 2000
    lstCargas.Columns.Add , , "ORIGEM", , 4000
    lstCargas.Columns.Add , , "DESTINO", , 4000
    lstCargas.Columns.Add , , "DT. AGENDAMENTO", , 1700
    lstCargas.Columns.Add , , "DT. CARGA", , 1700
    lstCargas.Columns.Add , , "STATUS", , 5000
    
    Me.Top = 0
    Me.Left = 0

    Call vVB_Generica_001.ExcluiBind(vBanco)

    vBanco.Parameters.Add "pAcao", 5, 1
    vBanco.Parameters.Add "pUsuarioRede", "", 1
    vBanco.Parameters.Add "pDireito", "", 1

    vBanco.Parameters.Add "pRetorno1", 0, 2
    vBanco.Parameters("pRetorno1").ServerType = 102
    vBanco.Parameters("pRetorno1").DynasetOption = &H2&
    vBanco.Parameters("pRetorno1").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Add "pCodErro", 0, 2
    vBanco.Parameters.Add "pMsgErro", "", 2

    vSql = _
            "PRODUCAO.PCK_CON001.PRPROCEDURESGERAIS(" & _
            ":pAcao, " & _
            ":pUsuarioRede, " & _
            ":pDireito, " & _
            ":pRetorno1, " & _
            ":pCodErro, " & _
            ":pMsgErro)"

    Call Aguardar

    vBanco.ExecuteSQL "Begin " & vSql & "; End;"

    Unload frmAguardar

    If vBanco.Parameters("pCodErro") <> 0 Then

        vErro = "Ocorreu um erro" & Chr(13) & _
                "C�d. do erro: " & CStr(vBanco.Parameters("pCodErro")) & Chr(13) & _
                "Msg. de erro: " & CStr(vBanco.Parameters("pMsgErro"))

        Call vVB_Generica_001.ProcessaErro(vErro)
        Exit Sub

    End If

    Set vObjOracle = vBanco.Parameters("pRetorno1").Value

    If Not vObjOracle.EOF Then

        Call vVB_Generica_001.PreencheComboList(vObjOracle, cmbAnalista, vObjOracle.Fields(0).Name)

    End If

    Exit Sub

Erro:

    Unload frmAguardar

    vErro = "Ocorreu um erro" & Chr(13) & _
            "C�d. do erro: " & Err.Number & Chr(13) & _
            "Msg. de erro: " & Err.Description

    Call vVB_Generica_001.ProcessaErro(vErro)

End Sub
