VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Object = "{E57FB72C-1106-44AF-9706-0BA099A707C7}#4.2#0"; "XPFrame.ocx"
Begin VB.Form dlgUsuarios 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "::: USU�RIOS QUE EST�O COM O ARQUIVO ABERTO :::"
   ClientHeight    =   4740
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6270
   ControlBox      =   0   'False
   Icon            =   "dlgUsuarios.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   316
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   418
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   810
      Width           =   6180
      _ExtentX        =   10901
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Cancel          =   -1  'True
      Height          =   690
      Left            =   5535
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "dlgUsuarios.frx":000C
      PICN            =   "dlgUsuarios.frx":0028
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin CoolXPFrame.xpFrame fra 
      Height          =   3930
      Left            =   45
      Top             =   765
      Width           =   6180
      _ExtentX        =   10901
      _ExtentY        =   6932
      Caption         =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   13977088
      BorderColor     =   8421504
      ColorStyle      =   99
      UseCoolToolTip  =   0   'False
      Begin VB.ListBox lstUsuarios 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3630
         Left            =   90
         Sorted          =   -1  'True
         TabIndex        =   2
         Top             =   225
         Width           =   6000
      End
   End
   Begin Bot�o.cmd cmdAtualizar 
      Height          =   690
      Left            =   45
      TabIndex        =   3
      TabStop         =   0   'False
      ToolTipText     =   "Atualizar informa��es"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "dlgUsuarios.frx":0D02
      PICN            =   "dlgUsuarios.frx":0D1E
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "dlgUsuarios"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdAtualizar_Click()

    fra.Visible = False
    lstUsuarios.Clear
    lstUsuarios.Refresh

    Me.MousePointer = vbHourglass

    Dim vDiretorio As String
    Dim vArquivo As String

    If Right(frmCargaManual.lstArquivosDestino.Path, 1) <> "\" Then

        vDiretorio = frmCargaManual.lstArquivosDestino.Path & "\"

    Else

        vDiretorio = frmCargaManual.lstArquivosDestino.Path

    End If

    vArquivo = frmCargaManual.lstArquivosDestino

    If fVerificarSeArquivoEstaAberto(vDiretorio & vArquivo) = True Then

        Open "C:\LOG_USO.BAT" For Output As #1
        Print #1, "PSFILE \\" & frmCargaManual.lblIPDestino & " -U " & frmCargaManual.lblUsuarioDestino & " -P " & frmCargaManual.lblSenhaDestino & " " & "D:" & Replace(frmCargaManual.lblCompartilhamentoDestino, "$", "") & "\" & Replace(vDiretorio, "Z:\", "") & vArquivo & " > C:\LOG_USO.LOG"
        Close #1

        Dim a
        a = Shell("C:\LOG_USO.BAT", vbHide)
        Call ExecutarEsperar("C:\LOG_USO.BAT")
        Call Sleep(2000)
        Kill "C:\LOG_USO.BAT"

        Open "C:\LOG_USO.LOG" For Input As #1

        While Not EOF(1)

            Line Input #1, vLinhaArquivo

            If InStr(1, UCase(vLinhaArquivo), "USER:") > 0 Then

                vUsuario = UCase(Mid(Trim(vLinhaArquivo), 13))
                
                If lstUsuarios.ListCount > 0 Then
                
                    For i = 0 To lstUsuarios.ListCount - 1
                    
                        If UCase(lstUsuarios.List(i)) <> vUsuario Then
                    
                            lstUsuarios.AddItem vUsuario
                            Exit For
                        
                        End If
    
                    Next
                
                Else
                
                    lstUsuarios.AddItem vUsuario
                
                End If
                
            End If

        Wend

        Close #1

        Me.MousePointer = vbDefault

        Kill "C:\LOG_USO.LOG"

        fra.Visible = True

        Me.SetFocus

    Else

        Me.MousePointer = vbDefault
        Call vVB_Generica_001.Informar("Arquivo n�o est� em uso.")
        Exit Sub

    End If

    Me.MousePointer = vbDefault

End Sub
Private Sub cmdVoltar_Click()

    Unload Me

End Sub

Private Sub Form_Deactivate()

    Me.SetFocus

End Sub
Private Sub Form_Load()

    On Error GoTo Erro

    Me.Top = 0
    Me.Left = 0

    Exit Sub

Erro:

    Unload frmAguardar

    vErro = "Ocorreu um erro" & Chr(13) & _
            "C�d. do erro: " & Err.Number & Chr(13) & _
            "Msg. de erro: " & Err.Description

    Call vVB_Generica_001.ProcessaErro(vErro)

End Sub
