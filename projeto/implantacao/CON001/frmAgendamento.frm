VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{E57FB72C-1106-44AF-9706-0BA099A707C7}#4.2#0"; "XPFrame.ocx"
Object = "{E910F8E1-8996-4EE9-90F1-3E7C64FA9829}#1.1#0"; "VBALISTVIEW6.OCX"
Begin VB.Form frmAgendamento 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "::: AGENDAMENTO DE CARGAS :::"
   ClientHeight    =   8745
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   15180
   ControlBox      =   0   'False
   Icon            =   "frmAgendamento.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   583
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   1012
   Begin Bot�o.cmd cmdAgendar 
      Height          =   1005
      Left            =   7785
      TabIndex        =   18
      TabStop         =   0   'False
      ToolTipText     =   "Incluir a carga na agenda"
      Top             =   5130
      Width           =   1050
      _ExtentX        =   1852
      _ExtentY        =   1773
      BTYPE           =   3
      TX              =   "Incluir"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmAgendamento.frx":23D2
      PICN            =   "frmAgendamento.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   810
      Width           =   15090
      _ExtentX        =   26617
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   14445
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmAgendamento.frx":30C8
      PICN            =   "frmAgendamento.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin CoolXPFrame.xpFrame fraOrigem 
      Height          =   4200
      Left            =   45
      Top             =   810
      Width           =   4470
      _ExtentX        =   7885
      _ExtentY        =   7408
      Caption         =   "Origem"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   13977088
      BorderColor     =   8421504
      ColorStyle      =   99
      UseCoolToolTip  =   0   'False
      Begin vbalListViewLib6.vbalListViewCtl lstArquivosOrigem 
         Height          =   3165
         Left            =   90
         TabIndex        =   25
         TabStop         =   0   'False
         Top             =   945
         Width           =   4290
         _ExtentX        =   7567
         _ExtentY        =   5583
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         View            =   1
         LabelEdit       =   0   'False
         FullRowSelect   =   -1  'True
         AutoArrange     =   0   'False
         Appearance      =   0
         HeaderButtons   =   0   'False
         HeaderTrackSelect=   0   'False
         HideSelection   =   0   'False
         InfoTips        =   0   'False
      End
      Begin Bot�o.cmd cmdAtualizar 
         Height          =   375
         Left            =   4005
         TabIndex        =   4
         TabStop         =   0   'False
         ToolTipText     =   "Atualizar arquivos de origem"
         Top             =   225
         Width           =   375
         _ExtentX        =   661
         _ExtentY        =   661
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmAgendamento.frx":3DBE
         PICN            =   "frmAgendamento.frx":3DDA
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.FileListBox lstArquivosOrigemOld 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   90
         TabIndex        =   6
         TabStop         =   0   'False
         ToolTipText     =   "Duplo clique para incluir o arquivo da carga"
         Top             =   945
         Visible         =   0   'False
         Width           =   375
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Diret�rio origem padr�o:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   90
         TabIndex        =   3
         Top             =   270
         Width           =   1980
      End
      Begin VB.Label lblOrigem 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "P:\ORACLE\SISTEMAS\CARGAS\"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00D54600&
         Height          =   285
         Left            =   90
         TabIndex        =   5
         Top             =   630
         Width           =   4080
      End
   End
   Begin CoolXPFrame.xpFrame fraDepositos 
      Height          =   4200
      Left            =   4590
      Top             =   810
      Width           =   3120
      _ExtentX        =   5503
      _ExtentY        =   7408
      Caption         =   "Dep�sitos"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   13977088
      BorderColor     =   8421504
      ColorStyle      =   99
      UseCoolToolTip  =   0   'False
      Begin VB.ListBox lstDepositos 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3855
         ItemData        =   "frmAgendamento.frx":4174
         Left            =   90
         List            =   "frmAgendamento.frx":4176
         Style           =   1  'Checkbox
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   270
         Width           =   2940
      End
   End
   Begin CoolXPFrame.xpFrame fraInformacoesAdicionais 
      Height          =   1140
      Left            =   45
      Top             =   4995
      Width           =   7665
      _ExtentX        =   13520
      _ExtentY        =   2011
      Caption         =   "Informa��es adicionais"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   13977088
      BorderColor     =   8421504
      ColorStyle      =   99
      UseCoolToolTip  =   0   'False
      Begin MSComCtl2.DTPicker txtDataCarga 
         BeginProperty DataFormat 
            Type            =   0
            Format          =   "dd/MM/yyyy"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1046
            SubFormatType   =   0
         EndProperty
         Height          =   375
         Left            =   2385
         TabIndex        =   12
         TabStop         =   0   'False
         ToolTipText     =   "Data em que o sistema dever� estar dispon�vel"
         Top             =   270
         Width           =   1410
         _ExtentX        =   2487
         _ExtentY        =   661
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CustomFormat    =   "dd/MM/yyyy"
         Format          =   56819715
         CurrentDate     =   38569
      End
      Begin VB.TextBox txtDiretorioDestino 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1530
         TabIndex        =   17
         TabStop         =   0   'False
         Text            =   "Z:\ORACLE\SISTEMAS\VB\32BITS\"
         Top             =   720
         Width           =   5595
      End
      Begin Bot�o.cmd cmdPadrao 
         Height          =   375
         Left            =   7200
         TabIndex        =   16
         TabStop         =   0   'False
         ToolTipText     =   "Diret�rio padr�o de sistemas"
         Top             =   675
         Width           =   375
         _ExtentX        =   661
         _ExtentY        =   661
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmAgendamento.frx":4178
         PICN            =   "frmAgendamento.frx":4194
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.ComboBox cmbAnalista 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   4545
         Style           =   2  'Dropdown List
         TabIndex        =   14
         TabStop         =   0   'False
         Top             =   270
         Width           =   3030
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Disponibilizar �s 0:00 hr. de"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   90
         TabIndex        =   11
         Top             =   315
         Width           =   2205
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Diret�rio destino"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   90
         TabIndex        =   15
         Top             =   765
         Width           =   1350
      End
      Begin VB.Label Label4 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Analista"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   3870
         TabIndex        =   13
         Top             =   315
         Width           =   615
      End
   End
   Begin Bot�o.cmd cmdGravar 
      Height          =   690
      Left            =   45
      TabIndex        =   0
      TabStop         =   0   'False
      ToolTipText     =   "Gravar agendamento"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmAgendamento.frx":452E
      PICN            =   "frmAgendamento.frx":454A
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdCancelar 
      Height          =   690
      Left            =   765
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Limpar todos os campos"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmAgendamento.frx":5224
      PICN            =   "frmAgendamento.frx":5240
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin CoolXPFrame.xpFrame fraCargasAgendadas 
      Height          =   5325
      Left            =   8910
      Top             =   810
      Width           =   6225
      _ExtentX        =   10980
      _ExtentY        =   9393
      Caption         =   "Cargas agendadas"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   13977088
      BorderColor     =   8421504
      ColorStyle      =   99
      UseCoolToolTip  =   0   'False
      Begin vbalListViewLib6.vbalListViewCtl lstCargasAgendadas 
         Height          =   4110
         Left            =   90
         TabIndex        =   10
         TabStop         =   0   'False
         ToolTipText     =   "Duplo clique para excluir agendamento"
         Top             =   720
         Width           =   6045
         _ExtentX        =   10663
         _ExtentY        =   7250
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         View            =   1
         LabelEdit       =   0   'False
         FullRowSelect   =   -1  'True
         AutoArrange     =   0   'False
         Appearance      =   0
         HeaderButtons   =   0   'False
         HeaderTrackSelect=   0   'False
         HideSelection   =   0   'False
         InfoTips        =   0   'False
      End
      Begin MSComCtl2.DTPicker txtDataParametro 
         BeginProperty DataFormat 
            Type            =   0
            Format          =   "dd/MM/yyyy"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1046
            SubFormatType   =   0
         EndProperty
         Height          =   375
         Left            =   540
         TabIndex        =   23
         TabStop         =   0   'False
         ToolTipText     =   "Data em que o sistema dever� estar dispon�vel"
         Top             =   270
         Width           =   1410
         _ExtentX        =   2487
         _ExtentY        =   661
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CustomFormat    =   "dd/MM/yyyy"
         Format          =   56819715
         CurrentDate     =   38569
      End
      Begin VB.TextBox txtTamanhoArquivo 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   4365
         Locked          =   -1  'True
         TabIndex        =   20
         TabStop         =   0   'False
         Top             =   4905
         Width           =   1500
      End
      Begin Bot�o.cmd cmdAtualizarCargas 
         Height          =   375
         Left            =   90
         TabIndex        =   9
         TabStop         =   0   'False
         ToolTipText     =   "Atualizar agenda"
         Top             =   270
         Width           =   375
         _ExtentX        =   661
         _ExtentY        =   661
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmAgendamento.frx":5B1A
         PICN            =   "frmAgendamento.frx":5B36
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label lblNenhumaCarga 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Nenhuma carga agendada"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   435
         Left            =   90
         TabIndex        =   24
         Top             =   675
         Width           =   4665
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Tamanho dos arquivos das cargas agendadas"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   630
         TabIndex        =   22
         Top             =   4950
         Width           =   3660
      End
      Begin VB.Label Label5 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "KB"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   5940
         TabIndex        =   21
         Top             =   4950
         Width           =   210
      End
   End
   Begin CoolXPFrame.xpFrame fraInformacoes 
      Height          =   2580
      Left            =   45
      Top             =   6120
      Width           =   15090
      _ExtentX        =   26617
      _ExtentY        =   4551
      Caption         =   "Agendamentos"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   13977088
      BorderColor     =   8421504
      ColorStyle      =   99
      UseCoolToolTip  =   0   'False
      Begin vbalListViewLib6.vbalListViewCtl lstCargas 
         Height          =   2220
         Left            =   90
         TabIndex        =   19
         TabStop         =   0   'False
         ToolTipText     =   "Duplo clique para retirar o arquivo da carga"
         Top             =   270
         Width           =   14910
         _ExtentX        =   26300
         _ExtentY        =   3916
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         View            =   1
         LabelEdit       =   0   'False
         FullRowSelect   =   -1  'True
         AutoArrange     =   0   'False
         Appearance      =   0
         HeaderButtons   =   0   'False
         HeaderTrackSelect=   0   'False
         HideSelection   =   0   'False
         InfoTips        =   0   'False
      End
   End
End
Attribute VB_Name = "frmAgendamento"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdSair_Click()

    Unload Me

End Sub

Private Sub cmdAgendar_Click()

    Dim vSelecionouDeposito As Boolean
    Dim vSelecionouArquivo As Boolean

    vSelecionouDeposito = False
    vSelecionouArquivo = False

    For i = 1 To lstArquivosOrigem.ListItems.Count

        If lstArquivosOrigem.ListItems(i).Selected = True Then

            vSelecionouArquivo = True
            vArquivo = lstArquivosOrigem.ListItems(i).Text

        End If
        
    Next

    If vSelecionouArquivo = False Then

        Call vVB_Generica_001.Informar("Escolha o arquivo desejado.")
        Exit Sub

    End If

    For i = 1 To lstDepositos.ListCount - 1

        If lstDepositos.Selected(i) = True Then

            vSelecionouDeposito = True

        End If

    Next

    If vSelecionouDeposito = False Then

        Call vVB_Generica_001.Informar("Escolha o dep�sito desejado.")
        Exit Sub

    End If

    If cmbAnalista = "" Then

        Call vVB_Generica_001.Informar("Informe o analista que est� realizando o agendamento.")
        cmbAnalista.SetFocus
        Exit Sub

    End If

    If txtDiretorioDestino = "" Then

        Call vVB_Generica_001.Informar("Informe o diret�rio destino.")
        txtDiretorioDestino.SetFocus
        Exit Sub

    End If

    If UCase(Mid(txtDiretorioDestino, 1, 3)) <> "Z:\" Then

        Call vVB_Generica_001.Informar("Diret�rio inv�lido. O diret�rio correto � Z:\<DIRET�RIO>")
        Exit Sub

    End If

    vDiretorioOrigem = Replace(lblOrigem, "P:", "H:")

    If Right(txtDiretorioDestino, 1) <> "\" Then

        vDiretorioDestino = UCase(txtDiretorioDestino) & "\"

    Else

        vDiretorioDestino = UCase(txtDiretorioDestino)

    End If

    Dim vItem As vbalListViewLib6.cListItem

    For i = 1 To lstDepositos.ListCount - 1

        If lstDepositos.Selected(i) = True Then

            vArquivoOrigem = UCase(vArquivo)

            Set vItem = lstCargas.ListItems.Add(, , UCase(lstDepositos.List(i)))
            vItem.SubItems(1).Caption = UCase(vArquivoOrigem)
            vItem.SubItems(2).Caption = fTamanhoArquivo(lblOrigem & vArquivoOrigem)
            vItem.SubItems(3).Caption = txtDataCarga.Value
            vItem.SubItems(4).Caption = vDiretorioOrigem
            vItem.SubItems(5).Caption = vDiretorioDestino
            vItem.SubItems(6).Caption = cmbAnalista

            lstCargas.Columns(4).SortType = eLVSortString
            lstCargas.Columns(4).SortOrder = eSortOrderAscending
            lstCargas.ListItems.SortItems

        End If
        
    Next

End Sub
Private Sub cmdAtualizar_Click()

    lstArquivosOrigemOld.Refresh

    If lstArquivosOrigemOld.ListCount > 0 Then
    
        lstArquivosOrigem.ListItems.Clear
        
        Dim vItem As vbalListViewLib6.cListItem

        For i = 0 To lstArquivosOrigemOld.ListCount - 1
        
            Set vItem = lstArquivosOrigem.ListItems.Add(, , UCase(lstArquivosOrigemOld.List(i)))
            vItem.SubItems(1).Caption = CStr(fTamanhoArquivo(lblOrigem & lstArquivosOrigemOld.List(i)))
        
        Next
    
        lstArquivosOrigemOld.Selected(0) = True
        
    End If

End Sub
Private Sub cmdAtualizarCargas_Click()

    Call CarregarAgendamentos_HOJE_Agenda(txtDataParametro)

End Sub
Private Sub cmdCancelar_Click()

    lstCargas.ListItems.Clear
    txtDataCarga.Value = Format(Now + 1, "DD/MM/YYYY")
    txtDataParametro.Value = Format(Now + 1, "DD/MM/YYYY")
    cmbAnalista.ListIndex = -1

    For i = 1 To lstDepositos.ListCount - 1

        lstDepositos.Selected(i) = False

    Next

    lstArquivosOrigemOld.Visible = True
    cmdAgendar.Enabled = True

End Sub
Private Sub cmdGravar_Click()

    On Error GoTo Erro

    If lstCargas.ListItems.Count = 0 Then

        Call vVB_Generica_001.Informar("Selecione as informa��es da agenda.")
        Exit Sub

    End If

    If vVB_Generica_001.Perguntar("Confirma opera��o?") = 6 Then

        Call Aguardar

        Dim vCodLoja As Integer
        Dim vArquivo As String
        Dim vDtProgramada As String
        Dim vDtCarga As String
        Dim vDiretorioOrigem As String
        Dim vDiretorioDestino As String
        Dim vCodAnalista As Integer

        For i = 1 To lstCargas.ListItems.Count

            vCodLoja = Val(lstCargas.ListItems(i).Text)
            vArquivo = lstCargas.ListItems(i).SubItems(1).Caption
            vDtProgramada = lstCargas.ListItems(i).SubItems(3).Caption
            vDiretorioOrigem = lstCargas.ListItems(i).SubItems(4).Caption
            vDiretorioDestino = lstCargas.ListItems(i).SubItems(5).Caption
            vCodAnalista = Val(lstCargas.ListItems(i).SubItems(6).Caption)

            Call vVB_Generica_001.ExcluiBind(vBanco)

            vBanco.Parameters.Add "pAcao", 2, 1
            vBanco.Parameters.Add "pSequencia", 0, 1
            vBanco.Parameters.Add "pCodLoja", vCodLoja, 1
            vBanco.Parameters.Add "pArquivo", vArquivo, 1
            vBanco.Parameters.Add "pDtAgendamento", "", 1
            vBanco.Parameters.Add "pDtProgramada", vDtProgramada, 1
            vBanco.Parameters.Add "pDtCarga", "", 1
            vBanco.Parameters.Add "pDiretorioOrigem", vDiretorioOrigem, 1
            vBanco.Parameters.Add "pDiretorioDestino", vDiretorioDestino, 1
            vBanco.Parameters.Add "pCodAnalista", vCodAnalista, 1

            vBanco.Parameters.Add "pRetorno1", 0, 2
            vBanco.Parameters("pRetorno1").ServerType = 102
            vBanco.Parameters("pRetorno1").DynasetOption = &H2&
            vBanco.Parameters("pRetorno1").DynasetCacheParams 256, 16, 20, 2000, 0

            vBanco.Parameters.Add "pCodErro", 0, 2
            vBanco.Parameters.Add "pMsgErro", "", 2

            vSql = _
                    "PRODUCAO.PCK_CON001.PRAGENDAMENTO(" & _
                    ":pAcao, " & _
                    ":pSequencia, " & _
                    ":pCodLoja, " & _
                    ":pArquivo, " & _
                    ":pDtAgendamento, " & _
                    ":pDtProgramada, " & _
                    ":pDtCarga, " & _
                    ":pDiretorioOrigem, " & _
                    ":pDiretorioDestino, " & _
                    ":pCodAnalista, " & _
                    ":pRetorno1, " & _
                    ":pCodErro, " & _
                    ":pMsgErro)"

            vBanco.ExecuteSQL "Begin " & vSql & "; End;"

            If vBanco.Parameters("pCodErro") <> 0 Then

                Unload frmAguardar

                vErro = "Ocorreu um erro" & Chr(13) & _
                        "C�d. do erro: " & CStr(vBanco.Parameters("pCodErro")) & Chr(13) & _
                        "Msg. de erro: " & CStr(vBanco.Parameters("pMsgErro"))

                Call vVB_Generica_001.ProcessaErro(vErro)
                Exit Sub

            End If

        Next

        Unload frmAguardar
        Call vVB_Generica_001.Informar("Opera��o realizada com sucesso." & Chr(13) & _
                "ATEN��O, no processo de carga AUTOM�TICA, caso o arquivo destino esteja em uso, " & Chr(13) & _
                "o mesmo ser� liberado automaticamente pelo sistema, para que a carga seja realizada.")
        Call cmdCancelar_Click
        Call cmdAtualizarCargas_Click

    End If

    Exit Sub

Erro:

    Unload frmAguardar

    vErro = "Ocorreu um erro" & Chr(13) & _
            "C�d. do erro: " & Err.Number & Chr(13) & _
            "Msg. de erro: " & Err.Description

    Call vVB_Generica_001.ProcessaErro(vErro)

End Sub
Private Sub cmdPadrao_Click()

    txtDiretorioDestino = "Z:\ORACLE\SISTEMAS\VB\32BITS\"

End Sub
Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()

    On Error GoTo Erro

    Me.Top = 0
    Me.Left = 0

    Set vObjOracle = vVB_Generica_001.TabelaLoja(vBanco)
    lstDepositos.AddItem "TODOS"
    While Not vObjOracle.EOF

        lstDepositos.AddItem vObjOracle.Fields(0)
        vObjOracle.MoveNext

    Wend

    lstArquivosOrigem.Columns.Add , , "ARQUIVO", , 3100
    lstArquivosOrigem.Columns.Add , , "TAM (KB)", , 900
    lstArquivosOrigem.Columns(2).Alignment = eLVColumnAlignRight
        
    lstCargas.Columns.Add , , "DEP�SITO", , 2000
    lstCargas.Columns.Add , , "ARQUIVO", , 2000
    lstCargas.Columns.Add , , "TAMANHO (KB)", , 1400
    lstCargas.Columns.Add , , "DT. PROGRAMADA", , 2000
    lstCargas.Columns.Add , , "ORIGEM (SERVIDOR)", , 5200
    lstCargas.Columns.Add , , "DESTINO (SERVIDOR)", , 5200
    lstCargas.Columns.Add , , "ANALISTA", , 3000
    lstCargas.Columns(3).Alignment = eLVColumnAlignRight

    lstCargasAgendadas.Columns.Add , , "ID", , 800
    lstCargasAgendadas.Columns.Add , , "DEP�SITO", , 2000
    lstCargasAgendadas.Columns.Add , , "ARQUIVO", , 2000
    lstCargasAgendadas.Columns.Add , , "DT. AGENDAMENTO", , 2000
    lstCargasAgendadas.Columns.Add , , "ORIGEM", , 5200
    lstCargasAgendadas.Columns.Add , , "DESTINO", , 5200
    lstCargasAgendadas.Columns.Add , , "ANALISTA", , 3000
    lstCargasAgendadas.Columns(1).Alignment = eLVColumnAlignCenter

    lstArquivosOrigemOld.Path = lblOrigem
    txtDataCarga.Value = Format(Now + 1, "DD/MM/YYYY")
    txtDataParametro.Value = Format(Now + 1, "DD/MM/YYYY")
    
    If lstArquivosOrigemOld.ListCount > 0 Then
    
        Dim vItem As vbalListViewLib6.cListItem

        For i = 0 To lstArquivosOrigemOld.ListCount - 1
        
            Set vItem = lstArquivosOrigem.ListItems.Add(, , UCase(lstArquivosOrigemOld.List(i)))
            vItem.SubItems(1).Caption = CStr(fTamanhoArquivo(lblOrigem & lstArquivosOrigemOld.List(i)))
        
        Next
    
        lstArquivosOrigemOld.Selected(0) = True
        
    End If
    
    Call vVB_Generica_001.ExcluiBind(vBanco)

    vBanco.Parameters.Add "pAcao", 5, 1
    vBanco.Parameters.Add "pUsuarioRede", "", 1
    vBanco.Parameters.Add "pDireito", "", 1

    vBanco.Parameters.Add "pRetorno1", 0, 2
    vBanco.Parameters("pRetorno1").ServerType = 102
    vBanco.Parameters("pRetorno1").DynasetOption = &H2&
    vBanco.Parameters("pRetorno1").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Add "pCodErro", 0, 2
    vBanco.Parameters.Add "pMsgErro", "", 2

    vSql = _
            "PRODUCAO.PCK_CON001.PRPROCEDURESGERAIS(" & _
            ":pAcao, " & _
            ":pUsuarioRede, " & _
            ":pDireito, " & _
            ":pRetorno1, " & _
            ":pCodErro, " & _
            ":pMsgErro)"

    Call Aguardar

    vBanco.ExecuteSQL "Begin " & vSql & "; End;"

    Unload frmAguardar

    If vBanco.Parameters("pCodErro") <> 0 Then

        vErro = "Ocorreu um erro" & Chr(13) & _
                "C�d. do erro: " & CStr(vBanco.Parameters("pCodErro")) & Chr(13) & _
                "Msg. de erro: " & CStr(vBanco.Parameters("pMsgErro"))

        Call vVB_Generica_001.ProcessaErro(vErro)
        Exit Sub

    End If

    Set vObjOracle = vBanco.Parameters("pRetorno1").Value

    If Not vObjOracle.EOF Then

        Call vVB_Generica_001.PreencheComboList(vObjOracle, cmbAnalista, vObjOracle.Fields(0).Name)

    End If

    Call CarregarAgendamentos_HOJE_Agenda(Format(Now + 1, "DD/MM/YYYY"))

    Exit Sub

Erro:

    Unload frmAguardar

    vErro = "Ocorreu um erro" & Chr(13) & _
            "C�d. do erro: " & Err.Number & Chr(13) & _
            "Msg. de erro: " & Err.Description

    Call vVB_Generica_001.ProcessaErro(vErro)

End Sub
Private Sub lstCargas_ItemDblClick(Item As vbalListViewLib6.cListItem)

    lstCargas.ListItems.Remove (lstCargas.SelectedItem.Index)

End Sub
Private Sub lstCargasAgendadas_ItemDblClick(Item As vbalListViewLib6.cListItem)

    On Error GoTo Erro

    If vVB_Generica_001.Perguntar("Confirma exclus�o da carga selecionada?") = 6 Then

        Call vVB_Generica_001.ExcluiBind(vBanco)

        vBanco.Parameters.Add "pAcao", 3, 1
        vBanco.Parameters.Add "pSequencia", Val(lstCargasAgendadas.SelectedItem), 1
        vBanco.Parameters.Add "pCodLoja", 0, 1
        vBanco.Parameters.Add "pArquivo", "", 1
        vBanco.Parameters.Add "pDtAgendamento", "", 1
        vBanco.Parameters.Add "pDtProgramada", "", 1
        vBanco.Parameters.Add "pDtCarga", "", 1
        vBanco.Parameters.Add "pDiretorioOrigem", 1, 1
        vBanco.Parameters.Add "pDiretorioDestino", 1, 1
        vBanco.Parameters.Add "pCodAnalista", 1, 1

        vBanco.Parameters.Add "pRetorno1", 0, 2
        vBanco.Parameters("pRetorno1").ServerType = 102
        vBanco.Parameters("pRetorno1").DynasetOption = &H2&
        vBanco.Parameters("pRetorno1").DynasetCacheParams 256, 16, 20, 2000, 0

        vBanco.Parameters.Add "pCodErro", 0, 2
        vBanco.Parameters.Add "pMsgErro", "", 2

        vSql = _
                "PRODUCAO.PCK_CON001.PRAGENDAMENTO(" & _
                ":pAcao, " & _
                ":pSequencia, " & _
                ":pCodLoja, " & _
                ":pArquivo, " & _
                ":pDtAgendamento, " & _
                ":pDtProgramada, " & _
                ":pDtCarga, " & _
                ":pDiretorioOrigem, " & _
                ":pDiretorioDestino, " & _
                ":pCodAnalista, " & _
                ":pRetorno1, " & _
                ":pCodErro, " & _
                ":pMsgErro)"

        Call Aguardar

        vBanco.ExecuteSQL "Begin " & vSql & "; End;"

        Unload frmAguardar

        If vBanco.Parameters("pCodErro") <> 0 Then

            vErro = "Ocorreu um erro" & Chr(13) & _
                    "C�d. do erro: " & CStr(vBanco.Parameters("pCodErro")) & Chr(13) & _
                    "Msg. de erro: " & CStr(vBanco.Parameters("pMsgErro"))

            Call vVB_Generica_001.ProcessaErro(vErro)
            Exit Sub

        End If

        Call vVB_Generica_001.Informar("Opera��o realizada com sucesso.")
        Call cmdAtualizarCargas_Click

    End If

    Exit Sub

Erro:

    Unload frmAguardar

    vErro = "Ocorreu um erro" & Chr(13) & _
            "C�d. do erro: " & Err.Number & Chr(13) & _
            "Msg. de erro: " & Err.Description

    Call vVB_Generica_001.ProcessaErro(vErro)

End Sub
Private Sub lstDepositos_ItemCheck(Item As Integer)

    If Item = 0 Then

        For i = 1 To lstDepositos.ListCount - 1

            lstDepositos.Selected(i) = lstDepositos.Selected(0)

        Next

    End If

End Sub

Private Sub txtDataCarga_Change()

    If CDate(txtDataCarga.Value) <= CDate(Format(Now, "dd/mm/yyyy")) Then

        Call vVB_Generica_001.Informar("N�o � poss�vel fazer agendamento para hoje ou para dias anteriores.")
        txtDataCarga.Value = Format(Now + 1, "dd/mm/yyyy")
        Exit Sub

    End If

End Sub
Private Sub txtDataParametro_Change()

    Call CarregarAgendamentos_HOJE_Agenda(txtDataParametro)

End Sub
Private Sub txtDiretorioDestino_GotFocus()

    Call vVB_Generica_001.SelecionaCampo(txtDiretorioDestino)

End Sub
Private Sub txtDiretorioDestino_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)

End Sub
