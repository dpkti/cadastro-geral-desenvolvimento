VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.MDIForm mdiCon001 
   Appearance      =   0  'Flat
   BackColor       =   &H8000000F&
   ClientHeight    =   11115
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   15240
   Icon            =   "mdiCon001.frx":0000
   LinkTopic       =   "MDIForm1"
   LockControls    =   -1  'True
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin Threed.SSPanel SSPanel1 
      Align           =   1  'Align Top
      Height          =   1230
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   15240
      _Version        =   65536
      _ExtentX        =   26882
      _ExtentY        =   2170
      _StockProps     =   15
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BevelWidth      =   0
      BorderWidth     =   0
      BevelOuter      =   0
      Begin Bot�o.cmd cmdSobre 
         Height          =   1005
         Left            =   8325
         TabIndex        =   0
         TabStop         =   0   'False
         ToolTipText     =   "Sobre"
         Top             =   45
         Width           =   825
         _ExtentX        =   1773
         _ExtentY        =   1773
         BTYPE           =   3
         TX              =   "Sobre"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiCon001.frx":23D2
         PICN            =   "mdiCon001.frx":23EE
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdSair 
         Height          =   1005
         Left            =   14535
         TabIndex        =   8
         TabStop         =   0   'False
         ToolTipText     =   "Sair do sistema"
         Top             =   45
         Width           =   735
         _ExtentX        =   1773
         _ExtentY        =   1773
         BTYPE           =   3
         TX              =   "Sair"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiCon001.frx":30C8
         PICN            =   "mdiCon001.frx":30E4
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin pkGradientControl.pkGradient pkGradient1 
         Height          =   30
         Left            =   45
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   1125
         Width           =   15180
         _ExtentX        =   26776
         _ExtentY        =   53
         Color1          =   0
         Color2          =   -2147483633
         BackColor       =   -2147483633
      End
      Begin Bot�o.cmd cmdJanelas 
         Height          =   1005
         Left            =   7425
         TabIndex        =   7
         TabStop         =   0   'False
         ToolTipText     =   "Clique aqui para alternar entre janelas"
         Top             =   45
         Width           =   870
         _ExtentX        =   1535
         _ExtentY        =   1773
         BTYPE           =   3
         TX              =   "Janelas"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiCon001.frx":3DBE
         PICN            =   "mdiCon001.frx":3DDA
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdCadastro 
         Height          =   1005
         Left            =   1350
         TabIndex        =   2
         TabStop         =   0   'False
         ToolTipText     =   "Cadastro das informa��es do servidor dos dep�sitos"
         Top             =   45
         Width           =   1005
         _ExtentX        =   1773
         _ExtentY        =   1773
         BTYPE           =   3
         TX              =   "Cadastro"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiCon001.frx":4AB4
         PICN            =   "mdiCon001.frx":4AD0
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdManual 
         Height          =   1005
         Left            =   2385
         TabIndex        =   3
         TabStop         =   0   'False
         ToolTipText     =   "Fazer carga MANUAL"
         Top             =   45
         Width           =   870
         _ExtentX        =   1535
         _ExtentY        =   1773
         BTYPE           =   3
         TX              =   "Manual"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiCon001.frx":57AA
         PICN            =   "mdiCon001.frx":57C6
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdAgenda 
         Height          =   1005
         Left            =   3285
         TabIndex        =   4
         TabStop         =   0   'False
         ToolTipText     =   "Agendamento de cargas"
         Top             =   45
         Width           =   915
         _ExtentX        =   1614
         _ExtentY        =   1773
         BTYPE           =   3
         TX              =   "Agenda"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiCon001.frx":64A0
         PICN            =   "mdiCon001.frx":64BC
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdAutomatica 
         Height          =   1005
         Left            =   4230
         TabIndex        =   5
         TabStop         =   0   'False
         ToolTipText     =   "Realizar carga AUTOM�TICA"
         Top             =   45
         Width           =   1275
         _ExtentX        =   2249
         _ExtentY        =   1773
         BTYPE           =   3
         TX              =   "Autom�tica"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiCon001.frx":7196
         PICN            =   "mdiCon001.frx":71B2
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdAcompanhamento 
         Height          =   1005
         Left            =   5535
         TabIndex        =   6
         TabStop         =   0   'False
         ToolTipText     =   "Acompanhamento das cargas autom�ticas e agendamentos"
         Top             =   45
         Width           =   1860
         _ExtentX        =   3281
         _ExtentY        =   1773
         BTYPE           =   3
         TX              =   "Acompanhamento"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiCon001.frx":7E8C
         PICN            =   "mdiCon001.frx":7EA8
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Image Image1 
         Height          =   1185
         Left            =   -90
         Picture         =   "mdiCon001.frx":8B82
         Stretch         =   -1  'True
         Top             =   -45
         Width           =   1455
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   10
      Top             =   10785
      Width           =   15240
      _ExtentX        =   26882
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   13732
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   2646
            MinWidth        =   2646
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   2381
            MinWidth        =   2381
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   3528
            MinWidth        =   3528
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            Alignment       =   1
            Object.Width           =   2381
            MinWidth        =   2381
            TextSave        =   "07/04/06"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   1
            Object.Width           =   1587
            MinWidth        =   1587
            TextSave        =   "15:53"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Menu mnuJanelas 
      Caption         =   "Janelas"
      Visible         =   0   'False
      WindowList      =   -1  'True
   End
   Begin VB.Menu RCPopup 
      Caption         =   "Menu"
      Visible         =   0   'False
      Begin VB.Menu rest1 
         Caption         =   "Restaurar"
      End
   End
End
Attribute VB_Name = "mdiCon001"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Sub cmdAcompanhamento_Click()

    frmAcompanhamento.Show

End Sub
Private Sub cmdAgenda_Click()

    frmAgendamento.Show

End Sub
Private Sub cmdAutomatica_Click()

    frmCargaAutomatica.Show

End Sub
Private Sub cmdCadastro_Click()

    frmCadastro.Show

End Sub
Private Sub cmdJanelas_Click()

    Me.PopupMenu mnuJanelas

End Sub

Private Sub cmdManual_Click()

    frmCargaManual.Show

End Sub
Private Sub cmdSair_Click()

    If vVB_Generica_001.Sair = 6 Then

        Call Desconectar

        If Not vBanco Is Nothing Then

            Call vVB_Generica_001.ExcluiBind(vBanco)

        End If

        Set vObjOracle = Nothing
        Set vBanco = Nothing
        Set vSessao = Nothing

        End

    End If

End Sub
Private Sub cmdSobre_Click()

    frmSobre.Show 1

End Sub
Private Sub MDIForm_Load()

    On Error GoTo Erro

    theNetResource.lpLocalName = "Z:"

    Me.Top = 0
    Me.Left = 0

    If App.PrevInstance = True Then

        Call vVB_Generica_001.Informar("J� existe uma inst�ncia desse sistema no ar.")
        End

    End If

    frmLogo.Show 1
    
    vErro = vVB_Generica_001.ConectaOracle("PRODUCAO", "CON001", True, Me)

    If vErro <> "" Then

        End

    End If

    vCd = vVB_Generica_001.vCd
    vTipoCD = vVB_Generica_001.vTipoCD
    Set vSessao = vVB_Generica_001.vSessao
    Set vBanco = vVB_Generica_001.vBanco
    
    Set vObjOracle = vVB_Generica_001.InformacoesSistema(vBanco, UCase(App.Title))

    If vObjOracle.EOF Then

        Call vVB_Generica_001.Informar("Sistema n�o cadastrado. � necess�rio fazer o cadastro para executar o programa.")
        End

    End If

    Call DefinirTelaSobre

    Me.Caption = "::: " & UCase(App.Title) & " - " & CStr(vObjOracle.Fields(0)) & "          VERS�O: " & App.Major & "." & App.Minor & "." & App.Revision & " :::"

    If UCase(Command$) = "CPD" Then

        cmdCadastro.Enabled = False
        cmdManual.Enabled = False
        cmdAgenda.Enabled = False
        cmdAutomatica.Enabled = True
        cmdAcompanhamento.Enabled = False
        cmdJanelas.Enabled = False

        frmCargaAutomatica.Show

    Else

        Call ConfigurarDireitos

    End If

    Call Desconectar

    Exit Sub

Erro:

    Unload frmAguardar

    vErro = "Ocorreu um erro" & Chr(13) & _
            "C�d. do erro: " & Err.Number & Chr(13) & _
            "Msg. de erro: " & Err.Description

    Call vVB_Generica_001.ProcessaErro(vErro)

End Sub
