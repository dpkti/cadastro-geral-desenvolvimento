Attribute VB_Name = "Module1"
Option Explicit
Public vNovo As Boolean
Public vComando As String
Public vDiretorioOrigem As String
Public vDiretorioDestino As String
Public vArquivoOrigem As String
Public vArquivoDestino As String
Public theNetResource As NETRESOURCE
Public UserName As String
Public UserPassword As String

Public Const NORMAL_PRIORITY_CLASS = &H20&
Public Const INFINITE = -1&
Public Const TEMPO_ESPERA = 2000
Public Const VCONSTANTE = &H1
Public Const HWND_TOPMOST = -1
Public Const SWP_NOMOVE = &H2
Public Const SWP_NOSIZE = &H1
Public Const TOPMOST_FLAGS = SWP_NOMOVE Or SWP_NOSIZE
Public Const RESOURCETYPE_DISK = &H1

Public Type STARTUPINFO
    cb As Long
    lpReserved As String
    lpDesktop As String
    lpTitle As String
    dwX As Long
    dwY As Long
    dwXSize As Long
    dwYSize As Long
    dwXCountChars As Long
    dwYCountChars As Long
    dwFillAttribute As Long
    dwFlags As Long
    wShowWindow As Integer
    cbReserved2 As Integer
    lpReserved2 As Long
    hStdInput As Long
    hStdOutput As Long
    hStdError As Long
End Type
Public Type PROCESS_INFORMATION
    hProcess As Long
    hThread As Long
    dwProcessID As Long
    dwThreadID As Long
End Type
Public Type NETRESOURCE
    dwScope As Long
    dwType As Long
    dwDisplayType As Long
    dwUsage As Long
    lpLocalName As String
    lpRemoteName As String
    lpComment As String
    lpProvider As String
End Type

Public Declare Function CreateProcessA Lib "kernel32" (ByVal _
        lpApplicationName As String, ByVal lpCommandLine As String, ByVal _
        lpProcessAttributes As Long, ByVal lpThreadAttributes As Long, _
        ByVal bInheritHandles As Long, ByVal dwCreationFlags As Long, _
        ByVal lpEnvironment As Long, ByVal lpCurrentDirectory As String, _
        lpStartupInfo As STARTUPINFO, lpProcessInformation As _
        PROCESS_INFORMATION) As Long
Public Declare Function WaitForSingleObject Lib "kernel32" (ByVal _
        hHandle As Long, ByVal dwMilliseconds As Long) As Long
Public Declare Function CloseHandle Lib "kernel32" _
        (ByVal hObject As Long) As Long
Public Declare Function GetExitCodeProcess Lib "kernel32" _
        (ByVal hProcess As Long, lpExitCode As Long) As Long
Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)
Public Declare Function lOpen Lib "kernel32" Alias "_lopen" (ByVal lpPathName As String, ByVal iReadWrite As Long) As Long
Public Declare Function lClose Lib "kernel32" Alias "_lclose" (ByVal hFile As Long) As Long
Public Declare Function SetWindowPos Lib "user32" (ByVal hwnd As Long, ByVal hWndInsertAfter As Long, ByVal x As Long, y, ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long) As Long
Public Declare Function WNetAddConnection2 Lib "mpr.dll" Alias "WNetAddConnection2A" (lpNetResource As NETRESOURCE, ByVal lpPassword As String, ByVal lpUserName As String, ByVal dwFlags As Long) As Long
Public Declare Function WNetCancelConnection2 Lib "mpr.dll" Alias "WNetCancelConnection2A" (ByVal lpName As String, ByVal dwFlags As Long, ByVal fForce As Long) As Long
Public Sub MakeTopMost(Handle As Long)

    SetWindowPos Handle, HWND_TOPMOST, 0, 0, 0, 0, TOPMOST_FLAGS

End Sub
Sub ConfigurarDireitos(Optional pOrigem As String)

    On Error GoTo Erro

    mdiCon001.cmdCadastro.Enabled = fVerificarDireito("CADASTRO")
    mdiCon001.cmdManual.Enabled = fVerificarDireito("MANUAL")
    mdiCon001.cmdAgenda.Enabled = fVerificarDireito("AGENDAMENTO")
    mdiCon001.cmdAutomatica.Enabled = fVerificarDireito("AUTOMATICA")
    mdiCon001.cmdAcompanhamento.Enabled = fVerificarDireito("ACOMPANHAMENTO")

    Exit Sub

Erro:

    Unload frmAguardar

    vErro = "Ocorreu um erro" & Chr(13) & _
            "C�d. do erro: " & Err.Number & Chr(13) & _
            "Msg. de erro: " & Err.Description

    Call vVB_Generica_001.ProcessaErro(vErro)

End Sub
Sub FecharArquivo(pIP As String, pUsuario As String, pSenha As String, pDiretorio As String, pArquivo As String)

    'COPIAR PSKILL PARA C:\WINDOWS
    'COPIAR PSFILE PARA C:\WINDOWS

    Open "C:\FECHAR_ARQUIVO.BAT" For Output As #1

    Print #1, "@ECHO OFF"

    Dim vExtensao As String
    Dim vPonto As Integer
    vPonto = InStr(1, pArquivo, ".")

    If vPonto > 0 Then

        vExtensao = UCase(Mid(pArquivo, vPonto + 1))
        Print #1, "PSKILL " & Replace(UCase(pArquivo), "." & vExtensao, "")
        Print #1, "PSFILE " & pDiretorio & pArquivo & " -C"
        Print #1, "PSKILL \\" & pIP & " -U " & pUsuario & " -P " & pSenha & " " & Replace(UCase(pArquivo), "." & vExtensao, "")
        Print #1, "PSFILE \\" & pIP & " -U " & pUsuario & " -P " & pSenha & " " & pDiretorio & pArquivo & " -C"

    Else

        vExtensao = ""

        Print #1, "PSKILL " & Replace(UCase(pArquivo), ".EXE", "")
        Print #1, "PSFILE " & pDiretorio & pArquivo & " -C"
        Print #1, "PSKILL \\" & pIP & " -U " & pUsuario & " -P " & pSenha & " " & Replace(UCase(pArquivo), ".EXE", "")
        Print #1, "PSFILE \\" & pIP & " -U " & pUsuario & " -P " & pSenha & " " & pDiretorio & pArquivo & " -C"

    End If

    Close #1

    Dim vShell As String
    vShell = Shell("C:\FECHAR_ARQUIVO.BAT", vbHide)
    Call ExecutarEsperar("C:\FECHAR_ARQUIVO.BAT")
    Call Sleep(2000)

    Kill "C:\FECHAR_ARQUIVO.BAT"

End Sub
Function fVerificarDireito( _
            pDireito As String _
            ) As Double

    On Error GoTo Erro

    Call vVB_Generica_001.ExcluiBind(vBanco)

    vBanco.Parameters.Add "pAcao", 4, 1
    vBanco.Parameters.Add "pUsuarioRede", UCase(vVB_Generica_001.NomeUsuarioRede), 1
    vBanco.Parameters.Add "pDireito", UCase(pDireito), 1

    vBanco.Parameters.Add "pRetorno1", 0, 2
    vBanco.Parameters("pRetorno1").ServerType = 102
    vBanco.Parameters("pRetorno1").DynasetOption = &H2&
    vBanco.Parameters("pRetorno1").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Add "pCodErro", 0, 2
    vBanco.Parameters.Add "pMsgErro", "", 2

    vSql = _
            "PRODUCAO.PCK_CON001.PRPROCEDURESGERAIS(" & _
            ":pAcao, " & _
            ":pUsuarioRede, " & _
            ":pDireito, " & _
            ":pRetorno1, " & _
            ":pCodErro, " & _
            ":pMsgErro)"

    vBanco.ExecuteSQL "Begin " & vSql & "; End;"

    If vBanco.Parameters("pCodErro") <> 0 Then

        vErro = "Ocorreu um erro" & Chr(13) & _
                "C�d. do erro: " & CStr(vBanco.Parameters("pCodErro")) & Chr(13) & _
                "Msg. de erro: " & CStr(vBanco.Parameters("pMsgErro"))

        Call vVB_Generica_001.ProcessaErro(vErro)
        Exit Function

    End If

    Set vObjOracle = vBanco.Parameters("pRetorno1").Value

    fVerificarDireito = vObjOracle.RecordCount

    Exit Function

Erro:

    Unload frmAguardar

    vErro = "Ocorreu um erro" & Chr(13) & _
            "C�d. do erro: " & Err.Number & Chr(13) & _
            "Msg. de erro: " & Err.Description

    Call vVB_Generica_001.ProcessaErro(vErro)

    End

End Function
Sub IncluirParametros( _
            vParametro As String, _
            vUsuario As String)

    On Error GoTo Erro

    Call vVB_Generica_001.ExcluiBind(vBanco)

    vBanco.Parameters.Add "pAcao", 3, 1
    vBanco.Parameters.Add "pUsuarioRede", UCase(vUsuario), 1
    vBanco.Parameters.Add "pDireito", vParametro, 1

    vBanco.Parameters.Add "pRetorno1", 0, 2
    vBanco.Parameters("pRetorno1").ServerType = 102
    vBanco.Parameters("pRetorno1").DynasetOption = &H2&
    vBanco.Parameters("pRetorno1").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Add "pCodErro", 0, 2
    vBanco.Parameters.Add "pMsgErro", "", 2

    vSql = _
            "PRODUCAO.PCK_CON001.PRPROCEDURESGERAIS(" & _
            ":pAcao, " & _
            ":pUsuarioRede, " & _
            ":pDireito, " & _
            ":pRetorno1, " & _
            ":pCodErro, " & _
            ":pMsgErro)"

    vBanco.ExecuteSQL "Begin " & vSql & "; End;"

    If vBanco.Parameters("pCodErro") <> 0 Then

        vErro = "Ocorreu um erro" & Chr(13) & _
                "C�d. do erro: " & CStr(vBanco.Parameters("pCodErro")) & Chr(13) & _
                "Msg. de erro: " & CStr(vBanco.Parameters("pMsgErro"))

        Call vVB_Generica_001.ProcessaErro(vErro)
        Exit Sub

    End If

    Exit Sub

Erro:

    Unload frmAguardar

    vErro = "Ocorreu um erro" & Chr(13) & _
            "C�d. do erro: " & Err.Number & Chr(13) & _
            "Msg. de erro: " & Err.Description

    Call vVB_Generica_001.ProcessaErro(vErro)

End Sub
Sub CarregarAgendamentos_TODOS()

    On Error GoTo Erro

    frmAgendamento.fraCargasAgendadas.Visible = False

    Call vVB_Generica_001.ExcluiBind(vBanco)

    vBanco.Parameters.Add "pAcao", 1, 1
    vBanco.Parameters.Add "pSequencia", 0, 1
    vBanco.Parameters.Add "pCodLoja", 0, 1
    vBanco.Parameters.Add "pArquivo", "", 1
    vBanco.Parameters.Add "pDtAgendamento", "", 1
    vBanco.Parameters.Add "pDtProgramada", "", 1
    vBanco.Parameters.Add "pDtCarga", "", 1
    vBanco.Parameters.Add "pDiretorioOrigem", 1, 1
    vBanco.Parameters.Add "pDiretorioDestino", 1, 1
    vBanco.Parameters.Add "pCodAnalista", 1, 1

    vBanco.Parameters.Add "pRetorno1", 0, 2
    vBanco.Parameters("pRetorno1").ServerType = 102
    vBanco.Parameters("pRetorno1").DynasetOption = &H2&
    vBanco.Parameters("pRetorno1").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Add "pCodErro", 0, 2
    vBanco.Parameters.Add "pMsgErro", "", 2

    vSql = _
            "PRODUCAO.PCK_CON001.PRAGENDAMENTO(" & _
            ":pAcao, " & _
            ":pSequencia, " & _
            ":pCodLoja, " & _
            ":pArquivo, " & _
            ":pDtAgendamento, " & _
            ":pDtProgramada, " & _
            ":pDtCarga, " & _
            ":pDiretorioOrigem, " & _
            ":pDiretorioDestino, " & _
            ":pCodAnalista, " & _
            ":pRetorno1, " & _
            ":pCodErro, " & _
            ":pMsgErro)"

    Call Aguardar

    vBanco.ExecuteSQL "Begin " & vSql & "; End;"

    Unload frmAguardar

    If vBanco.Parameters("pCodErro") <> 0 Then

        vErro = "Ocorreu um erro" & Chr(13) & _
                "C�d. do erro: " & CStr(vBanco.Parameters("pCodErro")) & Chr(13) & _
                "Msg. de erro: " & CStr(vBanco.Parameters("pMsgErro"))

        Call vVB_Generica_001.ProcessaErro(vErro)
        Exit Sub

    End If

    Set vObjOracle = vBanco.Parameters("pRetorno1").Value

    If Not vObjOracle.EOF Then

        Dim vItem As vbalListViewLib6.cListItem

        frmAgendamento.lstCargasAgendadas.ListItems.Clear

        frmAgendamento.txtTamanhoArquivo = 0

        While Not vObjOracle.EOF

            Set vItem = frmAgendamento.lstCargasAgendadas.ListItems.Add(, , vObjOracle.Fields(0))

            For i = 1 To vObjOracle.Fields.Count - 1

                vItem.SubItems(i).Caption = vObjOracle.Fields(CInt(i))

            Next

            frmAgendamento.txtTamanhoArquivo = Val(frmAgendamento.txtTamanhoArquivo) + FileLen(vItem.SubItems(5).Caption & vItem.SubItems(2).Caption)

            vObjOracle.MoveNext

        Wend

        frmAgendamento.txtTamanhoArquivo = Format(frmAgendamento.txtTamanhoArquivo / 1024, "###,###,##0")
        frmAgendamento.fraCargasAgendadas.Visible = True

    End If

    Exit Sub

Erro:

    Unload frmAguardar

    vErro = "Ocorreu um erro" & Chr(13) & _
            "C�d. do erro: " & Err.Number & Chr(13) & _
            "Msg. de erro: " & Err.Description

    Call vVB_Generica_001.ProcessaErro(vErro)

End Sub
Sub CarregarAgendamentos_HOJE()

    On Error GoTo Erro

    frmCargaAutomatica.fraCargas.Visible = False
    frmCargaAutomatica.lblNenhumaCarga.Visible = False
    frmCargaAutomatica.fraInformacoes.Visible = False

    Call vVB_Generica_001.ExcluiBind(vBanco)

    vBanco.Parameters.Add "pAcao", 4, 1
    vBanco.Parameters.Add "pSequencia", 0, 1
    vBanco.Parameters.Add "pCodLoja", 0, 1
    vBanco.Parameters.Add "pArquivo", "", 1
    vBanco.Parameters.Add "pDtAgendamento", "", 1
    vBanco.Parameters.Add "pDtProgramada", "", 1
    vBanco.Parameters.Add "pDtCarga", "", 1
    vBanco.Parameters.Add "pDiretorioOrigem", 1, 1
    vBanco.Parameters.Add "pDiretorioDestino", 1, 1
    vBanco.Parameters.Add "pCodAnalista", 1, 1

    vBanco.Parameters.Add "pRetorno1", 0, 2
    vBanco.Parameters("pRetorno1").ServerType = 102
    vBanco.Parameters("pRetorno1").DynasetOption = &H2&
    vBanco.Parameters("pRetorno1").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Add "pCodErro", 0, 2
    vBanco.Parameters.Add "pMsgErro", "", 2

    vSql = _
            "PRODUCAO.PCK_CON001.PRAGENDAMENTO(" & _
            ":pAcao, " & _
            ":pSequencia, " & _
            ":pCodLoja, " & _
            ":pArquivo, " & _
            ":pDtAgendamento, " & _
            ":pDtProgramada, " & _
            ":pDtCarga, " & _
            ":pDiretorioOrigem, " & _
            ":pDiretorioDestino, " & _
            ":pCodAnalista, " & _
            ":pRetorno1, " & _
            ":pCodErro, " & _
            ":pMsgErro)"

    vBanco.ExecuteSQL "Begin " & vSql & "; End;"

    If vBanco.Parameters("pCodErro") <> 0 Then

        vErro = "Ocorreu um erro" & Chr(13) & _
                "C�d. do erro: " & CStr(vBanco.Parameters("pCodErro")) & Chr(13) & _
                "Msg. de erro: " & CStr(vBanco.Parameters("pMsgErro"))

        Call vVB_Generica_001.ProcessaErro(vErro)
        Exit Sub

    End If

    Set vObjOracle = vBanco.Parameters("pRetorno1").Value

    If Not vObjOracle.EOF Then

        Dim vItem As vbalListViewLib6.cListItem

        frmCargaAutomatica.lstCargasAgendadas.ListItems.Clear

        frmCargaAutomatica.txtTamanhoArquivo = 0

        While Not vObjOracle.EOF

            Set vItem = frmCargaAutomatica.lstCargasAgendadas.ListItems.Add(, , vObjOracle.Fields(0))

            For i = 1 To vObjOracle.Fields.Count - 1

                vItem.SubItems(i).Caption = vObjOracle.Fields(CInt(i))

            Next

            If fVerificarSeArquivoExiste(vItem.SubItems(4).Caption & vItem.SubItems(2).Caption) = True Then

                frmCargaAutomatica.txtTamanhoArquivo = Val(frmCargaAutomatica.txtTamanhoArquivo) + FileLen(vItem.SubItems(4).Caption & vItem.SubItems(2).Caption)

            Else

                frmCargaAutomatica.txtTamanhoArquivo = Val(frmCargaAutomatica.txtTamanhoArquivo) + 0

            End If

            vObjOracle.MoveNext

        Wend

        frmCargaAutomatica.txtTamanhoArquivo = Format(frmCargaAutomatica.txtTamanhoArquivo / 1024, "###,###,##0")
        frmCargaAutomatica.fraCargas.Visible = True
        frmCargaAutomatica.fraInformacoes.Visible = True
        frmCargaAutomatica.Refresh

    Else

        frmCargaAutomatica.lblNenhumaCarga.Visible = True

        Exit Sub

    End If

    Exit Sub

Erro:

    vErro = "Ocorreu um erro" & Chr(13) & _
            "C�d. do erro: " & Err.Number & Chr(13) & _
            "Msg. de erro: " & Err.Description

    Call vVB_Generica_001.ProcessaErro(vErro)

End Sub
Sub CarregarAgendamentos_HOJE_Agenda(Optional pData As String)

    On Error GoTo Erro

    frmAgendamento.fraCargasAgendadas.Visible = False

    Call vVB_Generica_001.ExcluiBind(vBanco)

    vBanco.Parameters.Add "pAcao", 4, 1
    vBanco.Parameters.Add "pSequencia", 0, 1
    vBanco.Parameters.Add "pCodLoja", 0, 1
    vBanco.Parameters.Add "pArquivo", "", 1
    vBanco.Parameters.Add "pDtAgendamento", "", 1
    vBanco.Parameters.Add "pDtProgramada", pData, 1
    vBanco.Parameters.Add "pDtCarga", "", 1
    vBanco.Parameters.Add "pDiretorioOrigem", 1, 1
    vBanco.Parameters.Add "pDiretorioDestino", 1, 1
    vBanco.Parameters.Add "pCodAnalista", 1, 1

    vBanco.Parameters.Add "pRetorno1", 0, 2
    vBanco.Parameters("pRetorno1").ServerType = 102
    vBanco.Parameters("pRetorno1").DynasetOption = &H2&
    vBanco.Parameters("pRetorno1").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Add "pCodErro", 0, 2
    vBanco.Parameters.Add "pMsgErro", "", 2

    vSql = _
            "PRODUCAO.PCK_CON001.PRAGENDAMENTO(" & _
            ":pAcao, " & _
            ":pSequencia, " & _
            ":pCodLoja, " & _
            ":pArquivo, " & _
            ":pDtAgendamento, " & _
            ":pDtProgramada, " & _
            ":pDtCarga, " & _
            ":pDiretorioOrigem, " & _
            ":pDiretorioDestino, " & _
            ":pCodAnalista, " & _
            ":pRetorno1, " & _
            ":pCodErro, " & _
            ":pMsgErro)"

    Call Aguardar

    vBanco.ExecuteSQL "Begin " & vSql & "; End;"

    Unload frmAguardar

    If vBanco.Parameters("pCodErro") <> 0 Then

        vErro = "Ocorreu um erro" & Chr(13) & _
                "C�d. do erro: " & CStr(vBanco.Parameters("pCodErro")) & Chr(13) & _
                "Msg. de erro: " & CStr(vBanco.Parameters("pMsgErro"))

        Call vVB_Generica_001.ProcessaErro(vErro)
        Exit Sub

    End If

    Set vObjOracle = vBanco.Parameters("pRetorno1").Value

    If Not vObjOracle.EOF Then

        Dim vItem As vbalListViewLib6.cListItem

        frmAgendamento.lstCargasAgendadas.ListItems.Clear

        frmAgendamento.txtTamanhoArquivo = 0

        While Not vObjOracle.EOF

            Set vItem = frmAgendamento.lstCargasAgendadas.ListItems.Add(, , vObjOracle.Fields(0))

            For i = 1 To vObjOracle.Fields.Count - 1

                vItem.SubItems(i).Caption = vObjOracle.Fields(CInt(i))

            Next

            frmAgendamento.txtTamanhoArquivo = Val(frmAgendamento.txtTamanhoArquivo) + FileLen(Replace(vItem.SubItems(4).Caption & vItem.SubItems(2).Caption, "H:", "P:"))

            vObjOracle.MoveNext

        Wend

        frmAgendamento.txtTamanhoArquivo = Format(frmAgendamento.txtTamanhoArquivo / 1024, "###,###,##0")
        frmAgendamento.lstCargasAgendadas.Visible = True
        frmAgendamento.txtTamanhoArquivo.Visible = True
        frmAgendamento.Label3.Visible = True
        frmAgendamento.Label5.Visible = True
        frmAgendamento.fraCargasAgendadas.Visible = True

    Else

        frmAgendamento.fraCargasAgendadas.Visible = True
        frmAgendamento.lstCargasAgendadas.Visible = False
        frmAgendamento.txtTamanhoArquivo.Visible = False
        frmAgendamento.Label3.Visible = False
        frmAgendamento.Label5.Visible = False

    End If

    Exit Sub

Erro:

    Unload frmAguardar

    vErro = "Ocorreu um erro" & Chr(13) & _
            "C�d. do erro: " & Err.Number & Chr(13) & _
            "Msg. de erro: " & Err.Description

    Call vVB_Generica_001.ProcessaErro(vErro)

End Sub
Sub ExcluirArquivos()

    For i = 0 To frmCargaAutomatica.lstArquivos.ListCount - 1

        Call vVB_Generica_001.ExcluiBind(vBanco)

        vBanco.Parameters.Add "pAcao", 2, 1
        vBanco.Parameters.Add "pSequencia", 0, 1
        vBanco.Parameters.Add "pStatus", 0, 1
        vBanco.Parameters.Add "pArquivo", UCase(frmCargaAutomatica.lstArquivos.List(i)), 1
        vBanco.Parameters.Add "pCodAnalista", 0, 1

        vBanco.Parameters.Add "pRetorno1", 0, 2
        vBanco.Parameters("pRetorno1").ServerType = 102
        vBanco.Parameters("pRetorno1").DynasetOption = &H2&
        vBanco.Parameters("pRetorno1").DynasetCacheParams 256, 16, 20, 2000, 0

        vBanco.Parameters.Add "pCodErro", 0, 2
        vBanco.Parameters.Add "pMsgErro", "", 2

        vSql = _
                "PRODUCAO.PCK_CON001.PRCARGAAUTOMATICA(" & _
                ":pAcao, " & _
                ":pSequencia, " & _
                ":pStatus, " & _
                ":pArquivo, " & _
                ":pCodAnalista, " & _
                ":pRetorno1, " & _
                ":pCodErro, " & _
                ":pMsgErro)"

        vBanco.ExecuteSQL "Begin " & vSql & "; End;"

        If vBanco.Parameters("pCodErro") <> 0 Then

            vErro = "Ocorreu um erro" & Chr(13) & _
                    "C�d. do erro: " & CStr(vBanco.Parameters("pCodErro")) & Chr(13) & _
                    "Msg. de erro: " & CStr(vBanco.Parameters("pMsgErro"))

            Call vVB_Generica_001.ProcessaErro(vErro)
            Exit Sub

        End If

        Set vObjOracle = vBanco.Parameters("pRetorno1").Value

        If vObjOracle.EOF Then

            Dim fso As FileSystemObject
            Set fso = New FileSystemObject

            If fso.FileExists("H:\ORACLE\SISTEMAS\CARGAS\" & CStr(frmCargaAutomatica.lstArquivos.List(i))) = True Then

                fso.DeleteFile "H:\ORACLE\SISTEMAS\CARGAS\" & CStr(frmCargaAutomatica.lstArquivos.List(i)), True

            End If

        End If

    Next

End Sub
Function fConectar(pCodLoja As Integer, pRetornarMensagemFalha As Boolean) As Boolean

    Dim vChecaDiretorio
    Dim vIP As String
    Dim vCompartilhamento As String
    Dim vUsuario As String
    Dim vSenha As String

    fConectar = False

    Call vVB_Generica_001.ExcluiBind(vBanco)

    vBanco.Parameters.Add "pAcao", 1, 1
    vBanco.Parameters.Add "pCodLoja", Val(pCodLoja), 1
    vBanco.Parameters.Add "pIP", "", 1
    vBanco.Parameters.Add "pCompartilhamento", "", 1
    vBanco.Parameters.Add "pUsuario", "", 1
    vBanco.Parameters.Add "pSenha", "", 1

    vBanco.Parameters.Add "pRetorno1", 0, 2
    vBanco.Parameters("pRetorno1").ServerType = 102
    vBanco.Parameters("pRetorno1").DynasetOption = &H2&
    vBanco.Parameters("pRetorno1").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Add "pCodErro", 0, 2
    vBanco.Parameters.Add "pMsgErro", "", 2

    vSql = _
            "PRODUCAO.PCK_CON001.PRCADASTRO(" & _
            ":pAcao, " & _
            ":pCodLoja, " & _
            ":pIP, " & _
            ":pCompartilhamento, " & _
            ":pUsuario, " & _
            ":pSenha, " & _
            ":pRetorno1, " & _
            ":pCodErro, " & _
            ":pMsgErro)"

    vBanco.ExecuteSQL "Begin " & vSql & "; End;"

    If vBanco.Parameters("pCodErro") <> 0 Then

        vErro = "Ocorreu um erro" & Chr(13) & _
                "C�d. do erro: " & CStr(vBanco.Parameters("pCodErro")) & Chr(13) & _
                "Msg. de erro: " & CStr(vBanco.Parameters("pMsgErro"))

        Call vVB_Generica_001.ProcessaErro(vErro)
        Exit Function

    End If

    Set vObjOracle = vBanco.Parameters("pRetorno1").Value

    If Not vObjOracle.EOF Then

        vIP = CStr(vObjOracle.Fields(0))
        vCompartilhamento = CStr(vObjOracle.Fields(1))
        vUsuario = CStr(vObjOracle.Fields(2))
        vSenha = CStr(vObjOracle.Fields(3))
        
        theNetResource.lpRemoteName = "\\" & vIP & vCompartilhamento
        UserName = vUsuario
        UserPassword = vSenha
        theNetResource.dwType = RESOURCETYPE_DISK

        Dim Result&
        Result = WNetAddConnection2(theNetResource, UserPassword, UserName, 0)
        
        'vComando = "NET USE /PERSISTENT:NO Z: \\" & vIP & vCompartilhamento & " /USER:" & vUsuario & " " & vSenha
        'ExecutarEsperar vComando
        'Call Sleep(TEMPO_ESPERA)

        vChecaDiretorio = Dir("Z:\", vbDirectory)

        If vChecaDiretorio = "" Then

            If pRetornarMensagemFalha = True Then

                Call vVB_Generica_001.Informar("N�o foi poss�vel conectar no dep�sito com as informa��es digitadas.")

            End If

            Exit Function

        Else

            Open "Z:\ORACLE\DADOS\32BITS\CD.TXT" For Input As #1
            Line Input #1, vLinhaArquivo
            Close #1

            If Val(Mid(vLinhaArquivo, 1, 2)) <> pCodLoja Then

                If pRetornarMensagemFalha = True Then

                    Call vVB_Generica_001.Informar("N�o foi poss�vel conectar no dep�sito com as informa��es digitadas.")

                End If

                Exit Function

            End If

            fConectar = True

        End If
        
    End If

End Function
Sub Desconectar()

    Dim Result&
    Result = WNetCancelConnection2(theNetResource.lpLocalName, 0, 1)

    'vComando = "NET USE Z: /DELETE /Y"
    'ExecutarEsperar vComando
    'Call Sleep(TEMPO_ESPERA)

End Sub
Function fTamanhoArquivo(pArquivo As String) As String

    fTamanhoArquivo = Format(FileLen(pArquivo) / 1024, "###,###,##0")

End Function
Function fValidarCampos(pForm As Form) As Boolean

    Dim vControles As Control
    Dim vTag As String

    fValidarCampos = True

    For i = 0 To pForm.Count - 1

        Set vControles = pForm.Controls(i)

        If vControles.Tag <> "" Then

            If InStr(1, vControles.Tag, ";") > 0 Then

                vTag = Mid(vControles.Tag, 1, InStr(1, vControles.Tag, ";") - 1)

            Else

                vTag = vControles.Tag

            End If

            Select Case UCase(vTag)

                Case "NULO"

                    If Trim(vControles) = "" Then

                        vTag = Mid(vControles.Tag, InStr(1, vControles.Tag, ";") + 1)

                        fValidarCampos = False

                        Call vVB_Generica_001.Informar(vTag)

                        vControles.SetFocus

                        Exit Function

                    End If

            End Select

        End If

    Next

End Function
Public Function ExecutarEsperar(cmdline$)

    Dim proc As PROCESS_INFORMATION
    Dim start As STARTUPINFO
    Dim ret&
    Dim ret2

    start.cb = Len(start)

    ret2 = Shell(cmdline$, vbHide)

    'COM FOCO
    'ret& = CreateProcessA(vbNullString, cmdline$, 0&, 0&, 1&, NORMAL_PRIORITY_CLASS, 0&, vbNullString, start, proc)
    ret& = CreateProcessA(0&, cmdline$, 0&, 0&, 1&, NORMAL_PRIORITY_CLASS, 0&, 0&, start, proc)

    ret& = WaitForSingleObject(proc.hProcess, INFINITE)

    Call GetExitCodeProcess(proc.hProcess, ret&)
    Call CloseHandle(proc.hThread)
    Call CloseHandle(proc.hProcess)

    ExecutarEsperar = ret&

End Function
Function fVerificarSeArquivoExiste(pArquivo As String) As Boolean

    Dim fso As FileSystemObject
    Set fso = New FileSystemObject

    fVerificarSeArquivoExiste = fso.FileExists(pArquivo)

End Function
Function fVerificarSeArquivoEstaAberto(pArquivo As String) As Boolean

    fVerificarSeArquivoEstaAberto = False

    Dim hFile As Long
    Dim lastErr As Long

    hFile = -1
    lastErr = 0

    hFile = lOpen(pArquivo, VCONSTANTE)

    If hFile = -1 Then

        fVerificarSeArquivoEstaAberto = True

    Else

        lClose (hFile)

    End If

End Function
Sub CopiarArquivo(pArquivoOrigem As String, pArquivoDestino As String, pGerarBackup As Boolean)

    On Error GoTo Erro

    Dim fso As FileSystemObject
    Set fso = CreateObject("Scripting.FileSystemObject")

    If pGerarBackup = True Then

        If fso.FileExists(pArquivoDestino) = True Then

            'fso.CopyFile pArquivoDestino, Replace(UCase(pArquivoDestino), ".EXE", ".BKP_" & Format(Now, "DDMMYYYYHHMMSS"))
            Dim vExtensao As String
            Dim vPonto As Integer
            vPonto = InStr(1, pArquivoDestino, ".")

            If vPonto > 0 Then

                vExtensao = UCase(Mid(pArquivoDestino, vPonto + 1))
                Name pArquivoDestino As Replace(UCase(pArquivoDestino), "." & vExtensao, ".BKP_" & Format(Now, "DDMMYYYYHHMMSS"))

            Else

                vExtensao = ""
                Name pArquivoDestino As UCase(pArquivoDestino) & ".BKP_" & Format(Now, "DDMMYYYYHHMMSS")

            End If

        End If

    End If

    If fso.FileExists(pArquivoDestino) = True Then

        fso.DeleteFile pArquivoDestino, True

    End If

    fso.CopyFile pArquivoOrigem, pArquivoDestino, True

    Exit Sub

Erro:

    Exit Sub

End Sub
Sub GeraLog(pForm As Form, pTexto As String, pLimpar As Boolean)

    If pLimpar = True Then

        pForm.lstLog.Clear

    End If

    pForm.lstLog.AddItem CStr(Format(Now, "dd/mm/yyyy hh:mm:ss")) & " -> " & pTexto
    pForm.lstLog.Refresh

    If InStr(1, UCase(pForm.Name), "MDI") = 0 Then

        pForm.Refresh

    End If

End Sub
