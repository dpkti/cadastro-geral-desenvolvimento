VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Object = "{E57FB72C-1106-44AF-9706-0BA099A707C7}#4.2#0"; "XPFrame.ocx"
Object = "{BD5DC045-AFFA-40B8-A887-95734A65FE55}#1.3#0"; "XPCheck.ocx"
Object = "{4E6B00F6-69BE-11D2-885A-A1A33992992C}#2.6#0"; "ACTIVETEXT.OCX"
Begin VB.Form frmParametros 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "::: PAR�METROS :::"
   ClientHeight    =   3840
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4515
   ControlBox      =   0   'False
   Icon            =   "frmParametros.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   256
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   301
   StartUpPosition =   2  'CenterScreen
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   810
      Width           =   4425
      _ExtentX        =   7805
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   3780
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmParametros.frx":0CCA
      PICN            =   "frmParametros.frx":0CE6
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdConsultar 
      Height          =   690
      Left            =   45
      TabIndex        =   8
      TabStop         =   0   'False
      ToolTipText     =   "Consultar os direitos do usu�rio"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmParametros.frx":19C0
      PICN            =   "frmParametros.frx":19DC
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdGravar 
      Height          =   690
      Left            =   765
      TabIndex        =   14
      TabStop         =   0   'False
      ToolTipText     =   "Gravar os direitos do usu�rio"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmParametros.frx":26B6
      PICN            =   "frmParametros.frx":26D2
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin CoolXPFrame.xpFrame fraSenha 
      Height          =   690
      Left            =   45
      Top             =   810
      Width           =   1770
      _ExtentX        =   3122
      _ExtentY        =   1217
      Caption         =   "Senha"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   13977088
      BorderColor     =   8421504
      ColorStyle      =   99
      UseCoolToolTip  =   0   'False
      Begin Bot�o.cmd cmdValidarSenha 
         Height          =   330
         Left            =   1170
         TabIndex        =   3
         TabStop         =   0   'False
         ToolTipText     =   "Voltar"
         Top             =   270
         Width           =   510
         _ExtentX        =   900
         _ExtentY        =   582
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmParametros.frx":33AC
         PICN            =   "frmParametros.frx":33C8
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.TextBox txtSenha 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Wingdings"
            Size            =   9
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00D54600&
         Height          =   330
         IMEMode         =   3  'DISABLE
         Left            =   90
         MaxLength       =   6
         PasswordChar    =   "l"
         TabIndex        =   0
         Top             =   270
         Width           =   1050
      End
   End
   Begin CoolXPFrame.xpFrame fraPar�metros 
      Height          =   2985
      Left            =   45
      Top             =   810
      Width           =   4425
      _ExtentX        =   7805
      _ExtentY        =   5265
      Caption         =   "Par�metros"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   13977088
      BorderColor     =   8421504
      ColorStyle      =   99
      UseCoolToolTip  =   0   'False
      Begin rdActiveText.ActiveText txtUsuarioRede 
         Height          =   330
         Left            =   90
         TabIndex        =   4
         Top             =   945
         Width           =   4245
         _ExtentX        =   7488
         _ExtentY        =   582
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxLength       =   150
         TextCase        =   1
         RawText         =   0
         FontName        =   "Courier New"
         FontSize        =   9
      End
      Begin CoolXPFrame.xpFrame fraDireitos 
         Height          =   1635
         Left            =   90
         Top             =   1260
         Width           =   4245
         _ExtentX        =   7488
         _ExtentY        =   2884
         Caption         =   "Direitos"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   13977088
         BorderColor     =   8421504
         ColorStyle      =   99
         UseCoolToolTip  =   0   'False
         Begin CoolXPCheck.xpCheck chkACOMPANHAMENTO 
            Height          =   240
            Left            =   45
            TabIndex        =   13
            Top             =   1350
            Width           =   1770
            _ExtentX        =   3122
            _ExtentY        =   423
            Caption         =   "Acompanhamento"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin CoolXPCheck.xpCheck chkAUTOMATICA 
            Height          =   240
            Left            =   45
            TabIndex        =   12
            Top             =   1080
            Width           =   1680
            _ExtentX        =   2963
            _ExtentY        =   423
            Caption         =   "Carga autom�tica"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin CoolXPCheck.xpCheck chkAGENDAMENTO 
            Height          =   240
            Left            =   45
            TabIndex        =   11
            Top             =   810
            Width           =   1455
            _ExtentX        =   2566
            _ExtentY        =   423
            Caption         =   "Agendamento"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin CoolXPCheck.xpCheck chkMANUAL 
            Height          =   240
            Left            =   45
            TabIndex        =   10
            Top             =   540
            Width           =   1365
            _ExtentX        =   2408
            _ExtentY        =   423
            Caption         =   "Carga manual"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin CoolXPCheck.xpCheck chkCADASTRO 
            Height          =   240
            Left            =   45
            TabIndex        =   9
            Top             =   270
            Width           =   1005
            _ExtentX        =   1773
            _ExtentY        =   423
            Caption         =   "Cadastro"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Nome do usu�rio (Rede)"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   90
         TabIndex        =   7
         Top             =   675
         Width           =   2010
      End
      Begin VB.Label lblCodSoftware 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00D8E9EC&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "1248"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   330
         Left            =   810
         TabIndex        =   6
         ToolTipText     =   "Nome do fornecedor"
         Top             =   270
         Width           =   645
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Sistema"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   105
         TabIndex        =   5
         Top             =   315
         Width           =   630
      End
   End
End
Attribute VB_Name = "frmParametros"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdConsultar_Click()

    On Error GoTo Erro

    If txtUsuarioRede = "" Then

        Call vVB_Generica_001.Informar("Informe o usu�rio e consulte os direitos do mesmo.")
        Exit Sub

    End If

    chkCADASTRO.Value = cNichtAktiviert
    chkMANUAL.Value = cNichtAktiviert
    chkAGENDAMENTO.Value = cNichtAktiviert
    chkAUTOMATICA.Value = cNichtAktiviert
    chkACOMPANHAMENTO.Value = cNichtAktiviert

    Call vVB_Generica_001.ExcluiBind(vBanco)

    vBanco.Parameters.Add "pAcao", 1, 1
    vBanco.Parameters.Add "pUsuarioRede", txtUsuarioRede, 1
    vBanco.Parameters.Add "pDireito", "", 1

    vBanco.Parameters.Add "pRetorno1", 0, 2
    vBanco.Parameters("pRetorno1").ServerType = 102
    vBanco.Parameters("pRetorno1").DynasetOption = &H2&
    vBanco.Parameters("pRetorno1").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Add "pCodErro", 0, 2
    vBanco.Parameters.Add "pMsgErro", "", 2

    vSql = _
            "PRODUCAO.PCK_CON001.PRPROCEDURESGERAIS(" & _
            ":pAcao, " & _
            ":pUsuarioRede, " & _
            ":pDireito, " & _
            ":pRetorno1, " & _
            ":pCodErro, " & _
            ":pMsgErro)"

    vBanco.ExecuteSQL "Begin " & vSql & "; End;"

    If vBanco.Parameters("pCodErro") <> 0 Then

        vErro = "Ocorreu um erro" & Chr(13) & _
                "C�d. do erro: " & CStr(vBanco.Parameters("pCodErro")) & Chr(13) & _
                "Msg. de erro: " & CStr(vBanco.Parameters("pMsgErro"))

        Call vVB_Generica_001.ProcessaErro(vErro)
        Exit Sub

    End If

    Set vObjOracle = vBanco.Parameters("pRetorno1").Value

    If Not vObjOracle.EOF Then

        While Not vObjOracle.EOF

            If vObjOracle.Fields(0) = "CADASTRO" Then

                chkCADASTRO.Value = cAktiviert

            ElseIf vObjOracle.Fields(0) = "MANUAL" Then

                chkMANUAL.Value = cAktiviert

            ElseIf vObjOracle.Fields(0) = "AGENDAMENTO" Then

                chkAGENDAMENTO.Value = cAktiviert

            ElseIf vObjOracle.Fields(0) = "AUTOMATICA" Then

                chkAUTOMATICA.Value = cAktiviert

            ElseIf vObjOracle.Fields(0) = "ACOMPANHAMENTO" Then

                chkACOMPANHAMENTO.Value = cAktiviert

            End If

            vObjOracle.MoveNext

        Wend

    End If

    Exit Sub

Erro:

    Unload frmAguardar

    vErro = "Ocorreu um erro" & Chr(13) & _
            "C�d. do erro: " & Err.Number & Chr(13) & _
            "Msg. de erro: " & Err.Description

    Call vVB_Generica_001.ProcessaErro(vErro)

End Sub
Private Sub cmdGravar_Click()

    On Error GoTo Erro

    If txtUsuarioRede = "" Then

        Call vVB_Generica_001.Informar("Informe o usu�rio e consulte os direitos do mesmo.")
        Exit Sub

    End If

    If vVB_Generica_001.Perguntar("Confirma opera��o?") = 6 Then

        Call vVB_Generica_001.ExcluiBind(vBanco)

        vBanco.Parameters.Add "pAcao", 2, 1
        vBanco.Parameters.Add "pUsuarioRede", txtUsuarioRede, 1
        vBanco.Parameters.Add "pDireito", "", 1

        vBanco.Parameters.Add "pRetorno1", 0, 2
        vBanco.Parameters("pRetorno1").ServerType = 102
        vBanco.Parameters("pRetorno1").DynasetOption = &H2&
        vBanco.Parameters("pRetorno1").DynasetCacheParams 256, 16, 20, 2000, 0

        vBanco.Parameters.Add "pCodErro", 0, 2
        vBanco.Parameters.Add "pMsgErro", "", 2

        vSql = _
                "PRODUCAO.PCK_CON001.PRPROCEDURESGERAIS(" & _
                ":pAcao, " & _
                ":pUsuarioRede, " & _
                ":pDireito, " & _
                ":pRetorno1, " & _
                ":pCodErro, " & _
                ":pMsgErro)"

        vBanco.ExecuteSQL "Begin " & vSql & "; End;"

        If vBanco.Parameters("pCodErro") <> 0 Then

            vErro = "Ocorreu um erro" & Chr(13) & _
                    "C�d. do erro: " & CStr(vBanco.Parameters("pCodErro")) & Chr(13) & _
                    "Msg. de erro: " & CStr(vBanco.Parameters("pMsgErro"))

            Call vVB_Generica_001.ProcessaErro(vErro)
            Exit Sub

        End If

        If chkCADASTRO.Value = cAktiviert Then

            Call IncluirParametros("CADASTRO", txtUsuarioRede)

        End If

        If chkMANUAL.Value = cAktiviert Then

            Call IncluirParametros("MANUAL", txtUsuarioRede)

        End If

        If chkAGENDAMENTO.Value = cAktiviert Then

            Call IncluirParametros("AGENDAMENTO", txtUsuarioRede)

        End If

        If chkAUTOMATICA.Value = cAktiviert Then

            Call IncluirParametros("AUTOMATICA", txtUsuarioRede)

        End If

        If chkACOMPANHAMENTO.Value = cAktiviert Then

            Call IncluirParametros("ACOMPANHAMENTO", txtUsuarioRede)

        End If

        Call vVB_Generica_001.Informar("Opera��o realizada com sucesso.")

        chkCADASTRO.Value = cNichtAktiviert
        chkMANUAL.Value = cNichtAktiviert
        chkAGENDAMENTO.Value = cNichtAktiviert
        chkAUTOMATICA.Value = cNichtAktiviert
        chkACOMPANHAMENTO.Value = cNichtAktiviert

        txtUsuarioRede = ""
        txtUsuarioRede.SetFocus

    End If

    Exit Sub

Erro:

    Unload frmAguardar

    vErro = "Ocorreu um erro" & Chr(13) & _
            "C�d. do erro: " & Err.Number & Chr(13) & _
            "Msg. de erro: " & Err.Description

    Call vVB_Generica_001.ProcessaErro(vErro)

End Sub
Private Sub cmdValidarSenha_Click()

    If txtSenha <> "DPK001" Then

        Call vVB_Generica_001.Informar("Senha inv�lida")
        txtSenha = ""
        txtSenha.SetFocus
        Exit Sub

    End If

    fraSenha.Visible = False
    fraPar�metros.Visible = True

    txtUsuarioRede.SetFocus

End Sub
Private Sub cmdVoltar_Click()

    Unload Me

    Call ConfigurarDireitos

End Sub
Private Sub Form_Load()

    If vCd = 1 Then

        Select Case UCase(vBanco.DatabaseName)

            Case "CD02"

                vOwner2 = "Dep02"

            Case "CD11"

                vOwner2 = "Dep11"

            Case "CD12"

                vOwner2 = "Dep12"

            Case "CD13"

                vOwner2 = "Dep13"

            Case "CD16"

                vOwner2 = "Dep16"

            Case Else

                vOwner2 = "Helpdesk"

        End Select

    Else

        Select Case vTipoCD

            Case "U"

                vOwner = "Helpdesk"

            Case "M"

                vOwner = "Dep" & Format(vCd, "00")

        End Select

    End If

    fraPar�metros.Visible = False
    fraSenha.Visible = True

End Sub
Private Sub fraOA_Click()

End Sub


Private Sub txtSenha_GotFocus()

    Call vVB_Generica_001.SelecionaCampo(txtSenha)

End Sub

Private Sub txtSenha_KeyPress(KeyAscii As Integer)

    If KeyAscii = 13 Then

        Call cmdValidarSenha_Click

    End If

End Sub

Private Sub txtUsuarioRede_KeyPress(KeyAscii As Integer)

    If KeyAscii = 13 Then

        Call cmdConsultar_Click

    End If

End Sub
