VERSION 5.00
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "MSWINSCK.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Object = "{E57FB72C-1106-44AF-9706-0BA099A707C7}#4.2#0"; "XPFrame.ocx"
Begin VB.Form frmSobre 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   Caption         =   "::: SOBRE :::"
   ClientHeight    =   3660
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5235
   ClipControls    =   0   'False
   Icon            =   "frmSobre.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   244
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   349
   StartUpPosition =   2  'CenterScreen
   Begin MSWinsockLib.Winsock WinSock 
      Left            =   45
      Top             =   1980
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   555
      Left            =   45
      TabIndex        =   0
      Top             =   45
      Width           =   5145
      _ExtentX        =   9075
      _ExtentY        =   979
      Gradient        =   12
      Color1          =   0
      BackColor       =   -2147483633
      Begin VB.Label lblNomeSistema 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   435
         Left            =   60
         TabIndex        =   1
         Top             =   60
         Width           =   5040
         WordWrap        =   -1  'True
      End
   End
   Begin CoolXPFrame.xpFrame fraTipo 
      Height          =   1005
      Left            =   45
      Top             =   585
      Width           =   5145
      _ExtentX        =   9075
      _ExtentY        =   1773
      Caption         =   "Analista"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   13977088
      BorderColor     =   8421504
      ColorStyle      =   99
      Begin VB.Label lblBackup 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   285
         Left            =   855
         TabIndex        =   6
         Top             =   675
         Width           =   4200
      End
      Begin VB.Label lblResponsavel 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   285
         Left            =   1350
         TabIndex        =   5
         Top             =   315
         Width           =   3705
      End
      Begin VB.Label lbl2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Backup:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   90
         TabIndex        =   8
         Top             =   675
         Width           =   720
      End
      Begin VB.Label lbl1 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Respons�vel:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   90
         TabIndex        =   7
         Top             =   315
         Width           =   1200
      End
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      Caption         =   "by Maxxipel"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   195
      Left            =   45
      TabIndex        =   4
      Top             =   3465
      Width           =   915
   End
   Begin VB.Image Image1 
      Height          =   1725
      Left            =   1485
      Picture         =   "frmSobre.frx":23D2
      Stretch         =   -1  'True
      Top             =   1935
      Width           =   2040
   End
   Begin VB.Label lblVersao 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   195
      Left            =   3600
      TabIndex        =   3
      Top             =   3465
      Width           =   1590
   End
   Begin VB.Label lblIp 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Caption         =   "Seu I.P. para acesso remoto �: "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   45
      TabIndex        =   2
      Top             =   1665
      Width           =   5145
   End
End
Attribute VB_Name = "frmSobre"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)

    If KeyCode = 121 And _
            Shift = 3 Then

        Unload Me

        frmParametros.Show

    End If

End Sub
Private Sub Form_Load()

    Set vObjOracle = vVB_Generica_001.InformacoesSistema(vBanco, UCase(App.Title))

    Call DefinirTelaSobre

    lblIp = lblIp + Winsock.LocalIP

    lblVersao = "VERS�O: " & App.Major & "." & App.Minor & "." & App.Revision

End Sub
