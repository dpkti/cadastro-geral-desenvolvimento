VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Object = "{E57FB72C-1106-44AF-9706-0BA099A707C7}#4.2#0"; "XPFrame.ocx"
Begin VB.Form frmCadastro 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "::: CADASTRO :::"
   ClientHeight    =   3300
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5415
   ControlBox      =   0   'False
   Icon            =   "frmCadastro.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   220
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   361
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   810
      Width           =   5325
      _ExtentX        =   9393
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   4680
      TabIndex        =   4
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadastro.frx":23D2
      PICN            =   "frmCadastro.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdNovo 
      Height          =   690
      Left            =   45
      TabIndex        =   0
      TabStop         =   0   'False
      ToolTipText     =   "Novo registro"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadastro.frx":30C8
      PICN            =   "frmCadastro.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdGravar 
      Height          =   690
      Left            =   765
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Gravas as informa��es"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadastro.frx":3DBE
      PICN            =   "frmCadastro.frx":3DDA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdExcluir 
      Height          =   690
      Left            =   1485
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Excluir registro atual"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadastro.frx":4AB4
      PICN            =   "frmCadastro.frx":4AD0
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdCancelar 
      Height          =   690
      Left            =   2205
      TabIndex        =   3
      TabStop         =   0   'False
      ToolTipText     =   "Limpar todos os campos"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadastro.frx":57AA
      PICN            =   "frmCadastro.frx":57C6
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin CoolXPFrame.xpFrame fraDeposito 
      Height          =   690
      Left            =   45
      Top             =   810
      Width           =   3390
      _ExtentX        =   5980
      _ExtentY        =   1217
      Caption         =   "Dep�sito"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   13977088
      BorderColor     =   8421504
      ColorStyle      =   99
      UseCoolToolTip  =   0   'False
      Begin VB.ComboBox cmbLoja 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   90
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Tag             =   "NULO;Informe o dep�sito."
         Top             =   270
         Width           =   3210
      End
   End
   Begin CoolXPFrame.xpFrame fraInformacoes 
      Height          =   1770
      Left            =   45
      Top             =   1485
      Visible         =   0   'False
      Width           =   5325
      _ExtentX        =   9393
      _ExtentY        =   3122
      Caption         =   "Informa��es"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   13977088
      BorderColor     =   8421504
      ColorStyle      =   99
      UseCoolToolTip  =   0   'False
      Begin VB.TextBox txtIP 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1260
         TabIndex        =   8
         Tag             =   "NULO;Informe o IP."
         Top             =   270
         Width           =   1995
      End
      Begin VB.TextBox txtCompartilhamento 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1620
         TabIndex        =   10
         Tag             =   "NULO;Informe o compartilhamento."
         Top             =   630
         Width           =   3615
      End
      Begin VB.TextBox txtUsuario 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   765
         TabIndex        =   12
         Tag             =   "NULO;Informe o usu�rio."
         Top             =   990
         Width           =   2490
      End
      Begin VB.TextBox txtSenha 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Wingdings"
            Size            =   9
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00D54600&
         Height          =   330
         IMEMode         =   3  'DISABLE
         Left            =   765
         PasswordChar    =   "l"
         TabIndex        =   15
         Tag             =   "NULO;Informe a senha."
         Top             =   1350
         Width           =   2490
      End
      Begin Bot�o.cmd cmdTestar 
         Height          =   690
         Left            =   4545
         TabIndex        =   14
         TabStop         =   0   'False
         ToolTipText     =   "Testar conex�o"
         Top             =   990
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadastro.frx":60A0
         PICN            =   "frmCadastro.frx":60BC
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Compartilhamento"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   90
         TabIndex        =   9
         Top             =   675
         Width           =   1485
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "IP do servidor"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   90
         TabIndex        =   7
         Top             =   315
         Width           =   1125
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Usu�rio"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   90
         TabIndex        =   11
         Top             =   1035
         Width           =   585
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Senha"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   90
         TabIndex        =   13
         Top             =   1395
         Width           =   510
      End
   End
End
Attribute VB_Name = "frmCadastro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdSair_Click()

    Unload Me

End Sub

Private Sub cmbLoja_Click()

    On Error GoTo Erro

    If vNovo = False Then

        If cmbLoja.ListIndex <> -1 Then

            fraInformacoes.Visible = False

            Call vVB_Generica_001.ExcluiBind(vBanco)

            vBanco.Parameters.Add "pAcao", 1, 1
            vBanco.Parameters.Add "pCodLoja", Val(cmbLoja), 1
            vBanco.Parameters.Add "pIP", "", 1
            vBanco.Parameters.Add "pCompartilhamento", "", 1
            vBanco.Parameters.Add "pUsuario", "", 1
            vBanco.Parameters.Add "pSenha", "", 1

            vBanco.Parameters.Add "pRetorno1", 0, 2
            vBanco.Parameters("pRetorno1").ServerType = 102
            vBanco.Parameters("pRetorno1").DynasetOption = &H2&
            vBanco.Parameters("pRetorno1").DynasetCacheParams 256, 16, 20, 2000, 0

            vBanco.Parameters.Add "pCodErro", 0, 2
            vBanco.Parameters.Add "pMsgErro", "", 2

            vSql = _
                    "PRODUCAO.PCK_CON001.PRCADASTRO(" & _
                    ":pAcao, " & _
                    ":pCodLoja, " & _
                    ":pIP, " & _
                    ":pCompartilhamento, " & _
                    ":pUsuario, " & _
                    ":pSenha, " & _
                    ":pRetorno1, " & _
                    ":pCodErro, " & _
                    ":pMsgErro)"

            vBanco.ExecuteSQL "Begin " & vSql & "; End;"

            If vBanco.Parameters("pCodErro") <> 0 Then

                vErro = "Ocorreu um erro" & Chr(13) & _
                        "C�d. do erro: " & CStr(vBanco.Parameters("pCodErro")) & Chr(13) & _
                        "Msg. de erro: " & CStr(vBanco.Parameters("pMsgErro"))

                Call vVB_Generica_001.ProcessaErro(vErro)
                Exit Sub

            End If

            Set vObjOracle = vBanco.Parameters("pRetorno1").Value

            If Not vObjOracle.EOF Then

                txtIP = vObjOracle.Fields(0)
                txtCompartilhamento = vObjOracle.Fields(1)
                txtUsuario = vObjOracle.Fields(2)
                txtSenha = vObjOracle.Fields(3)

                fraInformacoes.Visible = True

            End If

        End If

    End If

    Exit Sub

Erro:

    Unload frmAguardar

    vErro = "Ocorreu um erro" & Chr(13) & _
            "C�d. do erro: " & Err.Number & Chr(13) & _
            "Msg. de erro: " & Err.Description

    Call vVB_Generica_001.ProcessaErro(vErro)

End Sub
Private Sub cmdCancelar_Click()

    cmbLoja.ListIndex = -1
    fraInformacoes.Visible = False
    cmbLoja.SetFocus

    vNovo = False

End Sub

Private Sub cmdExcluir_Click()

    On Error GoTo Erro

    If vNovo = False Then

        If fraInformacoes.Visible = True Then

            If vVB_Generica_001.Perguntar("Confirma opera��o?") = 6 Then

                Call vVB_Generica_001.ExcluiBind(vBanco)

                vBanco.Parameters.Add "pAcao", 3, 1
                vBanco.Parameters.Add "pCodLoja", Val(cmbLoja), 1
                vBanco.Parameters.Add "pIP", "", 1
                vBanco.Parameters.Add "pCompartilhamento", "", 1
                vBanco.Parameters.Add "pUsuario", "", 1
                vBanco.Parameters.Add "pSenha", "", 1

                vBanco.Parameters.Add "pRetorno1", 0, 2
                vBanco.Parameters("pRetorno1").ServerType = 102
                vBanco.Parameters("pRetorno1").DynasetOption = &H2&
                vBanco.Parameters("pRetorno1").DynasetCacheParams 256, 16, 20, 2000, 0

                vBanco.Parameters.Add "pCodErro", 0, 2
                vBanco.Parameters.Add "pMsgErro", "", 2

                vSql = _
                        "PRODUCAO.PCK_CON001.PRCADASTRO(" & _
                        ":pAcao, " & _
                        ":pCodLoja, " & _
                        ":pIP, " & _
                        ":pCompartilhamento, " & _
                        ":pUsuario, " & _
                        ":pSenha, " & _
                        ":pRetorno1, " & _
                        ":pCodErro, " & _
                        ":pMsgErro)"

                vBanco.ExecuteSQL "Begin " & vSql & "; End;"

                If vBanco.Parameters("pCodErro") <> 0 Then

                    vErro = "Ocorreu um erro" & Chr(13) & _
                            "C�d. do erro: " & CStr(vBanco.Parameters("pCodErro")) & Chr(13) & _
                            "Msg. de erro: " & CStr(vBanco.Parameters("pMsgErro"))

                    Call vVB_Generica_001.ProcessaErro(vErro)
                    Exit Sub

                End If

                Call vVB_Generica_001.Informar("Opera��o realizada com sucesso.")
                Call cmdCancelar_Click

                vNovo = False

            End If

        End If

    End If

    Exit Sub

Erro:

    Unload frmAguardar

    vErro = "Ocorreu um erro" & Chr(13) & _
            "C�d. do erro: " & Err.Number & Chr(13) & _
            "Msg. de erro: " & Err.Description

    Call vVB_Generica_001.ProcessaErro(vErro)

End Sub
Private Sub cmdGravar_Click()

    On Error GoTo Erro

    If fraInformacoes.Visible = True Then

        If fValidarCampos(Me) = True Then

            If vVB_Generica_001.Perguntar("Confirma opera��o?") = 6 Then

                Call vVB_Generica_001.ExcluiBind(vBanco)

                vBanco.Parameters.Add "pAcao", 2, 1
                vBanco.Parameters.Add "pCodLoja", Val(cmbLoja), 1
                vBanco.Parameters.Add "pIP", txtIP, 1
                vBanco.Parameters.Add "pCompartilhamento", txtCompartilhamento, 1
                vBanco.Parameters.Add "pUsuario", txtUsuario, 1
                vBanco.Parameters.Add "pSenha", txtSenha, 1

                vBanco.Parameters.Add "pRetorno1", 0, 2
                vBanco.Parameters("pRetorno1").ServerType = 102
                vBanco.Parameters("pRetorno1").DynasetOption = &H2&
                vBanco.Parameters("pRetorno1").DynasetCacheParams 256, 16, 20, 2000, 0

                vBanco.Parameters.Add "pCodErro", 0, 2
                vBanco.Parameters.Add "pMsgErro", "", 2

                vSql = _
                        "PRODUCAO.PCK_CON001.PRCADASTRO(" & _
                        ":pAcao, " & _
                        ":pCodLoja, " & _
                        ":pIP, " & _
                        ":pCompartilhamento, " & _
                        ":pUsuario, " & _
                        ":pSenha, " & _
                        ":pRetorno1, " & _
                        ":pCodErro, " & _
                        ":pMsgErro)"

                vBanco.ExecuteSQL "Begin " & vSql & "; End;"

                If vBanco.Parameters("pCodErro") <> 0 Then

                    vErro = "Ocorreu um erro" & Chr(13) & _
                            "C�d. do erro: " & CStr(vBanco.Parameters("pCodErro")) & Chr(13) & _
                            "Msg. de erro: " & CStr(vBanco.Parameters("pMsgErro"))

                    Call vVB_Generica_001.ProcessaErro(vErro)
                    Exit Sub

                End If

                Call vVB_Generica_001.Informar("Opera��o realizada com sucesso.")
                Call cmdCancelar_Click

                vNovo = False

            End If

        End If

    End If

    Exit Sub

Erro:

    Unload frmAguardar

    vErro = "Ocorreu um erro" & Chr(13) & _
            "C�d. do erro: " & Err.Number & Chr(13) & _
            "Msg. de erro: " & Err.Description

    Call vVB_Generica_001.ProcessaErro(vErro)

End Sub
Private Sub cmdNovo_Click()

    vNovo = True

    cmbLoja.ListIndex = -1

    txtIP = ""
    txtCompartilhamento = ""
    txtUsuario = ""
    txtSenha = ""

    cmbLoja.SetFocus

    fraInformacoes.Visible = True

End Sub

Private Sub cmdTestar_Click()

    On Error GoTo Erro

    Call Aguardar

    Call Desconectar
    If fConectar(Val(cmbLoja), True) = False Then

        Unload frmAguardar
        Exit Sub

    End If

    Call Desconectar
    Unload frmAguardar
    Call vVB_Generica_001.Informar("Conex�o realizada com sucesso.")

    Exit Sub

Erro:

    Unload frmAguardar

    vErro = "Ocorreu um erro" & Chr(13) & _
            "C�d. do erro: " & Err.Number & Chr(13) & _
            "Msg. de erro: " & Err.Description

    Call vVB_Generica_001.ProcessaErro(vErro)

End Sub
Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()

    On Error GoTo Erro

    Me.Top = 0
    Me.Left = 0

    Set vObjOracle = vVB_Generica_001.TabelaLoja(vBanco)
    Call vVB_Generica_001.PreencheComboList(vObjOracle, cmbLoja, vObjOracle.Fields(0).Name)

    Exit Sub

Erro:

    Unload frmAguardar

    vErro = "Ocorreu um erro" & Chr(13) & _
            "C�d. do erro: " & Err.Number & Chr(13) & _
            "Msg. de erro: " & Err.Description

    Call vVB_Generica_001.ProcessaErro(vErro)

End Sub

Private Sub txtCompartilhamento_GotFocus()

    Call vVB_Generica_001.SelecionaCampo(txtCompartilhamento)

End Sub
Private Sub txtCompartilhamento_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)

End Sub
Private Sub txtIP_GotFocus()

    Call vVB_Generica_001.SelecionaCampo(txtIP)

End Sub

Private Sub txtSenha_GotFocus()

    Call vVB_Generica_001.SelecionaCampo(txtSenha)

End Sub
Private Sub txtUsuario_GotFocus()

    Call vVB_Generica_001.SelecionaCampo(txtUsuario)

End Sub
