VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmLayOut 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Lay-Out de Arquivo para Importa��o"
   ClientHeight    =   6720
   ClientLeft      =   45
   ClientTop       =   1950
   ClientWidth     =   16935
   Icon            =   "frmLayOut.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   448
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   1129
   Begin VB.Frame fra 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   4695
      Left            =   30
      TabIndex        =   3
      Top             =   1245
      Width           =   16890
      Begin VB.TextBox txtLayOut 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4470
         Left            =   60
         MultiLine       =   -1  'True
         TabIndex        =   4
         Top             =   165
         Width           =   16755
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   6390
      Width           =   16935
      _ExtentX        =   29871
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   29819
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      Top             =   810
      Width           =   16845
      _ExtentX        =   29713
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmLayOut.frx":23D2
      PICN            =   "frmLayOut.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmLayOut"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
'C.FELIPE.CASSIANI - 13/05/2015 - TI-1646 - Corre��o cadastro em massa - CAD620
Private Sub Form_Load()
    
    Me.Top = 1605
    Me.Left = 0
    
    
    txtLayOut = "Campo          Tipo                Tamanho     Posi��es" & vbCrLf
    txtLayOut = txtLayOut & "--------------         ----------- ----------- --------" & vbCrLf
    txtLayOut = txtLayOut & "C�d.Montadora          Num�rico    2           01 - 02 " & vbCrLf
    txtLayOut = txtLayOut & "C�d.Original           AlfaNumer.  20          03 - 22 " & vbCrLf
    txtLayOut = txtLayOut & "C�d.F�brica            AlfaNumer.  19          23 - 41 " & vbCrLf
    txtLayOut = txtLayOut & "C�d.DPK                Num�rico    18          42 - 59 " & vbCrLf
    txtLayOut = txtLayOut & "Descri��o Item         AlfaNumer.  40          60 - 99 " & vbCrLf
    txtLayOut = txtLayOut & "Categoria              AlfaNumer.  1           100 - 100 " & vbCrLf
    'txtLayOut = txtLayOut & "Sequ�ncia              Num�rico    2           101 - 102 " & vbCrLf
    txtLayOut = txtLayOut & "Descri��o Cat�logo     AlfaNumer.  45          101 - 145 " & vbCrLf & vbCrLf & vbCrLf
    
    txtLayOut = txtLayOut & "Exemplo de Arquivo:" & vbCrLf
    txtLayOut = txtLayOut & "-------------------" & vbCrLf
    txtLayOut = txtLayOut & "1593247344            1987429606         000000000000063855FILTRO DE AR                            LPrim�rio = GMC 6.100 c/motor 'Maxion' S4 ap�s" & vbCrLf
    txtLayOut = txtLayOut & "1593277239            1987429606         000000000000063855FILTRO DE AR                            L96. GMC 6.150 c/motor MWM Sprint 6T ap�s 96." & vbCrLf
    txtLayOut = txtLayOut & "1593247344            1987429606         000000000000063855FILTRO DE AR                            LSilverado c/motor Maxion S4 ap�s 97.Silverado" & vbCrLf
    txtLayOut = txtLayOut & "1593247344            1987429606         000000000000063855FILTRO DE AR                            Lc/motor MWM Sprint 6T ap�s 97. Grand Blazer"

    
    
End Sub

