VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmCadastro 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Leitura de Arquivo e Acerto"
   ClientHeight    =   7455
   ClientLeft      =   180
   ClientTop       =   1980
   ClientWidth     =   10815
   Icon            =   "frmCadastro.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   497
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   721
   Begin VB.Frame fraLog 
      Caption         =   "Log de Erros"
      Height          =   2220
      Left            =   0
      TabIndex        =   5
      Top             =   4860
      Visible         =   0   'False
      Width           =   10770
      Begin VB.ListBox txtLog 
         Height          =   1815
         Left            =   135
         TabIndex        =   12
         Top             =   270
         Width           =   10395
      End
   End
   Begin VB.Frame fraDados 
      Caption         =   "Dados"
      Height          =   2760
      Left            =   0
      TabIndex        =   4
      Top             =   2070
      Visible         =   0   'False
      Width           =   10770
      Begin VB.TextBox txtMont 
         Height          =   285
         Left            =   765
         MaxLength       =   2
         TabIndex        =   17
         Text            =   "99"
         Top             =   2295
         Visible         =   0   'False
         Width           =   375
      End
      Begin VB.TextBox txtDpk 
         Height          =   285
         Left            =   5265
         MaxLength       =   5
         TabIndex        =   16
         Text            =   "99999"
         Top             =   2295
         Visible         =   0   'False
         Width           =   615
      End
      Begin VB.TextBox txtLin 
         Height          =   285
         Left            =   9825
         MaxLength       =   1
         TabIndex        =   15
         Text            =   "X"
         Top             =   2295
         Visible         =   0   'False
         Width           =   375
      End
      Begin VB.TextBox txtSeq 
         Height          =   285
         Left            =   10185
         MaxLength       =   1
         TabIndex        =   14
         Text            =   "9"
         Top             =   2295
         Visible         =   0   'False
         Width           =   495
      End
      Begin VB.TextBox txtDesc 
         Height          =   285
         Left            =   5865
         MaxLength       =   40
         TabIndex        =   13
         Text            =   "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
         Top             =   2295
         Visible         =   0   'False
         Width           =   3975
      End
      Begin MSGrid.Grid grdArquivo 
         Height          =   1920
         Left            =   135
         TabIndex        =   11
         Top             =   270
         Width           =   10530
         _Version        =   65536
         _ExtentX        =   18574
         _ExtentY        =   3387
         _StockProps     =   77
         ForeColor       =   8388608
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Cols            =   9
      End
   End
   Begin VB.Frame fraArquivo 
      Caption         =   "Arquivo"
      Height          =   1095
      Left            =   0
      TabIndex        =   3
      Top             =   945
      Width           =   10770
      Begin VB.TextBox txtArquivo 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   1575
         Locked          =   -1  'True
         TabIndex        =   10
         Top             =   450
         Width           =   8880
      End
      Begin VB.Label lblNomeArq 
         AutoSize        =   -1  'True
         Caption         =   "Nome do Arquivo:"
         Height          =   195
         Left            =   180
         TabIndex        =   9
         Top             =   495
         Width           =   1275
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   7125
      Width           =   10815
      _ExtentX        =   19076
      _ExtentY        =   582
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   19024
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      Top             =   810
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   0
      MPTR            =   1
      MICON           =   "frmCadastro.frx":23D2
      PICN            =   "frmCadastro.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdArquivo 
      Height          =   690
      Left            =   810
      TabIndex        =   6
      TabStop         =   0   'False
      ToolTipText     =   "Importar Arquivo"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   0
      MPTR            =   1
      MICON           =   "frmCadastro.frx":30C8
      PICN            =   "frmCadastro.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdConfirma 
      Height          =   690
      Left            =   1575
      TabIndex        =   7
      TabStop         =   0   'False
      ToolTipText     =   "Gravar Aplica��es"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   0
      MPTR            =   1
      MICON           =   "frmCadastro.frx":3DBE
      PICN            =   "frmCadastro.frx":3DDA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdLog 
      Height          =   690
      Left            =   2340
      TabIndex        =   8
      TabStop         =   0   'False
      ToolTipText     =   "Gravar Arquivo de Log"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   0
      MPTR            =   1
      MICON           =   "frmCadastro.frx":4AB4
      PICN            =   "frmCadastro.frx":4AD0
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSComDlg.CommonDialog dlgOpen 
      Left            =   4215
      Top             =   195
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      InitDir         =   "c:\cad620"
   End
   Begin Bot�o.cmd cmdLayout 
      Height          =   690
      Left            =   3105
      TabIndex        =   18
      TabStop         =   0   'False
      ToolTipText     =   "Exibe Lay-out do Arquivo"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   0
      MPTR            =   1
      MICON           =   "frmCadastro.frx":57AA
      PICN            =   "frmCadastro.frx":57C6
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmCadastro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim vNomeArq As String
Dim fl_Log As Boolean
Dim vGuardaCol As Integer
Dim vGuardaLin As Long

Public OraParameters As Object

'C.FELIPE.CASSIANI - 13/05/2015 - TI-1646 - Corre��o cadastro em massa - CAD620
Private Sub cmdArquivo_Click()
    
    Dim Arquivo As Integer
    Dim i As Integer
    Dim vQtdErro As Integer
    Dim varLinhaArquivo As String
    
    Dim vLinha As String
    Dim vSeq As String
    
    Dim tmpCOD_CATEGORIA As String
    Dim tmpCOD_DPK As String
    Dim tmpCOD_MONTADORA, tmpCOD_MONTADORA_ant As String
    
    Dim ChecarMont As Boolean
    Dim ChecarDPK
    'Dim SEQ_Retorno, SEQ_Retorno_aux
        
    On Error GoTo Trata_Erro
    
    dlgOpen.ShowOpen
    
    If dlgOpen.FileName = "" Then
        MsgBox "Indique o arquivo a ser aberto", vbOKOnly, "CAD620"
        Exit Sub
    End If
    
    fraDados.Visible = True
    fraLog.Visible = True
    
    txtLog.Clear
    
    vNomeArq = dlgOpen.FileName
    
    txtArquivo.Text = vNomeArq
    
    'MONTA O GRID
    grdArquivo.Row = 0
    
    grdArquivo.Col = 0
    grdArquivo.ColWidth(0) = 600
    grdArquivo.Text = "Num.Lin."
    
    grdArquivo.Col = 1
    grdArquivo.ColWidth(1) = 450
    grdArquivo.Text = "Mont."
    
    grdArquivo.Col = 2
    grdArquivo.ColWidth(2) = 2000
    grdArquivo.Text = "            C�d. Original    "
    
    grdArquivo.Col = 3
    grdArquivo.ColWidth(3) = 2000
    grdArquivo.Text = "            C�d.F�brica"
    
    grdArquivo.Col = 4
    grdArquivo.ColWidth(4) = 2000
    grdArquivo.Text = " DPK "
        
    grdArquivo.Col = 5
    grdArquivo.ColWidth(5) = 4000
    grdArquivo.Text = "                                 Descri��o "

    grdArquivo.Col = 6
    grdArquivo.ColWidth(6) = 300
    grdArquivo.Text = "Lin."
        
'    grdArquivo.Col = 7
'    grdArquivo.ColWidth(7) = 400
'    grdArquivo.Text = " Seq."
        
    grdArquivo.Col = 8
    grdArquivo.ColWidth(8) = 4500
    grdArquivo.Text = "                   Aplica��o para Cat�logo "
    
    Arquivo = FreeFile
    
    Open vNomeArq For Input As #Arquivo
    
    i = 0
    vQtdErro = 0
    
    stbBarra.SimpleText = "Carregando Arquivo. Aguarde..."
    
    Screen.MousePointer = 11
      
    DoEvents
      
    Do While Not EOF(Arquivo)
    
      i = i + 1
    
      'Inicializa exist�ncia do log como inexistente para a linha que est� sendo lida do arquivo
      fl_Log = False
      
      stbBarra.SimpleText = "Lendo Registro " & i & ". Aguarde..."
      stbBarra.Refresh
      DoEvents
      
      'L� registro - linha - do arquivo, joga na vari�vel  e seta ponteiro para o pr�ximo
      Line Input #Arquivo, varLinhaArquivo
      
      'Se linha lida for em branco, sai do loop
      If Trim(Mid(varLinhaArquivo, 1, 147)) = "" Then
          Exit Do
      End If
            
      tmpCOD_DPK = Mid(varLinhaArquivo, 42, 18)
      tmpCOD_MONTADORA = Mid(varLinhaArquivo, 1, 2)
      tmpCOD_CATEGORIA = Mid(varLinhaArquivo, 100, 1)
      
      
      If Not IsNumeric(tmpCOD_MONTADORA) Then
          ChecarMont = False
          txtLog.AddItem "LINHA " & i & " DO ARQUIVO - Montadora " & Mid(varLinhaArquivo, 1, 2) & " n�o � num�rico!"
          fl_Log = True
          vQtdErro = vQtdErro + 1
      Else
          ChecarMont = True
      End If

      If Not ChecarMont Then
          tmpCOD_MONTADORA = 0
      End If
      
      If Not IsNumeric(tmpCOD_DPK) Then
          ChecarDPK = False
          txtLog.AddItem "LINHA " & i & " DO ARQUIVO - DPK " & Mid(varLinhaArquivo, 42, 5) & " n�o � num�rico!"
          fl_Log = True
          vQtdErro = vQtdErro + 1
      Else
          ChecarDPK = True
      End If
      
      If Not ChecarDPK Then
          tmpCOD_DPK = 0
      End If
      
      
      If ChecarMont Or ChecarDPK Then
      
          Set OraParameters = vBanco.Parameters
          
          OraParameters.Remove "COD_MONTADORA": OraParameters.Add "COD_MONTADORA", Trim(tmpCOD_MONTADORA), 1
          OraParameters.Remove "COD_DPK": OraParameters.Add "COD_DPK", Trim(tmpCOD_DPK), 1
          'OraParameters.Remove "COD_CATEGORIA": OraParameters.Add "COD_CATEGORIA", Trim(tmpCOD_CATEGORIA), 2
          OraParameters.Remove "QTD_MONTADORA": OraParameters.Add "QTD_MONTADORA", 0, 2
          OraParameters.Remove "QTD_DPK": OraParameters.Add "QTD_DPK", 0, 2
          'OraParameters.Remove "SEQ_RETORNO": OraParameters.Add "SEQ_RETORNO", 0, 2
          OraParameters.Remove "ERRO": OraParameters.Add "ERRO", 0, 2
          
          vSql = "PRODUCAO.PCK_CAD620.PR_BUSCA_DADOS(:COD_MONTADORA, :COD_DPK, :QTD_MONTADORA, :QTD_DPK, :ERRO)"
          
          If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
              Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
              Exit Sub
          End If
          
          If OraParameters("ERRO").Value <> 0 Then
              Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
              Exit Sub
          End If
          
          If (OraParameters("QTD_MONTADORA").Value = 0) And (ChecarMont) Then
              txtLog.AddItem "LINHA " & i & " DO ARQUIVO - Montadora " & Mid(varLinhaArquivo, 1, 2) & " inexistente!"
              fl_Log = True
              vQtdErro = vQtdErro + 1
          End If
          
          If (OraParameters("QTD_DPK").Value = 0) And (ChecarDPK) Then
            txtLog.AddItem "LINHA " & i & " DO ARQUIVO - DPK " & Mid(varLinhaArquivo, 42, 18) & " inexistente!"
            fl_Log = True
            vQtdErro = vQtdErro + 1
          End If
          
          'SEQ_Retorno = OraParameters("SEQ_RETORNO").Value
                    
      End If
      
      'Vai verificar se a linha da descri��o est� em branco, caso esteja vai marcar como erro
      If Trim(Mid(varLinhaArquivo, 60, 40)) = "" Then
          txtLog.AddItem "LINHA " & i & " DO ARQUIVO - Descri��o n�o pode ser branco!"
          fl_Log = True
          vQtdErro = vQtdErro + 1
      End If
      
      
      'Vai verificar se a linha � igual a: "P", "L", "U","M","A"
      vLinha = Mid(varLinhaArquivo, 100, 1)
      If vLinha <> "P" And vLinha <> "L" And vLinha <> "U" And vLinha <> "M" And vLinha <> "A" Then
          txtLog.AddItem "LINHA " & i & " DO ARQUIVO - Tipo de Linha '" & vLinha & "' inexistente!"
          fl_Log = True
          vQtdErro = vQtdErro + 1
      End If
      
      
            
      'Vai verificar se a sequencia >=1 e <= 99
'      vSeq = Val(Mid(varLinhaArquivo, 101, 2))
'      If vSeq < 1 Or vSeq > 99 Then
'          txtLog.AddItem "LINHA " & i & " DO ARQUIVO - Sequ�ncia " & vSeq & " precisa ser >= 1 e <= 99!"
'          fl_Log = True
'          vQtdErro = vQtdErro + 1
'      End If
      
      'Descri��o da aplica��o obrigat�ria e maior ou igual a 10 caracteres - Alethea - 14/09/06
      'Descri��o da aplica��o obrigat�ria e maior ou igual a 9 caracteres - Alethea - 22/02/07
      If Len(Trim(Mid(varLinhaArquivo, 101, 45))) < 9 Then
          txtLog.AddItem "LINHA " & i & " DO ARQUIVO - Descri��o da aplica��o deve ter no m�nimo 9 caracteres!"
          fl_Log = True
          vQtdErro = vQtdErro + 1
      End If
      
      
      
      'Tamanho do grid ser� correspondente � qtd. de linhas lidas do arquivo texto
      grdArquivo.Rows = i + 1
      
      'Grava as informa��es - linha a linha - no grid
      grdArquivo.Row = i
      
      'Colunas ir�o receber informa��es vindas do arquivo
      grdArquivo.Col = 0
      If fl_Log = True Then   'Insere um asterisco e a qtd. de erros, ao inv�s do n�mero da linha, qdo. tiver problemas (Ex.: *qtd.erros* no.linha = *3* 103)
              grdArquivo.Text = "*" & Format(vQtdErro, "0") & "* " & Format(i, "0")
      Else
              grdArquivo.Text = Format(i, "0000")
      End If
      grdArquivo.Col = 1
      grdArquivo.Text = Format(Mid(varLinhaArquivo, 1, 2), "00")
      grdArquivo.Col = 2
      grdArquivo.Text = Mid(varLinhaArquivo, 3, 20)
      grdArquivo.Col = 3
      grdArquivo.Text = Mid(varLinhaArquivo, 23, 19)
      grdArquivo.Col = 4
      grdArquivo.Text = Format(Mid(varLinhaArquivo, 42, 18), "000000000000000000")
      grdArquivo.Col = 5
      grdArquivo.Text = Mid(varLinhaArquivo, 60, 40)
      grdArquivo.Col = 6
      grdArquivo.Text = Mid(varLinhaArquivo, 100, 1)
'      grdArquivo.Col = 7
'      grdArquivo.Text = SEQ_Retorno 'Format(Mid(varLinhaArquivo, 101, 2), "0")
      grdArquivo.Col = 8
      'grdArquivo.Text = CStr(Mid(varLinhaArquivo, 90, 45))
      grdArquivo.Text = Mid(varLinhaArquivo, 101, 45)

      vQtdErro = 0
      
    Loop
    
    Close #Arquivo
    
    Screen.MousePointer = 0
    
    stbBarra.SimpleText = "Arquivo Lido."
    
    Exit Sub
    


Trata_Erro:
    
    If Err.Number = 53 Then
        MsgBox "Arquivo de Controle n�o Encontrado. ", , "CAD620"
    Else
        Call MsgBox(Err.Description, vbOKOnly, "CAD620")
    End If
    
End Sub

'C.FELIPE.CASSIANI - 13/05/2015 - TI-1646 - Corre��o cadastro em massa - CAD620
Private Sub cmdConfirma_Click()
    
    Dim Fl_Erro As Boolean
    Dim Fl_Prim As Boolean
    Dim i As Long
    Dim vNumLin As Long
    Dim vDPK_Ant As Long             'Guarda o c�digo DPK anterior
    Dim vDPK_Atu As Long             'Guarda o c�digo DPK atual
    Dim vMontAnt As Integer
    Dim vMontAtu As Integer
'    Dim vSeqAnt As Integer
'    Dim vSeqAtu As Integer
    Dim vLinAnt As String
    Dim vLinAtu As String
    Dim Fl_PrimLin
    Dim Fl_Aux
    'Vari�veis para parte de translate de descri��o de aplica��o - Alethea - 01/08/05
    Dim vDescApliCatalogo As String, vDescApliNormal As String
    Dim vSql2 As String
    Dim objTranslate As Object
   
    stbBarra.SimpleText = ""
    
    Fl_Erro = False
    
    grdArquivo.Col = 0
    For i = 1 To grdArquivo.Rows - 1
        'A 1a. linha que for encontrada com erro, j� vai setar flag e sair do loop
        grdArquivo.Row = i
        If Mid(grdArquivo, 1, 1) = "*" Then
            Fl_Erro = True
            Exit For
        End If
    Next i
    
    'Se ainda existir erro no arquivo (grid), envia mensagem ao usu�rio e n�o faz processamento
    If Fl_Erro = True Then
        MsgBox "Ainda existem erros no arquivo! Favor verificar linha " & i & " e acertar.", vbCritical, "Aten��o"
        Exit Sub
    End If
    
    'Vai verificar a ordem das linhas, ordenando por DPK, MONTADORA e SEQUENCIA
    grdArquivo.Row = 1
    grdArquivo.Col = 4
    vDPK_Ant = Val(grdArquivo.Text)
    grdArquivo.Col = 1
    vMontAnt = Val(grdArquivo.Text)
'    grdArquivo.Col = 7
'    vSeqAnt = Val(grdArquivo.Text)
    grdArquivo.Col = 6
    vLinAnt = grdArquivo.Text
    Fl_Prim = True
    
    For i = 1 To grdArquivo.Rows - 1
        grdArquivo.Row = i
        grdArquivo.Col = 4
        vDPK_Atu = Val(grdArquivo.Text)
        grdArquivo.Col = 1
        vMontAtu = Val(grdArquivo.Text)
        grdArquivo.Col = 6
        vLinAtu = grdArquivo.Text
'        grdArquivo.Col = 7
'        vSeqAtu = Val(grdArquivo.Text)
        
        'Caso DPK da linha atual seja menor que DPK da linha anterior, avisa usu�rio que n�o vai poder processar arquivo por estar desordenado
        If vDPK_Atu < vDPK_Ant Then
            MsgBox "Arquivo n�o pode ser processado. DPKs n�o est�o em ordem crescente!" & Chr(13) & _
                          "Verifique a linha " & i & " do arquivo original Excel, acerte os dados, reclassifique e " & Chr(13) & _
                          "gere arquivo .PRN novamente.", vbCritical, "Aten��o!"
            Exit Sub
        'Sen�o, Se DPK atual for igual ao DPK anterior e tiverem mesma montadora, por�m a seq. do anterior for > do que a seq. do atual
        '
'        ElseIf vDPK_Atu = vDPK_Ant And vMontAnt = vMontAtu And vSeqAnt >= vSeqAtu And vLinAnt = vLinAtu And Fl_Prim = False Then
'            MsgBox "Arquivo n�o pode ser processado. DPK, Mont. e Seq. n�o est�o em ordem crescente!" & Chr(13) & _
'                          "Verifique a linha " & i & " do arquivo original Excel, acerte os dados, reclassifique e " & Chr(13) & _
'                          "gere arquivo .PRN novamente.", vbCritical, "Aten��o!"
'            Exit Sub
        End If
        
        vDPK_Ant = vDPK_Atu
        vMontAnt = vMontAtu
'        vSeqAnt = vSeqAtu
        vLinAnt = vLinAtu
        Fl_Prim = False
    
    Next i
    
    'Se n�o for confirmado, retorna opera��o
    If MsgBox("Confirma o processamento deste arquivo?", vbYesNo, "Aten��o") = vbNo Then
        Exit Sub
    End If
    
    Screen.MousePointer = 11
    stbBarra.SimpleText = "Acertando cadastro de Aplica��es...."
    
    vNumLin = grdArquivo.Rows
    Fl_PrimLin = True
    Fl_Aux = False
    
    Set OraParameters = vBanco.Parameters
    
    OraParameters.Remove "ERRO": OraParameters.Add "ERRO", 0, 2
    OraParameters.Remove "TXT_ERRO": OraParameters.Add "TXT_ERRO", Null, 2
    
    For i = 1 To vNumLin - 1
             
      grdArquivo.Row = i          'Vai pegar informa��es da linha apontada no grid
      grdArquivo.Col = 4           'Vai pegar o c�digo DPK
      
      'Se estiver lendo a primeira linha do grid E ela n�o estiver em branco, atribui o DPK para as vari�veis vDPK_atu e vDPK_Ant
      If grdArquivo.Text <> "" And Fl_PrimLin = True Then
          vDPK_Ant = Val(grdArquivo.Text)
          vDPK_Atu = vDPK_Ant
          Fl_Aux = True
      'Se n�o for a 1a. linha  E ela n�o estiver em branco, vDPK_Atu vai ser igual ao DPK lido na linha atual
      ElseIf grdArquivo.Text <> "" And Fl_PrimLin = False Then
          vDPK_Atu = Val(grdArquivo.Text)
          'Seto ponteiro na linha anterior � que est� sendo lida para pegar o DPK anterior e gravar na vari�vel
          grdArquivo.Row = i - 1
          grdArquivo.Col = 4
          vDPK_Ant = Val(grdArquivo.Text)
          'Volto para linha atual
          grdArquivo.Row = i
      End If
      
      'Se a linha lida do grid n�o estiver em branco, vai processar informa��o
      If grdArquivo.Text <> "" Then
      
          grdArquivo.Col = 4
          OraParameters.Remove "COD_DPK": OraParameters.Add "COD_DPK", vDPK_Atu, 1
          
          grdArquivo.Col = 5
          OraParameters.Remove "DESCRICAO": OraParameters.Add "DESCRICAO", grdArquivo.Text, 1
          
          'Se DPK lido na linha atual for diferente (<>) do anterior OU for a primeira linha do grid
          If vDPK_Atu <> vDPK_Ant Or Fl_PrimLin = True Then
      
              'Vai deletar todas as aplica��es do DPK Atual, j� cadastradas no Oracle para depois cadastrar de novo
              'E vai altera descri��o do item
          
              vSql = "PRODUCAO.PCK_CAD620.PR_SALVA_PRIM(:COD_DPK, :DESCRICAO, :ERRO, :TXT_ERRO)"
              
              If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
                  Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
                  Screen.MousePointer = 0
                  stbBarra.SimpleText = ""
                  Exit Sub
              End If
              
              If OraParameters("ERRO").Value <> 0 Then
                  Call vVB_Generica_001.Informar("Erro no processamento da linha " & CStr(i) & "!" & Chr(13) & "Erro: " & OraParameters("ERRO").Value & " - " & OraParameters("TXT_ERRO").Value)
                  Screen.MousePointer = 0
                  stbBarra.SimpleText = ""
                  Exit Sub
              End If
                          
          End If
          
          'Vai gravar a aplica��o que consta na linha do grid (inclusive da linha lida onde dpk ant <> dpk atual
          grdArquivo.Col = 6
          OraParameters.Remove "CATEGORIA": OraParameters.Add "CATEGORIA", grdArquivo.Text, 1
          
          grdArquivo.Col = 1
          OraParameters.Remove "MONTADORA": OraParameters.Add "MONTADORA", grdArquivo.Text, 1
          
'          grdArquivo.Col = 7
'          OraParameters.Remove "SEQUENCIA": OraParameters.Add "SEQUENCIA", grdArquivo.Text, 1
          
          grdArquivo.Col = 2
          OraParameters.Remove "ORIGINAL": OraParameters.Add "ORIGINAL", grdArquivo.Text, 1
          
          grdArquivo.Col = 8
          OraParameters.Remove "APLIC_CATALOGO": OraParameters.Add "APLIC_CATALOGO", grdArquivo.Text, 1
          
            '------------------------------------------------------------------------------------------------------------
            'Converter a descri��o da aplica��o para cat�logo, para a descri��o normal (sem acentos, caracteres especiais
            'e colocar tudo em letra mai�scula) - Alethea - 01/08/05
            vDescApliCatalogo = grdArquivo.Text
            OraParameters.Remove "APLIC_COMPL":     OraParameters.Add "APLIC_COMPL", vDescApliCatalogo, 1
            OraParameters.Remove "APLIC_NORMAL":    OraParameters.Add "APLIC_NORMAL", "", 2
            vSql2 = "PRODUCAO.Pr_Ret_Caracteres_Validos(:APLIC_COMPL,:APLIC_NORMAL)"
            If vVB_Generica_001.ExecutaPl(vBanco, vSql2) <> "" Then
                Call vVB_Generica_001.Informar("Erro na transforma��o de caracteres, entre em contato com o SUPORTE!")
                Screen.MousePointer = 0
                stbBarra.SimpleText = ""
                Exit Sub
            End If
            vDescApliNormal = OraParameters("APLIC_NORMAL").Value
            
            OraParameters.Remove "APLICACAO": OraParameters.Add "APLICACAO", vDescApliNormal, 1
            
            '------------------------------------------------------------------------------------------------------------
          
          'Insere nova aplica��o
          'vSql = "PRODUCAO.PCK_CAD620.PR_SALVA_APLIC(:COD_DPK, :CATEGORIA, :MONTADORA, :SEQUENCIA, :ORIGINAL, :APLICACAO, :ERRO, :TXT_ERRO)"
          vSql = "PRODUCAO.PCK_CAD620.PR_SALVA_APLIC(:COD_DPK, :CATEGORIA, :MONTADORA, :ORIGINAL, " & _
                                                    ":APLICACAO, :APLIC_CATALOGO, :ERRO, :TXT_ERRO)"
          
          If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
              Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
              Screen.MousePointer = 0
              stbBarra.SimpleText = ""
              Exit Sub
          End If
          
          If OraParameters("ERRO").Value <> 0 Then
              Call vVB_Generica_001.Informar("Erro no processamento da linha " & CStr(i) & "!" & Chr(13) & "Erro: " & OraParameters("ERRO").Value & " - " & OraParameters("TXT_ERRO").Value)
              Screen.MousePointer = 0
              stbBarra.SimpleText = ""
              Exit Sub
          End If
       
      End If
      
      'Se a 1a. linha j� tiver sido lida, seta flag para ler pr�ximas
      If Fl_Aux = True And Fl_PrimLin = True Then
              Fl_PrimLin = False
      End If
       
            
    Next i
    
    Screen.MousePointer = 0
    stbBarra.SimpleText = "Dados processados com sucesso!"
    
    LIMPA_GRID
    fraDados.Visible = False
    txtMont.Visible = False
    txtDpk.Visible = False
    txtLin.Visible = False
    txtSeq.Visible = False
    fraLog.Visible = False
    
End Sub

Private Sub cmdLayout_Click()
    frmLayOut.Show
    
End Sub

Private Sub cmdLog_Click()
  
  Dim i As Integer
  Dim Arq As Integer
  
  Arq = FreeFile
  
  'Gera arquivo de log, sobrescrevendo o anterior
  'Open "C:\Temp\Log.txt" For Output As #Arq
  Open "C:\Cad620\Log.txt" For Output As #Arq
  
  For i = 0 To txtLog.ListCount
      If i = 0 Then
          Print #Arq, txtLog.List(i) & " DO ARQUIVO: " & vNomeArq
      Else
          Print #Arq, txtLog.List(i)
      End If
  Next i
  
  Close #Arq
  
  MsgBox "Arquivo gerado com sucesso!", vbInformation, "Aten��o!"
  
End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    
    Me.Top = 0
    Me.Left = 0
    
    txtMont.Visible = False
    txtMont.Text = ""
    txtDpk.Visible = False
    txtDpk.Text = ""
    txtDesc.Visible = False
    txtDesc.Text = ""
    txtLin.Visible = False
    txtLin.Text = ""
    txtSeq.Visible = False
    txtSeq.Text = ""
    
End Sub

'C.FELIPE.CASSIANI - 13/05/2015 - TI-1646 - Corre��o cadastro em massa - CAD620
Private Sub grdArquivo_DblClick()
    'Guarda a linha e a coluna onde foi dado o click
    vGuardaCol = grdArquivo.Col
    vGuardaLin = grdArquivo.Row
    
    'Caso for dado duplo clique, vai verificar se a linha necessita de acertos
    grdArquivo.Col = 0
    If Mid(grdArquivo.Text, 1, 1) = "*" Then
            'Volta posicionamento para a coluna original
            grdArquivo.Col = vGuardaCol
            'De acordo com a coluna clicada, vai disponibilizar o campo para o acerto
            If grdArquivo.Col = 1 Then
                    txtMont.Visible = True      'Montadora
                    txtMont.SetFocus
            ElseIf grdArquivo.Col = 4 Then      'DPK
                    txtDpk.Visible = True
                    txtDpk.SetFocus
            ElseIf grdArquivo.Col = 5 Then      'Descri��o
                    txtDesc.Visible = True
                    txtDesc.SetFocus
            ElseIf grdArquivo.Col = 6 Then      'Linha
                    txtLin.Visible = True
                    txtLin.SetFocus
'            ElseIf grdArquivo.Col = 7 Then      'Sequ�ncia
'                    txtSeq.Visible = True
'                    txtSeq.SetFocus
            End If
    End If
End Sub

Private Sub txtDesc_KeyPress(KeyAscii As Integer)
    KeyAscii = Maiusculo(KeyAscii)
End Sub

Private Sub txtDesc_LostFocus()
    Dim vDescGrid As String
    Dim vDescNovo As String
    
    Dim vQtdErro As Integer
    
    'Atribui a var. a Linha do grid e Linha digitada
    grdArquivo.Col = 5
    grdArquivo.Row = vGuardaLin
    vDescGrid = grdArquivo.Text
    vDescNovo = txtLin
    
    'Verifica a categoria (linha) digitada
    If Trim(txtDesc) = "" Then
        MsgBox "Descri��o n�o pode estar em branco", vbCritical, "Aten��o"
        txtDesc.Visible = True
        txtDesc.SetFocus
        Exit Sub
    Else
        'Vai voltar na posi��o original no grid p/ poder alterar o dado
        grdArquivo.Col = vGuardaCol
        grdArquivo.Row = vGuardaLin
        grdArquivo.Text = txtDesc
        'Vai diminuir de 1, a qtd. de erros da linha
        grdArquivo.Col = 0
        vQtdErro = Val(Mid(grdArquivo, 2, 1)) - 1
        'Se a qtd. de erros restante for = 0, ent�o acerta a 1a. coluna do grid para que n�o apare�a mais o asterisco
        If vQtdErro = 0 Then
            grdArquivo.Text = Format(Mid(grdArquivo, 5), "0000")
        'Sen�o, vai acertar a 1a. coluna para a qtd. atual de erros
        Else
            grdArquivo.Text = "*" & Format(vQtdErro, "0") & "* " & Format(Mid(grdArquivo, 5), "0000")
        End If
        txtDesc.Text = ""
        txtDesc.Visible = False
    End If

End Sub

Private Sub txtDpk_KeyPress(KeyAscii As Integer)
    KeyAscii = Numerico(KeyAscii)
End Sub

Private Sub txtDpk_LostFocus()
    Dim vQtdErro As Integer
    Dim vDPKAnterior As Long
    Dim vDPKGrid As Long
    Dim vDPKNovo As Long
    
    'Atribui a var. o cod DPK do grid e DPK digitado
    grdArquivo.Col = 4
    grdArquivo.Row = vGuardaLin
    vDPKGrid = Val(grdArquivo.Text)
    vDPKNovo = Val(txtDpk)
    
    'Verifica DPK digitado
    Set OraParameters = vBanco.Parameters
    
    OraParameters.Remove "COD_MONTADORA": OraParameters.Add "COD_MONTADORA", 0, 1
    OraParameters.Remove "COD_DPK": OraParameters.Add "COD_DPK", vDPKNovo, 1
    OraParameters.Remove "QTD_MONTADORA": OraParameters.Add "QTD_MONTADORA", 0, 2
    OraParameters.Remove "QTD_DPK": OraParameters.Add "QTD_DPK", 0, 2
    OraParameters.Remove "ERRO": OraParameters.Add "ERRO", 0, 2
    
    vSql = "PRODUCAO.PCK_CAD620.PR_BUSCA_DADOS(:COD_MONTADORA, :COD_DPK, :QTD_MONTADORA, :QTD_DPK, :ERRO)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
        Exit Sub
    End If
    
    If OraParameters("ERRO").Value <> 0 Then
        Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
        Exit Sub
    End If
    
    If OraParameters("QTD_DPK").Value = 0 Then
        MsgBox "Este DPK n�o existe, favor digitar outro c�digo!", vbCritical, "Aten��o"
        txtDpk.Visible = True
        txtDpk.SetFocus
        Exit Sub
    'Se a Montadora digitada for o mesmo que existe no grid, n�o altera nada
    ElseIf vDPKGrid = vDPKNovo Then
        MsgBox "Mesmo DPK!", vbCritical, "Aten��o"
        txtDpk.Visible = True
        txtDpk.SetFocus
        Exit Sub
    End If
    
    'Vai verificar se o c�digo PK digitado n�o � menor do que o existente na linha anterior (pois n�o pode-se tirar o arquivo da ordem)
    grdArquivo.Col = 4          'Posiciona na coluna do DPK
    'Enquanto o conte�do do campo de DPK da linha anterior for branco, volta mais uma linha pois preciso saber qual foi o �ltimo c�d. DPK antes do digitado
    grdArquivo.Row = grdArquivo.Row - 1
    Do While grdArquivo.Text = ""
            grdArquivo.Row = grdArquivo.Row - 1     'Volta uma linha
    Loop
    vDPKAnterior = Val(grdArquivo.Text)
    'N�o permite que o DPK digitado seja menor do que o da linha anterior
    If Val(txtDpk) < vDPKAnterior Then
            MsgBox "O DPK digitado deve ser maior do que a da linha anterior, pois o arquivo n�o pode perder sua ordem.", vbCritical, "Aten��o!"
            txtDpk.SetFocus
            Exit Sub
    End If
            
    'Vai voltar na posi��o original no grid p/ poder alterar o dado
    grdArquivo.Col = vGuardaCol
    grdArquivo.Row = vGuardaLin
    grdArquivo.Text = Format(txtDpk, "00000")
    'Vai diminuir de 1, a qtd. de erros da linha
    grdArquivo.Col = 0
    vQtdErro = Val(Mid(grdArquivo, 2, 1)) - 1
    'Se a qtd. de erros restante for = 0, ent�o acerta a 1a. coluna do grid para que n�o apare�a mais o asterisco
    If vQtdErro = 0 Then
        grdArquivo.Text = Format(Mid(grdArquivo, 5), "0000")
    'Sen�o, vai acertar a 1a. coluna para a qtd. atual de erros
    Else
        grdArquivo.Text = "*" & Format(vQtdErro, "0") & "* " & Format(Mid(grdArquivo, 5), "0000")
    End If
    txtDpk.Text = ""
    txtDpk.Visible = False
End Sub


Private Sub txtLin_KeyPress(KeyAscii As Integer)
    KeyAscii = Maiusculo(KeyAscii)
End Sub

Private Sub txtLin_LostFocus()
    Dim vLinGrid As String
    Dim vLinNovo As String
    
    Dim vQtdErro As Integer

    'Atribui a var. a Linha do grid e Linha digitada
    grdArquivo.Col = 6
    grdArquivo.Row = vGuardaLin
    vLinGrid = grdArquivo.Text
    vLinNovo = txtLin
    
    'Verifica a categoria (linha) digitada
    If txtLin <> "P" And txtLin <> "L" And txtLin <> "U" And txtLin <> "M" And txtLin <> "A" Then
        MsgBox "Linha deve ser igual a: 'P','L','U','M' ou 'A'", vbCritical, "Aten��o"
        txtLin.Visible = True
        txtLin.SetFocus
        Exit Sub
    'Se a Linha digitada for o mesmo que existe no grid, n�o altera nada
    ElseIf vLinGrid = vLinNovo Then
        MsgBox "Mesma Linha!", vbCritical, "Aten��o"
        txtLin.Visible = True
        txtLin.SetFocus
        Exit Sub
    Else
        'Vai voltar na posi��o original no grid p/ poder alterar o dado
        grdArquivo.Col = vGuardaCol
        grdArquivo.Row = vGuardaLin
        grdArquivo.Text = txtLin
        'Vai diminuir de 1, a qtd. de erros da linha
        grdArquivo.Col = 0
        vQtdErro = Val(Mid(grdArquivo, 2, 1)) - 1
        'Se a qtd. de erros restante for = 0, ent�o acerta a 1a. coluna do grid para que n�o apare�a mais o asterisco
        If vQtdErro = 0 Then
                grdArquivo.Text = Format(Mid(grdArquivo, 5), "0000")
        'Sen�o, vai acertar a 1a. coluna para a qtd. atual de erros
        Else
                grdArquivo.Text = "*" & Format(vQtdErro, "0") & "* " & Format(Mid(grdArquivo, 5), "0000")
        End If
        txtLin.Text = ""
        txtLin.Visible = False
    End If

End Sub


Private Sub txtLog_KeyPress(KeyAscii As Integer)
    KeyAscii = 0
End Sub

Private Sub txtMont_KeyPress(KeyAscii As Integer)
    KeyAscii = Numerico(KeyAscii)
End Sub

Private Sub txtMont_LostFocus()
    Dim vQtdErro As Integer
    Dim vMontGrid As Long
    Dim vMontNovo As Long

    'Atribui a var. o cod DPK do grid e DPK digitado
    grdArquivo.Col = 1
    grdArquivo.Row = vGuardaLin
    vMontGrid = Val(grdArquivo.Text)
    vMontNovo = Val(txtMont)
    
    Set OraParameters = vBanco.Parameters
    
    OraParameters.Remove "COD_MONTADORA": OraParameters.Add "COD_MONTADORA", Trim(vMontNovo), 1
    OraParameters.Remove "COD_DPK": OraParameters.Add "COD_DPK", 0, 1
    OraParameters.Remove "QTD_MONTADORA": OraParameters.Add "QTD_MONTADORA", 0, 2
    OraParameters.Remove "QTD_DPK": OraParameters.Add "QTD_DPK", 0, 2
    OraParameters.Remove "ERRO": OraParameters.Add "ERRO", 0, 2
    
    vSql = "PRODUCAO.PCK_CAD620.PR_BUSCA_DADOS(:COD_MONTADORA, :COD_DPK, :QTD_MONTADORA, :QTD_DPK, :ERRO)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
        Exit Sub
    End If
    
    If OraParameters("ERRO").Value <> 0 Then
        Call vVB_Generica_001.Informar("Erro no processamento, entre em contato com o SUPORTE!")
        Exit Sub
    End If
    
    If OraParameters("QTD_MONTADORA").Value = 0 Then
        MsgBox "Esta montadora n�o existe, favor digitar outro c�digo!", vbCritical, "Aten��o"
        txtMont.Visible = True
        txtMont.SetFocus
        Exit Sub
    'Se a Montadora digitada for o mesmo que existe no grid, n�o altera nada
    ElseIf vMontGrid = vMontNovo Then
        MsgBox "Mesma Montadora!", vbCritical, "Aten��o"
        txtMont.Visible = True
        txtMont.SetFocus
        Exit Sub
    Else
        'Vai voltar na posi��o original no grid p/ poder alterar o dado
        grdArquivo.Col = vGuardaCol
        grdArquivo.Row = vGuardaLin
        grdArquivo.Text = Format(txtMont, "00")
        'Vai diminuir de 1, a qtd. de erros da linha
        grdArquivo.Col = 0
        vQtdErro = Val(Mid(grdArquivo, 2, 1)) - 1
        'Se a qtd. de erros restante for = 0, ent�o acerta a 1a. coluna do grid para que n�o apare�a mais o asterisco
        If vQtdErro = 0 Then
            grdArquivo.Text = Format(Mid(grdArquivo, 5), "0000")
        'Sen�o, vai acertar a 1a. coluna para a qtd. atual de erros
        Else
            grdArquivo.Text = "*" & Format(vQtdErro, "0") & "* " & Format(Mid(grdArquivo, 5), "0000")
        End If
        txtMont.Text = ""
        txtMont.Visible = False
    End If

End Sub

Private Sub txtSeq_KeyPress(KeyAscii As Integer)
    KeyAscii = Numerico(KeyAscii)
End Sub

'C.FELIPE.CASSIANI - 13/05/2015 - TI-1646 - Corre��o cadastro em massa - CAD620
'Private Sub txtSeq_LostFocus()
'    Dim vSeqGrid As Long
'    Dim vSeqNovo As Long
'
'    Dim vQtdErro As Integer
'
'    'Atribui a var. o cod DPK do grid e DPK digitado
'    grdArquivo.Col = 7
'    grdArquivo.Row = vGuardaLin
'    vSeqGrid = Val(grdArquivo.Text)
'    vSeqNovo = Val(txtSeq)
'
'    'Verifica sequ�ncia digitada
'    If Val(txtSeq) < 1 Or Val(txtSeq) > 9 Then
'        MsgBox "Sequ�ncia deve ser igual a 1,2,3,4,5,6,7,8 ou 9", vbCritical, "Aten��o"
'        txtSeq.Visible = True
'        txtSeq.SetFocus
'        Exit Sub
'    'Se a Sequ6encia digitada for o mesmo que existe no grid, n�o altera nada
'    ElseIf vSeqGrid = vSeqNovo Then
'        MsgBox "Mesma Sequ�ncia!", vbCritical, "Aten��o"
'        txtSeq.Visible = True
'        txtSeq.SetFocus
'        Exit Sub
'    Else
'        'Vai voltar na posi��o original no grid p/ poder alterar o dado
'         grdArquivo.Col = vGuardaCol
'         grdArquivo.Row = vGuardaLin
'         grdArquivo.Text = Format(txtSeq, "0")
'         'Vai diminuir de 1, a qtd. de erros da linha
'         grdArquivo.Col = 0
'         vQtdErro = Val(Mid(grdArquivo, 2, 1)) - 1
'         'Se a qtd. de erros restante for = 0, ent�o acerta a 1a. coluna do grid para que n�o apare�a mais o asterisco
'         If vQtdErro = 0 Then
'              grdArquivo.Text = Format(Mid(grdArquivo, 5), "0000")
'         'Sen�o, vai acertar a 1a. coluna para a qtd. atual de erros
'         Else
'              grdArquivo.Text = "*" & Format(vQtdErro, "0") & "* " & Format(Mid(grdArquivo, 5), "0000")
'         End If
'         txtSeq.Text = ""
'         txtSeq.Visible = False
'    End If
'
'End Sub


Sub LIMPA_GRID()

    Dim i
    
    'Limpa dados do grid
    For i = 1 To grdArquivo.Rows - 2
            grdArquivo.RemoveItem 1
    Next i
        
End Sub
