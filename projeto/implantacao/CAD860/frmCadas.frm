VERSION 4.00
Begin VB.Form frmCadAutom 
   Caption         =   "CAD860 - Cadastro de Data de Primeira Entrada - AUTOM�TICO"
   ClientHeight    =   6615
   ClientLeft      =   2370
   ClientTop       =   810
   ClientWidth     =   6825
   Height          =   7020
   Left            =   2310
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   6615
   ScaleWidth      =   6825
   Top             =   465
   Width           =   6945
   Begin Threed.SSFrame SSFrame2 
      Height          =   6270
      Left            =   405
      TabIndex        =   0
      Top             =   105
      Width           =   5955
      _Version        =   65536
      _ExtentX        =   10504
      _ExtentY        =   11060
      _StockProps     =   14
      ForeColor       =   12582912
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin Threed.SSFrame SSFrame3 
         Height          =   1275
         Left            =   225
         TabIndex        =   1
         Top             =   150
         Width           =   5475
         _Version        =   65536
         _ExtentX        =   9657
         _ExtentY        =   2249
         _StockProps     =   14
         Caption         =   "Arquivo"
         ForeColor       =   12582912
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Begin VB.Label lblHelp1 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H0000FFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Abrir Arquivo"
            ForeColor       =   &H80000008&
            Height          =   225
            Left            =   135
            TabIndex        =   4
            Top             =   960
            Visible         =   0   'False
            Width           =   930
         End
         Begin Threed.SSCommand cmdArquivo 
            Height          =   585
            Left            =   120
            TabIndex        =   3
            Top             =   345
            Width           =   795
            _Version        =   65536
            _ExtentX        =   1402
            _ExtentY        =   1032
            _StockProps     =   78
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Picture         =   "frmCadas.frx":0000
         End
         Begin MSComDlg.CommonDialog controle 
            Left            =   120
            Top             =   240
            _ExtentX        =   847
            _ExtentY        =   847
            _Version        =   393216
            DefaultExt      =   "*.PRN"
            InitDir         =   "C:\"
            MaxFileSize     =   32000
         End
         Begin VB.Label lblArquivo 
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H00C00000&
            Height          =   330
            Left            =   990
            TabIndex        =   2
            Top             =   585
            Width           =   4320
         End
      End
      Begin Threed.SSCommand cmdLayout 
         Height          =   630
         Left            =   4005
         TabIndex        =   13
         Top             =   5145
         Width           =   690
         _Version        =   65536
         _ExtentX        =   1217
         _ExtentY        =   1111
         _StockProps     =   78
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "frmCadas.frx":031A
      End
      Begin VB.Label lblMsg3 
         AutoSize        =   -1  'True
         Caption         =   "Mensagem"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00004080&
         Height          =   240
         Left            =   150
         TabIndex        =   12
         Top             =   4740
         Width           =   1155
      End
      Begin VB.Label lblMsg2 
         AutoSize        =   -1  'True
         Caption         =   "Mensagem"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000080&
         Height          =   240
         Left            =   150
         TabIndex        =   11
         Top             =   4485
         Width           =   1155
      End
      Begin VB.Label lblMsg 
         AutoSize        =   -1  'True
         Caption         =   "Mensagem"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   240
         Left            =   150
         TabIndex        =   10
         Top             =   4245
         Width           =   1155
      End
      Begin MSGrid.Grid grdArquivo 
         Height          =   2700
         Left            =   165
         TabIndex        =   9
         Top             =   1530
         Width           =   5595
         _Version        =   65536
         _ExtentX        =   9869
         _ExtentY        =   4763
         _StockProps     =   77
         ForeColor       =   8388608
         BackColor       =   16777215
         Cols            =   6
      End
      Begin Threed.SSCommand cmdVoltar 
         Height          =   630
         Left            =   4995
         TabIndex        =   8
         Top             =   5145
         Width           =   690
         _Version        =   65536
         _ExtentX        =   1217
         _ExtentY        =   1111
         _StockProps     =   78
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "frmCadas.frx":0634
      End
      Begin Threed.SSCommand cmdConfirma 
         Height          =   630
         Left            =   3165
         TabIndex        =   7
         Top             =   5145
         Width           =   690
         _Version        =   65536
         _ExtentX        =   1217
         _ExtentY        =   1111
         _StockProps     =   78
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "frmCadas.frx":094E
      End
      Begin VB.Label lblHelp2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H0000FFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Gerar Arq.de Log"
         ForeColor       =   &H80000008&
         Height          =   225
         Left            =   2340
         TabIndex        =   6
         Top             =   5775
         Visible         =   0   'False
         Width           =   1245
      End
      Begin Threed.SSCommand cmdLog 
         Height          =   630
         Left            =   2325
         TabIndex        =   5
         Top             =   5145
         Width           =   690
         _Version        =   65536
         _ExtentX        =   1217
         _ExtentY        =   1111
         _StockProps     =   78
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "frmCadas.frx":0C68
      End
   End
End
Attribute VB_Name = "frmCadAutom"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Public I As Integer                'Ponteiro / Contador
Dim vNomeArq As String
Dim varLinhaArquivo As String   'Recebe a linha lida do arquivo texto
Dim fl_Log As Boolean
Dim vGuardaCol As Integer
Dim vGuardaLin As Long

Dim vFL_DPK_OK As Boolean       'Indica se DPK est� Ok ou n�o (se n�o estiver, nem continua demais valida��es
Dim vCD As String           'Vai guardar o CD lido do arquivo
Dim vDPK As String          'Vai guardar o DPK lido do arquivo
Dim vDATA As String         'Vai guardar a DATA DE PRIM.ENTRADA lido do arquivo
Dim vTRAN As String         'Vai guardar o tipo da transa��o (Incl./Alter.)

Dim vFl_Erro As Boolean         'Vai indicar se a grava��o de determinado item, deu erro ou n�o
Sub LIMPA_GRID()
Dim L As Long
Dim C As Integer
Dim vTotLin As Long

vTotLin = grdArquivo.Rows - 1

'Varre todas as linhas do grid
For L = 1 To vTotLin
    'Se for a �ltima linha a ser removida, deleta os campos de cada coluna
    If L = vTotLin Then
        For C = 0 To grdArquivo.Cols - 1
            grdArquivo.Col = C
            grdArquivo.Text = ""
        Next C
    'Sen�o remove o item
    Else
        grdArquivo.RemoveItem 1
    End If
Next L

End Sub







Private Sub cmdArquivo_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp1.Visible = True
End Sub

Private Sub cmdConfirma_Click()

Dim vObs As String            'Guarda o conte�do da coluna de Observa��es
Dim vReg_Gravado As Long      'Guarda a qtd. de itens gravados
Dim vReg_Nao_Gravado As Long  'Guarda a qtd. de itens n�o gravados
Dim vLinSemObs As Long        'Guarda a qtd.de linhas sem observa��o
Dim objconexao As Object      'Guarda resultado do select na LOJA_CONEXAO
Dim vCommit As Long           'Contador de transa��es para fazer commit
Dim K As Long                 'Auxiliar de bind


If MsgBox("Confirma inclus�o/altera��o da Data de Prim.Entrada?", vbYesNo, "CAD860") = vbYes Then

    Screen.MousePointer = 11

    'Verifica se o grid n�o est� em branco
    grdArquivo.Row = 1
    grdArquivo.Col = 0
    If grdArquivo.Text = "" Then
        Screen.MousePointer = 0
        MsgBox "Nenhum dado poder� ser alterado!", vbCritical, "Aten��o!"
        Exit Sub
    End If

    'Primeiro vai percorrer todo o grid p/ ver se tem itens a serem gravados
    CmdPl = ""
    vLinSemObs = 0
    For I = 1 To grdArquivo.Rows - 1
        grdArquivo.Row = I
        grdArquivo.Col = 5
        vObs = grdArquivo.Text
        'Se a coluna de Observa��o n�o contiver nenhuma mensagem de inconsist�ncia vai gravar
        If vObs = "" Then
            vLinSemObs = vLinSemObs + 1
        End If
    Next I
    If vLinSemObs = 0 Then
        Screen.MousePointer = 0
        MsgBox "Nenhum dado poder� ser alterado!", vbCritical, "Aten��o!"
        Exit Sub
    End If
    
    'Se houverem itens a serem gravados, vai iniciar todo o processo
    'Zera contador de linhas gravadas
    vReg_Gravado = 0
    vReg_Nao_Gravado = 0
    vCommit = 0
    K = 0
    CmdPl = "Begin "

    'Pega linha a linha do grid para gravar as que cont�m campo de Observa��o <> ""
    For I = 1 To grdArquivo.Rows - 1
        
        grdArquivo.Row = I
        grdArquivo.Col = 5
        vObs = Trim(grdArquivo.Text)
        
        'Se a coluna de Observa��o n�o contiver nenhuma mensagem de inconsist�ncia vai gravar
        If vObs = "" Then
            
            'Alimenta vari�veis com os dados do grid
            grdArquivo.Col = 1
            vCD = Format(Trim(grdArquivo.Text), "00")
            grdArquivo.Col = 2
            vDPK = Trim(grdArquivo.Text)
            grdArquivo.Col = 3
            vDATA = Trim(grdArquivo.Text)
            
            K = K + 1
            
            db.Parameters.Remove "CD" & K: db.Parameters.Add "CD" & K, Val(vCD), 1
            db.Parameters.Remove "DPK" & K: db.Parameters.Add "DPK" & K, Val(vDPK), 1
            db.Parameters.Remove "DATA" & K: db.Parameters.Add "DATA" & K, vDATA, 1
            db.Parameters.Remove "Cod_Errora": db.Parameters.Add "Cod_Errora", 0, 2
            db.Parameters.Remove "Txt_Errora": db.Parameters.Add "Txt_Errora", "", 2
            
            
            CmdPl = CmdPl & " Update ITEM_PRIMEIRA_ENTRADA " & _
                            "    Set Dt_Primeira_Entrada = TO_DATE(:DATA" & K & ",'DD/MM/RR') " & _
                            "  Where Cod_Loja = :CD" & K & " " & _
                            "    And Cod_Dpk = :DPK" & K & "; "
            CmdPl = CmdPl & " If SQL%NOTFOUND Then " & _
                                " Insert into ITEM_PRIMEIRA_ENTRADA " & _
                                " (Cod_Loja, Cod_Dpk, Dt_Primeira_Entrada) " & _
                                " Values (:CD" & K & ", :DPK" & K & ", " & _
                                " TO_DATE(:DATA" & K & ",'DD/MM/RR')); " & _
                            " End If;"
            
            vCommit = vCommit + 1
            vReg_Gravado = vReg_Gravado + 1
            
            'Commita de 20 em 20 transa��es
            If vCommit = 20 Then
                CmdPl = CmdPl & "Commit; " & _
                                "Exception When Others Then Rollback; " & _
                                ":Cod_Errora := SQLCODE; " & _
                                ":Txt_Errora := SQLERRM; " & _
                                "End; "
                'Executa transa��es no CD
                db.ExecuteSQL CmdPl
                
                If Val(IIf((IsNull(db.Parameters("Cod_Errora")) Or db.Parameters("Cod_Errora") = ""), 0, db.Parameters("Cod_Errora"))) <> 0 Then
                    Screen.MousePointer = 0
                    MsgBox db.Parameters("Cod_Errora") & " - " & db.Parameters("Txt_Errora") & Chr(13) & _
                            "Erro ao ativar item no CD " & vCD & "! Ligue p/ Depto.Sistemas."
                    vFl_Erro = True
                End If
                vCommit = 0
                CmdPl = "Begin "
                K = 1
            End If
        
        Else
            vReg_Nao_Gravado = vReg_Nao_Gravado + 1
        End If
            
    Next I

    CmdPl = CmdPl & "Commit; " & _
                    "Exception When Others Then Rollback; " & _
                    ":Cod_Errora := SQLCODE; " & _
                    ":Txt_Errora := SQLERRM; " & _
                    "End; "
    'Executa transa��es no CD
    db.ExecuteSQL CmdPl
    If Val(IIf((IsNull(db.Parameters("Cod_Errora")) Or db.Parameters("Cod_Errora") = ""), 0, db.Parameters("Cod_Errora"))) <> 0 Then
        Screen.MousePointer = 0
        MsgBox db.Parameters("Cod_Errora") & " - " & db.Parameters("Txt_Errora") & Chr(13) & _
                "Erro ao incluir/alterar datas! Ligue p/ Depto.Sistemas."
        vFl_Erro = True
    End If
    
    Screen.MousePointer = 0
    MsgBox vReg_Gravado & IIf(vReg_Gravado = 1, " Item teve ", " Itens tiveram ") & "sua Dt.Prim.Entrada Inclu�da/Alterada!", vbInformation, "Aten��o!"
    lblMsg2.Visible = True
    lblMsg2 = vReg_Gravado & IIf(vReg_Gravado = 1, " Data Inclu�da/Alterada!", " Datas Incluidas/Alteradas!")
    lblMsg3.Visible = True
    lblMsg3 = vReg_Nao_Gravado & IIf(vReg_Nao_Gravado = 1, " Data N�o Inclu�da/Alterada!", " Datas N�o Inclu�das/Alteradas!")
    
End If

End Sub
Private Sub cmdConfirma_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp2.Left = 3165
    lblHelp2.Visible = True
    lblHelp2.Caption = "Gravar Dados"
End Sub



Private Sub cmdLayout_Click()
    frmLayOut.Show vbModal
End Sub

Private Sub cmdLayout_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp2.Left = 4005
    lblHelp2.Visible = True
    lblHelp2.Caption = "Lay-Out Arq."
End Sub


Private Sub cmdLog_Click()

Dim J As Long           'Ponteiro de caracter no campo observa��o
Dim vTamObs As Long     'Guarda a qtd. de caracteres da coluna Observa��o
Dim sOBS As String      'Guarda o conte�do da coluna de Observa��es
Dim sDPK As String      'Guarda o c�digo de DPK para impress�o
Dim sDATA As String     'Guarda a Data
Dim sCD As String       'Guarda o c�digo loja para impress�o
    
    
If MsgBox("Confirma gera��o do arquivo de log?", vbYesNo, "CAD750") = vbYes Then

    'Gera arquivo de log com o nome LODDMMAA.TXT
    'Open "C:\Arquivos-Alethea\CAD860\LOG\Lo" & Mid(vDt_Cad, 1, 2) & _
                        Mid(vDt_Cad, 4, 2) & Mid(vDt_Cad, 7, 2) & ".txt" For Output As #1
    Open "C:\CAD860\LOG\Lo" & Mid(vDt_Cad, 1, 2) & _
                        Mid(vDt_Cad, 4, 2) & Mid(vDt_Cad, 7, 2) & ".txt" For Output As #1
    
    Print #1, "LOG DO ARQUIVO: " & vNomeArq
    Print #1, ""
    Print #1, "CD DPK   Data      Observa��es"
    Print #1, "-- ----- --------- -------------------------------------------------------------"
    '          01 54357 26-AUG-02
    
    'Pega linha a linha do grid para gravar as que cont�m campo de Observa��o <> ""
    For I = 1 To grdArquivo.Rows - 1
        
        grdArquivo.Row = I
        
        grdArquivo.Col = 1
        sCD = Trim(grdArquivo.Text)        'Pega o c�digo loja
        grdArquivo.Col = 2
        sDPK = Trim(grdArquivo.Text)       'Pega o c�digo do DPK
        grdArquivo.Col = 3
        sDATA = Trim(grdArquivo.Text)       'Pega a situa��o do item
        grdArquivo.Col = 5
        sOBS = Trim(grdArquivo.Text)        'Pega as Observa��es
        
        'Se a coluna de Observa��o n�o estiver em branco
        If sOBS <> "" Then
            
            'Zero ponteiro que vai me indicar que caracter no campo obs. estou lendo
            J = 0
            'Pego o tamanho da observa��o para esta linha
            vTamObs = CLng(Len(sOBS))
            
            'Se o campo de observa��o contiver mais de 36 caracteres, o arquivo de log ter�
            'que conter mais de 1 linha de mensagens para aquele item
            'Conto 36 pois antes da Obs., a 1a. linha ir� conter o CD-DPK  -SIT-C�d.F�br.EDI...(44 caracteres)
            'E para a impress�o correta do arquivo s� pode ter 80 colunas
            
            'Se for menor ou igual a 61, gravo a linha no arquivo normalmente
            If vTamObs <= 61 Then
                Print #1, String(2 - Len(sCD), " ") & sCD & " " & _
                          String(5 - Len(sDPK), " ") & sDPK & " " & _
                          sDATA & " " & sOBS
            'Sen�o
            Else
                'Imprimo os primeiros 61 caracteres
                Print #1, String(2 - Len(sCD), " ") & sCD & " " & _
                          String(5 - Len(sDPK), " ") & sDPK & " " & _
                          sDATA & " " & Mid(sOBS, 1, 61)
                
                'Tamanho agora vai ser igual ao tamanho original - 61
                vTamObs = vTamObs - 61
                'Indico de que caracter vou come�ar a ler no cpo. Observa��o
                J = J + 62
                
                'Enquanto o tamanho for maior que 0
                Do While vTamObs > 0
                    'Se tamanho maior que 61, imprime os pr�ximos 61 caracteres e atualiza tam.e ponteiro
                    If vTamObs > 61 Then
                        Print #1, Space(19) & Mid(sOBS, J, 61)
                        vTamObs = vTamObs - 61
                        J = J + 62
                    'Se tamanho menor que 61, imprime os caracteres que faltam
                    Else
                        Print #1, Space(19) & Mid(sOBS, J, vTamObs)
                        vTamObs = 0
                    End If
                Loop
            End If
            
        End If
            
    Next I

    Print #1, ""
    Print #1, "***************** FIM DO ARQUIVO DE LOG ***********************"
    
    Close #1

    MsgBox "Arquivo gerado com sucesso!", vbInformation, "Aten��o!"


End If

End Sub

Private Sub cmdLog_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp2.Left = 2325
    lblHelp2.Visible = True
    lblHelp2.Caption = "Gerar Arq.de Log"
End Sub


Private Sub cmdVoltar_Click()
    Unload Me
End Sub

Private Sub cmdVoltar_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp2.Left = 4965
    lblHelp2.Visible = True
    lblHelp2.Caption = "Voltar"
End Sub


Private Sub Form_Load()
    
    grdArquivo.Visible = False
    lblHelp1.Visible = False
    lblHelp2.Visible = False
    lblMsg.Visible = False
    lblMsg2.Visible = False
    lblMsg3.Visible = False

End Sub



Private Sub lblHelp1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp1.Visible = False
End Sub


Private Sub cmdArquivo_Click()
    
    'Vari�veis locais
    
    On Error GoTo Trata_Erro
    
    Screen.MousePointer = 11
    Call LIMPA_GRID
    lblMsg = ""
    lblMsg2 = ""
    lblMsg3 = ""
    
    'Indica que o controle ser� usado para abrir o arquivo
    controle.ShowOpen
    lblArquivo.Caption = controle.filename
    vNomeArq = lblArquivo

    'Verifica se foi indicado o diret�rio e nome do arquivo a ser lido
    If lblArquivo.Caption = "" Then
        MsgBox "Indique o arquivo a ser aberto", , MSG_TIT
        Screen.MousePointer = 0
        Exit Sub
    End If


    '/////////////////////////
    'MONTA O GRID
    '/////////////////////////
    grdArquivo.Visible = True
    grdArquivo.Row = 0
    
    grdArquivo.Col = 0
    grdArquivo.ColWidth(0) = 450
    grdArquivo.Text = " Reg"
    
    grdArquivo.Col = 1
    grdArquivo.ColWidth(1) = 350
    grdArquivo.ColAlignment(1) = 2
    grdArquivo.Text = " CD"
    
    grdArquivo.Col = 2
    grdArquivo.ColWidth(2) = 500
    grdArquivo.ColAlignment(2) = 2
    grdArquivo.Text = " Dpk"
    
    grdArquivo.Col = 3
    grdArquivo.ColWidth(3) = 1500
    grdArquivo.ColAlignment(3) = 2
    grdArquivo.Text = " Dt.Prim.Entrada"
    
    'Para discriminar se vai ser altera��o ou inclus�o
    grdArquivo.Col = 4
    grdArquivo.ColWidth(4) = 450
    grdArquivo.ColAlignment(4) = 2
    grdArquivo.Text = " I/A"
    
    grdArquivo.Col = 5
    grdArquivo.ColWidth(5) = 10000
    grdArquivo.Text = "Observa��o"
    
    
    
    '//////////////////////////
    'LEITURA DO ARQUIVO
    '//////////////////////////
    Open vNomeArq For Input As #1
    
    'Inicializa contadores de linhas do grid
    I = 0
    lblMsg = ""
    lblMsg.Visible = True
    
    'L� enquanto n�o for final de arquivo
    Do While Not EOF(1)
    
        I = I + 1
        
        lblMsg = "CARREGANDO ...." & I
        lblMsg.Refresh
        
        'Inicializa flags e vari�veis
        vCD = ""
        vDPK = ""
        vDATA = ""
        vTRAN = ""
        vFL_DPK_OK = True
        
        'L� registro - linha - do arquivo, joga na vari�vel  e seta ponteiro para o pr�ximo
        Line Input #1, varLinhaArquivo
        'Se linha lida for em branco, sai do loop
        If Mid(varLinhaArquivo, 1, 16) = "" Then
            Exit Do
        End If
        
        'Qtd. Linhas do grid ser� correspondente � qtd. de linhas lidas do arquivo texto
        grdArquivo.Rows = I + 1
        grdArquivo.Row = I
        
        '1a Coluna vai receber a numera��o de registros lidos do arquivo
        grdArquivo.Col = 0
        grdArquivo.Text = Format(I, "0000")
        
        'Atribui valores da linha do arquivo para as vari�veis
        vCD = Format(Trim(Mid(varLinhaArquivo, 1, 2)), "00")
        vDPK = Trim(Mid(varLinhaArquivo, 3, 5))
        vDATA = Trim(Mid(varLinhaArquivo, 8, 8))
        
        'Valida��es gerais com o CD, DPK e SITUA��O que vieram no arq., verificando se n�o est�o inv�lidos
        If vCD = "" Or Val(vCD) = 0 Then
            grdArquivo.Col = 5
            grdArquivo.Text = "CD Inv�lido! "
            vFL_DPK_OK = False
        ElseIf vDPK = "" Or Val(vDPK) = 0 Then
            grdArquivo.Col = 5
            grdArquivo.Text = "DPK Inv�lido! "
            vFL_DPK_OK = False
        ElseIf vDATA = "" Or Val(vDATA) = 0 Then
            grdArquivo.Col = 5
            grdArquivo.Text = "Data Inv�lida! "
            vFL_DPK_OK = False
        Else
            
            'Valida��o Geral do CD e DPK na ITEM_ESTOQUE de Campinas
            'Selecionando Situa��o do Item e do Fornecedor, C�d.Forn. e Flag EDI
            db.Parameters.Remove "CD": db.Parameters.Add "CD", Val(vCD), 1
            db.Parameters.Remove "DPK": db.Parameters.Add "DPK", Val(vDPK), 1
            
            CmdSql = "Select Situacao Sit " & _
                     "From Item_Estoque " & _
                     "Where Cod_Loja = :CD " & _
                     "And Cod_Dpk = :DPK "
            Set db_CONS = db.dbcreatedynaset(CmdSql, 0&)
                
            'Se select n�o trouxer nada, h� problema com o DPK ou com o CD
            If db_CONS.EOF Then
                grdArquivo.Col = 5
                grdArquivo.Text = "Probl.c/CD ou DPK! "
                vFL_DPK_OK = False
            Else
                'Se a situa��o do DPK estiver substituido ou desativado, n�o faz nada
                If Val(db_CONS!Sit) = 8 Or Val(db_CONS!Sit) = 9 Then
                    grdArquivo.Col = 5
                    grdArquivo.Text = "DPK Subst.ou Desat.!"
                    vFL_DPK_OK = False
                End If
            End If
        
        End If
        
        
        'Se as valida��es acima estiverem corretas, continua; sen�o l� pr�xima linha do arq.
        If vFL_DPK_OK = True Then
        
            db.Parameters.Remove "CD": db.Parameters.Add "CD", Val(vCD), 1
            db.Parameters.Remove "DPK": db.Parameters.Add "DPK", Val(vDPK), 1
            'db.Parameters.Remove "DATA": db.Parameters.Add "DATA", vDATA, 1
            
            CmdSql = "Select Count(*) Qtd " & _
                     "From Item_Primeira_Entrada " & _
                     "Where Cod_Loja = :CD " & _
                     "And Cod_Dpk = :DPK "
            Set db_CONS = db.dbcreatedynaset(CmdSql, 0&)
                
            'Se select n�o trouxer nada, � inclus�o, sen�o � exclus�o
            If db_CONS!Qtd = 0 Then
                vTRAN = "I"
            Else
                vTRAN = "A"
            End If
        End If
        
        'Alimenta o grid com as informa��es do arquivo, independente de estarem corretas ou n�o
        grdArquivo.Col = 1
        grdArquivo.Text = Format(vCD, "00")
        grdArquivo.Col = 2
        grdArquivo.Text = vDPK
        grdArquivo.Col = 3
        grdArquivo.Text = vDATA
        grdArquivo.Col = 4
        grdArquivo.Text = vTRAN
    
    'L� pr�xima linha
    Loop
    
    Screen.MousePointer = 0
    MsgBox "Arquivo Lido!", vbInformation, "Aten��o!"
    lblMsg = I & " Linhas Lidas do Arquivo!"
    lblMsg.Refresh
    
    Close #1    ' Close file.
    Screen.MousePointer = 0
        
    Exit Sub
        
        
Trata_Erro:
    
    If Err.Number = 53 Then
        MsgBox "Arquivo de Controle n�o Encontrado. ", , MSG_TIT
    Else
        Call Process_Line_Errors
    End If

End Sub
Private Sub lblHelp2_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp2.Visible = False
End Sub






Private Sub SSFrame2_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp1.Visible = False
    lblHelp2.Visible = False
End Sub


Private Sub SSFrame3_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp1.Visible = False
End Sub



































