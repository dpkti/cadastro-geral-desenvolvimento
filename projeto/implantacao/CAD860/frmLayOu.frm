VERSION 4.00
Begin VB.Form frmLayOut 
   Caption         =   "Lay-Out do Arquivo para Cadastro Autom�tico de Data Prim.Entrada"
   ClientHeight    =   4050
   ClientLeft      =   3015
   ClientTop       =   3060
   ClientWidth     =   6060
   Height          =   4455
   Left            =   2955
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   4050
   ScaleWidth      =   6060
   Top             =   2715
   Width           =   6180
   Begin VB.Frame Frame1 
      Height          =   3195
      Left            =   150
      TabIndex        =   0
      Top             =   75
      Width           =   5715
      Begin VB.Label Label15 
         AutoSize        =   -1  'True
         Caption         =   "   100 linhas."
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   2385
         TabIndex        =   16
         Top             =   2910
         Width           =   1125
      End
      Begin VB.Label Label14 
         AutoSize        =   -1  'True
         Caption         =   "3) Fazer arquivos de - no m�ximo - "
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   2340
         TabIndex        =   15
         Top             =   2715
         Width           =   3000
      End
      Begin VB.Label Label13 
         AutoSize        =   -1  'True
         Caption         =   "   mato DD/MM/YY."
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   2385
         TabIndex        =   13
         Top             =   2490
         Width           =   1710
      End
      Begin VB.Label Label12 
         AutoSize        =   -1  'True
         Caption         =   "2) Campos Data dever�o vir com o for-"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   2340
         TabIndex        =   12
         Top             =   2295
         Width           =   3300
      End
      Begin VB.Label Label11 
         AutoSize        =   -1  'True
         Caption         =   "   zeros � esquerda."
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   2370
         TabIndex        =   11
         Top             =   2055
         Width           =   1710
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         Caption         =   "1) Campos Num�ricos dever�o vir com"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   2325
         TabIndex        =   10
         Top             =   1845
         Width           =   3270
      End
      Begin VB.Label Label9 
         AutoSize        =   -1  'True
         Caption         =   "Observa��es:"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000040C0&
         Height          =   195
         Left            =   2355
         TabIndex        =   9
         Top             =   1590
         Width           =   1185
      End
      Begin VB.Line Line3 
         X1              =   2235
         X2              =   2235
         Y1              =   1560
         Y2              =   3165
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         Caption         =   "015435727/07/02"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   150
         TabIndex        =   8
         Top             =   2235
         Width           =   1575
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "01    727/07/02"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   150
         TabIndex        =   7
         Top             =   2055
         Width           =   1575
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         Caption         =   "01 323227/07/02"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   150
         TabIndex        =   6
         Top             =   1860
         Width           =   1575
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Exemplo de Arquivo:"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000040C0&
         Height          =   195
         Left            =   150
         TabIndex        =   5
         Top             =   1590
         Width           =   1755
      End
      Begin VB.Line Line2 
         X1              =   0
         X2              =   5700
         Y1              =   1545
         Y2              =   1545
      End
      Begin VB.Line Line1 
         X1              =   0
         X2              =   5685
         Y1              =   405
         Y2              =   405
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Campo                                  Tipo de Dados"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000040C0&
         Height          =   195
         Left            =   555
         TabIndex        =   4
         Top             =   135
         Width           =   3870
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Data de Primeira Entrada        DATA"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   585
         TabIndex        =   3
         Top             =   1035
         Width           =   3120
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "C�digo DPK                          NUM�RICO (5)          "
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   585
         TabIndex        =   2
         Top             =   795
         Width           =   4470
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "C�digo da Loja                      NUM�RICO (2)"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   570
         TabIndex        =   1
         Top             =   555
         Width           =   3885
      End
   End
   Begin Threed.SSCommand cmdVoltar 
      Height          =   615
      Left            =   5235
      TabIndex        =   14
      Top             =   3375
      Width           =   630
      _Version        =   65536
      _ExtentX        =   1111
      _ExtentY        =   1085
      _StockProps     =   78
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "frmLayOu.frx":0000
   End
End
Attribute VB_Name = "frmLayOut"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Private Sub cmdVoltar_Click()
    Unload Me
End Sub


