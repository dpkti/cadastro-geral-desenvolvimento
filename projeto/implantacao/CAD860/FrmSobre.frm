VERSION 4.00
Begin VB.Form frmSobre 
   Caption         =   "Sobre"
   ClientHeight    =   4245
   ClientLeft      =   2670
   ClientTop       =   2925
   ClientWidth     =   6690
   Height          =   4650
   Left            =   2610
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   4245
   ScaleWidth      =   6690
   Top             =   2580
   Width           =   6810
   Begin Threed.SSFrame SSFrame1 
      Height          =   3855
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   6495
      _Version        =   65536
      _ExtentX        =   11456
      _ExtentY        =   6800
      _StockProps     =   14
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin VB.Label lblDpk 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         Caption         =   "DPK Distribuidora de Pe�as Ltda"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   495
         Left            =   240
         TabIndex        =   4
         Top             =   600
         Width           =   6015
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         Caption         =   "Programa: CAD860 - Cadastro de Data de Primeira Entrada (Manual e Autom�tico)-  Vers�o: 1.0 - 32Bits"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   615
         Left            =   720
         TabIndex        =   3
         Top             =   1200
         Width           =   5175
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         Caption         =   "Respons�vel:  ALETH�A - SISTEMAS (Ramal 8161)"
         ForeColor       =   &H00800000&
         Height          =   375
         Left            =   1320
         TabIndex        =   2
         Top             =   2160
         Width           =   4095
      End
      Begin Threed.SSCommand SSCommand1 
         Height          =   495
         Left            =   2760
         TabIndex        =   1
         Top             =   3000
         Width           =   1335
         _Version        =   65536
         _ExtentX        =   2355
         _ExtentY        =   873
         _StockProps     =   78
         Caption         =   "OK"
         ForeColor       =   8388608
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
End
Attribute VB_Name = "frmSobre"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Private Sub SSCommand1_Click()
    Unload Me
End Sub


