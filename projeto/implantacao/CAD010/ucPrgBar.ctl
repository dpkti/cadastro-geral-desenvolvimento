VERSION 5.00
Begin VB.UserControl ucPrgBar 
   AutoRedraw      =   -1  'True
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   1  'Fixed Single
   CanGetFocus     =   0   'False
   ClientHeight    =   375
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   3135
   DrawMode        =   14  'Copy Pen
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   9.75
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ScaleHeight     =   375
   ScaleWidth      =   3135
End
Attribute VB_Name = "ucPrgBar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'3� Passo: Colar o c�digo abaixo no UserControl:
Option Explicit

Const Escala = 7.35

Public Enum Estilos
    ePercentual = 0
    eTexto = 1
    eAmbos = 2
    eNenhum = 3
End Enum

Dim lMax!
Dim lValor!
Dim lCor&
Dim lTexto$
Dim lEstilo As Estilos

Public Property Let Estilo(ByVal newEstilo As Estilos)
    lEstilo = newEstilo
    Call Atualizar
    Call PropertyChanged("Estilo")
End Property

Public Property Get Estilo() As Estilos
    Estilo = lEstilo
End Property

Public Property Let Texto(ByVal newTexto$)
    lTexto = newTexto
    Call Atualizar
    Call PropertyChanged("Texto")
End Property

Public Property Get Texto$()
    Texto = lTexto
End Property

Public Property Let Valor(ByVal newValor!)
    If newValor > lMax Or newValor < 0 Then Exit Property
    lValor = newValor
    Call Atualizar
    Call PropertyChanged("Valor")
End Property

Public Property Get Valor!()
    Valor = lValor
End Property

Public Property Let Cor(ByVal newCor As OLE_COLOR)
    lCor = newCor
    Call Atualizar
    Call PropertyChanged("Cor")
End Property

Public Property Get Cor() As OLE_COLOR
    Cor = lCor
End Property

Public Property Let Max(ByVal newMax!)
    If newMax <= 0 Then Exit Property
    lMax = newMax
    If lValor > lMax Then lValor = lMax
    Call Atualizar
    Call PropertyChanged("Max")
End Property

Public Property Get Max!()
    Max = lMax
End Property

Private Sub UserControl_InitProperties()
    lMax = 100
    lCor = &H800000
    lEstilo = ePercentual
    lTexto = ""
    Valor = 0
End Sub

Private Sub UserControl_ReadProperties(PropBag As PropertyBag)
    lValor = PropBag.ReadProperty("Valor", 0)
    lMax = PropBag.ReadProperty("Max", 100)
    lCor = PropBag.ReadProperty("Cor", &H800000)
    lEstilo = PropBag.ReadProperty("Estilo", ePercentual)
    lTexto = PropBag.ReadProperty("Texto", "")
    Call Atualizar
End Sub

Private Sub UserControl_Resize()
    Call Atualizar
End Sub

Private Sub UserControl_WriteProperties(PropBag As PropertyBag)
    Call PropBag.WriteProperty("Valor", lValor, 0)
    Call PropBag.WriteProperty("Max", lMax, 100)
    Call PropBag.WriteProperty("Cor", lCor, &H800000)
    Call PropBag.WriteProperty("Estilo", lEstilo, ePercentual)
    Call PropBag.WriteProperty("Texto", lTexto, "")
End Sub

Private Sub Atualizar()
    Dim strVlr$
    Dim dblVlr#
    Dim sngWidth!

    Cls

    If lValor > 0 Then
        dblVlr = (lValor * 100) / lMax
    End If

    Select Case lEstilo
    Case ePercentual
        strVlr = CStr(CInt(dblVlr)) & "%"
    Case eTexto
        strVlr = lTexto
    Case eAmbos
        strVlr = CStr(CInt(dblVlr)) & "% " & lTexto
    Case eNenhum
        strVlr = ""
    End Select

    FillColor = lCor
    ForeColor = lCor

    CurrentX = (ScaleWidth / 2) - TextWidth(strVlr) / 2
    CurrentY = (ScaleHeight / 2) - TextHeight(strVlr) / 2
    Print strVlr

    sngWidth = CInt(((ScaleWidth / Escala) / 100) * dblVlr)
    If sngWidth = 0 Then sngWidth = 1

    If dblVlr > 0 Then
        DrawWidth = sngWidth
        Line (0, 0)-(0, ScaleHeight)
    End If
End Sub


