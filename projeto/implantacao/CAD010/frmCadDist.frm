VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmCadDist 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cadastro de Dist�ncias"
   ClientHeight    =   6735
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6600
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6735
   ScaleWidth      =   6600
   StartUpPosition =   3  'Windows Default
   Begin MSFlexGridLib.MSFlexGrid grdLojaDist 
      Height          =   3975
      Left            =   600
      TabIndex        =   6
      Top             =   1080
      Width           =   5415
      _ExtentX        =   9551
      _ExtentY        =   7011
      _Version        =   393216
      Cols            =   3
      FixedCols       =   2
   End
   Begin VB.TextBox txtCidade 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1320
      TabIndex        =   3
      Top             =   600
      Width           =   4695
   End
   Begin VB.TextBox txtUF 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1320
      TabIndex        =   2
      Top             =   240
      Width           =   735
   End
   Begin Threed.SSCommand cmdSair 
      Height          =   495
      Left            =   5520
      TabIndex        =   4
      Top             =   6000
      Width           =   495
      _Version        =   65536
      _ExtentX        =   873
      _ExtentY        =   873
      _StockProps     =   78
      Picture         =   "frmCadDist.frx":0000
   End
   Begin Threed.SSCommand cmdGravar 
      Height          =   495
      Left            =   4920
      TabIndex        =   5
      Top             =   6000
      Width           =   495
      _Version        =   65536
      _ExtentX        =   873
      _ExtentY        =   873
      _StockProps     =   78
      Picture         =   "frmCadDist.frx":031A
   End
   Begin VB.TextBox txtIbge 
      Height          =   375
      Left            =   4560
      TabIndex        =   7
      Top             =   4440
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label lblCidade 
      AutoSize        =   -1  'True
      Caption         =   "Cidade"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   600
      TabIndex        =   1
      Top             =   600
      Width           =   600
   End
   Begin VB.Label lblUF 
      AutoSize        =   -1  'True
      Caption         =   "UF"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   600
      TabIndex        =   0
      Top             =   240
      Width           =   255
   End
End
Attribute VB_Name = "frmCadDist"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Flag As Boolean
Dim intCount As Long
Public p_Cod_IBGE As Long



Private Sub cmdGravar_Click()
    Dim contLinha As Integer
    Dim upSQL As String
    Dim objDistCD As Object
    Dim distancia As Double
    Dim codIbge As Long
    Dim codLoja As Integer
    Dim int_num_Erro As Integer, str_Erro As String

    If MsgBox("Deseja gravar as altera��es e encerrar a tela?", vbYesNo + vbQuestion, "Confirmar a��o.") = vbNo Then
        Exit Sub
    End If
    
    Flag = False
    
    For contLinha = 1 To grdLojaDist.Rows - 1

        If grdLojaDist.TextMatrix(contLinha, 2) = "" Then
            MsgBox "Dist�ncia KM deve ser maior ou igual a zero.", vbInformation, "Aten��o"
            Flag = True
            Exit Sub
        Else

            If InStr((grdLojaDist.TextMatrix(contLinha, 2)), ",") > 8 Or (InStr((grdLojaDist.TextMatrix(contLinha, 2)), ",") = 0 And Len(grdLojaDist.TextMatrix(contLinha, 2)) > 6) Or Len(grdLojaDist.TextMatrix(contLinha, 2)) > 11 Then
                MsgBox "Dist�ncia KM maior que o permitido - Dist�ncia m�xima 999.999,999.", vbInformation, "Aten��o"
                Flag = True
                Exit Sub
            Else
                distancia = Replace(Str(grdLojaDist.TextMatrix(contLinha, 2)), ".", ",")
            End If
        End If

        codIbge = p_Cod_IBGE   'txtIbge.Text    '=frmCadastro.lblCod_Cidade
        codLoja = CInt(grdLojaDist.TextMatrix(contLinha, 0))

        ''    upSQL = " UPDATE PRODUCAO.R_DISTANCIA_CIDADE_CD D "
        ''    upSQL = upSQL & " SET D.DISTANCIA_KM = :distancia "
        ''    upSQL = upSQL & " Where D.COD_IBGE = :codIbge "
        ''    upSQL = upSQL & " AND D.COD_LOJA = :codLoja "
        ''    oradatabase.ExecuteSQL upSQL

        '''    oradatabase.Parameters.Remove "distancia"
        '''    oradatabase.Parameters.Add "distancia", distancia, 1
        '''    oradatabase.Parameters.Remove "codIbge"
        '''    oradatabase.Parameters.Add "codIbge", codIbge, 1
        '''    oradatabase.Parameters.Remove "codLoja"
        '''    oradatabase.Parameters.Add "codLoja", codLoja, 1
        '''    oradatabase.ExecuteSQL "BEGIN PRODUCAO.PCK_CAD010.PR_UPDATE_CAD_DIST( :codIbge, :codLoja, :distancia,); END;"

        oradatabase.Parameters.Remove "PM_COD_IBGE"
        oradatabase.Parameters.Remove "PM_COD_LOJA"
        oradatabase.Parameters.Remove "PM_DISTANCIA_KM"

        'oradatabase.Parameters.Add "PM_DISTANCIA_KM", Replace(Str(Round((distancia), 3)), ",", "."), 1 'ORAPARM_INPUT
        oradatabase.Parameters.Add "PM_DISTANCIA_KM", Round(distancia, 3), 1    'ORAPARM_INPUT
        oradatabase.Parameters("PM_DISTANCIA_KM").ServerType = 4    'ORATYPE_FLOAT

        oradatabase.Parameters.Add "PM_COD_IBGE", (codIbge), 1    'ORAPARM_INPUT
        oradatabase.Parameters("PM_COD_IBGE").ServerType = 2    'ORATYPE_NUMBER
        oradatabase.Parameters.Add "PM_COD_LOJA", (codLoja), 1    'ORAPARM_INPUT
        oradatabase.Parameters("PM_COD_LOJA").ServerType = 2    'ORATYPE_NUMBER

        If intCount = 0 Then
           oradatabase.Parameters.Remove "PM_NUM_ERRO"
           oradatabase.Parameters.Remove "PM_TXT_ERRO"
           
           oradatabase.Parameters.Add "PM_NUM_ERRO", int_num_Erro, 2
           oradatabase.Parameters("PM_NUM_ERRO").ServerType = 2
           oradatabase.Parameters.Add "PM_TXT_ERRO", str_Erro, 2
           oradatabase.Parameters("PM_TXT_ERRO").ServerType = 97
           
           oradatabase.ExecuteSQL "BEGIN PRODUCAO.PCK_CAD010.PR_INSERIR_CAD_DIST( :PM_COD_IBGE, :PM_COD_LOJA, :PM_DISTANCIA_KM, :PM_NUM_ERRO, :PM_TXT_ERRO); COMMIT; END;"
           oradatabase.CommitTrans
        Else
            oradatabase.ExecuteSQL "BEGIN PRODUCAO.PCK_CAD010.PR_UPDATE_CAD_DIST( :PM_COD_IBGE, :PM_COD_LOJA, :PM_DISTANCIA_KM); END;"
        End If
        
        oradatabase.Parameters.Remove "PM_COD_IBGE"
        oradatabase.Parameters.Remove "PM_COD_LOJA"
        oradatabase.Parameters.Remove "PM_DISTANCIA_KM"

    Next

    Call CarregaGrid(p_Cod_IBGE)
    Unload Me

End Sub

Private Sub cmdSair_Click()
    Dim resp As Integer

    resp = 0
    If Flag = True Then
        resp = MsgBox("Existem altera��es pendentes.Verificar a altera��o?", vbYesNo, "Aten��o")
        If resp = 7 Then
            Unload Me
        End If
    Else
        Unload Me
    End If

End Sub

Private Sub Form_Load()
    Call CarregaGrid(p_Cod_IBGE)
    Flag = False
   
    
End Sub

Sub CarregaGrid(p_Cod_IBGE As Long)
    Dim Linha As Integer
    Dim vSQL As String
    Dim objDistancia As Object
    Dim distanciaKM As Variant

    vSQL = " SELECT LJA.COD_LOJA, LJ.NOME_FANTASIA, R_DIST.DISTANCIA_KM "
    vSQL = vSQL & " FROM PRODUCAO.R_DISTANCIA_CIDADE_CD R_DIST, PRODUCAO.LOJA_ATIVA LJA, PRODUCAO.LOJA LJ, "
    vSQL = vSQL & " PRODUCAO.R_CIDADE_IBGE CIDADE "
    vSQL = vSQL & " Where R_DIST.COD_LOJA = LJA.COD_LOJA And LJA.COD_LOJA = LJ.COD_LOJA AND R_DIST.COD_IBGE = CIDADE.COD_IBGE"
    vSQL = vSQL & " AND CIDADE.COD_IBGE = " & CStr(p_Cod_IBGE)
    vSQL = vSQL & " ORDER BY LJA.COD_LOJA "

    Set objDistancia = oradatabase.CreateDynaset(vSQL, 0&)
    intCount = objDistancia.RecordCount
    
    If intCount = 0 Then
        vSQL = " SELECT LJA.COD_LOJA, LJ.NOME_FANTASIA, NULL DISTANCIA_KM "
        vSQL = vSQL & " FROM PRODUCAO.LOJA_ATIVA LJA, PRODUCAO.LOJA LJ "
        vSQL = vSQL & " Where LJA.COD_LOJA = LJ.COD_LOJA "
        vSQL = vSQL & " ORDER BY LJA.COD_LOJA "
    
        Set objDistancia = oradatabase.CreateDynaset(vSQL, 0&)
    End If

    With grdLojaDist

        .Row = 0
        .Col = 0
        .Text = "C�digo Loja"
        .ColWidth(0) = 1000
        .FixedAlignment(0) = 2

        .Col = 1
        .Text = "Nome da Loja"
        .ColWidth(1) = 2500
        .FixedAlignment(1) = 0

        .Col = 2
        .Text = "Dist�ncia (KM)"
        .ColWidth(2) = 1500
        .FixedAlignment(2) = 0

    End With

    Linha = 0
    On Error Resume Next
    
    While Not objDistancia.EOF
        distanciaKM = ""
        Linha = Linha + 1
        frmCadDist.grdLojaDist.Rows = Linha + 1
        frmCadDist.grdLojaDist.Row = Linha

        frmCadDist.grdLojaDist.Col = 0
        frmCadDist.grdLojaDist.ColAlignment(0) = flexAlignCenterCenter
        frmCadDist.grdLojaDist.Text = Format(objDistancia.Fields("COD_LOJA"), "000")

        frmCadDist.grdLojaDist.Col = 1
        frmCadDist.grdLojaDist.ColAlignment(1) = flexAlignLeftCenter
        frmCadDist.grdLojaDist.Text = objDistancia.Fields("NOME_FANTASIA")

        frmCadDist.grdLojaDist.Col = 2
        frmCadDist.grdLojaDist.ColAlignment(2) = flexAlignRightCenter
        distanciaKM = Replace(objDistancia.Fields("DISTANCIA_KM"), ".", ",")
        frmCadDist.grdLojaDist.Text = Format(distanciaKM, "###,###,##0.000")


        frmCadDist.grdLojaDist.Enabled = True
        objDistancia.MoveNext
    Wend

End Sub

Private Sub grdLojaDist_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If grdLojaDist.Col <> 2 Then Exit Sub
    
    If KeyCode = vbKeyDelete Then
        grdLojaDist.Text = Empty
    End If
    
End Sub

Private Sub grdLojaDist_KeyPress(KeyAscii As Integer)
'rotina para editar coluna da Grid
    
    If grdLojaDist.Col <> 2 Then Exit Sub
    
    Select Case ValidaKm(KeyAscii)
    Case vbKeyReturn, vbKeyTab
        'move para a proxima celula.

        With grdLojaDist
            If .Col + 1 <= .Cols - 1 Then
                .Col = .Col + 1
            Else
                If .Row + 1 <= .Rows - 1 Then
                    .Row = .Row + 1
                    .Col = 0
                Else
                    .Row = 1
                    .Col = 0
                End If
            End If
        End With

    Case vbKeyBack

        With grdLojaDist
            'remove o ultimo caractere
            If Len(.Text) Then
                .Text = Left(.Text, Len(.Text) - 1)
            End If
        End With

    Case Is < 32

    Case Else
        With grdLojaDist
            .Text = .Text & Chr(KeyAscii)
        End With
    End Select

End Sub

Function ValidaKm(ByVal KeyAscii As Integer) As Integer

    On Error GoTo Trata_Erro

    If KeyAscii = 8 Or KeyAscii = 13 Or KeyAscii = 44 Or KeyAscii = 127 Then

        ValidaKm = KeyAscii
        Exit Function

    End If

    If Chr(KeyAscii) < "0" Or Chr(KeyAscii) > "9" Then

        KeyAscii = 0

    End If

    ValidaKm = KeyAscii

    Exit Function

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Function ValidaKm" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description, "Aten��o", &H40000
    End If
End Function

