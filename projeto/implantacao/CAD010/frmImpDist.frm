VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmImpDist 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Importa��o de Dist�ncias"
   ClientHeight    =   645
   ClientLeft      =   4065
   ClientTop       =   4275
   ClientWidth     =   6720
   HasDC           =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   645
   ScaleWidth      =   6720
   Begin Project1.ucPrgBar prgImportar 
      Height          =   375
      Left            =   120
      Top             =   120
      Width           =   6495
      _ExtentX        =   11456
      _ExtentY        =   661
   End
   Begin MSComDlg.CommonDialog cdlImportar 
      Left            =   120
      Top             =   120
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      DialogTitle     =   "Abrir Arquivo para Importa��o:"
      Filter          =   "Arquivos Excel 97-2003 (*.xls)|*.xls|Arquivos Excel 2010 (*.xlsx)|*.xlsx"
      FilterIndex     =   1
   End
End
Attribute VB_Name = "frmImpDist"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()

' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'                   Grupo Meta Consultoria em TI.                   '
'                   Site:     www.meta.com.br                       '
'                   e-mail:   meta@meta.com.br                      '
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'
' SDS: 2424
' Changepoint: DP-2011-00213 - Cadastro de dist�ncia por CD DPK.
' Respons�vel - F�bio Vicente
' Data   - 14/11/2011
'
' Rotina: Criar tela de Importa��o de Dist�ncias
' Objetivo: Gerar informa��es para tela de Cadastro de Dist�ncias.
' ID Req. T�cnico: 1.2
'
' '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' M�todo Form_Load(): Abertura do Formul�rio e chamada do m�todo de excu��o da Rotina de Importa��o.
'

    On Error GoTo Tratamento_Erro

    ' Declara��o de Vari�veis:
    Dim intImportacao As Integer

    'In�cio da Rotina:
    '
    '---------------------------------------------------------------------------------------------------
    '1.  O formul�rio frmImpDist dever� ter a propriedade Visible = False;
    '---------------------------------------------------------------------------------------------------
    Me.Visible = False

    '
    '---------------------------------------------------------------------------------------------------
    '2.  O componente cdlImportar  dever� ser exibido;
    '---------------------------------------------------------------------------------------------------
    Me.cdlImportar.ShowOpen
    If cdlImportar.filename = "" Then
        Exit Sub
    End If

    '
    'Tratamento de arquivo: permite apenas que arquivos do tipo excel (xls) seja aberto
    '
    strExtensaoArq = Right(cdlImportar.filename, Len(cdlImportar.filename) - InStr(1, cdlImportar.filename, ".xls", vbTextCompare))
    If (Left(strExtensaoArq, 3) <> "xls") Then
        MsgBox "Tipo de Arquivo Inv�lido.", vbExclamation + vbOKOnly, "Aten��o."
        Exit Sub
    End If


    '
    '---------------------------------------------------------------------------------------------------
    '5.  O formul�rio frmImpDist dever� ter a propriedade Visible = True;
    '---------------------------------------------------------------------------------------------------
    Me.Visible = True
    Me.SetFocus
    'Me.prgImportar.Min = 0
    'Me.prgImportar.Max = Abs(100)
    DoEvents

    '
    '---------------------------------------------------------------------------------------------------
    '3.  Ap�s selecionar o arquivo e clicar no Bot�o "Abrir" do componente,
    'dever� ser executado o m�todo para importa��o do Arquivo de Dist�ncias,
    'descrito no Requisito T�cnico 1.3;
    '---------------------------------------------------------------------------------------------------
    intImportacao = Module1.ImportarArquivoExcel(Me.cdlImportar.filename)

    If intImportacao = 0 Then
        MsgBox "Importa��o Realizada com Sucesso." & vbCrLf & "Pressione Ok para continuar.", _
               vbInformation + vbOKOnly, "T�rmino do processamento."
    Else
        If intImportacao = 70 Then
            MsgBox "Ocorreram erros na importa��o.", vbInformation + vbOKOnly, "T�rmino do processamento."
        Else
            MsgBox "Ocorreram erros na importa��o." & vbCrLf & "Visualize a planilha LOG para maiores detalhes.", _
                   vbInformation + vbOKOnly, "T�rmino do processamento."
        End If
    End If



    ' Se ocorrer erro na execu��o da rotina
Tratamento_Erro:

    If Err.Number <> 0 Then

        MsgBox "Ocorreu o erro n�: " & Str(Err.Number) & " - " & _
               Err.Description & vbCrLf & "Entre em contato com o Suporte T�cnico.", _
               vbCritical + vbOKOnly, "Erro"
    End If

    Exit Sub

End Sub
