Attribute VB_Name = "MDCAD997"
Option Explicit
'Variaveis de Cursor
Global Const ORATYPE_CURSOR = 102
Global Const ORADYN_NO_BLANKSTRIP = &H2&
Global Const ORAPARM_INPUT = 1              'CONSTANTE DE BIND INPUT
Global Const ORAPARM_OUTPUT = 2             'CONSTANTE DE BIND OUTPUT
Global Const ORAPARM_BOTH = 3               'CONSTANTE DE BIND INPUT/OUTPUT
Global Const ORATYPE_NUMBER = 2
       
Public vBanco As Object
Public vSessao As Object
Public vNomeBanco As String
Public vUsuarioBanco As String
Public vSenhaBanco As String
Public vOwner As String
Public vDataAtual As String

Sub Conectar(Optional pForm As Form)

1         On Error GoTo Trata_erro
          
2         If Not pForm Is Nothing Then
3             pForm.stb.Panels(1).Text = "Conectando... Aguarde..."
4         End If
              
5         Set vSessao = CreateObject("oracleinprocserver.xorasession")
7         Set vBanco = vSessao.OpenDatabase(vNomeBanco, vUsuarioBanco & "/" & vSenhaBanco, 0&)
          
8         If Not pForm Is Nothing Then
9             pForm.stb.Panels(1).Text = "Pronto"
10        End If

Trata_erro:
11        If Err.Number <> 0 Then

12            MsgBox "Sub: Conectar" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
13        End If
          
End Sub

Sub Desconectar(Optional pForm As Form)

1         On Error GoTo Trata_erro

2         If Not pForm Is Nothing Then
3             pForm.stb.Panels(1).Text = "Desconectando... Aguarde..."
4         End If

          Dim vIndice

5         If vBanco Is Nothing Then Conectar

6         For vIndice = vBanco.Parameters.Count To 0 Step -1

7             vBanco.Parameters.Remove (vIndice)

8         Next
          
9         Set vSessao = Nothing
10        Set vBanco = Nothing

Trata_erro:
11        If Err.Number <> 0 Then
12            MsgBox "Sub: Desconectar" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
13        End If
          
End Sub

Sub Criar_Cursor()
          
1         vBanco.Parameters.Remove "PM_Cursor"
2         vBanco.Parameters.Add "PM_Cursor", 0, 2
3         vBanco.Parameters("PM_Cursor").serverType = ORATYPE_CURSOR
4         vBanco.Parameters("PM_Cursor").DynasetOption = ORADYN_NO_BLANKSTRIP
5         vBanco.Parameters("PM_Cursor").DynasetCacheParams 256, 16, 20, 20000, 0

End Sub

Function Numerico(ByVal KeyAscii As Integer) As Integer
1         If KeyAscii = 8 Then    'BACKSPACE
2             Numerico = KeyAscii
3             Exit Function
4         End If
5         If Chr(KeyAscii) < "0" Or Chr(KeyAscii) > "9" Then
6             KeyAscii = 0
7             Beep
8         End If
9         Numerico = KeyAscii
End Function
