VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Begin VB.Form frmFornecedores 
   Caption         =   "NFE Fornecedor"
   ClientHeight    =   8310
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10455
   Icon            =   "frmFornecedores.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   8310
   ScaleWidth      =   10455
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ProgressBar pb1 
      Height          =   165
      Left            =   4320
      TabIndex        =   12
      Top             =   30
      Width           =   6135
      _ExtentX        =   10821
      _ExtentY        =   291
      _Version        =   393216
      BorderStyle     =   1
      Appearance      =   0
      Scrolling       =   1
   End
   Begin Bot�o.cmd cmd1 
      Height          =   345
      Left            =   6360
      TabIndex        =   8
      Top             =   7950
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   609
      BTYPE           =   3
      TX              =   "Gravar"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmFornecedores.frx":0CCA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   345
      Left            =   3330
      TabIndex        =   4
      Top             =   7950
      Width           =   1365
      _ExtentX        =   2408
      _ExtentY        =   609
      _Version        =   393216
      Format          =   16252929
      CurrentDate     =   39899
   End
   Begin VB.ComboBox cboSimNao 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      ItemData        =   "frmFornecedores.frx":0CE6
      Left            =   2310
      List            =   "frmFornecedores.frx":0CF0
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   7950
      Width           =   975
   End
   Begin VB.TextBox txtCGC 
      Appearance      =   0  'Flat
      Height          =   345
      Left            =   30
      MaxLength       =   14
      TabIndex        =   2
      Top             =   7950
      Width           =   2235
   End
   Begin MSComctlLib.ListView lsvFornecedores 
      Height          =   7425
      Left            =   0
      TabIndex        =   0
      Top             =   240
      Width           =   10425
      _ExtentX        =   18389
      _ExtentY        =   13097
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   4
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "CGC"
         Object.Width           =   2646
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Nome do Fornecedor"
         Object.Width           =   10408
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "NFE"
         Object.Width           =   2223
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Data In�cio"
         Object.Width           =   2540
      EndProperty
   End
   Begin Bot�o.cmd cmd2 
      Height          =   345
      Left            =   7740
      TabIndex        =   9
      Top             =   7950
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   609
      BTYPE           =   3
      TX              =   "Excluir"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmFornecedores.frx":0CFE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd3 
      Height          =   345
      Left            =   9120
      TabIndex        =   10
      Top             =   7950
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   609
      BTYPE           =   3
      TX              =   "Limpar"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmFornecedores.frx":0D1A
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdConsultar 
      Height          =   345
      Left            =   4950
      TabIndex        =   11
      Top             =   7950
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   609
      BTYPE           =   3
      TX              =   "Consultar CGC"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmFornecedores.frx":0D36
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      Caption         =   "A partir de"
      Height          =   195
      Left            =   3330
      TabIndex        =   7
      Top             =   7740
      Width           =   720
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      Caption         =   "NFE"
      Height          =   195
      Left            =   2310
      TabIndex        =   6
      Top             =   7740
      Width           =   315
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "CGC"
      Height          =   195
      Left            =   30
      TabIndex        =   5
      Top             =   7740
      Width           =   330
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Fornecedores - Nota Fiscal Eletr�nica"
      Height          =   195
      Left            =   0
      TabIndex        =   1
      Top             =   30
      Width           =   2655
   End
End
Attribute VB_Name = "frmFornecedores"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmd1_Click()

          'Realiza valida��o dos campos digitados pelo usu�rio
1         If txtCGC.Text = "" Or Not IsNumeric(txtCGC) Then
2             MsgBox "Preencha o campo CGC.", vbCritical, "Aten��o"
3             txtCGC.SetFocus
4             Exit Sub
5         End If
6         If cboSimNao.Text = "" Then
7             MsgBox "Preencha o campo NFE.", vbCritical, "Aten��o"
8             cboSimNao.SetFocus
9             Exit Sub
10        End If
          
11        If CDate(DTPicker1.Value) < CDate(vDataAtual) Then
12           If MsgBox("A data � menor que a data atual." & vbCrLf & "Voc� confirma a grava��o ?", vbQuestion + vbYesNo, "Aten��o - CAD997") = vbNo Then
13              DTPicker1.SetFocus
14              Exit Sub
15            End If
16        End If
          
17        GravaDados 1

End Sub

Private Sub cmd2_Click()

1         If MsgBox("Confirma exclus�o do regisro?", vbYesNo, "CAD997") = vbYes Then
2             GravaDados 2
3         End If
          
4         Botoes True, False
          
5         LimpaCampos

End Sub

Private Sub cmd3_Click()
    
    LimpaCampos
    
    Botoes True, False
    
End Sub

Private Sub cmdConsultar_Click()
        
    If Val(txtCGC) = 0 Then
        MsgBox "Informe o CGC.", vbInformation, "Aten��o"
        txtCGC.SetFocus
        Exit Sub
    End If
    
    Dim vItemFound As ListItem
    Set vItemFound = Me.lsvFornecedores.FindItem(Me.txtCGC, lvwText, , lvwPartial)
    
    If Not vItemFound Is Nothing Then
        vItemFound.EnsureVisible
        vItemFound.Selected = True
        lsvFornecedores.SetFocus
    Else
        MsgBox "CGC n�o encontrado", vbInformation, "Aten��o"
    End If
End Sub

'Banco : Producao
'Tabela: PRODUCAO.R_NFE_FORNECEDOR

Private Sub Form_Load()
    
    'Par�metro for 1 � DPK, se 2 DPA
    If Command = 1 Then
        vNomeBanco = "PRODUCAO"
        vUsuarioBanco = "CAD997"
        vSenhaBanco = "DPK2387"
        vOwner = "PRODUCAO"
    ElseIf Command = 2 Then
        vNomeBanco = "PRODODS"
        vUsuarioBanco = ""
        vSenhaBanco = ""
        vOwner = "TABODS"
    Else
        MsgBox "Escolha Par�metro 1 - DPK ou 2 - DPA.", vbCritical, "Aten��o"
        End
    End If
    
    vDataAtual = fPegarData
    
    'Carrega os fornecedores
    CarregaLista
    
    DTPicker1.Value = vDataAtual
    
End Sub
Private Sub GravaDados(ByVal pAcao As Byte)

      Dim vObj As Object, vMSG As String

1         On Error GoTo Trata_erro
          
          'Quando pAcao for 1 insere, se for 2 exclui
2         Conectar
           
3         vBanco.Parameters.Remove "pm_acao"
4         vBanco.Parameters.Add "pm_acao", pAcao, 1
           
5         vBanco.Parameters.Remove "pm_msg"
6         vBanco.Parameters.Add "pm_msg", "", 2

7         vBanco.Parameters.Remove "pm_cgc"
8         vBanco.Parameters.Add "pm_cgc", txtCGC.Text, 1

9         vBanco.Parameters.Remove "pm_nfe"
10        vBanco.Parameters.Add "pm_nfe", Left(cboSimNao.Text, 1), 1

11        vBanco.Parameters.Remove "pm_data"
12        vBanco.Parameters.Add "pm_data", IIf(Left(cboSimNao.Text, 1) = "N", Null, DTPicker1.Value), 1

13        vBanco.ExecuteSQL "Begin " & vOwner & ".PCK_CAD997.PR_CADASTRA_forn(:PM_ACAO, " & _
                                                                      ":PM_MSG, " & _
                                                                      ":pm_cgc, " & _
                                                                      ":pm_nfe, " & _
                                                                      ":pm_data);END;"
          
14        vMSG = vBanco.Parameters("pm_msg").Value
          
15        MsgBox vMSG, vbInformation, "CAD997"
          
16        CarregaLista
          
          LimpaCampos
          
17        Desconectar
         
18        Botoes True, False

Trata_erro:
19        If Err.Number <> 0 Then
20            MsgBox "Sub: GravaDados" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl
21        End If
          
End Sub

Private Sub Botoes(ByVal vB1 As Boolean, ByVal vB2 As Boolean)
    
    cmd1.Enabled = vB1
    cmd2.Enabled = vB2
    
End Sub

Private Sub CarregaLista()

          Dim vObj As Object
          Dim Itmx As Object, i As Long
          
1         On Error GoTo Trata_erro
          
2         Me.Visible = True
          
3         lsvFornecedores.ListItems.Clear
          
          'Conforme os dados v�o sendo inseridos ou apagados, carrega o listview
4         Conectar
          
5         Criar_Cursor

6         vBanco.ExecuteSQL "Begin " & vOwner & ".PCK_CAD997.PR_CARREGA_FORNECEDOR(:pm_cursor);END;"
          
7         Set vObj = vBanco.Parameters("pm_cursor").Value
          
8         Me.pb1.Max = vObj.RECORDCOUNT
          
          'Carregando listview
9         For i = 1 To vObj.RECORDCOUNT
10            If i Mod 100 = 0 Then
11                  DoEvents
12            End If
13            Set Itmx = lsvFornecedores.ListItems.Add()
14                Itmx.Text = vObj!CGC
15                Itmx.SubItems(1) = vObj!FORN
16                Itmx.SubItems(2) = vObj!NFE
17                Itmx.SubItems(3) = vObj!Data
18            vObj.MoveNext
19            pb1.Value = pb1.Value + 1
20        Next
21        pb1.Value = 0
22        Me.pb1.Visible = False
Trata_erro:
23        If Err.Number <> 0 Then
24            MsgBox "Sub: CarregaLista" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl
25        End If
End Sub

Private Sub LimpaCampos()

    txtCGC.Text = ""
    cboSimNao.ListIndex = -1
    txtCGC.Enabled = True
    DTPicker1.Value = Date
    
End Sub

Private Sub lsvFornecedores_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
   
    lsvFornecedores.SortKey = ColumnHeader.Index - 1
    
    If lsvFornecedores.SortOrder = lvwAscending Then
        lsvFornecedores.SortOrder = lvwDescending
    Else
        lsvFornecedores.SortOrder = lvwAscending
    End If
    
    ' Set Sorted to True to sort the list.
    lsvFornecedores.Sorted = True


    
End Sub

Private Sub lsvFornecedores_DblClick()
    
    'Quando clicar 2 vezes no listivew, carrega os dados nos campos
    txtCGC.Text = lsvFornecedores.SelectedItem.Text
    cboSimNao.Text = lsvFornecedores.SelectedItem.SubItems(2)
    If cboSimNao.Text = "Sim" Then
        If Trim(lsvFornecedores.SelectedItem.SubItems(3)) <> "" Then
            DTPicker1.Value = lsvFornecedores.SelectedItem.SubItems(3)
        End If
    End If
    txtCGC.Enabled = False
    
    Botoes True, True
    
End Sub
Private Sub txtCGC_KeyPress(KeyAscii As Integer)
    KeyAscii = Numerico(KeyAscii)
    
End Sub


Function fPegarData()
          Dim vObj As Object
          
1         On Error GoTo Trata_erro
          
2         Conectar
          
3         Criar_Cursor

4         vBanco.ExecuteSQL "Begin " & vOwner & ".PCK_CAD997.PR_SELECT_DATA(:pm_cursor);END;"
          
5         Set vObj = vBanco.Parameters("pm_cursor").Value
          
          'Carregando listview
6         fPegarData = vObj!Data
          
Trata_erro:
7         If Err.Number <> 0 Then
8             MsgBox "Sub: fPegarData" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl
9         End If

End Function
