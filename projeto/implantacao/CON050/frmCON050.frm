VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "MSWINSCK.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmCON050 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "CON050 - Controle de Erros"
   ClientHeight    =   9000
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   13980
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "frmCON050.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9000
   ScaleWidth      =   13980
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H00FF8080&
      Caption         =   "Localizar Sistema com a palavra"
      ClipControls    =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1035
      Left            =   1110
      TabIndex        =   21
      Top             =   3990
      Visible         =   0   'False
      Width           =   2985
      Begin Bot�o.cmd cmdLocalizar 
         Height          =   255
         Left            =   2280
         TabIndex        =   23
         ToolTipText     =   "Finalizar Pesquisa"
         Top             =   690
         Width           =   585
         _ExtentX        =   1032
         _ExtentY        =   450
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCON050.frx":23D2
         PICN            =   "frmCON050.frx":23EE
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.TextBox txtLocalizar 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   120
         TabIndex        =   22
         Top             =   270
         Width           =   2775
      End
      Begin Bot�o.cmd cmdProximo 
         Height          =   255
         Left            =   120
         TabIndex        =   24
         ToolTipText     =   "Proximo"
         Top             =   690
         Width           =   585
         _ExtentX        =   1032
         _ExtentY        =   450
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCON050.frx":2788
         PICN            =   "frmCON050.frx":27A4
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
   End
   Begin MSWinsockLib.Winsock Winsock1 
      Left            =   9990
      Top             =   630
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin MSComctlLib.ListView lsvErros 
      Height          =   2145
      Left            =   5400
      TabIndex        =   11
      Top             =   1380
      Width           =   8535
      _ExtentX        =   15055
      _ExtentY        =   3784
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   5
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Cod Soft"
         Object.Width           =   1411
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Cod Erro:"
         Object.Width           =   1940
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Descri��o do Erro"
         Object.Width           =   5292
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Descri��o da Solu��o"
         Object.Width           =   6174
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "Seq"
         Object.Width           =   0
      EndProperty
   End
   Begin VB.Frame frmAdd 
      Height          =   5055
      Left            =   5400
      TabIndex        =   4
      Top             =   3570
      Width           =   8505
      Begin VB.TextBox txtSequencia 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3000
         Locked          =   -1  'True
         TabIndex        =   15
         TabStop         =   0   'False
         Top             =   450
         Width           =   810
      End
      Begin VB.TextBox txtDescSolucao 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1950
         Left            =   90
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   9
         Top             =   3030
         Width           =   8340
      End
      Begin VB.TextBox txtDescErro 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1710
         Left            =   90
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   7
         Top             =   1050
         Width           =   8340
      End
      Begin VB.TextBox txtCodigoErro 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   90
         TabIndex        =   5
         Top             =   450
         Width           =   2130
      End
      Begin Bot�o.cmd cmdSalvar 
         Height          =   825
         Left            =   7620
         TabIndex        =   12
         TabStop         =   0   'False
         ToolTipText     =   "Salvar"
         Top             =   150
         Width           =   825
         _ExtentX        =   1455
         _ExtentY        =   1455
         BTYPE           =   3
         TX              =   "Salvar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCON050.frx":2D3E
         PICN            =   "frmCON050.frx":2D5A
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdExcluir 
         Height          =   825
         Left            =   6750
         TabIndex        =   13
         TabStop         =   0   'False
         ToolTipText     =   "Excluir"
         Top             =   150
         Width           =   825
         _ExtentX        =   1455
         _ExtentY        =   1455
         BTYPE           =   3
         TX              =   "Excluir"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCON050.frx":3A34
         PICN            =   "frmCON050.frx":3A50
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdNovo 
         Height          =   825
         Left            =   5880
         TabIndex        =   17
         TabStop         =   0   'False
         ToolTipText     =   "Novo"
         Top             =   150
         Width           =   825
         _ExtentX        =   1455
         _ExtentY        =   1455
         BTYPE           =   3
         TX              =   "Novo"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCON050.frx":472A
         PICN            =   "frmCON050.frx":4746
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Sequ�ncia:"
         Height          =   195
         Left            =   3000
         TabIndex        =   16
         Top             =   240
         Width           =   810
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Descri��o da Solu��o:"
         Height          =   195
         Left            =   90
         TabIndex        =   10
         Top             =   2820
         Width           =   1620
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Descri��o do erro:"
         Height          =   195
         Left            =   90
         TabIndex        =   8
         Top             =   840
         Width           =   1305
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "C�digo do Erro:"
         Height          =   195
         Left            =   90
         TabIndex        =   6
         Top             =   240
         Width           =   1095
      End
   End
   Begin VB.ListBox lstSistemas 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7245
      Left            =   60
      TabIndex        =   3
      Top             =   1380
      Width           =   5325
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   2
      Top             =   1125
      Width           =   13965
      _ExtentX        =   24633
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   8670
      Width           =   13980
      _ExtentX        =   24659
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   11959
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   2646
            MinWidth        =   2646
            Object.ToolTipText     =   "Usu�rio da rede"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   2381
            MinWidth        =   2381
            Object.ToolTipText     =   "Usu�rio do banco de dados"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   3528
            MinWidth        =   3528
            Object.ToolTipText     =   "Banco de dados conectado"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            Alignment       =   1
            Object.Width           =   2381
            MinWidth        =   2381
            TextSave        =   "01/09/08"
            Object.ToolTipText     =   "Data"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   1
            Object.Width           =   1587
            MinWidth        =   1587
            TextSave        =   "17:41"
            Object.ToolTipText     =   "Hora"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   1005
      Left            =   12900
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Sair do sistema"
      Top             =   30
      Width           =   1005
      _ExtentX        =   1773
      _ExtentY        =   1773
      BTYPE           =   3
      TX              =   "Sair"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCON050.frx":5420
      PICN            =   "frmCON050.frx":543C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdPesquisar 
      Height          =   1005
      Left            =   11850
      TabIndex        =   18
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisar"
      Top             =   30
      Width           =   1005
      _ExtentX        =   1773
      _ExtentY        =   1773
      BTYPE           =   3
      TX              =   "Pesquisar"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCON050.frx":6116
      PICN            =   "frmCON050.frx":6132
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label6 
      AutoSize        =   -1  'True
      Caption         =   "Lista de Erros:"
      Height          =   195
      Left            =   5400
      TabIndex        =   20
      Top             =   1170
      Width           =   1005
   End
   Begin VB.Label Label5 
      AutoSize        =   -1  'True
      Caption         =   "Sistemas"
      Height          =   195
      Left            =   60
      TabIndex        =   19
      Top             =   1170
      Width           =   630
   End
   Begin VB.Label lblSoft 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   855
      Left            =   1920
      TabIndex        =   14
      Top             =   90
      Width           =   9615
   End
   Begin VB.Image Image1 
      Height          =   1185
      Left            =   -90
      Picture         =   "frmCON050.frx":6E0C
      Stretch         =   -1  'True
      ToolTipText     =   "Acessar a Intranet"
      Top             =   -45
      Width           =   1455
   End
End
Attribute VB_Name = "frmCON050"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdExcluir_Click()
    If Me.txtSequencia = "" Then
        MsgBox "Selecione um item da lista.", vbInformation, "Aten��o"
        Exit Sub
    End If
    
    If MsgBox("Confirma exclus�o?", vbQuestion + vbYesNo) = vbYes Then
        Conn.Execute "Delete from Erros where sequencia = " & Val(txtSequencia)
        cmdNovo_Click
        lstSistemas_Click
    End If
    
End Sub

Private Sub cmdLocalizar_Click()
    Frame1.Visible = False
End Sub

Private Sub cmdNovo_Click()
    txtCodigoErro = ""
    txtDescErro = ""
    txtSequencia = ""
    txtDescSolucao = ""
    txtCodigoErro.SetFocus
End Sub

Private Sub cmdPesquisar_Click()
    frmPesquisar.Show 1
End Sub

Private Sub cmdProximo_Click()
  For i = lstSistemas.ListIndex + 1 To Me.lstSistemas.ListCount - 1
        If InStr(1, UCase(lstSistemas.List(i)), UCase(txtLocalizar)) > 0 Then
            lstSistemas.Selected(i) = True
            Exit For
        End If
    Next
End Sub

Private Sub cmdSair_Click()
    
    oConn.Close
    Set oConn = Nothing
    
    Conn.Close
    Set Conn = Nothing
    
    End

End Sub

Private Sub cmdSalvar_Click()
    
    If lstSistemas.SelCount = 0 Then
        MsgBox "Selecione um sistema da lista.", vbInformation, "Aten��o"
        Exit Sub
    End If
        
    If txtDescErro = "" Then
       MsgBox "Informe a Descri��o do Erro.", vbInformation, "Aten��o"
       txtDescErro.SetFocus
       Exit Sub
    End If
    
    If txtDescSolucao = "" Then
       MsgBox "Informe a Descri��o da Solu��o.", vbInformation, "Aten��o"
       txtDescSolucao.SetFocus
       Exit Sub
    End If
    
    If txtSequencia = "" Then
        Conn.Execute "Insert into Erros (Cod_Soft, Cod_Erro, Desc_Erro, Desc_Solucao) values (" & Val(lstSistemas) & ",'" & Replace(txtCodigoErro, "'", "") & "','" & Replace(txtDescErro, "'", "") & "','" & Replace(txtDescSolucao, "'", "") & "')"
    Else
        Conn.Execute "UPDATE Erros set Cod_Erro='" & Replace(txtCodigoErro, "'", "") & "', Desc_Erro='" & Replace(txtDescErro, "'", "") & "', Desc_Solucao='" & Replace(txtDescSolucao, "'", "") & "' WHERE Sequencia =" & txtSequencia
    End If
    txtCodigoErro = ""
    txtDescErro = ""
    txtSequencia = ""
    txtDescSolucao = ""
    lstSistemas_Click
End Sub

Public Function DBOpen(Optional ConnectionString As String = "") As Boolean
On Error GoTo err_DBopen
   
   Set oConn = New ADODB.Connection
   
   If oConn.State = adStateClosed Then  'Check for previous connection
       If ConnectionString <> "" Then
           ConnString = ConnectionString
       End If
       oConn.CursorLocation = adUseClient
       oConn.Open ConnectionString
    
       Set connCommand = New ADODB.Command
       Set connCommand.ActiveConnection = oConn 'Set command object
   End If
   
   DBOpen = True
   
   Exit Function

err_DBopen:
   DBOpen = False
   Set oConn = Nothing
   'Err.Raise Err.Number, Err.source & "[DBOpen]", Err.Description

End Function

Private Sub Form_Load()
    Dim vLocalBanco As String
    
    frmLogo.Show 1
    
    If Winsock1.LocalIP = "10.10.6.123" Then
        vLocalBanco = "E:\"
    Else
        vLocalBanco = "H:\"
    End If
    
    Set Conn = New ADODB.Connection
    
    
    Conn.Open "Provider=Microsoft.Jet.OLEDB.4.0;" & _
              "Data Source=" & vLocalBanco & "Oracle\dados\32bits\Tratar_Erros.mdb;" & _
              "User Id=admin;" & _
              "Password=;"
    
   DBOpen "Provider=MSDAORA;" & _
           "Password=PROD;" & _
           "User ID=VDA020;" & _
           "Data Source=PRODUCAO;" & _
           "Persist Security Info=True"

    
    Me.Top = 0
    Me.Left = 0
    Preencher_Lista_Soft

End Sub

Public Sub lstSistemas_Click()
    Dim litem As ListItem
    
    lblSoft.Caption = ".:" & lstSistemas & ":."
    Set rst = New ADODB.Recordset
    rst.Open "Select * from Erros where cod_Soft = " & Val(lstSistemas), Conn, adOpenKeyset
    lsvErros.ListItems.Clear
    For i = 1 To rst.RecordCount
        Set litem = Me.lsvErros.ListItems.Add
        litem = rst!Cod_Soft
        litem.SubItems(1) = rst!Cod_Erro
        litem.SubItems(2) = rst!Desc_Erro
        litem.SubItems(3) = rst!Desc_Solucao
        litem.SubItems(4) = rst!Sequencia
        rst.MoveNext
    Next
    rst.Close
    Set rst = Nothing
    
    txtCodigoErro = ""
    txtDescErro = ""
    txtSequencia = ""
    txtDescSolucao = ""
    
End Sub

Private Sub lstSistemas_KeyPress(KeyAscii As Integer)
    vvezes = vvezes + 1
    lsvErros.SetFocus
    txtLocalizar = ""
    Frame1.Visible = True
    txtLocalizar.SetFocus
    txtLocalizar = Chr(KeyAscii)
    txtLocalizar.SelStart = 2
End Sub

Public Sub lsvErros_ItemClick(ByVal item As MSComctlLib.ListItem)
    txtCodigoErro = lsvErros.SelectedItem.SubItems(1)
    txtDescErro = lsvErros.SelectedItem.SubItems(2)
    txtDescSolucao = lsvErros.SelectedItem.SubItems(3)
    txtSequencia = lsvErros.SelectedItem.SubItems(4)
End Sub

Sub Preencher_Lista_Soft()
    Set rst = New ADODB.Recordset
    Set rst = rsSelect("Select COD_SOFTWARE, NOME_SOFTWARE, DESCRICAO FROM HELPDESK.SOFTWARE ORDER BY nome_SOFTWARE")
    Me.lstSistemas.Clear
    For i = 1 To rst.RecordCount
        lstSistemas.AddItem Format(rst!Cod_Software, "0000") & " - " & rst!nome_software & " - " & rst!DESCRICAO
        lstSistemas.ItemData(lstSistemas.NewIndex) = rst!Cod_Software
        rst.MoveNext
    Next
End Sub

Public Function rsSelect(Optional strCommand As String = "") As ADODB.Recordset
   
   If strCommand <> "" Then
       Set connCommand = New ADODB.Command
       connCommand.ActiveConnection = oConn
       connCommand.CommandText = strCommand
       connCommand.CommandType = adCmdText
       Set rsSelect = connCommand.Execute
   End If

End Function

Private Sub txtLocalizar_Change()
    For i = 0 To Me.lstSistemas.ListCount - 1
        If InStr(1, UCase(lstSistemas.List(i)), UCase(txtLocalizar)) > 0 Then
            lstSistemas.Selected(i) = True
            Exit For
        End If
    Next
End Sub

