VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmPesquisar 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Pesquisar"
   ClientHeight    =   5190
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9705
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "frmPesquisar.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5190
   ScaleWidth      =   9705
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ListView lsvPesquisar 
      Height          =   3375
      Left            =   30
      TabIndex        =   4
      Top             =   1770
      Width           =   9645
      _ExtentX        =   17013
      _ExtentY        =   5953
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   5
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Sequencia"
         Object.Width           =   1235
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Sistema"
         Object.Width           =   2469
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Codigo do Erro"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Descri��o do Erro"
         Object.Width           =   6174
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "Descri��o da Solu��o"
         Object.Width           =   6174
      EndProperty
   End
   Begin VB.TextBox txtPalavra 
      Appearance      =   0  'Flat
      Height          =   345
      Left            =   30
      TabIndex        =   0
      Top             =   1170
      Width           =   2625
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   3
      Top             =   810
      Width           =   9615
      _ExtentX        =   16960
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   30
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmPesquisar.frx":23D2
      PICN            =   "frmPesquisar.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd1 
      Default         =   -1  'True
      Height          =   690
      Left            =   9000
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisar"
      Top             =   60
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmPesquisar.frx":30C8
      PICN            =   "frmPesquisar.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "Informe uma palavra para Pesquisar:"
      Height          =   195
      Left            =   60
      TabIndex        =   6
      Top             =   930
      Width           =   2580
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Itens encontrados:"
      Height          =   195
      Left            =   30
      TabIndex        =   5
      Top             =   1560
      Width           =   1320
   End
End
Attribute VB_Name = "frmPesquisar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmd1_Click()
    Dim litem As ListItem
    Set rst = New ADODB.Recordset
    rst.Open "Select * from erros where cod_erro like '%" & txtPalavra & "%' OR ucase(Desc_Erro) like '%" & UCase(txtPalavra) & "%' OR ucase(Desc_Solucao) like '%" & UCase(txtPalavra) & "%'", Conn, adOpenKeyset
    lsvPesquisar.ListItems.Clear
    For i = 1 To rst.RecordCount
        Set litem = lsvPesquisar.ListItems.Add
        litem = rst!Sequencia
        For ii = 0 To frmCON050.lstSistemas.ListCount - 1
            If frmCON050.lstSistemas.ItemData(ii) = rst!Cod_Soft Then
                litem.SubItems(1) = frmCON050.lstSistemas.List(ii)
            End If
        Next
        litem.SubItems(2) = rst!Cod_Erro
        litem.SubItems(3) = rst!Desc_Erro
        litem.SubItems(4) = rst!Desc_Solucao
        rst.MoveNext
    Next
    rst.Close
    Set rst = Nothing
End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub

Private Sub lsvPesquisar_DblClick()
    Dim item As ListItem
    
    For i = 0 To frmCON050.lstSistemas.ListCount - 1
        If frmCON050.lstSistemas.List(i) = Me.lsvPesquisar.SelectedItem.SubItems(1) Then
            frmCON050.lstSistemas.Selected(i) = True
            For ii = 1 To frmCON050.lsvErros.ListItems.Count
                If Val(frmCON050.lsvErros.ListItems(ii).SubItems(4)) = Val(lsvPesquisar.SelectedItem) Then
                    frmCON050.lsvErros.ListItems(ii).Selected = True
                    Set item = frmCON050.lsvErros.SelectedItem
                    
                    frmCON050.lsvErros_ItemClick item
                End If
            Next
        End If
    Next
    Unload Me
End Sub
