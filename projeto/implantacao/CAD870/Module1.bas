Attribute VB_Name = "Module1"
Public orasession As Object
Public oradatabase As Object
Public oradynaset As Object

Function Numero(ByVal KeyAscii As Integer) As Integer
    If KeyAscii = 8 Then    'BACKSPACE
        Numero = KeyAscii
        Exit Function
    End If
    If Chr(KeyAscii) < "0" Or Chr(KeyAscii) > "9" Then
        KeyAscii = 0
    End If
    Numero = KeyAscii
End Function

Sub AAAAMM(ByRef KeyAscii, ByRef txtCampo)
    
    On Error GoTo TrataErro

    Dim bTam As Byte    'tamanho do campo JA digitado
    Dim strData As String
    Dim ParteAno As Double
    Dim bKey As Byte
    bTam = Len(txtCampo.Text)
    
    If KeyAscii = 8 And bTam > 0 Then 'backspace
        If Mid$(txtCampo.Text, bTam) = "/" Then
            txtCampo.Text = Mid$(txtCampo.Text, 1, bTam - 2)
        Else
            txtCampo.Text = Mid$(txtCampo.Text, 1, bTam - 1)
        End If
            
    ElseIf Chr$(KeyAscii) >= "0" And Chr$(KeyAscii) <= "9" Then
        If bTam = 5 Then 'mes
            strData = txtCampo.Text & Chr$(KeyAscii)
            If CInt(Mid(strData, 6, 1)) > 12 Then
                Beep
            Else
                txtCampo.Text = txtCampo.Text & Chr$(KeyAscii)
            End If
        
        ElseIf bTam = 6 Then 'mes
            strData = Mid$(txtCampo.Text, 6, 2)
            If CInt(strData > 12) Then
               Beep
            Else
               strData = txtCampo.Text & Chr$(KeyAscii)
               If Not IsDate(CDate(strData)) Then
                  Beep
               Else
                  txtCampo.Text = strData
               End If
            End If
            
        ElseIf bTam < 7 Then
        
            'SE ESTIVER NO PRIMEIRO ALGARISMO DO ANO, N�O PODE SER MENOR QUE 1
            If bTam = 0 Then
                ParteAno = Val(Mid$(txtCampo.Text, 4, 1)) & Val(Chr$(KeyAscii))
                If ParteAno < 1 Then
                    Beep
                Else
                    bKey = KeyAscii
                End If
            ElseIf bTam = 1 Then
                ParteAno = Val(Mid$(txtCampo.Text, 1, 2)) & Val(Chr$(KeyAscii))
                If ParteAno <= 18 Then
                    Beep
                Else
                    bKey = KeyAscii
                End If
            ElseIf bTam = 3 Then
                txtCampo.Text = txtCampo.Text & Chr$(KeyAscii) & "/"
            Else
                bKey = KeyAscii
            End If
        Else
            bKey = 0
        End If
    Else
        Beep
    End If
    
    SendKeys "{END}"
    KeyAscii = bKey
    Exit Sub
    
TrataErro:

    If Err.Number = 13 Then
        MsgBox strData, vbInformation, "Data Inv�lida"
        KeyAscii = 0
        Beep
        Err.Clear
    End If

End Sub

