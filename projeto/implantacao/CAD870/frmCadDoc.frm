VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Begin VB.Form frmCadDoc 
   Caption         =   "CAD870 - Cadastro de NF por Representante"
   ClientHeight    =   6075
   ClientLeft      =   3120
   ClientTop       =   3045
   ClientWidth     =   10440
   Icon            =   "frmCadDoc.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   6075
   ScaleWidth      =   10440
   Begin Threed.SSCommand sscmdSair 
      Height          =   555
      Left            =   9870
      TabIndex        =   26
      Top             =   30
      Width           =   555
      _Version        =   65536
      _ExtentX        =   979
      _ExtentY        =   979
      _StockProps     =   78
      Picture         =   "frmCadDoc.frx":0442
   End
   Begin VB.Frame frmAlteracao 
      Caption         =   "Altera��o de Documentos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   1905
      Left            =   60
      TabIndex        =   10
      Top             =   2070
      Width           =   5145
      Begin VB.ComboBox cmbData 
         Height          =   315
         Left            =   1575
         TabIndex        =   14
         Top             =   720
         Width           =   1680
      End
      Begin VB.TextBox txtReprAlt 
         Height          =   285
         Left            =   2115
         TabIndex        =   12
         Top             =   315
         Width           =   780
      End
      Begin VB.TextBox txtNotaAlt 
         Height          =   285
         Left            =   1575
         TabIndex        =   16
         Top             =   1170
         Width           =   1050
      End
      Begin Threed.SSCommand sscmdAlterar 
         Height          =   555
         Left            =   4455
         TabIndex        =   17
         Top             =   1200
         Width           =   555
         _Version        =   65536
         _ExtentX        =   979
         _ExtentY        =   979
         _StockProps     =   78
         Picture         =   "frmCadDoc.frx":075C
      End
      Begin Threed.SSCommand sscmdVoltarAlt 
         Height          =   555
         Left            =   3825
         TabIndex        =   30
         Top             =   1200
         Width           =   555
         _Version        =   65536
         _ExtentX        =   979
         _ExtentY        =   979
         _StockProps     =   78
         Picture         =   "frmCadDoc.frx":0A76
      End
      Begin VB.Label lblPseudoAlt 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   2970
         TabIndex        =   29
         Top             =   405
         Width           =   1950
      End
      Begin VB.Label Label7 
         Caption         =   "Ano/M�s:"
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   270
         TabIndex        =   13
         Top             =   855
         Width           =   825
      End
      Begin VB.Label Label6 
         Caption         =   "N�mero da Nota:"
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   270
         TabIndex        =   15
         Top             =   1260
         Width           =   1320
      End
      Begin VB.Label Label5 
         Caption         =   "C�d. do Representante: "
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   270
         TabIndex        =   11
         Top             =   405
         Width           =   1815
      End
   End
   Begin VB.Frame frmInclusao 
      Caption         =   "Inclus�o de Documentos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   1905
      Left            =   60
      TabIndex        =   0
      Top             =   4050
      Width           =   5145
      Begin Threed.SSCommand sscmdIncluir 
         Height          =   555
         Left            =   4500
         TabIndex        =   9
         Top             =   1275
         Width           =   555
         _Version        =   65536
         _ExtentX        =   979
         _ExtentY        =   979
         _StockProps     =   78
         Picture         =   "frmCadDoc.frx":0EC8
      End
      Begin VB.TextBox txtNumNota 
         Height          =   285
         Left            =   1575
         MaxLength       =   6
         TabIndex        =   8
         Top             =   1395
         Width           =   1050
      End
      Begin VB.TextBox txtData 
         Height          =   285
         Left            =   1575
         MaxLength       =   7
         TabIndex        =   6
         Top             =   1035
         Width           =   1050
      End
      Begin VB.TextBox txtCIC 
         Height          =   285
         Left            =   1575
         TabIndex        =   4
         Top             =   675
         Width           =   2175
      End
      Begin VB.TextBox txtCodRepres 
         Height          =   285
         Left            =   2115
         TabIndex        =   2
         Top             =   315
         Width           =   780
      End
      Begin Threed.SSCommand sscmdVoltarInc 
         Height          =   555
         Left            =   3900
         TabIndex        =   31
         Top             =   1275
         Width           =   555
         _Version        =   65536
         _ExtentX        =   979
         _ExtentY        =   979
         _StockProps     =   78
         Picture         =   "frmCadDoc.frx":11E2
      End
      Begin VB.Label Label9 
         Caption         =   "(AAAA/MM)"
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   2745
         TabIndex        =   28
         Top             =   1080
         Width           =   915
      End
      Begin VB.Label lblPseudo 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   3015
         TabIndex        =   27
         Top             =   405
         Width           =   1950
      End
      Begin VB.Label Label4 
         Caption         =   "C�d. do Representante: "
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   270
         TabIndex        =   1
         Top             =   405
         Width           =   1815
      End
      Begin VB.Label Label3 
         Caption         =   "N�mero da Nota:"
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   270
         TabIndex        =   7
         Top             =   1485
         Width           =   1320
      End
      Begin VB.Label Label2 
         Caption         =   "Ano/M�s:"
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   270
         TabIndex        =   5
         Top             =   1125
         Width           =   825
      End
      Begin VB.Label Label1 
         Caption         =   "N�mero do CIC: "
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   270
         TabIndex        =   3
         Top             =   765
         Width           =   1320
      End
   End
   Begin VB.Frame frmRegional 
      Caption         =   "Consulta por Regional"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   1980
      Left            =   4950
      TabIndex        =   22
      Top             =   60
      Width           =   4845
      Begin MSGrid.Grid grdRegional 
         Height          =   1185
         Left            =   75
         TabIndex        =   25
         Top             =   720
         Width           =   4695
         _Version        =   65536
         _ExtentX        =   8281
         _ExtentY        =   2090
         _StockProps     =   77
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Cols            =   3
      End
      Begin VB.TextBox txtRegional 
         Height          =   285
         Left            =   1515
         TabIndex        =   24
         Top             =   315
         Width           =   780
      End
      Begin VB.Label Label8 
         Caption         =   "C�d. da Regional: "
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   120
         TabIndex        =   23
         Top             =   405
         Width           =   1410
      End
   End
   Begin VB.Frame frmRepresentante 
      Caption         =   "Consulta por Representante"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   1980
      Left            =   30
      TabIndex        =   18
      Top             =   60
      Width           =   4875
      Begin MSGrid.Grid grdRepresentante 
         Height          =   1185
         Left            =   75
         TabIndex        =   21
         Top             =   720
         Width           =   4695
         _Version        =   65536
         _ExtentX        =   8281
         _ExtentY        =   2090
         _StockProps     =   77
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.TextBox txtReprCons 
         Height          =   285
         Left            =   1965
         TabIndex        =   20
         Top             =   315
         Width           =   780
      End
      Begin VB.Label Label10 
         Caption         =   "C�d. do Representante: "
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   120
         TabIndex        =   19
         Top             =   405
         Width           =   1815
      End
   End
   Begin VB.Menu mnuCadastro 
      Caption         =   "&Cadastro"
      Begin VB.Menu mnuInclusao 
         Caption         =   "&Inclus�o"
      End
      Begin VB.Menu mnuAlteracao 
         Caption         =   "&Altera��o"
      End
   End
   Begin VB.Menu mnuConsulta 
      Caption         =   "Co&nsulta"
      Begin VB.Menu mnuRepres 
         Caption         =   "por &Representante"
      End
      Begin VB.Menu mnuRegional 
         Caption         =   "por Re&gional"
      End
   End
   Begin VB.Menu mnuSair 
      Caption         =   "&Sair"
   End
   Begin VB.Menu mnuSobre 
      Caption         =   "S&obre"
   End
End
Attribute VB_Name = "frmCadDoc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Limpa_Grid_Repres()
    If grdRepresentante <> "" Then
        With grdRepresentante
            .Rows = 2
            .Row = 1
            
            For I = 0 To .Cols - 1
                .Col = I: .Text = ""
            Next I
        End With
    
    Call Inicia_Grid_Repres
    End If
End Sub

Private Sub Inicia_Grid_Repres()
    grdRepresentante.Row = 0
    grdRepresentante.Col = 0
    grdRepresentante.ColWidth(0) = 1200
    grdRepresentante.FixedAlignment(0) = 2
    grdRepresentante.Text = "ANO/MES"
    grdRepresentante.Col = 1
    grdRepresentante.ColWidth(1) = 1200
    grdRepresentante.FixedAlignment(1) = 2
    grdRepresentante.Text = "NUM.NOTA"
End Sub

Private Sub Limpa_Grid_Regional()
    If grdRegional <> "" Then
        With grdRegional
            .Rows = 2
            .Row = 1
            
            For I = 0 To .Cols - 1
                .Col = I: .Text = ""
            Next I
        End With
    
    Call Inicia_Grid_Regional
    End If
End Sub

Private Sub Inicia_Grid_Regional()
    grdRegional.Row = 0
    grdRegional.Col = 0
    grdRegional.ColWidth(0) = 1200
    grdRegional.FixedAlignment(0) = 2
    grdRegional.Text = "COD.REPRES"
    grdRegional.Col = 1
    grdRegional.ColWidth(1) = 1200
    grdRegional.FixedAlignment(1) = 2
    grdRegional.Text = "ANO/MES"
    grdRegional.Col = 2
    grdRegional.ColWidth(2) = 1200
    grdRegional.FixedAlignment(2) = 2
    grdRegional.Text = "NUM.NOTA"

End Sub

Private Sub Form_Load()
    Dim CMD As String
        
   'ESTABELECE CONEX�O COM O ORACLE
    Set orasession = CreateObject("oracleinproCServer.xorasession")
    Set oradatabase = orasession.OpenDatabase("PRODUCAO", "CAD870/PROD", 0&)
   'Set oradatabase = orasession.OpenDatabase("DESENV", "PRODUCAO/DES", 0&)
    
    frmInclusao.Visible = False
    frmAlteracao.Visible = False
    frmRepresentante.Visible = False
    frmRegional.Visible = False
End Sub

Private Sub mnuAlteracao_Click()
    frmInclusao.Visible = False
    frmAlteracao.Visible = True
    frmRepresentante.Visible = False
    frmRegional.Visible = False
    txtReprAlt.SetFocus
End Sub

Private Sub mnuInclusao_Click()
    frmInclusao.Visible = True
    frmAlteracao.Visible = False
    frmRepresentante.Visible = False
    frmRegional.Visible = False
    txtCodRepres.SetFocus
End Sub

Private Sub mnuRegional_Click()
    frmInclusao.Visible = False
    frmAlteracao.Visible = False
    frmRepresentante.Visible = False
    frmRegional.Visible = True
    txtRegional.SetFocus
End Sub

Private Sub mnuRepres_Click()
    frmInclusao.Visible = False
    frmAlteracao.Visible = False
    frmRepresentante.Visible = True
    frmRegional.Visible = False
    txtReprCons.SetFocus
End Sub

Private Sub mnuSair_Click()
    End
End Sub

Private Sub mnuSobre_Click()
    frmSobre.Show vbModal
End Sub

Private Sub sscmdAlterar_Click()
    Dim Data As Double
    Dim CIC
    
    CMD = "Select CIC " & _
          "from REPRESENTANTE " & _
          "where COD_REPRES = " & txtReprAlt.Text
      
    Set oradynaset = oradatabase.dbcreatedynaset(CMD, 0&)
    
    CIC = oradynaset!CIC
    Data = Mid(cmbData.Text, 1, 4) & Mid(cmbData.Text, 6, 2)

    CMD = "Update COMISSAO.REPRES_NF " & _
          "set NUM_NOTA = " & txtNotaAlt.Text & " " & _
          "where CIC =" & CIC & " and ANO_MES = " & Data
        
    oradatabase.dbexecutesql CMD
    
    MsgBox "Registro alterado com sucesso!"
    
    txtReprAlt.Text = ""
    cmbData.Clear
    txtNotaAlt.Text = ""
    lblPseudoAlt.Caption = ""
    
End Sub

Private Sub sscmdIncluir_Click()
    Dim AnoMes As Double

    If txtCodRepres.Text = "" Then
        MsgBox "Digite o c�digo do Representante!"
        txtCodRepres.SetFocus
        Exit Sub
    ElseIf txtCIC.Text = "" Then
        MsgBox "Digite o CIC do Representante!"
        txtCIC.SetFocus
        Exit Sub
    ElseIf txtData.Text = "" Then
        MsgBox "Digite o ANO/MES para cadastrar!"
        txtData.SetFocus
        Exit Sub
    ElseIf txtNumNota.Text = "" Then
        MsgBox "Digite a nota do Representante!"
        txtNumNota.SetFocus
        Exit Sub
    End If

    AnoMes = Mid(txtData.Text, 1, 4) & Mid(txtData.Text, 6, 2)

    CMD = "Select * " & _
          "from COMISSAO.REPRES_NF " & _
          "where CIC = " & txtCIC.Text & " and " & _
          "      ANO_MES = " & AnoMes
      
    Set oradynaset = oradatabase.dbcreatedynaset(CMD, 0&)
    
    If Not oradynaset.EOF Then
        MsgBox "J� existe registro cadastrado para esta data. Favor verificar!"
    Else
        CMD = "Insert into COMISSAO.REPRES_NF " & _
              "values (" & txtCIC.Text & "," & AnoMes & "," & txtNumNota.Text & ")"
            
        oradatabase.dbexecutesql CMD
    
        MsgBox "Registro gravado com sucesso!"
        
        txtCodRepres.Text = ""
        txtCIC.Text = ""
        txtData.Text = ""
        txtNumNota.Text = ""
        lblPseudo.Caption = ""
    End If
End Sub

Private Sub sscmdSair_Click()
    End
End Sub

Private Sub sscmdVoltarAlt_Click()
    frmAlteracao.Visible = False
End Sub

Private Sub sscmdVoltarInc_Click()
    frmInclusao.Visible = False
End Sub

Private Sub txtCodRepres_Click()
    txtCodRepres.Text = ""
    txtCIC.Text = ""
    txtData.Text = ""
    txtNumNota.Text = ""
    lblPseudo.Caption = ""
End Sub

Private Sub txtCodRepres_LostFocus()
    If txtCodRepres.Text <> "" Then
        CMD = "Select CIC, PSEUDONIMO " & _
              "from REPRESENTANTE " & _
              "where COD_REPRES = " & txtCodRepres.Text
          
        Set oradynaset = oradatabase.dbcreatedynaset(CMD, 0&)
        
        lblPseudo.Caption = oradynaset!PSEUDONIMO
        txtCIC.Text = oradynaset!CIC
        txtData.SetFocus
    End If
End Sub

Private Sub txtData_KeyPress(KeyAscii As Integer)
    Call AAAAMM(KeyAscii, txtData)
End Sub

Private Sub txtData_LostFocus()
    Dim vDataAtual As Date
    Dim vObj As Object
    
    If txtData = "" Then
        MsgBox "Informe o ano e o mes.", vbInformation, "Aten��o"
        txtData.SetFocus
        Exit Sub
    End If
        
    Set vObj = oradatabase.dbcreatedynaset("Select to_char(sysdate,'DD/MM/RR') from dual", 0&)
    
    vDataAtual = vObj.fields(0).Value
    
    If CInt(Left(txtData, 4)) > Year(vDataAtual) Then
        MsgBox "Ano n�o pode ser maior que ano atual.", vbInformation, "Aten��o"
        txtData.SetFocus
        Exit Sub
    End If
    
    If CInt(Mid(txtData, 6, 2)) > 12 Then
        MsgBox "M�s inv�lido.", vbInformation, "Aten��o"
        txtData.SetFocus
        Exit Sub
    End If
    
End Sub

Private Sub txtNumNota_KeyPress(KeyAscii As Integer)
    KeyAscii = Numero(KeyAscii)
End Sub

Private Sub txtRegional_Click()
    Call Limpa_Grid_Regional
    txtRegional.Text = ""
End Sub

Private Sub txtRegional_LostFocus()
    Call Limpa_Grid_Regional
    Call Inicia_Grid_Regional
    
    If txtRegional.Text <> "" Then
        CMD = ""
        CMD = "select a.cod_repres,b.ano_mes,b.num_nota " & _
              "from representante a, comissao.repres_nf b, filial c " & _
              "where c.cod_regional= " & txtRegional.Text & " and a.cod_filial=c.cod_filial and " & _
              "a.CIC = b.CIC order by b.ANO_MES"
          
        Set oradynaset = oradatabase.dbcreatedynaset(CMD, 0&)
        
        If oradynaset.EOF Then
            MsgBox "N�o existe registro de notas para esta regional!"
        Else
            oradynaset.movelast
            oradynaset.movefirst
            grdRegional.Rows = oradynaset.recordcount + 1
            grdRegional.Row = 0
            Do While Not oradynaset.EOF
                grdRegional.Row = grdRegional.Row + 1
                grdRegional.Col = 0
                grdRegional.Text = oradynaset!cod_repres
                grdRegional.Col = 1
                grdRegional.Text = Mid(oradynaset!ano_mes, 1, 4) & "/" & Mid(oradynaset!ano_mes, 5, 2)
                grdRegional.Col = 2
                grdRegional.Text = oradynaset!num_nota
                oradynaset.movenext
            Loop
        End If
        sscmdSair.SetFocus
    End If
End Sub

Private Sub txtReprAlt_LostFocus()
    If txtReprAlt.Text <> "" Then
        CMD = ""
        CMD = "Select a.PSEUDONIMO, b.ANO_MES " & _
              "from REPRESENTANTE a, COMISSAO.REPRES_NF b " & _
              "where a.COD_REPRES = " & txtReprAlt.Text & " and " & _
              "a.CIC = b.CIC order by b.ANO_MES"
          
        Set oradynaset = oradatabase.dbcreatedynaset(CMD, 0&)
        If oradynaset.EOF Then
            MsgBox "N�o existe registro para este representante!"
        Else
            lblPseudoAlt.Caption = oradynaset!PSEUDONIMO
            cmbData.Clear
            oradynaset.movelast
            oradynaset.movefirst
            Do While Not oradynaset.EOF
                cmbData.AddItem Mid(oradynaset!ano_mes, 1, 4) & "/" & Mid(oradynaset!ano_mes, 5, 2)
                oradynaset.movenext
            Loop
        End If
    End If
End Sub

Private Sub txtReprCons_Click()
    Call Limpa_Grid_Repres
    txtReprCons.Text = ""
End Sub

Private Sub txtReprCons_LostFocus()
    Call Limpa_Grid_Repres
    Call Inicia_Grid_Repres
    
    If txtReprCons.Text <> "" Then
        CMD = ""
        CMD = "select b.ano_mes,b.num_nota " & _
              "from representante a, comissao.repres_nf b " & _
              "where a.cod_repres= " & txtReprCons.Text & " and " & _
              "a.CIC = b.CIC order by b.ANO_MES"
          
        Set oradynaset = oradatabase.dbcreatedynaset(CMD, 0&)
        
        If oradynaset.EOF Then
            MsgBox "N�o existe registro de notas para este representante!"
        Else
            oradynaset.movelast
            oradynaset.movefirst
            grdRepresentante.Rows = oradynaset.recordcount + 1
            grdRepresentante.Row = 0
            Do While Not oradynaset.EOF
                grdRepresentante.Row = grdRepresentante.Row + 1
                grdRepresentante.Col = 0
                grdRepresentante.Text = Mid(oradynaset!ano_mes, 1, 4) & "/" & Mid(oradynaset!ano_mes, 5, 2)
                grdRepresentante.Col = 1
                grdRepresentante.Text = oradynaset!num_nota
                oradynaset.movenext
            Loop
        End If
        sscmdSair.SetFocus
    End If

End Sub
