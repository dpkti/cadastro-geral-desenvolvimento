VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form frmPrinc 
   Caption         =   "CAD100 - Cadastro de Loja"
   ClientHeight    =   5295
   ClientLeft      =   2700
   ClientTop       =   1860
   ClientWidth     =   6735
   Icon            =   "frmPrinc.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5295
   ScaleWidth      =   6735
   Begin VB.ComboBox cmbCodCidade 
      Height          =   315
      Left            =   4320
      TabIndex        =   15
      Top             =   2880
      Width           =   2295
   End
   Begin VB.TextBox txtReparticao 
      Height          =   285
      Left            =   1920
      MaxLength       =   14
      TabIndex        =   27
      Top             =   4800
      Width           =   1935
   End
   Begin VB.TextBox txtInscricao 
      Height          =   285
      Left            =   1920
      MaxLength       =   14
      TabIndex        =   25
      Top             =   4320
      Width           =   1935
   End
   Begin VB.TextBox txtDtCadastro 
      Height          =   285
      Left            =   5400
      TabIndex        =   7
      Top             =   1440
      Width           =   1215
   End
   Begin VB.TextBox txtFax 
      Height          =   285
      Left            =   4440
      MaxLength       =   13
      TabIndex        =   23
      Top             =   3840
      Width           =   2175
   End
   Begin VB.TextBox txtFone 
      Height          =   285
      Left            =   840
      MaxLength       =   9
      TabIndex        =   21
      Top             =   3840
      Width           =   2175
   End
   Begin VB.TextBox txtDDD 
      Height          =   285
      Left            =   5400
      MaxLength       =   4
      TabIndex        =   19
      Top             =   3360
      Width           =   1215
   End
   Begin VB.TextBox txtCEP 
      Height          =   285
      Left            =   840
      MaxLength       =   8
      TabIndex        =   17
      Top             =   3360
      Width           =   1695
   End
   Begin VB.TextBox txtBairro 
      Height          =   285
      Left            =   840
      MaxLength       =   15
      TabIndex        =   13
      Top             =   2880
      Width           =   1695
   End
   Begin VB.TextBox txtEndereco 
      Height          =   285
      Left            =   1560
      MaxLength       =   30
      TabIndex        =   11
      Top             =   2400
      Width           =   5055
   End
   Begin VB.TextBox txtFantasia 
      Height          =   285
      Left            =   1560
      MaxLength       =   10
      TabIndex        =   5
      Top             =   1440
      Width           =   1815
   End
   Begin VB.TextBox txtRazao 
      Height          =   285
      Left            =   1560
      MaxLength       =   40
      TabIndex        =   9
      Top             =   1920
      Width           =   5055
   End
   Begin VB.TextBox txtCGC 
      Height          =   285
      Left            =   4680
      MaxLength       =   14
      TabIndex        =   3
      Top             =   960
      Width           =   1935
   End
   Begin VB.TextBox txtCodLoja 
      Height          =   285
      Left            =   1560
      MaxLength       =   2
      TabIndex        =   1
      Top             =   930
      Width           =   735
   End
   Begin Threed.SSPanel SSPanel 
      Height          =   735
      Left            =   0
      TabIndex        =   32
      Top             =   0
      Width           =   6735
      _Version        =   65536
      _ExtentX        =   11880
      _ExtentY        =   1296
      _StockProps     =   15
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin Threed.SSCommand sscGravar 
         Height          =   495
         Left            =   120
         TabIndex        =   28
         Top             =   120
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   873
         _StockProps     =   78
         Picture         =   "frmPrinc.frx":0442
      End
      Begin Threed.SSCommand sscSobre 
         Height          =   495
         Left            =   5160
         TabIndex        =   30
         Top             =   120
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   873
         _StockProps     =   78
         Picture         =   "frmPrinc.frx":075C
      End
      Begin Threed.SSCommand sscSair 
         Height          =   495
         Left            =   6000
         TabIndex        =   31
         Top             =   120
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   873
         _StockProps     =   78
         Picture         =   "frmPrinc.frx":086E
      End
      Begin Threed.SSCommand sscConsultar 
         Height          =   495
         Left            =   960
         TabIndex        =   29
         Top             =   120
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   873
         _StockProps     =   78
         Picture         =   "frmPrinc.frx":0B88
      End
   End
   Begin VB.Label lblReparticao 
      AutoSize        =   -1  'True
      Caption         =   "Reparti��o Fiscal:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   195
      Left            =   120
      TabIndex        =   26
      Top             =   4800
      Width           =   1560
   End
   Begin VB.Label lblFax 
      AutoSize        =   -1  'True
      Caption         =   "Fax:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   195
      Left            =   3840
      TabIndex        =   22
      Top             =   3840
      Width           =   375
   End
   Begin VB.Label lblInscricao 
      AutoSize        =   -1  'True
      Caption         =   "Inscri��o Estadual:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   195
      Left            =   120
      TabIndex        =   24
      Top             =   4320
      Width           =   1650
   End
   Begin VB.Label lblFone 
      AutoSize        =   -1  'True
      Caption         =   "Fone:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   195
      Left            =   120
      TabIndex        =   20
      Top             =   3840
      Width           =   495
   End
   Begin VB.Label lblDtCadastro 
      AutoSize        =   -1  'True
      Caption         =   "Data de Cadastro:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   195
      Left            =   3720
      TabIndex        =   6
      Top             =   1440
      Width           =   1560
   End
   Begin VB.Label lblDDD 
      AutoSize        =   -1  'True
      Caption         =   "DDD:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   195
      Left            =   4800
      TabIndex        =   18
      Top             =   3360
      Width           =   480
   End
   Begin VB.Label lblBairro 
      AutoSize        =   -1  'True
      Caption         =   "Bairro:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   195
      Left            =   120
      TabIndex        =   12
      Top             =   2880
      Width           =   570
   End
   Begin VB.Label lblCEP 
      AutoSize        =   -1  'True
      Caption         =   "CEP:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   195
      Left            =   120
      TabIndex        =   16
      Top             =   3360
      Width           =   435
   End
   Begin VB.Label lblEndereco 
      AutoSize        =   -1  'True
      Caption         =   "Endere�o:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   195
      Left            =   120
      TabIndex        =   10
      Top             =   2400
      Width           =   885
   End
   Begin VB.Label lblCodCidade 
      Alignment       =   1  'Right Justify
      Caption         =   "C�d. da Cidade:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   315
      Left            =   2760
      TabIndex        =   14
      Top             =   2880
      Width           =   1455
   End
   Begin VB.Label lblFantasia 
      AutoSize        =   -1  'True
      Caption         =   "Nome Fantasia:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   195
      Left            =   120
      TabIndex        =   4
      Top             =   1440
      Width           =   1335
   End
   Begin VB.Label lblRazao 
      AutoSize        =   -1  'True
      Caption         =   "Raz�o Social:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   195
      Left            =   120
      TabIndex        =   8
      Top             =   1920
      Width           =   1200
   End
   Begin VB.Label lblCGC 
      AutoSize        =   -1  'True
      Caption         =   "N�mero do CGC:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   195
      Left            =   3120
      TabIndex        =   2
      Top             =   960
      Width           =   1545
   End
   Begin VB.Label lblCodLoja 
      AutoSize        =   -1  'True
      Caption         =   "C�digo da Loja:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   195
      Left            =   120
      TabIndex        =   0
      Top             =   960
      Width           =   1335
   End
   Begin VB.Menu mnu 
      Caption         =   "&Menu"
      Begin VB.Menu mnuIncl 
         Caption         =   "&Gravar"
      End
      Begin VB.Menu mnuCons 
         Caption         =   "&Consultar"
      End
   End
   Begin VB.Menu mnuSobre 
      Caption         =   "S&obre"
   End
   Begin VB.Menu mnuSair 
      Caption         =   "&Sair"
   End
End
Attribute VB_Name = "frmPrinc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Sub VALIDA_IE()

On Error GoTo Erro
Dim vDtIE As String     'Guarda m�s e ano (MMAAAA) de cadastramento da IE qdo. UF = "AC" ou "RO"
Dim LstrSQL As String
Dim valor_IE As Integer

Call cmbCodCidade_LostFocus

'Se a UF for "AC" ou "RO" � preciso digitar m�s e ano de cadastramento da IE, sen�o passa
'6 espa�os em branco como par�metro para a fun��o
If (ResultUF = "AC" Or ResultUF = "RO") Then
    Screen.MousePointer = 0
    If ResultUF = "AC" And Len(txtInscricao) = 9 Then
        vDtIE = "101999"
    ElseIf ResultUF = "AC" And Len(txtInscricao) = 13 Then
        vDtIE = "121999"
    ElseIf ResultUF = "RO" And Len(txtInscricao) = 9 Then
        vDtIE = "062000"
    ElseIf ResultUF = "RO" And Len(txtInscricao) = 14 Then
        vDtIE = "092000"
    Else
        MsgBox "Tamanho da Inscri��o Estadual incorreto!"
        txtInscricao.SetFocus
    End If
Else
    vDtIE = "      "
End If
        
'Se a IE n�o for Isenta, ent�o chama a fun��o para validar a IE
If txtInscricao <> "" And txtInscricao <> "ISENTO" Then
            
        LstrSQL = "BEGIN PRODUCAO.PCK_IE.PR_IE(:UF,:DT_IE,:IE,:IE_VALIDA); END; "
            
        oradatabase.Parameters.Remove "UF"
        oradatabase.Parameters.Add "UF", ResultUF, 1
        oradatabase.Parameters.Remove "DT_IE"
        oradatabase.Parameters.Add "DT_IE", vDtIE, 1
        oradatabase.Parameters.Remove "IE"
        oradatabase.Parameters.Add "IE", txtInscricao, 1
        oradatabase.Parameters.Remove "IE_VALIDA"
        oradatabase.Parameters.Add "IE_VALIDA", 0, 3
        
        oradatabase.Executesql LstrSQL
     
        valor_IE = oradatabase.Parameters("IE_VALIDA")
     
        'Se fun��o retornar:
        '0 = IE ok
        '1 = IE n�o ok
        If valor_IE = 1 Then
            Screen.MousePointer = 0
            MsgBox "INSCRI��O ESTADUAL INV�LIDA!", 16, "Aten��o!"
            txtInscricao.SetFocus
            Exit Sub
        End If
     
        Screen.MousePointer = 0
     
End If

Exit Sub
Erro:
MsgBox Err.Description, 16, Me.Caption


End Sub


Private Sub chkDesativar_Click()
    If chkDesativar.Value = 0 Then
        txtCodLoja.Enabled = True
        txtCGC.Enabled = True
        txtRazao.Enabled = True
        txtFantasia.Enabled = True
        txtEndereco.Enabled = True
        txtBairro.Enabled = True
        cmbCodCidade.Enabled = True
        txtCEP.Enabled = True
        txtDDD.Enabled = True
        txtFone.Enabled = True
        txtFax.Enabled = True
        txtDtCadastro.Enabled = True
        txtInscricao.Enabled = True
        txtReparticao.Enabled = True
    ElseIf chkDesativar.Value = 1 Then
        txtCodLoja.Enabled = False
        txtCGC.Enabled = False
        txtRazao.Enabled = False
        txtFantasia.Enabled = False
        txtEndereco.Enabled = False
        txtBairro.Enabled = False
        cmbCodCidade.Enabled = False
        txtCEP.Enabled = False
        txtDDD.Enabled = False
        txtFone.Enabled = False
        txtFax.Enabled = False
        txtDtCadastro.Enabled = False
        txtInscricao.Enabled = False
        txtReparticao.Enabled = False
    End If
End Sub

Private Sub cmbCodCidade_LostFocus()

    Dim varCidade As String
    Dim varNumCidade As Integer
    Dim ResultCidade As Integer
    
    If Trim(cmbCodCidade) = "" Then Exit Sub
    
    varCidade = cmbCodCidade.Text
    varNumCidade = InStr(varCidade, "-") - 1
    ResultCidade = Mid(varCidade, 1, varNumCidade)

    LstrSQL = " SELECT b.cod_uf FROM cidade a, uf b " & _
              " WHERE a.cod_cidade = " & ResultCidade & " and " & _
              " a.cod_uf = b.cod_uf "
    
    Set oradynaset = oradatabase.dbcreatedynaset(LstrSQL, 0&)

    If Not oradynaset.EOF Then
        ResultUF = oradynaset!cod_uf
    End If
    
    
    
End Sub


Private Sub Form_Load()
   'Conexao oracle
     
    Set orasession = CreateObject("oracleinprocserver.xorasession")
   'Set oradatabase = orasession.OpenDatabase("DESENV", "PRODUCAO/DES", 0&)
   'Set oradatabase = orasession.OpenDatabase("SDPK", "CAD100/PROD", 0&)
    Set oradatabase = orasession.OpenDatabase("PRODUCAO", "CAD100/PROD", 0&)
    
    Dim LstrSQL As String
    Dim codcidade

    LstrSQL = "Select cod_cidade, nome_cidade from cidade order by nome_cidade"
    
    Set oradynaset = oradatabase.dbcreatedynaset(LstrSQL, 0&)

    Do While Not oradynaset.EOF
        CodNome = oradynaset!COD_CIDADE & " - " & oradynaset!nome_cidade
        cmbCodCidade.AddItem CodNome
        oradynaset.dbMoveNext
    Loop
    
    vCharEspeciais = fCharEspeciais
    
End Sub

Private Sub mnuCons_Click()
    Screen.MousePointer = 11
   
    If Trim(txtCodLoja) = "" Then
        MsgBox "Digite o c�digo da Loja.", vbInformation, "Aten��o"
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
   
   'Traz os dados de todas as lojas existentes no banco
    LstrSQL = " SELECT a.*, b.nome_cidade FROM loja a, cidade b "
    LstrSQL = LstrSQL & " WHERE a.cod_loja = " & txtCodLoja & " and "
    LstrSQL = LstrSQL & " a.cod_cidade=b.cod_cidade"
    
    Set oradynaset = oradatabase.dbcreatedynaset(LstrSQL, 0&)

    If oradynaset.EOF Then
        MsgBox ("N�o existe loja correspondente a este c�digo. Tente novamente!")
        txtCodLoja.Text = ""
        sscSair.SetFocus
        Screen.MousePointer = 0
        Exit Sub
    End If
        
    If IsNull(oradynaset!CGC) Then
        txtCGC.Text = "0"
    Else
        txtCGC.Text = oradynaset!CGC
    End If
    If IsNull(oradynaset!NOME_FANTASIA) Then
        txtFantasia.Text = " "
    Else
        txtFantasia = oradynaset!NOME_FANTASIA
    End If
    If IsNull(oradynaset!DT_CADASTR) Then
        txtFantasia.Text = "0"
    Else
        txtDtCadastro = oradynaset!DT_CADASTR
    End If
    If IsNull(oradynaset!RAZAO_SOCIAL) Then
        txtRazao.Text = " "
    Else
        txtRazao = oradynaset!RAZAO_SOCIAL
    End If
    If IsNull(oradynaset!ENDERECO) Then
        txtEndereco.Text = " "
    Else
        txtEndereco = oradynaset!ENDERECO
    End If
    If IsNull(oradynaset!BAIRRO) Then
        txtBairro.Text = " "
    Else
        txtBairro = oradynaset!BAIRRO
    End If
    If IsNull(oradynaset!COD_CIDADE) Then
        cmbCodCidade.Text = "Selecionar"
    Else
        cmbCodCidade = oradynaset!COD_CIDADE & " - " & oradynaset!nome_cidade
    End If
    If IsNull(oradynaset!CEP) Then
        txtCEP.Text = "0"
    Else
        txtCEP = oradynaset!CEP
    End If
    If IsNull(oradynaset!DDD) Then
        txtDDD.Text = "0"
    Else
        txtDDD = oradynaset!DDD
    End If
    If IsNull(oradynaset!FONE) Then
        txtFone.Text = "0"
    Else
        txtFone = oradynaset!FONE
    End If
    If IsNull(oradynaset!FAX) Then
        txtFax.Text = "0"
    Else
        txtFax = oradynaset!FAX
    End If
    If IsNull(oradynaset!INSCR_ESTADUAL) Then
        txtInscricao.Text = "0"
    Else
        txtInscricao = oradynaset!INSCR_ESTADUAL
    End If
    If IsNull(oradynaset!REPART_FISCAL) Then
        txtReparticao.Text = "0"
    Else
        txtReparticao = oradynaset!REPART_FISCAL
    End If
    
    Screen.MousePointer = 0
    sscSair.SetFocus

End Sub

Private Sub mnuIncl_Click()
   
    Screen.MousePointer = 11
   'Traz os dados de todas as lojas existentes no banco
    LstrSQL = " SELECT * FROM loja "
    LstrSQL = LstrSQL & " WHERE cod_loja = '" & txtCodLoja.Text & "' "
    
    Set oradynaset = oradatabase.dbcreatedynaset(LstrSQL, 0&)

    If oradynaset.EOF Then
        'Insert
        LstrSQL = " Begin "
        LstrSQL = LstrSQL & " Insert into loja (COD_LOJA, CGC, RAZAO_SOCIAL, NOME_FANTASIA, "
        LstrSQL = LstrSQL & " ENDERECO, BAIRRO, COD_CIDADE, CEP, DDD, FONE, FAX, DT_CADASTR, "
        LstrSQL = LstrSQL & " INSCR_ESTADUAL, REPART_FISCAL) "
        LstrSQL = LstrSQL & " values (:COD_LOJA, :CGC, :RAZAO_SOCIAL, "
        LstrSQL = LstrSQL & " :NOME_FANTASIA, :ENDERECO, :BAIRRO, :COD_CIDADE, :CEP, "
        LstrSQL = LstrSQL & " :DDD, :FONE, :FAX, to_date(:DT_CADASTR,'dd/mm/rr'), :INSCR_ESTADUAL, :REPART_FISCAL);"
        LstrSQL = LstrSQL & " Commit; "
        LstrSQL = LstrSQL & " Exception "
        LstrSQL = LstrSQL & " When Others then "
        LstrSQL = LstrSQL & " RollBack; "
        LstrSQL = LstrSQL & " :Cod_ErrorPl :=SQLCODE; "
        LstrSQL = LstrSQL & " :Txt_ErrorPl :=SQLERRM; "
        LstrSQL = LstrSQL & " End; "
        
        oradatabase.Parameters.Remove "Cod_ErrorPl"
        oradatabase.Parameters.Remove "Txt_ErrorPl"
        oradatabase.Parameters.Remove "COD_LOJA"
        oradatabase.Parameters.Remove "CGC"
        oradatabase.Parameters.Remove "RAZAO_SOCIAL"
        oradatabase.Parameters.Remove "NOME_FANTASIA"
        oradatabase.Parameters.Remove "ENDERECO"
        oradatabase.Parameters.Remove "BAIRRO"
        oradatabase.Parameters.Remove "COD_CIDADE"
        oradatabase.Parameters.Remove "CEP"
        oradatabase.Parameters.Remove "DDD"
        oradatabase.Parameters.Remove "FONE"
        oradatabase.Parameters.Remove "FAX"
        oradatabase.Parameters.Remove "DT_CADASTR"
        oradatabase.Parameters.Remove "INSCR_ESTADUAL"
        oradatabase.Parameters.Remove "REPART_FISCAL"
        
        oradatabase.Parameters.Add "Cod_ErrorPl", 0, 2
        oradatabase.Parameters.Add "Txt_ErrorPl", "", 2
       'linha de tratamento de erro para a nova OLE
        oradatabase.Parameters("cod_errorpl").serverType = ORATYPE_NUMBER
       'linha de tratamento de erro para a nova OLE
        oradatabase.Parameters("txt_errorpl").serverType = ORATYPE_STRING
        
        oradatabase.Parameters.Add "COD_LOJA", txtCodLoja.Text, 1
        oradatabase.Parameters.Add "CGC", txtCGC.Text, 1
        oradatabase.Parameters.Add "RAZAO_SOCIAL", txtRazao.Text, 1
        oradatabase.Parameters.Add "NOME_FANTASIA", txtFantasia.Text, 1
        oradatabase.Parameters.Add "ENDERECO", txtEndereco.Text, 1
        oradatabase.Parameters.Add "BAIRRO", txtBairro.Text, 1
        oradatabase.Parameters.Add "COD_CIDADE", Left(cmbCodCidade.Text, 4), 1
        oradatabase.Parameters.Add "CEP", txtCEP.Text, 1
        oradatabase.Parameters.Add "DDD", txtDDD.Text, 1
        oradatabase.Parameters.Add "FONE", txtFone.Text, 1
        oradatabase.Parameters.Add "FAX", txtFax.Text, 1
        oradatabase.Parameters.Add "DT_CADASTR", txtDtCadastro.Text, 1
        oradatabase.Parameters.Add "INSCR_ESTADUAL", txtInscricao.Text, 1
        oradatabase.Parameters.Add "REPART_FISCAL", txtReparticao.Text, 1
    
        oradatabase.Executesql LstrSQL
            
        VErro = IIf((oradatabase.Parameters("COD_ERRORpl").Value = Null Or oradatabase.Parameters("cod_errorpl").Value = ""), 0, oradatabase.Parameters("cod_errorpl").Value)
        If Val(VErro) <> 0 Then
            MsgBox "Erro - " & oradatabase.Parameters("Txt_ErrorPl") & ". Favor avisar Depto. de Sistemas!", vbExclamation
            Exit Sub
        Else
            MsgBox "Inclus�o Efetuada!"
        End If
        
        Screen.MousePointer = 0
        txtCodLoja.Text = ""
        txtCGC.Text = ""
        txtRazao.Text = ""
        txtFantasia.Text = ""
        txtEndereco.Text = ""
        txtBairro.Text = ""
        cmbCodCidade.Text = ""
        txtCEP.Text = ""
        txtDDD.Text = ""
        txtFone.Text = ""
        txtFax.Text = ""
        txtDtCadastro.Text = ""
        txtInscricao.Text = ""
        txtReparticao.Text = ""
        
    Else
        'Update
        Screen.MousePointer = 11
        resp = MsgBox("Confirma Altera��o?", 4)
        If resp = vbYes Then
            LstrSQL = " BEGIN "
            LstrSQL = LstrSQL & " Update loja "
            LstrSQL = LstrSQL & " set   CGC =:CGC, RAZAO_SOCIAL =:RAZAO_SOCIAL, NOME_FANTASIA =:NOME_FANTASIA, "
            LstrSQL = LstrSQL & "       ENDERECO =:ENDERECO, BAIRRO =:BAIRRO, COD_CIDADE =:COD_CIDADE, CEP =:CEP, "
            LstrSQL = LstrSQL & "       DDD =:DDD, FONE =:FONE, FAX =:FAX, DT_CADASTR = to_date(:DT_CADASTR,'dd/mm/rr'), "
            LstrSQL = LstrSQL & "       INSCR_ESTADUAL =:INSCR_ESTADUAL, REPART_FISCAL =:REPART_FISCAL "
            LstrSQL = LstrSQL & " where COD_LOJA =:COD_LOJA;"
            LstrSQL = LstrSQL & " Commit; "
            LstrSQL = LstrSQL & " Exception "
            LstrSQL = LstrSQL & " When Others then "
            LstrSQL = LstrSQL & " RollBack; "
            LstrSQL = LstrSQL & " :Cod_ErrorPl := SQLCODE; "
            LstrSQL = LstrSQL & " :Txt_ErrorPl := SQLERRM; "
            LstrSQL = LstrSQL & " End; "
        
            oradatabase.Parameters.Remove "Cod_ErrorPl"
            oradatabase.Parameters.Remove "Txt_ErrorPl"
            oradatabase.Parameters.Remove "COD_LOJA"
            oradatabase.Parameters.Remove "CGC"
            oradatabase.Parameters.Remove "RAZAO_SOCIAL"
            oradatabase.Parameters.Remove "NOME_FANTASIA"
            oradatabase.Parameters.Remove "ENDERECO"
            oradatabase.Parameters.Remove "BAIRRO"
            oradatabase.Parameters.Remove "COD_CIDADE"
            oradatabase.Parameters.Remove "CEP"
            oradatabase.Parameters.Remove "DDD"
            oradatabase.Parameters.Remove "FONE"
            oradatabase.Parameters.Remove "FAX"
            oradatabase.Parameters.Remove "DT_CADASTR"
            oradatabase.Parameters.Remove "INSCR_ESTADUAL"
            oradatabase.Parameters.Remove "REPART_FISCAL"
            
            oradatabase.Parameters.Add "Cod_ErrorPl", 0, 2
            oradatabase.Parameters.Add "Txt_ErrorPl", "", 2
           'linha de tratamento de erro para a nova OLE
            oradatabase.Parameters("cod_errorpl").serverType = ORATYPE_NUMBER
           'linha de tratamento de erro para a nova OLE
            oradatabase.Parameters("txt_errorpl").serverType = ORATYPE_STRING
                 
            oradatabase.Parameters.Add "COD_LOJA", txtCodLoja.Text, 1
            oradatabase.Parameters.Add "CGC", txtCGC.Text, 1
            oradatabase.Parameters.Add "RAZAO_SOCIAL", txtRazao.Text, 1
            oradatabase.Parameters.Add "NOME_FANTASIA", txtFantasia.Text, 1
            oradatabase.Parameters.Add "ENDERECO", txtEndereco.Text, 1
            oradatabase.Parameters.Add "BAIRRO", txtBairro.Text, 1
            oradatabase.Parameters.Add "COD_CIDADE", Left(cmbCodCidade.Text, 4), 1
            oradatabase.Parameters.Add "CEP", txtCEP.Text, 1
            oradatabase.Parameters.Add "DDD", txtDDD.Text, 1
            oradatabase.Parameters.Add "FONE", txtFone.Text, 1
            oradatabase.Parameters.Add "FAX", txtFax.Text, 1
            oradatabase.Parameters.Add "DT_CADASTR", txtDtCadastro.Text, 1
            oradatabase.Parameters.Add "INSCR_ESTADUAL", txtInscricao.Text, 1
            oradatabase.Parameters.Add "REPART_FISCAL", txtReparticao.Text, 1
        
            oradatabase.Executesql LstrSQL
            
            VErro = IIf((oradatabase.Parameters("COD_ERRORpl").Value = Null Or oradatabase.Parameters("cod_errorpl").Value = ""), 0, oradatabase.Parameters("cod_errorpl").Value)
            If Val(VErro) <> 0 Then
                Screen.MousePointer = 0
                MsgBox "Erro - " & oradatabase.Parameters("Txt_ErrorPl") & ". Favor avisar Depto. de Sistemas!", vbExclamation
                Exit Sub
            Else
                MsgBox "Altera��o efetuada!"
            End If
        Else
            MsgBox "Altera��o Cancelada!"
        End If
        
        Screen.MousePointer = 0
        txtCodLoja.Text = ""
        txtCGC.Text = ""
        txtRazao.Text = ""
        txtFantasia.Text = ""
        txtEndereco.Text = ""
        txtBairro.Text = ""
        cmbCodCidade.Text = ""
        txtCEP.Text = ""
        txtDDD.Text = ""
        txtFone.Text = ""
        txtFax.Text = ""
        txtDtCadastro.Text = ""
        txtInscricao.Text = ""
        txtReparticao.Text = ""

    End If

End Sub

Private Sub mnuSair_Click()
    End
End Sub

Private Sub mnuSobre_Click()
    frmSobre.Show
End Sub

Private Sub sscConsultar_Click()
    Screen.MousePointer = 11
    
    If Trim(txtCodLoja) = "" Then
        MsgBox "Digite o c�digo da Loja."
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
   'Traz os dados de todas as lojas existentes no banco
    LstrSQL = " SELECT a.*, b.nome_cidade FROM loja a, cidade b "
    LstrSQL = LstrSQL & " WHERE a.cod_loja = " & txtCodLoja & " and "
    LstrSQL = LstrSQL & " a.cod_cidade=b.cod_cidade"
    
    Set oradynaset = oradatabase.dbcreatedynaset(LstrSQL, 0&)

    If oradynaset.EOF Then
        MsgBox ("N�o existe loja correspondente a este c�digo. Tente novamente!")
        txtCodLoja.Text = ""
        sscSair.SetFocus
        Screen.MousePointer = 0
        Exit Sub
    End If
        
    If IsNull(oradynaset!CGC) Then
        txtCGC.Text = "0"
    Else
        txtCGC.Text = oradynaset!CGC
    End If
    If IsNull(oradynaset!NOME_FANTASIA) Then
        txtFantasia.Text = " "
    Else
        txtFantasia = oradynaset!NOME_FANTASIA
    End If
    If IsNull(oradynaset!DT_CADASTR) Then
        txtFantasia.Text = "0"
    Else
        txtDtCadastro = oradynaset!DT_CADASTR
    End If
    If IsNull(oradynaset!RAZAO_SOCIAL) Then
        txtRazao.Text = " "
    Else
        txtRazao = oradynaset!RAZAO_SOCIAL
    End If
    If IsNull(oradynaset!ENDERECO) Then
        txtEndereco.Text = " "
    Else
        txtEndereco = oradynaset!ENDERECO
    End If
    If IsNull(oradynaset!BAIRRO) Then
        txtBairro.Text = " "
    Else
        txtBairro = oradynaset!BAIRRO
    End If
    If IsNull(oradynaset!COD_CIDADE) Then
        cmbCodCidade.Text = "Selecionar"
    Else
        cmbCodCidade = oradynaset!COD_CIDADE & " - " & oradynaset!nome_cidade
    End If
    If IsNull(oradynaset!CEP) Then
        txtCEP.Text = "0"
    Else
        txtCEP = oradynaset!CEP
    End If
    If IsNull(oradynaset!DDD) Then
        txtDDD.Text = "0"
    Else
        txtDDD = oradynaset!DDD
    End If
    If IsNull(oradynaset!FONE) Then
        txtFone.Text = "0"
    Else
        txtFone = oradynaset!FONE
    End If
    If IsNull(oradynaset!FAX) Then
        txtFax.Text = "0"
    Else
        txtFax = oradynaset!FAX
    End If
    If IsNull(oradynaset!INSCR_ESTADUAL) Then
        txtInscricao.Text = "0"
    Else
        txtInscricao = oradynaset!INSCR_ESTADUAL
    End If
    If IsNull(oradynaset!REPART_FISCAL) Then
        txtReparticao.Text = "0"
    Else
        txtReparticao = oradynaset!REPART_FISCAL
    End If
    
    Screen.MousePointer = 0
    sscSair.SetFocus

End Sub


Private Sub sscExcluir_Click()

End Sub


Private Sub sscGravar_Click()
    'SDS 1886
    'Eduardo Relvas
    If Val(txtCodLoja) = 0 Then
        MsgBox "Informe o c�digo da loja.", vbInformation, "Aten��o"
        txtCodLoja.SetFocus
        Exit Sub
    End If
    If Trim(txtFantasia) = "" Then
        MsgBox "Informe o nome da loja.", vbInformation, "Aten��o"
        txtFantasia.SetFocus
        Exit Sub
    End If
   
    If Len(Trim(txtDtCadastro)) < 8 Then
        MsgBox "Formato da data est� inv�lido.", vbInformation, "Aten��o"
        txtDtCadastro.SetFocus
        Exit Sub
    End If
    
    If Trim(txtRazao) = "" Then
        MsgBox "Informe a Raz�o Social.", vbInformation, "Aten��o"
        txtRazao.SetFocus
        Exit Sub
    End If
    
    If Trim(txtEndereco) = "" Then
        MsgBox "Informe o Endere�o.", vbInformation, "Aten��o"
        txtEndereco.SetFocus
        Exit Sub
    End If
    
    If Trim(txtBairro) = "" Then
        MsgBox "Informe o Bairro.", vbInformation, "Aten��o"
        txtBairro.SetFocus
        Exit Sub
    End If
    
    If Val(cmbCodCidade) = 0 Then
        MsgBox "Informe a Cidade.", vbInformation, "Aten��o"
        cmbCodCidade.SetFocus
        Exit Sub
    End If
    
    If Val(txtCEP) = 0 Or Len(txtCEP) < 8 Then
        MsgBox "Informe o CEP.", vbInformation, "Aten��o"
        txtCEP.SetFocus
        Exit Sub
    End If
    
    If Not ValidaBranco(txtFantasia) Then
        MsgBox "Existe mais de um espa�o em branco no campo Nome Fantasia.", vbCritical, "CAD060 - Aten��o"
        txtFantasia.SetFocus
        Exit Sub
    End If

    If Not ValidaBranco(Me.txtRazao) Then
        MsgBox "Existe mais de um espa�o em branco no campo Raz�o Social.", vbCritical, "CAD060 - Aten��o"
        txtRazao.SetFocus
        Exit Sub
    End If
    
    If Not ValidaBranco(txtEndereco) Then
        MsgBox "Existe mais de um espa�o em branco no campo Endere�o.", vbCritical, "CAD060 - Aten��o"
        txtEndereco.SetFocus
        Exit Sub
    End If
    
    If Not ValidaBranco(txtBairro) Then
        MsgBox "Existe mais de um espa�o em branco no campo Bairro.", vbCritical, "CAD060 - Aten��o"
        txtBairro.SetFocus
        Exit Sub
    End If
    
    'Fim SDS1886
    
    Screen.MousePointer = 11
   'Traz os dados de todas as lojas existentes no banco
    LstrSQL = " SELECT * FROM loja "
    LstrSQL = LstrSQL & " WHERE cod_loja = '" & txtCodLoja.Text & "' "
    
    Set oradynaset = oradatabase.dbcreatedynaset(LstrSQL, 0&)

    If oradynaset.EOF Then
        'Insert
        LstrSQL = " Begin "
        LstrSQL = LstrSQL & " Insert into loja (COD_LOJA, CGC, RAZAO_SOCIAL, NOME_FANTASIA, "
        LstrSQL = LstrSQL & " ENDERECO, BAIRRO, COD_CIDADE, CEP, DDD, FONE, FAX, DT_CADASTR, "
        LstrSQL = LstrSQL & " INSCR_ESTADUAL, REPART_FISCAL) "
        LstrSQL = LstrSQL & " values (:COD_LOJA, :CGC, :RAZAO_SOCIAL, "
        LstrSQL = LstrSQL & " :NOME_FANTASIA, :ENDERECO, :BAIRRO, :COD_CIDADE, :CEP, "
        LstrSQL = LstrSQL & " :DDD, :FONE, :FAX, to_date(:DT_CADASTR,'dd/mm/rr'), :INSCR_ESTADUAL, :REPART_FISCAL);"
        LstrSQL = LstrSQL & " Commit; "
        LstrSQL = LstrSQL & " Exception "
        LstrSQL = LstrSQL & " When Others then "
        LstrSQL = LstrSQL & " RollBack; "
        LstrSQL = LstrSQL & " :Cod_ErrorPl :=SQLCODE; "
        LstrSQL = LstrSQL & " :Txt_ErrorPl :=SQLERRM; "
        LstrSQL = LstrSQL & " End; "
        
        oradatabase.Parameters.Remove "Cod_ErrorPl"
        oradatabase.Parameters.Remove "Txt_ErrorPl"
        oradatabase.Parameters.Remove "COD_LOJA"
        oradatabase.Parameters.Remove "CGC"
        oradatabase.Parameters.Remove "RAZAO_SOCIAL"
        oradatabase.Parameters.Remove "NOME_FANTASIA"
        oradatabase.Parameters.Remove "ENDERECO"
        oradatabase.Parameters.Remove "BAIRRO"
        oradatabase.Parameters.Remove "COD_CIDADE"
        oradatabase.Parameters.Remove "CEP"
        oradatabase.Parameters.Remove "DDD"
        oradatabase.Parameters.Remove "FONE"
        oradatabase.Parameters.Remove "FAX"
        oradatabase.Parameters.Remove "DT_CADASTR"
        oradatabase.Parameters.Remove "INSCR_ESTADUAL"
        oradatabase.Parameters.Remove "REPART_FISCAL"
        
        oradatabase.Parameters.Add "Cod_ErrorPl", 0, 2
        oradatabase.Parameters.Add "Txt_ErrorPl", "", 2
       'linha de tratamento de erro para a nova OLE
        oradatabase.Parameters("cod_errorpl").serverType = ORATYPE_NUMBER
       'linha de tratamento de erro para a nova OLE
        oradatabase.Parameters("txt_errorpl").serverType = ORATYPE_STRING
        
        oradatabase.Parameters.Add "COD_LOJA", txtCodLoja.Text, 1
        oradatabase.Parameters.Add "CGC", txtCGC.Text, 1
        oradatabase.Parameters.Add "RAZAO_SOCIAL", txtRazao.Text, 1
        oradatabase.Parameters.Add "NOME_FANTASIA", txtFantasia.Text, 1
        oradatabase.Parameters.Add "ENDERECO", txtEndereco.Text, 1
        oradatabase.Parameters.Add "BAIRRO", txtBairro.Text, 1
        oradatabase.Parameters.Add "COD_CIDADE", Left(cmbCodCidade.Text, 4), 1
        oradatabase.Parameters.Add "CEP", txtCEP.Text, 1
        oradatabase.Parameters.Add "DDD", txtDDD.Text, 1
        oradatabase.Parameters.Add "FONE", txtFone.Text, 1
        oradatabase.Parameters.Add "FAX", txtFax.Text, 1
        oradatabase.Parameters.Add "DT_CADASTR", txtDtCadastro.Text, 1
        oradatabase.Parameters.Add "INSCR_ESTADUAL", txtInscricao.Text, 1
        oradatabase.Parameters.Add "REPART_FISCAL", txtReparticao.Text, 1
    
        oradatabase.Executesql LstrSQL
            
        VErro = IIf((oradatabase.Parameters("COD_ERRORPL").Value = Null Or oradatabase.Parameters("cod_errorpl").Value = ""), 0, oradatabase.Parameters("cod_errorpl").Value)
        If Val(VErro) <> 0 Then
            MsgBox "Erro - " & oradatabase.Parameters("Txt_ErrorPl") & ". Favor avisar Depto. de Sistemas!", vbExclamation
            Exit Sub
        Else
            MsgBox "Inclus�o Efetuada!"
        End If
        
        Screen.MousePointer = 0
        txtCodLoja.Text = ""
        txtCGC.Text = ""
        txtRazao.Text = ""
        txtFantasia.Text = ""
        txtEndereco.Text = ""
        txtBairro.Text = ""
        cmbCodCidade.Text = ""
        txtCEP.Text = ""
        txtDDD.Text = ""
        txtFone.Text = ""
        txtFax.Text = ""
        txtDtCadastro.Text = ""
        txtInscricao.Text = ""
        txtReparticao.Text = ""
        
    Else
        'Update
        Screen.MousePointer = 11
        resp = MsgBox("Confirma Altera��o?", 4)
        If resp = vbYes Then
            LstrSQL = " BEGIN "
            LstrSQL = LstrSQL & " Update loja "
            LstrSQL = LstrSQL & " set   CGC =:CGC, RAZAO_SOCIAL =:RAZAO_SOCIAL, NOME_FANTASIA =:NOME_FANTASIA, "
            LstrSQL = LstrSQL & "       ENDERECO =:ENDERECO, BAIRRO =:BAIRRO, COD_CIDADE =:COD_CIDADE, CEP =:CEP, "
            LstrSQL = LstrSQL & "       DDD =:DDD, FONE =:FONE, FAX =:FAX, DT_CADASTR = to_date(:DT_CADASTR,'dd/mm/rr'), "
            LstrSQL = LstrSQL & "       INSCR_ESTADUAL =:INSCR_ESTADUAL, REPART_FISCAL =:REPART_FISCAL "
            LstrSQL = LstrSQL & " where COD_LOJA =:COD_LOJA;"
            LstrSQL = LstrSQL & " Commit; "
            LstrSQL = LstrSQL & " Exception "
            LstrSQL = LstrSQL & " When Others then "
            LstrSQL = LstrSQL & " RollBack; "
            LstrSQL = LstrSQL & " :Cod_ErrorPl := SQLCODE; "
            LstrSQL = LstrSQL & " :Txt_ErrorPl := SQLERRM; "
            LstrSQL = LstrSQL & " End; "
        
            oradatabase.Parameters.Remove "Cod_ErrorPl"
            oradatabase.Parameters.Remove "Txt_ErrorPl"
            oradatabase.Parameters.Remove "COD_LOJA"
            oradatabase.Parameters.Remove "CGC"
            oradatabase.Parameters.Remove "RAZAO_SOCIAL"
            oradatabase.Parameters.Remove "NOME_FANTASIA"
            oradatabase.Parameters.Remove "ENDERECO"
            oradatabase.Parameters.Remove "BAIRRO"
            oradatabase.Parameters.Remove "COD_CIDADE"
            oradatabase.Parameters.Remove "CEP"
            oradatabase.Parameters.Remove "DDD"
            oradatabase.Parameters.Remove "FONE"
            oradatabase.Parameters.Remove "FAX"
            oradatabase.Parameters.Remove "DT_CADASTR"
            oradatabase.Parameters.Remove "INSCR_ESTADUAL"
            oradatabase.Parameters.Remove "REPART_FISCAL"
            
            oradatabase.Parameters.Add "Cod_ErrorPl", 0, 2
            oradatabase.Parameters.Add "Txt_ErrorPl", "", 2
           'linha de tratamento de erro para a nova OLE
            oradatabase.Parameters("cod_errorpl").serverType = ORATYPE_NUMBER
           'linha de tratamento de erro para a nova OLE
            oradatabase.Parameters("txt_errorpl").serverType = ORATYPE_STRING
            
            oradatabase.Parameters.Add "COD_LOJA", txtCodLoja.Text, 1
            oradatabase.Parameters.Add "CGC", txtCGC.Text, 1
            oradatabase.Parameters.Add "RAZAO_SOCIAL", txtRazao.Text, 1
            oradatabase.Parameters.Add "NOME_FANTASIA", txtFantasia.Text, 1
            oradatabase.Parameters.Add "ENDERECO", txtEndereco.Text, 1
            oradatabase.Parameters.Add "BAIRRO", txtBairro.Text, 1
            oradatabase.Parameters.Add "COD_CIDADE", Left(cmbCodCidade.Text, 4), 1
            oradatabase.Parameters.Add "CEP", txtCEP.Text, 1
            oradatabase.Parameters.Add "DDD", txtDDD.Text, 1
            oradatabase.Parameters.Add "FONE", txtFone.Text, 1
            oradatabase.Parameters.Add "FAX", txtFax.Text, 1
            oradatabase.Parameters.Add "DT_CADASTR", txtDtCadastro.Text, 1
            oradatabase.Parameters.Add "INSCR_ESTADUAL", txtInscricao.Text, 1
            oradatabase.Parameters.Add "REPART_FISCAL", txtReparticao.Text, 1
        
            oradatabase.Executesql LstrSQL
            
            VErro = IIf((oradatabase.Parameters("COD_ERRORPL").Value = Null Or oradatabase.Parameters("cod_errorpl").Value = ""), 0, oradatabase.Parameters("cod_errorpl").Value)
            If Val(VErro) <> 0 Then
                Screen.MousePointer = 0
                MsgBox "Erro - " & oradatabase.Parameters("Txt_ErrorPl") & ". Favor avisar Depto. de Sistemas!", vbExclamation
                Exit Sub
            Else
                MsgBox "Altera��o efetuada!"
            End If
        Else
            MsgBox "Altera��o Cancelada!"
        End If
        
        Screen.MousePointer = 0
        txtCodLoja.Text = ""
        txtCGC.Text = ""
        txtRazao.Text = ""
        txtFantasia.Text = ""
        txtEndereco.Text = ""
        txtBairro.Text = ""
        cmbCodCidade.Text = ""
        txtCEP.Text = ""
        txtDDD.Text = ""
        txtFone.Text = ""
        txtFax.Text = ""
        txtDtCadastro.Text = ""
        txtInscricao.Text = ""
        txtReparticao.Text = ""

    End If

End Sub

Private Sub sscSair_Click()
    End
End Sub

Private Sub sscSobre_Click()
    frmSobre.Show
End Sub

Private Sub txtBairro_KeyPress(KeyAscii As Integer)
    KeyAscii = ValidaMaiuscula(KeyAscii)
    KeyAscii = ValidaAlfaNumerico(KeyAscii)
End Sub

Private Sub txtCEP_KeyPress(KeyAscii As Integer)
    KeyAscii = Valor(KeyAscii, txtCEP)
End Sub

Private Sub txtCGC_KeyPress(KeyAscii As Integer)
    KeyAscii = Valor(KeyAscii, txtCGC)
End Sub

Private Sub cmbCodCidade_KeyPress(KeyAscii As Integer)
    KeyAscii = Valor(KeyAscii, cmbCodCidade)
End Sub

Private Sub txtCGC_LostFocus()
    If VALIDA_CGC(txtCGC.Text) = False Then
        MsgBox ("CGC inv�lido! Verifique e digite novamente.")
    End If
End Sub

Private Sub txtCodLoja_GotFocus()
    txtCodLoja.Text = ""
    txtCGC.Text = ""
    txtFantasia.Text = ""
    txtDtCadastro.Text = ""
    txtRazao.Text = ""
    txtEndereco.Text = ""
    txtBairro.Text = ""
    cmbCodCidade.Text = ""
    txtCEP.Text = ""
    txtDDD.Text = ""
    txtFone.Text = ""
    txtFax.Text = ""
    txtInscricao.Text = ""
    txtReparticao.Text = ""
End Sub

Private Sub txtCodLoja_KeyPress(KeyAscii As Integer)
    KeyAscii = Valor(KeyAscii, txtCodLoja)
End Sub

Private Sub txtDDD_KeyPress(KeyAscii As Integer)
    KeyAscii = Valor(KeyAscii, txtDDD)
End Sub

Private Sub txtDtCadastro_KeyPress(KeyAscii As Integer)
  Call Data(KeyAscii, txtDtCadastro)
End Sub

Private Sub txtEndereco_KeyPress(KeyAscii As Integer)
    KeyAscii = ValidaMaiuscula(KeyAscii)
    KeyAscii = ValidaAlfaNumerico(KeyAscii)
End Sub

Private Sub txtFantasia_KeyPress(KeyAscii As Integer)
    KeyAscii = ValidaMaiuscula(KeyAscii)
    KeyAscii = ValidaAlfaNumerico(KeyAscii)
End Sub

Private Sub txtFax_KeyPress(KeyAscii As Integer)
    KeyAscii = Valor(KeyAscii, txtFax)
End Sub

Private Sub txtFone_KeyPress(KeyAscii As Integer)
    KeyAscii = Valor(KeyAscii, txtFone)
End Sub

Private Sub txtInscricao_KeyPress(KeyAscii As Integer)
    KeyAscii = Maiusculo(KeyAscii)
    KeyAscii = ValidaAlfaNumerico(KeyAscii)
End Sub

Private Sub txtInscricao_LostFocus()

   '************************************
   'Faz valida��o da Inscri��o Estadual
    
    VALIDA_IE

End Sub

Private Sub txtRazao_KeyPress(KeyAscii As Integer)
    KeyAscii = ValidaMaiuscula(KeyAscii)
    KeyAscii = ValidaAlfaNumerico(KeyAscii)
End Sub

Private Sub txtReparticao_KeyPress(KeyAscii As Integer)
    KeyAscii = ValidaMaiuscula(KeyAscii)
    KeyAscii = ValidaAlfaNumerico(KeyAscii)
End Sub
