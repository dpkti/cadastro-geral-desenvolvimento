Attribute VB_Name = "Modulo"
Option Explicit

Public vCharEspeciais As String

Global oradatabase As Object
Global orasession As Object
Global oradynaset As Object
Public ResultUF As String

'VARI�VEIS DE ERRO
Public VErro As Variant
Global Const ORATYPE_NUMBER = 2
Global Const ORATYPE_STRING = 5

Function Maiusculo(KeyAscii As Integer) As Integer
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    Maiusculo = KeyAscii
End Function

Sub Data(ByRef KeyAscii, ByRef txtCampo)
    On Error GoTo TrataErro

    Dim bTam As Byte    'tamanho do campo JA digitado
    Dim strData As String
    Dim bKey As Byte
    bTam = Len(txtCampo.Text)
    
    If KeyAscii = 8 And bTam > 0 Then 'backspace
        If Mid$(txtCampo.Text, bTam) = "/" Then
            txtCampo.Text = Mid$(txtCampo.Text, 1, bTam - 2)
        Else
            txtCampo.Text = Mid$(txtCampo.Text, 1, bTam - 1)
        End If
            
    ElseIf Chr$(KeyAscii) >= "0" And Chr$(KeyAscii) <= "9" Then
        If bTam = 1 Then
            strData = txtCampo.Text & Chr$(KeyAscii)
            If CInt(strData) < 1 Or CInt(strData > 31) Then
                Beep
            Else
                txtCampo.Text = txtCampo.Text & Chr$(KeyAscii) & "/"
            End If
            
        ElseIf bTam = 4 Then
            strData = Mid$(txtCampo.Text, 4) & Chr$(KeyAscii)
            If CInt(strData) < 1 Or CInt(strData > 12) Then
                Beep
            Else
                txtCampo.Text = txtCampo.Text & Chr$(KeyAscii) & "/"
            End If
        
        ElseIf bTam = 7 Then
            strData = Mid$(txtCampo.Text, 1, 2)     'dia
            If CInt(strData) < 1 Or CInt(strData > 31) Then
                Beep
            Else
                strData = Mid$(txtCampo.Text, 4, 2)     'mes
                If CInt(strData) < 1 Or CInt(strData > 12) Then
                    Beep
                Else
                    strData = txtCampo.Text & Chr$(KeyAscii)
                    strData = Left(strData, 6) & IIf(Val(Right(strData, 2)) > 50, "19", "20") & Right(strData, 2)
                    If Not IsDate(CDate(strData)) Then
                        Beep
                    Else
                        txtCampo.Text = Format(strData, "dd/mm/yy")
                    End If
                End If
            End If
            
        ElseIf bTam < 8 Then
            bKey = KeyAscii
            
        Else
            bKey = 0
        End If
    Else
        Beep
    End If
    
    SendKeys "{END}"
    KeyAscii = bKey
    Exit Sub
    
TrataErro:

    If Err.Number = 13 Then
        MsgBox "Data Inv�lida " & strData, vbCritical, "Aten��o"
        KeyAscii = 0
        Beep
        Err.Clear
    End If

End Sub




Function Valor(ByVal KeyAscii As Integer, strCampo As String) As Integer
    If KeyAscii = 8 Then    'BACKSPACE
        Valor = KeyAscii
        Exit Function
    End If
    If Chr$(KeyAscii) = "," Then
        KeyAscii = Asc(".")
    End If
    If Chr$(KeyAscii) = "," Or Chr$(KeyAscii) = "." Then
        If InStr(strCampo, ",") > 0 Or InStr(strCampo, ".") > 0 Then
            KeyAscii = 0
            Beep
        End If
    Else
        If Chr$(KeyAscii) < "0" Or Chr$(KeyAscii) > "9" Then
            KeyAscii = 0
            Beep
        End If
    End If
    
    Valor = KeyAscii
End Function


Function VALIDA_CGC(CGC As String) As Boolean

Dim contador As Integer
Dim Fator As Integer
Dim Soma As Double
Dim resto As Double
Dim digito As Integer
Dim verif As Integer

Fator = 2
Soma = 0
CGC = Format(CGC, "00000000000000")

For contador = 12 To 1 Step -1
    Soma = Soma + (Val(Mid(CGC, contador, 1)) * Fator)
    Fator = Fator + 1
    If Fator = 10 Then
       Fator = 2
    End If
Next

resto = Int(Soma / 11) * 11
resto = Soma - resto

If resto = 0 Or resto = 1 Then
   digito = 0
Else
   digito = 11 - resto
End If

verif = digito * 10

Soma = 0
Fator = 2
For contador = 13 To 1 Step -1
    Soma = Soma + (Val(Mid(CGC, contador, 1)) * Fator)
    Fator = Fator + 1
    If Fator = 10 Then
       Fator = 2
    End If
Next

resto = Int(Soma / 11) * 11
resto = Soma - resto

If resto = 0 Or resto = 1 Then
   digito = 0
Else
   digito = 11 - resto
End If

verif = verif + digito
verif = Format(verif, "00")

If verif = Mid(CGC, 13, 2) Then
   VALIDA_CGC = True
Else
   VALIDA_CGC = False
End If


End Function

Public Function fCharEspeciais()
    
    oradatabase.Parameters.Remove "PM_CD"
    oradatabase.Parameters.Remove "PM_CARACTERES"

    oradatabase.Parameters.Add "PM_CD", 1, 1
    oradatabase.Parameters.Add "PM_CARACTERES", "", 2
    
    oradatabase.Executesql "BEGIN PRODUCAO.PR_SELECT_CARACT_ESP_NFE(:PM_CD, :PM_CARACTERES); END;"

    fCharEspeciais = oradatabase.Parameters("PM_CARACTERES").Value

End Function

Function ValidaLetra(ByVal KeyAscii As Integer) As Integer

    On Error GoTo Trata_Erro
    
    If KeyAscii = 8 Or KeyAscii = 32 Or KeyAscii = 13 Then

        ValidaLetra = KeyAscii
        Exit Function

    End If

    If KeyAscii < 65 Or KeyAscii > 90 Then

        KeyAscii = 0
        
    End If

    ValidaLetra = KeyAscii
    
    Exit Function

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Function ValidaLetra" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description, "Aten��o", &H40000
    End If

End Function

Function ValidaNumero(ByVal KeyAscii As Integer) As Integer

On Error GoTo Trata_Erro

    If KeyAscii = 8 Or KeyAscii = 13 Then

        ValidaNumero = KeyAscii
        Exit Function

    End If

    If Chr(KeyAscii) < "0" Or Chr(KeyAscii) > "9" Then

        KeyAscii = 0

    End If

    ValidaNumero = KeyAscii

Exit Function

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Function ValidaNumero" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description, "Aten��o", &H40000
    End If
End Function

Function ValidaAlfaNumerico(ByVal KeyAscii As Integer) As Integer

On Error GoTo Trata_Erro

    If KeyAscii = 8 Or KeyAscii = 32 Or KeyAscii = 13 Then

        ValidaAlfaNumerico = KeyAscii
        Exit Function

    End If
    
    If InStr(1, vCharEspeciais, Chr(KeyAscii)) > 0 Then
        
        KeyAscii = 0

    End If

    ValidaAlfaNumerico = KeyAscii

Exit Function

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Function ValidaAlfaNumerico" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description, "Aten��o", &H40000
    End If
End Function

Public Function ValidaMaiuscula(pKeyascii As Integer) As Integer

    ValidaMaiuscula = Asc(UCase(Chr(pKeyascii)))

    Exit Function

End Function

Function ValidaBranco(pTXT As TextBox) As Boolean
        
    Dim vbranco As Byte
    Dim I  As Long
    
    'Atribui true na fun��o
    ValidaBranco = True
    
    'Valida se existe mais de um espa�o em branco nos campos
    For I = 1 To Len(pTXT)
        If Asc(Mid(pTXT, I, 1)) = 32 Then
            vbranco = vbranco + 1
        Else
            vbranco = 0
        End If
        If vbranco >= 2 Then
            ValidaBranco = False
            Exit Function
        End If
    Next
 
End Function

Function ValidaTamanho(pTXT As TextBox, pTamanho As Integer) As Boolean
    
    'Atribui true na fun��o
    ValidaTamanho = True
    
    'Valida o tamnho do campo
    If Len(pTXT) < pTamanho Then
        ValidaTamanho = False
    End If
 
End Function



