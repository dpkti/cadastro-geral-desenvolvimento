VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmEstatistica 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Estat�stica de Aplica��o"
   ClientHeight    =   8970
   ClientLeft      =   945
   ClientTop       =   330
   ClientWidth     =   10695
   Icon            =   "frmEstatistica.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   598
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   713
   Begin VB.Frame Frame2 
      Caption         =   "Nr. Itens com Aplica��o"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   3375
      Left            =   2760
      TabIndex        =   5
      Top             =   4800
      Width           =   6135
      Begin MSGrid.Grid Grid2 
         Height          =   2055
         Left            =   240
         TabIndex        =   6
         Top             =   360
         Width           =   5535
         _Version        =   65536
         _ExtentX        =   9763
         _ExtentY        =   3625
         _StockProps     =   77
         ForeColor       =   8388608
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label7 
         Caption         =   "%"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   4920
         TabIndex        =   18
         Top             =   2880
         Width           =   135
      End
      Begin VB.Label lblPc_Aplic 
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   4440
         TabIndex        =   17
         Top             =   2880
         Width           =   375
      End
      Begin VB.Label lblTotal_Aplicacao 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3360
         TabIndex        =   14
         Top             =   2880
         Width           =   855
      End
      Begin VB.Label lblTotal_Itens 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3360
         TabIndex        =   13
         Top             =   2520
         Width           =   855
      End
      Begin VB.Label Label4 
         Caption         =   "Total Itens com Aplica��o"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   840
         TabIndex        =   12
         Top             =   2880
         Width           =   2055
      End
      Begin VB.Label Label3 
         Caption         =   "Total Itens"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   840
         TabIndex        =   11
         Top             =   2520
         Width           =   1335
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Nr. Ve�culos Cadastrados"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   3255
      Left            =   2760
      TabIndex        =   3
      Top             =   1320
      Width           =   6135
      Begin MSGrid.Grid Grid1 
         Height          =   2055
         Left            =   960
         TabIndex        =   4
         Top             =   360
         Width           =   4095
         _Version        =   65536
         _ExtentX        =   7223
         _ExtentY        =   3625
         _StockProps     =   77
         ForeColor       =   8388608
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label5 
         Caption         =   "%"
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   4800
         TabIndex        =   16
         Top             =   2880
         Width           =   135
      End
      Begin VB.Label lblPC_Veiculo 
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   4320
         TabIndex        =   15
         Top             =   2880
         Width           =   375
      End
      Begin VB.Label lblTotal_Veiculos_Data 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3240
         TabIndex        =   10
         Top             =   2880
         Width           =   855
      End
      Begin VB.Label lblTotal_Veiculos 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3240
         TabIndex        =   9
         Top             =   2520
         Width           =   855
      End
      Begin VB.Label Label2 
         Caption         =   "Total Ve�culos com data"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   960
         TabIndex        =   8
         Top             =   2880
         Width           =   1815
      End
      Begin VB.Label Label1 
         Caption         =   "Total Ve�culos"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   960
         TabIndex        =   7
         Top             =   2520
         Width           =   1335
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   8640
      Width           =   10695
      _ExtentX        =   18865
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   18362
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      Top             =   810
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmEstatistica.frx":23D2
      PICN            =   "frmEstatistica.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmEstatistica"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    Dim i As Long
    Dim lngTotal_Veiculos As Long
    Dim lngTotal_Veiculos_Datas As Long
    Dim lngTotal_Itens As Long
    Dim lngTotal_Aplicacao As Long
    
    
    Me.Top = 0
    Me.Left = 0
    
    lngTotal_Veiculos = 0
    lngTotal_Veiculos_Datas = 0
    lngTotal_Itens = 0
    lngTotal_Aplicacao = 0
    
    BuscaEstat_Veiculo
      
    vVB_Generica_001.CarregaGridTabela frmEstatistica.Grid1, vObjOracle, 4
    
    For i = 1 To vObjOracle.RecordCount
      lngTotal_Veiculos = lngTotal_Veiculos + vObjOracle(1)
      lngTotal_Veiculos_Datas = lngTotal_Veiculos_Datas + vObjOracle(2)
      vObjOracle.MoveNext
    Next
    lblTotal_Veiculos = lngTotal_Veiculos
    lblTotal_Veiculos_Data = lngTotal_Veiculos_Datas
    If lngTotal_Veiculos = 0 Or lngTotal_Veiculos_Datas = 0 Then
      lblPC_Veiculo = 0
    Else
      lblPC_Veiculo = Format((lngTotal_Veiculos_Datas / lngTotal_Veiculos) * 100, "0.00")
    End If
    
    
     
    BuscaEstat_Aplicacao
      
     vVB_Generica_001.CarregaGridTabela frmEstatistica.Grid2, vObjOracle, 5
     
     For i = 1 To vObjOracle.RecordCount
      lngTotal_Itens = lngTotal_Itens + vObjOracle(2)
      lngTotal_Aplicacao = lngTotal_Aplicacao + vObjOracle(3)
      vObjOracle.MoveNext
    Next
    
    lblTotal_Itens = lngTotal_Itens
    lblTotal_Aplicacao = lngTotal_Aplicacao
    If lngTotal_Itens = 0 Or lngTotal_Aplicacao = 0 Then
      lblPc_Aplic = 0
    Else
      lblPc_Aplic = Format((lngTotal_Aplicacao / lngTotal_Itens) * 100, "0.00")
    End If
    
     
    
End Sub

