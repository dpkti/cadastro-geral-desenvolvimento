VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.OCX"
Begin VB.MDIForm mdiCAD985 
   Appearance      =   0  'Flat
   BackColor       =   &H8000000F&
   Caption         =   "CAD985 - Cadastro de Aplica��o"
   ClientHeight    =   8175
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11400
   Icon            =   "mdiCAD985.frx":0000
   LinkTopic       =   "MDIForm1"
   LockControls    =   -1  'True
   MouseIcon       =   "mdiCAD985.frx":23D2
   WindowState     =   2  'Maximized
   Begin MSComctlLib.ProgressBar PrgBarCatACC 
      Align           =   1  'Align Top
      Height          =   405
      Left            =   0
      TabIndex        =   17
      Top             =   1830
      Visible         =   0   'False
      Width           =   11400
      _ExtentX        =   20108
      _ExtentY        =   714
      _Version        =   393216
      Appearance      =   0
      Min             =   1
      Max             =   16
      Scrolling       =   1
   End
   Begin Threed.SSPanel sspPainelAcc 
      Align           =   1  'Align Top
      Height          =   600
      Left            =   0
      TabIndex        =   16
      Top             =   1230
      Visible         =   0   'False
      Width           =   11400
      _Version        =   65536
      _ExtentX        =   20108
      _ExtentY        =   1058
      _StockProps     =   15
      BackColor       =   14215660
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin VB.Label lblCatACC 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2810
         TabIndex        =   19
         Top             =   120
         Width           =   8535
      End
      Begin VB.Label Label1 
         Caption         =   " Gerando catalogo loja ACC:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   0
         TabIndex        =   18
         Top             =   120
         Width           =   2700
      End
   End
   Begin Threed.SSPanel SSPanel1 
      Align           =   1  'Align Top
      Height          =   1230
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   11400
      _Version        =   65536
      _ExtentX        =   20108
      _ExtentY        =   2170
      _StockProps     =   15
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BevelWidth      =   0
      BorderWidth     =   0
      BevelOuter      =   0
      Begin Bot�o.cmd cmdCatLjAcc 
         Height          =   975
         Left            =   12480
         TabIndex        =   14
         TabStop         =   0   'False
         ToolTipText     =   "Gerar Catalogo para lojas ACC"
         Top             =   0
         Width           =   855
         _ExtentX        =   1508
         _ExtentY        =   1720
         BTYPE           =   3
         TX              =   "Catalogo Loja ACC"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   14215660
         BCOLO           =   14215660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiCAD985.frx":26DC
         PICN            =   "mdiCAD985.frx":26F8
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdSobre 
         Height          =   975
         Left            =   11520
         TabIndex        =   13
         TabStop         =   0   'False
         ToolTipText     =   "Sobre"
         Top             =   0
         Width           =   855
         _ExtentX        =   1508
         _ExtentY        =   1720
         BTYPE           =   3
         TX              =   "Sobre"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiCAD985.frx":2A12
         PICN            =   "mdiCAD985.frx":2A2E
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdSair 
         Height          =   975
         Left            =   13440
         TabIndex        =   15
         TabStop         =   0   'False
         ToolTipText     =   "Sair do sistema"
         Top             =   0
         Width           =   855
         _ExtentX        =   1508
         _ExtentY        =   1720
         BTYPE           =   3
         TX              =   "Sair"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiCAD985.frx":3708
         PICN            =   "mdiCAD985.frx":3724
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin pkGradientControl.pkGradient pkGradient1 
         Height          =   30
         Left            =   45
         TabIndex        =   2
         Top             =   1125
         Width           =   11760
         _ExtentX        =   20743
         _ExtentY        =   53
         Color1          =   0
         Color2          =   -2147483633
         BackColor       =   -2147483633
      End
      Begin Bot�o.cmd cmd1 
         Height          =   975
         Left            =   1440
         TabIndex        =   3
         TabStop         =   0   'False
         ToolTipText     =   "Montadora"
         Top             =   0
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   1720
         BTYPE           =   3
         TX              =   "Montadora"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiCAD985.frx":43FE
         PICN            =   "mdiCAD985.frx":441A
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmd2 
         Height          =   975
         Left            =   2640
         TabIndex        =   4
         TabStop         =   0   'False
         ToolTipText     =   "Veiculo"
         Top             =   0
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   1720
         BTYPE           =   3
         TX              =   "Veiculo"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiCAD985.frx":4734
         PICN            =   "mdiCAD985.frx":4750
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmd3 
         Height          =   975
         Left            =   3720
         TabIndex        =   5
         TabStop         =   0   'False
         ToolTipText     =   "Modelo"
         Top             =   0
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   1720
         BTYPE           =   3
         TX              =   "Modelo"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiCAD985.frx":4A6A
         PICN            =   "mdiCAD985.frx":4A86
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmd4 
         Height          =   975
         Left            =   4800
         TabIndex        =   6
         TabStop         =   0   'False
         ToolTipText     =   "Motor"
         Top             =   0
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   1720
         BTYPE           =   3
         TX              =   "Motor"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiCAD985.frx":5760
         PICN            =   "mdiCAD985.frx":577C
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmd5 
         Height          =   975
         Left            =   5880
         TabIndex        =   7
         TabStop         =   0   'False
         ToolTipText     =   "Motor"
         Top             =   0
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   1720
         BTYPE           =   3
         TX              =   "Aplica��o"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiCAD985.frx":6056
         PICN            =   "mdiCAD985.frx":6072
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmd6 
         Height          =   975
         Left            =   6960
         TabIndex        =   8
         TabStop         =   0   'False
         ToolTipText     =   "Grupo Restri��o"
         Top             =   0
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   1720
         BTYPE           =   3
         TX              =   "Grupo Complemento"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiCAD985.frx":694C
         PICN            =   "mdiCAD985.frx":6968
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmd7 
         Height          =   975
         Left            =   8160
         TabIndex        =   9
         TabStop         =   0   'False
         ToolTipText     =   "SubGrupo"
         Top             =   0
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   1720
         BTYPE           =   3
         TX              =   "SubGrupo Complemento"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiCAD985.frx":753A
         PICN            =   "mdiCAD985.frx":7556
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmd8 
         Height          =   975
         Left            =   9360
         TabIndex        =   10
         TabStop         =   0   'False
         ToolTipText     =   "Relacionamento DPK X APLICA��O"
         Top             =   0
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   1720
         BTYPE           =   3
         TX              =   "DPK x Aplica��o"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiCAD985.frx":8128
         PICN            =   "mdiCAD985.frx":8144
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdEstatistica 
         Height          =   975
         Left            =   10560
         TabIndex        =   11
         TabStop         =   0   'False
         ToolTipText     =   "Sobre"
         Top             =   0
         Width           =   855
         _ExtentX        =   1508
         _ExtentY        =   1720
         BTYPE           =   3
         TX              =   "Estat�stica"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiCAD985.frx":8A1E
         PICN            =   "mdiCAD985.frx":8A3A
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Image Image1 
         Height          =   1185
         Left            =   -90
         Picture         =   "mdiCAD985.frx":8D54
         Stretch         =   -1  'True
         ToolTipText     =   "Acessar a Intranet"
         Top             =   -45
         Width           =   1455
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   7845
      Width           =   11400
      _ExtentX        =   20108
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   6932
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   2646
            MinWidth        =   2646
            Object.ToolTipText     =   "Usu�rio da rede"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   2381
            MinWidth        =   2381
            Object.ToolTipText     =   "Usu�rio do banco de dados"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   3528
            MinWidth        =   3528
            Object.ToolTipText     =   "Banco de dados conectado"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            Alignment       =   1
            Object.Width           =   2381
            MinWidth        =   2381
            TextSave        =   "26/01/15"
            Object.ToolTipText     =   "Data"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   1
            Object.Width           =   1587
            MinWidth        =   1587
            TextSave        =   "17:32"
            Object.ToolTipText     =   "Hora"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox Picture1 
      Align           =   1  'Align Top
      Height          =   0
      Left            =   0
      ScaleHeight     =   0
      ScaleWidth      =   11400
      TabIndex        =   12
      Top             =   0
      Width           =   11400
   End
End
Attribute VB_Name = "mdiCAD985"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmd1_Click()
    frmMontadora.Show
End Sub

Private Sub cmd2_Click()
    frmVeiculo.Show
End Sub

Private Sub cmd3_Click()
    frmModelo.Show
End Sub

Private Sub cmd4_Click()
    frmMotor.Show
End Sub

Private Sub cmd5_Click()
    frmVeicAplicacao.Show
End Sub

Private Sub cmd6_Click()
    frmGrupo.Show
End Sub

Private Sub cmd7_Click()
    frmSubGrupo.Show
End Sub

Private Sub cmd8_Click()
  
  frmDpkAplicacao.Show 1
End Sub

Private Sub cmdCatLjAcc_Click()


    Call InicioGeracaoCat
    

End Sub

Private Sub cmdEstatistica_Click()
  frmEstatistica.Show 1
End Sub

Private Sub cmdSair_Click()

    If vVB_Generica_001.Sair = 6 Then
    
        If Not vBanco Is Nothing Then
        
            Call vVB_Generica_001.ExcluiBind(vBanco)
        
        End If
        
        Set vObjOracle = Nothing
        Set vBanco = Nothing
        Set vSessao = Nothing
        
        End
    
    End If

End Sub
Private Sub cmdSobre_Click()

    frmSobre.Show 1
  

End Sub
Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)

    Cancel = 1
    
End Sub
Private Sub MDIForm_Load()

    If App.PrevInstance = True Then
    
        Call vVB_Generica_001.Informar("J� existe uma inst�ncia desse sistema no ar")
        End
    
    End If
    
    'PEGA O C�DIGO DO USU�RIO PASSADO PELO MENU AUTOM�TICO E
    'VERIFICA SE O SISTEMA EST� SENDO EXECUTADO PELO MENU AUTOM�TICO
'    If Command = "" Then
'
'        Call vVB_Generica_001.Informar("S� � poss�vel executar este sistema atrav�s do Menu Autom�tico. " & Chr(13) & _
'            "Favor entrar em contato com o Helpdesk")
'
'        End
'
'    Else
'
'        vCodUsuario = Command
'
'    End If

    'INCLUIR OS ARQUIVOS: (PROJECT -> ADD FILE OU CTRL+D)
    'H:\Usr\Padroes\VB6\Forms\Aguardar.frm
    'H:\Usr\Padroes\VB6\Forms\Logo.frm
    'H:\Usr\Padroes\VB6\M�dulos\M�dulo padr�o.bas
    frmLogo.Show 1
    
    Me.Top = 0
    Me.Left = 0

    '<BANCO> = BANCO DE DADOS A SER CONECTADO
    '<USU�RIO> = USU�RIO DE CONEX�O COM O ORACLE
    vErro = vVB_Generica_001.ConectaOracle("PRODUCAO", "CAD985", True, Me)
    'vErro = vVB_Generica_001.ConectaOracle("SDPK-DEV.CAL.COM.BR", "CAD985", True, Me)

    If vErro <> "" Then

        End

    End If

    vCD = vVB_Generica_001.vCD
    vTipoCD = vVB_Generica_001.vTipoCD
    Set vSessao = vVB_Generica_001.vSessao
    Set vBanco = vVB_Generica_001.vBanco
    
    'PARA SISTEMAS QUE FOREM CONECTAR NOS CD'S, DESCOMENTAR A LINHA
    'ABAIXO E INCLUIR O FORM ABAIXO (PROJECT -> ADD FORM)
    'Conex�o CD's
    'dlgConexao.Show 1

    Set vObjOracle = vVB_Generica_001.InformacoesSistema(vBanco, UCase(App.Title))

    If vObjOracle.EOF Then

        Call vVB_Generica_001.Informar("Sistema n�o cadastrado. � necess�rio fazer o cadastro para executar o programa.")
        End

    End If

    Call DefinirTelaSobre
    
    frmSenha.Show 1
    
End Sub
