VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.OCX"
Begin VB.Form frmDpkAplicacao 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Relacionamento DPK x Aplica��o"
   ClientHeight    =   10575
   ClientLeft      =   135
   ClientTop       =   330
   ClientWidth     =   13830
   Icon            =   "frmDpkAplicacao.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   705
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   922
   Begin VB.Frame Frame2 
      Appearance      =   0  'Flat
      Caption         =   "Aplica��o"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   8295
      Left            =   0
      TabIndex        =   11
      Top             =   1800
      Width           =   13635
      Begin VB.CommandButton cmd_EXCLUI_Item 
         Caption         =   "EXCLUIR  Aplic. ve�culos selec."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   12000
         TabIndex        =   39
         Top             =   2160
         Visible         =   0   'False
         Width           =   1455
      End
      Begin VB.CommandButton cmd_INCLUI_Item 
         Caption         =   "INCLUIR  Item ve�culos selec."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   12000
         TabIndex        =   38
         Top             =   1200
         Visible         =   0   'False
         Width           =   1455
      End
      Begin VB.ComboBox cmbMotor 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   345
         Left            =   9360
         TabIndex        =   34
         Top             =   480
         Width           =   2895
      End
      Begin VB.ComboBox cmbVeiculo 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   345
         Left            =   5400
         Style           =   2  'Dropdown List
         TabIndex        =   26
         Top             =   480
         Width           =   2895
      End
      Begin Threed.SSCommand SSCommand1 
         Height          =   495
         Left            =   12600
         TabIndex        =   23
         ToolTipText     =   "Refaz lista de Modelos de ve�culos"
         Top             =   360
         Width           =   495
         _Version        =   65536
         _ExtentX        =   873
         _ExtentY        =   873
         _StockProps     =   78
         Picture         =   "frmDpkAplicacao.frx":23D2
      End
      Begin VB.ComboBox cmbMontadora 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   345
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   12
         Top             =   480
         Width           =   2895
      End
      Begin MSGrid.Grid grdVeiculo 
         Height          =   1560
         Left            =   360
         TabIndex        =   14
         ToolTipText     =   "Duplo Clique para incluir aplica��o"
         Top             =   1200
         Visible         =   0   'False
         Width           =   11535
         _Version        =   65536
         _ExtentX        =   20346
         _ExtentY        =   2752
         _StockProps     =   77
         ForeColor       =   8388608
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         HighLight       =   0   'False
      End
      Begin MSGrid.Grid grdAplicacao 
         Height          =   2640
         Left            =   360
         TabIndex        =   19
         ToolTipText     =   "Duplo clique para incluir Nr. Original e Restri��o Espec�fica / DEL para excluir aplica��o"
         Top             =   3000
         Visible         =   0   'False
         Width           =   12735
         _Version        =   65536
         _ExtentX        =   22463
         _ExtentY        =   4657
         _StockProps     =   77
         ForeColor       =   8388608
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         HighLight       =   0   'False
      End
      Begin MSGrid.Grid grdRestricao_Generica 
         Height          =   2400
         Left            =   360
         TabIndex        =   20
         ToolTipText     =   "Duplo clique para incluir, alterar ou exclui restri��o gen�rica"
         Top             =   5880
         Visible         =   0   'False
         Width           =   12735
         _Version        =   65536
         _ExtentX        =   22463
         _ExtentY        =   4233
         _StockProps     =   77
         ForeColor       =   8388608
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         HighLight       =   0   'False
      End
      Begin VB.Label Label9 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Motor:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   210
         Left            =   8640
         TabIndex        =   36
         Top             =   480
         Width           =   540
      End
      Begin VB.Label lblPesquisa 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   1560
         TabIndex        =   27
         Top             =   840
         Visible         =   0   'False
         Width           =   4815
      End
      Begin VB.Label Label4 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Ve�culo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   4560
         TabIndex        =   25
         Top             =   480
         Width           =   735
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Complemento Gen�rico "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   210
         Left            =   480
         TabIndex        =   21
         Top             =   5640
         Visible         =   0   'False
         Width           =   1980
      End
      Begin VB.Label Label8 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Aplica��o/Complemento Espec�fico "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   210
         Left            =   360
         TabIndex        =   18
         Top             =   2760
         Visible         =   0   'False
         Width           =   2880
      End
      Begin VB.Label Label7 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Modelos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   210
         Left            =   360
         TabIndex        =   17
         Top             =   840
         Visible         =   0   'False
         Width           =   660
      End
      Begin VB.Label Label11 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Montadora:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   360
         TabIndex        =   13
         Top             =   480
         Width           =   975
      End
   End
   Begin VB.Frame fra 
      Appearance      =   0  'Flat
      Caption         =   "Item"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1140
      Left            =   0
      TabIndex        =   3
      Top             =   720
      Width           =   13635
      Begin VB.TextBox txtDPK_Copia 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   330
         Left            =   1080
         MaxLength       =   9
         TabIndex        =   32
         Top             =   720
         Width           =   1000
      End
      Begin Threed.SSCommand sscmdEquivalentes 
         Height          =   255
         Left            =   2340
         TabIndex        =   29
         ToolTipText     =   "Itens Equivalentes"
         Top             =   240
         Width           =   255
         _Version        =   65536
         _ExtentX        =   450
         _ExtentY        =   450
         _StockProps     =   78
         Caption         =   "="
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.TextBox txtCodFabrica 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   330
         Left            =   6600
         TabIndex        =   9
         Top             =   240
         Width           =   1950
      End
      Begin VB.ComboBox cmbFornecedor 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   330
         Left            =   3120
         Sorted          =   -1  'True
         TabIndex        =   5
         Top             =   270
         Width           =   2325
      End
      Begin VB.TextBox txtDPK 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   330
         Left            =   1080
         MaxLength       =   9
         TabIndex        =   4
         Top             =   240
         Width           =   1000
      End
      Begin Threed.SSCommand SSCommand2 
         Height          =   375
         Left            =   13080
         TabIndex        =   24
         ToolTipText     =   "Copiar Aplica��o/Restri��o para DPKs Equivalentes"
         Top             =   240
         Width           =   375
         _Version        =   65536
         _ExtentX        =   661
         _ExtentY        =   661
         _StockProps     =   78
         Picture         =   "frmDpkAplicacao.frx":2B4C
      End
      Begin Threed.SSCommand SSCommand3 
         Height          =   375
         Left            =   13080
         TabIndex        =   30
         ToolTipText     =   "Copiar Aplica��o/Restri��o para outro DPK"
         Top             =   720
         Width           =   375
         _Version        =   65536
         _ExtentX        =   661
         _ExtentY        =   661
         _StockProps     =   78
         Picture         =   "frmDpkAplicacao.frx":2EE6
      End
      Begin VB.Label lblItem_Copia 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   210
         Left            =   2280
         TabIndex        =   33
         Top             =   840
         Width           =   60
      End
      Begin VB.Label Label5 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "DPK Copia:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   210
         Left            =   120
         TabIndex        =   31
         Top             =   720
         Width           =   885
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "C�d. F�brica:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   210
         Left            =   5520
         TabIndex        =   10
         Top             =   360
         Width           =   1050
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Forn.:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   210
         Left            =   2640
         TabIndex        =   8
         Top             =   360
         Width           =   480
      End
      Begin VB.Label lblDPK 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   8520
         TabIndex        =   7
         Top             =   360
         Width           =   4335
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "DPK:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   210
         Left            =   120
         TabIndex        =   6
         Top             =   360
         Width           =   390
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   10245
      Width           =   13830
      _ExtentX        =   24395
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   24342
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      Top             =   810
      Width           =   9915
      _ExtentX        =   17489
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   615
      Left            =   120
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1085
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmDpkAplicacao.frx":3280
      PICN            =   "frmDpkAplicacao.frx":329C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdCancel 
      Height          =   615
      Left            =   960
      TabIndex        =   15
      TabStop         =   0   'False
      ToolTipText     =   "Limpar a Tela"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1085
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmDpkAplicacao.frx":3F76
      PICN            =   "frmDpkAplicacao.frx":3F92
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdImp_Arq_Aplic 
      Height          =   615
      Left            =   1800
      TabIndex        =   16
      TabStop         =   0   'False
      ToolTipText     =   "Importar Arq. Aplica��o/Restri��o Espec�fica"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1085
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmDpkAplicacao.frx":4C6C
      PICN            =   "frmDpkAplicacao.frx":4C88
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdImport_Arq_Restr 
      Height          =   615
      Left            =   2640
      TabIndex        =   22
      TabStop         =   0   'False
      ToolTipText     =   "Importar Arq. Restri��o"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1085
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmDpkAplicacao.frx":5962
      PICN            =   "frmDpkAplicacao.frx":597E
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdFotos 
      Height          =   615
      Left            =   3480
      TabIndex        =   28
      TabStop         =   0   'False
      ToolTipText     =   "Cadastro de Fotos"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1085
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmDpkAplicacao.frx":5F18
      PICN            =   "frmDpkAplicacao.frx":5F34
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label10 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "Ve�culo:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   0
      TabIndex        =   37
      Top             =   0
      Width           =   735
   End
   Begin VB.Label Label6 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "Ve�culo:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   0
      TabIndex        =   35
      Top             =   0
      Width           =   735
   End
End
Attribute VB_Name = "frmDpkAplicacao"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public strPesquisa As String

Public Sub Preenche_Grid_modelos()

   On Error GoTo Trata_erro
    vBanco.Parameters.Remove "montadora"
    vBanco.Parameters.Add "montadora", cmbMontadora.ItemData(cmbMontadora.ListIndex), 1
    vBanco.Parameters.Remove "veiculo"
    If cmbVeiculo.Text = "" Then
       vBanco.Parameters.Add "veiculo", 0, 1
    Else
       vBanco.Parameters.Add "veiculo", cmbVeiculo.ItemData(cmbVeiculo.ListIndex), 1
    End If
    
    'eoliveira - CiT - 08/08/2012
    'Implementa��o para considerar motor - ISA-173
    vBanco.Parameters.Remove "motor"
    If cmbMotor.Text = "" Then
       vBanco.Parameters.Add "motor", 0, 1
    Else
       vBanco.Parameters.Add "motor", cmbMotor.ItemData(cmbMotor.ListIndex), 1
    End If
    
    vBanco.Parameters.Remove "dpk"
    vBanco.Parameters.Add "dpk", txtDPK, 1
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2

    'eoliveira - CiT - 08/08/2012
    'Implementa��o para considerar motor - ISA-173
    vSql = "PRODUCAO.PCK_CAD985.PR_CONSULTA_VEICULO(:MONTADORA,:VEICULO,:MOTOR,:DPK,:PM_CURSOR1, :PM_CODERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
  
    
    If vObjOracle.EOF And vObjOracle.BOF Then
    '  MsgBox "N�o h� ve�culos cadastrados para a montadora escolhida", vbInformation, "Aten��o"
      grdVeiculo.Visible = False
      vVB_Generica_001.CarregaGridTabela grdVeiculo, vObjOracle, 6
      
      'Emerson - 03/03/15 - TI-1843 - Acertar alguns campos do CAD985...
      Me.cmd_INCLUI_Item.Visible = False
      '--------------------

    Else
        vVB_Generica_001.CarregaGridTabela grdVeiculo, vObjOracle, 6
         grdVeiculo.Visible = True
         
         'Emerson - 03/03/15 - TI-1843 - Acertar alguns campos do CAD985...
         Me.cmd_INCLUI_Item.Visible = True
         '--------------------

    End If
    
    'Emerson - 03/03/15 - TI-1843 - Acertar alguns campos do CAD985...
    If Me.grdAplicacao.Rows > 0 And Trim(Me.cmbMontadora.Text) <> "" Then
        Me.cmd_EXCLUI_Item.Visible = True
    End If
    '--------------------

    Exit Sub
Trata_erro:
  If Err.Number <> 0 Then
    Exit Sub
  End If
    

End Sub
Public Sub Busca_Item()
  On Error GoTo Trata_erro
  If txtCodFabrica Like "*%*" Then
    strFl_Equiv = "N"
    Screen.MousePointer = 11
    Label7.Visible = True
    Label8.Visible = True
    Label1.Visible = True
    vBanco.Parameters.Remove "loja"
    vBanco.Parameters.Add "loja", 1, 1
    vBanco.Parameters.Remove "dpk"
    vBanco.Parameters.Add "dpk", 0, 1
    vBanco.Parameters.Remove "forn"
    vBanco.Parameters.Add "forn", Val(Mid(Trim(cmbFornecedor), 1, 3)), 1
    vBanco.Parameters.Remove "fabrica"
    vBanco.Parameters.Add "fabrica", txtCodFabrica, 1
    vBanco.Parameters.Remove "desc"
    vBanco.Parameters.Add "desc", "0", 1
    
    
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    
    
    vSql = "PRODUCAO.PCK_VDA320.pr_aplica1(:PM_CURSOR1,1,:forn,:fabrica,:desc, :PM_CODERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
  
    
    If vObjOracle.EOF And vObjOracle.BOF Then
      
      
      MsgBox "Item n�o encontrado", vbInformation, "Aten��o"
      Exit Sub
    End If
    If vObjOracle.RecordCount = 1 Then
      txtCodFabrica = vObjOracle!cod_fabrica
      GoTo inicio
    Else
       frmItens.grd.Visible = True
       vVB_Generica_001.CarregaGridTabela frmItens.grd, vObjOracle, 4
       frmItens.Caption = ""
       
       Screen.MousePointer = 0
       frmItens.Show 1
    
      
       If txtCodFabrica Like "*%*" Then
         Exit Sub
       Else
         GoTo inicio
       End If
    End If
  
  Else
inicio:
    Label7.Visible = True
    Label8.Visible = True
    Label1.Visible = True
    vBanco.Parameters.Remove "loja"
    vBanco.Parameters.Add "loja", 1, 1
    vBanco.Parameters.Remove "dpk"
    vBanco.Parameters.Add "dpk", txtDPK, 1
    vBanco.Parameters.Remove "forn"
    vBanco.Parameters.Add "forn", Val(Mid(Trim(cmbFornecedor), 1, 3)), 1
    vBanco.Parameters.Remove "fabrica"
    vBanco.Parameters.Add "fabrica", txtCodFabrica, 1
    
    
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    
    
    vSql = "PRODUCAO.PCK_VDA320.PR_ITEM(:PM_CURSOR1,:loja,:dpk,:forn,:fabrica, :PM_CODERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
  
    
    If vObjOracle.EOF Then
      MsgBox "DPK n�o cadastrado, verifique", vbInformation, "Aten��o"
      Limpa_Item
    Else
      If vObjOracle!Situacao = 9 Then
        MsgBox "DPK desativado.", vbInformation, "Aten��o"
        Limpa_Item
      ElseIf vObjOracle!Situacao = 8 Then
        MsgBox "DPK subtituido", vbInformation, "Aten��o"
        txtDPK = vObjOracle!cod_dpk_ant
        GoTo inicio
      Else
        cmbFornecedor = Format(vObjOracle("COD_FORNECEDOR"), "000") & " - " & vObjOracle("SIGLA")
        txtCodFabrica = vObjOracle("COD_Fabrica")
        lblDPK = vObjOracle("desc_item")
        txtDPK = vObjOracle("cod_dpk")
        
       
        vBanco.Parameters.Remove "dpk"
        vBanco.Parameters.Add "dpk", txtDPK, 1
        vBanco.Parameters.Remove "PM_CURSOR1"
        vBanco.Parameters.Add "PM_CURSOR1", 0, 3
        vBanco.Parameters("PM_CURSOR1").ServerType = 102
        vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
        vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
        vBanco.Parameters.Remove "PM_CODERRO"
        vBanco.Parameters.Add "PM_CODERRO", 0, 2
        vBanco.Parameters.Remove "PM_txtERRO"
        vBanco.Parameters.Add "PM_txtERRO", 0, 2

    
        
        vSql = "PRODUCAO.PCK_CAD985.PR_CONSULTA_DPK_VEICULO(:DPK,:PM_CURSOR1, :PM_CODERRO)"
        
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        
        Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
      
        
        If vObjOracle.EOF And vObjOracle.BOF Then
        '  MsgBox "N�o h� ve�culos cadastrados para a montadora escolhida", vbInformation, "Aten��o"
          grdAplicacao.Visible = False
          vVB_Generica_001.CarregaGridTabela grdAplicacao, vObjOracle, 9
          
          'Emerson - 03/03/15 - TI-1843 - Acertar alguns campos do CAD985...
          Me.cmd_EXCLUI_Item.Visible = False
          
        Else
            vVB_Generica_001.CarregaGridTabela grdAplicacao, vObjOracle, 9
             grdAplicacao.Visible = True
        End If
        
        vSql = "PRODUCAO.PCK_CAD985.PR_DPK_RESTRICAO_resumo(:DPK,0,:PM_CURSOR1, :PM_CODERRO, :PM_TXTERRO)"
        
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        
        Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
        DoEvents
        If vObjOracle.EOF And vObjOracle.BOF Then
           ' vVB_Generica_001.CarregaGridTabela grdRestricao_Generica, vObjOracle, 4
            grdRestricao_Generica.SelStartCol = grdRestricao_Generica.FixedCols
            grdRestricao_Generica.SelStartRow = grdRestricao_Generica.FixedRows
            grdRestricao_Generica.SelEndCol = grdRestricao_Generica.Cols - 1
            grdRestricao_Generica.SelEndRow = grdRestricao_Generica.Rows - 1
            grdRestricao_Generica.Clip = ""
            grdRestricao_Generica.Col = grdRestricao_Generica.FixedCols
            grdRestricao_Generica.Row = grdRestricao_Generica.FixedRows
            grdRestricao_Generica.SelEndCol = grdRestricao_Generica.SelStartCol
            grdRestricao_Generica.SelEndRow = grdRestricao_Generica.SelStartRow
             grdRestricao_Generica.Visible = True
         Else
          vVB_Generica_001.CarregaGridTabela grdRestricao_Generica, vObjOracle, 4
          grdRestricao_Generica.Visible = True
        End If
        
      End If
    End If
  End If
  Screen.MousePointer = 0
Trata_erro:
  If Err = 401 Or Err = 20 Then
    Exit Sub
  End If
  
End Sub
Public Sub Busca_Item_Copia()
  On Error GoTo Trata_erro
  
  If txtDPK_Copia <> "" Then
  
inicio:
    vBanco.Parameters.Remove "loja"
    vBanco.Parameters.Add "loja", 1, 1
    vBanco.Parameters.Remove "dpk"
    vBanco.Parameters.Add "dpk", txtDPK_Copia, 1
    vBanco.Parameters.Remove "forn"
    vBanco.Parameters.Add "forn", 0, 1
    vBanco.Parameters.Remove "fabrica"
    vBanco.Parameters.Add "fabrica", "", 1
    
    
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    
    
    vSql = "PRODUCAO.PCK_VDA320.PR_ITEM(:PM_CURSOR1,:loja,:dpk,:forn,:fabrica, :PM_CODERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
  
    
    If vObjOracle.EOF Then
      MsgBox "DPK n�o cadastrado, verifique", vbInformation, "Aten��o"
      Limpa_Item_Copia
    Else
      If vObjOracle!Situacao = 9 Then
        MsgBox "DPK desativado.", vbInformation, "Aten��o"
        Limpa_Item_Copia
      ElseIf vObjOracle!Situacao = 8 Then
        MsgBox "DPK subtituido", vbInformation, "Aten��o"
        txtDPK_Copia = vObjOracle!cod_dpk_ant
        GoTo inicio
      Else
        lblItem_Copia.Caption = Format(vObjOracle("COD_FORNECEDOR"), "000") & " - " & vObjOracle("SIGLA") & _
        vObjOracle("COD_Fabrica") & " - " & vObjOracle("desc_item")
      End If
   End If
  
  
  Screen.MousePointer = 0
End If
  
Trata_erro:
  If Err = 401 Or Err = 20 Then
    Exit Sub
  End If
  
End Sub
Public Sub Insere_Aplic()
    vBanco.Parameters.Remove "PM_ACT"
    vBanco.Parameters.Add "PM_ACT", "I", 1
    
    vBanco.Parameters.Remove "p_cod_dpk"
    vBanco.Parameters.Add "p_cod_dpk", txtDPK, 1
    
    grdVeiculo.Col = 5
    vBanco.Parameters.Remove "p_cod_aplicacao"
    vBanco.Parameters.Add "p_cod_aplicacao", Val(grdVeiculo.Text), 1
    
    vBanco.Parameters.Remove "p_cod_original"
    vBanco.Parameters.Add "p_cod_original", "0", 1
     
    vBanco.Parameters.Remove "PM_SITUACAO"
    vBanco.Parameters.Add "PM_SITUACAO", 0, 1
  
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "PRODUCAO.PCK_CAD985.PR_ACT_DPK_APLIC(:PM_ACT,:p_cod_dpk,:p_cod_aplicacao,:p_cod_original,0,:PM_SITUACAO,:PM_CODERRO,:PM_TXTERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
    
    If vBanco.Parameters("PM_CODERRO").Value = 0 Then
'      Call cmbMontadora_LostFocus
      Call txtDPK_LostFocus
    End If

End Sub
Public Sub Altera_Aplic(txtOriginal As String, txtOriginal_Novo As String)
    vBanco.Parameters.Remove "PM_ACT"
    vBanco.Parameters.Add "PM_ACT", "A", 1
    
    vBanco.Parameters.Remove "p_cod_dpk"
    vBanco.Parameters.Add "p_cod_dpk", txtDPK, 1
    
    grdAplicacao.Col = 5
    vBanco.Parameters.Remove "p_cod_aplicacao"
    vBanco.Parameters.Add "p_cod_aplicacao", lngAplicacao, 1
    
    vBanco.Parameters.Remove "p_cod_original"
    vBanco.Parameters.Add "p_cod_original", CStr(txtOriginal), 1
    
      vBanco.Parameters.Remove "original_novo"
    vBanco.Parameters.Add "original_novo", CStr(txtOriginal_Novo), 1
     
    vBanco.Parameters.Remove "PM_SITUACAO"
    vBanco.Parameters.Add "PM_SITUACAO", 0, 1
  
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "PRODUCAO.PCK_CAD985.PR_ACT_DPK_APLIC(:PM_ACT,:p_cod_dpk,:p_cod_aplicacao,:p_cod_original,:original_novo,:PM_SITUACAO,:PM_CODERRO,:PM_TXTERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
    
    If vBanco.Parameters("PM_CODERRO").Value = 0 Then
      Call Preenche_Grid_modelos
      Call txtDPK_LostFocus
    End If

End Sub
Public Sub Exclui_Aplic()

    If txtDPK <> "" Then
      vBanco.Parameters.Remove "PM_ACT"
      vBanco.Parameters.Add "PM_ACT", "D", 1
      
      vBanco.Parameters.Remove "p_cod_dpk"
      vBanco.Parameters.Add "p_cod_dpk", txtDPK, 1
      
      grdAplicacao.Col = 5
      vBanco.Parameters.Remove "p_cod_aplicacao"
      vBanco.Parameters.Add "p_cod_aplicacao", Val(grdAplicacao.Text), 1
      
      grdAplicacao.Col = 4
      vBanco.Parameters.Remove "p_cod_original"
      vBanco.Parameters.Add "p_cod_original", grdAplicacao.Text, 1
       
      vBanco.Parameters.Remove "PM_SITUACAO"
      vBanco.Parameters.Add "PM_SITUACAO", 0, 1
    
      vBanco.Parameters.Remove "PM_CODERRO"
      vBanco.Parameters.Add "PM_CODERRO", 0, 2
      vBanco.Parameters.Remove "PM_TXTERRO"
      vBanco.Parameters.Add "PM_TXTERRO", "", 2
      
      vSql = "PRODUCAO.PCK_CAD985.PR_ACT_DPK_APLIC(:PM_ACT,:p_cod_dpk,:p_cod_aplicacao,:p_cod_original,0,:PM_SITUACAO,:PM_CODERRO,:PM_TXTERRO)"
      
      vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
      
      Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
      
      If vBanco.Parameters("PM_CODERRO").Value = 0 Then
      '  Call cmbMontadora_LostFocus
        Call txtDPK_LostFocus
      End If
    End If

End Sub

Public Sub Limpa_Item()
  strFl_Equiv = "N"
  txtDPK = ""
  txtCodFabrica = ""
  lblDPK = ""
  cmbFornecedor = ""
  grdVeiculo.Visible = False
  grdAplicacao.Visible = False
  
  'Emerson - 03/03/15 - TI-1843 - Acertar alguns campos do CAD985...
  Me.cmd_INCLUI_Item.Visible = False
  Me.cmd_EXCLUI_Item.Visible = False
  '--------------------
  
  grdRestricao_Generica.Visible = False
  strRestricao = ""
  Label7.Visible = False
  Label8.Visible = False
  Label1.Visible = False
  cmbVeiculo.Clear
  frmItens.grd.Visible = False
  frmItens.Caption = ""
'  cmbVeiculo.Text = ""

End Sub
Public Sub Limpa_Item_Copia()
  
  txtDPK_Copia = ""
  lblItem_Copia.Caption = ""
  
End Sub


Private Sub cmbFornecedor_Click()
  If txtDPK <> "" Then
    Limpa_Item
  End If
End Sub

Private Sub cmbFornecedor_DropDown()
  If cmbFornecedor.ListCount = 0 Then
  
     
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2

    
    vSql = "PRODUCAO.PCK_VDA320.PR_FORNECEDOR(:PM_CURSOR1, :PM_CODERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
  
    cmbFornecedor.AddItem ""
    While Not vObjOracle.EOF
        
        cmbFornecedor.AddItem Format(vObjOracle("COD_FORNECEDOR"), "00") & " - " & vObjOracle("SIGLA")
        vObjOracle.MoveNext
    Wend
  Else
    'LIMPA INFORMA��ES DOS ITENS
    If txtDPK <> "" Then
      Limpa_Item
    End If
  End If
End Sub


Private Sub cmbMontadora_Click()
  grdVeiculo.Visible = False
  
  'Emerson - 03/03/15 - TI-1843 - Acertar alguns campos do CAD985...
  Me.cmd_INCLUI_Item.Visible = False
  '--------------------

  lblPesquisa.Caption = ""
  strPesquisa = ""
  Call cmbMontadora_LostFocus
End Sub

Private Sub cmbMontadora_LostFocus()

   If cmbMontadora <> "" And txtDPK <> "" Then
    
    'eoliveira - CiT - 08/08/2012
    'Implementa��o para considerar motor - ISA-173
    'Popular lista de motor a partir de sele��o de montadora.
    cmbMotor.Clear
    vBanco.Parameters.Remove "PM_MONTADORA"
    vBanco.Parameters.Add "PM_MONTADORA", cmbMontadora.ItemData(cmbMontadora.ListIndex), 1
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", 0, 2
    vSql = "PRODUCAO.PCK_CAD985.PR_BUSCA_MOTOR(:PM_MONTADORA, :PM_CURSOR1,:PM_CODERRO, :PM_TXTERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
    cmbMotor.AddItem " "
    While Not vObjOracle.EOF
    cmbMotor.AddItem vObjOracle("DESC_MOTOR")
    cmbMotor.ItemData(cmbMotor.NewIndex) = vObjOracle("COD_MOTOR")
    vObjOracle.MoveNext
    Wend
    
    
    
    'ALIMENTA COMBO DE VE�ULOS
    cmbVeiculo.Clear
    vBanco.Parameters.Remove "montadora"
    vBanco.Parameters.Add "montadora", cmbMontadora.ItemData(cmbMontadora.ListIndex), 1
   
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", 0, 2


    
    vSql = "PRODUCAO.PCK_CAD985.PR_BUSCA_VEICULO(:MONTADORA,:PM_CURSOR1, :PM_CODERRO,:PM_TXTERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
  
    
    cmbVeiculo.AddItem " "
    While Not vObjOracle.EOF
        cmbVeiculo.AddItem vObjOracle("DESC_VEICULO")
        cmbVeiculo.ItemData(cmbVeiculo.NewIndex) = vObjOracle("COD_VEICULO")
        vObjOracle.MoveNext
    Wend
   
  End If
End Sub


Private Sub cmd1_Click()

End Sub

'Emerson - 03/03/15 - TI-1843 - Acertar alguns campos do CAD985...
Private Sub cmd_INCLUI_Item_Click()

    If Trim(cmbVeiculo.Text) = "" Then
        MsgBox "Favor informar o Ve�culo.", vbExclamation
        Exit Sub
    End If
    
    'verifica se existe no grid de Ve�culos...
    Dim Z As Integer
    Dim vQtdeVeic As Integer
    
    vQtdeVeic = 0
    
    For Z = 1 To grdVeiculo.Rows - 1
    
        grdVeiculo.Row = Z
        
        grdVeiculo.Col = 1
        
        If Trim(Me.cmbVeiculo.Text) <> "" Then
        
          'compara descri��o do ve�culo com o combo de ve�culo...
          If InStr(grdVeiculo.Text, Me.cmbVeiculo.Text) > 0 Then
          
              If Trim(Me.cmbMotor.Text) <> "" Then
              
                'compara descri��o do motor com o combo de motor...
                If InStr(grdVeiculo.Text, Me.cmbMotor.Text) > 0 Then
                    vQtdeVeic = vQtdeVeic + 1
                Else
                    vQtdeVeic = 0
                End If
              
              Else
                  vQtdeVeic = vQtdeVeic + 1
              End If
          End If
        End If
    Next

    Dim vstrMsg As String

    If vQtdeVeic = 0 Or vQtdeVeic <> (grdVeiculo.Rows - 1) Then
    
        vstrMsg = "Efetue a Pesquisa antes de incluir o Item."
        
        MsgBox vstrMsg, vbExclamation, "ATEN��O"
        Exit Sub
    End If

    If Trim(Me.cmbMotor.Text) = "" Then
        vstrMsg = "Confirma INCLUS�O do Item  " & Me.lblDPK.Caption & Chr(13) & Chr(13) & _
                  "para a Montadora  " & Me.cmbMontadora.Text & Chr(13) & Chr(13) & _
                  "e Ve�culo  " & Me.cmbVeiculo.Text & "  ?"
    Else
        vstrMsg = "Confirma INCLUS�O do Item  " & Me.lblDPK.Caption & Chr(13) & Chr(13) & _
                  "para a Montadora  " & Me.cmbMontadora.Text & Chr(13) & Chr(13) & _
                  "e Ve�culo  " & Me.cmbVeiculo.Text & Chr(13) & Chr(13) & _
                  "e Motor  " & Me.cmbMotor.Text & "  ?"
    End If

    If MsgBox(vstrMsg, vbQuestion + vbYesNo) = vbNo Then
        Exit Sub
    End If

    'mouse
    Screen.MousePointer = vbHourglass
    
    grdVeiculo.Col = 5
    
    For Z = 1 To grdVeiculo.Rows - 1
        
        vBanco.Parameters.Remove "PM_ACT"
        vBanco.Parameters.Add "PM_ACT", "I", 1
        
        vBanco.Parameters.Remove "p_cod_dpk"
        vBanco.Parameters.Add "p_cod_dpk", txtDPK, 1
        
        grdVeiculo.Row = Z
        vBanco.Parameters.Remove "p_cod_aplicacao"
        vBanco.Parameters.Add "p_cod_aplicacao", Val(grdVeiculo.Text), 1
        
        vBanco.Parameters.Remove "p_cod_original"
        vBanco.Parameters.Add "p_cod_original", "0", 1
         
        vBanco.Parameters.Remove "PM_SITUACAO"
        vBanco.Parameters.Add "PM_SITUACAO", 0, 1
      
        vBanco.Parameters.Remove "PM_CODERRO"
        vBanco.Parameters.Add "PM_CODERRO", 0, 2
        vBanco.Parameters.Remove "PM_TXTERRO"
        vBanco.Parameters.Add "PM_TXTERRO", "", 2
        
        vSql = "PRODUCAO.PCK_CAD985.PR_ACT_DPK_APLIC(:PM_ACT,:p_cod_dpk,:p_cod_aplicacao,:p_cod_original,0,:PM_SITUACAO,:PM_CODERRO,:PM_TXTERRO)"
        
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        
        Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
        
        If vBanco.Parameters("PM_CODERRO").Value <> 0 Then
            MsgBox "Erro ao Incluir o Item  " & Me.lblDPK.Caption & Chr(13) & _
                   "  para o Ve�culo " & Me.cmbVeiculo.Text & ". Avise o analista !"
            'mouse
            Screen.MousePointer = vbDefault
            Exit Sub
        End If

    Next

    If Z > 1 Then

        If Trim(Me.cmbMotor.Text) = "" Then
        
            vstrMsg = "O Item  " & Me.lblDPK.Caption & Chr(13) & Chr(13) & _
                    "foi INCLU�DO para o Ve�culo  " & Me.cmbVeiculo.Text & Chr(13) & Chr(13) & _
                    "da Montadora  " & Me.cmbMontadora.Text & "."
        Else
            vstrMsg = "O Item  " & Me.lblDPK.Caption & Chr(13) & Chr(13) & _
                    "foi INCLU�DO para o Ve�culo  " & Me.cmbVeiculo.Text & Chr(13) & Chr(13) & _
                    "com motor  " & Me.cmbMotor.Text & Chr(13) & Chr(13) & _
                    "da Montadora  " & Me.cmbMontadora.Text & "."
        End If

        'atualiza grid de aplica��o...
        Busca_Item

        Me.cmbVeiculo.ListIndex = 0
        Me.cmbMotor.ListIndex = 0

        MsgBox vstrMsg, vbInformation

        Me.grdVeiculo.Visible = False
        Me.cmd_INCLUI_Item.Visible = False
    End If
    
    'mouse
    Screen.MousePointer = vbDefault
    
End Sub

Private Sub cmd_EXCLUI_Item_Click()

    If Trim(Me.cmbMontadora.Text) = "" Then
        MsgBox "Favor informar a Montadora.", vbExclamation
        Exit Sub
    End If

    'verifica se existe no grid de Aplica��o...
    Dim Z As Integer
    Dim vQtdeAplic As Integer

    vQtdeAplic = 0
    
    For Z = 1 To grdAplicacao.Rows - 1
            
        grdAplicacao.Row = Z
        
        grdAplicacao.Col = 1
            
        If Achou_Aplicacao_Grid(Me.grdAplicacao.Text) Then
            vQtdeAplic = vQtdeAplic + 1
        End If
    Next
    
    Dim vstrMsg As String

    If vQtdeAplic = 0 Then
    
        If Trim(Me.cmbMotor.Text) <> "" Then
        
            If Trim(Me.cmbVeiculo.Text) = "" Then
            
                vstrMsg = "N�o foi encontrada Aplica��o do Item  " & Me.lblDPK.Caption & Chr(13) & Chr(13) & _
                        "para os Ve�culos da Montadora  " & Me.cmbMontadora.Text & Chr(13) & Chr(13) & _
                        "com motor  " & Me.cmbMotor.Text & "."
            Else
                vstrMsg = "N�o foi encontrada Aplica��o do Item  " & Me.lblDPK.Caption & Chr(13) & Chr(13) & _
                        "para o Ve�culo  " & Me.cmbVeiculo.Text & Chr(13) & Chr(13) & _
                        "com motor  " & Me.cmbMotor.Text & Chr(13) & Chr(13) & _
                        "da Montadora  " & Me.cmbMontadora.Text & "."
            End If
            
        Else
            If Trim(Me.cmbVeiculo.Text) = "" Then
                    vstrMsg = "N�o foi encontrada Aplica��o do Item  " & Me.lblDPK.Caption & Chr(13) & Chr(13) & _
                            "para nenhum Ve�culo  " & Chr(13) & Chr(13) & _
                            "da Montadora  " & Me.cmbMontadora.Text & "."
            Else
                vstrMsg = "N�o foi encontrada Aplica��o do Item  " & Me.lblDPK.Caption & Chr(13) & Chr(13) & _
                        "para o Ve�culo  " & Me.cmbVeiculo.Text & Chr(13) & Chr(13) & _
                        "da Montadora  " & Me.cmbMontadora.Text & "."
            End If
        End If
    
        MsgBox vstrMsg, vbExclamation, "ATEN��O"
        Exit Sub
    End If
    
    If Trim(Me.cmbMotor.Text) <> "" Then
    
        If Trim(Me.cmbVeiculo.Text) = "" Then
        
            vstrMsg = "Confirma EXCLUS�O de   " & vQtdeAplic & " registro(s)" & Chr(13) & Chr(13) & _
                    "da Aplica��o do Item  " & Me.lblDPK.Caption & Chr(13) & Chr(13) & _
                    "para os Ve�culos da Montadora  " & Me.cmbMontadora.Text & Chr(13) & Chr(13) & _
                    "com motor  " & Me.cmbMotor.Text & "  ?"
        Else
            vstrMsg = "Confirma EXCLUS�O de   " & vQtdeAplic & " registro(s)" & Chr(13) & Chr(13) & _
                    "da Aplica��o do Item  " & Me.lblDPK.Caption & Chr(13) & Chr(13) & _
                    "para o Ve�culo  " & Me.cmbVeiculo.Text & Chr(13) & Chr(13) & _
                    "com motor  " & Me.cmbMotor.Text & Chr(13) & Chr(13) & _
                    "da Montadora  " & Me.cmbMontadora.Text & "  ?"
        End If
        
    Else
        If Trim(Me.cmbVeiculo.Text) = "" Then
                vstrMsg = "Confirma EXCLUS�O  de   " & vQtdeAplic & " registro(s)" & Chr(13) & Chr(13) & _
                        "da Aplica��o do Item  " & Me.lblDPK.Caption & Chr(13) & Chr(13) & _
                        "para TODOS os Ve�culos  " & Chr(13) & Chr(13) & _
                        "da Montadora  " & Me.cmbMontadora.Text & "  ?"
        Else
            vstrMsg = "Confirma EXCLUS�O de   " & vQtdeAplic & " registro(s)" & Chr(13) & Chr(13) & _
                    "da Aplica��o do Item  " & Me.lblDPK.Caption & Chr(13) & Chr(13) & _
                    "para o Ve�culo  " & Me.cmbVeiculo.Text & Chr(13) & Chr(13) & _
                    "da Montadora  " & Me.cmbMontadora.Text & "  ?"
        End If
    End If
    
    If MsgBox(vstrMsg, vbQuestion + vbYesNo) = vbNo Then
        Exit Sub
    End If

    'mouse
    Screen.MousePointer = vbHourglass
    
    Dim vQtdeExc As Integer
    
    vQtdeExc = 0
    
    For Z = 1 To grdAplicacao.Rows - 1
        
        grdAplicacao.Row = Z
        
        grdAplicacao.Col = 1
        
        If Achou_Aplicacao_Grid(Me.grdAplicacao.Text) Then
        
            vBanco.Parameters.Remove "PM_ACT"
            vBanco.Parameters.Add "PM_ACT", "D", 1
            
            vBanco.Parameters.Remove "p_cod_dpk"
            vBanco.Parameters.Add "p_cod_dpk", txtDPK, 1
      
            grdAplicacao.Col = 5
            vBanco.Parameters.Remove "p_cod_aplicacao"
            vBanco.Parameters.Add "p_cod_aplicacao", Val(grdAplicacao.Text), 1
            
            grdAplicacao.Col = 4
            vBanco.Parameters.Remove "p_cod_original"
            vBanco.Parameters.Add "p_cod_original", grdAplicacao.Text, 1
            
            vBanco.Parameters.Remove "PM_SITUACAO"
            vBanco.Parameters.Add "PM_SITUACAO", 0, 1
            
            vBanco.Parameters.Remove "PM_CODERRO"
            vBanco.Parameters.Add "PM_CODERRO", 0, 2
            vBanco.Parameters.Remove "PM_TXTERRO"
            vBanco.Parameters.Add "PM_TXTERRO", "", 2
            
            vSql = "PRODUCAO.PCK_CAD985.PR_ACT_DPK_APLIC(:PM_ACT,:p_cod_dpk,:p_cod_aplicacao,:p_cod_original,0,:PM_SITUACAO,:PM_CODERRO,:PM_TXTERRO)"
            
            vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
            
            Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
            
            If vBanco.Parameters("PM_CODERRO").Value = 0 Then
                vQtdeExc = vQtdeExc + 1
            Else
            
                If Trim(Me.cmbVeiculo.Text) = "" Then
                        vstrMsg = "Erro ao EXCLUIR a aplica��o do Item  " & Me.lblDPK.Caption & Chr(13) & Chr(13) & _
                                "para TODOS os Ve�culos  " & Chr(13) & Chr(13) & _
                                "da Montadora  " & Me.cmbMontadora.Text & ". Avise o analista !"
                Else
                    If Trim(Me.cmbMotor.Text) = "" Then
                    
                        vstrMsg = "Erro ao EXCLUIR a aplica��o do Item  " & Me.lblDPK.Caption & Chr(13) & Chr(13) & _
                                "para o Ve�culo  " & Me.cmbVeiculo.Text & Chr(13) & Chr(13) & _
                                "da Montadora  " & Me.cmbMontadora.Text & ". Avise o analista !"
                    Else
                        vstrMsg = "Erro ao EXCLUIR a aplica��o do Item  " & Me.lblDPK.Caption & Chr(13) & Chr(13) & _
                                "para o Ve�culo  " & Me.cmbVeiculo.Text & Chr(13) & Chr(13) & _
                                "com motor  " & Me.cmbMotor.Text & Chr(13) & Chr(13) & _
                                "da Montadora  " & Me.cmbMontadora.Text & ". Avise o analista !"
                    End If
                End If
                
                MsgBox vstrMsg, vbInformation
                
                'atualiza grid de aplica��o...
                Busca_Item
                
                'mouse
                Screen.MousePointer = vbDefault
                
                Exit Sub
            
            End If
        End If
    Next

    If vQtdeExc > 0 Then

        If Trim(Me.cmbMotor.Text) <> "" Then
        
            If Trim(Me.cmbVeiculo.Text) = "" Then
            
                vstrMsg = "EXCLU�DO(s)   " & vQtdeExc & " registro(s)" & Chr(13) & Chr(13) & _
                        "da Aplica��o do Item  " & Me.lblDPK.Caption & Chr(13) & Chr(13) & _
                        "para os Ve�culos da Montadora  " & Me.cmbMontadora.Text & Chr(13) & Chr(13) & _
                        "com motor  " & Me.cmbMotor.Text & "."
            Else
                vstrMsg = "EXCLU�DO(s)   " & vQtdeExc & " registro(s)" & Chr(13) & Chr(13) & _
                        "da Aplica��o do Item  " & Me.lblDPK.Caption & Chr(13) & Chr(13) & _
                        "para o Ve�culo  " & Me.cmbVeiculo.Text & Chr(13) & Chr(13) & _
                        "com motor  " & Me.cmbMotor.Text & Chr(13) & Chr(13) & _
                        "da Montadora  " & Me.cmbMontadora.Text & "."
            End If
            
        Else
            If Trim(Me.cmbVeiculo.Text) = "" Then
                    vstrMsg = "EXCLU�DO(s)   " & vQtdeExc & " registro(s)" & Chr(13) & Chr(13) & _
                            "da Aplica��o do Item  " & Me.lblDPK.Caption & Chr(13) & Chr(13) & _
                            "para TODOS os Ve�culos  " & Chr(13) & Chr(13) & _
                            "da Montadora  " & Me.cmbMontadora.Text & "."
            Else
                vstrMsg = "EXCLU�DO(s)   " & vQtdeExc & " registro(s)" & Chr(13) & Chr(13) & _
                        "da Aplica��o do Item  " & Me.lblDPK.Caption & Chr(13) & Chr(13) & _
                        "para o Ve�culo  " & Me.cmbVeiculo.Text & Chr(13) & Chr(13) & _
                        "da Montadora  " & Me.cmbMontadora.Text & "."
            End If
        End If

        'atualiza grid de aplica��o...
        Busca_Item

        Me.cmbVeiculo.ListIndex = 0
        Me.cmbMotor.ListIndex = 0

        MsgBox vstrMsg, vbInformation

        'Me.grdVeiculo.Visible = False
        'Me.cmd_INCLUI_Item.Visible = False
    End If
    
    'mouse
    Screen.MousePointer = vbDefault
    
End Sub

Function Achou_Aplicacao_Grid(texto_Grid) As Boolean

Dim vstrCompara As String

    Achou_Aplicacao_Grid = False

    vstrCompara = ""
    
    'compara descri��o da Montadora com o combo de Montadora...
    If Mid(texto_Grid, 1, Len(Me.cmbMontadora.Text)) = Me.cmbMontadora.Text Then
    
        If Trim(Me.cmbVeiculo.Text) <> "" Then
            'inclui Ve�culo na string de pesquisa no grid...
            vstrCompara = Me.cmbMontadora.Text & " " & Me.cmbVeiculo.Text
        End If
        
        If Trim(Me.cmbMotor.Text) <> "" Then
            'inclui Motor na string de pesquisa no grid...
            vstrCompara = vstrCompara & " " & Me.cmbMotor.Text
        End If
        
        If InStr(texto_Grid, vstrCompara) > 0 Then
            'achou no grid...
            Achou_Aplicacao_Grid = True
        Else
            'procura por partes da string...
            If Trim(Me.cmbVeiculo.Text) <> "" Then
            
                If InStr(texto_Grid, Me.cmbVeiculo.Text) > 0 Then
                
                    If Trim(Me.cmbMotor.Text) <> "" Then
                    
                        If InStr(texto_Grid, Me.cmbMotor.Text) > 0 Then
                            'achou no grid...
                            Achou_Aplicacao_Grid = True
                        End If
                    Else
                        'achou no grid...
                        Achou_Aplicacao_Grid = True
                    End If
                End If
            End If
        End If
    End If
        
End Function
'------------------------------

Private Sub cmdCancel_Click()
  Limpa_Item
  Limpa_Item_Copia
End Sub

Private Sub cmdImportar_Arq_Aplic_Click()

End Sub

Private Sub cmdFotos_Click()
  frmCad_Fotos.Show 1
End Sub

Private Sub cmdImp_Arq_Aplic_Click()
  frmImport_dpk_aplic.Show 1
End Sub

Private Sub cmdImport_Arq_Restr_Click()
  frmImport_DPk_Rest.Show 1
End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    
    Me.Top = 0
    Me.Left = 0
    
    
    BuscaMontadora 0, ""
    cmbMontadora.AddItem " "
    While Not vObjOracle.EOF
        cmbMontadora.AddItem vObjOracle("MONTADORA")
        cmbMontadora.ItemData(cmbMontadora.NewIndex) = vObjOracle("C�D.")
        vObjOracle.MoveNext
    Wend
  
    
End Sub

Private Sub txtCodFrabrica_Change()

End Sub

Private Sub grdAplicacao_DblClick()
  Dim strVeiculo As String
  Dim strOriginal_old As String
  
    strRestricao = "E"
    frmOriginal.cmdGravar.Visible = True
    frmOriginal.Label1.Visible = True
    frmOriginal.Text1.Visible = True
    frmOriginal.Frame1.Caption = "Complementos Espec�ficos"
    
    grdAplicacao.Col = 5
    lngAplicacao = grdAplicacao.Text
    grdAplicacao.Col = 1
    strVeiculo = grdAplicacao.Text
    grdAplicacao.Col = 4
    frmOriginal.Text1.Text = CStr(grdAplicacao.Text)
    strOriginal_old = grdAplicacao.Text
    strOriginal = grdAplicacao.Text
    frmOriginal.Caption = strVeiculo
    frmOriginal.Show 1
    
    Call Altera_Aplic(strOriginal_old, strOriginal)
 
End Sub

Private Sub grdAplicacao_KeyDown(KeyCode As Integer, Shift As Integer)
  
   If KeyCode = 46 Then
    Call Exclui_Aplic
    SSCommand1_Click
   End If
 End Sub

Private Sub grdRestricao_Generica_DblClick()
    
    strRestricao = "G"
    frmOriginal.Label1.Visible = False
    frmOriginal.Text1.Visible = False
    frmOriginal.Frame1.Caption = "Complementos Gen�ricos"

  
    lngAplicacao = 0
    strVeiculo = "DPK: " & txtDPK & " - " & lblDPK
    frmOriginal.Text1.Visible = False
   
   
    frmOriginal.Caption = strVeiculo
    frmOriginal.Show 1
    Call txtDPK_LostFocus
    
 
End Sub

Private Sub grdVeiculo_DblClick()
  Call Insere_Aplic
  lblPesquisa = ""
  strPesquisa = ""
  SSCommand1_Click
End Sub

Private Sub grdVeiculo_KeyPress(KeyAscii As Integer)
 Dim iAscii As Integer
    Dim tam As Byte
    Dim i As Integer
    Dim j As Integer
    Dim iLinha As Integer
        
    'carrega variavel de pesquisa
    tam = Len(strPesquisa)
    grdVeiculo.Col = 1
    If KeyAscii = 8 Then 'backspace
        'mouse
        Screen.MousePointer = vbHourglass

        If tam > 0 Then
            strPesquisa = Mid$(strPesquisa, 1, tam - 1)
            If strPesquisa = "" Then
                lblPesquisa.Caption = strPesquisa
                lblPesquisa.Visible = False
                grdVeiculo.Row = 1
                SendKeys "{LEFT}+{END}"
            Else
                lblPesquisa.Caption = strPesquisa
                DoEvents
                With grdVeiculo
                    iLinha = .Row
                    j = .Row - 1
                    For i = j To 1 Step -1
                        .Row = i
                        If (.Text Like strPesquisa & "*") Then
                            iLinha = .Row
                            Exit For
                        End If
                    Next
                    .Row = iLinha
                    SendKeys "{LEFT}+{END}"
                End With
            End If
        End If
        
    ElseIf KeyAscii = 27 Then
        strPesquisa = ""
        lblPesquisa.Caption = strPesquisa
        lblPesquisa.Visible = False
        lblPesq.Visible = False
        grdPedido.Row = 1
        SendKeys "{LEFT}{RIGHT}"
    
    Else
        'mouse
        Screen.MousePointer = vbHourglass

        If tam < 33 Then
            iAscii = Texto(KeyAscii)
            If iAscii > 0 Then
                'pesquisa
                strPesquisa = strPesquisa & Chr$(iAscii)
                If tam >= 0 Then
                   '    lblPesquisa.Visible = True
                   ' lblPesq.Visible = True
                    lblPesquisa.Caption = strPesquisa
                    DoEvents
                    With grdVeiculo
                        iLinha = .Row
                        If tam = 0 Then
                            .Row = 0
                            For i = 1 To .Rows - 1
                                .Row = i
                                If (.Text Like strPesquisa & "*") Then
                                    iLinha = .Row
                                    Exit For
                                End If
                            Next i
                        Else
                            j = .Row
                            For i = j To .Rows - 1
                                .Row = i
                                If (.Text Like strPesquisa & "*") Then
                                    iLinha = .Row
                                    Exit For
                                End If
                            Next
                        End If
                        If grdVeiculo.Row <> iLinha Then
                            .Row = iLinha
                            strPesquisa = Mid$(strPesquisa, 1, tam)
                            lblPesquisa.Caption = strPesquisa
                            Beep
                        End If
                        SendKeys "{LEFT}+{END}"
                    End With
                    
                End If
            End If
        Else
            Beep
        End If
    End If
    
    'mouse
    Screen.MousePointer = vbDefault
End Sub

Private Sub sscmdEquivalentes_Click()
  If txtDPK <> "" Then
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "dpk"
    vBanco.Parameters.Add "dpk", txtDPK, 1
    
    
    
    vSql = "PRODUCAO.PCK_VDA230.pr_equiv(:PM_CURSOR1,:dpk,:PM_CODERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
  
    
    If vObjOracle.EOF And vObjOracle.BOF Then
      MsgBox "N�o h� itens equivalentes cadastrados para o DPK: " & txtDPK, vbInformation, "Aten��o"
      Exit Sub
    Else
      strFl_Equiv = "S"
      frmItens.Caption = "ITENS EQUIVALENTES"
      vVB_Generica_001.CarregaGridTabela frmItens.grd, vObjOracle, 4
      frmItens.grd.Visible = True
      Screen.MousePointer = 0
      frmItens.Show 1
      If txtDPK <> "" Then
        txtDPK_LostFocus
      End If
    End If
  End If
End Sub

Private Sub SSCommand1_Click()
   lblPesquisa = ""
   strPesquisa = ""
   Call Preenche_Grid_modelos
End Sub

Private Sub SSCommand2_Click()
  Dim strResposta As String

  If txtDPK = "" Then
    MsgBox "Selecione um DPK para efetuar a copia", vbInformation, "Aten��o"
    Exit Sub
  Else
     strResposta = MsgBox("Confirma a copia de aplica��o e restri��o do DPK: " & txtDPK & " para os itens equivalentes ?", vbYesNo, "Aten��o")
     If strResposta = 7 Then
       Exit Sub
     End If
    If Copia_Equiv(txtDPK) = "N" Then
      MsgBox "Este DPK n�o possui itens equivalentes cadastrados", vbInformation, "Aten��o"
    Else
       MsgBox "Copia concluida com sucesso", vbInformation, "Aten��o"
    End If
  End If
End Sub

Private Sub SSCommand3_Click()
   Dim strResposta As String

  If txtDPK = "" Then
    MsgBox "Selecione um DPK para efetuar a copia", vbInformation, "Aten��o"
    Exit Sub
  ElseIf txtDPK_Copia = "" Then
     MsgBox "Selecione para qual DPK deseja efetuar a copia", vbInformation, "Aten��o"
    Exit Sub
  Else
     strResposta = MsgBox("Confirma a copia de aplica��o e restri��o do DPK: " & txtDPK & " para o DPK: " & txtDPK_Copia & " ?", vbYesNo, "Aten��o")
     If strResposta = 7 Then
       Exit Sub
     End If
    If Copia_Item(txtDPK, txtDPK_Copia) = "N" Then
      MsgBox "Ocorreu problema na copia, por favor verifique", vbInformation, "Aten��o"
    Else
       MsgBox "Copia concluida com sucesso", vbInformation, "Aten��o"
    End If
  End If
End Sub

Private Sub txtCodFabrica_DblClick()
    If txtCodFabrica <> "" Then
      frmItens.Show 1
      DoEvents
      txtCodFabrica_LostFocus
      Screen.MousePointer = 0
    End If
  
End Sub

Private Sub txtCodFabrica_KeyPress(KeyAscii As Integer)
   KeyAscii = Maiusculo(KeyAscii)
End Sub

Private Sub txtCodFabrica_LostFocus()

If cmbFornecedor = "" And txtCodFabrica <> "" Then
  MsgBox "Para fazer a consulta por codigo de fabrica, selecione o fornecedor", vbInformation, "Aten��o"
  Exit Sub
End If

If txtDPK = "" And cmbFornecedor <> "" And txtCodFabrica <> "" Then
  Busca_Item
  Screen.MousePointer = 0
End If


End Sub

Private Sub txtDPK_Copia_KeyPress(KeyAscii As Integer)
  KeyAscii = Numerico(KeyAscii)
End Sub

Private Sub txtDPK_Copia_LostFocus()
   If txtDPK_Copia <> "" Then
    Busca_Item_Copia
  Else
    Limpa_Item_Copia
  End If
End Sub

Private Sub txtDPK_KeyPress(KeyAscii As Integer)
   KeyAscii = Numerico(KeyAscii)
End Sub

Private Sub txtDPK_LostFocus()
  
  If txtDPK <> "" Then
    Busca_Item
  Else
    Limpa_Item
  End If
  
End Sub

