VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmSubGrupo 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "SubGrupo Complemento"
   ClientHeight    =   5655
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11880
   Icon            =   "frmSubGrupo.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   377
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   792
   Begin VB.Frame fra 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1680
      Left            =   45
      TabIndex        =   4
      Top             =   945
      Width           =   6450
      Begin VB.ComboBox cmbSituacao 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1125
         Style           =   2  'Dropdown List
         TabIndex        =   13
         Top             =   1170
         Width           =   2220
      End
      Begin VB.ComboBox cmbGrupo 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1125
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   12
         Top             =   270
         Width           =   3255
      End
      Begin VB.TextBox txtSubGrupo 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1125
         MaxLength       =   100
         TabIndex        =   9
         Top             =   720
         Width           =   4740
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Situa��o:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   315
         TabIndex        =   11
         Top             =   1260
         Width           =   750
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "SubGrupo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   180
         TabIndex        =   10
         Top             =   810
         Width           =   870
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Grupo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   495
         TabIndex        =   5
         Top             =   360
         Width           =   555
      End
   End
   Begin VB.TextBox txtCodSubGrupo 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3645
      MaxLength       =   10
      TabIndex        =   3
      Top             =   270
      Visible         =   0   'False
      Width           =   780
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   5325
      Width           =   11880
      _ExtentX        =   20955
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   20452
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      Top             =   810
      Width           =   6450
      _ExtentX        =   11377
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   735
      Left            =   0
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1296
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmSubGrupo.frx":23D2
      PICN            =   "frmSubGrupo.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdGravar 
      Height          =   735
      Left            =   840
      TabIndex        =   6
      TabStop         =   0   'False
      ToolTipText     =   "Gravar"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1296
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmSubGrupo.frx":30C8
      PICN            =   "frmSubGrupo.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSGrid.Grid grd 
      Height          =   2580
      Left            =   45
      TabIndex        =   7
      Top             =   2700
      Width           =   11730
      _Version        =   65536
      _ExtentX        =   20690
      _ExtentY        =   4551
      _StockProps     =   77
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      HighLight       =   0   'False
   End
   Begin Bot�o.cmd cmdCancel 
      Height          =   735
      Left            =   1680
      TabIndex        =   8
      TabStop         =   0   'False
      ToolTipText     =   "Cancelar"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1296
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmSubGrupo.frx":3DBE
      PICN            =   "frmSubGrupo.frx":3DDA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd4 
      Height          =   735
      Left            =   2520
      TabIndex        =   14
      TabStop         =   0   'False
      ToolTipText     =   "Exporta Complementos"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1296
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmSubGrupo.frx":4AB4
      PICN            =   "frmSubGrupo.frx":4AD0
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmSubGrupo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Sub PlanilhaExcel(ByVal Imprimir As Boolean)

    On Error GoTo TrataErro
    
    Dim X As Integer, Y As Integer, Z As Integer
    
    Dim EApp As Excel.Application
    Dim EwkB As Excel.Workbook
    Dim EwkS As Excel.Worksheet
    
    Screen.MousePointer = 11
    DoEvents
    
    On Error Resume Next
    
    Set EApp = CreateObject("Excel.Application")
    Set EwkB = EApp.Workbooks.Add
    Set EwkS = EwkB.Sheets(1)
    
        
    ' Esconde o Excel
    'EApp.Application.Visible = True

    ' Configura Margens da P�gina
    'EApp.ActiveSheet.PageSetup.PrintArea = ""
    
   ' With EApp.ActiveSheet.PageSetup
        '.LeftHeader = "INDICE DE INFLA��O DE COMPRA"
        '.CenterHeader = cmbFornecedor.Text
        '.RightHeader = "&D"
        '.LeftFooter = "Comercial Automotiva Ltda. Confidencial"
        '.CenterFooter = "Depto. Controladoria"
    '    .RightFooter = "P�gina &P/&N"
    '    .LeftMargin = 10 ''Application.InchesToPoints(0.393700787401575)
    '    .RightMargin = 10 ''Application.InchesToPoints(0.393700787401575)
    '    .TopMargin = 10 ''Application.InchesToPoints(0.393700787401575)
    '    .BottomMargin = 10 ''Application.InchesToPoints(0.393700787401575)
        '.HeaderMargin = Application.InchesToPoints(0)
        '.FooterMargin = Application.InchesToPoints(0)
    '    .PrintHeadings = False
    '    .PrintGridlines = False
    '    .PrintComments = xlPrintNoComments
    '    .PrintQuality = 600
    '    .CenterHorizontally = False
    '    .CenterVertically = False
        '.Orientation = xlPortrait
    '    .Draft = False
    '    .PaperSize = xlPaperA4
    '    .FirstPageNumber = xlAutomatic
    '    .Order = xlDownThenOver
    '    .BlackAndWhite = False
        '.Zoom = 65
   ' End With
    
'    EApp.Columns(1).ColumnWidth = 15
'    EApp.Columns(2).ColumnWidth = 18
'    EApp.Columns(3).ColumnWidth = 40
'
'    EApp.Cells(1, 1).Value = "INFORME DE RENDIMENTO DO ANO " & frmConsulta.txtAno.Text
'    EApp.Range(EApp.Cells(1, 1), EApp.Cells(1, 5)).Select
'    With EApp.Selection
'        .HorizontalAlignment = xlCenter
'        .VerticalAlignment = xlBottom
'        .WrapText = False
'        .Orientation = 1
'        .ShrinkToFit = False
'        .MergeCells = True
'    End With
'    EApp.Selection.Merge
'
'    EApp.Cells(1, 1).Font.Bold = True
'
'    EApp.Cells(4, 1).Value = "COD. REPRES"
'    EApp.Cells(4, 1).Font.Bold = True
'
'    EApp.Cells(4, 2).Value = "CGC"
'    EApp.Cells(4, 2).Font.Bold = True
'
'    EApp.Cells(4, 3).Value = "RAZ�O SOCIAL"
'    EApp.Cells(4, 3).Font.Bold = True
'
'
'
'    x = 1
'    For I = 4 To 28 Step 2
'        EApp.Cells(3, I).Value = UCase(MonthName(x))
'        EApp.Range(EApp.Cells(3, I), EApp.Cells(3, I + 1)).Select
'        EApp.Selection.Merge
'        EApp.Selection.HorizontalAlignment = xlCenter
'        EApp.Cells(3, I).Font.Bold = True
'        x = x + 1
'    Next
'
'    For I = 4 To 27 Step 2
'        EApp.Cells(4, I).Value = "VALOR"
'        EApp.Cells(4, I + 1).Value = "IR"
'        EApp.Cells(4, I).HorizontalAlignment = xlCenter
'        EApp.Cells(4, I + 1).HorizontalAlignment = xlCenter
'        EApp.Cells(4, I).Font.Bold = True
'        EApp.Cells(4, I + 1).Font.Bold = True
'    Next

    ' Gera Dados na P�gina e Formata C�lulas
    Z = 1
    vObjOracle.MoveFirst
    
    Col = vObjOracle.Fields.Count

    For Y = 0 To Col
        EApp.Cells(Z, Y + 1).Value = vObjOracle.Fields(Y).Name
    Next

    Z = 2

    While Not vObjOracle.EOF
        For Y = 0 To 5
            EApp.Cells(Z, Y + 1).Value = vObjOracle(Y)
           'EApp.Cells(Z, y + 1).Font.Bold = True
        Next
        Z = Z + 1
        vObjOracle.MoveNext
    Wend
    
    
    If Not Imprimir Then
        ' Exibe Excel
        EApp.Application.Visible = True
    Else
        EApp.ActiveWindow.SelectedSheets.PrintOut Copies:=1
        EApp.ActiveWorkbook.Close (False)
        EApp.Quit
    End If
    
    ' Volta Cursor do Mouse
    Screen.MousePointer = 1
    
    ' Desliga o Excel do Programa
    Set EwkS = Nothing
    Set EwkB = Nothing
    Set EApp = Nothing
    
    Exit Sub
    
TrataErro:
    
       Screen.MousePointer = 1
       MsgBox "Ocorreu erro: " & Err.Number & " - " & Err.Description, 16
       Set EwkS = Nothing
       Set EwkB = Nothing
       Set EApp = Nothing

   
End Sub

Private Sub cmbGrupo_Click()
    BuscaSubGrupo IIf(cmbGrupo.Text = "", 0, Left(cmbGrupo.Text, 3)), 0, txtSubGrupo.Text, ""
    If vObjOracle.EOF Then
        vVB_Generica_001.LimpaGridComTitulo grd
    Else
        vVB_Generica_001.CarregaGridTabela grd, vObjOracle, 4
    End If
End Sub

Private Sub cmd4_Click()
   

    Screen.MousePointer = 11
    stbBarra.Panels(1).Text = "Gerando Relat�rio. Aguarde..."
    DoEvents
    
  
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
    
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "PRODUCAO.PCK_CAD985.PR_ARQ_RESTRICAO(:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
    
    If vObjOracle.EOF Then
        MsgBox "Nenhum complemento cadastrado", , "Aten��o"
        Exit Sub
    End If
    
    PlanilhaExcel False
    
    Screen.MousePointer = 0
    stbBarra.Panels(1).Text = ""
    DoEvents

End Sub

Private Sub cmdCancel_Click()

    txtCodSubGrupo.Text = ""
    txtSubGrupo.Text = ""
    cmbSituacao.Text = "0 - Ativo"
    cmbGrupo.Locked = False
    cmdCancel.Visible = False

End Sub

Private Sub cmdGravar_Click()

    If cmbGrupo.Text = "" Then
        MsgBox "Selecione um Grupo", , "Aten��o"
        cmbGrupo.SetFocus
        Exit Sub
    End If
    
    If txtSubGrupo.Text = "" Then
        MsgBox "Digite o nome do SubGrupo", , "Aten��o"
        txtSubGrupo.SetFocus
        Exit Sub
    End If
    
    If txtCodSubGrupo.Text <> "" Then
        msg = GravarSubGrupo("A", Mid(cmbGrupo.Text, 1, 3), txtCodSubGrupo.Text, txtSubGrupo.Text, Left(cmbSituacao.Text, 1))
        If msg <> "" Then
            MsgBox "Erro: " & msg
            Exit Sub
        Else
            MsgBox "SubGrupo alterado com sucesso"
        End If
    Else
        BuscaSubGrupo Left(cmbGrupo.Text, 3), 0, txtSubGrupo.Text, ""
    
        If vObjOracle.EOF Then
            msg = GravarSubGrupo("I", Mid(cmbGrupo.Text, 1, 3), 0, txtSubGrupo.Text, Left(cmbSituacao.Text, 1))
            If msg <> "" Then
                MsgBox "Erro: " & msg
                Exit Sub
            Else
                MsgBox "SubGrupo incluido com sucesso"
            End If
        Else
            MsgBox "Este SubGrupo j� esta cadastrado para esta Grupo", , "Aten��o"
            Exit Sub
        End If
    End If
    
    cmdCancel_Click
    cmbGrupo_Click

End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    
    Me.Top = 0
    Me.Left = 0
    
    BuscaGrupo 0, "", ""
    vVB_Generica_001.PreencheComboList vObjOracle, cmbGrupo, "C�D.", "Grupo"
    
    cmbSituacao.AddItem "0 - Ativo"
    cmbSituacao.AddItem "9 - Desativado"
    
    cmbSituacao.Text = "0 - Ativo"
    
    cmbGrupo_Click
    
End Sub

Private Sub grd_DblClick()

    txtSubGrupo.Text = ""
    txtCodSubGrupo.Text = ""
    
    If grd.Text <> "" Then
    
        grd.Col = 1
        grupo = grd.Text
        grd.Col = 2
        vtxtsubgrupo = Mid(grd.Text, 7, Len(grd.Text))
        vcodsubgrupo = Mid(grd.Text, 1, 3)
        grd.Col = 3
        Situacao = grd.Text
        
        cmbGrupo.Text = grupo
        txtSubGrupo.Text = vtxtsubgrupo
        txtCodSubGrupo.Text = vcodsubgrupo
        cmbSituacao.Text = Situacao
        
        cmbGrupo.Locked = True
        
        cmdCancel.Visible = True
    End If
End Sub


Private Function GravarSubGrupo(pACT As String, codGrupo As Integer, codSubGrupo As Integer, descSubGrupo As String, Situacao As Integer) As String

    vBanco.Parameters.Remove "PM_ACT"
    vBanco.Parameters.Add "PM_ACT", pACT, 1
    
    vBanco.Parameters.Remove "PM_CODGRUPO"
    vBanco.Parameters.Add "PM_CODGRUPO", codGrupo, 1
    
    vBanco.Parameters.Remove "PM_CODSUBGRUPO"
    vBanco.Parameters.Add "PM_CODSUBGRUPO", codSubGrupo, 1
    
    vBanco.Parameters.Remove "PM_DESCSUBGRUPO"
    vBanco.Parameters.Add "PM_DESCSUBGRUPO", descSubGrupo, 1
    
    vBanco.Parameters.Remove "PM_SITUACAO"
    vBanco.Parameters.Add "PM_SITUACAO", Situacao, 1
  
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "PRODUCAO.PCK_CAD985.PR_ACT_SubGrupo(:PM_ACT,:PM_CODGRUPO,:PM_CODSUBGRUPO,:PM_DESCSUBGRUPO,:PM_SITUACAO,:PM_CODERRO,:PM_TXTERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    If vBanco.Parameters("PM_CODERRO").Value <> 0 Then
        GravarSubGrupo = vBanco.Parameters("PM_CODERRO").Value & " - " & vBanco.Parameters("PM_TXTERRO").Value
    Else
        GravarSubGrupo = ""
    End If

End Function

