VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{EAB22AC0-30C1-11CF-A7EB-0000C05BAE0B}#1.1#0"; "SHDOCVW.DLL"
Begin VB.Form frmCad_Fotos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cadastro de Fotos"
   ClientHeight    =   7440
   ClientLeft      =   300
   ClientTop       =   495
   ClientWidth     =   11385
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7440
   ScaleWidth      =   11385
   Begin VB.Frame Frame1 
      Caption         =   "Cadastro de Fotos"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   6135
      Left            =   720
      TabIndex        =   2
      Top             =   720
      Width           =   9975
      Begin TabDlg.SSTab SSTab1 
         Height          =   5175
         Left            =   480
         TabIndex        =   3
         Top             =   600
         Width           =   9000
         _ExtentX        =   15875
         _ExtentY        =   9128
         _Version        =   393216
         Tabs            =   2
         TabHeight       =   520
         TabMaxWidth     =   5203
         ForeColor       =   16711680
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Gravar Foto"
         TabPicture(0)   =   "frmCad_Fotos.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "Label2"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "Label1"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "cmd_Salva_Foto"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "chk_Todas_Fotos"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txt_Local_Arquivo"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "File1"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "Dir1"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "Drive1"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).ControlCount=   8
         TabCaption(1)   =   "Visualizar Foto"
         TabPicture(1)   =   "frmCad_Fotos.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "Label5"
         Tab(1).Control(1)=   "webfoto"
         Tab(1).Control(2)=   "txt_Cod_DPK"
         Tab(1).Control(3)=   "pctFoto"
         Tab(1).Control(4)=   "cmd_Exibe_Foto"
         Tab(1).ControlCount=   5
         Begin VB.TextBox txt_Cod_DPK 
            BackColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   -72000
            MaxLength       =   7
            TabIndex        =   12
            Top             =   840
            Width           =   1455
         End
         Begin VB.PictureBox pctFoto 
            AutoSize        =   -1  'True
            Height          =   2265
            Left            =   -72000
            Picture         =   "frmCad_Fotos.frx":0038
            ScaleHeight     =   2205
            ScaleMode       =   0  'User
            ScaleWidth      =   2805
            TabIndex        =   11
            Top             =   2040
            Visible         =   0   'False
            Width           =   2865
         End
         Begin VB.DriveListBox Drive1 
            Height          =   315
            Left            =   2520
            TabIndex        =   10
            Top             =   1260
            Width           =   4335
         End
         Begin VB.DirListBox Dir1 
            Height          =   1440
            Left            =   2520
            TabIndex        =   9
            Top             =   1740
            Width           =   4335
         End
         Begin VB.FileListBox File1 
            Height          =   1455
            Left            =   2520
            Pattern         =   "*.JPG"
            TabIndex        =   8
            Top             =   3300
            Width           =   4335
         End
         Begin VB.TextBox txt_Local_Arquivo 
            BackColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   2520
            TabIndex        =   7
            Top             =   780
            Width           =   6135
         End
         Begin VB.CheckBox chk_Todas_Fotos 
            Caption         =   "Todas as fotos"
            Height          =   195
            Left            =   7320
            TabIndex        =   6
            Top             =   3310
            Width           =   1455
         End
         Begin VB.CommandButton cmd_Exibe_Foto 
            Caption         =   "Exibir Foto"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   795
            Left            =   -70020
            Picture         =   "frmCad_Fotos.frx":0AD7
            Style           =   1  'Graphical
            TabIndex        =   5
            Top             =   840
            Width           =   1170
         End
         Begin VB.CommandButton cmd_Salva_Foto 
            Caption         =   "Salvar Foto"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   795
            Left            =   7320
            Picture         =   "frmCad_Fotos.frx":0DE1
            Style           =   1  'Graphical
            TabIndex        =   4
            Top             =   1740
            Width           =   1170
         End
         Begin SHDocVwCtl.WebBrowser webfoto 
            Height          =   2535
            Left            =   -72000
            TabIndex        =   13
            Top             =   2040
            Visible         =   0   'False
            Width           =   3165
            ExtentX         =   5583
            ExtentY         =   4471
            ViewMode        =   0
            Offline         =   0
            Silent          =   0
            RegisterAsBrowser=   0
            RegisterAsDropTarget=   1
            AutoArrange     =   0   'False
            NoClientEdge    =   0   'False
            AlignLeft       =   0   'False
            NoWebView       =   0   'False
            HideFileNames   =   0   'False
            SingleClick     =   0   'False
            SingleSelection =   0   'False
            NoFolders       =   0   'False
            Transparent     =   0   'False
            ViewID          =   "{0057D0E0-3573-11CF-AE69-08002B2E1262}"
            Location        =   "http:///"
         End
         Begin VB.Label Label5 
            AutoSize        =   -1  'True
            Caption         =   "C�digo Item DPK :"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   -73800
            TabIndex        =   16
            Top             =   900
            Width           =   1515
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Nome do Arquivo :"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   720
            TabIndex        =   15
            Top             =   855
            Width           =   1545
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "Local do Arquivo :"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   210
            Left            =   720
            TabIndex        =   14
            Top             =   1320
            Width           =   1470
         End
      End
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   0
      TabIndex        =   0
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   0
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCad_Fotos.frx":0EF0
      PICN            =   "frmCad_Fotos.frx":0F0C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd1 
      Height          =   690
      Left            =   0
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   0
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCad_Fotos.frx":1BE6
      PICN            =   "frmCad_Fotos.frx":1C02
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmCad_Fotos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Emerson - 13/08/15 - TI-1842
Dim oradatabase As Object
Dim OraParameters As Object

Dim vFlCursor As Boolean

Const ORATYPE_CURSOR = 102
Const ORADYN_NO_BLANKSTRIP = &H2&
Const ORAPARM_INPUT = 1              'CONSTANTE DE BIND INPUT
Const ORAPARM_OUTPUT = 2             'CONSTANTE DE BIND OUTPUT
Const ORAPARM_BOTH = 3               'CONSTANTE DE BIND INPUT/OUTPUT
Const ORATYPE_NUMBER = 2

Const sCOD_VEND = 4613
'Emerson - 13/08/15 - TI-1842 - FIM...

Private Sub Form_Load()
 '  webfoto.Navigate ("http://DPKNET/CATALOGO/CADFOTOS/CADFOTOS_MENU.asp ?&fl=""&act=1&edtForn=0&edtgrupo=0&edtsub=0&status=0&Ordem=""&nPag=""")
  'Emerson - 13/08/15 - TI-1842
  'webfoto.Navigate ("http://DPKNET/CATALOGO/CADFOTOS/CADFOTOS_MENU.asp")
  '  webfoto.Navigate ("http://DPKNETWEBS/Informe/ChamadoCA/Clientes.asp?fl=1&act=1&codcli=" & frmVenda.txtCOD_CLIENTE)
    Me.Dir1.Path = App.Path
    vFlCursor = False
    Me.Drive1.Enabled = False
    Me.Drive1.BackColor = &HC0C0C0
    Me.Dir1.Enabled = False
    Me.Dir1.BackColor = &HC0C0C0
    Me.File1.Enabled = False
    Me.File1.BackColor = &HC0C0C0
  
End Sub

Private Sub cmdVoltar_Click()
    
    'Emerson - 13/08/15 - TI-1842
    If vFlCursor Then
        'Fecha a conex�o com o DB...
        oradatabase.Close
        Set oradatabase = Nothing
    End If
    'Emerson - 13/08/15 - TI-1842 - FIM...

  Unload Me
End Sub

'Emerson - 13/08/15 - TI-1842
Private Sub chk_Todas_Fotos_Click()

    If Me.chk_Todas_Fotos.Value = 1 Then
        Me.txt_Local_Arquivo.Enabled = False
        Me.txt_Local_Arquivo.BackColor = &HC0C0C0
        
        Me.Drive1.Enabled = True
        Me.Drive1.BackColor = &H80000005
        Me.Dir1.Enabled = True
        Me.Dir1.BackColor = &H80000005
        Me.File1.Enabled = True
        Me.File1.BackColor = &H80000005

        Me.Drive1.SetFocus
    Else
        Me.txt_Local_Arquivo.Enabled = True
        Me.txt_Local_Arquivo.BackColor = &H80000005
        
        Me.Drive1.Enabled = False
        Me.Drive1.BackColor = &HC0C0C0
        Me.Dir1.Enabled = False
        Me.Dir1.BackColor = &HC0C0C0
        Me.File1.Enabled = False
        Me.File1.BackColor = &HC0C0C0
        
        Me.txt_Local_Arquivo.SetFocus
    End If

End Sub

Private Sub cmd_Salva_Foto_Click()

    If Not Consiste_Dados Then
        If Me.chk_Todas_Fotos.Value = 0 Then
            Me.txt_Local_Arquivo.SetFocus
        End If
        Exit Sub
    End If

    If Me.chk_Todas_Fotos.Value = 1 Then
    
    'Grava TODAS AS FOTOS da pasta selecionada..
    
        Dim Z As Integer
        Dim vQtdeFotos As Integer
        Dim vstrnomefoto As String
        Dim vVet_Fotos() As String
    
        vQtdeFotos = Me.File1.ListCount - 1
        
        ReDim vVet_Fotos(vQtdeFotos) As String
        
        For Z = 0 To vQtdeFotos
            vVet_Fotos(Z) = Me.File1.List(Z)
        Next
        
        For Z = 0 To vQtdeFotos
        
            vstrnomefoto = Mid(Me.File1.Path & "\" & vVet_Fotos(Z), 1, Len(Me.File1.Path & "\" & vVet_Fotos(Z)) - 4)
            
            Grava_Foto (vstrnomefoto)
        
        Next
        
        Me.File1.Refresh
    Else
    
    'Grava imagem avulsa...
        
        vstrnomefoto = Mid(Me.txt_Local_Arquivo, 1, Len(Me.txt_Local_Arquivo) - 4)
        
        Grava_Foto (vstrnomefoto)
    End If

End Sub

Function Consiste_Dados() As Boolean

Dim Z As Integer
Dim vTamanhoNome As Integer
Dim vstrnomefoto As String
Dim vstrnomefoto_aux As String

    Consiste_Dados = False

    If Me.chk_Todas_Fotos.Value = 1 Then
    
        If Me.File1.ListCount = 0 Then
            MsgBox "N�o consta nenhuma Foto (arquivo.JPG)  na Pasta  " & Me.File1.Path & ". Verifique !", vbExclamation, "Aten��o"
            Exit Function
        End If
        
        If MsgBox("Confirma importa��o de TODAS AS FOTOS" & Chr(13) & Chr(13) & "da Pasta  " & Me.File1.Path & "  ?", vbQuestion + vbYesNo, "Aten��o") = vbNo Then
            Exit Function
        End If
    Else
    
        If Trim(Me.txt_Local_Arquivo.Text) = "" Then
            MsgBox "Informe o Local da Foto.", vbExclamation, "Aten��o"
            Me.txt_Local_Arquivo.SetFocus
            Exit Function
        End If
        
        'Verifica o nome da foto..
        
        vTamanhoNome = Len(Me.txt_Local_Arquivo) - 4
        
        If vTamanhoNome < 1 Then
            MsgBox "Nome da foto inv�lido.", vbExclamation
            Exit Function
        End If
        
        If Right(UCase$(Me.txt_Local_Arquivo), 4) <> ".JPG" Then
            MsgBox "Extens�o do nome da foto deve ser somente '.JPG'", vbExclamation
            Exit Function
        End If
        
        vstrnomefoto = Mid(Me.txt_Local_Arquivo, 1, Len(Me.txt_Local_Arquivo) - 4)
        
        If Trim(vstrnomefoto) = "" Then
            MsgBox "Nome da foto inv�lido.", vbExclamation
            Exit Function
        End If
        
        For Z = vTamanhoNome To 1 Step -1
            If IsNumeric(Mid(vstrnomefoto, Z, 1)) Then
                vstrnomefoto_aux = Mid(vstrnomefoto, Z, 1) & vstrnomefoto_aux
            Else
                vstrnomefoto_aux = vstrnomefoto_aux & ".jpg"
                Exit For
            End If
        Next

        'verifica se o nome da foto � o c�d. item DPK..
        If vstrnomefoto_aux = ".jpg" Then
            MsgBox "Nome da Foto deve ser o C�digo do Item DPK (num�rico at� 7 d�gitos).", vbExclamation
            Exit Function
        End If

        'Verifica se a foto existe no local (Origem)...
        If Dir(Me.txt_Local_Arquivo.Text) = "" Then
            MsgBox "Foto N�O Encontrada no local de origem. Verifique !", vbExclamation
            Exit Function
        End If

    End If
    
    Consiste_Dados = True

End Function

Private Sub cmd_Exibe_Foto_Click()

Dim valorPadrao As String
Dim strDominioDpkNetWeb As Variant
    
Dim ss As Object
    
    valorPadrao = "dpknetwebs"
    
    Dim vArquivoINI As String
    vArquivoINI = "H:\oracle\sistemas\vb\32bits\DPKNETWEB.INI"
    
    If Dir(vArquivoINI) <> "" Then
        strDominioDpkNetWeb = GetKeyVal(vArquivoINI, "DPKNETWEB", "DOMINIO")
    Else
        WritePrivateProfileString "DPKNETWEB", "DOMINIO", valorPadrao, vArquivoINI
    End If

    Set vSessao = CreateObject("oracleinprocserver.xorasession")

    'Producao
    Set oradatabase = vSessao.OpenDatabase("PRODUCAO", "tlm_" & sCOD_VEND & "/PROD", 0&)

    'Desenvolvimento
    'Set oradatabase = vSessao.OpenDatabase("SDPK_DEV", "tlm_" & sCOD_VEND & "/PROD", 0&)

    Set vBanco = oradatabase
    Set OraParameters = oradatabase.Parameters

    Criar_Cursor

    OraParameters.Remove "dpk"
    OraParameters.Add "dpk", Me.txt_Cod_DPK.Text, 1

    OraParameters.Remove "vErro"
    OraParameters.Add "vErro", 0, 2

    vSql = "Begin producao.pck_vda230.pr_item_foto(:vCursor,:dpk, :vErro);END;"

    'Criar_Cursor
    oradatabase.ExecuteSQL vSql
    Set ss = oradatabase.Parameters("vCursor").Value

    If Not ss.EOF Then
        webfoto.Navigate ("http://" & strDominioDpkNetWeb & "/catalogo/cadfotos/mostraimg.ASP?ft=" & CStr(ss(1)))
        webfoto.Visible = True
    Else
        webfoto.Visible = False
        MsgBox "N�o foi poss�vel encontrar esta foto no banco de dados !", vbExclamation
        Me.txt_Cod_DPK.SetFocus
    End If

    Set ss = Nothing

End Sub

Private Function GetKeyVal(ByVal FileName As String, ByVal Section As String, ByVal Key As String)
    'Returns info from an INI file
    Dim RetVal As String, Worked As Integer
    If Dir(FileName) = "" Then MsgBox FileName & " N�o Encontrado.", vbCritical, "Urvaz System": Exit Function
    RetVal = String$(255, 0)
    Worked = GetPrivateProfileString(Section, Key, "", RetVal, Len(RetVal), FileName)
    If Worked = 0 Then
        GetKeyVal = ""
    Else
        GetKeyVal = Left(RetVal, InStr(RetVal, Chr(0)) - 1)
    End If
End Function

Public Sub Criar_Cursor()
    OraParameters.Remove "vCursor"
    OraParameters.Add "vCursor", 0, 2
    OraParameters("vCursor").ServerType = ORATYPE_CURSOR
    OraParameters("vCursor").DynasetOption = ORADYN_NO_BLANKSTRIP
    OraParameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
    vFlCursor = True
End Sub

Private Sub Dir1_Change()
    Me.File1.Path = Me.Dir1.Path
End Sub

Private Sub Drive1_Change()
    Dir1.Path = Me.Drive1.Drive
End Sub

Sub Grava_Foto(NomeFoto)

Dim X As Integer
Dim Z As Integer
Dim vTamanhoNome As Integer
Dim vstrnomefoto As String
Dim vstrnomefoto_aux As String

Dim vImagem_FOTO As String
Dim vImagem_FOTO_JPG As String

Dim vCodFoto As Long
Dim vstrCaminhoPadrao_Fotos As String

Dim vstrOrigem As String
Dim vstrDestino As String
Dim vConfereCopia As String

    On Error GoTo TrataErro

    'ativa ampulheta...
    Screen.MousePointer = 11

    vstrCaminhoPadrao_Fotos = "\\nt_idpk2000\intranet$\Web\itens\fotos"
    
    vstrnomefoto = NomeFoto
    
    vTamanhoNome = Len(vstrnomefoto)

    vCodFoto = 0

    For Z = vTamanhoNome To 1 Step -1
        If IsNumeric(Mid(vstrnomefoto, Z, 1)) Then
            vstrnomefoto_aux = Mid(vstrnomefoto, Z, 1) & vstrnomefoto_aux
        Else
            vCodFoto = vstrnomefoto_aux
            Exit For
        End If
    Next

    vImagem_FOTO = vstrnomefoto_aux & ".JPG"

    vstrDestino = vstrCaminhoPadrao_Fotos & "\" & vImagem_FOTO
        
    'verifica se a imagem j� existe...
    If Dir(vstrDestino) <> "" Then
    
        MsgBox "Foto  " & vImagem_FOTO & "  j� cadastrada !", vbExclamation
        'desativa ampulheta...
        Screen.MousePointer = 0
        Exit Sub
    Else

        If Me.chk_Todas_Fotos.Value = 0 Then
            vstrOrigem = Me.txt_Local_Arquivo.Text
        Else
            vstrOrigem = Me.File1.Path & "\" & vImagem_FOTO
        End If

       FileCopy vstrOrigem, vstrDestino

'exc
'            vstrDestino = vstrCaminhoPadrao_Fotos & "\3.JPG"

       vConfereCopia = Dir(vstrDestino)

       While vConfereCopia = ""
           For X = 1 To 1000
               vConfereCopia = Dir(vstrDestino)
               If vConfereCopia <> "" Then
                   Exit For
               End If
           Next
       Wend

       'caso a imagem j� tenha sido copiada...
       If vConfereCopia <> "" Then
        
            'Producao
            Set oradatabase = vSessao.OpenDatabase("PRODUCAO", "tlm_" & sCOD_VEND & "/PROD", 0&)
            
            'Desenvolvimento
            'Set oradatabase = vSessao.OpenDatabase("SDPK_DEV", "tlm_" & sCOD_VEND & "/PROD", 0&)
            
            Set vBanco = oradatabase
            Set OraParameters = oradatabase.Parameters

            'Criar_Cursor
        
            OraParameters.Remove "PM_DPK"
            OraParameters.Add "PM_DPK", vCodFoto, 1
            
            OraParameters.Remove "PM_CODFOTO"
            OraParameters.Add "PM_CODFOTO", vCodFoto, 1

            OraParameters.Remove "PM_DTFOTO"
            OraParameters.Add "PM_DTFOTO", Format(Date, "dd/mm/yyyy") & Format(Time, "hh:mm:ss"), 1
            
            OraParameters.Remove "PM_ERRO"
            OraParameters.Add "PM_ERRO", 0, 2
            
            oradatabase.ExecuteSQL "BEGIN PRODUCAO.pck_CADFOTO.pr_con_GRAVAFOTOS(:PM_DPK, :PM_CODFOTO, :PM_DTFOTO, :PM_ERRO); END;"
            
'exc
'            oradatabase.ExecuteSQL "BEGIN PRODUCAO.pck_CADFOTO.pr_con_APAGAFOTO(:PM_DPK, :PM_ERRO); END;"
        

            If oradatabase.Parameters("PM_Erro").Value <> 0 Then
                
                'remover imagem da pasta padr�o...
                Kill vstrDestino

                MsgBox "Erro " & oradatabase.Parameters("PM_Erro").Value
            Else
                'atribui o caminho ao nome da foto...
                vImagem_FOTO_JPG = Mid(vstrnomefoto, 1, Z) & vstrnomefoto_aux & ".JPG"
                
                vImagem_FOTO = Mid(vstrnomefoto, 1, Z) & vstrnomefoto_aux & ".IMP"
                
                'verifica se j� existe...
                If Dir(vImagem_FOTO) <> "" Then
                    Kill vImagem_FOTO
                End If
                
                'renomeia foto importada...
                FileCopy vstrOrigem, vImagem_FOTO
                
                'confirma rename...
                If Dir(vImagem_FOTO) <> "" Then
                    'deleta foto origem...
                    Kill vstrOrigem
                End If

                If Me.chk_Todas_Fotos.Value = 1 Then
                    Me.File1.Refresh
                End If

                MsgBox "Foto  " & vImagem_FOTO_JPG & "  gravada com sucesso !", vbInformation
            End If
        End If
    End If

    'desativa ampulheta...
    Screen.MousePointer = 0

    Exit Sub
    
TrataErro:
    If Err.Number <> 0 Then
        MsgBox "Erro ao gravar imagem. Erro: " & Err.Number & vbCrLf & " - " & Err.Description & vbCrLf & "Linha: " & Erl, "Aten�ao", &H40000 + 0
        'Resume
    End If

End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)

    If Me.SSTab1.Tab = 0 Then
        If Me.chk_Todas_Fotos.Value = 0 Then
            Me.txt_Local_Arquivo.SetFocus
        Else
            Me.Drive1.SetFocus
        End If
    Else
        Me.txt_Cod_DPK.SetFocus
    End If

End Sub
'Emerson - 13/08/15 - TI-1842 - FIM...



