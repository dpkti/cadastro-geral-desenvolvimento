VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Begin VB.Form frmOriginal 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   6495
   ClientLeft      =   1605
   ClientTop       =   1305
   ClientWidth     =   10365
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6495
   ScaleWidth      =   10365
   Begin VB.Frame Frame1 
      Caption         =   "Complementos Espec�ficos"
      ForeColor       =   &H00FF0000&
      Height          =   4935
      Left            =   120
      TabIndex        =   4
      Top             =   1440
      Width           =   10095
      Begin Threed.SSCommand sscmd_Insere 
         Height          =   495
         Left            =   240
         TabIndex        =   13
         Top             =   1560
         Width           =   1335
         _Version        =   65536
         _ExtentX        =   2355
         _ExtentY        =   873
         _StockProps     =   78
         Caption         =   "Incluir / Alterar"
      End
      Begin VB.ComboBox cmbSubgrupo_Restricao 
         ForeColor       =   &H00800000&
         Height          =   315
         Left            =   5160
         Sorted          =   -1  'True
         TabIndex        =   12
         Top             =   960
         Width           =   3975
      End
      Begin VB.ComboBox cmbGrupo_Restricao 
         ForeColor       =   &H00800000&
         Height          =   315
         Left            =   600
         Sorted          =   -1  'True
         TabIndex        =   10
         Top             =   960
         Width           =   3615
      End
      Begin VB.TextBox txtDt_Fim 
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   3960
         MaxLength       =   8
         TabIndex        =   8
         Top             =   360
         Width           =   1215
      End
      Begin VB.TextBox txtDt_Inicio 
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   1440
         MaxLength       =   8
         TabIndex        =   7
         Top             =   360
         Width           =   1215
      End
      Begin MSGrid.Grid grdRestricao_Cad 
         Height          =   2640
         Left            =   240
         TabIndex        =   14
         Top             =   2160
         Visible         =   0   'False
         Width           =   9375
         _Version        =   65536
         _ExtentX        =   16536
         _ExtentY        =   4657
         _StockProps     =   77
         ForeColor       =   8388608
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         HighLight       =   0   'False
      End
      Begin Threed.SSCommand sscmdExcluir 
         Height          =   495
         Left            =   1680
         TabIndex        =   15
         Top             =   1560
         Width           =   1215
         _Version        =   65536
         _ExtentX        =   2143
         _ExtentY        =   873
         _StockProps     =   78
         Caption         =   "Excluir"
      End
      Begin VB.Label Label5 
         Caption         =   "Subgrupo"
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   4320
         TabIndex        =   11
         Top             =   960
         Width           =   735
      End
      Begin VB.Label Label4 
         Caption         =   "Grupo"
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   960
         Width           =   615
      End
      Begin VB.Label Label3 
         Caption         =   "Dt. Mod. Fim"
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   2880
         TabIndex        =   6
         Top             =   360
         Width           =   1095
      End
      Begin VB.Label Label2 
         Caption         =   "Dt. Mod. Inicio"
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   120
         TabIndex        =   5
         Top             =   360
         Width           =   1095
      End
   End
   Begin VB.TextBox Text1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00400000&
      Height          =   375
      Left            =   1440
      TabIndex        =   0
      Top             =   840
      Visible         =   0   'False
      Width           =   3375
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   735
      Left            =   0
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1296
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmOriginal.frx":0000
      PICN            =   "frmOriginal.frx":001C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdGravar 
      Height          =   690
      Left            =   840
      TabIndex        =   3
      TabStop         =   0   'False
      ToolTipText     =   "Gravar nr. Original"
      Top             =   0
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmOriginal.frx":0CF6
      PICN            =   "frmOriginal.frx":0D12
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label1 
      Caption         =   "Nr. Original"
      ForeColor       =   &H00FF0000&
      Height          =   255
      Left            =   240
      TabIndex        =   1
      Top             =   840
      Visible         =   0   'False
      Width           =   1095
   End
End
Attribute VB_Name = "frmOriginal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public lngCod_Grupo As Long
Public lngCod_Subgrupo As Long
Public lngSequencia As Long
Public strTipo_acao As String
Public Sub Insere_Restricao()

    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_DESCERRO"
    vBanco.Parameters.Add "PM_DESCERRO", 0, 2
    
    
    vBanco.Parameters.Remove "act"
    vBanco.Parameters.Add "act", strTipo_acao, 1
    vBanco.Parameters.Remove "DPK"
    vBanco.Parameters.Add "DPK", CLng(frmDpkAplicacao.txtDPK), 1
    vBanco.Parameters.Remove "aplic"
    vBanco.Parameters.Add "aplic", IIf(strRestricao = "G", 0, lngAplicacao), 1
    vBanco.Parameters.Remove "seq"
    vBanco.Parameters.Add "seq", lngSequencia, 1
    vBanco.Parameters.Remove "grupo"
    vBanco.Parameters.Add "grupo", lngCod_Grupo, 1
    vBanco.Parameters.Remove "subgrupo"
    vBanco.Parameters.Add "subgrupo", lngCod_Subgrupo, 1
    vBanco.Parameters.Remove "dt_ini"
    vBanco.Parameters.Add "dt_ini", txtDt_Inicio, 1
    vBanco.Parameters.Remove "dt_fim"
    vBanco.Parameters.Add "dt_fim", txtDt_Fim, 1
    

    
    vSql = "PRODUCAO.PCK_CAD985.PR_act_dpk_rest(:act,:dpk,:APLIC,:SEQ,:GRUPO,:subgrupo,:dt_ini,:dt_fim,:PM_DESCERRO, :PM_CODERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
   
   
     Call Consulta_Restricao
     strTipo_acao = "I"
     txtDt_Inicio = ""
     txtDt_Fim = ""
     
     'Emerson - 03/03/15 - TI-1843 - Acertar alguns campos do CAD985...
     'Solicitado inicializar o conte�do dos campos Grupo e SubGrupo para n�o replicar na pr�xima grava��o...
     lngCod_Grupo = 0
     lngCod_Subgrupo = 0
     '--------------------
     
     cmbGrupo_Restricao.Text = ""
     cmbSubgrupo_Restricao.Text = ""

  
End Sub
Public Sub Exclui_Restricao()

    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_DESCERRO"
    vBanco.Parameters.Add "PM_DESCERRO", 0, 2
    
    
    vBanco.Parameters.Remove "act"
    vBanco.Parameters.Add "act", strTipo_acao, 1
    vBanco.Parameters.Remove "DPK"
    vBanco.Parameters.Add "DPK", CLng(frmDpkAplicacao.txtDPK), 1
    vBanco.Parameters.Remove "aplic"
    vBanco.Parameters.Add "aplic", IIf(strRestricao = "G", 0, lngAplicacao), 1
    vBanco.Parameters.Remove "seq"
    vBanco.Parameters.Add "seq", lngSequencia, 1
    vBanco.Parameters.Remove "grupo"
    vBanco.Parameters.Add "grupo", lngCod_Grupo, 1
    vBanco.Parameters.Remove "subgrupo"
    vBanco.Parameters.Add "subgrupo", lngCod_Subgrupo, 1
    vBanco.Parameters.Remove "dt_ini"
    vBanco.Parameters.Add "dt_ini", txtDt_Inicio, 1
    vBanco.Parameters.Remove "dt_fim"
    vBanco.Parameters.Add "dt_fim", txtDt_Fim, 1
    

    
    vSql = "PRODUCAO.PCK_CAD985.PR_act_dpk_rest(:act,:dpk,:APLIC,:SEQ,:GRUPO,:subgrupo,:dt_ini,:dt_fim,:PM_DESCERRO, :PM_CODERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
   
   
     Call Consulta_Restricao
     strTipo_acao = "A"
     txtDt_Inicio = ""
     txtDt_Fim = ""
     cmbGrupo_Restricao.Text = ""
     cmbSubgrupo_Restricao.Text = ""

  
End Sub
Public Sub Consulta_Restricao()

   If strRestricao = "E" And lngAplicacao = 0 Then
     grdRestricao_Cad.Visible = False
     cmdGravar.Visible = True
   Else
        cmdGravar.Visible = False
        vBanco.Parameters.Remove "PM_CURSOR1"
        vBanco.Parameters.Add "PM_CURSOR1", 0, 3
        vBanco.Parameters("PM_CURSOR1").ServerType = 102
        vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
        vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
        vBanco.Parameters.Remove "PM_CODERRO"
        vBanco.Parameters.Add "PM_CODERRO", 0, 2
        vBanco.Parameters.Remove "PM_DESCERRO"
        vBanco.Parameters.Add "PM_DESCERRO", 0, 2
        
        vBanco.Parameters.Remove "DPK"
        vBanco.Parameters.Add "DPK", CLng(frmDpkAplicacao.txtDPK), 1
        vBanco.Parameters.Remove "aplic"
        vBanco.Parameters.Add "aplic", IIf(strRestricao = "G", 0, lngAplicacao), 1
    
        
        vSql = "PRODUCAO.PCK_CAD985.PR_DPK_RESTRICAO(:dpk,:APLIC,:PM_CURSOR1,:PM_DESCERRO, :PM_CODERRO)"
        
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        
        Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
        
        If vObjOracle.EOF And vObjOracle.BOF Then
          grdRestricao_Cad.Visible = False
        Else
          vVB_Generica_001.CarregaGridTabela grdRestricao_Cad, vObjOracle, 8
          grdRestricao_Cad.Visible = True
        End If
  End If
  
End Sub


Private Sub cmbGrupo_Restricao_Change()
'  cmbGrupo_Restricao.ListIndex = -1
End Sub

Private Sub cmbGrupo_Restricao_Click()
  cmbSubgrupo_Restricao.Clear
  cmbSubgrupo_Restricao.Text = ""
  lngCod_Grupo = cmbGrupo_Restricao.ItemData(cmbGrupo_Restricao.ListIndex)
End Sub

Private Sub cmbGrupo_Restricao_LostFocus()
 If cmbGrupo_Restricao.Text <> "" Then
   lngCod_Grupo = cmbGrupo_Restricao.ItemData(cmbGrupo_Restricao.ListIndex)
 End If
End Sub

Private Sub cmbSubgrupo_Restricao_Change()
'  If cmbSubgrupo_Restricao.Text <> "" Then
'    cmbSubgrupo_Restricao.ListIndex = -1
'  End If
End Sub

Private Sub cmbSubgrupo_Restricao_DropDown()
    
   If cmbGrupo_Restricao.ListIndex <> -1 Then
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_DESCERRO"
    vBanco.Parameters.Add "PM_DESCERRO", 0, 2
    
    vBanco.Parameters.Remove "grupo"
    vBanco.Parameters.Add "grupo", lngCod_Grupo, 1
    vBanco.Parameters.Remove "dpk"
    vBanco.Parameters.Add "dpk", frmDpkAplicacao.txtDPK, 1
    vBanco.Parameters.Remove "aplic"
    vBanco.Parameters.Add "aplic", lngAplicacao, 1


    
    vSql = "PRODUCAO.PCK_CAD985.PR_CON_SUBRESTRICAO(:grupo,:dpk,:aplic,:PM_CURSOR1,:PM_DESCERRO, :PM_CODERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value


   If cmbSubgrupo_Restricao.ListCount = 0 Then
        While Not vObjOracle.EOF
            cmbSubgrupo_Restricao.AddItem vObjOracle("DESC_subGRUPO")
            cmbSubgrupo_Restricao.ItemData(cmbSubgrupo_Restricao.NewIndex) = vObjOracle("COD_subGRUPO")
            vObjOracle.MoveNext
        Wend
   End If
 End If
End Sub

Private Sub cmbSubgrupo_Restricao_LostFocus()
  If cmbSubgrupo_Restricao.ListIndex <> -1 Then
    lngCod_Subgrupo = cmbSubgrupo_Restricao.ItemData(cmbSubgrupo_Restricao.ListIndex)
  End If
End Sub

Private Sub cmdGravar_Click()
  strOriginal = UCase(Text1)
  Unload Me
End Sub

Private Sub cmdVoltar_Click()
  grdRestricao_Cad.Visible = False
  Unload Me
End Sub

Private Sub Dt_Fim_KeyPress(KeyAscii As Integer)
   DATA KeyAscii, txtDt_Inicio
End Sub

Private Sub Form_Load()
    strTipo_acao = "I"
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_DESCERRO"
    vBanco.Parameters.Add "PM_DESCERRO", 0, 2

    
    vSql = "PRODUCAO.PCK_CAD985.PR_CON_RESTRICAO(:PM_CURSOR1,:PM_DESCERRO, :PM_CODERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value


   If cmbGrupo_Restricao.ListCount = 0 Then
        While Not vObjOracle.EOF
            cmbGrupo_Restricao.AddItem vObjOracle("DESC_GRUPO")
            cmbGrupo_Restricao.ItemData(cmbGrupo_Restricao.NewIndex) = vObjOracle("COD_GRUPO")
            vObjOracle.MoveNext
        Wend
   End If
   
   Call Consulta_Restricao

End Sub

Private Sub Form_Terminate()
   grdRestricao_Cad.Visible = False
End Sub

Private Sub grdRestricao_Cad_DblClick()
    strTipo_acao = "A"
    grdRestricao_Cad.Col = 1
    lngCod_Grupo = IIf(grdRestricao_Cad.Text = "", 0, grdRestricao_Cad.Text)
    grdRestricao_Cad.Col = 2
    cmbGrupo_Restricao = grdRestricao_Cad.Text
    grdRestricao_Cad.Col = 3
    lngCod_Subgrupo = IIf(grdRestricao_Cad.Text = "", 0, grdRestricao_Cad.Text)
    grdRestricao_Cad.Col = 4
    cmbSubgrupo_Restricao = grdRestricao_Cad.Text
    grdRestricao_Cad.Col = 5
    txtDt_Inicio = grdRestricao_Cad.Text
    grdRestricao_Cad.Col = 6
    txtDt_Fim = grdRestricao_Cad.Text
    grdRestricao_Cad.Col = 7
    lngSequencia = grdRestricao_Cad.Text
    
End Sub



Private Sub sscmd_Insere_Click()
  
  If grdRestricao_Cad.Visible And _
     txtDt_Inicio = "" And txtDt_Fim = "" And _
     cmbGrupo_Restricao = "" And cmbSubgrupo_Restricao = "" Then
      MsgBox "Selecione um registro para fazer a altera��o ou informe os dados para fazer a inclus�o", vbInformation, "Aten��o"
      Exit Sub
  ElseIf txtDt_Inicio = "" And txtDt_Fim = "" And _
     cmbGrupo_Restricao = "" And cmbSubgrupo_Restricao = "" Then
      MsgBox "Selecione um registro para fazer a inclus�o", vbInformation, "Aten��o"
      Exit Sub
  ElseIf cmbGrupo_Restricao <> "" And cmbSubgrupo_Restricao = "" Then
       MsgBox "Selecione um subgrupo de complemento para fazer a inclus�o", vbInformation, "Aten��o"
      Exit Sub
  Else
    If grdRestricao_Cad.Visible = False Then
      strTipo_acao = "I"
    End If
      Call Insere_Restricao
  End If
  
End Sub

Private Sub sscmdExcluir_Click()
    If grdRestricao_Cad.Visible And _
     txtDt_Inicio = "" And txtDt_Fim = "" And _
     cmbGrupo_Restricao = "" And cmbSubgrupo_Restricao = "" Then
      MsgBox "Selecione o registro desejado para fazer a exclus�o", vbInformation, "Aten��o"
      Exit Sub
    Else
       strTipo_acao = "D"
      Call Exclui_Restricao
      
    End If
End Sub

Private Sub txtDt_Fim_KeyPress(KeyAscii As Integer)
   DATA KeyAscii, txtDt_Fim
End Sub

Private Sub txtDt_Fim_LostFocus()
  If txtDt_Fim <> "" And txtDt_Inicio <> "" Then
    If CDate(txtDt_Fim) < CDate(txtDt_Inicio) Then
      MsgBox "A data final de fabrica��o deve ser maior que a inicial", vbInformation, "Aten��o"
      txtDt_Fim = ""
      txtDt_Fim.SetFocus
    End If
  End If
End Sub

Private Sub txtDt_Inicio_KeyPress(KeyAscii As Integer)
  DATA KeyAscii, txtDt_Inicio
End Sub
