VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmVeiculo 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Veiculo"
   ClientHeight    =   5655
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11835
   Icon            =   "frmVeiculo.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   377
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   789
   Begin VB.Frame fra 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1680
      Left            =   45
      TabIndex        =   4
      Top             =   945
      Width           =   6450
      Begin VB.ComboBox cmbSituacao 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1125
         Style           =   2  'Dropdown List
         TabIndex        =   13
         Top             =   1170
         Width           =   2220
      End
      Begin VB.ComboBox cmbMontadora 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1125
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   12
         Top             =   270
         Width           =   2175
      End
      Begin VB.TextBox txtVeiculo 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1125
         MaxLength       =   30
         TabIndex        =   9
         Top             =   720
         Width           =   3255
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Situa��o:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   315
         TabIndex        =   11
         Top             =   1260
         Width           =   750
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Veiculo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   405
         TabIndex        =   10
         Top             =   810
         Width           =   645
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Montadora:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   135
         TabIndex        =   5
         Top             =   360
         Width           =   930
      End
   End
   Begin VB.TextBox txtCodVeiculo 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3645
      MaxLength       =   10
      TabIndex        =   3
      Top             =   240
      Visible         =   0   'False
      Width           =   780
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   5325
      Width           =   11835
      _ExtentX        =   20876
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   20373
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      Top             =   810
      Width           =   6450
      _ExtentX        =   11377
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmVeiculo.frx":23D2
      PICN            =   "frmVeiculo.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdGravar 
      Height          =   690
      Left            =   840
      TabIndex        =   6
      TabStop         =   0   'False
      ToolTipText     =   "Gravar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmVeiculo.frx":30C8
      PICN            =   "frmVeiculo.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSGrid.Grid grd 
      Height          =   2580
      Left            =   45
      TabIndex        =   7
      Top             =   2700
      Width           =   11730
      _Version        =   65536
      _ExtentX        =   20690
      _ExtentY        =   4551
      _StockProps     =   77
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      HighLight       =   0   'False
   End
   Begin Bot�o.cmd cmdCancel 
      Height          =   690
      Left            =   1560
      TabIndex        =   8
      TabStop         =   0   'False
      ToolTipText     =   "Cancelar"
      Top             =   45
      Visible         =   0   'False
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmVeiculo.frx":3DBE
      PICN            =   "frmVeiculo.frx":3DDA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmVeiculo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmbMontadora_Click()

    'BuscaVeiculo IIf(cmbMontadora.Text = "", 0, Left(cmbMontadora.Text, 2)), 0, txtVeiculo.Text, ""
    If cmbMontadora.Text = "" Then
        montadora = 0
    Else
        montadora = cmbMontadora.ItemData(cmbMontadora.ListIndex)
    End If
    BuscaVeiculo CInt(montadora), 0, "", 0
    If vObjOracle.EOF Then
        vVB_Generica_001.LimpaGridComTitulo grd
    Else
        vVB_Generica_001.CarregaGridTabela grd, vObjOracle, 5, "1;2;3;4"
    End If
End Sub

Private Sub cmdCancel_Click()

    txtCodVeiculo.Text = ""
    txtVeiculo.Text = ""
    cmbSituacao.Text = "0 - Ativo"
    cmbMontadora.Locked = False
    cmdCancel.Visible = False

End Sub

Private Sub cmdGravar_Click()

    If txtVeiculo.Text = "" Then
        MsgBox "Digite o nome do veiculo", , "Aten��o"
        txtVeiculo.SetFocus
        Exit Sub
    End If
    
    If txtCodVeiculo.Text <> "" Then
        BuscaAplicacaoVeiculo 0, cmbMontadora.ItemData(cmbMontadora.ListIndex), txtCodVeiculo.Text, 0, 0
        If Not vObjOracle.EOF And Left(cmbSituacao.Text, 1) = 9 Then
            MsgBox "Este veiculo n�o pode ser desativado, pois est� sendo utilizado no relacionamento VEICULO X APLICACAO", , "Aten��o"
            Exit Sub
        End If
        msg = GravarVeiculo("A", cmbMontadora.ItemData(cmbMontadora.ListIndex), txtCodVeiculo.Text, txtVeiculo.Text, Left(cmbSituacao.Text, 1))
        If msg <> "" Then
            MsgBox "Erro: " & msg
            Exit Sub
        Else
            MsgBox "Veiculo alterado com sucesso"
        End If
    Else
        BuscaVeiculo 0, cmbMontadora.ItemData(cmbMontadora.ListIndex), txtVeiculo.Text, ""
    
        If vObjOracle.EOF Then
            msg = GravarVeiculo("I", cmbMontadora.ItemData(cmbMontadora.ListIndex), 0, txtVeiculo.Text, Left(cmbSituacao.Text, 1))
            If msg <> "" Then
                MsgBox "Erro: " & msg
                Exit Sub
            Else
                MsgBox "Veiculo incluido com sucesso"
            End If
        Else
            MsgBox "Este veiculo j� esta cadastrado para esta montadora", , "Aten��o"
            Exit Sub
        End If
    End If
    
    cmdCancel_Click
    cmbMontadora_Click

End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    
    Me.Top = 0
    Me.Left = 0
    
    BuscaMontadora 0, ""
    While Not vObjOracle.EOF
        cmbMontadora.AddItem vObjOracle("MONTADORA")
        cmbMontadora.ItemData(cmbMontadora.NewIndex) = vObjOracle("C�D.")
        vObjOracle.MoveNext
    Wend
    'vVB_Generica_001.PreencheComboList vObjOracle, cmbMontadora, "C�D.", "montadora"
    
    cmbSituacao.AddItem "0 - Ativo"
    cmbSituacao.AddItem "9 - Desativado"
    
    cmbSituacao.Text = "0 - Ativo"
    
    cmbMontadora_Click
    
End Sub

Private Sub grd_DblClick()
    grd.Col = 1
    If grd.Text <> "" Then
    
        cmbMontadora.Locked = True
        grd.Col = 1
        montadora = grd.Text
        grd.Col = 2
        codVeiculo = grd.Text
        grd.Col = 3
        Veiculo = grd.Text
        grd.Col = 4
        Situacao = grd.Text
        
        cmbMontadora.Text = montadora
        txtCodVeiculo.Text = codVeiculo
        txtVeiculo.Text = Veiculo
        cmbSituacao.Text = Situacao
        
        cmdCancel.Visible = True
    End If
End Sub


Private Function GravarVeiculo(pACT As String, codMontadora As Integer, codVeiculo As Integer, descVeiculo As String, Situacao As Integer) As String

    vBanco.Parameters.Remove "PM_ACT"
    vBanco.Parameters.Add "PM_ACT", pACT, 1
    
    vBanco.Parameters.Remove "PM_CODMONTA"
    vBanco.Parameters.Add "PM_CODMONTA", codMontadora, 1
    
    vBanco.Parameters.Remove "PM_CODVEIC"
    vBanco.Parameters.Add "PM_CODVEIC", codVeiculo, 1
    
    vBanco.Parameters.Remove "PM_DESCVEIC"
    vBanco.Parameters.Add "PM_DESCVEIC", descVeiculo, 1
    
    vBanco.Parameters.Remove "PM_SITUACAO"
    vBanco.Parameters.Add "PM_SITUACAO", Situacao, 1
  
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "PRODUCAO.PCK_CAD985.PR_ACT_VEICULO(:PM_ACT,:PM_CODMONTA,:PM_CODVEIC,:PM_DESCVEIC,:PM_SITUACAO,:PM_CODERRO,:PM_TXTERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
    
    If vBanco.Parameters("PM_CODERRO").Value <> 0 Then
        GravarVeiculo = vBanco.Parameters("PM_CODERRO").Value & " - " & vBanco.Parameters("PM_TXTERRO").Value
    Else
        GravarVeiculo = ""
    End If

End Function
