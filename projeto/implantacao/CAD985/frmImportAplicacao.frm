VERSION 5.00
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "msmask32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form frmImportAplicacao 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Importa��o de Aplica��o x Veiculo"
   ClientHeight    =   7095
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11910
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   473
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   794
   Begin VB.Frame Frame1 
      Caption         =   "Dados do Arquivo"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   4470
      Left            =   45
      TabIndex        =   6
      Top             =   2250
      Width           =   11640
      Begin Bot�o.cmd cmdImp 
         Height          =   420
         Left            =   10080
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   3720
         Width           =   1140
         _ExtentX        =   2011
         _ExtentY        =   741
         BTYPE           =   3
         TX              =   "Importar"
         ENAB            =   0   'False
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmImportAplicacao.frx":0000
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MSFlexGridLib.MSFlexGrid mfgArq 
         Height          =   3120
         Left            =   135
         TabIndex        =   8
         Top             =   360
         Width           =   11370
         _ExtentX        =   20055
         _ExtentY        =   5503
         _Version        =   393216
         Cols            =   9
         BackColorBkg    =   -2147483633
         AllowUserResizing=   1
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblMsg2 
         BeginProperty Font 
            Name            =   "Arial Black"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   420
         Left            =   180
         TabIndex        =   10
         Top             =   3960
         Width           =   5325
      End
      Begin VB.Label lbMsg 
         AutoSize        =   -1  'True
         BeginProperty Font 
            Name            =   "Arial Black"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000FFFF&
         Height          =   300
         Left            =   180
         TabIndex        =   9
         Top             =   3555
         Width           =   75
      End
   End
   Begin VB.Frame fra 
      Caption         =   "Arquivo para Leitura"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1230
      Left            =   45
      TabIndex        =   3
      Top             =   945
      Width           =   7080
      Begin Bot�o.cmd cmd1 
         Height          =   690
         Left            =   135
         TabIndex        =   4
         TabStop         =   0   'False
         ToolTipText     =   "Abrir"
         Top             =   360
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmImportAplicacao.frx":001C
         PICN            =   "frmImportAplicacao.frx":0038
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MSMask.MaskEdBox txtArquivo 
         Height          =   330
         Left            =   1035
         TabIndex        =   5
         Top             =   585
         Width           =   5730
         _ExtentX        =   10107
         _ExtentY        =   582
         _Version        =   393216
         Appearance      =   0
         MaxLength       =   14
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   "###,###,##0.00"
         PromptChar      =   "�"
      End
   End
   Begin MSComDlg.CommonDialog CMMDIALOG 
      Left            =   6660
      Top             =   180
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   6765
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   20505
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      Top             =   810
      Width           =   7080
      _ExtentX        =   12488
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmImportAplicacao.frx":0D12
      PICN            =   "frmImportAplicacao.frx":0D2E
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmImportAplicacao"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public FLERRO As Integer

Private Sub cmd1_Click()
On Error GoTo Trata_erro

    Dim xl As New Excel.Application
    'Dim xlB As Excel.Workbook
    Dim Fim, ErroC As Integer
    Dim Celula As String
    
    lblMsg2.Caption = ""
    lbMsg.Caption = ""

    CMMDIALOG.ShowOpen
    
    strCaminho = CMMDIALOG.FileName
    
    If InStr(1, strCaminho, ".xls") = 0 Then
        MsgBox "O arquivo de importa��o deve ser em Excel", , "Aten��o!"
        Exit Sub
    End If
    
    txtArquivo.Text = strCaminho
    
    Screen.MousePointer = 11
    
    'Verifica se foi indicado o diret�rio e nome do arquivo a ser lido
    If strCaminho = "" Then
        MsgBox "Indique o arquivo a ser aberto", , MSG_TIT
        Screen.MousePointer = 0
        Exit Sub
    End If
    

    xl.Workbooks.Add strCaminho
    
    'xlB.Activate
    'xl.Visible = True
    'xl.Visible = True
    lbMsg.Caption = ""
    
    If xl.Cells(1, 1).Value = "" Then
        MsgBox "Primeira linha do Arquivo n�o poder estar em branco", vbInformation, "Aten��o"
        Screen.MousePointer = 0
        Exit Sub
    ElseIf xl.Cells(2, 1).Value = "" Then
        
        Fim = 1
    
    Else
        
        xl.Cells(1, 1).Select
        xl.ActiveCell.End(xlDown).Select

        Fim = xl.ActiveCell.Row
        
    End If
    
    xl.Cells(1, 1).Select
    
    FLERRO = 0
    
    For i = 1 To Fim
        ErroC = 0
        For j = 1 To 8
        
            Celula = xl.Cells(i, j).Value
            Celula = Trim(UCase(Celula))
            
            mfgArq.Rows = i + 1
            
            Select Case j
                Case 1
                    If Not IsNumeric(Celula) Then
                        ErroC = 1
                    Else
                        BuscaMontadora CInt(Celula), ""
                        If vObjOracle.EOF Then
                            ErroC = 1
                        End If
                    End If
                    montadora = Celula
                Case 2
                    If Not IsNumeric(Celula) Then
                        ErroC = 1
                    Else
                        BuscaVeiculo CInt(montadora), CInt(Celula), "", ""
                        If vObjOracle.EOF Then
                            ErroC = 1
                        End If
                    End If
                    Veiculo = Celula
                Case 3
                    If Not IsNumeric(Celula) Then
                        ErroC = 1
                    Else
                        BuscaModelo CInt(Celula), "", ""
                        If vObjOracle.EOF Then
                            ErroC = 1
                        End If
                    End If
                Case 4
                    If Not IsNumeric(Celula) Then
                        ErroC = 1
                    Else
                        BuscaMotor CInt(Celula), "", ""
                        If vObjOracle.EOF Then
                            ErroC = 1
                        End If
                    End If
                Case 5
                    If InStr(1, "P,L,U,M,A", Celula) = 0 Then
                        ErroC = 1
                    End If
                Case 5
                    If InStr(1, "0,9", Celula) = 0 Then
                        ErroC = 1
                    End If
            End Select
            
            With mfgArq
                '.Rows = I + 1
                
                .Row = i
                .Col = j
                .Text = Celula
            End With
        
        Next
        
        If ErroC = 1 Then
            FLERRO = 1
            lbMsg.Caption = "* Registros com problemas n�o ser�o importados"
            
            With mfgArq
                For X = 1 To 8
                    .Row = i
                    .Col = X
                    .CellBackColor = vbYellow
                Next
            End With
        End If
    Next
    
    xl.Workbooks.Close
    
    Set xl = Nothing
    Screen.MousePointer = vbNormal
    
    cmdImp.Enabled = True
    
Trata_erro:
    If Err.Number = -2147417851 Then
        Resume Next
    ElseIf Err.Number <> 0 Then
        MsgBox Err.Number & vbCrLf & Err.Description
        Screen.MousePointer = vbNormal
    End If
End Sub

Private Sub cmdImp_Click()

    If FLERRO = 1 Then
        If MsgBox("Os registro marcados em AMARELO, n�o ser�o inseridos. Deseja continuar?", vbYesNo) = vbNo Then
             Exit Sub
        End If
    End If
    Call SUBGRAVA

End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    
    Me.Top = 0
    Me.Left = 0
    
    mfgArq.ColWidth(0) = 200
    
    With mfgArq
        .Row = 0
        
        .Col = 1
        .Text = "COD. MONTADORA"
        .ColWidth(.Col) = 1600
        
        .Col = 2
        .Text = "COD. VEICULO"
        .ColWidth(.Col) = 1200
        
        .Col = 3
        .Text = "COD. MODELO"
        .ColWidth(.Col) = 1200
        
        .Col = 4
        .Text = "COD. MOTOR"
        .ColWidth(.Col) = 1600
        
        .Col = 5
        .Text = "COD. CATEGORIA"
        .ColWidth(.Col) = 1600
        
        .Col = 6
        .Text = "SITUA��O"
        .ColWidth(.Col) = 1600
        
        .Col = 7
        .Text = "DT.INI.MOD"
        .ColWidth(.Col) = 1600
        
        .Col = 8
        .Text = "DT.FIM MOD"
        .ColWidth(.Col) = 1600
        
    End With
    
End Sub


Sub SUBGRAVA()

Dim ArrDados(8) As String
Dim vCor As String

    Screen.MousePointer = 0

    For i = 1 To mfgArq.Rows - 1
    
        With mfgArq
            .Row = i
            .Col = 1
            vCor = .CellBackColor
        End With
        
        If vCor <> 65535 Then
            For j = 1 To 8
            
                With mfgArq
                    .Row = i
                    .Col = j
                    ArrDados(j) = .Text
                End With
                
            Next
                
            BuscaAplicacaoVeiculo 0, CInt(ArrDados(1)), CInt(ArrDados(2)), CInt(ArrDados(3)), CInt(ArrDados(4))
            
            If vObjOracle.EOF Then
                msg = GravarAplicacao("I", 0, CInt(ArrDados(1)), CInt(ArrDados(2)), CInt(ArrDados(3)), CInt(ArrDados(4)), ArrDados(5), CInt(ArrDados(6)), ArrDados(7), ArrDados(8))
                
                If msg <> "" Then
                    MsgBox "Erro:" & msg & ".Ligue para o Depto de Sistemas"
                    Screen.MousePointer = 0
                    Exit Sub
                End If
            Else
              msg = GravarAplicacao("A", vObjOracle!COD_APLICACAO, CInt(ArrDados(1)), CInt(ArrDados(2)), CInt(ArrDados(3)), CInt(ArrDados(4)), ArrDados(5), CInt(ArrDados(6)), ArrDados(7), ArrDados(8))
              If msg <> "" Then
                  MsgBox "Erro:" & msg & ".Ligue para o Depto de Sistemas"
                  Screen.MousePointer = 0
                  Exit Sub
              End If
              '  With mfgArq
              '      For X = 1 To 8
              '          .Row = i
              '          .Col = X
              '          .CellBackColor = vbRed
              '      Next
              '  End With
                
              '  lblMsg2.Caption = "* Registro j� cadastrado"
            End If
        End If
    Next
    
    MsgBox "Inclus�o/Altera��o OK ", vbInformation, "Aten��o"
    Screen.MousePointer = 0
    cmdImp.Enabled = False
    
    Exit Sub

End Sub

