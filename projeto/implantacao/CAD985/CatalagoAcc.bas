Attribute VB_Name = "CatalagoAcc"
Option Explicit

Dim ArqAux      As Database
Dim ArqLoja     As Database

Global dbConn As Database



Public Sub InicioGeracaoCat()
    
    Set dbConn = OpenDatabase("", False, False, "ODBC;DSN=PRODUCAO;")
    'Set dbConn = OpenDatabase("", False, False, "ODBC;DSN=SDPK-DEV;")
    
   
    If VerificaSessaoOracle Then
        mdiCAD985.Enabled = False
        mdiCAD985.SSPanel1.Enabled = False
        mdiCAD985.sspPainelAcc.Visible = True
        mdiCAD985.PrgBarCatACC.Visible = True
        
        GeraCat
        MsgBox ("Cat�logo criado com sucesso.")
        
        mdiCAD985.PrgBarCatACC.Visible = False
        mdiCAD985.sspPainelAcc.Visible = False
        mdiCAD985.SSPanel1.Enabled = True
        mdiCAD985.Enabled = True
        
    End If
        
End Sub
Private Function VerificaSessaoOracle() As Boolean

    Dim rs          As Object
    Dim v_dt
    Dim SQL         As String
    Dim AuxUsu()    As String
    Dim usuario     As String
    Dim X, i        As Integer
    Dim aux         As Integer
    Dim achou       As Boolean
    Dim DataVersao  As String
    Dim ContAux     As Long
    
On Error GoTo TrataErro:
    
    
    VerificaSessaoOracle = True
    
    If MsgBox("Deseja realmente fazer a gera��o do catalogo? Esta opera��o ser� monitorada!", vbYesNo + vbCritical + vbExclamation, "Gera��o de Cat�logo") = vbNo Then
        VerificaSessaoOracle = False
        Exit Function
    Else
         
        
        usuario = ""
        X = 1
        achou = False
        ReDim AuxUsu(0)
        
        SQL = "SELECT machine, program, username, status" & _
              " FROM v$session" & _
              " WHERE UPPER(program) like '%CAD985%'"
        
        Set rs = dbConn.OpenRecordset(SQL, dbOpenSnapshot, 64)
        
        If Not rs.EOF Then
              rs.MoveLast
              rs.MoveFirst
              aux = rs.RecordCount
                    
              Do While Not rs.EOF
                 If X = 1 Then
                     'Usuario = Trim(rs!machine)
                     X = X + 1
                     ReDim Preserve AuxUsu(UBound(AuxUsu) + 1)
                     AuxUsu(UBound(AuxUsu)) = Trim(rs!machine)
                 Else
                     For i = 1 To UBound(AuxUsu)
                         If Trim(AuxUsu(i)) = Trim(rs!machine) Then
                             achou = True
                             Exit For
                         Else
                             achou = False
                         End If
                     Next i
                     If achou = False Then
                         ReDim Preserve AuxUsu(UBound(AuxUsu) + 1)
                         AuxUsu(UBound(AuxUsu)) = Trim(rs!machine)
                     End If
                     'Usuario = Usuario & ", " & Trim(rs!machine)
                     X = X + 1
                 End If
                 rs.MoveNext
             Loop
             If UBound(AuxUsu) > 1 Then
                 For i = 1 To UBound(AuxUsu)
                     usuario = usuario & ", " & Trim(AuxUsu(i))
                 Next i
             ElseIf UBound(AuxUsu) = 1 Then
                 usuario = Trim(AuxUsu(1))
             End If
             
             If UBound(AuxUsu) > 1 Then
                 Screen.MousePointer = vbArrow
                 MsgBox "Os usu�rios: " & usuario & " est�o utilizando o sistema." & vbCr & _
                 "Favor solicitar que os mesmos saiam do sistema (exceto o seu computador) para poder efetuar a gera��o do Catalogo da Loja" & _
                 " e que s� retornem ap�s a finaliza��o deste processo.", vbOKOnly + vbExclamation, "Aten��o"
                 VerificaSessaoOracle = False
                 Exit Function
             End If
         End If
    End If
    Exit Function
    
TrataErro:
Screen.MousePointer = vbArrow
MsgBox Err.Number & " - " & Err.Description, vbInformation, "Aten��o"
    

End Function


Private Sub GeraCat()

Dim v_dt
Dim DataLoja    As String
Dim DataTeste   As String
Dim contador    As Integer
Dim origem      As String
Dim destino     As String


Dim rs          As Recordset
Dim Caminho     As String


On Error GoTo TrataErro:
    
    
    mdiCAD985.PrgBarCatACC.Min = 1
    mdiCAD985.PrgBarCatACC.Max = 22
    mdiCAD985.PrgBarCatACC.Value = mdiCAD985.PrgBarCatACC.Min
    
    contador = 1
    mdiCAD985.PrgBarCatACC.Value = contador
    
    
    mdiCAD985.lblCatACC.Caption = "Gerando catalogo.mdb"
    mdiCAD985.lblCatACC.Refresh
    
    DataLoja = ""
    DataTeste = ""
    
    DoEvents
    
    v_dt = ""
    
    'William Leite
    'VXAVIER - TI-2820 - 22/07/2015 - Altera��o do caminho do arquivo modeloACC.mdb
    Caminho = "F:\sistemas\catalogo\"
    'Caminho = "C:\Fontes\catalogo\"
    
    'Reparar e compactar a base de dados modelo
     RepairDatabase (Caminho & "aplicaca\modeloACC.mdb")
     CompactDatabase Caminho & "aplicaca\modeloACC.mdb", Caminho & "aplicaca\cmodeloACC.mdb"

     Kill Caminho & "aplicaca\modeloACC.mdb"
     Kill Caminho & "aplicaca\cmodeloACC.ldb"
     'Renomear arquivo compactado
     Name Caminho & "aplicaca\cmodeloACC.mdb" As Caminho & "aplicaca\modeloACC.mdb"

     origem = Caminho & "aplicaca\ModeloACC.mdb"
     destino = "c:\catalogo\Cat.mdb"
     

     If Dir("c:\catalogo", 16) = "" Then
        MkDir "c:\catalogo"
     End If

    If Dir("c:\catalogo\Cat.mdb") <> "" Then Kill "c:\catalogo\Cat.mdb"
    
    FileCopy origem, destino
    DoEvents
    
    Set ArqLoja = OpenDatabase("c:\catalogo\Cat.mdb")
    

    contador = contador + 1
    mdiCAD985.PrgBarCatACC.Value = contador
    mdiCAD985.lblCatACC.Caption = "Gravando Data Vers�o"
    mdiCAD985.lblCatACC.Refresh
    Call Grava_DataVersao
    
    contador = contador + 1
    mdiCAD985.PrgBarCatACC.Value = contador
    mdiCAD985.lblCatACC.Caption = "Gerando Tp Posi��o Item"
    mdiCAD985.lblCatACC.Refresh
    Call Copia_Tp_posicao_item
    
    
    contador = contador + 1
    mdiCAD985.PrgBarCatACC.Value = contador
    mdiCAD985.lblCatACC.Caption = "Gerando Tp Item"
    mdiCAD985.lblCatACC.Refresh
    Call Copia_Tp_ITEM
    
    contador = contador + 1
    mdiCAD985.PrgBarCatACC.Value = contador
    mdiCAD985.lblCatACC.Caption = "Tabela Tp Veiculo"
    mdiCAD985.lblCatACC.Refresh
    Call Copia_Tp_Veiculo
    
    contador = contador + 1
    mdiCAD985.PrgBarCatACC.Value = contador
    mdiCAD985.lblCatACC.Caption = "Tabela Tp Caracter Veiculos"
    mdiCAD985.lblCatACC.Refresh
    Call Copia_TP_CARACTER_VEICULOS
    
    contador = contador + 1
    mdiCAD985.PrgBarCatACC.Value = contador
    mdiCAD985.lblCatACC.Caption = "Tabela Tp Motor Veiculo"
    mdiCAD985.lblCatACC.Refresh
    Call Copia_TP_MOTOR_VEICULOS
    
    contador = contador + 1
    mdiCAD985.PrgBarCatACC.Value = contador
    mdiCAD985.lblCatACC.Caption = "Tabela Tp Op��es Veiculos"
    mdiCAD985.lblCatACC.Refresh
    Call Copia_TP_OPCOES_VEICULOS
    
    contador = contador + 1
    mdiCAD985.PrgBarCatACC.Value = contador
    mdiCAD985.lblCatACC.Caption = "Tabela Tp Seq Apresenta��o"
    mdiCAD985.lblCatACC.Refresh
    Call Copia_Tp_seq_apresentacao
    
    contador = contador + 1
    mdiCAD985.PrgBarCatACC.Value = contador
    mdiCAD985.lblCatACC.Caption = "Tabela Tp Modelo Veiculos"
    mdiCAD985.lblCatACC.Refresh
    Call Copia_TP_MODELO_VEICULOS
    
    contador = contador + 1
    mdiCAD985.PrgBarCatACC.Value = contador
    mdiCAD985.lblCatACC.Caption = "Tabela Modelos Veiculos"
    mdiCAD985.lblCatACC.Refresh
    Call Copia_MODELOS_VEICULOS
    
    contador = contador + 1
    mdiCAD985.PrgBarCatACC.Value = contador
    mdiCAD985.lblCatACC.Caption = "Tabela Montadora Veiculos"
    mdiCAD985.lblCatACC.Refresh
    Call Copia_MONTADORA_VEICULOS
    
    contador = contador + 1
    mdiCAD985.PrgBarCatACC.Value = contador
    mdiCAD985.lblCatACC.Caption = "Tabela Marcas Veiculos"
    mdiCAD985.lblCatACC.Refresh
    Call Copia_MARCAS_VEICULOS
    
    contador = contador + 1
    mdiCAD985.PrgBarCatACC.Value = contador
    mdiCAD985.lblCatACC.Caption = "Tabela Item Veiculos"
    mdiCAD985.lblCatACC.Refresh
    Call Copia_ITEM_VEICULOS
    
    contador = contador + 1
    mdiCAD985.PrgBarCatACC.Value = contador
    mdiCAD985.lblCatACC.Caption = "Tabela Item Veiculo Restri��o"
    mdiCAD985.lblCatACC.Refresh
    Call Copia_ITEM_VEICULO_RESTRICAO
    
    contador = contador + 1
    mdiCAD985.PrgBarCatACC.Value = contador
    mdiCAD985.lblCatACC.Caption = "Tabela Item"
    mdiCAD985.lblCatACC.Refresh
    Call Copia_Item
    
    contador = contador + 1
    mdiCAD985.PrgBarCatACC.Value = contador
    mdiCAD985.lblCatACC.Caption = "Tabela Item EAN"
    mdiCAD985.lblCatACC.Refresh
    Call Copia_Item_EAN
    
    
    contador = contador + 1
    mdiCAD985.PrgBarCatACC.Value = contador
    mdiCAD985.lblCatACC.Caption = "Tabela Item Substitui��o"
    mdiCAD985.lblCatACC.Refresh
    Call Copia_ITEM_SUBSTITUICAO
    
    'Victor Nardi Vilella - CI&T - 12/03/2014
    'SMS1347 - Exportando informa��es do fornecedor - ACC
    contador = contador + 1
    mdiCAD985.PrgBarCatACC.Value = contador
    mdiCAD985.lblCatACC.Caption = "Tabela Fornecedor"
    mdiCAD985.lblCatACC.Refresh
    Call Copia_Fornecedor
    
    'William Leite - Plano Catalogo ACC
    contador = contador + 1
    mdiCAD985.PrgBarCatACC.Value = contador
    mdiCAD985.lblCatACC.Caption = "Tabela Equival�ncia"
    mdiCAD985.lblCatACC.Refresh
    Call Copia_R_DPK_EQUIV
    
    contador = contador + 1
    mdiCAD985.PrgBarCatACC.Value = contador
    mdiCAD985.lblCatACC.Caption = "Tabela Fotos"
    mdiCAD985.lblCatACC.Refresh
    Call Copia_R_DPK_FOTO
    dbConn.Close
    ArqLoja.Close
    
    DoEvents
    
    If Dir("c:\catalogo\cat.mdb") <> "" Then
        If Dir("c:\catalogo\catalogo.mdb") <> "" Then
            Kill "c:\catalogo\catalogo.mdb"
        End If
        Name "c:\catalogo\cat.mdb" As "c:\catalogo\catalogo.mdb"
    End If
    
    DoEvents
    
    
    Exit Sub
    
TrataErro:
    Screen.MousePointer = vbArrow
    MsgBox Err.Number & " - " & Err.Description, vbInformation, "Aten��o"
    If Dir(destino) <> "" Then Kill destino

End Sub
Private Sub Copia_Tp_seq_apresentacao()

Dim strsql As String
Dim StrAux As String
Dim vErro As String
Dim rs As Recordset


On Error GoTo TrataErro

    'TP_SEQ_APRESENTACAO
    
     strsql = " SELECT * FROM CAT_TP_SEQ_APRESENTACAO"
    
    StrAux = "INSERT INTO TP_SEQ_APRESENTACAO IN 'C:\CATALOGO\CAT.MDB' " & strsql
    
    dbConn.Execute StrAux
    
    
    
    Exit Sub

TrataErro:
Screen.MousePointer = vbArrow
MsgBox Err.Number & " - " & Err.Description, vbInformation, "Aten��o"

End Sub

Private Sub Copia_Tp_ITEM()

Dim strsql As String
Dim StrAux As String
Dim vErro As String
Dim rs As Recordset
Dim strDATE As String


On Error GoTo TrataErro

    'Tp_ITEM
    
    If Len(Date) = 8 Then
        
        strDATE = "'" & Mid(Date, 1, 2) & "/" & Mid(Date, 4, 2) & "/" & Mid(Date, 7, 2) & "'"
                
    ElseIf Len(Date) = 10 Then
 
        strDATE = "'" & Mid(Date, 1, 2) & "/" & Mid(Date, 4, 2) & "/" & Mid(Date, 9, 2) & "'"
    End If
    
    StrAux = "INSERT INTO TP_ITEM VALUES('RV','REVENDA'," & strDATE & ")"
    
    ArqLoja.Execute StrAux
    
    
    
    Exit Sub

TrataErro:
Screen.MousePointer = vbArrow
MsgBox Err.Number & " - " & Err.Description, vbInformation, "Aten��o"

End Sub

Private Sub Copia_Tp_posicao_item()

Dim strsql As String
Dim StrAux As String
Dim vErro As String
Dim rs As Recordset
Dim strDATE As String


On Error GoTo TrataErro

    'tp_posicao_item
    
    If Len(Date) = 8 Then
        
        strDATE = "'" & Mid(Date, 1, 2) & "/" & Mid(Date, 4, 2) & "/" & Mid(Date, 7, 2) & "'"
                
    ElseIf Len(Date) = 10 Then
 
        strDATE = "'" & Mid(Date, 1, 2) & "/" & Mid(Date, 4, 2) & "/" & Mid(Date, 9, 2) & "'"
    End If
    
    StrAux = "insert into tp_posicao_item values ('0','NAO INFORMADO'," & strDATE & ")"
    
    ArqLoja.Execute StrAux
    
    
    
    Exit Sub

TrataErro:
Screen.MousePointer = vbArrow
MsgBox Err.Number & " - " & Err.Description, vbInformation, "Aten��o"

End Sub
Private Sub Copia_Tp_Veiculo()

Dim strsql As String
Dim StrAux As String
Dim vErro As String
Dim rs As Recordset
Dim strDATE As String


On Error GoTo TrataErro

    'Tp_Veiculo
    
    If Len(Date) = 8 Then
        
        strDATE = "'" & Mid(Date, 1, 2) & "/" & Mid(Date, 4, 2) & "/" & Mid(Date, 7, 2) & "'"
                
    ElseIf Len(Date) = 10 Then
 
        strDATE = "'" & Mid(Date, 1, 2) & "/" & Mid(Date, 4, 2) & "/" & Mid(Date, 9, 2) & "'"
    End If
    
    StrAux = "INSERT INTO TP_VEICULO VALUES(0,'NAO INFORMADO'," & strDATE & ")"
    
    ArqLoja.Execute StrAux
    
    
    
    Exit Sub

TrataErro:
Screen.MousePointer = vbArrow
MsgBox Err.Number & " - " & Err.Description, vbInformation, "Aten��o"

End Sub
Private Sub Copia_TP_OPCOES_VEICULOS()

Dim strsql As String
Dim StrAux As String
Dim vErro As String
Dim rs As Recordset


On Error GoTo TrataErro

    'TP_SEQ_APRESENTACAO
    
     strsql = " SELECT * FROM CAT_TP_OPCOES_VEICULOS"
    
    StrAux = "INSERT INTO TP_OPCOES_VEICULOS IN 'C:\CATALOGO\CAT.MDB' " & strsql
    
    dbConn.Execute StrAux
    
    
    
    Exit Sub

TrataErro:
Screen.MousePointer = vbArrow
MsgBox Err.Number & " - " & Err.Description, vbInformation, "Aten��o"

End Sub

Private Sub Copia_MARCAS_VEICULOS()

Dim strsql As String
Dim StrAux As String
Dim vErro As String
Dim rs As Recordset


On Error GoTo TrataErro

    'MARCAS_VEICULOS
    
     strsql = " SELECT * FROM CAT_MARCAS_VEICULOS"
    
    StrAux = "INSERT INTO MARCAS_VEICULOS IN 'C:\CATALOGO\CAT.MDB' " & strsql
    
    dbConn.Execute StrAux
    
    
    
    Exit Sub

TrataErro:
Screen.MousePointer = vbArrow
MsgBox Err.Number & " - " & Err.Description, vbInformation, "Aten��o"

End Sub

Private Sub Copia_MODELOS_VEICULOS()

Dim strsql As String
Dim StrAux As String
Dim vErro As String
Dim rs As Recordset


On Error GoTo TrataErro

    'MODELOS_VEICULOS
    
     strsql = " SELECT * FROM CAT_MODELOS_VEICULOS"
    
    StrAux = "INSERT INTO MODELOS_VEICULOS IN 'C:\CATALOGO\CAT.MDB' " & strsql
    
    dbConn.Execute StrAux
    
    
    
    Exit Sub

TrataErro:
Screen.MousePointer = vbArrow
MsgBox Err.Number & " - " & Err.Description, vbInformation, "Aten��o"

End Sub

Private Sub Copia_MONTADORA_VEICULOS()

Dim strsql As String
Dim StrAux As String
Dim vErro As String
Dim rs As Recordset


On Error GoTo TrataErro

    'MONTADORA_VEICULOS
    
     strsql = " SELECT * FROM CAT_MONTADORA_VEICULOS"
    
    StrAux = "INSERT INTO MONTADORA_VEICULOS IN 'C:\CATALOGO\CAT.MDB' " & strsql
    
    dbConn.Execute StrAux
    
    
    
    Exit Sub

TrataErro:
Screen.MousePointer = vbArrow
MsgBox Err.Number & " - " & Err.Description, vbInformation, "Aten��o"

End Sub

Private Sub Copia_TP_CARACTER_VEICULOS()

Dim strsql As String
Dim StrAux As String
Dim vErro As String
Dim rs As Recordset


On Error GoTo TrataErro

    'TP_CARACTER_VEICULOS
    
     strsql = " SELECT * FROM CAT_TP_CARACTER_VEICULOS"
    
    StrAux = "INSERT INTO TP_CARACTER_VEICULOS IN 'C:\CATALOGO\CAT.MDB' " & strsql
    
    dbConn.Execute StrAux
    
    
    
    Exit Sub

TrataErro:
Screen.MousePointer = vbArrow
MsgBox Err.Number & " - " & Err.Description, vbInformation, "Aten��o"

End Sub

Private Sub Copia_TP_MODELO_VEICULOS()

Dim strsql As String
Dim StrAux As String
Dim vErro As String
Dim rs As Recordset


On Error GoTo TrataErro

    'TP_MODELO_VEICULOS
    
     strsql = " SELECT * FROM CAT_TP_MODELO_VEICULOS"
    
    StrAux = "INSERT INTO TP_MODELO_VEICULOS IN 'C:\CATALOGO\CAT.MDB' " & strsql
    
    dbConn.Execute StrAux
    
    
    
    Exit Sub

TrataErro:
Screen.MousePointer = vbArrow
MsgBox Err.Number & " - " & Err.Description, vbInformation, "Aten��o"

End Sub

Private Sub Copia_TP_MOTOR_VEICULOS()

Dim strsql As String
Dim StrAux As String
Dim vErro As String
Dim rs As Recordset


On Error GoTo TrataErro

    'TP_MOTOR_VEICULOS
    
     strsql = " SELECT * FROM CAT_TP_MOTOR_VEICULOS"
    
    StrAux = "INSERT INTO TP_MOTOR_VEICULOS IN 'C:\CATALOGO\CAT.MDB' " & strsql
    
    dbConn.Execute StrAux
    
    
    
    Exit Sub

TrataErro:
Screen.MousePointer = vbArrow
MsgBox Err.Number & " - " & Err.Description, vbInformation, "Aten��o"

End Sub


Private Sub Copia_ITEM_SUBSTITUICAO()

Dim strsql As String
Dim StrAux As String
Dim vErro As String
Dim rs As Recordset


On Error GoTo TrataErro

    'ITEM SUBSTITUICAO
    
     strsql = " SELECT * FROM CAT_ITEM_SUBSTITUICAO"
    
    StrAux = "INSERT INTO ITEM_SUBSTITUICAO IN 'C:\CATALOGO\CAT.MDB' " & strsql
    
    dbConn.Execute StrAux
    
    
    
    Exit Sub

TrataErro:
Screen.MousePointer = vbArrow
MsgBox Err.Number & " - " & Err.Description, vbInformation, "Aten��o"

End Sub
Private Sub Copia_ITEM_VEICULOS()

Dim strsql As String
Dim StrAux As String
Dim vErro As String
Dim rs As Recordset


On Error GoTo TrataErro

    'ITEM_VEICULOS
    
     strsql = " SELECT * FROM CAT_ITEM_VEICULOS"
    
    StrAux = "INSERT INTO ITEM_VEICULOS IN 'C:\CATALOGO\CAT.MDB' " & strsql
    
    dbConn.Execute StrAux
    
    
    
    Exit Sub

TrataErro:
Screen.MousePointer = vbArrow
MsgBox Err.Number & " - " & Err.Description, vbInformation, "Aten��o"

End Sub
Private Sub Copia_ITEM_VEICULO_RESTRICAO()

Dim strsql As String
Dim StrAux As String
Dim vErro As String
Dim rs As Recordset


On Error GoTo TrataErro

    'ITEM_VEICULO_RESTRICAO
    
     strsql = " SELECT * FROM CAT_ITEM_VEICULO_RESTRICAO"
    
    StrAux = "INSERT INTO ITEM_VEICULOS_RESTRICAO IN 'C:\CATALOGO\CAT.MDB' " & strsql
    
    dbConn.Execute StrAux
    
    
    
    Exit Sub

TrataErro:
Screen.MousePointer = vbArrow
MsgBox Err.Number & " - " & Err.Description, vbInformation, "Aten��o"

End Sub

Private Sub Copia_Item()

Dim strsql As String
Dim StrAux As String
Dim vErro As String
Dim rs As Recordset


On Error GoTo TrataErro

    'ITEM
    
     strsql = " SELECT * FROM CAT_ITEM"
    
    StrAux = "INSERT INTO ITEM IN 'C:\CATALOGO\CAT.MDB' " & strsql
    
    dbConn.Execute StrAux
    
    Exit Sub

TrataErro:
Screen.MousePointer = vbArrow
MsgBox Err.Number & " - " & Err.Description, vbInformation, "Aten��o"

End Sub


Private Sub Copia_Item_EAN()

Dim strsql As String
Dim StrAux As String
Dim vErro As String
Dim rs As Recordset


On Error GoTo TrataErro

    'ITEM
    
     strsql = " SELECT * FROM CAT_ITEM_EAN"
    
    StrAux = "INSERT INTO ITEM_EAN IN 'C:\CATALOGO\CAT.MDB' " & strsql
    
    dbConn.Execute StrAux
    
    Exit Sub

TrataErro:
Screen.MousePointer = vbArrow
MsgBox Err.Number & " - " & Err.Description, vbInformation, "Aten��o"

End Sub

Private Sub Copia_Fornecedor()

Dim strsql As String
Dim StrAux As String
Dim vErro As String
Dim rs As Recordset

    'Victor Nardi Vilella - CI&T - 12/03/2014
    'SMS1347 - Exportando informa��es do fornecedor - ACC

On Error GoTo TrataErro

    'FORNECEDOR
    
     strsql = " SELECT * FROM CAT_FORNECEDOR"
    
    StrAux = "INSERT INTO FORNECEDOR IN 'C:\CATALOGO\CAT.MDB' " & strsql
    
    dbConn.Execute StrAux
    
    Exit Sub

TrataErro:
Screen.MousePointer = vbArrow
MsgBox Err.Number & " - " & Err.Description, vbInformation, "Aten��o"

End Sub

Public Sub Copia_R_DPK_EQUIV()

Dim strsql As String
Dim StrAux As String
Dim vErro As String
Dim rs As Recordset

On Error GoTo TrataErro

    'R_DPK_EQUIV
    
    strsql = " SELECT * FROM CAT_R_DPK_EQUIV"
    
    StrAux = "INSERT INTO R_DPK_EQUIV IN 'C:\CATALOGO\CAT.MDB' " & strsql
    
    dbConn.Execute StrAux
    
    Exit Sub

TrataErro:
Screen.MousePointer = vbArrow
MsgBox Err.Number & " - " & Err.Description, vbInformation, "Aten��o"

End Sub

Public Sub Copia_R_DPK_FOTO()

Dim strsql As String
Dim StrAux As String
Dim vErro As String
Dim rs As Recordset

On Error GoTo TrataErro

    'R_DPK_FOTO
    
    strsql = " SELECT * FROM CAT_R_DPK_FOTO"
    
    StrAux = "INSERT INTO R_DPK_FOTO IN 'C:\CATALOGO\CAT.MDB' " & strsql
    
    dbConn.Execute StrAux
    
    Exit Sub

TrataErro:
Screen.MousePointer = vbArrow
MsgBox Err.Number & " - " & Err.Description, vbInformation, "Aten��o"

End Sub

Private Sub Grava_DataVersao()

Dim StrAccess As String

On Error GoTo TrataErro
          
    If Len(Date) = 8 Then
        
        StrAccess = "INSERT INTO dataversao " & _
            "(dataversao) VALUES ( " & "'" & Mid(Date, 1, 2) & "/" & Mid(Date, 4, 2) & "/" & Mid(Date, 7, 2) & "'" & ")"
                
    ElseIf Len(Date) = 10 Then
 
        StrAccess = "INSERT INTO dataversao " & _
            "(dataversao) VALUES ( " & "'" & Mid(Date, 1, 2) & "/" & Mid(Date, 4, 2) & "/" & Mid(Date, 9, 2) & "'" & ")"
    End If
    
    ArqLoja.Execute StrAccess, dbFailOnError
   
    Exit Sub

TrataErro:
Screen.MousePointer = vbArrow
MsgBox Err.Number & " - " & Err.Description, vbInformation, "Aten��o"

End Sub

