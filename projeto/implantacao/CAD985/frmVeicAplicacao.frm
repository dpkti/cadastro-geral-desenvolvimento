VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmVeicAplicacao 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Relacionamento Ve�culo X Aplica��o"
   ClientHeight    =   7380
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11880
   Icon            =   "frmVeicAplicacao.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   492
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   792
   Begin VB.Frame fra 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   2250
      Left            =   45
      TabIndex        =   4
      Top             =   945
      Width           =   9915
      Begin VB.TextBox txtDt_Fim_Modelo 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   6600
         MaxLength       =   8
         TabIndex        =   24
         Top             =   1680
         Width           =   1200
      End
      Begin VB.TextBox txtDt_Inicio_Modelo 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1800
         MaxLength       =   8
         TabIndex        =   23
         Top             =   1680
         Width           =   1200
      End
      Begin VB.ComboBox cmbCategoria 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1080
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   18
         Top             =   1215
         Width           =   2805
      End
      Begin VB.ComboBox cmbMontadora 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1080
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   14
         Top             =   315
         Width           =   2805
      End
      Begin VB.ComboBox cmbMotor 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   5940
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   13
         Top             =   765
         Width           =   3840
      End
      Begin VB.ComboBox cmbModelo 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1080
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   12
         Top             =   765
         Width           =   3840
      End
      Begin VB.ComboBox cmbVeiculo 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   5940
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   11
         Top             =   315
         Width           =   3840
      End
      Begin VB.ComboBox cmbSituacao 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   5940
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   1215
         Width           =   2805
      End
      Begin VB.Label Label8 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Final Fab.Modelo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   5160
         TabIndex        =   22
         Top             =   1800
         Width           =   1395
      End
      Begin VB.Label Label7 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Inicio Fab.Modelo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   240
         TabIndex        =   21
         Top             =   1800
         Width           =   1470
      End
      Begin VB.Label Label6 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Categoria:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   225
         TabIndex        =   19
         Top             =   1305
         Width           =   825
      End
      Begin VB.Label Label5 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Modelo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   360
         TabIndex        =   17
         Top             =   855
         Width           =   645
      End
      Begin VB.Label Label4 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Motor:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   5355
         TabIndex        =   16
         Top             =   855
         Width           =   540
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Veiculo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   5265
         TabIndex        =   15
         Top             =   405
         Width           =   645
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Situa��o:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   5130
         TabIndex        =   7
         Top             =   1305
         Width           =   750
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Montadora:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   90
         TabIndex        =   6
         Top             =   405
         Width           =   930
      End
   End
   Begin VB.TextBox txtCodAplicacao 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   5520
      MaxLength       =   10
      TabIndex        =   3
      Top             =   270
      Visible         =   0   'False
      Width           =   780
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   7050
      Width           =   11880
      _ExtentX        =   20955
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   20452
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      Top             =   810
      Width           =   9915
      _ExtentX        =   17489
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   735
      Left            =   0
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1296
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmVeicAplicacao.frx":23D2
      PICN            =   "frmVeicAplicacao.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdGravar 
      Height          =   735
      Left            =   840
      TabIndex        =   8
      TabStop         =   0   'False
      ToolTipText     =   "Gravar"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1296
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmVeicAplicacao.frx":30C8
      PICN            =   "frmVeicAplicacao.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSGrid.Grid grd 
      Height          =   3480
      Left            =   45
      TabIndex        =   9
      Top             =   3510
      Width           =   11715
      _Version        =   65536
      _ExtentX        =   20664
      _ExtentY        =   6138
      _StockProps     =   77
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      HighLight       =   0   'False
   End
   Begin Bot�o.cmd cmdCancel 
      Height          =   735
      Left            =   1680
      TabIndex        =   10
      TabStop         =   0   'False
      ToolTipText     =   "Cancelar"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1296
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmVeicAplicacao.frx":3DBE
      PICN            =   "frmVeicAplicacao.frx":3DDA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd1 
      Height          =   735
      Left            =   3360
      TabIndex        =   20
      TabStop         =   0   'False
      ToolTipText     =   "Importar"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1296
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmVeicAplicacao.frx":4AB4
      PICN            =   "frmVeicAplicacao.frx":4AD0
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd4 
      Height          =   735
      Left            =   4200
      TabIndex        =   25
      TabStop         =   0   'False
      ToolTipText     =   "Exporta Aplica��es"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1296
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmVeicAplicacao.frx":57AA
      PICN            =   "frmVeicAplicacao.frx":57C6
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdExcluir 
      Height          =   735
      Left            =   2520
      TabIndex        =   26
      TabStop         =   0   'False
      ToolTipText     =   "Excluir Aplica��o"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1296
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmVeicAplicacao.frx":64A0
      PICN            =   "frmVeicAplicacao.frx":64BC
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmVeicAplicacao"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Dim flg As Boolean
Public Sub PlanilhaExcel(ByVal Imprimir As Boolean)


    On Error GoTo TrataErro
    
    Dim X As Integer, Y As Integer, Z As Integer
    
    Dim EApp As Excel.Application
    Dim EwkB As Excel.Workbook
    Dim EwkS As Excel.Worksheet
    
    Screen.MousePointer = 11
    DoEvents
    
    On Error Resume Next
    
    Set EApp = CreateObject("Excel.Application")
    Set EwkB = EApp.Workbooks.Add
    Set EwkS = EwkB.Sheets(1)
    
        
    ' Esconde o Excel
    'EApp.Application.Visible = True

    ' Configura Margens da P�gina
    'EApp.ActiveSheet.PageSetup.PrintArea = ""
    
    'With EApp.ActiveSheet.PageSetup
        '.LeftHeader = "INDICE DE INFLA��O DE COMPRA"
        '.CenterHeader = cmbFornecedor.Text
        '.RightHeader = "&D"
        '.LeftFooter = "Comercial Automotiva Ltda. Confidencial"
        '.CenterFooter = "Depto. Controladoria"
        
       ' .RightFooter = "P�gina &P/&N"
       ' .LeftMargin = 10 ''Application.InchesToPoints(0.393700787401575)
       ' .RightMargin = 10 ''Application.InchesToPoints(0.393700787401575)
       ' .TopMargin = 10 ''Application.InchesToPoints(0.393700787401575)
       ' .BottomMargin = 10 ''Application.InchesToPoints(0.393700787401575)
        
        '.HeaderMargin = Application.InchesToPoints(0)
        '.FooterMargin = Application.InchesToPoints(0)
        
       ' .PrintHeadings = False
       ' .PrintGridlines = False
       ' .PrintComments = xlPrintNoComments
       ' .PrintQuality = 600
       ' .CenterHorizontally = False
       ' .CenterVertically = False
        
        '.Orientation = xlPortrait
  '      .Draft = False
  '      .PaperSize = xlPaperA4
  '      .FirstPageNumber = xlAutomatic
  '      .Order = xlDownThenOver
  '      .BlackAndWhite = False
        '.Zoom = 65
  '  End With
    
'    EApp.Columns(1).ColumnWidth = 15
'    EApp.Columns(2).ColumnWidth = 18
'    EApp.Columns(3).ColumnWidth = 40
'
'    EApp.Cells(1, 1).Value = "INFORME DE RENDIMENTO DO ANO " & frmConsulta.txtAno.Text
'    EApp.Range(EApp.Cells(1, 1), EApp.Cells(1, 5)).Select
'    With EApp.Selection
'        .HorizontalAlignment = xlCenter
'        .VerticalAlignment = xlBottom
'        .WrapText = False
'        .Orientation = 1
'        .ShrinkToFit = False
'        .MergeCells = True
'    End With
'    EApp.Selection.Merge
'
'    EApp.Cells(1, 1).Font.Bold = True
'
'    EApp.Cells(4, 1).Value = "COD. REPRES"
'    EApp.Cells(4, 1).Font.Bold = True
'
'    EApp.Cells(4, 2).Value = "CGC"
'    EApp.Cells(4, 2).Font.Bold = True
'
'    EApp.Cells(4, 3).Value = "RAZ�O SOCIAL"
'    EApp.Cells(4, 3).Font.Bold = True
'
'
'
'    x = 1
'    For I = 4 To 28 Step 2
'        EApp.Cells(3, I).Value = UCase(MonthName(x))
'        EApp.Range(EApp.Cells(3, I), EApp.Cells(3, I + 1)).Select
'        EApp.Selection.Merge
'        EApp.Selection.HorizontalAlignment = xlCenter
'        EApp.Cells(3, I).Font.Bold = True
'        x = x + 1
'    Next
'
'    For I = 4 To 27 Step 2
'        EApp.Cells(4, I).Value = "VALOR"
'        EApp.Cells(4, I + 1).Value = "IR"
'        EApp.Cells(4, I).HorizontalAlignment = xlCenter
'        EApp.Cells(4, I + 1).HorizontalAlignment = xlCenter
'        EApp.Cells(4, I).Font.Bold = True
'        EApp.Cells(4, I + 1).Font.Bold = True
'    Next

    ' Gera Dados na P�gina e Formata C�lulas
    Z = 1
    vObjOracle.MoveFirst
    
    Col = vObjOracle.Fields.Count

    For Y = 0 To Col
        EApp.Cells(Z, Y + 1).Value = vObjOracle.Fields(Y).Name
    Next

    Z = 2

    While Not vObjOracle.EOF
        For Y = 0 To 7
            EApp.Cells(Z, Y + 1).Value = vObjOracle(Y)
           'EApp.Cells(Z, y + 1).Font.Bold = True
        Next
        Z = Z + 1
        vObjOracle.MoveNext
    Wend
    
    
    If Not Imprimir Then
        ' Exibe Excel
        EApp.Application.Visible = True
    Else
        EApp.ActiveWindow.SelectedSheets.PrintOut Copies:=1
        EApp.ActiveWorkbook.Close (False)
        EApp.Quit
    End If
    
    ' Volta Cursor do Mouse
    Screen.MousePointer = 1
    
    ' Desliga o Excel do Programa
    Set EwkS = Nothing
    Set EwkB = Nothing
    Set EApp = Nothing
    
    Exit Sub
    
TrataErro:
    
       Screen.MousePointer = 1
       MsgBox "Ocorreu erro: " & Err.Number & " - " & Err.Description, 16
       Set EwkS = Nothing
       Set EwkB = Nothing
       Set EApp = Nothing

End Sub
Private Sub cmbModelo_Click()
    cmbMotor.ListIndex = -1
   carregaGrid
   DoEvents
End Sub

Private Sub cmbMontadora_Change()
  cmdCancel_Click
End Sub

Private Sub cmbMontadora_Click()
     
    cmbModelo.ListIndex = -1
    cmbMotor.ListIndex = -1
    DoEvents
    
    'BuscaVeiculo Left(cmbMontadora.Text, 2), 0, "", 0
    BuscaVeiculo cmbMontadora.ItemData(cmbMontadora.ListIndex), 0, "", 0
    cmbVeiculo.Clear
    While Not vObjOracle.EOF
        cmbVeiculo.AddItem vObjOracle("DESC_VEICULO")
        cmbVeiculo.ItemData(cmbVeiculo.NewIndex) = vObjOracle("COD_VEICULO")
        vObjOracle.MoveNext
    Wend
    'vVB_Generica_001.PreencheComboList vObjOracle, cmbVeiculo, "VEICULO"
    
    carregaGrid

End Sub

Private Sub cmbMotor_Click()
   
   carregaGrid
   DoEvents

End Sub

Private Sub cmbVeiculo_Click()
  cmbModelo.ListIndex = -1
  cmbMotor.ListIndex = -1
  DoEvents
    
  carregaGrid
End Sub

Private Sub cmd1_Click()
    frmImportAplicacao.Show
End Sub

Private Sub cmd4_Click()
  Dim strResposta As String


   If cmbMontadora <> "" Then
     strResposta = MsgBox("Confirma a gera��o de planilha com os ve�culos da montadora: " & cmbMontadora.Text & " ?", vbYesNo, "Aten��o")
     If strResposta = 7 Then
       Exit Sub
     End If
   Else
    strResposta = MsgBox("Confirma a gera��o de planilha com os ve�culos de todas as montadoras" & " ?", vbYesNo, "Aten��o")
     If strResposta = 7 Then
       Exit Sub
     End If
   End If
    
 
    Screen.MousePointer = 11
    stbBarra.Panels(1).Text = "Gerando Relat�rio. Aguarde..."
    DoEvents
    
  
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
    
        
    vBanco.Parameters.Remove "PM_COD_MONTADORA"
    If cmbMontadora = "" Then
       vBanco.Parameters.Add "PM_COD_MONTADORA", 0, 1
    Else
       vBanco.Parameters.Add "PM_COD_MONTADORA", cmbMontadora.ItemData(cmbMontadora.ListIndex), 1
    End If
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "PRODUCAO.PCK_CAD985.PR_ARQ_APLICACAO(:PM_COD_MONTADORA,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
    
    If vObjOracle.EOF Then
        MsgBox "Nenhuma restri��o cadastrada", , "Aten��o"
        Exit Sub
    End If
    
    PlanilhaExcel False
    
    stbBarra.Panels(1).Text = ""
    DoEvents
    Screen.MousePointer = 0


End Sub

Private Sub cmdCancel_Click()

    txtCodAplicacao.Text = ""
    cmbCategoria.ListIndex = -1
    cmbMotor.ListIndex = -1
    cmbModelo.ListIndex = -1
    cmbVeiculo.ListIndex = -1
    'cmbMontadora.ListIndex = -1
    
    cmbMontadora.Locked = False
    cmbVeiculo.Locked = False
    cmbModelo.Locked = False
    cmbMotor.Locked = False
    cmbCategoria.Locked = False
    txtDt_Inicio_Modelo = ""
    txtDt_Fim_Modelo = ""

End Sub

Private Sub cmdExcluir_Click()
  If cmbMontadora.Text = "" Then
        MsgBox "Selecione a montadora", , "Aten��o"
        cmbMontadora.SetFocus
        Exit Sub
    End If
    If cmbVeiculo.Text = "" Then
        MsgBox "Selecione o ve�culo", , "Aten��o"
        cmbVeiculo.SetFocus
        Exit Sub
    End If
    If cmbModelo.Text = "" Then
        MsgBox "Selecione o modelo", , "Aten��o"
        cmbModelo.SetFocus
        Exit Sub
    End If
    If cmbMotor.Text = "" Then
        MsgBox "Selecione o motor", , "Aten��o"
        cmbMotor.SetFocus
        Exit Sub
    End If
    If cmbCategoria.Text = "" Then
        MsgBox "Selecione a categoria", , "Aten��o"
        cmbCategoria.SetFocus
        Exit Sub
    End If

    If txtCodAplicacao.Text <> "" Then
        msg = GravarAplicacao("D", CLng(txtCodAplicacao.Text), cmbMontadora.ItemData(cmbMontadora.ListIndex), cmbVeiculo.ItemData(cmbVeiculo.ListIndex), cmbModelo.ItemData(cmbModelo.ListIndex), cmbMotor.ItemData(cmbMotor.ListIndex), Left(cmbCategoria.Text, 1), Left(cmbSituacao.Text, 1), txtDt_Inicio_Modelo, txtDt_Fim_Modelo)
        If msg <> "" Then
            MsgBox "Erro: " & msg
            Exit Sub
        Else
            MsgBox "Aplica��o excluida com sucesso"
        End If
         txtCodAplicacao.Text = ""
         cmbCategoria.ListIndex = -1
         cmbMotor.ListIndex = -1
         cmbModelo.ListIndex = -1
         cmbVeiculo.ListIndex = -1
         txtDt_Inicio_Modelo = ""
         txtDt_Fim_Modelo = ""
         
         carregaGrid
     End If
End Sub

Private Sub cmdGravar_Click()

    If cmbMontadora.Text = "" Then
        MsgBox "Selecione a montadora", , "Aten��o"
        cmbMontadora.SetFocus
        Exit Sub
    End If
    If cmbVeiculo.Text = "" Then
        MsgBox "Selecione o ve�culo", , "Aten��o"
        cmbVeiculo.SetFocus
        Exit Sub
    End If
    If cmbModelo.Text = "" Then
        MsgBox "Selecione o modelo", , "Aten��o"
        cmbModelo.SetFocus
        Exit Sub
    End If
    If cmbMotor.Text = "" Then
        MsgBox "Selecione o motor", , "Aten��o"
        cmbMotor.SetFocus
        Exit Sub
    End If
    If cmbCategoria.Text = "" Then
        MsgBox "Selecione a categoria", , "Aten��o"
        cmbCategoria.SetFocus
        Exit Sub
    End If

    If txtCodAplicacao.Text <> "" Then
        msg = GravarAplicacao("A", CLng(txtCodAplicacao.Text), cmbMontadora.ItemData(cmbMontadora.ListIndex), cmbVeiculo.ItemData(cmbVeiculo.ListIndex), cmbModelo.ItemData(cmbModelo.ListIndex), cmbMotor.ItemData(cmbMotor.ListIndex), Left(cmbCategoria.Text, 1), Left(cmbSituacao.Text, 1), txtDt_Inicio_Modelo, txtDt_Fim_Modelo)
        If msg <> "" Then
            MsgBox "Erro: " & msg
            Exit Sub
        Else
            MsgBox "Aplica��o alterada com sucesso"
        End If
    Else
        BuscaAplicacaoVeiculo 0, IIf(cmbMontadora.ItemData(cmbMontadora.ListIndex) = 0, 0, cmbMontadora.ItemData(cmbMontadora.ListIndex)), IIf(cmbVeiculo.ItemData(cmbVeiculo.ListIndex) = 0, 0, cmbVeiculo.ItemData(cmbVeiculo.ListIndex)), IIf(cmbModelo.ItemData(cmbModelo.ListIndex) = 0, 0, cmbModelo.ItemData(cmbModelo.ListIndex)), IIf(cmbMotor.ItemData(cmbMotor.ListIndex) = 0, 0, cmbMotor.ItemData(cmbMotor.ListIndex))
    
        If vObjOracle.EOF Then
            msg = GravarAplicacao("I", 0, cmbMontadora.ItemData(cmbMontadora.ListIndex), cmbVeiculo.ItemData(cmbVeiculo.ListIndex), cmbModelo.ItemData(cmbModelo.ListIndex), cmbMotor.ItemData(cmbMotor.ListIndex), Left(cmbCategoria.Text, 1), Left(cmbSituacao.Text, 1), txtDt_Inicio_Modelo, txtDt_Fim_Modelo)
            If msg <> "" Then
                MsgBox "Erro: " & msg
                Exit Sub
            Else
                MsgBox "Aplica��o incluida com sucesso"
            End If
        Else
            MsgBox "Esta aplica��o j� esta cadastrada", , "Aten��o"
            Exit Sub
        End If
    End If
   'COMENTADO CONFORME SOLICITA��O
   'DO USU�RIO QUE PREFERE QUE AS INFORMA��ES
   'PERMANE�AM NA TELA (20/06/07)
   ' txtCodAplicacao.Text = ""
   ' cmbCategoria.ListIndex = -1
   ' cmbMotor.ListIndex = -1
   ' cmbModelo.ListIndex = -1
   ' cmbVeiculo.ListIndex = -1
   ' txtDt_Inicio_Modelo = ""
   ' txtDt_Fim_Modelo = ""

    
   ' carregaGrid

End Sub

Private Sub cmdVoltar_Click()

  On Error GoTo Trata_erro

    Unload Me
    
  Exit Sub
  
Trata_erro:
    If Err = 365 Then
      Resume Next
    End If

End Sub
Private Sub Form_Load()
    
    Me.Top = 0
    Me.Left = 0
    
    cmbSituacao.AddItem "0 - Ativo"
    cmbSituacao.AddItem "9 - Desativado"
    cmbSituacao.Text = "0 - Ativo"
    
    
    cmbCategoria.AddItem "P - PESADO"
    cmbCategoria.AddItem "L - LEVE"
    cmbCategoria.AddItem "U - UNIVERSAL"
    cmbCategoria.AddItem "M - MOTO"
    cmbCategoria.AddItem "A - AGR�COLA"

    
    BuscaMontadora 0, ""
    While Not vObjOracle.EOF
        cmbMontadora.AddItem vObjOracle("MONTADORA")
        cmbMontadora.ItemData(cmbMontadora.NewIndex) = vObjOracle("C�D.")
        vObjOracle.MoveNext
    Wend
    'vVB_Generica_001.PreencheComboList vObjOracle, cmbMontadora, "C�D.", "MONTADORA"
    
    BuscaModelo 0, "", 0
    While Not vObjOracle.EOF
        cmbModelo.AddItem vObjOracle("MODELO")
        cmbModelo.ItemData(cmbModelo.NewIndex) = vObjOracle("C�D.")
        vObjOracle.MoveNext
    Wend
    'vVB_Generica_001.PreencheComboList vObjOracle, cmbModelo, "C�D.", "MODELO"

    BuscaMotor 0, "", 0
    While Not vObjOracle.EOF
        cmbMotor.AddItem vObjOracle("MOTOR")
        cmbMotor.ItemData(cmbMotor.NewIndex) = vObjOracle("C�D.")
        vObjOracle.MoveNext
    Wend
    'vVB_Generica_001.PreencheComboList vObjOracle, cmbMotor, "C�D.", "MOTOR"
    
End Sub


Sub carregaGrid()

    If flg = False Then
        If cmbMontadora.Text = "" Then
            montadora = 0
        Else
            montadora = cmbMontadora.ItemData(cmbMontadora.ListIndex)
        End If
        If cmbVeiculo.Text = "" Then
            Veiculo = 0
        Else
            Veiculo = cmbVeiculo.ItemData(cmbVeiculo.ListIndex)
        End If
        If cmbModelo.Text = "" Then
            Modelo = 0
        Else
            Modelo = cmbModelo.ItemData(cmbModelo.ListIndex)
        End If
        If cmbMotor.Text = "" Then
            MOTOR = 0
        Else
            MOTOR = cmbMotor.ItemData(cmbMotor.ListIndex)
        End If
        
        BuscaAplicacaoVeiculo 0, CInt(montadora), CInt(Veiculo), CInt(Modelo), CInt(MOTOR)
    
        If vObjOracle.EOF Then
            vVB_Generica_001.LimpaGridComTitulo grd
        Else
            vVB_Generica_001.CarregaGridTabela grd, vObjOracle, 10
        End If
    End If

End Sub

Private Sub grd_DblClick()

   grd.Col = 1
    If grd.Text <> "" Then
        flg = True
        txtCodAplicacao.Text = grd.Text
        grd.Col = 2
        'cmbMontadora.Text = grd.Text
        cmbMontadora.Locked = True
        grd.Col = 3
        cmbVeiculo.Text = grd.Text
        cmbVeiculo.Locked = True
        grd.Col = 4
        cmbModelo.Text = grd.Text
        cmbModelo.Locked = True
        grd.Col = 5
        cmbMotor.Text = grd.Text
        cmbMotor.Locked = True
        grd.Col = 6
        cmbCategoria.Text = grd.Text
       ' cmbCategoria.Locked = True
        grd.Col = 7
        cmbSituacao.Text = grd.Text
        grd.Col = 8
        txtDt_Inicio_Modelo.Text = grd.Text
        grd.Col = 9
        txtDt_Fim_Modelo.Text = grd.Text
        
        flg = False
        
        cmdCancel.Visible = True
        'cmdGravar.Visible = True
    End If

End Sub

Private Sub Text1_Change()

End Sub

Private Sub Text2_Change()

End Sub

Private Sub txtDt_Fim_Modelo_KeyPress(KeyAscii As Integer)
  DATA KeyAscii, txtDt_Fim_Modelo
End Sub

Private Sub txtDt_Inicio_Modelo_KeyPress(KeyAscii As Integer)
  DATA KeyAscii, txtDt_Inicio_Modelo
End Sub
