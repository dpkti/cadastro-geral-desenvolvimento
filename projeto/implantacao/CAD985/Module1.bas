Attribute VB_Name = "Module1"
Public strOriginal As String
Public lngAplicacao As Long
Public strRestricao As String
Public strDepto As String
Public strNome_usuario As String
Public lngCod_Usuario As Long
Public usuario As String
Public strFl_Equiv As String
Public strPesquisa As String
Const Key_F2 = &H71
Function Numerico(ByVal KeyAscii As Integer) As Integer
    If KeyAscii = 8 Then    'BACKSPACE
        Numerico = KeyAscii
        Exit Function
    End If
    If Chr(KeyAscii) < "0" Or Chr(KeyAscii) > "9" Then
        KeyAscii = 0
        Beep
    End If
    Numerico = KeyAscii
End Function

Function Texto(ByVal KeyAscii As Integer) As Integer
    If KeyAscii = 8 Then    'BACKSPACE
        Texto = KeyAscii
        Exit Function
    End If
    
    If Chr$(KeyAscii) = "'" Then
        KeyAscii = 0
        Beep
    Else
        KeyAscii = Asc(UCase(Chr$(KeyAscii)))
    End If
    
    Texto = KeyAscii
End Function

Public Function Copia_Equiv(lngCod_DPK As Long) As String
    vBanco.Parameters.Remove "PM_CODDPK"
    vBanco.Parameters.Add "PM_CODDPK", lngCod_DPK, 1
    
    
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
    
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "PRODUCAO.PCK_CAD985.PR_BUSCA_DPK_EQUIV(:PM_CODDPK,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
    
    If vObjOracle(0) = 0 Then
      Copia_Equiv = "N"
    Else
        vBanco.Parameters.Remove "PM_CODDPK"
        vBanco.Parameters.Add "PM_CODDPK", lngCod_DPK, 1
        
        
        vBanco.Parameters.Remove "PM_CURSOR1"
        vBanco.Parameters.Add "PM_CURSOR1", 0, 3
        vBanco.Parameters("PM_CURSOR1").ServerType = 102
        vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
        vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
        
        vBanco.Parameters.Remove "PM_CODERRO"
        vBanco.Parameters.Add "PM_CODERRO", 0, 2
        vBanco.Parameters.Remove "PM_TXTERRO"
        vBanco.Parameters.Add "PM_TXTERRO", "", 2
        
        vSql = "PRODUCAO.PCK_CAD985.PR_COPIA_DPK_EQUIV(:PM_CODDPK,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
        
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        
       
    End If
    
End Function
Public Function Copia_Item(lngCod_DPK As Long, lngCod_DPK_CP As Long) As String
    
    
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
    
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
        vBanco.Parameters.Remove "PM_CODDPK"
        vBanco.Parameters.Add "PM_CODDPK", lngCod_DPK, 1
        
        vBanco.Parameters.Remove "PM_DPKCOPIA"
        vBanco.Parameters.Add "PM_DPKCOPIA", lngCod_DPK_CP, 1
        
        
        vBanco.Parameters.Remove "PM_CURSOR1"
        vBanco.Parameters.Add "PM_CURSOR1", 0, 3
        vBanco.Parameters("PM_CURSOR1").ServerType = 102
        vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
        vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
        
        vBanco.Parameters.Remove "PM_CODERRO"
        vBanco.Parameters.Add "PM_CODERRO", 0, 2
        vBanco.Parameters.Remove "PM_TXTERRO"
        vBanco.Parameters.Add "PM_TXTERRO", "", 2
        
        vSql = "PRODUCAO.PCK_CAD985.PR_COPIA_DPK(:PM_CODDPK,:PM_DPKCOPIA,:PM_CODERRO,:PM_TXTERRO)"
        
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        If Val(vErro) <> 0 Then
          Copia_Item = "N"
        Else
          Copia_Item = "S"
        End If
       
   
End Function
Sub DATA(ByRef KeyAscii, ByRef TxtCampo)
    On Error GoTo TrataErro

    Dim bTam As Byte    'tamanho do campo JA digitado
    Dim strData As String
    Dim bKey As Byte
    bTam = Len(TxtCampo.Text)
    
    If KeyAscii = 8 And bTam > 0 Then 'backspace
        If Mid$(TxtCampo.Text, bTam) = "/" Then
            TxtCampo.Text = Mid$(TxtCampo.Text, 1, bTam - 2)
        Else
            TxtCampo.Text = Mid$(TxtCampo.Text, 1, bTam - 1)
        End If
            
    ElseIf Chr$(KeyAscii) >= "0" And Chr$(KeyAscii) <= "9" Then
        If bTam = 1 Then
            strData = TxtCampo.Text & Chr$(KeyAscii)
            If CInt(strData) < 1 Or CInt(strData > 31) Then
                Beep
            Else
                TxtCampo.Text = TxtCampo.Text & Chr$(KeyAscii) & "/"
            End If
            
        ElseIf bTam = 4 Then
            strData = Mid$(TxtCampo.Text, 4) & Chr$(KeyAscii)
            If CInt(strData) < 1 Or CInt(strData > 12) Then
                Beep
            Else
                TxtCampo.Text = TxtCampo.Text & Chr$(KeyAscii) & "/"
            End If
        
        ElseIf bTam = 7 Then
            strData = Mid$(TxtCampo.Text, 1, 2)     'dia
            If CInt(strData) < 1 Or CInt(strData > 31) Then
                Beep
            Else
                strData = Mid$(TxtCampo.Text, 4, 2)     'mes
                If CInt(strData) < 1 Or CInt(strData > 12) Then
                    Beep
                Else
                    strData = TxtCampo.Text & Chr$(KeyAscii)
                    If Not IsDate(CDate(strData)) Then
                        Beep
                    Else
                        TxtCampo.Text = strData
                    End If
                End If
            End If
            
        ElseIf bTam < 8 Then
            bKey = KeyAscii
            
        Else
            bKey = 0
        End If
    Else
        Beep
    End If
    
    SendKeys "{END}"
    KeyAscii = bKey
    Exit Sub
    
TrataErro:

    If Err.Number = 13 Then
        MsgBox strData, vbInformation, "Data Inv�lida"
        KeyAscii = 0
        Beep
        Err.Clear
    End If

End Sub
Public Sub BuscaMontadora(codMontadora As Integer, descMontadora As String)

    vBanco.Parameters.Remove "PM_CODCMONTA"
    vBanco.Parameters.Add "PM_CODCMONTA", codMontadora, 1
    
    vBanco.Parameters.Remove "PM_DESCMONTA"
    vBanco.Parameters.Add "PM_DESCMONTA", IIf(descMontadora = "", Null, descMontadora), 1
  
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
    
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "PRODUCAO.PCK_CAD985.PR_CON_MONTADORA(:PM_CODCMONTA,:PM_DESCMONTA,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value

End Sub
Public Sub BuscaEstat_Veiculo()

   
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
    
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "PRODUCAO.PCK_CAD985.PR_EST_VEICULO(:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value

End Sub
Public Sub BuscaEstat_Aplicacao()

   
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
    
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "PRODUCAO.PCK_CAD985.PR_EST_Aplicacao(:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value

End Sub

Function Maiusculo(KeyAscii As Integer) As Integer
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    Maiusculo = KeyAscii
End Function

Public Sub BuscaVeiculo(codMontadora As Integer, codVeiculo As Integer, descVeiculo As String, Situacao As String)


    vBanco.Parameters.Remove "PM_CODMONTA"
    vBanco.Parameters.Add "PM_CODMONTA", codMontadora, 1
    
    vBanco.Parameters.Remove "PM_CODVEIC"
    vBanco.Parameters.Add "PM_CODVEIC", codVeiculo, 1
    
    vBanco.Parameters.Remove "PM_DESCVEIC"
    vBanco.Parameters.Add "PM_DESCVEIC", IIf(descVeiculo = "", Null, descVeiculo), 1
    
    vBanco.Parameters.Remove "PM_SITUACAO"
    vBanco.Parameters.Add "PM_SITUACAO", IIf(Situacao = "", Null, Situacao), 1
  
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
    
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "PRODUCAO.PCK_CAD985.PR_CON_VEICULO(:PM_CODMONTA,:PM_CODVEIC,:PM_DESCVEIC,:PM_SITUACAO,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value

End Sub
Public Sub BuscaDPK_Aplic(lngCod_DPK As Long, lngCod_Aplic As Long, strOriginal As String)
    vBanco.Parameters.Remove "PM_DPK"
    vBanco.Parameters.Add "PM_DPK", lngCod_DPK, 1
    
    vBanco.Parameters.Remove "PM_APLIC"
    vBanco.Parameters.Add "PM_APLIC", lngCod_Aplic, 1
    
  
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
    
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "PRODUCAO.PCK_CAD985.PR_BUSCA_DPK_APLIC(:PM_DPK,:PM_APLIC,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
End Sub
Public Sub BuscaDPK_Restr(lngCod_DPK As Long, lngCod_Aplic As Long)
    vBanco.Parameters.Remove "PM_DPK"
    vBanco.Parameters.Add "PM_DPK", lngCod_DPK, 1
    
    vBanco.Parameters.Remove "PM_APLIC"
    vBanco.Parameters.Add "PM_APLIC", lngCod_Aplic, 1
    
  
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
    
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "PRODUCAO.PCK_CAD985.PR_BUSCA_DPK_RESTR(:PM_DPK,:PM_APLIC,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
End Sub

Public Sub BuscaModelo(codModelo As Integer, descModelo As String, Situacao As String)


    vBanco.Parameters.Remove "PM_CODMODELO"
    vBanco.Parameters.Add "PM_CODMODELO", codModelo, 1
    
    vBanco.Parameters.Remove "PM_DESCMODELO"
    vBanco.Parameters.Add "PM_DESCMODELO", IIf(descModelo = "", Null, descModelo), 1
  
    vBanco.Parameters.Remove "PM_SITUACAO"
    vBanco.Parameters.Add "PM_SITUACAO", IIf(Situacao = "", Null, Situacao), 1
  
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
    
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "PRODUCAO.PCK_CAD985.PR_CON_MODELO(:PM_CODMODELO,:PM_DESCMODELO,:PM_SITUACAO,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value

End Sub

Public Sub BuscaMotor(codMotor As Integer, descMotor As String, Situacao As String)


    vBanco.Parameters.Remove "PM_CODMOTOR"
    vBanco.Parameters.Add "PM_CODMOTOR", codMotor, 1
    
    vBanco.Parameters.Remove "PM_DESCMotor"
    vBanco.Parameters.Add "PM_DESCMotor", IIf(descMotor = "", Null, descMotor), 1
  
    vBanco.Parameters.Remove "PM_SITUACAO"
    vBanco.Parameters.Add "PM_SITUACAO", IIf(Situacao = "", Null, Situacao), 1
  
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
    
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "PRODUCAO.PCK_CAD985.PR_CON_Motor(:PM_CODMOTOR,:PM_DESCMotor,:PM_SITUACAO,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value

End Sub
Public Sub ValidaDPK(lngCod_DPK As Long)


   vBanco.Parameters.Remove "PM_DPK"
    vBanco.Parameters.Add "PM_DPK", lngCod_DPK, 1
    
  
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
    
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "PRODUCAO.PCK_CAD985.PR_VALIDA_DPK(:PM_DPK,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value

End Sub
Public Sub ValidaAplic(lngCod_Aplic As Long)


   vBanco.Parameters.Remove "PM_APLIC"
    vBanco.Parameters.Add "PM_APLIC", lngCod_Aplic, 1
    
  
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
    
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "PRODUCAO.PCK_CAD985.PR_VALIDA_APLIC(:PM_APLIC,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value

End Sub
Public Sub BuscaGrupo(codGrupo As Integer, descGrupo As String, Situacao As String)


    vBanco.Parameters.Remove "PM_CODGrupo"
    vBanco.Parameters.Add "PM_CODGrupo", codGrupo, 1
    
    vBanco.Parameters.Remove "PM_DESCGrupo"
    vBanco.Parameters.Add "PM_DESCGrupo", IIf(descGrupo = "", Null, descGrupo), 1
  
    vBanco.Parameters.Remove "PM_SITUACAO"
    vBanco.Parameters.Add "PM_SITUACAO", IIf(Situacao = "", Null, Situacao), 1
  
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
    
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "PRODUCAO.PCK_CAD985.PR_CON_Grupo(:PM_CODGrupo,:PM_DESCGrupo,:PM_SITUACAO,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value

End Sub



Public Sub BuscaAplicacaoVeiculo(codAplicacao As Integer, codMontadora As Integer, codVeiculo As Integer, codModelo As Integer, codMotor As Integer)


    vBanco.Parameters.Remove "PM_CODAPLIC"
    vBanco.Parameters.Add "PM_CODAPLIC", codAplicacao, 1
  
    vBanco.Parameters.Remove "PM_MONTADORA"
    vBanco.Parameters.Add "PM_MONTADORA", codMontadora, 1
    
    vBanco.Parameters.Remove "PM_VEICULO"
    vBanco.Parameters.Add "PM_VEICULO", codVeiculo, 1
  
    vBanco.Parameters.Remove "PM_MODELO"
    vBanco.Parameters.Add "PM_MODELO", codModelo, 1
    
    vBanco.Parameters.Remove "PM_MOTOR"
    vBanco.Parameters.Add "PM_MOTOR", codMotor, 1
  
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
    
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "PRODUCAO.PCK_CAD985.PR_CON_APLIC_VEICULO(:PM_CODAPLIC,:PM_MONTADORA,:PM_VEICULO,:PM_MODELO,:PM_MOTOR,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value

End Sub


Public Function GravarDPK_Aplic(strAction As String, lngCod_DPK As Long, lngCod_Aplic As Long, strOriginal As String, strOriginal_Novo As String) As String

    vBanco.Parameters.Remove "PM_ACT"
    vBanco.Parameters.Add "PM_ACT", strAction, 1
    
    vBanco.Parameters.Remove "PM_CODDPK"
    vBanco.Parameters.Add "PM_CODDPK", lngCod_DPK, 1
    
    vBanco.Parameters.Remove "PM_CODAPLIC"
    vBanco.Parameters.Add "PM_CODAPLIC", lngCod_Aplic, 1
    
     vBanco.Parameters.Remove "PM_ORIGINAL"
    vBanco.Parameters.Add "PM_ORIGINAL", strOriginal, 1
    
     vBanco.Parameters.Remove "PM_ORIGINAL_NOVO"
    vBanco.Parameters.Add "PM_ORIGINAL_NOVO", strOriginal_Novo, 1
    
    vBanco.Parameters.Remove "PM_SITUACAO"
    vBanco.Parameters.Add "PM_SITUACAO", 0, 1
    
    
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "PRODUCAO.PCK_CAD985.PR_ACT_DPK_APLIC(:PM_ACT,:PM_CODDPK, :PM_CODAPLIC,:PM_ORIGINAL,:PM_ORIGINAL_NOVO,:PM_SITUACAO,:PM_CODERRO,:PM_TXTERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
    
    If vBanco.Parameters("PM_CODERRO").Value <> 0 Then
        GravarDPK_Aplic = vBanco.Parameters("PM_CODERRO").Value & " - " & vBanco.Parameters("PM_TXTERRO").Value
    Else
        GravarDPK_Aplic = ""
    End If
End Function
Public Function GravarDPK_Restr(strAction As String, lngCod_DPK As Long, lngCod_Aplic As Long, lngCod_Grupo As Long, lngCod_Subgrupo As Long, strDt_Inicio As String, strDt_Fim As String) As String

    vBanco.Parameters.Remove "PM_ACT"
    vBanco.Parameters.Add "PM_ACT", strAction, 1
    
    vBanco.Parameters.Remove "PM_CODDPK"
    vBanco.Parameters.Add "PM_CODDPK", lngCod_DPK, 1
    
    vBanco.Parameters.Remove "PM_CODAPLIC"
    vBanco.Parameters.Add "PM_CODAPLIC", lngCod_Aplic, 1
    
     vBanco.Parameters.Remove "PM_GRUPO"
    vBanco.Parameters.Add "PM_GRUPO", lngCod_Grupo, 1
    
     vBanco.Parameters.Remove "PM_SUBGRUPO"
    vBanco.Parameters.Add "PM_SUBGRUPO", lngCod_Subgrupo, 1
    
    vBanco.Parameters.Remove "PM_INICIO"
    If strDt_Inicio = "" Then
      vBanco.Parameters.Add "PM_INICIO", Null, 1
    Else
     vBanco.Parameters.Add "PM_INICIO", CDate(strDt_Inicio), 1
    End If
    
    vBanco.Parameters.Remove "PM_FIM"
    If strDt_Fim = "" Then
        vBanco.Parameters.Add "PM_FIM", Null, 1
    Else
        vBanco.Parameters.Add "PM_FIM", strDt_Fim, 1
    End If
        
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "PRODUCAO.PCK_CAD985.PR_ACT_DPK_REST(:PM_ACT,:PM_CODDPK, :PM_CODAPLIC,0,:PM_GRUPO,:PM_SUBGRUPO,:PM_INICIO,:PM_FIM,:PM_CODERRO,:PM_TXTERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
    
    If vBanco.Parameters("PM_CODERRO").Value <> 0 Then
        GravarDPK_Restr = vBanco.Parameters("PM_CODERRO").Value & " - " & vBanco.Parameters("PM_TXTERRO").Value
    Else
        GravarDPK_Restr = ""
    End If
End Function
Public Function GravarAplicacao(pACT As String, codAplicacao As Integer, codMontadora As Integer, codVeiculo As Integer, codModelo As Integer, codMotor As Integer, Categoria As String, Situacao As Integer, Dt_Inicio As String, Dt_Fim As String) As String

    vBanco.Parameters.Remove "PM_ACT"
    vBanco.Parameters.Add "PM_ACT", pACT, 1
    
    vBanco.Parameters.Remove "PM_CODAPLIC"
    vBanco.Parameters.Add "PM_CODAPLIC", codAplicacao, 1
    
     vBanco.Parameters.Remove "PM_CODMONTA"
    vBanco.Parameters.Add "PM_CODMONTA", codMontadora, 1
    
     vBanco.Parameters.Remove "PM_CODVEIC"
    vBanco.Parameters.Add "PM_CODVEIC", codVeiculo, 1
    
     vBanco.Parameters.Remove "PM_CODMODELO"
    vBanco.Parameters.Add "PM_CODMODELO", codModelo, 1
    
     vBanco.Parameters.Remove "PM_CODMOTOR"
    vBanco.Parameters.Add "PM_CODMOTOR", codMotor, 1
    
     vBanco.Parameters.Remove "PM_CODCATEG"
    vBanco.Parameters.Add "PM_CODCATEG", Categoria, 1
    
    vBanco.Parameters.Remove "PM_SITUACAO"
    vBanco.Parameters.Add "PM_SITUACAO", Situacao, 1
    
    vBanco.Parameters.Remove "PM_DT_INICIO"
    If Dt_Inicio = "" Then
      vBanco.Parameters.Add "PM_DT_INICIO", "", 1
    Else
      vBanco.Parameters.Add "PM_DT_INICIO", CDate(Dt_Inicio), 1
    End If
     
    vBanco.Parameters.Remove "PM_DT_FIM"
    If Dt_Fim = "" Then
      vBanco.Parameters.Add "PM_DT_FIM", "", 1
    Else
      vBanco.Parameters.Add "PM_DT_FIM", CDate(Dt_Fim), 1
    End If
  
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "PRODUCAO.PCK_CAD985.PR_ACT_APLIC_VEICULO(:PM_ACT,:PM_CODAPLIC,:PM_CODMONTA,:PM_CODVEIC,:PM_CODMODELO,:PM_CODMOTOR,:PM_CODCATEG,:PM_SITUACAO,:PM_DT_INICIO,:PM_DT_FIM,:PM_CODERRO,:PM_TXTERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
    
    If vBanco.Parameters("PM_CODERRO").Value <> 0 Then
        GravarAplicacao = vBanco.Parameters("PM_CODERRO").Value & " - " & vBanco.Parameters("PM_TXTERRO").Value
    Else
        GravarAplicacao = ""
    End If

End Function



Public Sub BuscaSubGrupo(codGrupo As Integer, codSubGrupo As Integer, descSubGrupo As String, Situacao As String)


    vBanco.Parameters.Remove "PM_CODGRUPO"
    vBanco.Parameters.Add "PM_CODGRUPO", codGrupo, 1
    
    vBanco.Parameters.Remove "PM_CODSUBGRUPO"
    vBanco.Parameters.Add "PM_CODSUBGRUPO", codSubGrupo, 1
    
    vBanco.Parameters.Remove "PM_DESCSUBGRUPO"
    vBanco.Parameters.Add "PM_DESCSUBGRUPO", IIf(descSubGrupo = "", Null, descSubGrupo), 1
    
    vBanco.Parameters.Remove "PM_SITUACAO"
    vBanco.Parameters.Add "PM_SITUACAO", IIf(Situacao = "", Null, Situacao), 1
  
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
    
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "PRODUCAO.PCK_CAD985.PR_CON_SubGrupo(:PM_CODGRUPO,:PM_CODSUBGRUPO,:PM_DESCSUBGRUPO,:PM_SITUACAO,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value

End Sub
