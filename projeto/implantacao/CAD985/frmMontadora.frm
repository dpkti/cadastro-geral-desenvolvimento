VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmMontadora 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Montadora"
   ClientHeight    =   5460
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4680
   Icon            =   "frmMontadora.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   364
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   312
   Begin VB.TextBox txtCodMontadora 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3645
      MaxLength       =   10
      TabIndex        =   8
      Top             =   270
      Visible         =   0   'False
      Width           =   780
   End
   Begin VB.Frame fra 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   915
      Left            =   45
      TabIndex        =   4
      Top             =   945
      Width           =   4560
      Begin VB.TextBox txtMontadora 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1125
         MaxLength       =   10
         TabIndex        =   6
         Top             =   360
         Width           =   3255
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Montadora:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   135
         TabIndex        =   7
         Top             =   450
         Width           =   930
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   5130
      Width           =   4680
      _ExtentX        =   8255
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   7752
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      Top             =   810
      Width           =   4560
      _ExtentX        =   8043
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmMontadora.frx":23D2
      PICN            =   "frmMontadora.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdGravar 
      Height          =   690
      Left            =   855
      TabIndex        =   3
      TabStop         =   0   'False
      ToolTipText     =   "Gravar"
      Top             =   45
      Visible         =   0   'False
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmMontadora.frx":30C8
      PICN            =   "frmMontadora.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSGrid.Grid grd 
      Height          =   3120
      Left            =   45
      TabIndex        =   5
      Top             =   1935
      Width           =   4560
      _Version        =   65536
      _ExtentX        =   8043
      _ExtentY        =   5503
      _StockProps     =   77
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      HighLight       =   0   'False
   End
   Begin Bot�o.cmd cmdCancel 
      Height          =   690
      Left            =   1575
      TabIndex        =   9
      TabStop         =   0   'False
      ToolTipText     =   "Cancelar"
      Top             =   45
      Visible         =   0   'False
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmMontadora.frx":3DBE
      PICN            =   "frmMontadora.frx":3DDA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmMontadora"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdCancel_Click()
    txtMontadora.Text = ""
    txtCodMontadora.Text = ""
    cmdCancel.Visible = False
    cmdGravar.Visible = False
End Sub

Private Sub cmdGravar_Click()
    
    If txtMontadora.Text = "" Then
        MsgBox "Digite o nome da montadora", , "Aten��o"
        txtMontadora.SetFocus
        Exit Sub
    End If
    
    If txtCodMontadora.Text <> "" Then
        msg = GravarMontadora("A", txtCodMontadora.Text, txtMontadora.Text)
        If msg <> "" Then
            MsgBox "Erro: " & msg
            Exit Sub
        Else
            MsgBox "Montadora alterada com sucesso"
        End If
    Else
        BuscaMontadora 0, txtMontadora.Text
    
        If vObjOracle.EOF Then
            msg = GravarMontadora("I", 0, txtMontadora.Text)
            If msg <> "" Then
                MsgBox "Erro: " & msg
                Exit Sub
            Else
                MsgBox "Montadora incluida com sucesso"
            End If
        Else
            MsgBox "Esta montadora j� esta cadastrada", , "Aten��o"
            Exit Sub
        End If
    End If
    
    montaGrid
    cmdCancel_Click
    
End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    
    Me.Top = 0
    Me.Left = 0
    
    montaGrid
    
End Sub

Private Sub grd_DblClick()
    grd.Col = 1
    If grd.Text <> "" Then
        txtCodMontadora.Text = grd.Text
        grd.Col = 2
        txtMontadora.Text = grd.Text
        cmdCancel.Visible = True
        cmdGravar.Visible = True
    End If
End Sub


Private Function GravarMontadora(pACT As String, codMontadora As Integer, descMontadora As String) As String

    vBanco.Parameters.Remove "PM_ACT"
    vBanco.Parameters.Add "PM_ACT", pACT, 1
    
    vBanco.Parameters.Remove "PM_CODMONTA"
    vBanco.Parameters.Add "PM_CODMONTA", codMontadora, 1
    
    vBanco.Parameters.Remove "PM_DESCMONTA"
    vBanco.Parameters.Add "PM_DESCMONTA", descMontadora, 1
  
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "PRODUCAO.PCK_CAD985.PR_ACT_MONTADORA(:PM_ACT,:PM_CODMONTA,:PM_DESCMONTA,:PM_CODERRO,:PM_TXTERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
    
    If vBanco.Parameters("PM_CODERRO").Value <> 0 Then
        GravarMontadora = vBanco.Parameters("PM_CODERRO").Value & " - " & vBanco.Parameters("PM_TXTERRO").Value
    Else
        GravarMontadora = ""
    End If

End Function

Private Sub montaGrid()

    BuscaMontadora 0, ""
    
    If Not vObjOracle.EOF Then
        vVB_Generica_001.CarregaGridTabela grd, vObjOracle, 3
    End If

End Sub

