VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmCombustivel 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Combustivel"
   ClientHeight    =   5910
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11835
   Icon            =   "frmCombustivel.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   394
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   789
   Begin VB.Frame fra 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1320
      Left            =   45
      TabIndex        =   4
      Top             =   945
      Width           =   5730
      Begin VB.ComboBox cmbSituacao 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1170
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   810
         Width           =   2175
      End
      Begin VB.TextBox txtCombustivel 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1170
         MaxLength       =   30
         TabIndex        =   5
         Top             =   360
         Width           =   3255
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Situa��o:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   405
         TabIndex        =   8
         Top             =   900
         Width           =   750
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Combustivel:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   135
         TabIndex        =   7
         Top             =   450
         Width           =   1035
      End
   End
   Begin VB.TextBox txtCodCombustivel 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3600
      MaxLength       =   10
      TabIndex        =   3
      Top             =   270
      Visible         =   0   'False
      Width           =   780
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   5580
      Width           =   11835
      _ExtentX        =   20876
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   20373
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      Top             =   810
      Width           =   5910
      _ExtentX        =   10425
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCombustivel.frx":23D2
      PICN            =   "frmCombustivel.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin pkGradientControl.pkGradient pkGradient2 
      Height          =   30
      Left            =   45
      TabIndex        =   9
      Top             =   810
      Width           =   5235
      _ExtentX        =   9234
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdGravar 
      Height          =   690
      Left            =   810
      TabIndex        =   10
      TabStop         =   0   'False
      ToolTipText     =   "Gravar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCombustivel.frx":30C8
      PICN            =   "frmCombustivel.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSGrid.Grid grd 
      Height          =   3165
      Left            =   45
      TabIndex        =   11
      Top             =   2340
      Width           =   11730
      _Version        =   65536
      _ExtentX        =   20690
      _ExtentY        =   5583
      _StockProps     =   77
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      HighLight       =   0   'False
   End
   Begin Bot�o.cmd cmdCancel 
      Height          =   690
      Left            =   1530
      TabIndex        =   12
      TabStop         =   0   'False
      ToolTipText     =   "Cancelar"
      Top             =   45
      Visible         =   0   'False
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCombustivel.frx":3DBE
      PICN            =   "frmCombustivel.frx":3DDA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmCombustivel"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdCancel_Click()

    txtCodCombustivel.Text = ""
    txtCombustivel.Text = ""
    cmbSituacao.Text = "0 - Ativo"
    cmdCancel.Visible = False

End Sub

Private Sub cmdGravar_Click()

    If txtCombustivel.Text = "" Then
        MsgBox "Digite o nome do Combustivel", , "Aten��o"
        txtCombustivel.SetFocus
        Exit Sub
    End If
    
    If txtCodCombustivel.Text <> "" Then
        msg = GravarCombustivel("A", txtCodCombustivel.Text, txtCombustivel.Text, Left(cmbSituacao.Text, 1))
        If msg <> "" Then
            MsgBox "Erro: " & msg
            Exit Sub
        Else
            MsgBox "Combustivel alterado com sucesso"
        End If
    Else
        BuscaCombustivel
    
        If vObjOracle.EOF Then
            msg = GravarCombustivel("I", 0, txtCombustivel.Text, Left(cmbSituacao.Text, 1))
            If msg <> "" Then
                MsgBox "Erro: " & msg
                Exit Sub
            Else
                MsgBox "Combustivel incluido com sucesso"
            End If
        Else
            MsgBox "Este Combustivel j� esta cadastrado", , "Aten��o"
            Exit Sub
        End If
    End If
    
    cmdCancel_Click
    carregaGrid

End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    
    Me.Top = 0
    Me.Left = 0
    
    carregaGrid
    
    cmbSituacao.AddItem "0 - Ativo"
    cmbSituacao.AddItem "9 - Desativado"
    
    cmbSituacao.Text = "0 - Ativo"
    
End Sub

Sub BuscaCombustivel()


    vBanco.Parameters.Remove "PM_DESCCOMBUSTIVEL"
    vBanco.Parameters.Add "PM_DESCCOMBUSTIVEL", IIf(txtCombustivel.Text = "", Null, txtCombustivel.Text), 1
  
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
    
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "PRODUCAO.PCK_CAD985.PR_CON_Combustivel(:PM_DESCCOMBUSTIVEL,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value

End Sub

Private Sub grd_DblClick()
    grd.Col = 1
    If grd.Text <> "" Then
        txtCodCombustivel.Text = grd.Text
        grd.Col = 2
        txtCombustivel.Text = grd.Text
        grd.Col = 3
        cmbSituacao.Text = grd.Text
        cmdCancel.Visible = True
    End If
End Sub


Private Function GravarCombustivel(pACT As String, codCombustivel As Integer, descCombustivel As String, Situacao As Integer) As String

    vBanco.Parameters.Remove "PM_ACT"
    vBanco.Parameters.Add "PM_ACT", pACT, 1
    
    vBanco.Parameters.Remove "PM_CODCombustivel"
    vBanco.Parameters.Add "PM_CODCombustivel", codCombustivel, 1
    
    vBanco.Parameters.Remove "PM_DESCCombustivel"
    vBanco.Parameters.Add "PM_DESCCombustivel", descCombustivel, 1
    
    vBanco.Parameters.Remove "PM_SITUACAO"
    vBanco.Parameters.Add "PM_SITUACAO", Situacao, 1
  
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "PRODUCAO.PCK_CAD985.PR_ACT_Combustivel(:PM_ACT,:PM_CODCombustivel,:PM_DESCCombustivel,:PM_SITUACAO,:PM_CODERRO,:PM_TXTERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
    
    If vBanco.Parameters("PM_CODERRO").Value <> 0 Then
        GravarCombustivel = vBanco.Parameters("PM_CODERRO").Value & " - " & vBanco.Parameters("PM_TXTERRO").Value
    Else
        GravarCombustivel = ""
    End If

End Function

Sub carregaGrid()

    BuscaCombustivel
    vVB_Generica_001.CarregaGridTabela grd, vObjOracle, 4

End Sub

