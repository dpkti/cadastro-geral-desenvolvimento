VERSION 4.00
Begin VB.Form frmAlteracao 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Altera��o"
   ClientHeight    =   3090
   ClientLeft      =   1470
   ClientTop       =   2610
   ClientWidth     =   6720
   Height          =   3495
   Left            =   1410
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3090
   ScaleWidth      =   6720
   Top             =   2265
   Width           =   6840
   Begin VB.TextBox txtDescricao 
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   3360
      MaxLength       =   10
      TabIndex        =   1
      Top             =   1080
      Width           =   1215
   End
   Begin VB.TextBox txtCodigo 
      Enabled         =   0   'False
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   3360
      MaxLength       =   2
      TabIndex        =   0
      Top             =   480
      Width           =   375
   End
   Begin Threed.SSFrame SSFrame1 
      Height          =   975
      Left            =   1920
      TabIndex        =   2
      Top             =   1800
      Width           =   2895
      _Version        =   65536
      _ExtentX        =   5106
      _ExtentY        =   1720
      _StockProps     =   14
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin Threed.SSCommand SSCommand1 
         Height          =   495
         Left            =   360
         TabIndex        =   5
         Top             =   360
         Width           =   495
         _Version        =   65536
         _ExtentX        =   873
         _ExtentY        =   873
         _StockProps     =   78
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FRMALTER.frx":0000
      End
      Begin Threed.SSCommand SSCommand2 
         Height          =   495
         Left            =   1200
         TabIndex        =   4
         Top             =   360
         Width           =   495
         _Version        =   65536
         _ExtentX        =   873
         _ExtentY        =   873
         _StockProps     =   78
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FRMALTER.frx":031A
      End
      Begin Threed.SSCommand SSCommand3 
         Height          =   495
         Left            =   2040
         TabIndex        =   3
         Top             =   360
         Width           =   495
         _Version        =   65536
         _ExtentX        =   873
         _ExtentY        =   873
         _StockProps     =   78
         ForeColor       =   -2147483640
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FRMALTER.frx":0634
      End
   End
   Begin VB.Label Label1 
      Caption         =   "C�digo"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   2040
      TabIndex        =   7
      Top             =   480
      Width           =   735
   End
   Begin VB.Label Label2 
      Caption         =   "Descri��o"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   2040
      TabIndex        =   6
      Top             =   1080
      Width           =   975
   End
End
Attribute VB_Name = "frmAlteracao"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
   MDIForm1.ssmsg.Caption = ""
End Sub


Private Sub SSCommand1_Click()

  On Error GoTo trataErro

    
     If frmAlteracao.txtDescricao.Text = "" Then
       MsgBox "Entre com todas as informa��es", vbExclamation, "Aten��o"
       frmAlteracao.txtDescricao.SetFocus
       Exit Sub
     End If
         
     Screen.MousePointer = 11
     oradatabase.Parameters.Remove "cod"
     oradatabase.Parameters.Add "cod", frmAlteracao.txtcodigo.Text, 1
     oradatabase.Parameters.Remove "desc"
     oradatabase.Parameters.Add "desc", frmAlteracao.txtDescricao.Text, 1
     oradatabase.Parameters.Remove "cod_errora"
     oradatabase.Parameters.Add "cod_errora", 0, 2
     oradatabase.Parameters.Remove "txt_errora"
     oradatabase.Parameters.Add "txt_errora", "", 2
          
     pl = "BEGIN"
     pl = pl & " Update montadora set "
     pl = pl & " desc_montadora = :desc where cod_montadora = :cod;"
     pl = pl & " COMMIT;"
     pl = pl & "EXCEPTION"
     pl = pl & " WHEN OTHERS THEN"
     pl = pl & " ROLLBACK;"
     pl = pl & " :cod_errora := SQLCODE;"
     pl = pl & " :txt_errora := SQLERRM;"
     pl = pl & "END;"
  
     oradatabase.ExecuteSQL pl
  
    If Val(oradatabase.Parameters("cod_errora").Value) <> 0 Then
      cod_errora = oradatabase.Parameters("cod_errora").Value
      txt_errora = oradatabase.Parameters("txt_errora").Value
      MsgBox "Ocorreu o erro: " & cod_errora & "-" & txt_errora, vbExclamation, "Aten��o"
      Exit Sub
    Else
      MsgBox "Altera��o OK ", vbInformation, "Aten��o"
      Unload Me
      
    End If
  
  Screen.MousePointer = 0
  Exit Sub
  
trataErro:
    MsgBox "Ocorreu o erro: " & Err & "-" & Err.Description, vbExclamation, "Aten��o"


End Sub


Private Sub SSCommand1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
  MDIForm1.ssmsg.Caption = "Salvar a Altera��o"
End Sub


Private Sub SSCommand2_Click()
  frmAlteracao.txtDescricao.Text = ""
End Sub


Private Sub SSCommand2_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
  MDIForm1.ssmsg.Caption = "Limpar a Tela"
End Sub


Private Sub SSCommand3_Click()
  Unload Me
End Sub


Private Sub SSCommand3_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
  MDIForm1.ssmsg.Caption = "Sair do M�dulo de Altera��o"
End Sub

Private Sub txtDescricao_KeyPress(KeyAscii As Integer)
  KeyAscii = Maiusculo(KeyAscii)
End Sub


