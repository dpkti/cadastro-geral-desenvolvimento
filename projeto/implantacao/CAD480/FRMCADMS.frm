VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Begin VB.Form FRMCADMS 
   Caption         =   "Cadastro de Mensagem:"
   ClientHeight    =   3060
   ClientLeft      =   3135
   ClientTop       =   3630
   ClientWidth     =   6810
   Icon            =   "FRMCADMS.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   3060
   ScaleWidth      =   6810
   StartUpPosition =   1  'CenterOwner
   Begin ComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   660
      Left            =   0
      TabIndex        =   6
      Top             =   0
      Width           =   6810
      _ExtentX        =   12012
      _ExtentY        =   1164
      ButtonWidth     =   1032
      ButtonHeight    =   1005
      Appearance      =   1
      ImageList       =   "ImageList1"
      _Version        =   327682
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   8
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button2 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   "TBConsultar"
            Object.ToolTipText     =   "Consultar Uf�s:"
            Object.Tag             =   ""
            ImageIndex      =   1
         EndProperty
         BeginProperty Button3 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button4 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   "TBConfirma"
            Object.ToolTipText     =   "Confirma Dados:"
            Object.Tag             =   ""
            ImageIndex      =   2
         EndProperty
         BeginProperty Button5 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button6 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   "TBCancela"
            Object.ToolTipText     =   "Cancela:"
            Object.Tag             =   ""
            ImageIndex      =   3
         EndProperty
         BeginProperty Button7 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button8 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   "TBVoltar"
            Object.ToolTipText     =   "Voltar para tela anterior:"
            Object.Tag             =   ""
            ImageIndex      =   4
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton Command1 
      Cancel          =   -1  'True
      Caption         =   "Sair"
      Height          =   525
      Left            =   2783
      TabIndex        =   7
      ToolTipText     =   "Sair da Consulta:"
      Top             =   4860
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.Frame fraManutencao 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2295
      Left            =   98
      TabIndex        =   0
      Top             =   720
      Width           =   6615
      Begin VB.TextBox txtDescMsg 
         Height          =   285
         Left            =   120
         MaxLength       =   60
         TabIndex        =   4
         Top             =   1830
         Width           =   6405
      End
      Begin VB.TextBox txtCodMsg 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   120
         MaxLength       =   2
         TabIndex        =   3
         Top             =   990
         Width           =   465
      End
      Begin VB.Label lblDescMsg 
         Caption         =   "Mensagem:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   120
         TabIndex        =   2
         Top             =   1470
         Width           =   1515
      End
      Begin VB.Label lblCodMsg 
         Caption         =   "C�digo:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   120
         TabIndex        =   1
         Top             =   600
         Width           =   1035
      End
   End
   Begin MSFlexGridLib.MSFlexGrid Grid1 
      Height          =   4680
      Left            =   158
      TabIndex        =   5
      Top             =   60
      Visible         =   0   'False
      Width           =   6495
      _ExtentX        =   11456
      _ExtentY        =   8255
      _Version        =   393216
      FixedCols       =   0
      FormatString    =   $"FRMCADMS.frx":0442
   End
   Begin ComctlLib.ImageList ImageList1 
      Left            =   7560
      Top             =   840
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   4
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FRMCADMS.frx":0563
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FRMCADMS.frx":087D
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FRMCADMS.frx":0B97
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FRMCADMS.frx":0EB1
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Menu Consulta 
      Caption         =   "&Consulta"
   End
   Begin VB.Menu Confirma 
      Caption         =   "C&onfirma"
   End
   Begin VB.Menu Cancela 
      Caption         =   "C&ancela"
   End
   Begin VB.Menu Sair1 
      Caption         =   "&Sair"
   End
   Begin VB.Menu Sair2 
      Caption         =   "&Sair"
   End
   Begin VB.Menu RBPopup2 
      Caption         =   "RBPopup2"
      Visible         =   0   'False
      Begin VB.Menu PPLimpar 
         Caption         =   "Limpar Campos"
      End
      Begin VB.Menu PPSeparator 
         Caption         =   "-"
      End
      Begin VB.Menu PPConsulta 
         Caption         =   "Consulta"
      End
      Begin VB.Menu PPConfirmar 
         Caption         =   "Confirmar"
      End
      Begin VB.Menu PPCancelar 
         Caption         =   "Cancelar"
      End
      Begin VB.Menu PPSeparator2 
         Caption         =   "-"
      End
      Begin VB.Menu PPVoltar 
         Caption         =   "Voltar"
      End
   End
End
Attribute VB_Name = "FRMCADMS"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Cancela_Click() 'menu
    
    Call Limpa_Campos
    
End Sub

Private Sub Command1_Click()
        
        Call Sair2_Click
        
End Sub

Private Sub Confirma_Click()    'menu
    On Error GoTo TRATA_ERRO
    
    Call Confirma_Campos 'CONFERE SE OS CAMPOS EST�O PREENCHIDOS - N�O PODE HAVER CAMPO EM BRANCO
    
    If fl_erro = True Then
        Exit Sub
    End If
    
    db.Parameters.Remove "Cod_ErrorPl"
    db.Parameters.Remove "Txt_ErrorPl"
    db.Parameters.Remove "COD_MENSAGEM"
    db.Parameters.Remove "DESC_MENSAGEM"
    
    db.Parameters.Add "Cod_ErrorPl", 0, 2
    db.Parameters.Add "Txt_ErrorPl", "", 2
    db.Parameters.Add "COD_MENSAGEM", txtCodMsg.Text, 1
    db.Parameters.Add "DESC_MENSAGEM", txtDescMsg.Text, 1
    
    CmdSql = "BEGIN "
        
    If fl_alteracao = True Then  'CASO SEJA ALTERA��O
    
        CmdSql = CmdSql & "UPDATE NAT_MENSAGEM SET " & _
                          "DESC_MENSAGEM = :DESC_MENSAGEM " & _
                          "WHERE COD_MENSAGEM = :COD_MENSAGEM; "
        
        CmdSql = CmdSql & "Commit; " & _
                          "Exception " & _
                          "When Others then " & _
                          "RollBack; " & _
                          ":Cod_ErrorPl := SQLCODE; " & _
                          ":Txt_ErrorPl := SQLERRM; " & _
                          "End;"
    
        Dim r As String
        r = MsgBox("Confirma altera��o?", vbYesNo + vbQuestion)
            
        If r = vbNo Then    'SE N�O CONFIRMA ALTERA��O, LIMPA CAMPOS E SAI
            Limpa_Campos
            Exit Sub
        Else
            Screen.MousePointer = 11
            db.ExecuteSQL CmdSql
        End If
        
        If Val(db.Parameters("Cod_ErrorPl")) <> 0 Then
            Screen.MousePointer = 0
            MsgBox "ERRO - " & db.Parameters("Txt_ErrorPl") & ". FAVOR AVISAR DEPTO. DE SISTEMAS!", vbExclamation, MSG_TIT
        Else
            Screen.MousePointer = 0
            Limpa_Campos
        End If
        
    Else                   'CASO SEJA INCLUS�O
        CmdSql = CmdSql & "INSERT INTO NAT_MENSAGEM (COD_MENSAGEM, " & _
                          "DESC_MENSAGEM) VALUES(:COD_MENSAGEM, :DESC_MENSAGEM);"
        CmdSql = CmdSql & "Commit; " & _
                          "Exception " & _
                          "When Others then " & _
                          "RollBack; " & _
                          ":Cod_ErrorPl := SQLCODE; " & _
                          ":Txt_ErrorPl := SQLERRM; " & _
                          "End;"
                          
        r = MsgBox("Confirma Inclus�o?", vbYesNo + vbQuestion)
            
        ' SE N�O CONFIRMA ALTERA��O, LIMPA CAMPOS E SAI
        If r = vbNo Then
            Limpa_Campos
            Exit Sub
        Else
            Screen.MousePointer = 11
            db.ExecuteSQL CmdSql
        End If
        
        If Val(db.Parameters("Cod_ErrorPl")) <> 0 Then
            Screen.MousePointer = 0
            MsgBox "ERRO - " & db.Parameters("Txt_ErrorPl") & ". FAVOR AVISAR DEPTO. DE SISTEMAS!", vbExclamation, MSG_TIT
        Else
            Screen.MousePointer = 0
            Limpa_Campos
        End If
                          
    End If
    
    db.Parameters.Remove "Cod_ErrorPl"
    db.Parameters.Remove "Txt_ErrorPl"
    db.Parameters.Remove "COD_MENSAGEM"
    db.Parameters.Remove "DESC_MENSAGEM"
    
    Call Sequencia
    
    Screen.MousePointer = 0
    
    Exit Sub
    
TRATA_ERRO:
        Call Process_Line_Errors
End Sub

Private Sub Consulta_Click()
    Dim Pesquisa As String
        
        'BUSCA MENSAGEM POR DESCRI��O
        
        Pesquisa = UCase(InputBox("Digite parte da descri��o da mensagem a ser pesquisada:", MSG_TIT))
        
        If Trim(Pesquisa) = "" Then   ' CASO N�O SE TENHA
            Exit Sub                  '   DIGITADO NADA
        
        Else
            Pesquisa = "%" & Pesquisa & "%"
            Screen.MousePointer = 11
           
            db.Parameters.Remove "DESC_MENSAGEM"
            db.Parameters.Add "DESC_MENSAGEM", Pesquisa, 1
            
            CmdSql = "SELECT * FROM NAT_MENSAGEM WHERE " & _
                     "DESC_MENSAGEM LIKE :DESC_MENSAGEM ORDER BY DESC_MENSAGEM"
        
            Set db_CONS = db.dbcreatedynaset(CmdSql, 0&)
            
            db.Parameters.Remove "DESC_MENSAGEM"
           
            If db_CONS.EOF = True Then
                Screen.MousePointer = 0
                MsgBox "N�o existem mensagens com essa descri��o", vbCritical, MSG_TIT
                Exit Sub
            
            Else  'CASO ENCONTRE ALGUM C�DIGO V�LIDO
                
                fraManutencao.Visible = False
                Toolbar1.Visible = False
                Command1.Visible = True
                Grid1.Visible = True
                'MENU
                    '1�BLOCO
                        Consulta.Visible = False
                        Confirma.Visible = False
                        Cancela.Visible = False
                        Sair1.Visible = False
                    '2� BLOCO
                        Sair2.Visible = True
                        FRMCADMS.Height = 6210
                'FIM DO TRATAMENTO DE MENUS
                  
                  'MONTA O GRID NA TELA
                FRMCADMS.Caption = "Consulta Mensagem"
                Grid1.Rows = (db_CONS.RecordCount() + 1) ' N�MERO DE LINHAS
                
                Dim I As Integer
                For I = 1 To (Grid1.Rows - 1) Step 1     ' CARREGA O GRID
                    Grid1.Row = I
                    Grid1.Col = 0
                    Grid1.Text = db_CONS!cod_mensagem
                    Grid1.Col = 1
                    Grid1.Text = db_CONS!DESC_MENSAGEM
                    db_CONS.MoveNext
                    SendKeys "^{HOME}"
                Next
            
            End If
        
        End If
        
        txtCodMsg.Text = ""
        txtDescMsg.Text = ""
        
        Screen.MousePointer = 0
End Sub

Private Sub Form_Load()
 
Call Sequencia
Screen.MousePointer = 0

'MENUS
    FRMCADMS.Sair2.Visible = False
'FIM DO TRATAMENTO DE MENUS

End Sub
Sub Sequencia() 'seta campo "c�digo" para "�ltimo registro" + 1

    Screen.MousePointer = 11
    CmdSql = "SELECT MAX(COD_MENSAGEM) seq  FROM NAT_MENSAGEM"
    Set db_CONS = db.dbcreatedynaset(CmdSql, 0&)
    Contador = CStr(db_CONS!seq) + 1
    txtCodMsg.Text = Contador
    Screen.MousePointer = 0
    fraManutencao.Caption = "Inclus�o"

End Sub

Sub Confirma_Campos() 'n�o permite que nenhum campo esteja vazio

    fl_erro = False
    
    If Trim(txtCodMsg.Text) = "" Then
        fl_erro = True
        MsgBox "Favor digitar c�digo da mensagem", vbExclamation, MSG_TIT
        Call Sequencia
        Exit Sub
    ElseIf Trim(txtDescMsg.Text) = "" Then
        fl_erro = True
        MsgBox "Favor digitar conte�do da mensagem", vbExclamation, MSG_TIT
        txtDescMsg.SetFocus
        Exit Sub
    End If

End Sub

Sub Limpa_Campos() 'limpa campos do form para nova digita��o

    txtCodMsg.Text = ""
    txtDescMsg.Text = ""
    fraManutencao.Caption = ""
    Call Sequencia
    txtCodMsg.Enabled = True
    txtCodMsg.SetFocus
    

End Sub

Private Sub fraManutencao_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)

If Button = vbRightButton Then
    FRMCADMS.PopupMenu RBPopup2
End If


End Sub


Private Sub Grid1_DblClick()

    Grid1.Col = 0
    txtCodMsg.Text = Grid1.Text
    Grid1.Col = 1
    txtDescMsg.Text = Grid1.Text
    
    Command1.Visible = False
        Grid1.Visible = False
        fraManutencao.Visible = True
        Toolbar1.Visible = True
        'MENU
                '1�BLOCO
                    Consulta.Visible = True
                    Confirma.Visible = True
                    Cancela.Visible = True
                    Sair1.Visible = True
                '2� BLOCO
                    Sair2.Visible = False
                'FIM DO TRATAMENTO DE MENUS
        fraManutencao.Caption = "Manuten��o de Mensagem"
        FRMCADMS.Height = 3750
        
        txtCodMsg.Enabled = False

End Sub
    
Private Sub PPCancelar_Click()
Call Cancela_Click
End Sub

Private Sub PPConfirmar_Click()
Call Confirma_Click
End Sub


Private Sub PPConsulta_Click()
Call Consulta_Click
End Sub


Private Sub PPLimpar_Click()
Call Limpa_Campos
End Sub


Private Sub PPVoltar_Click()
Call Sair1_Click
End Sub

Private Sub Sair1_Click()   'menu
    
    Unload Me
    
End Sub

Private Sub Sair2_Click()   'menu
        Command1.Visible = False
        Grid1.Visible = False
        fraManutencao.Visible = True
        Toolbar1.Visible = True
        'MENU
                '1�BLOCO
                    Consulta.Visible = True
                    Confirma.Visible = True
                    Cancela.Visible = True
                    Sair1.Visible = True
                '2� BLOCO
                    Sair2.Visible = False
                'FIM DO TRATAMENTO DE MENUS
        fraManutencao.Caption = "Manuten��o de Mensagem"
        FRMCADMS.Height = 3750
        txtCodMsg.SetFocus
                
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As ComctlLib.Button)
    
    Select Case Button.Key
    
        Case "TBConsultar"
        
            Call Consulta_Click
        
        Case "TBConfirma"
        
            Call Confirma_Click
        
        Case "TBCancela"
        
            Call Limpa_Campos
        
        Case "TBVoltar"
        
            Unload Me
    
    End Select
    
End Sub

Private Sub txtCodMsg_GotFocus()

    txtCodMsg.SelStart = 0
    txtCodMsg.SelLength = Len(txtCodMsg.Text)
    
End Sub

Private Sub txtCodMsg_KeyPress(KeyAscii As Integer)
    KeyAscii = Valor(KeyAscii, FRMCADMS.txtCodMsg.Text)
End Sub

Private Sub txtCodMsg_LostFocus()

    fl_alteracao = False
    
    If Trim(txtCodMsg.Text) <> "" Then 'CASO TENHA SE DIGITADO ALGUMA COISA
        
        If CInt(txtCodMsg.Text) > Contador Then
            txtCodMsg.Text = Contador
            txtCodMsg.SetFocus
            Exit Sub
        End If
            
        db.Parameters.Remove "COD_MENSAGEM"
        db.Parameters.Add "COD_MENSAGEM", txtCodMsg.Text, 1
        
        CmdSql = "select * from NAT_MENSAGEM " & _
                 "WHERE COD_MENSAGEM = :COD_MENSAGEM"
        
        Set db_CONS = db.dbcreatedynaset(CmdSql, 0&)
        
        db.Parameters.Remove "COD_MENSAGEM"
    
        If Not db_CONS.EOF Then    'CASO TENHA ACHADO ALGUM, TRATA-SE DE UMA ALTERA��O
            fl_alteracao = True
            fraManutencao.Caption = "Altera��o"
            txtDescMsg.Text = db_CONS!DESC_MENSAGEM
            txtCodMsg.Enabled = False
        Else                       'CASO N�O TENHA ACHADO, TRATA-SE DE UMA INCLUS�O
            fl_alteracao = False
            txtDescMsg.Text = ""
            fraManutencao.Caption = "Inclus�o"
        End If
    
    End If

End Sub

Private Sub txtDescMsg_GotFocus()

    txtDescMsg.SelStart = 0
    txtDescMsg.SelLength = Len(txtDescMsg.Text)

End Sub

Private Sub txtDescMsg_KeyPress(KeyAscii As Integer)
    KeyAscii = Maiusculo(KeyAscii)
End Sub
