VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.MDIForm MDICad480 
   BackColor       =   &H8000000C&
   Caption         =   "CAD480 - CADASTRO DE MENSAGENS"
   ClientHeight    =   5535
   ClientLeft      =   60
   ClientTop       =   630
   ClientWidth     =   7410
   Icon            =   "MDICad480.frx":0000
   LinkTopic       =   "MDIForm1"
   StartUpPosition =   1  'CenterOwner
   WindowState     =   2  'Maximized
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   405
      Left            =   0
      TabIndex        =   1
      Top             =   5130
      Width           =   7410
      _ExtentX        =   13070
      _ExtentY        =   714
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   12568
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   660
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   7410
      _ExtentX        =   13070
      _ExtentY        =   1164
      ButtonWidth     =   1032
      ButtonHeight    =   1005
      Appearance      =   1
      ImageList       =   "ImageList1"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   6
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "TBCadastro"
            Object.ToolTipText     =   "Cadastro de Mensagens:"
            ImageIndex      =   3
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "TBSobre"
            Object.ToolTipText     =   "Informa��es Sobre o Progama:"
            ImageIndex      =   1
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "TBSair"
            Object.ToolTipText     =   "Sair do Programa:"
            ImageIndex      =   2
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   6780
      Top             =   720
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDICad480.frx":0442
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDICad480.frx":0B96
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDICad480.frx":0EB2
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Menu Cadastro 
      Caption         =   "&Cadastro"
      Begin VB.Menu CadastrodeMensagem 
         Caption         =   "&Cadastro de Mensagem"
      End
   End
   Begin VB.Menu Sobre 
      Caption         =   "&Sobre"
   End
   Begin VB.Menu Sair 
      Caption         =   "S&air"
   End
   Begin VB.Menu RBpopup1 
      Caption         =   "RBPopup1"
      Visible         =   0   'False
      Begin VB.Menu PPCad 
         Caption         =   "Cadastro"
         Begin VB.Menu PPCadMsg 
            Caption         =   "Cadastro de Mensagens"
         End
      End
      Begin VB.Menu PPSobre 
         Caption         =   "Sobre"
      End
      Begin VB.Menu PPSeparator 
         Caption         =   "-"
      End
      Begin VB.Menu PPSair 
         Caption         =   "Sair"
      End
   End
End
Attribute VB_Name = "MDICad480"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub CadastrodeMensagem_Click()
    
    FRMCADMS.Show vbModal
    
End Sub

Private Sub MDIForm_Load()
    

    On Error GoTo TRATA_ERRO
        
        MSG_TIT = "CAD480 - Cadastro de Mensagem"
        
        If App.PrevInstance = True Then
            MsgBox "Este programa j� est� aberto.", vbExclamation, MSG_TIT
            End
        End If
    
        'WindowState = 2    'POSICIONAR TELA
    
       
        '//////////////////////////////////////////
        '///  ESTABELECE CONEX�O COM O ORACLE  ///
        '//////////////////////////////////////////
    
       Set dbOra = CreateObject("oracleinproCServer.xorasession")
       'Set db = dbOra.OpenDatabase("PRODUCAO", "PRODUCAO/DPK", 0&)
       Set db = dbOra.OpenDatabase("DESENV", "PRODUCAO/DES", 0&)
    
        '8 -> PARA UMA LINHA
        '0 -> PARA VARIAS LINHAS
          
        Exit Sub
       
TRATA_ERRO:
        
        Call Process_Line_Errors
    
    
End Sub

Private Sub MDIForm_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)

If Button = vbRightButton Then
    MDICad480.PopupMenu RBpopup1
End If

End Sub

Private Sub PPCadMsg_Click()
FRMCADMS.Show vbModal
End Sub

Private Sub PPSair_Click()
End
End Sub

Private Sub PPSobre_Click()
frmSobre.Show vbModal
End Sub

Private Sub Sair_Click()
    End
End Sub

Private Sub Sobre_Click()

    frmSobre.Show vbModal
    
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case Button.Key
        
        Case "TBCadastro"
            Call CadastrodeMensagem_Click
        Case "TBSobre"
            Call Sobre_Click
        Case "TBSair"
            Call Sair_Click
            
        End Select
            
End Sub
