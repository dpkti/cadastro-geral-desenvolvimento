VERSION 5.00
Begin VB.Form frmSobre 
   Caption         =   "Informações sobre o programa"
   ClientHeight    =   3975
   ClientLeft      =   165
   ClientTop       =   735
   ClientWidth     =   4680
   Icon            =   "frmSobre.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   3975
   ScaleWidth      =   4680
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton Command1 
      Caption         =   "OK!"
      Height          =   525
      Left            =   1673
      TabIndex        =   1
      ToolTipText     =   "Sair das Informações:"
      Top             =   3420
      Width           =   1245
   End
   Begin VB.PictureBox Picture1 
      Height          =   1455
      Left            =   1433
      Picture         =   "frmSobre.frx":0742
      ScaleHeight     =   1395
      ScaleWidth      =   1665
      TabIndex        =   0
      Top             =   30
      Width           =   1725
   End
   Begin VB.Label Label1 
      Caption         =   "Programa:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   15
      TabIndex        =   7
      Top             =   1590
      Width           =   885
   End
   Begin VB.Label Label3 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "CAD480-Cadastro de Mensagens"
      BeginProperty Font 
         Name            =   "Lucida Console"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   15
      TabIndex        =   6
      Top             =   1800
      Width           =   4575
   End
   Begin VB.Label Label2 
      Caption         =   "Responsável:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   15
      TabIndex        =   5
      Top             =   2190
      Width           =   1245
   End
   Begin VB.Label Label4 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Renato "
      BeginProperty Font 
         Name            =   "Lucida Console"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   15
      TabIndex        =   4
      Top             =   2400
      Width           =   4575
   End
   Begin VB.Label Label5 
      Caption         =   "Contato:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   0
      TabIndex        =   3
      Top             =   2790
      Width           =   735
   End
   Begin VB.Label Label6 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "supsist@dpk.com.br"
      BeginProperty Font 
         Name            =   "Lucida Console"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   15
      TabIndex        =   2
      ToolTipText     =   "supsist@dpk.com.br"
      Top             =   3000
      Width           =   4575
   End
   Begin VB.Menu mnuSair 
      Caption         =   "&Sair"
   End
End
Attribute VB_Name = "frmSobre"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Command1_Click()
Call mnuSair_Click
End Sub

Private Sub mnuSair_Click()
Unload Me
End Sub
