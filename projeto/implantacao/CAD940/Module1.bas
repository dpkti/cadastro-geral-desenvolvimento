Attribute VB_Name = "Module1"


Public Sub Limpar(frm As Form)
    Dim Mascara
    Dim Controle As Control
    For Each Controle In frm.Controls
        If TypeOf Controle Is TextBox Then
            Controle.Text = Empty
        
        ElseIf TypeOf Controle Is MaskEdBox Then
            Mascara = Controle.Mask
            Controle.Mask = ""
            Controle.Text = ""
            Controle.Mask = Mascara
        ElseIf TypeOf Controle Is ComboBox Then
            If Controle.Tag <> "N�o Limpar" Then
                If Controle.Style = 0 Then
                    Controle.Text = Empty
                ElseIf Controle.Style = 2 Then
                    Controle.ListIndex = -1
                End If
            End If
        ElseIf TypeOf Controle Is CheckBox Then
            Controle.Value = 0
        ElseIf TypeOf Controle Is ListBox Then
            Controle.Clear
        ElseIf TypeOf Controle Is PictureBox Then
            Controle.Picture = LoadPicture("")
        ElseIf TypeOf Controle Is ListView Then
            Controle.ListItems.Clear
        ElseIf TypeOf Controle Is MSFlexGrid Then
            Controle.Clear
        End If
    Next
End Sub

