VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmConsultar 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Consultar Fornecedor"
   ClientHeight    =   6150
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5430
   Icon            =   "frmConsultar.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   410
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   362
   Begin VB.Frame Frame1 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   3345
      Left            =   45
      TabIndex        =   8
      Top             =   2430
      Width           =   5325
      Begin MSFlexGridLib.MSFlexGrid mfgConsulta 
         Height          =   2580
         Left            =   90
         TabIndex        =   9
         Top             =   315
         Width           =   5100
         _ExtentX        =   8996
         _ExtentY        =   4551
         _Version        =   393216
         BackColorBkg    =   -2147483633
         AllowUserResizing=   1
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         Caption         =   "Duplo click no Fabricante para Editar"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   225
         Left            =   675
         TabIndex        =   10
         Top             =   2970
         Width           =   4020
      End
   End
   Begin VB.Frame fra 
      Caption         =   "Filtro"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1455
      Left            =   45
      TabIndex        =   3
      Top             =   900
      Width           =   5325
      Begin VB.TextBox txtSigla 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1620
         TabIndex        =   5
         Top             =   360
         Width           =   1725
      End
      Begin VB.ComboBox cmbSituacao 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1620
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   855
         Width           =   1725
      End
      Begin Bot�o.cmd cmd1 
         Height          =   420
         Left            =   3825
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   810
         Width           =   1140
         _ExtentX        =   2011
         _ExtentY        =   741
         BTYPE           =   3
         TX              =   "Consultar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmConsultar.frx":23D2
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         Caption         =   "Sigla Fornecedor:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   90
         TabIndex        =   7
         Top             =   405
         Width           =   1455
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         Caption         =   "Situa��o:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   720
         TabIndex        =   6
         Top             =   900
         Width           =   780
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   5820
      Width           =   5430
      _ExtentX        =   9578
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   9525
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      Top             =   810
      Width           =   5325
      _ExtentX        =   9393
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsultar.frx":23EE
      PICN            =   "frmConsultar.frx":240A
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmConsultar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmd1_Click()
Dim vSITUACAO

    Screen.MousePointer = 11
    
    mfgConsulta.Clear
    
    If cmbSituacao = "" Then
        vSITUACAO = Null
    Else
        vSITUACAO = Mid(cmbSituacao, 1, 1)
    End If
       
    vSql = "PRODUCAO.PCK_CAD940.pr_CON_FABRICANTE(:PM_CODFABRICANTE,:PM_SIGLA,:PM_SITUACAO,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
     
    vBanco.Parameters.Remove "PM_CODFABRICANTE"
    vBanco.Parameters.Add "PM_CODFABRICANTE", Null, 1
    
    vBanco.Parameters.Remove "PM_SIGLA"
    vBanco.Parameters.Add "PM_SIGLA", txtSigla, 1
    
    vBanco.Parameters.Remove "PM_SITUACAO"
    vBanco.Parameters.Add "PM_SITUACAO", vSITUACAO, 1
    
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
     
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
          
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
    
    If vObjOracle.EOF And vObjOracle.BOF Then
        Screen.MousePointer = vbDefault
        MsgBox "N�o Fornecedores cadastrados para esta consulta", vbInformation, "Aten��o"
        Screen.MousePointer = 0
        Exit Sub
    End If
    
    vVB_Generica_001.CarregaGridTabela mfgConsulta, vObjOracle, 4
       
    Screen.MousePointer = 0


End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub

Private Sub Form_Load()
    
    Me.Top = 2000
    Me.Left = 1000
    
    cmbSituacao.AddItem ""
    cmbSituacao.AddItem "0 - Ativo"
    cmbSituacao.AddItem "9 - Inativo"
    
End Sub

Private Sub mfgConsulta_DblClick()

    Linha = mfgConsulta.Row
    
    frmEditar.lblLinha.Caption = Linha
  
    With mfgConsulta
    
        .Col = 1
        frmEditar.lblCODFABRICANTE.Caption = .Text
        
        .Col = 2
        frmEditar.txtSigla.Text = .Text
        
        .Col = 3
        vSITUACAO = .Text
        
        If vSITUACAO = 0 Then
            frmEditar.cmbSituacao.ListIndex = 1
        ElseIf vSITUACAO = 9 Then
            frmEditar.cmbSituacao.ListIndex = 2
        End If
        
    End With

    frmEditar.Show 1

End Sub
