VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "gradient.ocx"
Begin VB.Form frmEditar 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Alterar/Excluir Fornecedor"
   ClientHeight    =   3255
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5430
   Icon            =   "frmEditar.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   217
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   362
   Begin VB.Frame fra 
      Caption         =   "Dados Fornecedor"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1905
      Left            =   45
      TabIndex        =   3
      Top             =   945
      Width           =   5325
      Begin VB.TextBox txtSigla 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1710
         TabIndex        =   5
         Top             =   900
         Width           =   1725
      End
      Begin VB.ComboBox cmbSituacao 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1710
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   1395
         Width           =   1725
      End
      Begin VB.Label lblLinha 
         Appearance      =   0  'Flat
         Caption         =   "Label1"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   4500
         TabIndex        =   12
         Top             =   1530
         Visible         =   0   'False
         Width           =   690
      End
      Begin VB.Label lblCODFABRICANTE 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   1755
         TabIndex        =   11
         Top             =   450
         Width           =   1680
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         Caption         =   "Cod. Fornecedor:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   180
         TabIndex        =   10
         Top             =   450
         Width           =   1455
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         Caption         =   "Sigla Fornecedor:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   180
         TabIndex        =   7
         Top             =   945
         Width           =   1455
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         Caption         =   "Situa��o:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   810
         TabIndex        =   6
         Top             =   1440
         Width           =   780
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   2925
      Width           =   5430
      _ExtentX        =   9578
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   9525
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      Top             =   810
      Width           =   6000
      _ExtentX        =   10583
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmEditar.frx":23D2
      PICN            =   "frmEditar.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd1 
      Height          =   690
      Left            =   990
      TabIndex        =   8
      TabStop         =   0   'False
      ToolTipText     =   "Gravar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmEditar.frx":30C8
      PICN            =   "frmEditar.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd2 
      Height          =   690
      Left            =   1755
      TabIndex        =   9
      TabStop         =   0   'False
      ToolTipText     =   "Exclir"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmEditar.frx":3DBE
      PICN            =   "frmEditar.frx":3DDA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmEditar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmd1_Click()
        
        Linha = lblLinha.Caption
    
        vSql = "PRODUCAO.PCK_CAD940.pr_ACT_FABRICANTE(:PM_CODFABRICANTE,:PM_SIGLA,:PM_SITUACAO,:PM_ACT,:PM_CODERRO,:PM_TXTERRO)"
         
        vBanco.Parameters.Remove "PM_CODFABRICANTE"
        vBanco.Parameters.Add "PM_CODFABRICANTE", lblCODFABRICANTE.Caption, 1
        
        vBanco.Parameters.Remove "PM_SIGLA"
        vBanco.Parameters.Add "PM_SIGLA", UCase(txtSigla.Text), 1
        
        vBanco.Parameters.Remove "PM_SITUACAO"
        vBanco.Parameters.Add "PM_SITUACAO", Mid(cmbSituacao, 1, 1), 1
        
        vBanco.Parameters.Remove "PM_ACT"
        vBanco.Parameters.Add "PM_ACT", "A", 1
         
        vBanco.Parameters.Remove "PM_CODERRO"
        vBanco.Parameters.Add "PM_CODERRO", 0, 2
        vBanco.Parameters.Remove "PM_TXTERRO"
        vBanco.Parameters.Add "PM_TXTERRO", "", 2
              
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
         
        vErro = IIf(vBanco.Parameters("PM_CODERRO") = "", 0, vBanco.Parameters("PM_CODERRO"))
         
        If vErro <> 0 Then
            MsgBox vBanco.Parameters("PM_TXTERRO") & ".Ligue para o Depto de Sistemas"
            Screen.MousePointer = 0
            Exit Sub
        End If
        
        frmConsultar.mfgConsulta.TextMatrix(Linha, 2) = txtSigla.Text
        frmConsultar.mfgConsulta.TextMatrix(Linha, 3) = Mid(cmbSituacao, 1, 1)
        
        
        MsgBox "Altera��o OK", , ""
        Unload Me

End Sub

Private Sub cmd2_Click()
Dim vCODFABRI

    vCODFABRI = lblCODFABRICANTE.Caption
    Linha = lblLinha.Caption
        
    strMsg = "Confirma a exclus�o?"
    strResposta = MsgBox(strMsg, vbYesNo, "ATEN��O")
    If strResposta = vbYes Then
    
        vSql = "PRODUCAO.PCK_CAD940.pr_ACT_FABRICANTE(:PM_CODFABRICANTE,:PM_SIGLA,:PM_SITUACAO,:PM_ACT,:PM_CODERRO,:PM_TXTERRO)"
         
        vBanco.Parameters.Remove "PM_CODFABRICANTE"
        vBanco.Parameters.Add "PM_CODFABRICANTE", vCODFABRI, 1
        
        vBanco.Parameters.Remove "PM_SIGLA"
        vBanco.Parameters.Add "PM_SIGLA", Null, 1
        
        vBanco.Parameters.Remove "PM_SITUACAO"
        vBanco.Parameters.Add "PM_SITUACAO", Null, 1
        
        vBanco.Parameters.Remove "PM_ACT"
        vBanco.Parameters.Add "PM_ACT", "D", 1
         
        vBanco.Parameters.Remove "PM_CODERRO"
        vBanco.Parameters.Add "PM_CODERRO", 0, 2
        vBanco.Parameters.Remove "PM_TXTERRO"
        vBanco.Parameters.Add "PM_TXTERRO", "", 2
              
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
         
        vErro = IIf(vBanco.Parameters("PM_CODERRO") = "", 0, vBanco.Parameters("PM_CODERRO"))
         
        If vErro <> 0 Then
            If vErro = -2292 Then
                MsgBox "Este Fabricante possui itens cadastrados. N�o � possivel excluir"
            Else
                MsgBox vBanco.Parameters("PM_TXTERRO") & ".Ligue para o Depto de Sistemas"
            End If
            Screen.MousePointer = 0
            Exit Sub
        End If
        
        If frmConsultar.mfgConsulta.Rows = 2 Then
            For i = 1 To frmConsultar.mfgConsulta.Cols - 1
                frmConsultar.mfgConsulta.TextMatrix(1, i) = ""
            Next
        Else
            frmConsultar.mfgConsulta.RemoveItem Linha
        End If
        
        MsgBox "Exclus�o OK", , ""
        Unload Me
    
    End If

End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    
    Me.Top = 3000
    Me.Left = 3000
    
    cmbSituacao.AddItem ""
    cmbSituacao.AddItem "0 - Ativo"
    cmbSituacao.AddItem "9 - Inativo"
    
End Sub

