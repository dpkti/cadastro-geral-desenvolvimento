VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form frmCadAutom 
   Caption         =   "CAD880 - Cadastro de Equival�ncias  (DPK  x  C�d.F�brica N�o Trabalhado)"
   ClientHeight    =   6360
   ClientLeft      =   1470
   ClientTop       =   1035
   ClientWidth     =   8370
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6360
   ScaleWidth      =   8370
   Begin Threed.SSFrame SSFrame2 
      Height          =   6150
      Left            =   45
      TabIndex        =   0
      Top             =   -15
      Width           =   8235
      _Version        =   65536
      _ExtentX        =   14526
      _ExtentY        =   10848
      _StockProps     =   14
      ForeColor       =   12582912
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin Threed.SSFrame SSFrame3 
         Height          =   1275
         Left            =   105
         TabIndex        =   1
         Top             =   150
         Width           =   7995
         _Version        =   65536
         _ExtentX        =   14102
         _ExtentY        =   2249
         _StockProps     =   14
         Caption         =   "Arquivo"
         ForeColor       =   12582912
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Begin Threed.SSCommand cmdArquivo 
            Height          =   585
            Left            =   120
            TabIndex        =   3
            Top             =   345
            Width           =   795
            _Version        =   65536
            _ExtentX        =   1402
            _ExtentY        =   1032
            _StockProps     =   78
            Picture         =   "frmCadas.frx":0000
         End
         Begin MSComDlg.CommonDialog controle 
            Left            =   120
            Top             =   240
            _ExtentX        =   847
            _ExtentY        =   847
            _Version        =   393216
            DefaultExt      =   "*.PRN"
            InitDir         =   "C:\"
            MaxFileSize     =   32000
         End
         Begin VB.Label lblHelp1 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H0000FFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Abrir Arquivo"
            ForeColor       =   &H80000008&
            Height          =   225
            Left            =   135
            TabIndex        =   4
            Top             =   960
            Visible         =   0   'False
            Width           =   930
         End
         Begin VB.Label lblArquivo 
            BackColor       =   &H00C0FFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H00C00000&
            Height          =   330
            Left            =   990
            TabIndex        =   2
            Top             =   585
            Width           =   4320
         End
      End
      Begin Threed.SSCommand cmdLayout 
         Height          =   630
         Left            =   6405
         TabIndex        =   13
         Top             =   5150
         Width           =   690
         _Version        =   65536
         _ExtentX        =   1217
         _ExtentY        =   1111
         _StockProps     =   78
         Picture         =   "frmCadas.frx":031A
      End
      Begin MSGrid.Grid grdArquivo 
         Height          =   2700
         Left            =   165
         TabIndex        =   9
         Top             =   1530
         Width           =   7875
         _Version        =   65536
         _ExtentX        =   13891
         _ExtentY        =   4762
         _StockProps     =   77
         ForeColor       =   8388608
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Cols            =   7
      End
      Begin Threed.SSCommand cmdVoltar 
         Height          =   630
         Left            =   7395
         TabIndex        =   8
         Top             =   5150
         Width           =   690
         _Version        =   65536
         _ExtentX        =   1217
         _ExtentY        =   1111
         _StockProps     =   78
         Picture         =   "frmCadas.frx":0634
      End
      Begin Threed.SSCommand cmdConfirma 
         Height          =   630
         Left            =   5565
         TabIndex        =   7
         Top             =   5150
         Width           =   690
         _Version        =   65536
         _ExtentX        =   1217
         _ExtentY        =   1111
         _StockProps     =   78
         Picture         =   "frmCadas.frx":094E
      End
      Begin Threed.SSCommand cmdLog 
         Height          =   630
         Left            =   4725
         TabIndex        =   5
         Top             =   5150
         Width           =   690
         _Version        =   65536
         _ExtentX        =   1217
         _ExtentY        =   1111
         _StockProps     =   78
         Picture         =   "frmCadas.frx":0C68
      End
      Begin VB.Label lblMsg3 
         AutoSize        =   -1  'True
         Caption         =   "Mensagem"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   240
         Left            =   150
         TabIndex        =   12
         Top             =   4800
         Width           =   1155
      End
      Begin VB.Label lblMsg2 
         AutoSize        =   -1  'True
         Caption         =   "Mensagem"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   240
         Left            =   150
         TabIndex        =   11
         Top             =   4545
         Width           =   1155
      End
      Begin VB.Label lblMsg 
         AutoSize        =   -1  'True
         Caption         =   "Mensagem"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   240
         Left            =   150
         TabIndex        =   10
         Top             =   4300
         Width           =   1155
      End
      Begin VB.Label lblHelp2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H0000FFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Gerar Arq.de Log"
         ForeColor       =   &H80000008&
         Height          =   225
         Left            =   4740
         TabIndex        =   6
         Top             =   5775
         Visible         =   0   'False
         Width           =   1245
      End
   End
End
Attribute VB_Name = "frmCadAutom"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public I As Integer                'Ponteiro / Contador
Dim vNomeArq As String
Dim varLinhaArquivo As String   'Recebe a linha lida do arquivo texto
Dim fl_Log As Boolean
Dim vGuardaCol As Integer
Dim vGuardaLin As Long

Dim vFL_DPK_OK As Boolean   'Indica se DPK est� Ok ou n�o (se n�o estiver, nem continua demais valida��es
Dim vFABR As String         'Vai guardar o C�d.F�brica n�o trabalhado, lido do arquivo
Dim vFABRNOVO As String     'Vai guardar o C�d.F�brica NOVO - n�o trabalhado, lido do arquivo (ser� usado qdo. for altera��o)
Dim vSIGLA As String        'Vai guardar a string do fornecedor (que a DPK n�o trabalha)
Dim vDPK As String          'Vai guardar o DPK lido do arquivo
Dim vTRAN As String         'Vai guardar o tipo da transa��o (Incl./Alter.)

Dim vFl_Erro As Boolean         'Vai indicar se a grava��o de determinado item, deu erro ou n�o
Sub LIMPA_GRID()
Dim L As Long
Dim C As Integer
Dim vTotLin As Long

vTotLin = grdArquivo.Rows - 1

'Varre todas as linhas do grid
For L = 1 To vTotLin
    'Se for a �ltima linha a ser removida, deleta os campos de cada coluna
    If L = vTotLin Then
        For C = 0 To grdArquivo.Cols - 1
            grdArquivo.Col = C
            grdArquivo.Text = ""
        Next C
    'Sen�o remove o item
    Else
        grdArquivo.RemoveItem 1
    End If
Next L

End Sub







Private Sub cmdArquivo_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp1.Visible = True
End Sub

Private Sub cmdConfirma_Click()

Dim vObs As String            'Guarda o conte�do da coluna de Observa��es
Dim vReg_Gravado As Long      'Guarda a qtd. de itens gravados
Dim vReg_Nao_Gravado As Long  'Guarda a qtd. de itens n�o gravados
Dim vLinSemObs As Long        'Guarda a qtd.de linhas sem observa��o
Dim objconexao As Object      'Guarda resultado do select na LOJA_CONEXAO
Dim vCommit As Long           'Contador de transa��es para fazer commit
Dim K As Long                 'Auxiliar de bind
Dim J As Long
Dim cmdPlSql As String
Dim vLoja As Integer
Dim vDep As String
Dim fl_conexao As Boolean


If MsgBox("Confirma transa��es de C�digos de F�brica N�o Trabalhados?", vbYesNo, "CAD860") = vbYes Then

    Screen.MousePointer = 11

    'Verifica se o grid n�o est� em branco
    grdArquivo.Row = 1
    grdArquivo.Col = 0
    If grdArquivo.Text = "" Then
        Screen.MousePointer = 0
        MsgBox "Nenhuma transa��o ser� efetuada!", vbCritical, "Aten��o!"
        Exit Sub
    End If

    'Primeiro vai percorrer todo o grid p/ ver se tem itens a serem gravados
    CmdPl = ""
    vLinSemObs = 0
    For I = 1 To grdArquivo.Rows - 1
        grdArquivo.Row = I
        grdArquivo.Col = 6
        vObs = grdArquivo.Text
        'Se a coluna de Observa��o n�o contiver nenhuma mensagem de inconsist�ncia vai gravar
        If vObs = "" Then
            vLinSemObs = vLinSemObs + 1
        End If
    Next I
    If vLinSemObs = 0 Then
        Screen.MousePointer = 0
        MsgBox "Nenhuma transa��o poder� ser efetuada!", vbCritical, "Aten��o!"
        Exit Sub
    End If
    
    'Se houverem itens a serem gravados, vai iniciar todo o processo
    'Zera contador de linhas gravadas
    vReg_Gravado = 0
    vReg_Nao_Gravado = 0
    vCommit = 0
    K = 0
    CmdPl = "Begin "

    db.Parameters.Remove "DTCAD": db.Parameters.Add "DTCAD", Format(vDt_Cad, "dd/mm/yy"), 1

    'Pega linha a linha do grid para gravar as que cont�m campo de Observa��o <> ""
    For I = 1 To grdArquivo.Rows - 1
        
        grdArquivo.Row = I
        grdArquivo.Col = 6
        vObs = Trim(grdArquivo.Text)
        
        'Se a coluna de Observa��o n�o contiver nenhuma mensagem de inconsist�ncia vai gravar
        If vObs = "" Then
            
            K = K + 1
            'Alimenta vari�veis com os dados do grid
            grdArquivo.Col = 1: vTRAN = Trim(grdArquivo.Text)
            grdArquivo.Col = 2: vDPK = Trim(grdArquivo.Text)
            grdArquivo.Col = 3: vFABR = Trim(grdArquivo.Text)
            grdArquivo.Col = 4: vFABRNOVO = Trim(grdArquivo.Text)
            grdArquivo.Col = 5: vSIGLA = Trim(grdArquivo.Text)
            
            db.Parameters.Remove "DPK" & K: db.Parameters.Add "DPK" & K, Val(vDPK), 1
            db.Parameters.Remove "FABR" & K: db.Parameters.Add "FABR" & K, vFABR, 1
            db.Parameters.Remove "FABRNOVO" & K: db.Parameters.Add "FABRNOVO" & K, vFABRNOVO, 1
            db.Parameters.Remove "SIGLA" & K: db.Parameters.Add "SIGLA" & K, vSIGLA, 1
            db.Parameters.Remove "Cod_Errora": db.Parameters.Add "Cod_Errora", 0, 2
            db.Parameters.Remove "Txt_Errora": db.Parameters.Add "Txt_Errora", "", 2
            
            If vTRAN = "I" Then
                
                'Vai fazer insert desta forma para evitar erro
                'Inserido campo cod_fornecedor em jan/08 - Alethea
                CmdPl = CmdPl & _
                            "Update PRODUCAO.R_FABRICA_DPK " & _
                               "Set Cod_Fabrica = :FABR" & K & ", " & _
                               "    Sigla = :SIGLA" & K & " " & _
                             "Where Cod_Fabrica = :FABR" & K & " " & _
                               "And Cod_Dpk = :DPK" & K & " " & _
                               "And Fl_Tipo = 'E' " & _
                               "And Cod_Fornecedor = 0; "
                CmdPl = CmdPl & _
                            " If SQL%NOTFOUND Then " & _
                                "Insert into PRODUCAO.R_FABRICA_DPK " & _
                                       "(Cod_Fabrica, Cod_Dpk, Dt_Cadastro, Fl_Tipo, Sigla, Cod_Fornecedor) " & _
                                "Values (:FABR" & K & ", :DPK" & K & ", " & _
                                     "To_Date(:DTCAD,'DD/MM/RR'), 'E', :SIGLA" & K & ",0); " & _
                            " End If; "

            ElseIf vTRAN = "A" Then
                
                CmdPl = CmdPl & _
                        "Update PRODUCAO.R_FABRICA_DPK " & _
                           "Set Cod_Fabrica = :FABRNOVO" & K & ", " & _
                           "    Sigla = :SIGLA" & K & " " & _
                         "Where Cod_Fabrica = :FABR" & K & " " & _
                           "And Cod_Dpk = :DPK" & K & " " & _
                           "And Fl_Tipo = 'E' " & _
                           "And Cod_Fornecedor = 0; "
            
            ElseIf vTRAN = "E" Then
                
                CmdPl = CmdPl & _
                        "Delete PRODUCAO.R_FABRICA_DPK " & _
                         "Where Cod_Fabrica = :FABR" & K & " " & _
                           "And Cod_Dpk = :DPK" & K & " " & _
                           "And Fl_Tipo = 'E' " & _
                           "And Cod_Fornecedor = 0; "
            End If
            
            vCommit = vCommit + 1
            vReg_Gravado = vReg_Gravado + 1
            
            'Commita de 20 em 20 transa��es
            If vCommit = 20 Then
                CmdPl = CmdPl & "Commit; " & _
                        "Exception When Others Then Rollback; " & _
                        ":Cod_Errora := SQLCODE; :Txt_Errora := SQLERRM; End; "
                'Executa transa��es no CD
                db.ExecuteSQL CmdPl
                If Ver_Erro(db.Parameters("Cod_Errora").Value) = True Then
                    Screen.MousePointer = 0
                    MsgBox vERRO & " - " & db.Parameters("Txt_Errora") & Chr(13) & _
                            "Erro ao executar transa��es! "
                    Exit For
                End If
                vCommit = 0
                CmdPl = "Begin "
                K = 1
            End If
        
        Else
            vReg_Nao_Gravado = vReg_Nao_Gravado + 1
        End If
            
    Next I

    CmdPl = CmdPl & "Commit; " & _
                    "Exception When Others Then Rollback; " & _
                    ":Cod_Errora := SQLCODE; :Txt_Errora := SQLERRM; End; "
    'Executa transa��es no CD
    db.ExecuteSQL CmdPl
    If Ver_Erro(db.Parameters("Cod_Errora").Value) = True Then
        Screen.MousePointer = 0
        MsgBox vERRO & " - " & db.Parameters("Txt_Errora") & Chr(13) & _
                "Erro ao executar transa��es! "
        Exit Sub
    Else
        MsgBox "Transa��es efetuadas com sucesso em CAMPINAS!!!", vbInformation, "Aten��o!"
    End If
    
    Screen.MousePointer = 0
    lblMsg2.Visible = True
    lblMsg2 = Format(vReg_Gravado, "000") & IIf(vReg_Gravado = 1, " Transa��o ", " Transa��es ") & "F�brica N/Trab " & _
                    IIf(vReg_Gravado = 1, " Efetuada! ", " Efetuadas! ")
    lblMsg3.Visible = True
    lblMsg3 = Format(vReg_Nao_Gravado, "000") & IIf(vReg_Nao_Gravado = 1, " Transa��o ", " Transa��es ") & "F�brica N/Trab " & _
                    IIf(vReg_Nao_Gravado = 1, " N�o Efetuada! ", " N�o Efetuadas! ") & "!"
    
    If MsgBox("Confirma transa��es on-line?", vbYesNo, "CAD880") = vbYes Then

        Screen.MousePointer = 11
        
        CmdSql = "Select Cod_Loja " & _
                   "From PRODUCAO.DEPOSITO_VISAO " & _
                  "Where Nome_Programa = 'CAD120' " & _
                  "And Cod_Loja not in (Select Cod_Loja From PRODUCAO.DEPOSITO)"
        
        Set db_CONS = db.dbcreatedynaset(CmdSql, 0&)
        
        If db_CONS.EOF Then
            MsgBox "N�o h� CDs cadastrados na tabela DEPOSITO_VISAO!" & Chr(13) & _
                    "Favor avisar analista respons�vel.", vbInformation, "Aten��o!"
            Exit Sub
        End If

        For I = 1 To db_CONS.RecordCount
            
            vLoja = Val(db_CONS!Cod_Loja.Value)
            vDep = "DEP" & Format(db_CONS!Cod_Loja.Value, "00") & "."
            
            'Vou pingar o IP do dep�sito para verificar se vale a pena a conex�o
            If vLoja <> 1 And vLoja <> 2 Then
    
                db.Parameters.Remove "LJ": db.Parameters.Add "LJ", vLoja, 1
                
                cmdPlSql = "Select COD_LOJA, IP, TIME_OUT " & _
                             "From PRODUCAO.LOJA_CONEXAO " & _
                            "Where COD_LOJA = :LJ "
                Set objconexao = db.dbcreatedynaset(cmdPlSql, 0&)
                
                If objconexao.EOF Then
                    MsgBox "CD " & vLoja & " n�o cadastrada na tabela LOJA_CONEXAO! " & Chr(13) & _
                            "Chame o analista respons�vel.", vbCritical, "Aten��o"
                    Exit Sub
                End If
        
                IP = objconexao!IP.Value
                TIMEOUT = objconexao!TIME_OUT.Value
                CDLJ = objconexao!Cod_Loja.Value
                success = Ping(IP, CDLJ, ECHO)
        
                If InStr(GetStatusCode(success), ("ip success")) = 0 Or _
                    ECHO.RoundTripTime > TIMEOUT Then
                    MsgBox "CD " & CDLJ & " sem comunica��o!" & Chr(13) & _
                        "Por favor, tente novamente mais tarde. " & Chr(13) & _
                        "Caso contr�rio a atualiza��o ser� feita somente � noite!!", vbInformation, "CAD180 - ATEN��O!"
                    fl_conexao = False
                Else
                    fl_conexao = True
                End If
                
            End If
    
            'Se valer a pena a conex�o, faz comunica��o e efetua transa��es nos CDs
            If fl_conexao = True Then
        
                CmdPl = "Begin "
                K = 0
                
                'Pega linha a linha do grid para gravar as que cont�m campo de Observa��o <> ""
                For J = 1 To grdArquivo.Rows - 1
                    
                    grdArquivo.Row = J
                    grdArquivo.Col = 6
                    vObs = Trim(grdArquivo.Text)
                    
                    'Se a coluna de Observa��o n�o contiver nenhuma mensagem de inconsist�ncia vai gravar
                    If vObs = "" Then
                           
                        K = K + 1
                        'Alimenta vari�veis com os dados do grid
                        grdArquivo.Col = 1: vTRAN = Trim(grdArquivo.Text)
                        grdArquivo.Col = 2: vDPK = Trim(grdArquivo.Text)
                        grdArquivo.Col = 3: vFABR = Trim(grdArquivo.Text)
                        grdArquivo.Col = 4: vFABRNOVO = Trim(grdArquivo.Text)
                        grdArquivo.Col = 5: vSIGLA = Trim(grdArquivo.Text)
                        
                        db.Parameters.Remove "DPK" & K: db.Parameters.Add "DPK" & K, Val(vDPK), 1
                        db.Parameters.Remove "FABR" & K: db.Parameters.Add "FABR" & K, vFABR, 1
                        db.Parameters.Remove "FABRNOVO" & K: db.Parameters.Add "FABRNOVO" & K, vFABRNOVO, 1
                        db.Parameters.Remove "SIGLA" & K: db.Parameters.Add "SIGLA" & K, vSIGLA, 1
                        db.Parameters.Remove "Cod_Errora": db.Parameters.Add "Cod_Errora", 0, 2
                        db.Parameters.Remove "Txt_Errora": db.Parameters.Add "Txt_Errora", "", 2
                        
                        If vTRAN = "I" Then
                            
                            CmdPl = CmdPl & _
                                "Update " & vDep & "R_FABRICA_DPK " & _
                                   "Set Cod_Fabrica = :FABR" & K & ", " & _
                                   "    Sigla = :SIGLA" & K & " " & _
                                 "Where Cod_Fabrica = :FABR" & K & " " & _
                                   "And Cod_Dpk = :DPK" & K & " " & _
                                   "And Fl_Tipo = 'E' " & _
                                   "And Cod_Fornecedor = 0; "
                            CmdPl = CmdPl & _
                                " If SQL%NOTFOUND Then " & _
                                    "Insert into " & vDep & "R_FABRICA_DPK " & _
                                           "(Cod_Fabrica, Cod_Dpk, Dt_Cadastro, Fl_Tipo, Sigla, Cod_Fornecedor) " & _
                                    "Values (:FABR" & K & ", :DPK" & K & ", " & _
                                         "To_Date(:DTCAD,'DD/MM/RR'), 'E', :SIGLA" & K & ",0); " & _
                                " End If; "
                        
                        ElseIf vTRAN = "A" Then
                            
                            CmdPl = CmdPl & _
                                    "Update " & vDep & "R_FABRICA_DPK " & _
                                       "Set Cod_Fabrica = :FABRNOVO" & K & ", " & _
                                          " Sigla = :SIGLA" & K & " " & _
                                     "Where Cod_Dpk = :DPK" & K & " " & _
                                       "And Cod_Fabrica = :FABR" & K & " " & _
                                       "And Fl_Tipo = 'E' " & _
                                       "And Cod_Fornecedor = 0; "
                        
                        ElseIf vTRAN = "E" Then
                            
                            CmdPl = CmdPl & _
                                    "Delete " & vDep & "R_FABRICA_DPK " & _
                                     "Where Cod_Fabrica = :FABR" & K & " " & _
                                       "And Cod_Dpk = :DPK" & K & " " & _
                                       "And Fl_Tipo = 'E' " & _
                                       "And Cod_Fornecedor = 0; "
                        End If
                        
                        vCommit = vCommit + 1
                        vReg_Gravado = vReg_Gravado + 1
                        
                        'Commita de 20 em 20 transa��es
                        If vCommit = 20 Then
                            CmdPl = CmdPl & "Commit; " & _
                                    "Exception When Others Then Rollback; " & _
                                    ":Cod_Errora := SQLCODE; :Txt_Errora := SQLERRM; End; "
                            'Executa transa��es no CD
                            db.ExecuteSQL CmdPl
                            If Ver_Erro(db.Parameters("Cod_Errora").Value) = True Then
                                Screen.MousePointer = 0
                                MsgBox vERRO & " - " & db.Parameters("Txt_Errora") & Chr(13) & _
                                        "Erro ao executar transa��es! "
                                Exit For
                            End If
                            vCommit = 0
                            CmdPl = "Begin "
                            K = 1
                        End If
                    
                    Else
                        vReg_Nao_Gravado = vReg_Nao_Gravado + 1
                    End If
                        
                Next J
            
                CmdPl = CmdPl & "Commit; " & _
                                "Exception When Others Then Rollback; " & _
                                ":Cod_Errora := SQLCODE; :Txt_Errora := SQLERRM; End; "
                'Executa transa��es no CD
                db.ExecuteSQL CmdPl
                If Ver_Erro(db.Parameters("Cod_Errora").Value) = True Then
                    Screen.MousePointer = 0
                    MsgBox vERRO & " - " & db.Parameters("Txt_Errora") & Chr(13) & _
                            "Erro ao executar transa��es no CD " & Format(vLoja, "00") & "! "
                    Exit Sub
                Else
                    MsgBox "Transa��es efetuadas com sucesso no CD " & Format(vLoja, "00") & "!!!", vbInformation, "Aten��o!"
                End If
                
            End If
            
            db_CONS.MoveNext

        Next I

    End If
    Screen.MousePointer = 0

End If

End Sub
Private Sub cmdConfirma_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp2.Left = cmdConfirma.Left
    lblHelp2.Visible = True
    lblHelp2.Caption = "Gravar Dados"
End Sub



Private Sub cmdLayout_Click()
    frmLayOut.Show vbModal
End Sub

Private Sub cmdLayout_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp2.Left = cmdLayout.Left
    lblHelp2.Visible = True
    lblHelp2.Caption = "Lay-Out Arq."
End Sub


Private Sub cmdLog_Click()

Dim J As Long           'Ponteiro de caracter no campo observa��o
Dim vTamObs As Long     'Guarda a qtd. de caracteres da coluna Observa��o
Dim sOBS As String      'Guarda o conte�do da coluna de Observa��es
Dim sTRAN As String     'Guarda a transa��o
Dim sDPK As String      'Guarda o c�digo de DPK para impress�o
Dim sFABR As String     'Guarda o c�digo de f�brica n/trab
Dim sFABRNOVO As String 'Guarda o novo c�digo de f�brica n/trab
    
    
If MsgBox("Confirma gera��o do arquivo de log?", vbYesNo, "CAD880") = vbYes Then

    'Gera arquivo de log com o nome LODDMMAA.TXT
    'Open "C:\Arquivos-Alethea\CAD880\LOG\Lo" & Mid(vDt_Cad, 1, 2) & _
                        Mid(vDt_Cad, 4, 2) & Mid(vDt_Cad, 7, 2) & ".txt" For Output As #1
    Open "C:\CAD880\LOG\Lo" & Mid(vDt_Cad, 1, 2) & _
                        Mid(vDt_Cad, 4, 2) & Mid(vDt_Cad, 7, 2) & ".txt" For Output As #1
    
    Print #1, "LOG DO ARQUIVO: " & vNomeArq
    Print #1, ""
    Print #1, "Tr DPK   C�d.F�br.N/Trb.     C�d.F�br.N/Trb.Novo Observa��es"
    Print #1, "-- ----- ------------------- ------------------- -------------------------------"
   'Print #1, "CD DPK   Data      Observa��es"
   'Print #1, "-- ----- --------- -------------------------------------------------------------"
    '          01 54357 26-AUG-02
    
    'Pega linha a linha do grid para gravar as que cont�m campo de Observa��o <> ""
    For I = 1 To grdArquivo.Rows - 1
        
        grdArquivo.Row = I
        
        grdArquivo.Col = 1
        sTRAN = Trim(grdArquivo.Text)       'Transa��o
        grdArquivo.Col = 2
        sDPK = Trim(grdArquivo.Text)        'C�digo do DPK
        grdArquivo.Col = 3
        sFABR = Trim(grdArquivo.Text)       'C�d.F�br.n�o trabalhado
        grdArquivo.Col = 4
        sFABRNOVO = Trim(grdArquivo.Text)   'C�d.F�br.n�o trabalhado novo
        grdArquivo.Col = 6
        sOBS = Trim(grdArquivo.Text)        'Observa��es
        
        'Se a coluna de Observa��o n�o estiver em branco
        If sOBS <> "" Then
            
            'Zero ponteiro que vai me indicar que caracter no campo obs. estou lendo
            J = 0
            'Pego o tamanho da observa��o para esta linha
            vTamObs = CLng(Len(sOBS))
            
            'Se o campo de observa��o contiver mais de 31 caracteres, o arquivo de log ter�
            'que conter mais de 1 linha de mensagens para aquele item
            'Conto 31 pois antes da Obs., a 1a. linha ir� conter o Tr-DPK  C�d.F�br....(49 caracteres)
            'E para a impress�o correta do arquivo s� pode ter 80 colunas
            
            'Se for menor ou igual a 31, gravo a linha no arquivo normalmente
            If vTamObs <= 31 Then
                Print #1, sTRAN & String(2 - Len(sTRAN), " ") & " " & _
                          String(5 - Len(sDPK), " ") & sDPK & " " & _
                          sFABR & String(19 - Len(sFABR), " ") & " " & _
                          sFABRNOVO & String(19 - Len(sFABRNOVO), " ") & " " & _
                          sOBS
            'Sen�o
            Else
                'Imprimo os primeiros 31 caracteres
                Print #1, sTRAN & String(2 - Len(sTRAN), " ") & " " & _
                          String(5 - Len(sDPK), " ") & sDPK & " " & _
                          sFABR & String(19 - Len(sFABR), " ") & " " & _
                          sFABRNOVO & String(19 - Len(sFABRNOVO), " ") & " " & _
                          Mid(sOBS, 1, 31)
                
                'Tamanho agora vai ser igual ao tamanho original - 31
                vTamObs = vTamObs - 31
                'Indico de que caracter vou come�ar a ler no cpo. Observa��o
                J = J + 32
                
                'Enquanto o tamanho for maior que 0
                Do While vTamObs > 0
                    'Se tamanho maior que 31, imprime os pr�ximos 31 caracteres e atualiza tam.e ponteiro
                    If vTamObs > 31 Then
                        Print #1, Space(49) & Mid(sOBS, J, 31)
                        vTamObs = vTamObs - 31
                        J = J + 31
                    'Se tamanho menor que 31, imprime os caracteres que faltam
                    Else
                        Print #1, Space(49) & Mid(sOBS, J, vTamObs)
                        vTamObs = 0
                    End If
                Loop
            End If
            
        End If
            
    Next I

    Print #1, ""
    Print #1, "***************** FIM DO ARQUIVO DE LOG ***********************"
    
    Close #1

    MsgBox "Arquivo gerado com sucesso!", vbInformation, "Aten��o!"


End If

End Sub

Private Sub cmdLog_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp2.Left = cmdLog.Left
    lblHelp2.Visible = True
    lblHelp2.Caption = "Gerar Arq.de Log"
End Sub


Private Sub cmdVoltar_Click()
    Unload Me
End Sub

Private Sub cmdVoltar_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp2.Left = cmdVoltar.Left
    lblHelp2.Visible = True
    lblHelp2.Caption = "Voltar"
End Sub


Private Sub Form_Load()
    
    grdArquivo.Visible = False
    lblHelp1.Visible = False
    lblHelp2.Visible = False
    lblMsg.Visible = False
    lblMsg2.Visible = False
    lblMsg3.Visible = False

End Sub



Private Sub lblHelp1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp1.Visible = False
End Sub


Private Sub cmdArquivo_Click()
'on Error GoTo Trata_Erro
'Observa��es: Transa��o = (I/A/E)
             'I = Inclus�o / A = Altera��o / E = Exclus�o
Screen.MousePointer = 11
Call LIMPA_GRID
lblMsg = "": lblMsg2 = "": lblMsg3 = ""

'Indica que o controle ser� usado para abrir o arquivo
controle.ShowOpen
lblArquivo.Caption = controle.FileName
vNomeArq = lblArquivo

'Verifica se foi indicado o diret�rio e nome do arquivo a ser lido
If lblArquivo.Caption = "" Then
    MsgBox "Indique o arquivo a ser aberto", , MSG_TIT
    Screen.MousePointer = 0
    Exit Sub
End If

'MONTA O GRID
With grdArquivo
    .Visible = True
    .Row = 0
    .Col = 0: .ColWidth(0) = 450: .Text = " Reg"
    'Discriminar transa��o (inclus�o/altera��o/exclus�o)
    .Col = 1: .ColWidth(1) = 550:   .ColAlignment(1) = 2: .Text = " I/A/E"
    .Col = 2: .ColWidth(2) = 550:   .ColAlignment(2) = 2: .Text = "   Dpk"
    .Col = 3: .ColWidth(3) = 2000:  .ColAlignment(3) = 0: .Text = "    F�brica N/Trab."
    .Col = 4: .ColWidth(4) = 2000:  .ColAlignment(4) = 0: .Text = "F�brica N/Trab.Novo"
    .Col = 5: .ColWidth(5) = 2000:  .ColAlignment(5) = 0: .Text = "   Fornecedor"
    .Col = 6: .ColWidth(6) = 10000: .Text = "Observa��o"
End With

'//////////////////////////
'LEITURA DO ARQUIVO
'//////////////////////////
Open vNomeArq For Input As #1

'Inicializa contadores de linhas do grid
I = 0
lblMsg = ""
lblMsg.Visible = True

'L� enquanto n�o for final de arquivo
Do While Not EOF(1)

    I = I + 1
    
    lblMsg = "CARREGANDO ...." & I
    lblMsg.Refresh
    
    'Inicializa flags e vari�veis
    vTRAN = ""
    vDPK = ""
    vFABR = ""
    vFABRNOVO = ""
    vSIGLA = ""
    vFL_DPK_OK = True
    
    'L� registro - linha - do arquivo, joga na vari�vel  e seta ponteiro para o pr�ximo
    Line Input #1, varLinhaArquivo
    'Se linha lida for em branco, sai do loop
    If Mid(Trim(varLinhaArquivo), 1, 59) = "" Then
        Exit Do
    End If
    
    'Qtd. Linhas do grid ser� correspondente � qtd. de linhas lidas do arquivo texto
    grdArquivo.Rows = I + 1
    grdArquivo.Row = I
    
    '1a Coluna vai receber a numera��o de registros lidos do arquivo
    grdArquivo.Col = 0
    grdArquivo.Text = Format(I, "0000")
    
    'Atribui valores da linha do arquivo para as vari�veis
    vTRAN = Trim(Mid(varLinhaArquivo, 1, 1))
    vDPK = Trim(Mid(varLinhaArquivo, 2, 5))
    vFABR = Trim(Mid(varLinhaArquivo, 7, 19))
    vFABRNOVO = Trim(Mid(varLinhaArquivo, 26, 19))
    vSIGLA = Trim(Mid(varLinhaArquivo, 44, 15))
    
    'Valida��es gerais com TRANSA��O/DPK/FABR/FABR NOVO que vieram no arq., verificando se n�o est�o inv�lidos
    If (vTRAN <> "I" And vTRAN <> "A" And vTRAN <> "E") Then
        grdArquivo.Col = 6
        grdArquivo.Text = "Transa��o deve ser I/A/E!"
        vFL_DPK_OK = False
    ElseIf vDPK = "" Or Val(vDPK) = 0 Then
        grdArquivo.Col = 6
        grdArquivo.Text = "DPK Inv�lido! "
        vFL_DPK_OK = False
    ElseIf vFABR = "" Then
        grdArquivo.Col = 6
        grdArquivo.Text = "F�br.N/Trab. Inv�lido! "
        vFL_DPK_OK = False
    ElseIf (vTRAN = "A" And vFABRNOVO = "") Then
        grdArquivo.Col = 6
        grdArquivo.Text = "F�br.N/Trab.Novo Inv�lido! "
        vFL_DPK_OK = False
    ElseIf (vTRAN = "I" And vSIGLA = "") Then
        grdArquivo.Col = 6
        grdArquivo.Text = "Sigla do Forn. Inv�lida! "
        vFL_DPK_OK = False
    Else
        
        'Valida��o Geral do DPK na ITEM_ESTOQUE de Campinas
        db.Parameters.Remove "DPK": db.Parameters.Add "DPK", Val(vDPK), 1
        CmdSql = "Select C.Cod_Dpk, E.Situacao Sit " & _
                 "From Item_Estoque E, Item_Cadastro C " & _
                 "Where E.Cod_Loja = 1 " & _
                   "And E.Cod_Dpk = C.Cod_Dpk " & _
                   "And C.Cod_Dpk = :DPK "
        Set db_CONS = db.dbcreatedynaset(CmdSql, 0&)
        'Se select n�o trouxer nada, h� problema com o DPK
        If db_CONS.EOF Then
            grdArquivo.Col = 6
            grdArquivo.Text = "DPK N/Cad.! "
            vFL_DPK_OK = False
        Else
            'Se a situa��o do DPK estiver substituido ou desativado, n�o faz nada
            If Val(db_CONS!Sit.Value) = 9 Then
                grdArquivo.Col = 6
                grdArquivo.Text = "DPK Desat.!"
                vFL_DPK_OK = False
            End If
        End If
    
        'Se por enquanto estiver tudo ok, termina verifica��es
        If vFL_DPK_OK = True Then
            
            'Verifica exist�ncia do registro
            db.Parameters.Remove "DPK": db.Parameters.Add "DPK", Val(vDPK), 1
            db.Parameters.Remove "FABR": db.Parameters.Add "FABR", vFABR, 1
            CmdSql = "Select Count(Cod_Dpk) Qtd " & _
                       "From Producao.R_Fabrica_Dpk " & _
                      "Where Cod_Dpk = :DPK " & _
                        "And Cod_Fabrica = :FABR " & _
                        "And Fl_Tipo = 'E' " & _
                        "And Cod_Fornecedor = 0"
            Set db_CONS = db.dbcreatedynaset(CmdSql, 0&)
            
            'Se Transa��o = I e j� existir o registro, grava observa��o
            If vTRAN = "I" And (db_CONS!Qtd.Value <> 0) Then
                grdArquivo.Col = 6
                grdArquivo.Text = "DPK-Fabr.N/Trab j� existe!"
                vFL_DPK_OK = False
            'Se Transa��o = A ou E e reg. n�o existir, grava observa��o
            ElseIf (vTRAN = "A" Or vTRAN = "E") And (db_CONS!Qtd.Value = 0) Then
                grdArquivo.Col = 6
                grdArquivo.Text = "DPK-Fabr.N/Trab n�o existe!"
                vFL_DPK_OK = False
            End If
            'Se for altera��o e estiver tudo ok, verifica se j� existe o dpk relacionado com o c�d.fabr.novo
            If vFL_DPK_OK = True And vTRAN = "A" Then
                db.Parameters.Remove "DPK": db.Parameters.Add "DPK", Val(vDPK), 1
                db.Parameters.Remove "FABRNOVO": db.Parameters.Add "FABRNOVO", vFABRNOVO, 1
                CmdSql = "Select Count(Cod_Dpk) Qtd " & _
                           "From Producao.R_Fabrica_Dpk " & _
                          "Where Cod_Dpk = :DPK " & _
                            "And Cod_Fabrica = :FABRNOVO " & _
                            "And Fl_Tipo = 'E' " & _
                            "And Cod_Fornecedor = 0"
                Set db_CONS = db.dbcreatedynaset(CmdSql, 0&)
                'Se encontrou registro � porque o dpk-c�d.f�br.novo j� existem no cadastro e n�o poder� fazer a altera��o
                If db_CONS!Qtd.Value <> 0 Then
                    grdArquivo.Col = 6
                    grdArquivo.Text = "DPK-Fabr.N/Trab.Novo j� existe!"
                    vFL_DPK_OK = False
                End If
            End If
        End If
    End If
    'Alimenta o grid com as informa��es do arquivo, independente de estarem corretas ou n�o
    With grdArquivo
        .Col = 1: .Text = vTRAN
        .Col = 2: .Text = vDPK
        .Col = 3: .Text = vFABR
        .Col = 4: .Text = vFABRNOVO
        .Col = 5: .Text = vSIGLA
    End With
'L� pr�xima linha
Loop


Screen.MousePointer = 0
MsgBox "Arquivo Lido!", vbInformation, "Aten��o!"
lblMsg = I & " Linhas Lidas do Arquivo!"
lblMsg.Refresh
Close #1    ' Close file.
Screen.MousePointer = 0
Exit Sub
        
Trata_Erro:
    If Err.Number = 53 Then
        MsgBox "Arquivo de Controle n�o Encontrado. ", , MSG_TIT
    Else
        Call Process_Line_Errors
    End If
End Sub
Private Sub lblHelp2_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp2.Visible = False
End Sub






Private Sub SSFrame2_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp1.Visible = False
    lblHelp2.Visible = False
End Sub


Private Sub SSFrame3_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp1.Visible = False
End Sub



































