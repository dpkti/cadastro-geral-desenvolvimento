VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.MDIForm mdiEquiv 
   BackColor       =   &H00C0C0C0&
   Caption         =   "CAD880 - Cadastro de Equival�ncias  (DPK  x  C�d.F�brica N�o Trabalhado)"
   ClientHeight    =   6075
   ClientLeft      =   1470
   ClientTop       =   1320
   ClientWidth     =   8370
   LinkTopic       =   "MDIForm1"
   LockControls    =   -1  'True
   Begin Threed.SSPanel SSPanel2 
      Align           =   1  'Align Top
      Height          =   7380
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   15240
      _Version        =   65536
      _ExtentX        =   26882
      _ExtentY        =   13017
      _StockProps     =   15
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin Threed.SSFrame SSFrame1 
         Height          =   1095
         Left            =   120
         TabIndex        =   1
         Top             =   105
         Width           =   8055
         _Version        =   65536
         _ExtentX        =   14208
         _ExtentY        =   1931
         _StockProps     =   14
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Begin Threed.SSCommand cmdCadAutom 
            Height          =   615
            Left            =   360
            TabIndex        =   6
            Top             =   240
            Width           =   735
            _Version        =   65536
            _ExtentX        =   1296
            _ExtentY        =   1085
            _StockProps     =   78
            Picture         =   "MdiSit.frx":0000
         End
         Begin Threed.SSCommand cmdSair 
            Height          =   615
            Left            =   3240
            TabIndex        =   3
            Top             =   240
            Width           =   735
            _Version        =   65536
            _ExtentX        =   1296
            _ExtentY        =   1085
            _StockProps     =   78
            Picture         =   "MdiSit.frx":031A
         End
         Begin Threed.SSCommand cmdCadManual 
            Height          =   615
            Left            =   1455
            TabIndex        =   2
            Top             =   240
            Width           =   735
            _Version        =   65536
            _ExtentX        =   1296
            _ExtentY        =   1085
            _StockProps     =   78
            Picture         =   "MdiSit.frx":0634
         End
      End
      Begin Threed.SSFrame SSFrame2 
         Height          =   615
         Left            =   90
         TabIndex        =   4
         Top             =   5370
         Width           =   8130
         _Version        =   65536
         _ExtentX        =   14340
         _ExtentY        =   1085
         _StockProps     =   14
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Begin VB.Label lblMsg 
            Caption         =   " msg"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C00000&
            Height          =   255
            Left            =   240
            TabIndex        =   5
            Top             =   165
            Visible         =   0   'False
            Width           =   5895
         End
      End
   End
   Begin VB.Menu mnuCadastro 
      Caption         =   "&Cadastro"
      Begin VB.Menu mnuManual 
         Caption         =   "&Manual"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuAutomatico 
         Caption         =   "&Autom�tico"
      End
   End
   Begin VB.Menu mnuSair 
      Caption         =   "&Sair"
   End
   Begin VB.Menu mnuSobre 
      Caption         =   "S&obre"
   End
End
Attribute VB_Name = "mdiEquiv"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False




Private Sub cmdCadAutom_Click()
    frmCadAutom.Show vbModal
End Sub

Private Sub cmdCadAutom_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblMsg.Visible = True
    lblMsg.Caption = "Cadastro de C�d. F�brica N/ Trabalhado - Autom�tico"
End Sub


Private Sub cmdCadManual_Click()
    frmCadManual.Show vbModal
End Sub

Private Sub cmdCadManual_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblMsg.Visible = True
    lblMsg.Caption = "Inserir/Alterar Data de Primeira Entrada - Manual"
End Sub


Private Sub cmdSair_Click()
    End
End Sub

Private Sub cmdSair_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblMsg.Visible = True
    lblMsg.Caption = "Sair do Programa"
End Sub


Private Sub MDIForm_Load()

    On Error GoTo TrataErro
    
    If App.PrevInstance Then
        MsgBox "J� EXISTE UMA INST�NCIA DO PROGRAMA NO AR"
        End
    End If
    
    'posicionar tela
    'mdiSituacao.Top = 0
    'mdiSituacao.Left = 0
    'mdiSituacao.Width = Screen.Width
    'mdiSituacao.Height = Screen.Height
        
    'Conexao oracle
    Set dbOra = CreateObject("oracleinproCServer.xorasession")
    
    'CAMPINAS
    'Set db = dbOra.OpenDatabase("DESENV", "PRODUCAO/DES", 0&)
    Set db = dbOra.OpenDatabase("PRODUCAO", "CAD880/PROD", 0&)
    'Set db = dbOra.OpenDatabase("PRODUCAO", "PRODUCAO/DPK", 0&)
    'Set db = dbOra.OpenDatabase("TITANIUM", "PRODUCAO/DPK", 0&)
           
    'Pega a data de faturamento do sistema para usar na montagem do nome do arq. de log e p/
    'servir de refer�ncia na data do cadastramento
    CmdSql = "Select To_Char(dt_faturamento,'dd/mm/yyyy') Dt_Cad From DATAS "
    Set db_CONS = db.dbcreatedynaset(CmdSql, 0&)
    
    vDt_Cad = Format(db_CONS!dt_cad.Value, "dd/mm/yy")
    vAnoMes = Format(db_CONS!dt_cad.Value, "yyyymm")
           
    cmdCadManual.Enabled = False
    cmdCadManual.Visible = False
    Exit Sub

TrataErro:
    MsgBox "Ocorreu o erro: " & Err & "-" & Err.Description, vbExclamation, "Aten��o"
    Exit Sub
End Sub

Private Sub mnuAutomatico_Click()
    Call cmdCadAutom_Click
End Sub

Private Sub mnuManual_Click()
    Call cmdCadManual_Click
End Sub

Private Sub mnuSair_Click()
    Call cmdSair_Click
End Sub

Private Sub mnuSobre_Click()
   frmSobre.Show vbModal
End Sub

Private Sub SSFrame1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblMsg.Visible = False
End Sub


Private Sub SSPanel2_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblMsg.Visible = False
End Sub


