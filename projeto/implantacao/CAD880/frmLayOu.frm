VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form frmLayOut 
   Caption         =   "Lay-Out do Arquivo para Cadastro Autom�tico de C�digos de F�brica N�o Trabalhados"
   ClientHeight    =   6360
   ClientLeft      =   1470
   ClientTop       =   1035
   ClientWidth     =   8370
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6360
   ScaleWidth      =   8370
   Begin VB.Frame Frame1 
      Height          =   5595
      Left            =   150
      TabIndex        =   0
      Top             =   -45
      Width           =   8115
      Begin VB.Label Label22 
         AutoSize        =   -1  'True
         Caption         =   "Sigla Fornecedor N/Trab               ALFANUM�RICO(15)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   645
         TabIndex        =   23
         Top             =   1500
         Width           =   4800
      End
      Begin VB.Label Label21 
         AutoSize        =   -1  'True
         Caption         =   "6) Se Transa��o = E, ent�o o ""C�d.F�br.N/Trab"" dever� ser preenchido com o"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   645
         TabIndex        =   22
         Top             =   4800
         Width           =   6750
      End
      Begin VB.Label Label13 
         AutoSize        =   -1  'True
         Caption         =   "     c�d.f�brica que a DPK n�o trabalha, a ser exclu�do."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   645
         TabIndex        =   21
         Top             =   4995
         Width           =   4770
      End
      Begin VB.Label Label20 
         AutoSize        =   -1  'True
         Caption         =   "     com o c�d.f�brica novo que a DPK n�o trabalha."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   645
         TabIndex        =   20
         Top             =   4605
         Width           =   4485
      End
      Begin VB.Label Label19 
         AutoSize        =   -1  'True
         Caption         =   "     c�d.f�brica que a DPK n�o trabalha, a ser alterado e ""C�d.F�brN/Trab Novo"""
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   645
         TabIndex        =   19
         Top             =   4395
         Width           =   6960
      End
      Begin VB.Label Label15 
         AutoSize        =   -1  'True
         Caption         =   "5) Se Transa��o = A, ent�o o ""C�d.F�br.N/Trab"" dever� ser preenchido com o"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   645
         TabIndex        =   18
         Top             =   4200
         Width           =   6750
      End
      Begin VB.Label Label18 
         AutoSize        =   -1  'True
         Caption         =   "     c�d.f�brica que a DPK n�o trabalha, a ser inclu�do."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   645
         TabIndex        =   17
         Top             =   4005
         Width           =   4725
      End
      Begin VB.Label Label17 
         AutoSize        =   -1  'True
         Caption         =   "4) Se Transa��o = I, ent�o o ""C�d.F�br.N/Trab"" dever� ser preenchido com o"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   645
         TabIndex        =   16
         Top             =   3795
         Width           =   6690
      End
      Begin VB.Label Label11 
         AutoSize        =   -1  'True
         Caption         =   "3) ""C�d.F�br.N/Trab"", se refere ao c�digo de f�brica que a DPK n�o trabalha."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   645
         TabIndex        =   15
         Top             =   3600
         Width           =   6705
      End
      Begin VB.Line Line3 
         X1              =   600
         X2              =   7480
         Y1              =   2880
         Y2              =   2880
      End
      Begin VB.Label Label16 
         AutoSize        =   -1  'True
         Caption         =   "C�d.F�br.N/Trab Novo                 ALFANUM�RICO(19)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   645
         TabIndex        =   14
         Top             =   1260
         Width           =   4755
      End
      Begin VB.Label Label14 
         AutoSize        =   -1  'True
         Caption         =   "7) Fazer arquivos de, no m�ximo 100 linhas."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   645
         TabIndex        =   13
         Top             =   5205
         Width           =   3750
      End
      Begin VB.Label Label12 
         AutoSize        =   -1  'True
         Caption         =   "2) Campos Alfanum�ricos dever�o vir com espa�os em branco � direita."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   645
         TabIndex        =   11
         Top             =   3405
         Width           =   6090
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         Caption         =   "1) Campos Num�ricos dever�o vir com zeros � esquerda."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   645
         TabIndex        =   10
         Top             =   3195
         Width           =   4845
      End
      Begin VB.Label Label9 
         AutoSize        =   -1  'True
         Caption         =   "Observa��es:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000040C0&
         Height          =   195
         Left            =   1035
         TabIndex        =   9
         Top             =   2955
         Width           =   1185
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         Caption         =   "E54357BBBW990050                             FAG"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   645
         TabIndex        =   8
         Top             =   2565
         Width           =   5040
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "A54357W990050            W990050T            FAG"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   645
         TabIndex        =   7
         Top             =   2355
         Width           =   5040
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         Caption         =   "I54357AAAW990050                             FAG"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   645
         TabIndex        =   6
         Top             =   2145
         Width           =   5040
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Exemplo de Arquivo:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000040C0&
         Height          =   195
         Left            =   1110
         TabIndex        =   5
         Top             =   1905
         Width           =   1755
      End
      Begin VB.Line Line2 
         X1              =   600
         X2              =   7480
         Y1              =   1830
         Y2              =   1830
      End
      Begin VB.Line Line1 
         X1              =   600
         X2              =   7480
         Y1              =   405
         Y2              =   405
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Campo                                  Tipo de Dados"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000040C0&
         Height          =   195
         Left            =   1035
         TabIndex        =   4
         Top             =   165
         Width           =   3870
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "C�d.F�br.N/Trab                          ALFANUM�RICO(19)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   645
         TabIndex        =   3
         Top             =   1020
         Width           =   4785
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "C�digo DPK                                 NUM�RICO (5)          "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   645
         TabIndex        =   2
         Top             =   780
         Width           =   4890
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Transa��o(I/A/E)                         NUM�RICO (1)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   645
         TabIndex        =   1
         Top             =   555
         Width           =   4290
      End
   End
   Begin Threed.SSCommand cmdVoltar 
      Height          =   615
      Left            =   7635
      TabIndex        =   12
      Top             =   5655
      Width           =   630
      _Version        =   65536
      _ExtentX        =   1111
      _ExtentY        =   1085
      _StockProps     =   78
      Picture         =   "frmLayOu.frx":0000
   End
End
Attribute VB_Name = "frmLayOut"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdVoltar_Click()
    Unload Me
End Sub


