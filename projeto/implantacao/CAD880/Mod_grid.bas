Attribute VB_Name = "Mod_Grid"
Public lbCOL_SEL As Byte
Public psRET_GRID As String
Public pbRET_GRID As Boolean
Public pboFL_ESCOLHA_GRID As Boolean
Public PATH_IMAG As String


Private Sub CONS_PEDIDO()

    Dim RET_GRID As String

    RET_GRID = ""
    For I = 0 To grid_padrao.Cols - 1
        grid_padrao.Col = I
        RET_GRID = RET_GRID & Trim(grid_padrao.Text) & IIf(I = grid_padrao.Cols - 1, "", "|")
    Next

    piCOD_CID_EMP = Mid(RET_GRID, 1, InStr(RET_GRID, "|") - 1)
    txtCID_EMPRESA = Mid(RET_GRID, InStr(RET_GRID, "|") + 1, 20)
    txtCEP_EMPRESA.SetFocus

    CURS_FOCUS = ""

End Sub


Public Sub DBCLICK_GRIDPAD(N_CAMPO As Object, N_GRID As Object)

    Dim I As Byte

    N_CAMPO = ""
    N_CAMPO.Visible = False

    'FOI ALTERADO EM 10/09/97 - JEFERSON

    psRET_GRID = "{1}"
    For I = 0 To N_GRID.Cols - 1
        N_GRID.Col = I
        psRET_GRID = psRET_GRID & Trim(N_GRID.Text) & IIf(I = N_GRID.Cols - 1, "", "{" & I + 2 & "}")
    Next

    'psRET_GRID = ""
    ' Vai trazer uma string com todas as informa�oes da
    ' linha e separar de acordo com as colunas
    'For I = 0 To N_GRID.Cols - 1
    '    N_GRID.Col = I
    '    psRET_GRID = psRET_GRID & Trim(N_GRID.Text) & IIf(I = N_GRID.Cols - 1, "", "-")
    'Next

End Sub

Public Sub CLICK_GRIDPAD(N_CAMPO As Object, N_GRID As Object)

    N_CAMPO.Visible = True
    N_CAMPO.Width = N_GRID.ColWidth(N_GRID.Col)
    N_CAMPO.Top = N_GRID.Top + N_GRID.Height + 150
    N_CAMPO.Left = N_GRID.Left

    lbCOL_SEL = N_GRID.Col

End Sub


Public Sub CONS_GRID(POS_TOP As Integer, _
                     POS_LEFT As Integer, _
                     POS_WIDTH As Integer, _
                     POS_HEIGHT As Integer, _
                     NUM_COL As Byte, _
                     TAM_COL As String, _
                     CABEC_COL As String, _
                     SEL As String, _
                     NOME_GRID As Object)
            
    Dim CONT_L As Integer
    Dim I As Byte
    Dim C_TAM_STR As Byte
    Dim F_TAM_STR As Byte
    Dim C_CAB_STR As Byte
    Dim F_CAB_STR As Byte
    Dim ULT_POS_TAM As Byte
    Dim ULT_POS_CAB As Byte
    Dim TIPO As Integer
   
    'POS_TOP    - Posicao do Topo do Grid
    'POS_LEFT   - Posicao da Coluna a Esquerda
    'POS_WIDTH  - Posicao da Largura
    'POS_HEIGHT - Posicao da Altura
    'NUM_COL    - Numero de Colunas
    'TAM_COL    - Tamanho das Colunas
    'CABEC_COL  - Cabe�alho das Colunas
    'FRM        - Nome do Form
    'SEL        - String que contem o Select

    Screen.MousePointer = 11
           
    'Executa o Select de Consulta
    Set db_CONS = db.dbcreatedynaset(SEL, 0&)

    psRET_GRID = ""
                    
    If db_CONS.RecordCount = 0 Then
        Screen.MousePointer = 0
        MsgBox "Registro n�o encontrado.", , "CONSULTA"
        psRET_GRID = ""
        pbRET_GRID = False
        Exit Sub
    Else
        pboFL_ESCOLHA_GRID = False
        pbRET_GRID = True
    End If

    NOME_GRID.Top = POS_TOP
    NOME_GRID.Left = POS_LEFT
    NOME_GRID.Width = POS_WIDTH
    NOME_GRID.Height = POS_HEIGHT
            
    NOME_GRID.Rows = db_CONS.RecordCount + 1
    NOME_GRID.Cols = NUM_COL
    'NOME_GRID.FontBold = True

    C_TAM_STR = 0
    F_TAM_STR = 0
    C_CAB_STR = 0
    F_CAB_STR = 0
    ULT_POS_TAM = 0
    ULT_POS_CAB = 0

    For I = 0 To NUM_COL - 1

        NOME_GRID.Row = 0

        If I < NUM_COL - 1 Then
            ULT_POS_TAM = InStr(F_TAM_STR + 1, TAM_COL, ",")
            C_TAM_STR = ULT_POS_TAM - F_TAM_STR
            ULT_POS_CAB = InStr(F_CAB_STR + 1, CABEC_COL, ",")
            C_CAB_STR = ULT_POS_CAB - F_CAB_STR
        Else
            C_TAM_STR = Len(TAM_COL) - F_TAM_STR + 1
            C_CAB_STR = Len(CABEC_COL) - F_CAB_STR + 1
        End If

        'Define o Tamanho das Colunas
        NOME_GRID.ColWidth(I) = Mid(TAM_COL, F_TAM_STR + 1, C_TAM_STR - 1)

        'Define o Cabe�alho das Colunas
        NOME_GRID.Col = I
        NOME_GRID.Text = Mid(CABEC_COL, F_CAB_STR + 1, C_CAB_STR - 1)

        F_TAM_STR = ULT_POS_TAM
        F_CAB_STR = ULT_POS_CAB

    Next

    'Monta o Conteudo do Grid
    For CONT_L = 1 To db_CONS.RecordCount
    
        NOME_GRID.Row = CONT_L

        NOME_GRID.Col = 0
        NOME_GRID.Text = db_CONS(0)
        If NUM_COL = 2 Then
            NOME_GRID.Col = 1
            NOME_GRID.Text = db_CONS(1)
        ElseIf NUM_COL = 3 Then
            NOME_GRID.Col = 1
            NOME_GRID.Text = db_CONS(1)
            NOME_GRID.Col = 2
            NOME_GRID.Text = db_CONS(2)
        ElseIf NUM_COL = 4 Then
            NOME_GRID.Col = 1
            NOME_GRID.Text = db_CONS(1)
            NOME_GRID.Col = 2
            NOME_GRID.Text = db_CONS(2)
            NOME_GRID.Col = 3
            NOME_GRID.Text = db_CONS(3)
        ElseIf NUM_COL = 5 Then
            NOME_GRID.Col = 1
            NOME_GRID.Text = db_CONS(1)
            NOME_GRID.Col = 2
            NOME_GRID.Text = db_CONS(2)
            NOME_GRID.Col = 3
            NOME_GRID.Text = db_CONS(3)
            NOME_GRID.Col = 4
            NOME_GRID.Text = db_CONS(4)
        ElseIf NUM_COL = 6 Then
            NOME_GRID.Col = 1
            NOME_GRID.Text = db_CONS(1)
            NOME_GRID.Col = 2
            NOME_GRID.Text = db_CONS(2)
            NOME_GRID.Col = 3
            NOME_GRID.Text = db_CONS(3)
            NOME_GRID.Col = 4
            NOME_GRID.Text = db_CONS(4)
            NOME_GRID.Col = 5
            NOME_GRID.Text = db_CONS(5)
        ElseIf NUM_COL = 7 Then
            NOME_GRID.Col = 1
            NOME_GRID.Text = db_CONS(1)
            NOME_GRID.Col = 2
            NOME_GRID.Text = db_CONS(2)
            NOME_GRID.Col = 3
            NOME_GRID.Text = db_CONS(3)
            NOME_GRID.Col = 4
            NOME_GRID.Text = db_CONS(4)
            NOME_GRID.Col = 5
            NOME_GRID.Text = db_CONS(5)
            NOME_GRID.Col = 6
            NOME_GRID.Text = db_CONS(6)
        ElseIf NUM_COL = 8 Then
            NOME_GRID.Col = 1
            NOME_GRID.Text = db_CONS(1)
            NOME_GRID.Col = 2
            NOME_GRID.Text = db_CONS(2)
            NOME_GRID.Col = 3
            NOME_GRID.Text = db_CONS(3)
            NOME_GRID.Col = 4
            NOME_GRID.Text = db_CONS(4)
            NOME_GRID.Col = 5
            NOME_GRID.Text = db_CONS(5)
            NOME_GRID.Col = 6
            NOME_GRID.Text = db_CONS(6)
            NOME_GRID.Col = 7
            NOME_GRID.Text = db_CONS(7)
        ElseIf NUM_COL = 10 Then
            NOME_GRID.Col = 1
            NOME_GRID.Text = db_CONS(1)
            NOME_GRID.Col = 2
            NOME_GRID.Text = db_CONS(2)
            NOME_GRID.Col = 3
            NOME_GRID.Text = db_CONS(3)
            NOME_GRID.Col = 4
            NOME_GRID.Text = db_CONS(4)
            NOME_GRID.Col = 5
            NOME_GRID.Text = db_CONS(5)
            NOME_GRID.Col = 6
            NOME_GRID.Text = db_CONS(6)
            NOME_GRID.Col = 7
            NOME_GRID.Text = db_CONS(7)
            NOME_GRID.Col = 8
            NOME_GRID.Text = db_CONS(8)
            NOME_GRID.Col = 9
            NOME_GRID.Text = db_CONS(9)
        End If

        db_CONS.dbMoveNext
        
    Next

    NOME_GRID.Visible = True
    NOME_GRID.Row = 1
    SendKeys "^+{HOME}"

    Screen.MousePointer = 0

End Sub




Public Sub KEYPRESS_GRIDPAD(N_CAMPO As Object, N_GRID As Object, KeyAscii As Integer)

    Dim CONT_ROW As Integer
    Dim SALVA_POS As Integer
    Dim J, I As Integer

    KeyAscii = Asc(UCase(Chr(KeyAscii)))

    If KeyAscii = 27 Then
        N_CAMPO = ""
        N_CAMPO.Visible = False
        Exit Sub
    ElseIf KeyAscii = 13 Then
        N_CAMPO = ""
        N_CAMPO.Visible = False
        psRET_GRID = ""
        For I = 0 To N_GRID.Cols - 1
            N_GRID.Col = I
            psRET_GRID = psRET_GRID & Trim(N_GRID.Text) & IIf(I = N_GRID.Cols - 1, "", "-")
        Next
        Exit Sub
    ElseIf KeyAscii = 8 Then
        If N_CAMPO <> "" Then
            N_CAMPO = Mid(N_CAMPO, 1, Len(N_CAMPO) - 1)
        Else
            N_CAMPO.Visible = False
        End If
        If N_CAMPO = "" Then
            SendKeys "{LEFT}+{END}"
            Exit Sub
        End If
    Else
        N_CAMPO = N_CAMPO & Chr(KeyAscii)
    End If

    N_GRID.Row = 1
    N_GRID.Col = lbCOL_SEL

    CONT_ROW = 1
    N_GRID.Row = CONT_ROW
    While N_CAMPO <> Mid(N_GRID.Text, 1, Len(N_CAMPO))
        If CONT_ROW = N_GRID.Rows Then
            Beep
            N_GRID.Row = SALVA_POS
            N_CAMPO = Mid(N_CAMPO, 1, Len(N_CAMPO) - 1)
            Exit Sub
        End If
        N_GRID.Row = CONT_ROW
        CONT_ROW = CONT_ROW + 1
    Wend

    N_GRID.SetFocus
    N_GRID.Row = CONT_ROW - 1
    If CONT_ROW = 1 Then
        N_GRID.SelStartRow = 1
        N_GRID.SelEndRow = 1
    Else
        N_GRID.SelStartRow = CONT_ROW - 1
        N_GRID.SelEndRow = CONT_ROW - 1
    End If
    SendKeys "{LEFT}+{END}"

End Sub


