VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmSobre 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Informações sobre o programa:"
   ClientHeight    =   4845
   ClientLeft      =   2820
   ClientTop       =   2415
   ClientWidth     =   5790
   Icon            =   "frmsobre.frx":0000
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4845
   ScaleWidth      =   5790
   StartUpPosition =   1  'CenterOwner
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   8
      Top             =   4470
      Width           =   5790
      _ExtentX        =   10213
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   2
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton Command1 
      Caption         =   "OK!"
      Height          =   525
      Left            =   2273
      TabIndex        =   1
      ToolTipText     =   "Sair das Informações:"
      Top             =   3765
      Width           =   1245
   End
   Begin VB.PictureBox Picture1 
      Height          =   1455
      Left            =   2033
      Picture         =   "frmsobre.frx":0742
      ScaleHeight     =   1395
      ScaleWidth      =   1665
      TabIndex        =   0
      Top             =   120
      Width           =   1725
   End
   Begin VB.Label Label1 
      Caption         =   "Programa:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   188
      TabIndex        =   7
      Top             =   1695
      Width           =   885
   End
   Begin VB.Label Label3 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "CAD260-Cadastro de Plano de Contas \        Grupo Contábil"
      BeginProperty Font 
         Name            =   "Lucida Console"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   173
      TabIndex        =   6
      Top             =   1905
      Width           =   5475
   End
   Begin VB.Label Label2 
      Caption         =   "Responsável:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   158
      TabIndex        =   5
      Top             =   2535
      Width           =   1245
   End
   Begin VB.Label Label4 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Renato H."
      BeginProperty Font 
         Name            =   "Lucida Console"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   158
      TabIndex        =   4
      Top             =   2745
      Width           =   4575
   End
   Begin VB.Label Label5 
      Caption         =   "Contato:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   143
      TabIndex        =   3
      Top             =   3135
      Width           =   735
   End
   Begin VB.Label Label6 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "supsist@dpk.com.br"
      BeginProperty Font 
         Name            =   "Lucida Console"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   158
      TabIndex        =   2
      ToolTipText     =   "supsist@dpk.com.br"
      Top             =   3345
      Width           =   4575
   End
   Begin VB.Menu Voltar 
      Caption         =   "&Voltar"
   End
End
Attribute VB_Name = "frmSobre"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Command1_Click()
    Unload Me
End Sub

Private Sub Voltar_Click()
    Unload Me
End Sub
