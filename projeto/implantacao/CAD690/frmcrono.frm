VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form frmcronologia 
   Caption         =   "Altera��o Cronologia"
   ClientHeight    =   3090
   ClientLeft      =   150
   ClientTop       =   1470
   ClientWidth     =   6690
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   3090
   ScaleWidth      =   6690
   Begin VB.Frame Frame1 
      Height          =   3015
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   6375
      Begin VB.ComboBox cboconceito 
         Height          =   315
         Left            =   4080
         TabIndex        =   6
         Top             =   1080
         Width           =   2055
      End
      Begin VB.TextBox txtpagamento 
         Height          =   285
         Left            =   1800
         MaxLength       =   8
         TabIndex        =   5
         Top             =   1080
         Width           =   1215
      End
      Begin VB.TextBox txtsequencia 
         Height          =   285
         Left            =   1080
         TabIndex        =   2
         Top             =   480
         Width           =   735
      End
      Begin VB.Frame Frame2 
         Height          =   1215
         Left            =   4320
         TabIndex        =   1
         Top             =   1680
         Width           =   1815
         Begin Threed.SSCommand cmdvoltar 
            Height          =   495
            Left            =   1080
            TabIndex        =   9
            Top             =   240
            Width           =   495
            _Version        =   65536
            _ExtentX        =   873
            _ExtentY        =   873
            _StockProps     =   78
            Picture         =   "frmcrono.frx":0000
         End
         Begin Threed.SSCommand cmdconfirmar 
            Height          =   495
            Left            =   240
            TabIndex        =   8
            Top             =   240
            Width           =   495
            _Version        =   65536
            _ExtentX        =   873
            _ExtentY        =   873
            _StockProps     =   78
            Picture         =   "frmcrono.frx":0452
         End
         Begin VB.Label lblmsg 
            AutoSize        =   -1  'True
            BackColor       =   &H0000FFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "lblmsg"
            Height          =   255
            Left            =   1080
            TabIndex        =   10
            Top             =   840
            Visible         =   0   'False
            Width           =   495
         End
      End
      Begin VB.Label Label3 
         Caption         =   "Conceito"
         Height          =   255
         Left            =   3360
         TabIndex        =   7
         Top             =   1080
         Width           =   855
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Data do Pagamento"
         Height          =   195
         Left            =   240
         TabIndex        =   4
         Top             =   1080
         Width           =   1425
      End
      Begin VB.Label Label1 
         Caption         =   "Sequ�ncia"
         Height          =   255
         Left            =   240
         TabIndex        =   3
         Top             =   480
         Width           =   855
      End
   End
End
Attribute VB_Name = "frmcronologia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cboconceito_KeyPress(KeyAscii As Integer)
KeyAscii = 0

End Sub


Private Sub cmdconfirmar_Click()
On Error GoTo trata_erro
Dim resp As Integer

    resp = MsgBox("Confirma a altera��o das Informa��es?", vbQuestion + vbYesNo, "Altera��o")
        If resp = 6 Then
            Screen.MousePointer = 11
            PL1 = "Begin"
            PL1 = PL1 & " Update sci.cronologia "
            PL1 = PL1 & " Set dt_pagto = :pagamento,"
            PL1 = PL1 & " conceito = :conceito "
            PL1 = PL1 & " Where cod_cliente = :cliente and "
            PL1 = PL1 & " sequencia = :sequencia;"
            PL1 = PL1 & " Commit;"
            PL1 = PL1 & " Exception"
            PL1 = PL1 & " When others then"
            PL1 = PL1 & " Rollback;"
            PL1 = PL1 & " :cod_errora := sqlcode;"
            PL1 = PL1 & " :txt_errora := sqlerrm;"
            PL1 = PL1 & " End;"
            
            
            oradatabase.Parameters.Remove "pagamento"
            oradatabase.Parameters.Remove "conceito"
            oradatabase.Parameters.Remove "cliente"
            oradatabase.Parameters.Remove "sequencia"
            oradatabase.Parameters.Remove "cod_errora"
            oradatabase.Parameters.Remove "txt_errora"
            oradatabase.Parameters.Add "pagamento", IIf(IsNull(txtpagamento.Text), "", Mid(txtpagamento, 1, 2) & Mid(txtpagamento, 4, 2) & Mid(txtpagamento, 7, 2)), 1
              If cboconceito = "PONTUAL" Then
                    oradatabase.Parameters.Add "conceito", "P", 1
                ElseIf cboconceito = "PROTESTADO" Then
                    oradatabase.Parameters.Add "conceito", "R", 1
                ElseIf cboconceito = "ATRASADO" Then
                    oradatabase.Parameters.Add "conceito", "A", 1
                Else
                    oradatabase.Parameters.Add "conceito", "", 1
                End If
            oradatabase.Parameters.Add "cliente", frmsci.txtcliente.Text, 1
            oradatabase.Parameters.Add "sequencia", txtsequencia.Text, 1
            oradatabase.Parameters.Add "cod_errora", 0, 2
            oradatabase.Parameters.Add "txt_errora", "", 2
            
            
            oradatabase.ExecuteSQL PL1
            
            
            
            
            SQL = " Select cod_cliente, sequencia,dt_pagto,conceito from sci.cronologia where cod_cliente = " & frmsci.txtcliente.Text & "order by sequencia"
        
            Set ss = oradatabase.CreateDynaset(SQL, 0&)
           
            frmsci.grdcronologia.Row = 0
            frmsci.grdcronologia.Cols = 3
            
            
            frmsci.grdcronologia.Col = 0
            frmsci.grdcronologia.ColWidth(0) = 500
            frmsci.grdcronologia.Text = "Seq."
            
            frmsci.grdcronologia.Col = 1
            frmsci.grdcronologia.ColWidth(1) = 1250
            frmsci.grdcronologia.Text = "Data Pagamento"
            
            frmsci.grdcronologia.Col = 2
            frmsci.grdcronologia.ColWidth(2) = 700
            frmsci.grdcronologia.Text = "Conceito"
            
            i = 0
                
            Do While Not ss.EOF
                i = i + 1
             
                frmsci.grdcronologia.Rows = i + 1
                frmsci.grdcronologia.Row = i
                
                frmsci.grdcronologia.Col = 0
                frmsci.grdcronologia.Text = ss!sequencia
                
                frmsci.grdcronologia.Col = 1
                frmsci.grdcronologia.Text = IIf(IsNull(ss!dt_pagto), "", ss!dt_pagto)
                
                frmsci.grdcronologia.Col = 2
                frmsci.grdcronologia.Text = IIf(IsNull(ss!conceito), "", ss!conceito)
    
               ss.MoveNext
            Loop
            Screen.MousePointer = 0
            Unload Me
        End If
    

Exit Sub
trata_erro:
            If Err <> 0 Then
                MsgBox "Ocorreu o Erro" & Err & "-" & Err.Description & ".Ligue para o Depto. de Sistemas", vbCritical, "Erro"
                End
            End If


End Sub

Private Sub cmdconfirmar_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
lblmsg.Visible = True
lblmsg.Left = 240
lblmsg.Caption = "Alterar"

End Sub


Private Sub cmdvoltar_Click()
Unload Me

End Sub

Private Sub cmdvoltar_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
lblmsg.Visible = True
lblmsg.Left = 1080
lblmsg.Caption = "Voltar"

End Sub


Private Sub Form_Load()

 If cboconceito.ListCount = 0 Then
       cboconceito.AddItem "PONTUAL"
       cboconceito.AddItem "ATRASADO"
       cboconceito.AddItem "PROTESTADO"
 End If
End Sub

Private Sub Frame1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
lblmsg.Visible = False

End Sub


Private Sub Frame2_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
lblmsg.Visible = False

End Sub


Private Sub SSCommand2_Click()

End Sub


Private Sub SSCommand2_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

End Sub


Private Sub lblmsg_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
lblmsg.Visible = False

End Sub


Private Sub txtpagamento_KeyPress(KeyAscii As Integer)
Data KeyAscii, txtpagamento

End Sub


Private Sub txtsequencia_KeyPress(KeyAscii As Integer)
KeyAscii = 0

End Sub


