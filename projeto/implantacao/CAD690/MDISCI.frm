VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.MDIForm MDIForm1 
   BackColor       =   &H8000000C&
   Caption         =   "CAD690 - Cadastro de SCI"
   ClientHeight    =   5940
   ClientLeft      =   4050
   ClientTop       =   3675
   ClientWidth     =   6690
   Icon            =   "MDISCI.frx":0000
   LinkTopic       =   "MDIForm1"
   WindowState     =   2  'Maximized
   Begin Threed.SSPanel SSPanel1 
      Align           =   1  'Align Top
      Height          =   615
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6690
      _Version        =   65536
      _ExtentX        =   11800
      _ExtentY        =   1085
      _StockProps     =   15
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin Threed.SSCommand cmdsair 
         Height          =   495
         Left            =   1200
         TabIndex        =   3
         Top             =   60
         Width           =   495
         _Version        =   65536
         _ExtentX        =   873
         _ExtentY        =   873
         _StockProps     =   78
         Picture         =   "MDISCI.frx":0442
      End
      Begin Threed.SSCommand cmdcadastro 
         Height          =   495
         Left            =   120
         TabIndex        =   2
         Top             =   60
         Width           =   495
         _Version        =   65536
         _ExtentX        =   873
         _ExtentY        =   873
         _StockProps     =   78
         MouseIcon       =   "MDISCI.frx":075C
         Picture         =   "MDISCI.frx":0778
      End
   End
   Begin Threed.SSPanel SSPanel2 
      Align           =   2  'Align Bottom
      Height          =   735
      Left            =   0
      TabIndex        =   1
      Top             =   5205
      Width           =   6690
      _Version        =   65536
      _ExtentX        =   11800
      _ExtentY        =   1296
      _StockProps     =   15
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin VB.Label lblmsg 
         AutoSize        =   -1  'True
         Caption         =   "lblmsg"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   360
         Visible         =   0   'False
         Width           =   555
      End
   End
   Begin VB.Menu mnusci 
      Caption         =   "&SCI"
   End
   Begin VB.Menu mnusobre 
      Caption         =   "S&obre"
   End
   Begin VB.Menu mnusair 
      Caption         =   "S&air"
   End
End
Attribute VB_Name = "MDIForm1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdcadastro_Click()

lblmsg.Visible = False
frmsci.Show vbModal

End Sub


Private Sub cmdcadastro_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
lblmsg.Visible = True
lblmsg.Caption = "Cadastro e Consulta de SCI"

End Sub


Private Sub cmdconsulta_Click()
lblmsg.Visible = False

End Sub

Private Sub cmdconsulta_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
lblmsg.Visible = True
lblmsg.Caption = "Consulta de SCI j� Cadastrado"
End Sub


Private Sub cmdsair_Click()
Dim resp As Integer

lblmsg.Visible = False

resp = MsgBox("Confirma a Sa�da do Sistema?", vbQuestion + vbYesNo, "Sair")

If resp = 6 Then
    End
End If


End Sub


Private Sub cmdsair_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
lblmsg.Visible = True
lblmsg.Caption = "Sair do Programa"

End Sub


Private Sub MDIForm_Load()
On Error GoTo trata_erro


If App.PrevInstance Then
    MsgBox "J� EXISTE UMA INST�NCIA DO PROGRAMA NO AR!", vbExclamation, "Aten��o"
    End
End If

Set orasession = CreateObject("oracleinprocserver.xorasession")
'Set oradatabase = orasession.OpenDatabase("Producao", "CAD690/PROD", 0&)
Set oradatabase = orasession.OpenDatabase("Desenv", "producao/des", 0&)

Exit Sub

trata_erro:
    If Err <> 0 Then
        MsgBox "Ocorreu o Erro" & Err & "-" & Err.Description & ".Ligue para o Depto. de Sistemas", vbExclamation, "Erro"
        End
    End If
    
    
End Sub


Private Sub MDIForm_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
lblmsg.Visible = False

End Sub


Private Sub mnucadastro_Click()
Call cmdcadastro_Click

End Sub

Private Sub mnuconsulta_Click()
Call cmdconsulta_Click

End Sub

Private Sub mnusair_Click()
Call cmdsair_Click

End Sub

Private Sub mnusci_Click()
Call cmdcadastro_Click

End Sub


Private Sub mnusobre_Click()
frmsobre.Show vbModal

End Sub

Private Sub SSPanel1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
lblmsg.Visible = False

End Sub


