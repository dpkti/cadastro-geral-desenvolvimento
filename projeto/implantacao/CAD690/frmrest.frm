VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form frmrestricao 
   Caption         =   "Altera��o Restri��o/Fornecedor"
   ClientHeight    =   3090
   ClientLeft      =   75
   ClientTop       =   3405
   ClientWidth     =   6690
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   3090
   ScaleWidth      =   6690
   Begin VB.Frame Frame1 
      Height          =   3015
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   6375
      Begin VB.Frame Frame3 
         Caption         =   "Qtd de Fornecedores"
         Height          =   855
         Left            =   120
         TabIndex        =   11
         Top             =   240
         Width           =   6135
         Begin VB.TextBox txtacimade15 
            Height          =   285
            Left            =   5160
            MaxLength       =   3
            TabIndex        =   3
            Top             =   360
            Width           =   735
         End
         Begin VB.TextBox txtde5a15 
            Height          =   285
            Left            =   2880
            MaxLength       =   3
            TabIndex        =   2
            Top             =   360
            Width           =   735
         End
         Begin VB.TextBox txtvista 
            Height          =   285
            Left            =   720
            MaxLength       =   3
            TabIndex        =   1
            Top             =   360
            Width           =   735
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            Caption         =   "Acima de 15 dias"
            Height          =   195
            Left            =   3840
            TabIndex        =   14
            Top             =   360
            Width           =   1215
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "De 5 a 15 dias"
            Height          =   195
            Left            =   1800
            TabIndex        =   13
            Top             =   360
            Width           =   1035
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   " � Vista"
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   12
            Top             =   360
            Width           =   540
         End
      End
      Begin VB.TextBox txttotalprotestos 
         Height          =   285
         Left            =   5160
         MaxLength       =   3
         TabIndex        =   5
         Top             =   1320
         Width           =   735
      End
      Begin VB.Frame Frame2 
         Height          =   1215
         Left            =   4320
         TabIndex        =   9
         Top             =   1680
         Width           =   1815
         Begin Threed.SSCommand cmdvoltar 
            Height          =   495
            Left            =   1080
            TabIndex        =   7
            Top             =   240
            Width           =   495
            _Version        =   65536
            _ExtentX        =   873
            _ExtentY        =   873
            _StockProps     =   78
            Picture         =   "frmrest.frx":0000
         End
         Begin Threed.SSCommand cmdalterar 
            Height          =   495
            Left            =   240
            TabIndex        =   6
            Top             =   240
            Width           =   495
            _Version        =   65536
            _ExtentX        =   873
            _ExtentY        =   873
            _StockProps     =   78
            Picture         =   "frmrest.frx":0452
         End
         Begin VB.Label lblmsg 
            AutoSize        =   -1  'True
            BackColor       =   &H0000FFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "lblmsg"
            Height          =   255
            Left            =   240
            TabIndex        =   15
            Top             =   840
            Visible         =   0   'False
            Width           =   495
         End
      End
      Begin VB.TextBox txtrestricao 
         Height          =   1335
         Left            =   960
         MaxLength       =   100
         MultiLine       =   -1  'True
         TabIndex        =   4
         Top             =   1440
         Width           =   2655
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Total de Protestos"
         Height          =   195
         Left            =   3720
         TabIndex        =   10
         Top             =   1320
         Width           =   1290
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Restri��o"
         Height          =   195
         Left            =   240
         TabIndex        =   8
         Top             =   1440
         Width           =   675
      End
   End
End
Attribute VB_Name = "frmrestricao"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Text3_Change()

End Sub


Private Sub cmdalterar_Click()
On Error GoTo trata_erro

Dim resp As Integer

    resp = MsgBox("Confirma a altera��o das Informa��es?", vbQuestion + vbYesNo, "Altera��o")
        If resp = 6 Then
            Screen.MousePointer = 11
            PL1 = "Begin"
            PL1 = PL1 & " Update sci.restricao "
            PL1 = PL1 & " Set restricao = :restricao,"
            PL1 = PL1 & " total_protesto = :total "
            PL1 = PL1 & " Where cod_cliente = :cliente;"
            PL1 = PL1 & " If sql%notfound then"
            PL1 = PL1 & " Insert into sci.restricao"
            PL1 = PL1 & " (cod_cliente,restricao,total_protesto)"
            PL1 = PL1 & " values (:cliente,:restricao,:total);"
            PL1 = PL1 & " End if;"
            PL1 = PL1 & " Update sci.fornecedor"
            PL1 = PL1 & " Set qtd_forn_vista = :vista,"
            PL1 = PL1 & " qtd_forn_5 = :forn5,"
            PL1 = PL1 & " qtd_forn_15 = :forn15"
            PL1 = PL1 & " where cod_cliente = :cliente;"
            PL1 = PL1 & " If sql%notfound then"
            PL1 = PL1 & " Insert into sci.fornecedor"
            PL1 = PL1 & " (cod_cliente,qtd_forn_vista,qtd_forn_5,qtd_forn_15)"
            PL1 = PL1 & " Values(:cliente,:vista,:forn5,:forn15);"
            PL1 = PL1 & " End if;"
            PL1 = PL1 & " Commit;"
            PL1 = PL1 & " Exception"
            PL1 = PL1 & " When others then"
            PL1 = PL1 & " Rollback;"
            PL1 = PL1 & " :cod_errora := sqlcode;"
            PL1 = PL1 & " :txt_errora := sqlerrm;"
            PL1 = PL1 & " End;"
            
            
            oradatabase.Parameters.Remove "restricao"
            oradatabase.Parameters.Remove "total"
            oradatabase.Parameters.Remove "cliente"
            oradatabase.Parameters.Remove "vista"
            oradatabase.Parameters.Remove "forn5"
            oradatabase.Parameters.Remove "forn15"
            oradatabase.Parameters.Remove "cod_errora"
            oradatabase.Parameters.Remove "txt_errora"
            oradatabase.Parameters.Add "restricao", IIf(IsNull(txtrestricao.Text), "", txtrestricao.Text), 1
            oradatabase.Parameters.Add "total", IIf(IsNull(txttotalprotestos.Text), "", txttotalprotestos.Text), 1
            oradatabase.Parameters.Add "cliente", frmsci.txtcliente.Text, 1
            oradatabase.Parameters.Add "vista", IIf(IsNull(txtvista.Text), "", txtvista.Text), 1
            oradatabase.Parameters.Add "forn5", IIf(IsNull(txtde5a15.Text), "", txtde5a15.Text), 1
            oradatabase.Parameters.Add "forn15", IIf(IsNull(txtacimade15.Text), "", txtacimade15.Text), 1
            oradatabase.Parameters.Add "cod_errora", 0, 2
            oradatabase.Parameters.Add "txt_errora", "", 2
            
            
            oradatabase.ExecuteSQL PL1
            
            
             SQL = " Select a.cod_cliente, b.cod_cliente, b.restricao,b.total_protesto," & _
              " c.cod_cliente,c.qtd_forn_vista,c.qtd_forn_5,c.qtd_forn_15" & _
              " from cliente a, sci.restricao b, sci.fornecedor c " & _
              " where a.cod_cliente = " & frmsci.txtcliente.Text & " and a.cod_cliente = b.cod_cliente(+) " & _
              " and a.cod_cliente = c.cod_cliente"
          
        Set ss = oradatabase.CreateDynaset(SQL, 0&)
        
            
            frmsci.grdrestricao.Row = 0
            frmsci.grdrestricao.Cols = 5
            
            frmsci.grdrestricao.Col = 0
            frmsci.grdrestricao.ColWidth(0) = 1000
            frmsci.grdrestricao.Text = "Restri��o"
            
            frmsci.grdrestricao.Col = 1
            frmsci.grdrestricao.ColWidth(1) = 1200
            frmsci.grdrestricao.Text = "Total Protestos"
            
            frmsci.grdrestricao.Col = 2
            frmsci.grdrestricao.ColWidth(2) = 1200
            frmsci.grdrestricao.Text = "Forn. A Vista"
            
            frmsci.grdrestricao.Col = 3
            frmsci.grdrestricao.ColWidth(3) = 1200
            frmsci.grdrestricao.Text = "Forn. 5 dias"
            
            
            frmsci.grdrestricao.Col = 4
            frmsci.grdrestricao.ColWidth(4) = 1400
            frmsci.grdrestricao.Text = "Forn. Acima de 15 dias"
            
        
            i = 0
            
            Do While Not ss.EOF
                i = i + 1
                
                frmsci.grdrestricao.Rows = i + 1
                frmsci.grdrestricao.Row = i
                
                frmsci.grdrestricao.Col = 0
                frmsci.grdrestricao.Text = IIf(IsNull(ss!restricao), "", ss!restricao)
                
                frmsci.grdrestricao.Col = 1
                frmsci.grdrestricao.Text = IIf(IsNull(ss!total_protesto), "", ss!total_protesto)
                
                frmsci.grdrestricao.Col = 2
                frmsci.grdrestricao.Text = IIf(IsNull(ss!qtd_forn_vista), "", ss!qtd_forn_vista)
                
                frmsci.grdrestricao.Col = 3
                frmsci.grdrestricao.Text = IIf(IsNull(ss!qtd_forn_5), "", ss!qtd_forn_5)
                
                frmsci.grdrestricao.Col = 4
                frmsci.grdrestricao.Text = IIf(IsNull(ss!qtd_forn_15), "", ss!qtd_forn_15)
              
              ss.MoveNext
            Loop
            
            Screen.MousePointer = 0
            Unload Me
        End If

Exit Sub
trata_erro:
            If Err <> 0 Then
                MsgBox "Ocorreu o Erro" & Err & "-" & Err.Description & ".Ligue para o Depto. de Sistemas", vbCritical, "Erro"
                End
            End If


End Sub


Private Sub cmdalterar_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
lblmsg.Visible = True
lblmsg.Left = 240
lblmsg.Caption = "Alterar"


End Sub


Private Sub cmdvoltar_Click()
Unload Me

End Sub


Private Sub cmdvoltar_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
lblmsg.Visible = True
lblmsg.Left = 1080
lblmsg.Caption = "Voltar"
End Sub


Private Sub Frame1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
lblmsg.Visible = False

End Sub


Private Sub Frame2_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
lblmsg.Visible = False

End Sub


Private Sub lblmsg_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
lblmsg.Visible = False

End Sub


Private Sub txtacimade15_KeyPress(KeyAscii As Integer)
KeyAscii = Numerico(KeyAscii)

End Sub


Private Sub txtde5a15_KeyPress(KeyAscii As Integer)
KeyAscii = Numerico(KeyAscii)

End Sub


Private Sub txtrestricao_KeyPress(KeyAscii As Integer)
KeyAscii = Maiusculo(KeyAscii)

End Sub


Private Sub txttotalprotestos_KeyPress(KeyAscii As Integer)
KeyAscii = Numerico(KeyAscii)

End Sub


Private Sub txtvista_KeyPress(KeyAscii As Integer)
KeyAscii = Numerico(KeyAscii)

End Sub


