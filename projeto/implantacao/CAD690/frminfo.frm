VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form frminformacao 
   Caption         =   "Altera��o Informa��o"
   ClientHeight    =   3090
   ClientLeft      =   90
   ClientTop       =   3390
   ClientWidth     =   6690
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   3090
   ScaleWidth      =   6690
   Begin VB.Frame Frame1 
      Height          =   3015
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   6375
      Begin VB.Frame Frame2 
         Height          =   1215
         Left            =   4320
         TabIndex        =   7
         Top             =   1680
         Width           =   1815
         Begin Threed.SSCommand cmdvoltar 
            Height          =   495
            Left            =   1080
            TabIndex        =   9
            Top             =   240
            Width           =   495
            _Version        =   65536
            _ExtentX        =   873
            _ExtentY        =   873
            _StockProps     =   78
            Picture         =   "frminfo.frx":0000
         End
         Begin Threed.SSCommand cmdalterar 
            Height          =   495
            Left            =   240
            TabIndex        =   8
            Top             =   240
            Width           =   495
            _Version        =   65536
            _ExtentX        =   873
            _ExtentY        =   873
            _StockProps     =   78
            Picture         =   "frminfo.frx":0452
         End
         Begin VB.Label lblmsg 
            AutoSize        =   -1  'True
            BackColor       =   &H0000FFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "lblmsg"
            Height          =   255
            Left            =   240
            TabIndex        =   10
            Top             =   840
            Visible         =   0   'False
            Width           =   495
         End
      End
      Begin VB.TextBox txtqtdconsulta 
         Height          =   285
         Left            =   5160
         MaxLength       =   3
         TabIndex        =   6
         Top             =   1320
         Width           =   735
      End
      Begin VB.TextBox txtconsulta 
         Height          =   285
         Left            =   1560
         MaxLength       =   8
         TabIndex        =   4
         Top             =   1320
         Width           =   1215
      End
      Begin VB.TextBox txtsequencia 
         Height          =   285
         Left            =   1080
         TabIndex        =   1
         Top             =   720
         Width           =   735
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Qtd Consulta nos �lt. 6 meses"
         Height          =   195
         Left            =   3000
         TabIndex        =   5
         Top             =   1320
         Width           =   2130
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Data da  Consulta"
         Height          =   195
         Left            =   240
         TabIndex        =   3
         Top             =   1320
         Width           =   1275
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Sequ�ncia"
         Height          =   195
         Left            =   240
         TabIndex        =   2
         Top             =   720
         Width           =   765
      End
   End
End
Attribute VB_Name = "frminformacao"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdalterar_Click()
On Error GoTo trata_erro
Dim resp As Integer

    resp = MsgBox("Confirma a altera��o das Informa��es?", vbQuestion + vbYesNo, "Altera��o")
        If resp = 6 Then
            Screen.MousePointer = 11
            PL1 = "Begin"
            PL1 = PL1 & " Update sci.informacao "
            PL1 = PL1 & " Set dt_consulta = :consulta,"
            PL1 = PL1 & " qtd_consulta = :quantidade "
            PL1 = PL1 & " Where cod_cliente = :cliente and "
            PL1 = PL1 & " sequencia = :sequencia;"
            PL1 = PL1 & " Commit;"
            PL1 = PL1 & " Exception"
            PL1 = PL1 & " When others then"
            PL1 = PL1 & " Rollback;"
            PL1 = PL1 & " :cod_errora := sqlcode;"
            PL1 = PL1 & " :txt_errora := sqlerrm;"
            PL1 = PL1 & " End;"
            
            
            oradatabase.Parameters.Remove "consulta"
            oradatabase.Parameters.Remove "quantidade"
            oradatabase.Parameters.Remove "cliente"
            oradatabase.Parameters.Remove "sequencia"
            oradatabase.Parameters.Remove "cod_errora"
            oradatabase.Parameters.Remove "txt_errora"
            oradatabase.Parameters.Add "consulta", IIf(IsNull(txtconsulta.Text), "", Mid(txtconsulta, 1, 2) & Mid(txtconsulta, 4, 2) & Mid(txtconsulta, 7, 2)), 1
            oradatabase.Parameters.Add "quantidade", IIf(IsNull(txtqtdconsulta.Text), "", txtqtdconsulta), 1
            oradatabase.Parameters.Add "cliente", frmsci.txtcliente.Text, 1
            oradatabase.Parameters.Add "sequencia", txtsequencia.Text, 1
            oradatabase.Parameters.Add "cod_errora", 0, 2
            oradatabase.Parameters.Add "txt_errora", "", 2
            
            
            oradatabase.ExecuteSQL PL1
            
            
            
            
            SQL = " Select cod_cliente, sequencia,dt_consulta,qtd_consulta from sci.informacao where cod_cliente = " & frmsci.txtcliente.Text & "order by sequencia"
        
            Set ss = oradatabase.CreateDynaset(SQL, 0&)
           
            frmsci.grdinformacao.Row = 0
            frmsci.grdinformacao.Cols = 3
            
            
            frmsci.grdinformacao.Col = 0
            frmsci.grdinformacao.ColWidth(0) = 500
            frmsci.grdinformacao.Text = "Seq."
            
            frmsci.grdinformacao.Col = 1
            frmsci.grdinformacao.ColWidth(1) = 1250
            frmsci.grdinformacao.Text = "Data Consulta"
            
            frmsci.grdinformacao.Col = 2
            frmsci.grdinformacao.ColWidth(2) = 700
            frmsci.grdinformacao.Text = "Qtd."
            
            i = 0
                
            Do While Not ss.EOF
                i = i + 1
             
                frmsci.grdinformacao.Rows = i + 1
                frmsci.grdinformacao.Row = i
                
                frmsci.grdinformacao.Col = 0
                frmsci.grdinformacao.Text = ss!sequencia
                
                frmsci.grdinformacao.Col = 1
                frmsci.grdinformacao.Text = IIf(IsNull(ss!dt_consulta), "", ss!dt_consulta)
                
                frmsci.grdinformacao.Col = 2
                frmsci.grdinformacao.Text = IIf(IsNull(ss!qtd_consulta), "", ss!qtd_consulta)
    
               ss.MoveNext
            Loop
            Screen.MousePointer = 0
            Unload Me
        End If

Exit Sub
trata_erro:
            If Err <> 0 Then
                MsgBox "Ocorreu o Erro" & Err & "-" & Err.Description & ".Ligue para o Depto. de Sistemas", vbCritical, "Erro"
                End
            End If


End Sub

Private Sub cmdalterar_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
lblmsg.Visible = True
lblmsg.Left = 240
lblmsg.Caption = "Alterar"

End Sub


Private Sub cmdvoltar_Click()
Unload Me
End Sub


Private Sub cmdvoltar_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
lblmsg.Visible = True
lblmsg.Left = 1080
lblmsg.Caption = "Voltar"

End Sub


Private Sub Frame1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
lblmsg.Visible = False

End Sub


Private Sub Frame2_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
lblmsg.Visible = False

End Sub


Private Sub lblmsg_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
lblmsg.Visible = False

End Sub


Private Sub txtconsulta_KeyPress(KeyAscii As Integer)
Data KeyAscii, txtconsulta

End Sub


Private Sub txtqtdconsulta_KeyPress(KeyAscii As Integer)
KeyAscii = Numerico(KeyAscii)

End Sub


