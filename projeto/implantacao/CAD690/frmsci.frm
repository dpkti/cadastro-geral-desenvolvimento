VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Begin VB.Form frmsci 
   Caption         =   "Cadastro de SCI"
   ClientHeight    =   6660
   ClientLeft      =   60
   ClientTop       =   1545
   ClientWidth     =   6705
   Icon            =   "frmsci.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6660
   ScaleWidth      =   6705
   Begin VB.Frame Frame1 
      Height          =   6615
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   6375
      Begin VB.Frame fraconsulta 
         Caption         =   "Consulta"
         Height          =   6615
         Left            =   0
         TabIndex        =   37
         Top             =   0
         Visible         =   0   'False
         Width           =   6375
         Begin VB.TextBox txtcliente 
            Enabled         =   0   'False
            Height          =   285
            Left            =   2040
            TabIndex        =   50
            Top             =   240
            Width           =   855
         End
         Begin VB.Frame frarestricao 
            Caption         =   "Restri��o e Fornecedores"
            Height          =   2415
            Left            =   3240
            TabIndex        =   44
            Top             =   3120
            Width           =   3015
            Begin MSGrid.Grid grdrestricao 
               Height          =   1815
               Left            =   120
               TabIndex        =   48
               Top             =   360
               Width           =   2775
               _Version        =   65536
               _ExtentX        =   4895
               _ExtentY        =   3201
               _StockProps     =   77
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
            End
         End
         Begin VB.Frame fraprotesto 
            Caption         =   "Protesto"
            Height          =   2415
            Left            =   120
            TabIndex        =   43
            Top             =   3120
            Width           =   3015
            Begin MSGrid.Grid grdprotesto 
               Height          =   1815
               Left            =   120
               TabIndex        =   47
               Top             =   360
               Width           =   2775
               _Version        =   65536
               _ExtentX        =   4895
               _ExtentY        =   3201
               _StockProps     =   77
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
            End
         End
         Begin VB.Frame frainformacao 
            Caption         =   "Informa��o"
            Height          =   2415
            Left            =   3240
            TabIndex        =   42
            Top             =   600
            Width           =   3015
            Begin MSGrid.Grid grdinformacao 
               Height          =   1815
               Left            =   120
               TabIndex        =   46
               Top             =   360
               Width           =   2775
               _Version        =   65536
               _ExtentX        =   4895
               _ExtentY        =   3201
               _StockProps     =   77
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
            End
         End
         Begin VB.Frame fracronologia 
            Caption         =   "Cronologia"
            Height          =   2415
            Left            =   120
            TabIndex        =   41
            Top             =   600
            Width           =   3015
            Begin MSGrid.Grid grdcronologia 
               Height          =   1815
               Left            =   120
               TabIndex        =   45
               Top             =   360
               Width           =   2775
               _Version        =   65536
               _ExtentX        =   4895
               _ExtentY        =   3201
               _StockProps     =   77
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
            End
         End
         Begin Threed.SSCommand cmdvolta 
            Height          =   495
            Left            =   5520
            TabIndex        =   38
            Top             =   5640
            Width           =   495
            _Version        =   65536
            _ExtentX        =   873
            _ExtentY        =   873
            _StockProps     =   78
            Picture         =   "frmsci.frx":0442
         End
         Begin VB.Label Label14 
            AutoSize        =   -1  'True
            Caption         =   "Cliente"
            BeginProperty Font 
               Name            =   "Times New Roman"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Left            =   1320
            TabIndex        =   49
            Top             =   240
            Width           =   600
         End
         Begin VB.Label Label13 
            Caption         =   "* Duplo Clique no Grid, Permite Altera��o das Informa��es   * Conceito: P = Pontual, A = Atrasado, R= Protestado"
            Height          =   375
            Left            =   240
            TabIndex        =   40
            Top             =   6000
            Width           =   4215
         End
         Begin VB.Label lblms 
            AutoSize        =   -1  'True
            BackColor       =   &H0000FFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "lblmsg"
            Height          =   255
            Left            =   5520
            TabIndex        =   39
            Top             =   6240
            Visible         =   0   'False
            Width           =   495
         End
      End
      Begin VB.ComboBox cbonome 
         Height          =   315
         Left            =   2040
         TabIndex        =   3
         Top             =   360
         Width           =   4095
      End
      Begin VB.Frame Frame7 
         Height          =   1095
         Left            =   3240
         TabIndex        =   31
         Top             =   5400
         Width           =   2895
         Begin Threed.SSCommand cmdconsultar 
            Height          =   495
            Left            =   720
            TabIndex        =   33
            Top             =   200
            Width           =   495
            _Version        =   65536
            _ExtentX        =   873
            _ExtentY        =   873
            _StockProps     =   78
            Picture         =   "frmsci.frx":0894
         End
         Begin Threed.SSCommand cmdvoltar 
            Height          =   495
            Left            =   2280
            TabIndex        =   35
            Top             =   195
            Width           =   495
            _Version        =   65536
            _ExtentX        =   873
            _ExtentY        =   873
            _StockProps     =   78
            Picture         =   "frmsci.frx":0BAE
         End
         Begin Threed.SSCommand cmdlimpar 
            Height          =   495
            Left            =   1320
            TabIndex        =   34
            Top             =   195
            Width           =   495
            _Version        =   65536
            _ExtentX        =   873
            _ExtentY        =   873
            _StockProps     =   78
            Picture         =   "frmsci.frx":1000
         End
         Begin Threed.SSCommand cmdconfirmar 
            Height          =   495
            Left            =   135
            TabIndex        =   32
            Top             =   195
            Width           =   495
            _Version        =   65536
            _ExtentX        =   873
            _ExtentY        =   873
            _StockProps     =   78
            Picture         =   "frmsci.frx":1452
         End
         Begin VB.Label lblmsg 
            AutoSize        =   -1  'True
            BackColor       =   &H0000FFFF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "lblmsg"
            Height          =   255
            Left            =   2280
            TabIndex        =   36
            Top             =   720
            Visible         =   0   'False
            Width           =   495
         End
      End
      Begin VB.Frame Frame6 
         Caption         =   "Quantidade de Fornecedores"
         Height          =   735
         Left            =   240
         TabIndex        =   24
         Top             =   4560
         Width           =   5895
         Begin VB.TextBox txtacimade15dias 
            Height          =   285
            Left            =   5040
            MaxLength       =   3
            TabIndex        =   30
            Top             =   360
            Width           =   495
         End
         Begin VB.TextBox txt5a15dias 
            Height          =   285
            Left            =   3000
            MaxLength       =   3
            TabIndex        =   28
            Top             =   360
            Width           =   495
         End
         Begin VB.TextBox txtavista 
            Height          =   285
            Left            =   840
            MaxLength       =   3
            TabIndex        =   26
            Top             =   360
            Width           =   495
         End
         Begin VB.Label Label12 
            Caption         =   "Acima de 15 dias"
            Height          =   255
            Left            =   3720
            TabIndex        =   29
            Top             =   360
            Width           =   1335
         End
         Begin VB.Label Label11 
            Caption         =   "De 5 a 15 dias"
            Height          =   255
            Left            =   1800
            TabIndex        =   27
            Top             =   360
            Width           =   1095
         End
         Begin VB.Label Label10 
            Caption         =   "� Vista"
            Height          =   255
            Left            =   120
            TabIndex        =   25
            Top             =   360
            Width           =   975
         End
      End
      Begin VB.Frame Frame5 
         Caption         =   "Restri��es"
         Height          =   1455
         Left            =   240
         TabIndex        =   19
         Top             =   3000
         Width           =   5895
         Begin VB.TextBox txttotalprotestos 
            Height          =   285
            Left            =   5040
            MaxLength       =   3
            TabIndex        =   23
            Top             =   360
            Width           =   495
         End
         Begin VB.TextBox txtrestricoes 
            Height          =   1095
            Left            =   1200
            MaxLength       =   100
            MultiLine       =   -1  'True
            TabIndex        =   21
            Top             =   240
            Width           =   2535
         End
         Begin VB.Label Label9 
            Caption         =   "Total Protestos"
            Height          =   375
            Left            =   3840
            TabIndex        =   22
            Top             =   360
            Width           =   1095
         End
         Begin VB.Label Label8 
            Caption         =   "Restri��es"
            Height          =   255
            Left            =   240
            TabIndex        =   20
            Top             =   360
            Width           =   975
         End
      End
      Begin VB.Frame Frame4 
         Caption         =   "Protesto"
         Height          =   615
         Left            =   240
         TabIndex        =   12
         Top             =   2280
         Width           =   5895
         Begin VB.TextBox txtvlprotesto 
            Height          =   285
            Left            =   4320
            MaxLength       =   9
            TabIndex        =   18
            Top             =   240
            Width           =   975
         End
         Begin VB.TextBox txtdtprotesto 
            Height          =   285
            Left            =   1200
            TabIndex        =   16
            Top             =   240
            Width           =   975
         End
         Begin VB.Label Label7 
            Caption         =   "Valor Protesto"
            Height          =   255
            Left            =   3240
            TabIndex        =   17
            Top             =   240
            Width           =   1095
         End
         Begin VB.Label Label6 
            Caption         =   "Data Protesto"
            Height          =   255
            Left            =   120
            TabIndex        =   15
            Top             =   240
            Width           =   1095
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Informa��o"
         Height          =   615
         Left            =   240
         TabIndex        =   7
         Top             =   1560
         Width           =   5895
         Begin VB.TextBox txtqtdconsulta 
            Height          =   285
            Left            =   4800
            MaxLength       =   3
            TabIndex        =   9
            Top             =   240
            Width           =   495
         End
         Begin VB.TextBox txtdtconsulta 
            Height          =   285
            Left            =   1200
            MaxLength       =   8
            TabIndex        =   8
            Top             =   240
            Width           =   975
         End
         Begin VB.Label Label5 
            Caption         =   "Qtd. Consultas nos �lt. 6 meses"
            Height          =   255
            Left            =   2520
            TabIndex        =   14
            Top             =   240
            Width           =   2295
         End
         Begin VB.Label Label4 
            Caption         =   "Data Consulta"
            Height          =   255
            Left            =   120
            TabIndex        =   13
            Top             =   240
            Width           =   1095
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Cronologia"
         Height          =   615
         Left            =   240
         TabIndex        =   4
         Top             =   840
         Width           =   5895
         Begin VB.ComboBox cboconceito 
            Height          =   315
            Left            =   3600
            TabIndex        =   6
            Top             =   240
            Width           =   1695
         End
         Begin VB.TextBox txtdtpagamento 
            Height          =   285
            Left            =   1440
            MaxLength       =   8
            TabIndex        =   5
            Top             =   255
            Width           =   975
         End
         Begin VB.Label Label3 
            Caption         =   "Data Pagamento"
            Height          =   255
            Left            =   120
            TabIndex        =   11
            Top             =   240
            Width           =   1215
         End
         Begin VB.Label Label2 
            Caption         =   "Conceito"
            Height          =   255
            Left            =   2880
            TabIndex        =   10
            Top             =   240
            Width           =   855
         End
      End
      Begin VB.TextBox txtcodigo 
         Height          =   285
         Left            =   960
         MaxLength       =   6
         TabIndex        =   2
         Top             =   360
         Width           =   870
      End
      Begin VB.Label Label1 
         Caption         =   "Cliente"
         Height          =   255
         Left            =   360
         TabIndex        =   1
         Top             =   360
         Width           =   495
      End
   End
End
Attribute VB_Name = "frmsci"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cboconceito_KeyPress(KeyAscii As Integer)
KeyAscii = 0

End Sub


Private Sub cbonome_Click()

    If cbonome.Text <> "" Then
        sql = "Select cod_cliente from  cliente where nome_cliente like '%" & cbonome.Text & "%'"
        Set ss = oradatabase.CreateDynaset(sql, 0&)
        ss.MoveFirst
        If ss.EOF = False Then
            cbonome.Enabled = False
            txtcodigo.Text = ss!cod_cliente
            txtcodigo.Enabled = False
        
          sql = "Select cod_cliente from sci.cronologia where cod_cliente = " & txtcodigo.Text
            Set ss = oradatabase.CreateDynaset(sql, 8&)
            FreeLocks
            ss.MoveFirst
            
            If ss.EOF = False Then
               resp = MsgBox(" Esse Cliente j� possui Cadastro de SCI.Deseja Visualisar?", vbQuestion + vbYesNo, "Aten��o")
               If resp = 6 Then
                    Call cmdconsultar_Click
                Else
                    Frame5.Enabled = False
                    Frame6.Enabled = False
                End If
            End If
        
        Else
            MsgBox "N�o foi Encontrado o C�digo deste Cliente", vbCritical, "Aten��o"
        End If
    End If
    
        
    
End Sub

Private Sub cbonome_KeyPress(KeyAscii As Integer)

KeyAscii = Maiusculo(KeyAscii)

End Sub


Private Sub cbonome_LostFocus()
    
    If cbonome.Text <> "" And txtcodigo.Text = "" Then
        Screen.MousePointer = 11
        sql = "Select nome_cliente, cod_cliente from cliente where nome_cliente like '%" & cbonome.Text & "%' order by nome_cliente"
        Set ss = oradatabase.CreateDynaset(sql, 0&)
        FreeLocks
        ss.MoveFirst
        
        If ss.EOF = False Then
           If ss.RecordCount > 1 Then
             If cbonome.ListCount = 0 Then
                 While ss.EOF = False
                    cbonome.AddItem ss!nome_cliente
                    ss.MoveNext
                 Wend
             End If
                 cbonome.Text = ""
                 cbonome.SetFocus
                MsgBox "Encontrei mais de um cliente, verifique a lista", vbExclamation, "Aten��o"
                Screen.MousePointer = 0
            Else
                cbonome.Text = ss!nome_cliente
                txtcodigo.Text = ss!cod_cliente
                cbonome.Enabled = False
                txtcodigo.Enabled = False
                  sql = "Select cod_cliente from sci.cronologia where cod_cliente = " & txtcodigo.Text
                  Set ss = oradatabase.CreateDynaset(sql, 8&)
                  FreeLocks
                  ss.MoveFirst
            
                    If ss.EOF = False Then
                       resp = MsgBox(" Esse Cliente j� possui Cadastro de SCI.Deseja Visualisar?", vbQuestion + vbYesNo, "Aten��o")
                       If resp = 6 Then
                            Call cmdconsultar_Click
                        Else
                            Frame5.Enabled = False
                            Frame6.Enabled = False
                        End If
                    End If
                Screen.MousePointer = 0
            
            
            End If
            
        Else
            MsgBox "Cliente n�o Encontrado", vbCritical, "Aten��o"
            cbonome.Text = ""
            cbonome.SetFocus
            Screen.MousePointer = 0
        End If
    
   End If
            
End Sub


Private Sub cmdconfirmar_Click()
On Error GoTo trata_erro
Dim resp As Integer
Dim s1 As Double
Dim s2 As Double
Dim s3 As Double

 lblmsg.Visible = False
 verifica_formulario
  If fl = 0 Then
        resp = MsgBox("Confirma a Inclus�o deste Cadastro", vbQuestion + vbYesNo, "Aten��o")
        If resp = 6 Then
        
           Screen.MousePointer = 11
        
            PL1 = ""
            
            oradatabase.Parameters.Remove "cliente"
            oradatabase.Parameters.Add "cliente", txtcodigo.Text, 1
            
            If txtdtpagamento.Text <> "" Or cboconceito.Text <> "" Then
                
                sql = "Select sci.seq_cronologia.nextval seqinfo from dual "
                Set ss = oradatabase.CreateDynaset(sql, 0&)
                s1 = CDbl(ss!seqinfo)
                
                oradatabase.Parameters.Remove "info"
                oradatabase.Parameters.Add "info", s1, 1
                oradatabase.Parameters.Remove "pagamento"
                oradatabase.Parameters.Add "pagamento", IIf(IsNull(txtdtpagamento.Text), "", Mid(txtdtpagamento.Text, 1, 2) & Mid(txtdtpagamento.Text, 4, 2) & Mid(txtdtpagamento.Text, 7, 2)), 1
                oradatabase.Parameters.Remove "conceito"
                If cboconceito = "PONTUAL" Then
                    oradatabase.Parameters.Add "conceito", "P", 1
                ElseIf cboconceito = "PROTESTADO" Then
                    oradatabase.Parameters.Add "conceito", "R", 1
                ElseIf cboconceito = "ATRASADO" Then
                    oradatabase.Parameters.Add "conceito", "A", 1
                Else
                    oradatabase.Parameters.Add "conceito", "", 1
                End If
                PL1 = PL1 & "Insert into sci.cronologia (cod_cliente,sequencia,dt_pagto,conceito) "
                PL1 = PL1 & "Values(:cliente,:info,:pagamento,:conceito); "
                        
            End If
            
            If txtdtconsulta.Text <> "" Or txtqtdconsulta.Text <> "" Then
                
                sql = "Select sci.seq_informacao.nextval seqinfo from dual "
                Set ss = oradatabase.CreateDynaset(sql, 0&)
                s2 = CDbl(ss!seqinfo)
                
                oradatabase.Parameters.Remove "seq"
                oradatabase.Parameters.Add "seq", s2, 1
                oradatabase.Parameters.Remove "consulta"
                oradatabase.Parameters.Add "consulta", IIf(IsNull(txtdtconsulta.Text), "", Mid(txtdtconsulta.Text, 1, 2) & Mid(txtdtconsulta.Text, 4, 2) & Mid(txtdtconsulta.Text, 7, 2)), 1
                oradatabase.Parameters.Remove "qtdconsulta"
                oradatabase.Parameters.Add "qtdconsulta", IIf(IsNull(txtqtdconsulta.Text), "", txtqtdconsulta.Text), 1
                
                PL1 = PL1 & "Insert into sci.informacao(cod_cliente,sequencia,dt_consulta,qtd_consulta) "
                PL1 = PL1 & "Values (:cliente,:seq,:consulta,:qtdconsulta); "
            
            End If
            
            If txtdtprotesto.Text <> "" Or txtvlprotesto.Text <> "" Then
                
                sql = "Select sci.seq_protesto.nextval seqinfo from dual "
                Set ss = oradatabase.CreateDynaset(sql, 0&)
                s3 = CDbl(ss!seqinfo)

                oradatabase.Parameters.Remove "sequencia"
                oradatabase.Parameters.Add "sequencia", s3, 1
                oradatabase.Parameters.Remove "protesto"
                oradatabase.Parameters.Add "protesto", IIf(IsNull(txtdtprotesto.Text), "", Mid(txtdtprotesto.Text, 1, 2) & Mid(txtdtprotesto.Text, 4, 2) & Mid(txtdtprotesto.Text, 7, 2)), 1
                oradatabase.Parameters.Remove "valor"
                oradatabase.Parameters.Add "valor", IIf(IsNull(txtvlprotesto.Text), "", txtvlprotesto.Text), 1
                
                PL1 = PL1 & "Insert into sci.protesto (cod_cliente, sequencia, dt_protesto, vl_protesto) "
                PL1 = PL1 & "Values (:cliente,:sequencia,:protesto,:valor); "
                
            End If
            
            If txtrestricoes.Text <> "" Or txttotalprotestos.Text <> "" Then
                
                oradatabase.Parameters.Remove "rest"
                oradatabase.Parameters.Add "rest", IIf(IsNull(txtrestricoes.Text), "", txtrestricoes.Text), 1
                oradatabase.Parameters.Remove "total"
                oradatabase.Parameters.Add "total", IIf(IsNull(txttotalprotestos.Text), "", txttotalprotestos.Text), 1
                
                PL1 = PL1 & "Insert into sci.restricao(cod_cliente,restricao,total_protesto) "
                PL1 = PL1 & "Values (:cliente,:rest,:total); "
                                         
                Frame5.Enabled = False
            End If
            
            If txtavista.Text <> "" Or txt5a15dias.Text <> "" Or txtacimade15dias.Text <> "" Then
                
                oradatabase.Parameters.Remove "avista"
                oradatabase.Parameters.Add "avista", IIf(IsNull(txtavista.Text), "", Val(txtavista.Text)), 1
                oradatabase.Parameters.Remove "de5a15dias"
                oradatabase.Parameters.Add "de5a15dias", IIf(IsNull(txt5a15dias.Text), "", Val(txt5a15dias.Text)), 1
                oradatabase.Parameters.Remove "acimade15"
                oradatabase.Parameters.Add "acimade15", IIf(IsNull(txtacimade15dias.Text), "", Val(txtacimade15dias.Text)), 1
                
                PL1 = PL1 & "Insert into sci.fornecedor (cod_cliente, qtd_forn_vista, qtd_forn_5, qtd_forn_15) "
                PL1 = PL1 & "Values (:cliente, :avista, :de5a15dias, :acimade15); "
                                
                Frame6.Enabled = False
                
                
            End If
                
            oradatabase.Parameters.Remove "cod_errora"
            oradatabase.Parameters.Add "cod_errora", 0, 2
            oradatabase.Parameters.Remove "txt_errora"
            oradatabase.Parameters.Add "txt_errora", "", 2
                
            PL1 = "Begin " & PL1 & "Commit; "
            PL1 = PL1 & " Exception"
            PL1 = PL1 & " When others then"
            PL1 = PL1 & " Rollback; "
            PL1 = PL1 & " :cod_errora := sqlcode; "
            PL1 = PL1 & " :txt_errora := sqlerrm; "
            PL1 = PL1 & " End; "
                
            oradatabase.ExecuteSQL PL1
                
            If Val(oradatabase.Parameters("cod_errora")) <> 0 Then
                    MsgBox oradatabase.Parameters("txt_errora") & ".Ligue para o Depto. de Sistemas"
                    Screen.MousePointer = 0
                    Exit Sub
            Else
                    MsgBox "A Inclus�o Foi Realizada com Sucesso", vbExclamation, "Conclu�do"
                    txtdtpagamento.Text = ""
                    cboconceito.Text = ""
                    txtdtconsulta.Text = ""
                    txtqtdconsulta.Text = ""
                    txtdtprotesto.Text = ""
                    txtvlprotesto.Text = ""
                    txtrestricoes.Text = ""
                    txttotalprotestos.Text = ""
                    txtavista.Text = ""
                    txt5a15dias.Text = ""
                    txtacimade15dias.Text = ""
                    txtdtpagamento.SetFocus
                    Screen.MousePointer = 0
            End If
        End If
    End If

Exit Sub
trata_erro:
        If Err <> 0 Then
            MsgBox "Ocorreu o Erro" & Err & "-" & Err.Description & ".Ligue para o Depto. de Sistemas", vbCritical, "Erro"
            End
        End If
        

End Sub

Private Sub cmdconfirmar_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
lblmsg.Visible = True
lblmsg.Left = 120
lblmsg.Caption = "Confirmar"

End Sub


Private Sub cmdli_Click()

End Sub

Private Sub cmdconsultar_Click()
On Error GoTo trata_erro

Dim i As Integer

lblmsg.Visible = False
verifica_formulario
If fl = 0 Then
Screen.MousePointer = 11
    
    sql = " Select a.cod_cliente, b.cod_cliente,c.cod_cliente,d.cod_cliente,e.cod_cliente,f.cod_cliente" & _
          " from cliente a, sci.cronologia b, sci.informacao c, sci.protesto d, sci.restricao e, sci.fornecedor f " & _
          " where a.cod_cliente = " & txtcodigo.Text & " and a.cod_cliente = b.cod_cliente(+) and " & _
          " a.cod_cliente = c.cod_cliente(+) and a.cod_cliente = d.cod_cliente(+) and " & _
          " a.cod_cliente = e.cod_cliente(+) and a.cod_cliente = f.cod_cliente(+)"
    
    Set ss = oradatabase.CreateDynaset(sql, 0&)
    
    txtcliente.Text = txtcodigo.Text
    If ss.EOF = True Then
        MsgBox "N�o Existe SCI Cadastrado para este Cliente", vbExclamation, "Aten��o"
        Screen.MousePointer = 0
        txtdtpagamento.SetFocus
    Else
    
        fraconsulta.Visible = True
       
        sql = " Select cod_cliente, sequencia,dt_pagto,conceito from sci.cronologia where cod_cliente = " & txtcodigo.Text & "order by sequencia"
        
        Set ss = oradatabase.CreateDynaset(sql, 0&)
           
            grdcronologia.Row = 0
            grdcronologia.Cols = 3
            
            
            grdcronologia.Col = 0
            grdcronologia.ColWidth(0) = 500
            grdcronologia.Text = "Seq."
            
            grdcronologia.Col = 1
            grdcronologia.ColWidth(1) = 1250
            grdcronologia.Text = "Data Pagamento"
            
            grdcronologia.Col = 2
            grdcronologia.ColWidth(2) = 700
            grdcronologia.Text = "Conceito"
            
            i = 0
                
            Do While Not ss.EOF
                i = i + 1
             
                grdcronologia.Enabled = True
                grdcronologia.Rows = i + 1
                grdcronologia.Row = i
                
                grdcronologia.Col = 0
                grdcronologia.Text = ss!sequencia
                
                grdcronologia.Col = 1
                grdcronologia.Text = IIf(IsNull(ss!dt_pagto), "", ss!dt_pagto)
                
                grdcronologia.Col = 2
                grdcronologia.Text = IIf(IsNull(ss!conceito), "", ss!conceito)
    
               ss.MoveNext
            Loop
            
        sql = " Select cod_cliente, sequencia,dt_consulta,qtd_consulta from sci.informacao where cod_cliente = " & txtcodigo.Text & "order by sequencia"
        
        Set ss = oradatabase.CreateDynaset(sql, 0&)
        
        
            grdinformacao.Row = 0
            grdinformacao.Cols = 3
            
            
            grdinformacao.Col = 0
            grdinformacao.ColWidth(0) = 500
            grdinformacao.Text = "Seq."
            
            grdinformacao.Col = 1
            grdinformacao.ColWidth(1) = 1250
            grdinformacao.Text = "Data Consulta"
            
            grdinformacao.Col = 2
            grdinformacao.ColWidth(2) = 700
            grdinformacao.Text = "Qtd."
            
            i = 0
                
            Do While Not ss.EOF
                i = i + 1
             
                grdinformacao.Enabled = True
                grdinformacao.Rows = i + 1
                grdinformacao.Row = i
                
                grdinformacao.Col = 0
                grdinformacao.Text = ss!sequencia
                
                grdinformacao.Col = 1
                grdinformacao.Text = IIf(IsNull(ss!dt_consulta), "", ss!dt_consulta)
                
                grdinformacao.Col = 2
                grdinformacao.Text = IIf(IsNull(ss!qtd_consulta), "", ss!qtd_consulta)
    
               ss.MoveNext
            Loop
    
    sql = "Select cod_cliente, sequencia, dt_protesto,vl_protesto from sci.protesto where cod_cliente = " & txtcodigo.Text & " order by sequencia"
    
    Set ss = oradatabase.CreateDynaset(sql, 0&)
    
                
            grdprotesto.Row = 0
            grdprotesto.Cols = 3
            
            grdprotesto.Col = 0
            grdprotesto.ColWidth(0) = 500
            grdprotesto.Text = "Seq."
            
            grdprotesto.Col = 1
            grdprotesto.ColWidth(1) = 1250
            grdprotesto.Text = "Data Protesto"
            
            grdprotesto.Col = 2
            grdprotesto.ColWidth(2) = 700
            grdprotesto.Text = "Valor"
                
            i = 0
            
            Do While Not ss.EOF
                i = i + 1
                
                grdprotesto.Enabled = True
                grdprotesto.Rows = i + 1
                grdprotesto.Row = i
                
                grdprotesto.Col = 0
                grdprotesto.Text = ss!sequencia
                
                grdprotesto.Col = 1
                grdprotesto.Text = IIf(IsNull(ss!dt_protesto), "", ss!dt_protesto)
                
                grdprotesto.Col = 2
                grdprotesto.Text = IIf(IsNull(ss!vl_protesto), "", ss!vl_protesto)
             
             ss.MoveNext
            Loop
            
        sql = " Select a.cod_cliente, b.cod_cliente, b.restricao,b.total_protesto," & _
              " c.cod_cliente,c.qtd_forn_vista,c.qtd_forn_5,c.qtd_forn_15" & _
              " from cliente a, sci.restricao b, sci.fornecedor c " & _
              " where a.cod_cliente = " & txtcodigo.Text & " and a.cod_cliente = b.cod_cliente(+) " & _
              " and a.cod_cliente = c.cod_cliente(+)"
          
        Set ss = oradatabase.CreateDynaset(sql, 0&)
        
            
            grdrestricao.Row = 0
            grdrestricao.Cols = 5
            
            grdrestricao.Col = 0
            grdrestricao.ColWidth(0) = 1500
            grdrestricao.Text = "Restri��o"
            
            grdrestricao.Col = 1
            grdrestricao.ColWidth(1) = 1400
            grdrestricao.Text = "Total Protestos"
            
            grdrestricao.Col = 2
            grdrestricao.ColWidth(2) = 1400
            grdrestricao.Text = "Forn. A Vista"
            
            grdrestricao.Col = 3
            grdrestricao.ColWidth(3) = 1400
            grdrestricao.Text = "De 5 a 15 dias"
            
            
            grdrestricao.Col = 4
            grdrestricao.ColWidth(4) = 1400
            grdrestricao.Text = "Acima de 15 dias"
            
        
            i = 0
            
            Do While Not ss.EOF
                i = i + 1
                
                
                grdrestricao.Rows = i + 1
                grdrestricao.Row = i
                
                grdrestricao.Col = 0
                grdrestricao.Text = IIf(IsNull(ss!restricao), "", ss!restricao)
                If grdrestricao.Text <> "" Then
                   grdrestricao.Enabled = True
                End If
                
                grdrestricao.Col = 1
                grdrestricao.Text = IIf(IsNull(ss!total_protesto), "", ss!total_protesto)
                If grdrestricao.Text <> "" Then
                   grdrestricao.Enabled = True
                End If
                
                grdrestricao.Col = 2
                grdrestricao.Text = IIf(IsNull(ss!qtd_forn_vista), "", ss!qtd_forn_vista)
                If grdrestricao.Text <> "" Then
                   grdrestricao.Enabled = True
                End If
                
                grdrestricao.Col = 3
                grdrestricao.Text = IIf(IsNull(ss!qtd_forn_5), "", ss!qtd_forn_5)
                If grdrestricao.Text <> "" Then
                   grdrestricao.Enabled = True
                End If
                
                grdrestricao.Col = 4
                grdrestricao.Text = IIf(IsNull(ss!qtd_forn_15), "", ss!qtd_forn_15)
                If grdrestricao.Text <> "" Then
                   grdrestricao.Enabled = True
                End If
              ss.MoveNext
            Loop
      End If
   Screen.MousePointer = 0
End If
    
    
Exit Sub
trata_erro:
        If Err <> 0 Then
            MsgBox "Ocorreu o Erro" & Err & "-" & Err.Description & ".Ligue para o Depto. de Sistemas", vbCritical, "Erro"
            End
        End If
        



End Sub

Private Sub cmdconsultar_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
lblmsg.Visible = True
lblmsg.Left = 720
lblmsg.Caption = "Consultar"

End Sub


Private Sub cmdlimpar_Click()
txtcodigo.Text = ""
txtcodigo.Enabled = True
cbonome.Text = ""
cbonome.Enabled = True
txtdtpagamento.Text = ""
cboconceito.Text = ""
txtdtconsulta.Text = ""
txtqtdconsulta.Text = ""
txtdtprotesto.Text = ""
txtvlprotesto.Text = ""
txtrestricoes.Text = ""
txttotalprotestos.Text = ""
txtavista.Text = ""
txt5a15dias.Text = ""
txtacimade15dias.Text = ""
txtcodigo.SetFocus
cbonome.Clear
Frame5.Enabled = True
Frame6.Enabled = True

End Sub

Private Sub cmdlimpar_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
lblmsg.Visible = True
lblmsg.Left = 1320
lblmsg.Caption = "Limpar"

End Sub


Private Sub cmdvo_Click()

End Sub

Private Sub cmdvolta_Click()
Call cmdlimpar_Click
fraconsulta.Visible = False
With grdcronologia
           
                .Rows = 2
                .Row = 1
                
                For i = 0 To .Cols - 1
                    .Col = i: .Text = ""
                Next i
    grdcronologia.Enabled = False
           End With
            
            With grdinformacao
           
                .Rows = 2
                .Row = 1
                
                For i = 0 To .Cols - 1
                    .Col = i: .Text = ""
                Next i
   grdinformacao.Enabled = False
           End With
            With grdprotesto
            
                .Rows = 2
                .Row = 1
                
                For i = 0 To .Cols - 1
                    .Col = i: .Text = ""
                Next i
    grdprotesto.Enabled = False
           End With
           
           With grdrestricao
           
                .Rows = 2
                .Row = 1
                
                For i = 0 To .Cols - 1
                    .Col = i: .Text = ""
                Next i
     grdrestricao.Enabled = False
           End With
End Sub

Private Sub cmdvolta_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
lblms.Visible = True
lblms.Caption = "Voltar"

End Sub


Private Sub cmdvoltar_Click()
Unload Me

End Sub


Private Sub cmdvoltar_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
lblmsg.Visible = True
lblmsg.Left = 2280
lblmsg.Caption = "Voltar"

End Sub


Private Sub Form_Load()

If cboconceito.ListCount = 0 Then
    cboconceito.AddItem "PONTUAL"
    cboconceito.AddItem "ATRASADO"
    cboconceito.AddItem "PROTESTADO"
End If

    '*****************************************************
    'Rotina utilizada pelo VDA070 - Bloqueio de Cr�dito
    'Abrindo direto na tela, para consulta dos SCI's
    If Mid(Command$, 1, 6) <> "" Then
       txtcodigo.Text = Command$
       Call txtcodigo_LostFocus
    End If
    '*****************************************************

End Sub

Private Sub Text1_Change()

End Sub


Private Sub Text1_KeyPress(KeyAscii As Integer)
KeyAscii = Numerico(KeyAscii)

End Sub


Private Sub fraconsulta_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
lblms.Visible = False

End Sub


Private Sub Frame1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
lblmsg.Visible = False

End Sub


Private Sub Grid1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
lblmsg.Visible = False

End Sub

Private Sub Frame7_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
lblmsg.Visible = False

End Sub


Private Sub grdcronologia_DblClick()
On Error GoTo trata_erro

grdcronologia.Col = 0
frmcronologia.txtsequencia.Text = grdcronologia.Text

grdcronologia.Col = 1
frmcronologia.txtpagamento.Text = Format(grdcronologia.Text, "00/00/00")

grdcronologia.Col = 2
 If grdcronologia.Text = "P" Then
    frmcronologia.cboconceito.Text = "PONTUAL"
 ElseIf grdcronologia.Text = "R" Then
    frmcronologia.cboconceito.Text = "PROTESTADO"
 ElseIf grdcronologia.Text = "A" Then
    frmcronologia.cboconceito.Text = "ATRASADO"
 Else
    frmcronologia.cboconceito.Text = ""
 End If

frmcronologia.txtsequencia.Enabled = False

frmcronologia.Show vbModal


Exit Sub
trata_erro:
            If Err <> 0 Then
                MsgBox "Ocorreu o Erro" & Err & "-" & Err.Description & ".Ligue para o Depto. de Sistemas", vbCritical, "Erro"
                End
            End If
            
End Sub

Private Sub grdinformacao_DblClick()
On Error GoTo trata_erro

grdinformacao.Col = 0
frminformacao.txtsequencia.Text = grdinformacao.Text

grdinformacao.Col = 1
frminformacao.txtconsulta.Text = Format(grdinformacao.Text, "00/00/00")

grdinformacao.Col = 2
frminformacao.txtqtdconsulta.Text = grdinformacao.Text

frminformacao.txtsequencia.Enabled = False
frminformacao.Show vbModal


Exit Sub
trata_erro:
            If Err <> 0 Then
                MsgBox "Ocorreu o Erro" & Err & "-" & Err.Description & ".Ligue para o Depto. de Sistemas", vbCritical, "Erro"
                End
            End If
End Sub

Private Sub grdprotesto_DblClick()
On Error GoTo trata_erro

grdprotesto.Col = 0
frmprotesto.txtsequencia.Text = grdprotesto.Text

grdprotesto.Col = 1
frmprotesto.txtprotesto.Text = Format(grdprotesto.Text, "00/00/00")

grdprotesto.Col = 2
frmprotesto.txtvalor.Text = grdprotesto.Text

frmprotesto.txtsequencia.Enabled = False

frmprotesto.Show vbModal

Exit Sub
trata_erro:
            If Err <> 0 Then
                MsgBox "Ocorreu o Erro" & Err & "-" & Err.Description & ".Ligue para o Depto. de Sistemas", vbCritical, "Erro"
                End
            End If

End Sub

Private Sub grdrestricao_DblClick()
On Error GoTo trata_erro

grdrestricao.Col = 0
frmrestricao.txtrestricao.Text = grdrestricao.Text

grdrestricao.Col = 2
frmrestricao.txtvista.Text = grdrestricao.Text

grdrestricao.Col = 3
frmrestricao.txtde5a15.Text = grdrestricao.Text

grdrestricao.Col = 4
frmrestricao.txtacimade15.Text = grdrestricao.Text

grdrestricao.Col = 1
frmrestricao.txttotalprotestos.Text = grdrestricao.Text

frmrestricao.Show vbModal

Exit Sub
trata_erro:
            If Err <> 0 Then
                MsgBox "Ocorreu o Erro" & Err & "-" & Err.Description & ".Ligue para o Depto. de Sistemas", vbCritical, "Erro"
                End
            End If

End Sub

Private Sub lblms_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
lblms.Visible = False

End Sub


Private Sub lblmsg_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
lblmsg.Visible = False

End Sub


Private Sub SSCommand1_Click()

End Sub

Private Sub txt5a15dias_KeyPress(KeyAscii As Integer)
KeyAscii = Numerico(KeyAscii)

End Sub


Private Sub txtacimade15dias_KeyPress(KeyAscii As Integer)
KeyAscii = Numerico(KeyAscii)

End Sub


Private Sub txtavista_KeyPress(KeyAscii As Integer)
KeyAscii = Numerico(KeyAscii)

End Sub


Private Sub txtcliente_KeyPress(KeyAscii As Integer)
KeyAscii = 0

End Sub


Private Sub txtcodigo_KeyPress(KeyAscii As Integer)
KeyAscii = Numerico(KeyAscii)

End Sub


Private Sub txtcodigo_LostFocus()

        If txtcodigo.Text <> "" Then
        sql = "Select nome_cliente from cliente where cod_cliente = " & txtcodigo.Text
        Set ss = oradatabase.CreateDynaset(sql, 8&)
        FreeLocks
        ss.MoveFirst
        
        If ss.EOF = False Then
            cbonome.Text = ss!nome_cliente
            cbonome.Enabled = False
            txtcodigo.Enabled = False
        Else
            MsgBox "C�digo de Cliente n�o Cadastrado", vbCritical, "Aten��o"
            txtcodigo.Text = ""
            txtcodigo.SetFocus
        End If
      End If
        
        If txtcodigo.Text <> "" Then
            sql = " Select cod_cliente from sci.cronologia where cod_cliente =  " & txtcodigo.Text & _
                  " union Select cod_cliente from sci.informacao where cod_cliente =  " & txtcodigo.Text & _
                  " union Select cod_cliente from sci.protesto where cod_cliente =  " & txtcodigo.Text & _
                  " union Select cod_cliente from sci.restricao where cod_cliente =  " & txtcodigo.Text & _
                  " union Select cod_cliente from sci.fornecedor where cod_cliente =  " & txtcodigo.Text

            Set ss = oradatabase.CreateDynaset(sql, 8&)
            FreeLocks
            ss.MoveFirst
            
            If ss.EOF = False Then
               resp = MsgBox(" Esse Cliente j� possui Cadastro de SCI.Deseja Visualisar?", vbQuestion + vbYesNo, "Aten��o")
               If resp = 6 Then
                    Call cmdconsultar_Click
                Else
                    sql = " Select cod_cliente from sci.restricao  where cod_cliente = " & txtcodigo.Text & _
                          " union  Select cod_cliente from sci.fornecedor  where cod_cliente = " & txtcodigo.Text
                     
                     Set ss = oradatabase.CreateDynaset(sql, 0&)
                     
                     If ss.EOF = False Then
                            Frame5.Enabled = False
                            Frame6.Enabled = False
                     End If
                End If
            End If
        End If
    
    
    
        
        
End Sub


Private Sub txtdtconsulta_KeyPress(KeyAscii As Integer)
data KeyAscii, txtdtconsulta

End Sub


Private Sub txtdtpagamento_KeyPress(KeyAscii As Integer)
data KeyAscii, txtdtpagamento

End Sub


Private Sub txtdtprotesto_KeyPress(KeyAscii As Integer)
data KeyAscii, txtdtprotesto

End Sub


Private Sub txtnome_Change()

End Sub

Private Sub txtnome_LinkNotify()

End Sub


Private Sub txtnome_LostFocus()
End Sub


Private Sub txtqtdconsulta_KeyPress(KeyAscii As Integer)
KeyAscii = Numerico(KeyAscii)

End Sub


Private Sub txtrestricoes_KeyPress(KeyAscii As Integer)
KeyAscii = Maiusculo(KeyAscii)

End Sub


Private Sub txttotalprotestos_KeyPress(KeyAscii As Integer)
KeyAscii = Numerico(KeyAscii)

End Sub


Private Sub txtvlprotesto_KeyPress(KeyAscii As Integer)
'KeyAscii = Numerico(KeyAscii)
valor KeyAscii, txtvlprotesto

End Sub


Private Sub txtvlprotesto_LostFocus()
'txtvlprotesto = txtvlprotesto.Text & ".00"
End Sub


