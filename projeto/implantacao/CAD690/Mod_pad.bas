Attribute VB_Name = "MOD_PAD"
'VARIAVEIS DE BANCO DE DADOS
Public orasession As Object
Public oradatabase As Object
Public oradynaset As Object
Public ss As Object
Public ds As Object
Public sql As String
Public PL1 As String
Public data_faturamento As Date
Public fl As Integer
Public FG As Integer
Public arq As String
Public retdir As String
Public dat As String
Public rec As String
Public flag As Boolean
Public cgc As String
Public icm As Double
Public cod_loja As Integer
Public arquivo As String
Public lin As Variant
Public a As Date
Public cont As Long
Public inicial As String
Public final As String
Public flcont As Boolean
Public login As String
Public erro As Variant
Public verdir As String
Public calog As String
Public caminho As String

Function BISSEXTO(ano As String) As Boolean
    
    Dim i As Long
    
    ano = Val(Mid(ano, 3, 2))

    'EXEMPLO: ANO = 97
    For i = 0 To 100 Step 4
        
        If i = 100 Then
            
            If Val(Mid(Str(i), 3, 2)) = ano Then
                BISSEXTO = True
                Exit Function
            Else
                BISSEXTO = False
                Exit Function
            End If
        
        ElseIf i = ano Then
            
            BISSEXTO = True
            Exit Function
        
        ElseIf i > ano Then
            
            BISSEXTO = False
            Exit Function
        
        End If
        
    Next i

End Function


Sub MMAA(ByRef KeyAscii, ByRef txtCampo)
    On Error GoTo TrataErro

    Dim bTam As Byte    'tamanho do campo JA digitado
    Dim strData As String
    Dim bKey As Byte
    bTam = Len(txtCampo.Text)
    
    If KeyAscii = 8 And bTam > 0 Then 'backspace
        If Mid$(txtCampo.Text, bTam) = "/" Then
            txtCampo.Text = Mid$(txtCampo.Text, 1, bTam - 2)
        Else
            txtCampo.Text = Mid$(txtCampo.Text, 1, bTam - 1)
        End If
            
    ElseIf Chr$(KeyAscii) >= "0" And Chr$(KeyAscii) <= "9" Then
        If bTam = 1 Then
            strData = txtCampo.Text & Chr$(KeyAscii)
            If CInt(strData) < 1 Or CInt(strData > 12) Then
                Beep
            Else
                txtCampo.Text = txtCampo.Text & Chr$(KeyAscii) & "/"
            End If
        
        ElseIf bTam = 5 Then
            strData = Mid$(txtCampo.Text, 1, 2)     'mes
            If CInt(strData) < 1 Or CInt(strData > 12) Then
               Beep
            Else
               strData = txtCampo.Text & Chr$(KeyAscii)
               If Not IsDate(CDate(strData)) Then
                  Beep
               Else
                  txtCampo.Text = strData
               End If
            End If
            
        ElseIf bTam < 5 Then
            bKey = KeyAscii
            
        Else
            bKey = 0
        End If
    Else
        Beep
    End If
    
    SendKeys "{END}"
    KeyAscii = bKey
    Exit Sub
    
TrataErro:

    If Err.Number = 13 Then
        MsgBox strData, vbInformation, "Data Inv�lida"
        KeyAscii = 0
        Beep
        Err.Clear
    End If

End Sub




Sub data(ByRef KeyAscii, ByRef txtCampo)
    On Error GoTo TrataErro

    Dim bTam As Byte    'tamanho do campo JA digitado
    Dim strData As String
    Dim bKey As Byte
    bTam = Len(txtCampo.Text)
    
    If KeyAscii = 8 And bTam > 0 Then 'backspace
        If Mid$(txtCampo.Text, bTam) = "/" Then
            txtCampo.Text = Mid$(txtCampo.Text, 1, bTam - 2)
        Else
            txtCampo.Text = Mid$(txtCampo.Text, 1, bTam - 1)
        End If
            
    ElseIf Chr$(KeyAscii) >= "0" And Chr$(KeyAscii) <= "9" Then
        If bTam = 1 Then
            strData = txtCampo.Text & Chr$(KeyAscii)
            If CInt(strData) < 1 Or CInt(strData > 31) Then
                Beep
            Else
                txtCampo.Text = txtCampo.Text & Chr$(KeyAscii) & "/"
            End If
            
        ElseIf bTam = 4 Then
            strData = Mid$(txtCampo.Text, 4) & Chr$(KeyAscii)
            If CInt(strData) < 1 Or CInt(strData > 12) Then
                Beep
            Else
                txtCampo.Text = txtCampo.Text & Chr$(KeyAscii) & "/"
            End If
        
        ElseIf bTam = 7 Then
            strData = Mid$(txtCampo.Text, 1, 2)     'dia
            If CInt(strData) < 1 Or CInt(strData > 31) Then
                Beep
            Else
                strData = Mid$(txtCampo.Text, 4, 2)     'mes
                If CInt(strData) < 1 Or CInt(strData > 12) Then
                    Beep
                Else
                    strData = txtCampo.Text & Chr$(KeyAscii)
                    If Not IsDate(CDate(strData)) Then
                        Beep
                    Else
                        txtCampo.Text = strData
                    End If
                End If
            End If
            
        ElseIf bTam < 8 Then
            bKey = KeyAscii
            
        Else
            bKey = 0
        End If
    Else
        Beep
    End If
    
    SendKeys "{END}"
    KeyAscii = bKey
    Exit Sub
    
TrataErro:

    If Err.Number = 13 Then
        MsgBox strData, vbInformation, "Data Inv�lida"
        KeyAscii = 0
        Beep
        Err.Clear
    End If

End Sub


Function CONV_VALOR(strValor As String) As String
    Dim i As Byte
    i = InStr(strValor, ",")
    Do Until i = 0
        Mid(strValor, i, 1) = "."
        i = InStr(strValor, ",")
    Loop
    CONV_VALOR = strValor
End Function



Public Sub MostraLog(Texto As String)
    frmarq.lstLog.AddItem Texto
    frmarq.lstLog.Refresh
End Sub

Sub Process_Line_Errors(ByRef sql)
    
    Dim iFnum As Integer
    'On Error GoTo Handler_Process_Line_Errors
                
    If Err.Number = 3186 Or Err.Number = 3188 Or Err.Number = 3260 Or Err.Number = 3197 Then
        Resume
    Else
       'iFnum = FreeFile
       'Open strPathUsuario & "ERRORLOG.TXT" For Append As #iFnum
       'Print #iFnum, ""
       'Print #iFnum, App.ProductName & " " & Date & " " & Time
       'Print #iFnum, "  Erro: " & Err.Number & " - " & Err.Description
       'Print #iFnum, "  " & SQL
       'Close #iFnum
    
        MsgBox "Ocorreu o erro: " & Err.Number & " -" & Err.Description & ". Ligue para o departamento de sistemas"
         
        'cursor
        Screen.MousePointer = vbDefault
        'MsgBox "Erro na aplica��o, ligue para o departamento de sistemas", vbCritical, "A T E N � � O"
    
        'para a aplicacao
        End

    End If

    Exit Sub

Handler_Process_Line_Errors:
    DoEvents
    Resume Next

End Sub

Function FmtBR(ByVal valor) As String

    Dim Temp As String
    Dim i As Integer
    
    Temp = Trim(valor)
        
    For i = 1 To Len(Temp)
        If Mid$(Temp, i, 1) = "," Then
            Mid$(Temp, i, 1) = "."
        End If
    Next i

    FmtBR = Temp

End Function


Function iRetornaDigito(ByVal iOrigem As Long) As Integer

    Dim iSoma As Integer
    Dim gResto As Single
    Dim iTam As Integer
    Dim iFator As Integer
    Dim iContador As Integer
    Dim sOrigem As String

    iTam = Len(Trim$(Str$(iOrigem)))
    iFator = 2
    
    sOrigem = Trim$(Str$(iOrigem))
    
    For iContador = iTam To 1 Step -1
        iSoma = iSoma + Val(Mid$(sOrigem, iContador, 1)) * iFator
        iFator = iFator + 1
    Next iContador
    
    gResto = iSoma Mod 11
    
    If gResto = 0 Then
        iRetornaDigito = 1
    ElseIf gResto = 1 Then
        iRetornaDigito = 0
    Else
        iRetornaDigito = 11 - gResto
    End If

End Function


Function Maiusculo(KeyAscii As Integer) As Integer
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    Maiusculo = KeyAscii
End Function

Function Numerico(ByVal KeyAscii As Integer) As Integer
    If KeyAscii = 8 Then    'BACKSPACE
        Numerico = KeyAscii
        Exit Function
    End If
    If Chr(KeyAscii) < "0" Or Chr(KeyAscii) > "9" Then
        KeyAscii = 0
    End If
    Numerico = KeyAscii
End Function

Function Texto(ByVal KeyAscii As Integer) As Integer
    If KeyAscii = 8 Then    'BACKSPACE
        Texto = KeyAscii
        Exit Function
    End If
    
    If Chr$(KeyAscii) = "'" Or Chr$(KeyAscii) = "," Or Chr$(KeyAscii) = ";" Or Chr$(KeyAscii) = "�" Or Chr$(KeyAscii) = "~" Then
        KeyAscii = 0
        Beep
    Else
        KeyAscii = Asc(UCase(Chr$(KeyAscii)))
    End If
    
    Texto = KeyAscii
End Function

Function valor(ByRef KeyAscii As Integer, strCampo As String) As Integer
    If KeyAscii = 8 Then    'BACKSPACE
        valor = KeyAscii
        Exit Function
    End If
    
    If Chr$(KeyAscii) = "," Or Chr$(KeyAscii) = "." Then
        If InStr(strCampo, ",") > 0 Or InStr(strCampo, ".") > 0 Then
            KeyAscii = 0
            Beep
        End If
    Else
        If Chr$(KeyAscii) < "0" Or Chr$(KeyAscii) > "9" Then
            KeyAscii = 0
            Beep
        End If
    End If
    
    valor = KeyAscii
End Function

Sub Hora(ByRef KeyAscii, ByRef txtCampo)

    On Error GoTo TrataErro

    Dim bTam As Byte    'tamanho do campo JA digitado
    Dim strData As String
    Dim bKey As Byte
    bTam = Len(txtCampo.Text)
    
    If Chr$(KeyAscii) >= "0" And Chr$(KeyAscii) <= "9" Then
    
        If bTam = 0 Then
            If Chr$(KeyAscii) = "0" Or Chr$(KeyAscii) = "1" Or Chr$(KeyAscii) = "2" Then
                bKey = KeyAscii
            Else
                Beep
                bKey = 0
            End If
                
        ElseIf bTam = 1 Then
            strData = txtCampo
            If Mid(strData, 1, 1) = "1" Or Mid(strData, 1, 1) = "0" Then
                txtCampo.Text = strData & Chr$(KeyAscii) & ":"
                
            ElseIf Mid(strData, 1, 1) = "2" And Chr$(KeyAscii) <= "3" Then
                txtCampo.Text = strData & Chr$(KeyAscii) & ":"
                
            Else
                Beep
                bKey = 0
                
            End If
            
        ElseIf bTam = 3 Then
            strData = txtCampo
            If Chr$(KeyAscii) < 6 Then
                bKey = KeyAscii
            Else
                Beep
                bKey = 0
            End If
            
        
        ElseIf bTam = 4 Then
            bKey = KeyAscii
            
        Else
            bKey = 0
            
        End If
        
    End If
      
    SendKeys "{END}"
    KeyAscii = bKey
    Exit Sub
    
TrataErro:

    If Err.Number = 13 Then
        MsgBox strData, vbInformation, "Hora Inv�lida"
        KeyAscii = 0
        Beep
        Err.Clear
    End If


End Sub


Public Sub BLOCO_PL_SQL()
 
End Sub


Public Sub VerArquivo()
 Dim oldfile As String
 
 'Verifica a existencia do arquivo no diret�rio especificado
    frmarq.lstLog.Visible = True
    MostraLog ("Verificando exist�ncia do Arquivo")
 
    sql = "select 'microempresa01' || to_char (dt_faturamento, 'mmyyyy') || '.txt' nome_arquivo from datas"
    Set ss = oradatabase.CreateDynaset(sql, 0&)
    ss.MoveFirst
 
 arq = ss!nome_arquivo
 retdir = ""
 
 retdir = Dir("C:\arq_fisc\" & arq)
 

     If retdir = "" Then
        MsgBox "N�o existe arquivo a ser importado", vbExclamation, "Aten��o"
        flag = False
    Else
       'Renomeia o arquivo, para apenas 8 caracteres
        MostraLog ("Renomeando o arquivo, para o Padr�o da Rede")
        oldfile = arq
        arq = Mid(oldfile, 13, 8) & ".txt"
        Name "C:\arq_fisc\" & oldfile As "H:\oracle\sistemas\vb\32bits\fis070\arq_fisc\" & arq
        MostraLog ("O Arquivo foi renomeado para " & arq & " pois deve conter apenas 8 Caracteres!")
        flag = True
    End If
    
 
 
End Sub


Public Sub VerData()
  Dim arquivo As String
  Dim linha As String
 
 arquivo = "H:\oracle\sistemas\vb\32bits\fis070\arq_fisc\" & arq
     'Abre o arquivo e seleciona a data para verificar se o m�s do arquivo � o m�s atual
      MostraLog ("Verificando a Data do Arquivo")
 
 Open arquivo For Input As #1
     Line Input #1, linha
        sql = "select '01' || to_char (dt_faturamento, 'mmyyyy')  nome_arquivo from datas"
        Set ss = oradatabase.CreateDynaset(sql, 0&)
        ss.MoveFirst
            rec = Format(ss!nome_arquivo, "0#/##/####")
            dat = Mid(linha, 26, 10)
Close #1
  
End Sub

Public Sub backup()
  
  Dim mes As String
 Dim ano As Integer
 Dim retval
 Dim cont As Integer
 Dim lin As String
 Dim ArqDel As String
 
 'Seleciona todos os dados da tabela
     sql = " select to_char (cod_loja, '00') loja, " & _
           " to_char(cgc, '00000000000000') cgc, " & _
           " rtrim(ltrim(replace(to_char(pc_icm,'00.00'),'.',''))) pc_icm " & _
           " from producao.aliquota_me"
       
       
    Set ss = oradatabase.CreateDynaset(sql, 0&)
    ss.MoveFirst
 
 'Cria o arquivo para o backup
 
  
  MostraLog ("Criando o Arquivo para o Backup")
 
     mes = Mid(arq, 3, 2) - 1
 
         If mes < 1 Then
            mes = 12
            ano = Mid(arq, 5, 4) - 1
            arquivo = "H:\oracle\sistemas\vb\32bits\fis070\bkp_fisc\" & "01" & mes & ano & ".txt"
        Else
            arquivo = "H:\oracle\sistemas\vb\32bits\fis070\bkp_fisc\" & "01" & Format(mes, "00") & Mid(arq, 5, 4) & ".txt"
        End If
   verdir = ""
   verdir = Dir(Mid(arquivo, 1, 53) & ".zip")
  
   If verdir = "" Then
          Open arquivo For Output As #1
            cont = 0
            'Acrescenta as informa��es dentro do arquivo
           
            MostraLog ("Copiando informa��es para o Arquivo")
                 Do While Not ss.EOF
                    Print #1, Trim(ss!loja) & Trim(ss!cgc) & Trim(ss!pc_icm)
                     ss.MoveNext
                Loop
           
        Close #1
          'Zipa o Arquivo e mata o txt
   
  
    MostraLog ("Zipando o Arquivo do Backup")
  
     DoEvents
     retval = Shell("H:\oracle\sistemas\vb\32bits\fis070\bkp_fisc\pkzip  H:\oracle\sistemas\vb\32bits\fis070\bkp_fisc\" & Mid(arquivo, 46, 8) & ".zip " & arquivo, 0)
     DoEvents
  
    MsgBox "Arquivo compactado. Pressione a Tecla Enter para continuar", 64, "Aviso"
  
    Kill arquivo
  End If
  


  frmarq.Refresh
  frmarq.lstLog.Refresh
End Sub

Private Sub LimpaGrid()
       
           With grdcronologia
           
                .Rows = 2
                .Row = 1
                
                For i = 0 To .Cols - 1
                    .Col = i: .Text = ""
                Next i
           
           End With
           
           

End Sub

Public Sub truncate()

    'Deleta todas as informa��es da Tabela
    MostraLog ("Preparando o Banco para Receber os Dados.Aguarde...")

    retdir = ""
    retdir = Dir("H:\oracle\sistemas\vb\32bits\fis070\bkp_fisc\" & Mid(arquivo, 46, 8) & ".zip ")
 
    If retdir = "" Then
        MsgBox " O Backup n�o foi executado Corretamente. O Programa ser� fechado", vbExclamation, "Aten��o"
        End
    Else
        sql = " Select * from producao.aliquota_me"
        Set ss = oradatabase.CreateDynaset(sql, 0&)
    
        oradatabase.dbExecuteSQL " Begin producao.pr_limpa_aliq;end;"
        
        MostraLog ("Prepara��o Conclu�da! Iniciando Grava��o")
    End If
End Sub

Public Sub leitura(fl_cont)
       
       Dim App As Object ' Declare variable to hold the reference.
    
    Set App = CreateObject("excel.application")
    App.Visible = False
        'Abre o arquivo para leitura e chama o procedimento que armazenar� as informa��es na tabela
        
         MostraLog ("Arquivo sendo Configurado para Leitura...")
        
        If fl_cont = True Then
            cont = 0
        End If
          MostraLog ("Iniciando a Leitura do Arquivo.")
          
        App.Workbooks.OpenText FileName:="H:\oracle\sistemas\vb\32bits\fis070\arq_fisc\" & arq, _
            FieldInfo:=Array(Array(0, 2), Array(23, 1), Array(73, 1))
    
        App.Range("B2:B50000").Select
        App.Selection.Clear
        App.Application.DisplayAlerts = False
        a = "H:\oracle\sistemas\vb\32bits\fis070\arq_fisc\" & Day(Now) & Format(Month(Now), "00") & Mid(Time, 1, 2) & Mid(Time, 4, 2) & ".txt"
        App.ActiveWorkbook.Saveas FileName:="H:\oracle\sistemas\vb\32bits\fis070\arq_fisc\" & Day(Now) & Format(Month(Now), "00") & Mid(Time, 1, 2) & Mid(Time, 4, 2) & ".txt"
        App.ActiveWorkbook.Close
        App.quit
    
    MostraLog ("Inserindo Informa��es no Banco de Dados. Aguarde...")
    
    Open a For Input As #1
        While EOF(1) = False
            If cont = 0 Then
                cont = cont + 1
                Line Input #1, lin
            Else
                Line Input #1, lin
                cgc = Mid(lin, 10, 14)
                icm = Mid(lin, 26, 2)
                Call Ler
            End If
            cont = cont + 1
            frmarq.lbl4.Visible = True
            frmarq.lbl4.Caption = "Registro: " & cont
            frmarq.lbl4.Refresh
        Wend
        
        oradatabase.ExecuteSQL "Begin Commit; End;"
        
    Close #1
  
End Sub
Public Sub Ler()
 'Insere ou atualiza as informa��es na tabela aliquota_me
 
 cod_loja = 4
 PL1 = " Begin"
 PL1 = PL1 & " Update aliquota_me set"
 PL1 = PL1 & "        pc_icm = :pc_icm "
 PL1 = PL1 & "  where cod_loja = :cod_loja and "
 PL1 = PL1 & "        cgc      = :cgc;"
 PL1 = PL1 & "    if sql%notfound then"
 PL1 = PL1 & "       Insert into aliquota_me(cod_loja,cgc,pc_icm) "
 PL1 = PL1 & "       values(:cod_loja,:cgc,:pc_icm);"
 PL1 = PL1 & "    end if;"
 PL1 = PL1 & " Exception"
 PL1 = PL1 & " When others then "
 PL1 = PL1 & " Rollback;"
 PL1 = PL1 & "    :cod_errora:= sqlcode;"
 PL1 = PL1 & "    :txt_errora:= sqlerrm;"
 PL1 = PL1 & " end;"
 
 oradatabase.Parameters.Remove "cod_loja"
 oradatabase.Parameters.Remove "cgc"
 oradatabase.Parameters.Remove "pc_icm"
 oradatabase.Parameters.Remove "cod_errora"
 oradatabase.Parameters.Remove "txt_errora"
 oradatabase.Parameters.Add "cod_loja", cod_loja, 1
 oradatabase.Parameters.Add "cgc", cgc, 1
 oradatabase.Parameters.Add "pc_icm", icm, 1
 oradatabase.Parameters.Add "cod_errora", 0, 2
 oradatabase.Parameters.Add "txt_errora", "", 2
 
 oradatabase.ExecuteSQL PL1
 
 If Val(oradatabase.Parameters("cod_errora")) <> 0 Then
     flag = False
     erro = Val(oradatabase.Parameters("cod_errora"))
     final = Now
     Call crialog
     MsgBox oradatabase.Parameters("txt_errora") & ". Ligue para o Depto. de Sistemas", vbExclamation, "Erro"
Else
   flag = True
 End If
 
End Sub




Public Sub crialog()
   login = "H:\oracle\sistemas\vb\32bits\fis070\log_fisc\" & Format(Year(Now), "####") & Format(Month(Now), "00") & Format(Day(Now), "00") & ".log"
   
        
   'Acrescenta as informa��es dentro do arquivo
   Open login For Output As #1
        Print #1, " In�cio: " & inicial & _
                  " Fim : " & final & _
                  " Quantidade Processada: " & cont - 2 & _
                  " Erro: " & erro
  Close #1
End Sub

Public Sub incluir()
   Dim b As Integer
   Dim c As Integer
     b = 0
    
   
   sql = " insert into cliente_novo (cgc, nome_cliente, endereco,nome_cidade, "
   sql = sql & " cod_uf, bairro, cep, ddd, fone, fax, nome_contato1,nome_contato2, email, "
   sql = sql & " cod_responsavel, vl_potencial,dt_inclusao, situacao,dt_envio_gerente) values "
   sql = sql & " ( '" & Mid(lin, 1, 14) & "', '" & Mid(lin, 15, 30) & "', '" & Mid(lin, 45, 33) & "', '" & Mid(lin, 78, 20) & "', "
   sql = sql & "  '" & Mid(lin, 98, 2) & "', '" & Mid(lin, 100, 20) & "', " & Mid(lin, 120, 8) & ", " & Mid(lin, 128, 4) & ", "
   sql = sql & "  " & Mid(lin, 132, 9) & ", '" & Mid(lin, 141, 13) & "', '" & Mid(lin, 154, 15) & "', '" & Mid(lin, 169, 15) & "', "
   sql = sql & "  '" & Mid(lin, 184, 50) & "', " & Mid(lin, 235, 4) & ", " & Trim(Mid(lin, 239, 15)) & ", '" & a & "'," & b & ",null) ; "
   
         
         
     
     db.Execute sql
     FreeLocks
     
     
     
End Sub








Public Sub log()
 login = calog & "cad\log\" & Format(Day(Now), "00") & Format(Month(Now), "00") & Format(Year(Now), "0000") & ".log"
  
 Open login For Append As #2
    Print #2, "Registro: " & Mid(lin, 1, 14) & Mid(lin, 15, 29) & _
              "  Mensagem: Cliente j� Cadastrado na Base de Dados DPK"
    Close #2
End Sub

Public Sub renomear()
 Name arquivo As calog & "cad\" & Format(Hour(Now), "00") & Format(Minute(Now), "00") & Format(Day(Now), "00") & Format(Month(Now), "00") & ".txt"
 
End Sub



Public Sub verifica_formulario()
    fl = 0
    If frmsci.txtcodigo.Text = "" Then
        MsgBox "Preencha o Campo com o C�digo do Cliente", vbExclamation, "Aten��o"
        frmsci.txtcodigo.SetFocus
        fl = 1
    ElseIf frmsci.cbonome.Text = "" Then
        MsgBox "Preencha o Campo com o Nome do Cliente", vbExclamation, "Aten��o"
        frmsci.cbonome.SetFocus
        fl = 1
    
    End If
    
    
End Sub
