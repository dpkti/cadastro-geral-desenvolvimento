VERSION 4.00
Begin VB.MDIForm mdicad 
   BackColor       =   &H8000000C&
   Caption         =   "Cadastro de Antecipa��o Tribut�ria"
   ClientHeight    =   5940
   ClientLeft      =   1860
   ClientTop       =   1770
   ClientWidth     =   6690
   Height          =   6630
   Icon            =   "mdicad.frx":0000
   Left            =   1800
   LinkTopic       =   "MDIForm1"
   Top             =   1140
   Width           =   6810
   WindowState     =   2  'Maximized
   Begin Threed.SSPanel SSPanel1 
      Align           =   1  'Align Top
      Height          =   615
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6690
      _Version        =   65536
      _ExtentX        =   11800
      _ExtentY        =   1085
      _StockProps     =   15
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin Threed.SSCommand cmdsair 
         Height          =   495
         Left            =   1650
         TabIndex        =   4
         Top             =   75
         Width           =   495
         _Version        =   65536
         _ExtentX        =   873
         _ExtentY        =   873
         _StockProps     =   78
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "mdicad.frx":0442
      End
      Begin Threed.SSCommand cmdconsulta 
         Height          =   495
         Left            =   900
         TabIndex        =   3
         Top             =   75
         Width           =   495
         _Version        =   65536
         _ExtentX        =   873
         _ExtentY        =   873
         _StockProps     =   78
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "mdicad.frx":075C
      End
      Begin Threed.SSCommand cmdinclusao 
         Height          =   495
         Left            =   150
         TabIndex        =   2
         Top             =   70
         Width           =   495
         _Version        =   65536
         _ExtentX        =   873
         _ExtentY        =   873
         _StockProps     =   78
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "mdicad.frx":0BAE
      End
   End
   Begin Threed.SSPanel SSPanel2 
      Align           =   2  'Align Bottom
      Height          =   615
      Left            =   0
      TabIndex        =   1
      Top             =   5325
      Width           =   6690
      _Version        =   65536
      _ExtentX        =   11800
      _ExtentY        =   1085
      _StockProps     =   15
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin VB.Label lblmsg 
         AutoSize        =   -1  'True
         Caption         =   "msg"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   2700
         TabIndex        =   5
         Top             =   150
         Visible         =   0   'False
         Width           =   465
      End
   End
   Begin VB.Menu mnuinclusao 
      Caption         =   "&Inclus�o"
   End
   Begin VB.Menu mnuconsulta 
      Caption         =   "&Consulta"
   End
   Begin VB.Menu mnusair 
      Caption         =   "&Sair"
   End
   Begin VB.Menu mnusobre 
      Caption         =   "Sobr&e"
   End
End
Attribute VB_Name = "mdicad"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Private Sub cmdconsulta_Click()
lblmsg.Visible = False

frmconsulta.Show vbModal

End Sub

Private Sub cmdconsulta_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
lblmsg.Visible = True
lblmsg.Left = 4650
lblmsg.Caption = "Formul�rio de Consulta"

End Sub


Private Sub cmdinclusao_Click()
FG = 1

lblmsg.Visible = False

frminclusao.Show vbModal

End Sub

Private Sub cmdinclusao_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
lblmsg.Visible = True
lblmsg.Left = 4650
lblmsg.Caption = "Formul�rio para Cadastro"

End Sub


Private Sub cmdsair_Click()
Dim vbresp As Integer
lblmsg.Visible = False

 vbresp = MsgBox("Confirma a Sa�da?", vbQuestion + vbYesNo, "Sair")
   If vbresp = vbYes Then
     End
   End If
   
End Sub

Private Sub cmdsair_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
lblmsg.Visible = True
lblmsg.Left = 4650
lblmsg.Caption = "Sair do Programa"

End Sub


Private Sub MDIForm_Load()
On Error GoTo trata_erro

If PrevInstance Then
 MsgBox "J� EXISTE UMA INST�NCIA DO PROGRAMA NO AR", vbExclamation, "Aten��o"
End If

 Set orasession = CreateObject("oracleinprocserver.xorasession")
 'Set oradatabase = orasession.OpenDatabase("desenv", "producao/des", 0&)
 Set oradatabase = orasession.OpenDatabase("producao", "CAD640/PROD", 0&)
 Exit Sub
 
trata_erro:
  Call Process_Line_Errors(sql)
  
 

End Sub

Private Sub MDIForm_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
lblmsg.Visible = False

End Sub


Private Sub mnuconsulta_Click()
Call cmdconsulta_Click

End Sub

Private Sub mnuinclusao_Click()
Call cmdinclusao_Click

End Sub

Private Sub mnusair_Click()
Call cmdsair_Click

End Sub

Private Sub mnusobre_Click()
frmsobre.Show vbModal

End Sub

Private Sub SSPanel1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
lblmsg.Visible = False

End Sub


