VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmConsultas 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Consultas"
   ClientHeight    =   8775
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   13725
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "frmConsultas.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8775
   ScaleWidth      =   13725
   Tag             =   "frmConsultas"
   Begin VB.ComboBox cboPromotor 
      Height          =   315
      Left            =   30
      TabIndex        =   21
      Text            =   "Combo1"
      Top             =   1140
      Width           =   3765
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   0
      TabIndex        =   20
      Top             =   1500
      Width           =   13695
      _ExtentX        =   24156
      _ExtentY        =   53
      BackColor       =   -2147483633
   End
   Begin VB.CheckBox chkPorPeriodo 
      Appearance      =   0  'Flat
      Caption         =   "Por Per�odo"
      ForeColor       =   &H80000008&
      Height          =   195
      Left            =   3960
      TabIndex        =   19
      Top             =   630
      Width           =   1185
   End
   Begin VB.CheckBox chkPorCliente 
      Appearance      =   0  'Flat
      Caption         =   "Por Cliente"
      ForeColor       =   &H80000008&
      Height          =   195
      Left            =   6660
      TabIndex        =   18
      Top             =   630
      Width           =   1125
   End
   Begin VB.CheckBox chkPorRoteiro 
      Appearance      =   0  'Flat
      Caption         =   "Por Roteiro"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   195
      Left            =   12450
      TabIndex        =   17
      Top             =   630
      Width           =   1125
   End
   Begin VB.Frame fraPeriodo 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      ForeColor       =   &H80000008&
      Height          =   825
      Left            =   3900
      TabIndex        =   11
      Top             =   630
      Width           =   2535
      Begin MSComCtl2.DTPicker dtDe 
         Height          =   345
         Left            =   90
         TabIndex        =   12
         Top             =   420
         Width           =   1155
         _ExtentX        =   2037
         _ExtentY        =   609
         _Version        =   393216
         Format          =   50200577
         CurrentDate     =   39624
      End
      Begin MSComCtl2.DTPicker DtAte 
         Height          =   315
         Left            =   1290
         TabIndex        =   13
         Top             =   420
         Width           =   1155
         _ExtentX        =   2037
         _ExtentY        =   556
         _Version        =   393216
         Format          =   50200577
         CurrentDate     =   39624
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "De:"
         Height          =   195
         Left            =   90
         TabIndex        =   15
         Top             =   210
         Width           =   255
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "At�:"
         Height          =   195
         Left            =   1290
         TabIndex        =   14
         Top             =   210
         Width           =   285
      End
   End
   Begin VB.Frame fraClientes 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      ForeColor       =   &H80000008&
      Height          =   825
      Left            =   6600
      TabIndex        =   7
      Top             =   630
      Width           =   5685
      Begin VB.ComboBox cboCliente 
         Height          =   315
         Left            =   60
         Style           =   2  'Dropdown List
         TabIndex        =   8
         Top             =   450
         Width           =   5175
      End
      Begin Bot�o.cmd cmdPesqCliente 
         Height          =   285
         Left            =   5310
         TabIndex        =   9
         ToolTipText     =   "Consultar Cliente"
         Top             =   480
         Width           =   285
         _ExtentX        =   503
         _ExtentY        =   503
         BTYPE           =   3
         TX              =   "..."
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmConsultas.frx":038A
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Clientes:"
         Height          =   195
         Left            =   60
         TabIndex        =   10
         Top             =   240
         Width           =   600
      End
   End
   Begin VB.Frame fraPorRoteiro 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      ForeColor       =   &H80000008&
      Height          =   825
      Left            =   12390
      TabIndex        =   3
      Top             =   630
      Width           =   1305
      Begin VB.OptionButton optComRoteiro 
         Caption         =   "Com Roteiro"
         Height          =   225
         Left            =   60
         TabIndex        =   5
         Top             =   240
         Value           =   -1  'True
         Width           =   1185
      End
      Begin VB.OptionButton optSemRoteiro 
         Caption         =   "Sem Roteiro"
         Height          =   225
         Left            =   60
         TabIndex        =   4
         Top             =   480
         Width           =   1185
      End
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   2
      Top             =   8460
      Width           =   13725
      _ExtentX        =   24209
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   21590
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Text            =   "Qtde de Registros:"
            TextSave        =   "Qtde de Registros:"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView lsv 
      Height          =   6405
      Left            =   30
      TabIndex        =   1
      Top             =   2040
      Width           =   13665
      _ExtentX        =   24104
      _ExtentY        =   11298
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   0
   End
   Begin Bot�o.cmd cmdPesquisar 
      Height          =   525
      Left            =   13110
      TabIndex        =   6
      ToolTipText     =   "Pesquisar"
      Top             =   30
      Width           =   585
      _ExtentX        =   1032
      _ExtentY        =   926
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsultas.frx":03A6
      PICN            =   "frmConsultas.frx":03C2
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdImpressao 
      Height          =   525
      Left            =   12450
      TabIndex        =   16
      ToolTipText     =   "Visualizar Impress�o"
      Top             =   30
      Width           =   585
      _ExtentX        =   1032
      _ExtentY        =   926
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsultas.frx":109C
      PICN            =   "frmConsultas.frx":10B8
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin pkGradientControl.pkGradient pkGradient2 
      Height          =   30
      Left            =   0
      TabIndex        =   23
      Top             =   570
      Width           =   13695
      _ExtentX        =   24156
      _ExtentY        =   53
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   495
      Left            =   30
      TabIndex        =   24
      ToolTipText     =   "Voltar"
      Top             =   30
      Width           =   555
      _ExtentX        =   979
      _ExtentY        =   873
      BTYPE           =   9
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsultas.frx":1D92
      PICN            =   "frmConsultas.frx":1DAE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label17 
      AutoSize        =   -1  'True
      Caption         =   "Promotores"
      Height          =   195
      Left            =   30
      TabIndex        =   22
      Top             =   930
      Width           =   825
   End
   Begin VB.Label lblNomeConsulta 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Nome da Consulta"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   435
      Left            =   30
      TabIndex        =   0
      Top             =   1620
      Width           =   13665
   End
End
Attribute VB_Name = "frmConsultas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Check1_Click()

End Sub

Private Sub chkPorCliente_Click()
    fraClientes.Enabled = chkPorCliente.Value
    NomeConsulta
    PreencherCboCliente
End Sub

Private Sub chkPorPeriodo_Click()
    fraPeriodo.Enabled = chkPorPeriodo.Value
    NomeConsulta
End Sub

Private Sub chkPorRoteiro_Click()
    fraPorRoteiro.Enabled = chkPorRoteiro.Value
    NomeConsulta
End Sub

Private Sub cmdPesqCliente_Click()
    frmConsultaCliente.vOrigem = "frmConsultas"
    frmConsultaCliente.Show 1
End Sub

Private Sub cmdPesquisar_Click()
    Dim vObj As Object
    
    If Val(cboPromotor) = 0 Then
        MsgBox "Selecione um promotor", vbInformation, "Aten��o"
        Exit Sub
    End If
    
    Conectar
    
    vBanco.Parameters.Remove "PM_PROMOTOR"
    vBanco.Parameters.Remove "PM_DTINI"
    vBanco.Parameters.Remove "PM_DTFIM"
    vBanco.Parameters.Remove "PM_CLIENTE"
    vBanco.Parameters.Remove "PM_STATUS"
    
    vBanco.Parameters.Add "PM_PROMOTOR", Val(Me.cboPromotor), 1
    
    If chkPorPeriodo.Value = 1 Then
        
        vBanco.Parameters.Add "PM_DTINI", CDate(Formata_Data(dtDe.Value)), 1
        vBanco.Parameters.Add "PM_DTFIM", CDate(Formata_Data(DtAte.Value)), 1
        
    Else
    
        vBanco.Parameters.Add "PM_DTINI", Null, 1
        vBanco.Parameters.Add "PM_DTFIM", Null, 1
        
    End If
    
    vBanco.Parameters.Add "PM_CLIENTE", Val(cboCliente), 1
    
        
    If chkPorRoteiro.Value = 1 And Me.optSemRoteiro.Value = True Then
        
        Criar_Cursor
        
        vBanco.ExecuteSQL "Begin PRODUCAO.PCK_CAD035.PR_CON_CLIENTES_SEM_ROTEIRO(:vCursor, :PM_PROMOTOR, :PM_DTINI, :PM_DTFIM, :PM_CLIENTE);END;"
        Set vObj = vBanco.Parameters("vCursor").Value
        
        Preencher_Lsv "Clientes_Sem_Roteiro", vObj
        
        Set vObj = Nothing
    
    End If
    
    vBanco.Parameters.Remove "PM_PROMOTOR"
    vBanco.Parameters.Remove "PM_DTINI"
    vBanco.Parameters.Remove "PM_DTFIM"
    vBanco.Parameters.Remove "PM_CLIENTE"
    vBanco.Parameters.Remove "PM_STATUS"
        
    Desconectar
    
End Sub

Private Sub cmdVoltar_Click()
    Unload Me
End Sub

Private Sub Form_Activate()
    Me.Top = (mdiRoteiro.Height - Me.Height) / 2 - mdiRoteiro.mnuXP.Height
    Me.Left = (mdiRoteiro.Width - Me.Width) / 2
    frmCadastro.Preencher_Cbo_Promotor frmConsultas
End Sub

Sub NomeConsulta()
    Dim vTexto As String
    
    'Periodo
    If chkPorPeriodo.Value = 1 And chkPorCliente.Value = 0 And chkPorRoteiro.Value = 0 Then
        lblNomeConsulta.Caption = "Consulta Cliente Por Periodo"
        Exit Sub
    End If
    
    'Periodo e Roteiro
    If chkPorPeriodo.Value = 1 And chkPorCliente.Value = 0 And chkPorRoteiro.Value = 1 Then
        lblNomeConsulta.Caption = "Consulta Cliente Por Periodo " & IIf(Me.optComRoteiro.Value = True, " Com Roteiro", " Sem Roteiro")
        Exit Sub
    End If
    
    
    'Periodo e Cliente
    If chkPorPeriodo.Value = 1 And chkPorCliente.Value = 1 And chkPorRoteiro.Value = 0 Then
        lblNomeConsulta.Caption = "Consulta Clientes Espec�ficos Por Periodo"
        Exit Sub
    End If
    
    'Periodo, Cliente e Status
    If chkPorPeriodo.Value = 1 And chkPorCliente.Value = 1 And Me.chkPorRoteiro.Value = 0 Then
        lblNomeConsulta.Caption = "Consulta Clientes Espec�ficos no Per�odo"
        Exit Sub
    End If
    
    'Periodo, Cliente, Status, Roteiro
    If chkPorPeriodo.Value = 1 And chkPorCliente.Value = 1 And chkPorRoteiro.Value = 1 Then
        lblNomeConsulta.Caption = "Consulta Clientes Espec�ficos " & IIf(Me.optComRoteiro.Value = True, " com Roteiro", " sem Roteiro") & " no Per�odo"
        Exit Sub
    End If


'2
    'Periodo e Cliente
    If chkPorPeriodo.Value = 0 And chkPorCliente.Value = 1 And chkPorRoteiro.Value = 0 Then
        lblNomeConsulta.Caption = "Consulta Clientes Espec�ficos"
        Exit Sub
    End If
    
    'Periodo, Cliente e Status
    If chkPorPeriodo.Value = 0 And chkPorCliente.Value = 1 And Me.chkPorRoteiro.Value = 0 Then
        lblNomeConsulta.Caption = "Consulta Clientes " & " Espec�ficos "
        Exit Sub
    End If
    
    'Periodo, Cliente, Status, Roteiro
    If chkPorPeriodo.Value = 0 And chkPorCliente.Value = 1 And chkPorRoteiro.Value = 1 Then
        lblNomeConsulta.Caption = "Consulta Clientes Espec�ficos " & IIf(Me.optComRoteiro.Value = True, " com Roteiro", " sem Roteiro")
        Exit Sub
    End If

'3
    'Status
    If chkPorPeriodo.Value = 0 And chkPorCliente.Value = 0 And chkPorRoteiro.Value = 0 Then
        lblNomeConsulta.Caption = "Consulta Clientes "
        Exit Sub
    End If
    
    'Status, Roteiro
    If chkPorPeriodo.Value = 0 And chkPorCliente.Value = 0 And chkPorRoteiro.Value = 1 Then
        lblNomeConsulta.Caption = "Consulta Clientes " & IIf(Me.optComRoteiro.Value = True, " com Roteiro", " sem Roteiro")
        Exit Sub
    End If

'4
    'Roteiro
    If chkPorPeriodo.Value = 0 And chkPorCliente.Value = 0 And chkPorRoteiro.Value = 1 Then
        lblNomeConsulta.Caption = "Consulta Clientes " & IIf(Me.optComRoteiro.Value = True, "com Roteiro", "sem Roteiro")
        Exit Sub
    End If


End Sub

Private Sub optAtivo_Click()
    NomeConsulta
End Sub

Private Sub optComRoteiro_Click()
    NomeConsulta
End Sub

Private Sub optInativo_Click()
    NomeConsulta
End Sub

Private Sub optSemRoteiro_Click()
    NomeConsulta
End Sub

Sub PreencherCboCliente()
    Dim vObj As Object
    Dim i As Integer
    
    Conectar
    
    Criar_Cursor
    
    vBanco.Parameters.Remove "PM_PROMOTOR"
    vBanco.Parameters.Add "PM_PROMOTOR", Val(Me.cboPromotor), 1
    
    vBanco.ExecuteSQL "Begin PRODUCAO.PCK_CAD035.PR_SEL_CLIENTES_PROMOTOR(:vCursor, :PM_PROMOTOR);END;"
    Set vObj = vBanco.Parameters("vCursor").Value
    
    For i = 1 To vObj.RecordCount
        
        cboCliente.AddItem Format(vObj!Cod_Cliente, "000000") & " - " & vObj!nome_cliente
        vObj.MoveNext
        
    Next

    Set vObj = Nothing
    
    vBanco.Parameters.Remove "PM_PROMOTOR"
    
    Desconectar
End Sub

Sub Preencher_Lsv(pQual As String, pObj As Object)
    
    Dim lItem As ListItem
    Dim vCol As ColumnHeader
    Dim i As Integer
    
    lsv.ListItems.Clear
    
    If pQual = "Clientes_Sem_Roteiro" Then
        
        Set vCol = lsv.ColumnHeaders.Add()
        vCol.Text = "C�digo"
        
        Set vCol = lsv.ColumnHeaders.Add()
        vCol.Text = "Nome Cliente"
        
        For i = 1 To pObj.RecordCount
            Set lItem = Me.lsv.ListItems.Add
            lItem = pObj!Cod_Cliente
            lItem.SubItems(1) = pObj!nome_cliente
            pObj.MoveNext
        Next
    
    End If

    Set pObj = Nothing

    Me.StatusBar1.Panels(2).Text = "Registros: " & Me.lsv.ListItems.Count

End Sub
