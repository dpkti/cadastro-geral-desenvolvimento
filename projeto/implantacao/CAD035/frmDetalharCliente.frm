VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmDetalharCliente 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Form1"
   ClientHeight    =   4995
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6030
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4995
   ScaleWidth      =   6030
   Begin MSComctlLib.StatusBar Stb 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   2
      Top             =   4680
      Width           =   6030
      _ExtentX        =   10636
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   10583
         EndProperty
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   0
      TabIndex        =   1
      Top             =   540
      Width           =   6015
      _ExtentX        =   10610
      _ExtentY        =   53
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   495
      Left            =   30
      TabIndex        =   3
      ToolTipText     =   "Voltar"
      Top             =   30
      Width           =   555
      _ExtentX        =   979
      _ExtentY        =   873
      BTYPE           =   9
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmDetalharCliente.frx":0000
      PICN            =   "frmDetalharCliente.frx":001C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdExportarArqTexto 
      Height          =   495
      Left            =   4800
      TabIndex        =   4
      ToolTipText     =   "Exportar para Arquivo Texto"
      Top             =   30
      Width           =   555
      _ExtentX        =   979
      _ExtentY        =   873
      BTYPE           =   9
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmDetalharCliente.frx":0CF6
      PICN            =   "frmDetalharCliente.frx":0D12
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdExportarExcel 
      Height          =   495
      Left            =   5430
      TabIndex        =   5
      ToolTipText     =   "Exportar para Excel"
      Top             =   30
      Width           =   555
      _ExtentX        =   979
      _ExtentY        =   873
      BTYPE           =   9
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmDetalharCliente.frx":19EC
      PICN            =   "frmDetalharCliente.frx":1A08
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSComctlLib.ListView lsv 
      Height          =   3885
      Left            =   30
      TabIndex        =   6
      Top             =   780
      Width           =   5985
      _ExtentX        =   10557
      _ExtentY        =   6853
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      MousePointer    =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   15
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Width           =   476
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   1
         Text            =   "C�digo"
         Object.Width           =   1411
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Nome do Cliente"
         Object.Width           =   4410
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   2
         SubItemIndex    =   3
         Text            =   "N� Visitas"
         Object.Width           =   1499
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   4
         Text            =   "Carteira"
         Object.Width           =   1587
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Text            =   "Cidade"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Text            =   "Bairro"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   7
         Text            =   "UF"
         Object.Width           =   706
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   8
         Text            =   "Endereco"
         Object.Width           =   5292
      EndProperty
      BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   9
         Text            =   "CEP"
         Object.Width           =   1587
      EndProperty
      BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   10
         Text            =   "Quadrante"
         Object.Width           =   1587
      EndProperty
      BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   11
         Text            =   "Tipo de Cliente"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   12
         Text            =   "Segmento"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   13
         Text            =   "Dt Ult Compra"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(15) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   14
         Text            =   "Dt Ult Compra Indent"
         Object.Width           =   2540
      EndProperty
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Label1"
      Height          =   195
      Left            =   30
      TabIndex        =   0
      Top             =   570
      Width           =   465
   End
End
Attribute VB_Name = "frmDetalharCliente"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmd1_Click()
    
End Sub

Private Sub cmdExportarArqTexto_Click()
    Dim vNomeArquivo As String
    
    vNomeArquivo = "C:\Roteiro_" & Format(Now, "HHMMSS") & ".TXT"
   
   Gerar_Arq_Texto vNomeArquivo, ";", Me.Caption
   
   MsgBox "Arquivo " & vNomeArquivo & " gerado com sucesso !", vbInformation, "Aten��o"
   
End Sub

Private Sub cmdExportarExcel_Click()
    
    Dim vNomeArquivo As String
    
    vNomeArquivo = "C:\Roteiro_" & Format(Now, "HHMMSS") & ".TXT"
    
    Gerar_Arq_Texto vNomeArquivo, Chr(39), Me.Caption
    Gerar_Excel vNomeArquivo, Chr(39)
    
End Sub

Sub Gerar_Excel(pNomeArquivo As String, pSeparador As String)

    Dim vExcel As Object

    Set vExcel = CreateObject("EXCEL.APPLICATION")
    vExcel.Visible = True
    vExcel.Workbooks.OpenText pNomeArquivo, , , xlDelimited, , , , , , , True, pSeparador
    
End Sub

Sub Gerar_Arq_Texto(pNomeArquivo As String, pSeparador As String, pCaption As String)

    Dim vArq As Integer
    Dim vTexto As String
    Dim i As Integer
    Dim ii As Integer
    
    vArq = FreeFile
    
    Open pNomeArquivo For Output As #vArq
    
    For i = 1 To Me.lsv.ColumnHeaders.Count
        If vTexto = "" Then
            vTexto = lsv.ColumnHeaders(i).Text
        Else
            vTexto = vTexto & Chr(9) & lsv.ColumnHeaders(i).Text
        End If
    Next
    
    Print #vArq, pCaption
    
    Print #vArq, vTexto
    
    vTexto = ""
    
    For i = 1 To lsv.ListItems.Count
            
        For ii = 1 To lsv.ColumnHeaders.Count - 1
            If vTexto = "" Then
                vTexto = lsv.ListItems(i).ListSubItems(ii).Text
            Else
                vTexto = vTexto & pSeparador & lsv.ListItems(i).ListSubItems(ii).Text
            End If
        Next
        Print #vArq, vTexto
        vTexto = ""
    Next
    Close #vArq
    
End Sub

Private Sub cmdVoltar_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Me.Top = (Screen.Height - Me.Height) / 2
    Me.Left = (Screen.Width - Me.Width) / 2
    StayOnTop Me
End Sub

Private Sub lsv_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    If ColumnHeader.Text = "C�digo" Or ColumnHeader.Text = "N� Visitas" Then
        SortLvwOnLong Me.lsv, ColumnHeader.Index
    ElseIf InStr(1, ColumnHeader.Text, "Dt") > 0 Then
        SortLvwOnDate Me.lsv, ColumnHeader.Index
    Else
        lsv.SortKey = ColumnHeader.Index - 1
        If lsv.SortOrder = lvwAscending Then
            lsv.SortOrder = lvwDescending
        Else
            lsv.SortOrder = lvwAscending
        End If
        lsv.Sorted = True
    End If
End Sub

Private Sub lsv_DblClick()
    Dim i As Integer
    
    If Val(lsv.SelectedItem.SubItems(1)) > 0 Then
        For i = 1 To frmCadastro.lsv.ListItems.Count
            If frmCadastro.lsv.ListItems(i).SubItems(1) = Me.lsv.SelectedItem.SubItems(1) Then
                frmCadastro.lsv.SetFocus
                frmCadastro.lsv.ListItems(i).Selected = True
                frmCadastro.lsv.ListItems(i).EnsureVisible
                Exit For
            End If
        Next
    End If
    Unload Me
End Sub
