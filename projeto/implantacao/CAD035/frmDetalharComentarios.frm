VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmDetalharComentarios 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Form1"
   ClientHeight    =   4755
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8295
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4755
   ScaleWidth      =   8295
   Begin MSComctlLib.StatusBar Stb 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   0
      Top             =   4440
      Width           =   8295
      _ExtentX        =   14631
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   14579
         EndProperty
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   0
      TabIndex        =   1
      Top             =   540
      Width           =   8325
      _ExtentX        =   14684
      _ExtentY        =   53
      BackColor       =   -2147483633
   End
   Begin MSComctlLib.ListView lsv 
      Height          =   3585
      Left            =   30
      TabIndex        =   2
      Top             =   810
      Width           =   8205
      _ExtentX        =   14473
      _ExtentY        =   6324
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   4
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "C�digo"
         Object.Width           =   1411
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Nome Cliente"
         Object.Width           =   4939
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   2
         Text            =   "Data"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Coment�rio"
         Object.Width           =   5292
      EndProperty
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   495
      Left            =   30
      TabIndex        =   4
      ToolTipText     =   "Voltar"
      Top             =   30
      Width           =   555
      _ExtentX        =   979
      _ExtentY        =   873
      BTYPE           =   9
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmDetalharComentarios.frx":0000
      PICN            =   "frmDetalharComentarios.frx":001C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdExportarArqTexto 
      Height          =   495
      Left            =   7050
      TabIndex        =   5
      ToolTipText     =   "Exportar para Arquivo Texto"
      Top             =   30
      Width           =   555
      _ExtentX        =   979
      _ExtentY        =   873
      BTYPE           =   9
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmDetalharComentarios.frx":0CF6
      PICN            =   "frmDetalharComentarios.frx":0D12
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdExportarExcel 
      Height          =   495
      Left            =   7680
      TabIndex        =   6
      ToolTipText     =   "Exportar para Excel"
      Top             =   30
      Width           =   555
      _ExtentX        =   979
      _ExtentY        =   873
      BTYPE           =   9
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmDetalharComentarios.frx":19EC
      PICN            =   "frmDetalharComentarios.frx":1A08
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Label1"
      Height          =   195
      Left            =   30
      TabIndex        =   3
      Top             =   570
      Width           =   465
   End
End
Attribute VB_Name = "frmDetalharComentarios"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdExportarArqTexto_Click()
    Dim vNomeArquivo As String
    
    vNomeArquivo = "C:\Roteiro_" & Format(Now, "HHMMSS") & ".TXT"
   
   Gerar_Arq_Texto vNomeArquivo, ";"
   
   MsgBox "Arquivo " & vNomeArquivo & " gerado com sucesso !", vbInformation, "Aten��o"
   
End Sub

Private Sub cmdExportarExcel_Click()
    Dim vNomeArquivo As String
    
    vNomeArquivo = "C:\Roteiro_" & Format(Now, "HHMMSS") & ".TXT"
    
    Gerar_Arq_Texto vNomeArquivo, Chr(39)
    Gerar_Excel vNomeArquivo, Chr(39)
End Sub

Private Sub cmdVoltar_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Me.Top = (Screen.Height - Me.Height) / 2
    Me.Left = (Screen.Width - Me.Width) / 2
End Sub

Private Sub lsv_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    If ColumnHeader.Text = "C�digo" Then
        SortLvwOnLong Me.lsv, ColumnHeader.Index
    ElseIf InStr(1, ColumnHeader.Text, "Data") > 0 Then
        SortLvwOnDate Me.lsv, ColumnHeader.Index
    Else
        lsv.SortKey = ColumnHeader.Index - 1
        If lsv.SortOrder = lvwAscending Then
            lsv.SortOrder = lvwDescending
        Else
            lsv.SortOrder = lvwAscending
        End If
        lsv.Sorted = True
    End If
End Sub

Private Sub lsv_DblClick()
    If Me.lsv.SelectedItem = "" Then Exit Sub
    
    Me.Visible = False
    
    Dim i As Integer
    Dim vKey
    
    For i = 1 To frmCadastro.trv.Nodes.Count
    
        If frmCadastro.trv.Nodes(i).Image = "Cliente" Then
        
            vKey = Split(frmCadastro.trv.Nodes(i).Key, "_")
        
            If Val(vKey(1)) = Year(CDate(lsv.SelectedItem.SubItems(2))) And Val(vKey(2)) = Month(CDate(lsv.SelectedItem.SubItems(2))) And Val(vKey(4)) = Day(CDate(lsv.SelectedItem.SubItems(2))) And vKey(5) = "C" & lsv.SelectedItem Then
                frmCadastro.trv.SetFocus
                frmCadastro.trv.Nodes(i).Selected = True
                frmCadastro.trv.Nodes(i).Expanded = True
                frmCadastro.trv.Nodes(i).EnsureVisible
                frmCadastro.Selecionar_Comentario
                Exit For
            End If
        End If
    Next
    
    frmCadastro.fraVisitas.Refresh
    frmCadastro.lsv.Refresh
    frmCadastro.trv.Refresh
    Unload Me
End Sub
Sub Gerar_Excel(pNomeArquivo As String, pSeparador As String)

    Dim vExcel As Object

    Set vExcel = CreateObject("EXCEL.APPLICATION")
    vExcel.Visible = True
    vExcel.Workbooks.OpenText pNomeArquivo, , , xlDelimited, , , , , , , True, pSeparador
    
End Sub

Sub Gerar_Arq_Texto(pNomeArquivo As String, pSeparador As String)

    Dim vArq As Integer
    Dim vTexto As String
    Dim i As Integer
    Dim ii As Integer
    
    vArq = FreeFile
    
    Open pNomeArquivo For Output As #vArq
    
    For i = 1 To Me.lsv.ColumnHeaders.Count
        If vTexto = "" Then
            vTexto = lsv.ColumnHeaders(i).Text
        Else
            vTexto = vTexto & pSeparador & lsv.ColumnHeaders(i).Text
        End If
    Next
    
    Print #vArq, vTexto
    
    vTexto = ""
    
    For i = 1 To lsv.ListItems.Count
            
        For ii = 1 To lsv.ColumnHeaders.Count - 1
            If vTexto = "" Then
                vTexto = lsv.ListItems(i).ListSubItems(ii).Text
            Else
                vTexto = vTexto & pSeparador & lsv.ListItems(i).ListSubItems(ii).Text
            End If
        Next
        Print #vArq, vTexto
        vTexto = ""
    Next
    Close #vArq
    
End Sub
