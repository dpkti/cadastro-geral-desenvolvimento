Attribute VB_Name = "modPromotor"
Option Explicit

Global Const ORATYPE_CURSOR = 102
Global Const ORADYN_NO_BLANKSTRIP = &H2&
Global Const ORAPARM_INPUT = 1              'CONSTANTE DE BIND INPUT
Global Const ORAPARM_OUTPUT = 2             'CONSTANTE DE BIND OUTPUT
Global Const ORAPARM_BOTH = 3               'CONSTANTE DE BIND INPUT/OUTPUT
Global Const ORATYPE_NUMBER = 2

Public vBanco As Object
Public vSessao As Object
Public vConectado As Boolean
Public vLogin As String
Public vTipo As String

Public Const LOCALE_STIMEFORMAT = &H1003
Public Const LOCALE_SSHORTDATE = &H1F
Public Const LOCALE_STHOUSAND = &HF
Public Const LOCALE_SDECIMAL = &HE
Public Const COLORONCOLOR = 3
Public Const HALFTONE = 4
Public Const SW_SHOWMAXIMIZED = 3
Public Const SW_SHOWNORMAL = 1
Public Const NERR_BASE = 2100
Public Const NERR_InvalidComputer = (NERR_BASE + 251)
Public Const NERR_UseNotFound = (NERR_BASE + 150)
Public Const NERR_Success = 0
Public Const CP_ACP = 0
Public Const NORMAL_PRIORITY_CLASS = &H20&
Public Const INFINITE = -1&
Public Const VCONSTANTE = &H1
Public Const vCorDivergente As Long = &HC0C0FF
Public Const vCorNormal As Long = &H80000005
Public Const BIF_RETURNONLYFSDIRS = 1
Public Const MAX_PATH = 260

Declare Function GetLocaleInfo Lib "kernel32" Alias "GetLocaleInfoA" (ByVal Locale As Long, ByVal LCType As Long, ByVal lpLCData As String, ByVal cchData As Long) As Long
Declare Function SetLocaleInfo Lib "kernel32" Alias "SetLocaleInfoA" (ByVal Locale As Long, ByVal LCType As Long, ByVal lpLCData As String) As Boolean
Declare Function GetUserDefaultLCID% Lib "kernel32" ()

Public Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hWnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long
Public Declare Function MessageBox Lib "user32" Alias "MessageBoxA" (ByVal hWnd As Long, ByVal lpText As String, ByVal lpCaption As String, ByVal wType As Long) As Long

#If Win32 Then
    Public Const HWND_TOPMOST& = -1
#Else
    Public Const HWND_TOPMOST& = -1
#End If 'WIN32

#If Win32 Then
    Const SWP_NOMOVE& = &H2
    Const SWP_NOSIZE& = &H1
#Else
    Const SWP_NOMOVE& = &H2
    Const SWP_NOSIZE& = &H1
#End If 'WIN32

#If Win32 Then
    Declare Function SetWindowPos& Lib "user32" (ByVal hWnd As Long, ByVal hWndInsertAfter As Long, ByVal x As Long, ByVal y As Long, ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long)
#Else
    Declare Sub SetWindowPos Lib "user" (ByVal hWnd As Integer, ByVal hWndInsertAfter As Integer, ByVal x As Integer, ByVal y As Integer, ByVal cx As Integer, ByVal cy As Integer, ByVal wFlags As Integer)
#End If 'WIN32


Function StayOnTop(Form As Form)
    Dim lFlags As Long
    Dim lStay As Long

    lFlags = SWP_NOSIZE Or SWP_NOMOVE
    lStay = SetWindowPos(Form.hWnd, HWND_TOPMOST, 0, 0, 0, 0, lFlags)
End Function

Function fPegarIndice(pKeyImagem As String) As Byte
    Dim i As Integer
    
    For i = 1 To frmCadastro.img.ListImages.Count
        If UCase(frmCadastro.img.ListImages(i).Key) = UCase(pKeyImagem) Then
            fPegarIndice = i
            Exit For
        End If
    Next

End Function


Sub Conectar(Optional pForm As Form)
    
    If Not pForm Is Nothing Then
        pForm.Stb.Panels(1).Text = "Conectando... Aguarde..."
    End If
    
    Set vSessao = CreateObject("oracleinprocserver.xorasession")
    Set vBanco = vSessao.OpenDatabase("PRODUCAO", "CAD035/DPK035", 0&)
    'Set vBanco = vSessao.OpenDatabase("CDDPK", "CAD035/DPK035", 0&)
    'Set vBanco = vSessao.OpenDatabase("CD04", "CAD035/DPK035", 0&)
    'Set vBanco = vSessao.OpenDatabase("SDPKT", "CAD035/DPK035", 0&)
    'Set vBanco = vSessao.OpenDatabase("CD16", "CAD035/DPK035", 0&)
    
    If Not pForm Is Nothing Then
        pForm.Stb.Panels(1).Text = "Pronto"
    End If
    
End Sub

Sub Desconectar(Optional pForm As Form)

    If Not pForm Is Nothing Then
        pForm.Stb.Panels(1).Text = "Conectando... Aguarde..."
    End If

    Dim vIndice

    If vBanco Is Nothing Then Conectar

    For vIndice = vBanco.Parameters.Count To 0 Step -1

        vBanco.Parameters.Remove (vIndice)

    Next
    
    Set vSessao = Nothing
    Set vBanco = Nothing
    
    vConectado = False
    
    If Not pForm Is Nothing Then
        pForm.Stb.Panels(1).Text = "Pronto"
    End If

End Sub

Public Sub Criar_Cursor()
    vBanco.Parameters.Remove "vCursor"
    vBanco.Parameters.Add "vCursor", 0, 2
    vBanco.Parameters("vCursor").serverType = ORATYPE_CURSOR
    vBanco.Parameters("vCursor").DynasetOption = ORADYN_NO_BLANKSTRIP
    vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
End Sub


Function Formata_Data(pData As Date) As String

    Formata_Data = Day(pData)
    
    Formata_Data = Formata_Data & "-" & Choose(Month(pData), "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC") & "-" & Year(pData)

End Function

Function fValidaLogin(pLogin As String, pSenha As String) As Boolean

    Dim vObj As Object
    
    Conectar
    
    Criar_Cursor
    
    vBanco.Parameters.Remove "PM_LOGIN"
    vBanco.Parameters.Remove "PM_SENHA"
    
    vBanco.Parameters.Add "PM_LOGIN", Val(pLogin), 1
    vBanco.Parameters.Add "PM_SENHA", pSenha, 1
    
    vBanco.ExecuteSQL "Begin PRODUCAO.PCK_CAD035.PR_VALIDA_LOGIN(:vCursor, :PM_LOGIN, :PM_SENHA);END;"
    
    Set vObj = vBanco.Parameters("vCursor").Value
    
    If vObj.EOF = False Then
        
        fValidaLogin = True
        vLogin = vObj!Login
        vTipo = vObj!tp_usuario
        
    Else
    
        vLogin = ""
        vTipo = ""
        fValidaLogin = False
        
    End If

    Set vObj = Nothing
    vBanco.Parameters.Remove "PM_LOGIN"
    vBanco.Parameters.Remove "PM_SENHA"
    
    Desconectar
    
End Function

Public Sub Contar_Visitas()
    Dim i As Long
    Dim ii As Long
    Dim vClientesSemana As Long
    Dim vQtdNos As Long

    frmCadastro.stbCadastro.Panels(1).Text = "Contando clientes do dia... Aguarde..."

    vQtdNos = frmCadastro.trv.Nodes.Count

    For i = 1 To vQtdNos
        If frmCadastro.trv.Nodes(i).Image = "Dia" Then
            frmCadastro.trv.Nodes(i).Text = Left(frmCadastro.trv.Nodes(i).Text, 14) & " - " & frmCadastro.trv.Nodes(i).Children
        End If
    Next
    
    frmCadastro.stbCadastro.Panels(1).Text = "Pronto"
    
End Sub
Function fPegarDataAtual() As String

    Dim vObj As Object
    
    Conectar
    
    Criar_Cursor
    
    vBanco.ExecuteSQL "Begin PRODUCAO.PCK_CAD035.PR_SELECT_DT_FATURAMENTO(:vCursor);END;"
    Set vObj = vBanco.Parameters("vCursor").Value
    
    fPegarDataAtual = vObj!dt_faturamento

    Set vObj = Nothing
    
    Desconectar

End Function

' Este � um exemplo para o separador de Data
' MAS VOC� PODER� ALTERAR todas as outras Configura��es Regionais
' contantes no m�dulo BAS, como Formato da Data, Hora etc
Public Function Get_locale(pTipoParametro) As String
    Dim Symbol As String
    Dim iRet1 As Long
    Dim iRet2 As Long
    Dim lpLCDataVar As String
    Dim Pos As Integer
    Dim Locale As Long
    Locale = GetUserDefaultLCID()
    
    'LOCALE_SDATE � a constante para separador de data
    'para outro locale setting apenas troque a constante
    'Fun��o para alterar o separador atual de data
    iRet1 = GetLocaleInfo(Locale, pTipoParametro, lpLCDataVar, 0)
    Symbol = String$(iRet1, 0)
    iRet2 = GetLocaleInfo(Locale, pTipoParametro, Symbol, iRet1)
    Pos = InStr(Symbol, Chr$(0))
    
    If Pos > 0 Then
        Symbol = Left$(Symbol, Pos - 1)
    End If
    Get_locale = Symbol
End Function
