VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmAcoes 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Cadastro de A��es"
   ClientHeight    =   3195
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4680
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3195
   ScaleWidth      =   4680
   ShowInTaskbar   =   0   'False
   Tag             =   "frmAcoes"
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   0
      TabIndex        =   0
      Top             =   540
      Width           =   4665
      _ExtentX        =   8229
      _ExtentY        =   53
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   495
      Left            =   0
      TabIndex        =   1
      ToolTipText     =   "Voltar"
      Top             =   0
      Width           =   555
      _ExtentX        =   979
      _ExtentY        =   873
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmAcoes.frx":0000
      PICN            =   "frmAcoes.frx":001C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmAcoes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdVoltar_Click()
    Unload Me
End Sub

Private Sub Form_Activate()
    mdiRoteiro.Arrange vbCascade
    Me.Top = (mdiRoteiro.Height - Me.Height) / 2 - mdiRoteiro.mnuXP.Height
    Me.Left = (mdiRoteiro.Width - Me.Width) / 2
End Sub

Private Sub Form_Load()
    Me.Top = (mdiRoteiro.Height - Me.Height) / 2 - mdiRoteiro.mnuXP.Height
    Me.Left = (mdiRoteiro.Width - Me.Width) / 2
End Sub


