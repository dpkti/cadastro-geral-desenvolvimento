VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmDatasAgendadas 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Datas Agendadas"
   ClientHeight    =   3660
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   1740
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3660
   ScaleWidth      =   1740
   Tag             =   "frmDatasAgendadas"
   Begin VB.ListBox lstDatas 
      Appearance      =   0  'Flat
      Height          =   2760
      Left            =   30
      TabIndex        =   0
      Top             =   810
      Width           =   1665
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   0
      TabIndex        =   2
      Top             =   540
      Width           =   1725
      _ExtentX        =   3043
      _ExtentY        =   53
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVisualizarImpressao 
      Height          =   495
      Left            =   1140
      TabIndex        =   3
      ToolTipText     =   "Visualizar Tela de Impress�o"
      Top             =   0
      Width           =   555
      _ExtentX        =   979
      _ExtentY        =   873
      BTYPE           =   9
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmDatasAgendadas.frx":0000
      PICN            =   "frmDatasAgendadas.frx":001C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   495
      Left            =   30
      TabIndex        =   4
      ToolTipText     =   "Voltar"
      Top             =   0
      Width           =   555
      _ExtentX        =   979
      _ExtentY        =   873
      BTYPE           =   9
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmDatasAgendadas.frx":0CF6
      PICN            =   "frmDatasAgendadas.frx":0D12
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "M�s atual"
      Height          =   195
      Left            =   60
      TabIndex        =   1
      Top             =   600
      Width           =   690
   End
End
Attribute VB_Name = "frmDatasAgendadas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdVoltar_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Me.Top = (mdiRoteiro.Height - Me.Height) / 2 - mdiRoteiro.mnuXP.Height
    Me.Left = (mdiRoteiro.Width - Me.Width) / 2
    Preencher_Visitas
End Sub

Sub Preencher_Visitas()
    Dim vObj As Object
    Dim i As Integer
    
    Conectar
    
    Criar_Cursor
    
    vBanco.Parameters.Remove "PM_PROMOTOR"
    vBanco.Parameters.Add "PM_PROMOTOR", Val(frmCadastro.cboPromotor), 1
    
    vBanco.Parameters.Remove "PM_CLIENTE"
    vBanco.Parameters.Add "PM_CLIENTE", Val(frmCadastro.lsv.SelectedItem.SubItems(1)), 1
    
    vBanco.Parameters.Remove "PM_PERIODO"
    vBanco.Parameters.Add "PM_PERIODO", Format(frmCadastro.lblDataAtual, "YYYYMM"), 1
    
    vBanco.ExecuteSQL "Begin PRODUCAO.PCK_CAD035.PR_SEL_DIAS_CLIENTE(:vCursor, :PM_PROMOTOR, :PM_CLIENTE, :PM_PERIODO);END;"
    Set vObj = vBanco.Parameters("vCursor").Value
    
    lstDatas.Clear
    
    For i = 1 To vObj.RecordCount
    
        lstDatas.AddItem vObj!DT_VISITA
        vObj.MoveNext
        
    Next
    
    Set vObj = Nothing
    
    vBanco.Parameters.Remove "PM_PROMOTOR"
    vBanco.Parameters.Remove "PM_CLIENTE"
    vBanco.Parameters.Remove "PM_PERIODO"
    
    Desconectar
End Sub
