VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmCopiar 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Copiar Clientes do Per�odo"
   ClientHeight    =   7440
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4485
   ControlBox      =   0   'False
   Icon            =   "frmCopiar.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7440
   ScaleWidth      =   4485
   Tag             =   "frmCopiar"
   Begin VB.Frame fraCopia 
      Enabled         =   0   'False
      Height          =   4695
      Left            =   30
      TabIndex        =   11
      Top             =   2700
      Width           =   4395
      Begin VB.Frame fraDePara 
         Caption         =   "Confirme as datas"
         Height          =   3075
         Left            =   60
         TabIndex        =   17
         Top             =   120
         Width           =   4245
         Begin MSComctlLib.ListView lsvCopiar 
            Height          =   2745
            Left            =   90
            TabIndex        =   18
            Top             =   240
            Width           =   4065
            _ExtentX        =   7170
            _ExtentY        =   4842
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   -1  'True
            HideSelection   =   -1  'True
            FullRowSelect   =   -1  'True
            GridLines       =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   0
            NumItems        =   4
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "Data Origem"
               Object.Width           =   2293
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Text            =   "Dia"
               Object.Width           =   882
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Text            =   "Data Destino"
               Object.Width           =   2293
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Text            =   "Dia"
               Object.Width           =   882
            EndProperty
         End
      End
      Begin VB.Frame fraMudarData 
         Caption         =   "Mudar data"
         Height          =   825
         Left            =   360
         TabIndex        =   12
         Top             =   3180
         Width           =   3615
         Begin MSComCtl2.DTPicker dtOrigemNova 
            Height          =   315
            Left            =   90
            TabIndex        =   13
            Top             =   420
            Width           =   1185
            _ExtentX        =   2090
            _ExtentY        =   556
            _Version        =   393216
            Format          =   21037057
            CurrentDate     =   39625
         End
         Begin MSComCtl2.DTPicker dtDestinoNova 
            Height          =   315
            Left            =   1350
            TabIndex        =   14
            Top             =   420
            Width           =   1185
            _ExtentX        =   2090
            _ExtentY        =   556
            _Version        =   393216
            Format          =   21037057
            CurrentDate     =   39625
         End
         Begin VB.Label Label5 
            AutoSize        =   -1  'True
            Caption         =   "Dt Origem:"
            Height          =   195
            Left            =   90
            TabIndex        =   16
            Top             =   210
            Width           =   750
         End
         Begin VB.Label Label6 
            AutoSize        =   -1  'True
            Caption         =   "Dt Destino:"
            Height          =   195
            Left            =   1350
            TabIndex        =   15
            Top             =   210
            Width           =   795
         End
         Begin VB.Image cmdAtualizar 
            Height          =   300
            Left            =   2580
            Picture         =   "frmCopiar.frx":038A
            Top             =   420
            Width           =   300
         End
      End
      Begin MSComctlLib.ProgressBar pb 
         Height          =   165
         Left            =   360
         TabIndex        =   19
         Top             =   4440
         Width           =   3645
         _ExtentX        =   6429
         _ExtentY        =   291
         _Version        =   393216
         BorderStyle     =   1
         Appearance      =   0
         Scrolling       =   1
      End
      Begin Bot�o.cmd cmdCopiar 
         Height          =   345
         Left            =   510
         TabIndex        =   20
         ToolTipText     =   "Copiar os clientes de um per�odo para outro"
         Top             =   4050
         Width           =   3345
         _ExtentX        =   5900
         _ExtentY        =   609
         BTYPE           =   3
         TX              =   "Iniciar c�pia dos dados"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCopiar.frx":0442
         PICN            =   "frmCopiar.frx":045E
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
   End
   Begin VB.Frame Frame1 
      Height          =   1665
      Left            =   330
      TabIndex        =   1
      Top             =   630
      Width           =   3735
      Begin VB.Frame fraOrigem 
         Caption         =   "Per�odo de Origem:"
         Height          =   1365
         Left            =   60
         TabIndex        =   6
         Top             =   210
         Width           =   1695
         Begin MSComCtl2.DTPicker DtIniOrigem 
            Height          =   315
            Left            =   420
            TabIndex        =   7
            Top             =   270
            Width           =   1185
            _ExtentX        =   2090
            _ExtentY        =   556
            _Version        =   393216
            Format          =   21037057
            CurrentDate     =   39625
         End
         Begin MSComCtl2.DTPicker DtFimOrigem 
            Height          =   315
            Left            =   420
            TabIndex        =   8
            Top             =   600
            Width           =   1185
            _ExtentX        =   2090
            _ExtentY        =   556
            _Version        =   393216
            Format          =   21037057
            CurrentDate     =   39625
         End
         Begin VB.Label lblDiasUteis 
            AutoSize        =   -1  'True
            Height          =   195
            Left            =   870
            TabIndex        =   24
            Top             =   1050
            Width           =   45
         End
         Begin VB.Label Label8 
            AutoSize        =   -1  'True
            Caption         =   "Dias �teis:"
            Height          =   195
            Left            =   90
            TabIndex        =   23
            Top             =   1050
            Width           =   735
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "De:"
            Height          =   195
            Left            =   120
            TabIndex        =   10
            Top             =   390
            Width           =   255
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "At�:"
            Height          =   195
            Left            =   90
            TabIndex        =   9
            Top             =   690
            Width           =   285
         End
      End
      Begin VB.Frame fraDestino 
         Caption         =   "Per�odo de Destino:"
         Height          =   1365
         Left            =   1950
         TabIndex        =   2
         Top             =   210
         Width           =   1695
         Begin MSComCtl2.DTPicker DtIniDestino 
            Height          =   315
            Left            =   420
            TabIndex        =   3
            Top             =   270
            Width           =   1185
            _ExtentX        =   2090
            _ExtentY        =   556
            _Version        =   393216
            Format          =   21037057
            CurrentDate     =   39625
         End
         Begin VB.Label lblDiasUteisDestino 
            AutoSize        =   -1  'True
            Height          =   195
            Left            =   840
            TabIndex        =   26
            Top             =   1020
            Width           =   45
         End
         Begin VB.Label Label9 
            AutoSize        =   -1  'True
            Caption         =   "Dias �teis:"
            Height          =   195
            Left            =   60
            TabIndex        =   25
            Top             =   1020
            Width           =   735
         End
         Begin VB.Label lblDtFimDestino 
            AutoSize        =   -1  'True
            Height          =   195
            Left            =   420
            TabIndex        =   22
            Top             =   690
            Width           =   45
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            Caption         =   "At�:"
            Height          =   195
            Left            =   90
            TabIndex        =   5
            Top             =   690
            Width           =   285
         End
         Begin VB.Label Label4 
            AutoSize        =   -1  'True
            Caption         =   "De:"
            Height          =   195
            Left            =   120
            TabIndex        =   4
            Top             =   390
            Width           =   255
         End
      End
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   0
      TabIndex        =   0
      Top             =   540
      Width           =   4455
      _ExtentX        =   7858
      _ExtentY        =   53
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdMontarPeriodo 
      Height          =   345
      Left            =   510
      TabIndex        =   21
      ToolTipText     =   "Copiar os clientes de um per�odo para outro"
      Top             =   2340
      Width           =   3345
      _ExtentX        =   5900
      _ExtentY        =   609
      BTYPE           =   3
      TX              =   "Montar C�pia"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCopiar.frx":07F8
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   495
      Left            =   30
      TabIndex        =   27
      ToolTipText     =   "Voltar"
      Top             =   0
      Width           =   555
      _ExtentX        =   979
      _ExtentY        =   873
      BTYPE           =   9
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCopiar.frx":0814
      PICN            =   "frmCopiar.frx":0830
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmCopiar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Private Sub cmd1_Click()

End Sub

Private Sub cmdAtualizar_Click()
    lsvCopiar.SelectedItem.Text = Me.dtOrigemNova.Value
    lsvCopiar.SelectedItem.SubItems(2) = Me.dtDestinoNova.Value
End Sub

Private Sub cmdCopiar_Click()
    Dim i As Integer
    
    'If DateDiff("d", DtIniOrigem.Value, DtFimOrigem.Value) <> DateDiff("d", DtIniDestino.Value, CDate(lblDtFimDestino)) Then
    '    MsgBox "N�mero de dias incorreto", vbInformation, "Aten��o"
    '    Exit Sub
    'End If

    If DtIniOrigem.Value > Me.DtFimOrigem.Value Then
        MsgBox "Per�odo incorreto." & vbCrLf & "A data de fim deve ser maior que a data de in�cio", vbInformation, "Aten��o"
        DtFimOrigem.SetFocus
        Exit Sub
    End If
    If DtIniDestino.Value > CDate(lblDtFimDestino) Then
        MsgBox "Per�odo incorreto." & vbCrLf & "A data de fim deve ser maior que a data de in�cio", vbInformation, "Aten��o"
        Exit Sub
    End If
    
    If DtIniOrigem.Value > DtIniDestino.Value And DtIniOrigem.Value < CDate(lblDtFimDestino) Or _
       DtFimOrigem.Value > DtIniDestino.Value And DtIniOrigem.Value < CDate(lblDtFimDestino) Then
        MsgBox "Os per�odos est�o incorretos, por favor verifique.", vbInformation, "Aten��o"
        Exit Sub
    End If
    If DtFimOrigem.Value = CDate(lblDtFimDestino) Then
        MsgBox "A data de destino deve ser diferente da data de origem.", vbInformation, "Aten��o"
        Exit Sub
    End If
    
    If DtIniOrigem.Value = DtIniDestino.Value Then
        MsgBox "N�o � permitido copiar dados do mesmo dia.", vbInformation, "Aten��o"
        Exit Sub
    End If
    
    'If MsgBox("Confirma a c�pia do per�odo " & IIf(Val(cboCliente) > 0, "para o cliente selecionado", "") & "?", vbQuestion + vbYesNo) = vbYes Then
    If MsgBox("Confirma a c�pia do per�odo ?", vbQuestion + vbYesNo) = vbYes Then
        pb.Max = lsvCopiar.ListItems.Count
        pb.Value = 1
        pb.Min = 0
        
        Conectar
        
        For i = 1 To Me.lsvCopiar.ListItems.Count

            Copiar_periodo lsvCopiar.ListItems(i), lsvCopiar.ListItems(i).SubItems(2), Val(frmCadastro.cboPromotor), 0 'Val(cboCliente)
            pb.Value = i
            
        Next
        
        Desconectar
        
        MsgBox "Dados copiados com sucesso !", vbInformation, "Aten��o"
        
        frmCadastro.Tag = "1"
        
        pb.Value = 1
        
    End If
    
End Sub

Private Sub cmdMontarPeriodo_Click()
    Dim vDif As Integer
    Dim lItem As ListItem
    Dim X As Integer
    Dim Y As Integer
    
    fraCopia.Enabled = True
    
    lsvCopiar.ListItems.Clear
    
    vDif = DateDiff("d", DtIniOrigem.Value, DtFimOrigem.Value)
    
    'Marcel - 03/11/2008
    'verifica o dia Origem que nao seje: sab e dom.
    Do
        If Weekday(DateAdd("d", X, DtIniOrigem.Value)) = vbSaturday Then
            X = X + 1
            
        ElseIf Weekday(DateAdd("d", X, DtIniOrigem.Value)) = vbSunday Then
            X = X + 1
        
        Else
            
            'verifica o dia Destino que esse dia Origem encontrado vai entrar,
            'que tbem nao seje: sab nem dom.
            
            Do
                If Weekday(DateAdd("d", Y, DtIniDestino.Value)) = vbSaturday Then
                    Y = Y + 1
                    
                ElseIf Weekday(DateAdd("d", Y, DtIniDestino.Value)) = vbSunday Then
                    Y = Y + 1
                    
                Else
                    
                    Set lItem = lsvCopiar.ListItems.Add
                    'origem
                    lItem = DateAdd("d", X, DtIniOrigem.Value)
                    lItem.SubItems(1) = Format(DateAdd("d", X, DtIniOrigem.Value), "DDD")
                    
                    'destino
                    lItem.SubItems(2) = DateAdd("d", Y, DtIniDestino.Value)
                    lItem.SubItems(3) = Format(DateAdd("d", Y, DtIniDestino.Value), "DDD")
                            
                    Y = Y + 1 'add + 1 no dia Destino
                    
                    GoTo VerificaProxDia:
                End If
                
            Loop
            
VerificaProxDia:
            X = X + 1 'add + 1 no dia Origem
            
        End If
                
        'sai da funcao ao terminar de inserir os dias
        If Y > vDif Then Exit Sub
            
    Loop
    
End Sub


Private Sub cmdPesqCliente_Click()
    frmConsultaCliente.Show 1
End Sub

Private Sub cmdVoltar_Click()
    Unload Me
End Sub

Private Sub DtFimOrigem_Change()
    If DtFimOrigem.Value < Me.DtIniOrigem.Value Then
        MsgBox "A data de t�rmino deve ser maior ou igual a data de in�cio.", vbInformation, "Aten��o"
        DtFimOrigem.SetFocus
        Exit Sub
    End If
    lblDiasUteis.Caption = fCalcular_Dias_Uteis(DtIniOrigem.Value, DtFimOrigem.Value)
End Sub

Private Sub DtIniDestino_Change()
    
    If DtIniDestino.Value < Me.DtFimOrigem.Value Then
        MsgBox "A data de Destino deve ser maior que a data de origem.", vbInformation, "Aten��o"
        Me.DtIniDestino.SetFocus
        Exit Sub
    End If
    
    lblDtFimDestino.Caption = fPegar_Dt_Fim_Destino(Me.DtIniDestino.Value)
    
    If Me.lblDtFimDestino.Caption = "" Then Exit Sub
    
    lblDiasUteisDestino.Caption = fCalcular_Dias_Uteis(DtIniDestino.Value, CDate(lblDtFimDestino.Caption))
End Sub

Private Sub DtIniDestino_LostFocus()
'    If Weekday(DtIniDestino.Value) <> vbMonday Then
'        MsgBox "A data de in�cio deve ser uma segunda feira.", vbInformation, "Aten��o"
'        DtIniDestino.SetFocus
'        Exit Sub
'    End If
    Me.lblDtFimDestino.Caption = fPegar_Dt_Fim_Destino(DtIniDestino.Value)
End Sub

Private Sub DtIniOrigem_Change()
    'If Weekday(DtIniOrigem.Value) <> vbMonday Then
    '    MsgBox "A data de in�cio deve ser uma segunda feira.", vbInformation, "Aten��o"
    '    DtIniOrigem.SetFocus
    '    Exit Sub
    'End If
    Me.lblDiasUteis.Caption = fCalcular_Dias_Uteis(Me.DtIniOrigem.Value, Me.DtFimOrigem.Value)
End Sub

Private Sub DtIniOrigem_LostFocus()
'    If Weekday(DtIniOrigem.Value) <> vbMonday Then
'        MsgBox "A data de in�cio deve ser uma segunda feira.", vbInformation, "Aten��o"
'        DtIniOrigem.SetFocus
'        Exit Sub
'    End If
    lblDiasUteis.Caption = fCalcular_Dias_Uteis(DtIniOrigem.Value, DtFimOrigem.Value)
End Sub

Private Sub Form_Activate()
    Me.Top = (mdiRoteiro.Height - Me.Height) / 2 - mdiRoteiro.mnuXP.Height
    Me.Left = (mdiRoteiro.Width - Me.Width) / 2
    
End Sub

Private Sub Form_Load()
    Me.Top = (mdiRoteiro.Height - Me.Height) / 2 - mdiRoteiro.mnuXP.Height
    Me.Left = (mdiRoteiro.Width - Me.Width) / 2
    
    Me.DtIniOrigem.Value = fPegarDataAtual
    Me.DtFimOrigem.Value = fPegarDataAtual
    
    Me.DtIniDestino.Value = fPegarDataAtual
    Me.dtOrigemNova.Value = fPegarDataAtual
    Me.dtDestinoNova.Value = fPegarDataAtual
    
    
End Sub

Sub Copiar_periodo(pDtIniOrigem As Date, pDtIniDestino As Date, pPromotor As Long, pCodCliente As Long)
    
    On Error GoTo Trata_Erro
    
    vBanco.Parameters.Remove "PM_INI_ORI"
    vBanco.Parameters.Remove "PM_INI_DES"
    vBanco.Parameters.Remove "PM_PROMOTOR"
    vBanco.Parameters.Remove "PM_CLIENTE"
    vBanco.Parameters.Remove "PM_COD_ERRO"
    vBanco.Parameters.Remove "PM_TXT_ERRO"
    
    vBanco.Parameters.Add "PM_COD_ERRO", 0, 2
    vBanco.Parameters.Add "PM_TXT_ERRO", 0, 2
    
    vBanco.Parameters.Add "PM_INI_ORI", Formata_Data(pDtIniOrigem), 1
    vBanco.Parameters.Add "PM_INI_DES", Formata_Data(pDtIniDestino), 1
    
    vBanco.Parameters.Add "PM_PROMOTOR", pPromotor, 1
    
    vBanco.Parameters.Add "PM_CLIENTE", pCodCliente, 1
    
    vBanco.ExecuteSQL "Begin PRODUCAO.PCK_CAD035.PR_COPIAR_PERIODO(:PM_INI_ORI, :PM_INI_DES, :PM_PROMOTOR, :PM_CLIENTE, :PM_COD_ERRO, :PM_TXT_ERRO);END;"
    
    If Val(0 & vBanco.Parameters("PM_COD_ERRO").Value) <> 0 Then
        MsgBox "Erro Oracle" & vbCrLf & "Sub Copiar_Periodo" & vbCrLf & "C�digo: " & vBanco.Parameters("PM_COD_ERRO").Value & vbCrLf & "Descri��o: " & vBanco.Parameters("PM_TXT_ERRO").Value
    End If
    
Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub Copiar_periodo" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
    End If
    
End Sub

Private Sub Image1_Click()

End Sub

Private Sub lsvCopiar_ItemClick(ByVal Item As MSComctlLib.ListItem)
    Me.dtOrigemNova.Value = Item
    Me.dtDestinoNova.Value = Item.SubItems(2)
End Sub

Function fCalcular_Dias_Uteis(pDtInicial As Date, pDtFinal As Date) As Integer
    
    Dim vDtInicial As Date
    Dim vDtFinal As Date
    Dim i As Integer
    
    vDtInicial = pDtInicial
    vDtFinal = pDtFinal
    
    Do While vDtInicial <= pDtFinal
        If Weekday(vDtInicial) <> 7 And Weekday(vDtInicial) <> 1 Then
            i = i + 1
        End If
        vDtInicial = DateAdd("d", 1, vDtInicial)
    Loop
    
    fCalcular_Dias_Uteis = i
    
End Function

Function fPegar_Dt_Fim_Destino(pDtIniDestino As Date)

    Dim vDtFimDestino As Date
    Dim vDtIniDestino As Date
    Dim ii As Integer
    
    vDtIniDestino = pDtIniDestino
    vDtFimDestino = pDtIniDestino
    
    ii = 1
    
    Do While ii < Val(lblDiasUteis.Caption)
    
        If Weekday(vDtFimDestino) <> vbSaturday And Weekday(vDtFimDestino) <> vbSunday Then
            vDtFimDestino = DateAdd("d", 1, vDtFimDestino)
            ii = ii + 1
        Else
            vDtFimDestino = DateAdd("d", 1, vDtFimDestino)
        End If
            
    Loop
    fPegar_Dt_Fim_Destino = vDtFimDestino
    
End Function
