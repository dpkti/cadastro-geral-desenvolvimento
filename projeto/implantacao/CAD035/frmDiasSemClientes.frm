VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmDiasSemClientes 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Dias Sem Clientes"
   ClientHeight    =   3630
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   1740
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3630
   ScaleWidth      =   1740
   Begin VB.ListBox lstDatas 
      Appearance      =   0  'Flat
      Height          =   2760
      Left            =   30
      TabIndex        =   0
      Top             =   810
      Width           =   1665
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   0
      TabIndex        =   1
      Top             =   540
      Width           =   1725
      _ExtentX        =   3043
      _ExtentY        =   53
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   495
      Left            =   0
      TabIndex        =   3
      ToolTipText     =   "Voltar"
      Top             =   0
      Width           =   555
      _ExtentX        =   979
      _ExtentY        =   873
      BTYPE           =   9
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmDiasSemClientes.frx":0000
      PICN            =   "frmDiasSemClientes.frx":001C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "M�s atual"
      Height          =   195
      Left            =   30
      TabIndex        =   2
      Top             =   600
      Width           =   690
   End
End
Attribute VB_Name = "frmDiasSemClientes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdVoltar_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Me.Top = frmCadastro.Height / 2 - (cmdVoltar.Height * 2)
    Me.Left = (frmCadastro.Width / 2) + (frmCadastro.trv.Width / 2) + Me.Width
End Sub

Private Sub lstDatas_DblClick()
    If lstDatas.List(lstDatas.ListIndex) = "" Then Exit Sub
    
    Me.Visible = False
    
    Dim i As Integer
    For i = 1 To frmCadastro.trv.Nodes.Count
    
        If Trim(Mid(frmCadastro.trv.Nodes(i).Text, 1, 8)) = lstDatas.List(lstDatas.ListIndex) Then
            frmCadastro.trv.SetFocus
            frmCadastro.trv.Nodes(i).Selected = True
            frmCadastro.trv.Nodes(i).Expanded = True
            frmCadastro.trv.Nodes(i).EnsureVisible
            Exit For
        End If
    Next
    Unload Me
End Sub
