VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{0F0877EF-2A93-4AE6-8BA8-4129832C32C3}#230.0#0"; "smartmenuxp.ocx"
Object = "{00025600-0000-0000-C000-000000000046}#4.6#0"; "crystl32.ocx"
Begin VB.MDIForm mdiRoteiro 
   Appearance      =   0  'Flat
   BackColor       =   &H8000000C&
   Caption         =   "CAD035 .: Sistema de Cadastro de Roteiros :."
   ClientHeight    =   8355
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   12645
   Icon            =   "mdiRoteiro.frx":0000
   LinkTopic       =   "MDIForm1"
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin Crystal.CrystalReport crp 
      Left            =   750
      Top             =   660
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   262150
   End
   Begin VBSmartXPMenu.SmartMenuXP mnuXP 
      Align           =   1  'Align Top
      Height          =   375
      Left            =   0
      Top             =   0
      Width           =   855
      _ExtentX        =   1508
      _ExtentY        =   661
      BackColor       =   16761024
      FontForeColor   =   8388608
      FontBackColor   =   16761024
      CheckBackColor  =   16777215
      SelBackColor    =   16761024
      SelForeColor    =   16777215
      SeparatorColor  =   16711680
      DisabledColor   =   16761024
      SmoothMenuBar   =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Shadow          =   0   'False
   End
   Begin MSComctlLib.ImageList iml 
      Left            =   90
      Top             =   480
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   23
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "mdiRoteiro.frx":0CCA
            Key             =   "Cliente_Atribuir"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "mdiRoteiro.frx":10F1
            Key             =   "Ferramentas"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "mdiRoteiro.frx":19CB
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "mdiRoteiro.frx":26A5
            Key             =   "Configuracao"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "mdiRoteiro.frx":337F
            Key             =   "Acoes"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "mdiRoteiro.frx":3B62
            Key             =   "Pesquisar_2"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "mdiRoteiro.frx":3F3A
            Key             =   "Help"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "mdiRoteiro.frx":4357
            Key             =   "Sair"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "mdiRoteiro.frx":46F1
            Key             =   "Cliente"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "mdiRoteiro.frx":4AE8
            Key             =   "Primeiro"
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "mdiRoteiro.frx":4EBA
            Key             =   "Copiar_Clientes"
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "mdiRoteiro.frx":52E8
            Key             =   "Calendario"
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "mdiRoteiro.frx":53C3
            Key             =   "Relatorio"
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "mdiRoteiro.frx":549D
            Key             =   "Foto_Cliente"
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "mdiRoteiro.frx":55A5
            Key             =   "Ultimo"
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "mdiRoteiro.frx":5972
            Key             =   "Proximo"
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "mdiRoteiro.frx":5D24
            Key             =   "Anterior"
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "mdiRoteiro.frx":60D6
            Key             =   "Salvar"
         EndProperty
         BeginProperty ListImage19 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "mdiRoteiro.frx":64CB
            Key             =   "Impressora"
         EndProperty
         BeginProperty ListImage20 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "mdiRoteiro.frx":68E1
            Key             =   "Pesquisar"
         EndProperty
         BeginProperty ListImage21 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "mdiRoteiro.frx":6CDF
            Key             =   "Excluir_Cliente"
         EndProperty
         BeginProperty ListImage22 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "mdiRoteiro.frx":70F1
            Key             =   "Parametros"
         EndProperty
         BeginProperty ListImage23 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "mdiRoteiro.frx":7285
            Key             =   "Excluir"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "mdiRoteiro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub MDIForm_Activate()
    On Error Resume Next
    
    frmCadastro.fraVisitas.Refresh
    frmCadastro.lsv.Refresh
    frmCadastro.trv.Refresh
    
End Sub

Private Sub MDIForm_Load()
    
    Me.Caption = Me.Caption & " Vers�o: " & App.Major & "." & App.Minor & "." & App.Revision
    
    mnuXP.SmoothMenuBar = True
        
    'Valida��o de configura��es regionais.
    If Get_locale(LOCALE_SDECIMAL) <> "." Or Get_locale(LOCALE_STHOUSAND) <> "," Or Get_locale(LOCALE_SSHORTDATE) <> "dd/MM/yy" Or Get_locale(LOCALE_STIMEFORMAT) <> "HH:mm:ss" Then
        MsgBox "As Configura��es Regionais de seu computador est�o incorretas." & vbCrLf & "Entre em contato com o Helpdesk e informe esta mensagem.", vbInformation, "Aten��o"
        End
    End If
    
    
    
    With mdiRoteiro.mnuXP.MenuItems
      
      .Add 0, "Cadastros_Visitas", smiPicture, "&Visitas", , vbShiftMask
      
      If fCDtxt = "01" Then
        .Add 0, "Cadastros_Parametros", smiPicture, "&Par�metros", , vbShiftMask
      End If
      
      '.Add 0, "Resumo", , "&Resumo"
      
      '.Add 0, "Help", , "&Help"
      
      .Add 0, "Sair", , "&Sair"
      
    End With
    
    'App.HelpFile = App.Path & "\COM500.chm"
End Sub

Private Sub MnuXP_Click(ByVal ID As Long)
    Select Case mnuXP.MenuItems.Key(ID)
        
        Case "Cadastros_Visitas"
            Me.WindowState = vbMaximized
            frmCadastro.Show
            
        Case "Cadastros_Parametros"
            Me.WindowState = vbMaximized
            frmParametros.Show 1
            
        Case "Consultas"
            Me.WindowState = vbMaximized
            frmConsultas.Show
            
        Case "Cadastros_Sair"
            'vbanco.Close
            'Set vbanco = Nothing
            End
        
        Case "Sair"
            End
            
    End Select
End Sub

Function fCDtxt() As String
    
    Dim vArq As Integer
    Dim vLinha As String
    
    vArq = FreeFile
    Open "H:\ORACLE\DADOS\32BITS\CD.TXT" For Input As vArq

    Input #vArq, vLinha

    fCDtxt = Mid(vLinha, 1, 2)

    Close vArq
    
End Function
