VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmCadastro 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cadastro de Roteiro de Visitas"
   ClientHeight    =   8655
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   14775
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8655
   ScaleWidth      =   14775
   Tag             =   "frmCadastro"
   Begin Bot�o.cmd cmdVoltar 
      Height          =   525
      Left            =   30
      TabIndex        =   51
      ToolTipText     =   "Voltar"
      Top             =   30
      Width           =   555
      _ExtentX        =   979
      _ExtentY        =   926
      BTYPE           =   9
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   -2147483633
      BCOLO           =   -2147483633
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadastro.frx":0000
      PICN            =   "frmCadastro.frx":001C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSComctlLib.ImageList img 
      Left            =   7590
      Top             =   30
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   6
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCadastro.frx":0CF6
            Key             =   "Cliente"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCadastro.frx":10B3
            Key             =   "Cliente_Comentario"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCadastro.frx":198D
            Key             =   "Mes"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCadastro.frx":2667
            Key             =   "Dia"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCadastro.frx":2C01
            Key             =   "Ano"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCadastro.frx":38DB
            Key             =   "Semana"
         EndProperty
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   0
      TabIndex        =   21
      Top             =   570
      Width           =   14295
      _ExtentX        =   25215
      _ExtentY        =   53
      BackColor       =   -2147483633
   End
   Begin VB.Frame fraTotaisRoteiro 
      Caption         =   "Resumo do m�s atual:"
      Height          =   1095
      Left            =   8160
      TabIndex        =   4
      Top             =   7260
      Width           =   6555
      Begin VB.Label Label12 
         AutoSize        =   -1  'True
         Caption         =   "Qtde clientes coment�rios:"
         Height          =   195
         Left            =   3300
         TabIndex        =   35
         Top             =   780
         Width           =   1920
      End
      Begin VB.Label lblTotalComentariosPeriodo 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   5220
         TabIndex        =   34
         Top             =   720
         Width           =   645
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         Caption         =   "Total visitas no m�s:"
         Height          =   195
         Left            =   90
         TabIndex        =   33
         Top             =   810
         Width           =   1470
      End
      Begin VB.Label lblTotalVisitasPeriodo 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   1980
         TabIndex        =   32
         Top             =   750
         Width           =   645
      End
      Begin VB.Label lblQtdDiasComClientes 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00000000&
         Height          =   255
         Left            =   5220
         TabIndex        =   16
         Top             =   450
         Width           =   645
      End
      Begin VB.Label Label11 
         AutoSize        =   -1  'True
         Caption         =   "Qtde de dias com clientes:"
         Height          =   195
         Left            =   3300
         TabIndex        =   15
         Top             =   510
         Width           =   1890
      End
      Begin VB.Label lblQtdDiasSemClientes 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   1980
         TabIndex        =   14
         Top             =   480
         Width           =   645
      End
      Begin VB.Label Label9 
         AutoSize        =   -1  'True
         Caption         =   "Qtde dias sem clientes:"
         Height          =   195
         Left            =   90
         TabIndex        =   13
         Top             =   540
         Width           =   1665
      End
      Begin VB.Label lblDiaComMenorNumCliente 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   5220
         MousePointer    =   4  'Icon
         TabIndex        =   12
         Top             =   180
         Width           =   1155
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "Dia com menor n� Clientes:"
         Height          =   195
         Left            =   3270
         TabIndex        =   11
         Top             =   240
         Width           =   1935
      End
      Begin VB.Label lblDiaComMaiorNumCliente 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   1980
         TabIndex        =   10
         Top             =   210
         Width           =   1155
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Dia com maior n� Clientes:"
         Height          =   195
         Left            =   90
         TabIndex        =   9
         Top             =   270
         Width           =   1875
      End
   End
   Begin VB.Frame fraTotais 
      Caption         =   "Resumo de Clientes no m�s atual:"
      Height          =   825
      Left            =   30
      TabIndex        =   3
      Top             =   7260
      Width           =   7335
      Begin VB.Label lblTotalClientesNaoRoteirizados 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   5070
         TabIndex        =   20
         Top             =   180
         Width           =   645
      End
      Begin VB.Label Label14 
         AutoSize        =   -1  'True
         Caption         =   "Qtde clientes n�o roteirizados:"
         Height          =   195
         Left            =   2850
         TabIndex        =   19
         Top             =   240
         Width           =   2205
      End
      Begin VB.Label lblTotalClientesRoteirizados 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   2040
         TabIndex        =   18
         Top             =   510
         Width           =   645
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Qtde clientes roteirizados:"
         Height          =   195
         Left            =   60
         TabIndex        =   17
         ToolTipText     =   "Clientes roteirizados no m�s atual"
         Top             =   570
         Width           =   1890
      End
      Begin VB.Label lblTotalClientes 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   5070
         TabIndex        =   8
         Top             =   480
         Width           =   645
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Total de Clientes:"
         Height          =   195
         Left            =   3780
         TabIndex        =   7
         Top             =   540
         Width           =   1260
      End
      Begin VB.Label lblTotalClientesSemRoteiro 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   2040
         TabIndex        =   6
         Top             =   210
         Width           =   645
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Clientes novos sem roteiro:"
         Height          =   195
         Left            =   60
         TabIndex        =   5
         Top             =   270
         Width           =   1965
      End
   End
   Begin MSComctlLib.StatusBar stbCadastro 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   36
      Top             =   8370
      Width           =   14775
      _ExtentX        =   26061
      _ExtentY        =   503
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   5
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   15743
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Text            =   "N� Anos:"
            TextSave        =   "N� Anos:"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Text            =   "N� Meses:"
            TextSave        =   "N� Meses:"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Text            =   "N� Semanas:"
            TextSave        =   "N� Semanas:"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Text            =   "N� Dias:"
            TextSave        =   "N� Dias:"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ProgressBar pb 
      Height          =   165
      Left            =   30
      TabIndex        =   37
      Top             =   8160
      Width           =   7305
      _ExtentX        =   12885
      _ExtentY        =   291
      _Version        =   393216
      BorderStyle     =   1
      Appearance      =   0
      Scrolling       =   1
   End
   Begin VB.Frame fraVisitas 
      Caption         =   "Cadastro de Visitas"
      Height          =   6615
      Left            =   30
      TabIndex        =   0
      Top             =   630
      Width           =   14715
      Begin VB.Frame fraImprimir 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "Per�odo"
         ForeColor       =   &H80000008&
         Height          =   1125
         Left            =   2475
         TabIndex        =   87
         Top             =   2790
         Visible         =   0   'False
         Width           =   3405
         Begin MSComCtl2.DTPicker cmbDtInicial 
            Height          =   330
            Left            =   315
            TabIndex        =   90
            Top             =   225
            Width           =   1095
            _ExtentX        =   1931
            _ExtentY        =   582
            _Version        =   393216
            Format          =   16121857
            CurrentDate     =   39927
         End
         Begin Bot�o.cmd cmd1 
            Height          =   255
            Left            =   2730
            TabIndex        =   88
            ToolTipText     =   "Finalizar Pesquisa"
            Top             =   780
            Width           =   585
            _ExtentX        =   1032
            _ExtentY        =   450
            BTYPE           =   3
            TX              =   ""
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   -1  'True
            BCOL            =   13160660
            BCOLO           =   13160660
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "frmCadastro.frx":45B5
            PICN            =   "frmCadastro.frx":45D1
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   0
            NGREY           =   0   'False
            FX              =   0
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin MSComCtl2.DTPicker cmbDtFinal 
            Height          =   330
            Left            =   1665
            TabIndex        =   92
            Top             =   225
            Width           =   1095
            _ExtentX        =   1931
            _ExtentY        =   582
            _Version        =   393216
            Format          =   16121857
            CurrentDate     =   39927
         End
         Begin VB.Label Label29 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            Caption         =   "a"
            ForeColor       =   &H80000008&
            Height          =   195
            Left            =   1485
            TabIndex        =   91
            Top             =   300
            Width           =   90
         End
         Begin VB.Image Image2 
            Appearance      =   0  'Flat
            Height          =   480
            Left            =   2850
            Picture         =   "frmCadastro.frx":496B
            Top             =   195
            Width           =   480
         End
         Begin VB.Label Label27 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            Caption         =   "De"
            ForeColor       =   &H80000008&
            Height          =   195
            Left            =   90
            TabIndex        =   89
            Top             =   300
            Width           =   195
         End
      End
      Begin VB.Frame fraFiltrar 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "Filtrar"
         ForeColor       =   &H80000008&
         Height          =   1125
         Left            =   1800
         TabIndex        =   82
         Top             =   2805
         Visible         =   0   'False
         Width           =   4665
         Begin VB.TextBox txtFiltrar 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   90
            TabIndex        =   83
            Top             =   420
            Width           =   3945
         End
         Begin Bot�o.cmd cmdAplicarFiltrar 
            Height          =   255
            Left            =   3450
            TabIndex        =   84
            ToolTipText     =   "Finalizar Pesquisa"
            Top             =   780
            Width           =   585
            _ExtentX        =   1032
            _ExtentY        =   450
            BTYPE           =   3
            TX              =   ""
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   -1  'True
            BCOL            =   13160660
            BCOLO           =   13160660
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "frmCadastro.frx":5635
            PICN            =   "frmCadastro.frx":5651
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   0
            NGREY           =   0   'False
            FX              =   0
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin VB.Label Label28 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            Caption         =   "Por C�digo: Exemplo: 52306; 628; 575; 3655"
            ForeColor       =   &H80000008&
            Height          =   195
            Left            =   90
            TabIndex        =   85
            Top             =   210
            Width           =   3255
         End
         Begin VB.Image imgFiltrarFechar 
            Appearance      =   0  'Flat
            Height          =   480
            Left            =   4110
            Picture         =   "frmCadastro.frx":59EB
            Top             =   330
            Width           =   480
         End
      End
      Begin VB.Frame fraPesq 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "Pesquisar"
         ForeColor       =   &H80000008&
         Height          =   1185
         Left            =   1800
         TabIndex        =   73
         Top             =   2700
         Visible         =   0   'False
         Width           =   4665
         Begin VB.TextBox txtLocalizarPorNome 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   390
            Left            =   1290
            TabIndex        =   78
            Top             =   420
            Width           =   2805
         End
         Begin VB.TextBox txtLocalizarPorCodigo 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   11.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   390
            Left            =   90
            TabIndex        =   75
            Top             =   420
            Width           =   1095
         End
         Begin Bot�o.cmd cmdLocalizar 
            Height          =   255
            Left            =   600
            TabIndex        =   74
            ToolTipText     =   "Finalizar Pesquisa"
            Top             =   840
            Width           =   585
            _ExtentX        =   1032
            _ExtentY        =   450
            BTYPE           =   3
            TX              =   ""
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   -1  'True
            BCOL            =   13160660
            BCOLO           =   13160660
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "frmCadastro.frx":66B5
            PICN            =   "frmCadastro.frx":66D1
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   0
            NGREY           =   0   'False
            FX              =   0
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin Bot�o.cmd cmdFinalizarPorNome 
            Height          =   255
            Left            =   3510
            TabIndex        =   77
            ToolTipText     =   "Finalizar Pesquisa"
            Top             =   840
            Width           =   585
            _ExtentX        =   1032
            _ExtentY        =   450
            BTYPE           =   3
            TX              =   ""
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   -1  'True
            BCOL            =   13160660
            BCOLO           =   13160660
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "frmCadastro.frx":6A6B
            PICN            =   "frmCadastro.frx":6A87
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   0
            NGREY           =   0   'False
            FX              =   0
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin VB.Image ImgSair 
            Appearance      =   0  'Flat
            Height          =   480
            Left            =   4140
            Picture         =   "frmCadastro.frx":6E21
            Top             =   390
            Width           =   480
         End
         Begin VB.Label Label26 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            Caption         =   "Por Nome:"
            ForeColor       =   &H80000008&
            Height          =   195
            Left            =   1290
            TabIndex        =   79
            Top             =   210
            Width           =   750
         End
         Begin VB.Label Label25 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            Caption         =   "Por C�digo:"
            ForeColor       =   &H80000008&
            Height          =   195
            Left            =   90
            TabIndex        =   76
            Top             =   210
            Width           =   840
         End
      End
      Begin VB.Frame fraOrder 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "Ordenar por"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   825
         Left            =   510
         TabIndex        =   60
         Top             =   1740
         Visible         =   0   'False
         Width           =   7005
         Begin VB.ComboBox cboOrder4 
            Height          =   315
            ItemData        =   "frmCadastro.frx":7AEB
            Left            =   4500
            List            =   "frmCadastro.frx":7B19
            TabIndex        =   67
            Top             =   420
            Width           =   1425
         End
         Begin VB.ComboBox cboOrder3 
            Height          =   315
            ItemData        =   "frmCadastro.frx":7BB5
            Left            =   3030
            List            =   "frmCadastro.frx":7BE3
            TabIndex        =   65
            Top             =   420
            Width           =   1425
         End
         Begin VB.ComboBox cboOrder2 
            Height          =   315
            ItemData        =   "frmCadastro.frx":7C7F
            Left            =   1560
            List            =   "frmCadastro.frx":7CAD
            TabIndex        =   63
            Top             =   420
            Width           =   1425
         End
         Begin VB.ComboBox cboOrder1 
            Height          =   315
            ItemData        =   "frmCadastro.frx":7D49
            Left            =   60
            List            =   "frmCadastro.frx":7D77
            TabIndex        =   61
            Top             =   420
            Width           =   1455
         End
         Begin VB.Image imgFechar 
            Appearance      =   0  'Flat
            Height          =   480
            Left            =   6480
            Picture         =   "frmCadastro.frx":7E13
            Top             =   210
            Width           =   480
         End
         Begin VB.Image Image1 
            Height          =   480
            Left            =   5970
            Picture         =   "frmCadastro.frx":8ADD
            ToolTipText     =   "Aplicar Orderna��o"
            Top             =   210
            Width           =   480
         End
         Begin VB.Label Label24 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            Caption         =   "Depois Por"
            ForeColor       =   &H80000008&
            Height          =   195
            Left            =   4500
            TabIndex        =   68
            Top             =   210
            Width           =   765
         End
         Begin VB.Label Label23 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            Caption         =   "Depois Por"
            ForeColor       =   &H80000008&
            Height          =   195
            Left            =   3030
            TabIndex        =   66
            Top             =   210
            Width           =   765
         End
         Begin VB.Label Label22 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            Caption         =   "Depois Por"
            ForeColor       =   &H80000008&
            Height          =   195
            Left            =   1560
            TabIndex        =   64
            Top             =   210
            Width           =   765
         End
         Begin VB.Label Label21 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            BackColor       =   &H80000005&
            Caption         =   "Primeiro Por"
            ForeColor       =   &H80000008&
            Height          =   195
            Left            =   60
            TabIndex        =   62
            Top             =   210
            Width           =   855
         End
      End
      Begin VB.ComboBox cboCarteira 
         Enabled         =   0   'False
         Height          =   315
         Left            =   3180
         TabIndex        =   58
         Top             =   420
         Width           =   2625
      End
      Begin VB.Frame Frame1 
         Appearance      =   0  'Flat
         ForeColor       =   &H80000008&
         Height          =   885
         Left            =   13050
         TabIndex        =   38
         Top             =   5670
         Width           =   1605
         Begin VB.Label lblAcimaDoMaximo 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Acima do M�ximo"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   -1  'True
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   195
            Left            =   270
            TabIndex        =   44
            ToolTipText     =   "Quantidade de clientes acima do M�ximo"
            Top             =   660
            Width           =   1230
         End
         Begin VB.Label Label20 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Dentro do Padr�o"
            Height          =   195
            Left            =   270
            TabIndex        =   43
            ToolTipText     =   "Quantidade de clientes dentro dos intervalos de M�nimo e M�ximo"
            Top             =   390
            Width           =   1275
         End
         Begin VB.Label lblVermelho 
            Appearance      =   0  'Flat
            BackColor       =   &H000000FF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H00FFFFFF&
            Height          =   165
            Left            =   60
            TabIndex        =   42
            Top             =   135
            Width           =   165
         End
         Begin VB.Label Label2 
            Appearance      =   0  'Flat
            BackColor       =   &H0000FF00&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Left            =   60
            TabIndex        =   41
            Top             =   405
            Width           =   165
         End
         Begin VB.Label Label6 
            Appearance      =   0  'Flat
            BackColor       =   &H00FF0000&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H80000008&
            Height          =   165
            Left            =   60
            TabIndex        =   40
            Top             =   675
            Width           =   165
         End
         Begin VB.Label lblAbaixoDoMinimo 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Abaixo do M�nimo"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   -1  'True
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   195
            Left            =   270
            TabIndex        =   39
            ToolTipText     =   "Quantidade de clientes abaixo da quantidade m�nima."
            Top             =   120
            Width           =   1245
         End
      End
      Begin Bot�o.cmd cmdRetrair 
         Height          =   285
         Left            =   14010
         TabIndex        =   23
         ToolTipText     =   "Retrair Tudo"
         Top             =   120
         Width           =   285
         _ExtentX        =   503
         _ExtentY        =   503
         BTYPE           =   5
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadastro.frx":97A7
         PICN            =   "frmCadastro.frx":97C3
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdExpandir 
         Height          =   285
         Left            =   13680
         TabIndex        =   22
         ToolTipText     =   "Expandir Tudo"
         Top             =   120
         Width           =   285
         _ExtentX        =   503
         _ExtentY        =   503
         BTYPE           =   5
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadastro.frx":9B25
         PICN            =   "frmCadastro.frx":9B41
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MSComctlLib.ListView lsv 
         Height          =   5505
         Left            =   90
         TabIndex        =   2
         Top             =   1050
         Width           =   7905
         _ExtentX        =   13944
         _ExtentY        =   9710
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         MousePointer    =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   15
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Width           =   476
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   1
            Text            =   "C�digo"
            Object.Width           =   1411
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Nome do Cliente"
            Object.Width           =   4410
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   3
            Text            =   "N� Visitas"
            Object.Width           =   1499
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Text            =   "Carteira"
            Object.Width           =   1587
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Text            =   "Cidade"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Text            =   "Bairro"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Text            =   "UF"
            Object.Width           =   706
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Text            =   "Endereco"
            Object.Width           =   5292
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Text            =   "CEP"
            Object.Width           =   1587
         EndProperty
         BeginProperty ColumnHeader(11) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   10
            Text            =   "Quadrante"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(12) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   11
            Text            =   "Tipo de Cliente"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(13) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   12
            Text            =   "Segmento"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(14) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   13
            Text            =   "Dt Ult Compra"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(15) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   14
            Text            =   "Dt Ult Compra Indent"
            Object.Width           =   2540
         EndProperty
      End
      Begin Bot�o.cmd cmdAtribuir 
         Height          =   405
         Left            =   8010
         TabIndex        =   31
         ToolTipText     =   "Atribuir os clientes com as datas selecionadas"
         Top             =   3390
         Width           =   465
         _ExtentX        =   820
         _ExtentY        =   714
         BTYPE           =   9
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   -2147483633
         BCOLO           =   -2147483633
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadastro.frx":9EA8
         PICN            =   "frmCadastro.frx":9EC4
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdGravarComentario 
         Height          =   330
         Left            =   12720
         TabIndex        =   30
         ToolTipText     =   "Salvar Comnet�rio"
         Top             =   5430
         Width           =   345
         _ExtentX        =   609
         _ExtentY        =   582
         BTYPE           =   9
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadastro.frx":AB9E
         PICN            =   "frmCadastro.frx":ABBA
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.ComboBox cboPromotor 
         Height          =   315
         Left            =   60
         TabIndex        =   26
         Text            =   "Combo1"
         Top             =   420
         Width           =   3075
      End
      Begin VB.TextBox txtComentario 
         Appearance      =   0  'Flat
         Height          =   795
         Left            =   8520
         TabIndex        =   25
         Top             =   5760
         Width           =   4515
      End
      Begin Bot�o.cmd cmdIrParaMesAtual 
         Height          =   435
         Left            =   8070
         TabIndex        =   29
         ToolTipText     =   "Ir para m�s atual"
         Top             =   330
         Width           =   405
         _ExtentX        =   714
         _ExtentY        =   767
         BTYPE           =   9
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   -2147483633
         BCOLO           =   -2147483633
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadastro.frx":BE0C
         PICN            =   "frmCadastro.frx":BE28
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdSelCliente 
         Height          =   255
         Left            =   1530
         TabIndex        =   69
         ToolTipText     =   "Selecionar Cliente"
         Top             =   780
         Width           =   1185
         _ExtentX        =   2090
         _ExtentY        =   450
         BTYPE           =   3
         TX              =   "Marcar Cliente"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   -2147483633
         BCOLO           =   -2147483633
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadastro.frx":CB02
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdAumentar 
         Height          =   255
         Left            =   2790
         TabIndex        =   70
         ToolTipText     =   "Selecionar Cliente"
         Top             =   780
         Width           =   675
         _ExtentX        =   1191
         _ExtentY        =   450
         BTYPE           =   3
         TX              =   "Zoom +"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   -2147483633
         BCOLO           =   -2147483633
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadastro.frx":CB1E
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdDiminuir 
         Height          =   255
         Left            =   3540
         TabIndex        =   71
         ToolTipText     =   "Selecionar Cliente"
         Top             =   780
         Width           =   645
         _ExtentX        =   1138
         _ExtentY        =   450
         BTYPE           =   3
         TX              =   "Zoom -"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   -2147483633
         BCOLO           =   -2147483633
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadastro.frx":CB3A
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MSComctlLib.TreeView trv 
         Height          =   5265
         Left            =   8490
         TabIndex        =   1
         Top             =   150
         Width           =   6165
         _ExtentX        =   10874
         _ExtentY        =   9287
         _Version        =   393217
         Indentation     =   529
         LabelEdit       =   1
         LineStyle       =   1
         Style           =   7
         Checkboxes      =   -1  'True
         ImageList       =   "img"
         BorderStyle     =   1
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         OLEDragMode     =   1
         OLEDropMode     =   1
      End
      Begin Bot�o.cmd cmdOrdenar 
         Height          =   255
         Left            =   720
         TabIndex        =   72
         ToolTipText     =   "Selecionar Cliente"
         Top             =   780
         Width           =   735
         _ExtentX        =   1296
         _ExtentY        =   450
         BTYPE           =   3
         TX              =   "Ordenar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   -2147483633
         BCOLO           =   -2147483633
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadastro.frx":CB56
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdPesquisar 
         Height          =   255
         Left            =   4260
         TabIndex        =   80
         ToolTipText     =   "Selecionar Cliente"
         Top             =   780
         Width           =   855
         _ExtentX        =   1508
         _ExtentY        =   450
         BTYPE           =   3
         TX              =   "Pesquisar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   -2147483633
         BCOLO           =   -2147483633
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadastro.frx":CB72
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdFiltrar 
         Height          =   255
         Left            =   5160
         TabIndex        =   81
         ToolTipText     =   "Selecionar Cliente"
         Top             =   780
         Width           =   645
         _ExtentX        =   1138
         _ExtentY        =   450
         BTYPE           =   3
         TX              =   "Filtrar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   -2147483633
         BCOLO           =   -2147483633
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadastro.frx":CB8E
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label Label13 
         AutoSize        =   -1  'True
         Caption         =   "Carteiras"
         Height          =   195
         Left            =   3180
         TabIndex        =   59
         Top             =   210
         Width           =   660
      End
      Begin VB.Label Label19 
         AutoSize        =   -1  'True
         Caption         =   "Data Atual:"
         Height          =   195
         Left            =   5970
         TabIndex        =   50
         Top             =   450
         Width           =   825
      End
      Begin VB.Label lblDataAtual 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   225
         Left            =   6825
         TabIndex        =   49
         Top             =   450
         Width           =   1095
      End
      Begin VB.Label lblDataInicio 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   225
         Left            =   6825
         TabIndex        =   48
         Top             =   210
         Width           =   1095
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         Caption         =   "Data Inicial:"
         Height          =   195
         Left            =   5940
         TabIndex        =   47
         Top             =   210
         Width           =   855
      End
      Begin VB.Label lblDataFim 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   225
         Left            =   6825
         TabIndex        =   46
         Top             =   690
         Width           =   1095
      End
      Begin VB.Label Label15 
         AutoSize        =   -1  'True
         Caption         =   "Data Final:"
         Height          =   195
         Left            =   6015
         TabIndex        =   45
         Top             =   690
         Width           =   780
      End
      Begin VB.Label Label18 
         AutoSize        =   -1  'True
         Caption         =   "Clientes"
         Height          =   195
         Left            =   90
         TabIndex        =   28
         Top             =   840
         Width           =   570
      End
      Begin VB.Label Label17 
         AutoSize        =   -1  'True
         Caption         =   "Promotores"
         Height          =   195
         Left            =   60
         TabIndex        =   27
         Top             =   210
         Width           =   825
      End
      Begin VB.Label Label16 
         AutoSize        =   -1  'True
         Caption         =   "Coment�rios sobre cliente e data selecionados:"
         Height          =   195
         Left            =   8520
         TabIndex        =   24
         Top             =   5550
         Width           =   3390
      End
      Begin VB.Shape Shape2 
         BackColor       =   &H8000000F&
         BackStyle       =   1  'Opaque
         Height          =   825
         Left            =   5910
         Top             =   150
         Width           =   2085
      End
   End
   Begin Bot�o.cmd cmdCalcular 
      Height          =   525
      Left            =   10740
      TabIndex        =   52
      ToolTipText     =   "Atualizar Totais"
      Top             =   0
      Width           =   555
      _ExtentX        =   979
      _ExtentY        =   926
      BTYPE           =   9
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   -2147483633
      BCOLO           =   -2147483633
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadastro.frx":CBAA
      PICN            =   "frmCadastro.frx":CBC6
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdCopiar 
      Height          =   525
      Left            =   11334
      TabIndex        =   53
      ToolTipText     =   "Copiar per�odo"
      Top             =   0
      Width           =   555
      _ExtentX        =   979
      _ExtentY        =   926
      BTYPE           =   9
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   -2147483633
      BCOLO           =   -2147483633
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadastro.frx":D8A0
      PICN            =   "frmCadastro.frx":D8BC
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdExcluirClientesSemana 
      Height          =   525
      Left            =   11928
      TabIndex        =   54
      ToolTipText     =   "Excluir todos os clientes da semana"
      Top             =   0
      Width           =   555
      _ExtentX        =   979
      _ExtentY        =   926
      BTYPE           =   9
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   -2147483633
      BCOLO           =   -2147483633
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadastro.frx":E596
      PICN            =   "frmCadastro.frx":E5B2
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdExcluirClientesDia 
      Height          =   525
      Left            =   12522
      TabIndex        =   55
      ToolTipText     =   "Excluir todos os clientes do dia"
      Top             =   0
      Width           =   555
      _ExtentX        =   979
      _ExtentY        =   926
      BTYPE           =   9
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   -2147483633
      BCOLO           =   -2147483633
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadastro.frx":F28C
      PICN            =   "frmCadastro.frx":F2A8
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdExcluirCliente 
      Height          =   525
      Left            =   13116
      TabIndex        =   56
      ToolTipText     =   "Excluir cliente selecionado"
      Top             =   0
      Width           =   555
      _ExtentX        =   979
      _ExtentY        =   926
      BTYPE           =   9
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   -2147483633
      BCOLO           =   -2147483633
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadastro.frx":FF82
      PICN            =   "frmCadastro.frx":FF9E
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdExcluirClientesSelec 
      Height          =   525
      Left            =   13710
      TabIndex        =   57
      ToolTipText     =   "Excluir Clientes Selecionados"
      Top             =   0
      Width           =   555
      _ExtentX        =   979
      _ExtentY        =   926
      BTYPE           =   9
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   -2147483633
      BCOLO           =   -2147483633
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadastro.frx":10C78
      PICN            =   "frmCadastro.frx":10C94
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdImprimir 
      Height          =   525
      Left            =   10125
      TabIndex        =   86
      ToolTipText     =   "Imprimir roteiro"
      Top             =   0
      Width           =   555
      _ExtentX        =   979
      _ExtentY        =   926
      BTYPE           =   9
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   -2147483633
      BCOLO           =   -2147483633
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadastro.frx":1196E
      PICN            =   "frmCadastro.frx":1198A
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmCadastro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'// variable that tells us if
'// we are dragging (ie the user is dragging a node from this treeview control
'// or not (ie the user is trying to drag an object from another
'// control and/or program)
Private blnDragging As Boolean
Sub Gerar_Excel(pNomeArquivo As String, pSeparador As String)

    Dim vExcel As Object

    Set vExcel = CreateObject("EXCEL.APPLICATION")
    vExcel.Visible = True
    vExcel.Workbooks.OpenText pNomeArquivo, , , xlDelimited, , , , , , , True, pSeparador
    
End Sub
Sub Gerar_Arq_Texto(pObj As Object, pNomeArquivo As String, pSeparador As String, pCaption As String)

    Dim vArq As Integer
    Dim vTexto As String
    Dim i As Integer
    Dim ii As Integer
    
    vArq = FreeFile
    
    Open pNomeArquivo For Output As #vArq
    
    vTexto = ".:Roteiro de visitas do Per�odo: " & Format(frmCadastro.cmbDtInicial.Value, "dd/MM/yy") & " a " & Format(frmCadastro.cmbDtFinal.Value, "dd/MM/yy")
    Print #vArq, vTexto
    vTexto = ""
    
    For i = 0 To pObj.Fields.Count - 1
    
        If vTexto = "" Then
            vTexto = pObj.Fields(i).Name & Chr(9)
        Else
            vTexto = vTexto & pObj.Fields(i).Name & Chr(9)
        End If
        
    Next
    
    Print #vArq, vTexto
    
    While Not pObj.EOF
    
        vTexto = ""
            
        For i = 0 To pObj.Fields.Count - 1
        
            If vTexto = "" Then
                vTexto = pObj.Fields(i) & Chr(9)
            Else
                vTexto = vTexto & pObj.Fields(i) & Chr(9)
            End If
            
        Next
        
        Print #vArq, vTexto
        pObj.MoveNext
        vTexto = ""
        
    Wend
    
    Close #vArq
    
End Sub
'*****************************************************************************
'O Trv de datas dever� ser preenchido com 12 meses, sendo da seguinte maneira:
'6 meses anteriores ao mes atual
'O mes atual
'5 meses ap�s do mes atual
'*****************************************************************************
Sub Preencher_Trv()
    
    Dim vDataInicio As Date
    Dim vDataAtual As Date
    Dim vDataFim As Date
    Dim vData As Date
    Dim No As Node
    Dim i As Integer
    Dim ii As Integer
    Dim iii As Integer
    
    Dim vQtdDiasMes As Byte
    Dim vNumSemana As Byte
    Dim vNumAnos As Byte
    Dim vNumMeses As Byte
    Dim vNumSemanas As Byte
    Dim vNumDias As Integer
    
    frmCadastro.stbCadastro.Panels(1).Text = "Inserindo datas no Treeview... Aguarde..."
    
    'Pegar a data atual menos 6 meses
    vDataInicio = DateAdd("m", -6, frmCadastro.lblDataAtual.Caption)
    
    'Pegar da data atual
    vDataAtual = frmCadastro.lblDataAtual.Caption
    
    'Pegar a data atual mais 6 meses
    vDataFim = DateAdd("m", 6, frmCadastro.lblDataAtual.Caption)
    
    trv.Nodes.Clear
    
    'Criar todos os N�s referentes aos Anos.
    vData = vDataInicio
    For i = Year(vDataInicio) To Year(vDataFim)
        
        vNumAnos = vNumAnos + 1
        
        Set No = trv.Nodes.Add(, , "_" & i, i, "Ano")
        No.Expanded = True
    
        'Preencher Meses
        For ii = Month(vData) To 12
                
            vNumMeses = vNumMeses + 1
            
            Set No = trv.Nodes.Add("_" & i, tvwChild, "_" & i & "_" & ii, Choose(ii, "Janeiro", "Fevereiro", "Mar�o", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"), "Mes")
            If Format(CDate(frmCadastro.lblDataAtual), "YYYYMM") = Format(vData, "YYYYMM") Then
                No.Expanded = True
            End If
            
            vQtdDiasMes = fQtdDiasMes(Year(vData), Month(vData))
            
            For iii = 1 To vQtdDiasMes
                'Preencher Semanas
                If iii = 1 Then
                    vNumSemana = 1
                    vNumSemanas = vNumSemanas + 1
                    Set No = trv.Nodes.Add("_" & i & "_" & ii, tvwChild, "_" & i & "_" & ii & "_SEMANA" & vNumSemana, "Semana" & vNumSemana, "Semana")
                    No.Expanded = True
                ElseIf Weekday(CDate(iii & "/" & ii & "/" & i)) = vbMonday Then
                    vNumSemana = vNumSemana + 1
                    vNumSemanas = vNumSemanas + 1
                    Set No = trv.Nodes.Add("_" & i & "_" & ii, tvwChild, "_" & i & "_" & ii & "_SEMANA" & vNumSemana, "Semana" & vNumSemana, "Semana")
                    No.Expanded = True
                End If
                'Preencher Dias
                Set No = trv.Nodes.Add("_" & i & "_" & ii & "_SEMANA" & vNumSemana, tvwChild, "_" & i & "_" & ii & "_SEMANA" & vNumSemana & "_" & iii, CDate(iii & "/" & ii & "/" & i) & " - " & Replace(Format(CDate(iii & "/" & ii & "/" & i), "Ddd"), "�", "a"), "Dia")
                No.Expanded = True
                vNumDias = vNumDias + 1
            Next
            vData = DateAdd("m", 1, vData)
            If Format(vData, "YYYYMM") > Format(vDataFim, "YYYYMM") Then Exit For
        Next
    
    Next
    
    For i = 1 To trv.Nodes.Count
        If trv.Nodes(i).Image = "Dia" Then
            lblDataInicio.Caption = Left(trv.Nodes(i), 8)
            Exit For
        End If
    Next
    
    For i = trv.Nodes.Count To 1 Step -1
        If trv.Nodes(i).Image = "Dia" Then
            lblDataFim.Caption = Left(trv.Nodes(i), 8)
            Exit For
        End If
    Next
    
    stbCadastro.Panels(1).Text = "Pronto"
    stbCadastro.Panels(2).Text = "N� Anos: " & vNumAnos
    stbCadastro.Panels(3).Text = "N� Meses: " & vNumMeses
    stbCadastro.Panels(4).Text = "N� Semanas: " & vNumSemanas
    stbCadastro.Panels(5).Text = "N� Dias: " & vNumDias
    
End Sub


Private Sub cboCarteira_Change()
    PreencherClientes
End Sub

Private Sub cboCarteira_Click()
    PreencherClientes
End Sub

Private Sub cboPromotor_Click()

    Me.MousePointer = vbHourglass

    Unload frmCopiar

    frmAguardar.Show
    frmAguardar.Refresh

    cmdCopiar.Enabled = cboPromotor <> ""
    
    Preencher_Combo_Carteira
    Preencher_Trv
    PreencherClientes
    Preencher_Datas_Com_Clientes
    Resumo
    Pintar_Parametros
    
    'Inluido por Luciano - Calculo de totais de cliente do dia e do m�s.
    TotalPorSemana
    
    Conectar
        lblTotalClientes.Caption = Me.lsv.ListItems.Count
        lblTotalClientesRoteirizados.Caption = fPegarQtdeClientesRoteirizados(Val(cboPromotor))
        lblTotalClientesNaoRoteirizados.Caption = lblTotalClientes - (Val(lblTotalClientesRoteirizados) + Val(lblTotalClientesSemRoteiro))
        ContarDiasComClientesNoPeriodo
        ContarDiasSemClientesNoPeriodo
        DiaComMaiorQtdeClientesNoPeriodo
        DiaComMenorQtdeClientesNoPeriodo
        lblTotalVisitasPeriodo.Caption = fTotalVisitasPeriodo
        lblTotalComentariosPeriodo.Caption = fTotalComentariosPeriodo
    Desconectar
    
    cboCarteira.Enabled = True
    
    Unload frmAguardar
    'frmCadastro.Refresh
    Me.trv.Refresh
    Me.fraVisitas.Refresh
    Me.MousePointer = vbNormal
    
End Sub



Private Sub cmd1_Click()
    
    Dim vObj As Object
    Dim vObjParam As Object
    Dim i As Integer
    Dim ii As Integer
    Dim lItem As ListItem
    Dim vQtdDia As Long
    Dim vUltNoEncontrado As Long
    
    Me.stbCadastro.Panels(1).Text = "Selecionando... Aguarde..."
    Me.stbCadastro.Refresh
    
    Conectar
    
    Criar_Cursor
    
    vBanco.Parameters.Remove "PM_PROMOTOR"
    vBanco.Parameters.Add "PM_PROMOTOR", Val(cboPromotor), 1
    
    vBanco.Parameters.Remove "PM_INICIO"
    vBanco.Parameters.Add "PM_INICIO", Formata_Data(CDate(Me.cmbDtInicial.Value)), 1
    
    vBanco.Parameters.Remove "PM_FIM"
    vBanco.Parameters.Add "PM_FIM", Formata_Data(CDate(Me.cmbDtFinal.Value)), 1
    
    vBanco.ExecuteSQL "Begin PRODUCAO.PCK_CAD035.PR_SEL_ROTEIRO_IMPRESSAO(:vCursor, :PM_PROMOTOR, :PM_INICIO, :PM_FIM);END;"
    Set vObj = vBanco.Parameters("vCursor").Value
    
    If vObj.EOF Then
    
        MsgBox "Nenhuma informa��o encontrada"
        Exit Sub
    
    End If
    
    Me.stbCadastro.Panels(1).Text = "Gerando arquivo no Excel... Aguarde..."
    Me.stbCadastro.Refresh
    
    Dim vNomeArquivo As String
    
    vNomeArquivo = "C:\Roteiro_" & Format(Now, "HHMMSS") & ".TXT"
    
    Gerar_Arq_Texto vObj, vNomeArquivo, Chr(39), Me.Caption
    Gerar_Excel vNomeArquivo, Chr(39)
    
    Set vObj = Nothing
    
    vBanco.Parameters.Remove "PM_PROMOTOR"
    vBanco.Parameters.Remove "PM_INICIO"
    vBanco.Parameters.Remove "PM_FIM"
    
    Desconectar
    
    pb.Value = 0
    
    Me.stbCadastro.Panels(1).Text = "Pronto"
    Me.stbCadastro.Refresh
    
End Sub
Private Sub cmdAplicarFiltrar_Click()
    
    cmdFiltrar.Caption = "Todos"
    
    PreencherClientes

End Sub

Private Sub cmdAtribuir_Click()
    
    On Error GoTo Trata_Erro
    
    Dim i As Integer
    Dim ii As Integer
    Dim iii As Integer
    Dim vKey
    Dim vInserido As Boolean
    
    Conectar
    
    If lsv.ListItems.Count > 0 Then
        
        For i = 1 To lsv.ListItems.Count
        
            'verifica quais promotores estao seleconados
            If lsv.ListItems(i).Checked = True Then
            
                For ii = 1 To trv.Nodes.Count
                    
                    'verifica a chave selecionada para inserir o promotor selecionado
                    If trv.Nodes(ii).Checked = True And trv.Nodes(ii).Image = "Dia" Then
                        
                        vKey = Split(trv.Nodes(ii).Key, "_")
                                                                       
                        'Marcel - 14/11/08
                        'compara ano, n�o deixando fazer agendamenos para anos anteriores
                        'verifica se o ano do agendamento � maior que o atual,
                        'caso for add a data agendada sem verificar o m�s..
                        If Val(vKey(1)) > Year(CDate(lblDataAtual.Caption)) Then
                            
                            vInserido = True
                                
                            InserirCliente Val(cboPromotor), lsv.ListItems(i).SubItems(1), CStr(Mid(trv.Nodes(ii).Text, 1, 8))
                    
                            trv.Nodes.Add trv.Nodes(ii).Key, tvwChild, trv.Nodes(ii).Key & "_" & Format(lsv.ListItems(i).SubItems(1), "000000"), Format(lsv.ListItems(i).SubItems(1), "000000") & " - " & lsv.ListItems(i).SubItems(2), "Cliente"
                                                      
                        Else 'ano � igual o atual ent�o compara-se apenas o m�s
                        
                            'compara m�s, n�o deixando fazer agendamenos para m�ses anteriores
                            If Val(vKey(2)) >= Month(CDate(lblDataAtual.Caption)) Then
                                
                                vInserido = True
                                
                                InserirCliente Val(cboPromotor), lsv.ListItems(i).SubItems(1), CStr(Mid(trv.Nodes(ii).Text, 1, 8))
                        
                                trv.Nodes.Add trv.Nodes(ii).Key, tvwChild, trv.Nodes(ii).Key & "_" & Format(lsv.ListItems(i).SubItems(1), "000000"), Format(lsv.ListItems(i).SubItems(1), "000000") & " - " & lsv.ListItems(i).SubItems(2), "Cliente"
                                                    
                            End If
                            
                        End If
                                                
                    End If
                
                Next
                
            End If
        
        Next
        
    End If
    
    Desconectar
    
    If vInserido = True Then
    
        PreencherClientes
    
        Pintar_Parametros
        
        Resumo
        
        Conectar
            
            lblTotalClientes.Caption = Me.lsv.ListItems.Count
            
            lblTotalClientesRoteirizados.Caption = fPegarQtdeClientesRoteirizados(Val(cboPromotor))
        
            lblTotalClientesNaoRoteirizados.Caption = lblTotalClientes - (Val(lblTotalClientesRoteirizados) + Val(lblTotalClientesSemRoteiro))
        
            ContarDiasComClientesNoPeriodo
            
            ContarDiasSemClientesNoPeriodo
            
            DiaComMaiorQtdeClientesNoPeriodo
            
            DiaComMenorQtdeClientesNoPeriodo
            
            lblTotalVisitasPeriodo.Caption = fTotalVisitasPeriodo
            
            lblTotalComentariosPeriodo.Caption = fTotalComentariosPeriodo
        
        Desconectar
    
    End If
    
Trata_Erro:
    If Err.Number = 35602 Then
        Resume Next
    ElseIf Err.Number <> 0 Then
        MsgBox "Sub cmdAtribuir_click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If
End Sub

Private Sub cmdAumentar_Click()
    If lsv.Width > 14000 Then Exit Sub
    lsv.Width = Me.lsv.Width + 500
    
End Sub

Private Sub cmdCalcular_Click()
    
    Me.MousePointer = vbHourglass

    If frmCopiar.Visible = True Then frmCopiar.Visible = False

    frmAguardar.Show
    frmAguardar.Refresh

    cmdCopiar.Enabled = cboPromotor <> ""
    
    Preencher_Trv

    Preencher_Datas_Com_Clientes
    
    Pintar_Parametros
    
    Resumo
    
    TotalPorSemana
    
    Contar_Visitas
    
    Unload frmAguardar
    
    Me.MousePointer = vbNormal
    
End Sub


Private Sub cmdCopiar_Click()
    frmCopiar.Show 1
End Sub



Private Sub cmdDiminuir_Click()
    If lsv.Width > 7935 Then
       lsv.Width = Me.lsv.Width - 500
    Else
       lsv.Width = 7935
    End If
End Sub

Private Sub cmdExcluirCliente_Click()
    
    If trv.SelectedItem Is Nothing Then Exit Sub
    
    If fContarCaracteresTraco(Me.trv.SelectedItem.Key) = 5 Then

        If MsgBox("Confirma a exclus�o do cliente selecionado ?", vbQuestion + vbYesNo, "Aten��o") = vbYes Then
    
            stbCadastro.Panels(1).Text = "Excluindo cliente... Aguarde..."
            stbCadastro.Refresh
    
            Conectar
            
            vBanco.Parameters.Remove "PM_CLIENTE"
            vBanco.Parameters.Add "PM_CLIENTE", Val(trv.SelectedItem.Text), 1
            
            vBanco.Parameters.Remove "PM_PROMOTOR"
            vBanco.Parameters.Add "PM_PROMOTOR", Val(cboPromotor), 1
            
            vBanco.Parameters.Remove "PM_DATA"
            vBanco.Parameters.Add "PM_DATA", Formata_Data(Trim(Mid(trv.SelectedItem.Parent, 1, InStr(1, trv.SelectedItem.Parent, "-") - 1))), 1
    
            vBanco.Parameters.Remove "PM_DATA_FIM"
            vBanco.Parameters.Add "PM_DATA_FIM", Formata_Data(Trim(Mid(trv.SelectedItem.Parent, 1, InStr(1, trv.SelectedItem.Parent, "-") - 1))), 1
    
            vBanco.Parameters.Remove "PM_TIPO"
            vBanco.Parameters.Add "PM_TIPO", 1, 1
            
            vBanco.ExecuteSQL "Begin PRODUCAO.PCK_CAD035.PR_DELETE_CLIENTE_TRV(:PM_CLIENTE, :PM_PROMOTOR, :PM_DATA, :PM_DATA_FIM, :PM_TIPO);END;"
    
            Me.stbCadastro.Panels(1).Text = "Pronto"
            Me.stbCadastro.Refresh
    
            vBanco.Parameters.Remove "PM_CLIENTE"
            vBanco.Parameters.Remove "PM_PROMOTOR"
            vBanco.Parameters.Remove "PM_DATA"
            vBanco.Parameters.Remove "PM_DATA_FIM"
            vBanco.Parameters.Remove "PM_TIPO"
    
            Desconectar
    
            trv.Nodes.Remove trv.SelectedItem.Key
            
            trv.SelectedItem.Parent.Text = Trim(Left(trv.SelectedItem.Parent.Text, 14)) & " - " & trv.SelectedItem.Parent.Children
            
            TotalPorSemana
            
            Resumo
            
        End If
    End If
End Sub


Private Sub cmdExcluirClientesDia_Click()
    
    Dim vKey As String
    Dim i As Integer
    
    If trv.SelectedItem Is Nothing Then Exit Sub
    
    If fContarCaracteresTraco(Me.trv.SelectedItem.Key) = 4 Then

        If MsgBox("Confirma a exclus�o de todos os clientes do dia " & Mid(trv.SelectedItem.Text, 1, 8) & "?", vbQuestion + vbYesNo, "Aten��o") = vbYes Then
    
            stbCadastro.Panels(1).Text = "Excluindo clientes... Aguarde..."
            stbCadastro.Refresh
    
            Conectar
            
            vBanco.Parameters.Remove "PM_CLIENTE"
            vBanco.Parameters.Add "PM_CLIENTE", 0, 1
            
            vBanco.Parameters.Remove "PM_PROMOTOR"
            vBanco.Parameters.Add "PM_PROMOTOR", Val(cboPromotor), 1
            
            vBanco.Parameters.Remove "PM_DATA"
            vBanco.Parameters.Add "PM_DATA", Formata_Data(Trim(Mid(trv.SelectedItem, 1, InStr(1, trv.SelectedItem, "-") - 1))), 1
    
            vBanco.Parameters.Remove "PM_DATA_FIM"
            vBanco.Parameters.Add "PM_DATA_FIM", Formata_Data(Trim(Mid(trv.SelectedItem, 1, InStr(1, trv.SelectedItem, "-") - 1))), 1
    
            vBanco.Parameters.Remove "PM_TIPO"
            vBanco.Parameters.Add "PM_TIPO", 2, 1
            
            vBanco.ExecuteSQL "Begin PRODUCAO.PCK_CAD035.PR_DELETE_CLIENTE_TRV(:PM_CLIENTE, :PM_PROMOTOR, :PM_DATA, :PM_DATA_FIM, :PM_TIPO);END;"
    
            Me.stbCadastro.Panels(1).Text = "Pronto"
            Me.stbCadastro.Refresh
    
            vBanco.Parameters.Remove "PM_CLIENTE"
            vBanco.Parameters.Remove "PM_PROMOTOR"
            vBanco.Parameters.Remove "PM_DATA"
            vBanco.Parameters.Remove "PM_DATA_FIM"
            vBanco.Parameters.Remove "PM_TIPO"
            
            Desconectar
    
            vKey = trv.SelectedItem.Key
    
            For i = 1 To trv.Nodes.Count
                If i >= trv.Nodes.Count Then Exit For
                If Left(trv.Nodes(i).Key, Len(vKey)) = vKey And trv.Nodes(i).Image = "Cliente" Then
                    trv.Nodes.Remove trv.Nodes(i).Key
                    i = i - 1
                End If
            Next
            
            trv.SelectedItem.Text = Trim(Mid(trv.SelectedItem.Text, 1, 14))
            
            TotalPorSemana
            
            Resumo
            
        End If
    End If
End Sub


Private Sub cmdExcluirClientesSelec_Click()
       
1         On Error GoTo Trata_Erro
       
2         If trv.SelectedItem Is Nothing Then Exit Sub
          
          Dim i As Integer
          Dim vSeq As Integer
          
3         stbCadastro.Panels(1).Text = "Excluindo clientes selecionados... Aguarde..."
4         stbCadastro.Refresh
             
5         Conectar
          
6         vSeq = 1
          
7         For vSeq = 1 To Me.trv.Nodes.Count
              
8             If vSeq > trv.Nodes.Count Then Exit For
              
9             If trv.Nodes(vSeq).Checked = True And trv.Nodes(vSeq).Image = "Cliente" Then
              
10                vBanco.Parameters.Remove "PM_CLIENTE"
11                vBanco.Parameters.Add "PM_CLIENTE", Val(trv.Nodes(vSeq)), 1
                  
12                vBanco.Parameters.Remove "PM_PROMOTOR"
13                vBanco.Parameters.Add "PM_PROMOTOR", Val(cboPromotor), 1
                  
14                vBanco.Parameters.Remove "PM_DATA"
15                vBanco.Parameters.Add "PM_DATA", Formata_Data(Trim(Mid(trv.Nodes(vSeq).Parent, 1, InStr(1, trv.Nodes(vSeq).Parent, "-") - 1))), 1
          
16                vBanco.Parameters.Remove "PM_DATA_FIM"
17                vBanco.Parameters.Add "PM_DATA_FIM", Formata_Data(Trim(Mid(trv.Nodes(vSeq).Parent, 1, InStr(1, trv.Nodes(vSeq).Parent, "-") - 1))), 1
          
18                vBanco.Parameters.Remove "PM_TIPO"
19                vBanco.Parameters.Add "PM_TIPO", 1, 1
                  
20                vBanco.ExecuteSQL "Begin PRODUCAO.PCK_CAD035.PR_DELETE_CLIENTE_TRV(:PM_CLIENTE, :PM_PROMOTOR, :PM_DATA, :PM_DATA_FIM, :PM_TIPO);END;"
          
                  'trv.Nodes.Remove trv.Nodes(vSeq).Key
                  
                  'trv.Nodes(vSeq).Parent.Text = Trim(Left(trv.Nodes(vSeq).Parent.Text, 14)) & " - " & trv.Nodes(vSeq).Parent.Children
                  
21            End If
              
22        Next
          
23        Me.stbCadastro.Panels(1).Text = "Pronto"
24        Me.stbCadastro.Refresh

25        vBanco.Parameters.Remove "PM_CLIENTE"
26        vBanco.Parameters.Remove "PM_PROMOTOR"
27        vBanco.Parameters.Remove "PM_DATA"
28        vBanco.Parameters.Remove "PM_DATA_FIM"
29        vBanco.Parameters.Remove "PM_TIPO"

30        Desconectar
          
31        Preencher_Trv
          
32        Preencher_Datas_Com_Clientes
            
33        Pintar_Parametros
          
34        Resumo
          
35        TotalPorSemana
Trata_Erro:
36        If Err.Number <> 0 Then
37      MsgBox "Sub cmdExcluirClientesSelec_Click" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
38        End If
End Sub

Private Sub cmdExcluirClientesSemana_Click()
    Dim vKey As String
    Dim i As Integer
    
    If fContarCaracteresTraco(Me.trv.SelectedItem.Key) = 3 Then

        If MsgBox("Confirma a exclus�o de todos os clientes da " & trv.SelectedItem.Text & "?", vbQuestion + vbYesNo, "Aten��o") = vbYes Then
    
            stbCadastro.Panels(1).Text = "Excluindo clientes... Aguarde..."
            stbCadastro.Refresh
    
            Conectar
            
            vBanco.Parameters.Remove "PM_CLIENTE"
            vBanco.Parameters.Add "PM_CLIENTE", 0, 1
            
            vBanco.Parameters.Remove "PM_PROMOTOR"
            vBanco.Parameters.Add "PM_PROMOTOR", Val(cboPromotor), 1
            
            vBanco.Parameters.Remove "PM_DATA"
            vBanco.Parameters.Add "PM_DATA", Formata_Data(Trim(Mid(trv.SelectedItem.Child.FirstSibling, 1, InStr(1, trv.SelectedItem.Child.FirstSibling, "-") - 1))), 1
    
            vBanco.Parameters.Remove "PM_DATA_FIM"
            vBanco.Parameters.Add "PM_DATA_FIM", Formata_Data(Trim(Mid(trv.SelectedItem.Child.LastSibling, 1, InStr(1, trv.SelectedItem.Child.LastSibling, "-") - 1))), 1
    
            vBanco.Parameters.Remove "PM_TIPO"
            vBanco.Parameters.Add "PM_TIPO", 3, 1
            
            vBanco.ExecuteSQL "Begin PRODUCAO.PCK_CAD035.PR_DELETE_CLIENTE_TRV(:PM_CLIENTE, :PM_PROMOTOR, :PM_DATA, :PM_DATA_FIM, :PM_TIPO);END;"
    
            Me.stbCadastro.Panels(1).Text = "Pronto"
            Me.stbCadastro.Refresh
    
            vBanco.Parameters.Remove "PM_CLIENTE"
            vBanco.Parameters.Remove "PM_PROMOTOR"
            vBanco.Parameters.Remove "PM_DATA"
            vBanco.Parameters.Remove "PM_DATA_FIM"
            vBanco.Parameters.Remove "PM_TIPO"
    
            Desconectar
            
            vKey = trv.SelectedItem.Key
    
            For i = 1 To trv.Nodes.Count
                If i >= trv.Nodes.Count Then Exit For
                If Left(trv.Nodes(i).Key, Len(vKey)) = vKey And trv.Nodes(i).Image = "Cliente" Then
                    trv.Nodes.Remove trv.Nodes(i).Key
                    i = i - 1
                End If
            Next
            TotalPorSemana
            
            Resumo
        End If
    End If
End Sub


Private Sub cmdExpandir_Click()
    Dim i As Integer
    For i = 1 To trv.Nodes.Count
        If trv.Nodes(i).Children > 0 Then
            trv.Nodes(i).Expanded = True
        End If
    Next
End Sub

Private Sub cmdFiltrar_Click()
    
    If cboPromotor = "" Then Exit Sub
    
    If UCase(cmdFiltrar.Caption) = "TODOS" Then
        cmdFiltrar.Caption = "Filtrar"
        txtFiltrar = ""
        PreencherClientes
    Else
        Me.fraFiltrar.Visible = True
    End If
End Sub

Private Sub cmdFinalizarPorNome_Click()
   
    Dim i As Integer
    Dim vReg As Integer
    
    vReg = Val(txtLocalizarPorNome.Tag) + 1
    
    For i = vReg To lsv.ListItems.Count
    
        If InStr(1, lsv.ListItems(i).SubItems(2), UCase(txtLocalizarPorNome)) > 0 Then
            lsv.ListItems(i).Selected = True
            lsv.ListItems(i).EnsureVisible
            lsv.SetFocus
            txtLocalizarPorNome.Tag = i
            Exit For
        End If
        
    Next
   
End Sub

Private Sub cmdGravarComentario_Click()

    If Trim(txtComentario) = "" Then Exit Sub

    Conectar

    vBanco.Parameters.Remove "PM_PROMOTOR"
    vBanco.Parameters.Add "PM_PROMOTOR", Val(cboPromotor), 1
    
    vBanco.Parameters.Remove "PM_CLIENTE"
    vBanco.Parameters.Add "PM_CLIENTE", Val(trv.SelectedItem), 1
    
    vBanco.Parameters.Remove "PM_DATA"
    vBanco.Parameters.Add "PM_DATA", Formata_Data(CDate(Mid(trv.SelectedItem.Parent, 1, 8))), 1
    
    vBanco.Parameters.Remove "PM_COMENTARIO"
    vBanco.Parameters.Add "PM_COMENTARIO", txtComentario, 1
    
    vBanco.ExecuteSQL "Begin PRODUCAO.PCK_CAD035.PR_UPD_COMENTARIO(:PM_PROMOTOR, :PM_CLIENTE, :PM_DATA, :PM_COMENTARIO);END;"

    vBanco.Parameters.Remove "PM_PROMOTOR"
    vBanco.Parameters.Remove "PM_CLIENTE"
    vBanco.Parameters.Remove "PM_DATA"
    vBanco.Parameters.Remove "PM_COMENTARIO"
    
    Desconectar

End Sub

Private Sub cmdImprimir_Click()
    
    If cboPromotor.ListIndex = -1 Then
    
        MsgBox "Escolha o promotor"
        Exit Sub
    
    End If
    
    cmbDtInicial.Value = Now
    cmbDtFinal.Value = Now + 1
    fraImprimir.Visible = True

End Sub
Private Sub cmdIrParaMesAtual_Click()
    Dim i As Integer
    cmdRetrair_Click
    For i = 1 To Me.trv.Nodes.Count
        If trv.Nodes(i).Text = Format(Me.lblDataAtual.Caption, "YYYY") Then
            trv.Nodes(i).Expanded = True
        Else
            If UCase(trv.Nodes(i).Text) = UCase(Format(Me.lblDataAtual.Caption, "MMMM")) Then
                trv.Nodes(i).Expanded = True
                Me.trv.Nodes(i).EnsureVisible
                Exit For
            End If
        End If
    Next
End Sub


Private Sub cmdLocalizar_Click()

    Dim i As Integer
    
    For i = 1 To Me.lsv.ListItems.Count
    
        If InStr(1, lsv.ListItems(i).SubItems(1), UCase(txtLocalizarPorCodigo)) > 0 Then
            lsv.ListItems(i).Selected = True
            lsv.ListItems(i).EnsureVisible
            lsv.SetFocus
            txtLocalizarPorCodigo.Tag = i
            Exit For
        End If
        
    Next
End Sub

Private Sub cmdOrdenar_Click()
    If Me.lsv.ListItems.Count = 0 Then Exit Sub
    Me.fraOrder.Visible = True
End Sub

Private Sub cmdPesquisar_Click()
    fraPesq.Visible = True
    Me.txtLocalizarPorNome.Tag = 0
End Sub


Private Sub cmdRetrair_Click()
    Dim i As Integer
    For i = 1 To trv.Nodes.Count
        If trv.Nodes(i).Children > 0 Then
            trv.Nodes(i).Expanded = False
        End If
    Next
End Sub

Private Sub cmdSelCliente_Click()
    If Me.lsv.ListItems.Count = 0 Then Exit Sub
    lsv.SelectedItem.Checked = True
End Sub

Private Sub cmdVoltar_Click()
    Desconectar
    Unload Me
End Sub


Private Sub Form_Load()
    Me.Top = (mdiRoteiro.Height - Me.Height) / 2 - mdiRoteiro.mnuXP.Height
    Me.Left = (mdiRoteiro.Width - Me.Width) / 2

    lblDataAtual = fPegarDataAtual
    
    Preencher_Cbo_Promotor frmCadastro

    cmdCopiar.Enabled = False
    
End Sub


Private Sub Form_Paint()
    If frmCadastro.Tag = "1" Then
        frmCadastro.Tag = ""
        cboPromotor_Click
    End If
    
End Sub

Private Sub Image1_Click()
    fraOrder.Visible = False
    
    If cboOrder1 = "" Then
        MsgBox "Informe o Primeiro campo para Ordernar.", vbInformation, "Aten��o"
        Me.cboOrder1.SetFocus
        Exit Sub
    End If
    
    If cboOrder2 = "" And (cboOrder3 <> "" Or cboOrder4 <> "") Then
        MsgBox "Campos de ordena��o preenchidos incorretamente.", vbInformation, "Aten��o"
        Exit Sub
    End If
    
    PreencherClientes
End Sub

Private Sub Image2_Click()
    txtFiltrar = ""
    fraImprimir.Visible = False
End Sub
Private Sub imgFechar_Click()
    cboOrder1 = ""
    cboOrder2 = ""
    cboOrder3 = ""
    cboOrder4 = ""
    Me.fraOrder.Visible = False
End Sub


Private Sub imgFiltrarFechar_Click()
    txtFiltrar = ""
    fraFiltrar.Visible = False
End Sub

Private Sub ImgSair_Click()
    Me.fraPesq.Visible = False
End Sub

Private Sub lblAbaixoDoMinimo_Click()
    Fora_Do_Padrao "ABAIXO"
End Sub

Private Sub lblAcimaDoMaximo_Click()
    Fora_Do_Padrao "ACIMA"
End Sub

Private Sub lblDiaComMaiorNumCliente_Click()
    If lblDiaComMaiorNumCliente.Caption = "" Then Exit Sub
    
    Dim i As Integer
    For i = 1 To trv.Nodes.Count
    
        If Trim(Mid(trv.Nodes(i).Text, 1, InStr(1, lblDiaComMaiorNumCliente.Caption, "-") - 1)) = Trim(Mid(lblDiaComMaiorNumCliente.Caption, 1, InStr(1, lblDiaComMaiorNumCliente.Caption, "-") - 1)) Then
            trv.SetFocus
            trv.Nodes(i).Selected = True
            trv.Nodes(i).Expanded = True
            trv.Nodes(i).EnsureVisible
            Exit For
        End If
        
    Next
End Sub

Private Sub lblDiaComMenorNumCliente_Click()
    
    If lblDiaComMenorNumCliente.Caption = "" Then Exit Sub
    
    Dim i As Integer
    For i = 1 To trv.Nodes.Count
    
        If Trim(Mid(trv.Nodes(i).Text, 1, InStr(1, lblDiaComMenorNumCliente.Caption, "-") - 1)) = Trim(Mid(lblDiaComMenorNumCliente.Caption, 1, InStr(1, lblDiaComMenorNumCliente.Caption, "-") - 1)) Then
            trv.SetFocus
            trv.Nodes(i).Selected = True
            trv.Nodes(i).Expanded = True
            trv.Nodes(i).EnsureVisible
            Exit For
        End If
        
    Next
    
End Sub


Private Sub lblQtdDiasSemClientes_Click()
    
    Dim vObj As Object
    Dim vContarSabado As Boolean
    Dim vContarDomingo As Boolean
    Dim i As Integer
    Dim vDias(30) As String
    Dim vContador As Byte
    
    If Val(lblQtdDiasSemClientes.Caption) = 0 Then Exit Sub
    
    vContarSabado = IIf(Val(fPegarVlrParam("CHK_CONTAR_SABADO")) = 1, True, False)
    vContarDomingo = IIf(Val(fPegarVlrParam("CHK_CONTAR_DOMINGO")) = 1, True, False)
    
    vContador = 0
    For i = 1 To trv.Nodes.Count
    
        If trv.Nodes(i).Image = "Dia" And trv.Nodes(i).Children = 0 Then
            If Format(CDate(Left(trv.Nodes(i).Text, 8)), "YYYYMM") = Format(CDate(Me.lblDataAtual.Caption), "YYYYMM") Then
                
                If UCase(Right(trv.Nodes(i), 3)) = "SAB" And vContarSabado = True Then
                    If trv.Nodes(i).Children = 0 Then
                        'trv.SetFocus
                        'trv.Nodes(i).Selected = True
                        'trv.Nodes(i).Expanded = True
                        'trv.Nodes(i).EnsureVisible
                        'Exit For
                        vDias(vContador) = Mid(trv.Nodes(i), 1, 8)
                        vContador = vContador + 1
                    End If
                ElseIf UCase(Right(trv.Nodes(i), 3)) = "DOM" And vContarDomingo = True Then
                    If trv.Nodes(i).Children = 0 Then
                        'trv.SetFocus
                        'trv.Nodes(i).Selected = True
                        'trv.Nodes(i).Expanded = True
                        'trv.Nodes(i).EnsureVisible
                        'Exit For
                        vDias(vContador) = Mid(trv.Nodes(i), 1, 8)
                        vContador = vContador + 1
                    End If
                ElseIf UCase(Right(trv.Nodes(i), 3)) <> "SAB" And UCase(Right(trv.Nodes(i), 3)) <> "DOM" Then
                    If trv.Nodes(i).Children = 0 Then
                        'trv.SetFocus
                        'trv.Nodes(i).Selected = True
                        'trv.Nodes(i).Expanded = True
                        'trv.Nodes(i).EnsureVisible
                        'Exit For
                        vDias(vContador) = Mid(trv.Nodes(i), 1, 8)
                        vContador = vContador + 1
                    End If
                End If
            End If
        End If
        
    Next
    
    For i = 0 To 29
        
        If vDias(i) = "" Then Exit For
        
        frmDiasSemClientes.lstDatas.AddItem vDias(i)
    
    Next
    
    frmDiasSemClientes.Show 1
End Sub

Private Sub lblTotalClientesNaoRoteirizados_Click()
    Dim lItem As ListItem
    Dim i As Integer
    
    frmDetalharCliente.Caption = ".:Clientes Sem Roteiro no Per�odo:."
    
    frmDetalharCliente.Label1.Caption = "Clientes Sem Roteiro no Per�odo"
    
    frmDetalharCliente.lsv.ListItems.Clear
    
    For i = 1 To lsv.ListItems.Count
        
        'If Val(lsv.ListItems(i).SubItems(3)) = 0 Then
        If Val(lsv.ListItems(i).SubItems(3)) = 0 And lsv.ListItems(i).SubItems(13) <> "" Then
        
            Set lItem = frmDetalharCliente.lsv.ListItems.Add
            
            lItem.SubItems(1) = lsv.ListItems(i).SubItems(1)
            lItem.SubItems(2) = lsv.ListItems(i).SubItems(2)
            lItem.SubItems(3) = lsv.ListItems(i).SubItems(3)
            lItem.SubItems(4) = lsv.ListItems(i).SubItems(4)
            lItem.SubItems(5) = lsv.ListItems(i).SubItems(5)
            lItem.SubItems(6) = lsv.ListItems(i).SubItems(6)
            lItem.SubItems(7) = lsv.ListItems(i).SubItems(7)
            lItem.SubItems(8) = lsv.ListItems(i).SubItems(8)
            lItem.SubItems(9) = lsv.ListItems(i).SubItems(9)
            lItem.SubItems(10) = lsv.ListItems(i).SubItems(10)
            lItem.SubItems(11) = lsv.ListItems(i).SubItems(11)
            lItem.SubItems(12) = lsv.ListItems(i).SubItems(12)
            lItem.SubItems(13) = lsv.ListItems(i).SubItems(13)
            lItem.SubItems(14) = lsv.ListItems(i).SubItems(14)
        
        End If
        
    Next
        
    frmDetalharCliente.Stb.Panels(1).Text = "Qtde de Registros: " & frmDetalharCliente.lsv.ListItems.Count
    
    frmDetalharCliente.Visible = True
    
End Sub

Private Sub lblTotalClientesRoteirizados_Click()
    Dim lItem As ListItem
    Dim i As Integer
    
    frmDetalharCliente.Caption = ".:Clientes Roteirizados no Per�odo:."
    
    frmDetalharCliente.Label1.Caption = "Clientes Roteirizados no Per�odo"
    
    frmDetalharCliente.lsv.ListItems.Clear
    
    For i = 1 To lsv.ListItems.Count
        
        If Val(lsv.ListItems(i).SubItems(3)) > 0 Then
        
            Set lItem = frmDetalharCliente.lsv.ListItems.Add
            
            lItem.SubItems(1) = lsv.ListItems(i).SubItems(1)
            lItem.SubItems(2) = lsv.ListItems(i).SubItems(2)
            lItem.SubItems(3) = lsv.ListItems(i).SubItems(3)
            lItem.SubItems(4) = lsv.ListItems(i).SubItems(4)
            lItem.SubItems(5) = lsv.ListItems(i).SubItems(5)
            lItem.SubItems(6) = lsv.ListItems(i).SubItems(6)
            lItem.SubItems(7) = lsv.ListItems(i).SubItems(7)
            lItem.SubItems(8) = lsv.ListItems(i).SubItems(8)
            lItem.SubItems(9) = lsv.ListItems(i).SubItems(9)
            lItem.SubItems(10) = lsv.ListItems(i).SubItems(10)
            lItem.SubItems(11) = lsv.ListItems(i).SubItems(11)
            lItem.SubItems(12) = lsv.ListItems(i).SubItems(12)
            lItem.SubItems(13) = lsv.ListItems(i).SubItems(13)
            lItem.SubItems(14) = lsv.ListItems(i).SubItems(14)
        
        End If
        
    Next
        
    frmDetalharCliente.Stb.Panels(1).Text = "Qtde de Registros: " & frmDetalharCliente.lsv.ListItems.Count
    
    frmDetalharCliente.Visible = True
    
End Sub

Private Sub lblTotalClientesSemRoteiro_Click()
    
    Dim lItem As ListItem
    Dim i As Integer
    
    frmDetalharCliente.Caption = ".:Clientes Novos Sem Roteiro no Per�odo:."
    
    frmDetalharCliente.Label1.Caption = "Clientes Novos Sem Roteiro no Per�odo"
    
    frmDetalharCliente.lsv.ListItems.Clear
    
    For i = 1 To lsv.ListItems.Count
        
        If Val(lsv.ListItems(i).SubItems(3)) = 0 And lsv.ListItems(i).SubItems(13) = "" Then
        
            Set lItem = frmDetalharCliente.lsv.ListItems.Add
            
            lItem.SubItems(1) = lsv.ListItems(i).SubItems(1)
            lItem.SubItems(2) = lsv.ListItems(i).SubItems(2)
            lItem.SubItems(3) = lsv.ListItems(i).SubItems(3)
            lItem.SubItems(4) = lsv.ListItems(i).SubItems(4)
            lItem.SubItems(5) = lsv.ListItems(i).SubItems(5)
            lItem.SubItems(6) = lsv.ListItems(i).SubItems(6)
            lItem.SubItems(7) = lsv.ListItems(i).SubItems(7)
            lItem.SubItems(8) = lsv.ListItems(i).SubItems(8)
            lItem.SubItems(9) = lsv.ListItems(i).SubItems(9)
            lItem.SubItems(10) = lsv.ListItems(i).SubItems(10)
            lItem.SubItems(11) = lsv.ListItems(i).SubItems(11)
            lItem.SubItems(12) = lsv.ListItems(i).SubItems(12)
            lItem.SubItems(13) = lsv.ListItems(i).SubItems(13)
            lItem.SubItems(14) = lsv.ListItems(i).SubItems(14)
        
        End If
        
    Next
        
    frmDetalharCliente.Stb.Panels(1).Text = "Qtde de Registros: " & frmDetalharCliente.lsv.ListItems.Count
    
    frmDetalharCliente.Visible = True

End Sub

Private Sub lblTotalComentariosPeriodo_Click()
    
    Dim lItem As ListItem
    Dim vObj As Object
    Dim i As Integer
    
    frmDetalharComentarios.Caption = ".:Coment�rios no Per�odo:."
    
    frmDetalharComentarios.Label1.Caption = "Coment�rios do M�s Atual"
    
    frmDetalharComentarios.lsv.ListItems.Clear
    
    Conectar
    
    Criar_Cursor
    
    vBanco.Parameters.Remove "PM_PROMOTOR"
    vBanco.Parameters.Add "PM_PROMOTOR", Val(Me.cboPromotor), 1
    
    vBanco.Parameters.Remove "PM_ANO_MES"
    vBanco.Parameters.Add "PM_ANO_MES", Format(CDate(lblDataAtual.Caption), "YYYYMM"), 1
    
    vBanco.ExecuteSQL "Begin PRODUCAO.PCK_CAD035.PR_SEL_COMENTARIO_TODOS(:vCursor, :PM_PROMOTOR, :PM_ANO_MES);END;"
    Set vObj = vBanco.Parameters("vCursor").Value
    
    vBanco.Parameters.Remove "PM_PROMOTOR"
    vBanco.Parameters.Remove "PM_ANO_MES"
    
    Desconectar
    
    For i = 1 To vObj.RecordCount
        
        Set lItem = frmDetalharComentarios.lsv.ListItems.Add
        
        lItem = vObj!Cod_Cliente
        lItem.SubItems(1) = vObj!nome_cliente
        lItem.SubItems(2) = vObj!DT_VISITA
        lItem.SubItems(3) = "" & vObj!COMENTARIOS
        
        vObj.MoveNext
        
    Next
        
    frmDetalharComentarios.Stb.Panels(1).Text = "Qtde de Registros: " & frmDetalharComentarios.lsv.ListItems.Count
    
    frmDetalharComentarios.Show 1
    
End Sub

Private Sub lsv_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    
    Dim vClas As String
    
    If Me.lsv.SortOrder = lvwAscending Then
        Me.lsv.SortOrder = lvwDescending
        vClas = ""
    Else
        Me.lsv.SortOrder = lvwAscending
        vClas = "DESC"
    End If
    
    Select Case UCase(ColumnHeader.Text)
        Case "C�DIGO"
            Call PreencherClientes("COD_CLIENTE " & vClas)
        Case "CEP"
            Call PreencherClientes("CEP " & vClas)
        Case "NOME DO CLIENTE"
            Call PreencherClientes("C.NOME_CLIENTE " & vClas)
        Case "N� VISITAS"
            Call PreencherClientes("QTD_VISITAS " & vClas)
        'Case "CARTEIRA"
        Case "CIDADE"
            Call PreencherClientes("D.NOME_CIDADE " & vClas)
        Case "BAIRRO"
            Call PreencherClientes("C.BAIRRO " & vClas)
        Case "UF"
            Call PreencherClientes("D.COD_UF " & vClas)
        Case "ENDERECO"
            Call PreencherClientes("C.ENDERECO " & vClas)
        'Case "CEP"
        Case "QUADRANTE"
            Call PreencherClientes("E.COD_QUADRANTE " & vClas)
        Case "TIPO DE CLIENTE"
            Call PreencherClientes("G.DESC_TIPO_CLI " & vClas)
        Case "SEGMENTO"
            Call PreencherClientes("H.DESC_SEGMENTO " & vClas)
        Case "DT ULT COMPRA"
            Call PreencherClientes("I.DT_ULT_COMPRA " & vClas)
        Case "DT ULT COMPRA INDENT"
            Call PreencherClientes("I.DT_ULT_COMPRA_INDENT " & vClas)
    End Select
    
    'If ColumnHeader.Text = "C�digo" Or ColumnHeader.Text = "N� Visitas" Then
    '    SortLvwOnLong Me.lsv, ColumnHeader.Index
    'ElseIf InStr(1, ColumnHeader.Text, "Dt") > 0 Then
    '    SortLvwOnDate Me.lsv, ColumnHeader.Index
    'Else
    '    lsv.SortKey = ColumnHeader.Index - 1
    '    If lsv.SortOrder = lvwAscending Then
    '        lsv.SortOrder = lvwDescending
    '    Else
    '        lsv.SortOrder = lvwAscending
    '    End If
    '    lsv.Sorted = True
    'End If
    
End Sub

Private Sub lsv_DblClick()
    frmDatasAgendadas.Show 1
End Sub

Function fQtdDiasMes(pAno As Integer, pMes As Byte) As Byte
    fQtdDiasMes = DatePart("d", DateSerial(pAno, pMes + 1, 0))
End Function

Private Sub trv_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    On Error Resume Next
    Dim nodNode As Node
  
    '// get the node we are over
    Set nodNode = trv.HitTest(x, y)
    
    If nodNode Is Nothing Or trv.SelectedItem.Image <> "Cliente" Then
        Exit Sub '// no node
    End If
    
    'if nodnode.Key
    '// ensure node is actually selected, just incase we start dragging.
    nodNode.Selected = True
    
End Sub

Private Sub trv_NodeCheck(ByVal Node As MSComctlLib.Node)
    If Node.Image = "Cliente" Then
        cmdExcluirClientesSelec.Enabled = True
    End If
End Sub

Private Sub trv_NodeClick(ByVal Node As MSComctlLib.Node)
    'Verificar quantos caracteres UnderScore existem na string.
'    Dim vQtdTracos As Byte
'    vQtdTracos = fContarCaracteresTraco(Node.Key)

    Dim vKey

    'Selecionar comentarios do cliente
    Selecionar_Comentario
    
    vKey = Split(trv.SelectedItem.Key, "_")
    
    If UBound(vKey) <= 1 Then Exit Sub
    
    If Val(vKey(1) & Format(vKey(2), "00")) >= Format(CDate(Me.lblDataAtual.Caption), "YYYYMM") Then
        
        cmdExcluirClientesSelec.Enabled = True
        
        'If vQtdTracos = 5 Then
        If Node.Image = "Cliente" Then
            cmdExcluirCliente.Enabled = True
            cmdExcluirClientesDia.Enabled = False
            cmdExcluirClientesSemana.Enabled = False
    
        'Se for 4 significa que o usuario clicou no NODE com data
        ElseIf Node.Image = "Dia" Then
            cmdExcluirCliente.Enabled = False
            cmdExcluirClientesDia.Enabled = True
            cmdExcluirClientesSemana.Enabled = False
    
        'Se for 3 significa que o usuario clicou no NODE com Semana
        ElseIf Node.Image = "Semana" Then
            cmdExcluirCliente.Enabled = False
            cmdExcluirClientesDia.Enabled = False
            cmdExcluirClientesSemana.Enabled = True
    
        End If
        
    Else
    
        cmdExcluirCliente.Enabled = False
        cmdExcluirClientesDia.Enabled = False
        cmdExcluirClientesSemana.Enabled = False
        cmdExcluirClientesSelec.Enabled = False
        
    End If

End Sub

'// occurs when the user starts dragging
'// this is where you assign the effect and the data.
Private Sub trv_OLEStartDrag(Data As MSComctlLib.DataObject, AllowedEffects As Long)
    
    Dim vKey
    vKey = Split(trv.SelectedItem.Key, "_")
    If UBound(vKey) < 2 Then Exit Sub
    If Val(vKey(2)) < Month(CDate(lblDataAtual.Caption)) Then Exit Sub
    
    If trv.SelectedItem.Image <> "Cliente" Then
        Exit Sub
    End If
    
    '// Set the effect to move
    AllowedEffects = vbDropEffectMove
    
    '// Assign the selected item's key to the DataObject
    Data.SetData trv.SelectedItem.Key
    
    '// we are dragging from this control
    blnDragging = True
    
End Sub

'// occurs when the object is dragged over the control.
'// this is where you check to see if the mouse is over
'// a valid drop object
Private Sub trv_OLEDragOver(Data As MSComctlLib.DataObject, Effect As Long, Button As Integer, Shift As Integer, x As Single, y As Single, state As Integer)
            
    Dim nodNode As Node
    
    '// set the effect
    Effect = vbDropEffectMove
    
    '// get the node that the object is being dragged over
    Set nodNode = trv.HitTest(x, y)
    
    If nodNode Is Nothing Or blnDragging = False Then
        '// the dragged object is not over a node, invalid drop target
        '// or the object is not from this control.
        Effect = vbDropEffectNone
    End If

End Sub

'// occurs when the user drops the object
'// this is where you move the node and its children.
'// this will not occur if Effect = vbDropEffectNone
Private Sub trv_OLEDragDrop(Data As MSComctlLib.DataObject, Effect As Long, Button As Integer, Shift As Integer, x As Single, y As Single)
    
    On Error GoTo Trata_Erro
    Dim vKey
    Dim vDataAtual
    Dim vData As String
    Dim strSourceKey As String
    Dim nodTarget    As Node
    
    '// get the carried data
    strSourceKey = Data.GetData(vbCFText)
    
    '// get the target node
    Set nodTarget = trv.HitTest(x, y)
    
    If nodTarget.Image <> "Dia" Then
        Set nodTarget = Nothing
        Exit Sub
    End If
    
    vDataAtual = Split(strSourceKey, "_")
    
    vData = Formata_Data(CDate(Format(vDataAtual(4), "00") & "/" & Format(vDataAtual(2), "00") & "/" & Format(vDataAtual(1), "0000")))
    
    '// if the target node is not a folder or the root item
    '// then get it's parent (that is a folder or the root item)
    If UCase(nodTarget.Image) = "CLIENTE" Then
        Set nodTarget = nodTarget.Parent
    End If
    
    If nodTarget.Image <> "Dia" Then Exit Sub
        
    vKey = Split(nodTarget.Key, "_")
    
    If Val(vKey(2)) < Month(CDate(Me.lblDataAtual.Caption)) Then
        Set nodTarget = Nothing
        Exit Sub
    End If
        
    '// move the source node to the target node
    Set trv.Nodes(strSourceKey).Parent = nodTarget
    
    'Atualizar a base de dados com a nova data para o cliente
        stbCadastro.Panels(1).Text = "Mudando data da visita do cliente... Aguarde..."
        stbCadastro.Refresh

        Conectar
        
        vBanco.Parameters.Remove "PM_CLIENTE"
        vBanco.Parameters.Add "PM_CLIENTE", Val(trv.SelectedItem.Text), 1
        
        vBanco.Parameters.Remove "PM_PROMOTOR"
        vBanco.Parameters.Add "PM_PROMOTOR", Val(cboPromotor), 1
        
        vBanco.Parameters.Remove "PM_DATA"
        vBanco.Parameters.Add "PM_DATA", vData, 1

        vBanco.Parameters.Remove "PM_DATA_FIM"
        vBanco.Parameters.Add "PM_DATA_FIM", vData, 1

        vBanco.Parameters.Remove "PM_TIPO"
        vBanco.Parameters.Add "PM_TIPO", 1, 1
        
        vBanco.ExecuteSQL "Begin PRODUCAO.PCK_CAD035.PR_DELETE_CLIENTE_TRV(:PM_CLIENTE, :PM_PROMOTOR, :PM_DATA, :PM_DATA_FIM, :PM_TIPO);END;"

        vBanco.Parameters.Remove "PM_DATA"
        vBanco.Parameters.Add "PM_DATA", Formata_Data(Trim(Mid(nodTarget.Text, 1, InStr(1, nodTarget.Text, "-") - 1))), 1

        vBanco.ExecuteSQL "Begin PRODUCAO.PCK_CAD035.PR_INS_VISITA(:PM_PROMOTOR, :PM_CLIENTE, :PM_DATA);END;"
        
        cmdGravarComentario_Click
        
        vBanco.Parameters.Remove "PM_CLIENTE"
        vBanco.Parameters.Remove "PM_PROMOTOR"
        vBanco.Parameters.Remove "PM_DATA"
        vBanco.Parameters.Remove "PM_DATA_FIM"
        vBanco.Parameters.Remove "PM_TIPO"
        
        Desconectar

        trv.SelectedItem.Parent.Text = Trim(Left(trv.SelectedItem.Parent.Text, 14)) & " - " & trv.SelectedItem.Parent.Children
        
        'frmAguardar.Show
        
        'TotalPorSemana
    
        'Unload frmAguardar
    
    '// NOTE: You will also need to update the key to reflect the changes
    '// if you are using it
    '// we are not dragging from this control any more
    blnDragging = False
    '// cancel effect so that VB doesn't muck up your transfer
    Effect = 0
    
Trata_Erro:
    If Err.Number = 440 And InStr(1, UCase(Err.Description), "UNIQUE CONSTRAINT") Then
        Resume Next
    ElseIf Err.Number <> 0 Then
        MsgBox "Sub OleDragDrop" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Sub

Sub MoverParaDataAtual()
    Dim i As Integer
    For i = 1 To Me.trv.Nodes.Count
        If UCase(trv.Nodes(i).Text) = UCase(Format(CDate(lblDataAtual), "MMMM")) Then
                trv.Nodes(i).EnsureVisible
                Exit For
            End If
    Next
End Sub



Function fPegarQtdeClientesRoteirizados(pPromotor As Long) As Long

    Dim vObj As Object
    
    Criar_Cursor
    
    vBanco.Parameters.Remove "PM_PROMOTOR"
    vBanco.Parameters.Add "PM_PROMOTOR", pPromotor, 1
    
    vBanco.ExecuteSQL "Begin PRODUCAO.PCK_CAD035.PR_SEL_CLIENTES_ROTEIRIZADOS(:vCursor, :PM_PROMOTOR);END;"
    Set vObj = vBanco.Parameters("vCursor").Value
    
    fPegarQtdeClientesRoteirizados = Val(0 & vObj!qtde)

    Set vObj = Nothing
    
    vBanco.Parameters.Remove "PM_PROMOTOR"
    
End Function

Public Sub Preencher_Cbo_Promotor(pForm As Form)

    Dim vObj As Object
    Dim i As Integer
    
    Conectar
    
    Criar_Cursor
    
    vBanco.Parameters.Remove "PM_LOGIN"
    vBanco.Parameters.Add "PM_LOGIN", Val(vLogin), 1
    
    If vTipo = "G" Then
        vBanco.ExecuteSQL "Begin PRODUCAO.PCK_CAD035.PR_SEL_PROMOTOR_GERENTE(:vCursor, :PM_LOGIN);END;"
    ElseIf vTipo = "C" Then
        vBanco.ExecuteSQL "Begin PRODUCAO.PCK_CAD035.PR_SEL_PROMOTOR_COORD(:vCursor, :PM_LOGIN);END;"
    ElseIf vTipo = "D" Then
        vBanco.ExecuteSQL "Begin PRODUCAO.PCK_CAD035.PR_SEL_PROMOTOR_GER_DIVISAO(:vCursor, :PM_LOGIN);END;"
    End If
    
    Set vObj = vBanco.Parameters("vCursor").Value
    
    pForm.cboPromotor.Clear
    
    For i = 1 To vObj.RecordCount
    
        pForm.cboPromotor.AddItem Format(vObj!COD_REPRES, "0000") & " - " & vObj!PSEUDONIMO
        vObj.MoveNext
        
    Next
    
    Set vObj = Nothing
    vBanco.Parameters.Remove "PM_LOGIN"
    
    Desconectar
    
End Sub

Sub PreencherClientes(Optional ByRef pOrdem As String)
    
    Dim vObj As Object
    Dim vObjParam As Object
    Dim i As Integer
    Dim lItem As ListItem
    Dim vClienteNovoSemRoteiro As Integer
    Dim vClientesSemRoteiro As Integer
    Dim vOrder As String
    Dim vClientes
    Dim vFiltro As String
    
    Me.stbCadastro.Panels(1).Text = "Selecionando... Aguarde..."
    Me.stbCadastro.Refresh
    
    vClientes = Split(txtFiltrar, ";")
    
    For i = 0 To UBound(vClientes)
        If Trim(vClientes(i)) <> "" Then
            If vFiltro = "" Then
                vFiltro = vClientes(i)
            Else
                vFiltro = vFiltro & "," & vClientes(i)
            End If
        End If
    Next
    
    vOrder = fPegarCampo(cboOrder1) & IIf(cboOrder2 <> "", "," & fPegarCampo(cboOrder2), "") & IIf(cboOrder3 <> "", "," & fPegarCampo(cboOrder3), "") & IIf(cboOrder4 <> "", "," & fPegarCampo(cboOrder4), "")
    
    'Ordena o select pelo nome da coluna passada no parametro
    
    If pOrdem <> "" Then
    
        vOrder = pOrdem
    
    End If
    
    Conectar
    
    Criar_Cursor
    
    vBanco.Parameters.Remove "PM_PROMOTOR"
    vBanco.Parameters.Add "PM_PROMOTOR", Val(cboPromotor), 1
    
    vBanco.Parameters.Remove "PM_CARTEIRA"
    vBanco.Parameters.Add "PM_CARTEIRA", Val(cboCarteira), 1
    
    vBanco.Parameters.Remove "PM_ORDER"
    vBanco.Parameters.Add "PM_ORDER", vOrder, 1
    
    vBanco.Parameters.Remove "PM_FILTRO"
    vBanco.Parameters.Add "PM_FILTRO", vFiltro, 1
    
    vBanco.ExecuteSQL "Begin PRODUCAO.PCK_CAD035.PR_SEL_CLIENTES_PROMOTOR(:vCursor, :PM_PROMOTOR, :PM_CARTEIRA, :PM_ORDER, :PM_FILTRO);END;"
    Set vObj = vBanco.Parameters("vCursor").Value
    
    Me.stbCadastro.Panels(1).Text = "Preenchendo... Aguarde..."
    Me.stbCadastro.Refresh
    
    lsv.ListItems.Clear
    
    lsv.CheckBoxes = True
    
    For i = 1 To vObj.RecordCount
        
        Set lItem = lsv.ListItems.Add
        
        'lItem.SubItems(1) = vObj!Cod_Cliente
        'lItem.SubItems(2) = vObj!nome_cliente
        'lItem.SubItems(3) = Val(0 & vObj!QTD_VISITAS)
        'lItem.SubItems(4) = vObj!COD_REPRES
        'lItem.SubItems(5) = vObj!NOME_CIDADE
        'lItem.SubItems(6) = vObj!BAIRRO
        'lItem.SubItems(7) = vObj!COD_UF
        'lItem.SubItems(8) = vObj!ENDERECO
        'lItem.SubItems(9) = "" & vObj!COD_QUADRANTE
        'lsv.ListItems(i).ListSubItems(9).ToolTipText = "" & vObj!DESC_QUADR
        'lItem.SubItems(10) = "" & vObj!DESC_TIPO_CLI
        'lItem.SubItems(11) = "" & vObj!DESC_SEGMENTO
        'lItem.SubItems(12) = "" & vObj!DT_ULT_COMPRA
        'lItem.SubItems(13) = "" & vObj!DT_ULT_COMPRA_INDENT
        '
        'If lItem.SubItems(12) = "" And lItem.SubItems(3) = 0 Then
        'vClienteNovoSemRoteiro = vClienteNovoSemRoteiro + 1
        'ElseIf lItem.SubItems(12) <> "" And lItem.SubItems(3) = 0 Then
        'vClientesSemRoteiro = vClientesSemRoteiro + 1
        'End If
        'lItem.SubItems(14) = "" & vObj!CEP
        
        lItem.SubItems(1) = vObj!Cod_Cliente
        lItem.SubItems(2) = vObj!nome_cliente
        lItem.SubItems(3) = Val(0 & vObj!QTD_VISITAS)
        lItem.SubItems(4) = vObj!COD_REPRES
        lItem.SubItems(5) = vObj!NOME_CIDADE
        lItem.SubItems(6) = vObj!BAIRRO
        lItem.SubItems(7) = vObj!COD_UF
        lItem.SubItems(8) = vObj!ENDERECO
        lItem.SubItems(9) = "" & vObj!CEP
        lItem.SubItems(10) = "" & vObj!COD_QUADRANTE
        lsv.ListItems(i).ListSubItems(10).ToolTipText = "" & vObj!DESC_QUADR
        lItem.SubItems(11) = "" & vObj!DESC_TIPO_CLI
        lItem.SubItems(12) = "" & vObj!DESC_SEGMENTO
        lItem.SubItems(13) = "" & vObj!DT_ULT_COMPRA
        lItem.SubItems(14) = "" & vObj!DT_ULT_COMPRA_INDENT
        
        If lItem.SubItems(13) = "" And lItem.SubItems(3) = 0 Then
            vClienteNovoSemRoteiro = vClienteNovoSemRoteiro + 1
        ElseIf lItem.SubItems(13) <> "" And lItem.SubItems(3) = 0 Then
            vClientesSemRoteiro = vClientesSemRoteiro + 1
        End If
        vObj.MoveNext
    Next
        
    lblTotalClientes.Caption = vObj.RecordCount
    lblTotalClientesSemRoteiro.Caption = vClienteNovoSemRoteiro
    lblTotalClientesNaoRoteirizados.Caption = vClientesSemRoteiro
    
    Set vObj = Nothing
    
    vBanco.Parameters.Remove "PM_PROMOTOR"
    
    Desconectar

    Me.stbCadastro.Panels(1).Text = "Pronto"
    Me.stbCadastro.Refresh

End Sub
Sub Preencher_Datas_Com_Clientes()
    
    Dim vObj As Object
    Dim vObjParam As Object
    Dim i As Integer
    Dim ii As Integer
    Dim lItem As ListItem
    Dim vQtdDia As Long
    Dim vUltNoEncontrado As Long
    
    Me.stbCadastro.Panels(1).Text = "Selecionando... Aguarde..."
    Me.stbCadastro.Refresh
    
    Conectar
    
    Criar_Cursor
    
    vBanco.Parameters.Remove "PM_PROMOTOR"
    vBanco.Parameters.Add "PM_PROMOTOR", Val(cboPromotor), 1
    
    vBanco.Parameters.Remove "PM_INICIO"
    vBanco.Parameters.Add "PM_INICIO", Formata_Data(CDate(Me.lblDataInicio.Caption)), 1
    
    vBanco.Parameters.Remove "PM_FIM"
    vBanco.Parameters.Add "PM_FIM", Formata_Data(CDate(Me.lblDataFim.Caption)), 1
    
    vBanco.ExecuteSQL "Begin PRODUCAO.PCK_CAD035.PR_SEL_CLIENTES_DATAS(:vCursor, :PM_PROMOTOR, :PM_INICIO, :PM_FIM);END;"
    Set vObj = vBanco.Parameters("vCursor").Value
    
    Me.stbCadastro.Panels(1).Text = "Preenchendo Datas com clientes... Aguarde..."
    Me.stbCadastro.Refresh
    
    pb.Max = IIf(vObj.RecordCount = 0, 1, vObj.RecordCount)
    
    vUltNoEncontrado = 1
    
    For i = 1 To vObj.RecordCount
        For ii = vUltNoEncontrado To trv.Nodes.Count
            If trv.Nodes(ii).Image = "Dia" Then
                If CDate(Mid(trv.Nodes(ii).Text, 1, 8)) = CDate(vObj!DT_VISITA) Then
                    trv.Nodes.Add trv.Nodes(ii).Key, tvwChild, trv.Nodes(ii).Key & "_C" & vObj!Cod_Cliente, Format(vObj!Cod_Cliente, "000000") & " - " & vObj!nome_cliente, "Cliente"
                    vUltNoEncontrado = ii
                    Exit For
                End If
            End If
            
        Next
        
        vObj.MoveNext
        
        Me.pb.Value = pb.Value + 1
        
    Next
        
    Set vObj = Nothing
    
    vBanco.Parameters.Remove "PM_PROMOTOR"
    vBanco.Parameters.Remove "PM_INICIO"
    vBanco.Parameters.Remove "PM_FIM"
    
    Desconectar
    
    pb.Value = 0
    
    Me.stbCadastro.Panels(1).Text = "Pronto"
    Me.stbCadastro.Refresh
    
End Sub

Sub Pintar_Parametros()
    
    Dim vObj As Object
    Dim vObjParam As Object
    Dim i As Integer
    Dim ii As Integer
    Dim lItem As ListItem
    Dim vVlrParam
    Dim vPintarSoMesAtual As Byte
    Dim vMesAtual
    
    Me.stbCadastro.Panels(1).Text = "Consultando... Aguarde..."
    
    Conectar
    
    Criar_Cursor
    
    vBanco.ExecuteSQL "Begin PRODUCAO.PCK_CAD035.PR_SEL_PARAMETROS_MES(:vCursor);END;"
    Set vObj = vBanco.Parameters("vCursor").Value
    
    If IsNull(vObj!VLR_PARAM) Then
        MsgBox "Por favor, efetue o cadastro dos par�metros.", vbInformation, "Aten��o"
        End
    End If
    
    vMesAtual = Split(vObj!VLR_PARAM, ";")
    
    vPintarSoMesAtual = Val(fPegarVlrParametro("CHK_PINTAR_MES_ATUAL"))
    
    vBanco.ExecuteSQL "Begin PRODUCAO.PCK_CAD035.PR_SEL_PARAMETROS_DIAS(:vCursor);END;"
    Set vObj = vBanco.Parameters("vCursor").Value
    
    stbCadastro.Panels(1).Text = "Preenchendo... Aguarde..."
    
    For i = 1 To trv.Nodes.Count
            
            If vPintarSoMesAtual = 1 Then
                If trv.Nodes(i).Image = "Mes" Then
                    If Mid(trv.Nodes(i).Key, 2, 4) & Format(Mid(trv.Nodes(i).Key, 7), "00") = Format(CDate(lblDataAtual), "YYYYMM") Then
                        
                        If Mid(trv.Nodes(i), InStr(trv.Nodes(i), "-") + 1) < Val(vMesAtual(0)) Then
                            trv.Nodes(i).ForeColor = vbRed
                        ElseIf Mid(trv.Nodes(i), InStr(trv.Nodes(i), "-") + 1) > Val(vMesAtual(1)) Then
                            trv.Nodes(i).ForeColor = vbBlue
                        Else
                            trv.Nodes(i).ForeColor = vbGreen
                        End If
                        
                    End If
                
                'Pintar somente os dias do mes atual
                ElseIf trv.Nodes(i).Image = "Dia" Then
                    If Format(CDate(Mid(trv.Nodes(i), 1, InStr(trv.Nodes(i), "-") - 1)), "YYYYMM") = Format(CDate(lblDataAtual), "YYYYMM") Then
                    
                        vObj.MoveFirst
                        For ii = 1 To vObj.RecordCount
                                    
                            vVlrParam = Split(vObj!VLR_PARAM, ";")
                        
                            If UCase(Right(Replace(trv.Nodes(i).Text, "�", "a"), 3)) = UCase(Mid(vObj!VLR_PARAM, 1, 3)) Then
                                
                                'Esta abaixo do minimo
                                If trv.Nodes(i).Children < Val(vVlrParam(1)) Then
                                    trv.Nodes(i).ForeColor = vbRed
                                    Exit For
                                
                                'Esta acima do maximo
                                ElseIf trv.Nodes(i).Children > Val(vVlrParam(2)) Then
                                    trv.Nodes(i).ForeColor = vbBlue
                                    Exit For
                                Else
                                    trv.Nodes(i).ForeColor = vbGreen
                                    Exit For
                                End If
                        
                            End If
                            vObj.MoveNext
                        Next
                        
                    End If
                End If
            Else
                If trv.Nodes(i).Image = "Mes" Then
                
                    If Mid(trv.Nodes(i).Key, 2, 4) & Format(Mid(trv.Nodes(i).Key, 7), "00") = Format(CDate(lblDataAtual), "YYYYMM") Then
                        
                        If Mid(trv.Nodes(i), InStr(trv.Nodes(i), "-") + 1) < Val(vMesAtual(0)) Then
                            trv.Nodes(i).ForeColor = vbRed
                        ElseIf Mid(trv.Nodes(i), InStr(trv.Nodes(i), "-") + 1) > Val(vMesAtual(1)) Then
                            trv.Nodes(i).ForeColor = vbBlue
                        Else
                            trv.Nodes(i).ForeColor = vbGreen
                        End If
                        
                    End If
                
                'Pintar todos os dias de todos os meses
                ElseIf trv.Nodes(i).Image = "Dia" Then
                    
                    vObj.MoveFirst
                    For ii = 1 To vObj.RecordCount
                                
                        vVlrParam = Split(vObj!VLR_PARAM, ";")
                    
                        If UCase(Right(Replace(trv.Nodes(i).Text, "�", "a"), 3)) = UCase(Mid(vObj!VLR_PARAM, 1, 3)) Then
                            
                            'Esta abaixo do minimo
                            If trv.Nodes(i).Children < Val(vVlrParam(1)) Then
                                trv.Nodes(i).ForeColor = vbRed
                                Exit For
                            
                            'Esta acima do maximo
                            ElseIf trv.Nodes(i).Children > Val(vVlrParam(2)) Then
                                trv.Nodes(i).ForeColor = vbBlue
                                Exit For
                            Else
                                trv.Nodes(i).ForeColor = vbGreen
                                Exit For
                            End If
                    
                        End If
                        vObj.MoveNext
                    Next
                    
                End If
            
            End If
        
    Next
        
    Set vObj = Nothing
    
    Desconectar
    
    Me.stbCadastro.Panels(1).Text = "Pronto"

End Sub

Sub ContarDiasComClientesNoPeriodo()
    
    Dim vObj As Object
    Dim vContarSabado As Byte
    Dim vContarDomingo As Byte
    
    Criar_Cursor
    
    vBanco.Parameters.Remove "PM_PROMOTOR"
    vBanco.Parameters.Add "PM_PROMOTOR", Val(cboPromotor), 1
    
    vBanco.Parameters.Remove "PM_PERIODO"
    vBanco.Parameters.Add "PM_PERIODO", Format(frmCadastro.lblDataAtual, "YYYYMM"), 1
    
    vBanco.ExecuteSQL "Begin PRODUCAO.PCK_CAD035.PR_SEL_QTD_DIAS_COM_CLIENTES(:vCursor, :PM_PROMOTOR, :PM_PERIODO);END;"
    Set vObj = vBanco.Parameters("vCursor").Value
    
    Me.lblQtdDiasComClientes.Caption = vObj!qtde
        
    Set vObj = Nothing
    
    vBanco.Parameters.Remove "PM_PROMOTOR"
    vBanco.Parameters.Remove "PM_PERIODO"
    
End Sub

Sub ContarDiasSemClientesNoPeriodo()
    
    Dim vObj As Object
    Dim vContarSabado As Boolean
    Dim vContarDomingo As Boolean
    Dim vQtdDias As Integer
    Dim i As Integer
    
    vContarSabado = IIf(Val(fPegarVlrParam("CHK_CONTAR_SABADO")) = 1, True, False)
    vContarDomingo = IIf(Val(fPegarVlrParam("CHK_CONTAR_DOMINGO")) = 1, True, False)
    
    stbCadastro.Panels(1).Text = "Contando Dias Sem Clientes no Per�odo...Aguarde..."
    
    For i = 1 To trv.Nodes.Count
    
        If trv.Nodes(i).Image = "Dia" And trv.Nodes(i).Children = 0 Then
            If Format(CDate(Left(trv.Nodes(i).Text, 8)), "YYYYMM") = Format(CDate(Me.lblDataAtual.Caption), "YYYYMM") Then
                If UCase(Right(trv.Nodes(i), 3)) = "SAB" And vContarSabado = True Then
                    vQtdDias = vQtdDias + 1
                ElseIf UCase(Right(trv.Nodes(i), 3)) = "DOM" And vContarDomingo = True Then
                    vQtdDias = vQtdDias + 1
                ElseIf UCase(Right(trv.Nodes(i), 3)) <> "SAB" And UCase(Right(trv.Nodes(i), 3)) <> "DOM" Then
                    vQtdDias = vQtdDias + 1
                End If
            End If
        End If
        
    Next
    
    Me.lblQtdDiasSemClientes.Caption = vQtdDias
    
    Me.stbCadastro.Panels(1).Text = ""
    
End Sub

Sub DiaComMaiorQtdeClientesNoPeriodo()
    Dim vObj As Object
    
    Criar_Cursor
    
    vBanco.Parameters.Remove "PM_PROMOTOR"
    vBanco.Parameters.Add "PM_PROMOTOR", Val(cboPromotor), 1
    
    vBanco.Parameters.Remove "PM_PERIODO"
    vBanco.Parameters.Add "PM_PERIODO", Format(frmCadastro.lblDataAtual, "YYYYMM"), 1
    
    vBanco.ExecuteSQL "Begin PRODUCAO.PCK_CAD035.PR_SELECT_DT_MAIOR_QTD_CLIENT(:vCursor, :PM_PROMOTOR, :PM_PERIODO);END;"
    Set vObj = vBanco.Parameters("vCursor").Value
    
    lblDiaComMaiorNumCliente.Caption = vObj!DT_VISITA & " - " & vObj!qtde
    
    Set vObj = Nothing
    
    vBanco.Parameters.Remove "PM_PROMOTOR"
    vBanco.Parameters.Remove "PM_PERIODO"
    
End Sub

Sub DiaComMenorQtdeClientesNoPeriodo()
    Dim vObj As Object
    
    Criar_Cursor
    
    vBanco.Parameters.Remove "PM_PROMOTOR"
    vBanco.Parameters.Add "PM_PROMOTOR", Val(cboPromotor), 1
    
    vBanco.Parameters.Remove "PM_PERIODO"
    vBanco.Parameters.Add "PM_PERIODO", Format(frmCadastro.lblDataAtual, "YYYYMM"), 1
    
    vBanco.ExecuteSQL "Begin PRODUCAO.PCK_CAD035.PR_SELECT_DT_MENOR_QTD_CLIENT(:vCursor, :PM_PROMOTOR, :PM_PERIODO);END;"
    Set vObj = vBanco.Parameters("vCursor").Value
    
    lblDiaComMenorNumCliente.Caption = vObj!DT_VISITA & " - " & vObj!qtde
    
    Set vObj = Nothing
    
    vBanco.Parameters.Remove "PM_PROMOTOR"
    vBanco.Parameters.Remove "PM_PERIODO"
    
End Sub

Function fPegarVlrParametro(pNomeParametro As String) As String
    
    Dim vObj As Object
    
    Criar_Cursor
    
    vBanco.Parameters.Remove "PM_NOME"
    vBanco.Parameters.Add "PM_NOME", pNomeParametro, 1
    
    vBanco.ExecuteSQL "Begin PRODUCAO.PCK_CAD035.PR_SEL_PARAMETROS(:vCursor, :PM_NOME);END;"
    Set vObj = vBanco.Parameters("vCursor").Value
    
    If vObj.EOF = True Then
        MsgBox "Cadastre o parametro de Pintar Mes Atual", vbInformation, "Aten��o"
        fPegarVlrParametro = ""
    Else
        fPegarVlrParametro = vObj!VLR_PARAM
    End If
    
    Set vObj = Nothing
    vBanco.Parameters.Remove "PM_NOME"
    
End Function

Function fTotalVisitasPeriodo() As String
    
    Dim vObj As Object
    
    frmCadastro.stbCadastro.Panels(1).Text = "Selecionando Total de Visitas...Aguarde..."
    
    Criar_Cursor
    
    vBanco.Parameters.Remove "PM_PROMOTOR"
    vBanco.Parameters.Add "PM_PROMOTOR", Val(cboPromotor), 1
    
    vBanco.Parameters.Remove "PM_PERIODO"
    vBanco.Parameters.Add "PM_PERIODO", Format(frmCadastro.lblDataAtual, "YYYYMM"), 1
    
    vBanco.ExecuteSQL "Begin PRODUCAO.PCK_CAD035.PR_SEL_QTD_VISITAS_PERIODO(:vCursor, :PM_PROMOTOR, :PM_PERIODO);END;"
    Set vObj = vBanco.Parameters("vCursor").Value
    
    fTotalVisitasPeriodo = vObj!qtde
    
    Set vObj = Nothing
    
    vBanco.Parameters.Remove "PM_PROMOTOR"
    vBanco.Parameters.Remove "PM_PERIODO"
    
    frmCadastro.stbCadastro.Panels(1).Text = "Pronto"
    
End Function

Function fTotalComentariosPeriodo() As String
    
    Dim vObj As Object
    
    frmCadastro.stbCadastro.Panels(1).Text = "Selecionando Total de Coment�rios...Aguarde..."
    
    Criar_Cursor
    
    vBanco.Parameters.Remove "PM_PROMOTOR"
    vBanco.Parameters.Add "PM_PROMOTOR", Val(cboPromotor), 1
    
    vBanco.Parameters.Remove "PM_PERIODO"
    vBanco.Parameters.Add "PM_PERIODO", Format(frmCadastro.lblDataAtual, "YYYYMM"), 1
    
    vBanco.ExecuteSQL "Begin PRODUCAO.PCK_CAD035.PR_SEL_QTD_COMENT_PERIODO(:vCursor, :PM_PROMOTOR, :PM_PERIODO);END;"
    Set vObj = vBanco.Parameters("vCursor").Value
    
    fTotalComentariosPeriodo = vObj!qtde
    
    Set vObj = Nothing
    
    vBanco.Parameters.Remove "PM_PROMOTOR"
    vBanco.Parameters.Remove "PM_PERIODO"
    
    frmCadastro.stbCadastro.Panels(1).Text = "Pronto"
   
End Function

Function fContarCaracteresTraco(pString As String) As Byte
    Dim i As Byte
    For i = 1 To Len(pString)
        If Mid(pString, i, 1) = "_" Then
            fContarCaracteresTraco = fContarCaracteresTraco + 1
        End If
    Next
End Function

Private Sub InserirCliente(pPromotor As Long, pCliente As Long, pData As String)

    On Error GoTo Trata_Erro

    vBanco.Parameters.Remove "PM_PROMOTOR"
    vBanco.Parameters.Add "PM_PROMOTOR", pPromotor, 1
    
    vBanco.Parameters.Remove "PM_CLIENTE"
    vBanco.Parameters.Add "PM_CLIENTE", pCliente, 1
    
    vBanco.Parameters.Remove "PM_DATA"
    vBanco.Parameters.Add "PM_DATA", Formata_Data(CDate(pData)), 1
    
    vBanco.ExecuteSQL "Begin PRODUCAO.PCK_CAD035.PR_INS_VISITA(:PM_PROMOTOR, :PM_CLIENTE, :PM_DATA);END;"

    vBanco.Parameters.Remove "PM_PROMOTOR"
    vBanco.Parameters.Remove "PM_CLIENTE"
    vBanco.Parameters.Remove "PM_DATA"

Trata_Erro:
    If Err.Number <> 0 Then
        If Err.Number = 440 And InStr(1, UCase(Err.Description), "UNIQUE CONSTRAINT") <> 0 Then
            Resume Next
        ElseIf Err.Number <> 0 Then
            MsgBox "Sub InserirCliente" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl
        End If
    End If
End Sub

Function fPegarVlrParam(pNomeParam As String) As String
    Dim vObj As Object
    Dim vContarSabado As Byte
    Dim vContarDomingo As Byte
    Dim vDesconectar As Boolean
    
    If vBanco Is Nothing Then
        vDesconectar = True
        Conectar
    End If
    
    Criar_Cursor
    
    vBanco.Parameters.Remove "PM_NOME"
    vBanco.Parameters.Add "PM_NOME", pNomeParam, 1
    
    vBanco.ExecuteSQL "Begin PRODUCAO.PCK_CAD035.PR_SEL_PARAMETROS(:vCursor, :PM_NOME);END;"
    Set vObj = vBanco.Parameters("vCursor").Value
    
    If vObj.EOF = False Then
        fPegarVlrParam = vObj!VLR_PARAM
    Else
        vContarSabado = ""
    End If
    
    Set vObj = Nothing
    
    vBanco.Parameters.Remove "PM_NOME"
    
    If vDesconectar = True Then
        Desconectar
    End If
    
End Function

Sub TotalPorSemana()
    
    Dim vObj As Object
    Dim i As Integer
    Dim ii As Integer
    Dim vUltPosicao As Long
    
    Conectar
    
    Criar_Cursor
    
    vBanco.Parameters.Remove "PM_PROMOTOR"
    vBanco.Parameters.Add "PM_PROMOTOR", Val(cboPromotor), 1
    
    vBanco.Parameters.Remove "PM_INICIO"
    vBanco.Parameters.Add "PM_INICIO", Formata_Data(CDate(lblDataInicio.Caption)), 1
    
    vBanco.Parameters.Remove "PM_FIM"
    vBanco.Parameters.Add "PM_FIM", Formata_Data(CDate(lblDataFim.Caption)), 1
    
    vBanco.ExecuteSQL "Begin PRODUCAO.PCK_CAD035.PR_TOTAL_POR_SEMANA(:vCursor, :PM_PROMOTOR, :PM_INICIO, :PM_FIM);END;"
    Set vObj = vBanco.Parameters("vCursor").Value
    
    If vObj.EOF = False Then
        frmCadastro.stbCadastro.Panels(1).Text = "Aguarde... Preenchedo Totais..."
        
        vUltPosicao = 1
        
        For i = 1 To vObj.RecordCount
            Me.trv.Refresh
            For ii = vUltPosicao To trv.Nodes.Count
                If Val(vObj!mes) = 0 And Val(vObj!semana) = 0 Then
                    If trv.Nodes(ii).Key = "_" & vObj!ANO And trv.Nodes(ii).Image = "Ano" Then
                        If InStrRev(trv.Nodes(ii).Text, " -") > 0 Then
                            trv.Nodes(ii).Text = Trim(Left(trv.Nodes(ii).Text, InStrRev(trv.Nodes(ii).Text, " -"))) & " - " & vObj!CLIENTES
                            vUltPosicao = ii
                            Exit For
                        Else
                            trv.Nodes(ii).Text = Trim(trv.Nodes(ii).Text) & " - " & vObj!CLIENTES
                            vUltPosicao = ii
                            Exit For
                        End If
                     End If
                Else
                    If Val(vObj!semana) = 0 And Val(vObj!mes) <> 0 Then
                        If trv.Nodes(ii).Key = "_" & vObj!ANO & "_" & Val(vObj!mes) And trv.Nodes(ii).Image = "Mes" Then
                            If InStrRev(trv.Nodes(ii).Text, " -") > 0 Then
                                trv.Nodes(ii).Text = Trim(Left(trv.Nodes(ii).Text, InStrRev(trv.Nodes(ii).Text, " -"))) & " - " & vObj!CLIENTES
                                vUltPosicao = ii
                                Exit For
                            Else
                                trv.Nodes(ii).Text = Trim(trv.Nodes(ii).Text) & " - " & vObj!CLIENTES
                                vUltPosicao = ii
                                Exit For
                            End If
                        End If
                    Else
                        'If trv.Nodes(ii).Key = "_" & vObj!ANO & "_" & Val(vObj!mes) & "_SEMANA" & vObj!semana Then
                        '    trv.Nodes(ii).Text = Trim(Left(trv.Nodes(ii).Text, 7)) & " - " & vObj!CLIENTES
                        '    vUltPosicao = ii
                        '    Exit For
                        'End If
                    End If
                End If
            Next
            vObj.MoveNext
        Next
    End If
    
    Set vObj = Nothing
    
    vBanco.Parameters.Remove "PM_PROMOTOR"
    vBanco.Parameters.Remove "PM_INICIO"
    vBanco.Parameters.Remove "PM_FIM"
    
    Desconectar
End Sub

Public Sub Selecionar_Comentario()
    
    Dim vObj As Object
    
    If UCase(trv.SelectedItem.Image) <> "CLIENTE" Then Exit Sub
    
    Conectar
    
    Criar_Cursor
    
    vBanco.Parameters.Remove "PM_PROMOTOR"
    vBanco.Parameters.Add "PM_PROMOTOR", Val(cboPromotor), 1
    
    vBanco.Parameters.Remove "PM_CLIENTE"
    vBanco.Parameters.Add "PM_CLIENTE", Val(trv.SelectedItem.Text), 1
    
    vBanco.Parameters.Remove "PM_DATA"
    vBanco.Parameters.Add "PM_DATA", Formata_Data(Trim(Mid(trv.SelectedItem.Parent, 1, InStr(1, trv.SelectedItem.Parent, "-") - 1))), 1, 1
    
    vBanco.ExecuteSQL "Begin PRODUCAO.PCK_CAD035.PR_SEL_COMENTARIO(:VCURSOR, :PM_PROMOTOR, :PM_CLIENTE, :PM_DATA);END;"
    
    Set vObj = vBanco.Parameters("vCursor").Value
    
    Me.txtComentario = "" & vObj!COMENTARIOS
    
    vBanco.Parameters.Remove "PM_PROMOTOR"
    vBanco.Parameters.Remove "PM_CLIENTE"
    vBanco.Parameters.Remove "PM_DATA"
    
    Set vObj = Nothing
    
    Desconectar
    
End Sub

Sub Resumo()
    Conectar
        
        lblTotalClientesRoteirizados.Caption = fPegarQtdeClientesRoteirizados(Val(cboPromotor))
    
        ContarDiasComClientesNoPeriodo
        
        ContarDiasSemClientesNoPeriodo
        
        DiaComMaiorQtdeClientesNoPeriodo
        
        DiaComMenorQtdeClientesNoPeriodo
        
        lblTotalVisitasPeriodo.Caption = fTotalVisitasPeriodo
        
        lblTotalComentariosPeriodo.Caption = fTotalComentariosPeriodo
    
    Desconectar
End Sub


Sub Fora_Do_Padrao(pContar As String)
    
    Dim vObj As Object
    Dim vObjParam As Object
    Dim i As Integer
    Dim ii As Integer
    Dim lItem As ListItem
    Dim vVlrParam
    Dim vAbaixo(30) As String
    Dim vAcima(30) As String
    Dim vContAcima As Byte
    Dim vContAbaixo As Byte
    
    Conectar
    
    Criar_Cursor
    
    vBanco.ExecuteSQL "Begin PRODUCAO.PCK_CAD035.PR_SEL_PARAMETROS_MES(:vCursor);END;"
    Set vObj = vBanco.Parameters("vCursor").Value
    
    vBanco.ExecuteSQL "Begin PRODUCAO.PCK_CAD035.PR_SEL_PARAMETROS_DIAS(:vCursor);END;"
    Set vObj = vBanco.Parameters("vCursor").Value
    
    For i = 1 To trv.Nodes.Count
            
        'Pintar somente os dias do mes atual
        If trv.Nodes(i).Image = "Dia" Then
            If Format(CDate(Mid(trv.Nodes(i), 1, InStr(trv.Nodes(i), "-") - 1)), "YYYYMM") = Format(CDate(lblDataAtual), "YYYYMM") Then
                
                vObj.MoveFirst
                
                For ii = 1 To vObj.RecordCount
                            
                    vVlrParam = Split(vObj!VLR_PARAM, ";")
                
                    If UCase(Right(Replace(trv.Nodes(i).Text, "�", "a"), 3)) = UCase(Mid(vObj!VLR_PARAM, 1, 3)) Then
                        
                        'Esta abaixo do minimo
                        If trv.Nodes(i).Children < Val(vVlrParam(1)) Then
                            vAbaixo(vContAbaixo) = Mid(trv.Nodes(i).Text, 1, 8)
                            vContAbaixo = vContAbaixo + 1
                            Exit For
                        
                        'Esta acima do maximo
                        ElseIf trv.Nodes(i).Children > Val(vVlrParam(2)) Then
                            vAcima(vContAcima) = Mid(trv.Nodes(i).Text, 1, 8)
                            vContAcima = vContAcima + 1
                            Exit For
                        End If
                
                    End If
                    vObj.MoveNext
                Next
                
            End If
        End If
        
    Next
        
    Set vObj = Nothing
    
    Desconectar
    
    If pContar = "ABAIXO" Then
        For i = 0 To 29
            If vAbaixo(i) = "" Then Exit For
            frmDiasSemClientes.lstDatas.AddItem vAbaixo(i)
        Next
        frmDiasSemClientes.Caption = "Abaixo do M�nimo"
    Else
        For i = 0 To 29
            If vAbaixo(i) = "" Then Exit For
            frmDiasSemClientes.lstDatas.AddItem vAcima(i)
        Next
        frmDiasSemClientes.Caption = "Acima do M�ximo"
    End If
    frmDiasSemClientes.Visible = True
End Sub


Sub Preencher_Combo_Carteira()
    Dim vObj As Object
    Dim i As Integer
    
    cboCarteira.Clear
    
    Conectar
    
    Criar_Cursor
    
    vBanco.Parameters.Remove "PM_PROMOTOR"
    vBanco.Parameters.Add "PM_PROMOTOR", Val(cboPromotor), 1
    
    vBanco.ExecuteSQL "Begin PRODUCAO.PCK_CAD035.PR_SEL_CARTEIRA_PROMOTOR(:VCURSOR, :PM_PROMOTOR);END;"
    
    Set vObj = vBanco.Parameters("vCursor").Value
    
    cboCarteira.AddItem "<TODOS>"
    
    For i = 1 To vObj.RecordCount
        cboCarteira.AddItem vObj!COD_REPRES & " - " & vObj!PSEUDONIMO
        vObj.MoveNext
    Next
        
    vBanco.Parameters.Remove "PM_PROMOTOR"
    
    Set vObj = Nothing
    
    Desconectar
End Sub

Function fPegarCampo(pNomeCampo As String) As String
    Select Case UCase(pNomeCampo)
        Case "C�DIGO CLIENTE"
            fPegarCampo = "C.COD_CLIENTE"
        
        Case "NOME CLIENTE"
            fPegarCampo = "C.NOME_CLIENTE"
    
        Case "CEP"
            fPegarCampo = "CEP"
    
        Case "CIDADE"
            fPegarCampo = "D.NOME_CIDADE"
        
        Case "BAIRRO"
            fPegarCampo = "C.BAIRRO"

        Case "UF"
            fPegarCampo = "D.COD_UF"

        Case "ENDERE�O"
            fPegarCampo = "C.ENDERECO"

        Case "TIPO CLIENTE"
            fPegarCampo = "G.DESC_TIPO_CLI"

        Case "SEGMENTO"
            fPegarCampo = "H.DESC_SEGMENTO"

        Case "DT ULT COMPRA"
            fPegarCampo = "I.DT_ULT_COMPRA"

        Case "DT ULT COMPRA INDENT"
            fPegarCampo = "I.DT_ULT_COMPRA_INDENT"

        Case "QUADRANTE"
            fPegarCampo = "E.COD_QUADRANTE"

        Case "NUM VISITAS"
            fPegarCampo = "F.QTD_VISITAS"

    End Select
End Function

Private Sub txtLocalizarPorCodigo_GotFocus()
    txtLocalizarPorNome = ""
    txtLocalizarPorNome.Tag = 0
End Sub

Private Sub txtLocalizarPorNome_GotFocus()
    txtLocalizarPorCodigo = ""
End Sub
