VERSION 5.00
Begin VB.Form frmAguardar 
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   2100
   ClientLeft      =   45
   ClientTop       =   45
   ClientWidth     =   6060
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2100
   ScaleWidth      =   6060
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Image Image2 
      Height          =   1905
      Left            =   4050
      Picture         =   "frmSobre.frx":0000
      Stretch         =   -1  'True
      Top             =   135
      Width           =   1905
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "as informa��es solicitadas..."
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   345
      Left            =   180
      TabIndex        =   2
      Top             =   1395
      Width           =   4050
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "O sistema est� processando"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   345
      Left            =   180
      TabIndex        =   1
      Top             =   990
      Width           =   4005
   End
   Begin VB.Label lbl 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Aguarde..."
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   1395
      TabIndex        =   0
      Top             =   270
      Width           =   1620
   End
   Begin VB.Image Image1 
      Height          =   2100
      Left            =   0
      Picture         =   "frmSobre.frx":37BD
      Stretch         =   -1  'True
      Top             =   0
      Width           =   11985
   End
End
Attribute VB_Name = "frmAguardar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Sub Form_Load()

    Screen.MousePointer = vbHourglass

End Sub
Private Sub Form_Unload(Cancel As Integer)

    Screen.MousePointer = vbDefault

End Sub

