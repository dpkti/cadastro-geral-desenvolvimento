VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmConsultaCliente 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Consulta de cliente"
   ClientHeight    =   4755
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4680
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmConsultaCliente.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4755
   ScaleWidth      =   4680
   Tag             =   "frmConsultaCliente"
   Begin MSComctlLib.StatusBar STB 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   6
      Top             =   4470
      Width           =   4680
      _ExtentX        =   8255
      _ExtentY        =   503
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   5636
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin VB.TextBox txtPesqCliente 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   780
      Width           =   4215
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   0
      TabIndex        =   4
      Top             =   540
      Width           =   4665
      _ExtentX        =   8229
      _ExtentY        =   53
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   495
      Left            =   30
      TabIndex        =   5
      ToolTipText     =   "Voltar"
      Top             =   0
      Width           =   555
      _ExtentX        =   979
      _ExtentY        =   873
      BTYPE           =   9
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsultaCliente.frx":0CCA
      PICN            =   "frmConsultaCliente.frx":0CE6
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.ListBox lstClientes 
      Appearance      =   0  'Flat
      Height          =   3150
      Left            =   0
      TabIndex        =   1
      Top             =   1290
      Width           =   4665
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "Clientes encontrados:"
      Height          =   195
      Left            =   0
      TabIndex        =   3
      Top             =   1080
      Width           =   1575
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Nome do cliente:"
      Height          =   195
      Left            =   0
      TabIndex        =   2
      Top             =   600
      Width           =   1200
   End
   Begin VB.Image imgConsultar 
      Appearance      =   0  'Flat
      Height          =   300
      Left            =   4260
      Picture         =   "frmConsultaCliente.frx":19C0
      ToolTipText     =   "Consultar Cliente"
      Top             =   750
      Width           =   300
   End
End
Attribute VB_Name = "frmConsultaCliente"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public vOrigem As String

Private Sub cmdVoltar_Click()
    Unload Me
End Sub


Private Sub Form_Load()
    Me.Top = (mdiRoteiro.Height - Me.Height) / 2 - mdiRoteiro.mnuXP.Height
    Me.Left = (mdiRoteiro.Width - Me.Width) / 2
End Sub

'Private Sub imgConsultar_Click()
'    Dim vObj As Object
'    Dim i As Integer
'
'    Me.MousePointer = vbHourglass
'
'    Stb.Panels(1).Text = "Consultando... Aguarde..."
'
'    Conectar
'
'    Criar_Cursor
'
'    vBanco.Parameters.Remove "PM_CLIENTE"
'    vBanco.Parameters.Add "PM_CLIENTE", IIf(InStr(1, txtPesqCliente, "%") > 0, txtPesqCliente, txtPesqCliente & "%"), 1
'
'    vBanco.Parameters.Remove "PM_PROMOTOR"
'    vBanco.Parameters.Add "PM_PROMOTOR", Val(Me.cboPromotor), 1
'
'    vBanco.ExecuteSQL "Begin PRODUCAO.PCK_CAD035.PR_SEL_CLIENTE(:vCursor, :PM_CLIENTE, :PM_PROMOTOR);END;"
'    Set vObj = vBanco.Parameters("vCursor").Value
'
'    lstClientes.Clear
'
'    Stb.Panels(1).Text = "Preenchendo... Aguarde..."
'
'    For i = 1 To vObj.RecordCount
'
'        lstClientes.AddItem Format(vObj!Cod_Cliente, "000000") & " - " & vObj!nome_cliente
'        vObj.MoveNext
'
'    Next
'
'    Set vObj = Nothing
'
'    Desconectar
'
'    Stb.Panels(1).Text = "Pronto"
'
'    Stb.Panels(2).Text = "Clientes: " & lstClientes.ListCount
'
'    Me.MousePointer = vbNormal
'
'End Sub

Private Sub lstClientes_DblClick()
    If vOrigem = "frmConsultas" Then
        frmConsultas.cboCliente = Trim(lstClientes.Text)
    ElseIf vOrigem = "frmCopiar" Then
        'frmCopiar.cboCliente = Trim(lstClientes.Text)
    End If
End Sub

Private Sub txtPesqCliente_KeyPress(KeyAscii As Integer)
    If KeyAscii = 42 Then
        KeyAscii = 0
    Else
        KeyAscii = Asc(UCase(Chr(KeyAscii)))
    End If
End Sub
