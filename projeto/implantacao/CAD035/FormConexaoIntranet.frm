VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{E57FB72C-1106-44AF-9706-0BA099A707C7}#4.2#0"; "XPFRAME.OCX"
Object = "{44FDCC3D-6464-11D4-889B-00105AB0D2BD}#1.0#0"; "3DLINES.OCX"
Object = "{7E65892D-F38B-11D2-BC07-0055003B26DE}#3.0#0"; "HYPERLABEL.OCX"
Begin VB.Form dlgConexaoIntranet 
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   ".:  LOGIN INTRANET  :."
   ClientHeight    =   2085
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3030
   ControlBox      =   0   'False
   Icon            =   "FormConexaoIntranet.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2085
   ScaleWidth      =   3030
   StartUpPosition =   2  'CenterScreen
   Begin Lines3D.Line3D Line3D1 
      Height          =   105
      Left            =   45
      Top             =   720
      Width           =   2940
      _ExtentX        =   5186
      _ExtentY        =   185
   End
   Begin Bot�o.cmd cmdFechar 
      Cancel          =   -1  'True
      Height          =   645
      Left            =   2340
      TabIndex        =   4
      TabStop         =   0   'False
      ToolTipText     =   "Fechar janela atual"
      Top             =   45
      Width           =   645
      _ExtentX        =   1138
      _ExtentY        =   1138
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   2
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "FormConexaoIntranet.frx":0CCA
      PICN            =   "FormConexaoIntranet.frx":0CE6
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdConectar 
      Default         =   -1  'True
      Height          =   645
      Left            =   45
      TabIndex        =   3
      TabStop         =   0   'False
      ToolTipText     =   "Efetuar a conex�o"
      Top             =   45
      Width           =   645
      _ExtentX        =   1138
      _ExtentY        =   1138
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   2
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "FormConexaoIntranet.frx":19C0
      PICN            =   "FormConexaoIntranet.frx":19DC
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin CoolXPFrame.xpFrame fraLogin 
      Height          =   1275
      Left            =   45
      Top             =   765
      Width           =   2940
      _ExtentX        =   5186
      _ExtentY        =   2249
      Caption         =   "Login"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   13977088
      BorderColor     =   8421504
      ColorStyle      =   99
      Begin VB.TextBox txtSenha 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Wingdings"
            Size            =   9
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00D54600&
         Height          =   330
         IMEMode         =   3  'DISABLE
         Left            =   675
         PasswordChar    =   "n"
         TabIndex        =   1
         Top             =   630
         Width           =   2175
      End
      Begin VB.TextBox txtUsuario 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   675
         MaxLength       =   50
         TabIndex        =   0
         Top             =   270
         Width           =   2175
      End
      Begin HyperLinkLabel.HyperLabel hlblEsqueciSenha 
         Height          =   195
         Left            =   1350
         TabIndex        =   2
         ToolTipText     =   "Esqueceu sua senha? Clique aqui"
         Top             =   990
         Width           =   1485
         _ExtentX        =   2619
         _ExtentY        =   344
         Caption         =   "Esqueci minha senha"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Hover           =   0   'False
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Senha"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   90
         TabIndex        =   6
         Top             =   675
         Width           =   450
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Usu�rio"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   90
         TabIndex        =   5
         Top             =   315
         Width           =   540
      End
   End
End
Attribute VB_Name = "dlgConexaoIntranet"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdConectar_Click()

    If txtUsuario = "" Then

        MsgBox "Informe o usu�rio da Intranet", vbInformation, "Aten��o"
        txtUsuario.SetFocus
        Exit Sub

    End If

    If txtSenha = "" Then
        
        MsgBox "Informe a senha", vbInformation, "Aten��o"
        txtSenha.SetFocus
        Exit Sub

    End If

    If fValidaLogin(txtUsuario, txtSenha) = False Then

        MsgBox "Usu�rio/Senha inv�lido", vbInformation, "Aten��o"
        txtUsuario = ""
        txtSenha = ""
        txtUsuario.SetFocus
        Exit Sub

    Else
        
        Unload Me
        mdiRoteiro.Show
        
    End If

End Sub

Private Sub cmdFechar_Click()
    Unload Me
    End
End Sub


Private Sub hlblEsqueciSenha_Click()

    Dim vRetorno

    vRetorno = ShellExecute(Me.hWnd, "Open", "http://dpknet/esquecisenha.asp", vbNullString, vbNullString, SW_SHOWMAXIMIZED)

End Sub
Private Sub txtSenha_GotFocus()

    txtSenha.SetFocus

End Sub
Private Sub txtUsuario_GotFocus()

    txtUsuario.SetFocus

End Sub
Private Sub txtUsuario_KeyPress(KeyAscii As Integer)

    KeyAscii = Asc(UCase(Chr(KeyAscii)))

End Sub
