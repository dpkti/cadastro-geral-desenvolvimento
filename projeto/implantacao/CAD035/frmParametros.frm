VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmParametros 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cadastro de Par�metros"
   ClientHeight    =   4905
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8070
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmParametros.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4905
   ScaleWidth      =   8070
   Tag             =   "frmParametros"
   Begin VB.Frame fraContar 
      Caption         =   "Contar Dias com e sem Roteiros"
      Height          =   855
      Left            =   3000
      TabIndex        =   14
      Top             =   2670
      Width           =   5025
      Begin VB.CheckBox chkContarDomingos 
         Appearance      =   0  'Flat
         Caption         =   "Contar Domingos"
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   90
         TabIndex        =   16
         Tag             =   "CONTAR_DOMINGO"
         Top             =   540
         Width           =   1575
      End
      Begin VB.CheckBox chkContarSabados 
         Appearance      =   0  'Flat
         Caption         =   "Contar S�bados"
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   90
         TabIndex        =   15
         Tag             =   "CONTAR_SABADO"
         Top             =   270
         Width           =   1575
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Intervalo para real�ar"
      Height          =   675
      Left            =   3000
      TabIndex        =   12
      Top             =   1920
      Width           =   5025
      Begin VB.CheckBox chkPintarMes 
         Appearance      =   0  'Flat
         Caption         =   "Pintar somente as datas dentro do m�s corrente"
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   90
         TabIndex        =   13
         Tag             =   "PINTAR_MES_ATUAL"
         Top             =   270
         Width           =   3825
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Valor M�nimo e M�ximo Mensal"
      Height          =   1275
      Left            =   30
      TabIndex        =   10
      Top             =   3570
      Width           =   2865
      Begin MSComctlLib.ListView lsvMes 
         Height          =   945
         Left            =   60
         TabIndex        =   11
         Top             =   240
         Width           =   2715
         _ExtentX        =   4789
         _ExtentY        =   1667
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   3
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "M�s"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   1
            Text            =   "M�nimo"
            Object.Width           =   1235
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   2
            Text            =   "M�ximo"
            Object.Width           =   1323
         EndProperty
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Par�metros de C�pia de Per�odo"
      Height          =   1215
      Left            =   3000
      TabIndex        =   6
      Top             =   630
      Width           =   5025
      Begin VB.CheckBox chkCopiarDiasUteis 
         Appearance      =   0  'Flat
         Caption         =   "Copiar somente dias �teis"
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   90
         TabIndex        =   9
         Top             =   270
         Width           =   2205
      End
      Begin VB.CheckBox chkMudarParaSegunda 
         Appearance      =   0  'Flat
         Caption         =   "Quando data de destino for S�bado mudar para Segunda-feira"
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   90
         TabIndex        =   8
         Top             =   570
         Width           =   4845
      End
      Begin VB.CheckBox chkUltrapassarMes 
         Appearance      =   0  'Flat
         Caption         =   "Permitir que o n�mero de dias do destino ultrapasse o m�s"
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   90
         TabIndex        =   7
         Top             =   870
         Width           =   4515
      End
   End
   Begin VB.Frame fraValores 
      Caption         =   "Valores M�nimos e M�ximos Di�rios"
      Height          =   2895
      Left            =   30
      TabIndex        =   0
      Top             =   630
      Width           =   2865
      Begin VB.TextBox txtMinimo 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1080
         TabIndex        =   2
         Top             =   2490
         Width           =   705
      End
      Begin VB.TextBox txtMaximo 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1830
         TabIndex        =   1
         Top             =   2490
         Width           =   735
      End
      Begin MSComctlLib.ListView lsvMatriz 
         Height          =   2205
         Left            =   60
         TabIndex        =   3
         Top             =   240
         Width           =   2715
         _ExtentX        =   4789
         _ExtentY        =   3889
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   3
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Dia"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   1
            Text            =   "M�nimo"
            Object.Width           =   1235
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   2
            Text            =   "M�ximo"
            Object.Width           =   1323
         EndProperty
      End
      Begin VB.Label lblDiaSemana 
         AutoSize        =   -1  'True
         Caption         =   "Label1"
         Height          =   195
         Left            =   90
         TabIndex        =   4
         Top             =   2520
         Width           =   465
      End
      Begin VB.Image cmdOk 
         Height          =   300
         Left            =   2520
         Picture         =   "frmParametros.frx":0CCA
         ToolTipText     =   "Gravar Dados para o Dia"
         Top             =   2490
         Visible         =   0   'False
         Width           =   300
      End
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   0
      TabIndex        =   5
      Top             =   540
      Width           =   7995
      _ExtentX        =   14102
      _ExtentY        =   53
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   495
      Left            =   0
      TabIndex        =   17
      ToolTipText     =   "Voltar"
      Top             =   0
      Width           =   555
      _ExtentX        =   979
      _ExtentY        =   873
      BTYPE           =   9
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmParametros.frx":0D82
      PICN            =   "frmParametros.frx":0D9E
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmParametros"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub chkContarDomingos_Click()
    Conectar
    
    vBanco.Parameters.Remove "PM_NOME"
    vBanco.Parameters.Remove "PM_VLR"
    vBanco.Parameters.Remove "PM_ACAO"
    
    vBanco.Parameters.Add "PM_NOME", "CHK_CONTAR_DOMINGO", 1
    vBanco.Parameters.Add "PM_VLR", chkContarDomingos.Value, 1
    vBanco.Parameters.Add "PM_ACAO", 1, 1
    
    vBanco.ExecuteSQL "Begin PRODUCAO.PCK_CAD035.PR_ACAO_PARAMETROS(:PM_ACAO, :PM_NOME, :PM_VLR);END;"
    
    vBanco.Parameters.Remove "PM_NOME"
    vBanco.Parameters.Remove "PM_VLR"
    vBanco.Parameters.Remove "PM_ACAO"
    
    Desconectar
End Sub

Private Sub chkContarSabados_Click()
    Conectar
    
    vBanco.Parameters.Remove "PM_NOME"
    vBanco.Parameters.Remove "PM_VLR"
    vBanco.Parameters.Remove "PM_ACAO"
    
    vBanco.Parameters.Add "PM_NOME", "CHK_CONTAR_SABADO", 1
    vBanco.Parameters.Add "PM_VLR", chkContarSabados.Value, 1
    vBanco.Parameters.Add "PM_ACAO", 1, 1
    
    vBanco.ExecuteSQL "Begin PRODUCAO.PCK_CAD035.PR_ACAO_PARAMETROS(:PM_ACAO, :PM_NOME, :PM_VLR);END;"
    
    vBanco.Parameters.Remove "PM_NOME"
    vBanco.Parameters.Remove "PM_VLR"
    vBanco.Parameters.Remove "PM_ACAO"
    
    Desconectar
    
End Sub

Private Sub chkCopiarDiasUteis_Click()
    Conectar
    
    vBanco.Parameters.Remove "PM_NOME"
    vBanco.Parameters.Remove "PM_VLR"
    vBanco.Parameters.Remove "PM_ACAO"
    
    vBanco.Parameters.Add "PM_NOME", "CHK_COPIAR_DIAS_UTEIS", 1
    vBanco.Parameters.Add "PM_VLR", chkCopiarDiasUteis.Value, 1
    vBanco.Parameters.Add "PM_ACAO", 1, 1
    
    vBanco.ExecuteSQL "Begin PRODUCAO.PCK_CAD035.PR_ACAO_PARAMETROS(:PM_ACAO, :PM_NOME, :PM_VLR);END;"
    
    vBanco.Parameters.Remove "PM_NOME"
    vBanco.Parameters.Remove "PM_VLR"
    vBanco.Parameters.Remove "PM_ACAO"
    
    Desconectar
End Sub

Private Sub chkMudarParaSegunda_Click()
    Conectar
    
    vBanco.Parameters.Remove "PM_NOME"
    vBanco.Parameters.Remove "PM_VLR"
    vBanco.Parameters.Remove "PM_ACAO"
    
    vBanco.Parameters.Add "PM_NOME", "CHK_FINAL_SEMANA_PARA_SEGUNDA", 1
    vBanco.Parameters.Add "PM_VLR", chkMudarParaSegunda.Value, 1
    vBanco.Parameters.Add "PM_ACAO", 1, 1
    
    vBanco.ExecuteSQL "Begin PRODUCAO.PCK_CAD035.PR_ACAO_PARAMETROS(:PM_ACAO, :PM_NOME, :PM_VLR);END;"
    
    vBanco.Parameters.Remove "PM_NOME"
    vBanco.Parameters.Remove "PM_VLR"
    vBanco.Parameters.Remove "PM_ACAO"
    
    Desconectar
End Sub

Private Sub chkPintarMes_Click()
    Conectar
    
    vBanco.Parameters.Remove "PM_NOME"
    vBanco.Parameters.Remove "PM_VLR"
    vBanco.Parameters.Remove "PM_ACAO"
    
    vBanco.Parameters.Add "PM_NOME", "CHK_PINTAR_MES_ATUAL", 1
    vBanco.Parameters.Add "PM_VLR", chkPintarMes.Value, 1
    vBanco.Parameters.Add "PM_ACAO", 1, 1
    
    vBanco.ExecuteSQL "Begin PRODUCAO.PCK_CAD035.PR_ACAO_PARAMETROS(:PM_ACAO, :PM_NOME, :PM_VLR);END;"
    
    vBanco.Parameters.Remove "PM_NOME"
    vBanco.Parameters.Remove "PM_VLR"
    vBanco.Parameters.Remove "PM_ACAO"
    
    Desconectar
End Sub

Private Sub chkUltrapassarMes_Click()
    Conectar
    
    vBanco.Parameters.Remove "PM_NOME"
    vBanco.Parameters.Remove "PM_VLR"
    vBanco.Parameters.Remove "PM_ACAO"
    
    vBanco.Parameters.Add "PM_NOME", "CHK_ULTRAPASSAR_MES", 1
    vBanco.Parameters.Add "PM_VLR", chkUltrapassarMes.Value, 1
    vBanco.Parameters.Add "PM_ACAO", 1, 1
    
    vBanco.ExecuteSQL "Begin PRODUCAO.PCK_CAD035.PR_ACAO_PARAMETROS(:PM_ACAO, :PM_NOME, :PM_VLR);END;"
    
    vBanco.Parameters.Remove "PM_NOME"
    vBanco.Parameters.Remove "PM_VLR"
    vBanco.Parameters.Remove "PM_ACAO"
    
    Desconectar
End Sub

Private Sub cmdVoltar_Click()
    Unload Me
End Sub


Private Sub Form_Load()
    Me.Top = (mdiRoteiro.Height - Me.Height) / 2 - mdiRoteiro.mnuXP.Height
    Me.Left = (mdiRoteiro.Width - Me.Width) / 2
    Preencher_Parametros_Dias
    Preencher_Parametros_Mes
    Preencher_Parametros_Check
End Sub


Private Sub lsvMatriz_ItemClick(ByVal Item As MSComctlLib.ListItem)
    lblDiaSemana.Caption = Item.Text
    txtMinimo.Text = Item.SubItems(1)
    txtMaximo.Text = Item.SubItems(2)
End Sub

Sub Preencher_Parametros_Dias()
    
    Dim vObj As Object
    Dim lItem As ListItem
    Dim i As Integer
    Dim vVlrParam
    
    Conectar
    
    Criar_Cursor
    
    vBanco.ExecuteSQL "Begin PRODUCAO.PCK_CAD035.PR_SEL_PARAMETROS_DIAS(:vCursor);END;"
    Set vObj = vBanco.Parameters("vCursor").Value
    
    For i = 1 To vObj.RecordCount
        
        vVlrParam = Split(vObj!VLR_PARAM, ";")
        
        Set lItem = lsvMatriz.ListItems.Add
        lItem = vVlrParam(0)
        lItem.SubItems(1) = vVlrParam(1)
        lItem.SubItems(2) = vVlrParam(2)
        vObj.MoveNext
    Next

    Set vObj = Nothing
    
    Desconectar
End Sub

Sub Preencher_Parametros_Mes()
    
    Dim vObj As Object
    Dim lItem As ListItem
    Dim i As Integer
    Dim vVlrParam
    
    Conectar
    
    Criar_Cursor
    
    vBanco.ExecuteSQL "Begin PRODUCAO.PCK_CAD035.PR_SEL_PARAMETROS_MES(:vCursor);END;"
    Set vObj = vBanco.Parameters("vCursor").Value
    
    For i = 1 To vObj.RecordCount
        vVlrParam = Split(vObj!VLR_PARAM, ";")
        
        Set lItem = lsvMes.ListItems.Add
        lItem = "Janeiro"
        lItem.SubItems(1) = vVlrParam(0)
        lItem.SubItems(2) = vVlrParam(1)
        vObj.MoveNext
    Next

    Set vObj = Nothing
    
    Desconectar
End Sub

Sub Preencher_Parametros_Check()
    
    Dim vObj As Object
    Dim lItem As ListItem
    Dim i As Integer
    
    Conectar
    
    Criar_Cursor
    
    vBanco.ExecuteSQL "Begin PRODUCAO.PCK_CAD035.PR_SEL_PARAMETROS_CHECK(:vCursor);END;"
    Set vObj = vBanco.Parameters("vCursor").Value
    
    For i = 1 To vObj.RecordCount
                
        If Mid(vObj!nome_param, 5) = "CONTAR_SABADO" Then
            chkContarSabados.Value = Val(vObj!VLR_PARAM)
        ElseIf Mid(vObj!nome_param, 5) = "CONTAR_DOMINGO" Then
            chkContarDomingos.Value = Val(vObj!VLR_PARAM)
        ElseIf Mid(vObj!nome_param, 5) = "PINTAR_MES_ATUAL" Then
            chkPintarMes.Value = Val(vObj!VLR_PARAM)
        ElseIf Mid(vObj!nome_param, 5) = "COPIAR_DIAS_UTEIS" Then
            chkCopiarDiasUteis.Value = Val(vObj!VLR_PARAM)
        ElseIf Mid(vObj!nome_param, 5) = "FINAL_SEMANA_PARA_SEGUNDA" Then
            chkMudarParaSegunda.Value = Val(vObj!VLR_PARAM)
        ElseIf Mid(vObj!nome_param, 5) = "ULTRAPASSAR_MES" Then
            chkUltrapassarMes.Value = Val(vObj!VLR_PARAM)
        
        End If
        
        vObj.MoveNext
    Next

    Set vObj = Nothing
    
    Desconectar
End Sub
